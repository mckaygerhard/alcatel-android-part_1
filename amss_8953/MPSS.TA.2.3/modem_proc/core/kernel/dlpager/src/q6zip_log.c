/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "q6zip_log.h"
#include "q6zip_assert.h"
#include <q6protos.h>
#include <stdio.h>

#if defined( Q6ZIP_LOG_ENABLED )
/*===========================================================================

                     CONSTANTS

===========================================================================*/
#define Q6ZIP_LOG_VERSION_1   (0x1)
#define MAX_LOG_SAMPLES       (50)
#define NUM_LOG_BUFFERS       (2)
#define CAS(a,b,c) atomic_compare_and_set(a,b,c)
/*===========================================================================

                     DEFINITONS 

===========================================================================*/
/*===========================================================================*/
/* 
 Q6ZIP Log Header Data Structure
 
 <byte 1> <byte 2> <byte 3> <byte 4> 
 
 <byte 1>            -> Version
 <byte 2>            -> No of samples
 <byte 3><byte 4>    -> Reserved 
*/
typedef struct {
	uint8 version;
	uint8 numSamples;
	uint16 reserved;
} q6zip_log_header_type;

/* 
 Q6ZIP Log Sample Data Structure
 
 <byte 1> <byte 2> .... <byte 8> 
 
 <byte 1>            -> queue - 4 bits x 2 queues 
 <byte 2>            -> req/rsp(1) + op (rw/ro/rwd/dma)(3) + padding(4)
 <byte 3..4>         -> size + seq
*/

typedef struct
{
    /* 1 byte */
    uint8 lo_q:4;
    uint8 hi_q:4;
    
    /* 1 byte */
    uint8 seq_num;

    /* 2 bytes */ 
    uint8 req_rsp:1;
    uint8 op_type:3;
    uint8 q_id:1;
    uint16 block_size:11;

    uint32 timetick;
}q6zip_log_sample_type; /* 8 bytes */

/*---------------------------------------------------------------------------
   Structure for log packets
---------------------------------------------------------------------------*/
typedef struct {
	q6zip_log_header_type header;
	q6zip_log_sample_type samples[MAX_LOG_SAMPLES];
} q6zip_log_type;

/*---------------------------------------------------------------------------
   Global Buffer State Information
---------------------------------------------------------------------------*/
typedef union {
	atomic_word_t all;
	struct {
		uint8 bufferNum;
		uint8 index;
		uint16 bufferFullStatus;
	} details;
} q6zip_log_global_state_type;


/*============================================================================
PACKET   Log Packet for q6zip (hacked) 

ID       LOG_EFS_MONITOR_C

PURPOSE  Calls diag API to construct Q6ZIP log packet

RESPONSE Returns a standard diag header and the Q6ZIP log structure 
============================================================================*/
LOG_RECORD_DEFINE (LOG_EFS_MONITOR_C)
	q6zip_log_type q6zip_log;
LOG_RECORD_END

/* Q6ZIP log buffers */
static LOG_EFS_MONITOR_C_type           q6zip_log_req_buffer[NUM_LOG_BUFFERS];
static LOG_EFS_MONITOR_C_type           q6zip_log_rsp_buffer[NUM_LOG_BUFFERS];

/* Q6ZIP dropped log packets count */
static atomic_word_t q6zip_log_dropped_packet_count;

/* Q6ZIP log current state */
static q6zip_log_global_state_type      q6zip_log_req_status;
static q6zip_log_global_state_type      q6zip_log_rsp_status;

#define IS_BUFFER_FULL(current_state) ( current_state.details.bufferFullStatus & (1 << current_state.details.bufferNum))

#define SET_BUFFER_FULL(globalCurrentState) globalCurrentState.details.bufferFullStatus |= (1 << globalCurrentState.details.bufferNum)

#define SET_SHOULDFLUSH(localBufToSet) atomic_set_bit(&q6zip_log_buf_status[localBufToSet].all, 16)

#define RESET_SHOULDFLUSH(localBufToUnset) atomic_clear_bit(&q6zip_log_buf_status[localBufToUnset].all, 16)

#define RESET_ISFULL(globalBufToUnset) atomic_clear_bit(&q6zip_log_global_state, 16 + globalBufToUnset)

/* Q6ZIP log sample drop count */
static atomic_word_t                        q6zip_log_dropped_sample_count;

static void q6zip_log_submit(uint8 BufToFlush, uint8 q6zip_log_type);
/*===========================================================================
FUNCTION: q6zip_log_init

DESCRIPTION
    This function initializes q6zip log packets

DEPENDENCIES:

RETURN VALUE:
  None

SIDE EFFECTS:
  None
===========================================================================*/
void q6zip_log_init()
{
    int i;

    atomic_init(&q6zip_log_dropped_sample_count, 0);
    atomic_init(&q6zip_log_dropped_packet_count, 0);

    /* Initialise q6zip log req and rsp state */
    atomic_init(&q6zip_log_req_status.all, 0);
    atomic_init(&q6zip_log_rsp_status.all, 0);
    
    /* Initilize log buffers */
    for (i = 0; i < NUM_LOG_BUFFERS; i++)
    {
        q6zip_log_req_buffer[i].q6zip_log.header.version = Q6ZIP_LOG_VERSION_1;
        q6zip_log_req_buffer[i].q6zip_log.header.numSamples = 0;
        q6zip_log_rsp_buffer[i].q6zip_log.header.version = Q6ZIP_LOG_VERSION_1;
        q6zip_log_rsp_buffer[i].q6zip_log.header.numSamples = 0;
    }
}

/*===========================================================================
FUNCTION: q6zip_log_flush

DESCRIPTION
    This function checks the status of req/rsp buffers
    and submits if full 

DEPENDENCIES:

RETURN VALUE:
  None

SIDE EFFECTS:
  None
===========================================================================*/
void q6zip_log_flush()
{
    uint16 i;

    for (i = 0; i < NUM_LOG_BUFFERS; i++)
    {
        if (q6zip_log_req_status.details.bufferFullStatus & (1 << i))
        {
            q6zip_log_submit(i, Q6ZIP_LOG_TYPE_REQ);
        }
        if (q6zip_log_rsp_status.details.bufferFullStatus & (1 << i))
        {
            q6zip_log_submit(i, Q6ZIP_LOG_TYPE_RSP);
        }
    }
}

/*===========================================================================
FUNCTION: q6zip_log_submit

DESCRIPTION
    This function submits the log packet to diag 

DEPENDENCIES:

RETURN VALUE:
  None

SIDE EFFECTS:
  None
===========================================================================*/
void q6zip_log_submit(uint8 BufToFlush, uint8 q6zip_log_type)
{
    LOG_EFS_MONITOR_C_type *q6zip_log_to_flush = NULL;

    if (q6zip_log_type == Q6ZIP_LOG_TYPE_REQ)
        q6zip_log_to_flush = &q6zip_log_req_buffer[BufToFlush];
    else if (q6zip_log_type == Q6ZIP_LOG_TYPE_RSP)
        q6zip_log_to_flush = &q6zip_log_rsp_buffer[BufToFlush];
    else
        Q6ZIP_ASSERT(0);

    /* no need to flush if there is no new log pakets
     * this shouldnt happen since this is called only
     * when the buffer is full */
    if (q6zip_log_to_flush->q6zip_log.header.numSamples == 0) return;

    log_set_code(q6zip_log_to_flush, LOG_EFS_MONITOR_C);

    log_set_length(q6zip_log_to_flush,
                    sizeof(log_hdr_type) + sizeof(q6zip_log_header_type)
                    + sizeof(q6zip_log_sample_type)
                    * q6zip_log_to_flush->q6zip_log.header.numSamples);

    log_set_timestamp(q6zip_log_to_flush);

    /* submit log packet */
    if (!log_submit(q6zip_log_to_flush))
    {
        atomic_inc(&q6zip_log_dropped_packet_count);
    }
    /* clear the full bit since log packet is flushed */ 
    if (q6zip_log_type == Q6ZIP_LOG_TYPE_REQ)
        atomic_clear_bit(&q6zip_log_req_status, 16+BufToFlush);
    else if (q6zip_log_type == Q6ZIP_LOG_TYPE_RSP)
        atomic_clear_bit(&q6zip_log_rsp_status, 16+BufToFlush);
}

void q6zip_log_put
(
    uint32 timetick,
    uint8  seq_num,
    uint8  lo_q,
    uint8  hi_q,
    uint8  q_id,
    uint8 log_type,
    q6zip_algo_t algo,
    uint16 payload_size
)
{
    /* check if log code is enabled */
    if (!log_status(LOG_EFS_MONITOR_C))
        return;
    /* Write to this buffer and index */
    uint8 write_buf_num;
    uint8 buffer_index;

    q6zip_log_type *q6zip_log_to_write;
    
    /* start writing to the buffer q6zip_log_to_write */
    if (log_type == Q6ZIP_LOG_TYPE_REQ)
    {
        /* if current buffer is full, then don't write - drop sample
         * should never happen in the req context, since a flush is 
         * called after each request*/
        if (IS_BUFFER_FULL(q6zip_log_req_status))
        {
            atomic_inc(&q6zip_log_dropped_sample_count);
            return;
        }
        write_buf_num = q6zip_log_req_status.details.bufferNum; 
        buffer_index = q6zip_log_req_status.details.index;
        q6zip_log_req_status.details.index += 1;
        q6zip_log_to_write = &(q6zip_log_req_buffer[write_buf_num].q6zip_log);
    }
    else if (log_type == Q6ZIP_LOG_TYPE_RSP)
    {
        /* if current buffer is full, then don't write - drop sample 
         * shouldn't happen, but could. monitor this dropped count */
        if (IS_BUFFER_FULL(q6zip_log_rsp_status))
        {
            atomic_inc(&q6zip_log_dropped_sample_count);
            return;
        }
        write_buf_num = q6zip_log_rsp_status.details.bufferNum; 
        buffer_index = q6zip_log_rsp_status.details.index;
        q6zip_log_rsp_status.details.index += 1;
        q6zip_log_to_write = &(q6zip_log_rsp_buffer[write_buf_num].q6zip_log);
    }
    else
    {
        write_buf_num = 0;
        buffer_index = 0;
        q6zip_log_to_write = NULL;

        /* @todo nsujit Q6ZIP_ASSERT() is for unit-testing only. It is *NOT*
           guaranteed to be enabled for on-target builds! If this is an error
           path, abort explicitly */
        Q6ZIP_ASSERT(0);
    }
    
//    q6zip_log_type *const q6zip_log_to_write = &(q6zip_log_rsp_buffer[write_buf_num].q6zip_log);
    q6zip_log_sample_type *const q6zip_log_sample = &(q6zip_log_to_write->samples[buffer_index]);
    
    /* Make sure sample and buffer fall within limits */
    Q6ZIP_ASSERT(buffer_index  < MAX_LOG_SAMPLES);
    Q6ZIP_ASSERT(write_buf_num < NUM_LOG_BUFFERS);
  
    q6zip_log_sample->timetick = timetick;
    /*  q6zip_log_sample->reserved = write_buf_num;
    */
    q6zip_log_sample->seq_num = seq_num;
    q6zip_log_sample->lo_q = lo_q;
    q6zip_log_sample->hi_q = hi_q;
    q6zip_log_sample->q_id = q_id;
    q6zip_log_sample->req_rsp = log_type;
    q6zip_log_sample->op_type = algo;
    q6zip_log_sample->timetick = timetick;
    q6zip_log_sample->block_size = payload_size;
    q6zip_log_to_write->header.numSamples = buffer_index + 1;
    
    /* check if the buffer is full 
    */
    if (q6zip_log_to_write->header.numSamples == MAX_LOG_SAMPLES )
    {
        if (log_type == Q6ZIP_LOG_TYPE_REQ)
        {
            /* atomically set the buffer full status */
            atomic_set_bit(&q6zip_log_req_status, 16 + write_buf_num);
            /* reset index */
            q6zip_log_req_status.details.index = 0;
            /* check if next buffer is full too!!! */
            /*
            if (q6zip_log_req_status.details.bufferNum &= (1 << (write_buf_num + 1)))
                atomic_inc(&q6zip_log_dropped_packet_count);
            */
            /* go to next buffer */
            q6zip_log_req_status.details.bufferNum =(q6zip_log_req_status.details.bufferNum + 1) % NUM_LOG_BUFFERS;
        }
        else if (log_type == Q6ZIP_LOG_TYPE_RSP)
        {
            /* atomically set the buffer full status */
            atomic_set_bit(&q6zip_log_rsp_status, 16 + write_buf_num);
            /* reset index */
            q6zip_log_rsp_status.details.index = 0;
            /* go to next buffer */
            q6zip_log_rsp_status.details.bufferNum =(q6zip_log_rsp_status.details.bufferNum + 1) % NUM_LOG_BUFFERS;
        }
        /* submit log buffer */
#ifndef Q6ZIP_LOG_ASYNC_FLUSH 
        q6zip_log_submit(write_buf_num, log_type);
#endif
    }
}


#if 0
/*===========================================================================
FUNCTION: q6zip_log_put 

DESCRIPTION
  This function creates markers in the code
  Enter only if start cmd is received
 
    NOTE We call this once diag is up

DEPENDENCIES:
  None

RETURN VALUE:
  None

SIDE EFFECTS:
  None
===========================================================================*/
static void q6zip_log_put
(
    uint32 timetick,
    uint8  seqno,
    uint8  lo_queue_length,
    uint8  hi_queue_length
)
{
  /* for global CAS purposes */
  q6zip_log_global_state_type local_copy_global_state;
  q6zip_log_global_state_type old_local_copy_global_state;

  /* for local buffer CAS purposes */
  q6zip_log_buffer_state_type local_buf_state;
  q6zip_log_buffer_state_type old_local_buf_state;

  /* @todo nsujit Remove this if not needed
  uint32 localTimetick = 0; */

  /* Write to this buffer and index */
  uint8 write_buf_num;
  uint8 buffer_index;

RETRY:
  do
  {
    /* copy of TTL current state buffer */
    local_copy_global_state.all = q6zip_log_global_state.all;
    old_local_copy_global_state.all = local_copy_global_state.all;

    /* check if the current buffer is FULL
       the second check is a check against a race condition
       along with the last sample in the buffer there is another
       entry into ttl_create_marker...buffer full is read before it is set
       by the last sample, so the sample after that gets index 100 (not what we want)
       */
    if (IS_BUFFER_FULL(local_copy_global_state) || (local_copy_global_state.details.index >= MAX_LOG_SAMPLES))
    {
      /* go to next buffer */
      local_copy_global_state.details.bufferNum = (local_copy_global_state.details.bufferNum + 1) % NUM_LOG_BUFFERS;
      /* reset num samples to zero */
      local_copy_global_state.details.index = 0;
      /* check if the next buffer is full */
      if (IS_BUFFER_FULL(local_copy_global_state))
      {
        /* if next buffer is also full, drop marker */
        atomic_inc(&dropped_q6zip_log_sample);
        return;
      }

      /* update q6zip current state to the next buffer */
      if (!CAS(&q6zip_log_global_state.all, old_local_copy_global_state.all.value, local_copy_global_state.all.value))
      {
        goto RETRY;
      }
      /* reload oldLocal since CAS set global to local */
      old_local_copy_global_state = local_copy_global_state;
    } else
    {
      // current buffer is FULL
      if (local_copy_global_state.details.index == (MAX_LOG_SAMPLES- 1))
      {
        SET_BUFFER_FULL(local_copy_global_state);
      } else if (local_copy_global_state.details.index >= MAX_LOG_SAMPLES)
      {
       Q6ZIP_ASSERT(0);
      }
    }

    write_buf_num = local_copy_global_state.details.bufferNum;
    buffer_index = local_copy_global_state.details.index;

    //DalTimetick_Get(hHandle, &localTimetick);
    /* 
       index increment below timetick because of race-condition
           index should increment along with write count,
           but thats not possible,
       so try and bring them as close as possible!
    */
    local_copy_global_state.details.index += 1;


  }
  while (!(CAS(&q6zip_log_global_state.all,
               old_local_copy_global_state.all.value,
               local_copy_global_state.all.value)));
  /* reserved a row to write to in the current buffer
     a flush cannot occur while this write is pending */

  /* lock the buffer for writing */
  do
  {
    /* make a local copy of the current buffer state */
    local_buf_state = q6zip_log_buf_status[write_buf_num];
    old_local_buf_state = local_buf_state;
    local_buf_state.details.writeCount += 1;
  }
  while (!CAS(&q6zip_log_buf_status[write_buf_num].all, old_local_buf_state.all.value, local_buf_state.all.value));

  /* Locked buffer for writing. */
  /* Make sure sample and buffer fall within limits */
  Q6ZIP_ASSERT(buffer_index  < MAX_LOG_SAMPLES);
  Q6ZIP_ASSERT(write_buf_num < MAX_LOG_SAMPLES);
  
  /* start writing to the buffer q6zip_log_to_write */
  q6zip_log_type *const q6zip_log_to_write = &(q6zip_log_buffer[write_buf_num].q6zip_log);
  q6zip_log_sample_type *const q6zip_log_sample = &(q6zip_log_to_write->samples[buffer_index]);

  /* Write to reserved row in the current buffer.
         Packer format for System Profiling: LOG_SYSTEM_PROFILING_C (0x15B6) used */
  
  q6zip_log_sample->timetick = timetick;
/*  q6zip_log_sample->reserved = write_buf_num;
  q6zip_log_sample->marker.subsysID = CALC_SUBSYS_NUM(subsysMask);
  q6zip_log_sample->marker.markerNum = marker;
  q6zip_log_sample->timetick = localTimetick;
  q6zip_log_to_write->header.numSamples = buffer_index + 1;
*/
  /* if we are writing into the last index,
     tell last writer to flush
  */
  if (buffer_index == (MAX_LOG_SAMPLES - 1))
  {
    /*
    this should happen only once for a buffer before 
    its flushed. tells last writer (not necassarily last index) 
    to finish and flush out logs 
    */
    SET_SHOULDFLUSH(write_buf_num);
  }

/* Decrement writeCount since this write is complete */
  do
  {
    /* make a local copy of the current buffer state */
    local_buf_state = q6zip_log_buf_status[write_buf_num];
    old_local_buf_state = local_buf_state;
    
    /* Check if we should flush the current buffer */
    if ((local_buf_state.details.writeCount == 1) && (local_buf_state.details.shouldFlush == 1))
    {
      /* this is the only write that is touching this buffer
       so no need for atomics here */
      q6zip_log_buf_status[write_buf_num].details.writeCount -= 1;
      q6zip_log_flush(write_buf_num);
      RESET_SHOULDFLUSH(write_buf_num);
      RESET_ISFULL(write_buf_num);
      return;
    }
    atomic_dec(&(local_buf_state.details.writeCount));
  }
  
  while (!CAS(&q6zip_log_buf_status[write_buf_num].all, old_local_buf_state.all.value, local_buf_state.all.value));

}
#endif
#endif /* Q6ZIP_LOG_ENABLED */
