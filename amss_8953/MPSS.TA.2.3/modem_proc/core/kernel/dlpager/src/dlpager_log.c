/*==========================================================================
 * FILE:         dlpager_log.c
 *
 * SERVICES:     DL PAGER LOG
 *
 * DESCRIPTION:  This file provides the implementation of loggin service for
                 dlpager
 *
 * Copyright (c) 2010-2013 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
=============================================================================*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/src/dlpager_log.c#2 $ $DateTime: 2016/09/06 12:34:27 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/01/13   cp      First version
===========================================================================*/
#include <stdlib.h>
#include <assert.h>
#include <qurt.h>
#include <atomic_ops.h>
#include <dlpager_log.h>
#include <dlpager_params.h>
#include <q6_cache.h>

#define DLPAGER_LOG_ENTRIES_BITS  11
#define DLPAGER_LOG_ENTRIES_MASK  ((1<<DLPAGER_LOG_ENTRIES_BITS)-1)
#define DLPAGER_LOG_ENTRIES_MAX   (1<<DLPAGER_LOG_ENTRIES_BITS) /**< No. of log entries */


typedef struct {
    uint32_t        thread_id:9;     /**< Fault thread Id */
    uint32_t        event:4;
    dlpager_page_state_t start_state;
    dlpager_page_state_t end_state; 
    void*           addr;      /**< Faulting address */
} dlpager_pagelog_entry_t;


atomic_word_t dlpager_tasklog_idx = ATOMIC_INIT(-1); // initializing an unsigned value, but rollover to 0 will happen on first atomic-inc
atomic_word_t dlpager_pagelog_idx = ATOMIC_INIT(-1);
dlpager_tasklog_entry_t dlpager_tasklog[DLPAGER_LOG_ENTRIES_MAX]  __attribute__((aligned(32)));
dlpager_pagelog_entry_t dlpager_pagelog[DLPAGER_LOG_ENTRIES_MAX]  __attribute__((aligned(16)));

unsigned long long dlpager_last_pcycles; /* Last 64 bit pcycles logged */

/******************************************************************************!
    @brief
    Allocates an entry in dlpager_tasklog, records the event type, fault_addr
    and thread_id. Subsequently calling dlpager_log_end, with the log_index
    returned from the call to dlpager_log_start, computes the delta in ticks
    @param 
    [in] event one of DLP_FAULT, DLP_REMOVE_CLK_VOTE, DLP_FIRST_CLK_VOTE, DLP_MAX_CLK_VOTE
    [in] fault_addr the faulting virtual address
    [in] thread_id the thread id of the task that faulted
    [in] evicted_page physical address of the evicted page
    @return 
    [out] log_idx , allocated index into dlpager_tasklog, passed into subsequent call to dlpager_log_end
*********************************************************************************/ 
unsigned dlpager_tasklog_start(dlpager_tasklog_event_t log_event, unsigned int fault_addr, dlpager_event_t page_event, qurt_thread_t thread_id,  uint32_t evicted_page)
{
    unsigned log_idx = atomic_inc_return(&dlpager_tasklog_idx) & DLPAGER_LOG_ENTRIES_MASK;

    dczero((uint32_t)(&dlpager_tasklog[log_idx]), sizeof(dlpager_tasklog[0]));
    dlpager_tasklog[log_idx].start_pcycles = qurt_get_core_pcycles();
    dlpager_tasklog[log_idx].fault_addr    = fault_addr;
    dlpager_tasklog[log_idx].page_event    = page_event;
    dlpager_tasklog[log_idx].evicted_page  = evicted_page >> PAGE_SHIFT;
    dlpager_tasklog[log_idx].loader        = DLP_LOADER_NA;
    dlpager_tasklog[log_idx].thread_id     = thread_id;
    dlpager_tasklog[log_idx].delta_pcycles = 0;
    dlpager_tasklog[log_idx].log_event     = log_event;

    return log_idx;
}

void dlpager_tasklog_set_loader (unsigned int idx, dlpager_loader_t loader)
{
    dlpager_tasklog[ idx ].loader = loader;
}

/******************************************************************************!
    @brief
    Completes an entry in dlpager_tasklog, computes the delta in ticks
    @param 
    [in] log_idx , allocated index into dlpager_tasklog gotten from dlpager_log_start
    [in] clk
    @return 
    None
*********************************************************************************/ 
void dlpager_tasklog_end(unsigned log_idx, int clk)
{
    unsigned long long end_pcycles = qurt_get_core_pcycles();
    unsigned int delta_pcycles     = end_pcycles - dlpager_tasklog[log_idx].start_pcycles;

    dlpager_tasklog[log_idx].delta_pcycles = (delta_pcycles > DLPAGER_LOG_DELTA_PCYCLES_MASK) ?
                                              DLPAGER_LOG_DELTA_PCYCLES_MASK : delta_pcycles;
    dlpager_tasklog[log_idx].clk           =  clk;
    dlpager_tasklog[log_idx].end_ticks     = qurt_sysclock_get_hw_ticks();

    dlpager_last_pcycles                   = end_pcycles; /* Full 64 bits */
}

/******************************************************************************!
    @brief
    Allocates an entry in dlpager_pagelog
    Subsequently calling dlpager_state_log_end, with the log_index
    returned from the call to dlpager_state_log_start, computes the delta in ticks
    @param 
    [in] state_log contains start state, end state and event
    [in] addr address of the page being operated on
    [in] thread_id
    @return 
    [out] log_idx , allocated index into dlpager_pagelog, passed into subsequent call to dlpager_log_state_end
*********************************************************************************/ 
unsigned dlpager_pagelog_start(dlpager_page_state_t start_state, dlpager_page_state_t end_state, dlpager_event_t event, void* addr, qurt_thread_t thread_id)
{
    unsigned log_idx = atomic_inc_return(&dlpager_pagelog_idx) & DLPAGER_LOG_ENTRIES_MASK;

    if ((log_idx & 0x1) == 0)
    {
      dczero((uint32_t)(&dlpager_pagelog[log_idx]), sizeof(dlpager_pagelog[0]));
    }
    dlpager_pagelog[log_idx].addr         = addr;
    dlpager_pagelog[log_idx].start_state  = start_state;
    dlpager_pagelog[log_idx].end_state    = end_state;
    dlpager_pagelog[log_idx].event        = event;
    dlpager_pagelog[log_idx].thread_id    = thread_id;
    return log_idx;
}

/******************************************************************************!
    @brief
    Completes an entry in dlpager_pagelog, computes the delta in ticks
    @param 
    [in] log_idx , allocated index into dlpager_pagelog gotten from dlpager_state_log_start
    @return 
    None
*********************************************************************************/ 
void dlpager_pagelog_end(unsigned log_idx)
{
//    unsigned long long end_ticks   = qurt_sysclock_get_hw_ticks();
//    unsigned long long end_pcycles = qurt_get_core_pcycles();
//    unsigned int delta             = end_ticks - dlpager_pagelog[log_idx].ticks;

 //   dlpager_pagelog[log_idx].pcycles32    = end_pcycles; /* Only last 32 bits logged */
 //   dlpager_pagelog[log_idx].delta_ticks  = (delta > DLPAGER_LOG_TICKS_MASK) ? DLPAGER_LOG_TICKS_MASK : delta;
 //   dlpager_last_pcycles                    = end_pcycles; /* Full 64 bits */
}


/*
 * dump_tasklog
 */
/*void dump_tasklog (void)
{
    int i,dlpager_log_idx_start;

    dlpager_log_idx_start=(dlpager_tasklog_idx.value+1) & DLPAGER_LOG_ENTRIES_MASK;
    printf ("Log index is at: %d\n", dlpager_tasklog_idx.value);
    printf ("Log table:\n");
    printf("%-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s\n", "thread_id", "evicted_page", "fault_addr", "clk", "pcycles32", "ticks", "delta_ticks", "log_event", "page_event");

    i = dlpager_log_idx_start;
    do { 
        printf ("0x%-10x 0x%-10x 0x%-10x 0x%-10x 0x%-10lx 0x%-10llx 0x%-10x 0x%-10x 0x%-10x\n",
                dlpager_tasklog[i].thread_id,
                dlpager_tasklog[i].evicted_page,
                (unsigned int) dlpager_tasklog[i].fault_addr,
                dlpager_tasklog[i].clk,
                dlpager_tasklog[i].pcycles32,
                dlpager_tasklog[i].ticks,
                dlpager_tasklog[i].delta_ticks,
                dlpager_tasklog[i].log_event,
                dlpager_tasklog[i].page_event);
        i = (i + 1) & DLPAGER_LOG_ENTRIES_MASK;
    } while (i != dlpager_log_idx_start);
}*/

/*
 * dump_pagelog
 */
void dump_pagelog (void)
{
    int i, dlpager_state_log_idx_start;
    
    dlpager_state_log_idx_start=(dlpager_pagelog_idx.value+1) & DLPAGER_LOG_ENTRIES_MASK;
    printf ("State Log index is at: %d\n", dlpager_state_log_idx_start);
    printf ("State Log table:\n");
    printf("%-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s \n", "thread_id", "addr", "pcycles32", "ticks", "delta_ticks", "start_state", "end_state", "event");

    i = dlpager_state_log_idx_start;
    do { 
        printf ("\t0x%-10x 0x%-10x 0x%-10x 0x%-10x 0x%-10x\n",
                dlpager_pagelog[i].thread_id,
                (unsigned int) dlpager_pagelog[i].addr,
               // dlpager_pagelog[i].pcycles32,
               // dlpager_pagelog[i].ticks,
               // dlpager_pagelog[i].delta_ticks,
                dlpager_pagelog[i].start_state,
                dlpager_pagelog[i].end_state,
                dlpager_pagelog[i].event);
        i = (i + 1) & DLPAGER_LOG_ENTRIES_MASK;
    } while (i != dlpager_state_log_idx_start);
}

