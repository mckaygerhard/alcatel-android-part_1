/*==========================================================================
 * FILE:         dlpager_handlers.c
 *
 * SERVICES:     DL PAGER HANDLERS
 *
 * DESCRIPTION:  This file implements handlers for the dlpager page state machine 
 *
 * Copyright (c) 2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
=============================================================================*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/src/dlpager_handlers.c#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who      what, where, why
--------   ----     ----------------------------------------------------------
12/09/14   rc,rr    First version
===========================================================================*/
#include <comdef.h>
#include <assert.h>
#include <qurt.h>
#include <dlpager.h>
#include <dlpager_params.h>
#include <dlpager_swappool.h> 
#include <dlpager_pagesm.h>
#include <dlpager_meta.h>
#include <dlpager_waitlist.h>
#include <dlpager_q6zip_iface.h>
#include <dlpager_log.h>
#include <dlpager_rwbuffer.h>
#include <dlpager_stats.h>
#include <dlpager_t32.h>
#include <dlpager_debug.h>
#include <q6_cache.h>

extern const unsigned int dlpager_soft_cleaning_threshold;
extern const unsigned int dlpager_hard_cleaning_threshold;
extern unsigned int __swapped_segments_start__;

unsigned int dlpager_initiate_clean = 0;

static inline void resume_task(qurt_thread_t task_id)
{
  qurt_thread_resume(task_id); 
  dlpager_stats_faultresumetimer_stop(task_id);
  dlpager_stats_ipa_to_resume_stop(task_id);
}

/******************************************************************************!
    @brief
    Reads lists of active TLBs from Q6, marks those pages as LRU
    @param
    None
    @return 
    None 
*********************************************************************************/ 
void lru_pages(void)
{
   unsigned int page,number_active_pages;
   unsigned int *tlb_physical_addresses;

   /* @todo rraghave Remove this if not used */
   (void)page;
     
   /*Request for Phys addrs of the Paged segments curretently in TLB*/
   number_active_pages = qurt_tlb_get_pager_physaddr(&tlb_physical_addresses);
   dlpager_swappool_mru_pages(tlb_physical_addresses, number_active_pages);
}

/******************************************************************************!
    @brief
    Returns the PA in the swap pool, of the oldest clean page
    @param
    None
    @return 
    PA in the swap pool of the oldest clean page 
*********************************************************************************/ 
static unsigned int evict_page(void)
{
   unsigned int offset_from_lru=0;
   unsigned int page_is_evictable;
   dlpager_swappool_mapping_t page_mapping;

   /* block here if there are no clean pages in the system */
   dlpager_swappool_evictable_pages_dec();
   
   /* restore clean count right away. 
      works becuase only front end decrements clean count */
   dlpager_swappool_evictable_pages_inc();

   dlpager_swappool_lock();
   do
   {
      page_mapping = dlpager_swappool_get_lru_mapping(offset_from_lru++);                  
      page_is_evictable = dlpager_pagesm_is_page_evictable(page_mapping.va);    
   }while (page_is_evictable == FALSE);

   dlpager_swappool_unlock();

   dlpager_stats_lrusteps_store(offset_from_lru-1);

   // startup pages, hard cleaned pages have page_mapping.va == 0. 
   if(page_mapping.va != 0)
   {     
     dlpager_pagesm_handle_event(EVICT_PAGE,page_mapping.va, THREAD_NULL);
   }
   return(page_mapping.pa);
}

static unsigned int find_page_to_clean(dlpager_clean_t clean_type)
{
  unsigned int threshold;
  unsigned int page_is_dirty;
  unsigned int offset_from_lru=0;
  dlpager_swappool_mapping_t page;

  /* set threshold based on cleaning type.
     for hard clean, thershold is the size of the swap pool */
  if(clean_type == SOFT_CLEAN)
    threshold = dlpager_soft_cleaning_threshold;
  else
    threshold = DLPAGER_SWAPPOOL_SIZE >> PAGE_SHIFT;
  
  while(threshold-- > 0)
  {
    page = dlpager_swappool_get_lru_mapping(offset_from_lru++);
    page_is_dirty = dlpager_pagesm_is_page_cleanable(page.va);
    if(page_is_dirty)
      break;
  }
  
  if(page_is_dirty)
    return page.va;

  return 0; 
}

static void initiate_clean(dlpager_clean_t clean_type)
{
  unsigned int to_clean_page_va;

  to_clean_page_va = find_page_to_clean(clean_type);
  if(to_clean_page_va != 0)
  { 
    dlpager_pagesm_handle_event((dlpager_event_t)clean_type, to_clean_page_va, THREAD_NULL);
  }
}

/******************************************************************************!
    @brief
    When a thread has faulted, add to a waitlist for the page
      If the waitlist doesnt exist, the page is decompressed, so resume this thread
    @param
    [in] page_va Virtual address of page to decompress
    [in] task_id Task Id that generated this event and is waiting
    @return 
    None
*********************************************************************************/ 
static void add_task_to_waitlist(unsigned int page_va, dlpager_event_t event, qurt_thread_t task_id )
{  
  unsigned int log_idx;
  log_idx = dlpager_tasklog_start(DLP_FAULT, page_va, event, task_id, -1 /* no evicted page */);
  dlpager_waitlist_add_task(page_va, DECOMPRESSION_COMPLETE, task_id, log_idx); 
}

static void resume_tasks_from_waitlist(unsigned int page_va, dlpager_event_t event)
{
   dlpager_task_idx_pair_t task_idx_pair;
   unsigned int num_pending_tasks;

   do
   {
      num_pending_tasks = dlpager_waitlist_get_task(page_va, event, &task_idx_pair); 
      if(task_idx_pair.task_id != THREAD_NULL){
         resume_task(task_idx_pair.task_id);
         dlpager_tasklog_end(task_idx_pair.tasklog_idx, 0);
      }
   } while(num_pending_tasks > 0);
}

/******************************************************************************!
    @brief
    Handles the initial miss event on a page.
    Adds the task to a list wait on the decompression of this page
    @param
    [in] page_va Virtual address of page to decompress
    [in] task_id Task Id that generated this event and is waiting
    @return 
    None
*********************************************************************************/ 
static void dlpager_handler_tlb_miss(unsigned int page_va, dlpager_event_t event, qurt_thread_t task_id)
{
   unsigned int evicted_page_pa;
   unsigned int log_idx = 0;
   evicted_page_pa=evict_page();
   log_idx = dlpager_tasklog_start(DLP_FAULT, page_va, event, task_id, evicted_page_pa);
   dlpager_waitlist_add_task(page_va, DECOMPRESSION_COMPLETE, task_id, log_idx); 
   dlpager_swappool_insert_mapping(page_va, evicted_page_pa);
   dlpager_stats_fault_to_ipa_stop(task_id);
   dlpager_q6zip_schedule_decompression(page_va, task_id, log_idx);
   if(dlpager_initiate_clean){
      initiate_clean(SOFT_CLEAN);
      dlpager_initiate_clean = 0;
   }
}

static boolean dlpager_handler_clean_page(unsigned int page_va, dlpager_priority_t prio)
{
  void *rw_buffer;
  unsigned int page_pa;
  unsigned int rc;
  dlpager_iovec_t rw_iovec;

  if(!dlpager_meta_is_addr_in_rwdata_range(page_va))
    ASSERT(0);

  rw_buffer = dlpager_rwbuffer_alloc(prio); 
  
  if(! rw_buffer)
  {    
    return 0 /* failure */;
  }

  rw_iovec.addr = (unsigned int) rw_buffer;  
  rw_iovec.len = RWBUFFER_SIZE;

  dlpager_waitlist_add_rwbuffer(page_va, rw_iovec);
   
  dlpager_swappool_get_mapping(page_va, &page_pa);  
 
  rc = qurt_mapping_remove(page_va, page_pa, 4096);
  ASSERT(rc == QURT_EOK);

  dlpager_q6zip_schedule_compression(page_va, rw_iovec, prio);

  return 1; /* success */
}

static void dlpager_handler_try_set_bkpt(unsigned int page_va, unsigned int page_pa)
{
  // FIXME cporter: need to test this map/unmap approach
  // Three-step process:
  //  1. Map the RX page as RW for the purpose of writing the bkpt
  //  2. Write the breakpoint(s) (if one exists for this page)
  //  3. Remove the mapping and clean
  unsigned int rc = qurt_mapping_create(page_va, page_pa, PAGE_SIZE, QURT_MEM_CACHE_WRITEBACK, QURT_PERM_READ | QURT_PERM_WRITE);
  ASSERT(rc == QURT_EOK);
  T32_Pager(DLPAGER_ALIGN_DOWN(page_va, PAGE_SIZE), 0 /* space */); // write 0 or more bkpts
  rc = qurt_mapping_remove(page_va, page_pa, PAGE_SIZE);
  // XXX we could avoid flushing and invalidating the entire page by modifying dlpager_t32.c.
  //     from T32_Pager, we could invalidate just the address/es that was/were modified
  qurt_mem_cache_clean((qurt_addr_t) page_va,
                        PAGE_SIZE,
                        QURT_MEM_CACHE_FLUSH_INVALIDATE,
                        QURT_MEM_DCACHE);
  ASSERT(rc == QURT_EOK);
}


/******************************************************************************!
    @brief
    Handles the decompression complete event,
      create a TLB mapping and resume pending threads
    @param
    [in] page_va Virtual address of page to decompress
    [in] task_id Task Id that generated this event and is waiting
    @return 
    None
*********************************************************************************/ 
static void dlpager_handler_decompression_complete(unsigned int q6unzip_page_va, unsigned int perms)
{
   unsigned int rc;
   unsigned int q6unzip_page_pa;
   
   dlpager_swappool_get_mapping(q6unzip_page_va, &q6unzip_page_pa);
   
   if(dlpager_osam_debug_enabled 
	  && dlpager_meta_is_addr_in_text_range(q6unzip_page_va) 
	  && T32_PagerTable[0].action)
   {
     dlpager_handler_try_set_bkpt(q6unzip_page_va, q6unzip_page_pa);
   }

   rc = qurt_mapping_create (q6unzip_page_va, q6unzip_page_pa, 4096, QURT_MEM_CACHE_WRITEBACK, perms);
   ASSERT(rc==QURT_EOK);

   resume_tasks_from_waitlist(q6unzip_page_va, DECOMPRESSION_COMPLETE);
}

static void dlpager_handler_write_to_unmapped_allocated_page(unsigned int page_va, dlpager_event_t event, qurt_thread_t thread_id)
{
  unsigned int tasklog_idx;
  unsigned int rc;
  unsigned int page_pa;

  if(!dlpager_meta_is_addr_in_rwdata_range(page_va))
    ASSERT(0);

  tasklog_idx = dlpager_tasklog_start(DLP_FAULT, page_va, event, thread_id, -1 /*evicted page*/);
  dlpager_swappool_mru_page(page_va);
  dlpager_swappool_get_mapping(page_va, &page_pa);
  
  rc = qurt_mapping_create(page_va, page_pa, 4096, QURT_MEM_CACHE_WRITEBACK, QURT_PERM_READ | QURT_PERM_WRITE); 
  ASSERT(rc == QURT_EOK);

  resume_task(thread_id);

  dlpager_tasklog_end(tasklog_idx, 0 /*clk*/);
  
  if(dlpager_initiate_clean){
    initiate_clean(SOFT_CLEAN);
    dlpager_initiate_clean = 0;
  }
}

static void dlpager_handler_compression_complete(unsigned int page_va, unsigned int discard_result, dlpager_priority_t prio)
{
  dlpager_iovec_t rw_buffer;

  rw_buffer = dlpager_waitlist_get_rwbuffer(page_va);
 
  if(!discard_result)
    dlpager_meta_set_compressed_block(page_va, rw_buffer);

  dlpager_rwbuffer_free((void *)rw_buffer.addr, prio);
}


/******************************************************************************!
    @brief
    Handle reads from or writes to BSS.
        initiate evict
        memset page to 0
        invalidate and clean cache
        map va to swappool pa
        resume task
    @param
    [in] page_va Faulting virtual address of task
    [in] task_id Task Id that generated this event
    [in] perms Permissions of the newly mapped page. rx or rw.
    @return 
    None
*********************************************************************************/ 
static void dlpager_handler_bss(unsigned int page_va, dlpager_event_t event, qurt_thread_t task_id, unsigned int perms)
{
   unsigned int evicted_page_pa;
   unsigned int rc;
   unsigned int tasklog_idx;
   dlpager_swappool_page_info_t swappool_page_info;
 
   evicted_page_pa = evict_page();
   dlpager_swappool_insert_mapping(page_va, evicted_page_pa);
 
   tasklog_idx = dlpager_tasklog_start(DLP_FAULT, page_va, event, task_id, evicted_page_pa);

   // memset page to 0
   dlpager_swappool_lookup(page_va, &swappool_page_info);
  
   dczero(swappool_page_info.va_swap_rw, 4096);

   // clean, flush, invalidate dcache 
   qurt_mem_cache_clean((qurt_addr_t) swappool_page_info.va_swap_rw,
                        4096,
                        QURT_MEM_CACHE_FLUSH_INVALIDATE,
                        QURT_MEM_DCACHE);

   // map link-time va to swappool pa
   rc = qurt_mapping_create(page_va, evicted_page_pa, PAGE_SIZE, QURT_MEM_CACHE_WRITEBACK, perms);
   ASSERT(rc==QURT_EOK);

   // resume task
   resume_task(task_id);
   dlpager_tasklog_end(tasklog_idx, 0);

   if(dlpager_initiate_clean){
      initiate_clean(SOFT_CLEAN);
      dlpager_initiate_clean = 0;
   }
}

/******************************************************************************!
    @brief
    Event handlers
    @param
    [in] page_va Virtual address of page to decompress/evict
    [in] task_id Task Id that generated this event and is waiting
    @return 
    None
*********************************************************************************/ 
void dlpager_handler_default(unsigned int ignored, qurt_thread_t task_id)
{
  if(task_id != THREAD_NULL)
  {
  resume_task(task_id);
  }
}

void dlpager_handler_tlb_miss_x_at_unmapped_clean(unsigned int page_va,qurt_thread_t task_id)
{
   dlpager_handler_tlb_miss(page_va, TLB_MISS_X, task_id);
   if(dlpager_swappool_evictable_pages_dec() <= dlpager_hard_cleaning_threshold)
    initiate_clean(HARD_CLEAN);
}

void dlpager_handler_tlb_miss_r_at_unmapped_clean(unsigned int page_va,qurt_thread_t task_id)
{
   dlpager_handler_tlb_miss(page_va, TLB_MISS_R, task_id);
   if(dlpager_swappool_evictable_pages_dec() <= dlpager_hard_cleaning_threshold)
    initiate_clean(HARD_CLEAN);
}

/*
void dlpager_handler_evict_page_at_unmapped_clean(unsigned int page_va,qurt_thread_t task_id)
{
  // Nothing to do here
}
*/
void dlpager_handler_tlb_miss_x_at_unmapped_clean_decompressing(unsigned int page_va,qurt_thread_t task_id)
{
  add_task_to_waitlist(page_va, DECOMPRESSION_COMPLETE, task_id);
  dlpager_swappool_mru_page(page_va);
}

void dlpager_handler_tlb_miss_r_at_unmapped_clean_decompressing(unsigned int page_va,qurt_thread_t task_id)
{
  add_task_to_waitlist(page_va, DECOMPRESSION_COMPLETE, task_id);
  dlpager_swappool_mru_page(page_va);
}

void dlpager_handler_decompression_complete_at_unmapped_clean_decompressing(unsigned int page_va,qurt_thread_t task_id)
{
  unsigned int perms = 0;

  if (dlpager_meta_is_addr_in_text_range(page_va)) 
  {
    perms=(QURT_PERM_READ | QURT_PERM_EXECUTE);
  }
  else if (dlpager_meta_is_addr_in_rodata_range(page_va))
  {
    perms=QURT_PERM_READ;
  }
  else 
  {
    ASSERT(0);
  }

  if(dlpager_osam_debug_enabled) 
    dlpager_debug_apply_outstanding_writes(page_va); 

  dlpager_handler_decompression_complete(page_va, perms);
  dlpager_swappool_evictable_pages_inc();
}

void dlpager_handler_evict_page_at_mapped_clean(unsigned int page_va,qurt_thread_t task_id)
{
     int rc;
   dlpager_swappool_page_info_t page_info;
   
   dlpager_swappool_lookup(page_va, &page_info);
   rc=qurt_mapping_remove(page_va, page_info.pa,4096);
   ASSERT(rc==QURT_EOK);   
   dlpager_swappool_remove_mapping(page_va);

   /* @todo rraghave Enable I$ invalidation on eviction instead of parallelizing
      with Q6ZIP in decompression path. See corresponding @todo in
      dlpager_q6zip_iface.c */
   if (dlpager_meta_is_addr_in_text_range(page_va))
   {
      qurt_mem_cache_clean ((qurt_addr_t)page_info.va_swap_rx, 4096, QURT_MEM_CACHE_INVALIDATE, QURT_MEM_ICACHE);
   }
}

void dlpager_handler_tlb_miss_w_at_unmapped_clean(unsigned int page_va, qurt_thread_t task_id)
{
  dlpager_handler_tlb_miss(page_va, TLB_MISS_W, task_id);

  if(dlpager_swappool_evictable_pages_dec() <= dlpager_hard_cleaning_threshold)
    initiate_clean(HARD_CLEAN);
}

void dlpager_handler_tlb_miss_w_at_unmapped_bss(unsigned int page_va, qurt_thread_t task_id)
{
   dlpager_stats.bss_allocations++;
   if(dlpager_osam_debug_enabled) 
     dlpager_debug_apply_outstanding_writes(page_va);

   dlpager_handler_bss(page_va, TLB_MISS_W, task_id, (QURT_PERM_READ | QURT_PERM_WRITE));
   
   if(dlpager_swappool_evictable_pages_dec() <= dlpager_hard_cleaning_threshold)
     initiate_clean(HARD_CLEAN);
}

void dlpager_handler_tlb_miss_w_at_unmapped_dirty_decompressing(unsigned int page_va, qurt_thread_t task_id)
{
  add_task_to_waitlist(page_va, DECOMPRESSION_COMPLETE, task_id);
  dlpager_swappool_mru_page(page_va);
}

void dlpager_handler_decompression_complete_at_unmapped_dirty_decompressing(unsigned int page_va, qurt_thread_t task_id)
{
  if(!dlpager_meta_is_addr_in_rwdata_range(page_va))
    ASSERT(0);

  if(dlpager_osam_debug_enabled) 
    dlpager_debug_apply_outstanding_writes(page_va); 

  dlpager_handler_decompression_complete(page_va, (QURT_PERM_READ|QURT_PERM_WRITE));
}

void dlpager_handler_soft_clean_page_at_mapped_dirty(unsigned int page_va,qurt_thread_t thread_id) 
{
  boolean success;
  ++dlpager_stats.soft_cleans; // decremented later if soft fails
  success = dlpager_handler_clean_page(page_va, LOW_PRIORITY); 

  if(!success)    
    dlpager_pagesm_handle_event(SOFT_CLEAN_FAILED, page_va, thread_id);
}

void dlpager_handler_soft_clean_failed_at_unmapped_dirty_compressing_allocated(unsigned int page_va,qurt_thread_t null)
{
  --dlpager_stats.soft_cleans;
  ++dlpager_stats.soft_cleans_failed;  
}

void dlpager_handler_tlb_miss_w_at_unmapped_dirty_compressing_allocated(unsigned int page_va, qurt_thread_t thread_id)
{
  dlpager_handler_write_to_unmapped_allocated_page(page_va, TLB_MISS_W, thread_id);
}

void dlpager_handler_compression_complete_at_unmapped_dirty_compressing_allocated(unsigned int page_va,qurt_thread_t null)
{    
  dlpager_handler_compression_complete(page_va, 0 /* do not discrad result */, LOW_PRIORITY /* low prio compression */);
  dlpager_swappool_evictable_pages_inc();
}

void dlpager_handler_tlb_miss_w_at_unmapped_clean_allocated(unsigned int page_va, qurt_thread_t thread_id)
{
  dlpager_handler_write_to_unmapped_allocated_page(page_va, TLB_MISS_W, thread_id);

  if(dlpager_swappool_evictable_pages_dec() <= dlpager_hard_cleaning_threshold)
     initiate_clean(HARD_CLEAN);
}

void dlpager_handler_evict_page_at_unmapped_clean_allocated(unsigned int page_va, qurt_thread_t null)
{  
  dlpager_swappool_remove_mapping(page_va);
}

void dlpager_handler_compression_complete_at_mapped_dirty_compressing(unsigned int page_va,qurt_thread_t task_id) 
{  
  dlpager_handler_compression_complete(page_va, 1 /*discard result */, LOW_PRIORITY);
}

void dlpager_handler_hard_clean_page_at_mapped_dirty(unsigned int page_va,qurt_thread_t task_id) 
{
  ++dlpager_stats.hard_cleans;
  dlpager_handler_clean_page(page_va, HIGH_PRIORITY);
}

void dlpager_handler_tlb_miss_w_at_unmapped_hard_clean_compressing(unsigned int page_va,qurt_thread_t task_id)
{
  add_task_to_waitlist(page_va, COMPRESSION_COMPLETE, task_id);
}

void dlpager_handler_compression_complete_at_unmapped_hard_clean_compressing(unsigned int page_va,qurt_thread_t task_id) 
{
  dlpager_handler_compression_complete(page_va, 0 /*do not discard result */, HIGH_PRIORITY); 
  dlpager_swappool_remove_mapping(page_va); 
  dlpager_swappool_evictable_pages_inc();
  resume_tasks_from_waitlist(page_va, COMPRESSION_COMPLETE);  
}
