#include "q6zip_waiter.h"
#include <assert.h>

#if !defined( Q6ZIP_WAITER_USES_CONDITIONAL ) || !defined( Q6ZIP_WAITER_USES_SIGNAL )
/* By default, try using signal */
#define Q6ZIP_WAITER_USES_SIGNAL
#endif

#if defined( Q6ZIP_WAITER_USES_CONDITIONAL )
void q6zip_waiter_init (q6zip_waiter_t * waiter)
{
    pthread_mutexattr_t mutex_attr;

    (void) mutex_attr;
    ASSERT( !pthread_cond_init( &waiter->impl.conditional.cond, NULL ) );
    ASSERT( !pthread_mutexattr_init( &mutex_attr ) );
    ASSERT( !pthread_mutexattr_settype( &mutex_attr, PTHREAD_MUTEX_NORMAL ) );
    ASSERT( !pthread_mutex_init( &waiter->impl.conditional.mutex, &mutex_attr ) );

    waiter->woken = FALSE;
}

void q6zip_waiter_wait( q6zip_waiter_t * waiter)
{
    ASSERT( !pthread_mutex_lock( &waiter->impl.conditional.mutex ) );
    while ( !waiter->woken )
        ASSERT( !pthread_cond_wait( &waiter->impl.conditional.cond, &waiter->impl.conditional.mutex ) );
    waiter->woken = FALSE;
    ASSERT( !pthread_mutex_unlock( &waiter->impl.conditional.mutex ) );
}

void q6zip_waiter_wake (q6zip_waiter_t * waiter)
{
    ASSERT( !pthread_mutex_lock( &waiter->impl.conditional.mutex ) );
    waiter->woken = TRUE;
    ASSERT( !pthread_cond_signal( &waiter->impl.conditional.cond ) );
    ASSERT( !pthread_mutex_unlock( &waiter->impl.conditional.mutex ) );
}
#elif defined( Q6ZIP_WAITER_USES_SIGNAL )
void q6zip_waiter_init (q6zip_waiter_t * waiter)
{
    qurt_signal_init( &waiter->impl.signal.object );
}

void q6zip_waiter_wait (q6zip_waiter_t * waiter)
{
    qurt_signal_wait_any( &waiter->impl.signal.object, 0x1 );
    qurt_signal_clear( &waiter->impl.signal.object, 0x1 );
}

void q6zip_waiter_wake (q6zip_waiter_t * waiter)
{
    qurt_signal_set( &waiter->impl.signal.object, 0x1 );
}
#else
#error unknown implementation of Q6ZIP waiter
#endif
