/*===========================================================================
 * FILE:         dlpager_swapmem.c
 *
 * SERVICES:     DL PAGER
 *
 * DESCRIPTION:  dlpager swap pool memory managment
 *
 * Copyright (c) 2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
 ===========================================================================*/

/*===========================================================================

  EDIT HISTORY FOR MODULE
  
  $Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/src/dlpager_swapmem.c#1 $
  
when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/30/14   ao      Initial revision
===========================================================================*/
#include <assert.h>

#include <qurt.h>
#include "dlpager_swapmem.h"

#define DLPAGER_MAX_MEM_REGIONS 16
#define PAGE_SIZE 4096

struct dlpager_mem_regions {
    unsigned int seg_base; /* Demand Loaded segment base address */
    unsigned int seg_size; /* segment size */
    qurt_mem_region_t region; /* QuRT region of swap pool */
    qurt_mem_region_t region_rx; /* QuRT region of allocated virtual memory */
    unsigned int region_size; /* size of the allocated swap pool */
    unsigned int p_base; /* swap pool physical base address */
    unsigned int v_rw_base; /* swap pool virtual address with RW permissions */
    unsigned int v_rx_base; /* swap pool virtual address with RX permission */
} dlpager_mem_regions[DLPAGER_MAX_MEM_REGIONS];

static int dlpager_swapmem_pool_request(size_t size);

int dlpager_swapmem_create(size_t size)
{
    return dlpager_swapmem_pool_request(size);
}

unsigned int dlpager_swapmem_getrw(int idx)
{
    return dlpager_mem_regions[0].v_rw_base + (idx * PAGE_SIZE);
}

unsigned int dlpager_swapmem_getrx(int idx)
{
    return dlpager_mem_regions[0].v_rx_base + (idx * PAGE_SIZE);
}

unsigned int dlpager_swapmem_getpa(int idx)
{
    return dlpager_mem_regions[0].p_base + (idx * PAGE_SIZE);
}

unsigned int dlpager_swapmem_getidx_from_pa(unsigned int pa)
{
    return (pa - dlpager_mem_regions[0].p_base) / PAGE_SIZE;
}

void dlpager_swapmem_invalidate(void)
{
    qurt_mem_cache_clean ((qurt_addr_t)dlpager_mem_regions[0].v_rx_base,
                          dlpager_mem_regions[0].region_size,
                          QURT_MEM_CACHE_INVALIDATE,
                          QURT_MEM_ICACHE);
    qurt_mem_cache_clean ((qurt_addr_t)dlpager_mem_regions[0].v_rw_base,
                          dlpager_mem_regions[0].region_size,
                          QURT_MEM_CACHE_INVALIDATE,
                          QURT_MEM_DCACHE);
}

void dlpager_swapmem_removerw(void)
{
    qurt_mapping_remove(dlpager_mem_regions[0].v_rw_base,
                        dlpager_mem_regions[0].p_base,
                        dlpager_mem_regions[0].region_size);
}

void dlpager_swapmem_addrw(void)
{
    qurt_mapping_create(dlpager_mem_regions[0].v_rw_base,
                        dlpager_mem_regions[0].p_base,
                        dlpager_mem_regions[0].region_size,
                        QURT_MEM_CACHE_WRITEBACK,
                        QURT_PERM_READ | QURT_PERM_WRITE);
}

static int dlpager_swapmem_pool_request(size_t size)
{
    qurt_mem_region_attr_t attr;
    int ret;
    qurt_mem_region_t region;
    qurt_mem_pool_t default_pool;
    unsigned int p_addr, v_rw_addr, v_rx_addr, r_size;

    size_t pool_size = size;

    qurt_mem_pool_attach ("DEFAULT_PHYSPOOL", &default_pool);

    /* Create memory region for swap pool */
    qurt_mem_region_attr_init (&attr);
    qurt_mem_region_attr_set_mapping (&attr, QURT_MEM_MAPPING_VIRTUAL_RANDOM);
    ret = qurt_mem_region_create (&region, pool_size, default_pool, &attr);
    ASSERT (ret == QURT_EOK);
    qurt_mem_region_attr_get (region, &attr);

    qurt_mem_region_attr_get_physaddr (&attr, &p_addr);
    qurt_mem_region_attr_get_virtaddr (&attr, &v_rw_addr);
    qurt_mem_region_attr_get_size (&attr, &r_size);

    /* Initialize dlpager_mem_regions */
    dlpager_mem_regions[0].region = region;
    dlpager_mem_regions[0].region_size = r_size;
    dlpager_mem_regions[0].p_base = p_addr;
    dlpager_mem_regions[0].v_rw_base = v_rw_addr;

    /* Reserve virtual memory for RX operations */
    qurt_mem_region_attr_init (&attr);
    qurt_mem_region_attr_set_mapping (&attr, QURT_MEM_MAPPING_NONE);

    ret = qurt_mem_region_create (&region, r_size, default_pool, &attr);
    ASSERT (ret == QURT_EOK);
    qurt_mem_region_attr_get (region, &attr);
    qurt_mem_region_attr_get_virtaddr (&attr, &v_rx_addr);

    dlpager_mem_regions[0].v_rx_base = v_rx_addr;

    /* Map vitual memory for RX operation */
    qurt_mapping_create (v_rx_addr, p_addr, r_size,
                         QURT_MEM_CACHE_WRITEBACK,
                         QURT_PERM_READ | QURT_PERM_EXECUTE);

    return ret;
}
