#ifndef __DLPAGER_SWAPMEM_H__
#define __DLPAGER_SWAPMEM_H__

/*===========================================================================
 * FILE:         dlpager_swapmem.h
 *
 * SERVICES:     DL PAGER
 *
 * DESCRIPTION:  dlpager swap pool memory managment
 *
 * Copyright (c) 2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
 ===========================================================================*/

/*===========================================================================

  EDIT HISTORY FOR MODULE
  
  $Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/inc/dlpager_swapmem.h#1 $
  
when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/30/14   ao      Initial revision
===========================================================================*/

#include <stdlib.h>

int dlpager_swapmem_create(size_t size);
void dlpager_swapmem_invalidate(void);
void dlpager_swapmem_removerw(void);
void dlpager_swapmem_addrw(void);
unsigned int dlpager_swapmem_getrw(int idx);
unsigned int dlpager_swapmem_getrx(int idx);
unsigned int dlpager_swapmem_getpa(int idx);
unsigned int dlpager_swapmem_getidx_from_pa(unsigned int pa);

#endif
