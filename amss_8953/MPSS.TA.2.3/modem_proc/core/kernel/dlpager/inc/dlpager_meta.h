/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               DL PAGER TOP LEVEL HEADER FILE

GENERAL DESCRIPTION

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/inc/dlpager_meta.h#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   rc,rr,cp     Header file metadata associated APIs
===========================================================================*/

#pragma once
#include <comdef.h>
#include <dlpager_types.h>



boolean dlpager_meta_is_addr_in_text_range(unsigned int va);
boolean dlpager_meta_is_addr_in_rodata_range(unsigned int va);
boolean dlpager_meta_is_addr_in_text_or_rodata_range(unsigned int va);
boolean dlpager_meta_is_addr_in_rwdata_range(unsigned int va);

dlpager_iovec_t dlpager_meta_get_compressed_block_addr (unsigned int uncompressed_page_addr);
void dlpager_meta_set_compressed_block(unsigned int va, dlpager_iovec_t rw_buffer);

char * dlpager_meta_q6zip_dictionary(void);
unsigned int dlpager_meta_init();

