/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               DL PAGER TOP LEVEL HEADER FILE

GENERAL DESCRIPTION

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/inc/dlpager_pagesm.h#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   rc,rr,cp     Header file dlpager page state machine 
===========================================================================*/

#pragma once 
#include "dlpager_types.h"

unsigned int dlpager_pagesm_is_page_evictable(unsigned int);

unsigned int dlpager_pagesm_is_page_cleanable(unsigned int);

void dlpager_pagesm_handle_event(dlpager_event_t, unsigned int, qurt_thread_t);

int dlpager_pagesm_init(void);

