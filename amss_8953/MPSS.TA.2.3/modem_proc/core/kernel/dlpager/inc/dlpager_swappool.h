
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               DL PAGER TOP LEVEL HEADER FILE

GENERAL DESCRIPTION

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/inc/dlpager_swappool.h#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   rr      Header file for APIs related swap pool management
===========================================================================*/
#include <qurt_types.h>

typedef struct
{
  unsigned int va;
  unsigned int va_swap_rw;
  unsigned int va_swap_rx;
  unsigned int pa;
} dlpager_swappool_page_info_t; 

typedef struct
{
  unsigned int va;
  unsigned int pa;
} dlpager_swappool_mapping_t;

void dlpager_swappool_init(unsigned int pool_size);
void dlpager_swappool_get_info (qurt_addr_t * const va, unsigned int * const size);
void dlpager_swappool_mru_page(unsigned int va);
void dlpager_swappool_mru_pages(unsigned int *pa_array, unsigned int size);

void dlpager_swappool_get_mapping(unsigned int va, unsigned int *pa);
void dlpager_swappool_lookup(unsigned int va, dlpager_swappool_page_info_t *info);
void dlpager_swappool_remove_mapping(unsigned int va);
void dlpager_swappool_insert_mapping(unsigned int va, unsigned int pa);

void dlpager_swappool_rw_mapping_add(void);
void dlpager_swappool_rw_mapping_remove(void);

unsigned int dlpager_swappool_evictable_pages_dec(void);
void dlpager_swappool_evictable_pages_inc(void);

void dlpager_swappool_lock();
void dlpager_swappool_unlock();

/* caller should lock swappool */
dlpager_swappool_mapping_t dlpager_swappool_get_lru_mapping(unsigned int offset_from_lru);


