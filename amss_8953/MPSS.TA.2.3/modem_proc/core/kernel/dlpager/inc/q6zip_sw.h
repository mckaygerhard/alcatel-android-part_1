#pragma once
/**
   @file q6zip_sw.h 
   @author anandj 
   @brief Implementation of Q6ZIP interface using algorithms implemented in SW
    
   Copyright (c) 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#include "q6zip_iface.h"

extern q6zip_iface_t const Q6ZIP_SW;
