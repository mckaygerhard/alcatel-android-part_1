
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               DL PAGER TOP LEVEL HEADER FILE

GENERAL DESCRIPTION

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/core.mpss/3.9.1/kernel/dlpager/inc/dlpager_params.h#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when       who    what, where, why
--------   ---    ----------------------------------------------------------
12/09/14   rr     Header file for all config params for dlpager
===========================================================================*/
#include <q6zip_params.h>

#define MB (1024*1024)
#define DLPAGER_SWAPPOOL_SIZE (3 * MB)

#define PAGE_SHIFT (12)
#define PAGE_SIZE (1 << PAGE_SHIFT)

#define DLPAGER_SOFT_CLEANING_THRESHOLD  (2)
#define DLPAGER_HARD_CLEANING_THRESHOLD  (4)

// FIXME: Ensure maximums are correct.
//        Note that MAX_PENDING_PAGES may need additional adjusting (e.g. MAX * 2),
//        because the the same pending page may exist in two different lists.
#define MAX_PENDING_TASKS 32
#define MAX_PENDING_PAGES Q6ZIP_PARAM_CONTEXT_MAX_ITEMS

#define RWBUFFER_SIZE 4352  // worst case no match RW compress = 1024 * 34 / 8
#define NUM_SOFT_RWBUFFERS 2         
#define NUM_HARD_RWBUFFERS 4         

