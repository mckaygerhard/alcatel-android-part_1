#! /usr/bin/env python
import struct

firstHeader=1
payloadHoldList=[]

codeHistogram=[0,0,0,0]

def pushCompressedBitsRaw(bits,compressed):
    global numCompressedWordsRaw
    compressed.append(bits)
    numCompressedWordsRaw += 1                                              

def pushCompressedBitsEncoded(bits,numBits,compressed):
    global numCompressedWordsEncoded
    global compressedPartialWordEncoded
    global numCompressedPartialBitsEncoded

    temp = bits << numCompressedPartialBitsEncoded
    numCompressedPartialBitsEncoded += numBits
    compressedPartialWordEncoded = compressedPartialWordEncoded | temp
    if numCompressedPartialBitsEncoded >= 32:                                                                               
        compressed.append(compressedPartialWordEncoded & 0xFFFFFFFF)
        numCompressedWordsEncoded += 1                                              
        compressedPartialWordEncoded = compressedPartialWordEncoded >> 32   
        numCompressedPartialBitsEncoded -= 32
        
def finalizeCompressedBits(compressed):  
    global numCompressedWordsEncoded
    global compressedPartialWordEncoded
    global numCompressedPartialBitsencoded

    if numCompressedPartialBitsEncoded > 0:
        compressed.append(compressedPartialWordEncoded & 0xFFFFFFFF)  
        numCompressedWordsEncoded += 1   

def pushHeader(bits,numBits,compressed):
    global payloadHoldList
    global codeHistogram
    codeHistogram[bits]+=1
    pushCompressedBitsEncoded(bits,numBits,compressed)

    while(payloadHoldList):
        [bits1,numBits1]=payloadHoldList.pop()
        pushCompressedBitsEncoded(bits1,numBits1,compressed)

def pushPayload(bits,numBits,compressed):
    global payloadHoldList
    payloadHoldList.append([bits,numBits])
    #pushCompressedBits(bits,numBits,compressed)
    #[bits1,numBits1]=payloadHoldList.pop()
    #pushCompressedBits(bits1,numBits1,compressed)


def checkAnchor(anchor,val,compressed):
    global anchors
    anchor_val = anchors[anchor]; 
    if anchors[anchor] == val:
        pushHeader(1,2,compressed)
        pushPayload(anchor,2,compressed)
        return 1
    elif ((anchor_val & 0xFFFFFC00) == (val & 0xFFFFFC00)):
        pushHeader(2,2,compressed)
        pushPayload(((val&0x3FF)<<2) + anchor,12,compressed)
        anchors[anchor] = val; 
        return 1
    else:
        return 0

def deltaCompress (uncompressed,compressedStreamRaw):
    global numCompressedWordsRaw
    global compressedPartialWordRaw
    global numCompressedPartialBitsRaw
    global numCompressedWordsEncoded
    global compressedPartialWordEncoded
    global numCompressedPartialBitsEncoded
    global anchors

    numCompressedWordsRaw = 0
    compressedPartialWordRaw = 0
    numCompressedPartialBitsRaw = 0
    numCompressedWordsEncoded = 0
    compressedPartialWordEncoded = 0
    numCompressedPartialBitsEncoded = 0

    compressedStreamEncoded = []

    anchors = [0,0,0,0]
    anchorIndex = 3
    
    compressedStreamRaw.append(0) # will overwrite with size of raw stream later
    for i in xrange(len(uncompressed)):
        val = uncompressed[i]

        #if i < 5: # DEBUG
        #    print "i: %d" % i
        #    print "\tval: 0x%x" % val

        if (val == 0):
            pushHeader(0,2,compressedStreamEncoded)
            #if i < 5: # DEBUG
            #    print "\tmatched 0."
            continue
        anchor = anchorIndex
        #if i < 5: # DEBUG
        #        print "\tchecking anchors[anchor]: 0x%x" % anchors[anchor]
        if checkAnchor(anchor,val,compressedStreamEncoded) == 1:
            #if i < 5: # DEBUG
            #    print "\tmatched 1st anchor check."
            continue
        anchor = (anchor + (4 - 1)) & (3)
        #if i < 5: # DEBUG
        #        print "\tchecking anchors[anchor]: 0x%x" % anchors[anchor]
        if checkAnchor(anchor,val,compressedStreamEncoded) == 1:
            #if i < 5: # DEBUG
            #    print "\tmatched 2nd anchor check."
            continue
        anchor = (anchor + (4 - 1)) & (3)
        #if i < 5: # DEBUG
        #        print "\tchecking anchors[anchor]: 0x%x" % anchors[anchor]
        if checkAnchor(anchor,val,compressedStreamEncoded) == 1:
            #if i < 5: # DEBUG
            #    print "\tmatched 3rd anchor check."
            continue
        anchor = (anchor + (4 - 1)) & (3)
        #if i < 5: # DEBUG
        #        print "\tchecking anchors[anchor]: 0x%x" % anchors[anchor]
        if checkAnchor(anchor,val,compressedStreamEncoded) == 1:
            #if i < 5: # DEBUG
            #    print "\tmatched 4th anchor check."
            continue
        #if i < 5: # DEBUG
        #    print "\tno match with anchor. raw."
        anchorIndex = (anchorIndex + 1) & (3)
        anchors[anchorIndex] = val
        pushHeader(3,2,compressedStreamEncoded)
        pushCompressedBitsRaw(val,compressedStreamRaw)

    # Mask out the last partial
    #finalizeCompressedBitsEncoded(compressedStreamEncoded)

    #push an extra dummy code, otherwise compressed streams ends in DATA:DATA
    # it needs to end CODE:DATA:CODE:DATA:CODE:DATA
    pushHeader(0,2,compressedStreamEncoded)
    finalizeCompressedBits(compressedStreamEncoded)

    # append anchors to end of compressed stream
    for i in xrange(len(compressedStreamEncoded)):
        compressedStreamRaw.append(compressedStreamEncoded[i])
    # store the length of data stream in 0th word
    #print "size of compressedStreamRaw: %d" % len(compressedStreamRaw)
    #print "numCompressedWordsEncoded: %d" % numCompressedWordsEncoded
    #print "numCompressedWordsRaw: %d" % numCompressedWordsRaw
    compressedStreamRaw[0] = numCompressedWordsRaw
    #print "compressedStreamRaw[1]: 0x%x" % compressedStreamRaw[1]
    #print "uncompressed[0]: 0x%x" % uncompressed[0]
    #print "uncompressed[1]: 0x%x" % uncompressed[1]


    # FIXME ? what to return? rw_py_compress doesn't use the result anyway
    return numCompressedWordsRaw + numCompressedWordsEncoded

BLOCK_SIZE = 1024

def rw_py_compress(page_size=BLOCK_SIZE*4, VA_start=0, input=None):
    global codeHistogram
    instrList=[]
    for word in (input[i:i+4] for i in xrange(0,len(input),4)):
        if len(word) == 4:
            instrList.append( struct.unpack('I',word)[0] )

    n_blocks = len(instrList)/BLOCK_SIZE
    print "n_blocks of RW = %d"%(n_blocks)
    v_addrs = []
    va = VA_start + 2 + 2 + 4 * n_blocks  #2 bytes for n_blocks, 2 for 0, 4 per block start addr

    compressed_text = []
    for block in xrange(n_blocks):
        v_addrs.append( struct.pack('I',va) )
        compressed = []
        #print "calling deltaCompress, block = %d"%(block)
        deltaCompress(instrList[block*BLOCK_SIZE:(block+1)*BLOCK_SIZE],compressed)
        #print "compressed len = %d"%(len(compressed))
        for word in compressed:
            compressed_text.append(struct.pack('I',word))
        va += 4 * len(compressed)

    print codeHistogram

    print "creating metadata for RW"
    q6zip_rw_alg_version = 0x0001 # 0x<2-byte minor><2-byte major>
    metadata = [struct.pack("H",n_blocks), struct.pack("H",q6zip_rw_alg_version)]
    metadata += v_addrs
    metadata += compressed_text

    return ''.join(metadata)  #joins list elements together as string with no spaces

if __name__ == '__main__':
    rw_py_compress()

