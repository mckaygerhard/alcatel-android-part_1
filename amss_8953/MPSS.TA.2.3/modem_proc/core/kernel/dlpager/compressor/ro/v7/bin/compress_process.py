#! /usr/bin/env python

import multiprocessing
import math
import struct
from collections import Counter

#Pseudo enum type
def enum(**enums):
	return type('Enum', (), enums)

CODE = enum(
			NO_MATCH	       = (     0b011, 3, 0xFFFFFFFF ,"NO_MATCH"),
			DICT1_MATCH        = (     0b100, 3, 0xFFFFFFFF ,"DICT1_MATCH"),
			MATCH_FFFFFFFF_SQ0 = (     0b001, 3, 0xFFFFFFFF ,"MATCH_FFFFFFFF_SQ0"),
			MATCH_FFFFFFFF_SQ1 = (     0b111, 3, 0xFFFFFFFF ,"MATCH_FFFFFFFF_SQ1"),
			MATCH_FFFFFF00_SQ0 = (     0b000, 3, 0xFFFFFF00 ,"MATCH_FFFFFF00_SQ0"), 	#MATCH_6Nx0
			MATCH_FFFFFF00_SQ1 = (     0b110, 3, 0xFFFFFF00 ,"MATCH_FFFFFF00_SQ1"), 	#MATCH_6Nx0
			DICT2_MATCH        = (    0b0101, 4, 0xFFFFFFFF ,"DICT2_MATCH"),
			MATCH_FFFFF000_SQ0 = (    0b0010, 4, 0xFFFFF000 ,"MATCH_FFFFF000_SQ0"),     #MATCH_5Nx0
			MATCH_FFFF0000_SQ0  = (  0b01101, 5, 0xFFFF0000 ,"MATCH_FFFF0000_SQ0"), 	#MATCH_4Nx1			
			MATCH_FFFFF000_SQ1 = (   0b11101, 5, 0xFFFFF000 ,"MATCH_FFFFF000_SQ1"),     #MATCH_5Nx0
			MATCH_FFFF0000_SQ1 = (   0b001010, 6, 0xFFFF0000 ,"MATCH_FFFF0000_SQ1"), 	#MATCH_4Nx1
			MATCH_FFFF00FF_SQ0 = (   0b111010, 6, 0xFFFF00FF ,"MATCH_FFFF00FF_SQ0"),     #MATCH_6Nx1
			MATCH_FF00FFFF_SQ0 = (   0b101010, 6, 0xFF00FFFF ,"MATCH_FF00FFFF_SQ0"),     #MATCH_6Nx2
			MATCH_FFFF00FF_SQ1   = (0b1011010, 7, 0xFFFF00FF ,"MATCH_FFFF00FF_SQ1"),     #MATCH_6Nx1
			MATCH_FF00FFFF_SQ1   = (0b0011010, 7, 0xFF00FFFF ,"MATCH_FF00FFFF_SQ1"),     #MATCH_6Nx2
			END_BLOCK          =   (  0b0011010, 7, 0xFFFFFFFF ,"END_BLOCK")       # Alised onto MATCH_FF00FFFF_SQ1
			)

class CompressionProcess(multiprocessing.Process):

	def __init__(self, dict1Size, dict1Bits, dict2Size, dict2Bits, blockSize, lookbackLen, lookbackBits, instrChunk, dictionary, findFlip1Lookup, findFlip2Lookup, q, statisticsWords,statisticsBits):
		super(CompressionProcess,self).__init__()

		self.listOfMasks = [0xFFFFFFFF,0xFFFFFF00,0xFFFF00FF,0xFF00FFFF,0xFFFFF000,0xFFFF0000,0xFF787E18]
		self.listOfSuperLookbackMasks = [0xFFFFFFFF,0xFFFFFF00,0xFFFFF000]

		self.enableSuperLookback=True

		self.DICTIONARY1_SIZE = dict1Size
		self.DICTIONARY1_BITS = dict1Bits
		self.DICTIONARY2_SIZE = dict2Size
		self.DICTIONARY2_BITS = dict2Bits
		self.BLOCK_SIZE = blockSize 	  #in terms of words
		self.LOOKBACK_LEN = lookbackLen
		self.LOOKBACK_BITS = lookbackBits

		self.instrList = instrChunk
		self.wordSymbolByteDict = dictionary
		self.compressedStream = q
		self.statisticsWords = statisticsWords
		self.statisticsBits = statisticsBits
		self.codeNumOccurances = Counter()
		self.codeBitCount = Counter()

		self.blockStream = []
		self.rawStream = []		

		self.payloadBits = []
		self.offsetdict = {}

		self.numHeaders = 0
		self.numPayloads = 0
		


	def run(self):

		#run the compress class method individually on each block of the instrList
		for instrList in [self.instrList[i:i+self.BLOCK_SIZE] for i in range(0, len(self.instrList), self.BLOCK_SIZE)]:
			instrList.extend([0] * (self.BLOCK_SIZE-len(instrList)))
			self.compress(instrList)
		#copy stats to threadsafe structure
		for (stat,count) in self.codeNumOccurances.iteritems():
			self.statisticsWords[stat] = count
		for (stat,count) in self.codeBitCount.iteritems():
			self.statisticsBits[stat] = count


	############################################################################
	#check if there's a match directly after where last match was found
	############################################################################
	def findSequentialLookback(self,instrList,listLen,instr,codeTuple,*extraBitsList):
		if (self.lastOffsetFromEndOfListX==False):
			return (False)
		codeMask=codeTuple[2]
		codeStr=codeTuple[3]
		instr&=codeMask
		lastOffset = self.lastOffsetFromEndOfListX
		if lastOffset<listLen:
			#next=instrList[listLen - lastOffset - 1]
			next=instrList[listLen + lastOffset ]
			if (next&codeMask) == instr:
				codeBits=codeTuple[0]
				codeLen=codeTuple[1]
				#self.pushCompressedBits(codeStr,(codeBits,codeLen))
				self.pushHeaderBits(codeStr,(codeBits,codeLen))
				for extraBits in extraBitsList:
				  #print (extraBitsList), 
				  #print "check findSequentialLookback(), you are hosed! ", 
				  self.pushPayloadBits(codeStr,extraBits)


				self.codeNumOccurances[codeStr]+=1
				return (True)
		return (False)

	def findSuperLookback(self,instrList,listLen,instr,firstMask,maxLookBackLen):
		#Looks for a super mask match
		if self.enableSuperLookback:
			if firstMask in self.listOfSuperLookbackMasks:
				if (listLen+1)<len(instrList):
					firstMaskedInstr=instr&firstMask
					for secondMask in self.listOfSuperLookbackMasks:
						secondMaskedInstr=instrList[listLen+1]&secondMask
						superInstr=(firstMaskedInstr<<32)|secondMaskedInstr
						superMask=(firstMask<<32)|secondMask
						idx = self.offsetdict.get((superMask,superInstr),False)
						if idx != False:
							#convert from index to an offset from end 
							negOffset=idx-listLen
							if negOffset >= 0:
								print "negOffset>=0 %d"%negOffset
							if negOffset > (-maxLookBackLen):
								#print "idx=%d deltaOffset=%d listLen=%d negOffset=%d maxLookBackLen=%d"%(idx,deltaOffset,listLen,negOffset,-maxLookBackLen)
								return(negOffset)

		#Super mask not found, so look for a single mask match
		idx = self.offsetdict.get((firstMask,instr),False)
		if idx == False:
			return (False)
		if (instrList[idx]&firstMask)!=instr:
			print "Dictionary error"
		#convert from index to an offset from end 
		deltaOffset=(listLen - idx - 1) 
		negOffset=idx-listLen
		if negOffset >= 0:
			print "negOffset>=0 %d"%negOffset
		if negOffset > (-maxLookBackLen):
			#print "idx=%d deltaOffset=%d listLen=%d negOffset=%d maxLookBackLen=%d"%(idx,deltaOffset,listLen,negOffset,-maxLookBackLen)
			return(negOffset)
		return(False)

	def findLookback(self,instrList,listLen,instr,codeTuple,*extraBitsList):
		codeMask=codeTuple[2]
		codeStr=codeTuple[3]
		instr&=codeMask
		maxLookBackLen=self.LOOKBACK_LEN

		#check if masked match exists in list 
		negOffset=self.findSuperLookback(instrList,listLen,instr,codeMask,maxLookBackLen)
		#if no match, return nothing
		if negOffset == False:
			return(False)

		#otherwise, store match in dictionary of matches, and return match
		self.lastOffsetFromEndOfListX=negOffset

		codeBits=codeTuple[0]
		codeLen=codeTuple[1]
		self.pushHeaderBits(codeStr,(codeBits,codeLen))
		self.pushPayloadBits(codeStr,(negOffset,self.LOOKBACK_BITS))
		#self.pushCompressedBits(codeStr,(codeBits,codeLen))		
		#self.pushCompressedBits(codeStr,(negOffset,self.LOOKBACK_BITS))

		#print negOffset

		for extraBits in extraBitsList:
			#print (extraBitsList), 
			#print "check findLookback(), you are hosed! ", 
			self.pushPayloadBits(codeStr,extraBits)

		self.codeNumOccurances[codeStr]+=1

		#print "LB[%d] negOffset=%s payload=%s"%(self.codeNumOccurances[codeStr],(negOffset,self.LOOKBACK_BITS),extraBits)

		return (True)

	def  findDictionaryEntry1(self,instr):
		try:
			index=self.wordSymbolByteDict[instr]
			if index<self.DICTIONARY1_SIZE:
				codeTuple=CODE.DICT1_MATCH			
				codeStr=codeTuple[3]
				codeBits=codeTuple[0]
				codeLen=codeTuple[1]
				#self.pushCompressedBits(codeStr,(codeBits,codeLen))
				#self.pushCompressedBits(codeStr,(index,self.DICTIONARY1_BITS))
				self.pushHeaderBits(codeStr, (codeBits, codeLen))
				self.pushPayloadBits(codeStr, (index, self.DICTIONARY1_BITS))
				self.codeNumOccurances[codeStr]+=1
				return(True)
			return (False)
		except KeyError:
			return(False)

	def  findDictionaryEntry2(self,instr):
		try:
			index=self.wordSymbolByteDict[instr]
			if index>=self.DICTIONARY1_SIZE:
				codeTuple=CODE.DICT2_MATCH			
				codeStr=codeTuple[3]
				index=index-self.DICTIONARY1_SIZE
				codeBits=codeTuple[0]
				codeLen=codeTuple[1]
				#self.pushCompressedBits(codeStr,(codeBits,codeLen))
				#self.pushCompressedBits(codeStr,(index,self.DICTIONARY2_BITS))
				self.pushHeaderBits(codeStr, (codeBits, codeLen))
				self.pushPayloadBits(codeStr, (index, self.DICTIONARY2_BITS))
				#self.payloadString.append("DICT2_PAYLD_BITS")
				self.codeNumOccurances[codeStr]+=1
				return(True)
			return (False)
		except KeyError:
			return(False)

	def pushRawBits32(self,codeStr,bits32):

		#print " ", "{0:#0{1}b}".format(bits,numBits+2),
		self.codeBitCount[codeStr]+= 32
		rawWord =  (bits32&0xFFFFFFFF)
		word=struct.pack('I', (rawWord&0xFFFFFFFF))
		self.rawStream.append(word)
		#print "Raw word[%d]=%x "%(self.numPayloads,rawWord)
		self.numPayloads+=1
		#print "rawStream: ", self.rawStream

	compressedPartialWord=0
	compressedPartialBits=0
	
	def pushCompressedBits(self,codeStr,bitsNumBitsTuple):

		#print " ", "{0:#0{1}b}".format(bits,numBits+2),
		bits=bitsNumBitsTuple[0]
		numBits=bitsNumBitsTuple[1]
		#print "Compressed Bits[%s] Num=%d Bits=0x%d "%(codeStr,numBits,bits)
		bits=bits&((1<<numBits)-1)
		self.compressedPartialWord |= bits<<self.compressedPartialBits
		self.compressedPartialBits += numBits
		self.codeBitCount[codeStr]+= numBits
		while self.compressedPartialBits >= 32:
			compressedWord = self.compressedPartialWord & 0xFFFFFFFF
			self.compressedPartialWord >>= 32
			self.compressedPartialBits -= 32
			word=struct.pack('I', compressedWord & 0xFFFFFFFF)            
			self.blockStream.append(word)
			#self.blockStream.append(compressedWord)

			
	def pushHeaderBits(self, codeStr, bitsNumBitsTuple):
		"""push 4 bits of header for next jump target
			push all payload bits to the compressed stream
			push any remaining bits as payload
		"""        
		(bits, numBits) = bitsNumBitsTuple  
		bits1 = bits&0XF
		numBits1 = numBits
		if(numBits1 > 4):
			numBits1 = 4

		self.pushCompressedBits(codeStr, (bits&0xF, numBits1))		  
		
		while(self.payloadBits):				
			(codeStr1, bitsnumBitsTuple1) = self.payloadBits.pop(0)
			self.pushCompressedBits(codeStr1, bitsnumBitsTuple1)

		if(numBits > 4):
			bits = bits >> 4
			numBits = numBits - 4
			self.pushPayloadBits(codeStr, (bits, numBits))
		
	"""
	#payloadString = []
	def pushHeaderBits(self, codeStr, bitsNumBitsTuple):		
        
		(bits, numBits) = bitsNumBitsTuple  
			
		if numBits <= 4:
			#if numBits == 3:
			#	print "NO_MATCH_HDR_BITS ",
			#elif numBits == 4:
			#	print "DICT2_HDR_BITS ",
			self.pushCompressedBits(codeStr, bitsNumBitsTuple)		  
		else:			
			#print "END_BLOCK_HDR1_BITS ",
			self.pushCompressedBits("JUMP BITS", (bits&0xF, 0x4))
			numBits -= 4
			bits >>= 4
			#self.payloadString.append("END_BLOCK_HDR2_BITS")
			self.pushPayloadBits(codeStr, (bits, numBits))

		while(self.payloadBits):	
			#print self.payloadString.pop(0),
			(codeStr1, bitsnumBitsTuple1) = self.payloadBits.pop(0)
			self.pushCompressedBits(codeStr1, bitsnumBitsTuple1)
	"""

	def pushPayloadBits(self, codeStr, bitsNumBitsTuple):
		self.payloadBits.append((codeStr, bitsNumBitsTuple))
	

	############################################			
	def encode(self,instrList,i,instr):
		if (self.findSequentialLookback(instrList,i,instr,CODE.MATCH_FFFFFFFF_SQ1)):
			return
		if (self.findSequentialLookback(instrList,i,instr,CODE.MATCH_FFFFFF00_SQ1,(instr&0xFF,8))):
			return
		if (self.findLookback(instrList,i,instr,CODE.MATCH_FFFFFFFF_SQ0)):
			return
		if (self.findDictionaryEntry1(instr)):
			return
		if (self.findSequentialLookback(instrList,i,instr,CODE.MATCH_FFFF00FF_SQ1,((instr>>8)&0xFF,8))):
			return
		if (self.findSequentialLookback(instrList,i,instr,CODE.MATCH_FF00FFFF_SQ1,((instr>>16)&0xFF,8))):
			return
		if (self.findDictionaryEntry2(instr)):
			return
		if (self.findSequentialLookback(instrList,i,instr,CODE.MATCH_FFFFF000_SQ1,((instr&0xFFF),12))):
			return
		if (self.findLookback(instrList,i,instr,CODE.MATCH_FFFFFF00_SQ0,(instr&0xFF,8))):
			return
		if (self.findSequentialLookback(instrList,i,instr,CODE.MATCH_FFFFF000_SQ1,(instr&0xFFFF,16))):
			return
		if (self.findLookback(instrList,i,instr,CODE.MATCH_FFFF00FF_SQ0,((instr>>8)&0xFF,8))):
			return
		if (self.findLookback(instrList,i,instr,CODE.MATCH_FF00FFFF_SQ0,((instr>>16)&0xFF,8))):
			return		
		if (self.findLookback(instrList,i,instr,CODE.MATCH_FFFFF000_SQ0,((instr&0xFFF),12))):
			return
		if (self.findLookback(instrList,i,instr,CODE.MATCH_FFFF0000_SQ0,((instr&0xFFFF),16))):
			return

		###############################
		# No match so send the raw bits
		###############################
		codeTuple=CODE.NO_MATCH
		codeBits=codeTuple[0]
		codeLen=codeTuple[1]
		codeStr=codeTuple[3]
		#self.pushCompressedBits(codeStr,(codeBits,codeLen))		
		self.pushHeaderBits(codeStr,(codeBits,codeLen))		
		self.pushRawBits32(codeStr,(instr&0xFFFFFFFF))
		#self.pushPayloadBits(codeStr, ((instr&0xFFFFFFFF), 32))
		self.codeNumOccurances[codeStr]+=1
		#print "[%4.4d] instr=0x%8.8x"%(i,instr)

		#print "RawWord[%d]=%x"%(self.codeNumOccurances[codeStr],instr)

	############################################			
	def compress(self,instrList):
		#print
		#print "compress: len of instrList: ", len(instrList)
		self.offsetdict.clear()
		self.lastOffsetFromEndOfListX=False
		
		#############################################
		for i,instr in enumerate(instrList):

			self.encode(instrList,i,instr)
			#Add new instr to the lookback dictionary
			for mask in self.listOfMasks:
				maskedInstr=instr&mask
				self.offsetdict[(mask,maskedInstr)] = i

			if self.enableSuperLookback:
				for firstMask in self.listOfSuperLookbackMasks:
					firstMaskedInstr=instr&firstMask
					for secondMask in self.listOfSuperLookbackMasks:
						if (i+1)<len(instrList):
							secondMaskedInstr=instrList[i+1]&secondMask
							superInstr=(firstMaskedInstr<<32)|secondMaskedInstr
							superMask=(firstMask<<32)|secondMask
							self.offsetdict[(superMask,superInstr)] = i
		 
		## END FOR LOOP #######################################

		#######################################################
		#finish block is aliased onto the lowest freq code
		#we check for the number of words decoded on rx'ing this code
		#######################################################
		codeTuple=CODE.END_BLOCK
		codeBits=codeTuple[0]
		codeLen=codeTuple[1]
		codeStr=codeTuple[3]
		#self.pushCompressedBits(codeStr,(codeBits,codeLen))
		self.pushHeaderBits(codeStr,(codeBits,codeLen))
		self.codeNumOccurances[codeStr]+=1		
		

		#######################################################
		#pad and push remaining bits in word stream
		#######################################################
		"""
		if self.headerPartialBits>0:
			headerWord = self.headerPartialWord & 0xFFFFFFFF
			word=struct.pack('I', headerWord & 0xFFFFFFFF)
			self.headerStream.append(word)
		self.headerPartialBits = 0
		self.headerPartialWord = 0

		if self.payloadPartialBits>0:
			payloadWord = self.payloadPartialWord & 0xFFFFFFFF
			word=struct.pack('I', payloadWord & 0xFFFFFFFF)
			self.payloadStream.append(word)
		self.payloadPartialBits = 0
		self.payloadPartialWord = 0
		"""

		while(self.payloadBits):	
			(codeStr1, bitsnumBitsTuple1) = self.payloadBits.pop(0)
			self.pushCompressedBits(codeStr1, bitsnumBitsTuple1)

		if self.compressedPartialBits>0:
			compressedWord = self.compressedPartialWord & 0xFFFFFFFF
			word=struct.pack('I', compressedWord & 0xFFFFFFFF)
			self.blockStream.append(word)
		self.compressedPartialBits = 0
		self.compressedPartialWord = 0


		# We want the raw compressed data to be double word/64 bit aligned
		# Format of stream is | numRawWords | numRawWords*32bits |
		# So if we have an even number of raw words, add another one
		if (len(self.rawStream)%2 == 0):
			#self.stream.append(struct.pack('I', 0x0))
			#numRawWords=numRawWords+1
			#print "adjusting num raw words="
			#print "adjusting num raw words="+str(len(self.rawStream))
			self.rawStream.append(struct.pack('I', (0x0)))

		#add block stream to compressed stream
		self.stream = []
		numRawWords=struct.pack('I', len(self.rawStream) & 0xFFFFFFFF)
		#print "num raw words %d"%len(self.rawStream)
		
		self.stream.append(numRawWords)
		for word in self.rawStream:
			self.stream.append(word)
			#print 'stream[0] = ', struct.unpack('I', self.blockStream[0])
			#print 'stream[1] = ', struct.unpack('I', self.blockStream[1])

		for word in self.blockStream:
			self.stream.append(word)
		if(len(self.stream)%2 != 0):
			self.stream.append(struct.pack('I', (0x0)))	

		self.compressedStream.append(self.stream) 
			
		self.blockStream = []
		self.rawStream = []
		self.payloadBits = []		


	"""
	headerPartialWord = 0
	headerPartialBits = 0

	def pushHeaderBits(self, codeStr, bitsNumBitsTuple):
		(bits,numBits) = bitsNumBitsTuple
		bits &= ((1 << numBits) - 1)
		self.headerPartialWord |= bits << self.headerPartialBits
		self.headerPartialBits += numBits
		self.codeBitCount[codeStr]+= numBits
		while self.headerPartialBits >=  32:
			headerWord = self.headerPartialWord & 0xFFFFFFFF
			self.headerPartialWord >>= 32
			self.headerPartialBits -= 32
			word=struct.pack('I', headerWord & 0xFFFFFFFF)
			self.headerStream.append(word)

	payloadPartialWord = 0
	payloadPartialBits = 0
	def pushPayloadBits(self, codeStr, bitsNumBitsTuple):
		(bits,numBits) = bitsNumBitsTuple
		bits &= ((1 << numBits) - 1)
		self.payloadPartialWord |= bits << self.payloadPartialBits
		self.payloadPartialBits += numBits
		self.codeBitCount[codeStr]+= numBits
		while self.payloadPartialBits >=  32:
			payloadWord = self.payloadPartialWord & 0xFFFFFFFF
			self.payloadPartialWord >>= 32
			self.payloadPartialBits -= 32
			word=struct.pack('I', payloadWord & 0xFFFFFFFF)
			self.payloadStream.append(word)


	"""
