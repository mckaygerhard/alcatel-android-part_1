/***********************************************************************
 * tftp_threads_modem.c
 *
 * NON-HLOS Posix Threads API Abstraction
 * Copyright (c) 2014-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 *
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.mpss/3.9.1/storage/tftp/os/src/tftp_threads_modem.c#1 $ $DateTime: 2015/09/29 21:52:15 $ $Author: pwbldsvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2015-01-05   dks   Compile server for TN Apps
2014-12-30   dks   Fixes to config and log module.
2014-10-14   rp    Use asserts for control-logic, debug-asserts for data-logic
2014-08-04   rp    Debug checks to ensure mutex calls succeed.
2014-07-29   dks   Featurize creating Detached threads.
2014-07-28   rp    Add api to get thread-id.
2014-06-11   rp    Renamed DEBUG_ASSERT as TFTP_DEBUG_ASSERT
2014-06-04   rp    Create

===========================================================================*/

#include "tftp_config_i.h"
#include "tftp_comdef.h"
#include "tftp_threads.h"
#include "tftp_threads_modem.h"
#include "tftp_assert.h"
#include "tftp_log.h"
#include <stdio.h>

#if !defined (TFTP_NHLOS_BUILD)
  #error "This file should be included only for NHLOS Builds"
#endif

int
tftp_thread_mutex_init (tftp_mutex_handle *hdl_ptr)
{
  uint32 result = 0;
  pthread_mutexattr_t mutex_attr;

  result = pthread_mutexattr_init (&mutex_attr);
  TFTP_ASSERT (result == 0);
  if (result != 0)
  {
    return result;
  }

  result = pthread_mutexattr_settype (&mutex_attr, PTHREAD_MUTEX_RECURSIVE);
  TFTP_ASSERT (result == 0);
  if (result != 0)
  {
    goto End;
  }

  result = pthread_mutex_init (hdl_ptr, &mutex_attr);
  TFTP_ASSERT (result == 0);

End:
  (void) pthread_mutexattr_destroy (&mutex_attr);
  return result;
}

int
tftp_thread_lock (tftp_mutex_handle *hdl_ptr)
{
  int result = 0;

  result = pthread_mutex_lock (hdl_ptr);
  TFTP_ASSERT (result == 0);
  return result;
}

int
tftp_thread_unlock (tftp_mutex_handle *hdl_ptr)
{
  int result = 0;

  result = pthread_mutex_unlock (hdl_ptr);
  TFTP_ASSERT (result == 0);
  return result;
}

tftp_thread_handle
tftp_thread_self (void)
{
  tftp_thread_handle thread_handle = (tftp_thread_handle)NULL;

  thread_handle = pthread_self ();

  return (tftp_thread_handle) thread_handle;
}

#ifdef FEATURE_TFTP_SERVER_BUILD

#define TFTP_SERVER_PRIO             85  /* 0 - 200, 0->least priority */
#define TFTP_PTHREAD_CREATE_DETACHED 1

volatile static uint32 tftp_server_prio = TFTP_SERVER_PRIO;

int
tftp_thread_create(tftp_thread_handle *thread,
                   tftp_thread_return_type (*thread_start) (void *),
                   void *args)
{
  int result;
  pthread_attr_t pthread_attr;
  static int tftp_pthread_count = 0;
  char threadname[12];

  tftp_pthread_count++;
  snprintf (threadname, sizeof (threadname), "TFTP_%d", tftp_pthread_count);

  result = pthread_attr_init(&pthread_attr);
  TFTP_ASSERT (result == 0);

  pthread_attr_setstacksize (&pthread_attr, (4*1024));

  pthread_attr_setdetachedstate (&pthread_attr,
                                 TFTP_PTHREAD_CREATE_DETACHED);

  (void)pthread_attr_setthreadname (&pthread_attr, threadname);

  result = pthread_create (thread, &pthread_attr, thread_start, args);
  if (result != 0)
  {
    TFTP_LOG_ERR ("tftp_thread_create failed for count %d, error: %d %s",
                  tftp_pthread_count, result, strerror (result));
    return result;
  }

  result = pthread_setschedprio (*thread, tftp_server_prio);
  TFTP_ASSERT (result == 0);

  (void) pthread_attr_destroy (&pthread_attr);
  return result;
}

#endif /* TFTP_SPARROW_BUILD */

