/*=============================================================================

FILE:         qdss_ctrl_q6b_etm_stub.c

DESCRIPTION:  ETM not applicable

=============================================================================*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/ctrl_q6b/src/qdss_ctrl_q6b_etm_stub.c#1 $
=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "qdss_q6b_control.h"
#include "qdss_control_q6b.h"
#include "qdss_ctrl_q6b_etm.h"

/*---------------------------------------------------------------------------
 * Function Definitions
 * ------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
int qdss_ctrl_q6b_etm_get_param(uint16 param_id, uint32 *pval)
{
   return QDSS_CONTROL_RVAL_NOT_SUPPORTED;
}
int qdss_ctrl_q6b_etm_set_param(uint16 param_id, uint32 val)
{
   return QDSS_CONTROL_RVAL_NOT_SUPPORTED;
}

/*-------------------------------------------------------------------------*/
int qdss_ctrl_q6b_etm_get_config(const char *pget_str,
                                 char *presp_str,
                                 size_t resp_max_len)
{
   return QDSS_CONTROL_RVAL_NOT_SUPPORTED;
}
int qdss_ctrl_q6b_etm_set_config(const char *pset_str)
{
   return QDSS_CONTROL_RVAL_NOT_SUPPORTED;
}

/*-------------------------------------------------------------------------*/
int qdss_ctrl_q6b_etm_enable(void)
{
   return QDSS_CONTROL_RVAL_NOT_SUPPORTED;
}

int qdss_ctrl_q6b_etm_disable(void)
{
   return QDSS_CONTROL_RVAL_NOT_SUPPORTED;
}

/*-------------------------------------------------------------------------*/
int qdss_q6b_ctrl_register_etm_enable_cb(
   qdss_q6b_ctrl_etm_enable_cb_t cb_fcn)
{
   return QDSS_Q6B_CTRL_RVAL_NOT_SUPPORTED;
}
int qdss_q6b_ctrl_register_etm_set_config_cb(
   qdss_q6b_ctrl_etm_set_config_cb_t cb_fcn)
{
   return QDSS_Q6B_CTRL_RVAL_NOT_SUPPORTED;
}

/*-------------------------------------------------------------------------*/
void qdss_ctrl_q6b_etm_init(void)
{
   return;
}


