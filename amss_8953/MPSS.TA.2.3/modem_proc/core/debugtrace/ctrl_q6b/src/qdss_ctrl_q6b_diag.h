#ifndef QDSS_CTRL_Q6B_DIAG_H
#define QDSS_CTRL_Q6B_DIAG_H

/*=============================================================================

FILE:         qdss_ctrl_q6b_diag.h

DESCRIPTION:  QDSS control, acting proxy, for Q6B

=============================================================================*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/ctrl_q6b/src/qdss_ctrl_q6b_diag.h#1 $
=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "qdss_ctrl_diag.h"

/*---------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ------------------------------------------------------------------------*/

int qdss_ctrl_filter_etm_q6b_handler(qdss_ctrl_filter_etm_req *pReq,
                                     int req_len,
                                     qdss_ctrl_filter_etm_rsp *pRsp,
                                     int rsp_len);

int qdss_ctrl_etm_get_param_q6b_handler(qdss_ctrl_etm_get_param_req *pReq,
                                        int req_len,
                                        qdss_ctrl_etm_get_param_rsp *pRsp,
                                        int rsp_len);

int qdss_ctrl_etm_set_param_q6b_handler(qdss_ctrl_etm_set_param_req *pReq,
                                        int req_len,
                                        qdss_ctrl_etm_set_param_rsp *pRsp,
                                        int rsp_len);

int qdss_ctrl_filter_etm_rpm_q6b_handler(qdss_ctrl_filter_etm_rpm_req *pReq,
                                         int req_len,
                                         qdss_ctrl_filter_etm_rpm_rsp *pRsp,
                                         int rsp_len);

/* ----------------------------------------------------------------------- */

void qdss_ctrl_q6b_diag_init(void);

#endif /* QDSS_CTRL_Q6B_DIAG_H */

