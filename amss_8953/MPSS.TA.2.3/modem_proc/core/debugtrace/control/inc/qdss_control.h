#ifndef QDSS_CONTROL_H
#define QDSS_CONTROL_H

/*=============================================================================

FILE:         qdss_control.h

DESCRIPTION:  QDSS control API for debug agent

==============================================================================*/
/*=============================================================================
  Copyright (c) 2013-2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/control/inc/qdss_control.h#1 $
=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include <stdio.h>
#include "comdef.h"
#include "DALStdDef.h"

/*---------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ------------------------------------------------------------------------*/

/**
  Return values for QDSS control function call.
*/
#define QDSS_CONTROL_RVAL_SUCCESS         0  /**< Successfully completed. */
#define QDSS_CONTROL_RVAL_UNKNOWN_ERR     1  /**< Incomplete, failure, error occurred. */
#define QDSS_CONTROL_RVAL_NOT_SUPPORTED   2
#define QDSS_CONTROL_RVAL_MALFORMED       3
#define QDSS_CONTROL_RVAL_INVALID_ARG     4
#define QDSS_CONTROL_RVAL_MISSING_ARG     5
#define QDSS_CONTROL_RVAL_ACCESS_DENIED   6
#define QDSS_CONTROL_RVAL_SYNCH_ERR       7  /**< Device sync error occurred. */

/*=========================================================================
  Trace Sink
 =========================================================================*/

/**
  @brief  Get the current trace sink

  @param [out] sinkid : Current sink id is returned here
                        See DDITMC.h for supported sinkids

  @return 0 if successful, error code otherwise
 */
int qdss_control_get_sink(uint8 *sinkid);

/*-------------------------------------------------------------------------*/

/**
  @brief Set the trace sink

  @param [in ] sinkid : New trace sink
                        See DDITMC.h for supported sinkids

  @return 0 if successful, error code otherwise
 */
int qdss_control_set_sink(uint8 sinkid);

/*-------------------------------------------------------------------------*/

/**
  @brief  Return a list of supported trace sink ids

  @param [out] sinkid_list : Pointer to a buffer where supported
                             sinkids will be listed
  @param [in ] list_size   : Size of the input buffer
  @param [out] filled      : Number of trace sink ids returned

  @return 0 if successful, error code otherwise
 */
int qdss_control_get_sink_list(uint8 *sinkid_list, int list_size, int *filled);

/*=========================================================================
  ETM
 =========================================================================*/

/**
  @brief Get the current state of ETM on this core

  @param [out] state : ETM state 0 (disabled) or 1 (enabled)

  @return 0 if successful, error code otherwise
 */
int qdss_control_get_etm(uint8 *state);

/*-------------------------------------------------------------------------*/

/**
  @brief  Enable or disable ETM on this CPU

  @param [in ] state : 1 (enable), 0 (disable)
                       Bits [7:1] Overloaded to set ETM mode.

  @return 0 if successful, error code otherwise
 */
int qdss_control_set_etm(uint8 state);

/*-------------------------------------------------------------------------*/

/**
  @brief Get the ETM's configuration on this core

  @param [in ] pget_str       : Configuration being requested.
         [out] presp_str      : Setting of the config/
         [in ] resp_max_len   : Maximum length of all pstr's.

  @return QDSS_CONTROL_RVAL_...
 */
int qdss_control_get_etm_config(const char *pget_str,
                                char *presp_str,
                                size_t resp_max_len);

/*-------------------------------------------------------------------------*/

/**
  @brief  Set ETM's configuration on this core

  @param [in ] pset_str : Pointer to Null terminated string of
                          configuration being requested.

  @return QDSS_CONTROL_RVAL...
 */
int qdss_control_set_etm_config(const char *pset_str);

/*-------------------------------------------------------------------------*/

/**
  @brief Get the current state of ETM on RPM

  @param [out] state : ETM state: 0 (disabled) or 1 (enabled)

  @return 0 if successful, error code otherwise
 */
int qdss_control_get_etm_rpm(uint8 *state);

/*-------------------------------------------------------------------------*/

/**
  @brief  Enable or disable ETM on RPM

  @param [in ] state : 1 (enable), 0 (disable)

  @return 0 if successful, error code otherwise
 */
int qdss_control_set_etm_rpm(uint8 state);

/*========================================================================
 Funnel
=========================================================================*/

/**
  @brief Get the state of a funnel

  @param [in ] name  : Trace funnel name
                       See qdss_tfunnel.h for supported names
  @param [out] state : Current state of the funnel is returned here

  @return 0 if successful, error code otherwise
 */
int qdss_control_get_funnel(const char *name, uint8 *state);

/*-------------------------------------------------------------------------*/

/**
  @brief  Set the state of a funnel

  @param [in ] name  : Trace funnel name
  @param [in ] state : Enable (1) or disable (0)

  @return 0 if successful, error code otherwise
 */
int qdss_control_set_funnel(const char *name, uint8 state);

/*=========================================================================
 STM
 =========================================================================*/

/**
  @brief   Get the state of STM

  @param [in ] state : STM state is returned here

  @return 0 if successful, error code otherwise
 */
int qdss_control_get_stm(uint8 *state);

/*-------------------------------------------------------------------------*/

/**
  @brief Set STM state

  @param [in ] state : 1 (enable) or 0 (disable)

  @return 0 if successful, error code otherwise
 */
int qdss_control_set_stm(uint8 state);

/*=========================================================================
 HWE
 =========================================================================*/
/*-------------------------------------------------------------------------*/

/**
  @brief Get the state of HW events

  @param [in ] state : HW events state is returned here.
                       1 (HW events enabled), 0 HW events disabled

  @return 0 if successful, error code otherwise
 */
int qdss_control_get_hwe(uint8 *state);

/*-------------------------------------------------------------------------*/

/**
  @brief Set the state of HW events

  @param [in ] state : 1 (enable) or 0 (disable)

  @return 0 if successful, error code otherwise
 */
int qdss_control_set_hwe(uint8 state);

/*-------------------------------------------------------------------------*/

/**
  @brief Set a HW event register

  @param [in ] addr : address of register to set
  @param [in ] val  : value of register to set

  @return 0 if successful, error code otherwise
 */
int qdss_control_set_hwe_reg(unsigned long addr, unsigned long val);

/*-------------------------------------------------------------------------*/

/**
  @brief Set mask for HW event detection

  @param [in ] mask : 32-bit mask to be set

  @return 0 if successful, error code otherwise
 */
int qdss_control_set_hwe_detect_mask(unsigned long mask);

/*-------------------------------------------------------------------------*/

/**
  @brief Set mask for HW event trigger

  @param [in ] mask : 32-bit mask to be set

  @return 0 if successful, error code otherwise
 */

int qdss_control_set_hwe_trigger_mask(unsigned long mask);


#endif //QDSS_CONTROL_H

