#ifndef QDSS_QMI_CTRL_SVC_UTILS_H
#define QDSS_QMI_CTRL_SVC_UTILS_H
/*
  File: qdss_qmi_ctrl_svc_utils.h

  QDSS QMI Control Service common utilities
*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/qmi/ctrl_svc_common/src/qdss_qmi_ctrl_svc_utils.h#1 $

  when       who     what, where, why
  --------   ---    -----------------------------------------------------------
  02/02/15   lht    Initial release

=============================================================================*/

/*---------------------------------------------------------------------------
 * Common code execution by service request message handlers.
 * ------------------------------------------------------------------------*/

/* ----------------------------------------------------------------------- */
#define qdss_qmi_ctrl_svc_utils_verify_request_size(msg_t) \
   if (req_c_struct_len < sizeof(msg_t)) \
   {  resp_msg.resp.result = QMI_RESULT_FAILURE_V01; \
      resp_msg.resp.error = QMI_ERR_MALFORMED_MSG_V01; \
   } \
   else

/* ----------------------------------------------------------------------- */
#define qdss_qmi_ctrl_svc_utils_send_resp_and_return(msg_t) \
   if (QMI_CSI_NO_ERR == \
       qmi_csi_send_resp(req_handle, msg_id, (void *)pResp_msg, \
                         sizeof(msg_t))) \
   { return QMI_CSI_CB_NO_ERR; } \
   else \
   { return QMI_CSI_CB_INTERNAL_ERR; }

/* ----------------------------------------------------------------------- */
#define qdss_qmi_ctrl_svc_utils_resp_not_supported(msg_t) \
   msg_t resp_msg; \
   msg_t *pResp_msg = &resp_msg; \
   memset(pResp_msg, 0, sizeof(msg_t)); \
   resp_msg.resp.result = QMI_RESULT_FAILURE_V01; \
   resp_msg.resp.error = QMI_ERR_NOT_SUPPORTED_V01; \
   qdss_qmi_ctrl_svc_utils_send_resp_and_return(msg_t)


#endif /* QDSS_QMI_CTRL_SVC_UTILS_H */

