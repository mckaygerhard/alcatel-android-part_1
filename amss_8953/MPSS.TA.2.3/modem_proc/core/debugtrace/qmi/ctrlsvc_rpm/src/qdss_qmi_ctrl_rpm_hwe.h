#ifndef QDSS_QMI_CTRL_RPM_HWE_H
#define QDSS_QMI_CTRL_RPM_HWE_H
/*
  FILE:         qdss_qmi_ctrl_rpm_hwe.h

  QDSS QMI control service handlers for HW event requests for RPM
*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/qmi/ctrlsvc_rpm/src/qdss_qmi_ctrl_rpm_hwe.h#1 $

  when       who     what, where, why
  --------   ---    -----------------------------------------------------------
  01/26/15   lht    Initial release

=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/
#include "qmi_csi.h"

/*---------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ------------------------------------------------------------------------*/
/*===========================================================================*/
qmi_csi_cb_error qdss_qmi_ctrl_rpm_set_hwe_reg_h (
   qmi_req_handle req_handle,  //in
   unsigned int msg_id,  //in
   void *req_c_struct,  //in
   unsigned int req_c_struct_len,  //in
   void *service_cookie  //in
);

#endif /* #ifndef QDSS_QMI_CTRL_RPM_HWE_H */

