#ifndef QDSS_QMI_CTRL_SVC_RPM_H
#define QDSS_QMI_CTRL_SVC_RPM_H
/*
  File:         qdss_qmi_service_rpm.h

  QDSS QMI Control Service for RPM (proxy for second modem)
*/
/*=============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
/*=============================================================================
  $Header: //components/rel/core.mpss/3.9.1/debugtrace/qmi/ctrlsvc_rpm/src/qdss_qmi_ctrl_svc_rpm.h#1 $

  when       who     what, where, why
  --------   ---    -----------------------------------------------------------
  01/26/15   lht    Initial release

=============================================================================*/

/*---------------------------------------------------------------------------
 * Include Files
 * ------------------------------------------------------------------------*/

#include "qmi_csi.h"
#include "qmi_csi_target_ext.h"

/*---------------------------------------------------------------------------
 * Type Declarations
 * ------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ------------------------------------------------------------------------*/
qmi_csi_service_handle qdss_qmi_ctrl_svc_rpm_init(qmi_csi_os_params *os_params);

/*===========================================================================*/

#endif /* #ifndef QDSS_QMI_CTRL_SVC_RPM_H */

