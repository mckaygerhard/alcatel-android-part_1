/*===========================================================================

                     S E C U R I T Y    S E R V I C E S

                S E C U R E  B O O T   X.5 0 9  P A R S E R

                               M O D U L E

FILE:  debug_policy.c

DESCRIPTION:
  Debug policy implementation

EXTERNALIZED FUNCTIONS


Copyright (c) 2013-2014 by Qualcomm Technologies, Inc. All Rights Reserved.
===========================================================================*/

/*=========================================================================

                          EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/core.mpss/3.9.1/securemsm/secpil/oem/debug_policy.c#1 $
$DateTime: 2015/09/29 21:52:15 $
$Author: pwbldsvc $

when         who                what, where, why
--------   ----               ------------------------------------------- 
05/05/14   st                  Initial version
===========================================================================*/


/*==========================================================================

           Include Files for Module

==========================================================================*/
#include "sec_pil_util.h"
#include "secboot_x509.h"
#include "sec_debug_policy.h"
#include "debug_policy_private.h"
#include "HALhwio.h"
#include "mba_hwio.h"
#include "mba_rmb.h"

dbg_policy_t dbg_policy_local;

boolean debug_policy_is_valid = FALSE;
uint32 range_limit = 0x01000000;
uint32 dp_revision = 0x01000000;
uint32 policy_id_array_size = 0x20000000;
uint32 policy_cert_array_size = 0x08000000;


extern void debug_policy_override_jtag();

/*!
* 
* @brief
*    This function checks if debug policy disable fuse bit is blown or not
*    The DEBUG_POLICY_DISABLE fuse bit has been difined for this purpose on 8996
*
* @param[in]  dbgp_ptr              
*
* @retval
* @param[in/out]  TRUE   means the bit is 1 and debug policy should be by passed.
*                 FALSE  means the bit is 0 and debug policy shall be loaded.
* 
*/
boolean is_debug_policy_disable_fuse_bit_set()
{
// this macro needs to be used because 8952,8956 have field names as differnt for 
// using the BMSK value directly from the HWIO file of respective images 
#define HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DEBUG_POLICY_BMSK 0x10000000
  return 0 != HWIO_INM(QFPROM_RAW_OEM_CONFIG_ROW0_LSB, HWIO_QFPROM_RAW_OEM_CONFIG_ROW0_LSB_DEBUG_POLICY_BMSK);
}

/**
 * @brief See documentation in the header
 *
 */
dbg_policy_t* get_dbg_policy()
{
    return debug_policy_is_valid ? &dbg_policy_local : NULL;
}

/*!
* 
* @brief
*    This function checks if debug policy ELF file is valid for enabling debug policy
*
* @retval
* @param[in/out]  TRUE   The debug policy is valid and is OK to enable it.
*                 FALSE  The debug policy contains bogus information. Disable the policy.
* 
*/
boolean is_dbg_policy_bound(dbg_policy_t *dbgp_ptr)
{
  secboot_hw_etype   status = E_SECBOOT_HW_FAILURE;
  secboot_fuse_info_type fuse_info;
  
  //TRUE = fuse bit = 1 means to bypass DP
  if (is_debug_policy_disable_fuse_bit_set())
  {
   return(FALSE);  
  }  
  
  MEMSET(&fuse_info, 0, sizeof(fuse_info));
  status = secboot_hw_get_serial_num(&fuse_info.serial_num);

  if (E_SECBOOT_HW_SUCCESS != status)
  {
   return (FALSE);
  }
  
  if (((fuse_info.serial_num) <= (dbgp_ptr->serial_num_end)) &&
      ((fuse_info.serial_num) >= (dbgp_ptr->serial_num_start)))
  {
   return (TRUE);  
  }  

  return (FALSE);
}

/*!
* 
* @brief
*    This function checks if debug policy ELF file passes basic sanity checks
*
* @retval
* @param[in/out]  TRUE   The debug policy is valid and is OK to enable it.
*                 FALSE  The debug policy contains bogus information. Disable the policy.
 *
 */
boolean is_dbg_policy_valid(dbg_policy_t *dbgp_ptr)
{
  if(debug_policy_is_valid)
  {
      return TRUE;
  }

  if (!( ('D'==dbgp_ptr->magic[0])&&
         ('B'==dbgp_ptr->magic[1])&&
         ('G'==dbgp_ptr->magic[2])&&
         ('P'==dbgp_ptr->magic[3])) )
  {
   return(FALSE);
  }

#if 0
  // Officially, the size of the policy is variable
  // In practice, it's fixed. Ignore for now.
  if (dbgp_ptr->size != sizeof(dbg_policy_t))
  {
    return(FALSE);
  }
#endif

  if (dbgp_ptr->revision != DBG_POLICY_REVISION_NUMBER)
  {
    return(FALSE);
  }

  if (DBG_POLICY_SERIAL_NUM_RANGE_LIMIT < ((uint32)dbgp_ptr->serial_num_end - (uint32)dbgp_ptr->serial_num_start))
  {
    return(FALSE);  
  }

  if (dbgp_ptr->reserved != 0)
  {
    return(FALSE);
  }

  if (dbgp_ptr->flags.reserved_bits != 0)
  {
    // must be 0
    return(FALSE);
  }

  if (dbgp_ptr->image_id_count > DBG_POLICY_ID_ARRAY_SIZE)
  {
    // out of bounds
    return(FALSE);
  }

  if (dbgp_ptr->root_cert_hash_count > DBG_POLICY_CERT_ARRAY_SIZE)
  {
    // out of bounds
    return(FALSE);
  }

  if (0 == dbgp_ptr->root_cert_hash_count && 0 != dbgp_ptr->image_id_count)
  {
      // doesn't make sense
      return(FALSE);
  }

  return is_dbg_policy_bound(dbgp_ptr);
}

/**
 * @brief See documentation in the header
 *
 */
boolean copy_debug_policy(dbg_policy_t* dp)
{
    debug_policy_is_valid = is_dbg_policy_valid(dp);	
    if(debug_policy_is_valid)
    {
      memscpy(&dbg_policy_local, sizeof(dbg_policy_local), dp, sizeof(*dp));
    }
    return debug_policy_is_valid;
}

/**
 * @brief 
 *        Check whether a feature flag is set in the debug policy.
 *
 * @param[in] flag The feature flag in question
 * @retval    TRUE if the flag is set, FALSE otherwise
 *
 */
boolean is_debug_policy_flag_set ( uint32 flag )
{
  boolean enable_flag = FALSE;

  if(!debug_policy_is_valid)
  {
      return FALSE;
  }

  switch( flag )
  {
   case DBG_POLICY_ENABLE_ONLINE_CRASH_DUMPS:   
        enable_flag = dbg_policy_local.flags.enable_online_crash_dumps;   
        break;   
   case DBG_POLICY_ENABLE_OFFLINE_CRASH_DUMPS:   
        enable_flag = dbg_policy_local.flags.enable_offline_crash_dumps;   
        break;   
   case DBG_POLICY_DISABLE_AUTHENTICATION:   
        enable_flag = dbg_policy_local.flags.disable_authentication;   
        break;   
   case DBG_POLICY_ENABLE_JTAG:   
        enable_flag = dbg_policy_local.flags.enable_jtag;   
        break;   
   case DBG_POLICY_ENABLE_LOGGING:   
        enable_flag = dbg_policy_local.flags.enable_logs;   
        break;   
   default: break;
  }
   
   return( enable_flag );
}

/**
 * @brief 
 *        Check the current image ID is in the image_id_array
 *        in the debug policy.
 *
 * @param[in] sw_type is the current image ID being authenticated.
 *
 * @retval    TRUE if the id is present id the debug policy, FALSE otherwise
 *
 */
static boolean dbg_policy_check_image_id( uint32 sw_type )
{
    int32 index;

    for ( index = 0; index < dbg_policy_local.image_id_count; index++ )
    {
     if ( dbg_policy_local.image_id_array[index] == sw_type )
	 {
	  return( TRUE );
     }	
    }
    return( FALSE );	
}

/**
 * @brief See documentation in the header
 *
 */
boolean is_dbg_policy_rot_for_image( uint32 sw_type )
{
    if (!debug_policy_is_valid)
    {
        return FALSE;
    }

    if (0 == dbg_policy_local.root_cert_hash_count ) // no certs in policy
    {
      return ( FALSE ); 
    }

    if ( 0 == dbg_policy_local.image_id_count )  // authenticate all images against policy
    {
      return( TRUE );	
    }
    
    return dbg_policy_check_image_id( sw_type );
}

void debug_policy_override_jtag()
{
  //HWIO_OVERRIDE_4_OUT(HWIO_OVERRIDE_4_RMSK);
}

/**
 * @brief See documentation in the header
 *
 */
void apply_dbg_policy()
{
   if (is_debug_policy_flag_set(DBG_POLICY_ENABLE_JTAG))
   {
     debug_policy_override_jtag();   
   }

   if (is_debug_policy_flag_set(DBG_POLICY_ENABLE_ONLINE_CRASH_DUMPS))
   {
    /* Pass this value to MPSS */
    mba_rmb_set_debug_flags(DBG_POLICY_RMB_FLAG_ONLINE_CRASH_DUMPS);
   }		 
}
