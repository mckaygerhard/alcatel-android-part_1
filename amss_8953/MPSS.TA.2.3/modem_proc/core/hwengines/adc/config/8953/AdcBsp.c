/*============================================================================
  FILE:         AdcBsp.c

  OVERVIEW:     Board support package for the ADC DAL on 8953.

  DEPENDENCIES: None
 
                Copyright (c) 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.


  when        who    what, where, why
  ----------  ---    -----------------------------------------------------------
  2015-09-08  SA     Created (from 8952).

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "DalAdc.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof(a[0]))

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
/*
 * 8953_PM8953
 */
static const AdcPhysicalDeviceType adcPhysicalDevices_8953_PM8953[] = {
   /* VADC */
   {
      /* .pszDevName */ "DALDEVICEID_VADC_8953_PM8953",
      /* .uQueueSize */ 10
   },
};

const AdcBspType AdcBsp_8953_PM8953[] = {
   {
      /* .paAdcPhysicalDevices */ adcPhysicalDevices_8953_PM8953,
      /* .uNumPhysicalDevices  */ ARRAY_LENGTH(adcPhysicalDevices_8953_PM8953)
   }
};

