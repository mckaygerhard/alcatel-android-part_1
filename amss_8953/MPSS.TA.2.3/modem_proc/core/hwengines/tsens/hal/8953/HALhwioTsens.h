#ifndef HAL_HWIO_TSENS_H
#define HAL_HWIO_TSENS_H
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
*/
/*============================================================================
  @file HALhwioTsens.h

  Implementation of the TSENS HAL for 8953 - HWIO info was auto-generated

                Copyright (c) 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/


/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "msmhwiobase.h"

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * MODULE: MPM2_TSENS
 *--------------------------------------------------------------------------*/

#define MPM2_TSENS_REG_BASE                                                    (gpuMpm2MpmBase      + 0x00008000)
#define MPM2_TSENS_REG_BASE_OFFS                                               0x00008000

#define HWIO_MPM2_TSENS_HW_VER_ADDR                                            (MPM2_TSENS_REG_BASE      + 0x00000000)
#define HWIO_MPM2_TSENS_HW_VER_OFFS                                            (MPM2_TSENS_REG_BASE_OFFS + 0x00000000)
#define HWIO_MPM2_TSENS_HW_VER_RMSK                                            0xffffffff
#define HWIO_MPM2_TSENS_HW_VER_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_HW_VER_ADDR, HWIO_MPM2_TSENS_HW_VER_RMSK)
#define HWIO_MPM2_TSENS_HW_VER_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_HW_VER_ADDR, m)
#define HWIO_MPM2_TSENS_HW_VER_MAJOR_BMSK                                      0xf0000000
#define HWIO_MPM2_TSENS_HW_VER_MAJOR_SHFT                                            0x1c
#define HWIO_MPM2_TSENS_HW_VER_MINOR_BMSK                                       0xfff0000
#define HWIO_MPM2_TSENS_HW_VER_MINOR_SHFT                                            0x10
#define HWIO_MPM2_TSENS_HW_VER_STEP_BMSK                                           0xffff
#define HWIO_MPM2_TSENS_HW_VER_STEP_SHFT                                              0x0

#define HWIO_MPM2_TSENS_CTRL_ADDR                                              (MPM2_TSENS_REG_BASE      + 0x00000004)
#define HWIO_MPM2_TSENS_CTRL_OFFS                                              (MPM2_TSENS_REG_BASE_OFFS + 0x00000004)
#define HWIO_MPM2_TSENS_CTRL_RMSK                                              0xbfffffff
#define HWIO_MPM2_TSENS_CTRL_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_CTRL_ADDR, HWIO_MPM2_TSENS_CTRL_RMSK)
#define HWIO_MPM2_TSENS_CTRL_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_CTRL_ADDR, m)
#define HWIO_MPM2_TSENS_CTRL_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_CTRL_ADDR,v)
#define HWIO_MPM2_TSENS_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_CTRL_ADDR,m,v,HWIO_MPM2_TSENS_CTRL_IN)
#define HWIO_MPM2_TSENS_CTRL_TSENS_MTC_EN_BMSK                                 0x80000000
#define HWIO_MPM2_TSENS_CTRL_TSENS_MTC_EN_SHFT                                       0x1f
#define HWIO_MPM2_TSENS_CTRL_TSENS_MTC_EN_DISABLED_FVAL                               0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_MTC_EN_ENABLED_FVAL                                0x1
#define HWIO_MPM2_TSENS_CTRL_MAX_TEMP_PWM_EN_BMSK                              0x20000000
#define HWIO_MPM2_TSENS_CTRL_MAX_TEMP_PWM_EN_SHFT                                    0x1d
#define HWIO_MPM2_TSENS_CTRL_MAX_TEMP_PWM_EN_DISABLED_FVAL                            0x0
#define HWIO_MPM2_TSENS_CTRL_MAX_TEMP_PWM_EN_ENABLED_FVAL                             0x1
#define HWIO_MPM2_TSENS_CTRL_VALID_DELAY_BMSK                                  0x1e000000
#define HWIO_MPM2_TSENS_CTRL_VALID_DELAY_SHFT                                        0x19
#define HWIO_MPM2_TSENS_CTRL_PSHOLD_ARES_EN_BMSK                                0x1000000
#define HWIO_MPM2_TSENS_CTRL_PSHOLD_ARES_EN_SHFT                                     0x18
#define HWIO_MPM2_TSENS_CTRL_PSHOLD_ARES_EN_DISABLED_FVAL                             0x0
#define HWIO_MPM2_TSENS_CTRL_PSHOLD_ARES_EN_ENABLED_FVAL                              0x1
#define HWIO_MPM2_TSENS_CTRL_TEMP_BROADCAST_EN_BMSK                              0x800000
#define HWIO_MPM2_TSENS_CTRL_TEMP_BROADCAST_EN_SHFT                                  0x17
#define HWIO_MPM2_TSENS_CTRL_TEMP_BROADCAST_EN_DISABLED_FVAL                          0x0
#define HWIO_MPM2_TSENS_CTRL_TEMP_BROADCAST_EN_ENABLED_FVAL                           0x1
#define HWIO_MPM2_TSENS_CTRL_AUTO_ADJUST_PERIOD_EN_BMSK                          0x400000
#define HWIO_MPM2_TSENS_CTRL_AUTO_ADJUST_PERIOD_EN_SHFT                              0x16
#define HWIO_MPM2_TSENS_CTRL_AUTO_ADJUST_PERIOD_EN_DISABLED_FVAL                      0x0
#define HWIO_MPM2_TSENS_CTRL_AUTO_ADJUST_PERIOD_EN_ENABLED_FVAL                       0x1
#define HWIO_MPM2_TSENS_CTRL_RESULT_FORMAT_CODE_OR_TEMP_BMSK                     0x200000
#define HWIO_MPM2_TSENS_CTRL_RESULT_FORMAT_CODE_OR_TEMP_SHFT                         0x15
#define HWIO_MPM2_TSENS_CTRL_RESULT_FORMAT_CODE_OR_TEMP_ADC_CODE_FVAL                 0x0
#define HWIO_MPM2_TSENS_CTRL_RESULT_FORMAT_CODE_OR_TEMP_REAL_TEMPERATURE_FVAL         0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_CLAMP_BMSK                                    0x100000
#define HWIO_MPM2_TSENS_CTRL_TSENS_CLAMP_SHFT                                        0x14
#define HWIO_MPM2_TSENS_CTRL_TSENS_CLAMP_UNCLAMPED_FVAL                               0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_CLAMP_CLAMPED_FVAL                                 0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_BYPASS_EN_BMSK                                 0x80000
#define HWIO_MPM2_TSENS_CTRL_TSENS_BYPASS_EN_SHFT                                    0x13
#define HWIO_MPM2_TSENS_CTRL_TSENS_BYPASS_EN_DISABLED_FVAL                            0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_BYPASS_EN_ENABLED_FVAL                             0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR15_EN_BMSK                                     0x40000
#define HWIO_MPM2_TSENS_CTRL_SENSOR15_EN_SHFT                                        0x12
#define HWIO_MPM2_TSENS_CTRL_SENSOR15_EN_DISABLED_FVAL                                0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR15_EN_ENABLED_FVAL                                 0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR14_EN_BMSK                                     0x20000
#define HWIO_MPM2_TSENS_CTRL_SENSOR14_EN_SHFT                                        0x11
#define HWIO_MPM2_TSENS_CTRL_SENSOR14_EN_DISABLED_FVAL                                0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR14_EN_ENABLED_FVAL                                 0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR13_EN_BMSK                                     0x10000
#define HWIO_MPM2_TSENS_CTRL_SENSOR13_EN_SHFT                                        0x10
#define HWIO_MPM2_TSENS_CTRL_SENSOR13_EN_DISABLED_FVAL                                0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR13_EN_ENABLED_FVAL                                 0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR12_EN_BMSK                                      0x8000
#define HWIO_MPM2_TSENS_CTRL_SENSOR12_EN_SHFT                                         0xf
#define HWIO_MPM2_TSENS_CTRL_SENSOR12_EN_DISABLED_FVAL                                0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR12_EN_ENABLED_FVAL                                 0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR11_EN_BMSK                                      0x4000
#define HWIO_MPM2_TSENS_CTRL_SENSOR11_EN_SHFT                                         0xe
#define HWIO_MPM2_TSENS_CTRL_SENSOR11_EN_DISABLED_FVAL                                0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR11_EN_ENABLED_FVAL                                 0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR10_EN_BMSK                                      0x2000
#define HWIO_MPM2_TSENS_CTRL_SENSOR10_EN_SHFT                                         0xd
#define HWIO_MPM2_TSENS_CTRL_SENSOR10_EN_DISABLED_FVAL                                0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR10_EN_ENABLED_FVAL                                 0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR9_EN_BMSK                                       0x1000
#define HWIO_MPM2_TSENS_CTRL_SENSOR9_EN_SHFT                                          0xc
#define HWIO_MPM2_TSENS_CTRL_SENSOR9_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR9_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR8_EN_BMSK                                        0x800
#define HWIO_MPM2_TSENS_CTRL_SENSOR8_EN_SHFT                                          0xb
#define HWIO_MPM2_TSENS_CTRL_SENSOR8_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR8_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR7_EN_BMSK                                        0x400
#define HWIO_MPM2_TSENS_CTRL_SENSOR7_EN_SHFT                                          0xa
#define HWIO_MPM2_TSENS_CTRL_SENSOR7_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR7_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR6_EN_BMSK                                        0x200
#define HWIO_MPM2_TSENS_CTRL_SENSOR6_EN_SHFT                                          0x9
#define HWIO_MPM2_TSENS_CTRL_SENSOR6_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR6_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR5_EN_BMSK                                        0x100
#define HWIO_MPM2_TSENS_CTRL_SENSOR5_EN_SHFT                                          0x8
#define HWIO_MPM2_TSENS_CTRL_SENSOR5_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR5_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR4_EN_BMSK                                         0x80
#define HWIO_MPM2_TSENS_CTRL_SENSOR4_EN_SHFT                                          0x7
#define HWIO_MPM2_TSENS_CTRL_SENSOR4_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR4_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR3_EN_BMSK                                         0x40
#define HWIO_MPM2_TSENS_CTRL_SENSOR3_EN_SHFT                                          0x6
#define HWIO_MPM2_TSENS_CTRL_SENSOR3_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR3_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR2_EN_BMSK                                         0x20
#define HWIO_MPM2_TSENS_CTRL_SENSOR2_EN_SHFT                                          0x5
#define HWIO_MPM2_TSENS_CTRL_SENSOR2_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR2_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR1_EN_BMSK                                         0x10
#define HWIO_MPM2_TSENS_CTRL_SENSOR1_EN_SHFT                                          0x4
#define HWIO_MPM2_TSENS_CTRL_SENSOR1_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR1_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR0_EN_BMSK                                          0x8
#define HWIO_MPM2_TSENS_CTRL_SENSOR0_EN_SHFT                                          0x3
#define HWIO_MPM2_TSENS_CTRL_SENSOR0_EN_DISABLED_FVAL                                 0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR0_EN_ENABLED_FVAL                                  0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_ADC_CLK_SEL_BMSK                                   0x4
#define HWIO_MPM2_TSENS_CTRL_TSENS_ADC_CLK_SEL_SHFT                                   0x2
#define HWIO_MPM2_TSENS_CTRL_TSENS_ADC_CLK_SEL_INTERNAL_OSCILLATOR_FVAL               0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_ADC_CLK_SEL_EXTERNAL_CLOCK_SOURCE_FVAL             0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_SW_RST_BMSK                                        0x2
#define HWIO_MPM2_TSENS_CTRL_TSENS_SW_RST_SHFT                                        0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_SW_RST_RESET_DEASSERTED_FVAL                       0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_SW_RST_RESET_ASSERTED_FVAL                         0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_EN_BMSK                                            0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_EN_SHFT                                            0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_EN_DISABLED_FVAL                                   0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_EN_ENABLED_FVAL                                    0x1

#define HWIO_MPM2_TSENS_MEASURE_PERIOD_ADDR                                    (MPM2_TSENS_REG_BASE      + 0x00000008)
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_OFFS                                    (MPM2_TSENS_REG_BASE_OFFS + 0x00000008)
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_RMSK                                        0xffff
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_MEASURE_PERIOD_ADDR, HWIO_MPM2_TSENS_MEASURE_PERIOD_RMSK)
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_MEASURE_PERIOD_ADDR, m)
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_MEASURE_PERIOD_ADDR,v)
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_MEASURE_PERIOD_ADDR,m,v,HWIO_MPM2_TSENS_MEASURE_PERIOD_IN)
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_POWERDOWN_MEASURE_PERIOD_BMSK               0xff00
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_POWERDOWN_MEASURE_PERIOD_SHFT                  0x8
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_MAIN_MEASURE_PERIOD_BMSK                      0xff
#define HWIO_MPM2_TSENS_MEASURE_PERIOD_MAIN_MEASURE_PERIOD_SHFT                       0x0

#define HWIO_MPM2_TSENS_TEST_CTRL_ADDR                                         (MPM2_TSENS_REG_BASE      + 0x0000000c)
#define HWIO_MPM2_TSENS_TEST_CTRL_OFFS                                         (MPM2_TSENS_REG_BASE_OFFS + 0x0000000c)
#define HWIO_MPM2_TSENS_TEST_CTRL_RMSK                                              0x3ff
#define HWIO_MPM2_TSENS_TEST_CTRL_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_TEST_CTRL_ADDR, HWIO_MPM2_TSENS_TEST_CTRL_RMSK)
#define HWIO_MPM2_TSENS_TEST_CTRL_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_TEST_CTRL_ADDR, m)
#define HWIO_MPM2_TSENS_TEST_CTRL_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_TEST_CTRL_ADDR,v)
#define HWIO_MPM2_TSENS_TEST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_TEST_CTRL_ADDR,m,v,HWIO_MPM2_TSENS_TEST_CTRL_IN)
#define HWIO_MPM2_TSENS_TEST_CTRL_CM_DFT_ENABLE_BMSK                                0x200
#define HWIO_MPM2_TSENS_TEST_CTRL_CM_DFT_ENABLE_SHFT                                  0x9
#define HWIO_MPM2_TSENS_TEST_CTRL_BYPASS_DIST_SEL_BMSK                              0x1e0
#define HWIO_MPM2_TSENS_TEST_CTRL_BYPASS_DIST_SEL_SHFT                                0x5
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_BMSK                                0x1e
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SHFT                                 0x1
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_EN_BMSK                                  0x1
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_EN_SHFT                                  0x0
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_EN_TEST_DISABLED_FVAL                    0x0
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_EN_TEST_ENABLED_FVAL                     0x1

#define HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_ADDR                                (MPM2_TSENS_REG_BASE      + 0x00000010)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_OFFS                                (MPM2_TSENS_REG_BASE_OFFS + 0x00000010)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_RMSK                                0xffffffff
#define HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_ADDR, HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_RMSK)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_ADDR, m)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_MAX_INT_STATUS_BMSK                 0xffff0000
#define HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_MAX_INT_STATUS_SHFT                       0x10
#define HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_MIN_INT_STATUS_BMSK                     0xffff
#define HWIO_MPM2_TSENS_MAX_MIN_INT_STATUS_MIN_INT_STATUS_SHFT                        0x0

#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_ADDR                                 (MPM2_TSENS_REG_BASE      + 0x00000014)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_OFFS                                 (MPM2_TSENS_REG_BASE_OFFS + 0x00000014)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_RMSK                                 0xffffffff
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_ADDR, HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_RMSK)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_ADDR, m)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_ADDR,v)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_ADDR,m,v,HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_IN)
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_MAX_INT_CLEAR_BMSK                   0xffff0000
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_MAX_INT_CLEAR_SHFT                         0x10
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_MIN_INT_CLEAR_BMSK                       0xffff
#define HWIO_MPM2_TSENS_MAX_MIN_INT_CLEAR_MIN_INT_CLEAR_SHFT                          0x0

#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(n)                         (MPM2_TSENS_REG_BASE      + 0x00000020 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_OFFS(n)                         (MPM2_TSENS_REG_BASE_OFFS + 0x00000020 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_RMSK                             0x3ffffff
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAXn                                    15
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(n), HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_RMSK)
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(n), mask)
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(n),val)
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_ADDR(n),mask,val,HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_INI(n))
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_STATUS_MASK_BMSK             0x2000000
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_STATUS_MASK_SHFT                  0x19
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_STATUS_MASK_NORMAL_OPERATION_FVAL        0x0
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_STATUS_MASK_MASK_OFF_MAX_STATUS_FVAL        0x1
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_STATUS_MASK_BMSK             0x1000000
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_STATUS_MASK_SHFT                  0x18
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_STATUS_MASK_NORMAL_OPERATION_FVAL        0x0
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_STATUS_MASK_MASK_OFF_MIN_STATUS_FVAL        0x1
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_THRESHOLD_BMSK                0xfff000
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MAX_THRESHOLD_SHFT                     0xc
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_THRESHOLD_BMSK                   0xfff
#define HWIO_MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL_MIN_THRESHOLD_SHFT                     0x0

#define HWIO_MPM2_TSENS_Sn_CONVERSION_ADDR(n)                                  (MPM2_TSENS_REG_BASE      + 0x00000060 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_CONVERSION_OFFS(n)                                  (MPM2_TSENS_REG_BASE_OFFS + 0x00000060 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_CONVERSION_RMSK                                      0x1ffffff
#define HWIO_MPM2_TSENS_Sn_CONVERSION_MAXn                                             15
#define HWIO_MPM2_TSENS_Sn_CONVERSION_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_CONVERSION_ADDR(n), HWIO_MPM2_TSENS_Sn_CONVERSION_RMSK)
#define HWIO_MPM2_TSENS_Sn_CONVERSION_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_CONVERSION_ADDR(n), mask)
#define HWIO_MPM2_TSENS_Sn_CONVERSION_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_Sn_CONVERSION_ADDR(n),val)
#define HWIO_MPM2_TSENS_Sn_CONVERSION_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_Sn_CONVERSION_ADDR(n),mask,val,HWIO_MPM2_TSENS_Sn_CONVERSION_INI(n))
#define HWIO_MPM2_TSENS_Sn_CONVERSION_SHIFT_BMSK                                0x1800000
#define HWIO_MPM2_TSENS_Sn_CONVERSION_SHIFT_SHFT                                     0x17
#define HWIO_MPM2_TSENS_Sn_CONVERSION_SLOPE_BMSK                                 0x7ffc00
#define HWIO_MPM2_TSENS_Sn_CONVERSION_SLOPE_SHFT                                      0xa
#define HWIO_MPM2_TSENS_Sn_CONVERSION_CZERO_BMSK                                    0x3ff
#define HWIO_MPM2_TSENS_Sn_CONVERSION_CZERO_SHFT                                      0x0

#define HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_ADDR(n)                                      (MPM2_TSENS_REG_BASE + 0x000000a0 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_RMSK                                                     0xf
#define HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_MAXn                                                      15
#define HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_ADDR(n), HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_RMSK)
#define HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_ADDR(n), mask)
#define HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_ADDR(n),val)
#define HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_ADDR(n),mask,val,HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_INI(n))
#define HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_SENSOR_ID_BMSK                                           0xf
#define HWIO_MPM2_TSENS_Sn_ID_ASSIGNMENT_SENSOR_ID_SHFT                                           0x0

#define HWIO_MPM2_TS_CONTROL_ADDR                                              (MPM2_TSENS_REG_BASE      + 0x000000e0)
#define HWIO_MPM2_TS_CONTROL_OFFS                                              (MPM2_TSENS_REG_BASE_OFFS + 0x000000e0)
#define HWIO_MPM2_TS_CONTROL_RMSK                                               0x1ffffff
#define HWIO_MPM2_TS_CONTROL_IN          \
        in_dword_masked(HWIO_MPM2_TS_CONTROL_ADDR, HWIO_MPM2_TS_CONTROL_RMSK)
#define HWIO_MPM2_TS_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MPM2_TS_CONTROL_ADDR, m)
#define HWIO_MPM2_TS_CONTROL_OUT(v)      \
        out_dword(HWIO_MPM2_TS_CONTROL_ADDR,v)
#define HWIO_MPM2_TS_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TS_CONTROL_ADDR,m,v,HWIO_MPM2_TS_CONTROL_IN)
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_ISENSE_OUT_EN_BMSK             0x1000000
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_ISENSE_OUT_EN_SHFT                  0x18
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_ISENSE_OUT_EN_DISABLED_FVAL          0x0
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_ISENSE_OUT_EN_ENABLED_FVAL           0x1
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_BG_CORE_EN_BMSK                 0x800000
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_BG_CORE_EN_SHFT                     0x17
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_BG_CORE_EN_DISABLED_FVAL             0x0
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_BG_CORE_EN_ENABLED_FVAL              0x1
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_FREQ_BMSK                       0x400000
#define HWIO_MPM2_TS_CONTROL_REF_CURR_MIRROR_DEM_FREQ_SHFT                           0x16
#define HWIO_MPM2_TS_CONTROL_SLOPE_CALIBRATION_REF_EN_BMSK                       0x200000
#define HWIO_MPM2_TS_CONTROL_SLOPE_CALIBRATION_REF_EN_SHFT                           0x15
#define HWIO_MPM2_TS_CONTROL_SLOPE_CALIBRATION_REF_EN_DISABLED_FVAL                   0x0
#define HWIO_MPM2_TS_CONTROL_SLOPE_CALIBRATION_REF_EN_ENABLED_FVAL                    0x1
#define HWIO_MPM2_TS_CONTROL_REF_BJT_DEM_BMSK                                    0x180000
#define HWIO_MPM2_TS_CONTROL_REF_BJT_DEM_SHFT                                        0x13
#define HWIO_MPM2_TS_CONTROL_SEND_QUANTIZER_OUTPUT_BMSK                           0x40000
#define HWIO_MPM2_TS_CONTROL_SEND_QUANTIZER_OUTPUT_SHFT                              0x12
#define HWIO_MPM2_TS_CONTROL_SEND_QUANTIZER_OUTPUT_DISABLED_FVAL                      0x0
#define HWIO_MPM2_TS_CONTROL_SEND_QUANTIZER_OUTPUT_ENABLED_FVAL                       0x1
#define HWIO_MPM2_TS_CONTROL_REF_OPAMP_CHOPPING_BMSK                              0x30000
#define HWIO_MPM2_TS_CONTROL_REF_OPAMP_CHOPPING_SHFT                                 0x10
#define HWIO_MPM2_TS_CONTROL_NOT_USED_0_BMSK                                       0x8000
#define HWIO_MPM2_TS_CONTROL_NOT_USED_0_SHFT                                          0xf
#define HWIO_MPM2_TS_CONTROL_VBE_R_SENSE_CURRENT_DEM_BMSK                          0x6000
#define HWIO_MPM2_TS_CONTROL_VBE_R_SENSE_CURRENT_DEM_SHFT                             0xd
#define HWIO_MPM2_TS_CONTROL_BANDGAP_CORE_VREF_RES_TRIM_BMSK                       0x1c00
#define HWIO_MPM2_TS_CONTROL_BANDGAP_CORE_VREF_RES_TRIM_SHFT                          0xa
#define HWIO_MPM2_TS_CONTROL_SLOPE_CALIBRATION_REF_SEL_BMSK                         0x200
#define HWIO_MPM2_TS_CONTROL_SLOPE_CALIBRATION_REF_SEL_SHFT                           0x9
#define HWIO_MPM2_TS_CONTROL_SLOPE_CALIBRATION_REF_SEL_VR1_FVAL                       0x0
#define HWIO_MPM2_TS_CONTROL_SLOPE_CALIBRATION_REF_SEL_VR2_FVAL                       0x1
#define HWIO_MPM2_TS_CONTROL_VBE_R_SENSE_OPAMP_CHOPPING_BMSK                        0x180
#define HWIO_MPM2_TS_CONTROL_VBE_R_SENSE_OPAMP_CHOPPING_SHFT                          0x7
#define HWIO_MPM2_TS_CONTROL_RO_CLK_TO_PIN_BMSK                                      0x40
#define HWIO_MPM2_TS_CONTROL_RO_CLK_TO_PIN_SHFT                                       0x6
#define HWIO_MPM2_TS_CONTROL_RO_CLK_TO_PIN_DISABLED_FVAL                              0x0
#define HWIO_MPM2_TS_CONTROL_RO_CLK_TO_PIN_ENABLED_FVAL                               0x1
#define HWIO_MPM2_TS_CONTROL_BANDGAP_CORE_CTAT_RES_TRIM_BMSK                         0x3c
#define HWIO_MPM2_TS_CONTROL_BANDGAP_CORE_CTAT_RES_TRIM_SHFT                          0x2
#define HWIO_MPM2_TS_CONTROL_SENSE_BJT_DEM_BMSK                                       0x3
#define HWIO_MPM2_TS_CONTROL_SENSE_BJT_DEM_SHFT                                       0x0

#define HWIO_MPM2_TS_CONFIG_ADDR                                               (MPM2_TSENS_REG_BASE      + 0x000000e4)
#define HWIO_MPM2_TS_CONFIG_OFFS                                               (MPM2_TSENS_REG_BASE_OFFS + 0x000000e4)
#define HWIO_MPM2_TS_CONFIG_RMSK                                                     0xff
#define HWIO_MPM2_TS_CONFIG_IN          \
        in_dword_masked(HWIO_MPM2_TS_CONFIG_ADDR, HWIO_MPM2_TS_CONFIG_RMSK)
#define HWIO_MPM2_TS_CONFIG_INM(m)      \
        in_dword_masked(HWIO_MPM2_TS_CONFIG_ADDR, m)
#define HWIO_MPM2_TS_CONFIG_OUT(v)      \
        out_dword(HWIO_MPM2_TS_CONFIG_ADDR,v)
#define HWIO_MPM2_TS_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TS_CONFIG_ADDR,m,v,HWIO_MPM2_TS_CONFIG_IN)
#define HWIO_MPM2_TS_CONFIG_CLOCK_FREQ_BMSK                                          0xc0
#define HWIO_MPM2_TS_CONFIG_CLOCK_FREQ_SHFT                                           0x6
#define HWIO_MPM2_TS_CONFIG_NOT_USED_11_BMSK                                         0x30
#define HWIO_MPM2_TS_CONFIG_NOT_USED_11_SHFT                                          0x4
#define HWIO_MPM2_TS_CONFIG_ISENSE_MODE_FOR_BASE_RES_CAL_BMSK                         0x8
#define HWIO_MPM2_TS_CONFIG_ISENSE_MODE_FOR_BASE_RES_CAL_SHFT                         0x3
#define HWIO_MPM2_TS_CONFIG_VBE_R_SENSE_RES_TRIM_BMSK                                 0x7
#define HWIO_MPM2_TS_CONFIG_VBE_R_SENSE_RES_TRIM_SHFT                                 0x0

#define HWIO_MPM2_TSENS_SIDEBAND_EN_ADDR                                       (MPM2_TSENS_REG_BASE      + 0x000000e8)
#define HWIO_MPM2_TSENS_SIDEBAND_EN_OFFS                                       (MPM2_TSENS_REG_BASE_OFFS + 0x000000e8)
#define HWIO_MPM2_TSENS_SIDEBAND_EN_RMSK                                           0xffff
#define HWIO_MPM2_TSENS_SIDEBAND_EN_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_SIDEBAND_EN_ADDR, HWIO_MPM2_TSENS_SIDEBAND_EN_RMSK)
#define HWIO_MPM2_TSENS_SIDEBAND_EN_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_SIDEBAND_EN_ADDR, m)
#define HWIO_MPM2_TSENS_SIDEBAND_EN_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_SIDEBAND_EN_ADDR,v)
#define HWIO_MPM2_TSENS_SIDEBAND_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_SIDEBAND_EN_ADDR,m,v,HWIO_MPM2_TSENS_SIDEBAND_EN_IN)
#define HWIO_MPM2_TSENS_SIDEBAND_EN_SENSOR_EN_BMSK                                 0xffff
#define HWIO_MPM2_TSENS_SIDEBAND_EN_SENSOR_EN_SHFT                                    0x0

#define HWIO_MPM2_TSENS_TBCB_CONTROL_ADDR                                      (MPM2_TSENS_REG_BASE      + 0x000000ec)
#define HWIO_MPM2_TSENS_TBCB_CONTROL_OFFS                                      (MPM2_TSENS_REG_BASE_OFFS + 0x000000ec)
#define HWIO_MPM2_TSENS_TBCB_CONTROL_RMSK                                      0xffffffff
#define HWIO_MPM2_TSENS_TBCB_CONTROL_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_TBCB_CONTROL_ADDR, HWIO_MPM2_TSENS_TBCB_CONTROL_RMSK)
#define HWIO_MPM2_TSENS_TBCB_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_TBCB_CONTROL_ADDR, m)
#define HWIO_MPM2_TSENS_TBCB_CONTROL_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_TBCB_CONTROL_ADDR,v)
#define HWIO_MPM2_TSENS_TBCB_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_TBCB_CONTROL_ADDR,m,v,HWIO_MPM2_TSENS_TBCB_CONTROL_IN)
#define HWIO_MPM2_TSENS_TBCB_CONTROL_TBCB_ACK_DELAY_BMSK                       0xff000000
#define HWIO_MPM2_TSENS_TBCB_CONTROL_TBCB_ACK_DELAY_SHFT                             0x18
#define HWIO_MPM2_TSENS_TBCB_CONTROL_TBCB_CLK_DIV_BMSK                           0xff0000
#define HWIO_MPM2_TSENS_TBCB_CONTROL_TBCB_CLK_DIV_SHFT                               0x10
#define HWIO_MPM2_TSENS_TBCB_CONTROL_TBCB_CLIENT_EN_BMSK                           0xffff
#define HWIO_MPM2_TSENS_TBCB_CONTROL_TBCB_CLIENT_EN_SHFT                              0x0
#define HWIO_MPM2_TSENS_TBCB_CONTROL_TBCB_CLIENT_EN_DISABLED_FVAL                     0x0
#define HWIO_MPM2_TSENS_TBCB_CONTROL_TBCB_CLIENT_EN_ENABLED_FVAL                      0x1

#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_ADDR(n)                              (MPM2_TSENS_REG_BASE      + 0x000000f0 + 0x4 * (n))
#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_OFFS(n)                              (MPM2_TSENS_REG_BASE_OFFS + 0x000000f0 + 0x4 * (n))
#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_RMSK                                     0xffff
#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_MAXn                                         15
#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_ADDR(n), HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_RMSK)
#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_ADDR(n), mask)
#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_ADDR(n),val)
#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_ADDR(n),mask,val,HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_INI(n))
#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_TBCB_CLIENT_REQ_SENSOR_BMSK              0xffff
#define HWIO_MPM2_TSENS_TBCB_CLIENT_n_REQ_TBCB_CLIENT_REQ_SENSOR_SHFT                 0x0

#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_ADDR                                     (MPM2_TSENS_REG_BASE      + 0x00000130)
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_OFFS                                     (MPM2_TSENS_REG_BASE_OFFS + 0x00000130)
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_RMSK                                        0x3ffff
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_TBCB_OVERRIDE_ADDR, HWIO_MPM2_TSENS_TBCB_OVERRIDE_RMSK)
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_TBCB_OVERRIDE_ADDR, m)
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_TBCB_OVERRIDE_ADDR,v)
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_TBCB_OVERRIDE_ADDR,m,v,HWIO_MPM2_TSENS_TBCB_OVERRIDE_IN)
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_TBCB_OVERRIDE_DATA_BMSK                     0x3fffc
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_TBCB_OVERRIDE_DATA_SHFT                         0x2
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_TBCB_OVERRIDE_CMD_BMSK                          0x2
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_TBCB_OVERRIDE_CMD_SHFT                          0x1
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_TBCB_OVERRIDE_EN_BMSK                           0x1
#define HWIO_MPM2_TSENS_TBCB_OVERRIDE_TBCB_OVERRIDE_EN_SHFT                           0x0

#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_ADDR(n)                                 (MPM2_TSENS_REG_BASE      + 0x00000140 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_OFFS(n)                                 (MPM2_TSENS_REG_BASE_OFFS + 0x00000140 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_RMSK                                    0xffffffff
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_MAXn                                             3
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_ADDR(n), HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_RMSK)
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_ADDR(n), mask)
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_ADDR(n),val)
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_ADDR(n),mask,val,HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_INI(n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_ZONE_MTC_EN_BMSK                        0x80000000
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_ZONE_MTC_EN_SHFT                              0x1f
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_PS_COMMAND_TH2_VIOLATED_BMSK            0x7c000000
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_PS_COMMAND_TH2_VIOLATED_SHFT                  0x1a
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_PS_COMMAND_TH1_VIOLATED_BMSK             0x3e00000
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_PS_COMMAND_TH1_VIOLATED_SHFT                  0x15
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_PS_COMMAND_COOL_BMSK                      0x1f0000
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_PS_COMMAND_COOL_SHFT                          0x10
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_SENSOR_MAP_BMSK                             0xffff
#define HWIO_MPM2_TSENS_MTC_ZONEn_CTRL_SENSOR_MAP_SHFT                                0x0

#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_ADDR(n)                              (MPM2_TSENS_REG_BASE      + 0x00000150 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_OFFS(n)                              (MPM2_TSENS_REG_BASE_OFFS + 0x00000150 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_RMSK                                   0xffffff
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_MAXn                                         15
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_ADDR(n), HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_RMSK)
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_ADDR(n), mask)
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_ADDR(n),val)
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_ADDR(n),mask,val,HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_INI(n))
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_TH2_BMSK                               0xfff000
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_TH2_SHFT                                    0xc
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_TH1_BMSK                                  0xfff
#define HWIO_MPM2_TSENS_MTC_Sn_THRESHOLDS_TH1_SHFT                                    0x0

#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_ADDR(n)                              (MPM2_TSENS_REG_BASE      + 0x00000190 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_OFFS(n)                              (MPM2_TSENS_REG_BASE_OFFS + 0x00000190 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_RMSK                                      0xfff
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_MAXn                                         15
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_ADDR(n), HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_RMSK)
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_ADDR(n), mask)
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_ADDR(n),val)
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_ADDR(n),mask,val,HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_INI(n))
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_TH2_MARGIN_BMSK                           0xfc0
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_TH2_MARGIN_SHFT                             0x6
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_TH1_MARGIN_BMSK                            0x3f
#define HWIO_MPM2_TSENS_MTC_Sn_TH_MARGINS_TH1_MARGIN_SHFT                             0x0

/*----------------------------------------------------------------------------
 * MODULE: MPM2_TSENS_TM
 *--------------------------------------------------------------------------*/

#define MPM2_TSENS_TM_REG_BASE                                                   (gpuMpm2MpmBase      + 0x00009000)
#define MPM2_TSENS_TM_REG_BASE_OFFS                                              0x00009000

#define HWIO_MPM2_TSENS_CONTROLLER_ID_ADDR                                       (MPM2_TSENS_TM_REG_BASE      + 0x00000000)
#define HWIO_MPM2_TSENS_CONTROLLER_ID_OFFS                                       (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000000)
#define HWIO_MPM2_TSENS_CONTROLLER_ID_RMSK                                              0xf
#define HWIO_MPM2_TSENS_CONTROLLER_ID_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_CONTROLLER_ID_ADDR, HWIO_MPM2_TSENS_CONTROLLER_ID_RMSK)
#define HWIO_MPM2_TSENS_CONTROLLER_ID_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_CONTROLLER_ID_ADDR, m)
#define HWIO_MPM2_TSENS_CONTROLLER_ID_CONTROLLER_ID_BMSK                                0xf
#define HWIO_MPM2_TSENS_CONTROLLER_ID_CONTROLLER_ID_SHFT                                0x0

#define HWIO_MPM2_TSENS_TM_INT_EN_ADDR                                           (MPM2_TSENS_TM_REG_BASE      + 0x00000004)
#define HWIO_MPM2_TSENS_TM_INT_EN_OFFS                                           (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000004)
#define HWIO_MPM2_TSENS_TM_INT_EN_RMSK                                                  0x7
#define HWIO_MPM2_TSENS_TM_INT_EN_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_TM_INT_EN_ADDR, HWIO_MPM2_TSENS_TM_INT_EN_RMSK)
#define HWIO_MPM2_TSENS_TM_INT_EN_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_TM_INT_EN_ADDR, m)
#define HWIO_MPM2_TSENS_TM_INT_EN_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_TM_INT_EN_ADDR,v)
#define HWIO_MPM2_TSENS_TM_INT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_TM_INT_EN_ADDR,m,v,HWIO_MPM2_TSENS_TM_INT_EN_IN)
#define HWIO_MPM2_TSENS_TM_INT_EN_CRITICAL_INT_EN_BMSK                                  0x4
#define HWIO_MPM2_TSENS_TM_INT_EN_CRITICAL_INT_EN_SHFT                                  0x2
#define HWIO_MPM2_TSENS_TM_INT_EN_CRITICAL_INT_EN_DISABLED_FVAL                         0x0
#define HWIO_MPM2_TSENS_TM_INT_EN_CRITICAL_INT_EN_ENABLED_FVAL                          0x1
#define HWIO_MPM2_TSENS_TM_INT_EN_UPPER_INT_EN_BMSK                                     0x2
#define HWIO_MPM2_TSENS_TM_INT_EN_UPPER_INT_EN_SHFT                                     0x1
#define HWIO_MPM2_TSENS_TM_INT_EN_UPPER_INT_EN_DISABLED_FVAL                            0x0
#define HWIO_MPM2_TSENS_TM_INT_EN_UPPER_INT_EN_ENABLED_FVAL                             0x1
#define HWIO_MPM2_TSENS_TM_INT_EN_LOWER_INT_EN_BMSK                                     0x1
#define HWIO_MPM2_TSENS_TM_INT_EN_LOWER_INT_EN_SHFT                                     0x0
#define HWIO_MPM2_TSENS_TM_INT_EN_LOWER_INT_EN_DISABLED_FVAL                            0x0
#define HWIO_MPM2_TSENS_TM_INT_EN_LOWER_INT_EN_ENABLED_FVAL                             0x1

#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_ADDR                              (MPM2_TSENS_TM_REG_BASE      + 0x00000008)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_OFFS                              (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000008)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_RMSK                              0xffffffff
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_ADDR, HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_RMSK)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_ADDR, m)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_UPPER_INT_STATUS_BMSK             0xffff0000
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_UPPER_INT_STATUS_SHFT                   0x10
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_LOWER_INT_STATUS_BMSK                 0xffff
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_STATUS_LOWER_INT_STATUS_SHFT                    0x0

#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_CLEAR_ADDR                               (MPM2_TSENS_TM_REG_BASE      + 0x0000000c)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_CLEAR_OFFS                               (MPM2_TSENS_TM_REG_BASE_OFFS + 0x0000000c)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_CLEAR_RMSK                               0xffffffff
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_UPPER_LOWER_INT_CLEAR_ADDR,v)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_CLEAR_UPPER_INT_CLEAR_BMSK               0xffff0000
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_CLEAR_UPPER_INT_CLEAR_SHFT                     0x10
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_CLEAR_LOWER_INT_CLEAR_BMSK                   0xffff
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_CLEAR_LOWER_INT_CLEAR_SHFT                      0x0

#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_ADDR                                (MPM2_TSENS_TM_REG_BASE      + 0x00000010)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_OFFS                                (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000010)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_RMSK                                0xffffffff
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_ADDR, HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_RMSK)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_ADDR, m)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_ADDR,v)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_ADDR,m,v,HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_IN)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_UPPER_INT_MASK_BMSK                 0xffff0000
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_UPPER_INT_MASK_SHFT                       0x10
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_LOWER_INT_MASK_BMSK                     0xffff
#define HWIO_MPM2_TSENS_UPPER_LOWER_INT_MASK_LOWER_INT_MASK_SHFT                        0x0

#define HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_ADDR                                 (MPM2_TSENS_TM_REG_BASE      + 0x00000014)
#define HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_OFFS                                 (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000014)
#define HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_RMSK                                     0xffff
#define HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_ADDR, HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_RMSK)
#define HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_ADDR, m)
#define HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_CRITICAL_INT_STATUS_BMSK                 0xffff
#define HWIO_MPM2_TSENS_CRITICAL_INT_STATUS_CRITICAL_INT_STATUS_SHFT                    0x0

#define HWIO_MPM2_TSENS_CRITICAL_INT_CLEAR_ADDR                                  (MPM2_TSENS_TM_REG_BASE      + 0x00000018)
#define HWIO_MPM2_TSENS_CRITICAL_INT_CLEAR_OFFS                                  (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000018)
#define HWIO_MPM2_TSENS_CRITICAL_INT_CLEAR_RMSK                                      0xffff
#define HWIO_MPM2_TSENS_CRITICAL_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_CRITICAL_INT_CLEAR_ADDR,v)
#define HWIO_MPM2_TSENS_CRITICAL_INT_CLEAR_CRITICAL_INT_CLEAR_BMSK                   0xffff
#define HWIO_MPM2_TSENS_CRITICAL_INT_CLEAR_CRITICAL_INT_CLEAR_SHFT                      0x0

#define HWIO_MPM2_TSENS_CRITICAL_INT_MASK_ADDR                                   (MPM2_TSENS_TM_REG_BASE      + 0x0000001c)
#define HWIO_MPM2_TSENS_CRITICAL_INT_MASK_OFFS                                   (MPM2_TSENS_TM_REG_BASE_OFFS + 0x0000001c)
#define HWIO_MPM2_TSENS_CRITICAL_INT_MASK_RMSK                                       0xffff
#define HWIO_MPM2_TSENS_CRITICAL_INT_MASK_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_CRITICAL_INT_MASK_ADDR, HWIO_MPM2_TSENS_CRITICAL_INT_MASK_RMSK)
#define HWIO_MPM2_TSENS_CRITICAL_INT_MASK_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_CRITICAL_INT_MASK_ADDR, m)
#define HWIO_MPM2_TSENS_CRITICAL_INT_MASK_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_CRITICAL_INT_MASK_ADDR,v)
#define HWIO_MPM2_TSENS_CRITICAL_INT_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_CRITICAL_INT_MASK_ADDR,m,v,HWIO_MPM2_TSENS_CRITICAL_INT_MASK_IN)
#define HWIO_MPM2_TSENS_CRITICAL_INT_MASK_CRITICAL_INT_MASK_BMSK                     0xffff
#define HWIO_MPM2_TSENS_CRITICAL_INT_MASK_CRITICAL_INT_MASK_SHFT                        0x0

#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(n)                         (MPM2_TSENS_TM_REG_BASE      + 0x00000020 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_OFFS(n)                         (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000020 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_RMSK                              0xffffff
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_MAXn                                    15
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(n), HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_RMSK)
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(n), mask)
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(n),val)
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_ADDR(n),mask,val,HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_INI(n))
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_UPPER_THRESHOLD_BMSK              0xfff000
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_UPPER_THRESHOLD_SHFT                   0xc
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_LOWER_THRESHOLD_BMSK                 0xfff
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD_LOWER_THRESHOLD_SHFT                   0x0

#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(n)                            (MPM2_TSENS_TM_REG_BASE      + 0x00000060 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_OFFS(n)                            (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000060 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_RMSK                                    0xfff
#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_MAXn                                       15
#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(n), HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_RMSK)
#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(n), mask)
#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(n),val)
#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_ADDR(n),mask,val,HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_INI(n))
#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_CRITICAL_THRESHOLD_BMSK                 0xfff
#define HWIO_MPM2_TSENS_Sn_CRITICAL_THRESHOLD_CRITICAL_THRESHOLD_SHFT                   0x0

#define HWIO_MPM2_TSENS_Sn_STATUS_ADDR(n)                                        (MPM2_TSENS_TM_REG_BASE      + 0x000000a0 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_STATUS_OFFS(n)                                        (MPM2_TSENS_TM_REG_BASE_OFFS + 0x000000a0 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_STATUS_RMSK                                             0x3fffff
#define HWIO_MPM2_TSENS_Sn_STATUS_MAXn                                                   15
#define HWIO_MPM2_TSENS_Sn_STATUS_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_STATUS_ADDR(n), HWIO_MPM2_TSENS_Sn_STATUS_RMSK)
#define HWIO_MPM2_TSENS_Sn_STATUS_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_STATUS_ADDR(n), mask)
#define HWIO_MPM2_TSENS_Sn_STATUS_VALID_BMSK                                       0x200000
#define HWIO_MPM2_TSENS_Sn_STATUS_VALID_SHFT                                           0x15
#define HWIO_MPM2_TSENS_Sn_STATUS_MAX_STATUS_BMSK                                  0x100000
#define HWIO_MPM2_TSENS_Sn_STATUS_MAX_STATUS_SHFT                                      0x14
#define HWIO_MPM2_TSENS_Sn_STATUS_MAX_STATUS_MAX_THRESHOLD_NOT_VIOLATED_FVAL            0x0
#define HWIO_MPM2_TSENS_Sn_STATUS_MAX_STATUS_MAX_THRESHOLD_VIOLATED_FVAL                0x1
#define HWIO_MPM2_TSENS_Sn_STATUS_CRITICAL_STATUS_BMSK                              0x80000
#define HWIO_MPM2_TSENS_Sn_STATUS_CRITICAL_STATUS_SHFT                                 0x13
#define HWIO_MPM2_TSENS_Sn_STATUS_CRITICAL_STATUS_CRITICAL_THRESHOLD_NOT_VIOLATED_FVAL        0x0
#define HWIO_MPM2_TSENS_Sn_STATUS_CRITICAL_STATUS_CRITICAL_THRESHOLD_VIOLATED_FVAL        0x1
#define HWIO_MPM2_TSENS_Sn_STATUS_UPPER_STATUS_BMSK                                 0x40000
#define HWIO_MPM2_TSENS_Sn_STATUS_UPPER_STATUS_SHFT                                    0x12
#define HWIO_MPM2_TSENS_Sn_STATUS_UPPER_STATUS_UPPER_THRESHOLD_NOT_VIOLATED_FVAL        0x0
#define HWIO_MPM2_TSENS_Sn_STATUS_UPPER_STATUS_UPPER_THRESHOLD_VIOLATED_FVAL            0x1
#define HWIO_MPM2_TSENS_Sn_STATUS_LOWER_STATUS_BMSK                                 0x20000
#define HWIO_MPM2_TSENS_Sn_STATUS_LOWER_STATUS_SHFT                                    0x11
#define HWIO_MPM2_TSENS_Sn_STATUS_LOWER_STATUS_LOWER_THRESHOLD_NOT_VIOLATED_FVAL        0x0
#define HWIO_MPM2_TSENS_Sn_STATUS_LOWER_STATUS_LOWER_THRESHOLD_VIOLATED_FVAL            0x1
#define HWIO_MPM2_TSENS_Sn_STATUS_MIN_STATUS_BMSK                                   0x10000
#define HWIO_MPM2_TSENS_Sn_STATUS_MIN_STATUS_SHFT                                      0x10
#define HWIO_MPM2_TSENS_Sn_STATUS_MIN_STATUS_MIN_THRESHOLD_NOT_VIOLATED_FVAL            0x0
#define HWIO_MPM2_TSENS_Sn_STATUS_MIN_STATUS_MIN_THRESHOLD_VIOLATED_FVAL                0x1
#define HWIO_MPM2_TSENS_Sn_STATUS_SENSOR_ID_BMSK                                     0xf000
#define HWIO_MPM2_TSENS_Sn_STATUS_SENSOR_ID_SHFT                                        0xc
#define HWIO_MPM2_TSENS_Sn_STATUS_LAST_TEMP_BMSK                                      0xfff
#define HWIO_MPM2_TSENS_Sn_STATUS_LAST_TEMP_SHFT                                        0x0

#define HWIO_MPM2_TSENS_MAX_TEMP_ADDR                                            (MPM2_TSENS_TM_REG_BASE      + 0x000000e0)
#define HWIO_MPM2_TSENS_MAX_TEMP_OFFS                                            (MPM2_TSENS_TM_REG_BASE_OFFS + 0x000000e0)
#define HWIO_MPM2_TSENS_MAX_TEMP_RMSK                                                0xffff
#define HWIO_MPM2_TSENS_MAX_TEMP_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_MAX_TEMP_ADDR, HWIO_MPM2_TSENS_MAX_TEMP_RMSK)
#define HWIO_MPM2_TSENS_MAX_TEMP_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_MAX_TEMP_ADDR, m)
#define HWIO_MPM2_TSENS_MAX_TEMP_MAX_TEMP_SENSOR_ID_BMSK                             0xf000
#define HWIO_MPM2_TSENS_MAX_TEMP_MAX_TEMP_SENSOR_ID_SHFT                                0xc
#define HWIO_MPM2_TSENS_MAX_TEMP_MAX_TEMP_BMSK                                        0xfff
#define HWIO_MPM2_TSENS_MAX_TEMP_MAX_TEMP_SHFT                                          0x0

#define HWIO_MPM2_TSENS_TRDY_ADDR                                                (MPM2_TSENS_TM_REG_BASE      + 0x000000e4)
#define HWIO_MPM2_TSENS_TRDY_OFFS                                                (MPM2_TSENS_TM_REG_BASE_OFFS + 0x000000e4)
#define HWIO_MPM2_TSENS_TRDY_RMSK                                                       0xf
#define HWIO_MPM2_TSENS_TRDY_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_TRDY_ADDR, HWIO_MPM2_TSENS_TRDY_RMSK)
#define HWIO_MPM2_TSENS_TRDY_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_TRDY_ADDR, m)
#define HWIO_MPM2_TSENS_TRDY_FIRST_ROUND_COMPLETE_BMSK                                  0x8
#define HWIO_MPM2_TSENS_TRDY_FIRST_ROUND_COMPLETE_SHFT                                  0x3
#define HWIO_MPM2_TSENS_TRDY_OSC_CLK_OFF_BMSK                                           0x4
#define HWIO_MPM2_TSENS_TRDY_OSC_CLK_OFF_SHFT                                           0x2
#define HWIO_MPM2_TSENS_TRDY_OSC_CLK_OFF_CLK_IS_ON_FVAL                                 0x0
#define HWIO_MPM2_TSENS_TRDY_OSC_CLK_OFF_CLK_IS_OFF_FVAL                                0x1
#define HWIO_MPM2_TSENS_TRDY_SLP_CLK_OFF_BMSK                                           0x2
#define HWIO_MPM2_TSENS_TRDY_SLP_CLK_OFF_SHFT                                           0x1
#define HWIO_MPM2_TSENS_TRDY_SLP_CLK_OFF_CLK_IS_ON_FVAL                                 0x0
#define HWIO_MPM2_TSENS_TRDY_SLP_CLK_OFF_CLK_IS_OFF_FVAL                                0x1
#define HWIO_MPM2_TSENS_TRDY_TRDY_BMSK                                                  0x1
#define HWIO_MPM2_TSENS_TRDY_TRDY_SHFT                                                  0x0
#define HWIO_MPM2_TSENS_TRDY_TRDY_TEMPERATURE_MEASUREMENT_IN_PROGRESS_FVAL              0x0
#define HWIO_MPM2_TSENS_TRDY_TRDY_TEMPERATURE_READING_IS_READY_FVAL                     0x1

#define HWIO_MPM2_TSENS_DEBUG_CONTROL_ADDR                                       (MPM2_TSENS_TM_REG_BASE      + 0x00000130)
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_OFFS                                       (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000130)
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_RMSK                                        0x3ffff3f
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_DEBUG_CONTROL_ADDR, HWIO_MPM2_TSENS_DEBUG_CONTROL_RMSK)
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_DEBUG_CONTROL_ADDR, m)
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_DEBUG_CONTROL_ADDR,v)
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_DEBUG_CONTROL_ADDR,m,v,HWIO_MPM2_TSENS_DEBUG_CONTROL_IN)
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_ID_BMSK                     0x3fffc00
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_ID_SHFT                           0xa
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_HBRG_SLV_DEBUG_BUS_EN_BMSK                0x200
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_HBRG_SLV_DEBUG_BUS_EN_SHFT                  0x9
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_HBRG_SLV_DEBUG_BUS_EN_DISABLED_FVAL         0x0
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_HBRG_SLV_DEBUG_BUS_EN_ENABLED_FVAL          0x1
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_HBRG_MSTR_DEBUG_BUS_EN_BMSK               0x100
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_HBRG_MSTR_DEBUG_BUS_EN_SHFT                 0x8
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_HBRG_MSTR_DEBUG_BUS_EN_DISABLED_FVAL        0x0
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_HBRG_MSTR_DEBUG_BUS_EN_ENABLED_FVAL         0x1
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_SEL_BMSK                         0x3e
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_SEL_SHFT                          0x1
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_EN_BMSK                           0x1
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_EN_SHFT                           0x0
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_EN_DISABLED_FVAL                  0x0
#define HWIO_MPM2_TSENS_DEBUG_CONTROL_TSENS_DEBUG_BUS_EN_ENABLED_FVAL                   0x1

#define HWIO_MPM2_TSENS_DEBUG_READ_ADDR                                          (MPM2_TSENS_TM_REG_BASE      + 0x00000134)
#define HWIO_MPM2_TSENS_DEBUG_READ_OFFS                                          (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000134)
#define HWIO_MPM2_TSENS_DEBUG_READ_RMSK                                          0xffffffff
#define HWIO_MPM2_TSENS_DEBUG_READ_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_DEBUG_READ_ADDR, HWIO_MPM2_TSENS_DEBUG_READ_RMSK)
#define HWIO_MPM2_TSENS_DEBUG_READ_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_DEBUG_READ_ADDR, m)
#define HWIO_MPM2_TSENS_DEBUG_READ_DEBUG_DATA_READ_BMSK                          0xffffffff
#define HWIO_MPM2_TSENS_DEBUG_READ_DEBUG_DATA_READ_SHFT                                 0x0

#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_ADDR(n)                                (MPM2_TSENS_TM_REG_BASE      + 0x00000140 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_OFFS(n)                                (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000140 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_RMSK                                          0x7
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_MAXn                                            3
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_ADDR(n), HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_RMSK)
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_ADDR(n), mask)
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_ADDR(n),val)
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_ADDR(n),mask,val,HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_INI(n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_RESET_HISTORY_COUNTERS_BMSK                   0x4
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_RESET_HISTORY_COUNTERS_SHFT                   0x2
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_TH2_MTC_IN_EFFECT_BMSK                        0x2
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_TH2_MTC_IN_EFFECT_SHFT                        0x1
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_TH1_MTC_IN_EFFECT_BMSK                        0x1
#define HWIO_MPM2_TSENS_MTC_ZONEn_SW_MASK_TH1_MTC_IN_EFFECT_SHFT                        0x0

#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_ADDR(n)                                    (MPM2_TSENS_TM_REG_BASE      + 0x00000150 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_OFFS(n)                                    (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000150 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_RMSK                                       0x7fffffff
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_MAXn                                                3
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_ZONEn_LOG_ADDR(n), HWIO_MPM2_TSENS_MTC_ZONEn_LOG_RMSK)
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_ZONEn_LOG_ADDR(n), mask)
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOGS_VALID_BMSK                 0x40000000
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOGS_VALID_SHFT                       0x1e
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG5_BMSK                       0x3e000000
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG5_SHFT                             0x19
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG4_BMSK                        0x1f00000
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG4_SHFT                             0x14
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG3_BMSK                          0xf8000
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG3_SHFT                              0xf
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG2_BMSK                           0x7c00
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG2_SHFT                              0xa
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG1_BMSK                            0x3e0
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LOG1_SHFT                              0x5
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LATEST_BMSK                           0x1f
#define HWIO_MPM2_TSENS_MTC_ZONEn_LOG_PS_COMMAND_LATEST_SHFT                            0x0

#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_ADDR(n)                                (MPM2_TSENS_TM_REG_BASE      + 0x00000160 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_OFFS(n)                                (MPM2_TSENS_TM_REG_BASE_OFFS + 0x00000160 + 0x4 * (n))
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_RMSK                                   0x3fffffff
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_MAXn                                            3
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_ADDR(n), HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_RMSK)
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_ADDR(n), mask)
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_NUM_PS_RED_CMD_BMSK                    0x3ff00000
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_NUM_PS_RED_CMD_SHFT                          0x14
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_NUM_PS_YELLOW_CMD_BMSK                    0xffc00
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_NUM_PS_YELLOW_CMD_SHFT                        0xa
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_NUM_PS_COOL_CMD_BMSK                        0x3ff
#define HWIO_MPM2_TSENS_MTC_ZONEn_HISTORY_NUM_PS_COOL_CMD_SHFT                          0x0


/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/
#define SECURITY_CONTROL_CORE_REG_BASE                                                        (gpuSecCtrlBase      + 0x00000000)

#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004208)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00004208)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS15_OFFSET_BMSK                                   0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS15_OFFSET_SHFT                                         0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS14_OFFSET_BMSK                                    0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS14_OFFSET_SHFT                                         0x18
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_23_BMSK                                        0x800000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_23_SHFT                                            0x17
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_22_BMSK                                        0x400000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_22_SHFT                                            0x16
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_21_BMSK                                        0x200000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_21_SHFT                                            0x15
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_20_BMSK                                        0x100000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_20_SHFT                                            0x14
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_19_BMSK                                         0x80000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_19_SHFT                                            0x13
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_18_BMSK                                         0x40000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_18_SHFT                                            0x12
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_17_BMSK                                         0x20000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_17_SHFT                                            0x11
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_16_BMSK                                         0x10000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_16_SHFT                                            0x10
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_15_BMSK                                          0x8000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_15_SHFT                                             0xf
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_14_BMSK                                          0x4000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_SPARE_65_14_SHFT                                             0xe
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_USB2PHY_HSTX_CALIB_BMSK                                   0x3c00
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_USB2PHY_HSTX_CALIB_SHFT                                      0xa
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ELDO_SW_CALIB_BMSK                                         0x3e0
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ELDO_SW_CALIB_SHFT                                           0x5
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ELDO_HW_CALIB_BMSK                                          0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ELDO_HW_CALIB_SHFT                                           0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x0000420c)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x0000420c)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RSVD_31_BMSK                                          0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RSVD_31_SHFT                                                0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE_65_55_BMSK                                        0x800000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE_65_55_SHFT                                            0x17
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_BASE1_BMSK                                        0x7fe000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_BASE1_SHFT                                             0xd
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_BASE0_BMSK                                          0x1ff8
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_BASE0_SHFT                                             0x3
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_CAL_SEL_BMSK                                           0x7
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_CAL_SEL_SHFT                                           0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004210)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00004210)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS7_OFFSET_BMSK                                    0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS7_OFFSET_SHFT                                          0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS6_OFFSET_BMSK                                     0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS6_OFFSET_SHFT                                          0x18
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS5_OFFSET_BMSK                                      0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS5_OFFSET_SHFT                                          0x14
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS4_OFFSET_BMSK                                       0xf0000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS4_OFFSET_SHFT                                          0x10
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS3_OFFSET_BMSK                                        0xf000
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS3_OFFSET_SHFT                                           0xc
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS2_OFFSET_BMSK                                         0xf00
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS2_OFFSET_SHFT                                           0x8
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS1_OFFSET_BMSK                                          0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS1_OFFSET_SHFT                                           0x4
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS0_OFFSET_BMSK                                           0xf
#define HWIO_QFPROM_CORR_CALIB_ROW1_LSB_TSENS0_OFFSET_SHFT                                           0x0

#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x00004214)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_OFFS                                                  (SECURITY_CONTROL_CORE_REG_BASE_OFFS + 0x00004214)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RSVD_31_BMSK                                          0x80000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_RSVD_31_SHFT                                                0x1f
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_FEC_VALUE_BMSK                                        0x7f000000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_FEC_VALUE_SHFT                                              0x18
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS13_OFFSET_BMSK                                     0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS13_OFFSET_SHFT                                         0x14
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS12_OFFSET_BMSK                                      0xf0000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS12_OFFSET_SHFT                                         0x10
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS11_OFFSET_BMSK                                       0xf000
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS11_OFFSET_SHFT                                          0xc
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS10_OFFSET_BMSK                                        0xf00
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS10_OFFSET_SHFT                                          0x8
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS9_OFFSET_BMSK                                          0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS9_OFFSET_SHFT                                           0x4
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS8_OFFSET_BMSK                                           0xf
#define HWIO_QFPROM_CORR_CALIB_ROW1_MSB_TSENS8_OFFSET_SHFT                                           0x0

#endif /* HAL_HWIO_TSENS_H */
