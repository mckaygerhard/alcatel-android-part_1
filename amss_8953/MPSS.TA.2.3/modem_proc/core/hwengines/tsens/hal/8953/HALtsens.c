/*============================================================================
  FILE:         HALTsens.c

  OVERVIEW:     Implementation of the TSENS HAL for 8952

  DEPENDENCIES: None

                Copyright (c) 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.


  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-07-22  PR   Initial version for Jacala.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "HALhwio.h"
#include "HALhwioTsens.h"
#include "HALtsens.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
uint8 *gpuMpm2MpmBase;
uint8 *gpuSecCtrlBase;

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Externalized Function Definitions
 * -------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Get temperature measurement
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_GetSensorPrevTemp
**
**  Description:
**    Gets the adc result from the previous output.
**
**  Parameters:
**    uSensor - which of the sensors
**    puCode  - TSENS ADC code
**
**  Return: true if reading is valid, else false
**
**  Dependencies: adc result must be generated already
**
** ========================================================================= */
boolean HAL_tsens_GetSensorPrevTemp(uint32 uSensor, int32 *pnDeciDegC)
{
   uint32 uTsensSnStatus;
   uint32 uValidBit;
   int32 nDeciDegC;
   uTsensSnStatus = HWIO_INI(MPM2_TSENS_Sn_STATUS, uSensor);

   nDeciDegC = (int32)((uTsensSnStatus & HWIO_FMSK(MPM2_TSENS_Sn_STATUS, LAST_TEMP)) >> HWIO_SHFT(MPM2_TSENS_Sn_STATUS, LAST_TEMP));

   uValidBit = (uTsensSnStatus & HWIO_FMSK(MPM2_TSENS_Sn_STATUS, VALID)) >> HWIO_SHFT(MPM2_TSENS_Sn_STATUS, VALID);

   /* Temperature is a 12-bit number -- sign extend to 32 bits */
   if (nDeciDegC & 0x800)
   {
      nDeciDegC |= 0xFFFFF000;
   }

   *pnDeciDegC = nDeciDegC;

   if (uValidBit == 1)
   {
      return TRUE;
   }
   else
   {
      return FALSE;
   }

}

/* ============================================================================
**
**  HAL_tsens_TempMeasurementIsComplete
**
**  Description:
**    Finds out if the adc result of a given sensor is ready to read.
**
**  Parameters:
**    None
**
**  Return: whether temperature measurement has been completed w/ the associated
**          adc result in the register as a boolean (T/F).
**
**  Dependencies: tsens is enabled
**
** ========================================================================= */
boolean HAL_tsens_TempMeasurementIsComplete(void)
{
   uint32 uTempReady;

   uTempReady = HWIO_INF(MPM2_TSENS_TRDY, TRDY);

   if (uTempReady == 0)
   {
      return FALSE;
   }
   else
   {
      return TRUE;
   }
}

/*----------------------------------------------------------------------------
 * Interrupts
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_EnableUpperLowerInterrupt
**
**  Description:
**    Main enable for upper / lower thresholds
**
**  Parameters:
**    None
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_EnableUpperLowerInterrupt(void)
{
   uint32 uReg;

   uReg = HWIO_IN(MPM2_TSENS_TM_INT_EN);

   uReg &= ~HWIO_FMSK(MPM2_TSENS_TM_INT_EN, LOWER_INT_EN);
   uReg &= ~HWIO_FMSK(MPM2_TSENS_TM_INT_EN, UPPER_INT_EN);

   uReg |= HWIO_FVALV(MPM2_TSENS_TM_INT_EN, LOWER_INT_EN, ENABLED);
   uReg |= HWIO_FVALV(MPM2_TSENS_TM_INT_EN, UPPER_INT_EN, ENABLED);

   HWIO_OUT(MPM2_TSENS_TM_INT_EN, uReg);
}

/* ============================================================================
**
**  HAL_tsens_DisableUpperLowerInterrupt
**
**  Description:
**    Main disable for upper / lower thresholds
**
**  Parameters:
**    None
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_DisableUpperLowerInterrupt(void)
{
   uint32 uReg;

   uReg = HWIO_IN(MPM2_TSENS_TM_INT_EN);

   uReg &= ~HWIO_FMSK(MPM2_TSENS_TM_INT_EN, LOWER_INT_EN);
   uReg &= ~HWIO_FMSK(MPM2_TSENS_TM_INT_EN, UPPER_INT_EN);

   uReg |= HWIO_FVALV(MPM2_TSENS_TM_INT_EN, LOWER_INT_EN, DISABLED);
   uReg |= HWIO_FVALV(MPM2_TSENS_TM_INT_EN, UPPER_INT_EN, DISABLED);

   HWIO_OUT(MPM2_TSENS_TM_INT_EN, uReg);
}
/* ============================================================================
**
**  HAL_tsens_EnableCriticalInterrupt
**
**  Description:
**    Main enable for critical thresholds
**
**  Parameters:None
**    
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_EnableCriticalInterrupt(void)
{
   HWIO_OUTV(MPM2_TSENS_TM_INT_EN, CRITICAL_INT_EN, ENABLED);
}

/* ============================================================================
**
**  HAL_tsens_DisableCriticalInterrupt
**
**  Description:
**    Main disable for critical thresholds
**
**  Parameters:
**    
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_DisableCriticalInterrupt(void)
{
   HWIO_OUTV(MPM2_TSENS_TM_INT_EN, CRITICAL_INT_EN, DISABLED);
}
/* ============================================================================
**
**  HAL_tsens_EnableInterrupt
**
**  Description:
**    Enable tsens for a given threshold level
**
**  Parameters:
**    eThresholdLevel
**    uSensor
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_EnableInterrupt(HAL_tsens_ThresholdLevel eThresholdLevel, uint32 uSensor)
{
   uint32 uReg;
   switch(eThresholdLevel)
   {
      case HAL_TSENS_MIN_LIMIT_TH:
         HWIO_OUTVI(MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL, uSensor, MIN_STATUS_MASK, NORMAL_OPERATION);
         break;
      case HAL_TSENS_LOWER_LIMIT_TH:
         uReg = HWIO_INF(MPM2_TSENS_UPPER_LOWER_INT_MASK, LOWER_INT_MASK);
         uReg &= ~(1<<uSensor);
         HWIO_OUTF(MPM2_TSENS_UPPER_LOWER_INT_MASK, LOWER_INT_MASK, uReg);
         break;
      case HAL_TSENS_UPPER_LIMIT_TH:
         uReg = HWIO_INF(MPM2_TSENS_UPPER_LOWER_INT_MASK, UPPER_INT_MASK);
         uReg &= ~(1<<uSensor);
         HWIO_OUTF(MPM2_TSENS_UPPER_LOWER_INT_MASK, UPPER_INT_MASK, uReg);
         break;
      case HAL_TSENS_CRITICAL_LIMIT_TH:
         uReg = HWIO_INF(MPM2_TSENS_CRITICAL_INT_MASK, CRITICAL_INT_MASK);
         uReg &= ~(1<<uSensor);
         HWIO_OUTF(MPM2_TSENS_CRITICAL_INT_MASK, CRITICAL_INT_MASK, uReg);
         break;
      case HAL_TSENS_MAX_LIMIT_TH:
         HWIO_OUTVI(MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL, uSensor, MAX_STATUS_MASK, NORMAL_OPERATION);
         break;
      default:
         break;
   }
}

/* ============================================================================
**
**  HAL_tsens_DisableInterrupt
**
**  Description:
**    Disable tsens for a given threshold level
**
**  Parameters:
**    eThresholdLevel
**    uSensor
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_DisableInterrupt(HAL_tsens_ThresholdLevel eThresholdLevel, uint32 uSensor)
{
   uint32 uReg;
   switch(eThresholdLevel)
   {
      case HAL_TSENS_MIN_LIMIT_TH:
         HWIO_OUTVI(MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL, uSensor, MIN_STATUS_MASK, MASK_OFF_MIN_STATUS);
         break;
      case HAL_TSENS_LOWER_LIMIT_TH:
         uReg = HWIO_INF(MPM2_TSENS_UPPER_LOWER_INT_MASK, LOWER_INT_MASK);
         uReg |= (1<<uSensor);
         HWIO_OUTF(MPM2_TSENS_UPPER_LOWER_INT_MASK, LOWER_INT_MASK, uReg);
         break;
      case HAL_TSENS_UPPER_LIMIT_TH:
         uReg = HWIO_INF(MPM2_TSENS_UPPER_LOWER_INT_MASK, UPPER_INT_MASK);
         uReg |= (1<<uSensor);
         HWIO_OUTF(MPM2_TSENS_UPPER_LOWER_INT_MASK, UPPER_INT_MASK, uReg);
         break;
      case HAL_TSENS_CRITICAL_LIMIT_TH:
         uReg = HWIO_INF(MPM2_TSENS_CRITICAL_INT_MASK, CRITICAL_INT_MASK);
         uReg |= (1<<uSensor);
         HWIO_OUTF(MPM2_TSENS_CRITICAL_INT_MASK, CRITICAL_INT_MASK, uReg);
         break;
      case HAL_TSENS_MAX_LIMIT_TH:
         HWIO_OUTVI(MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL, uSensor, MAX_STATUS_MASK, MASK_OFF_MAX_STATUS);
         break;
      default:
         break;
   }
}

/* ============================================================================
**
**  HAL_tsens_ClearInterrupt
**
**  Description:
**    Clears tsens interrupt for a given threshold level
**
**  Parameters:
**    eThresholdLevel
**    uSensor
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_ClearInterrupt(HAL_tsens_ThresholdLevel eThresholdLevel, uint32 uSensor)
{
   uint32 uReg;   /* Note: clear register is write only */

   switch(eThresholdLevel)
   {
      case HAL_TSENS_MIN_LIMIT_TH:
         break;
      case HAL_TSENS_LOWER_LIMIT_TH:
         uReg = HWIO_FVAL(MPM2_TSENS_UPPER_LOWER_INT_CLEAR, LOWER_INT_CLEAR, 1<<uSensor);
         HWIO_OUT(MPM2_TSENS_UPPER_LOWER_INT_CLEAR, uReg);
         HWIO_OUT(MPM2_TSENS_UPPER_LOWER_INT_CLEAR, 0);
         break;
      case HAL_TSENS_UPPER_LIMIT_TH:
         uReg = HWIO_FVAL(MPM2_TSENS_UPPER_LOWER_INT_CLEAR, UPPER_INT_CLEAR, 1<<uSensor);
         HWIO_OUT(MPM2_TSENS_UPPER_LOWER_INT_CLEAR, uReg);
         HWIO_OUT(MPM2_TSENS_UPPER_LOWER_INT_CLEAR, 0);
         break;
      case HAL_TSENS_CRITICAL_LIMIT_TH:
         uReg = HWIO_FVAL(MPM2_TSENS_CRITICAL_INT_CLEAR, CRITICAL_INT_CLEAR, 1<<uSensor);
         HWIO_OUT(MPM2_TSENS_CRITICAL_INT_CLEAR, uReg);
         HWIO_OUT(MPM2_TSENS_CRITICAL_INT_CLEAR, 0);
         break;
      case HAL_TSENS_MAX_LIMIT_TH:
         break;
      default:
         break;
   }
}
/*----------------------------------------------------------------------------
 * Thresholds
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_CurrentlyBeyondThreshold
**
**  Description:
**    Finds out if a given threshold has crossed
**
**  Parameters:
**    eThresholdLevel
**    uSensor
**
**  Return: T/F
**
**  Dependencies: None
**
** ========================================================================= */
boolean HAL_tsens_CurrentlyBeyondThreshold(HAL_tsens_ThresholdLevel eThresholdLevel, uint32 uSensor)
{
   uint32 uThreshSet = 0;

   switch(eThresholdLevel)
   {
      case HAL_TSENS_MIN_LIMIT_TH:
         uThreshSet = HWIO_INFI(MPM2_TSENS_Sn_STATUS, uSensor, MIN_STATUS);
         break;
      case HAL_TSENS_LOWER_LIMIT_TH:
         uThreshSet = HWIO_INFI(MPM2_TSENS_Sn_STATUS, uSensor, LOWER_STATUS);
         break;
      case HAL_TSENS_UPPER_LIMIT_TH:
         uThreshSet = HWIO_INFI(MPM2_TSENS_Sn_STATUS, uSensor, UPPER_STATUS);
         break;
      case HAL_TSENS_CRITICAL_LIMIT_TH:
         uThreshSet = HWIO_INFI(MPM2_TSENS_Sn_STATUS, uSensor, CRITICAL_STATUS);
         break;
      case HAL_TSENS_MAX_LIMIT_TH:
         uThreshSet = HWIO_INFI(MPM2_TSENS_Sn_STATUS, uSensor, MAX_STATUS);
         break;
      default:
         break;
   }

   if (uThreshSet == 0)
   {
      return FALSE;
   }
   else
   {
      return TRUE;
   }
}

/* ============================================================================
**
**  HAL_tsens_SetThreshold
**
**  Description:
**    Sets a given threshold
**
**  Parameters:
**    eThresholdLevel
**    uSensor
**    uThresholdCode - Value for that threshold
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetThreshold(HAL_tsens_ThresholdLevel eThresholdLevel, uint32 uSensor, int32 nThreshold)
{
   switch(eThresholdLevel)
   {
      case HAL_TSENS_MIN_LIMIT_TH:
         HWIO_OUTFI(MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL, uSensor, MIN_THRESHOLD, nThreshold);
         break;
      case HAL_TSENS_LOWER_LIMIT_TH:
         HWIO_OUTFI(MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD, uSensor, LOWER_THRESHOLD, nThreshold);
         break;
      case HAL_TSENS_UPPER_LIMIT_TH:
         HWIO_OUTFI(MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD, uSensor, UPPER_THRESHOLD, nThreshold);
         break;
      case HAL_TSENS_CRITICAL_LIMIT_TH:
         HWIO_OUTFI(MPM2_TSENS_Sn_CRITICAL_THRESHOLD, uSensor, CRITICAL_THRESHOLD, nThreshold);
         break;
      case HAL_TSENS_MAX_LIMIT_TH:
         HWIO_OUTFI(MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL, uSensor, MAX_THRESHOLD, nThreshold);
         break;
      default:
         break;
   }
}

/* ============================================================================
**
**  HAL_tsens_GetThreshold
**
**  Description:
**    Sets a given threshold
**
**  Parameters:
**    eThresholdLevel
**    uSensor
**
**  Return: Value for that threshold
**
**  Dependencies: None
**
** ========================================================================= */
int32 HAL_tsens_GetThreshold(HAL_tsens_ThresholdLevel eThresholdLevel, uint32 uSensor)
{
   int32 nThreshold = 0;

   switch(eThresholdLevel)
   {
      case HAL_TSENS_MIN_LIMIT_TH:
         nThreshold = HWIO_INFI(MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL, uSensor, MIN_THRESHOLD);
         break;
      case HAL_TSENS_LOWER_LIMIT_TH:
         nThreshold = HWIO_INFI(MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD, uSensor, LOWER_THRESHOLD);
         break;
      case HAL_TSENS_UPPER_LIMIT_TH:
         nThreshold = HWIO_INFI(MPM2_TSENS_Sn_UPPER_LOWER_THRESHOLD, uSensor, UPPER_THRESHOLD);
         break;
      case HAL_TSENS_CRITICAL_LIMIT_TH:
         nThreshold = HWIO_INFI(MPM2_TSENS_Sn_CRITICAL_THRESHOLD, uSensor, CRITICAL_THRESHOLD);
         break;
      case HAL_TSENS_MAX_LIMIT_TH:
         nThreshold = HWIO_INFI(MPM2_TSENS_Sn_MAX_MIN_STATUS_CTRL, uSensor, MAX_THRESHOLD);
         break;
      default:
         break;
   }
   return nThreshold;
}

/*----------------------------------------------------------------------------
 * Measurement period
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_SetPeriod
**
**  Description:
**    Sets the period for the active case
**
**  Parameters:
**    
**    uTime
**     - 0x00 measures continiously
**     - 0x01-0xFE defines the length of sleep between two measurements
**                 approx equal to (upper 2bits) * 250ms + (lower 6bits) * 1.95ms
**     - 0xFF measures once
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetPeriod(uint32 uTime)
{
   HWIO_OUTF(MPM2_TSENS_MEASURE_PERIOD, MAIN_MEASURE_PERIOD, uTime);
}
/* ============================================================================
**
**  HAL_tsens_SetPeriodSleep
**
**  Description:
**    Sets the period for the sleep case
**
**  Parameters:
**    
**    uTime
**     - 0x00 measures continiously
**     - 0x01-0xFE defines the length of sleep between two measurements
**                 approx equal to (upper 2bits) * 250ms + (lower 6bits) * 1.95ms
**     - 0xFF measures once
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetPeriodSleep(uint32 uTime)
{
   HWIO_OUTF(MPM2_TSENS_MEASURE_PERIOD, POWERDOWN_MEASURE_PERIOD, uTime);
}

/* ============================================================================
**
**  HAL_tsens_SetAutoAdjustPeriod
**
**  Description:
**    Sets whether or not TSENS will auto-adjust the period going in and
**    out of sleep
**
**  Parameters:
**    
**    bEnable - TRUE / FALSE
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetAutoAdjustPeriod(boolean bEnable)
{
   if (bEnable == TRUE)
   {
      HWIO_OUTV(MPM2_TSENS_CTRL, AUTO_ADJUST_PERIOD_EN, ENABLED);
   }
   else
   {
      HWIO_OUTV(MPM2_TSENS_CTRL, AUTO_ADJUST_PERIOD_EN, DISABLED);
   }
}
/* ============================================================================
**
**  HAL_tsens_GetPeriod
**
**  Description:
**    Gets the period for the active case
**
**  Parameters:
**    None
**
**  Return: 
**     - 0x00 measures continiously
**     - 0x01-0xFE defines the length of sleep between two measurements
**                 approx equal to (upper 2bits) * 250ms + (lower 6bits) * 1.95ms
**     - 0xFF measures once
**  Dependencies: None
**
** ========================================================================= */

uint32 HAL_tsens_GetPeriod(void)
{
   return HWIO_INF(MPM2_TSENS_MEASURE_PERIOD, MAIN_MEASURE_PERIOD);
   
}

/* ============================================================================
**
**  HAL_tsens_GetLongestPeriod
**
**  Description:
**    Provides the longest measurement period
**
**  Parameters:
**    None
**
**  Return: Highest value supported
**
**  Dependencies: None
**
** ========================================================================= */
uint32 HAL_tsens_GetLongestPeriod(void)
{
   return HWIO_FMSK(MPM2_TSENS_MEASURE_PERIOD, MAIN_MEASURE_PERIOD) >> HWIO_SHFT(MPM2_TSENS_MEASURE_PERIOD, MAIN_MEASURE_PERIOD);
}

/*----------------------------------------------------------------------------
 * Sensor enable / disable
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_SetSensorsEnabled
**
**  Description:
**    Enable a set of sensors based on bitmask settings
**
**  Parameters:
**    uSensorMask - bit mask settings
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetSensorsEnabled(uint32 uSensorMask)
{
   uint32 uReg;

   uReg = HWIO_IN(MPM2_TSENS_CTRL);

   uReg &= ~(HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR0_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR1_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR2_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR3_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR4_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR5_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR6_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR7_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR8_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR9_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR10_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR11_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR12_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR13_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR14_EN) |
             HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR15_EN));

   uReg |= uSensorMask << HWIO_SHFT(MPM2_TSENS_CTRL, SENSOR0_EN) &
           (HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR0_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR1_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR2_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR3_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR4_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR5_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR6_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR7_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR8_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR9_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR10_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR11_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR12_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR13_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR14_EN) |
            HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR15_EN));

   HWIO_OUT(MPM2_TSENS_CTRL , uReg);
}

/* ============================================================================
**
**  HAL_tsens_GetEnabledSensors
**
**  Description:
**    Gets a bit mask of all enabled TSENS sensors.
**
**  Parameters:
**    None
**
**  Return: bit mask settings of enabled sensors;
**
**  Dependencies: None
**
** ========================================================================= */
uint32 HAL_tsens_GetEnabledSensors(void)
{
   uint32 uSensorMask;

   uSensorMask = HWIO_IN(MPM2_TSENS_CTRL);

   uSensorMask &= (HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR0_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR1_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR2_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR3_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR4_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR5_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR6_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR7_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR8_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR9_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR10_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR11_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR12_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR13_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR14_EN) |
                      HWIO_FMSK(MPM2_TSENS_CTRL, SENSOR15_EN));

   uSensorMask = uSensorMask >> HWIO_SHFT(MPM2_TSENS_CTRL, SENSOR0_EN);

   return uSensorMask;
}

/*----------------------------------------------------------------------------
 * Main enable / disable
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_SetState
**
**  Description:
**    Enable or disable tsens and returned the previous state
**
**  Parameters:
**    eTsensState - HAL_TSENS_ENABLE/HAL_TSENS_DISABLE
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetState(HAL_tsens_srot_State eTsensState)
{
   switch(eTsensState)
   {
      case HAL_TSENS_ENABLE:
         HWIO_OUTV(MPM2_TSENS_CTRL, TSENS_EN, ENABLED);
         break;
      case HAL_TSENS_DISABLE:
         HWIO_OUTV(MPM2_TSENS_CTRL, TSENS_EN, DISABLED);
         break;
      default:
         break;
   }
}
/*----------------------------------------------------------------------------
 * MTC enable / disable
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_SetMTCState
**
**  Description:
**    Enable or disable tsens MTC functionality.
**
**  Parameters:
**    eTsensState - HAL_TSENS_ENABLE_MTC/HAL_TSENS_DISABLE_MTC
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetMTCState(HAL_tsens_srot_State eTsensState)
{
   switch(eTsensState)
   {
      case HAL_TSENS_ENABLE_MTC:
         HWIO_OUTV(MPM2_TSENS_CTRL, TSENS_MTC_EN, ENABLED);
         break;
      case HAL_TSENS_DISABLE_MTC:
         HWIO_OUTV(MPM2_TSENS_CTRL, TSENS_MTC_EN, DISABLED);
         break;
      default:
         break;
   }
}

/*----------------------------------------------------------------------------
 * Software reset
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_Reset
**
**  Description:
**    Resets TSENS.
**
**  Parameters:
**    None
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_Reset(void)
{
   HWIO_OUTV(MPM2_TSENS_CTRL, TSENS_SW_RST, RESET_ASSERTED);

   HWIO_OUTV(MPM2_TSENS_CTRL, TSENS_SW_RST, RESET_DEASSERTED);
}

/*----------------------------------------------------------------------------
 * Config
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_Init
**
**  Description:
**    Initialization of tsens.
**
**  Parameters:
**    uVal
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_Init(uint32 uVal)
{
   HWIO_OUT(MPM2_TS_CONTROL, uVal);
}

/* ============================================================================
**
**  HAL_tsens_ConfigSensor
**
**  Description:
**    Initialization of tsens.
**
**  Parameters:
**    uSensor
**    uVal
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_ConfigSensor(uint32 uSensor, uint32 uVal)
{
   HWIO_OUT(MPM2_TS_CONFIG, uVal);
}
/* ============================================================================
**
**  HAL_tsens_SelectADCClkSrc
**
**  Description:
**    Sets a given threshold
**
**  Parameters:
**    eADCClkSrc - internal clk or external clk.
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SelectADCClkSrc(HAL_tsens_srot_ADCClkSrc eADCClkSrc)
{
   switch(eADCClkSrc)
   {
      case HAL_TSENS_INTERNAL:
         HWIO_OUTV(MPM2_TSENS_CTRL, TSENS_ADC_CLK_SEL, INTERNAL_OSCILLATOR);
         break;
      case HAL_TSENS_EXTERNAL:
         HWIO_OUTV(MPM2_TSENS_CTRL, TSENS_ADC_CLK_SEL, EXTERNAL_CLOCK_SOURCE);
         break;
      default:
         break;
   }
}
/* ============================================================================
**
**  HAL_tsens_GetMaxCode
**
**  Description:
**    Provides the max ADC code
**
**  Parameters:
**    None
**
**  Return: adc result
**
**  Dependencies: None
**
** ========================================================================= */
uint32 HAL_tsens_GetMaxCode(void)
{
   return 0x7FF;
}
/* ============================================================================
**
**  HAL_tsens_GetMaxTemp
**
**  Description:
**    Gets the maximum temperature in deci deg C
**
**  Parameters:
**    None
**
**  Return: Maximum temperature in deci deg C
**
**  Dependencies: None
**
** ========================================================================= */
int32 HAL_tsens_GetMaxTemp(void)
{
   return 0x7FF;
}

/* ============================================================================
**
**  HAL_tsens_GetMinTemp
**
**  Description:
**    Gets the minimum temperature in deci deg C
**
**  Parameters:
**    None
**
**  Return: Minimum temperature in deci deg C
**
**  Dependencies: None
**
** ========================================================================= */
int32 HAL_tsens_GetMinTemp(void)
{
   return 0xFFFFF800;
}
/*----------------------------------------------------------------------------
 * Config (2.X controller)
 * -------------------------------------------------------------------------*/

/* ============================================================================
**
**  HAL_tsens_GetControllerVersion
**
**  Description:
**    Gets the TSENS controller version
**
**  Parameters:
**    
**
**  Return: TSENS controller version
**
**  Dependencies: None
**
** ========================================================================= */
uint32 HAL_tsens_GetControllerVersion(void)
{
   return HWIO_IN(MPM2_TSENS_HW_VER);
}

/* ============================================================================
**
**  HAL_tsens_SetValidBitDelay
**
**  Description:
**    Sets the valid bit delay
**
**  Parameters:
**    
**    uNumClockCycles - number of clock cycles
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetValidBitDelay(uint32 uNumClockCycles)
{
   HWIO_OUTF(MPM2_TSENS_CTRL, VALID_DELAY, uNumClockCycles);
}

/* ============================================================================
**
**  HAL_tsens_SetPSHoldResetEn
**
**  Description:
**    Enables / disables PS_HOLD reset functionality
**
**  Parameters:
**    
**    bEnable - TRUE / FALSE
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetPSHoldResetEn(boolean bEnable)
{
   if (bEnable == TRUE)
   {
      HWIO_OUTV(MPM2_TSENS_CTRL, PSHOLD_ARES_EN, ENABLED);
   }
   else
   {
      HWIO_OUTV(MPM2_TSENS_CTRL, PSHOLD_ARES_EN, DISABLED);
   }
}

/* ============================================================================
**
**  HAL_tsens_SetResultFormat
**
**  Description:
**    Sets whether the result will be in code or real temperature
**
**  Parameters:
**    
**    eResultType - whether the result will be in code or real temperature
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetResultFormat(HAL_tsens_srot_ResultType eResultType)
{
   switch (eResultType)
   {
      case HAL_TSENS_RESULT_TYPE_ADC_CODE:
         HWIO_OUTV(MPM2_TSENS_CTRL, RESULT_FORMAT_CODE_OR_TEMP, ADC_CODE);
         break;
      case HAL_TSENS_RESULT_TYPE_DECI_DEG_C:
         HWIO_OUTV(MPM2_TSENS_CTRL, RESULT_FORMAT_CODE_OR_TEMP, REAL_TEMPERATURE);
         break;
      default:
         break;
   }
}

/* ============================================================================
**
**  HAL_tsens_SetConversionFactors
**
**  Description:
**    Sets up the parameters needed to convert from code to deci deg C.
**
**  Parameters:
**    
**    uSensor - which sensor
**    uShift - shift value of N = 7, 8, 9 or 10
**    uShiftedSlope - slope << N, i.e. (900 << N) / (C120 - C30)
**    uCodeAtZero - ADC code at zero degrees C
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetConversionFactors(uint32 uSensor, uint32 uShift, uint32 uShiftedSlope, uint32 uCodeAtZero)
{
   uint32 uReg;

   switch (uShift)
   {
      case 7:
         uShift = 0;
         break;
      case 8:
         uShift = 1;
         break;
      case 9:
         uShift = 2;
         break;
      case 10:
         uShift = 3;
         break;
      default:
         break;
   }

   uReg = HWIO_INI(MPM2_TSENS_Sn_CONVERSION, uSensor);

   uReg &= ~HWIO_FMSK(MPM2_TSENS_Sn_CONVERSION, SHIFT);
   uReg &= ~HWIO_FMSK(MPM2_TSENS_Sn_CONVERSION, SLOPE);
   uReg &= ~HWIO_FMSK(MPM2_TSENS_Sn_CONVERSION, CZERO);

   uReg |= HWIO_FVAL(MPM2_TSENS_Sn_CONVERSION, SHIFT, uShift);
   uReg |= HWIO_FVAL(MPM2_TSENS_Sn_CONVERSION, SLOPE, uShiftedSlope);
   uReg |= HWIO_FVAL(MPM2_TSENS_Sn_CONVERSION, CZERO, uCodeAtZero);

   HWIO_OUTI(MPM2_TSENS_Sn_CONVERSION, uSensor, uReg);
}

/* ============================================================================
**
**  HAL_tsens_SetSensorID
**
**  Description:
**    Sets the sensor ID for each sensor
**
**  Parameters:
**    
**    uSensorID - the sensor ID
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetSensorID(uint32 uSensor, uint32 uSensorID)
{
   HWIO_OUTFI(MPM2_TSENS_Sn_ID_ASSIGNMENT, uSensor, SENSOR_ID, uSensorID);
}
/* ============================================================================
**
**  HAL_tsens_SetPWMEn
**
**  Description:
**    Enables / disables PWM functionality
**
**  Parameters:
**    
**    bEnable - TRUE / FALSE
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetPWMEn(boolean bEnable)
{
   if (bEnable == TRUE)
   {
      HWIO_OUTV(MPM2_TSENS_CTRL, MAX_TEMP_PWM_EN, ENABLED);
   }
   else
   {
      HWIO_OUTV(MPM2_TSENS_CTRL, MAX_TEMP_PWM_EN, DISABLED);
   }
}
/*----------------------------------------------------------------------------
 * LMh-Lite - NOT supported on 8953
 * -------------------------------------------------------------------------*/

/* ============================================================================
**
**  HAL_tsens_SetSidebandSensorsEnabled
**
**  Description:
**    Sets the sensors Mask for side band interface
**
**  Parameters:
**    
**    uSensorEnableMask - bitmask of sensors to enable
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_SetSidebandSensorsEnabled(uint32 uSensorEnableMask)
{
   HWIO_OUTF(MPM2_TSENS_SIDEBAND_EN, SENSOR_EN, uSensorEnableMask);
}
uint32 HAL_tsens_GetDisabledCPUsMask()
{
   return 0;
}

void HAL_tsens_SetCpuIndexes(uint32 uCpuIndexes)
{
   return;
}

void HAL_tsens_SetCpuHighLowThreshold(uint32 uCpuIndex, int32 uHighThresholdCode, int32 uLowThresholdCode, boolean bHighEn, boolean bLowEn)
{
   return;
}

void HAL_tsens_SetCpuHighLowEnable(boolean bEnable)
{
   return;
}

/*----------------------------------------------------------------------------
 * MTC-related config
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_MTC_ConfigZone
**
**  Description:
**    Configures the specified MTC zone
**
**  Parameters:
**    uZone               Zone to be configured.
**    uPSCommandTh2Viol   Command to be sent by Pulse Swallower when TH2 temp is crossed.
**    uPSCommandTh1Viol   Command to be sent by Pulse Swallower when TH1 temp is crossed.
**    uPSCommandCool      Command to be sent by Pulse Swallower when temp falls to cool zone.
**    uSensorMask         Bit mask of sensors in this zone.
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_MTC_ConfigZone(uint32 uZone, uint32 uPSCommandTh2Viol, uint32 uPSCommandTh1Viol, uint32 uPSCommandCool, uint32 uSensorMask)
{
   uint32 zoneCtrlVal;

   zoneCtrlVal = HWIO_FVAL(MPM2_TSENS_MTC_ZONEn_CTRL, SENSOR_MAP, uSensorMask) |
                 HWIO_FVAL(MPM2_TSENS_MTC_ZONEn_CTRL, PS_COMMAND_COOL, uPSCommandCool) |
                 HWIO_FVAL(MPM2_TSENS_MTC_ZONEn_CTRL, PS_COMMAND_TH1_VIOLATED, uPSCommandTh1Viol) |
                 HWIO_FVAL(MPM2_TSENS_MTC_ZONEn_CTRL, PS_COMMAND_TH2_VIOLATED, uPSCommandTh2Viol) |
                 HWIO_FVAL(MPM2_TSENS_MTC_ZONEn_CTRL, ZONE_MTC_EN, 0x1);

   HWIO_OUTI(MPM2_TSENS_MTC_ZONEn_CTRL, uZone, zoneCtrlVal);
}

/* ============================================================================
**
**  HAL_tsens_MTC_SetThresholds
**
**  Description:
**    Sets the per-sensor threshold codes corresponding to TH1 and TH2 (yellow and red)
**    threshold temperatures.
**
**  Parameters:
**    uSensor      Sensor number for which threshold is to be set
**    nTh1Temp     TH1 threshold temperature (DeciDegrees)
**    nTh2Temp     TH2 threshold temperature (DeciDegrees)
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_MTC_SetThresholds(uint32 uSensor, int32 nTh1Temp, int32 nTh2Temp)
{
   uint32 uRegVal;

   uRegVal = HWIO_INI(MPM2_TSENS_MTC_Sn_THRESHOLDS, uSensor);

   uRegVal &= ~((HWIO_FMSK(MPM2_TSENS_MTC_Sn_THRESHOLDS, TH1)) |
                (HWIO_FMSK(MPM2_TSENS_MTC_Sn_THRESHOLDS, TH2)));

   uRegVal |= HWIO_FVAL(MPM2_TSENS_MTC_Sn_THRESHOLDS, TH1, nTh1Temp) |
              HWIO_FVAL(MPM2_TSENS_MTC_Sn_THRESHOLDS, TH2, nTh2Temp);

   HWIO_OUTI(MPM2_TSENS_MTC_Sn_THRESHOLDS, uSensor, uRegVal);
}

/* ============================================================================
**
**  HAL_tsens_MTC_SetMargins
**
**  Description:
**    Sets the margins for TH1 and TH2 thresholds.
**
**  Parameters:
**    nTh1Margin     TH1 margin (DeciDegrees)
**    nTh2Margin     TH2 margin (DeciDegrees)
**
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_MTC_SetMargins(uint32 uSensor, uint32 uTh1Margin, uint32 uTh2Margin)
{
   HWIO_OUTFI(MPM2_TSENS_MTC_Sn_TH_MARGINS, uSensor, TH1_MARGIN, uTh1Margin);
   HWIO_OUTFI(MPM2_TSENS_MTC_Sn_TH_MARGINS, uSensor, TH2_MARGIN, uTh2Margin);
}
/* ============================================================================
**
**  HAL_tsens_MTC_ConfigZoneSwMask
**
**  Description:
**    Configures the specified MTC zone SW Mask value
**
**  Parameters:
**    uZone               Zone to be configured.
**    bTH1_Enable         TRUE/FALSE
**    bTH2_Enable         TRUE/FALSE
**  Return: None
**
**  Dependencies: None
**
** ========================================================================= */
void HAL_tsens_MTC_ConfigZoneSwMask(uint32 uZone, boolean bTH1_Enable, boolean bTH2_Enable)
{
   if (bTH1_Enable == TRUE)
   {
      HWIO_OUTFI(MPM2_TSENS_MTC_ZONEn_SW_MASK, uZone, TH1_MTC_IN_EFFECT, 0x1);
   }
   else
   {
      HWIO_OUTFI(MPM2_TSENS_MTC_ZONEn_SW_MASK, uZone, TH1_MTC_IN_EFFECT, 0x0);
   }
   if (bTH2_Enable == TRUE)
   {
      HWIO_OUTFI(MPM2_TSENS_MTC_ZONEn_SW_MASK, uZone, TH2_MTC_IN_EFFECT, 0x1);
   }
   else
   {
      HWIO_OUTFI(MPM2_TSENS_MTC_ZONEn_SW_MASK, uZone, TH2_MTC_IN_EFFECT, 0x0);
   }
}

/*----------------------------------------------------------------------------
 * Char data
 * -------------------------------------------------------------------------*/
/* ============================================================================
**
**  HAL_tsens_UseRedundant
**
**  Description:
**    Determines whether or not to use the redundant fuses
**
**  Parameters:
**    None
**
**  Return: TRUE: use redundant char data
**
**  Dependencies: None
**
** ========================================================================= */
boolean HAL_tsens_UseRedundant(void)
{
   return FALSE;
}

/* ============================================================================
**
**  HAL_tsens_CalSelect
**
**  Description:
**    Determines whether there is 1) no cal data 2) single point cal data or
**    3) two point cal data
**
**  Parameters:
**    bUseRedundant
**
**  Return: HAL_tsens_Calibration (not calibrated, one point, or two point)
**
**  Dependencies: None
**
** ========================================================================= */
HAL_tsens_Calibration HAL_tsens_CalSelect(boolean bUseRedundant)
{
   return (HAL_tsens_Calibration) HWIO_INF(QFPROM_CORR_CALIB_ROW0_MSB, TSENS_CAL_SEL);
}

/* ============================================================================
**
**  HAL_tsens_GetBaseX1
**
**  Description:
**    Gets the Tsens base for cal point 1
**
**  Parameters:
**    bUseRedundant
**
**  Return: adc code
**
**  Dependencies: None
**
** ========================================================================= */
uint32 HAL_tsens_GetBaseX1(boolean bUseRedundant)
{
   return (HWIO_INF(QFPROM_CORR_CALIB_ROW0_MSB, TSENS_BASE0));
}

/* ============================================================================
**
**  HAL_tsens_GetBaseX2
**
**  Description:
**    Gets the Tsens base for cal point 2
**
**  Parameters:
**    bUseRedundant
**
**  Return: adc code
**
**  Dependencies: None
**
** ========================================================================= */
uint32 HAL_tsens_GetBaseX2(boolean bUseRedundant)
{
   return (HWIO_INF(QFPROM_CORR_CALIB_ROW0_MSB, TSENS_BASE1)); 
}

/* ============================================================================
**
**  HAL_tsens_GetPointX1
**
**  Description:
**    Gets Tsens uSensor point 1
**
**  Parameters:
**    bUseRedundant
**    uint32 uSensor
**
**  Return: adc code
**
**  Dependencies: None
**
** ========================================================================= */
uint32 HAL_tsens_GetPointX1(boolean bUseRedundant, uint32 uSensor)
{
   if (bUseRedundant == FALSE)
   {
      switch (uSensor)
      {
         case 0:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, TSENS0_OFFSET);
         case 1:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, TSENS1_OFFSET);
         case 2:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, TSENS2_OFFSET);
         case 3:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, TSENS3_OFFSET);
         case 4:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, TSENS4_OFFSET);
         case 5:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, TSENS5_OFFSET);
         case 6:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, TSENS6_OFFSET);
         case 7:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_LSB, TSENS7_OFFSET);
         case 8:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_MSB, TSENS8_OFFSET);
         case 9:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_MSB, TSENS9_OFFSET);
         case 10:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_MSB, TSENS10_OFFSET);
         case 11:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_MSB, TSENS11_OFFSET);
         case 12:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_MSB, TSENS12_OFFSET);
         case 13:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW1_MSB, TSENS13_OFFSET);
         case 14:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW0_LSB, TSENS14_OFFSET);
         case 15:
            return HWIO_INF(QFPROM_CORR_CALIB_ROW0_LSB, TSENS15_OFFSET);
         default:
            break;
      }
   }
   return 0; /* return 0 for sensor>TS15*/
}

/* ============================================================================
**
**  HAL_tsens_GetPointX2
**
**  Description:
**    Gets Tsens uSensor point 2
**
**  Parameters:
**    bUseRedundant
**    uint32 uSensor
**
**  Return: adc code
**
**  Dependencies: None
**
** ========================================================================= */
uint32 HAL_tsens_GetPointX2(boolean bUseRedundant, uint32 uSensor)
{
   return 0; /* return 0 for sensors as offset2 doesn't exist for 2.x TSens controllers */
}
