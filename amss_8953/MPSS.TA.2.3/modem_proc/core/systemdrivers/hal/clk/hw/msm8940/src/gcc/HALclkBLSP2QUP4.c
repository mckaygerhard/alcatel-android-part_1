/*
==============================================================================

FILE:         HALclkBLSP2QUP4.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   BLSP2 QUP4 clocks.

   List of clock domains:
   -HAL_clk_mGCCBLSP2QUP4I2CClkDomain
   -HAL_clk_mGCCBLSP2QUP4SPIClkDomain


==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.9.1/systemdrivers/hal/clk/hw/msm8940/src/gcc/HALclkBLSP2QUP4.c#1 $

when          who     what, where, why
----------    ---     --------------------------------------------------------
11/03/2014            Auto-generated.

==============================================================================
            Copyright (c) 2014 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mGCCClockDomainControl;


/* ============================================================================
**    Data
** ==========================================================================*/

                                    
/*                           
 *  HAL_clk_mBLSP2QUP4I2CClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mBLSP2QUP4I2CClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_blsp2_qup4_i2c_apps_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_BLSP2_QUP4_I2C_APPS_CBCR), HWIO_OFFS(GCC_BLSP2_QUP4_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_BLSP2_QUP4_I2C_APPS_CLK
  },
};


/*
 * HAL_clk_mGCCBLSP2QUP4I2CClkDomain
 *
 * BLSP2 QUP4 I2C clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP4I2CClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_BLSP2_QUP4_I2C_APPS_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mBLSP2QUP4I2CClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mBLSP2QUP4I2CClkDomainClks)/sizeof(HAL_clk_mBLSP2QUP4I2CClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


                                    
/*                           
 *  HAL_clk_mBLSP2QUP4SPIClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mBLSP2QUP4SPIClkDomainClks[] =
{
  {
    /* .szClockName      = */ "gcc_blsp2_qup4_spi_apps_clk",
    /* .mRegisters       = */ { HWIO_OFFS(GCC_BLSP2_QUP4_SPI_APPS_CBCR), HWIO_OFFS(GCC_BLSP2_QUP4_BCR), {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_GCC_TEST_GCC_BLSP2_QUP4_SPI_APPS_CLK
  },
};


/*
 * HAL_clk_mGCCBLSP2QUP4SPIClkDomain
 *
 * BLSP2 QUP4 SPI clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mGCCBLSP2QUP4SPIClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(GCC_BLSP2_QUP4_SPI_APPS_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mBLSP2QUP4SPIClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mBLSP2QUP4SPIClkDomainClks)/sizeof(HAL_clk_mBLSP2QUP4SPIClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mGCCClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


