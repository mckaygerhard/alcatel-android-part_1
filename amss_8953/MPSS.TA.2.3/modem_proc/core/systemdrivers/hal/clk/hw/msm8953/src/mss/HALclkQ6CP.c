/*
==============================================================================

FILE:         HALclkQ6.c

DESCRIPTION:
   This file contains the clock HAL code for the QDSP6 core clock.


==============================================================================

                             Edit History

$Header: //components/rel/core.mpss/3.9.1/systemdrivers/hal/clk/hw/msm8953/src/mss/HALclkQ6CP.c#1 $

when          who     what, where, why
--------      ---     ----------------------------------------------------------- 
04/12/2013    ll      Branched from 8974.
06/14/2012    frv     Added clock-as-source control for the Q6 MUX.
02/16/2012    vs      Created

==============================================================================
            Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/

static void    HAL_clk_Q6CPCoreClkEnable    ( HAL_clk_ClockDescType            *pmClockDesc );
static void    HAL_clk_Q6CPCoreClkDisable   ( HAL_clk_ClockDescType            *pmClockDesc );
static boolean HAL_clk_Q6CPCoreClkIsEnabled    ( HAL_clk_ClockDescType  *pmClockDesc );
static void    HAL_clk_Q6CPCoreClkConfig    ( HAL_clk_ClockDescType            *pmClockDesc, 
                                              HAL_clk_ClockConfigType           eConfig  );
static void    HAL_clk_Q6CPConfigMux        ( HAL_clk_ClockDomainDescType      *pmClockDomainDesc,
                                              const HAL_clk_ClockMuxConfigType *pmConfig );


/* ============================================================================
**    Externs and macros
** ==========================================================================*/

#define ROOT_EN_TIMEOUT_US 50

/* ============================================================================
**    Data
** ==========================================================================*/

/*
 * aQ6SourceMap
 *
 * Q6 HW source mapping
 *
 * NOTES:
 * - HAL_clk_SourceMapType is an array of mapped sources
 *   - see HALclkInternal.h.
 *
 * - If source index is reserved/not used in a clock diagram, please tie that
 *   to HAL_CLK_SOURCE_GROUND.
 *
 * - {HAL_CLK_SOURCE_NULL, HAL_CLK_SOURCE_INDEX_INVALID} is used to indicate
 *   the end of the mapping array. If we reach this element during our lookup,
 *   we'll know we could not find the matching source enum for the register
 *   value, or vice versa.
 *
 */
static HAL_clk_SourceMapType aQ6CPSourceMap[] =
{
  {HAL_CLK_SOURCE_XO,                    0},
  {HAL_CLK_SOURCE_MPLL2,                 1},
  {HAL_CLK_SOURCE_PLLTEST,               7},
  {HAL_CLK_SOURCE_NULL,                  HAL_CLK_SOURCE_INDEX_INVALID}
};


/*
 * HAL_clk_mQ6ClockDomainControl
 *
 * Functions for controlling Q6 CP clock domains
 */
HAL_clk_ClockDomainControlType HAL_clk_mQ6CPClockDomainControl =
{
   /* .ConfigMux          = */ HAL_clk_Q6CPConfigMux,
   /* .DetectMuxConfig    = */ HAL_clk_GenericDetectMuxConfig,
   /* .pmSourceMap        = */ aQ6CPSourceMap
};
                                 
   
/*
 * HAL_clk_mQ6ClockControl
 *
 * Functions for controlling Q6 CP clock functions.
 */
HAL_clk_ClockControlType HAL_clk_mQ6CPClockControl =
{
  /* .Enable           = */ HAL_clk_Q6CPCoreClkEnable,
  /* .Disable          = */ HAL_clk_Q6CPCoreClkDisable,
  /* .IsEnabled        = */ HAL_clk_Q6CPCoreClkIsEnabled,
  /* .IsOn             = */ HAL_clk_Q6CPCoreClkIsEnabled,
  /* .Reset            = */ NULL,
  /* .IsReset          = */ NULL,
  /* .Config           = */ HAL_clk_Q6CPCoreClkConfig,
  /* .DetectConfig     = */ NULL,
  /* .ConfigDivider    = */ HAL_clk_GenericConfigDivider,
  /* .DetectDivider    = */ NULL,
  /* .ConfigFootswitch = */ NULL,
};   
   
                                    
/*                           
 *  HAL_clk_mSDCC1APPSClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mQ6CPCoreClkDomainClks[] =
{
  {
    /* .szClockName      = */ "clk_q6_cp",
    /* .mRegisters       = */ { 0, 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mQ6CPClockControl,
    /* .nTestClock       = */ HAL_CLK_MSS_TEST_CLK_Q6_CP
  },
};


/*
 * HAL_clk_mMSSQ6ClkDomain
 *
 * Q6 clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mMSSQ6CPClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(MSS_CP_QDSP6SS_CORE_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mQ6CPCoreClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mQ6CPCoreClkDomainClks)/sizeof(HAL_clk_mQ6CPCoreClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mQ6CPClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};


/*============================================================================

               FUNCTION DEFINITIONS FOR MODULE

============================================================================*/

/* ===========================================================================
**  HAL_clk_Q6CoreClkEnable
**
** ======================================================================== */

void HAL_clk_Q6CPCoreClkEnable
( 
  HAL_clk_ClockDescType  *pmClockDesc 
)
{
  /* 
   * Need to assert CLK_ENA in MSS_CP_QDSP6SS_GFMUX_CTL register to enable clock
   * to vector Q6. POR value is 0. 
   */
   
   HWIO_OUTF(MSS_CP_QDSP6SS_GFMUX_CTL, CLK_ENA, 0x1);

}  /* END HAL_clk_Q6CoreClkEnable */

/* ===========================================================================
**  HAL_clk_Q6CoreClkIsEnabled
**
** ======================================================================== */

boolean HAL_clk_Q6CPCoreClkIsEnabled
( 
  HAL_clk_ClockDescType  *pmClockDesc 
)
{
  /* 
   *While enabling Vector Q6 core clock for first time, ROOT_OFF bit will not toggle
   *as the processor is not taken out of reset. Due to this Enable Clock API will poll
   *for timeout and throw warning. To avoid this, modified the logic to poll on CLK_ENA
   *bit in MSS_CP_QDSP6SS_GFMUX_CTL register.
   */ 

  if(HWIO_INF(MSS_CP_QDSP6SS_GFMUX_CTL, CLK_ENA))
  {
  return TRUE;
  }
  else
  {
    return FALSE;
  }

}  /* END HAL_clk_Q6CoreClkIsEnabled */

/* ===========================================================================
**  HAL_clk_Q6CoreClkDisable
**
** ======================================================================== */

void HAL_clk_Q6CPCoreClkDisable
( 
  HAL_clk_ClockDescType  *pmClockDesc
)
{
  /* 
   *Need to de-assert CLK_ENA in MSS_CP_QDSP6SS_GFMUX_CTL register to disable clock
   *to vector Q6 and ROOT_OFF is set to 1. This is required as next Enable Clock 
   *request will poll on ROOT_OFF bit in RCGR as part of IsClockOn API and returns
   *if ROOT_OFF is already 0.
   */

   HWIO_OUTF(MSS_CP_QDSP6SS_GFMUX_CTL, CLK_ENA, 0x0);

}  /* END HAL_clk_Q6CoreClkDisable */

/* ===========================================================================
**  HAL_clk_Q6CoreClkConfig
**
** ======================================================================== */

void HAL_clk_Q6CPCoreClkConfig
( 
  HAL_clk_ClockDescType   *pmClockDesc, 
  HAL_clk_ClockConfigType eConfig 
)
{
  uint32 nSrcSel;
  
  switch (eConfig)
  {
    case HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX:
      nSrcSel = 0x0;
      break;

    case HAL_CLK_CONFIG_Q6SS_CORE_PLL_MAIN:
      nSrcSel = 0x1;
      break;

  
    default:
      return;
  }

  HWIO_OUTF(MSS_CP_QDSP6SS_GFMUX_CTL, CLK_SRC_SEL, nSrcSel);

}  /* END HAL_clk_Q6CPCoreClkConfig */

/* ===========================================================================
**  HAL_clk_Q6CPConfigMux
**
** ======================================================================== */

void HAL_clk_Q6CPConfigMux
(
  HAL_clk_ClockDomainDescType      *pmClockDomainDesc,
  const HAL_clk_ClockMuxConfigType *pmConfig
)
{
  uint32                 nCmdCGRAddr, nCmdCGRVal;
  uint32                 nCfgCGRAddr, nCfgCGRVal;
  uint32                 nMAddr, nNAddr, nDAddr;
  uint32                 nSourceIndex = 0;
  uint32                 timeout = 0;
  HAL_clk_SourceMapType *pmSourceMap;


  /*
   * Sanity check
   */
  if((pmConfig                                    == NULL) ||
     (pmClockDomainDesc                           == NULL) ||
     (pmClockDomainDesc->nCGRAddr                 == 0   ) || 
     (pmClockDomainDesc->pmControl                == NULL) ||
     (pmClockDomainDesc->pmControl->pmSourceMap   == NULL) )
  {
    return;
  }

  /*
   * Get current CMD and CFG register values
   */
  nCmdCGRAddr = pmClockDomainDesc->nCGRAddr;
  nCmdCGRVal  = inpdw(nCmdCGRAddr);
  nCfgCGRAddr = pmClockDomainDesc->nCGRAddr + HAL_CLK_CFG_REG_OFFSET;
  nCfgCGRVal  = inpdw(nCfgCGRAddr);

  /* SET IDLE_CORE_CLK_EN in GFMUX_CTL */
  //HWIO_OUTF(MSS_CP_QDSP6SS_GFMUX_CTL, IDLE_CORE_CLK_EN, 0x1);
  
  /*
   * Set ROOT_EN on for prevent glitch during switching since Q6 clock might have gated-off.
   */
   nCmdCGRVal |= HAL_CLK_CMD_CGR_ROOT_EN_FMSK;
  
  outpdw(nCmdCGRAddr, nCmdCGRVal);
  /*Wait 50us for ROOT_OFF to toggle */
  while((inpdw(nCmdCGRAddr) & HAL_CLK_CMD_CGR_ROOT_OFF_FMSK) &&
         (timeout < ROOT_EN_TIMEOUT_US))
  {
    HAL_clk_BusyWait(10);
    timeout += 10;
  }
  
  /*
   * Clear the fields
   */
  nCfgCGRVal &= ~(HAL_CLK_CFG_CGR_SRC_SEL_FMSK |
                  HAL_CLK_CFG_CGR_SRC_DIV_FMSK |
                  HAL_CLK_CFG_CGR_MODE_FMSK);

  /*
   * Get source index from source enum
   */
  pmSourceMap = pmClockDomainDesc->pmControl->pmSourceMap;
  nSourceIndex = HAL_clk_GenericSourceMapToHW(pmSourceMap, pmConfig->eSource);

  /*
   * Bail if could not find matching source index
   */
  if (nSourceIndex == HAL_CLK_SOURCE_INDEX_INVALID)
  {
    return;
  }

  /*
   * Program the source and divider values.
   */
  nCfgCGRVal |= ((nSourceIndex << HAL_CLK_CFG_CGR_SRC_SEL_SHFT) & HAL_CLK_CFG_CGR_SRC_SEL_FMSK);
  nCfgCGRVal |= ((HALF_DIVIDER(pmConfig) << HAL_CLK_CFG_CGR_SRC_DIV_SHFT) 
                  & HAL_CLK_CFG_CGR_SRC_DIV_FMSK);

  /*
   * Set MND counter mode depending on if it is in use
   */
  if (pmConfig->nM != 0 && (pmConfig->nM < pmConfig->nN))
  {    
    /*
     * Get M, N and D addresses
     */
    nMAddr = pmClockDomainDesc->nCGRAddr + HAL_CLK_M_REG_OFFSET;
    nNAddr = pmClockDomainDesc->nCGRAddr + HAL_CLK_N_REG_OFFSET;
    nDAddr = pmClockDomainDesc->nCGRAddr + HAL_CLK_D_REG_OFFSET;

    /*
     * Set M value
     */
    outpdw(nMAddr, pmConfig->nM);

    /*
     * Set N value
     */
    outpdw(nNAddr, NOT_N_MINUS_M(pmConfig));

    /*
     * Set D value
     */
    outpdw(nDAddr, NOT_2D(pmConfig));

    /*
     * Dual-edge mode
     */
    nCfgCGRVal |= ((HAL_CLK_CFG_CFG_DUAL_EDGE_MODE_VAL << HAL_CLK_CFG_CGR_MODE_SHFT) 
                    & HAL_CLK_CFG_CGR_MODE_FMSK);
  }

  /*
   * Write the final CFG register value
   */
  outpdw(nCfgCGRAddr, nCfgCGRVal);

  /*
   * Trigger the update
   */
  nCmdCGRVal |= HAL_CLK_CMD_CFG_UPDATE_FMSK;
  outpdw(nCmdCGRAddr, nCmdCGRVal);

  /*
   * Wait until update finishes
   */
  while(inpdw(nCmdCGRAddr) & HAL_CLK_CMD_CFG_UPDATE_FMSK);
  
  /*
   * Update success, clear ROOT_EN
   */
  nCmdCGRVal = inpdw(nCmdCGRAddr);
  nCmdCGRVal &= ~HAL_CLK_CMD_CGR_ROOT_EN_FMSK;

  outpdw(nCmdCGRAddr, nCmdCGRVal);
  
  
  /* Update Success, clear IDLE_CORE_CLK_EN in GFMUX_CTL */
  //HWIO_OUTF(MSS_CP_QDSP6SS_GFMUX_CTL, IDLE_CORE_CLK_EN, 0x0);
  
} /* HAL_clk_Q6CPConfigMux */
