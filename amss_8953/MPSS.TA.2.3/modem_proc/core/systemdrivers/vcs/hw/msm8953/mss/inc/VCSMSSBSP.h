#ifndef __VCSMSSBSP_H__
#define __VCSMSSBSP_H__
/*
===========================================================================
*/
/**
  @file VCSMSSBSP.h 
  
  Header description for the MSS VCS driver BSP format.
*/
/*  
  ====================================================================

  Copyright (c) 2015 QUALCOMM Technologies Incorporated. All Rights Reserved.  
  QUALCOMM Proprietary and Confidential. 

  ==================================================================== 
  $Header: //components/rel/core.mpss/3.9.1/systemdrivers/vcs/hw/msm8953/mss/inc/VCSMSSBSP.h#1 $
  $DateTime: 2015/09/29 21:52:15 $
  $Author: pwbldsvc $
 
  when       who     what, where, why
  --------   ---     -------------------------------------------------
  01/22/14   lil     Created.
 
  ====================================================================
*/ 


/*=========================================================================
      Include Files
==========================================================================*/


#include "VCSBSP.h"


/*=========================================================================
      Macro Definitions
==========================================================================*/


/*=========================================================================
      Type Definitions
==========================================================================*/

#if 0
/*
 * VCSImageBSPConfigType
 *
 * BSP data structure for describing the image configuration.
 *
 *  bEnableMVC - MVC enable switch.
 */
typedef struct VCSImageBSPConfigType
{
  boolean bEnableMVC;
} VCSImageBSPConfigType;
#endif

#endif  /* __VCSMSSBSP_H__ */ 

