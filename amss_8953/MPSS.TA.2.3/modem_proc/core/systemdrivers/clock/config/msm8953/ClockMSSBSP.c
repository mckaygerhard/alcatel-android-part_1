/*
==============================================================================

FILE:         ClockMSSBSP.c 

DESCRIPTION:
  This file contains clock bsp data for the DAL based driver.


==============================================================================

$Header: //components/rel/core.mpss/3.9.1/systemdrivers/clock/config/msm8953/ClockMSSBSP.c#1 $

==============================================================================
            Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockMSSBSP.h"
#include "comdef.h"
#include "VCSDefs.h"

/*=========================================================================
      Data Declarations
==========================================================================*/


/*
 * Enumeration of scalar QDSP6 performance levels.
 */
enum
{
  CLOCK_QDSP6_PERF_LEVEL_0,
  CLOCK_QDSP6_PERF_LEVEL_1,
  CLOCK_QDSP6_PERF_LEVEL_2,
  CLOCK_QDSP6_PERF_LEVEL_3,
  CLOCK_QDSP6_PERF_LEVEL_4,
  CLOCK_QDSP6_PERF_LEVEL_5,
  CLOCK_QDSP6_PERF_LEVEL_6,
  CLOCK_QDSP6_PERF_LEVEL_7,
  CLOCK_QDSP6_PERF_LEVEL_8,
  CLOCK_QDSP6_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of scalar QDSP6 configurations.
 */
enum
{
  CLOCK_QDSP6_CONFIG_19P2MHz,
  CLOCK_QDSP6_CONFIG_115P2MHz,
  CLOCK_QDSP6_CONFIG_230P4MHz,
  CLOCK_QDSP6_CONFIG_326P4MHz,
  CLOCK_QDSP6_CONFIG_499P2MHz,
  CLOCK_QDSP6_CONFIG_556P8MHz,
  CLOCK_QDSP6_CONFIG_691P2MHz,
  CLOCK_QDSP6_CONFIG_748P8MHz,
  CLOCK_QDSP6_CONFIG_844P8MHz,
  CLOCK_QDSP6_CONFIG_TOTAL
};


/*
 * Scalar QDSP6 performance levels mappings
 */
static uint32 Clock_CPUPerfLevels [] =
{
  CLOCK_QDSP6_CONFIG_19P2MHz,
  CLOCK_QDSP6_CONFIG_115P2MHz,
  CLOCK_QDSP6_CONFIG_230P4MHz,
  CLOCK_QDSP6_CONFIG_326P4MHz,
  CLOCK_QDSP6_CONFIG_499P2MHz,
  CLOCK_QDSP6_CONFIG_556P8MHz,
  CLOCK_QDSP6_CONFIG_691P2MHz,
  CLOCK_QDSP6_CONFIG_748P8MHz,
  CLOCK_QDSP6_CONFIG_844P8MHz,
};


/*
 * Enumeration of vector QDSP6 performance levels.
 */
enum
{
  CLOCK_QDSP6CP_PERF_LEVEL_0,
  CLOCK_QDSP6CP_PERF_LEVEL_1,
  CLOCK_QDSP6CP_PERF_LEVEL_2,
  CLOCK_QDSP6CP_PERF_LEVEL_3,
  CLOCK_QDSP6CP_PERF_LEVEL_4,
  CLOCK_QDSP6CP_PERF_LEVEL_5,
  CLOCK_QDSP6CP_PERF_LEVEL_6,
  CLOCK_QDSP6CP_PERF_LEVEL_7,
  CLOCK_QDSP6CP_PERF_LEVEL_8,
  CLOCK_QDSP6CP_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of vector QDSP6 configurations.
 */
enum
{
  CLOCK_QDSP6CP_CONFIG_19P2MHz,
  CLOCK_QDSP6CP_CONFIG_144P0MHz,
  CLOCK_QDSP6CP_CONFIG_288P0MHz,
  CLOCK_QDSP6CP_CONFIG_326P4MHz,
  CLOCK_QDSP6CP_CONFIG_460P8MHz,
  CLOCK_QDSP6CP_CONFIG_518P4MHz,
  CLOCK_QDSP6CP_CONFIG_652P8MHz,
  CLOCK_QDSP6CP_CONFIG_710P4MHz,
  CLOCK_QDSP6CP_CONFIG_806P4MHz,
  CLOCK_QDSP6CP_CONFIG_TOTAL
};


/*
 * Vector QDSP6 performance levels mappings
 */
static uint32 Clock_CPU1PerfLevels [] =
{
  CLOCK_QDSP6CP_CONFIG_19P2MHz,
  CLOCK_QDSP6CP_CONFIG_144P0MHz,
  CLOCK_QDSP6CP_CONFIG_288P0MHz,
  CLOCK_QDSP6CP_CONFIG_326P4MHz,
  CLOCK_QDSP6CP_CONFIG_460P8MHz,
  CLOCK_QDSP6CP_CONFIG_518P4MHz,
  CLOCK_QDSP6CP_CONFIG_652P8MHz,
  CLOCK_QDSP6CP_CONFIG_710P4MHz,
  CLOCK_QDSP6CP_CONFIG_806P4MHz,
};


/*
 * Enumeration of MSS config bus performance levels.
 */
enum
{
  CLOCK_CONFIG_BUS_PERF_LEVEL_0,
  CLOCK_CONFIG_BUS_PERF_LEVEL_1,
  CLOCK_CONFIG_BUS_PERF_LEVEL_2,
  CLOCK_CONFIG_BUS_PERF_LEVEL_TOTAL
};


/*
 * Enumeration of MSS config bus configurations.
 */
enum
{
  CLOCK_CONFIG_BUS_CONFIG_XO,
  CLOCK_CONFIG_BUS_CONFIG_72MHz,
  CLOCK_CONFIG_BUS_CONFIG_144MHz,
  CLOCK_CONFIG_BUS_CONFIG_TOTAL
};


/*
 * MSS config bus performance levels
 */
static uint32 Clock_ConfigBusPerfLevels [] =
{
  CLOCK_CONFIG_BUS_CONFIG_XO,
  CLOCK_CONFIG_BUS_CONFIG_72MHz,
  CLOCK_CONFIG_BUS_CONFIG_144MHz,
};


/*=========================================================================
      Data
==========================================================================*/


/*
 * The MPLL0 source frequency configurations are defined in ClockBSP.c
 */
extern ClockSourceFreqConfigType SourceFreqConfig_MPLL0[6];


/*
 * The MPLL2 source frequency configurations are defined in ClockBSP.c
 */
extern ClockSourceFreqConfigType SourceFreqConfig_MPLL2[6];


/*
 * Scalar QDSP6 clock configurations
 */
static ClockCPUConfigType Clock_CPUConfig[] =
{
  /* NOTE: Divider value is (2*Div) due to potential use of fractional values */
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 19200000,
      .HALConfig         = { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = NULL
    },
    .eCornerMSS      = VCS_CORNER_LOW_MINUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 115200000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL0, 12, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL0[3] // 691.2 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW_MINUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 230400000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL0, 6, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL0[3] // 691.2 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW_MINUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 326400000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL0, 4, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL0[2] // 652.8 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW_MINUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 499200000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL0, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL0[0] // 499.2 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 556800000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL0, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL0[1] // 556.8 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW_PLUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 691200000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL0, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL0[3] // 691.2 MHz
    },
    .eCornerMSS      = VCS_CORNER_NOMINAL,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 748800000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL0, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL0[4] // 748.8 MHz
    },
    .eCornerMSS      = VCS_CORNER_NOMINAL_PLUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 844800000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL0, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL0[5] // 844.8 MHz
    },
    .eCornerMSS      = VCS_CORNER_TURBO,
  },
  { 0 }
};


/*
 * Vector QDSP6 clock configurations
 */
static ClockCPUConfigType Clock_CPU1Config[] =
{
  /* NOTE: Divider value is (2*Div) due to potential use of fractional values */
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 19200000,
      .HALConfig         = { HAL_CLK_SOURCE_XO, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = NULL
    },
    .eCornerMSS      = VCS_CORNER_LOW_MINUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 144000000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL2, 8, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL2[1] // 576.0 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW_MINUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 288000000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL2, 4, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL2[1] // 576.0 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW_MINUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 326400000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL2, 4, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL2[2] // 652.8 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 460800000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL2, 4, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL2[5] // 921.6 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 518400000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL2, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL2[0] // 518.4 MHz
    },
    .eCornerMSS      = VCS_CORNER_LOW_PLUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 652800000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL2, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL2[2] // 652.8 MHz
    },
    .eCornerMSS      = VCS_CORNER_NOMINAL,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 710400000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL2, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL2[3] // 710.4 MHz
    },
    .eCornerMSS      = VCS_CORNER_NOMINAL_PLUS,
  },
  {
    .CoreConfig      = HAL_CLK_CONFIG_Q6SS_CORE_CLOCK_MUX,
    .Mux             =
    {
      .nFreqHz           = 806400000,
      .HALConfig         = { HAL_CLK_SOURCE_MPLL2, 2, 0, 0, 0 },
      .HWVersion         = { {0x0, 0x0}, {0xFF, 0xFF} },
      .pSourceFreqConfig = &SourceFreqConfig_MPLL2[4] // 806.4 MHz
    },
    .eCornerMSS      = VCS_CORNER_TURBO,
  },
  { 0 }
};


static ClockConfigBusConfigType Clock_ConfigBusConfig[] =
{
  /*  NOTE: Divider value is (2*iv) due to potential use of fractional values */
  {
    /* .Mux */   { 19200 * 1000, { HAL_CLK_SOURCE_XO, 2 }, VCS_CORNER_LOW_MINUS }
  },
  {
    /* .Mux */   { 72000 * 1000, { HAL_CLK_SOURCE_MPLL1_OUT_EARLY_DIV2, 16 }, VCS_CORNER_LOW_MINUS }
  },
  {
    /* .Mux */   { 144000 * 1000, { HAL_CLK_SOURCE_MPLL1_OUT_EARLY_DIV2, 8 }, VCS_CORNER_LOW }
  }
};


/*
 * Performance level configuration data for the MSS Config Bus.
 */
static ClockConfigBusPerfConfigType Clock_ConfigBusPerfConfig =
{
  .nMinPerfLevel = CLOCK_CONFIG_BUS_PERF_LEVEL_0,
  .nMaxPerfLevel = CLOCK_CONFIG_BUS_PERF_LEVEL_1,
  .anPerfLevel   = Clock_ConfigBusPerfLevels
};


/*
 * Performance level configuration data for the CPU clock.
 */
static ClockCPUPerfConfigType Clock_CPUPerfConfig[] =
{
  {
    .HWVersion      = {{0x0, 0x0}, {0xFF, 0xFF}},
    .nMinPerfLevel  = CLOCK_QDSP6_PERF_LEVEL_1,
    .nMaxPerfLevel  = CLOCK_QDSP6_PERF_LEVEL_8,
    .anPerfLevel    = Clock_CPUPerfLevels,
    .nNumPerfLevels = ARR_SIZE(Clock_CPUPerfLevels)
  }
};

/*
 * Performance level configuration data for the CPU clock.
 */
static ClockCPUPerfConfigType Clock_CPU1PerfConfig[] =
{
  {
    .HWVersion      = {{0x0, 0x0}, {0xFF, 0xFF}},
    .nMinPerfLevel  = CLOCK_QDSP6_PERF_LEVEL_1,
    .nMaxPerfLevel  = CLOCK_QDSP6_PERF_LEVEL_8,
    .anPerfLevel    = Clock_CPU1PerfLevels,
    .nNumPerfLevels = ARR_SIZE(Clock_CPU1PerfLevels)
  }
};


/*
 * Image BSP data
 */
const ClockImageBSPConfigType ClockImageBSPConfig =
{
  .bEnableDCS               = TRUE,
  .pCPUConfig               = Clock_CPUConfig,
  .pCPU1Config              = Clock_CPU1Config,
  .pCPUPerfConfig           = Clock_CPUPerfConfig,
  .pCPU1PerfConfig          = Clock_CPU1PerfConfig,
  .nNumCPUPerfLevelConfigs  = ARR_SIZE(Clock_CPUPerfConfig),
  .nNumCPU1PerfLevelConfigs = ARR_SIZE(Clock_CPU1PerfConfig),
  .pConfigBusConfig         = Clock_ConfigBusConfig,
  .pConfigBusPerfConfig     = &Clock_ConfigBusPerfConfig
};
