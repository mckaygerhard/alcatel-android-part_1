/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include "mmpm.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "adsppm_test.h"
#include "adsppm_test_param.h"
#include "adsppm_test_utils.h"
#include "adsppm_test_pwr.h"


void createClientName(char clientName[], MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId);



/* Get test parameter and issue the call */
MMPM_STATUS InvokeTest(AdsppmTestType test[], uint32 numTest, MMPM_STATUS testSts[][MAX_TEST_SEQUENCE])
{
    MMPM_STATUS  sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    ClkTestType clk[MAX_CLK_REQUEST];
    ClkDomainTestType clkDomain[MAX_CLK_REQUEST];
    BwReqTestType bw[MAX_BW_REQUEST];
    uint32 i, j, numClks, mips, milliVol, numBw, microSec, clkId, match, memType, powerState;
    uint32 coreId, instanceId, relClkId[MAX_CLK_REQUEST];
    uint32 clientId[MAX_TEST_CLIENT];

    memset(clientId, 0,  sizeof(clientId));
    
    for (i = 0; i < numTest;i++)
    {
        coreId = test[i].coreId;
        instanceId = test[i].instanceId;
        for(j = 0;j < test[i].numTestSeq; j++)
        {
            
            switch (test[i].testSeq[j])
            {
            case REG:
            {
                clientId[test[i].clientNum] = Test_Register(coreId, instanceId);
                if (clientId[test[i].clientNum])
                {
                    sts = MMPM_STATUS_SUCCESS;
                }
            }
                break;
			case SETPARAM:
				break;
			case REQMEMPWR:
				{
					GetReqMemPwrParam(coreId, instanceId, &memType, &powerState);
					sts = Test_RequestMemPwr(clientId[test[i].clientNum], &memType, &powerState);
				}
				break;

            case REQCLK:
            {
               // Test
                GetReqClkParam(coreId, instanceId, clk, &numClks);
                sts = Test_RequestClk(clientId[test[i].clientNum], clk, numClks);
            }
                break;
            case REQCLKDOMAIN:
            {
               // Test
                GetReqClkDomainParam(coreId, instanceId, clkDomain, &numClks);
                sts = Test_RequestClkDomain(clientId[test[i].clientNum], clkDomain, numClks);
            }
                break;
            case REQMIPS:
                GetReqMipsParam(coreId, instanceId, &mips);
                sts = Test_RequestMips(clientId[test[i].clientNum], mips);
                break;

            case REQREGP:
                GetReqRegProgParam(coreId, instanceId, &match);
                sts = Test_RequestRegProg(clientId[test[i].clientNum], match);
                break;   

            case REQVREG:
                GetReqVregParam(coreId, instanceId, &milliVol);
                sts = Test_RequestVreg(clientId[test[i].clientNum], milliVol);
                break;

            case REQBW:
                GetReqBWParam(coreId, instanceId, bw, &numBw);
                sts = Test_RequestBw(clientId[test[i].clientNum], &bw[0], numBw);
                break;

            case REQSLATENCY:
                GetReqSleepLatencyParam(coreId, instanceId, &microSec);
                sts = Test_RequestSleepLatency(clientId[test[i].clientNum], microSec);
                break;

            case REQPWR:
                   sts = Test_RequestPwr(clientId[test[i].clientNum]);
                break;

			case RELMEMPWR:
				 sts = Test_ReleaseMemPwr(clientId[test[i].clientNum]);
                break;

            case RELCLK:
                GetRelClkParam(coreId, instanceId, relClkId, &numClks);
                sts = Test_ReleaseClk(clientId[test[i].clientNum], relClkId, numClks);
                break;

            case RELCLKDOMAIN:
                GetRelClkParam(coreId, instanceId, relClkId, &numClks);
                sts = Test_ReleaseClkDomain(clientId[test[i].clientNum], relClkId, numClks);
                break;

            case RELMIPS:
                sts = Test_ReleaseMips(clientId[test[i].clientNum]);
                break;

            case RELREGP:
                sts = Test_ReleaseRegProg(clientId[test[i].clientNum]);
                break;

            case RELVREG:
                sts = Test_ReleaseVreg(clientId[test[i].clientNum]);
                break;

            case RELBW:
                sts = Test_ReleaseBw(clientId[test[i].clientNum]);
                break;

            case RELSPATENCY:
                sts = Test_ReleaseSleepLatency(clientId[test[i].clientNum]);
                break;

            case RELPWR:
                sts = Test_ReleasePwr(clientId[test[i].clientNum]);
                break;
			
         
            case INFOCLK:
                GetInfoClkParam(coreId, instanceId, &clkId);
                sts = MMPM_STATUS_SUCCESS;
                break;

            case INFOPWR:
				  sts = MMPM_STATUS_SUCCESS;
                break;

            case THERML:
				 sts = MMPM_STATUS_SUCCESS;
                break;

            case DREG:
            {
                sts = Test_Deregister(clientId[test[i].clientNum]);
                clientId[test[i].clientNum] = 0;
            }
			break;
		 

            default:
                break;
			}
            if (MMPM_STATUS_SUCCESS != sts)
            {
				ADSPPM_TESTLOG_PRINTF_4( ADSPPMTEST_LOG_LEVEL_ERROR,  "ERROR in TestAPI %d, Test#%d TestSeq#%d coreId#%d \n", test[i].testSeq[j], i, j, test[i].coreId );
                testSts[i][j] = sts;
                retSts = sts;
            }
            else
            {
                sts = MMPM_STATUS_FAILED; // reset to failed by default after each test
            }
        }
    }
    return retSts;
}


uint32 testCallbackFunc(MmpmCallbackParamType *pCbParam)
{
	 uint32 ret;

	 ret = ((MmpmCompletionCallbackDataType*)(pCbParam->callbackData))->result;

     ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Async callback func: tag=%d, result=%d\n", ((MmpmCompletionCallbackDataType*)(pCbParam->callbackData))->reqTag, ((MmpmCompletionCallbackDataType*)(pCbParam->callbackData))->result);
	 return 0;
}


/* Test ADSPPM registration */
uint32 Test_Register(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId)
{
    MmpmRegParamType  mmpmRegParam;
    char              clientName[MAX_LEN_CLIENT_NAME];
    uint32            clientId = 0;

    memset(&mmpmRegParam, 0,  sizeof(MmpmRegParamType));
    memset(clientName, 0, sizeof(clientName));
    mmpmRegParam.rev = MMPM_REVISION;
    mmpmRegParam.coreId = coreId;
    mmpmRegParam.instanceId = instanceId;
    mmpmRegParam.MMPM_Callback = testCallbackFunc;
    createClientName(clientName, coreId, instanceId);
    mmpmRegParam.pClientName = clientName;
    mmpmRegParam.pwrCtrlFlag = PWR_CTRL_NONE;
    mmpmRegParam.callBackFlag = CALLBACK_REQUEST_COMPLETE;
    clientId = MMPM_Register_Ext(&mmpmRegParam);
    if (!clientId)
    {
        ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR,  "MMPM register FAILED for coreID %d instanceID %d\n", mmpmRegParam.coreId, mmpmRegParam.instanceId);
    }

    return clientId;
}

/* client name will be in the format ADSPPM_Test_<coreId>_<instanceId> */
__inline void createClientName(char clientName[], MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId)
{
    sprintf(clientName, "ADSPPM_Test_%X_%X",coreId, instanceId);
}





MMPM_STATUS Test_RequestMemPwr(uint32 clientId, uint32 *memType, uint32 *powerState)
{
	 MMPM_STATUS             sts = MMPM_STATUS_FAILED;
     MmpmRscParamType        mmpmRscParam;
	 MmpmRscExtParamType     mmpmRscExtParam;
	 MmpmMemPowerReqParamType memPwrReq;
	 if(clientId)
	 {
		  memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
		  memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));
		  memset(&memPwrReq, 0, sizeof(MmpmMemPowerReqParamType));

		  memPwrReq.memory = *memType;
		  memPwrReq.powerState = *powerState;

          mmpmRscParam.rscId = MMPM_RSC_ID_MEM_POWER;      
		  mmpmRscParam.rscParam.pMemPowerState = &memPwrReq;
          mmpmRscExtParam.numOfReq = 1;
          mmpmRscExtParam.pReqArray = &mmpmRscParam;
          mmpmRscExtParam.pStsArray = &sts;
          sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);
		  if(MMPM_STATUS_SUCCESS != sts)
         {
            // TODO - add details of why it failed.
            ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM mempower Request FAILED for clientId %ld, return %d\n", clientId, sts);
         } else {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "ADSPPM memPower Request Passed for clientId=%ld \n", clientId);
         }

	 }
	 else
	 {
		  // This is an error in the test code.
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");

	 }
	 return sts;

}

/* Test ADSPPM Request core clock */
MMPM_STATUS Test_RequestClk(uint32 clientId, ClkTestType clk[], uint32 numClk)
{
    MMPM_STATUS             sts = MMPM_STATUS_FAILED;
    MmpmRscParamType        mmpmRscParam;
    MmpmClkReqType          mmpmClkReqType;
    MmpmClkValType          mmpmClkValType[MAX_CLK_REQUEST];
    uint32                  i;
    MmpmRscExtParamType     mmpmRscExtParam;

   
    memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
    memset(&mmpmClkValType, 0, MAX_CLK_REQUEST *  sizeof(MmpmClkValType));
    memset(&mmpmClkReqType, 0, sizeof(MmpmClkReqType));
    memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));

    if (clientId)
    {
        for (i = 0; i < numClk; i++)
        {
            mmpmClkValType[i].clkFreqHz = clk[i].freq;
            mmpmClkValType[i].clkId.clkIdLpass = (MmpmClkIdLpassType)clk[i].clkId;
            mmpmClkValType[i].freqMatch = clk[i].freqMatch;

            // Log each clock request as a Info level.
            ADSPPM_TESTLOG_PRINTF_4( ADSPPMTEST_LOG_LEVEL_INFO,  "ADSPPM Clk Request: clock #%ld, ID=%ld, Freq=%ld Hz, Match=%ld.\n",
                i, clk[i].clkId, clk[i].freq, clk[i].freqMatch);
        }
        mmpmClkReqType.numOfClk = numClk; 
        mmpmClkReqType.pClkArray = mmpmClkValType;
        mmpmRscParam.rscId = MMPM_RSC_ID_CORE_CLK;      
        mmpmRscParam.rscParam.pCoreClk = &mmpmClkReqType;
        mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
        mmpmRscExtParam.numOfReq = 1;
        mmpmRscExtParam.pReqArray = &mmpmRscParam;
        mmpmRscExtParam.pStsArray = &sts;

        sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);
        if(MMPM_STATUS_SUCCESS != sts)
        {
            // TODO - add details of why it failed.
            ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Clk Request with Freq FAILED for clientId %ld, return %d\n", clientId, sts);
        } else {
            ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "ADSPPM Clk Request Passed for clientId=%ld, num clocks=%ld\n", clientId, numClk );
        }
    } else
    {
        // This is an error in the test code.
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}


/* Test ADSPPM Request core clock */
MMPM_STATUS Test_RequestClkDomain(uint32 clientId, ClkDomainTestType clk[], uint32 numClk)
{
    MMPM_STATUS             sts = MMPM_STATUS_FAILED;
    MmpmRscParamType        mmpmRscParam;
    MmpmClkDomainReqType    mmpmClkDomainReqType;
    MmpmClkDomainType       mmpmClkDomainValType[MAX_CLK_REQUEST];
    uint32                  i;
    MmpmRscExtParamType     mmpmRscExtParam;

   
    memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
    memset(&mmpmClkDomainValType, 0, MAX_CLK_REQUEST *  sizeof(MmpmClkValType));
    memset(&mmpmClkDomainReqType, 0, sizeof(MmpmClkReqType));
    memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));

    if (clientId)
    {
        for (i = 0; i < numClk; i++)
        {
            mmpmClkDomainValType[i].clkId.clkIdLpass = (MmpmClkIdLpassType)clk[i].clkId;
            mmpmClkDomainValType[i].clkFreqHz = clk[i].clkFreqHz;
            mmpmClkDomainValType[i].clkDomainSrc.clkDomainSrcIdLpass = clk[i].clkDomainSrc;

           // Log each clock request as a Info level.
            ADSPPM_TESTLOG_PRINTF_4( ADSPPMTEST_LOG_LEVEL_INFO,  "ADSPPM ClkDomain Request: clkNUM=%ld, clockID=%ld, clkFreqHz=%ld Hz, clkDomainSrc=%ld.\n",
                i, clk[i].clkId, clk[i].clkFreqHz, clk[i].clkDomainSrc);
        }
        mmpmClkDomainReqType.numOfClk = numClk; 
        mmpmClkDomainReqType.pClkDomainArray = mmpmClkDomainValType;
        mmpmRscParam.rscId = MMPM_RSC_ID_CORE_CLK_DOMAIN;      
        mmpmRscParam.rscParam.pCoreClkDomain = &mmpmClkDomainReqType;
        mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
        mmpmRscExtParam.numOfReq = 1;
        mmpmRscExtParam.pReqArray = &mmpmRscParam;
        mmpmRscExtParam.pStsArray = &sts;

        sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);
        if(MMPM_STATUS_SUCCESS != sts)
        {
            // TODO - add details of why it failed.
            ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Clk Domain Request with Freq FAILED for clientId %ld, return %d\n", clientId, sts);
        } else {
            ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "ADSPPM Clk Domain Request Passed for clientId=%ld, num clocks=%ld\n", clientId, numClk );
        }
    } else
    {
        // This is an error in the test code.
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}



/* Request ADSPPM for register programming (AHB clk) */
MMPM_STATUS Test_RequestRegProg(uint32 clientId, uint32 match)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmRscParamType   mmpmRscParam;
   MmpmRscExtParamType mmpmRscExtParam;

    if (0 != clientId)
    {
      memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
      memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));
      mmpmRscParam.rscId = MMPM_RSC_ID_REG_PROG;
      mmpmRscParam.rscParam.regProgMatch =  match;
      mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
      mmpmRscExtParam.numOfReq = 1;
      mmpmRscExtParam.pReqArray = &mmpmRscParam;
      mmpmRscExtParam.pStsArray = &sts;
      sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);
      if (MMPM_STATUS_SUCCESS != sts)
      {
         ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM clk register program request FAILED for clientId %ld \n", clientId);
      }
    } 
    else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}


/* Test ADSPPM bandwidth request usecase */
MMPM_STATUS Test_RequestBw(uint32 clientId, BwReqTestType *bw, uint32 numBw)
{
    MMPM_STATUS   sts = MMPM_STATUS_FAILED;
    MmpmRscParamType    mmpmRscParam;
    MmpmGenBwReqType    mmpmBwReqParam;
    MmpmGenBwValType    bandWidthArray[MAX_BW_REQUEST];
    uint32 i;
    MmpmRscExtParamType mmpmRscExtParam;
    uint32 clkFreq = 0;
	uint32 mipsValue = 0;
	uint64 bwValue = 0;

    if (0 != clientId)
    {
        memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
        memset(&mmpmBwReqParam, 0,  sizeof(mmpmBwReqParam));
        memset(bandWidthArray, 0,  MAX_BW_REQUEST * sizeof(MmpmGenBwValType));
        memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));

        for (i = 0;i < numBw;i++)
        {
            bandWidthArray[i].busRoute.masterPort = bw[i].masterPort;
            bandWidthArray[i].busRoute.slavePort = bw[i].slavePort;
            bandWidthArray[i].bwValue.busBwValue.bwBytePerSec = bw[i].bwVal;
            bandWidthArray[i].bwValue.busBwValue.usagePercentage = bw[i].usagePercent;
            bandWidthArray[i].bwValue.busBwValue.usageType= bw[i].usageType;
        }

        mmpmBwReqParam.numOfBw = numBw;
        mmpmBwReqParam.pBandWidthArray = bandWidthArray;
        mmpmRscParam.rscId = MMPM_RSC_ID_GENERIC_BW;
        mmpmRscParam.rscParam.pGenBwReq = &mmpmBwReqParam;
        mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
        mmpmRscExtParam.numOfReq = 1;
        mmpmRscExtParam.pReqArray = &mmpmRscParam;
        mmpmRscExtParam.pStsArray = &sts;
        sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM bandwidth request FAILED for clientId %ld \n", clientId);
        }
        else
        {
          Test_InfoClk(0, 0, MMPM_CLK_ID_LPASS_AHB_ROOT, &clkFreq);
		  Test_InfoMips(0, 0, &mipsValue);
		  Test_InfoBW(0, 0, &bwValue);
          ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "AHB root CLK= %ld \n", clkFreq);
		  ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "MAX mips value= %ld \n", mipsValue);
		  ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "MAX bw value= %ld \n", bwValue);		  
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}


/* Request ADSPPM for sleep latency */
MMPM_STATUS Test_RequestSleepLatency(uint32 clientId, uint32 microSec)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmRscParamType   mmpmRscParam;
    MmpmRscExtParamType mmpmRscExtParam;

    if (0 != clientId)
    {
      memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
      memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));
      mmpmRscParam.rscId = MMPM_RSC_ID_SLEEP_LATENCY;
      mmpmRscParam.rscParam.sleepMicroSec = microSec;
      mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
      mmpmRscExtParam.numOfReq = 1;
      mmpmRscExtParam.pReqArray = &mmpmRscParam;
      mmpmRscExtParam.pStsArray = &sts;
      sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);
     
     if (MMPM_STATUS_SUCCESS != sts)
     {
         ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Sleep Latency request FAILED for clientId %ld\n", clientId);
     }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}

/* Test ADSPPM for Vreg */
MMPM_STATUS Test_RequestVreg(uint32 clientId, uint32 milliVol)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmRscParamType   mmpmRscParam;

    if (0 != clientId)
    {
        memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
        mmpmRscParam.rscId = MMPM_RSC_ID_VREG;
        mmpmRscParam.rscParam.vregMilliVolt = milliVol;
        //TODO VREG SUPPORT
       // sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Vreg request FAILED for clientId %ld \n", clientId);
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}


/* Request ADSPPM for MIPS */
MMPM_STATUS Test_RequestMips(uint32 clientId, uint32 mips)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmMipsReqType    mmpmMipsParam;
    MmpmRscParamType   mmpmRscParam;
    MmpmRscExtParamType mmpmRscExtParam;
    uint32 clkFreq = 0;
	uint32 mipsValue = 0;
	uint64 bwValue = 0;

    if (0 != clientId)
    {
      memset(&mmpmMipsParam, 0,  sizeof(MmpmMipsReqType));
      memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
      memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));
      mmpmMipsParam.mipsTotal = mips + 50;
      mmpmMipsParam.mipsPerThread = mips;
      mmpmMipsParam.codeLocation = MMPM_BW_PORT_ID_ADSP_MASTER;
      mmpmMipsParam.reqOperation = MMPM_MIPS_REQUEST_CPU_CLOCK_ONLY;
      mmpmRscParam.rscId = MMPM_RSC_ID_MIPS_EXT;
      mmpmRscParam.rscParam.pMipsExt = &mmpmMipsParam;
      mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
      mmpmRscExtParam.numOfReq = 1;
      mmpmRscExtParam.pReqArray = &mmpmRscParam;
      mmpmRscExtParam.pStsArray = &sts;
      sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);
      if (MMPM_STATUS_SUCCESS != sts)
      {
         ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM MIPS request FAILED for clientId %ld \n", clientId);
      }
      else
      {
		  Test_InfoClk(0, 0, MMPM_CLK_ID_LPASS_ADSP_CORE, &clkFreq);
		  Test_InfoMips(0, 0, &mipsValue);
		  Test_InfoBW(0, 0, &bwValue);
          ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "AHB root CLK= %ld \n", clkFreq);
		  ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "MAX mips value= %ld \n", mipsValue);
		  ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "MAX bw value= %ld \n", bwValue);	
      }
    } 
    else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}

/* Request ADSPPM for power Id(GDHS) */
MMPM_STATUS Test_RequestPwr(uint32 clientId)
{

    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmRscParamType   mmpmRscParam;
    MmpmRscExtParamType mmpmRscExtParam;

    if (0 != clientId)
    {
        memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
        memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));
        mmpmRscParam.rscId = MMPM_RSC_ID_POWER;
        mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
        mmpmRscExtParam.numOfReq = 1;
        mmpmRscExtParam.pReqArray = &mmpmRscParam;
        mmpmRscExtParam.pStsArray = &sts;
        sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Power(GDHS) request FAILED for clientId %ld \n", clientId);
        }
    } 
    else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}



/* Test ADSPPM core clock release */
MMPM_STATUS Test_ReleaseClk(uint32 clientId, uint32 relClkId[], uint32 numClk)
{
    MMPM_STATUS             sts = MMPM_STATUS_FAILED;
    MmpmRscParamType        mmpmRscParam;
    MmpmRscExtParamType     mmpmRscExtParam;
    MmpmClkIdArrayParamType mmpmClkIdArray;
    MmpmCoreClkIdType       coreClkId[MAX_CLK_REQUEST];
    uint32 i;

    memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
    memset(&mmpmClkIdArray, 0, sizeof(MmpmClkIdArrayParamType));
    memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));

    if (clientId)
    {
        for (i = 0; i < numClk;i++)
        {
            coreClkId[i].clkIdLpass = (MmpmClkIdLpassType)relClkId[i];
        }
        mmpmClkIdArray.numOfClk = numClk ;
        mmpmClkIdArray.pClkIdArray = coreClkId;
        mmpmRscParam.rscId = MMPM_RSC_ID_CORE_CLK;
        mmpmRscParam.rscParam.pRelClkIdArray = &mmpmClkIdArray;
        mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
        mmpmRscExtParam.numOfReq = 1;
        mmpmRscExtParam.pReqArray = &mmpmRscParam;
        mmpmRscExtParam.pStsArray = &sts;
        sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM core clk release FAILED for clientId %ld \n", clientId);
        }
		else
		{
			ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "ADSPPM core clk release PASSED for clientId %ld \n", clientId);
		}
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}

/* Test ADSPPM core clock domain release */
MMPM_STATUS Test_ReleaseClkDomain(uint32 clientId, uint32 relClkId[], uint32 numClk)
{
    MMPM_STATUS             sts = MMPM_STATUS_FAILED;
    MmpmRscParamType        mmpmRscParam;
    MmpmRscExtParamType     mmpmRscExtParam;
    MmpmClkIdArrayParamType mmpmClkIdArray;
    MmpmCoreClkIdType       coreClkId[MAX_CLK_REQUEST];
    uint32 i;

    memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
    memset(&mmpmClkIdArray, 0, sizeof(MmpmClkIdArrayParamType));
    memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));

    if (clientId)
    {
        for (i = 0; i < numClk;i++)
        {
            coreClkId[i].clkIdLpass = (MmpmClkIdLpassType)relClkId[i];
        }
        mmpmClkIdArray.numOfClk = numClk ;
        mmpmClkIdArray.pClkIdArray = coreClkId;
        mmpmRscParam.rscId = MMPM_RSC_ID_CORE_CLK_DOMAIN;
        mmpmRscParam.rscParam.pRelClkIdArray = &mmpmClkIdArray;
        mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
        mmpmRscExtParam.numOfReq = 1;
        mmpmRscExtParam.pReqArray = &mmpmRscParam;
        mmpmRscExtParam.pStsArray = &sts;
        sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM core clk release FAILED for clientId %ld \n", clientId);
        }
		else
		{
			ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "ADSPPM core clk release PASSED for clientId %ld \n", clientId);
		}
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}




MMPM_STATUS Test_ReleaseMemPwr(uint32 clientId)
{
	  MMPM_STATUS        sts = MMPM_STATUS_FAILED;
	  MmpmRscParamType   mmpmRscParam;
      MmpmRscExtParamType mmpmRscExtParam;

    if (0 != clientId)
    {
      memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
      memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscParamType));
      mmpmRscParam.rscId = MMPM_RSC_ID_MEM_POWER;
      mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
      mmpmRscExtParam.numOfReq = 1;
      mmpmRscExtParam.pReqArray = &mmpmRscParam;
      mmpmRscExtParam.pStsArray = &sts;
      sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
	  if (MMPM_STATUS_SUCCESS != sts)
      {
        ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM release FAILED for clientId %ld \n", clientId);
      }
	}
	else
	{
		ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
	}
	return sts;


}

/* Test ADSPPM register programming release */
MMPM_STATUS Test_ReleaseRegProg(uint32 clientId)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmRscParamType   mmpmRscParam;
    MmpmRscExtParamType mmpmRscExtParam;

    if (0 != clientId)
    {
      memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
      memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscParamType));
      mmpmRscParam.rscId = MMPM_RSC_ID_REG_PROG;
      mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
      mmpmRscExtParam.numOfReq = 1;
      mmpmRscExtParam.pReqArray = &mmpmRscParam;
      mmpmRscExtParam.pStsArray = &sts;
        sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Register Programming FAILED for clientId %ld \n", clientId);
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}


/* Test ADSPPM bandwidth release */
MMPM_STATUS Test_ReleaseBw(uint32 clientId)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmRscParamType   mmpmRscParam;
    MmpmRscExtParamType mmpmRscExtParam;
    uint32 clkFreq = 0;
	uint32 mipsValue = 0;
	uint64 bwValue = 0;

    if (0 != clientId)
    {
      memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
      memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscParamType));
      mmpmRscParam.rscId = MMPM_RSC_ID_GENERIC_BW;
      mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
      mmpmRscExtParam.numOfReq = 1;
      mmpmRscExtParam.pReqArray = &mmpmRscParam;
      mmpmRscExtParam.pStsArray = &sts;
      sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
      if (MMPM_STATUS_SUCCESS != sts)
      {
         ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM bandwidth release FAILED for clientId %ld \n", clientId);
      }
      else
      {
          Test_InfoClk(0, 0, MMPM_CLK_ID_LPASS_AHB_ROOT, &clkFreq);
		  Test_InfoMips(0, 0, &mipsValue);
		  Test_InfoBW(0, 0, &bwValue);	
      }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}

/* Test ADSPPM sleep latency release */
MMPM_STATUS Test_ReleaseSleepLatency(uint32 clientId)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmRscParamType   mmpmRscParam;
    MmpmRscExtParamType mmpmRscExtParam;

    if (0 != clientId) 
    {
      memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
      memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscParamType));
      mmpmRscParam.rscId = MMPM_RSC_ID_SLEEP_LATENCY;
      mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
      mmpmRscExtParam.numOfReq = 1;
      mmpmRscExtParam.pReqArray = &mmpmRscParam;
      mmpmRscExtParam.pStsArray = &sts;

      sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
      if (MMPM_STATUS_SUCCESS != sts)
      {
         ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Sleep latency release FAILED for clientId %ld \n", clientId);
      }
    } 
    else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}


/* Test ADSPPM Vreg release */
MMPM_STATUS Test_ReleaseVreg(uint32 clientId)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmRscParamType   mmpmRscParam;

    if (0 != clientId)
    {
        memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
        mmpmRscParam.rscId = MMPM_RSC_ID_VREG;
        //sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Vreg release FAILED for clientId %ld \n", clientId);
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}

/* Test ADSPPM MIPS release */
MMPM_STATUS Test_ReleaseMips(uint32 clientId)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmRscParamType   mmpmRscParam;
    MmpmRscExtParamType mmpmRscExtParam;
    uint32 clkFreq = 0;
	uint32 mipsValue = 0;
	uint64 bwValue = 0;

    if (0 != clientId)
    {
      memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
      memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscParamType));
      mmpmRscParam.rscId = MMPM_RSC_ID_MIPS_EXT;
      mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
      mmpmRscExtParam.numOfReq = 1;
      mmpmRscExtParam.pReqArray = &mmpmRscParam;
      mmpmRscExtParam.pStsArray = &sts;
       
      sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
      if (MMPM_STATUS_SUCCESS != sts)
      {
         ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM MIPS release FAILED for clientId %ld \n", clientId);
      }
      else
      {
          Test_InfoClk(0, 0, MMPM_CLK_ID_LPASS_ADSP_CORE, &clkFreq);
		  Test_InfoMips(0, 0, &mipsValue);
		  Test_InfoBW(0, 0, &bwValue);
          ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "AHB root CLK= %ld \n", clkFreq);
		  ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "MAX mips value= %ld \n", mipsValue);
		  ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "MAX bw value= %ld \n", bwValue);	
      }
    } 
    else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}

/* Test ADSPPM power (gdhs) release */
MMPM_STATUS Test_ReleasePwr(uint32 clientId)
{
    MMPM_STATUS       sts = MMPM_STATUS_FAILED;
    MmpmRscParamType  mmpmRscParam;
    MmpmRscExtParamType mmpmRscExtParam;

    if (0 != clientId)
    {
      memset(&mmpmRscParam, 0,  sizeof(MmpmRscParamType));
      memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscParamType));
      mmpmRscParam.rscId = MMPM_RSC_ID_POWER;
      mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
      mmpmRscExtParam.numOfReq = 1;
      mmpmRscExtParam.pReqArray = &mmpmRscParam;
      mmpmRscExtParam.pStsArray = &sts;

        sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Power(gdhs) release FAILED for clientId %ld \n", clientId);
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}


/* Test ADSPPM clk getInfo */
MMPM_STATUS Test_InfoClk(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 clkId, uint32 *clkFreq)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmInfoDataType   infoData;
    MmpmInfoClkFreqType    retClkData;

    memset(&infoData, 0, sizeof(infoData));
    memset(&retClkData,0, sizeof(retClkData));
    infoData.infoId = MMPM_INFO_ID_CLK_FREQ;
    infoData.coreId = coreId;
    infoData.instanceId = instanceId;
    retClkData.clkId =  clkId;
    retClkData.forceMeasure =  1;
    infoData.info.pInfoClkFreqType = &retClkData;
    sts = MMPM_GetInfo(&infoData );
    if (MMPM_STATUS_SUCCESS != sts)
    {
        ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM GetInfo for clk FAILED for coreID %d instanceID %d\n", coreId, instanceId);
    } else
    {
        *clkFreq = retClkData.clkFreqHz;
		ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "getclkfreq clkid=%d, freq=%d\n", clkId,  *clkFreq);
    }
    return sts;
}


/* Test ADSPPM max/min mips getInfo */
MMPM_STATUS Test_InfoMips(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 *mipsValue)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmInfoDataType   infoData;
	
    memset(&infoData, 0, sizeof(infoData));
    infoData.infoId = MMPM_INFO_ID_MIPS_MAX;
    infoData.coreId = coreId;
    infoData.instanceId = instanceId;
    infoData.info.mipsValue = 0;
    sts = MMPM_GetInfo(&infoData );
    if (MMPM_STATUS_SUCCESS != sts)
    {
        ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM GetInfo for max mips FAILED for coreID %d instanceID\n", coreId, instanceId );
    } else
    {
        *mipsValue = infoData.info.mipsValue;
		ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "ADSPPM GetInfo FOR MAX mips=%d \n", *mipsValue);
    }
    return sts;
}

/* Test ADSPPM max/min bw getInfo */
MMPM_STATUS Test_InfoBW(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint64 *bwValue)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmInfoDataType   infoData;

    memset(&infoData, 0, sizeof(infoData)); 
    infoData.infoId = MMPM_INFO_ID_BW_MAX;
    infoData.coreId = coreId;
    infoData.instanceId = instanceId;
    infoData.info.bwValue = 0;
    sts = MMPM_GetInfo(&infoData );
    if (MMPM_STATUS_SUCCESS != sts)
    {
        ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM GetInfo for max BW FAILED for coreID %d instanceID\n", coreId, instanceId );
    } else
    {
        *bwValue = infoData.info.bwValue;
		ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_INFO,  "ADSPPM GetInfo FOR MAX bw=%d \n", *bwValue);
    }
    return sts;
}


/* Test ADSPPM clk getInfo */
MMPM_STATUS Test_InfoClkPerfMon(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 clkId, uint32 *clkFreq)
{
    MMPM_STATUS            sts = MMPM_STATUS_FAILED;
    MmpmInfoDataType       infoData;
    MmpmInfoClkFreqType    retClkData;

    memset(&infoData, 0, sizeof(infoData));
    memset(&retClkData,0, sizeof(retClkData));
    infoData.infoId = MMPM_INFO_ID_CLK_FREQ;
    infoData.coreId = coreId;
    infoData.instanceId = instanceId;
    retClkData.clkId =  clkId;
    retClkData.forceMeasure =  1;
    infoData.info.pInfoClkFreqType = &retClkData;
    sts = MMPM_GetInfo(&infoData );
    if (MMPM_STATUS_SUCCESS != sts)
    {
        ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM GetInfo for clk FAILED for coreID %d instanceID %d\n", coreId, instanceId);
    } else
    {
        *clkFreq = retClkData.clkFreqHz;
    }
    return sts;
}


/* Test ADSPPM power(gdhs) getInfo */
MMPM_STATUS Test_InfoPwr(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 *bInfoPower)
{
    MMPM_STATUS        sts = MMPM_STATUS_FAILED;
    MmpmInfoDataType   infoData;

    memset(&infoData, 0, sizeof(MmpmInfoDataType));
    infoData.infoId = MMPM_INFO_ID_POWER_SUPPORT;
    infoData.coreId = coreId;
    infoData.instanceId = instanceId;
    //sts = MMPM_GetInfo(&infoData );
    if (MMPM_STATUS_SUCCESS != sts)
    {
        ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM GetInfo for gdfs FAILED for coreID %d instanceID %d\n", coreId, instanceId);
    } else
    {
        *bInfoPower = infoData.info.bInfoPower;
    }
    return sts;
}




/* Test ADSPPM Thermal */
MMPM_STATUS Test_Thermal()
{
    MMPM_STATUS       sts = MMPM_STATUS_FAILED;

    return sts;
}


/* Test ADSPPM Deregister */
MMPM_STATUS Test_Deregister(uint32 clientId)
{
    MMPM_STATUS       sts = MMPM_STATUS_FAILED;

    if (clientId)
    {
        sts = MMPM_Deregister_Ext(clientId);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "ADSPPM Deregister FAILED for clientId %ld \n", clientId);
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "No valid ADSPPM client ID \n");
    }
    return sts;
}
