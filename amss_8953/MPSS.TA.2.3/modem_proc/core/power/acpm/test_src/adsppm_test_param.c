/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include "adsppm_test_param.h"
#include "adsppm_test_main.h"


const Mmpm2ReqClkTestParamTableType gReqClkParam[] = 
{
    {},/* NONE */
	{}, /*LPASS_ADSP*/
	{}, /*LPASS_LPM*/
	{}, /*LPASS_DML*/
	{}, /*LPASS_AIF*/
	{1,  /*LPASS_SLIMBUS*/
        {
            /* clkId,                   freq in Khz,    freqMatch      */
            {MMPM_CLK_ID_LPASS_SLIMBUS_CORE, 24576, MMPM_FREQ_AT_LEAST}
        }
    },
	{1,  /* MIDI */
        {
            /* clkId,                   freq in Khz,    freqMatch      */
            {MMPM_CLK_ID_LPASS_MIDI_CORE,   122000, MMPM_FREQ_AT_LEAST}
        }
    },
	{5,  /*LPASS_AVSYNC  */
        {
             /* clkId,                   freq in Khz,    freqMatch      */
            {MMPM_CLK_ID_LPASS_AVSYNC_XO,           20000, MMPM_FREQ_AT_LEAST},
            {MMPM_CLK_ID_LPASS_AVSYNC_BT,     27000,  MMPM_FREQ_AT_LEAST},
            {MMPM_CLK_ID_LPASS_AVSYNC_FM,       20000,  MMPM_FREQ_AT_LEAST},
			{MMPM_CLK_ID_LPASS_AVTIMER_CORE,    20000, MMPM_FREQ_AT_LEAST},
			{MMPM_CLK_ID_LPASS_ATIME_CORE,    20000, MMPM_FREQ_AT_LEAST}
        }
    },
    {1,/* HWRSP */
        {
            /* clkId,                   freq in Khz,    freqMatch      */
            {MMPM_CLK_ID_LPASS_HWRSP_CORE,        122000, MMPM_FREQ_AT_LEAST}
        }
    },
    {}, //LPASS_SRAM,	
	{}, // LPASS_DCODEC
	{}  // LPASS_SPDIF
};

const Mmpm2ReqClkDomainTestParamTableType gReqClkDomainParam[] =
{
    {},/* NONE */
	{}, /*LPASS_ADSP*/
	{}, /*LPASS_LPM*/
	{}, /*LPASS_DML*/
	{}, /*LPASS_AIF*/
	{},  /*LPASS_SLIMBUS*/
	{},  /* MIDI */
    {5,  /*LPASS_AVSYNC  */
        {
             /* clkId,                   freq in Khz,    freqMatch      */
            {MMPM_CLK_ID_LPASS_AVSYNC_XO,     20000, 1},
            {MMPM_CLK_ID_LPASS_AVSYNC_BT,     27000,  2},
            {MMPM_CLK_ID_LPASS_AVSYNC_FM,       20000,  3},
			{MMPM_CLK_ID_LPASS_AVTIMER_CORE,    20000, 1},
			{MMPM_CLK_ID_LPASS_ATIME_CORE,    20000, 2}
        }
    },
    {},  /* HWRSP */
    {}, //LPASS_SRAM,	
	{}, // LPASS_DCODEC
	{}  // LPASS_SPDIF
};
/* Request MIPS parameter table */
const uint32 gReqMipsParam[] = 
{ 
    0,/* NONE */
    /* Mips */
    60,/* LPASS_ADSP */
    50,/* LPASS_LPM */
    40,/* LPASS_DML */
    30,/* LPASS_AIF */
    20,/* LPASS_SLIMBUS */
    30,/* MIDI */
    40,/* LPASS_AVSYNC */
    50,/* HWRSP */
	40, /*LPASS_SRAM */
    40, /*LPASS_DCODEC*/
	50,  /* LPASS_SPDIF */
};

/* Request register programming parameters table */
const uint32 gReqRegProgParam[] = 
{
    0,/* NONE */
    /* Reg Prog Match */
    MMPM_REG_PROG_NORM,/* LPASS_ADSP */
    MMPM_REG_PROG_NORM,/* LPASS_LPM */
    MMPM_REG_PROG_NORM,/* LPASS_DML */
    MMPM_REG_PROG_NORM,/* LPASS_AIF */
    MMPM_REG_PROG_NORM,/* LPASS_SLIMBUS */
    MMPM_REG_PROG_NORM,/* MIDI */
    MMPM_REG_PROG_NORM,/* LPASS_AVSYNC */
    MMPM_REG_PROG_FAST,/* HWRSP */
    MMPM_REG_PROG_NORM, /*LPASS_SRAM */
	MMPM_REG_PROG_NORM, /* LPASS_DCODEC */
	MMPM_REG_PROG_NORM  /* LPASS_SPDIF */
};


/* Request Vreg parameters table */
const uint32 gReqVregParam[] = 
{
    0,/* NONE */
    /* millivolt */
    10,/* LPASS_ADSP */
    10,/* LPASS_LPM */
    10,/* LPASS_DML */
    10,/* LPASS_AIF */
    10,/* LPASS_SLIMBUS */
    10,/* MIDI */
    10,/* LPASS_AVSYNC */
    10,/* HWRSP */
    40, /*LPASS_SRAM */
    40, /*LPASS_DCODEC*/
	50  /* LPASS_SPDIF */
};

const Mmpm2ReqBwTestParamTableType gReqBwParam[] = 
{
    {},/* NONE */
    {11,/* numBw*/ /* LPASS_ADSP */
        {
            /* masterport,  slaveport, bwVal(bytePerSec, usagePercent , usageType      */
            {MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_DML_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_AIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_MIDI_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_HWRSMP_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_AVSYNC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_LPM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_SRAM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
//			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_EXT_AHB_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_DDR_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_PERIFNOC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_ADSP_MASTER,  MMPM_BW_PORT_ID_SPDIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
        }
    },
	{},/* LPASS_LPM */
    {9,/* LPASS_DML */
        {
            /* masterport,  slaveport, bwVal(bytePerSec, usagePercent , usageType      */
			{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_AIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_MIDI_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_HWRSMP_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_AVSYNC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_LPM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_SRAM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_EXT_AHB_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_DDR_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			//{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_OCMEM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_PERIFNOC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
	//		{MMPM_BW_PORT_ID_EXT_AHB_MASTER,  MMPM_BW_PORT_ID_DML_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA},
	//		{MMPM_BW_PORT_ID_APPS_MASTER,  MMPM_BW_PORT_ID_DML_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA}
	        {MMPM_BW_PORT_ID_DML_MASTER,  MMPM_BW_PORT_ID_SPDIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
        }
    },
    {9,/* LPASS_AIF */
        {
             /* masterport,  slaveport, bwVal(bytePerSec, usagePercent , usageType      */
            {MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_DML_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_MIDI_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_HWRSMP_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_AVSYNC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_LPM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_SRAM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_EXT_AHB_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_DDR_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_OCMEM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_PERIFNOC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_EXT_AHB_MASTER,  MMPM_BW_PORT_ID_AIF_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_APPS_MASTER,  MMPM_BW_PORT_ID_AIF_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA}
		    {MMPM_BW_PORT_ID_AIF_MASTER,  MMPM_BW_PORT_ID_SPDIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
        }
    },
    {9, /* LPASS_SLIMBUS */
        {
			  /* masterport,  slaveport, bwVal(bytePerSec, usagePercent , usageType      */
            {MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_DML_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_AIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_MIDI_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_HWRSMP_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_AVSYNC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_LPM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_SRAM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_EXT_AHB_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_DDR_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_OCMEM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_PERIFNOC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_EXT_AHB_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_APPS_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA}
		    {MMPM_BW_PORT_ID_SLIMBUS_MASTER,  MMPM_BW_PORT_ID_SPDIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},

         
        }
    },
    {9,/* LPASS_MIDI */
        {
             /* masterport,  slaveport, bwVal(bytePerSec, usagePercent , usageType      */
            {MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_DML_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_AIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_HWRSMP_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_AVSYNC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_LPM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_SRAM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_EXT_AHB_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_DDR_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_OCMEM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_PERIFNOC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_EXT_AHB_MASTER,  MMPM_BW_PORT_ID_MIDI_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_APPS_MASTER,  MMPM_BW_PORT_ID_MIDI_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA}
		    {MMPM_BW_PORT_ID_MIDI_MASTER,  MMPM_BW_PORT_ID_SPDIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
        }
    },
	{},
    {
	  9,/* LPASS_HWRSMP */
        {
             /* masterport,  slaveport, bwVal(bytePerSec, usagePercent , usageType      */
            {MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_DML_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_AIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_MIDI_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_AVSYNC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_LPM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_SRAM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_EXT_AHB_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_DDR_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_OCMEM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
		//	{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_PERIFNOC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
			//{MMPM_BW_PORT_ID_EXT_AHB_MASTER,  MMPM_BW_PORT_ID_HWRSMP_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			//{MMPM_BW_PORT_ID_APPS_MASTER,  MMPM_BW_PORT_ID_HWRSMP_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA}
			{MMPM_BW_PORT_ID_HWRSMP_MASTER,  MMPM_BW_PORT_ID_SPDIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DSP},
        }
	},
    {},/* LPASS_SRAM */
	{}, /* LPASS_DCODEC */
	 {9, /* LPASS_SPDIF */
        {
			  /* masterport,  slaveport, bwVal(bytePerSec, usagePercent , usageType      */
            {MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_DML_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_AIF_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_MIDI_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_HWRSMP_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_AVSYNC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_LPM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_SRAM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_EXT_AHB_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
			{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_DDR_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_OCMEM_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_PERIFNOC_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA},
		//	{MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000,        100,     MMPM_BW_USAGE_LPASS_DMA}
		    {MMPM_BW_PORT_ID_SPDIF_MASTER,  MMPM_BW_PORT_ID_SLIMBUS_SLAVE,     60000000,        100,     MMPM_BW_USAGE_LPASS_DMA},         
        }
	}
};


/* Request Sleep Latency parameters table */
const uint32 gReqSleepLatencyParam[] = 
{
    0,/* NONE */
     /* microSec */
    5,/* MMPM_CORE_ID_LPASS_ADSP */
    5,/* MMPM_CORE_ID_LPASS_LPM */
    5,/* MMPM_CORE_ID_LPASS_DML */
    5,/* MMPM_CORE_ID_LPASS_AIF */
    5,/* MMPM_CORE_ID_LPASS_SLIMBUS */
    5,/* MMPM_CORE_ID_LPASS_MIDI */
    5,/* MMPM_CORE_ID_LPASS_AVSYNC */
    5,/* MMPM_CORE_ID_LPASS_HWRSMP */
    5,/* MMPM_CORE_ID_LPASS_SRAM */
	5, /* MMPM_CORE_ID_LPASS_DCODEC */
	5  /*MMPM_CORE_ID_LPASS_SPDIF */
};

const Mmpm2RelClkTestParamTableType gRelClkParam[] = 
{
	{},/* NONE */
	{}, /*LPASS_ADSP*/
	{}, /*LPASS_LPM*/
	{}, /*LPASS_DML*/
	{}, /*LPASS_AIF*/
	{1, /*LPASS_SLIMBUS*/
        {
          MMPM_CLK_ID_LPASS_SLIMBUS_CORE
        },
    }, 
	{1,  /* MIDI */
	  {
		MMPM_CLK_ID_LPASS_MIDI_CORE
	  },    
    },
	{5,  /*LPASS_AVSYNC  */
        {
            MMPM_CLK_ID_LPASS_AVSYNC_XO, 
            MMPM_CLK_ID_LPASS_AVSYNC_BT, 
            MMPM_CLK_ID_LPASS_AVSYNC_FM,
			MMPM_CLK_ID_LPASS_AVTIMER_CORE,
			MMPM_CLK_ID_LPASS_ATIME_CORE,
        },
    },
    {1,/* HWRSP */
        {
			MMPM_CLK_ID_LPASS_HWRSP_CORE,
        },
    },
    {},
    {},
    {}  
};

/* GetInfo parameters table */
const uint32 gInfoClkParam [] = 
{
    0,/* NONE */
    /* clkId */
    MMPM_CLK_ID_LPASS_MIDI_CORE,     /* MIDI */
    MMPM_CLK_ID_LPASS_AVSYNC_XO,     /* LPASS_AVSYNC */
    MMPM_CLK_ID_LPASS_AVSYNC_BT,     /* LPASS_AVSYNC */
    MMPM_CLK_ID_LPASS_AVSYNC_FM,        /* LPASS_AVSYNC */
    MMPM_CLK_ID_LPASS_HWRSP_CORE,     /* MMPM_CLK_ID_LPASS_HWRSP_CORE */
	MMPM_CLK_ID_LPASS_AVTIMER_CORE,   /* LPASS_AVTIMER CLK*/
	MMPM_CLK_ID_LPASS_ATIME_CORE,     /* LPASS_ATIMER CLK */
};


/* Table that maps cores to ahb clk index in mmpm clock */
const Mmpm2ReqMemPwrTestParamTableType gReqMemPwrParam[] =
{
//{memorytype, state}
	{0,  0},/* NONE */
    {1,  1},/* MMPM_CORE_ID_LPASS_ADSP */
    {1,  2},/* MMPM_CORE_ID_LPASS_LPM */
    {1,  3},/* MMPM_CORE_ID_LPASS_DML */
	{2,  1},/* MMPM_CORE_ID_LPASS_AIF */
    {2,  2},/* MMPM_CORE_ID_LPASS_SLIMBUS */
    {2,  3},/* MMPM_CORE_ID_LPASS_MIDI */
    {3,  1},/* MMPM_CORE_ID_LPASS_AVSYNC */
    {3,  2},/* MMPM_CORE_ID_LPASS_HWRSMP */
    {3,  3},/* MMPM_CORE_ID_LPASS_SRAM */
    {1,  1},
    {1,  1}
};

/* Frequency plan table */
const Mmpm2FreqPlanTestParamTableType gFreqPlanParam[] = 
{
    {},/* NONE */
    /* numClk */
	{},/* MMPM_CORE_ID_LPASS_ADSP */
	{},/* MMPM_CORE_ID_LPASS_LPM */
	{},/* MMPM_CORE_ID_LPASS_DML */
	{},/* MMPM_CORE_ID_LPASS_AIF */
	{1, /* MMPM_CORE_ID_LPASS_SLIMBUS */
        {   /* numFreq */
            {1, MMPM_CLK_ID_LPASS_SLIMBUS_CORE, {24576}}
        }
    },
    {1, /* MMPM_CORE_ID_LPASS_MIDI */
        {
            /* numFreq */
            {3, MMPM_CLK_ID_LPASS_MIDI_CORE, {109227, 122880,140434}}
        }
    },
   
    {5,
	    {
			 /* numFreq */
            {1, MMPM_CLK_ID_LPASS_AVSYNC_XO, {20000}},
			{1, MMPM_CLK_ID_LPASS_AVSYNC_BT, {27000}},
			{1, MMPM_CLK_ID_LPASS_AVSYNC_FM, {20000}},
			{1, MMPM_CLK_ID_LPASS_AVTIMER_CORE, {20000}},
		    {1, MMPM_CLK_ID_LPASS_ATIME_CORE, {20000}}			
		}
	}, /*MMPM_CORE_ID_LPASS_AVSYNC*/

    {1,/* MMPM_CORE_ID_LPASS_HWRSMP */
        {
            {5, MMPM_CLK_ID_LPASS_HWRSP_CORE, {19200,61440,81920,122880,140434}}
        }
    },

    {}, //LPASS_SRAM,	
	{}, // LPASS_DCODEC
	{}  // LPASS_SPDIF
};



/* Index into the test parameters for a coreId and instanceId */
const uint32 gTestParamIndex[MMPM_CORE_ID_LPASS_END - MMPM_CORE_ID_LPASS_START][MMPM_CORE_INSTANCE_MAX] = 
{ 
    {0, NONE,			NONE,		NONE},
    {0, LPASS_ADSP,		0,			0},
    {0, LPASS_LPM,		0,			0},
    {0, LPASS_DML,		0,			0},
    {0, LPASS_AIF,		0,			0},
    {0, LPASS_SLIMBUS,  0,			0},
    {0, LPASS_MIDI,		0,			0},
    {0, LPASS_AVSYNC,   0,			0},
    {0, LPASS_HWRSMP,   0,			0},
    {0, LPASS_SRAM,		0,			0},
	{0, LPASS_DCODEC,	0,			0},
	{0, LPASS_SPDIF,	0,			0},
};


/* Get request clock parameters */
void GetReqClkParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, ClkTestType clk[], uint32 *numClks)
{
    uint32 i, index = 0;

    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];

    *numClks = gReqClkParam[index].numClks;
    for(i = 0; i < *numClks; i++)
    {
        clk[i].clkId = gReqClkParam[index].clk[i].clkId;
        clk[i].freq = gReqClkParam[index].clk[i].freq * 1000;
        clk[i].freqMatch = gReqClkParam[index].clk[i].freqMatch;
    }
}

/* Get request clock domain parameters */
void GetReqClkDomainParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, ClkDomainTestType clk[], uint32 *numClks)
{
    uint32 i, index = 0;

    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];

    *numClks = gReqClkDomainParam[index].numClks;
    for(i = 0; i < *numClks; i++)
    {
       clk[i].clkId = gReqClkDomainParam[index].clk[i].clkId;
       clk[i].clkFreqHz = gReqClkDomainParam[index].clk[i].clkFreqHz;
       clk[i].clkDomainSrc = gReqClkDomainParam[index].clk[i].clkDomainSrc;
    }

}



/* Get request parameters for MIPS */
void GetReqMipsParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 *mips)
{
    uint32  index = 0;

    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];
    *mips = gReqMipsParam[index];
}

/* Get request register programming parameters */
void GetReqRegProgParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 *match)
{
    uint32  index = 0;

    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];
    *match = gReqRegProgParam[index];
}

/* Get vreg request parameters */
void GetReqVregParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 *milliVol)
{
    uint32  index = 0;

    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];
    *milliVol = gReqVregParam[index];
}

/* Get request parameters for BW */
void GetReqBWParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, BwReqTestType bw[], uint32 *numBw)
{
    uint32 i, index = 0;

    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];

    *numBw = gReqBwParam[index].numBw;
    for(i = 0; i < *numBw; i++)
    {
        bw[i].masterPort = gReqBwParam[index].bw[i].masterPort;
        bw[i].slavePort = gReqBwParam[index].bw[i].slavePort;
        bw[i].bwVal = gReqBwParam[index].bw[i].bwVal;
        bw[i].usagePercent = gReqBwParam[index].bw[i].usagePercent;
        bw[i].usageType = gReqBwParam[index].bw[i].usageType;
    }
}

/* Get sleep latency request parameters */
void GetReqSleepLatencyParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 *microSec)
{
    uint32  index = 0;

    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];
    *microSec = gReqSleepLatencyParam[index];
}


/* Get release clk parameter */
void GetRelClkParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 clk[], uint32 *numClks)
{
    uint32 i, index = 0;

    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];

    *numClks = gRelClkParam[index].numClks;
    for(i = 0; i < *numClks; i++)
    {
        clk[i] = gRelClkParam[index].clk[i];
    }
}


void GetInfoClkParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 *clkId)
{
    uint32  index = 0;

    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];
    *clkId = gInfoClkParam[index];
}


void GetReqMemPwrParam(MmpmCoreIdType coreId, MmpmCoreInstanceIdType instanceId, uint32 *memType, uint32 *powerState)
{
	uint32  index = 0;
    index = gTestParamIndex[coreId-MMPM_CORE_ID_LPASS_START][instanceId];
	*memType = gReqMemPwrParam[index].memType;
	*powerState = gReqMemPwrParam[index].powerState;
}