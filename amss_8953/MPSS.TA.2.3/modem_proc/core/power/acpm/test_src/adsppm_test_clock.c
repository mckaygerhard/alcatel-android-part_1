/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include <stdio.h>
#include <string.h>
#include "mmpm.h"
#include "adsppm_test.h"
#include "adsppm_test_main.h"
#include "adsppm_test_param.h"

extern const uint32 gTestParamIndex[MMPM_CORE_ID_LPASS_END - MMPM_CORE_ID_LPASS_START ][MMPM_CORE_INSTANCE_MAX];
extern const Mmpm2FreqPlanTestParamTableType gFreqPlanParam[];
extern char *result[2];

/*NOTE: To Enable or Disable the Log Parsing, please toggle the corresponding void pointer entry for the function of interest
 * For Eg: To Enable Log parsing for REG the following code should be used for TestId1
 *    TestId1
    {1, 2, 0, MMPM_CORE_ID_2D_GRP, MMPM_CORE_INSTANCE_0,
       {PURGE,REG, DREG},
       {0,1,0}
    },
   Use Purge to ensure all the MMPM Log Entries before the function of interest are discarded so that the log entries that are relevant to the
   Function of interest are read and parsed. Note that when using PURGE, the number of test sequence entries in the structure below has to
   be incremented.

    Currently Supported for :
    1) REG
    2) DREG
    3) REQCLK
    4) RELCLK
    5) REQDCLK
    6) RELDCLK


    Please ensure that the Debug Level is appropriately set to "DEBUG_LEVEL 15" in the mminit script.
 *  */



AdsppmTestType testclk1 [] =
{
 
    /* Test Id1 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQCLK, RELCLK, DREG}
    },

     /* Test Id2 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG, REQCLK, RELCLK, DREG}
    },

     /* Test Id3 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, REQCLK, RELCLK, DREG}
    },

      /* Test Id4 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
       {REG, REQCLK, RELCLK, DREG}
    },
};

// These are used in the test case logging, and in the menu text.
#define CLOCK_01_NAME    "clock_01"
#define CLOCK_01_DETAILS "Verify all clocks can be enabled and disabled."

/** 
  @ingroup Clock 
  @test Clock_01
  @brief Verify all clocks can be enabled, disabled.

	Uses the testclk1[] data structure and calls InvokeTest().
	This will call:
		- MMPM_Register_Ext_Ext
	 	- MMPM_Request - using MMPM_RSC_ID_CORE_CLK resource ID
	 	- MMPM_GetInfo - using MMPM_INFO_ID_CORE_CLK info ID to read the clock
	 	- MMPM_Deregister_Ext_Ext

	 The following clocks will be checked:  2D, MDP, VCodec

	 A pass condition is when then MMPM returns success for all calls,
	 the clock freq is correct and the MMPM Log entry is ok.

  @todo Check the returned clock freq.
  @todo Read the MMPM logs to check a valid entry is created

  @param none

  @return MMPM_STATUS_SUCCESS Pass
  @return All other MMPM status Fail due to MMPM error
  @return -1 Fail due to incorrect clock frequency
  @return -2 Fail due to incorrect Log entry

  @see InvokeTest()
  @see testclk1[]
*/

/* Clock_01 */
MMPM_STATUS Test_Clock_01(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

	ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_01_NAME, CLOCK_01_DETAILS);
  
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testclk1)/sizeof(AdsppmTestType);
    sts = InvokeTest(testclk1, numTest, testSts);

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_01 - %s,%d\n",result[!sts], sts);
    return sts;
}

/* Clock_02 */
/* Multiple client clk request,release test */
AdsppmTestType testclk2 [] =
{
    /* Test Id1 */
    {1, 1, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG}
    },

    /* Test Id2 */
    {2, 1, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG}
    },

    /* Test Id3 */
    {1, 1, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REQCLK}
    },

    /* Test Id4 */
    {2, 1, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REQCLK}
    },

	 /* Test Id5 */
    {1, 1, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {RELCLK}
    },

    /* Test Id6 */
    {2, 1, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {RELCLK}
    },

    /* Test Id7 */
    {1, 1, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {DREG}
    },

    /* Test Id8 */
    {2, 1, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {DREG}
    }
};


#define CLOCK_02_NAME    "clock_02"
#define CLOCK_02_DETAILS "Multiple client clk request,release test."

/** 
  @ingroup Clock 
  @test Clock_02
  @brief Multiple clients.

  Verify all clocks can be enabled, disabled.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_02(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_02_NAME, CLOCK_02_DETAILS);
	
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testclk2)/sizeof(AdsppmTestType);
    sts = InvokeTest(testclk2, numTest, testSts);

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_02 - %s,%d\n",result[!sts], sts);
    return sts;
}


/* Clock_03 */
/* Test sequence reg-release . This should return failure since the client hasn't requested the resource */
AdsppmTestType testclk3 [] =
{
    /* Test Id1 */
    {1, 3, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, RELCLK, DREG}
    }
};

#define CLOCK_03_NAME    "clock_03"
#define CLOCK_03_DETAILS "Test sequence reg-rel. This should fail."

/** 
  @ingroup Clock 
  @test Clock_03
  @brief Release without request.

  Test sequence reg-rel. This should fail.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_03(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_03_NAME, CLOCK_03_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testclk3)/sizeof(AdsppmTestType);
    sts = InvokeTest(testclk3, numTest, testSts);

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,   "Clock_03 should fail. - %s,%d\n",result[!sts], sts);
    return sts;
}


/* Clock_04 */
/* Test sequence reg-request- release - release . This should return failure since the client hasn't requested the resource 2nd time */
AdsppmTestType testclk4 [] =
{
    /* Test Id1 */
    {1, 5, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG, REQCLK, RELCLK, RELCLK, DREG}
    }
};


#define CLOCK_04_NAME    "clock_04"
#define CLOCK_04_DETAILS "Verify calling extra release, it should fail."


/** 
  @ingroup Clock 
  @test Clock_04
  @brief Extra release test.

  Verify calling release, without calling request, on a clock enabled by another client remains on.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_04(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_04_NAME, CLOCK_04_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testclk4)/sizeof(AdsppmTestType);
    sts = InvokeTest(testclk4, numTest, testSts);
  
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_04 should fail - %s,%d\n",result[!sts], sts);

    return sts;
}

/* Clock_05 */
/* Test multiple request from same client and check the last frequency is set. Also checks the release, releases the clk */
MMPM_STATUS TestClkMultReqSameClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    ClkTestType clk;
    uint32 coreId, instanceId, index, clientId = 0;
    uint32 clkFreq = 0;
    coreId = MMPM_CORE_ID_LPASS_HWRSMP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        clk.clkId = gFreqPlanParam[index].clkParam[0].clkId;
        clk.freq = gFreqPlanParam[index].clkParam[0].freq[0] * 1000;
        clk.freqMatch = MMPM_FREQ_AT_LEAST;

        sts = Test_RequestClk(clientId, &clk, 1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            clk.freq = gFreqPlanParam[index].clkParam[0].freq[1] * 1000;
            sts = Test_RequestClk(clientId, &clk, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_InfoClkPerfMon(coreId, instanceId, clk.clkId, &clkFreq);
                if(clkFreq/1000 != clk.freq/1000)
                {
                   ADSPPM_TESTLOG_PRINTF_5(ADSPPMTEST_LOG_LEVEL_ERROR, "Error in second request, coreId  %ld, instanceId  %ld, clkId %ld requested clk %ld clk set %ld\n", coreId, instanceId, clk.clkId, clk.freq, clkFreq);
                  retSts = MMPM_STATUS_FAILED;
                }
            }
            else              
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Test_Request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } 
		else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st test request\n");
            retSts = MMPM_STATUS_FAILED;
        }

        sts = Test_ReleaseClk(clientId, &clk.clkId, 1);
         if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_InfoClkPerfMon(coreId, instanceId, clk.clkId, &clkFreq);
                if(clkFreq)
                {
                   ADSPPM_TESTLOG_PRINTF_5(ADSPPMTEST_LOG_LEVEL_ERROR, "error in release clk, coreId  %ld, instanceId  %ld, clkId %ld requested clk %ld clk set %ld\n", coreId, instanceId, clk.clkId, clk.freq, clkFreq);
                  retSts = MMPM_STATUS_FAILED;
                }
            }
        else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Release clk\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId);
		if (MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Deregister\n");
			retSts = MMPM_STATUS_FAILED;
		}
    } 
	else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}



#define CLOCK_05_NAME    "clock_05"
#define CLOCK_05_DETAILS "Verify a single release with multipel requests disables clock."

/** 
  @ingroup Clock 
  @test Clock_05
  @brief Single client multiple requests

  Verify a single release with multipel requests disables clock.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_05(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_05_NAME, CLOCK_05_DETAILS);
    sts = TestClkMultReqSameClient(); 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_05 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Clock_06 */
/* Test multiple clients request and check the hightest clk is set */
MMPM_STATUS TestClkMultReqDiffClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    ClkTestType clk;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0;
    uint32 clkFreq = 0;

    coreId = MMPM_CORE_ID_LPASS_MIDI;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        clk.clkId = gFreqPlanParam[index].clkParam[0].clkId;
        clk.freq = gFreqPlanParam[index].clkParam[0].freq[0] * 1000;
        clk.freqMatch = MMPM_FREQ_AT_LEAST;

        sts = Test_RequestClk(clientId1, &clk, 1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
		    clk.clkId = gFreqPlanParam[index].clkParam[0].clkId;
            clk.freq = gFreqPlanParam[index].clkParam[0].freq[1] * 1000;
			clk.freqMatch = MMPM_FREQ_AT_LEAST;
            sts = Test_RequestClk(clientId2, &clk, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_InfoClkPerfMon(coreId, instanceId, clk.clkId, &clkFreq);
                if(clkFreq/1000 != clk.freq/1000)
                {
                   ADSPPM_TESTLOG_PRINTF_5(ADSPPMTEST_LOG_LEVEL_ERROR, "Error in client2 request, coreId  %ld, instanceId  %ld, clkId %ld requested clk %ld clk set %ld\n", coreId, instanceId, clk.clkId, clk.freq, clkFreq);
                  retSts = MMPM_STATUS_FAILED;
                }
            }
            else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client2 Test_Request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } 
		else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client1 test request\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId1);		
			
			if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client1 Test_Deregister1\n");
                retSts = MMPM_STATUS_FAILED;
            }
		
		sts = Test_Deregister(clientId2);
		if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client2 Test_Deregister2\n");
                retSts = MMPM_STATUS_FAILED;
            }
    } 
	else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
 
    return retSts;
}


#define CLOCK_06_NAME    "clock_06"
#define CLOCK_06_DETAILS "Verify multiple clients of the same clock are handled."

/** 
  @ingroup Clock 
  @test Clock_06
  @brief Multiple clients - set freq.

  Verify multiple clients of the same clock are handled.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_06(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_06_NAME, CLOCK_06_DETAILS);

    sts = MMPM_STATUS_FAILED;
    sts = TestClkMultReqDiffClient();
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_06 - %s,%d\n",result[!sts], sts);

    return sts;
}

/* Clock_07 */
/* Test multiple clients request for same clkid and freq and verify request-release */
MMPM_STATUS TestClkMultEnableClkDiffClient(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    ClkTestType clk;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0;
    uint32 clkFreq;

    coreId = MMPM_CORE_ID_LPASS_MIDI;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        clk.clkId = gFreqPlanParam[index].clkParam[0].clkId;
        clk.freq = gFreqPlanParam[index].clkParam[0].freq[0] * 1000;
        clk.freqMatch = MMPM_FREQ_AT_LEAST;

        sts = Test_RequestClk(clientId1, &clk, 1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            sts = Test_RequestClk(clientId2, &clk, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_InfoClkPerfMon(coreId, instanceId, clk.clkId, &clkFreq);
                if(clkFreq/1000 != clk.freq/1000)
                {
                   ADSPPM_TESTLOG_PRINTF_5(ADSPPMTEST_LOG_LEVEL_ERROR, "Error in client2 request, coreId  %ld, instanceId  %ld, clkId %ld requested clk %ld clk set %ld\n", coreId, instanceId, clk.clkId, clk.freq, clkFreq);
                  retSts = MMPM_STATUS_FAILED;
                }
            }
            else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client2 Test_Request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } 
		else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client1 test request\n");
            retSts = MMPM_STATUS_FAILED;
        }    
        sts = Test_ReleaseClk(clientId1, &clk.clkId, 1);
        if(MMPM_STATUS_SUCCESS == sts)
        {
            sts = Test_ReleaseClk(clientId2, &clk.clkId, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_InfoClkPerfMon(coreId, instanceId, clk.clkId, &clkFreq);
                if(clkFreq)
                {
                   ADSPPM_TESTLOG_PRINTF_5(ADSPPMTEST_LOG_LEVEL_INFO, "Error in client2 release, coreId  %ld, instanceId  %ld, clkId %ld requested clk %ld clk set %ld\n", coreId, instanceId, clk.clkId, clk.freq, clkFreq);
                  retSts = MMPM_STATUS_FAILED;
                }
            }
            else
			{
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client2 Test_ReleaseClk\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } 
		else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client1 Test_ReleaseClk\n");
            retSts = MMPM_STATUS_FAILED;
        }

       sts = Test_Deregister(clientId1);		
			
			if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client1 Test_Deregister1\n");
                retSts = MMPM_STATUS_FAILED;
            }
		
		sts = Test_Deregister(clientId2);
		if (MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in client2 Test_Deregister2\n");
                retSts = MMPM_STATUS_FAILED;
            }
    } 
	else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
    return retSts;
}

#define CLOCK_07_NAME    "clock_07"
#define CLOCK_07_DETAILS "Test Multiple clients for setting same freq for a clk."

/** 
  @ingroup Clock 
  @test Clock_07
  @brief Multiple clients - enable

  Test Multiple clients for enable only clks.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_07(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    sts = TestClkMultEnableClkDiffClient();

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_07_NAME, CLOCK_07_DETAILS);

	sts = TestClkMultEnableClkDiffClient();

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_07 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Clock_10 */ 
/* Check clk release set the clk to 0 if this is the only client */
MMPM_STATUS TestClkRelease(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    ClkTestType clk;
    uint32 coreId, instanceId, index, clientId = 0;
    uint32 clkFreq = 0;

    coreId = MMPM_CORE_ID_LPASS_HWRSMP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        clk.clkId = gFreqPlanParam[index].clkParam[0].clkId;
        clk.freq = gFreqPlanParam[index].clkParam[0].freq[0] * 1000;
        clk.freqMatch = MMPM_FREQ_AT_LEAST;

        sts = Test_RequestClk(clientId, &clk, 1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            clk.freq = gFreqPlanParam[index].clkParam[0].freq[1] * 1000;
            sts = Test_RequestClk(clientId, &clk, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_InfoClkPerfMon(coreId, instanceId, clk.clkId, &clkFreq);
                if(clkFreq/1000 != clk.freq/1000)
                {
                   ADSPPM_TESTLOG_PRINTF_5(ADSPPMTEST_LOG_LEVEL_ERROR, "Error in second request, coreId  %ld, instanceId  %ld, clkId %ld requested clk %ld clk set %ld\n", coreId, instanceId, clk.clkId, clk.freq, clkFreq);
                  retSts = MMPM_STATUS_FAILED;
                }
            }
            else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd Test_Request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st test request\n");
            retSts = MMPM_STATUS_FAILED;
        }

        sts = Test_ReleaseClk(clientId, &clk.clkId, 1);
        if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_InfoClkPerfMon(coreId, instanceId, clk.clkId, &clkFreq);
                if(clkFreq)
                {
                   ADSPPM_TESTLOG_PRINTF_4(ADSPPMTEST_LOG_LEVEL_INFO, "Error in Test_ReleaseClk, coreId  %ld, instanceId  %ld, clkId %ld clk set %ld\n", coreId, instanceId, clk.clkId, clkFreq);
                  retSts = MMPM_STATUS_FAILED;
                }
            }
        else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in test release\n");
            retSts = MMPM_STATUS_FAILED;
        }

        sts = Test_Deregister(clientId);
		if (MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Deregister\n");
			retSts = MMPM_STATUS_FAILED;
		}
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
    return retSts;
}

#define CLOCK_10_NAME    "clock_10"
#define CLOCK_10_DETAILS "Check clk release set the clk to 0 if this is the only client."

/** 
  @ingroup Clock 
  @test Clock_10
  @brief Release freq test.

  Check clk release set the clk to 0 if this is the only client.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_10(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_10_NAME, CLOCK_10_DETAILS);

    sts = TestClkRelease();

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_10 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Clock_11 */ 
/* Check MMPM clk request for wrong non zero client Id */
MMPM_STATUS TestClkBadClientId(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    ClkTestType clk;
    uint32 coreId, instanceId, index, clientId = 0;

    coreId = MMPM_CORE_ID_LPASS_HWRSMP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        clk.clkId = gFreqPlanParam[index].clkParam[0].clkId;
        clk.freq = gFreqPlanParam[index].clkParam[0].freq[0] * 1000;
        clk.freqMatch = MMPM_FREQ_AT_LEAST;

        sts = Test_RequestClk(clientId + 1, &clk, 1);
        if (MMPM_STATUS_SUCCESS != sts)
        {
           ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_RequestClk\n");
		   retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId);
		if (MMPM_STATUS_SUCCESS != sts)
        {
           ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Deregister\n");
		   retSts = MMPM_STATUS_FAILED;
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
    return retSts;
}


#define CLOCK_11_NAME    "clock_11"
#define CLOCK_11_DETAILS "Verify incorrect handles, should fail"

/** 
  @ingroup Clock 
  @test Clock_11
  @brief Incorrect MMPM ID.

  Verify incorrect handles.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_11(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_11_NAME, CLOCK_11_DETAILS);
    sts = TestClkBadClientId();
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_11 should fail- %s,%d\n",result[!sts], sts);

    return sts;
}


/* Clock_12 */ 
/* Check MMPM clk request for wrong request params for clk */
MMPM_STATUS TestClkBadParam(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    ClkTestType clk;
    uint32 coreId, instanceId, index, clientId = 0;

    coreId = MMPM_CORE_ID_LPASS_HWRSMP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        /* Wrong freq match type */
        clk.clkId = gFreqPlanParam[index].clkParam[0].clkId;
        clk.freq = gFreqPlanParam[index].clkParam[0].freq[0] * 1000;
        clk.freqMatch = MMPM_FREQ_MAX;

        sts = Test_RequestClk(clientId, &clk, 1);
       if (MMPM_STATUS_SUCCESS != sts)
        {
           ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_RequestClk,wrong freqmatch\n");
		   retSts = MMPM_STATUS_FAILED;
        }
        /* Wrong clkId */
        clk.clkId = gFreqPlanParam[index].clkParam[0].clkId + 100;
        clk.freq = gFreqPlanParam[index].clkParam[0].freq[0] * 1000;
        clk.freqMatch = MMPM_FREQ_AT_LEAST;

        sts = Test_RequestClk(clientId, &clk, 1);
		if (MMPM_STATUS_SUCCESS != sts)
        {
           ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_RequestClk, wrong clkid\n");
		   retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId);
		if (MMPM_STATUS_SUCCESS != sts)
        {
           ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Deregister\n");
		   retSts = MMPM_STATUS_FAILED;
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}

#define CLOCK_12_NAME    "clock_12"
#define CLOCK_12_DETAILS "Verify bad parameters in MmpmClkReqType are handled gracefully, should fail."

/** 
  @ingroup Clock 
  @test Clock_12
  @brief Bad Parameters.

  Verify bad parameters in MmpmClkReqType are handled gracefully, verify log entries.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_12(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_12_NAME, CLOCK_12_DETAILS);
    sts = TestClkBadParam();
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_12 shoud fail- %s,%d\n",result[!sts], sts);
    return sts;
}


/* Clock_13 */
/* TODO: Test frequency plan */
MMPM_STATUS TestClkFreqPlan(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, i, j;
    uint32 index, clientId = 0;
    uint32 clkFreq = 0;
    ClkTestType clk;
    
    for (coreId = MMPM_CORE_ID_LPASS_ADSP; coreId < MMPM_CORE_ID_LPASS_END; coreId++)
    { 
       instanceId = MMPM_CORE_INSTANCE_0; 
	   clientId = Test_Register(coreId, instanceId);
	   if(0 != clientId)
	   {
       index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];
        if (index)
        {
            for(i = 0; i < gFreqPlanParam[index].numClks ;i++)
            {
               
                clk.freqMatch = MMPM_FREQ_AT_LEAST;
                clk.clkId = gFreqPlanParam[index].clkParam[i].clkId;

                for (j = 0; j < gFreqPlanParam[index].clkParam[i].numFreq;j++)
					{
                    
						clk.freq = gFreqPlanParam[index].clkParam[i].freq[j] * 1000;
						sts = Test_RequestClk(clientId, &clk, 1);
						if (MMPM_STATUS_SUCCESS == sts)
						{
							sts = Test_InfoClkPerfMon(coreId, instanceId, clk.clkId, &clkFreq);
							if(clkFreq/1000 != clk.freq/1000)
							{
								ADSPPM_TESTLOG_PRINTF_5(ADSPPMTEST_LOG_LEVEL_ERROR, "Error in frequency plan verification, coreId  %ld, instanceId  %ld, clkId %ld requested clk %ld clk set %ld\n", coreId, instanceId, clk.clkId, clk.freq, clkFreq);
								retSts = MMPM_STATUS_FAILED;
							}
						}
						else
						{
							ADSPPM_TESTLOG_PRINTF_4(ADSPPMTEST_LOG_LEVEL_ERROR, "Error in Test_RequestClk, coreId  %ld, instanceId  %ld, clkId %ld requested clk %ld\n", coreId, instanceId, clk.clkId, clk.freq);
                            retSts = MMPM_STATUS_FAILED;
						}
						sts = Test_ReleaseClk(clientId, &clk.clkId, 1); 
						if (MMPM_STATUS_SUCCESS != sts)
						{
						ADSPPM_TESTLOG_PRINTF_1(ADSPPMTEST_LOG_LEVEL_ERROR, "Error in Test_ReleaseClk, clientid=%d \n", clientId);
                        retSts = MMPM_STATUS_FAILED;
						}					
					} //numFreq
			} //numClks
		} //index
		sts = Test_Deregister(clientId);
		if (MMPM_STATUS_SUCCESS != sts)
        {
           ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Deregister clientID = %d \n", clientId);
		   retSts = MMPM_STATUS_FAILED;
        }
		} //clientid
		else
		{
		ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
		}
	} //coreid
	
    return retSts;
}


#define CLOCK_13_NAME    "clock_13"
#define CLOCK_13_DETAILS "Verify clock regime's freq. plan is correct."

/** 
  @ingroup Clock 
  @test Clock_13
  @brief Verify Freq. Plan

  Verify clock regime's freq. plan is correct.
    
  @param [IN] none

  @return  MMPM_STATUS

*/

MMPM_STATUS Test_Clock_13(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

	ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_13_NAME, CLOCK_13_DETAILS);
    sts = TestClkFreqPlan();  
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_13 - %s,%d\n",result[!sts], sts);
 
    return sts;
}




AdsppmTestType testclk14[] =
{
    /* Test Id1 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG, REQCLKDOMAIN, RELCLKDOMAIN, DREG}
    }
};

// These are used in the test case logging, and in the menu text.
#define CLOCK_14_NAME    "clock_14"
#define CLOCK_14_DETAILS "Verify clock domain request/release."

/** 
  @ingroup Clock 
  @test Clock_14
  @brief Verify all clock domains can be enabled, disabled.

	Uses the testclk1[] data structure and calls InvokeTest().
	This will call:
		- MMPM_Register_Ext_Ext
        - MMPM_Request - using MMPM_RSC_ID_CORE_CLK_DOMAIN
          resource ID
        - MMPM_GetInfo - using MMPM_INFO_ID_CORE_CLK_DOMAIN info
          ID to read the clock
	 	- MMPM_Deregister_Ext_Ext

	 The following clocks will be checked:  AVSYNC 

	 A pass condition is when then MMPM returns success for all calls,
	 the clock freq is correct and the MMPM Log entry is ok.

  @todo Check the returned clock freq.
  @todo Read the MMPM logs to check a valid entry is created

  @param none

  @return MMPM_STATUS_SUCCESS Pass
  @return All other MMPM status Fail due to MMPM error
  @return -1 Fail due to incorrect clock frequency
  @return -2 Fail due to incorrect Log entry

  @see InvokeTest()
  @see testclk1[]
*/

/* Clock_14 */
MMPM_STATUS Test_Clock_14(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

	ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", CLOCK_14_NAME, CLOCK_14_DETAILS);
  
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testclk14)/sizeof(AdsppmTestType);
    sts = InvokeTest(testclk14, numTest, testSts);

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Clock_14 - %s,%d\n",result[!sts], sts);
    return sts;
}


void Test_Clock()
{

    //test clock domain
    Test_Clock_14();

    /* Clock_13 */
    Test_Clock_13();

#ifndef TEST_PROFILING
    /* Clock_01 */
    Test_Clock_01();
    /* Clock_02 */
    Test_Clock_02();

    /* Clock_03 */
    Test_Clock_03();

    /* Clock_04 */
    Test_Clock_04();

    /* Clock_05 */
    Test_Clock_05();

    /* Clock_06 */
    Test_Clock_06();

    /* Clock_07 */
    Test_Clock_07();

    /* Clock_10 */
    Test_Clock_10();

    /* Clock_11 */
    Test_Clock_11();

    /* Clock_12 */
    Test_Clock_12();
#endif

}

