/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include <stdio.h>
#include <string.h>
#include "mmpm.h"
#include "adsppm_test.h"
#include "adsppm_test_main.h"
#include "adsppm_test_param.h"
#include "adsppm_test_utils.h"

extern char *result[2];

/* MIPS_01 */
/* Test MIPS for LPASS CORES*/
AdsppmTestType testmips1 [] =
{
    /* Test Id1 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },
       /* Test Id2 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_LPM, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },
       /* Test Id3 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },
      /* Test Id4 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AIF, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },
     /* Test Id5 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },
    /* Test Id6 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },
  /* Test Id6 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },

      /* Test Id7 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },

      /* Test Id8 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },
      /* Test Id9 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SRAM, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, DREG}
    },
};


#define MIPS_01_NAME    "mips_01"
#define MIPS_01_DETAILS "Verify node call is done."

/** 
  @ingroup MIPS 
  @test mips_01
  @brief Verify node call is done to DCVS MIPS node.

	
*/

/* MIPS_01 */
MMPM_STATUS Test_Mips_01(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", MIPS_01_NAME, MIPS_01_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testmips1)/sizeof(AdsppmTestType);
    sts = InvokeTest(testmips1, numTest, testSts);

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "MIPS_01 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* MIPS_02 */
/* Test MIPS for mutiple clients . */
AdsppmTestType testmips2 [] =
{
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS}
    },
    /* Test Id2 */
    {2, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS}
    },
    /* Test Id3 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {RELMIPS, DREG}
    },
    /* Test Id4 */
    {2, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {RELMIPS, DREG}
    }
};

#define MIPS_02_NAME    "mips_02"
#define MIPS_02_DETAILS "Verify multiple clients of the same owner ID are handled.  Each request/release is independent."

/** 
  @ingroup MIPS 
  @test misps_02
  @brief Verify multiple clients of the same owner ID are handled.  Each request/release is independent.


*/

/* MIPS_02 */
MMPM_STATUS Test_Mips_02(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", MIPS_02_NAME, MIPS_02_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testmips2)/sizeof(AdsppmTestType);
    sts = InvokeTest(testmips2, numTest, testSts);

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "MIPS_02 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* MIPS_03 */
/* Test mips . Extra release should failed */
AdsppmTestType testmips3 [] =
{
    /* Test Id1 */
    {1, 5, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQMIPS, RELMIPS, RELMIPS,  DREG}
    }
};


#define MIPS_03_NAME    "mips_03"
#define MIPS_03_DETAILS "extra release should failed."

/** 
  @ingroup MIPS 
  @test mips_03
  @brief Each release should result in a call to the node.

	
*/

/* MIPS_03 */
MMPM_STATUS Test_Mips_03(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", MIPS_03_NAME, MIPS_03_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testmips3)/sizeof(AdsppmTestType);
    sts = InvokeTest(testmips3, numTest, testSts);
 
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "MIPS_03 - %s,%d\n",result[!sts], sts);

    return sts;
}



/* SLEEP_01 */
/* Test Sleep latency for LPASS CORES */
AdsppmTestType testsleepLatency1 [] =
{
    /* Test Id1 */   
    {1, 4, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },
       /* Test Id2 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_LPM, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },
       /* Test Id3 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },
      /* Test Id4 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AIF, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },
     /* Test Id5 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },
    /* Test Id6 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },
  /* Test Id6 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },

      /* Test Id7 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },

      /* Test Id8 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },
      /* Test Id9 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_SRAM, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, DREG}
    },
};


#define SLEEP_01_NAME    "sleep_01"
#define SLEEP_01_DETAILS "Verify node call is done to sleep latency node."

/** 
  @ingroup Sleep 
  @test sleep_01
  @brief Verify node call is done to sleep latency node.

	
*/

/* Sleep_01 */
MMPM_STATUS Test_Sleep_01(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", SLEEP_01_NAME, SLEEP_01_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testsleepLatency1)/sizeof(AdsppmTestType);
    sts = InvokeTest(testsleepLatency1, numTest, testSts);

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Sleep_01 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* SLEEP_02 */
/* Test Sleep latency for mutiple clients */
AdsppmTestType testsleepLatency2 [] =
{
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY}
    },
    /* Test Id2 */
    {2, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY}
    },
    /* Test Id3 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {RELSPATENCY, DREG}
    },
    /* Test Id4 */
    {2, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {RELSPATENCY, DREG}
    }
};

#define SLEEP_02_NAME    "sleep_02"
#define SLEEP_02_DETAILS "Verify multiple clients of the same owner ID are handled.  Each request/release is independent."

/** 
  @ingroup Sleep 
  @test sleep_02
  @brief Verify multiple clients of the same owner ID are handled.  Each request/release is independent.

	
*/

/* Sleep_02 */
MMPM_STATUS Test_Sleep_02(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", SLEEP_02_NAME, SLEEP_02_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testsleepLatency2)/sizeof(AdsppmTestType);
    sts = InvokeTest(testsleepLatency2, numTest, testSts);
 
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Sleep_02 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* SLEEP_03 */
/* Test Sleep latency . Extra release should be success since all calls go to sleep node */
AdsppmTestType testsleepLatency3 [] =
{
    /* Test Id1 */
    {1, 5, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQSLATENCY, RELSPATENCY, RELSPATENCY,  DREG}
    }
};


#define SLEEP_03_NAME    "sleep_03"
#define SLEEP_03_DETAILS "Each release should result in a call to the node."

/** 
  @ingroup Sleep 
  @test sleep_03
  @brief Each release should result in a call to the node.

	
*/

/* Sleep_03 */
MMPM_STATUS Test_Sleep_03(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", SLEEP_03_NAME, SLEEP_03_DETAILS);
 
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testsleepLatency3)/sizeof(AdsppmTestType);
    sts = InvokeTest(testsleepLatency3, numTest, testSts);
 
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Sleep_03 - %s,%d\n",result[!sts], sts);

    return sts;
}


void Test_MipsSleep() 
{

	 /* SleepLatency Tests */
    /* SLEEP_01 */
    Test_Sleep_01();
	
	  /* MIPS tests */
    /* MIPS_01 */
    Test_Mips_01();

#ifndef TEST_PROFILING
    /* SLEEP_02 */
    Test_Sleep_02();

   /* SLEEP_03 */
    Test_Sleep_03();


    /* MIPS_02 */
    Test_Mips_02();

    /* MIPS_03 */
    Test_Mips_03();
#endif


   
}
