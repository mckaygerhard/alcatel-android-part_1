/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include <stdio.h>
#include <string.h>
#include "mmpm.h"
#include "adsppm_test.h"
#include "adsppm_test_main.h"
#include "adsppm_test_param.h"
#include "adsppm_test_utils.h"

extern const uint32 gTestParamIndex[MMPM_CORE_ID_LPASS_END - MMPM_CORE_ID_LPASS_START][MMPM_CORE_INSTANCE_MAX];
extern const Mmpm2ReqBwTestParamTableType gReqBwParam[];
extern const uint32 gAxiClkMap[MAX_CORE][MAX_AXI_CLK];
extern char *result[2];

/* Bandwidth_01 */
/* BW request */
AdsppmTestType testbw1 [] =
{
    /* Test Id1 */
    {1, 4, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQBW, RELBW, DREG}
    },
	/*{1, 4, 0, MMPM_CORE_ID_LPASS_LPM, MMPM_CORE_INSTANCE_0,
       {REG, REQBW, RELBW, DREG}
    },*/
	{1, 4, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, REQBW, RELBW, DREG}
    },
	{1, 4, 0, MMPM_CORE_ID_LPASS_AIF, MMPM_CORE_INSTANCE_0,
       {REG, REQBW, RELBW, DREG}
    },
	{1, 4, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
       {REG, REQBW, RELBW, DREG}
    },
    {1, 4, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQBW, RELBW, DREG}
    },
	/*{1, 4, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG, REQBW, RELBW, DREG}
    },*/
    {1, 4, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, REQBW, RELBW, DREG}
    },
    /* {1, 4, 0, MMPM_CORE_ID_LPASS_SRAM, MMPM_CORE_INSTANCE_0,
       {REG, REQBW, RELBW, DREG}
    }*/
};


#define BW_01_NAME    "bw_01"
#define BW_01_DETAILS "Verify BW can be requests & cleared. Verify that the bus & memory clocks changed."

/** 
  @ingroup Bandwidth 
  @test Bandwidth_01
  @brief Verify BW can be requests & cleared. Verify that the bus & memory clocks changed.  AXI clock enabled/cleared.

	
*/

/* Bandwidth_01 */
MMPM_STATUS Test_Bandwidth_01(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_01_NAME, BW_01_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testbw1)/sizeof(AdsppmTestType);
    sts = InvokeTest(testbw1, numTest, testSts);

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_01 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Bandwidth_02 */
/* Bw release without request. This should return failure */
AdsppmTestType testbw2 [] =
{
    /* Test Id1 */
    {1, 3, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, RELBW, DREG}
    }
};

#define BW_02_NAME    "bw_02"
#define BW_02_DETAILS "Verify calling release, without calling request.result should fail"

/** 
  @ingroup Bandwidth 
  @test Bandwidth_02
  @brief Verify calling release, without calling request.result should be failed
*/

/* Bandwidth_02 */
MMPM_STATUS Test_Bandwidth_02(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_02_NAME, BW_02_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testbw2)/sizeof(AdsppmTestType);
    sts = InvokeTest(testbw2, numTest, testSts);

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_02, should fail - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Bandwidth_03 */
/* For single client bad handle request */
MMPM_STATUS TestBwSingleClientBadHandleReq(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    BwReqTestType bw;
    uint32 coreId, instanceId, index, clientId = 0;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        bw.masterPort = gReqBwParam[index].bw[0].masterPort;
        bw.slavePort = gReqBwParam[index].bw[0].slavePort;
        bw.bwVal = gReqBwParam[index].bw[0].bwVal;
        bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
        bw.usageType = gReqBwParam[index].bw[0].usageType;

        sts = Test_RequestBw(clientId + 4, &bw, 1);
        if (MMPM_STATUS_BADHANDLE != sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW bad handle request\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId);
		if (MMPM_STATUS_BADHANDLE != sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW Test_Deregister\n");
            retSts = MMPM_STATUS_FAILED;
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}


#define BW_03_NAME    "bw_03"
#define BW_03_DETAILS "Verify incorrect handles. result should fail"

/** 
  @ingroup Bandwidth 
  @test Bandwidth_03
  @brief Verify incorrect handles.

	
*/

/* Bandwidth_03 */
MMPM_STATUS Test_Bandwidth_03(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_03_NAME, BW_03_DETAILS);
    sts = TestBwSingleClientBadHandleReq();

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_03, should fail - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Bandwidth_04 */
/* For single client multiple request and single release should release AXI clk */
MMPM_STATUS TestBwSingleClientMultiReqAxiClk(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    BwReqTestType bw;
    uint32 coreId, instanceId, index, clientId = 0;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        bw.masterPort = gReqBwParam[index].bw[0].masterPort;
        bw.slavePort = gReqBwParam[index].bw[0].slavePort;
        bw.bwVal = gReqBwParam[index].bw[0].bwVal;
        bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
        bw.usageType = gReqBwParam[index].bw[0].usageType;

        sts = Test_RequestBw(clientId, &bw, 1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
           bw.masterPort = gReqBwParam[index].bw[0].masterPort;
           bw.slavePort = gReqBwParam[index].bw[0].slavePort;
           bw.bwVal = gReqBwParam[index].bw[0].bwVal + 100000000;
           bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
           bw.usageType = gReqBwParam[index].bw[0].usageType;

           sts = Test_RequestBw(clientId, &bw, 1);
           if (MMPM_STATUS_SUCCESS == sts)
           {
               sts = Test_ReleaseBw(clientId);
          
              if(MMPM_STATUS_SUCCESS != sts)
              {
                  ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW release\n");
                  retSts = MMPM_STATUS_FAILED;
              }
           }
           else
           {
               ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW request2\n");
               retSts = MMPM_STATUS_FAILED;
           }
        }
        else
        {
           ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW request1\n");
           retSts = MMPM_STATUS_FAILED;
        }
		sts = Test_Deregister(clientId);
		if(MMPM_STATUS_SUCCESS != sts)
		  {
			  ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW Test_Deregister\n");
			  retSts = MMPM_STATUS_FAILED;
		  }
		
    } 
    else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
    return retSts;
}

#define BW_04_NAME    "bw_04"
#define BW_04_DETAILS "Verify a single release with multiple requests disables clock."

/** 
  @ingroup Bandwidth 
  @test Bandwidth_04
  @brief Verify a single release with multipel requests disables clock.

	
*/

/* Bandwidth_04 */
MMPM_STATUS Test_Bandwidth_04(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_04_NAME, BW_04_DETAILS);
    sts = TestBwSingleClientMultiReqAxiClk();

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_04 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Bandwidth_05 */
/* For single client bad parameter test */
MMPM_STATUS TestBwSingleClientBadParam(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    BwReqTestType bw;
    uint32 coreId, instanceId, index, clientId = 0;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        /* Bad master port */
        bw.masterPort = 255;
        bw.slavePort = gReqBwParam[index].bw[0].slavePort;
        bw.bwVal = gReqBwParam[index].bw[0].bwVal;
        bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
        bw.usageType = gReqBwParam[index].bw[0].usageType;

        sts = Test_RequestBw(clientId, &bw, 1);
        if (MMPM_STATUS_BADPARM != sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW bad masterport param testing\n");
            retSts = MMPM_STATUS_FAILED;
        }
        if (MMPM_STATUS_SUCCESS == retSts)
        {
            /* Bad axi port */
            bw.masterPort = gReqBwParam[index].bw[0].masterPort;
            bw.slavePort = 255;
            bw.bwVal = gReqBwParam[index].bw[0].bwVal;
            bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
            bw.usageType = gReqBwParam[index].bw[0].usageType;

            sts = Test_RequestBw(clientId, &bw, 1);
            if (MMPM_STATUS_BADPARM != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW bad slaveport param testing\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        if (MMPM_STATUS_SUCCESS == retSts)
        {
            /* Bad usage type */
            bw.masterPort = gReqBwParam[index].bw[0].masterPort;
            bw.slavePort = gReqBwParam[index].bw[0].slavePort;
            bw.bwVal = gReqBwParam[index].bw[0].bwVal;
            bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
            bw.usageType = 100; 

            sts = Test_RequestBw(clientId, &bw, 1);
            if (MMPM_STATUS_BADPARM != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW bad usage type param testing\n");
                retSts = MMPM_STATUS_FAILED;
            }
        }
        sts = Test_Deregister(clientId);
		if(MMPM_STATUS_SUCCESS != sts)
		  {
			  ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW Test_Deregister\n");
			  retSts = MMPM_STATUS_FAILED;
		  }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}


#define BW_05_NAME    "bw_05"
#define BW_05_DETAILS "Verify bad parameters: masterport, slaveport, BW values, percentage and usage  type. verify log entries. result should be failed"

/** 
  @ingroup Bandwidth 
  @test Bandwidth_05
  @brief Verify bad parameters: memory ID, port ID, BW values, percentage and usage  type. verify log entries.


*/

/* Bandwidth_05 */
MMPM_STATUS Test_Bandwidth_05(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_05_NAME, BW_05_DETAILS);
    sts = TestBwSingleClientBadParam();

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_05 - %s,%d\n",result[!sts], sts);
    return sts;
}


/* Bandwidth_06 */
/* For single client BW request - release - check ahb clock */
MMPM_STATUS TestBwSingleClientRelAhbClk(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    BwReqTestType bw;
    uint32 coreId, instanceId, index, clientId = 0;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        bw.masterPort = gReqBwParam[index].bw[0].masterPort;
        bw.slavePort = gReqBwParam[index].bw[0].slavePort;
        bw.bwVal = gReqBwParam[index].bw[0].bwVal;
        bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
        bw.usageType = gReqBwParam[index].bw[0].usageType;

        sts = Test_RequestBw(clientId, &bw, 1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            sts = Test_ReleaseBw(clientId);
            //TODO: need to verify AHB clock
            if(MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW release\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } 
        else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW request\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId);
		if(MMPM_STATUS_SUCCESS != sts)
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW Test_Deregister\n");
                retSts = MMPM_STATUS_FAILED;
            }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}


#define BW_06_NAME    "bw_06"
#define BW_06_DETAILS "For single client BW request - release - check AHB clk is disabled."

/** 
  @ingroup Bandwidth 
  @test Bandwidth_06
  @brief For single client BW request - release - check ahb clk is disabled.

	
*/

/* Bandwidth_06 */
MMPM_STATUS Test_Bandwidth_06(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_06_NAME, BW_06_DETAILS);
    sts = TestBwSingleClientRelAhbClk();

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_06 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Bandwidth_07 */
/* For single client check request BW enables ahb clk */
MMPM_STATUS TestBwSingleClientReqAhbClk(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    BwReqTestType bw;
    uint32 coreId, instanceId, index, clientId = 0;

    coreId = MMPM_CORE_ID_LPASS_AIF;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        bw.masterPort = gReqBwParam[index].bw[0].masterPort;
        bw.slavePort = gReqBwParam[index].bw[0].slavePort;
        bw.bwVal = gReqBwParam[index].bw[0].bwVal;
        bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
        bw.usageType = gReqBwParam[index].bw[0].usageType;

        sts = Test_RequestBw(clientId, &bw, 1);
        //TODO: verify AHB clock is enabled
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW request\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId);
		if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in BW Test_Deregister\n");
            retSts = MMPM_STATUS_FAILED;
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}


#define BW_07_NAME    "bw_07"
#define BW_07_DETAILS "Verify AHB gates can be enabled.  Check that BW requests keep AHB on."

/** 
  @ingroup Bandwidth 
  @test Bandwidth_07
  @brief Verify AXI gates can be enabled.  Check that BW requests keep AHB on.

	
*/

/* Bandwidth_08 */
MMPM_STATUS Test_Bandwidth_07(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_07_NAME, BW_07_DETAILS);
    sts = TestBwSingleClientReqAhbClk();
 
    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_07 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Bandwidth_08 */
/* For multiple clients - Request1 - request2 - release 1. AHB clk should not be released */
MMPM_STATUS TestBwMultiClientRelTest1AhbClk(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    BwReqTestType bw, bw2;
    uint32 coreId, instanceId, index, clientId1 = 0, clientId2 = 0;

    coreId = MMPM_CORE_ID_LPASS_DML;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        bw.masterPort = gReqBwParam[index].bw[0].masterPort;
        bw.slavePort = gReqBwParam[index].bw[0].slavePort;
        bw.bwVal = gReqBwParam[index].bw[0].bwVal;
        bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
        bw.usageType = gReqBwParam[index].bw[0].usageType;

        sts = Test_RequestBw(clientId1, &bw, 1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            bw2.masterPort = gReqBwParam[index].bw[1].masterPort;
            bw2.slavePort = gReqBwParam[index].bw[1].slavePort;
            bw2.bwVal = gReqBwParam[index].bw[1].bwVal;
            bw2.usagePercent = gReqBwParam[index].bw[1].usagePercent;
            bw2.usageType = gReqBwParam[index].bw[1].usageType;

            sts = Test_RequestBw(clientId2, &bw2, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_ReleaseBw(clientId1);
                //TODO: verify AHB clock is still on
                if(MMPM_STATUS_SUCCESS != sts)
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW release\n");
                    retSts = MMPM_STATUS_FAILED;
                }
            } 
            else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2ns BW request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } 
        else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW request\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId1);
		if(MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW Test_Deregister1\n");
			retSts = MMPM_STATUS_FAILED;
		}
        sts = Test_Deregister(clientId2);
		if(MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW Test_Deregister2\n");
			retSts = MMPM_STATUS_FAILED;
		}
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}


#define BW_08_NAME    "bw_08"
#define BW_08_DETAILS "For multiple clients - Request1 - request2 - release 1. AHB clk should not be released."

/** 
  @ingroup Bandwidth 
  @test Bandwidth_08
  @brief For multiple clients - Request1 - request2 - release 1. Ahb clk should not be released.

	
*/

/* Bandwidth_08 */
MMPM_STATUS Test_Bandwidth_08(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_08_NAME, BW_08_DETAILS);
    sts = TestBwMultiClientRelTest1AhbClk();

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_08 - %s,%d\n",result[!sts], sts);
    return sts;
}


/* Bandwidth_09 */
/* For multiple clients - Req1- req2 - release1 - rel 2 -ahb clk should be released */
MMPM_STATUS TestBwMultiClientRelTest2AhbClk(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    BwReqTestType bw, bw2;
    uint32 coreId, instanceId, index, clientId1 = 100, clientId2 = 0;

    coreId = MMPM_CORE_ID_LPASS_AIF;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId1 = Test_Register(coreId, instanceId);
    clientId2 = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId1 && clientId2)
    {
        bw.masterPort = gReqBwParam[index].bw[0].masterPort;
        bw.slavePort = gReqBwParam[index].bw[0].slavePort;
        bw.bwVal = gReqBwParam[index].bw[0].bwVal;
        bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
        bw.usageType = gReqBwParam[index].bw[0].usageType;

        sts = Test_RequestBw(clientId1, &bw, 1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            bw2.masterPort = gReqBwParam[index].bw[1].masterPort;
            bw2.slavePort = gReqBwParam[index].bw[1].slavePort;
            bw2.bwVal = gReqBwParam[index].bw[1].bwVal;
            bw2.usagePercent = gReqBwParam[index].bw[1].usagePercent;
            bw2.usageType = gReqBwParam[index].bw[1].usageType;

            sts = Test_RequestBw(clientId2, &bw2, 1);
            if (MMPM_STATUS_SUCCESS == sts)
            {
                sts = Test_ReleaseBw(clientId1);
                //TODO: verify AHB clock
                if(MMPM_STATUS_SUCCESS == sts)
                {
                    sts = Test_ReleaseBw(clientId2);
                    if(MMPM_STATUS_SUCCESS != sts)
                    {
                        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2nd BW release\n");
                        retSts = MMPM_STATUS_FAILED;
                    }
                } 
                else
                {
                    ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2st BW release\n");
                    retSts = MMPM_STATUS_FAILED;
                }
            } 
            else
            {
                ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 2st BW request\n");
                retSts = MMPM_STATUS_FAILED;
            }
        } else
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW request\n");
            retSts = MMPM_STATUS_FAILED;
        }
       sts = Test_Deregister(clientId1);
		if(MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW Test_Deregister1\n");
			retSts = MMPM_STATUS_FAILED;
		}
        sts = Test_Deregister(clientId2);
		if(MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW Test_Deregister2\n");
			retSts = MMPM_STATUS_FAILED;
		}
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}

#define BW_09_NAME    "bw_09"
#define BW_09_DETAILS "For multiple clients - Req1- req2 - rel1 - rel2 -ahb clk should be released."

/** 
  @ingroup Bandwidth 
  @test Bandwidth_08
  @brief For multiple clients - Req1- req2 - rel1 - rel2 -axi clk should be released.

	
*/

/* Bandwidth_09 */
MMPM_STATUS Test_Bandwidth_09(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_09_NAME, BW_09_DETAILS);
    sts = TestBwMultiClientRelTest2AhbClk();

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_09 - %s,%d\n",result[!sts], sts);

    return sts;
}



/* Bandwidth_10 */
/* Multiple cilent BW request test */
AdsppmTestType testbw10 [] =
{
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQBW}
    },

    {2, 2, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, REQBW}
    },

    {3, 2, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, REQBW}
    },

    {4, 2, 0, MMPM_CORE_ID_LPASS_AIF, MMPM_CORE_INSTANCE_0,
       {REG, REQBW}
    },

    {5, 2, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
       {REG, REQBW}
    },

    {6, 2, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, REQBW}
    },

	{1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {RELBW, DREG}
    },

    {2, 2, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {RELBW, DREG}
    },

	{3, 2, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {RELBW, DREG}
    },

    {4, 2, 0, MMPM_CORE_ID_LPASS_AIF, MMPM_CORE_INSTANCE_0,
       {RELBW, DREG}
    },

	{5, 2, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
       {RELBW, DREG}
    },

    {6, 2, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {RELBW, DREG}
    }    
};


#define BW_10_NAME    "bw_10"
#define BW_10_DETAILS "Verify Multiple client BW can be requests & released."

/** 
  @ingroup Bandwidth 
  @test Bandwidth_09
  @brief Verify Multiple client BW can be requests & released.

  Uses the testbw15[] data structure and calls InvokeTest().
	This will call:
		- MMPM_Register_Ext_Ext
	 	- MMPM_Request - using MMPM_RSC_ID_MEM_BW resource ID
	 	- MMPM_Request - using MMPM_RSC_ID_MEM_BW resource ID
	 	- MMPM_Deregister_Ext_Ext

	 The tests verifies mulitiple clients can request and release bandwidth. This tests uses six clients
     and MMPM_CORE_ID_MDP core Id. This also verifies a maximum array of sixteen bandwidth request can 
     be done in a single client request.

	 A pass condition is when then MMPM returns success for all calls and there is no hang condition.

  @param none

  @return MMPM_STATUS_SUCCESS Pass. Any other status return is considered failure.
  @return All other MMPM status Fail due to MMPM error
  @return -1 Fail due to system hang or apps hang

  @see InvokeTest()
  @see testbw10[]
*/

/* Bandwidth_10 */
MMPM_STATUS Test_Bandwidth_10(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_10_NAME, BW_10_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testbw10)/sizeof(AdsppmTestType);
    sts = InvokeTest(testbw10, numTest, testSts);
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_10 - %s,%d\n",result[!sts], sts);

    return sts;
}




/* Bandwidth_11 */
/* For testing duplicate bw request filtering from mmpm client 
Need to verify MMPM log to check if the duplicate requests are filtered 
correctly.*/
MMPM_STATUS TestBwDupReq(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    BwReqTestType bw;
    uint32 coreId, instanceId, index, clientId = 0;

    coreId = MMPM_CORE_ID_LPASS_ADSP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        bw.masterPort = gReqBwParam[index].bw[0].masterPort;
        bw.slavePort = gReqBwParam[index].bw[0].slavePort;
        bw.bwVal = gReqBwParam[index].bw[0].bwVal;
        bw.usagePercent = gReqBwParam[index].bw[0].usagePercent;
        bw.usageType = gReqBwParam[index].bw[0].usageType;

        sts = Test_RequestBw(clientId, &bw, 1);
        bw.masterPort = MMPM_BW_PORT_ID_DML_MASTER;
        sts = Test_RequestBw(clientId, &bw, 1);
        bw.masterPort = MMPM_BW_PORT_ID_AIF_MASTER;
        sts = Test_RequestBw(clientId, &bw, 1);
        bw.masterPort = MMPM_BW_PORT_ID_SLIMBUS_MASTER;
        sts = Test_RequestBw(clientId, &bw, 1);
        bw.masterPort = MMPM_BW_PORT_ID_MIDI_MASTER;
        sts = Test_RequestBw(clientId, &bw, 1);

        sts = Test_Deregister(clientId);
		if(MMPM_STATUS_SUCCESS != sts)
		{
			ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in 1st BW Test_Deregister\n");
			retSts = MMPM_STATUS_FAILED;
		}
		
    } 
    else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR, "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}


#define BW_11_NAME    "bw_11"
#define BW_11_DETAILS "Verify duplicate bw request are filtered correctly."

/** 
  @ingroup Bandwidth 
  @test Bandwidth_11
  @brief Verify duplicate bw request are filtered correctly for different physical address in the same memory bank.

  Uses TestBwDupReq() to invoke the test.
  This will call:
    - MMPM_Register_Ext_Ext
    - MMPM_Request - using MMPM_RSC_ID_MEM_BW resource ID
    - MMPM_Deregister_Ext_Ext

  The MMPM logs should be checked to verify the duplicate requests are filtered correctly
  at MMPM client library.

  A pass condition is when then MMPM returns success for all calls,
  and MMPM logs are verified that the duplicate calls are filtered correctly at MMPM client.

  @param bandwidth params

  @return MMPM_STATUS_SUCCESS Pass
  @return All other MMPM status Fail due to MMPM error
  
  @see TestBwDupReq()
*/

/* Bandwidth_11 */
MMPM_STATUS Test_Bandwidth_11(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_11_NAME, BW_11_DETAILS);
    sts = TestBwDupReq();

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_11 - %s,%d\n",result[!sts], sts);

    return sts;
}


/* Bandwidth_11 */
/* Verify duplicate bw request are filtered correctly for same bw request. 
Need to check MMPM logs to verify that the request are filtered correctly.  */
AdsppmTestType testbw12 [] =
{
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQBW}
    },
    {1, 1, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REQBW}
    },
    {2, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, REQBW}
    },
    {1, 1, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {RELBW}
    },
    {2, 1, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REQBW}
    },
    {1, 3, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REQBW, RELBW, DREG}
    },
    {2, 3, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REQBW, RELBW, DREG}
    }
};


#define BW_12_NAME    "bw_12"
#define BW_12_DETAILS "Verify duplicate bw request are filtered correctly for same bw request."

/** 
  @ingroup Bandwidth 
  @test Bandwidth_11
  @brief Verify duplicate bw request are filtered correctly for same bw request.

  Uses testbw11[] to invoke the test.
  This will call:
    - MMPM_Register_Ext_Ext
    - MMPM_Request - using MMPM_RSC_ID_MEM_BW resource ID
    - MMPM_Request - using MMPM_RSC_ID_MEM_BW resource ID
    - MMPM_Deregister_Ext_Ext

  The MMPM logs should be checked to verify the duplicate requests are filtered correctly
  at MMPM client library.Multiple clients reqeusts are used for the tests.

  A pass condition is when then MMPM returns success for all calls,
  and MMPM logs are verified that the duplicate calls are filtered correctly at MMPM client.

  @param bandwidth params

  @return MMPM_STATUS_SUCCESS Pass
  @return All other MMPM status Fail due to MMPM error
  
  @see testbw12[]
*/

/* Bandwidth_12 */
MMPM_STATUS Test_Bandwidth_12(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BW_12_NAME, BW_11_DETAILS);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testbw12)/sizeof(AdsppmTestType);
    sts = InvokeTest(testbw12, numTest, testSts);

    // TODO - make the result log a macro or function to be consistent for all tests.
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Bandwidth_12 - %s,%d\n",result[!sts], sts);

    return sts;
}


void Test_Bw(void) 
{

    /* Bandwidth_01 */
    Test_Bandwidth_01();

#ifndef TEST_PROFILING
    /* Bandwidth_02 */
    Test_Bandwidth_02();

    /* Bandwidth_03 */
    Test_Bandwidth_03();

    /* Bandwidth_04 */
    Test_Bandwidth_04();

    /* Bandwidth_05 */
    Test_Bandwidth_05();

    /* Bandwidth_06 */
    Test_Bandwidth_06();

    /* Bandwidth_07 */
    Test_Bandwidth_07();

    /* Bandwidth_08 */
    Test_Bandwidth_08();

    /* Bandwidth_09 */
    Test_Bandwidth_09();

    /* Bandwidth_10 */
    Test_Bandwidth_10();

    /* Bandwidth_11 */
    Test_Bandwidth_11();

    /* Bandwidth_12 */
    Test_Bandwidth_12();
#endif
}

