/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include <stdio.h>
#include <string.h>
#include "mmpm.h"
#include "adsppm_test.h"
#include "adsppm_test_main.h"
#include "adsppm_test_param.h"
#include "adsppm_test_utils.h"
#include <stdlib.h>



extern const uint32 gTestParamIndex[MMPM_CORE_ID_LPASS_END - MMPM_CORE_ID_LPASS_START][MMPM_CORE_INSTANCE_MAX];
extern const Mmpm2FreqPlanTestParamTableType gFreqPlanParam[];
extern char *result[2];

/* System_01 */
AdsppmTestType testsys1 [] =
{
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_SRAM, MMPM_CORE_INSTANCE_0,
       {REG, DREG}
    }
};

#define SYSTEM_01_NAME    "system_01"
#define SYSTEM_01_DETAILS "Checks MMPM log to ensure MMPM init has passed successfully."

/** 
  @ingroup System 
  @test system_01
  @brief Checks MMPM log to ensure MMPM init has passed successfully.

	
*/

/* System_01 */
MMPM_STATUS Test_System_01(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "Running %s test: %s\n", SYSTEM_01_NAME, SYSTEM_01_DETAILS);
    AdsppmTestDebugLogTestStart(SYSTEM_01_NAME, 0);
 
    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testsys1)/sizeof(AdsppmTestType);
    sts = InvokeTest(testsys1, numTest, testSts);
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "System_01 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}


/* System_02 */
AdsppmTestType testsys2 [] =
{
    /* Test Id1 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_ADSP, MMPM_CORE_INSTANCE_0,
       {REG, DREG}
    },

    /* Test Id2 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_LPM, MMPM_CORE_INSTANCE_1,
       {REG, DREG}
    },

     /* Test Id3 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_DML, MMPM_CORE_INSTANCE_0,
       {REG, DREG}
    },

     /* Test Id4 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_AIF, MMPM_CORE_INSTANCE_0,
       {REG, DREG}
    },
      /* Test Id5 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_SLIMBUS, MMPM_CORE_INSTANCE_0,
       {REG, DREG}
    },

     /* Test Id6 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_MIDI, MMPM_CORE_INSTANCE_0,
       {REG, DREG}
    },

      /* Test Id7 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_AVSYNC, MMPM_CORE_INSTANCE_0,
       {REG, DREG}
    },

     /* Test Id8 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_HWRSMP, MMPM_CORE_INSTANCE_0,
       {REG, DREG}
    },

     /* Test Id9 */
    {1, 2, 0, MMPM_CORE_ID_LPASS_SRAM, MMPM_CORE_INSTANCE_0,
       {REG, DREG}
    },
};

#define SYSTEM_02_NAME    "system_02"
#define SYSTEM_02_DETAILS "Calls MMPM_Register_Ext & MMPMDeregister, check return values are success and handle is non-zero."

/** 
  @ingroup System 
  @test system_02
  @brief Calls MMPM_Register_Ext_Ext & MMPMDeregister, check return values are success and handle is non-zero.

	
*/

/* System_02 */
MMPM_STATUS Test_System_02(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, testSts[MAX_TEST][MAX_TEST_SEQUENCE];
    uint32 numTest = 0;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "Running %s test: %s\n", SYSTEM_02_NAME, SYSTEM_02_DETAILS);
    AdsppmTestDebugLogTestStart(SYSTEM_02_NAME, 0);

    memset(testSts, 0,  sizeof(testSts));
    numTest = sizeof(testsys2)/sizeof(AdsppmTestType);
    sts = InvokeTest(testsys2, numTest, testSts);
    
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "System_02 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}


/* System_04 */
/* Test bad deregister */
MMPM_STATUS TestSysBadDereg(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    uint32 coreId, instanceId, clientId = 0;

    coreId = MMPM_CORE_ID_LPASS_HWRSMP;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);

    if (clientId)
    {
        sts = Test_Deregister(clientId + 1);
        if (MMPM_STATUS_SUCCESS == sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Deregister with a bad handle is successful\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Deregister failed\n");
            retSts = MMPM_STATUS_FAILED;
        }

    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }

    return retSts;
}

#define SYSTEM_04_NAME    "system_04"
#define SYSTEM_04_DETAILS "Calls MMPM_Deregister_Ext with an unused client handle.  Should fail."

/** 
  @ingroup System 
  @test system_04
  @brief Calls MMPM_Deregister_Ext_Ext with an unused client handle.  Should fail.

	
*/

/* System_04 */
MMPM_STATUS Test_System_04(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "Running %s test: %s\n", SYSTEM_04_NAME, SYSTEM_04_DETAILS);
    AdsppmTestDebugLogTestStart(SYSTEM_04_NAME, 0);

    sts = TestSysBadDereg();
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "System_04 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}


/* System_06 */
/* Test getinfo */
MMPM_STATUS TestSysGetInfo(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
    ClkTestType clk;
    uint32 coreId, instanceId, index, clientId = 0;

    coreId =  MMPM_CORE_ID_LPASS_MIDI;
    instanceId = MMPM_CORE_INSTANCE_0;
    clientId = Test_Register(coreId, instanceId);
    index = gTestParamIndex[coreId -MMPM_CORE_ID_LPASS_START ][instanceId];

    if (clientId)
    {
        clk.clkId = gFreqPlanParam[index].clkParam[0].clkId;
        clk.freq = gFreqPlanParam[index].clkParam[0].freq[0] * 1000;
        clk.freqMatch = MMPM_FREQ_AT_LEAST;

        sts = Test_RequestClk(clientId, &clk, 1);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in request\n");
            retSts = MMPM_STATUS_FAILED;
        }

        sts = Test_ReleaseClk(clientId, &clk.clkId, 1);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Release clk\n");
            retSts = MMPM_STATUS_FAILED;
        }
        sts = Test_Deregister(clientId);
        if (MMPM_STATUS_SUCCESS != sts)
        {
            ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Deregister failed\n");
            retSts = MMPM_STATUS_FAILED;
        }
    } else
    {
        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in Test_Register\n");
        retSts = MMPM_STATUS_FAILED;
    }
    return retSts;
}


#define SYSTEM_06_NAME    "system_06"
#define SYSTEM_06_DETAILS "Test GetInfo API.TODO"

/** 
  @ingroup System 
  @test system_06
  @brief Test GetInfo API.

	
*/

/* System_06 */
MMPM_STATUS Test_System_06(void)
{
    MMPM_STATUS sts = MMPM_STATUS_FAILED;

    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "Running %s test: %s\n", SYSTEM_06_NAME, SYSTEM_06_DETAILS);
    AdsppmTestDebugLogTestStart(SYSTEM_06_NAME, 0);

    sts = TestSysGetInfo();
 
    ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO, "System_06 - %s,%d\n",result[!sts], sts);
    AdsppmTestDebugLogTestDone(sts);

    return sts;
}




void Test_System(void) 
{
#ifndef TEST_PROFILING
    /* System_01 */
    Test_System_01();

    /* System_02 */
    Test_System_02();

    /* System_04 */
    Test_System_04();

    /* System_06 */
    Test_System_06();
#endif

}

