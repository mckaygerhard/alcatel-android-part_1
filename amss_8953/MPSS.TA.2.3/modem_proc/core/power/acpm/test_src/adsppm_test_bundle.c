/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

#include <stdio.h>
#include <string.h>
#include "mmpm.h"
#include "adsppm_test.h"
#include "adsppm_test_main.h"
#include "adsppm_test_param.h"
#include "adsppm_test_utils.h"

extern char *result[2];

// These are used in the test case logging, and in the menu text.
#define BUNDLE_01_NAME    "bundle_01"
#define BUNDLE_01_DETAILS "Verify bundle requests, releases."


/* Bundle_01 */
/* Test bundle requests/releases */
MMPM_STATUS Test_Bundle_01(void)
{
   MMPM_STATUS sts = MMPM_STATUS_FAILED, retSts = MMPM_STATUS_SUCCESS;
   uint32 clientId;
    
    MmpmRegParamType  mmpmRegParam;
    char  clientName[25];
	
	ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_INFO,  "Running %s test: %s\n", BUNDLE_01_NAME, BUNDLE_01_DETAILS);
	
    memset(&mmpmRegParam, 0,  sizeof(MmpmRegParamType));
    memset(clientName, 0, sizeof(clientName));
    strlcpy(clientName, "adsp", sizeof(clientName));  
    mmpmRegParam.rev = MMPM_REVISION;
    mmpmRegParam.coreId = MMPM_CORE_ID_LPASS_ADSP;
    mmpmRegParam.instanceId = MMPM_CORE_INSTANCE_0;
    mmpmRegParam.MMPM_Callback = NULL;
    mmpmRegParam.pClientName = clientName;
    mmpmRegParam.pwrCtrlFlag = PWR_CTRL_NONE;
    mmpmRegParam.callBackFlag = CALLBACK_NONE;
    clientId = MMPM_Register_Ext(&mmpmRegParam);

    if (0 != clientId)
    {
        MmpmRscExtParamType     mmpmRscExtParam;
        MmpmRscParamType        mmpmRscParam[8];
        MMPM_STATUS             stsArray[8];
        MmpmRegProgMatchType    mmpmRegProg = MMPM_REG_PROG_NORM;
        MmpmClkReqType          mmpmClkReq;
        MmpmClkValType          mmpmClkValArray[16];
        MmpmGenBwReqType           mmpmBwReq;
        MmpmGenBwValType           mmpmBwArray[16];

        memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));
        memset(&mmpmRscParam[0], 0,  sizeof(MmpmRscParamType)*8);
        memset(&stsArray[0], 0,  sizeof(MMPM_STATUS)*8);
        memset(&mmpmClkValArray[0], 0,  sizeof(MmpmClkValType)*16);
        memset(&mmpmBwArray[0], 0,  sizeof(MmpmBwValType)*16);

        //rscId = MMPM_RSC_ID_POWER
        {
            mmpmRscParam[0].rscId = MMPM_RSC_ID_POWER;
        }

        //rscId = MMPM_RSC_ID_REG_PROG
        {
            mmpmRscParam[1].rscId = MMPM_RSC_ID_REG_PROG;
            mmpmRscParam[1].rscParam.regProgMatch = mmpmRegProg;
        }

        //rscId = MMPM_RSC_ID_CORE_CLK
        {
            mmpmClkValArray[0].clkFreqHz = 20000000;
            mmpmClkValArray[0].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_HWRSP_CORE;
            mmpmClkValArray[0].freqMatch = MMPM_FREQ_AT_LEAST;
            mmpmClkValArray[1].clkFreqHz = 2700000;
            mmpmClkValArray[1].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_MIDI_CORE;
            mmpmClkValArray[1].freqMatch = MMPM_FREQ_AT_LEAST;
            mmpmClkValArray[2].clkFreqHz = 20000000;
            mmpmClkValArray[2].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_AVSYNC_XO;
            mmpmClkValArray[2].freqMatch = MMPM_FREQ_AT_LEAST;
			mmpmClkValArray[3].clkFreqHz = 20000000;
            mmpmClkValArray[3].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_AVSYNC_BT;
            mmpmClkValArray[3].freqMatch = MMPM_FREQ_AT_LEAST;
			mmpmClkValArray[4].clkFreqHz = 20000000;
            mmpmClkValArray[4].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_AVSYNC_FM;
            mmpmClkValArray[4].freqMatch = MMPM_FREQ_AT_LEAST;
            mmpmClkReq.numOfClk = 5; 
            mmpmClkReq.pClkArray = mmpmClkValArray;

            mmpmRscParam[2].rscId = MMPM_RSC_ID_CORE_CLK;
            mmpmRscParam[2].rscParam.pCoreClk = &mmpmClkReq;
        }

         //rscId = MMPM_RSC_ID_MEM_BW
        {
            mmpmBwArray[0].busRoute.masterPort = MMPM_BW_PORT_ID_ADSP_MASTER;
            mmpmBwArray[0].busRoute.slavePort = MMPM_BW_PORT_ID_AIF_SLAVE;
            mmpmBwArray[0].bwValue.busBwValue.bwBytePerSec = 5*1024;
            mmpmBwArray[0].bwValue.busBwValue.usagePercentage = 100;
            mmpmBwArray[0].bwValue.busBwValue.usageType = MMPM_BW_USAGE_LPASS_DSP;
            mmpmBwArray[1].busRoute.masterPort = MMPM_BW_PORT_ID_DML_MASTER;
            mmpmBwArray[1].busRoute.slavePort = MMPM_BW_PORT_ID_SLIMBUS_SLAVE;
            mmpmBwArray[1].bwValue.busBwValue.bwBytePerSec = 5*1024;
            mmpmBwArray[1].bwValue.busBwValue.usagePercentage = 100;
            mmpmBwArray[1].bwValue.busBwValue.usageType = MMPM_BW_USAGE_LPASS_DMA;
            mmpmBwArray[2].busRoute.masterPort = MMPM_BW_PORT_ID_AIF_MASTER;
            mmpmBwArray[2].busRoute.slavePort = MMPM_BW_PORT_ID_MIDI_SLAVE;
            mmpmBwArray[2].bwValue.busBwValue.bwBytePerSec = 5*1024;
            mmpmBwArray[2].bwValue.busBwValue.usagePercentage = 100;
            mmpmBwArray[2].bwValue.busBwValue.usageType = MMPM_BW_USAGE_LPASS_DSP;
            mmpmBwArray[3].busRoute.masterPort = MMPM_BW_PORT_ID_SLIMBUS_MASTER;
            mmpmBwArray[3].busRoute.slavePort = MMPM_BW_PORT_ID_SRAM_SLAVE;
            mmpmBwArray[3].bwValue.busBwValue.bwBytePerSec = 5*1024;
            mmpmBwArray[3].bwValue.busBwValue.usagePercentage = 100;
            mmpmBwArray[3].bwValue.busBwValue.usageType = MMPM_BW_USAGE_LPASS_DMA;
            mmpmBwReq.numOfBw = 4;
            mmpmBwReq.pBandWidthArray = mmpmBwArray;

            mmpmRscParam[3].rscId = MMPM_RSC_ID_MEM_BW;
            mmpmRscParam[3].rscParam.pGenBwReq = &mmpmBwReq;
        }

        mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
        mmpmRscExtParam.numOfReq = 4;
        mmpmRscExtParam.pReqArray = mmpmRscParam;
        mmpmRscExtParam.pStsArray = stsArray;

        sts = MMPM_Request_Ext(clientId, &mmpmRscExtParam);

            if (MMPM_STATUS_SUCCESS != sts)
			{
				  ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in MMPM_Request_Ext\n");
                  retSts = MMPM_STATUS_FAILED;
			}
			else
            {

               memset(&mmpmRscExtParam, 0,  sizeof(MmpmRscExtParamType));
               memset(&mmpmRscParam[0], 0,  sizeof(MmpmRscParamType)*8);
               memset(&stsArray[0], 0,  sizeof(MMPM_STATUS)*8);
               memset(&mmpmClkValArray[0], 0,  sizeof(MmpmClkValType)*16);
               memset(&mmpmBwArray[0], 0,  sizeof(MmpmBwValType)*16);

                //rscId = MMPM_RSC_ID_POWER
                {
                    mmpmRscParam[0].rscId = MMPM_RSC_ID_POWER;
                }

                //rscId = MMPM_RSC_ID_REG_PROG
                {
                    mmpmRscParam[1].rscId = MMPM_RSC_ID_REG_PROG;
                }

                 //rscId = MMPM_RSC_ID_CORE_CLK
        {
            mmpmClkValArray[0].clkFreqHz = 20000000;
            mmpmClkValArray[0].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_HWRSP_CORE;
            mmpmClkValArray[0].freqMatch = MMPM_FREQ_AT_LEAST;
            mmpmClkValArray[1].clkFreqHz = 2700000;
            mmpmClkValArray[1].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_MIDI_CORE;
            mmpmClkValArray[1].freqMatch = MMPM_FREQ_AT_LEAST;
            mmpmClkValArray[2].clkFreqHz = 20000000;
            mmpmClkValArray[2].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_AVSYNC_XO;
            mmpmClkValArray[2].freqMatch = MMPM_FREQ_AT_LEAST;
			mmpmClkValArray[3].clkFreqHz = 20000000;
            mmpmClkValArray[3].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_AVSYNC_BT;
            mmpmClkValArray[3].freqMatch = MMPM_FREQ_AT_LEAST;
			mmpmClkValArray[4].clkFreqHz = 20000000;
            mmpmClkValArray[4].clkId.clkIdLpass = MMPM_CLK_ID_LPASS_AVSYNC_FM;
            mmpmClkValArray[4].freqMatch = MMPM_FREQ_AT_LEAST;
            mmpmClkReq.numOfClk = 5; 
            mmpmClkReq.pClkArray = mmpmClkValArray;

            mmpmRscParam[2].rscId = MMPM_RSC_ID_CORE_CLK;
            mmpmRscParam[2].rscParam.pCoreClk = &mmpmClkReq;
        }

         //rscId = MMPM_RSC_ID_MEM_BW
        {
            mmpmBwArray[0].busRoute.masterPort = MMPM_BW_PORT_ID_ADSP_MASTER;
            mmpmBwArray[0].busRoute.slavePort = MMPM_BW_PORT_ID_AIF_SLAVE;
            mmpmBwArray[0].bwValue.busBwValue.bwBytePerSec = 5*1024;
            mmpmBwArray[0].bwValue.busBwValue.usagePercentage = 100;
            mmpmBwArray[0].bwValue.busBwValue.usageType = MMPM_BW_USAGE_LPASS_DSP;
            mmpmBwArray[1].busRoute.masterPort = MMPM_BW_PORT_ID_DML_MASTER;
            mmpmBwArray[1].busRoute.slavePort = MMPM_BW_PORT_ID_SLIMBUS_SLAVE;
            mmpmBwArray[1].bwValue.busBwValue.bwBytePerSec = 5*1024;
            mmpmBwArray[1].bwValue.busBwValue.usagePercentage = 100;
            mmpmBwArray[1].bwValue.busBwValue.usageType = MMPM_BW_USAGE_LPASS_DMA;
            mmpmBwArray[2].busRoute.masterPort = MMPM_BW_PORT_ID_AIF_MASTER;
            mmpmBwArray[2].busRoute.slavePort = MMPM_BW_PORT_ID_MIDI_SLAVE;
            mmpmBwArray[2].bwValue.busBwValue.bwBytePerSec = 5*1024;
            mmpmBwArray[2].bwValue.busBwValue.usagePercentage = 100;
            mmpmBwArray[2].bwValue.busBwValue.usageType = MMPM_BW_USAGE_LPASS_DSP;
            mmpmBwArray[3].busRoute.masterPort = MMPM_BW_PORT_ID_SLIMBUS_MASTER;
            mmpmBwArray[3].busRoute.slavePort = MMPM_BW_PORT_ID_SRAM_SLAVE;
            mmpmBwArray[3].bwValue.busBwValue.bwBytePerSec = 5*1024;
            mmpmBwArray[3].bwValue.busBwValue.usagePercentage = 100;
            mmpmBwArray[3].bwValue.busBwValue.usageType = MMPM_BW_USAGE_LPASS_DMA;
            mmpmBwReq.numOfBw = 4;
            mmpmBwReq.pBandWidthArray = mmpmBwArray;

            mmpmRscParam[3].rscId = MMPM_RSC_ID_MEM_BW;
            mmpmRscParam[3].rscParam.pGenBwReq = &mmpmBwReq;
        }

                mmpmRscExtParam.apiType = MMPM_API_TYPE_SYNC;
                mmpmRscExtParam.numOfReq = 4;
                mmpmRscExtParam.pReqArray = mmpmRscParam;
                mmpmRscExtParam.pStsArray = stsArray;

                sts = MMPM_Release_Ext(clientId, &mmpmRscExtParam);
				if (MMPM_STATUS_SUCCESS != sts)
                    {
                        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in MMPM_Release_Ext\n");
                        retSts = MMPM_STATUS_FAILED;
                    }
           
            }
        

        sts = MMPM_Deregister_Ext(clientId);
		if (MMPM_STATUS_SUCCESS != sts)
                    {
                        ADSPPM_TESTLOG_PRINTF_0( ADSPPMTEST_LOG_LEVEL_ERROR,  "Error in MMPM_Deregister_Ext\n");
                        retSts = MMPM_STATUS_FAILED;
                    }
           
    }

	ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_RESULT,  "Test_Bundle_01 - %s,%d\n",result[!sts], sts);
    return retSts;
}


void Test_Bundle(void)
{

    /* Bundle_01 */
    Test_Bundle_01();
}


