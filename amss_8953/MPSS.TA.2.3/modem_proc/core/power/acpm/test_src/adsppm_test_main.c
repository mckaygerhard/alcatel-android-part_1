/*
* Copyright (c) 2013 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/


#include <stdio.h>
#include "ULog.h"
#include "ULogFront.h"
#include "DALSys.h"

#include "mmpm.h"
#include "adsppm_test_main.h"
#include "adsppm_test_utils.h"


#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>

/*
  ADSPPM Test App.
 
    Doxygen is used to create the test documentation.  Please
    use the following conventions for commenting the test code
    in order to autogenerate the MMPM 2 test documentation.
 
    1. Use @ingroup to group the tests together
    2. Use @test to define the MMPM test ID, this must match the ID in the CSV output
    3. Use @brief to define a 1 line desription of the test case.
 
    The test results are collected in a CSV file that is
    imported into an Excel document used for presentation of
    weekly results.  The CSV output contains the following:
  
    ID, Description, result, data, Feature ID, Details
  
    Where:
      ID = the MMPM test ID, this should match @test value
      Description = Very short test description, fucntion name should be sufficient
      result = "Pass" or "Fail" if run
      data = Any relavent test results or data, i.e. Failure reason, clock freq., or execution time
      Feature ID = Reference back the MMPM Feature ID that this test is verifying
      details = 1 line text of test details, should match @brief value.
 
 */

/* Define the groups that Doxygen will used to group the test cases & functions */  
 /**
  * @defgroup System        System: MMPM Common and System related tests.
  * @defgroup Power         Power: GDHS test cases using MMPM_RSC_ID_POWER.
  * @defgroup PMICVreg      PMICVreg: External power rail test cases using MMPM_RSC_ID_VREG.
  * @defgroup RegisterProg  RegisterProg: AHB test cases using MMPM_RSC_ID_REG_PROG.
  * @defgroup Clock         Clock: Core clock request test cases using MMPM_RSC_ID_CORE_CLK.
  * @defgroup ClockDomain   ClockDomain: Core clock request test cases using MMPM_RSC_ID_CORE_CLK_DOMAIN.  
  * @defgroup Bandwidth     Bandwidth: Bandwidth requests and L2 Cache voting test cases using MMPM_RSC_ID_MEM_BW test cases.
  * @defgroup MIPS          MIPS: test cases using MMPM_RSC_ID_MIPS.
  * @defgroup Sleep         Sleep: Sleep latency voting test cases using MMPM_RSC_ID_SLEEP_LATENCY. 
  * @defgroup StaticScreen  StaticScreen: Static sceen and idle state test cases using MMPM_RSC_ID_IDLE_STATE.
  * @defgroup EXT           EXT: MMPM dependency (CLKRGM, Bus arb., etc.) test cases.  Timing values and external dependency checks.
  */

#define MAX_LOG_LINE 512
#define ADSPPM_TESTULOG_BUFFER_SIZE 15535
#define ADSPPM_TESTULOG_LEVEL ADSPPMTEST_LOG_LEVEL_DEBUG




AdsppmTestGlbCtxType gAdsppmTestCtx;

__inline ULogHandle GetTestUlogHandle(void)
{
   return 	gAdsppmTestCtx.test_hLog;
}

// Globals
char *result[2] =
{
    "Fail",
    "Pass"
};


void RunAllTests(void)
{

    /* Invoke adsppm clock tests */
    Test_Clock();

    /* Invoke adsppm mem power retention*/
	Test_MemPower();
 
   
	/* Invoke adsppm sleep and mips tests */
    Test_MipsSleep();

	/* Invoke adsppm Bandwidth tests */
    Test_Bw();
	

	/* Invoke adsppm register programming and axi enable tests */
    Test_AxiAhb();


    /* Invoke adsppm system tests */
    Test_System();

    /* Invoke adsppm power tests */
    Test_Power();

	 /* Invoke adsppm bundle tests */
    Test_Bundle();

    /* Invoke adsppm stress tests */
    Test_Stress();
   
}


/* MMPM2 test main() routine from where all the tests are invoked */
void adsppmtest_main(void)
{
	gAdsppmTestCtx.test_log_buffer_size = ADSPPM_TESTULOG_BUFFER_SIZE;
	gAdsppmTestCtx.testLogLevel = ADSPPM_TESTULOG_LEVEL;
#ifndef NO_ULOG
	if(DAL_SUCCESS == ULogFront_RealTimeInit(&gAdsppmTestCtx.test_hLog,
			"ADSPPMTEST Log",
			gAdsppmTestCtx.test_log_buffer_size,
			ULOG_MEMORY_LOCAL,
			ULOG_LOCK_OS ))
#endif
	{
	 RunAllTests();  
	}
}


// The Test start and exit log functions
void AdsppmTestDebugLogTestStart(char *name, int progmax)
{ 
  ADSPPM_TESTLOG_PRINTF_2( ADSPPMTEST_LOG_LEVEL_ERROR, "start testing %s %d\n", name, progmax);   
}

void AdsppmTestDebugLogTestUpdate(int progress)
{
	ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR, "testing %d\n", progress);   
}

void AdsppmTestDebugLogTestDone(int error)
{
	ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR, "testing done = %d\n", error);  
}


/* Adsppm test logging function */
/*void AdsppmTestDebugLog(char *format, ... )
{
	
    va_list args;
    char logStr[MAX_LOG_LINE+1];

    memset(logStr, 0, MAX_LOG_LINE+1);
    // Process the variable aruments into a string.
    va_start( args, format );
    vsnprintf( logStr, MAX_LOG_LINE, format, args );
    va_end( args );
	ADSPPM_TESTLOG_PRINTF_1( ADSPPMTEST_LOG_LEVEL_ERROR, "%s \n", logStr);  
}*/
