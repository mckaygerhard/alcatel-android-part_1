/*===========================================================================

  Copyright (c) 2015-2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include "cpr_enablement.h"
#include "cpr_device_hw_version.h"
#include "cpr_voltage_ranges.h"
#include "cpr_qfprom.h"
#include "HALhwio.h"
#include "CoreVerify.h"



////////////////////////////////////////////////
// Cx config
////////////////////////////////////////////////

static const cpr_enablement_versioned_rail_config_t cx_8940_versioned_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            {CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(0,0), CPR_CHIPINFO_VERSION(0xFF, 0xFF),0, 0xFF },
            {CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF },
        },
        .foundry_range_count = 2,
    },
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,  //read the fuses, but we won't actually set any voltages.
};

static const cpr_enablement_rail_config_t cx_8940_cpr_enablement =
{
    .rail_id = CPR_RAIL_CX,
    .versioned_rail_config = (const cpr_enablement_versioned_rail_config_t*[])
    {
        &cx_8940_versioned_cpr_enablement,
    },
    .versioned_rail_config_count = 1,
};



////////////////////////////////////////////////
// 8940 MSS config
////////////////////////////////////////////////

static const cpr_enablement_versioned_rail_config_t mss_8940_TSMC_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            { CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF ,DALCHIPINFO_FAMILY_MSM8940},
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF ,DALCHIPINFO_FAMILY_MSM8940},
        },
        .foundry_range_count = 2,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_GLOBAL_CEILING_VOLTAGE,
    .supported_level = (cpr_enablement_supported_level_t[])
    {                              /* Large intial values for testing*/
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS,              20,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS_L1,           40,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          13,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL_L1,       5,                   NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            15,                  NULL,                  1},
    },
    .supported_level_count = 5,
    .custom_voltage_fn = NULL,  // NULL ==  Use ordinary floor and ceiling calculation functions.
    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
    .aging_static_margin_limit = 13, //mV
};
static const cpr_enablement_versioned_rail_config_t mss_8940_GF_cpr_enablement_v1_0 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            { CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 2 ,DALCHIPINFO_FAMILY_MSM8940},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {                              /* Large intial values for testing*/
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS,              33,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS_L1,           13,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          51,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL_L1,       41,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            53,                  NULL,                  1},
    },
    .supported_level_count = 5,
    .custom_voltage_fn = NULL,  // NULL ==  Use ordinary floor and ceiling calculation functions.
    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
    .aging_static_margin_limit = 13, //mV
};

static const cpr_enablement_versioned_rail_config_t mss_8940_GF_cpr_enablement_v2_0 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            { CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 2, 0xFF ,DALCHIPINFO_FAMILY_MSM8940},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {                              /* Large intial values for testing*/
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS,              58,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS_L1,           65,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          50,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL_L1,       50,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            40,                  NULL,                  1},
    },
    .supported_level_count = 5,
    .custom_voltage_fn = NULL,  // NULL ==  Use ordinary floor and ceiling calculation functions.
    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
    .aging_static_margin_limit = 13, //mV
};

static const cpr_enablement_versioned_rail_config_t mss_8940_SMIC_cpr_enablement_v1_0 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            { CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF ,DALCHIPINFO_FAMILY_MSM8940},
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {                              /* Large intial values for testing*/
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS,              33,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS_L1,           52,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          36,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL_L1,       42,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            37,                  NULL,                  1},
    },
    .supported_level_count = 5,
    .custom_voltage_fn = NULL,  // NULL ==  Use ordinary floor and ceiling calculation functions.
    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
    .aging_static_margin_limit = 13, //mV
};

static const cpr_enablement_versioned_rail_config_t mss_8940_SMIC_cpr_enablement_v2_0 =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            { CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0x2, 0xFF,DALCHIPINFO_FAMILY_MSM8940 },
        },
        .foundry_range_count = 1,
    },
    .enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    //.enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {                              /* Large intial values for testing*/
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS,              33,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS_L1,           35,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          25,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL_L1,       47,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            36,                  NULL,                  1},
    },
    .supported_level_count = 5,
    .custom_voltage_fn = NULL,  // NULL ==  Use ordinary floor and ceiling calculation functions.
    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
    .aging_static_margin_limit = 13, //mV
};

////////////////////////////////////////////////
// 8920 MSS config
////////////////////////////////////////////////

static const cpr_enablement_versioned_rail_config_t mss_8920_TSMC_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            { CPR_FOUNDRY_SS, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF ,DALCHIPINFO_FAMILY_MSM8920 },
            { CPR_FOUNDRY_TSMC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF ,DALCHIPINFO_FAMILY_MSM8920 },
        },
        .foundry_range_count = 2,
    },
    //.enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {                              /* Large intial values for testing*/
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS,              20,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS_L1,           40,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          13,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL_L1,       5,                   NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            15,                  NULL,                  1},
    },
    .supported_level_count = 5,
    .custom_voltage_fn = NULL,  // NULL ==  Use ordinary floor and ceiling calculation functions.
    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
    .aging_static_margin_limit = 13, //mV
};
static const cpr_enablement_versioned_rail_config_t mss_8920_GF_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            { CPR_FOUNDRY_GF, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 2 ,DALCHIPINFO_FAMILY_MSM8920 },
        },
        .foundry_range_count = 1,
    },
    //.enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {                              /* Large intial values for testing*/
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS,              33,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS_L1,           13,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          51,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL_L1,       41,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            53,                  NULL,                  1},
    },
    .supported_level_count = 5,
    .custom_voltage_fn = NULL,  // NULL ==  Use ordinary floor and ceiling calculation functions.
    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
    .aging_static_margin_limit = 13, //mV
};

static const cpr_enablement_versioned_rail_config_t mss_8920_SMIC_cpr_enablement =
{
    .hw_versions =
    {
        .foundry_range = (const cpr_config_foundry_range[])
        {
            { CPR_FOUNDRY_SMIC, CPR_CHIPINFO_VERSION(0, 0), CPR_CHIPINFO_VERSION(0xFF, 0xFF), 0, 0xFF ,DALCHIPINFO_FAMILY_MSM8920 },
        },
        .foundry_range_count = 1,
    },
    //.enablement_init_params = &CPR_ENABLE_CLOSED_LOOP,
    .enablement_init_params = &CPR_ENABLE_OPEN_LOOP,
    .supported_level = (cpr_enablement_supported_level_t[])
    {                              /* Large intial values for testing*/
        //Mode                Static-Margin (mV) Custom static margin function    aging_scaling_factor 
        { CPR_VOLTAGE_MODE_SVS,              0,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_SVS_L1,           0,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL,          0,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_NOMINAL_L1,       0,                  NULL,                  1},
        { CPR_VOLTAGE_MODE_TURBO,            0,                  NULL,                  1},
    },
    .supported_level_count = 5,
    .custom_voltage_fn = NULL,  // NULL ==  Use ordinary floor and ceiling calculation functions.
    .mode_to_settle_count = 0,  // 0 == No init time CPR measurements
    .aging_static_margin_limit = 13, //mV
};


static const cpr_enablement_rail_config_t mss_8940_cpr_enablement =
{
    .rail_id = CPR_RAIL_MSS,
    .versioned_rail_config = (const cpr_enablement_versioned_rail_config_t*[])
    {
        &mss_8940_TSMC_cpr_enablement,
        &mss_8940_GF_cpr_enablement_v1_0,
		&mss_8940_GF_cpr_enablement_v2_0,
        &mss_8940_SMIC_cpr_enablement_v1_0,
		&mss_8940_SMIC_cpr_enablement_v2_0,
		&mss_8920_TSMC_cpr_enablement,
        &mss_8920_GF_cpr_enablement,
        &mss_8920_SMIC_cpr_enablement,
    },
    .versioned_rail_config_count = 8,
};



const cpr_enablement_config_t cpr_bsp_enablement_config =
{
    .rail_enablement_config = (const cpr_enablement_rail_config_t*[])
    {
        &mss_8940_cpr_enablement,
        &cx_8940_cpr_enablement,
    },
    .rail_enablement_config_count = 2,
};

