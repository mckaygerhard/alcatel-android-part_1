/**
 * @file:  cpr_image_target.h
 * @brief: image-specific and target-specific CPR functions.
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/image/mpss/inc/cpr_image_target.h#1 $
 * $Change: 9639486 $
 */
#ifndef CPR_IMAGE_TARGET_H
#define CPR_IMAGE_TARGET_H

#include "cpr.h"
#include "cpr_types.h"

#include "VCSDefs.h" /* Enums for the VCS power modes */

/**
*
* <!-- cpr_image_target_get_cx_eldo_voltage -->
*
* @brief Calculate the CX eLDO voltage for a corner
*
* @param info : pointer to the CPR domain info specifing the corner/mode (SVS, NOM, etc)
*
* @return eLDO voltage
*
*/
uint32 cpr_image_target_get_cx_eldo_voltage(cpr_domain_info* info);

/**
*
* <!-- cpr_image_target_get_foundry -->
*
* @brief Get the foundry and return a foundry enum that our CPR driver understands
*
*/
cpr_foundry_id cpr_image_target_get_foundry(void);

/**
*
* <!-- cpr_image_target_convert_vcs_to_cpr -->
*
* @brief Converts VCS corner enum to CPR voltage mode enum
*
*/
cpr_voltage_mode cpr_image_target_convert_vcs_to_cpr(VCSCornerType corner);

/**
*
* <!-- cpr_image_target_convert_vcs_to_cpr -->
*
* @brief Converts CPR voltage mode enum to VCS corner enum
*
*/
VCSCornerType cpr_image_target_convert_cpr_to_vcs(cpr_voltage_mode mode);

#endif /* CPR_IMAGE_TARGET_H */
