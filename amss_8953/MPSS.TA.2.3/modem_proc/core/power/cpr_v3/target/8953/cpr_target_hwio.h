/**
 * @file:  cpr_target_hwio.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/target/8953/cpr_target_hwio.h#1 $
 * $Change: 9639486 $
 *
 */
#ifndef CPR_TARGET_HWIO_H
#define CPR_TARGET_HWIO_H

#include "msmhwiobase.h"
#include "cpr_hwio.h"
#include "cpr_fuses_hwio.h"

#endif /* CPR_TARGET_HWIO_H */
