/**
 * @file:  cpr_target_hwio.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/02/17 20:35:29 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/target/8940/cpr_target_hwio.h#1 $
 * $Change: 9915170 $
 *
 */
#ifndef CPR_TARGET_HWIO_H
#define CPR_TARGET_HWIO_H

#include "msmhwiobase.h"
#include "cpr_hwio.h"
#include "cpr_fuses_hwio.h"

#endif /* CPR_TARGET_HWIO_H */
