/**
 * @file:  cpr_smem.h
 * @brief: 
 * 
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/inc/cpr_smem.h#1 $
 * $Change: 9639486 $
 */
#ifndef CPR_SMEM_H
#define CPR_SMEM_H

#include "cpr_cfg.h"
#include "cpr_data.h"

void cpr_smem_deserialize_config(cpr_rail* rail, cpr_rail_state* state);
void cpr_smem_serialize_config(cpr_rail_state* state, boolean append);

#endif
