/**
 * @file:  cpr_cfg.h
 * @brief:
 *
 * Copyright (c) 2016 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/02/16 03:52:08 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/inc/cpr_cfg.h#2 $
 * $Change: 9902037 $
 */
#ifndef CPR_CFG_H
#define	CPR_CFG_H

#include "cpr_types.h"
#include "cpr_voltage_plan.h"
#include "cpr_hal.h"

struct cpr_rail;

typedef struct cpr_cfg_funcs
{
    cpr_control_mode cMode;
    struct cpr_cfg_funcs* (*init)(struct cpr_rail* rail);
    struct cpr_cfg_funcs* (*enable)(struct cpr_rail* rail);
} cpr_cfg_funcs;

typedef struct
{
    cpr_domain_id id;
    cpr_version_range versions;
    cpr_cfg_funcs* funcs;

    // parameters not in the voltage plan
    bool dynamicfloor;
    bool enabletempadj;
    uint16 stepSize;
    int32 testMargin;
    uint32 fuseMultiplier;
    int32 thermalAdjustment[CPR_THERMAL_REGION_MAX];
} cpr_enablement;

/*
 * Static read-only config for rails
 */
typedef struct cpr_rail
{
    cpr_domain_id id;
    char* name;
    uint32 railIdx;

    // HW rail settings
    cpr_hal_handle hal;
    uint32 interruptId;
    cpr_hal_rail_cfg halRailCfg;

    cpr_voltage_mode *settleModes;
    uint32 settleModesCount;

    // versioned external configuration
    cpr_voltage_plan* vp;

    // versioned internal configuration
    cpr_enablement* enablement;
} cpr_rail;

/*
 * Static read-only config for controllers
 */
typedef struct
{
    cpr_hal_handle hal;
    char *refClk;
    char *ahbClk;
    uint16 stepQuotMin;
    uint16 stepQuotMax;
    uint8 *bypassSensors;
    uint16 bypassSensorsCount;
    uint8 *disableSensors;
    uint16 disableSensorsCount;
} cpr_controller;

typedef struct {
    cpr_fuse cprRev;
} cpr_misc_cfg;

#endif
