/**
 * @file:  cpr_isr.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/src/cpr_isr.c#1 $
 * $Change: 9639486 $
 */
#include "cpr_cfg.h"
#include "cpr_enablement.h"
#include "cpr_rail.h"

uint32 cpr_isr_get_interrupt(cpr_domain_id railId)
{
    cpr_rail* rail = cpr_get_rail( railId );
    return rail->interruptId;
}

void cpr_isr_process(cpr_domain_id railId)
{
    cpr_rail_isr( cpr_get_rail( railId ) );
}
