/**
 * @file:  cpr_rail.c
 *
 * Copyright (c) 2015-2016 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/04/19 20:16:34 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/src/cpr_rail.c#4 $
 * $Change: 10298483 $
 */
#include <stdbool.h>
#include <string.h>
#include "cpr_rail.h"
#include "cpr_hal.h"
#include "cpr_data.h"
#include "cpr_utils.h"
#include "cpr_image.h"
#include "cpr_logs.h"
#include "cpr_cfg.h"
#include "cpr_v3.h"
#include "cpr_voltage_plan.h"

extern void cpr_hal_mode_change_temp_adj(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp, cpr_enablement* enablement);

//******************************************************************************
// Macros / Definitions / Constants
//******************************************************************************

#define RESULTS_TIMEOUT_US  20000 // 20 ms
#define QUOT_OFFSET_MULTIPLIER 5
#define VOLT_OFFSET_MULTIPLIER 10 //10mV

//******************************************************************************
// Global Data
//******************************************************************************
static cpr_quotient *new_target_quotients; // used to calculate new target quotients

//******************************************************************************
// Local Helper Functions
//******************************************************************************

static inline uint32 quantize_up(uint32 quantum, uint32 value)
{
    uint32 remainder = value % quantum;

    if(remainder != 0) {
        value += quantum - remainder;
    }

    return value;
}

static boolean matches_device_version(const cpr_version* vers, uint32 count)
{
    for(int i = 0; i < count; i++)
    {
        const cpr_version* ver = &vers[i];

        if((ver->foundry == cpr_info.foundry || cpr_info.foundry == CPR_FOUNDRY_ANY || ver->foundry == CPR_FOUNDRY_ANY) &&
           cpr_info.chipVersion >= ver->min &&
           cpr_info.chipVersion <= ver->max)
        {
            return true;
        }

    }

    return false;
}

/*
 * Derives target quotient for finer grain frequency point in order to optimize power/performance.
 *
 * Reference: HPG - Target Quotient Scaling
 */
static void interpolate_quotients(cpr_rail* rail, cpr_mode_settings* modeState, cpr_submode_settings* submodeState,
                                  cpr_quotient* tgts, boolean cmprToExisting)
{
    uint32 quotScaler_x1000000 = 0;
    uint32 mode_idx = rail->vp->idxLookupFunc( modeState->mode );
    uint32 fdelta = rail->vp->modes[mode_idx].freqDelta;

    /*
     * fuses pointer could be NULL if Voltage Plan does not define yet
     */
    if(!fdelta && (rail->vp->modes[mode_idx].fuses != NULL) && (rail->vp->modes[mode_idx].fuses->quotOffset != NULL))
    {
        uint32 quotOffset = cpr_utils_decode_fuse_value( rail->vp->modes[mode_idx].fuses->quotOffset, 1, false ) * QUOT_OFFSET_MULTIPLIER;
        quotScaler_x1000000 = (quotOffset*1000000) / fdelta;
    }

    CPR_LOG_TRACE( " Interpolate quots. scaler: %u, fmax freq: %u, freq: %u",
                   quotScaler_x1000000, modeState->subModes[modeState->subModesCount - 1].freq, submodeState->freq );

    for(int i = 0; i < modeState->targetsCount; i++)
    {
        /*
         * QUOT(new frequency) = QUOT(fuse) - (Operation mode top frequency - new frequency in MHz)*scaling_factor
         */
        uint32 q = modeState->targets[i].quotient - (quotScaler_x1000000 * (modeState->subModes[modeState->subModesCount - 1].freq - submodeState->freq)) / 1000000;

        tgts[i].ro = modeState->targets[i].ro;
        tgts[i].quotient = cmprToExisting ? MAX( q, tgts[i].quotient ) : q;
    }
}

//******************************************************************************
// ISR Handler
//******************************************************************************

void cpr_rail_isr(void* ctx)
{
    cpr_results rslts;
    cpr_rail* rail = (cpr_rail*) ctx;

    CPR_LOG_TRACE( "=== ISR ===" );

    CPR_IMAGE_LOCK_CORE();

    cpr_info.processingIsr = true;
    cpr_utils_set_disable_vote(rail->id, CPR_DISABLE_VOTE_ISR);

    if(cpr_info.railStates[rail->railIdx].activeMode != NULL)
    {
        cpr_rail_get_results( rail, &rslts );
        cpr_rail_process_results( rail, &rslts );
        cpr_hal_clear_interrupts( &rail->hal );
    }

    cpr_utils_clear_disable_vote(rail->id, CPR_DISABLE_VOTE_ISR);
    cpr_info.processingIsr = false;

    CPR_IMAGE_UNLOCK_CORE();
}

//******************************************************************************
// Public API Functions
//******************************************************************************

static void cpr_rail_init_rail_config(cpr_rail* rail)
{
    uint32 railIdx = cpr_get_rail_idx( rail->id );
    rail->railIdx = railIdx;

    if(rail->vp == NULL)
    {
        const cpr_versioned_voltage_plan* vvp = cpr_get_versioned_voltage_plan( rail->id );

        if(vvp != NULL)
        {
            for(int i = 0; i < vvp->count; i++)
            {
                if(matches_device_version( &vvp->list[i].version, 1 )) {
                    rail->vp = vvp->list[i].cfg;
                    CPR_LOG_INFO( "Found voltage plan for rail %s: (idx=%d)", rail->name, i );
                    break;
                }
            }
        }
    }

    if(rail->vp == NULL) {
        CPR_LOG_FATAL( "Could not find voltage plan for rail %s", rail->name );
        return;
    }

    for(int i = 0; i < CPR_NUM_ENABLEMENTS && rail->enablement == NULL; i++)
    {
        cpr_enablement* e = cpr_enablements[i];
        if(e->id == rail->id)
        {
            if(matches_device_version( e->versions.versions, e->versions.count )) {
                rail->enablement = e;
                CPR_LOG_INFO( "Found enablement config for rail %s: (idx=%d)", rail->name, i );
                break;
            }
        }
    }

    if(rail->enablement == NULL) {
        CPR_LOG_FATAL( "Could not find enablement config for rail %s", rail->name );
        return;
    }
}

/*
 * Initialize config/data pointers and set all modes with global ceiling/floor voltages.
 */
static void cpr_rail_init_rail_data(cpr_rail* rail)
{
    uint32            railIdx        = rail->railIdx;
    cpr_rail_state   *railState      = &cpr_info.railStates[railIdx];
    cpr_voltage_plan *vp             = rail->vp;
    uint32            maxTargetCount = 0;

    railState->id                = rail->id;
    railState->modeSettingsCount = vp->modesCount;
    railState->modeSettings      = cpr_image_malloc(railState->modeSettingsCount * sizeof(cpr_mode_settings));

    /*
     * Set to global ceiling/floor first in order to make it easy to run aging algorithm.
     * Open loop voltages are set up later after aging algorihtm in cpr_rail_set_initial_voltages().
     */
    for(int modeIdx = 0; modeIdx < railState->modeSettingsCount; modeIdx++)
    {
        cpr_mode_settings *modeSettings = &railState->modeSettings[modeIdx];

        modeSettings->mode = vp->supportedModes[modeIdx];

        /*
         * Allocate memory for target quotient info
         * NOTE: target quotient values are set up in cpr_rail_update_target_quotients()
         */
        if(vp->modes[modeIdx].fuses != NULL && vp->modes[modeIdx].fuses->quot != NULL)
        {
            modeSettings->targetsCount = 1; // for local quots
        }
        else if(vp->modes[modeIdx].quotients)
        {
            modeSettings->targetsCount = vp->modes[modeIdx].quotients->count;
        }

        if(maxTargetCount < modeSettings->targetsCount)
        {
            maxTargetCount = modeSettings->targetsCount;
        }

        if(modeSettings->targetsCount)
        {
            modeSettings->targets   = cpr_image_malloc(modeSettings->targetsCount * sizeof(cpr_quotient));
        }

        modeSettings->subModesCount = vp->modes[modeIdx].subModesCount;
        modeSettings->subModes      = cpr_image_malloc(modeSettings->subModesCount * sizeof(cpr_submode_settings));

        for(int submodeIdx = 0; submodeIdx < modeSettings->subModesCount; submodeIdx++)
        {
            cpr_submode_settings *submode = &modeSettings->subModes[submodeIdx];

            submode->freq    = vp->modes[modeIdx].subModes[submodeIdx].freq;
            submode->ceiling = vp->modes[modeIdx].subModes[submodeIdx].ceiling;
            submode->current = vp->modes[modeIdx].subModes[submodeIdx].ceiling;
            submode->min     = vp->modes[modeIdx].subModes[submodeIdx].ceiling;
            submode->floor   = vp->modes[modeIdx].subModes[submodeIdx].floor;

            /*
             * Set active submode to the last one to avoid NULL pointer dereference
             * during cpr_measurements_settle()
             */
            modeSettings->activeSubMode = submode;
        }
    }

    if(maxTargetCount)
    {
        new_target_quotients = cpr_image_malloc(maxTargetCount * sizeof(cpr_quotient));
    }
}

void cpr_rail_init(cpr_rail* rail)
{
    /*
     * Find Voltage Plan and Enablement config for the rail
     */
    cpr_rail_init_rail_config(rail);

    /*
     * Initialize rail data with global ceiling/floor first
     * based on the Voltage Plan config because
     * Aging algorithm has to run with global ceiling.
     * Open loop voltage is set up in cpr_rail_set_initial_voltages()
     * after running aging algorithm.
     */
    cpr_rail_init_rail_data(rail);
}

void cpr_rail_get_results(cpr_rail* rail, cpr_results* rslts)
{
    uint32 timeout = RESULTS_TIMEOUT_US;

    do {
        cpr_hal_get_results( &rail->hal, rslts );

        if(rslts->busy) {
            cpr_image_wait(3); // 3 usec is recommended by HW team
        }
    } while(rslts->busy && --timeout > 0);

    if(timeout == 0) {
        CPR_LOG_FATAL( "Timeout waiting for results" );
    }

    CPR_LOG_TRACE( " Results - %s / up: %d / dn: %d / steps: %d", rail->name, rslts->up, rslts->down, rslts->steps );
}

boolean cpr_rail_process_results(cpr_rail* rail, cpr_results* rslts)
{
    boolean changed = false;
    boolean ackResult = false;
    uint32 railIdx = rail->railIdx;
    cpr_mode_settings* currentMode = cpr_info.railStates[railIdx].activeMode;
    cpr_submode_settings* currentSubMode = currentMode->activeSubMode;
    uint32 recommendation = currentSubMode->current;

    /*
     * NOTE:
     * Currently recommended by HW team to limit step size to 1
     * regardless of actual value
     */
    uint32 steps = 1; // rslts->steps

    // It's possible that both the up and dn interrupts are set.
    // In that case, the recommendation from the HW team is that we step-up the voltage.
    // This could happen if a BHS is toggled after the first interrupt triggers.
    if(rslts->up)
    {
        /*
         * Re-enable DOWN interrupt since there is room to take DOWN recommendation.
         */
        cpr_hal_enable_down_interrupt(&rail->hal, true);

        recommendation += steps * rail->enablement->stepSize;
        currentSubMode->debug.up += steps;
    }
    else if(rslts->down)
    {
        /*
         * Re-enable UP interrupt since there is room to take UP recommendation.
         */
        cpr_hal_enable_up_interrupt(&rail->hal, true);

        recommendation -= steps * rail->enablement->stepSize;
        currentSubMode->debug.down += steps;
    }

    CPR_LOG_TRACE( " New voltage recommendation for mode %u: %u (floor:%u) (ceiling:%u)",
                   currentMode->mode,
                   recommendation,
                   currentSubMode->floor,
                   currentSubMode->ceiling);

    // Clamp the recommendation
    if(recommendation >= currentSubMode->ceiling)
    {
        /*
         * Disable UP interrupt since hitting ceiling voltage and no room to take UP recommendation.
         */
        cpr_hal_enable_up_interrupt(&rail->hal, false);

        recommendation = currentSubMode->ceiling;
        currentSubMode->debug.ceiling++;
        CPR_LOG_TRACE(" Arrived at Ceiling. Disable UP interrupts. ");
    }
    else if(recommendation <= currentSubMode->floor)
    {
        /*
         * Disable DOWN interrupt since hitting floor voltage and no room to take DOWN recommendation.
         */
        cpr_hal_enable_down_interrupt(&rail->hal, false);

        recommendation = currentSubMode->floor;
        currentSubMode->debug.floor++;
        CPR_LOG_TRACE(" Arrived at Floor. Disable DOWN interrupts. ");
    }

    if (recommendation != currentSubMode->current)
    {
        currentSubMode->current = recommendation;
        currentSubMode->min = MIN( recommendation, currentSubMode->min );

        if(cpr_image_rail_transition_voltage( rail->id ))
        {
            changed = true;

            // Don't ack the result if we switched modes.
            if(currentMode == cpr_info.railStates[railIdx].activeMode)
            {
                ackResult = true;
            }
        }
        else
        {
            /*
             * Failed to transition voltage. Increment ignored counter.
             * Don't ack the result since no voltage change.
             */
            currentSubMode->debug.ignored++;
        }
    }
    else
    {
        currentSubMode->debug.ignored++;
    }

    CPR_LOG_TRACE( " Results ack: %u", ackResult );
    CPR_STATIC_LOG_MODE_INFO(&cpr_info.railStates[railIdx], currentMode, currentSubMode);
    CPR_STATIC_LOG_ISR_INFO(&cpr_info.railStates[railIdx], currentMode, currentSubMode);

    cpr_hal_ack_result( &rail->hal, ackResult );

    return changed;
}

void cpr_rail_set_initial_voltages(cpr_rail* rail, boolean useGlobalCeiling, boolean useGlobalFloor)
{
    uint32 railIdx = rail->railIdx;
    cpr_voltage_plan *vp = rail->vp;
    cpr_enablement *e = rail->enablement;
    uint32 minModeCeiling = 0;

    // Min mode is used for interpolation of the lowest supported mode
    if(vp->minMode)
    {
        if(vp->minMode->fuses && vp->minMode->fuses->volt && !useGlobalCeiling)
        {
            int32 offset = e->fuseMultiplier * cpr_utils_decode_fuse_value( vp->minMode->fuses->volt, 1, true );
            cpr_margin_data* margin = cpr_utils_get_margins( vp->minMode->margins );

            if(margin != NULL) {
                offset += margin->openLoop;
            }

            minModeCeiling = vp->minMode->fref + offset;
        }
        else{
            minModeCeiling = vp->minMode->subModes[vp->minMode->subModesCount - 1].ceiling;
        }
    }

    for(int modeIdx = 0; modeIdx < vp->modesCount; modeIdx++)
    {
        uint32 vScaling_x100 = 0;
        cpr_voltage_mode mode = vp->supportedModes[modeIdx];
        cpr_mode_settings *modeSettings = &cpr_info.railStates[railIdx].modeSettings[modeIdx];
        cpr_freq_data* fmaxVpData = &vp->modes[modeIdx].subModes[vp->modes[modeIdx].subModesCount - 1];
        uint32 fusedFloor = fmaxVpData->floor;
        uint32 fusedCeiling = fmaxVpData->ceiling;

        //
        // Calc open loop voltages for this mode using the fmax submode.
        //
        if(vp->modes[modeIdx].fuses && vp->modes[modeIdx].fuses->volt)
        {
            int32 offset;

            cpr_margin_data* margin = cpr_utils_get_margins( vp->modes[modeIdx].margins );

            modeSettings->decodedFuseSteps = cpr_utils_decode_fuse_value( vp->modes[modeIdx].fuses->volt, 1, true );

            offset = e->fuseMultiplier * modeSettings->decodedFuseSteps;

            if(margin != NULL) {
                offset += margin->openLoop;
            }

            if(!useGlobalCeiling) {
                fusedCeiling = vp->modes[modeIdx].fref + offset;
            }

            if(!useGlobalFloor)
            {
                int32 fToCAdjustment = 0;

                if(margin != NULL) {
                    fToCAdjustment += margin->maxFloorToCeil * 1000; //converting from mV to uV
                }

                if(fToCAdjustment != 0) {
                    // max-floor-ceil method
                    fusedFloor = fusedCeiling - fToCAdjustment;
                }
                else {
                    // old school formula
                    fusedFloor = ((vp->modes[modeIdx].fref * 90) / 100) + offset + 40000;
                }
            }

            CPR_LOG_INFO(" Calculated open loop voltages for mode %d: (floor:%u) (ceiling:%u)", mode, fusedFloor, fusedCeiling);
        }

        //
        // Calc the scaling factor used for interpolation
        //
        if(vp->modes[modeIdx].freqDelta > 0)
        {
            uint32 prevCeiling;

            if(modeIdx != 0) {
                cpr_mode_settings* prevMode = &cpr_info.railStates[railIdx].modeSettings[modeIdx-1];
                prevCeiling = prevMode->subModes[prevMode->subModesCount-1].ceiling;
            }
            else {
                prevCeiling = minModeCeiling == 0 ? fusedCeiling : minModeCeiling;
            }

            vScaling_x100 = (((fusedCeiling - prevCeiling)*100 ) / vp->modes[modeIdx].freqDelta*100)/100;

            CPR_LOG_TRACE( " Calculated scaling factor (x100): %u", vScaling_x100 );
        }

        //
        // Interpolate submodes using calculated open loop voltages
        //
        for(int submodeIdx = 0; submodeIdx < modeSettings->subModesCount; submodeIdx++)
        {
            cpr_submode_settings* submode = &modeSettings->subModes[submodeIdx];
            uint32 gceiling = vp->modes[modeIdx].subModes[submodeIdx].ceiling;
            uint32 gfloor = vp->modes[modeIdx].subModes[submodeIdx].floor;
            uint32 ceiling = fusedCeiling;
            uint32 floor = fusedFloor;

            // TODO
            #if 0
            if(submode.hasItsOwnfuse) {
                fusedFloor = xxx;
                fusedCeiling = xxx;
            }
            #endif

            ceiling -= (vScaling_x100 * (fmaxVpData->freq - submode->freq)) / 100;
            floor -= (vScaling_x100 * (fmaxVpData->freq - submode->freq)) / 100;

            // limit to global limits
            ceiling = MIN( gceiling, MAX( ceiling, gfloor ) );
            floor = MIN( gceiling, MAX( floor, gfloor ) );

            // round up to PMIC step size.
            ceiling = quantize_up( rail->enablement->stepSize, ceiling );
            floor = quantize_up( rail->enablement->stepSize, floor );

            CPR_LOG_INFO( " Interpolated mode %d submode %d: (floor:%u) (ceiling:%u)", mode, submodeIdx, floor, ceiling );

            submode->current = ceiling;
            submode->ceiling = ceiling;
            submode->min = ceiling;

            if(cpr_utils_is_closed_loop_mode(rail->id))
            {
                if(e->dynamicfloor == true)
                    submode->floor = floor;
                else
                    submode->floor = gfloor;
            }
            else
            {
                /*
                 * Set floor to ceiling to make sure CPR controller does not do anything for open loop.
                 * CPR controller is turned on even though it's open loop.
                 */
                submode->floor = ceiling;
            }

            if(floor > ceiling || ceiling > vp->modes[modeIdx].subModes[submodeIdx].ceiling || floor < vp->modes[modeIdx].subModes[submodeIdx].floor)
            {
                CPR_LOG_FATAL( "Voltages outside allowable settings [rail %s, mode %x] ceiling: %u, floor: %u",
                               rail->name, mode, ceiling, floor );
            }
        }
    }
}

/*
 * Updates target quotients.
 * NOTE: Does not configure HW.
 */
void cpr_rail_update_target_quotients(cpr_rail* rail, cpr_rail_state* state)
{
    cpr_voltage_plan* vp = rail->vp;

    for(int i = 0; i < vp->modesCount; i++)
    {
        cpr_quotient localQuot;
        cpr_quotient_cfg baseQuots;
        uint32 modeIdx = vp->idxLookupFunc( vp->supportedModes[i] );
        cpr_mode_settings* modeSettings = &state->modeSettings[modeIdx];
        cpr_margin_data* marginData = cpr_utils_get_margins( vp->modes[modeIdx].margins );
        int32 margin = marginData == NULL ? 0 : marginData->closedLoop;

        baseQuots.kvs = vp->modes[i].quotients->kvs;

        // Check if we are using local quots
        if(vp->modes[i].fuses != NULL && vp->modes[i].fuses->quot != NULL)
        {
            localQuot.quotient = cpr_utils_decode_fuse_value( vp->modes[i].fuses->quot, 1, false );
            localQuot.ro = cpr_utils_decode_fuse_value( vp->modes[i].fuses->rosel, 1, false );
            baseQuots.quots = &localQuot;
            baseQuots.count = 1;
        }
        else
        {
            baseQuots.quots = vp->modes[i].quotients->quots;
            baseQuots.count = vp->modes[i].quotients->count;
        }

        for(int k = 0; k < CPR_MARGIN_ADJUSTMENT_SOURCE_MAX; k++) {
            margin += state->marginAdjustments[k];
        }
        
        //adjusting the closed loop margin based on the offset fuse value
        if(vp->modes[i].fuses->voltOffset != NULL) {
            modeSettings->decodedFuseOffset = cpr_utils_decode_fuse_value( vp->modes[i].fuses->voltOffset, 1, true );
            margin += modeSettings->decodedFuseOffset * VOLT_OFFSET_MULTIPLIER ; //Adding the fuseoffset to margin in steps of mV
        } 

        CPR_LOG_INFO( "Updating %s quotients with %d mV total margin (%s, mode: %u)",
                      baseQuots.quots == &localQuot ? "LOCAL" : "GLOBAL",
                      margin, rail->name, vp->supportedModes[i] );

        CPR_ASSERT(baseQuots.count == modeSettings->targetsCount);

        for(int k = 0; k < baseQuots.count; k++)
        {
            modeSettings->targets[k] = baseQuots.quots[k];

            if(modeSettings->targets[k].quotient == 0) {
                CPR_LOG_WARNING( " Quotient is 0 for RO %d", baseQuots.quots[k].ro );
                continue;
            }

            // Target_Quotient = Base_Quotient + (((static_margin * ro_kv_x_100) + 99) / 100)
            modeSettings->targets[k].quotient += (((int32)(margin * baseQuots.kvs[k]) + 99) / 100);

            CPR_LOG_TRACE( " RO: %u, Base: %u, KV: %u, Adjusted: %u",
                           baseQuots.quots[k].ro,
                           baseQuots.quots[k].quotient,
                           baseQuots.kvs[k],
                           modeSettings->targets[k].quotient );
        }
    }
}

void cpr_rail_disable(cpr_rail* rail)
{
    cpr_rail_state* railState = &cpr_info.railStates[rail->railIdx];

    if(railState->activeMode != NULL)
    {
        CPR_LOG_INFO( " Disabling %s", rail->name );

        cpr_hal_disable_rail( &rail->hal );

        cpr_utils_set_active_mode_setting(rail->id, NULL, NULL);

        CPR_STATIC_LOG_RAIL_INFO(railState);
    }
}

void cpr_rail_enable(cpr_rail* rail, cpr_mode_settings* modeState, cpr_submode_settings* submodeState, boolean changeOveride)
{
    cpr_rail_state* railState = &cpr_info.railStates[rail->railIdx];
    boolean modeChanged = railState->activeMode == NULL || railState->activeMode->activeSubMode != submodeState;

    if(modeChanged || changeOveride)
    {
        /*
         * new_target_quotients should be initialized in cpr_rail_init_rail_data() for closed-loop rail.
         */
        CPR_ASSERT(new_target_quotients);

        /*
         * reset values to calculate new target quotients
         */
        memset(new_target_quotients, 0, modeState->targetsCount * sizeof(cpr_quotient));

        cpr_rail_disable( rail );

        cpr_utils_set_active_mode_setting(rail->id, modeState, submodeState);

        CPR_LOG_INFO( " Setting %s target quotients to mode %u, freq %u", rail->name, modeState->mode, submodeState->freq );

        interpolate_quotients( rail, modeState, submodeState, new_target_quotients, false );

        for(int i = 0; i < CPR_NUM_RAILS; i++)
        {
            cpr_rail* otherRail = cpr_rails[i];

            if(rail == otherRail || rail->hal.base != otherRail->hal.base || rail->hal.thread != otherRail->hal.thread) {
                continue;
            }

            cpr_mode_settings* otherMode = cpr_info.railStates[otherRail->railIdx].activeMode;

            if(otherMode && otherMode->activeSubMode && otherMode->targetsCount > 0) {
                interpolate_quotients( otherRail, otherMode, otherMode->activeSubMode, new_target_quotients, true );
            }
        }

        cpr_hal_set_targets( &rail->hal, modeState->mode, submodeState->freq,
                             new_target_quotients,
                             modeState->targetsCount );

        /* Temperature margin adjustment enable calculations*/
        cpr_hal_mode_change_temp_adj(rail->vp->idxLookupFunc(modeState->mode), &rail->hal, rail->vp, rail->enablement);

        CPR_STATIC_LOG_RAIL_INFO(railState);
        CPR_STATIC_LOG_MODE_INFO(railState, modeState, submodeState);
    }

    uint32 current = submodeState->current;
    boolean up = current < submodeState->ceiling;
    boolean dn = current > submodeState->floor;

    CPR_LOG_INFO( " Enabling %s, enabled (UP_IRQ_EN: %u, DOWN_IRQ_EN: %u)", rail->name, up, dn );

    cpr_hal_enable_rail( &rail->hal, up, dn, railState->cMode == CPR_CONTROL_SW_CLOSED_LOOP );
}

void cpr_rail_register_isr(cpr_rail* rail)
{
    cpr_image_register_isr( rail->id, rail->interruptId, cpr_rail_isr, rail );
}
