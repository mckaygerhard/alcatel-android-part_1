/**
 * @file:  cpr_controller.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/03/08 00:37:59 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/src/cpr_controller.c#2 $
 * $Change: 10025929 $
 */

#include "cpr_controller.h"
#include "cpr_logs.h"
#include "cpr_hal.h"
#include "cpr_image.h"
#include "cpr_mpss_hwio.h" 
/*
 * Initialize CPR controller for the common config among threads/rails
 */
void cpr_controller_init(cpr_controller* controller)
{
    CPR_LOG_TRACE( "Initializing controller @ 0x%x", (uint32)controller->hal.base );
     
    HWIO_OUTF(MSS_CLAMP_IO, RBCPR, 0x0);
    
    // Enable the clocks
    if(controller->ahbClk != NULL) {
        cpr_image_enable_clock(controller->ahbClk);
    }

    if(controller->refClk != NULL) {
        cpr_image_enable_clock(controller->refClk);
    }

    cpr_hal_configure_controller( &controller->hal, controller->stepQuotMin, controller->stepQuotMax );
    cpr_hal_bypass_sensors( &controller->hal, controller->bypassSensors, controller->bypassSensorsCount );
    cpr_hal_disable_sensors( &controller->hal, controller->disableSensors, controller->disableSensorsCount );
}
