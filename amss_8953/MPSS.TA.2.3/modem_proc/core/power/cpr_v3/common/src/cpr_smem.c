/**
 * @file:  cpr_smem.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/src/cpr_smem.c#1 $
 * $Change: 9639486 $
 */
#include "cpr_smem.h"
#include "cpr_image.h"
#include "cpr_logs.h"

//******************************************************************************
// Macros / Definitions / Constants
//******************************************************************************

#define CURRENT_SMEM_CFG_VERSION 2
#define INCREMENT_OFFSET(x) do { currOffset += x; CPR_ASSERT(currOffset < size); } while(false)

//******************************************************************************
// V1 Defines
//******************************************************************************

#define CPR_SMEM_MAX_RAILS 8
#define V1_CPR_ROSC_COUNT 16

typedef enum
{
    CPR_V1_MODE_OFF,
    CPR_V1_MODE_RETENTION,
    CPR_V1_MODE_SVS2,
    CPR_V1_MODE_SVS,
    CPR_V1_MODE_SVS_L1,
    CPR_V1_MODE_NOMINAL,
    CPR_V1_MODE_NOMINAL_L1,
    CPR_V1_MODE_TURBO,
    CPR_V1_MODE_SUPER_TURBO,
    CPR_V1_MODE_SUPER_TURBO_NO_CPR
} cpr_voltage_mode_v1;

typedef struct
{
    int16  ro_target_quotient[V1_CPR_ROSC_COUNT];
} cpr_target_quotient_set_t;

typedef struct
{
  cpr_voltage_mode          voltage_mode;
  uint32                    voltage_ceil;
  uint32                    voltage_floor;
  cpr_target_quotient_set_t rosc_targets;
} cpr_corner_params_t;

typedef enum
{
    CPR_EXTERNALIZED_STATE_ENABLED_OPEN_LOOP,
    CPR_EXTERNALIZED_STATE_ENABLED_CLOSED_LOOP,
    CPR_EXTERNALIZED_STATE_NO_CONFIG_AVAILABLE,
} cpr_extrnalized_state_rail_enablement_t;

typedef struct
{
    uint32                                      rail_id; // Actually a cpr_rail_id_t
    uint32                                      enablement; // cpr_extrnalized_state_rail_enablement_t
    uint32                                      voltage_config_count;
    uint32                                      voltage_config_offset;
} cpr_externalized_state_rail_index_t;

typedef struct
{
    cpr_corner_params_t         corner_params;
    uint32                      voltage;
} cpr_externalized_state_closed_loop_level_t;

typedef struct
{
    cpr_voltage_mode          voltage_mode;
    uint32                    voltage;
} cpr_externalized_state_open_loop_level_t;

typedef struct
{
    uint32                                  version;
    cpr_externalized_state_rail_index_t     rail_info_index[CPR_SMEM_MAX_RAILS];
    uint32                                  rail_info_count;
} cpr_externalized_state_header;

//******************************************************************************
// V2 Defines
//******************************************************************************

typedef struct __attribute__ ((__packed__))
{
    uint32 mode;  // cpr_voltage_mode
    uint16 targetsCount;
    uint16 submodesCount;
} cpr_smem_mode_state;

typedef struct __attribute__ ((__packed__))
{
    uint32  id;          // cpr_rail_id
    uint32  controlMode; // cpr_control_mode
    uint32  voltageMode; // cpr_voltage_mode
    uint32  freq;
    uint8   modeStateCount;
} cpr_smem_rail_hdr;

typedef struct __attribute__ ((__packed__))
{
    uint32  version;
    uint8   railCount;
} cpr_smem_hdr;

//******************************************************************************
// Local Helper Functions
//******************************************************************************

static cpr_voltage_mode v1_mode_to_current_mode(uint32 v1mode)
{
    switch(v1mode)
    {
        case CPR_V1_MODE_SVS2:
            return CPR_VOLTAGE_MODE_LOW_SVS;

        case CPR_V1_MODE_SVS:
            return CPR_VOLTAGE_MODE_SVS;

        case CPR_V1_MODE_NOMINAL:
            return CPR_VOLTAGE_MODE_NOMINAL;

        case CPR_V1_MODE_TURBO:
        case CPR_V1_MODE_SUPER_TURBO:
            return CPR_VOLTAGE_MODE_TURBO;
    }

    CPR_ASSERT(0);
    return CPR_VOLTAGE_MODE_OFF;
}

static void deserialize_v1(cpr_rail* rail, cpr_rail_state* state, void* buf, uint32 size)
{
    const cpr_externalized_state_header* extern_data = (cpr_externalized_state_header*)buf;
    const cpr_externalized_state_rail_index_t* rail_header = NULL;

    for(int i=0; i<extern_data->rail_info_count; i++)
    {
        if(extern_data->rail_info_index[i].rail_id == rail->id)
        {
            rail_header = &extern_data->rail_info_index[i];
            break;
        }
    }

    CPR_ASSERT(rail_header);

    if(state->modeSettings == NULL && rail_header->voltage_config_count != 0)
    {
        state->modeSettingsCount = rail_header->voltage_config_count;
        state->modeSettings = cpr_image_malloc(state->modeSettingsCount * sizeof(cpr_mode_settings));
    }
    else {
        CPR_ASSERT(state->modeSettingsCount >= rail_header->voltage_config_count);
    }

    if(rail_header->enablement == CPR_EXTERNALIZED_STATE_ENABLED_OPEN_LOOP)
    {
        state->id = rail->id;
        state->cMode = CPR_CONTROL_OPEN_LOOP;

        for(int i = 0; i < rail_header->voltage_config_count; i++)
        {
            cpr_externalized_state_open_loop_level_t* estate = (cpr_externalized_state_open_loop_level_t*)
                    ((&((const uint8*) extern_data)[rail_header->voltage_config_offset]) +
                     (i * sizeof(cpr_externalized_state_open_loop_level_t)));

            cpr_voltage_mode mode = v1_mode_to_current_mode( estate->voltage_mode );
            uint32 modeIdx = rail->vp->idxLookupFunc( mode );

            if(state->modeSettings[modeIdx].subModes == NULL) {
                state->modeSettings[modeIdx].subModesCount = 1;
                state->modeSettings[modeIdx].subModes = cpr_image_malloc(state->modeSettings[modeIdx].subModesCount * sizeof(cpr_submode_settings));
            }

            state->modeSettings[modeIdx].mode = mode;
            state->modeSettings[modeIdx].subModes[0].current = estate->voltage;
        }
    }
    else
    {
        CPR_ASSERT(rail_header->enablement == CPR_EXTERNALIZED_STATE_ENABLED_CLOSED_LOOP);

        state->id = rail->id;
        state->cMode = CPR_CONTROL_SW_CLOSED_LOOP;

        CPR_ASSERT( rail_header->voltage_config_count <= state->modeSettingsCount );

        for(int i = 0; i < rail_header->voltage_config_count; i++)
        {
            cpr_externalized_state_closed_loop_level_t* estate = (cpr_externalized_state_closed_loop_level_t*)
                    ((&((const uint8*) extern_data)[rail_header->voltage_config_offset]) +
                     (i * sizeof(cpr_externalized_state_closed_loop_level_t)));

            cpr_voltage_mode mode = v1_mode_to_current_mode( estate->corner_params.voltage_mode );
            uint32 modeIdx = rail->vp->idxLookupFunc( mode );

            if(state->modeSettings[modeIdx].subModes == NULL) {
                state->modeSettings[modeIdx].subModesCount = 1;
                state->modeSettings[modeIdx].subModes = cpr_image_malloc(state->modeSettings[modeIdx].subModesCount * sizeof(cpr_submode_settings));
            }

            state->modeSettings[modeIdx].subModes[0].current = estate->voltage;
            state->modeSettings[modeIdx].subModes[0].min = estate->voltage;
            state->modeSettings[modeIdx].subModes[0].ceiling = estate->corner_params.voltage_ceil;
            state->modeSettings[modeIdx].subModes[0].floor = estate->corner_params.voltage_floor;
            state->modeSettings[modeIdx].mode = mode;

            if(state->modeSettings[modeIdx].targets == NULL) {
                state->modeSettings[modeIdx].targets = cpr_image_malloc( V1_CPR_ROSC_COUNT * sizeof(cpr_quotient) );
                state->modeSettings[modeIdx].targetsCount = V1_CPR_ROSC_COUNT;
            }
            else {
                CPR_ASSERT( state->modeSettings[modeIdx].targetsCount >= V1_CPR_ROSC_COUNT );
            }

            for(int k = 0; k < V1_CPR_ROSC_COUNT; k++) {
                state->modeSettings[modeIdx].targets[k].ro = k;
                state->modeSettings[modeIdx].targets[k].quotient = estate->corner_params.rosc_targets.ro_target_quotient[k];
            }
        }
    }
}

static void deserialize_v2(cpr_rail* rail, cpr_rail_state* state, void* buf, uint32 size)
{
    cpr_smem_hdr* hdr = (cpr_smem_hdr*)buf;
    boolean foundRail = false;
    uint32 currOffset = sizeof(cpr_smem_hdr);

    for(int i = 0; i < hdr->railCount; i++)
    {
        cpr_smem_rail_hdr* railHdr = (cpr_smem_rail_hdr*) (((uint8*)hdr) + currOffset);
        foundRail = railHdr->id == rail->id;

        if(foundRail)
        {
            state->id = (cpr_domain_id)railHdr->id;
            state->cMode = (cpr_control_mode)railHdr->controlMode;

            if(state->modeSettings == NULL && railHdr->modeStateCount != 0)
            {
                state->modeSettingsCount = railHdr->modeStateCount;
                state->modeSettings = cpr_image_malloc( state->modeSettingsCount * sizeof(cpr_mode_settings) );
            }
            else
            {
                CPR_ASSERT( state->modeSettingsCount >= railHdr->modeStateCount );
            }

            cpr_voltage_mode mode = (cpr_voltage_mode)railHdr->voltageMode;
            state->activeMode = &state->modeSettings[rail->vp->idxLookupFunc( mode )];
        }

        INCREMENT_OFFSET( sizeof(cpr_smem_rail_hdr) );

        for(int i = 0; i < railHdr->modeStateCount; i++)
        {
            uint32 modeIdx = 0;
            cpr_smem_mode_state* modeState = (cpr_smem_mode_state*) (((uint8*)hdr) + currOffset);

            if(foundRail)
            {
                modeIdx = rail->vp->idxLookupFunc( (cpr_voltage_mode)modeState->mode );
                state->modeSettings[modeIdx].mode = (cpr_voltage_mode)modeState->mode;
            }

            INCREMENT_OFFSET( sizeof(cpr_smem_mode_state) );

            if(foundRail)
            {
                if(state->modeSettings[modeIdx].targets == NULL) {
                    state->modeSettings[modeIdx].targets = cpr_image_malloc( modeState->targetsCount * sizeof(cpr_quotient) );
                    state->modeSettings[modeIdx].targetsCount = modeState->targetsCount;
                }
                else {
                    CPR_ASSERT( state->modeSettings[modeIdx].targetsCount >= modeState->targetsCount );
                }

                for(int k = 0; k < modeState->targetsCount; k++) {
                    state->modeSettings[modeIdx].targets[k] = ((cpr_quotient*)(((uint8*)hdr) + currOffset))[k];
                }
            }

            INCREMENT_OFFSET( modeState->targetsCount * sizeof(cpr_quotient) );

            if(foundRail)
            {
                if(state->modeSettings[modeIdx].subModes == NULL) {
                    state->modeSettings[modeIdx].subModes = cpr_image_malloc( modeState->submodesCount * sizeof(cpr_submode_settings) );
                    state->modeSettings[modeIdx].subModesCount = modeState->submodesCount;
                }
                else {
                    CPR_ASSERT( state->modeSettings[modeIdx].subModesCount >= modeState->submodesCount );
                }

                for(int k = 0; k < modeState->submodesCount; k++)
                {
                    state->modeSettings[modeIdx].subModes[k] = ((cpr_submode_settings*) (((uint8*) hdr) + currOffset))[k];

                    if(state->modeSettings[modeIdx].subModes[k].freq == railHdr->freq) {
                        state->activeMode->activeSubMode = &state->modeSettings[modeIdx].subModes[k];
                    }
                }
            }

            INCREMENT_OFFSET( modeState->submodesCount * sizeof(cpr_submode_settings) );
        }

        if(foundRail) {
            break;
        }
    }

    CPR_ASSERT( foundRail );
}

//******************************************************************************
// Public API Functions
//******************************************************************************

void cpr_smem_deserialize_config(cpr_rail* rail, cpr_rail_state* state)
{
    uint32 size;
    cpr_smem_hdr* hdr;

    cpr_image_open_remote_cfg( (void**)&hdr, &size );

    switch(hdr->version)
    {
        case 1: deserialize_v1( rail, state, hdr, size ); break;
        case 2: deserialize_v2( rail, state, hdr, size ); break;

        default:
            CPR_LOG_FATAL( "Unsupported serialization version: %u", hdr->version );
    }

    for(int i = 0; i < state->modeSettingsCount; i++)
    {
        cpr_mode_settings* mode = &state->modeSettings[i];

        for(int k = 0; k < mode->subModesCount; k++)
        {
            CPR_LOG_INFO( "%s smem settings: [mode %2u, freq: %7u] (adjusted: %7u - %7u) (current: %7u) (min: %7u)",
                          rail->name, mode->mode, mode->subModes[k].freq,
                          mode->subModes[k].ceiling, mode->subModes[k].floor,
                          mode->subModes[k].current,
                          mode->subModes[k].min );
        }
    }

    cpr_image_close_remote_cfg();
}

void cpr_smem_serialize_config(cpr_rail_state* state, boolean append)
{
    static uint32 currOffset = sizeof(cpr_smem_hdr);
    uint32 size;
    cpr_smem_hdr* hdr;

    cpr_image_open_remote_cfg( (void**)&hdr, &size );

    if(!append)
    {
        currOffset = sizeof(cpr_smem_hdr);
        hdr->version = CURRENT_SMEM_CFG_VERSION;
        hdr->railCount = 0;
    }

    cpr_smem_rail_hdr* railHdr = (cpr_smem_rail_hdr*) (((uint8*)hdr) + currOffset);

    hdr->railCount++;

    railHdr->id = state->id;
    railHdr->controlMode = state->cMode;
    railHdr->voltageMode = state->activeMode->mode;
    railHdr->freq = state->activeMode->activeSubMode->freq;
    railHdr->modeStateCount = 0;

    INCREMENT_OFFSET( sizeof(cpr_smem_rail_hdr) );

    for(int i = 0; i < state->modeSettingsCount; i++)
    {
        if(state->modeSettings[i].mode != CPR_VOLTAGE_MODE_OFF)
        {
            cpr_smem_mode_state* modeState = (cpr_smem_mode_state*) (((uint8*)hdr) + currOffset);
            railHdr->modeStateCount++;

            modeState->mode = state->modeSettings[i].mode;
            modeState->targetsCount = state->modeSettings[i].targetsCount;
            modeState->submodesCount = state->modeSettings[i].subModesCount;

            INCREMENT_OFFSET( sizeof(cpr_smem_mode_state) );

            for(int k = 0; k < modeState->targetsCount; k++) {
                ((cpr_quotient*)(((uint8*)hdr) + currOffset))[k] = state->modeSettings[i].targets[k];
            }

            INCREMENT_OFFSET( modeState->targetsCount * sizeof(cpr_quotient) );

            for(int k = 0; k < modeState->submodesCount; k++) {
                ((cpr_submode_settings*)(((uint8*)hdr) + currOffset))[k] = state->modeSettings[i].subModes[k];
            }

            INCREMENT_OFFSET( modeState->submodesCount * sizeof(cpr_submode_settings) );
        }
    }

    cpr_image_close_remote_cfg();
}
