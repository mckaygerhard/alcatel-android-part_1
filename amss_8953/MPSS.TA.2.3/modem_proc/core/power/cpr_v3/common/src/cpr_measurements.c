/**
 * @file:  cpr_measurements.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/24 04:30:15 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/src/cpr_measurements.c#1 $
 * $Change: 9639486 $
 */
#include "cpr_measurements.h"
#include "cpr_logs.h"
#include "cpr_rail.h"
#include "cpr_image.h"
#include "cpr_utils.h"

void cpr_measurements_settle(cpr_rail* rail, cpr_voltage_mode mode)
{
    boolean changed;
    cpr_results rslts;
    uint32 railIdx = rail->railIdx;
    uint32 modeIdx = rail->vp->idxLookupFunc( mode );
    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {mode}};

    cpr_mode_settings* modeSettings = &cpr_info.railStates[railIdx].modeSettings[modeIdx];

    cpr_utils_set_active_mode_setting(rail->id, modeSettings, NULL);

    // we want to settle in the middle of our down and up thresholds.
    const int settle_steps_threshold = rail->halRailCfg.dnThresh - ((rail->halRailCfg.dnThresh + rail->halRailCfg.upThresh) / 2);
    CPR_ASSERT(settle_steps_threshold >= 0);   //For now, only support configs where dn_threshold >= up_threshold.

    if(cpr_image_set_rail_mode( rail->id, &info ))
    {
        cpr_image_measurements_start();

        // make sure to disable CPR before configuring target quotients
        cpr_hal_disable_rail( &rail->hal );
        cpr_hal_set_targets( &rail->hal, mode, 0, modeSettings->targets, modeSettings->targetsCount );
        cpr_hal_start_poll( &rail->hal );

        do {
            cpr_rail_get_results( rail, &rslts );
            changed = cpr_rail_process_results( rail, &rslts );
            cpr_hal_clear_interrupts( &rail->hal );
        }
        while(rslts.down && (rslts.steps > settle_steps_threshold) && changed);

        cpr_hal_stop_poll( &rail->hal );

        cpr_image_measurements_stop();
    }
    else
    {
        CPR_LOG_WARNING(" Failed to settle on mode %d", mode);
    }

    // Restore original mode
    cpr_mode_settings* prevModeSettings = cpr_utils_get_previous_mode_setting(rail->id);

    info.u.mode = prevModeSettings == NULL ? CPR_VOLTAGE_MODE_OFF : prevModeSettings->mode;
    cpr_image_set_rail_mode( rail->id, &info );

    cpr_utils_set_active_mode_setting(rail->id, prevModeSettings, NULL);

    CPR_STATIC_LOG_RAIL_INFO(&cpr_info.railStates[railIdx]);
}

void cpr_measurements_thermal_update(cpr_thermal_region region)
{
    CPR_IMAGE_LOCK_CORE();

    for(int i = 0; i < CPR_NUM_RAILS; i++)
    {
        cpr_rail_state* railState = &cpr_info.railStates[i];
        cpr_rail* rail = cpr_get_rail( railState->id );

        if(!cpr_utils_is_closed_loop_mode(railState->id)) {
            continue;
        }

        CPR_LOG_INFO( "Thermal adjustment on %s. Region: %u", rail->name, region );

        railState->marginAdjustments[CPR_MARGIN_ADJUSTMENT_SOURCE_THERMAL] = rail->enablement->thermalAdjustment[region];
        cpr_rail_update_target_quotients( rail, railState );

        if(cpr_info.railStates[rail->railIdx].activeMode != NULL) {
            cpr_rail_enable( rail, railState->activeMode, railState->activeMode->activeSubMode, true );
        }

        CPR_STATIC_LOG_RAIL_INFO(railState);
    }

    CPR_IMAGE_UNLOCK_CORE();
}

#if 0
/*
 * Set up voltage mode for aging
 * Set up HW for aging
 */
static void cpr_measurements_aging_init(cpr_rail* rail, cpr_aging_cfg* aging_cfg)
{
    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {aging_cfg->modeToRun}};

    // image specific init
    cpr_image_measurements_start();

    CPR_LOG_INFO("Aging init: set %s rail mode to %d", rail->name, aging_cfg->modeToRun);

    // Change mode to specified mode
    cpr_image_set_rail_mode( rail->id, &info );
}

    /*
     * TODO: set up HW for aging
     */
}

static void cpr_measurements_aging_main(cpr_rail* rail, cpr_voltage_mode mode)
{
    /*
     * TODO: run aging algorithm
     */
#if 0
    cpr_rail_state* state = cpr_utils_get_rail_state( rail->id );

    //run CPR aging calculations
    cpr_aging_poll(closed_loop_rail);

    state->marginAdjustments[CPR_MARGIN_ADJUSTMENT_SOURCE_AGING] = cpr_calc_aging();
#endif
}

static void cpr_measurements_aging_cleanup(cpr_rail* rail)
{
    /*
     * TODO: reset HW
     */

    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {CPR_VOLTAGE_MODE_OFF}};

    // image specific cleanup
    cpr_image_measurements_stop();

    CPR_LOG_INFO("Aging cleanup: reset %s rail mode", rail->name);

    //Remove the TURBO vote.
    cpr_image_set_rail_mode( rail->id, &info );
}
#endif

void cpr_measurements_aging(cpr_rail* rail)
{
#if 0
    const cpr_versioned_voltage_plan* vvp = cpr_get_versioned_voltage_plan(rail->id);

    if(!vvp || !vvp->aging_cfg)
    {
        CPR_LOG_INFO("Aging config not found for %s rail", rail->name);
        return;
    }

    cpr_measurements_aging_init( rail, vvp->aging_cfg );

    cpr_measurements_aging_main();

    cpr_measurements_aging_cleanup();

    CPR_STATIC_LOG_RAIL_INFO(state);
#else
    cpr_rail_state* state = cpr_utils_get_rail_state( rail->id );
    state->marginAdjustments[CPR_MARGIN_ADJUSTMENT_SOURCE_AGING] = 0;
#endif
}

