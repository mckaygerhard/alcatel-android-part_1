/**
 * @file:  cpr_hal.c
 *
 * Copyright (c) 2016 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/03/08 00:37:59 $
 * $Header: //components/rel/core.mpss/3.9.1/power/cpr_v3/common/hal/v3/src/cpr_hal.c#3 $
 * $Change: 10025929 $
 *
 */
#include "cpr_hal.h"
#include "cpr_logs.h"
#include "cpr_data.h"
#include "cpr_image.h"
#include "cpr_voltage_plan.h"
#include <string.h>

//******************************************************************************
// Macros / Definitions / Constants
//******************************************************************************

#define ROSC_COUNT 16
#define HAL_CPR_TIMER_DEFAULT_INTERVAL  (5 * 19200)   //5ms * 19.2MHz
#define CPR_DEFAULT_GCNT 19     //We always use 19 since we always run the CPR Ref Clk at 19.2MHz

//******************************************************************************
// Local Helper Functions
//******************************************************************************

static void updateBitMask32(uint32 readAddr, uint32 writeAddr, uint8* bitList, uint32 count, boolean set)
{
    for(int i=0; i < count; i++)
    {
        /* Compute the register to write to and bit position  */
        uint32 reg = (bitList[i] / 32);
        uint32 bit = (bitList[i] % 32);
        uint32 bmask = (1 << bit);

        uint32 val = CPR_HWIO_IN( readAddr + (sizeof(uint32) * reg) );

        /* Mask Sensor */
        if(set) {
            val |= bmask;
        }
        else /* Unmask sensor */ {
            val &= ~bmask;
        }

        CPR_HWIO_OUT( writeAddr + (sizeof(uint32) * reg), val );
    }
}

//******************************************************************************
// Public API Functions
//******************************************************************************

void cpr_hal_clear_interrupts(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT( HWIO_CPR_IRQ_CLEAR_t_ADDR( hdl->base, hdl->thread ), ~0 );
}

void cpr_hal_ack_result(cpr_hal_handle* hdl, boolean ack)
{
    cpr_hal_clear_interrupts(hdl);

    /*
     * set VDD_CHANGED_ONE_STEP bit (bit 0) of CPR_CONT_CMD_t register
     *
     * A write of any data value to this address will send a pulse to cpr_master.cont_ack.
     * The meaning of the cont_ack signal is to indicate that the cpr_master should continue
     * with a next measurement iteration, and that the recommendation of the prior iteration was taken.
     *
     * Tell the controller that the VDD has been changed by
     * exactly one PMIC step compared to the previous measurement.
     * This CSR is per thread
     */
    CPR_HWIO_OUT( HWIO_CPR_CONT_CMD_t_ADDR( hdl->base, hdl->thread ), ack );
}

void cpr_hal_bypass_sensors(cpr_hal_handle* hdl, uint8* sensors, uint16 count)
{
    updateBitMask32( HWIO_CPR_SENSOR_BYPASS_READn_ADDR( hdl->base, 0 ),
                     HWIO_CPR_SENSOR_BYPASS_WRITEn_ADDR( hdl->base, 0 ),
                     sensors,
                     count,
                     true );
}

void cpr_hal_disable_sensors(cpr_hal_handle* hdl, uint8* sensors, uint16 count)
{
    updateBitMask32( HWIO_CPR_SENSOR_MASK_READn_ADDR( hdl->base, 0 ),
                     HWIO_CPR_SENSOR_MASK_WRITEn_ADDR( hdl->base, 0 ),
                     sensors,
                     count,
                     true );
}

void cpr_hal_configure_controller(cpr_hal_handle* hdl, uint16 stepQuotMin, uint16 stepQuotMax)
{
    // TODO - needed?
    //CPR_HWIO_OUT_FIELD(HWIO_CPR_BIST_CHAIN_CHECK0_ADDR(hdl->hw_base_address), HWIO_CPR_BIST_CHAIN_CHECK0_SCLK_CNT_EXPECTED, 32*6);

    CPR_HWIO_OUT( HWIO_CPR_TIMER_AUTO_CONT_ADDR( hdl->base ), HAL_CPR_TIMER_DEFAULT_INTERVAL );
    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_IDLE_CLOCKS, 15 );

    CPR_HWIO_OUT_FIELD( HWIO_CPR_STEP_QUOT_INIT_ADDR( hdl->base ), HWIO_CPR_STEP_QUOT_INIT_STEP_QUOT_INIT_MAX, stepQuotMax );
    CPR_HWIO_OUT_FIELD( HWIO_CPR_STEP_QUOT_INIT_ADDR( hdl->base ), HWIO_CPR_STEP_QUOT_INIT_STEP_QUOT_INIT_MIN, stepQuotMin );

    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_COUNT_MODE, 0 /* ALL_AT_ONCE */);
    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_COUNT_REPEAT, 1 );

    /*
     * Just enable CPR controller. Thread is not enabled yet.
     */
    if(hdl->type != CPR_CONTROLLER_TYPE_HW_CL_CAPABLE) {
        cpr_hal_enable_controller(hdl);
    }
}

void cpr_hal_enable_controller(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_LOOP_EN, 1 );

    CPR_LOG_TRACE("--- cpr_hal_enable_controller (LOOP_EN:%u) ---",
                  CPR_HWIO_IN_FIELD(HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_LOOP_EN));
}

void cpr_hal_disable_controller(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_LOOP_EN, 0 );

    CPR_LOG_TRACE("--- cpr_hal_disable_controller (LOOP_EN:%u) ---",
                  CPR_HWIO_IN_FIELD(HWIO_CPR_FSM_CTL_ADDR( hdl->base ), HWIO_CPR_FSM_CTL_LOOP_EN));
}

void cpr_hal_enable_thread(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_MASK_THREAD_t_ADDR( hdl->base, hdl->thread ), HWIO_CPR_MASK_THREAD_t_DISABLE_THREAD, 0u );
    CPR_LOG_TRACE("--- cpr_hal_enable_thread %d ---", hdl->thread);
}

void cpr_hal_disable_thread(cpr_hal_handle* hdl)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_MASK_THREAD_t_ADDR( hdl->base, hdl->thread ), HWIO_CPR_MASK_THREAD_t_DISABLE_THREAD, 1u );
    CPR_LOG_TRACE("--- cpr_hal_disable_thread %d ---", hdl->thread);
}

void cpr_hal_get_results( cpr_hal_handle* hdl, cpr_results* rslts)
{
    uint32 status = CPR_HWIO_IN( HWIO_CPR_RESULT0_t_ADDR( hdl->base, hdl->thread ) );
    rslts->busy = !!CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT0_t_BUSY );
    rslts->up = !!CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT0_t_STEP_UP );
    rslts->down = !!CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT0_t_STEP_DN );

    rslts->steps = CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT0_t_ERROR_STEPS );

    status = CPR_HWIO_IN( HWIO_CPR_RESULT1_t_ADDR( hdl->base, hdl->thread ) );
    rslts->maxQuot = CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT1_t_QUOT_MAX );
    rslts->minQuot = CPR_HWIO_GET_FIELD_VALUE( status, HWIO_CPR_RESULT1_t_QUOT_MIN );
}

uint32 gcnts_array[] = { 0,0,0,0,0,0,19,19,19,19,19,19,0,0,0,0,0 };
void cpr_hal_init_rail_hw(cpr_hal_handle* hdl, cpr_hal_rail_cfg* cfg)
{
    /*
     * Specify which sensor belongs to which thread
     */
    for(int i = 0; i < cfg->verCfg.v3.sensorsCount; i++) {
        CPR_HWIO_OUT( HWIO_CPR_SENSOR_THREAD_n_ADDR( hdl->base, cfg->verCfg.v3.sensors[i] ), hdl->thread );
    }

    uint32 thresholds = CPR_HWIO_SET_FIELD_VALUE( cfg->upThresh, HWIO_CPR_THRESHOLD_t_UP_THRESHOLD ) |
                        CPR_HWIO_SET_FIELD_VALUE( cfg->dnThresh, HWIO_CPR_THRESHOLD_t_DN_THRESHOLD ) |
                        CPR_HWIO_SET_FIELD_VALUE( cfg->consecUp, HWIO_CPR_THRESHOLD_t_CONSECUTIVE_UP ) |
                        CPR_HWIO_SET_FIELD_VALUE( cfg->consecDn, HWIO_CPR_THRESHOLD_t_CONSECUTIVE_DN );

    CPR_HWIO_OUT( HWIO_CPR_THRESHOLD_t_ADDR( hdl->base, hdl->thread ), thresholds );
    CPR_HWIO_OUT( HWIO_CPR_MASK_THREAD_t_ADDR( hdl->base, hdl->thread ), ~0 );
    
    for(int i = 0; i < ROSC_COUNT; i++) 
        {
            uint32 gcntOffset = (HWIO_CPR_GCNT1_ADDR( 0 ) - HWIO_CPR_GCNT0_ADDR( 0 )) * i;
            CPR_HWIO_OUT_FIELD( HWIO_CPR_GCNT0_ADDR( hdl->base ) + gcntOffset, HWIO_CPR_GCNT0_GCNT0,  gcnts_array[i] );
    }
}

void cpr_hal_set_targets(cpr_hal_handle* hdl, cpr_voltage_mode mode, uint32 freq, cpr_quotient* tgts, uint32 count)
{
    for(int i = 0; i < count; i++)
    {
        if(tgts[i].quotient != 0)
        {
            CPR_LOG_TRACE( " Setting RO %u to %u", tgts[i].ro, tgts[i].quotient );

            uint32 targetOffset = (HWIO_CPR_TARGET1_t_m_ADDR( 0, 0, 0 ) - HWIO_CPR_TARGET0_t_m_ADDR( 0, 0, 0 )) * tgts[i].ro;

            // Unmask RO
            CPR_HWIO_OUT_CLEAR( HWIO_CPR_MASK_THREAD_t_ADDR( hdl->base, hdl->thread ), 1 << tgts[i].ro );

            // TODO: Hardcoded to mode 0 (default) Is this correct?
            CPR_HWIO_OUT_FIELD( HWIO_CPR_TARGET0_t_m_ADDR( hdl->base, hdl->thread, 0 ) + targetOffset,
                                HWIO_CPR_TARGET0_t_m_TARGET0, tgts[i].quotient );
        }
    }
}

void cpr_hal_start_poll(cpr_hal_handle* hdl)
{
    CPR_LOG_TRACE("Start polling");

    cpr_hal_enable_rail( hdl, true, true, true );
}

void cpr_hal_stop_poll(cpr_hal_handle* hdl)
{
    cpr_hal_disable_rail( hdl );

    CPR_LOG_TRACE("Stopped polling");
}

void cpr_hal_enable_up_interrupt(cpr_hal_handle* hdl, boolean enable)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_IRQ_EN_t_ADDR( hdl->base, hdl->thread ),
                        HWIO_CPR_IRQ_EN_t_UP_FLAG_EN,
                        enable );
}

void cpr_hal_enable_down_interrupt(cpr_hal_handle* hdl, boolean enable)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_IRQ_EN_t_ADDR( hdl->base, hdl->thread ),
                        HWIO_CPR_IRQ_EN_t_DOWN_FLAG_EN,
                        enable );
}

void cpr_hal_enable_mid_interrupt(cpr_hal_handle* hdl, boolean enable)
{
    CPR_HWIO_OUT_FIELD( HWIO_CPR_IRQ_EN_t_ADDR( hdl->base, hdl->thread ),
                        HWIO_CPR_IRQ_EN_t_MID_FLAG_EN,
                        enable );
}

/*
 * To re-enable CPR after setting parameters for new mode
 * - Enable Thread by simply writing into related CSR bit  - or set loop_en=1
 */
void cpr_hal_enable_rail(cpr_hal_handle* hdl, boolean up, boolean down, boolean swControl)
{
    if(swControl)
    {
        cpr_hal_enable_up_interrupt(hdl, up);
        cpr_hal_enable_down_interrupt(hdl, down);

        if(hdl->type == CPR_CONTROLLER_TYPE_SW_CL_ONLY) {
            cpr_hal_enable_controller( hdl );
        }

        cpr_hal_enable_thread( hdl );
    }
    else if(hdl->type == CPR_CONTROLLER_TYPE_HW_CL_CAPABLE)
    {
#if defined HWIO_APCS_APCC_CPR_SW_MODE_ADDR
        CPR_HWIO_OUT_FIELD( HWIO_APCS_APCC_CPR_SW_MODE_ADDR( hdl->base ), HWIO_APCS_APCC_CPR_SW_MODE_CPR_SW_MODE, swControl );
#endif
        cpr_hal_enable_controller( hdl );
    }
}

void cpr_hal_disable_rail(cpr_hal_handle* hdl)
{
    cpr_hal_enable_up_interrupt(hdl, false);
    cpr_hal_enable_down_interrupt(hdl, false);

    /*
     * Disable this rail
     *
     * From HPG:
     *
     * 1. One controller, one rail, one thread 
     *  In this case (like CX, GFX) each rail has its own controller and the controller only has one thread.
     *  We can use LOOP_EN for mode switching which is: disable cpr (LOOP_EN=0), reconfigure the mode,
     *  enable CPR (LOOP_EN=1). SW only needs to tell controller the new mode since the targets are already
     *  programmed in controller. We have per mode target registers in CPR3 controller.
     *
     * 2. One controller , one rail, two threads (APPS CPR)
     *  In this case (like HMSS in Istari) the threads are aggregated in HW and SW receives only one interrupt.
     *  In this case SW can use the LOOP_EN approach described in 1.
     *
     * 3. One controller, multi rail, multi thread
     *  In this case (like MX, DDR) SW needs to follow the THREAD disable approach and use the thread disable
     *  CSR for disabling a thread/rail on which mode switch is happening.
     */
    if(hdl->type == CPR_CONTROLLER_TYPE_SW_CL_MULTI_RAILS) {
        cpr_hal_disable_thread( hdl );
    }
    else {
        cpr_hal_disable_controller( hdl );
    }

    // Clear interrupts
    // CPR_HWIO_OUT( HWIO_CPR_IRQ_EN_t_ADDR( hdl->base, hdl->thread ), 0 ); // needed?
    cpr_hal_clear_interrupts(hdl);
}

#ifdef V3_HAL_VERSION

void cpr_hal_write_temp_margin_table(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp) 
{
  // Stubbed out for v4 hal
}

void cpr_hal_mode_change_temp_adj(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp, cpr_enablement* enablement)
{
  // Stubbed out for v4 hal
}

void cpr_hal_write_temp_margin_init_registers(uint8 modeIndex, cpr_hal_handle* hdl, cpr_voltage_plan* vp, cpr_enablement* enablement)
{
  // Stubbed out for v4 hal
}

#endif

