#! /usr/bin/env python
import os
import re
import zlib
import sys

import fuses
import traceback

__all__ = ['VersionedVoltagePlan', 'get_voltage_plan']


def error(msg):
    print msg
    sys.exit(1)

def railValueSet(value, rail):
    value1 = value.split('/')
    for i in range(len(value1)):
        value2 = value1[i].strip().split(':')
        if rail.lower() == value2[0].lower():
            return int(value2[1])
        else:
            value = 0
    return 0

class ParserDict(dict):
    def __init__(self, orig):
        dict.__init__(self, orig)

    def __getitem__(self, args):
        key = args
        rtnType = str

        if isinstance(args, tuple):
            key = args[0]
            rtnType = args[1]
            assert isinstance(rtnType, type)

        val = dict.__getitem__(self, key)
        if val == 'n/a':
            return (None, 0)[rtnType in [int, float]]

        if rtnType is int:
            return int(float(val))
        else:
            return rtnType(val)


class VersionedVoltagePlan:
    def __init__(self, rail, foundry, min, max):
        self.foundry = foundry
        self.rail = rail
        self.minminor = min.split('.')[0]
        self.minmajor = min.split('.')[1]
        self.maxminor = max.split('.')[0]
        self.maxmajor = max.split('.')[1]
        self.enablement = None
        self.modes = []

    class QutoientParams:
        def Z__init__(self):
            self.ro = None
            self.quot = None
            self.kv = None

        def __hash__(self):
            h = 0
            for v in self.__dict__.values():
                h = zlib.crc32(str(v), h)
            return h

        def __repr__(self):
            return str(self)

        def __str__(self):
            return ' [{ro:2}, {quot:4}, {kv:5}]'.format(**self.__dict__)

    class ModeParams:
        def __init__(self):
            self.enabled = []
            self.freq = []
            self.ceiling = []
            self.floor = []
            self.margins = []
            self.fuses = []
            self.fuseRef = 0
            self.quotients = []
            self.coreTable = []

        def __repr__(self):
            return str(self)

        def __str__(self):
            qStr = (str(q) for q in self.quotients)
            return 'e [{enabled:<5}] fq [{freq:<7}] c [{ceiling:<7}] f [{floor:<7}] m [{margins:<4}] fr [{fuseRef:<7}] ' + \
                    '\n\t\t\t\tf {fuses:15} \n\t\t\t\tct {coreTable}\n\t\t\t\ttt {tempThresholds}'.format(**self.__dict__) + \
                   '\n\t\t\t\tro,q,kv ' + ''.join(qStr) + '\n'

        def __hash__(self):
            h = 0
            for v in self.__dict__.values():
                if isinstance(v, list):
                    for x in v:
                        h = zlib.crc32(str(x), h)
                else:
                    h = zlib.crc32(str(v), h)
            return h

    def getModes(self, row, create=False):
        rawMode = row['Mode']

        if rawMode.lower() == 'binning': return []

        # CamelCase to underscores
        mode = re.sub('([a-z0-9])([A-Z])', r'\1_\2', rawMode).upper()

        rtn = [m[1] for m in self.modes if m[0] == mode]

        if create and len(rtn) == 0:
            params = VersionedVoltagePlan.ModeParams()
            self.modes.append((mode, params))
            rtn = [params]

        if rawMode == '*':
            return [m[1] for m in self.modes]

        return rtn

    def __hash__(self):
        h = 0
        for v in self.__dict__.values():
            h = zlib.crc32(str(v), h)
        return h

    def __repr__(self):
        return str(self)

    def __str__(self):
        modeStr = ('{0:11} {1}'.format(m, str(v)) for m, v in self.modes)
        return '{rail} {foundry} {minminor}.{minmajor} {maxminor}.{maxmajor}'.format(**self.__dict__) + '\n\t' + '\n\t'.join(modeStr)


def getrows(sheet):
    headers = [c.value for c in sheet.rows[0] if c.value]
    for row in sheet.rows[1:]:
        data = [str(c.value) for c in row]
        if reduce(lambda acc, x: acc and x == str(None), data, True): continue
        d = dict(zip(headers, data))
        yield d


class VoltagePlanParser:
    def __init__(self, rails):
        self.rails = rails
        self.results = { }
        self.hashval = ''

    def __find_rails_for_row(self, row, create=False):
        rail = row['Voltage Rail']
        domain = row.get('Clock Domain', '*')
        foundry = row['Foundry']
        version = row['Chip Version']
        rev = row.get('CPR Rev', '*')

        if domain != '*':
            rail += '_' + domain

        if rev == 'n/a':
            rev = '*'

        if foundry == '*':
            foundry = 'ANY'

        if self.rails or not rail:
            if not rail or not reduce(lambda acc, x: acc or x.startswith(rail), self.rails, False):
                return []

        if not create and version == '*':
            rtn = []
            for key in self.results:
                r, f, min, max = key
                if r == rail and f == foundry:
                    rtn.append(self.results[key])
            return rtn
        else:
            min = version.replace('x', '0')
            max = version.replace('x', '255')

            if version == '*':
                min = 0
                max = 255

            key = (rail, foundry, str(float(min)), str(float(max)))
            if create:
                self.results[key] = self.results.get(key, VersionedVoltagePlan(*key))

            try:
                return [self.results[key]]
            except KeyError:
                return []

    def __parse_worksheet_voltage_settings(self, file, wb):
        SHEET_NAME         = 'Voltage Settings'
        COL_NAME_ENAB      = 'Enable?'
        COL_NAME_FREQ      = 'Frequency (MHz)'
        COL_NAME_CEILING   = 'CPR Ceiling  (mV)'
        COL_NAME_FLOOR     = 'CPR Floor  (mV)'
        COL_NAME_FUSE_REF  = 'CPR Open Loop Efuse Reference Voltage (mV)'
        COL_NAME_FUSE_REG  = 'CPR Target Quotient Fuse Point'
        COL_NAME_OFFSET_FUSE_REG  = 'CPR Offset Fuse Point'

        if not SHEET_NAME in wb.sheetnames:
            print 'Warning: "%s" worksheet does not exist' % SHEET_NAME
            return

        for row in map(ParserDict, getrows(wb[SHEET_NAME])):
            for vvp in self.__find_rails_for_row(row, True):

                # We are assuming freqs are sorted low to high as we iterate, so fmax is always last...
                for mode in vvp.getModes(row, True):
                    mode.enabled.append(row[COL_NAME_ENAB].lower() == 'yes')
                    mode.freq.append(int(row[COL_NAME_FREQ, float] * 1000)) # to kHz
                    mode.ceiling.append(int(row[COL_NAME_CEILING, float] * 1000)) # to uV
                    mode.floor.append(int(row[COL_NAME_FLOOR, float] * 1000))
                    val = row[COL_NAME_FUSE_REF]
                    if val and (val.isdigit() or re.match(r'\d+\.\d+',val)):
                        mode.fuseRef = int(row[COL_NAME_FUSE_REF, float] * 1000)
                    else:
                        mode.fuseRef = 0
                    fuseval = row[COL_NAME_FUSE_REG]
                    offsetfuseval = row[COL_NAME_OFFSET_FUSE_REG]
  
                    if fuseval and fuseval != 'TBD':
                        if offsetfuseval and offsetfuseval != 'TBD':
                            mode.fuses = fuses.get_fuses(os.path.dirname(file), fuseval, offsetfuseval)
                        else:
                            mode.fuses = fuses.get_fuses(os.path.dirname(file), fuseval, None)

                        if len(mode.fuses) == 0:
                            error('Could not find fuse registers for fuse value: ' + fuseval)
                    
                    for i in range(16):
                        tempBands = []
                        for j in range(4):
                            coreSection = 'Core ' + str(i) + ' Temp Band ' + str(j)
                            if coreSection in row.keys():
                                tempBands.append(row[coreSection])
                        if len(tempBands) != 0:
                            mode.coreTable.append(tempBands)
                        else:
                            break

    def __parse_worksheet_cpr_fused_corner_adjustment(self, wb):
        class Margin:
            def __repr__(self):
                return str(self.__dict__)

        SHEET_NAME           = 'CPR Fused Corner Adjustment'
        COL_NAME_CPR_REV     = 'CPR Rev'
        COL_NAME_CL_ADJ      = 'CPR Closed Loop Voltage Adjustment (mV)'
        COL_NAME_OL_ADJ      = 'CPR Open Loop Voltage Adjustment (mV)'
        COL_NAME_MAX_F2C_ADJ = 'Max Floor To Ceil Range Constant (mV)'

        #import pdb; pdb.set_trace()
        if not SHEET_NAME in wb.sheetnames:
            print 'Warning: "%s" worksheet does not exist' % SHEET_NAME
            return

        for row in map(ParserDict, getrows(wb[SHEET_NAME])):
            for vvp in self.__find_rails_for_row(row):
                for mode in vvp.getModes(row):
                    rev = row[COL_NAME_CPR_REV]
                    cl = row[COL_NAME_CL_ADJ, int]
                    ol = row[COL_NAME_OL_ADJ, int]
                    try:
                        fToC = row[COL_NAME_MAX_F2C_ADJ]
                        if fToC == 'TBD':
                           fToC = 0;
                        else:
                           fToC = row[COL_NAME_MAX_F2C_ADJ, int]
                    except KeyError:
                        fToC = 0

                    curr = Margin()
                    curr.cl, curr.ol = cl, ol
                    curr.fToC = fToC

                    if rev == '*':
                        curr.min, curr.max = 0, 255
                        mode.margins.append(curr)
                    else:
                        rev = int(rev)

                        if len(mode.margins) == 0 or cl != mode.margins[-1].cl or ol != mode.margins[-1].ol or fToC != mode.margins[-1].fToC:
                            curr.min, curr.max = rev, rev
                            mode.margins.append(curr)
                        else:
                            mode.margins[-1].max = rev

    def __parse_worksheet_global_target_quotient(self, wb):
        SHEET_NAME        = 'Global Target Quotient'
        COL_NAME_RO       = 'RO'
        COL_NAME_QUOTIENT = 'CPR Quotient'

        if not SHEET_NAME in wb.sheetnames:
            print 'Warning: "%s" worksheet does not exist' % SHEET_NAME
            return

        for row in map(ParserDict, getrows(wb[SHEET_NAME])):
            for vvp in self.__find_rails_for_row(row):
                q = VersionedVoltagePlan.QutoientParams()
                q.ro = row[COL_NAME_RO, int]
                q.quot = row[COL_NAME_QUOTIENT, int]
                for mode in vvp.getModes(row):
                    mode.quotients.append(q)

    def __parse_worksheet_kv(self, wb):
        SHEET_NAME        = 'KV'
        COL_NAME_RO       = 'RO'
        COL_NAME_QUOTIENT = 'CPR Quotient'

        if not SHEET_NAME in wb.sheetnames:
            print 'Warning: "%s" worksheet does not exist' % SHEET_NAME
            return

        for row in map(ParserDict, getrows(wb[SHEET_NAME])):
            for vvp in self.__find_rails_for_row(row):
                ro = row[COL_NAME_RO, int]
                for mode in vvp.getModes(row):
                    kv = int(row[COL_NAME_QUOTIENT, float] * 100)
                    for q in mode.quotients:
                        if q.ro == ro:
                            q.kv = kv
                            break
                    else:
                        q = VersionedVoltagePlan.QutoientParams()
                        q.ro = ro
                        q.quot = 0
                        q.kv = kv
                        mode.quotients.append(q)

    def __parse_fuse_info(self, fuse_addrs_bits_pair):
        fuse_info = []

        for fuse_addr, fuse_bits in fuse_addrs_bits_pair:
            for bits in fuse_bits.split(','):
                bits = [int(n) if int(n) < 32 else int(n) - 32 for n in bits.split(':')]
                msb = bits[0]
                lsb = bits[len(bits)-1]
                offset = lsb
                mask = ' | '.join(['(1<<%d)' % n for n in range(msb, lsb-1, -1)])

                fuse_info.append({
                    'fuse_address' : 'SECURITY_CONTROL_CORE_REG_BASE | %s' % fuse_addr, # handle image virtual address
                    'fuse_offset'  : offset,
                    'fuse_mask'    : mask,
                })

        return fuse_info

    def __parse_worksheet_logic_aging(self, wb):
        if not 'Logic Aging' in wb.sheetnames:
            print 'Warning: "Logic Aging" worksheet does not exist'
            return

        aging_setting = {}
        rails = set([k[0] for k in self.results.keys() if isinstance(k, tuple)])
        for row in map(ParserDict, getrows(wb['Logic Aging'])):
            #
            # row is something like this:
            #
            #   row['Rail'                 ]: 'APC',
            #   row['CPR Aging enabled'    ]: 'YES'
            #   row['Mode to run aging'    ]: 'Nominal',
            #   row['Sensor ID for aging'  ]: '#11',
            #   row['AGE RO Kv'            ]: '3.2',
            #   row['DERATE_SCALING_FACTOR']: '1.0 (for all modes)',
            #   row['MAX_AGE_COMPENSATION' ]: '15 mV',
            #   row['reduced_age_margin'   ]: '10 mV',
            #   row['comment'              ]: 'Sensor #11 is in M4M. During aging flow M4M is powered ON',
            #
            rail = row['Rail']
            if row['CPR Aging enabled'].lower() == 'yes' and (not rails or rail in rails):
                mode               = row['Mode to run aging'].strip().upper()
                sensor_id          = int(row['Sensor ID for aging'].strip('#'))
                kv_x100            = int(float(re.match(r'(\d+\.\d+)',row['AGE RO Kv']).groups()[0]) * 100)
                scaling_factor_x10 = int(float(re.match(r'(\d+\.\d+)',row['DERATE_SCALING_FACTOR']).groups()[0]) * 10)
                margin_limit       = int(re.match(r'(\d+)', row['MAX_AGE_COMPENSATION']).groups()[0])

                aging_setting[rail] = {
                    'mode'               : mode,
                    'sensor_id'          : sensor_id,
                    'kv_x100'            : kv_x100,
                    'scaling_factor_x10' : scaling_factor_x10,
                    'margin_limit'       : margin_limit,
                }

        self.results['aging'] = aging_setting

    def __parse_worksheet_sw_settings(self, wb):
        if not 'SW Settings (non-HMSS)' in wb.sheetnames:
            print 'Warning: "SW Settings (non-HMSS)" worksheet does not exist'
            return

        sw_settings = {}
        rails = set([k[0] for k in self.results.keys() if isinstance(k, tuple)])
        for row in map(ParserDict, getrows(wb['SW Settings (non-HMSS)'])):
            for rail in rails:
                field   = row['Field']
                value   = row['Value']
                comment = row['Comment']

                if field == 'CPR_STEP_QUOT_INIT.STEP_QUOT_INIT_MAX':
                    field = 'step_quot_max'
                    m = re.match(r'%s:\s*(\d+)' % rail, comment)
                    if m:
                        value = int(m.groups()[0])
                    else:
                        value = int(value)
                elif field == 'CPR_STEP_QUOT_INIT.STEP_QUOT_INIT_MIN':
                    field = 'step_quot_min'
                    m = re.match(r'%s:\s*(\d+)' % rail, comment)
                    if m:
                        value = int(m.groups()[0])
                    else:
                        value = int(value)
                elif field == 'CPR_TIMER_AUTO_CONT.AUTO_CONT_INTERVAL':
                    field = 'auto_cont_interval'
                    value = int(re.match(r'(\d+)',value).groups()[0])
                elif field == 'CPR_THRESHOLD_t.UP_THRESHOLD':
                    field = 'up_thresh'
                    value = int(value)
                elif field == 'CPR_THRESHOLD_t.DN_THRESHOLD':
                    field = 'dn_thresh'
                    value = int(value)
                elif field == 'CPR_THRESHOLD_t.CONSECUTIVE_UP':
                    field = 'consec_up'
                    value = int(value)
                elif field == 'CPR_THRESHOLD_t.CONSECUTIVE_DN':
                    field = 'consec_dn'
                    value = int(value)
                elif field == 'CPR_FSM_CTL.IDLE_CLOCKS':
                    field = 'idle_clocks'
                    value = int(value)
                elif field == 'CPR_TIMER_CLAMP.CLAMP_TIMER_INTERVAL':
                    field = 'clamp_timer_interval'
                    if value and value.isdigit():
                        value = int(value)
                    else:
                        value = 0
                elif field == 'CPR_SENSOR_BYPASS_WRITEn.SENSOR_BYPASS':
                    field = 'sensor_bypass'
                    m = re.match(r'%s:\s*([0-9a-fA-Fx]+)' % rail, value)
                    if m:
                        value = int(m.groups()[0], 0)
                    else:
                        value = 0
                elif field == 'CPR_SENSOR_MASK_WRITEn.SENSOR_MASK':
                    field = 'sensor_mask'
                    value = int(value)
                elif field == 'CPR_FSM_CTL.COUNT_REPEAT':
                    field = 'count_repeat'
                    value = int(value)
                elif field == 'CPR_FSM_CTL.COUNT_MODE':
                    field = 'count_mode'
                    value = int(value)
                elif field.startswith('GCNT'):
                    value = int(value)
                elif field.startswith('CPR_MISC_REGISTER.TEMP_SENSOR_ID_END'):
                    value = railValueSet(value, rail)
                elif field.startswith('CPR_MISC_REGISTER.TEMP_SENSOR_ID_START'):
                    value = railValueSet(value, rail)
                elif field.startswith('CPR_MARGIN_TEMP_POINT0N1.POINT1'):
                    value = railValueSet(value, rail)
                elif field.startswith('CPR_MARGIN_TEMP_POINT0N1.POINT0'):
                    value = railValueSet(value, rail)
                elif field.startswith('CPR_MARGIN_TEMP_POINT2.POINT2'):
                    value = railValueSet(value, rail)
                elif field.startswith('CPR_MARGIN_ADJ_CTL.KV_MARGIN_ADJ_SINGLE_STEP_QUOT'):
                    value = railValueSet(value, rail)
                elif field.startswith('CPR_MARGIN_ADJ_CTL.PER_RO_KV_MARGIN_EN'):
                    value = railValueSet(value, rail)
                else:
                    pass

                if not sw_settings.has_key(rail):
                    sw_settings[rail] = {}

                sw_settings[rail][field] = value

        for rail in rails:
            gcnt = [0 for k in sw_settings[rail] if k.startswith('GCNT')]

            for k in sw_settings[rail].keys():
                if k.startswith('GCNT'):
                    num = int(re.match(r'GCNT(\d+)',k).groups()[0])
                    gcnt[num] = sw_settings[rail][k]
                    del sw_settings[rail][k]

            sw_settings[rail]['gcnt'] = gcnt

        self.results['sw_settings'] = sw_settings

    def __calculate_hash_value(self, wb):
        import hashlib

        #
        # We cannot simply calculate hash on excel file since data
        # can be different due to any styling/format change even though
        # all cell values are same.
        # Extract all cell values and calculate hash value.
        #
        md5 = hashlib.md5()
        for sheet in wb.worksheets:
            for row in sheet.rows:
                for c in row:
                    if isinstance(c.value, unicode):
                        md5.update(c.value.encode('utf-8'))
                    else:
                        md5.update(str(c.value))
        self.hashval = md5.hexdigest().upper()

    def parse_voltage_plan(self, file, enab_cfg=None):
        from openpyxl import load_workbook

        wb = load_workbook(filename=file, data_only=True)

        self.__calculate_hash_value(wb)

        self.__parse_worksheet_voltage_settings(file, wb)
        self.__parse_worksheet_cpr_fused_corner_adjustment(wb)
        self.__parse_worksheet_global_target_quotient(wb)
        self.__parse_worksheet_kv(wb)
        self.__parse_worksheet_logic_aging(wb)
        self.__parse_worksheet_sw_settings(wb)

        return self.results, self.hashval



def get_voltage_plan(rails, file, enab_cfg=None):
    import warnings
    warnings.simplefilter('ignore', UserWarning)

    vvp = VoltagePlanParser(rails)
    return vvp.parse_voltage_plan(file, enab_cfg)


if __name__ == "__main__":
    #
    # NOTE: in order to test this script, need to set PYTHONPATH manually to include
    # target, tools, openpyxl-2.2.2-py2.7.egg directories
    # For example, run below command on the command prompt
    #
    # set PYTHONPATH=<buildpath>\modem_proc\core\power\cpr\target\8996;<buildpath>\modem_proc\core\power\cpr\tools;<buildpath>\modem_proc\core\power\cpr\tools\openpyxl-2.2.2-py2.7.egg
    #

    #vp = get_voltage_plan(None, '../target/test/Voltage Plan - test.xlsx')
    file_path = sys.argv[1]
    rails = None

    if len(sys.argv)>1:
        rails = sys.argv[2:] # 'CX', 'MX', 'MSS', 'VDDA', 'GFX', 'APCC'

    vp, hashval = get_voltage_plan(rails, file_path)

    for k in vp:
        print(vp[k])

    print 'Hash Value:',hashval

