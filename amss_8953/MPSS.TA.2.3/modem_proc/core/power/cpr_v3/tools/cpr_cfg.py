#! /usr/bin/env python
import os
import sys

def main(img, target, out_dir, target_dir=None):
    print 'Generating CPR config files ...'

    tools_dir  = os.path.split(os.path.abspath(sys.argv[0]))[0]
    pyx_dir    = os.path.join(tools_dir, 'openpyxl-2.2.2-py2.7.egg')
    if target_dir is None:
        target_dir = os.path.join(tools_dir, os.sep.join(['..','target',target]))

    sys.path.append(pyx_dir)
    sys.path.append(target_dir)

    from voltage_plan_writer import write_cpr_cfg as vp
    from enablement_writer import write_cpr_cfg as enablement

    cfg = os.path.join(target_dir, 'enablement.cfg')
    src = os.path.join(out_dir, 'cpr_enablement.c')
    hdr = os.path.join(out_dir, 'cpr_enablement.h')

    if os.path.isfile(src):
        os.remove(src)
    if os.path.isfile(hdr):
        os.remove(hdr)

    print '  Parsing config file: %s ...' % cfg
    rails, enab_cfg = enablement(img, cfg, src, hdr)
    print '  Generated %s' % src
    print '  Generated %s' % hdr

    cfg = os.path.join(target_dir, 'Voltage Plan - {0}.xlsx'.format(target))
    src = os.path.join(out_dir, 'cpr_voltage_plan.c')

    if os.path.isfile(src):
        os.remove(src)

    print '  Parsing config file: %s ...' % cfg
    vp(rails, cfg, src, enab_cfg)
    print '  Generated %s' % src


###############################################################################
# Main
###############################################################################
if __name__ == '__main__':
    img    = 'mpss'
    target = '8953'
    out_dir = '.'
    target_dir = None

    if len(sys.argv) == 4:
        img    = sys.argv[1] #mpss, rpm, xbl (images in enablement.cfg)
        target = sys.argv[2] #8996, 8998, 9x55 ...
        out_dir = sys.argv[3]

    main(img, target, out_dir, target_dir=target_dir)

