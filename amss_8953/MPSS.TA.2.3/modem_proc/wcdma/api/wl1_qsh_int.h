#ifndef WL1_QSH_INT_H
#define WL1_QSH_INT_H


/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                            WL1_QSH_INT. H                 

GENERAL DESCRIPTION
   Contains declarations for function which are QUALCOMM proprietary 
   and may or may not be shipped as source code.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath:$
  $Header: 

when       who     what, where, why
--------   ---     ---------------------------------------------------------
06/07/16   sg     Changes to enum 
04/22/16   rn      Changes to support QSH searcher handlers
04/20/16   skk    QSH sleep handlers.
04/18/16   nga    QSH resel handlers
04/08/16   sg     Cell reselection handler
01/06/16   sg     New handlers to qsh testing
08/13/15   sg     Initial version for wl1_qsh_int.h

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#ifdef FEATURE_QSH_EVENT_NOTIFY_HANDLER
#error code not present
#endif
#endif
