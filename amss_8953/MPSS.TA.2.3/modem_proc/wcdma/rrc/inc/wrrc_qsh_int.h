#ifndef WRRC_QSH_INT_H
#define WRRC_QSH_INT_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                            WRRC_QSH_INT. H                 

GENERAL DESCRIPTION
   Contains declarations for function which are QUALCOMM proprietary 
   and may or may not be shipped as source code.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $PVCSPath:$
  $Header: 

when       who     what, where, why
--------   ---     ---------------------------------------------------------
05/16/16   vs     Added DTF handler to force read all the sibs
05/04/16   nr     Added DTF handlers to buffer and post received OTAs
05/04/16   nr     Added support for WRRC QSH Handler framework
05/04/16   vs     Initial version for wrrc_qsh_int.h

===========================================================================*/


/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "comdef.h"
#include <string.h>
#include "trm.h"
#include "qsh.h"
#include "sys.h"


#ifdef FEATURE_QSH_EVENT_NOTIFY_HANDLER
#error code not present
#endif /*FEATURE_QSH_EVENT_NOTIFY_HANDLER*/
#endif /*WRRC_QSH_INT_H*/

