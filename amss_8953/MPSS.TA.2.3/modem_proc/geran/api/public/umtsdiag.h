#ifndef DIAG_UMTS_CMD_H
#define DIAG_UMTS_CMD_H
/*==========================================================================

      Diagnostic Services Packet Processing Command Code Defintions

Description
  This file contains packet id definitions for the serial interface to
  the dmss. This file contains packets common to WCDMA and GSM modes (UMTS)

Copyright (c) 2009-2015 Qualcomm Technologies, Inc.
===========================================================================*/

/* <EJECT> */
/*===========================================================================

                            Edit History

$Header: //components/rel/geran.mpss/7.2/api/public/umtsdiag.h#1 $                             $DateTime: 2015/12/03 03:35:15 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---    -----------------------------------------------------------
09-02-19   tjw     Automatically split from original header file and then tidied.
===========================================================================*/
#define UMTS_CFA_CONFIG_F      9

#endif
