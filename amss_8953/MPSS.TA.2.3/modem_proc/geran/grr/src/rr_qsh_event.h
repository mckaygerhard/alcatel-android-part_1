#ifndef RR_QSH_EVENT_H
#define RR_QSH_EVENT_H

/*!
 * \file rr_qsh_event.h 
 *  
 * This module contains functionality to interface to QSH via EVENTs. 
 *  
 *              Copyright (c) 2016 Qualcomm Technologies, Inc.
 *              All Rights Reserved.
 *              Qualcomm Confidential and Proprietary
 */
/* $Header: //components/rel/geran.mpss/7.2/grr/src/rr_qsh_event.h#4 $ */
/* $DateTime: 2016/07/01 02:28:39 $$Author: pwbldsvc $ */

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include "geran_variation.h"

#if defined(FEATURE_QSH_EVENT_NOTIFY_TO_QSH) || defined(FEATURE_QSH_EVENT_NOTIFY_HANDLER)

#include "qsh.h"
#include "geran_dual_sim.h"

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*Enums for event notifications*/
typedef enum
{
  RR_QSH_EVENT_RESEL                         = 0,
  RR_QSH_EVENT_ASSIGNMENT_FAILURE            = 1,
  RR_QSH_EVENT_HANDOVER_FAILURE              = 2,
  RR_QSH_EVENT_DTM_ASSIGNMENT_FAILURE        = 3,
  RR_QSH_EVENT_G2W_HO_FAILURE                = 4,
  RR_QSH_EVENT_RESEL_FAILURE                 = 5,
  RR_QSH_EVENT_CON_REL_L2_RESET              = 6,
  RR_QSH_EVENT_PCCO                          = 7,
  RR_QSH_EVENT_RACH_FAILURE                  = 8,
  RR_QSH_EVENT_MPLMN_TIMEOUT                 = 9,
  RR_QSH_EVENT_IA_RECEIVED                   = 10,
  RR_QSH_EVENT_G2X_REDIR                     = 11,
  RR_QSH_EVENT_G2X_RESEL_FAILED              = 12,  
  RR_QSH_EVENT_G2W_HO_STARTED                = 13,  
  RR_QSH_EVENT_GSM_SYS_INFO_TIMER_EXPIRY     = 14,
  RR_QSH_EVENT_GPRS_SYS_INFO_TIMER_EXPIRY    = 15,
  RR_QSH_EVENT_RLF                           = 16, 
  RR_QSH_EVENT_IA_PART1_RCVD                 = 17,
  RR_QSH_EVENT_IA_PART2_RCVD                 = 18,
  RR_QSH_EVENT_FIELD_DEBUG_ANY_FAILURE       = 19,
  RR_QSH_EVENT_INVALID  = 0xff
}rr_qsh_event_id_t;

/*Enums for event handlers*/
typedef enum
{
 RR_QSH_EVENT_NOTIFY_L1_DS_ABORT_IND              = 0x00,
 RR_QSH_EVENT_NOTIFY_CONTINUOUS_WCDMA_UPDATE_REQ,
 RR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_W_NEIGH_REQ,
 RR_QSH_EVENT_NOTIFY_UE_MODE_CHANGE_L_NEIGH_REQ,
 RR_QSH_EVENT_NOTIFY_GL1_PTM_TO_IDLE_NEIGH_L,
 RR_QSH_EVENT_NOTIFY_GL1_PTM_TO_IDLE_NEIGH_W,
 RR_QSH_EVENT_NOTIFY_WCDMA_UPDATE_REQ_NO_IRAT,
 RR_QSH_EVENT_NOTIFY_T3126_TIMER_EXPIRY,
 RR_QSH_EVENT_NOTIFY_T3146_TIMER_EXPIRY,
 RR_QSH_EVENT_NOTIFY_SI13_INTERRUPT,
 RR_QSH_EVENT_NOTIFY_RACH_REQ_ABORT,
 RR_QSH_EVENT_NOTIFY_LAST_SI_READ,
 RR_QSH_EVENT_NOTIFY_RESET                        = 0xfe,
 RR_QSH_EVENT_NOTIFY_INVALID = 0xff
}rr_qsh_dtf_handler_id_enum_T;

typedef enum
{
  RR_QSH_RAT_NEIGH_IS_WCDMA = 0x00,
  RR_QSH_RAT_NEIGH_IS_LTE
} rr_qsh_neigh_type;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/*!
 * \brief Performs the EVENT CONFIG call-back action
 * 
 * \param cb_params_ptr (in)
 */
__attribute__((section(".uncompressible.text")))
extern void rr_qsh_event_config_perform_cb_action(qsh_client_cb_params_s *cb_params_ptr);

/*!
 * \brief Notifies an event to QSH if the event notification was enabled by QSH
 *
 * \param event_id(in), gas_id (in)
 */
__attribute__((section(".uncompressible.text")))
extern void rr_qsh_event_notify(rr_qsh_event_id_t event_id, const gas_id_t gas_id);

/*!
 * \brief Maps OTA failure message to QSH event and notifies QSH the same
 *
 * \param msg_type(in), gas_id (in)
 */
__attribute__((section(".uncompressible.text")))
extern void rr_qsh_event_notify_from_ota_failure_msg(uint8 msg_type, const gas_id_t gas_id);

/*!
 * \brief Initialisation function called from RR-QSH when event notification functionality is required.
 */
__attribute__((section(".uncompressible.text")))
extern void rr_qsh_event_init(void);

extern void rr_qsh_event_handler_config_perform_cb_action(qsh_client_cb_params_s *cb_params_ptr);

extern void rr_qsh_wcdma_cell_update_req( uint8 num_neigh, rr_qsh_neigh_type neigh_type, gas_id_t gas_id);

extern void rr_qsh_execute_reset_handler( rr_qsh_dtf_handler_id_enum_T event_id, gas_id_t gas_id);

#endif // FEATURE_QSH_EVENT_NOTIFY_TO_QSH || FEATURE_QSH_EVENT_NOTIFY_HANDLER_TO_QSH

#endif /* #ifndef RR_QSH_EVENT_H */

/* EOF */

