#ifndef RR_CXM_STATE_H
#define RR_CXM_STATE_H
/*============================================================================
  @file rr_cxm_state.h

  @brief This module contains the declarations of types and external functions for CXM L3 state 
  indication feature.

                Copyright (c) 2015 QUALCOMM Technologies, Inc.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
============================================================================*/
/* $Header: //components/rel/geran.mpss/7.2/grr/src/rr_cxm_state.h#1 $ */

/*----------------------------------------------------------------------------
 * Include Files
 *--------------------------------------------------------------------------*/

#include "geran_variation.h"
#include "customer.h"

#ifdef FEATURE_GSM_CXM_L3_STATE
#error code not present
#endif /* FEATURE_GSM_CXM_L3_STATE */
#endif /* RR_CXM_STATE_H */
