#ifndef __MCPM_NPA_H__
#define __MCPM_NPA_H__

/*=========================================================================

           M O D E M   C L O C K   A N D   P O W E R   M A N A G E R
               
                       N P A   H E A D E R   F I L E


  GENERAL DESCRIPTION
    This file contains the definitions for Modem Clock and Power Manager 
    SRR framework

        Copyright (c) 2017 by QUALCOMM Technologies, Inc.  All Rights Reserved.


==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
$Header: //components/rel/mpower_cmn.mpss/1.0.1/api/mcpm_npa.h#1 $

when       who     what, where, why 
--------   ---     ---------------------------------------------------------
03/29/16   ys      Deferred Async Implementation.
08/20/15   hg      Added bCompleteAndSched to mcpm_npa_resrc_request from Atlas as part of common code effort.
07/31/15   sz      Removed inline keyword from MCPM_NPA_Get_Active_Value because it contains translate function now.
07/13/15   sz      Fixed low level warnings.   
04/16/14   sr      Added back inline keyword before function declarations(LLVM compliant). 
04/11/14   sr      Removed inline keyword before function declarations(LLVM compliant). 
03/14/14   ls      Light sleep RPM Sync support. 
01/07/14   ls      RM optimization - unify resrc ID type.
01/24/13   ed      Initial version


==========================================================================*/ 


/*==========================================================================
                     INCLUDE FILES FOR MODULE
==========================================================================*/
#include "mcpm.h"
#include "mcpm_rm.h"
#include "mcpm_utils.h"
#include <npa.h>
#include <npa_scheduler.h>

/* Used for resources that we don't know the aggregated value of */
#define MCPM_NPA_NO_AGG_VALUE MCPM_RESRC_CFG_VALUE_DC


/*==========================================================================
                                TYPES
==========================================================================*/

/* -----------------------------------------------------------------------
**                           TYPES
** ----------------------------------------------------------------------- */
struct mcpm_npa_resrc_data_struct;

typedef struct mcpm_npa_resrc_data_struct mcpm_npa_resrc_data;

/*
 * mcpm_npa_tech_data 
 *  
 * Stores state data for one tech of one SRR resource.
 * 
 * == Set at init ==
 * tNpaHandle   - NPA handle for the tech/resource
 * eTech        - Tech that this client is associated with 
 * pParent      - Pointer to the parent resource data 
 * 
 * == Dynamic at runtime ==
 * nActiveState - Current applied request on the NPA handle
 * bReqPending  - Whether or not a scheduled request is pending on the handle
 * tSchedTime   - If bReqPending is true, stores the time the handle is scheduled for
 * nSchedReq    - If bReqPending is true, stores the value the handle is scheduled for
 */
typedef struct
{
/* These are set only at init */
  npa_client_handle     tNpaHandle;
  mcpm_tech_type        eTech;
  mcpm_npa_resrc_data   *pParent;
/* Resource state */
  npa_resource_state    nActiveState;
  boolean               bReqPending;
  uint64                tSchedTime;
  npa_resource_state    nSchedReq;
} mcpm_npa_tech_data;


/* 
 * mcpm_npa_resrc_data
 *
 * Stores overall resource data for an NPA resource, including data for each tech
 *
 * szResrcName        - Name of the resource 
 * nResrcID           - Index of the resource
 * tCallback          - Callback to be triggered when a scheduled request is applied
 * nAggregatedValue   - Current aggregated value of the resource, if available.
 *                      If not, contains MCPM_NPA_NO_AGG_VALUE
 * tTechData          - Array of data per tech
 */
struct mcpm_npa_resrc_data_struct
{
  const char                    *szResrcName;
  MCPM_Resrc_IDType             nResrcID;
  npa_scheduler_callback        tCallback;
  uint32                        nAggregatedValue;
  mcpm_npa_tech_data            tTechData[MCPM_NUM_TECH];
};


/* 
 * mcpm_npa_resrc_request
 *
 * ImmState       - The value to be applied. If bSchedReq is true, (and scheduling
 *                  is enabled) this value is ignored (assumed to be 0).
 * bSchedReq      - Denotes whether this is a sleep request (scheduled) or not (immediate).
 *                  If true, we will schedule for the currently active state of
 *                  the resource (or same value as was previously scheduled for,
 *                  if there is a pending scheduled request).
 * SchedState     - Schedule state.
 * tSchedTime     - Time to schedule for. If bSchedReq is FALSE, this is ignored
 * bFnFAllowed    - Indicates whether the request can be applied as Fire and
 *                  Forget. This will only get checked if ImmState is lower than
 *                  the previous request on this handle.
 *                  For RF, we can only do Fire and Forget if the request is for
 *                  pam_id 0 (sleep pam_id).
 * bCompleteAndSched - This flag denotes that for that particular resource where it's set
 *                     we need to first do NPA complete and then schedule it, instead of 
 *                     doing a release and schedule. If bSchedReq is FALSE, this is ignored
 */
typedef struct
{
  uint32               ImmState;
  boolean              bSchedReq;
  uint32               SchedState;
  uint64               tSchedTime;
  boolean              bFnFAllowed;
  boolean              bCompleteAndSched;
} mcpm_npa_resrc_request;


/*==========================================================================
               FUNCTION DEFINITIONS FOR MODULE
==========================================================================*/

/* =========================================================================
**  Function : MCPM_NPA_Resrc_Callback
** =========================================================================*/
/**
  Callback to be called when a scheduled resource is applied.
 
  This is a generic one, specific ones can be created if a resource needs it.

  @param client         [in] Client handle which scheduled the change
  @param state          [in] Notification state - 0 for late, 1 for on time
  @param context        [in] Pointer to the client's data structure


  @return
  MyState - Resource commited state.

  @dependencies
  TBD.

*/
void MCPM_NPA_Resrc_Callback
(
  npa_client_handle client, 
  npa_scheduled_notification_state state,
  void *context
);

/* =========================================================================
**  Function : MCPM_NPA_Client_Init
** =========================================================================*/
/**
  Initializes MCPM NPA data for one client (one resource, one tech).

  @param pResrcData     [in] Parent resource structure for this handle
  @param eTech          [in] Tech to register the client for
  @param szNodeName     [in] NPA Name of the node to be registered to
  @param szClientName   [in] Name of the client being registered

  @return
  None

  @dependencies
  TBD.

*/
void MCPM_NPA_Client_Init
(
  mcpm_npa_resrc_data * pResrcData,
  mcpm_tech_type        eTech,
  const char *          szNodeName,
  const char *          szClientName
);


/* =========================================================================
**  Function : MCPM_NPA_Send_Req
** =========================================================================*/
/**
  Checks the current immediate state of the NPA request for this eTech and
  resource, and either sends an immediate request for passed immediate state,
  or if this is a sleep request then schedules a request for passed schedule
  state based on passed wakeup time.

  @param pTechData        [in] Pointer to the SRR data for this tech/resource.
  @param pResrcReq        [in] Pointer to the request parameters.

  @return
  MCPMNPA_StatusType - tells whether request was short-circuited or not
                       This enum is defined in mcpm_api.h

  @dependencies
  Inside the pTechData structure, the following must be set before calling:
  - tNpaHandle  must have a valid NPA handle to a resource
  - eTech       must be set to the correct technology for the request
  - pParent     must be a valid pointer to this tech's parent resource structure
 
  In the parent resource structure, the following must be set before calling:
  - szResrcName must be a pointer to the resource's name string
  - nResrcID    must have a unique resource ID
  - tCallback   must be a pointer to a valid NPA callback function
*/
MCPMNPA_StatusType MCPM_NPA_Send_Req
(
  mcpm_npa_tech_data     *pTechData,
  mcpm_npa_resrc_request *pResrcReq
);


/* =========================================================================
**  Function : MCPM_NPA_Get_SRR_Resrc_Agg_Value
** =========================================================================*/
/**
  Gets aggregated value of a SRR resource.  If aggregated value is valid (i.e.,
  our own NPA driver function), it's return.  Otherwise, max value of all active
  techs is return as an aggregated value.
  

  @param nResrcID           [in] Resource ID.
  @param pResrcProperties   [in] Pointer to the resource properties.

  @return
  MCPM_RM_Resrc_StateType - SRR resource aggregated value.

  @dependencies
  Resource must be initialized with proper properties set.  In addition,
  resource's pData must not be NULL.
*/
MCPM_RM_Resrc_StateType MCPM_NPA_Get_SRR_Resrc_Agg_Value
(
  MCPM_Resrc_IDType                   nResrcID,
  const MCPM_RM_Resrc_PropertiesType  *pResrcProperties
);


/* =========================================================================
**  Function : MCPM_NPA_Async_SRR_Trigger
** =========================================================================*/
/**
  Function to trigger Async SRR requests

  @param eTech        [in] Tech to schedule for 

  @return
  None

  @dependencies
  TBD.

*/
void MCPM_NPA_Async_SRR_Trigger(mcpm_tech_type eTech, boolean basync_deferred);


/* =========================================================================
**  Function : MCPM_NPA_Async_SRR_Complete
** =========================================================================*/
/**
   Function to force a wait for Async SRR requests to complete and reset the
   request to some other value so NPA doesn't short-circuit 

  @param eTech        [in] Tech to wait for 

  @return
  None

  @dependencies
  TBD.

*/
void MCPM_NPA_Async_SRR_Complete(mcpm_tech_type eTech);


/* =========================================================================
**  Function : MCPM_NPA_Async_SRR_Init
** =========================================================================*/
/**
  Initializes Async SRR resource and handles

  @param
  None.

  @return
  None.

  @dependencies
  TBD.

*/
void MCPM_NPA_Async_SRR_Init(void);


/* =========================================================================
**  Function : MCPM_NPA_Sched_Req_Is_Pending
** =========================================================================*/
/**
  Gets "scheduled request pending" flag for a given resource and tech.
 
  This function should not be called for non-schedulable resources.
 
  @param eTech              [in] Tech to get flag for
  @param pResrcProperties   [in] Pointer to resource properties

  @return
  boolean - Returns TRUE if there is a scheduled request pending on that
            tech/resource

  @dependencies
  TBD.

*/
boolean MCPM_NPA_Sched_Req_Is_Pending
(
        mcpm_tech_type                eTech,
  const MCPM_RM_Resrc_PropertiesType *pResrcProperties
);


/* =========================================================================
**  Function : MCPM_NPA_Get_Sched_Req_Value
** =========================================================================*/
/**
  Gets the last value that was scheduled for a given resource and tech.
 
  This function should not be called for non-schedulable resources.
 
  @param eTech              [in] Tech to get value for
  @param pResrcProperties   [in] Pointer to resource properties

  @return
  uint32 - Value of the last scheduled request on that tech/resource

  @dependencies
  TBD.

*/
uint32 MCPM_NPA_Get_Sched_Req_Value
(
        mcpm_tech_type                eTech,
  const MCPM_RM_Resrc_PropertiesType *pResrcProperties
);


/* =========================================================================
**  Function : MCPM_NPA_Get_Active_Value
** =========================================================================*/
/**
  Gets the the current state for a given resource and tech, according to SRR.
 
  This function should not be called for non-schedulable resources.
 
  @param eTech              [in] Tech to get value for
  @param pResrcProperties   [in] Pointer to resource properties

  @return
  uint32 - Active state of tech's (schedulable) resource

  @dependencies
  TBD.

*/
uint32 MCPM_NPA_Get_Active_Value
(
        mcpm_tech_type                eTech,
  const MCPM_RM_Resrc_PropertiesType *pResrcProperties
);
#endif
