#ifndef __MCPM_H__
#define __MCPM_H__

/*=========================================================================

           M O D E M   C L O C K   A N D   P O W E R   M A N A G E R

                        H E A D E R   F I L E


  GENERAL DESCRIPTION
    This file contains the definitions for Modem Clock and Power Manager (MCPM).


  EXTERNALIZED FUNCTIONS
    MCPM_Init
    MCPM_Config_Modem

  INITIALIZATION AND SEQUENCING REQUIREMENTS
    Invoke the MCPM_Init function to initialize the MCPM.

        Copyright (c) 2017 by Qualcomm Technologies, Inc.  All Rights Reserved.


==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mpower_cmn.mpss/1.0.1/api/mcpm.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------
03/24/16   yz      Initial common code version and it's based on CL 10130557 (for TA) and CL 10130735 (for AT) 
                   on Thor: //components/rel/mpower.mpss/5.0/mcpm/common/src/mcpm.h#10 
                    


==========================================================================*/


/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/

/*
 * Includes in the local directory.
 */
#include "mcpm_state.h"


/*
 * Includes outside of local directory.
 */
#include <mcpm_api.h>
#include <mcpm_drv.h>
#include <comdef.h>
#include <slpc.h>
#include <rex.h>
#include <DDIChipInfo.h>


/*=========================================================================
      Constants and Macros
==========================================================================*/

/*=========================================================================
      Typedefs
==========================================================================*/


/*
 * mcpm_slp_tmln_opt_type
 *
 * Sleep timeline option type.
 */
typedef enum
{
  /* Extend timeline */
  MCPM_NPA_SLEEP_SHORTER_EXT,
  /* Opt. timeline */
  MCPM_NPA_SLEEP_LONGER_OPT
} mcpm_slp_tmln_opt_type;


/*
 * mcpm_npa_slp_tmln_type
 *
 * Sleep timeline type.
 */
typedef struct
{
  mcpm_npa_slp_tmln_callback_type slp_tmln_cb;
  mcpm_slp_tmln_opt_type          slp_tmln_option;
} mcpm_npa_slp_tmln_type;


/*
 * MCPM tech specific states.
 */
typedef struct
{

  /* Holds the current state */
  mcpm_state_type                     state;

  /* Holds rxd enable information */
  MCPM_Rcvr_Cfg_RxDType               rxd;

  /* Holds the EQ enable information */
  MCPM_Rcvr_Cfg_EQ_AEQType            eq_Aeq;

  /* Holds the QICE enable information */
  MCPM_Rcvr_Cfg_QICE_SAIC_QLICType    qice;

  /* Holds the WCDMA MIMO enable information */
  MCPM_Rcvr_Cfg_MIMOType              w_mimo;

  /* Holds the neighbor measurement bit mask information */
  MCPM_IRAT_Nbr_InfoType              nbr_meas_info;

  /* Holds the previous neighbor measurement bit mask information */
  MCPM_IRAT_Nbr_InfoType              prev_nbr_meas_info;

  /* Holds the DownLink rate */
  MCPM_dl_datarate_type               dl_rate;

  /* Holds the UpLink rate */
  mcpm_ul_datarate_type               ul_rate;

  /* Holds the bandwidth */
  mcpm_rf_bandwidth_type              bw;

  /* Flag to indicate if this technology is active */
  boolean                             is_active;

  /* MCPM tech cfg states */
  MCPM_Tech_CFG_StateType             tech_cfg_state;
  MCPM_Tech_CFG_StateType             prev_tech_cfg_state;

  /* Tech's active config index obtained from the system spreadsheet */
  MCPM_Sys_Cfg_IDType                 sys_cfg_id;
  MCPM_Sys_Cfg_IDType                 prev_sys_cfg_id;

  /* Tech's standby config index to get the wake-up state from the system spreadsheet */
  MCPM_Sys_Cfg_IDType                 stdby_cfg_id;

  mcpm_block_restore_callback_type    restore_cb[MCPM_NUM_BLOCK];

  mcpm_clk_speed_change_callback_type clk_change_cb[MCPM_NUM_CLK];

  /* RF wakeup time requirement for the tech */
  uint64                              wktm_rf_usec;

  /* Bus/Other rsource wakeup time requirement for the tech */
  uint64                              wktm_gen_usec;

  slpc_id_type                        sleepctl_id;

  /* Previous state is needed to find out if the state transition
   * is SLEEP->WAKEUP Or WAKEUP->SLEEP. WAKEUP->SLEEP can be
   * determined, but first SLEEP->WAKEUP is needed to see if
   * timed RPM requests have to be modified
   */
  mcpm_state_type                     prev_state;

  /* Tech activated or deactivated */
  boolean                             tech_activated;

  /* Tech's slp timeline option */
  mcpm_npa_slp_tmln_type              npa_slp_tmln;

  uint32                              modem_perf_mode;

  /* Tech's active request      */
  mcpm_request_type                   eRequest;
}mcpm_type;


/*=========================================================================
      Variables
==========================================================================*/


/*==========================================================================
               FUNCTION DEFINITIONS FOR MODULE
==========================================================================*/

/*==========================================================================

  FUNCTION      MCPM_Enter_Tech_Atomic_Section

  DESCRIPTION   This function enter MCPM Tech atomic section of a tech.

  PARAMETERS    eTech - MCPM tech info.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

============================================================================*/
void MCPM_Enter_Tech_Atomic_Section
(
  mcpm_tech_type eTech
);


/*==========================================================================

  FUNCTION      MCPM_Leave_Tech_Atomic_Section

  DESCRIPTION   This function exit MCPM Tech atomic section of a tech..

  PARAMETERS    eTech - MCPM tech info.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

============================================================================*/
void MCPM_Leave_Tech_Atomic_Section
(
  mcpm_tech_type eTech
);


/*==========================================================================

  FUNCTION      MCPM_Enter_MCVS_Atomic_Section

  DESCRIPTION   This function enter MCVS atomic section of a tech..

  PARAMETERS    eTech - MCPM tech info.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

============================================================================*/
void MCPM_Enter_MCVS_Atomic_Section
(
  mcpm_tech_type eTech
);


/*==========================================================================

  FUNCTION      MCPM_Leave_MCVS_Atomic_Section

  DESCRIPTION   This function exit MCVS atomic section of a tech..

  PARAMETERS    eTech - MCPM tech info.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

============================================================================*/
void MCPM_Leave_MCVS_Atomic_Section
(
  mcpm_tech_type eTech
);


/*==========================================================================
  FUNCTION      MCPM_Enter_Tech_MCVS_Atomic_Section
============================================================================*/
/**
  This function enters Tech and MCVS atomic section of a tech.

  @param
  eTech              [in]  MCPM tech.
  pmReqParms         [in]  Request parameters.
  pTechLockAcquired  [out] Whether tech lock has been acquired or not.

  @return
  None.

  @dependencies
  MCPM must be initialized.
 
*/
void MCPM_Enter_Tech_MCVS_Atomic_Section
(
  mcpm_tech_type                    eTech,
  mcpm_mcvsrequest_parms_type       *pmReqParms,
  boolean                           *pTechLockAcquired
);


/*==========================================================================
  FUNCTION      MCPM_Leave_Tech_MCVS_Atomic_Section
============================================================================*/
/**
  This function leaves Tech and MCVS atomic section of a tech.

  @param
  eTech              [in]  MCPM tech.
  pTechLockAcquired  [in]  Whether tech lock has been acquired or not.

  @return
  None.

  @dependencies
  MCPM must be initialized.
 
*/
void MCPM_Leave_Tech_MCVS_Atomic_Section
(
  mcpm_tech_type eTech,
  boolean        bTechLockAcquired
);


/*==========================================================================

  FUNCTION      MCPM_Get_State

  DESCRIPTION   This function gets the current state of a tech.

  PARAMETERS    eTech - Tech that we want current state for.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  mcpm_state_type - Current state of the tech.

  SIDE EFFECTS  None.

==========================================================================*/

mcpm_state_type MCPM_Get_State
(
  mcpm_tech_type eTech
);

/*==========================================================================

  FUNCTION      MCPM_Get_Tech_Cfg_State

  DESCRIPTION   This function gets the tech's cfg state.

  PARAMETERS    eTech - Tech that we want current state for.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  MCPM_Tech_CFG_StateType - tech config state

  SIDE EFFECTS  None.

==========================================================================*/

MCPM_Tech_CFG_StateType MCPM_Get_Tech_Cfg_State
(
  mcpm_tech_type eTech
);

/*==========================================================================

  FUNCTION      MCPM_Get_Prev_State

  DESCRIPTION   This function gets the previous state of a tech.

  PARAMETERS    eTech - Tech that we want current state for.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  mcpm_state_type - Current state of the tech.

  SIDE EFFECTS  None.

==========================================================================*/

mcpm_state_type MCPM_Get_Prev_State
(
  mcpm_tech_type eTech
);

/*==========================================================================

  FUNCTION      MCPM_Is_Initialized

  DESCRIPTION   This function returns whether MCPM has been initialized.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  Boolean - Whether MCPM has been initialized.

  SIDE EFFECTS  None.

==========================================================================*/
boolean MCPM_Is_Initialized(void);

/* =========================================================================
**  Function : MCPM_Get_Tech_Cfg_Params
** =========================================================================*/
/**
  Gets tech's configuration parameters based on the input state/parameters and
  associated mapping tables.

  @param
  pTech           [in]  Pointer to tech's input parameters.
  pTechCfgParams  [out] Pointer to tech's config parameters based on mapping
                        tables.

  @return
  None.

  @dependencies
  None.

*/

void MCPM_Get_Tech_Cfg_Params
(
  const mcpm_type          *pTech,
  MCPM_Tech_Cfg_ParamsType *pTechCfgParams
);


/* =========================================================================
**  Function : MCPM_Get_Tech_Instance
** =========================================================================*/
/**
  Gets an instance/copy of current mcpm[eTech].

  @param
  eTech           [in]  Requesting tech.

  @return
  returns an instance/copy of current mcpm[eTech].

  @dependencies
  None.

*/
mcpm_type MCPM_Get_Tech_Instance
(
  mcpm_tech_type eTech
);


/*==========================================================================

  FUNCTION      MCPM_Get_Init_Request

  DESCRIPTION   Returns the init request for a given technology.

  PARAMETERS    eTech - Tech that we want init request for.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  Init request for the given tech, or MCPM_TECH_MAX_REQ if
                the tech has no associated sleep request.

  SIDE EFFECTS  None.

==========================================================================*/

mcpm_request_type MCPM_Get_Init_Request(mcpm_tech_type eTech);



/*==========================================================================

  FUNCTION      MCPM_Get_Wakeup_Req

  DESCRIPTION   Returns the wakeup request for a given technology.

  PARAMETERS    eTech - Tech that we want wakeup request for.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  Wakeup request for the given tech, or MCPM_TECH_MAX_REQ if
                the tech has no associated wakeup request.

  SIDE EFFECTS  None.

==========================================================================*/
mcpm_request_type MCPM_Get_Wakeup_Req(mcpm_tech_type tech);


/*==========================================================================

  FUNCTION      MCPM_Get_Params_Update_Request

  DESCRIPTION   Returns the parameters update request for a given technology.

  PARAMETERS    eTech - Tech that we want parameters update request for.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  Parameters update request for the given tech, or MCPM_TECH_MAX_REQ if
                the tech has no associated parameters update request.

  SIDE EFFECTS  None.

==========================================================================*/

mcpm_request_type MCPM_Get_Params_Update_Request(mcpm_tech_type eTech);


/*==========================================================================

  FUNCTION      MCPM_Get_Sleep_Request

  DESCRIPTION   Returns the sleep request for a given technology.

  PARAMETERS    eTech - Tech that we want sleep request for.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  Sleep request for the given tech, or MCPM_TECH_MAX_REQ if
                the tech has no associated sleep request.

  SIDE EFFECTS  None.

==========================================================================*/

mcpm_request_type MCPM_Get_Sleep_Request(mcpm_tech_type eTech);


/*==========================================================================

  FUNCTION      MCPM_Get_Stop_Request

  DESCRIPTION   Returns the stop request for a given technology.

  PARAMETERS    eTech - Tech that we want stop request for.

  DEPENDENCIES  MCPM must be initialized.

  RETURN VALUE  Stop request for the given tech, or MCPM_TECH_MAX_REQ if
                the tech has no associated stop request.

  SIDE EFFECTS  None.

==========================================================================*/

mcpm_request_type MCPM_Get_Stop_Request(mcpm_tech_type eTech);

/* =========================================================================
**  Function : MCPM_Is_In_INIT_State
** =========================================================================*/
/**
  Returns whether a given tech is in INIT state.

  @param
  eTech    [in] Tech of interest.

  @return
  TRUE -  if tech is in INIT state.
  FALSE - if tech is NOT in INIT state.

  @dependencies
  None.

*/
boolean MCPM_Is_In_INIT_State
(
  mcpm_tech_type eTech
);

#endif /* __MCPM_H__ */

