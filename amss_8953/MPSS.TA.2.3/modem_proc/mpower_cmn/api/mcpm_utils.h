#ifndef __MCPM_UTILS_H__
#define __MCPM_UTILS_H__

/*=========================================================================

        M O D E M   C L O C K   A N D   P O W E R   M A N A G E R
        
                   U T I L S   H E A D E R    F I L E


  GENERAL DESCRIPTION
    This file contains utility functions for the MCPM driver.
  
  EXTERNALIZED FUNCTIONS
    
  INITIALIZATION AND SEQUENCING REQUIREMENTS
    
  
        Copyright (c) 2017 by Qualcomm Technologies, Inc.  All Rights Reserved.


==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
$Header: //components/rel/mpower_cmn.mpss/1.0.1/api/mcpm_utils.h#1 $

when         who     what, where, why 
--------     ---     ---------------------------------------------------------
02/10/16     sc      Added declaration of MCPM_Define_Timer function.
12/18/15     ls      Added PwrDbg feature enhancement - separate modem and tech's sleep/wakeup count.
09/15/15     hg      Added bit count macro.
09/14/15     hg      Added MCPM_MSG_MED_5.
07/08/15     vr      Common code changes. 
06/19/15     vr      Unify F3 msgs and uLogs across targets.
01/30/15     ls      Added multi-tech LPR support in power debug feature. 
03/25/15     sz      Removed sprintf message.
01/06/15     ls      Added light sleep support in power debug feature.
10/28/14     sz      Added disable ulog with NV settings.
07/30/14     ls      1. Added check for TCSR AXI IDLE right after MCDMA channel busy check.
                     2. Added Dalsys_busywait and timeout to all status check while loops. 
02/19/14     ls      Ported latest power debug changes from Dime.
02/13/14     ls      LTE VoLTE and CDRX support.
01/15/14     sr      Added new trace points for profiling. 
12/03/13     sr      Fix timestamps in F3 and ULOG msg for 64-bit timetick values.
10/21/13     sr      MSG_ macro changes.
09/11/13     sr      Removed all warnings in code.
09/03/13     ls      Fixed a bug when printing MCPM_MSG_HIGH6, it was using MSG_FATAL.
01/24/13     yz      Initial version for Bolt


==========================================================================*/ 


/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/

/*
 * Includes not in the local directory.
 */
#include <comdef.h>
#include <msg.h>
#include <err.h>
#include <rex.h>
#include <mcpm_api.h>
#include <mcpm_nv_cfg.h>
#include <mcpm_state.h>
#include <ULog.h>
#include <ULogFront.h>
#include <diagpkt.h>
#ifdef __QDSP6_ARCH__
#include <q6protos.h>
#else
#include <hexagon_protos.h>
#endif


/*=========================================================================
      Constants and Macros
==========================================================================*/
#define MCPM_NO_TRACE_DATA       0  
#define XO_FREQ_FACT             192
#define XO_FREQ_KHZ              (XO_FREQ_FACT * 100)
#define MCPM_TICKS_TO_US(x)      (uint32)(((x)*10)/XO_FREQ_FACT)
#define MCPM_TICKS_TO_MS(x)      (uint32)((MCPM_TICKS_TO_US((x)))/1000)
#define MCPM_MAX(a, b)           (uint32)Q6_R_maxu_RR(a, b)
#define MCPM_MIN(a, b)           (uint32)Q6_R_minu_RR(a, b) 
#define MCPM_BIT_COUNT(a)        (uint32)Q6_R_popcount_P(a)

/* Default next wakeup time. */
#define MCPM_MAX_SYS_TIME64      0xFFFFFFFFFFFFFFFF

/* Macros to display 64-bit timestamps*/
#define MCPM64_LOWWORD(x)  (uint32)x
#define MCPM64_HIGHWORD(x) (uint32)((uint64)x>>32)

/*   
 * Macro to allow forcing an enum to 32 bits.  The argument should be
 * an identifier in the namespace of the enumeration in question.
 */
#define MCPM_ENUM_32BITS(x) MCPM_##x##_FORCE32BITS = 0x7FFFFFFF

/*
 * MCPM_QDSS_TRACE macro
 */
#define MCPM_QDSS_TRACE(req_type)  MCPM_Log_QDSS_Event(req_type)

/*
 * There are cases that MCPM register access needs to wait some CPU cycles
 * before the next access.
 */

#define MCPM_WAIT_FIXEDTIME    MCPM_Wait_Fixed_Time()

/* Macro defines for the MCPM F3 msg utils*/
#define MCPM_MSG_FATAL(x_fmt, a, b, c)  \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_ERROR_UP) { \
    MSG_3(MSG_SSID_MPOWER, MSG_LEGACY_FATAL, x_fmt, a, b, c);  \
  }

#define MCPM_MSG_ERROR(x_fmt)  \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_ERROR_UP) { \
    MSG(MSG_SSID_MPOWER, MSG_LEGACY_ERROR, x_fmt);  \
  }

#define MCPM_MSG_ERROR_1(x_fmt, a)  \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_ERROR_UP) { \
    MSG_1(MSG_SSID_MPOWER, MSG_LEGACY_ERROR, x_fmt, a);  \
  }

#define MCPM_MSG_ERROR_2(x_fmt, a, b)  \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_ERROR_UP) { \
    MSG_2(MSG_SSID_MPOWER, MSG_LEGACY_ERROR, x_fmt, a, b);  \
  }

#define MCPM_MSG_ERROR_3(x_fmt, a, b, c)  \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_ERROR_UP) { \
    MCPM_ULOG_3(x_fmt, a, b, c);   \
    MSG_3(MSG_SSID_MPOWER, MSG_LEGACY_ERROR, x_fmt, a, b, c);  \
  }

#define MCPM_MSG_ERROR_4(x_fmt, a, b, c, d)  \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_ERROR_UP) { \
    MCPM_ULOG_4(x_fmt, a, b, c, d);   \
    MSG_4(MSG_SSID_MPOWER, MSG_LEGACY_ERROR, x_fmt, a, b, c, d);  \
  }
#define MCPM_MSG_HIGH(x_fmt)   \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) { \
    MSG(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt);  \
  }

#define MCPM_MSG_HIGH_1(x_fmt, a)   \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) { \
    MSG_1(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a);  \
  }

#define MCPM_MSG_HIGH_2(x_fmt, a, b)   \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) { \
    MSG_2(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b);  \
  }

#define MCPM_MSG_HIGH_3(x_fmt, a, b, c)   \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) { \
    MSG_3(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b, c);  \
  }

#define MCPM_MSG_HIGH_4(x_fmt, a, b, c, d)                    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) {                       \
    MSG_4(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b, c, d);  \
  }

#define MCPM_MSG_HIGH_5(x_fmt, a, b, c, d, e)  \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) {                       \
    MSG_5(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b, c, d, e);  \
  }

#define MCPM_MSG_HIGH_6(x_fmt, a, b, c, d, e, f)  \
   if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP ) { \
    MSG_6(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b, c, d, e, f);  \
   }

#define MCPM_MSG_HIGH_7(x_fmt, a, b, c, d, e, f, g)                    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) {                             \
    MSG_7(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b, c, d, e, f, g);  \
  }

#define MCPM_MSG_HIGH_8(x_fmt, a, b, c, d, e, f, g, h)                    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) {                             \
    MSG_8(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b, c, d, e, f, g, h);  \
  }

#define MCPM_MSG_HIGH_9(x_fmt, a, b, c, d, e, f, g, h, i)                    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) {                             \
    MSG_9(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b, c, d, e, f, g, h, i);  \
  }

#define MCPM_MSG_HIGH_10(x_fmt, a, b, c, d, e, f, g, h, i, j)                    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) {                             \
    MSG_10(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b, c, d, e, f, g, h, i, j);  \
  }

#define MCPM_MSG_MED(x_fmt)    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_MED_UP) { \
    MSG(MSG_SSID_MPOWER, MSG_LEGACY_MED, x_fmt);  \
  }

#define MCPM_MSG_MED_1(x_fmt, a)    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_MED_UP) { \
    MSG_1(MSG_SSID_MPOWER, MSG_LEGACY_MED, x_fmt, a);  \
  }

#define MCPM_MSG_MED_2(x_fmt, a, b)    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_MED_UP) { \
    MSG_2(MSG_SSID_MPOWER, MSG_LEGACY_MED, x_fmt, a, b);  \
  }

#define MCPM_MSG_MED_3(x_fmt, a, b, c)    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_MED_UP) { \
    MSG_3(MSG_SSID_MPOWER, MSG_LEGACY_MED, x_fmt, a, b, c);  \
  }

#define MCPM_MSG_MED_4(x_fmt, a, b, c, d)    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_MED_UP) { \
    MSG_4(MSG_SSID_MPOWER, MSG_LEGACY_MED, x_fmt, a, b, c, d);  \
  }

#define MCPM_MSG_MED_5(x_fmt, a, b, c, d, e)    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_MED_UP) { \
    MSG_5(MSG_SSID_MPOWER, MSG_LEGACY_MED, x_fmt, a, b, c, d, e);  \
  }

#define MCPM_MSG_LOW(x_fmt, a, b, c)    \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_ALL) { \
    MSG_3(MSG_SSID_MPOWER, MSG_LEGACY_LOW, x_fmt, a, b, c);  \
  }

#define MCPM_ERR_FATAL(x_fmt, a, b, c) \
  ERR_FATAL(x_fmt, a, b, c)

#define MCPM_MSG_HIGH_SPRINTF(x_fmt, a, b, c)   \
  if (MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP) { \
    MCPM_ULOG_3(x_fmt, a, b, c); \
    MSG_SPRINTF_3(MSG_SSID_MPOWER, MSG_LEGACY_HIGH, x_fmt, a, b, c);  \
  }

extern ULogHandle mcpm_ulog_handle;

#define MCPM_ULOG_0(formatStr) \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_0(mcpm_ulog_handle, formatStr); \
  }
#define MCPM_ULOG_1(formatStr, a)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_1(mcpm_ulog_handle, formatStr, a); \
  }
#define MCPM_ULOG_2(formatStr, a, b)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_2(mcpm_ulog_handle, formatStr, a, b); \
  }
#define MCPM_ULOG_3(formatStr, a, b, c)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_3(mcpm_ulog_handle, formatStr, a, b, c); \
  }
#define MCPM_ULOG_4(formatStr, a, b, c, d)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_4(mcpm_ulog_handle, formatStr, a, b, c, d); \
  }
#define MCPM_ULOG_5(formatStr, a, b, c, d, e)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_5(mcpm_ulog_handle, formatStr, a, b, c, d, e); \
  }
#define MCPM_ULOG_6(formatStr, a, b, c, d, e, f)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_6(mcpm_ulog_handle, formatStr, a, b, c, d, e, f); \
  }
#define MCPM_ULOG_7(formatStr, a, b, c, d, e, f, g)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_7(mcpm_ulog_handle, formatStr, a, b, c, d, e, f, g); \
  }
#define MCPM_ULOG_8(formatStr, a, b, c, d, e, f, g, h)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_8(mcpm_ulog_handle, formatStr, a, b, c, d, e, f, g, h); \
  }
#define MCPM_ULOG_9(formatStr, a, b, c, d, e, f, g, h, i)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_9(mcpm_ulog_handle, formatStr, a, b, c, d, e, f, g, h, i); \
  }
#define MCPM_ULOG_10(formatStr, a, b, c, d, e, f, g, h, i, j)  \
  if(MCPM_Utils_ULog_Enabled() && MCPM_Get_Log_Msg_Level() <= MCPM_MSG_HIGH_UP){ \
    ULOG_RT_PRINTF_10(mcpm_ulog_handle, formatStr, a, b, c, d, e, f, g, h, i, j); \
  }

/* The COMPILER_VERIFY(exp) macro allows checking (exp) is true or not at compile time instead
 * of runtime.
 */
#define GLUE(a,b) __GLUE(a,b)
#define __GLUE(a,b) a ## b
#define CMPL_VERIFY(exp, msg) typedef char GLUE(COMPIER_VERIFY_, msg) [(exp) ? (+1) : (-1)]
#define COMPILER_VERIFY(exp) CMPL_VERIFY((exp), __LINE__)

/*
 * MCPM Diag Cmd Definitions
 */
/* Command code mapping */ 
#define MCPM_DIAG_UNIT_TEST_F            0

/* MCPM Diag Unit Test Request Packet */
DIAGPKT_SUBSYS_REQ_DEFINE(MPOWER, MCPM_DIAG_UNIT_TEST_F)
DIAGPKT_REQ_END

 /* MCPM Diag Unit Test Response Packet  */
DIAGPKT_SUBSYS_RSP_DEFINE(MPOWER, MCPM_DIAG_UNIT_TEST_F)
  //variable can be defined here for response packet
DIAGPKT_RSP_END

/*=========================================================================
                    Typedefs
==========================================================================*/

/*
 * MCPM_Msg_Level_Type 
 *  
 * Enum types for MCPM message level.
 */
typedef enum
{
  MCPM_MSG_ALL,
  MCPM_MSG_MED_UP,
  MCPM_MSG_HIGH_UP,
  MCPM_MSG_ERROR_UP
} MCPM_Msg_Level_Type;


/*==========================================================================
               FUNCTION DEFINITIONS FOR MODULE
==========================================================================*/


/*==========================================================================

  FUNCTION      MCPM_Utils_Init

  DESCRIPTION   This function initializes MCPM util related states/variables.
                It is called from MCPM_Init function.

  PARAMETERS    pNVCfg - pointer to NV config.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Utils_Init(const MCPM_NV_CFG_T *pNVCfg);


/*==========================================================================

  FUNCTION      MCPM_ValidateRegSetTimeout

  DESCRIPTION   This function validates register status, if it has not been set
                properly, wait 1us every register-read until timeout.

  PARAMETERS    nAddr      - register address.
                nMask      - register mask.
                nTimeoutUS - time out value.

  DEPENDENCIES  None.

  RETURN VALUE  TRUE       - register has been set.
                FALSE      - register hasn't been set within nTimeoutUS.

  SIDE EFFECTS  None.

==========================================================================*/

boolean MCPM_ValidateRegSetTimeout
(
  uint32 nAddr,
  uint32 nMask,
  uint32 nTimeoutUS
);


/*==========================================================================

  FUNCTION      MCPM_ValidateRegClearTimeout

  DESCRIPTION   This function validates register status, if it has not been
                cleared properly, wait 1us every register-read until timeout.

  PARAMETERS    nAddr      - register address.
                nMask      - register mask.
                nTimeoutUS - time out value.

  DEPENDENCIES  None.

  RETURN VALUE  TRUE       - register has been cleared.
                FALSE      - register hasn't been cleared within nTimeoutUS.

  SIDE EFFECTS  None.

==========================================================================*/

boolean MCPM_ValidateRegClearTimeout
(
  uint32 nAddr,
  uint32 nMask,
  uint32 nTimeoutUS
);
/*==========================================================================

  FUNCTION      MCPM_Utils_ULog_Enabled

  DESCRIPTION   This function returns if MCPM Ulog is enabled by NV.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

uint32 MCPM_Utils_ULog_Enabled(void);
/*==========================================================================

  FUNCTION      MCPM_Utils_ULog_Set_Enabled

  DESCRIPTION   This function sets MCPM ulog enabled global.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Utils_ULog_Set_Enabled(uint32 enabled);

/*==========================================================================

  FUNCTION      MCPM_Get_Log_Msg_Level

  DESCRIPTION   This function returns currently set MCPM log message level.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

MCPM_Msg_Level_Type MCPM_Get_Log_Msg_Level(void);


/*==========================================================================

  FUNCTION      MCPM_Set_Log_Msg_Level

  DESCRIPTION   This function sets MCPM log message level.

  PARAMETERS    eMsgLevel - Message level.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Set_Log_Msg_Level(MCPM_Msg_Level_Type eMsgLevel);


/*==========================================================================

  FUNCTION      MCPM_Log_QDSS_Event

  DESCRIPTION   This logs mcpm event into QDSS buffer.

  PARAMETERS    eRequest - Request type.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Log_QDSS_Event(mcpm_request_type eRequest);


/*==========================================================================

  FUNCTION      MCPM_GetTimetick64

  DESCRIPTION   This function returns time tick (64-bit) value.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  uint64 - timetick value.

  SIDE EFFECTS  None.

==========================================================================*/

uint64 MCPM_GetTimetick64 (void);


/*==========================================================================

  FUNCTION      MCPM_Power_Debug_Init

  DESCRIPTION   This function initializes power debug parameters.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Power_Debug_Init(void);


/*==========================================================================

  FUNCTION      MCPM_Power_Debug_ErrFatal_CB

  DESCRIPTION   This function is called when power debug timer expires.

  PARAMETERS    action - Whether to crash UE or print F3 message.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Power_Debug_ErrFatal_CB(uint8 action);


/*==========================================================================

  FUNCTION      MCPM_Power_Debug_Set_Timer

  DESCRIPTION   This function sets power debug timer.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Power_Debug_Set_Timer(void);


/*==========================================================================

  FUNCTION      MCPM_Power_Debug_Clear_Timer

  DESCRIPTION   This function clears power debug timer.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Power_Debug_Clear_Timer(void);


/*==========================================================================

  FUNCTION      MCPM_Is_Power_Debug_Enabled

  DESCRIPTION   This API returns whether MCPM power debug is enabled.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  boolean - whether MCPM power debug is enabled.

  SIDE EFFECTS  None.

==========================================================================*/

boolean MCPM_Is_Power_Debug_Enabled(void);


/*==========================================================================

  FUNCTION      MCPM_Power_Debug

  DESCRIPTION   This is the main API for MCPM power debug functionality.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Power_Debug(void);


/*==========================================================================

  FUNCTION      MCPM_Power_Debug_Increase_Modem_Sleep_Count

  DESCRIPTION   This function increments power debug internal count for
                sleep. Please note that, sleep here indicate modem sleep.

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Power_Debug_Increase_Modem_Sleep_Count();


/*==========================================================================

  FUNCTION      MCPM_Power_Debug_Increase_Modem_Wakeup_Count

  DESCRIPTION   This function increments power debug internal count for
                wakeup. Please note that, sleep here indicate modem wakeup.

  PARAMETERS    None.
 
  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Power_Debug_Increase_Modem_Wakeup_Count();


/*==========================================================================

  FUNCTION      MCPM_Power_Debug_Increment_Count

  DESCRIPTION   This function increments power debug internal count for
                voice, data or sleep states.

  PARAMETERS    eTech      - current tech.
                eCurrState - current MCPM state.
                ePrevState - previous MCPM state.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Power_Debug_Increment_Count
(
  mcpm_tech_type  eTech,
  mcpm_state_type eCurrState,
  mcpm_state_type ePrevState
);

/*==========================================================================

  FUNCTION      MCPM_Power_Debug_Multi_Tech_LPR_Clear_Timer

  DESCRIPTION   In DSDS scenario, if current wakeup tech is not the one specified
                in NV, clear the timer which is set by LPR Exit so that the wakeup
                duration won't be checked on the current tech. 

  PARAMETERS    eTech - current wakeup tech.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/

void MCPM_Power_Debug_Multi_Tech_LPR_Clear_Timer(mcpm_tech_type eTech);
/*==========================================================================

  FUNCTION      MCPM_Wait_Fixed_Time

  DESCRIPTION   Time wait function to implement 10 ns wait to be used such as
        in b/w clk regiseter MUX and DIV

  PARAMETERS    None.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/
void MCPM_Wait_Fixed_Time(void);

/*==========================================================================

  FUNCTION      MCPM_Get_Q6_CpuVdd_Count

  DESCRIPTION   gets cpu_vdd (standalone + rpm assisted) count from sleep stats

  PARAMETERS    None.

  DEPENDENCIES  MCPM_Utils_Init must be run first

  RETURN VALUE  number of cycles that the 'cpu_vdd' mode has been entered

  SIDE EFFECTS  None.

==========================================================================*/
uint32 MCPM_Get_Q6_CpuVdd_Count(void);

/*==========================================================================

  FUNCTION      MCPM_Get_Q6_RPM_Count

  DESCRIPTION   gets 'rpm' assisted PC count from sleep stats

  PARAMETERS    None.

  DEPENDENCIES  MCPM_Utils_Init must be run first 

  RETURN VALUE  number of cycles that the 'rpm' mode has been entered

  SIDE EFFECTS  None.

==========================================================================*/
uint32 MCPM_Get_Q6_RPM_Count(void);

/*===========================================================================

  FUNCTION MCPM_DiagF3_NV_Disabled

  DESCRIPTION Reads diag F3 control NV.

  DEPENDENCIES NV 

  RETURN VALUE

  SIDE EFFECTS 
    This runs in RCInit worker thread context, blocking initialization while
    waiting on NV.

===========================================================================*/
uint32 MCPM_DiagF3_NV_Disabled( void );


/*==========================================================================

  FUNCTION      MCPM_Define_Timer

  DESCRIPTION   This function defines a timer.

  PARAMETERS    tmr_ptr         - pointer to the timer.
                tmr_group_ptr   - pointer to the timer group.
                timer_cb_ptr    - pointer to the timer callback.
                cb_param        - callback parameters.
                deferrable      - flag to indicate whether timer is deferrable.

  DEPENDENCIES  None.

  RETURN VALUE  None.

  SIDE EFFECTS  None.

==========================================================================*/


void MCPM_Define_Timer (timer_type*        tmr_ptr,
                        timer_group_type*  tmr_group_ptr,
                        rex_timer_cb_type  timer_cb_ptr,
                        unsigned long      cb_param, 
                        boolean            deferrable);


#endif /* __MCPM_UTILS_H__ */






