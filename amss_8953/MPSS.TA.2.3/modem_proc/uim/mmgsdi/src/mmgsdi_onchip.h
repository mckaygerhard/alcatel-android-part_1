#ifndef MMGSDI_ONCHIP_H
#define MMGSDI_ONCHIP_H
/*===========================================================================


           M M G S D I   O N C H I P   H E A D E R S


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2016 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.  
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/uim.mpss/5.1/mmgsdi/src/mmgsdi_onchip.h#1 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------
04/05/16   sp      Initial version

=============================================================================*/

/*=============================================================================

                     INCLUDE FILES FOR MODULE

=============================================================================*/

#include "mmgsdi.h"

/* ============================================================================

   FUNCTION:      MMGSDI_ONCHIP_GET_FILE_ATTR

   DESCRIPTION:
     This function returns file attribute for onchip files
     Currently support ICCID only

   DEPENDENCIES:
     None

   LIMITATIONS:
     None

   RETURN VALUE:
     mmgsdi_return_enum_type

     MMGSDI_SUCCESS:          The funtion was successful in getting the file
                              attributes of a particular file.
     MMGSDI_ERROR:
     MMGSDI_INCORRECT_PARAMS: The parameters supplied to the function are not
                              within appropriate ranges.

   SIDE EFFECTS:
     None

=============================================================================*/
mmgsdi_return_enum_type mmgsdi_onchip_get_file_attr(
  mmgsdi_get_file_attr_req_type * req_ptr,
  mmgsdi_protocol_enum_type       protocol
);


/* ============================================================================
  FUNCTION:      MMGSDI_ONCHIP_ACTIVATE

  DESCRIPTION:
    This function will be used to activate the ONCHIP USIM or SIM.  It will
    configure the USIM/SIM Cache.

  DEPENDENCIES:


  LIMITATIONS:
    USIM Authentication in USIM mode is not supported.

  RETURN VALUE:
    mmgsdi_return_enum_type

    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.
    MMGSDI_INCORRECT_PARAMS: The parameters supplied to the API are not
                             within appropriate ranges.
    MMGSDI_NOT_SUPPORTED:    When the file has an entry in the Service Table
                             but the service table indicates the card does not
                             have the support

  SIDE EFFECTS:
    None.

  CRITICAL SECTIONS:
    The function uses mmgsdi_client_app_data_crit_sect_ptr to protect accesses
    to slot data ptr, channel info ptr, pin info ptr and app info ptr.
============================================================================*/
mmgsdi_return_enum_type mmgsdi_onchip_activate(
  mmgsdi_activate_onchip_sim_req_type * onchip_sim_config_ptr,
  mmgsdi_onchip_mode_enum_type          onchip_mode
);


/*============================================================================
  FUNCTION:      MMGSDI_ONCHIP_ACTIVATE_SESSION

  DESCRIPTION:
    This function activates session for onchip and brings subscription to ready

  DEPENDENCIES:

  LIMITATIONS:
    
  RETURN VALUE:
    mmgsdi_return_enum_type

    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.

  SIDE EFFECTS:
    None.

  CRITICAL SECTIONS:
============================================================================*/
mmgsdi_return_enum_type mmgsdi_onchip_activate_session(
  mmgsdi_session_act_or_switch_prov_req_type *msg_ptr
);


/* ============================================================================
  FUNCTION:      MMGSDI_ONCHIP_DEACTIVATE_SESSION

   DESCRIPTION:
     This function will only update the channel info table info in regard to the 
     session that has been deactivated in the case of onchip.
     This function does not delete app related cache.

   DEPENDENCIES:

   LIMITATIONS:

   RETURN VALUE:
    mmgsdi_return_enum_type
 
    MMGSDI_SUCCESS:          The command processing was successful.
    MMGSDI_ERROR:            The command processing was not successful.

   SIDE EFFECTS:
     None.

   CRITICAL SECTIONS:
     The function uses mmgsdi_client_app_data_crit_sect_ptr to protect accesses
     to session info ptr and channel info ptr.
============================================================================*/
mmgsdi_return_enum_type mmgsdi_onchip_deactivate_session(
  mmgsdi_slot_id_enum_type             slot_id,
  mmgsdi_session_id_type               session_id,
  mmgsdi_int_app_info_type            *app_info_ptr
);

#endif /* MMGSDI_ONCHIP_H */
