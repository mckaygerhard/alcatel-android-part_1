/*============================================================================
  FILE:         uimdrv_enumeration.c

  OVERVIEW:     File deals with the configuration and reading of the efs file
                along with enabling defaults according to the chipset got by
                querying the Dal api to get the chip id.

  DEPENDENCIES: N/A

                Copyright (c) 2012 - 2015 QUALCOMM Technologies, Inc(QTI).
                All Rights Reserved.
                QUALCOMM Technologies Confidential and Proprietary
============================================================================*/

/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

$Header: //components/rel/uim.mpss/5.1/uimdrv/src/enumeration/uimdrv_enumeration.c#7 $
$DateTime: 2016/08/19 06:23:54 $
$Author: pwbldsvc $

 when       who        what, where, why
------      ----       -----------------------------------------------------------
08/18/16    gm         8920 bringup changes
05/31/16    gm         F3 message removal
05/27/16    ks         Removal of zero value check for GPIO num in uim_hw_config
04/06/16    sam        8940 bringup changes
04/01/16    ks         8976/56 Bring up change
03/28/16    sam        move declaration of'uim_get_chip_index_from_table' to a header 
03/03/16    na         Added support of external pull up
21/01/16    ssr        8953 Bring up changes
11/05/15    ll         8998 Bring up change
10/27/15    hyo        Avoid writing enum to EFS
09/24/15    ll         Add guards depends on UARTDM version and clock tree HW
08/17/15    ll         Enable HOTSWAP by default for CUST_1
08/11/15    hyo        usb uicc removal
07/20/15    ks         Consolidating UIM bring-up changes
05/21/15    hyo        Disable UIM2 for Fluid/Liquid when we read HWConfig NV
05/14/15    hyo        Disable UIM2 for Fluid platform
05/08/15    hyo        Disable UIM2 for Liquid platform
04/28/15    ll         9x55 Bring-up changes
04/08/15    sam        FR24498: Powerup logging new changes
04/06/15    ll         UIMDRV migrates to UIM COMMON EFS APIs for all EFS items
03/03/15    hyo        HW Config validation update
03/03/15    hyo        8996 Bring-up changes
03/02/15    na         Using the DAL chip enums instead of hardcoding values for 8952
02/17/15    hyo        Check for nearly all zero HW Config and apply default values
01/16/15    na         Suppport of RUMI
01/07/15    sam        Modification of msg macros for FR24498-UIM Powerup Logging
12/02/14    ks         Feature to support GPIO based hotswap
11/24/14    kr         Fixed Compilation warnings
11/11/14    ks         Using the DAL chip enums instad of hardcoding values
11/06/14    nmb        8952 bring up changes
09/23/14    ks         8909 bring up changes
08/25/14    akv        UIM disablement for all chipsets when RUMI_EMULATION flag defined
08/21/14    ak         Replace 9x45 DAL chip family reference for compilation on BOLT
08/20/14    nmb        9x45 Bring up changes
08/14/14    ks         Hotswap enhancement for flaky mechanical switches
07/04/14    ssr        Fix for APDU debug Mask
06/27/14    ks         Fixed to update the m_hwconfig global appropriately
06/23/14    ssr        Fixed to enable the SWP protocol by default for Slot1
06/16/14    ll         Switch to new string based TLMM APIs
05/12/14    ak         8994 Bring up changes
05/06/14    ks         8936 bring up changes
04/11/14    na         Added support for SWP NV item to encode SWP in Terminal Capability
04/01/14    lxu        Use new uim remote power down API to deal with card power
                       down command,set nv 70210 default version to 2
03/27/14    lxu        USB UICC uimdrv implement
02/18/14    sam        Usage of newer style diag macros
02/04/14    sam        Fix for Klocwork issues
01/31/14    ak         Removed uimBooleanEnum type
12/27/13    ks         8916 bring up changes
11/27/13    akv        Initialization of hw enumeration based on dal chip family
11/27/13    akv        Changes for voting against/for sleep on 9x35
10/31/13    ll         Enable both UIM1 and UIM2 by default for 8974pro and 8926
10/16/13    ak         Fix to ensure that disableUim == TRUE for disabled interfaces
10/09/13    ks         Fix in uim_get_hw_max_slots_available func to return number
                       of slots appropriately
09/30/13    akv        Removal of if-else cases and unused code in hw enumeration
09/11/13    na         8x62 bring up changes
08/29/13    na         allow UIM2 config on UIM1 when UIM1 is disabled and UIM2 enabled
08/22/13    js         Add 8926 chip family check
08/22/13    js         Add 8926 and 8974pro chip family checks
08/06/13    rm/ak      Bolt RUMI code
07/12/13    akv        HW Enumeration - modifying target specific hardware values
                       and organizing them in value-key pairs
06/24/13    js         TSTS changes
05/30/13    rm         Disable UIM2 by default on 8974
05/08/13    akv        Disabling UICC shutdown upon card removal by default on 8x26
04/10/13    nmb        8x10 bring up changes
03/18/13    js         8x26 UIM3 interface bring up fixes.
02/25/13    ak         Validate UIM1 GPIO values for 9x25
02/22/13    js         8x26 bring up changes
02/21/13    ak         9x25 GPIO defaults applied for both versions
02/21/13    ak         UIM1 support on 9x25 v2
02/12/13    js         General F3 macro cleanup
02/11/13    akv        UIM API for querying the available/enabled UIM slots
02/04/13    akv        Fix for using chip family instead of chip id for enumeration
02/01/13    akv        Run time config of UICC shutdown feature and code cleanup
12/05/12    js         UIM Parallel Processing changes
11/09/12    akv        BATT ALARM configuration for Dime
11/02/12    akv        Changes in HW Enumeration for 8x26 RUMI
10/26/12    akv/ssr    HW Enumeration changes
============================================================================*/
#include "uimdrv_enumeration.h"
#include "DDIChipInfo.h"
#include "DDIPlatformInfo.h"
#include "DALDeviceId.h"
#include "fs_public.h"

#include "uimdrv_gpio.h"
#include "uimdrv_clk.h"
#include "uimdrv_pmic.h"
#include "uimdrv_intctrl.h"
#include "uimdrv_uartdm.h"
#include "uimglobals.h"
#include "uim_common_efs.h"

#include "rex.h"

#define GPIO_NUM_DEFAULT          UIM_INVALID_GPIO_NUM
#define GPIO_FUNC_SEL_DEFAULT     GPIO_FUNC_SEL_1
#define GPIO_DRV_STRENGTH_DEFAULT DS_2MA
#define GPIO_PULL_SETTING_DEFAULT NO_PULL
#define GPIO_PULL_SETTING_PRESENT PULL_UP

/* Local forward declarations */
void configure_uim_enablement(DalChipInfoFamilyType  chipFamily,
                              DalChipInfoVersionType nChipVersion,
                              DalPlatformInfoPlatformType nPlatformType);
uint8 uim_get_hw_max_slots_available(DalChipInfoFamilyType  chipFamily,
                                     DalPlatformInfoPlatformType nPlatformType);
void uim_set_hotswap_flaky_switch_support(void);
void uim_hw_config_input_validation(uimHWConfig* m_HWConfig_ptr);

/* uimdrv_hw_nv_config_info structure holds all hw interface info got
   by reading NV UIM_HW_CONFIG_NV_EF */
uimdrv_hw_nv_config_info_type         uimdrv_hw_nv_config_info  = {0};
uim_hw_slot_info_type                 uim_hw_slot_info;

#define UIM_ONE_PHYSICAL_SLOT_AVAILABLE    0x01
#define UIM_TWO_PHYSICAL_SLOTS_AVAILABLE   0x02
#define UIM_THREE_PHYSICAL_SLOTS_AVAILABLE 0x03
#define UIM_FOUR_PHYSICAL_SLOTS_AVAILABLE  0x04

#define INVALID_IRQ_NUM  0xFFFF


static const uint16 uim_default_uartdm_num[UIM_INSTANCES_CONFIGURABLE] =
{
  BADGER_MSS_UIM0_UART_DM,
  BADGER_MSS_UIM1_UART_DM,
  INVALID_BADGER_MSS_UART_DM,
  INVALID_BADGER_MSS_UART_DM
};

static const uint16 uim_default_pmic_npa_resource[UIM_INSTANCES_CONFIGURABLE] =
{
  E_PMIC_NPA_RESOURCE_UIM1,
  E_PMIC_NPA_RESOURCE_UIM2,
  INVALID_PMIC_NPA_RESOURCE,
  INVALID_PMIC_NPA_RESOURCE
};

const uim_hw_param_type uim_hw_param[] =
{
  { DALCHIPINFO_FAMILY_MDM9x45 /* TESLA_9x45 */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, TRUE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    FALSE, FALSE,
    /* HOTSWAP IRQs for 4 slots  */
    { 147, 148, 149, 150 },
    /* UART IRQs for 4 slots     */
    { 151, 152, 153, 154 },
    /* There is an external Pull Up */
    { TRUE, TRUE, FALSE, FALSE}
  },  /* TESLA_9x45 */

  { DALCHIPINFO_FAMILY_MSM8952  /* TABASCO_SAHI */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, FALSE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    FALSE, FALSE,
    /* HOTSWAP IRQs for 4 slots  */
    {  89, 90, 254, INVALID_IRQ_NUM },
    /* UART IRQs for 4 slots     */
    {  87, 88, 253, INVALID_IRQ_NUM },
    /* There is an external Pull Up */
    { TRUE, TRUE, FALSE, FALSE}
  },  /* TABASCO_SAHI */

  { DALCHIPINFO_FAMILY_MSM8956  /* TABASCO_ELDARION */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, FALSE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    FALSE, FALSE,
    /* HOTSWAP IRQs for 4 slots  */
    {  89, 90, 254, INVALID_IRQ_NUM },
    /* UART IRQs for 4 slots     */
    {  87, 88, 253, INVALID_IRQ_NUM },
    /* There is an external Pull Up */
    { TRUE, TRUE, FALSE, FALSE}
  },  /* TABASCO_ELDARION */

  { DALCHIPINFO_FAMILY_MSM8909 /* JOLOKIA_8909 */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, TRUE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    FALSE, FALSE,
    /* HOTSWAP IRQs for 4 slots  */
    { 89, 90, 254, INVALID_IRQ_NUM },
    /* UART IRQs for 4 slots     */
    { 87, 88, 253, INVALID_IRQ_NUM },
    /* There is an external Pull Up */
    { TRUE, TRUE, TRUE, FALSE}
  },  /* TESLA_9x45 */

  { DALCHIPINFO_FAMILY_MDM9x55 /* McLaren_9x55 */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, FALSE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    TRUE, TRUE,
    /* HOTSWAP IRQs for 4 slots  */
    { 305, 306, 307, 308 },
    /* UART IRQs for 4 slots     */
    { 309, 310, 311, 312 },
    /* There is an external Pull Up */
    { TRUE, TRUE, FALSE, FALSE }
  },  /* McLaren_9x55 */

  { DALCHIPINFO_FAMILY_MSM8996 /* ISTARI_8996 */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, FALSE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    FALSE, FALSE,
    /* HOTSWAP IRQs for 4 slots  */
    { 147, 148, 149, 150 },
    /* UART IRQs for 4 slots     */
    { 151, 152, 153, 154 },
    /* There is an external Pull Up */
    { TRUE, TRUE, TRUE, TRUE}
  },  /* ISTARI_8996 */

  { DALCHIPINFO_FAMILY_MSM8998 /* Nazgul_8998 */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, FALSE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    TRUE, TRUE,
    /* HOTSWAP IRQs for 4 slots  */
    { 305, 306, 307, 308 },
    /* UART IRQs for 4 slots     */
    { 309, 310, 311, 312 },
    /* There is no external Pull Up */
    { FALSE, FALSE, FALSE, FALSE}
  },  /* Nazgul_8998 */

  { DALCHIPINFO_FAMILY_MSM8953  /* TABASCO_JACALA */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, FALSE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    FALSE, FALSE,
    /* HOTSWAP IRQs for 4 slots  */
    {  89, 90, INVALID_IRQ_NUM, INVALID_IRQ_NUM },
    /* UART IRQs for 4 slots     */
    {  87, 88, INVALID_IRQ_NUM, INVALID_IRQ_NUM },
    /* There is an external pull up */
    {  TRUE, TRUE, FALSE, FALSE }
  },  /* TABASCO_JACALA */

  { DALCHIPINFO_FAMILY_MSM8940  /* Feero_Cat6_8940 */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, FALSE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    FALSE, FALSE,
    /* HOTSWAP IRQs for 4 slots  */
    {  89, 90, INVALID_IRQ_NUM, INVALID_IRQ_NUM },
    /* UART IRQs for 4 slots     */
    {  87, 88, INVALID_IRQ_NUM, INVALID_IRQ_NUM },
    /* There is an external pull up */
    {  FALSE, FALSE, FALSE, FALSE }
  },  /* Feero_Cat6_8940 */

  { DALCHIPINFO_FAMILY_MSM8920  /* FeeroLite_Cat6_8920 */ ,
    /* num_of_physical_slots  uim_disable flag for 4 slots */
    UIM_TWO_PHYSICAL_SLOTS_AVAILABLE, FALSE, FALSE, TRUE, TRUE,
    /* SimClk_slot_diff_freq_support, UartClk_300MHz_source_support */
    FALSE, FALSE,
    /* HOTSWAP IRQs for 4 slots  */
    {  89, 90, INVALID_IRQ_NUM, INVALID_IRQ_NUM },
    /* UART IRQs for 4 slots     */
    {  87, 88, INVALID_IRQ_NUM, INVALID_IRQ_NUM },
    /* There is an external pull up */
    {  FALSE, FALSE, FALSE, FALSE }
  },  /* FeeroLite_Cat6_8920 */

  { DALCHIPINFO_FAMILY_UNKNOWN /* DALCHIPINFO_FAMILY_UNKNOWN */ ,
    0x00, TRUE, TRUE, TRUE, TRUE,
    FALSE, FALSE,
    { INVALID_IRQ_NUM, INVALID_IRQ_NUM, INVALID_IRQ_NUM, INVALID_IRQ_NUM },
    { INVALID_IRQ_NUM, INVALID_IRQ_NUM, INVALID_IRQ_NUM, INVALID_IRQ_NUM },
    /* No External Pull Up */
    { FALSE, FALSE, FALSE, FALSE}
  }  /* DALCHIPINFO_FAMILY_UNKNOWN */
};


/* This function copies m_HWConfig values to uimHWConfigEfs struct to avoid
   writing enum to EFS */
static void uimHWConfig_copy_to_efs_type(uimHWConfigEfs* m_HWConfig_efs)
{
  uint8 nUimInstance = 0;

  m_HWConfig_efs->version                     = (uint8)m_HWConfig.version;
  m_HWConfig_efs->uimBattAlarmGpioNum         = m_HWConfig.uimBattAlarmGpioNum;
  m_HWConfig_efs->uimBattAlarmGpioFuncSel     = m_HWConfig.uimBattAlarmGpioFuncSel;
  m_HWConfig_efs->uimBattAlarmGpioDrvStrength = (uint8)m_HWConfig.uimBattAlarmGpioDrvStrength;
  m_HWConfig_efs->uimBattAlarmGpioPullSetting = (uint8)m_HWConfig.uimBattAlarmGpioPullSetting;
  m_HWConfig_efs->uicc_shutdown_feature.uiccShutdownBattRemoval =
                                      m_HWConfig.uicc_shutdown_feature.uiccShutdownBattRemoval;
  uim_memscpy(m_HWConfig_efs->uicc_shutdown_feature.uiccShutdownCardRemoval,
              sizeof(m_HWConfig_efs->uicc_shutdown_feature.uiccShutdownCardRemoval),
              m_HWConfig.uicc_shutdown_feature.uiccShutdownCardRemoval,
              sizeof(m_HWConfig.uicc_shutdown_feature.uiccShutdownCardRemoval));
  uim_memscpy(m_HWConfig_efs->usb_uicc_obsolete,
              sizeof(m_HWConfig_efs->usb_uicc_obsolete),
              m_HWConfig.usb_uicc_obsolete,
              sizeof(m_HWConfig.usb_uicc_obsolete));
  uim_memscpy(m_HWConfig_efs->uimSwpCapabilitySupported,
              sizeof(m_HWConfig_efs->uimSwpCapabilitySupported),
              m_HWConfig.uimSwpCapabilitySupported,
              sizeof(m_HWConfig.uimSwpCapabilitySupported));
  uim_memscpy(m_HWConfig_efs->uim_hotswap_flaky_switch,
              sizeof(m_HWConfig_efs->uim_hotswap_flaky_switch),
              m_HWConfig.uim_hotswap_flaky_switch,
              sizeof(m_HWConfig.uim_hotswap_flaky_switch));
  for (nUimInstance = 0; nUimInstance < UIM_INSTANCES_CONFIGURABLE; nUimInstance++)
  {
    m_HWConfig_efs->uim_slot_config[nUimInstance].disableUim =
                              m_HWConfig.uim_slot_config[nUimInstance].disableUim;
    m_HWConfig_efs->uim_slot_config[nUimInstance].enableUimHotswap =
                              m_HWConfig.uim_slot_config[nUimInstance].enableUimHotswap;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimHotswapPolarity = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimHotswapPolarity;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimUart = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimUart;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimVcc = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimVcc;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimControllerIRQNum =
                              m_HWConfig.uim_slot_config[nUimInstance].uimControllerIRQNum;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimUartIRQNum =
                              m_HWConfig.uim_slot_config[nUimInstance].uimUartIRQNum;

    m_HWConfig_efs->uim_slot_config[nUimInstance].uimResetGpioNum =
                              m_HWConfig.uim_slot_config[nUimInstance].uimResetGpioNum;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimResetGpioFuncSel =
                              m_HWConfig.uim_slot_config[nUimInstance].uimResetGpioFuncSel;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimResetGpioDrvStrength = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimResetGpioDrvStrength;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimResetGpioPullSetting = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimResetGpioPullSetting;

    m_HWConfig_efs->uim_slot_config[nUimInstance].uimDataGpioNum =
                              m_HWConfig.uim_slot_config[nUimInstance].uimDataGpioNum;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimDataGpioFuncSel =
                              m_HWConfig.uim_slot_config[nUimInstance].uimDataGpioFuncSel;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimDataGpioDrvStrength = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimDataGpioDrvStrength;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimDataGpioPullSetting = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimDataGpioPullSetting;

    m_HWConfig_efs->uim_slot_config[nUimInstance].uimClkGpioNum =
                              m_HWConfig.uim_slot_config[nUimInstance].uimClkGpioNum;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimClkGpioFuncSel =
                              m_HWConfig.uim_slot_config[nUimInstance].uimClkGpioFuncSel;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimClkGpioDrvStrength = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimClkGpioDrvStrength;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimClkGpioPullSetting = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimClkGpioPullSetting;

    m_HWConfig_efs->uim_slot_config[nUimInstance].uimCardDetectGpioNum =
                              m_HWConfig.uim_slot_config[nUimInstance].uimCardDetectGpioNum;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimCardDetectGpioFuncSel =
                              m_HWConfig.uim_slot_config[nUimInstance].uimCardDetectGpioFuncSel;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimCardDetectGpioDrvStrength = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimCardDetectGpioDrvStrength;
    m_HWConfig_efs->uim_slot_config[nUimInstance].uimCardDetectGpioPullSetting = (uint8)
                              m_HWConfig.uim_slot_config[nUimInstance].uimCardDetectGpioPullSetting;

    m_HWConfig_efs->uimSwpCapabilitySupported[nUimInstance] =
                              m_HWConfig.uimSwpCapabilitySupported[nUimInstance];
  }
} /* uimHWConfig_copy_to_efs_type */


/* This function copies values in uimHWConfigEfs struct to m_HWConfig by
   converting to appropriate enum type */
static void uimHWConfig_copy_from_efs_type(uimHWConfigEfs* m_HWConfig_efs)
{
  uint8 i = 0;

  m_HWConfig.version                     = (uimVersionEnum)m_HWConfig_efs->version;
  m_HWConfig.uimBattAlarmGpioNum         = m_HWConfig_efs->uimBattAlarmGpioNum;
  m_HWConfig.uimBattAlarmGpioFuncSel     = m_HWConfig_efs->uimBattAlarmGpioFuncSel;
  m_HWConfig.uimBattAlarmGpioDrvStrength = (uimDriveStrengthEnum)
                                    m_HWConfig_efs->uimBattAlarmGpioDrvStrength;
  m_HWConfig.uimBattAlarmGpioPullSetting = (uimPullSettingEnum)
                                    m_HWConfig_efs->uimBattAlarmGpioPullSetting;
  m_HWConfig.uicc_shutdown_feature.uiccShutdownBattRemoval =
                                    m_HWConfig_efs->uicc_shutdown_feature.uiccShutdownBattRemoval;
  uim_memscpy(m_HWConfig.uicc_shutdown_feature.uiccShutdownCardRemoval,
              sizeof(m_HWConfig.uicc_shutdown_feature.uiccShutdownCardRemoval),
              m_HWConfig_efs->uicc_shutdown_feature.uiccShutdownCardRemoval,
              sizeof(m_HWConfig_efs->uicc_shutdown_feature.uiccShutdownCardRemoval));
  uim_memscpy(m_HWConfig.usb_uicc_obsolete,
              sizeof(m_HWConfig.usb_uicc_obsolete),
              m_HWConfig_efs->usb_uicc_obsolete,
              sizeof(m_HWConfig_efs->usb_uicc_obsolete));
  uim_memscpy(m_HWConfig.uimSwpCapabilitySupported,
              sizeof(m_HWConfig.uimSwpCapabilitySupported),
              m_HWConfig_efs->uimSwpCapabilitySupported,
              sizeof(m_HWConfig_efs->uimSwpCapabilitySupported));
  uim_memscpy(m_HWConfig.uim_hotswap_flaky_switch,
              sizeof(m_HWConfig.uim_hotswap_flaky_switch),
              m_HWConfig_efs->uim_hotswap_flaky_switch,
              sizeof(m_HWConfig_efs->uim_hotswap_flaky_switch));
  for(i = 0; i < UIM_INSTANCES_CONFIGURABLE; i++)
  {
    m_HWConfig.uim_slot_config[i].disableUim =
                                      m_HWConfig_efs->uim_slot_config[i].disableUim;
    m_HWConfig.uim_slot_config[i].enableUimHotswap =
                                      m_HWConfig_efs->uim_slot_config[i].enableUimHotswap;
    m_HWConfig.uim_slot_config[i].uimHotswapPolarity = (uimPolarityeEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimHotswapPolarity;
    m_HWConfig.uim_slot_config[i].uimControllerIRQNum =
                                      m_HWConfig_efs->uim_slot_config[i].uimControllerIRQNum;
    m_HWConfig.uim_slot_config[i].uimUart = (uimUartEnumList)
                                      m_HWConfig_efs->uim_slot_config[i].uimUart;
    m_HWConfig.uim_slot_config[i].uimUartIRQNum =
                                      m_HWConfig_efs->uim_slot_config[i].uimUartIRQNum;
    m_HWConfig.uim_slot_config[i].uimVcc = (uimVccEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimVcc;
    m_HWConfig.uim_slot_config[i].uimResetGpioNum =
                                      m_HWConfig_efs->uim_slot_config[i].uimResetGpioNum;
    m_HWConfig.uim_slot_config[i].uimResetGpioFuncSel =
                                      m_HWConfig_efs->uim_slot_config[i].uimResetGpioFuncSel;
    m_HWConfig.uim_slot_config[i].uimResetGpioDrvStrength = (uimDriveStrengthEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimResetGpioDrvStrength;
    m_HWConfig.uim_slot_config[i].uimResetGpioPullSetting = (uimPullSettingEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimResetGpioPullSetting;

    m_HWConfig.uim_slot_config[i].uimDataGpioNum =
                                      m_HWConfig_efs->uim_slot_config[i].uimDataGpioNum;
    m_HWConfig.uim_slot_config[i].uimDataGpioFuncSel =
                                      m_HWConfig_efs->uim_slot_config[i].uimDataGpioFuncSel;
    m_HWConfig.uim_slot_config[i].uimDataGpioDrvStrength = (uimDriveStrengthEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimDataGpioDrvStrength;
    m_HWConfig.uim_slot_config[i].uimDataGpioPullSetting = (uimPullSettingEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimDataGpioPullSetting;

    m_HWConfig.uim_slot_config[i].uimClkGpioNum =
                                      m_HWConfig_efs->uim_slot_config[i].uimClkGpioNum;
    m_HWConfig.uim_slot_config[i].uimClkGpioFuncSel =
                                      m_HWConfig_efs->uim_slot_config[i].uimClkGpioFuncSel;
    m_HWConfig.uim_slot_config[i].uimClkGpioDrvStrength = (uimDriveStrengthEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimClkGpioDrvStrength;
    m_HWConfig.uim_slot_config[i].uimClkGpioPullSetting = (uimPullSettingEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimClkGpioPullSetting;

    m_HWConfig.uim_slot_config[i].uimCardDetectGpioNum =
                                      m_HWConfig_efs->uim_slot_config[i].uimCardDetectGpioNum;
    m_HWConfig.uim_slot_config[i].uimCardDetectGpioFuncSel =
                                      m_HWConfig_efs->uim_slot_config[i].uimCardDetectGpioFuncSel;
    m_HWConfig.uim_slot_config[i].uimCardDetectGpioDrvStrength = (uimDriveStrengthEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimCardDetectGpioDrvStrength;
    m_HWConfig.uim_slot_config[i].uimCardDetectGpioPullSetting = (uimPullSettingEnum)
                                      m_HWConfig_efs->uim_slot_config[i].uimCardDetectGpioPullSetting;
  }
} /* uimHWConfig_copy_from_efs_type */

static void uim_apply_defaults(void)
{
  DalDeviceHandle               *phChipInfo    = NULL;
  DalChipInfoFamilyType         chipFamily     = DALCHIPINFO_FAMILY_UNKNOWN;
  DalChipInfoVersionType        nChipVersion   = 0;
  DalPlatformInfoPlatformType   nPlatformType  = DALPLATFORMINFO_TYPE_UNKNOWN;
  uint8                         nChipindex     = 0;
  uint8                         nUimInstance   = 0;
  uimHWConfigEfs                m_HWConfig_efs;

  UIM_MSG_HIGH_0("HW configuration applying defaults ");

  uimdrv_hw_nv_config_info.default_nv_applied  = TRUE;

  if (DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &phChipInfo) != DAL_SUCCESS)
  {
    return;
  }

  DalChipInfo_GetChipFamily(phChipInfo, &chipFamily);
  nChipVersion = DalChipInfo_ChipVersion();
  nPlatformType = DalPlatformInfo_Platform();

  UIM_MSG_HIGH_2("Chip Family 0x%x, Chip Version 0x%x", chipFamily, nChipVersion);

  /*
     All UIM instances are initially disabled.
     They are selectively enabled based on the chipset.
  */
  m_HWConfig.uim_slot_config[0].disableUim = TRUE;
  m_HWConfig.uim_slot_config[1].disableUim = TRUE;
  m_HWConfig.uim_slot_config[2].disableUim = TRUE;
  m_HWConfig.uim_slot_config[3].disableUim = TRUE;
  /* Disable/Enable UIM1-UIM4 based on the chipset */
  configure_uim_enablement(chipFamily, nChipVersion, nPlatformType);

  m_HWConfig.version = VER_4;

  m_HWConfig.uimBattAlarmGpioNum            = GPIO_NUM_DEFAULT;
  m_HWConfig.uimBattAlarmGpioFuncSel        = GPIO_FUNC_SEL_DEFAULT;
  m_HWConfig.uimBattAlarmGpioDrvStrength    = GPIO_DRV_STRENGTH_DEFAULT;
  m_HWConfig.uimBattAlarmGpioPullSetting    = GPIO_PULL_SETTING_DEFAULT;

  m_HWConfig.uicc_shutdown_feature.uiccShutdownBattRemoval  = FALSE;

  nChipindex = uim_get_chip_index_from_table(chipFamily);
  for (nUimInstance = 0; nUimInstance < UIM_INSTANCES_CONFIGURABLE; nUimInstance++)
  {
#ifdef FEATURE_ENABLE_HOTSWAP_DEFAULT
    m_HWConfig.uim_slot_config[nUimInstance].enableUimHotswap    = TRUE;
#else
    m_HWConfig.uim_slot_config[nUimInstance].enableUimHotswap    = FALSE;
#endif /* FEATURE_ENABLE_HOTSWAP_DEFAULT */
    m_HWConfig.uim_slot_config[nUimInstance].uimHotswapPolarity  = ACTIVE_HIGH;

    m_HWConfig.uim_slot_config[nUimInstance].uimUart  = uim_default_uartdm_num[nUimInstance];
    m_HWConfig.uim_slot_config[nUimInstance].uimVcc   = uim_default_pmic_npa_resource[nUimInstance];
    m_HWConfig.uim_slot_config[nUimInstance].uimControllerIRQNum     = uim_hw_param[nChipindex].controller_hs_irq_num[nUimInstance];
    m_HWConfig.uim_slot_config[nUimInstance].uimUartIRQNum           = uim_hw_param[nChipindex].uartdm_irq_num[nUimInstance];

    m_HWConfig.uim_slot_config[nUimInstance].uimResetGpioNum         = GPIO_NUM_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimResetGpioFuncSel     = GPIO_FUNC_SEL_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimResetGpioDrvStrength = GPIO_DRV_STRENGTH_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimResetGpioPullSetting = GPIO_PULL_SETTING_DEFAULT;

    m_HWConfig.uim_slot_config[nUimInstance].uimDataGpioNum          = GPIO_NUM_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimDataGpioFuncSel      = GPIO_FUNC_SEL_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimDataGpioDrvStrength  = GPIO_DRV_STRENGTH_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimDataGpioPullSetting  = GPIO_PULL_SETTING_DEFAULT;

    m_HWConfig.uim_slot_config[nUimInstance].uimClkGpioNum           = GPIO_NUM_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimClkGpioFuncSel       = GPIO_FUNC_SEL_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimClkGpioDrvStrength   = GPIO_DRV_STRENGTH_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimClkGpioPullSetting   = GPIO_PULL_SETTING_DEFAULT;

    m_HWConfig.uim_slot_config[nUimInstance].uimCardDetectGpioNum           = GPIO_NUM_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimCardDetectGpioFuncSel       = GPIO_FUNC_SEL_DEFAULT;
    m_HWConfig.uim_slot_config[nUimInstance].uimCardDetectGpioDrvStrength   = GPIO_DRV_STRENGTH_DEFAULT;
    if (FALSE == uim_hw_param[uimdrv_hw_nv_config_info.dalChipFamilyIndex].bIsExternalPullUPInstalled[nUimInstance])
    {
      /* External Pull Up is not installed on UIMx_PRESENT line */
      m_HWConfig.uim_slot_config[nUimInstance].uimCardDetectGpioPullSetting   = GPIO_PULL_SETTING_PRESENT;
    }
    else
    {
      /* External Pull Up is installed on UIMx_PRESENT line */
      m_HWConfig.uim_slot_config[nUimInstance].uimCardDetectGpioPullSetting   = GPIO_PULL_SETTING_DEFAULT;	  
    }

   /*
     UICC shutdown on card removal should be enabled by default,
     but disabling them on 8x26 due to call drop issues, seen because
     PMIC incorrectly shuts down the UIM LDO.
    */
    if (DALCHIPINFO_FAMILY_MSM8x26 == chipFamily)
    {
       m_HWConfig.uicc_shutdown_feature.uiccShutdownCardRemoval[nUimInstance]  = FALSE;
    }
    else
    {
       m_HWConfig.uicc_shutdown_feature.uiccShutdownCardRemoval[nUimInstance]  = TRUE;
    }
    /* making SWP support in Terminal Capability as NOT SUPPORTED */
    m_HWConfig.uimSwpCapabilitySupported[nUimInstance] = FALSE;
  }

  /* Most of target have the SWP support on slot1 */
  m_HWConfig.uimSwpCapabilitySupported[0] = TRUE;

  /* Copy HWConfig to EFS structure */
  memset(&m_HWConfig_efs, 0x00, sizeof(uimHWConfigEfs));
  uimHWConfig_copy_to_efs_type(&m_HWConfig_efs);

  if (UIM_COMMON_EFS_SUCCESS != uim_common_efs_write(UIM_COMMON_EFS_UIMDRV_HW_CONFIG,
                                                     UIM_COMMON_EFS_ITEM_FILE_TYPE,
                                                     UIM_COMMON_EFS_DEVICE,
                                                    (char *)&(m_HWConfig_efs),
                                                     sizeof(m_HWConfig_efs)))
  {
    UIM_MSG_ERR_0("Writing default values to EFS failed");
  }
}/* uim_apply_defaults */


/* This function sets the flaky hotswap feature globals after reading the NV70210 */
void uim_set_hotswap_flaky_switch_support(void)
{
  uint8	nUimInstance   = 0;
  uim_instance_global_type *uim_ptr	   = NULL;

  for (nUimInstance = 0; nUimInstance < UIM_INSTANCES_CONFIGURABLE; nUimInstance++)
  {
    uim_ptr = uim_get_instance_ptr((uim_instance_enum_type)(nUimInstance));

    if(m_HWConfig.uim_slot_config[nUimInstance].disableUim == TRUE)
    {
      continue;
    }
    if(uim_ptr != NULL)
    {
      if( m_HWConfig.uim_hotswap_flaky_switch[nUimInstance] == TRUE )
      {
        uim_ptr->hotswap.hotswap_flaky_switch_support = TRUE;
      }
      else
      {
        uim_ptr->hotswap.hotswap_flaky_switch_support = FALSE;
      }
    }
  }
}/* uim_set_hotswap_flaky_switch_support */


/*
  1. uimdrv_hw_nv_configuration() is invoked to read nv
     UIM_HW_CONFIG_NV_EF that contains information used to configure
     UIMDRV HW interfaces.
  2. It is instance independant as all instances run from information
     got from nv UIM_HW_CONFIG_NV_EF
  3. The NV is read only once by either of the following depending on who
     calls uimdrv_hw_nv_configuration() first,
     a. Either the first uim thread to reach dev init and in turn invoke
        uimdrv_hw_nv_configuration().
     b. Else by any external client who calls the UIM exposed api to get
        the UIM slot information. The exposed api is uim_get_hw_slot_info()
        which in turn invokes uimdrv_hw_nv_configuration().
*/
uim_hw_slot_info_type uimdrv_hw_nv_configuration(void)
{
  uint32                                i              = 0;
  uint32                                j              = 0;
  /* This is used to hold the list of enabled UIM i.e
     uim_enabled_slot_config[0 .. (supportedSlot-1)].disableUim = FALSE
   */
  uimSlotConfig                         uim_enabled_slot_config[UIM_INSTANCES_CONFIGURABLE];
  DalDeviceHandle                       *phChipInfo    = NULL;
  DalChipInfoFamilyType                 chipFamily     = DALCHIPINFO_FAMILY_UNKNOWN;
  DalPlatformInfoPlatformType           nPlatformType  = DALPLATFORMINFO_TYPE_UNKNOWN;
  uimHWConfigEfs                        m_HWConfig_efs;

  /* Enter critical section */
  rex_enter_crit_sect(&uimdrv_hw_nv_config_info.uimdrv_hw_nv_crit_sect);

  /* EFS not read, read EFS and return the number_of_active_interfaces */
  if (FALSE == uimdrv_hw_nv_config_info.efs_file_read_completed)
  {
    uimdrv_hw_nv_config_info.number_of_active_interfaces = 0;
    memset(&uim_hw_slot_info,0,sizeof(uim_hw_slot_info_type));

    if (DAL_DeviceAttach(DALDEVICEID_CHIPINFO, &phChipInfo) != DAL_SUCCESS)
    {
      UIM_MSG_ERR_0("DAL_DeviceAttach failed in uimdrv_hw_nv_configuration");
      /* Leave critical section and return */
      rex_leave_crit_sect(&uimdrv_hw_nv_config_info.uimdrv_hw_nv_crit_sect);
      return uim_hw_slot_info;
    }

    DalChipInfo_GetChipFamily(phChipInfo, &chipFamily);

    nPlatformType = DalPlatformInfo_Platform();

    uimdrv_hw_nv_config_info.dalChipFamily = chipFamily;

    uimdrv_hw_nv_config_info.hw_max_num_slots_supported =
                                uim_get_hw_max_slots_available(chipFamily,nPlatformType);
    uimdrv_hw_nv_config_info.dalChipFamilyIndex         =
                          uim_get_chip_index_from_table(chipFamily);

    memset(&m_HWConfig_efs, 0x00, sizeof(m_HWConfig_efs));
    memset(&m_HWConfig,0, sizeof(m_HWConfig));
    memset(&uim_enabled_slot_config, 0, sizeof(uim_enabled_slot_config));

    if (UIM_COMMON_EFS_SUCCESS == uim_common_efs_read( UIM_COMMON_EFS_UIMDRV_HW_CONFIG,
                                                       UIM_COMMON_EFS_ITEM_FILE_TYPE,
                                                       UIM_COMMON_EFS_DEVICE,
                                                      (uint8 *)&(m_HWConfig_efs),
                                                       sizeof(m_HWConfig_efs)))
    {
      /* copy HWConfig from EFS structure */
      uimHWConfig_copy_from_efs_type(&m_HWConfig_efs);

      uim_hw_config_input_validation(&m_HWConfig);

      if (DALPLATFORMINFO_TYPE_LIQUID == nPlatformType ||
          DALPLATFORMINFO_TYPE_FLUID == nPlatformType)
      {
        m_HWConfig.uim_slot_config[1].disableUim = TRUE;
      }
    }
    else
    {
      uim_apply_defaults();
    }

    uimdrv_hw_nv_config_info.efs_file_read_completed = TRUE;

    for (i=0; (i< uimdrv_hw_nv_config_info.hw_max_num_slots_supported) && (i<UIM_INSTANCES_CONFIGURABLE); i++)
    {
      uim_hw_slot_info.slot_status[i].slot_available = TRUE;
      if ( m_HWConfig.uim_slot_config[i].disableUim == FALSE )
      {
        uimdrv_hw_nv_config_info.number_of_active_interfaces++;
        uim_hw_slot_info.slot_status[i].slot_enabled = TRUE;
      }
    }

    if ( uimdrv_hw_nv_config_info.number_of_active_interfaces == 0)
    {
      UIM_MSG_ERR_0("Number of active interfaces is Zero, UIM cannot initialize HW");
      /* Leave critical section and return */
      rex_leave_crit_sect(&uimdrv_hw_nv_config_info.uimdrv_hw_nv_crit_sect);
      return uim_hw_slot_info;
    }

    /* scanning through m_HWConfig and picking up the uim_slot_config object corresponding to
       enabled slot, copying it into newly introduced uim_enabled_slot_config object.
       This (uim_enabled_slot_config) newly introduced object will hold the slot configuration
       data of enabled UIM only (UIM_DISABLE = FALSE)
    */
    for (i=0; (i< uimdrv_hw_nv_config_info.hw_max_num_slots_supported) && (i<UIM_INSTANCES_CONFIGURABLE); i++)
    {
      if (uim_hw_slot_info.slot_status[i].slot_enabled == TRUE)
      {
        uim_memscpy(&uim_enabled_slot_config[j],
                    sizeof(uimSlotConfig),
                    &m_HWConfig.uim_slot_config[i],
                    sizeof(m_HWConfig.uim_slot_config[i]));
        m_HWConfig.uimSwpCapabilitySupported[j] = m_HWConfig.uimSwpCapabilitySupported[i];
        m_HWConfig.uicc_shutdown_feature.uiccShutdownCardRemoval[j] = m_HWConfig.uicc_shutdown_feature.uiccShutdownCardRemoval[i];
        m_HWConfig.uim_hotswap_flaky_switch[j] = m_HWConfig.uim_hotswap_flaky_switch[i];
        uim_hotswap_nature[j] = uim_hotswap_nature[i];
        j++;
      }
    }
    /* while doing memset with ZERO,which indirectly made
       uim_enabled_slot_config[j].disableUim = ZERO (FALSE), causing this UIM to be enabled.
       therefore It is purposely made to DISABLE remaining UIM
       before overwriting m_HWConfig with uim_enabled_slot_config
     */
    for (;j < UIM_INSTANCES_CONFIGURABLE;j++)
    {
      uim_enabled_slot_config[j].disableUim = TRUE;
    }

    uim_memscpy(&m_HWConfig.uim_slot_config,
                sizeof(m_HWConfig.uim_slot_config),
                &uim_enabled_slot_config,
                sizeof(uim_enabled_slot_config));

    uim_set_hotswap_flaky_switch_support();
    /* Leave critical section and return */
    rex_leave_crit_sect(&uimdrv_hw_nv_config_info.uimdrv_hw_nv_crit_sect);
    return uim_hw_slot_info;
  }
  else /* EFS already read, just return the uim_hw_slot_info */
  {
    /* Leave critical section and return */
    rex_leave_crit_sect(&uimdrv_hw_nv_config_info.uimdrv_hw_nv_crit_sect);
    return uim_hw_slot_info;
  }
} /* uimdrv_hw_nv_configuration */


/*
   uim_get_hw_slot_info() is the API is exposed for other uim modules
   to get information on the available/enabled UIM interfaces.
*/
uim_hw_slot_info_type uim_get_hw_slot_info(void)
{
  return uimdrv_hw_nv_configuration();
}/* uim_get_hw_slot_info */


/* Disables/Enables UIM1-UIM4 based on chipset defaults  */
void configure_uim_enablement(DalChipInfoFamilyType  chipFamily,
                              DalChipInfoVersionType nChipVersion,
                              DalPlatformInfoPlatformType nPlatformType)
{
  uint32 i = 0;

  /* Find the corresponding entry in the chipset_info_table */
  /* i will be the index of the correct entry after this loop */
  for (i=0; uim_hw_param[i].chipFamily != DALCHIPINFO_FAMILY_UNKNOWN; i++)
  {
    if (uim_hw_param[i].chipFamily == chipFamily)
    {
      break;
    }
  }

  if (DALCHIPINFO_FAMILY_UNKNOWN == uim_hw_param[i].chipFamily)
  {
    UIM_MSG_ERR_0("INVALID chipFamily");
    return;
  }

  /* Enable/Disable the UIM interfaces based on the chipset_info_table entries */
  m_HWConfig.uim_slot_config[0].disableUim = uim_hw_param[i].bDisableUIM1;
  m_HWConfig.uim_slot_config[1].disableUim = uim_hw_param[i].bDisableUIM2;
  m_HWConfig.uim_slot_config[2].disableUim = uim_hw_param[i].bDisableUIM3;
  m_HWConfig.uim_slot_config[3].disableUim = uim_hw_param[i].bDisableUIM4;

  /* SPECIAL CASES */
  if (DALCHIPINFO_FAMILY_MDM9x25 == chipFamily)
  {
    if(nChipVersion < DALCHIPINFO_VERSION(2,0))
    {
      /* UIM2 is enabled by default instead of UIM1 */
      m_HWConfig.uim_slot_config[0].disableUim = TRUE;
      m_HWConfig.uim_slot_config[1].disableUim = FALSE;
    }
  }

  /* For Liquid and Fluid platforms, disable UIM2 */
  if (DALPLATFORMINFO_TYPE_LIQUID == nPlatformType ||
      DALPLATFORMINFO_TYPE_FLUID == nPlatformType)
  {
    m_HWConfig.uim_slot_config[1].disableUim = TRUE;
  }
}/* configure_uim_enablement */


/* uim_get_hw_max_slots_available returns the max available uim slots on that chipset */
uint8 uim_get_hw_max_slots_available(DalChipInfoFamilyType       chipFamily,
                                     DalPlatformInfoPlatformType nPlatformType)
{
  uint8 i;
  /* If the chip id is 8626 or 8926 and the platform type is RUMI then disable all UIM slots */
  if ((DALCHIPINFO_FAMILY_MSM8x26  == chipFamily || DALCHIPINFO_FAMILY_MSM8926 == chipFamily)
      && DALPLATFORMINFO_TYPE_RUMI == nPlatformType)
  {
    return 0;
  }
  for (i=0; uim_hw_param[i].chipFamily != DALCHIPINFO_FAMILY_UNKNOWN; i++)
  {
    if (uim_hw_param[i].chipFamily == chipFamily)
    {
      return uim_hw_param[i].numUimSlotsAvailable;
    }
  }
  UIM_MSG_ERR_0("uim_get_hw_max_slots_available: Chipset unknown, cannot initialize HW");
  return 0;
}/* uim_get_hw_max_slots_available */


/* uim_get_chip_index_from_table returns the index of the information related to the chip family */
uint8 uim_get_chip_index_from_table (DalChipInfoFamilyType  chipFamily)
{
  uint8 i = 0;


  for (i=0; uim_hw_param[i].chipFamily != DALCHIPINFO_FAMILY_UNKNOWN; i++)
  {
    if (uim_hw_param[i].chipFamily == chipFamily)
    {
      return i;
    }
  }

  ERR_FATAL("uim_get_chip_index_from_table: Chipset unknown, cannot initialize HW",0,0,0);
  return 0;
}/* uim_get_chip_index_from_table */


/*
   uim_hw_config_input_validation checks if crucial items for enabled UIM
   instance is set to 0.
 */
void uim_hw_config_input_validation(uimHWConfig* m_HWConfig_ptr)
{
  uint32 i = 0;

  if (m_HWConfig_ptr != NULL)
  {
    /* For each enabled UIM instance, check crucial items if they are 0 */
    for (i = 0; i < UIM_INSTANCES_CONFIGURABLE; i++)
    {
      if (FALSE == m_HWConfig_ptr->uim_slot_config[i].disableUim &&
          (m_HWConfig_ptr->uim_slot_config[i].uimControllerIRQNum == 0 ||
           m_HWConfig_ptr->uim_slot_config[i].uimUartIRQNum == 0))
      {
        UIM_MSG_LOW_0("UIM_HW_CONFIG_NV_EF nearly zero, populating defaults");
        uim_apply_defaults();
        break;
      }
    }
  }
} /* uim_hw_config_input_validation */