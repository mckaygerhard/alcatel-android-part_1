/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


       PBM    M E M O R Y    C L E A N U P    H E A D E R


*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2015 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/30/14    lm     Initial Version
===========================================================================*/

/*
Note : The functions in the  file are used only when FEATURE_UIM_TEST_FRAMEWORK 
enabled. */
#ifdef FEATURE_UIM_TEST_FRAMEWORK
#error code not present
#endif /* FEATURE_UIM_TEST_FRAMEWORK */
