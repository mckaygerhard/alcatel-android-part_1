#ifndef LPA_TIMER_H
#define LPA_TIMER_H
/*===========================================================================


            L P A   T I M E R   H E A D E R


===========================================================================*/

/*===========================================================================
                        COPYRIGHT INFORMATION

Copyright (c) 2016 QUALCOMM Technologies, Inc (QTI) and its licensors.
All Rights Reserved.  QUALCOMM Technologies Proprietary.
Export of this technology or software
is regulated by the U.S. Government. Diversion contrary to U.S. law prohibited.

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/uim.mpss/5.1/lpa/src/lpa_timer.h#1 $ $DateTime: 2016/03/30 04:03:40 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/28/16   av      Initial revision
===========================================================================*/

/*=============================================================================

                       FUNCTION PROTOTYPES

=============================================================================*/

/*===========================================================================
FUNCTION LPA_TIMER_CLEANUP

DESCRIPTION
  Undefines the LPA sanity timer.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void lpa_timer_cleanup (
  lpa_slot_id_enum_type           slot_id
);

/*===========================================================================
FUNCTION LPA_TIMER_CLEAR

DESCRIPTION
  Function clears the LPA sanity timer.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void lpa_timer_clear (
  lpa_slot_id_enum_type          slot_id
);

/*===========================================================================
FUNCTION LPA_TIMER_SET

DESCRIPTION
  Function sets the LPA sanity timer.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  lpa_result_enum_type

SIDE EFFECTS
  None
===========================================================================*/
lpa_result_enum_type lpa_timer_set (
  lpa_slot_id_enum_type          slot_id
);

/*===========================================================================
FUNCTION LPA_TIMER_HANDLE_SANITY_TIMER_EXPIRE_SIG

DESCRIPTION
  This function, called in lpa_task, is called to process sanity timer
  expiration signal.

DEPENDENCIES
  lpa_task must have finished initialization.

LIMITATIONS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void lpa_timer_handle_sanity_timer_expire_sig (
  lpa_slot_id_enum_type               slot_id
);

/*===========================================================================
FUNCTION LPA_TIMER_INIT

DESCRIPTION
  Function initializes and defines the LPA sanity timer.

DEPENDENCIES
  None

LIMITATIONS
  None

RETURN VALUE
  None

SIDE EFFECTS
  None
===========================================================================*/
void lpa_timer_init (
  void
);

#endif /* LPA_TIMER_H */

