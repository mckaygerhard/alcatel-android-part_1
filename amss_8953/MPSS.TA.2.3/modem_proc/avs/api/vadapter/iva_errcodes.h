#ifndef __IVA_ERRCODES_H__
#define __IVA_ERRCODES_H__

/**
  @file iva_errcodes.h
  @brief
      This file defines the error codes for the IMS Voice Adapter

*/

/*
  ============================================================================

   Copyright (C) 2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //components/rel/avs.mpss/6.2.1/api/vadapter/iva_errcodes.h#1 $
  $Author: pwbldsvc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------

  ===========================================================================*/

#define IVA_EOK          ( 0x00000000 ) /**< Success; completed with no errors. */
#define IVA_EFAILED      ( 0x00000001 ) /**< General failure. */
#define IVA_EBADPARAM    ( 0x00000002 ) /**< Bad operation parameter(s). */
#define IVA_EUNSUPPORTED ( 0x00000003 ) /**< Unsupported routine/operation. */
#define IVA_EVERSION     ( 0x00000004 ) /**< Unsupported version. */
#define IVA_EUNEXPECTED  ( 0x00000005 ) /**< Unexpected problem encountered. */
#define IVA_EPANIC       ( 0x00000006 ) /**< Unhandled problem occurred. */

#define IVA_ENORESOURCE  ( 0x00000007 ) /**< Unable to allocate resource(s). */
#define IVA_EHANDLE      ( 0x00000008 ) /**< Invalid handle. */
#define IVA_EALREADY     ( 0x00000009 ) /**< Operation is already processed. */
#define IVA_ENOTREADY    ( 0x0000000A ) /**< Operation is not ready to be processed. */
#define IVA_EPENDING     ( 0x0000000B ) /**< Operation is pending completion. */
#define IVA_EBUSY        ( 0x0000000C ) /**< Operation could not be accepted or processed. */
#define IVA_EABORTED     ( 0x0000000D ) /**< Operation aborted due to an error. */
#define IVA_EPREEMPTED   ( 0x0000000E ) /**< Operation preempted by a higher priority. */
#define IVA_ECONTINUE    ( 0x0000000F ) /**< Operation requests intervention to complete. */
#define IVA_EIMMEDIATE   ( 0x00000010 ) /**< Operation requests immediate intervention to complete. */
#define IVA_ENOTIMPL     ( 0x00000011 ) /**< Operation is not implemented. */
#define IVA_ENEEDMORE    ( 0x00000012 ) /**< Operation needs more data or resources. */
#define IVA_ELPC         ( 0x00000013 ) /**< Operation is a local procedure call. */
#define IVA_ENOMEMORY    ( 0x00000014 ) /**< Unable to allocate enough memory. */
#define IVA_ENOTEXIST    ( 0x00000015 ) /**< Item does not exist. */

#endif  /* __IVA_ERRCODES_H__ */

