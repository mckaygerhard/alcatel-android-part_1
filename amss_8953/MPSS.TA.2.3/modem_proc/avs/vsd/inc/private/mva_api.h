#ifndef __MVA_API_H__
#define __MVA_API_H__

/*
   Copyright (C) 2014 QUALCOMM Technologies, Inc.
   All Rights Reserved.
   Qualcomm Technologies, Inc. Confidential and Proprietary.

   $Header: //components/rel/avs.mpss/6.2.1/vsd/inc/private/mva_api.h#1 $
   $Author: pwbldsvc $
*/

#include "apr_comdef.h"
#include "apr_timer.h"

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

/****************************************************************************
 * DEFINITIONS                                                              *
 ****************************************************************************/

APR_INTERNAL uint32_t mva_call
(
  uint32_t cmd_id,
  void* params,
  uint32_t size
);

#endif  /* __MVA_API_H__ */
