/*
   Copyright (C) 2015-2016 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

   $Header: //components/rel/avs.mpss/6.2.1/vsd/vadapter/src/wva_module.c#11 $
   $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include <stddef.h>
#include <string.h>
#include "err.h"
#include "msg.h"
#include "rcinit.h"
#include "mmstd.h"

/* APR APIs. */
#include "apr_errcodes.h"
#include "apr_list.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_timer.h"
#include "apr_event.h"
#include "apr_thread.h"
#include "apr_memmgr.h"

/* WCDMA APIs. */
#include "wcdma_ext_api.h"

#ifdef FEATURE_SEGMENT_LOADING
#include "mcfg_seg_load.h"
//#include "IWCDMA.h"
#endif

/* VSD APIs. */
#include "drv_api.h"
#include "vs_task.h"
#include "voice_util_api.h"
#include "vs.h"
#include "voicelog_if.h"
#include "voicelog_utils.h"

/* SELF APIs. */
#include "wva_if.h"
#include "wva_iresource_if.h"
#include "wva_i.h"

/*****************************************************************************
 * Defines                                                                   *
 ****************************************************************************/

/* Currently voice agent support max two subscription and max two VSID. */
#define WVA_MAX_NUM_OF_SESSIONS_V ( 2 )

#define WVA_MAX_VOC_FRAME_LENGTH ( 70 )

/* Stmr tick count at which WCDMA reads DSM queue */
#define WVA_WCDMA_READS_UL_PACKET_AT_STMR_TICK_COUNT_V ( 30 )

/* Buffer time needed to put vocoder packet in DSM Queue.
 * Time in micro second. 
 */
#define WVA_BUF_TIME_FOR_PUTTING_UL_PACKET_IN_DSM_V ( 1000 )

/* The frame size in microseconds that each voice processing threads 
 * (vptx, vprx, encoder, decoder, decoder pp) operates on.
 */
#define WVA_VOICE_FRAME_SIZE_US_V ( 20000 ) 

#define WVA_VOICE_SAMPLE_RATE_UNDEFINED_V ( 0 ) 

#define WVA_VOICE_SAMPLE_RATE_NB_V ( 8000 ) 

#define WVA_VOICE_SAMPLE_RATE_WB_V ( 16000 ) 


/*****************************************************************************
 * Global Variables                                                          *
 ****************************************************************************/

static apr_lock_t wva_int_lock;
static apr_lock_t wva_thread_lock;
static apr_event_t wva_control_event;

static apr_memmgr_type wva_heapmgr;
static uint8_t wva_heap_pool[ WVA_HEAP_SIZE_V ];

static apr_objmgr_t wva_objmgr;
static apr_objmgr_object_t wva_object_table[ WVA_MAX_OBJECTS_V ];

static wva_gating_control_t wva_gating_work_pkt_q;
static apr_list_t wva_nongating_work_pkt_q;
static apr_list_t wva_free_work_pkt_q;
static wva_work_item_t wva_work_pkts[ WVA_NUM_WORK_PKTS_V ];


static apr_event_t wva_work_event;
static apr_thread_t wva_thread;
static uint8_t wva_task_stack[ WVA_TASK_STACK_SIZE ];

static wva_modem_subs_object_t* wva_subs_obj_list[WVA_MAX_NUM_OF_SESSIONS_V];
static wva_session_object_t* wva_session_obj_list[WVA_MAX_NUM_OF_SESSIONS_V];
static apr_lock_t wva_global_lock;

static bool_t wva_is_initialized = FALSE; 

#ifdef FEATURE_SEGMENT_LOADING
  interface_t *wva_ptr_W =  NULL;
#endif

/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

static void wva_int_lock_fn ( void )
{
  apr_lock_enter( wva_int_lock );
}

static void wva_int_unlock_fn ( void )
{
  apr_lock_leave( wva_int_lock );
}

static void wva_thread_lock_fn ( void )
{
  apr_lock_enter( wva_thread_lock );
}

static void wva_thread_unlock_fn ( void )
{
  apr_lock_leave( wva_thread_lock );
}


static void wva_sfg_timer_cb (
  void* session_context
)
{
  uint32_t rc = APR_EOK;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "WVA_INTERNAL_EVENT_SEND_SILENCE_FRAME -  session_obj=(0x%08x), "
         "event_id=(0x%08x)", session_context, WVA_INTERNAL_EVENT_SEND_SILENCE_FRAME );

  rc = wva_prepare_and_dispatch_event_packet( session_context,
         WVA_INTERNAL_EVENT_SEND_SILENCE_FRAME, NULL, 0 );

  return;
}

/****************************************************************************
 * WVA CMDs & EVENTs PACKET QUEUING FUNCTIONS                               *
 ****************************************************************************/

/**
 * Queues the wva_cmd_packet_t and wva_event_packet_t. In
 * case of failure to queue a apr packet, packet shall be
 * freed by the caller.
 */
WVA_INTERNAL uint32_t wva_queue_work_packet (
  wva_work_item_queue_type_t queue_type,
  wva_work_item_packet_type_t pkt_type,
  void* packet
)
{
  uint32_t rc = APR_EOK;
  wva_work_item_t* work_item = NULL;
  apr_list_t* work_queue = NULL;

  switch ( queue_type )
  {
   case WVA_WORK_ITEM_QUEUE_TYPE_NONGATING:
     work_queue = &wva_nongating_work_pkt_q;
     break;

   case WVA_WORK_ITEM_QUEUE_TYPE_GATING:
     work_queue = &wva_gating_work_pkt_q.cmd_q;
     break;

   default:
     rc = APR_EUNSUPPORTED;
     break;
  }

  for ( ;; )
  {
    /* Get a free command structure. */
    rc = apr_list_remove_head( &wva_free_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) );
    if ( rc )
    {
      rc = APR_ENORESOURCE;
      /* No free WORK packet structure is available. */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
             "wva_queue_work_packet(): Ran out of WORK packets, rc=0x%08x, "
             "wva_state=%d",   rc, wva_is_initialized );
      break;
    }

    if ( pkt_type == WVA_WORK_ITEM_PKT_TYPE_CMD )
    {
      work_item->pkt_type = WVA_WORK_ITEM_PKT_TYPE_CMD;
    }
    else if ( pkt_type == WVA_WORK_ITEM_PKT_TYPE_EVENT )
    {
     work_item->pkt_type = WVA_WORK_ITEM_PKT_TYPE_EVENT;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
           "wva_queue_work_packet(): Invalid packet type!!!" );
      WVA_PANIC_ON_ERROR ( APR_ENOTEXIST );
    }

    work_item->packet = packet;


    /* Add to incoming request work queue */
    rc = apr_list_add_tail( work_queue, &work_item->link );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_queue_work_packet() - ERROR: rc=0x%08x", rc );
      /* Add back to wva_free_work_pkt_q */
      work_item->pkt_type = WVA_WORK_ITEM_PKT_TYPE_NONE;
      work_item->packet = NULL;
      ( void ) apr_list_add_tail( &wva_free_work_pkt_q, &work_item->link );
    }
    else
    {
      /**
       * Signal appropriate thread.
       */
      wva_signal_run();
    }

    break;
  } /* for loop ends. */

  return rc;
}  /* wva_queue_work_packet() ends. */


/****************************************************************************
 * WVA CMDs/EVENTs PACKET PREPARE/DISPATCHER/FREE ROUTINES                  *
 ****************************************************************************/

WVA_INTERNAL uint32_t wva_free_cmd_packet (
  wva_cmd_packet_t* packet
)
{
  uint32_t rc = VS_EOK;

  if ( packet != NULL )
  {
    if( packet->params != NULL )
    {
      /* Free the memory - p_cmd_packet->params. */
      apr_memmgr_free( &wva_heapmgr, packet->params );
      packet->params = NULL;
    }

    /* Free the memeory - p_cmd_packet. */
    apr_memmgr_free( &wva_heapmgr, packet );
    packet = NULL;
  }

  return rc;
}

/**
 * This is a common routine facilitating to prepare and
 * dispatches a CMD PKT.
 */
WVA_INTERNAL uint32_t wva_prepare_and_dispatch_cmd_packet (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  wva_cmd_packet_t* packet = NULL;

  for ( ;; )
  {
    packet = ( ( wva_cmd_packet_t* ) apr_memmgr_malloc( &wva_heapmgr,
                                           sizeof( wva_cmd_packet_t ) ) );
    if ( packet == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR( APR_ENORESOURCE );
      rc = APR_ENORESOURCE;
      break;
    }

    packet->cmd_id = cmd_id;
    packet->params = NULL;

    if ( ( size > 0 ) && ( params != NULL ) )
    {
      packet->params = apr_memmgr_malloc(  &wva_heapmgr, size );

      if ( packet->params == NULL )
      {
        rc = APR_ENORESOURCE;
        WVA_REPORT_FATAL_ON_ERROR( rc );
        ( void ) wva_free_cmd_packet( packet );
        break;
      }
      mmstd_memcpy( packet->params, size, params, size );
    }

    /* Queue the command packet for processing. */
    rc = wva_queue_work_packet( WVA_WORK_ITEM_QUEUE_TYPE_NONGATING,
                                WVA_WORK_ITEM_PKT_TYPE_CMD, ( void*) packet );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_prepare_and_dispatch_cmd_packet() - cmd pkt queuing failed. "
             "rc=(0x%08x)", rc );
      ( void ) wva_free_cmd_packet( packet );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "wva_prepare_and_dispatch_cmd_packet() cmd pkt queued with "
             "cmd_id=(0x%08x)", cmd_id );
    }

    break;
  }

  return rc;
}


WVA_INTERNAL uint32_t wva_free_event_packet (
  wva_event_packet_t* packet
)
{
  uint32_t rc = VS_EOK;

  if ( packet != NULL )
  {
    if( packet->params != NULL )
    {
      /* Free the memory - p_cmd_packet->params. */
      apr_memmgr_free( &wva_heapmgr, packet->params );
      packet->params = NULL;
    }

    /* Free the memeory - p_cmd_packet. */
    apr_memmgr_free( &wva_heapmgr, packet );
    packet= NULL;
  }

  return rc;
}

/**
 * This is a common routine facilitating to prepare and
 * dispatches a CMD PKT.
 */
WVA_INTERNAL uint32_t wva_prepare_and_dispatch_event_packet (
  void* session_context,
  uint32_t event_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  wva_event_packet_t* packet = NULL;

  for ( ;; )
  {
    packet = ( ( wva_event_packet_t* ) apr_memmgr_malloc( &wva_heapmgr,
                                         sizeof( wva_event_packet_t ) ) );
    if ( packet == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR( APR_ENORESOURCE );
      rc = APR_ENORESOURCE;
      break;
    }

    packet->session_context = session_context;
    packet->event_id = event_id;
    packet->params = NULL;

    if ( ( size > 0 ) && ( params != NULL ) )
    {
      packet->params = apr_memmgr_malloc(  &wva_heapmgr, size );

      if ( packet->params == NULL )
      {
        rc = APR_ENORESOURCE;
        WVA_REPORT_FATAL_ON_ERROR( rc );
        ( void ) wva_free_event_packet( packet );
        break;
      }
      mmstd_memcpy( packet->params, size, params, size );
    }

    /* Queue the command packet for processing. */
    rc = wva_queue_work_packet( WVA_WORK_ITEM_QUEUE_TYPE_NONGATING,
                                WVA_WORK_ITEM_PKT_TYPE_EVENT, ( void*) packet );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_prepare_and_dispatch_event_packet()-event pkt queuing failed "
             "rc=(0x%08x)", rc );
      ( void ) wva_free_event_packet( packet );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "wva_prepare_and_dispatch_event_packet()-event pkt queued with "
             "event_id=(0x%08x)", event_id );
    }

    break;
  }

  return rc;
}


/****************************************************************************
 * WVA OBJECT CREATION, DESTRUCTION AND INITIALISATION ROUTINES             *
 ****************************************************************************/

static int32_t wva_get_object (
  uint32_t handle,
  wva_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_objmgr_find_object( &wva_objmgr, handle, &objmgr_obj );
  if ( rc )
  {
    return APR_EFAILED;
  }

  *ret_obj = ( ( wva_object_t* ) objmgr_obj->any.ptr );

  return APR_EOK;
}

static uint32_t wva_mem_alloc_object (
  uint32_t size,
  wva_object_t** ret_object
)
{
  int32_t rc;
  wva_object_t* wva_obj;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_object == NULL )
  {
    return APR_EBADPARAM;
  }

  { /* Allocate memory for the WVA object. */
    wva_obj = apr_memmgr_malloc( &wva_heapmgr, size );
    if ( wva_obj == NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_mem_alloc_object(): Out of memory, requested size (%d)", size );
      return APR_ENORESOURCE;
    }

    /* Allocate a new handle for the MVS object. */
    rc = apr_objmgr_alloc_object( &wva_objmgr, &objmgr_obj );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "wva_mem_alloc_object(): Out of objects, rc = (0x%08X)", rc );
      apr_memmgr_free( &wva_heapmgr, wva_obj );
      return APR_ENORESOURCE;
    }

    /* Use the custom object type. */
    objmgr_obj->any.ptr = wva_obj;

    /* Initialize the base MVS object header. */
    wva_obj->header.handle = objmgr_obj->handle;
    wva_obj->header.type = WVA_OBJECT_TYPE_ENUM_UNINITIALIZED;
  }

  *ret_object = wva_obj;

  return APR_EOK;
}

static uint32_t wva_mem_free_object (
  wva_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &wva_objmgr, object->header.handle );
  apr_memmgr_free( &wva_heapmgr, object );

  return APR_EOK;
}

static uint32_t wva_create_modem_subs_object ( 
  wva_modem_subs_object_t** ret_subs_obj )
{
  uint32_t rc = APR_EOK;
  wva_modem_subs_object_t* subs_obj = NULL;

  if ( ret_subs_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = wva_mem_alloc_object( sizeof( wva_modem_subs_object_t ),
                             ( ( wva_object_t** ) &subs_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the Object. */
    subs_obj->header.type = WVA_OBJECT_TYPE_ENUM_MODEM_SUBSCRIPTION;

    subs_obj->asid = SYS_MODEM_AS_ID_NONE;
    subs_obj->vsid = WVA_VSID_UNDEFINED_V;
    subs_obj->pending_vsid = WVA_VSID_UNDEFINED_V;

    subs_obj->wcdma_handle = APR_NULL_V;
    subs_obj->is_wcdma_ready = FALSE;
    subs_obj->session_obj = NULL;
  }

  *ret_subs_obj = subs_obj;

  return APR_EOK;
}

static uint32_t wva_create_session_object ( 
  wva_session_object_t** ret_session_obj )
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;

  if ( ret_session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = wva_mem_alloc_object( sizeof( wva_session_object_t ),
                             ( ( wva_object_t** ) &session_obj ) );
  if ( rc )
  {
    *ret_session_obj = NULL;
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    session_obj->header.type = WVA_OBJECT_TYPE_ENUM_SESSION;

    session_obj->vsid = WVA_VSID_UNDEFINED_V;
    session_obj->active_subs_obj = NULL;
    session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_RELEASED;
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &session_obj->data_lock );
    WVA_PANIC_ON_ERROR(rc);

    ( void ) voice_dsm_amr_q_init ( &session_obj->ul_queue, WVA_AMR_DSM_Q_LEN );
    ( void ) voice_dsm_amr_q_init ( &session_obj->dl_queue, WVA_AMR_DSM_Q_LEN );
    mmstd_memset ( &session_obj->ul_chan_state, 0, 
                   sizeof( session_obj->ul_chan_state ) );
    mmstd_memset ( &session_obj->dl_chan_state, 0, 
                   sizeof( session_obj->dl_chan_state ) );

    rc = apr_timer_create( &session_obj->sfg_timer, wva_sfg_timer_cb, session_obj );
    WVA_PANIC_ON_ERROR( rc );

    session_obj->va_wva_event_cb = NULL;
    session_obj->va_session_context =  NULL;
    session_obj->is_resource_granted = FALSE;

    session_obj->vocoder_type = WVA_VOCODER_ID_UNDEFINED_V;
    session_obj->codec_mode = WVA_CODEC_MODE_UNDEFINED;
    session_obj->dtx_mode = FALSE;
  
    session_obj->vs_handle = APR_NULL_V;
    session_obj->vs_read_buf = NULL;
    session_obj->primed_read_buf = NULL;
    session_obj->vs_write_buf = NULL;
    session_obj->is_vs_ready = FALSE;
  }

  *ret_session_obj = session_obj;

  return APR_EOK;
}

static uint32_t wva_create_simple_job_object (
  uint32_t parentjob_handle,
  wva_simple_job_object_t** ret_job_obj
)
{
  int32_t rc;
  wva_simple_job_object_t* wva_obj = NULL;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = wva_mem_alloc_object( sizeof( wva_simple_job_object_t ),
                             ( ( wva_object_t** ) &wva_obj ) );
  if ( rc )
  {
    *ret_job_obj = NULL;
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    wva_obj->header.type = WVA_OBJECT_TYPE_ENUM_SIMPLE_JOB;
    wva_obj->context_handle = parentjob_handle;
    wva_obj->is_completed = 0;
  }

  *ret_job_obj = wva_obj;

  return APR_EOK;
}

WVA_INTERNAL int32_t wva_create_sequencer_job_object (
  wva_sequencer_job_object_t** ret_job_obj
)
{
  int32_t rc;
  wva_sequencer_job_object_t* job_obj = NULL;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = wva_mem_alloc_object( sizeof( wva_sequencer_job_object_t ),
                             ( ( wva_object_t** ) &job_obj ) );
  if ( rc )
  {
    *ret_job_obj = NULL;
    return APR_ENORESOURCE;
  }

  { /* Initialize the pending job object. */
    job_obj->header.type = WVA_OBJECT_TYPE_ENUM_SEQUENCER_JOB;

    job_obj->state = APR_NULL_V;
    job_obj->subjob_obj = NULL;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}


/****************************************************************************
 * WVA WCDMA <> VS MAPPING  ROUTINES                                          *
 ****************************************************************************/

static uint32_t wva_map_vocamr_codec_mode_wcdma_to_vs( 
  uint32_t wcdma_codec_mode,
  uint32_t* vs_codec_mode
)
{
  uint32_t rc = APR_EOK;

  switch ( wcdma_codec_mode )
  {
   case WCDMA_IVOCAMR_CODEC_MODE_0475:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0475;
     break;

   case WCDMA_IVOCAMR_CODEC_MODE_0515:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0515;
     break;

   case WCDMA_IVOCAMR_CODEC_MODE_0590:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0590;
     break;

   case WCDMA_IVOCAMR_CODEC_MODE_0670:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0670;
     break;

   case WCDMA_IVOCAMR_CODEC_MODE_0740:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0740;
     break;

   case WCDMA_IVOCAMR_CODEC_MODE_0795:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0795;
     break;

   case WCDMA_IVOCAMR_CODEC_MODE_1020:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_1020;
     break;

   case WCDMA_IVOCAMR_CODEC_MODE_1220:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_1220;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *vs_codec_mode = 0xFFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t wva_map_vocamrwb_codec_mode_wcdma_to_vs( 
  uint32_t wcdma_codec_mode,
  uint32_t* vs_codec_mode
)
{
  uint32_t rc = APR_EOK;

  switch ( wcdma_codec_mode )
  {
   case WCDMA_IVOCAMRWB_CODEC_MODE_0660:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_0660;
     break;
  
   case WCDMA_IVOCAMRWB_CODEC_MODE_0885:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_0885;
     break;
  
   case WCDMA_IVOCAMRWB_CODEC_MODE_1265:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1265;
     break;
  
   case WCDMA_IVOCAMRWB_CODEC_MODE_1425:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1425;
     break;
    
   case WCDMA_IVOCAMRWB_CODEC_MODE_1585:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1585;
     break;
    
   case WCDMA_IVOCAMRWB_CODEC_MODE_1825:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1825;
     break;
    
   case WCDMA_IVOCAMRWB_CODEC_MODE_1985:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1985;
     break;
   
   case WCDMA_IVOCAMRWB_CODEC_MODE_2305:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_2305;
     break;
  
   case WCDMA_IVOCAMRWB_CODEC_MODE_2385:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_2385;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *vs_codec_mode = 0xFFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t wva_map_vocoder_type_wcdma_to_vs(
  uint32_t wcdma_vocoder_type
)
{
  uint32_t vs_media_id = 0xFFFFFFFF;

  switch ( wcdma_vocoder_type )
  {
   case WCDMA_IVOCODER_ID_AMR:
     vs_media_id = VS_COMMON_MEDIA_ID_AMR;
     break;

   case WCDMA_IVOCODER_ID_AMRWB:
     vs_media_id = VS_COMMON_MEDIA_ID_AMRWB;
     break;
  }

  return vs_media_id;
}


static uint32_t wva_update_chan_state (
  voice_amr_chan_state_t* chan_state,
  uint32_t chan_class
)
{
  uint32_t rc = APR_EOK;

  switch ( chan_class )
  {
   case WCDMA_ICOMMON_CHAN_CLASS_TYPE_ABC:
     chan_state->has_chan_c = TRUE;
     /* fall through */

   case WCDMA_ICOMMON_CHAN_CLASS_TYPE_AB:
     chan_state->has_chan_b = TRUE;
     /* fall through */

   case WCDMA_ICOMMON_CHAN_CLASS_TYPE_A:
     chan_state->has_chan_a = TRUE;
     break;

   case WCDMA_ICOMMON_CHAN_CLASS_TYPE_NONE:
    {
      chan_state->has_chan_a = FALSE;
      chan_state->has_chan_b = FALSE;
      chan_state->has_chan_c = FALSE;
    }
    break;

   default:
     rc = APR_EBADPARAM;
     break;
  }

  if ( rc )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_update_chan_state(): Invalid channel class=(0x%08x)", 
           chan_class );
  }

  return rc;
}

static void wva_downlink_channel_data_available (
  uint8_t lc_id,
  uint8_t n_unit,
  void* session_context 
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  session_obj = ( wva_session_object_t* ) session_context;

  for ( ;; )
  {
    if( lc_id == session_obj->dl_chan_state.lcc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wva_downlink_channel_data_available(): logical channel C data "
             "available lc_id =(%d)", lc_id );
      break;
    }

    if( lc_id == session_obj->dl_chan_state.lcb )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wva_downlink_channel_data_available(): logical channel B data "
             "available lc_id =(%d)", lc_id );
      break;
    }

    if( lc_id == session_obj->dl_chan_state.lca )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wva_downlink_channel_data_available(): logical channel A data "
             "available lc_id =(%d)", lc_id );
      /* Post an internal event for DL processing. */
      rc = wva_prepare_and_dispatch_event_packet ( 
             session_context, WVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE, NULL, 0 );
      break;
    }

    break;
  }

  return;
}

/****************************************************************************
 * WVA COMMON ROUTINES                                                      *
 ****************************************************************************/

static uint32_t wva_set_voc_codec_mode (
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_vocamr_cmd_set_codec_mode_t vocamr_codec_cmd;
  vs_vocamrwb_cmd_set_codec_mode_t vocamrwb_codec_cmd;
  uint32_t codec_mode;

  switch ( session_obj->vocoder_type )
  {
   case WCDMA_IVOCODER_ID_AMR:
     {
       rc = wva_map_vocamr_codec_mode_wcdma_to_vs( session_obj->codec_mode,
                                                   &codec_mode );
       if ( ( rc ) || ( session_obj->vs_handle == APR_NULL_V ) )
       {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "WVA: AMR: Unsupported CodecMode=(0x%08x)", session_obj->codec_mode );
       }
       else
       {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
                "WVA: AMR: CodecMode=(%d) sent to VS", codec_mode );
         vocamr_codec_cmd.handle = session_obj->vs_handle;
         vocamr_codec_cmd.codec_mode  = (vs_vocamr_codec_mode_t) codec_mode;
         rc  = vs_call( VS_VOCAMR_CMD_SET_CODEC_MODE, &vocamr_codec_cmd,
                        sizeof( vocamr_codec_cmd ) );
       }
     }
     break;

   case WCDMA_IVOCODER_ID_AMRWB:
     {
       rc = wva_map_vocamrwb_codec_mode_wcdma_to_vs ( session_obj->codec_mode,
                                                      &codec_mode );
       if ( ( rc ) || ( session_obj->vs_handle == APR_NULL_V ) )
       {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "WVA: AMR-WB: Unsupported CodecMode=(0x%08x)", session_obj->codec_mode );
       }
       else
       {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
                "WVA: AMR-WB: CodecMode=(%d) sent to VS", codec_mode );
         vocamrwb_codec_cmd.handle = session_obj->vs_handle;
         vocamrwb_codec_cmd.codec_mode  = (vs_vocamrwb_codec_mode_t) codec_mode;
         rc  = vs_call( VS_VOCAMRWB_CMD_SET_CODEC_MODE, &vocamrwb_codec_cmd,
                        sizeof( vocamrwb_codec_cmd ) );
       }
     }
     break;

   default:
    {
     rc = APR_EUNSUPPORTED;
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "wva_set_voc_codec_mode(): Unsupported vocoder=(0x%08x), ",
            session_obj->vocoder_type );
    }
    break;
  }

  return rc;
}


static uint32_t wva_set_voc_dtx_mode (
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_vocamr_cmd_set_dtx_mode_t vocamr_dtx_cmd;
  vs_vocamrwb_cmd_set_dtx_mode_t vocamrwb_dtx_cmd;

  switch ( session_obj->vocoder_type )
  {
   case WCDMA_IVOCODER_ID_AMR:
     {
       vocamr_dtx_cmd.handle = session_obj->vs_handle;
       vocamr_dtx_cmd.enable_flag  = session_obj->dtx_mode;
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WVA: AMR dtx_mode=(%d) sent to VS", session_obj->dtx_mode );
       rc  = vs_call( VS_VOCAMR_CMD_SET_DTX_MODE, &vocamr_dtx_cmd,
                      sizeof( vocamr_dtx_cmd ) );
     }
     break;

   case WCDMA_IVOCODER_ID_AMRWB:
     {
       vocamrwb_dtx_cmd.handle = session_obj->vs_handle;
       vocamrwb_dtx_cmd.enable_flag  = session_obj->dtx_mode;
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WVA: AMR-WB dtx_mode=(%d) sent to VS", session_obj->dtx_mode );
       rc  = vs_call( VS_VOCAMRWB_CMD_SET_DTX_MODE, &vocamrwb_dtx_cmd,
                      sizeof( vocamrwb_dtx_cmd ) );
     }
     break;

   default:
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "wva_set_voc_dtx_mode(): Unsupported vocoder=(0x%08x), ",
            session_obj->vocoder_type );
     break;
  }

  if ( rc != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "WVA: failed to set dtx_mode=(%d), vocoder=(0x%08x)",
           session_obj->dtx_mode, session_obj->vocoder_type );
  }
  return rc;

}


static uint32_t wva_get_dl_vocoder_packet ( 
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_buffer_t* vs_buffer = NULL;
  vs_vocamr_frame_info_t* vocamr_info = NULL;
  vs_vocamrwb_frame_info_t* vocamrwb_info = NULL;

  vs_buffer = session_obj->vs_write_buf;

  vs_buffer->media_id = wva_map_vocoder_type_wcdma_to_vs( session_obj->vocoder_type );

  rc = voice_amr_dl_processing( vs_buffer, &session_obj->dl_queue );
  if ( rc ) return rc;

  vs_buffer->flags = TRUE;

  if ( VS_COMMON_MEDIA_ID_AMR == vs_buffer->media_id )
  {
    vocamr_info = (vs_vocamr_frame_info_t*)vs_buffer->frame_info;
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "WVA: AMR: GET_DL_PACKET: FrameType=(%d), CodecMode=(%d)",
           vocamr_info->frame_type, vocamr_info->codec_mode );
  }
  else if ( VS_COMMON_MEDIA_ID_AMRWB == vs_buffer->media_id )
  {
    vocamrwb_info = (vs_vocamrwb_frame_info_t*)vs_buffer->frame_info;
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "WVA: AMR-WB: GET_DL_PACKET: FrameType=(%d), CodecMode=(%d)",
           vocamrwb_info->frame_type, vocamrwb_info->codec_mode );
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "WVA: AMR-WB: GET_DL_PACKET: Invalid Vocoder type!!!" );
  }

  return rc;
}


static uint32_t wva_deliver_ul_vocoder_packet ( 
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_buffer_t* vs_buffer = NULL;
  vs_vocamr_frame_info_t* vocamr_info = NULL;
  vs_vocamrwb_frame_info_t* vocamrwb_info = NULL;

  vs_buffer = session_obj->vs_read_buf;

  rc = voice_amr_ul_processing( vs_buffer, &session_obj->ul_chan_state,
                                &session_obj->ul_queue );

  if ( VS_COMMON_MEDIA_ID_AMR == vs_buffer->media_id )
  {
    vocamr_info = (vs_vocamr_frame_info_t*)vs_buffer->frame_info;
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "WVA: AMR: Deliver_UL_PACKET: FrameType=(%d), CodecMode=(%d)",
           vocamr_info->frame_type, vocamr_info->codec_mode );
  }
  else if ( VS_COMMON_MEDIA_ID_AMRWB == vs_buffer->media_id )
  {
    vocamrwb_info = (vs_vocamrwb_frame_info_t*)vs_buffer->frame_info;
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "WVA: AMR-WB: Deliver_UL_PACKET: FrameType=(%d), CodecMode=(%d)",
           vocamrwb_info->frame_type, vocamrwb_info->codec_mode );
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "WVA: AMR-WB: Deliver_UL_PACKET: Invalid MediaId!!!" );
  }

  return rc;
}


/****************************************************************************
 * WVA VS SESSION ROUTINES                                                  *
 ****************************************************************************/

static void wva_log_event_info(
  void* session_context,
  uint32_t event_id
)
{
  wva_modem_subs_object_t* subs_obj = ( wva_modem_subs_object_t* ) session_context;
  wva_session_object_t* session_obj = ( wva_session_object_t* ) session_context;
 
  if ( session_context == NULL ) return;
 
   switch( event_id )
   {
     case VS_COMMON_EVENT_CMD_RESPONSE:
     {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WVA: VSID=(0x%08x): VS_COMMON_EVENT_CMD_RESPONSE recieved",
              session_obj->vsid );
     }
     break;
 
     case VS_COMMON_EVENT_NOT_READY:
     {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WVA: VSID=(0x%08x): VS_COMMON_EVENT_NOT_READY recieved",
              session_obj->vsid );
     }
     break;
 
     case VS_COMMON_EVENT_READY:
     {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WVA: VSID=(0x%08x): VS_COMMON_EVENT_READY recieved",
              session_obj->vsid );
     }
     break;
 
     case VS_VOC_EVENT_READ_AVAILABLE:
     {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
              "WVA: VSID=(0x%08x): VS_VOC_EVENT_READ_AVAILABLE recieved",
              session_obj->vsid );
     }
     break;
 
     case VS_VOC_EVENT_WRITE_BUFFER_RETURNED:
     {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
              "WVA: VSID=(0x%08x): VS_VOC_EVENT_WRITE_BUFFER_RETURNED recieved",
              session_obj->vsid );
     }
     break;
 
     case VS_COMMOM_EVENT_EAMR_MODE_CHANGE:
     {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "WVA: VSID=(0x%08x): VS_COMMOM_EVENT_EAMR_MODE_CHANGE recieved",
             session_obj->vsid );
     }
     break;
 
     case WVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE:
     {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "WVA: VSID=(0x%08x): WVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE recieved",
             session_obj->vsid );
     }
     break;
 
     case WVA_INTERNAL_EVENT_SEND_SILENCE_FRAME:
     {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
             "WVA: VSID=(0x%08x): WVA_INTERNAL_EVENT_SEND_SILENCE_FRAME recieved",
             session_obj->vsid );
     }
     break;
 
     case WCDMA_IVOICE_EVENT_SET_LOGICAL_CHANNELS:
     {
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WVA: ASID=(%d): VSID=(0x%08x): WCDMA_IVOICE_EVENT_SET_LOGICAL_CHANNELS recieved",
              subs_obj->asid, subs_obj->vsid );
     }
     break;
 
     case WCDMA_IVOICE_EVENT_REQUEST_START:
     {
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WVA: VSID=(0x%08x): ASID=(%d): WCDMA_IVOICE_EVENT_REQUEST_START recieved",
              subs_obj->asid, subs_obj->vsid );
     }
     break;
 
     case WCDMA_IVOICE_EVENT_REQUEST_STOP:
     {
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WVA: ASID=(%d): VSID=(0x%08x): WCDMA_IVOICE_EVENT_REQUEST_STOP recieved",
              subs_obj->asid, subs_obj->vsid );
     }
     break;
 
     case WCDMA_IVOICE_EVENT_SELECT_OWNER:
     {
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WVA: ASID=(%d): VSID=(0x%08x): WCDMA_IVOICE_EVENT_SELECT_OWNER recieved",
              subs_obj->asid, subs_obj->vsid );
     }
     break;
 
     case WCDMA_IVOICE_EVENT_REQUEST_CODEC_MODE:
     {
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "WVA: ASID=(%d): VSID=(0x%08x): WCDMA_IVOICE_EVENT_REQUEST_CODEC_MODE recieved",
              subs_obj->asid, subs_obj->vsid );
     }
     break;
 
     case WCDMA_IVOICE_EVENT_REQUEST_SCR_MODE:
     {
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
              "WVA: ASID=(%d): VSID=(0x%08x): WCDMA_IVOICE_EVENT_REQUEST_SCR_MODE recieved",
              subs_obj->asid, subs_obj->vsid );
     }
     break;
 
     case WCDMA_IVOICEL1_EVENT_VFR_NOTIFICATION:
     {
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
              "WVA: ASID=(%d): VSID=(0x%08x): WCDMA_IVOICEL1_EVENT_VFR_NOTIFICATION recieved",
              subs_obj->asid, subs_obj->vsid );
     }
     break;
 
    default:
      break;
   }

   return;
}


static uint32_t wva_vs_event_cb(
  uint32_t event_id,
  void* params,
  uint32_t size,
  void* session_context
)
{
  uint32_t rc = APR_EOK;

  if ( wva_is_initialized == FALSE ) return APR_EOK;

  switch ( event_id )
  {
   case VS_COMMON_EVENT_CMD_RESPONSE:
   case VS_COMMON_EVENT_NOT_READY:
   case VS_COMMON_EVENT_READY:
   case VS_VOC_EVENT_READ_AVAILABLE:
   case VS_VOC_EVENT_WRITE_BUFFER_RETURNED:
   case VS_COMMOM_EVENT_EAMR_MODE_CHANGE:
    {
      (void) wva_log_event_info( session_context, event_id );
      rc = wva_prepare_and_dispatch_event_packet( session_context, event_id,
                                                  params, size );
    }
    break;

   default:
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
            "wva_vs_event_cb(): Unsupported event (%d)", event_id );
     rc = APR_EFAILED;
  }


  return rc;
}


static uint32_t wva_vs_prime_read_buffer (
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_prime_read_buffer_t prime_read_buf_cmd;

  prime_read_buf_cmd.handle = session_obj->vs_handle;
  prime_read_buf_cmd.buffer = session_obj->vs_read_buf;

  rc = vs_call( VS_VOC_CMD_PRIME_READ_BUFFER, ( void* ) &prime_read_buf_cmd,
                sizeof( prime_read_buf_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_vs_prime_read_buffer(): Failed to prime vs_read_buf, "
           "vs_read_buf=(0x%08x), rc=(0x%08x)", session_obj->vs_read_buf, rc );
  }
  else
  {
    session_obj->vs_read_buf = NULL;
  }

  return APR_EOK;
}


static uint32_t wva_vs_read_buffer (
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_read_buffer_t read_buf_cmd;

  read_buf_cmd.handle = session_obj->vs_handle;
  read_buf_cmd.ret_buffer = &session_obj->vs_read_buf;

  rc = vs_call( VS_VOC_CMD_READ_BUFFER, ( void* )&read_buf_cmd,
                sizeof( read_buf_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_vs_read_buffer(): Failed to read vs_buffer, "
           "vs_read_buf=(0x%08x), rc=(0x%08x)", session_obj->vs_read_buf, rc );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "wva_vs_read_buffer(): read buffer available vs_read_buf=(0x%08x) ",
           session_obj->vs_read_buf );
  }

  return rc;
}


static uint32_t wva_vs_write_buffer (
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_write_buffer_t write_buf_cmd;

  write_buf_cmd.handle = session_obj->vs_handle;
  write_buf_cmd.buffer = session_obj->vs_write_buf;

  rc = vs_call( VS_VOC_CMD_WRITE_BUFFER, ( void* )&write_buf_cmd,
                sizeof( write_buf_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_vs_write_buffer(): Failed to pass vs_write_buf  = (0x%08x), "
           "rc = (0x%08x)", session_obj->vs_write_buf, rc );
  }
  else
  {
    session_obj->vs_write_buf = NULL;
  }

  return APR_EOK;
}

static uint32_t wva_vs_free_buffer (
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_free_buffer_t free_buf_cmd;

  if ( session_obj->vs_read_buf != NULL )
  {
    free_buf_cmd.handle = session_obj->vs_handle;
    free_buf_cmd.buffer = session_obj->vs_read_buf;

    rc = vs_call( VS_VOC_CMD_FREE_BUFFER, ( void* )&free_buf_cmd,
                  sizeof( free_buf_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_vs_free_buffer(): Failed to free vs_read_buf = (0x%08x), " 
             "rc = (0x%08x)", session_obj->vs_read_buf, rc );
    }
    session_obj->vs_read_buf = NULL;
    session_obj->primed_read_buf = NULL;
  }

  if ( session_obj->vs_write_buf != NULL )
  {
    free_buf_cmd.handle = session_obj->vs_handle;
    free_buf_cmd.buffer = session_obj->vs_write_buf;

    rc = vs_call( VS_VOC_CMD_FREE_BUFFER, ( void* )&free_buf_cmd,
                  sizeof( free_buf_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_vs_free_buffer(): Failed to free vs_write_buf = (0x%08x), " 
             "rc = (0x%08x)", session_obj->vs_write_buf, rc );
    }
    session_obj->vs_write_buf = NULL;
  }

  return rc;
}

static uint32_t wva_vs_alloc_buffer (
  wva_session_object_t* session_obj
)
{
  uint32_t rc;
  vs_voc_cmd_alloc_buffer_t alloc_buf_cmd;

  for ( ;; )
  {
    /* Allocate read buffer. */
    session_obj->vs_read_buf = NULL;
    alloc_buf_cmd.handle = session_obj->vs_handle;
    alloc_buf_cmd.ret_buffer = &session_obj->vs_read_buf;
    alloc_buf_cmd.req_max_frame_size = WVA_MAX_VOC_FRAME_LENGTH;//322; 
    rc = vs_call( VS_VOC_CMD_ALLOC_BUFFER, ( void* )&alloc_buf_cmd,
                  sizeof( alloc_buf_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_vs_alloc_buffer(): Failed to allocate read buffer, "
             "rc = (0x%08x)", rc );
      break;
    }
    session_obj->primed_read_buf = session_obj->vs_read_buf;

    /* Allocate write buffer. */
    session_obj->vs_write_buf = NULL;
    alloc_buf_cmd.handle = session_obj->vs_handle;
    alloc_buf_cmd.ret_buffer = &session_obj->vs_write_buf;
    alloc_buf_cmd.req_max_frame_size = WVA_MAX_VOC_FRAME_LENGTH;//322; 
    rc = vs_call( VS_VOC_CMD_ALLOC_BUFFER, ( void* )&alloc_buf_cmd,
                  sizeof( alloc_buf_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_vs_alloc_buffer(): Failed to allocate write buffer, "
             "rc = (0x%08x)", rc );
      ( void ) wva_vs_free_buffer ( session_obj );
      break;
    }

    break;
  }

  return rc;
}

static uint32_t wva_vs_close_session (
  wva_session_object_t* session_obj,
  void* client_context
)
{
 uint32_t rc = APR_EOK;
 vs_voc_cmd_close_t close_cmd;
 
 for ( ;; )
 {
   if ( session_obj->vs_handle == APR_NULL_V )
   {
     rc = APR_EOK;
     break;
   }
 
   /* Free read and write buffers. */
   ( void ) wva_vs_free_buffer ( session_obj );
 
   close_cmd.handle = session_obj->vs_handle;
   close_cmd.client_context = client_context;
 
   rc = vs_call( VS_VOC_CMD_CLOSE, (void*)&close_cmd, sizeof( close_cmd ) );
   if ( rc )
   {
     MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
            "wva_vs_close_session(): Failed to close VS session, " 
            "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
   }
   else
   {
     session_obj->vs_handle = APR_NULL_V;
     rc = APR_EPENDING;
   }
 
   break;
 }

  return rc;
}

static uint32_t wva_vs_open_session (
 wva_session_object_t* session_obj
)
{
  uint32_t rc;
  vs_voc_cmd_open_t open_cmd;

  for ( ;; )
  {
    /* Open VS session. */
    session_obj->is_vs_ready = FALSE;

    open_cmd.ret_handle = &session_obj->vs_handle;
    open_cmd.vsid = session_obj->vsid;
    open_cmd.client_id = VS_VOC_CLIENT_WCDMA;
    open_cmd.session_context = ( void* )session_obj;
    open_cmd.event_cb = wva_vs_event_cb;

    rc = vs_call( VS_VOC_CMD_OPEN, (void*)&open_cmd, sizeof( open_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_vs_open_session(): failed to open VS session, "
             "client = (0x%08x), rc = (0x%08x)", open_cmd.client_id, rc );
      break;
    }

    rc = wva_vs_alloc_buffer( session_obj );
    if ( rc )
    {
      ( void ) wva_vs_close_session ( session_obj, NULL );
      
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_vs_open_session(): failed to alloc VS buffers, "
             "client = (0x%08x), rc = (0x%08x)", open_cmd.client_id, rc );
      break;
    }

    break;
  }

  return rc;
}

static uint32_t wva_vs_flush_buffers (
  wva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_flush_buffers_t vs_flush_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }
  
    vs_flush_cmd.handle = session_obj->vs_handle;
    vs_flush_cmd.client_context = client_context;
    rc = vs_call( VS_VOC_CMD_FLUSH_BUFFERS, &vs_flush_cmd,
                  sizeof( vs_flush_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_vs_flush_buffers(): Failed to flush VS buffers, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      rc = APR_EPENDING;
    }
  
    break;
  } /* For loop ends here. */
  
  return rc;
}


static uint32_t wva_vs_disable_vocoder (
  wva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_disable_t vs_disable_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }

    vs_disable_cmd.handle = session_obj->vs_handle;
    vs_disable_cmd.client_context = client_context;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "WVA: VS_VOC_CMD_DISABLE(): vocoder=(0x%08x)",
           wva_map_vocoder_type_wcdma_to_vs( session_obj->vocoder_type ) );
    rc = vs_call( VS_VOC_CMD_DISABLE, &vs_disable_cmd,
                  sizeof( vs_disable_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_vs_disable_vocoder(): Failed to disable VS session, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      rc = APR_EPENDING;
    }
   
    break;
  }/* For loop ends here. */

  return rc;
}


static uint32_t wva_vs_enable_vocoder (
  wva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  uint32_t media_id;
  vs_voc_cmd_enable_t vs_enable_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }

    media_id = wva_map_vocoder_type_wcdma_to_vs ( session_obj->vocoder_type );
    vs_enable_cmd.handle = session_obj->vs_handle;
    vs_enable_cmd.media_id = media_id;
    vs_enable_cmd.client_context = ( void* ) client_context;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "WVA: VS_VOC_CMD_ENABLE(): vocoder=(0x%08x)", vs_enable_cmd.media_id );
    rc = vs_call( VS_VOC_CMD_ENABLE, &vs_enable_cmd,
                  sizeof( vs_enable_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_vs_enable_vocoder(): Failed to enable VS session, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      rc = APR_EPENDING;
    }

    break;
  }/* For loop ends here. */

  return rc;
}


/****************************************************************************
 * WVA WCDMA SESSION ROUTINES                                                  *
 ****************************************************************************/

static uint32_t wva_wcdma_event_cb(
 sys_modem_as_id_e_type asid,
 uint32_t event_id,
 void* params,
 uint32_t size
)
{
  uint32_t rc = APR_EOK;
  void* session_context = NULL;

  if ( ( asid < SYS_MODEM_AS_ID_1 ) || ( asid > SYS_MODEM_AS_ID_2 ) )
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_wcdma_event_cb(): ASID=(%d) not supported", asid );
    return APR_EBADPARAM;
  }

  session_context = ( void* ) wva_subs_obj_list[ asid ];

  switch ( event_id )
  {
   case WCDMA_IVOICE_EVENT_REQUEST_START:
   case WCDMA_IVOICE_EVENT_REQUEST_STOP:
   case WCDMA_IVOICE_EVENT_SELECT_OWNER:
   case WCDMA_IVOICE_EVENT_SET_LOGICAL_CHANNELS:
   case WCDMA_IVOICE_EVENT_REQUEST_CODEC_MODE:
   case WCDMA_IVOICE_EVENT_REQUEST_SCR_MODE:
   case WCDMA_IVOICEL1_EVENT_VFR_NOTIFICATION:
    {
      ( void ) wva_log_event_info( session_context, event_id );
      rc = wva_prepare_and_dispatch_event_packet( session_context, event_id,
                                                  params, size );
    }
    break;

   default:
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
            "wva_wcdma_event_cb(): Unsupported event (%d)", event_id );
     rc = APR_EFAILED;
  }

  return rc;
}

static uint32_t wva_wcdma_open_session (
 wva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  wcdma_ivoice_cmd_open_t open_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "wva_wcdma_open_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  open_cmd.ret_handle = &subs_obj->wcdma_handle;
  open_cmd.asid = subs_obj->asid;
  open_cmd.event_cb = wva_wcdma_event_cb;  

#ifndef WINSIM
#ifdef FEATURE_SEGMENT_LOADING
  wva_ptr_W = get_wcdma_interface();
  if ( wva_ptr_W != NULL )
  {
    wcdma_ext_audio_api( WCDMA_VOICE_CMD_OPEN, &open_cmd, sizeof( open_cmd ) );
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "wva_wcdma_open_session(): WCDMA SEGMENT NOT LOADED!!");
  }
#else
  rc = wcdma_ext_audio_api( WCDMA_VOICE_CMD_OPEN, &open_cmd, sizeof( open_cmd ) );
#endif

  if ( APR_EUNSUPPORTED == rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_wcdma_open_session(): WCDMA is UNSUPPORTED asid = (0x%08x), "
           "rc = (0x%08x)", open_cmd.asid, rc );
    rc = APR_EOK;
  }
  else if ( APR_EOK != rc)
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_wcdma_open_session(): Failed to open WCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", open_cmd.asid, rc );
  }
  else
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "wva_wcdma_open_session(): WCDMA session successfully opened "
           " asid = (0x%08x), rc = (0x%08x)", open_cmd.asid, rc );
  }
#endif

  return rc;
}

static uint32_t wva_wcdma_close_session (
  wva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  wcdma_ivoice_cmd_close_t close_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "wva_wcdma_close_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  close_cmd.handle = subs_obj->wcdma_handle;

#ifndef WINSIM

#ifdef FEATURE_SEGMENT_LOADING
  wva_ptr_W = get_wcdma_interface();
  if ( wva_ptr_W != NULL )
  {
    rc = wcdma_ext_audio_api( WCDMA_VOICE_CMD_CLOSE,
                              &close_cmd, sizeof( close_cmd ) );
  }
  else
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "wva_wcdma_close_session(): WCDMA SEGMENT NOT LOADED!!");
  }

#else
  rc = wcdma_ext_audio_api( WCDMA_VOICE_CMD_CLOSE, &close_cmd, sizeof( close_cmd ) );
#endif

  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_wcdma_close_session(): failed to close WCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}


static uint32_t wva_wcdma_start_session (
  wva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  wcdma_ivoice_cmd_start_t start_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "wva_wcdma_start_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  start_cmd.handle = subs_obj->wcdma_handle;

#ifndef WINSIM
  rc = wcdma_ext_audio_api ( WCDMA_VOICE_CMD_START, &start_cmd,
                             sizeof( start_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_wcdma_start_session(): failed to close WCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}

static uint32_t wva_wcdma_stop_session (
  wva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  wcdma_ivoice_cmd_stop_t stop_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "wva_wcdma_stop_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  stop_cmd.handle = subs_obj->wcdma_handle;

#ifndef WINSIM
  rc = wcdma_ext_audio_api ( WCDMA_VOICE_CMD_STOP, &stop_cmd,
                             sizeof( stop_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_wcdma_stop_session(): failed to close WCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}

static uint32_t wva_wcdma_set_vfr_notification (
  wva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  wcdma_ivoicel1_cmd_set_vfr_notification_t set_vfr_cmd;

  if( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "wva_wcdma_set_vfr_notification(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  set_vfr_cmd.asid = subs_obj->asid;
  set_vfr_cmd.enable_flag = subs_obj->is_wcdma_ready;

#ifndef WINSIM
  rc = wcdma_ext_audio_api ( WCDMA_VOICE_CMD_SET_VFR_NOTIFICATION, &set_vfr_cmd,
                             sizeof( set_vfr_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_wcdma_stop_session(): failed to close WCDMA session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}


static uint32_t wva_wcdma_deregister_ul_logical_channels (
  wcdma_ivoice_event_set_logical_channels_t* chan_info,
  wva_modem_subs_object_t* subs_obj,
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  uint8_t nchan = 0;
  voice_amr_chan_state_t* chan_state;
  voice_amr_dsm_queue_t* ul_queue = NULL;
  wcdma_cmd_l2_ul_cmd_buffer_t ul_dereg_cmd_buf;

  for( ;; )
  {
    chan_state = &session_obj->ul_chan_state;
    ul_queue = &session_obj->ul_queue;

#ifndef WINSIM
    /* De-register logical channels if necessary
     *
     * L2 Layer works on command buffer, Get the command packet using 
     * WCDMA_GET_L2_UL_CMD_BUFFER, fill it with Command header 
     * (cmd_hdr.cmd_id) =  RLC_UL_DEREGISTER_SRVC_REQ and fill appropriate 
     * cmd_data.(ul_dereg) members and dispatch it WCDMA_PUT_L2_UL_CMD
     */
     ul_dereg_cmd_buf.service = NULL;
      rc  = wcdma_ext_audio_api( WCDMA_GET_L2_UL_CMD_BUFFER, &ul_dereg_cmd_buf,
                                 sizeof( ul_dereg_cmd_buf ) );
      if( ( rc != WCDMA_STATUS_GOOD ) || ( ul_dereg_cmd_buf.service == NULL ) )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "Couldn't get WCMDA L2 uplink command buffer for dereg. "
               "rc = (0x%08X)", rc );
        break;
      }
  
      /*<- command id for deregister these channels */
      ul_dereg_cmd_buf.service->cmd_hdr.cmd_id = RLC_UL_DEREGISTER_SRVC_REQ;
      ul_dereg_cmd_buf.service->cmd_hdr.as_id = subs_obj->asid;
 
      /* deregister class A channel if necessary */
      if ( ( chan_state->has_chan_a == FALSE ) || 
           ( chan_state->lca != chan_info->lc_class_a ) )
      {
        if ( chan_state->lca != 0 )
        {
          ul_dereg_cmd_buf.service->cmd_data.ul_dereg.rlc_id[nchan] = chan_state->lca;
          nchan++;
          chan_state->lca = 0;
        }
      }
 
      /* deregister class B channel if necessary */
      if ( ( chan_state->has_chan_b == FALSE ) || 
           ( chan_state->lcb != chan_info->lc_class_b ) )
      {
        if ( chan_state->lcb != 0 )
        {
         ul_dereg_cmd_buf.service->cmd_data.ul_dereg.rlc_id[nchan] = chan_state->lcb;
         nchan++;
         chan_state->lcb = 0;
        }
      }
  
      /* deregister class C channel if necessary */
      if ( ( chan_state->has_chan_c == FALSE ) || 
           ( chan_state->lcc != chan_info->lc_class_c ) )
      {
        if ( chan_state->lcc != 0 )
        {
          ul_dereg_cmd_buf.service->cmd_data.ul_dereg.rlc_id[nchan] = chan_state->lcc;
          nchan++;
          chan_state->lcc = 0;
        }
      }

      if( nchan > 0 )
      {
        ( void ) voice_dsm_amr_q_empty( &session_obj->ul_queue );
         MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "DSM UL queue empty during de-registration" );
      }

      ul_dereg_cmd_buf.service->cmd_data.ul_dereg.nchan = nchan;

      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
             "wva_wcdma_deregister_ul_logical_channels(): BEFORE: lca=(%d), "
             "lcb=(%d), lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );

      rc  = wcdma_ext_audio_api( WCDMA_PUT_L2_UL_CMD, &ul_dereg_cmd_buf,
                                 sizeof( ul_dereg_cmd_buf ) );
      if( rc == WCDMA_STATUS_GOOD )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "WCDMA L2 UL De-registration successful. Number of WCDMA L2 "
               "uplink logical channels, nchan = (%d).", nchan );
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
               "wva_wcdma_deregister_ul_logical_channels(): AFTER: lca=(%d), "
               "lcb=(%d), lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );
      }
      else
      {
        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
               "WCDMA L2 UL De-registration failed, rc = (0x%08X). Number of "
               "WCDMA L2 uplink logical channels, nchan = (%d).", rc, nchan );
      }
#endif

    break;
  }

  return rc;
}

static uint32_t wva_wcdma_register_ul_logical_channels (
  wcdma_ivoice_event_set_logical_channels_t* chan_info,
  wva_modem_subs_object_t* subs_obj,
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  uint8_t nchan = 0;
  voice_amr_chan_state_t* chan_state;
  voice_amr_dsm_queue_t* ul_queue = NULL;
  wcdma_cmd_l2_ul_cmd_buffer_t ul_reg_cmd_buf;

  for( ;; )
  {
    chan_state = &session_obj->ul_chan_state;
    ul_queue = &session_obj->ul_queue;
  
#ifndef WINSIM

   /* Register logical channels if necessary,
    *
    * L2 Layer works on command buffer. Get the command packet using 
    * WCDMA_GET_L2_UL_CMD_BUFFER; Fill it with Command header 
    * (cmd_hdr.cmd_id) =  RLC_UL_REGISTER_SRVC_REQ  fill appropriate 
    * cmd_data.(ul_reg) members and dispatch it WCDMA_PUT_L2_UL_CMD */

   ul_reg_cmd_buf.service = NULL;
    rc  = wcdma_ext_audio_api( WCDMA_GET_L2_UL_CMD_BUFFER, &ul_reg_cmd_buf,
                               sizeof( ul_reg_cmd_buf ) );
    if( ( rc != WCDMA_STATUS_GOOD ) || ( ul_reg_cmd_buf.service == NULL ) )
     {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "Couldn't get WCMDA L2 uplink command buffer for reg. "
              "rc = (0x%08X)", rc );
       return APR_EFAILED;
    }
    
    /*<- command id for Register these channels */
    ul_reg_cmd_buf.service->cmd_hdr.cmd_id = RLC_UL_REGISTER_SRVC_REQ;
    ul_reg_cmd_buf.service->cmd_hdr.as_id = subs_obj->asid;
   
    /* Register class A channel if necessary */
    if ( ( chan_state->has_chan_a == TRUE ) && ( chan_info->lc_class_a > 0 ) )
    {
      chan_state->lca = chan_info->lc_class_a;
      ul_reg_cmd_buf.service->cmd_data.ul_reg.rlc_data[nchan].lc_id = chan_state->lca;
      ul_reg_cmd_buf.service->cmd_data.ul_reg.rlc_data[nchan].ul_wm_ptr = &ul_queue->wm_a;
      nchan++;
    }
   
    /* Register class B channel if necessary */
    if ( ( chan_state->has_chan_b == TRUE ) && ( chan_info->lc_class_b > 0 ) )
    {
      chan_state->lcb = chan_info->lc_class_b;
      ul_reg_cmd_buf.service->cmd_data.ul_reg.rlc_data[nchan].lc_id = chan_state->lcb;
      ul_reg_cmd_buf.service->cmd_data.ul_reg.rlc_data[nchan].ul_wm_ptr = &ul_queue->wm_b;
      nchan++;
    }
   
    /* Register class C channel if necessary */
    if ( ( chan_state->has_chan_c == TRUE ) && ( chan_info->lc_class_c > 0 ) )
    {
      chan_state->lcc = chan_info->lc_class_c;
      ul_reg_cmd_buf.service->cmd_data.ul_reg.rlc_data[nchan].lc_id = chan_state->lcc;
      ul_reg_cmd_buf.service->cmd_data.ul_reg.rlc_data[nchan].ul_wm_ptr = &ul_queue->wm_c;
      nchan++;
    }
   
    if( nchan > 0 )
    {
      ( void ) voice_dsm_amr_q_empty( &session_obj->ul_queue );
       MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "DSM UL queue empty during registration" );
    }

    ul_reg_cmd_buf.service->cmd_data.ul_dereg.nchan = nchan;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
           "wva_wcdma_register_ul_logical_channels(): BEFORE: lca=(%d), lcb=(%d), "
           "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );

    rc  = wcdma_ext_audio_api( WCDMA_PUT_L2_UL_CMD, &ul_reg_cmd_buf,
                               sizeof( ul_reg_cmd_buf ) );
    if( rc == WCDMA_STATUS_GOOD )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "WCDMA L2 UL Registration successful. Number of WCDMA L2 "
             "uplink logical channels, nchan = (%d).", nchan );
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
             "wva_wcdma_register_ul_logical_channels(): AFTER: lca=(%d),lcb=(%d), "
             "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "WCDMA L2 UL Registration failed, rc = (0x%08X). Number of "
             "WCDMA L2 uplink logical channels, nchan = (%d).", rc, nchan );
    }
#endif
    break;
  }

  return rc;
}


static uint32_t wva_wcdma_deregister_dl_logical_channels (
  wcdma_ivoice_event_set_logical_channels_t* chan_info,
  wva_modem_subs_object_t* subs_obj,
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  uint8_t nchan = 0;
  voice_amr_chan_state_t* chan_state;
  voice_amr_dsm_queue_t* dl_queue = NULL;
  wcdma_cmd_l2_dl_cmd_buffer_t dl_dereg_cmd_buf;

  for( ;; )
  {
    chan_state = &session_obj->dl_chan_state;
    dl_queue = &session_obj->dl_queue;

#ifndef WINSIM
    /* De-register logical channels if necessary
     *
     * L2 Layer works on command buffer, Get the command packet using 
     * WCDMA_GET_L2_DL_CMD_BUFFER, fill it with Command header 
     * (cmd_hdr.cmd_id) =  RLC_DL_DEREGISTER_SRVC_REQ and fill appropriate 
     * cmd_data.(ul_dereg) members and dispatch it WCDMA_PUT_L2_DL_CMD
     */

     dl_dereg_cmd_buf.service = NULL;
     rc  = wcdma_ext_audio_api( WCDMA_GET_L2_DL_CMD_BUFFER, &dl_dereg_cmd_buf,
                                sizeof( dl_dereg_cmd_buf ) );
     if( ( rc != WCDMA_STATUS_GOOD ) || ( dl_dereg_cmd_buf.service == NULL ) )
     {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "Couldn't get WCMDA L2 downlink command buffer for dereg. "
              "rc = (0x%08X)", rc );
       break;
     }

    /*<- command id for deregister these channels */
     dl_dereg_cmd_buf.service->cmd_hdr.cmd_id = RLC_DL_DEREGISTER_SRVC_REQ;
     dl_dereg_cmd_buf.service->cmd_hdr.as_id = subs_obj->asid;
    
    
     /* deregister class A channel if necessary */
     if ( ( chan_state->has_chan_a == FALSE ) || 
          ( chan_state->lca != chan_info->lc_class_a ) )
     {
       if ( chan_state->lca != 0 )
       {
         dl_dereg_cmd_buf.service->cmd_data.dl_dereg.rlc_id[nchan] = chan_state->lca;
         nchan++;
         chan_state->lca = 0;
       }
    
     }
    
     /* deregister class B channel if necessary */
     if ( ( chan_state->has_chan_b == FALSE ) || 
          ( chan_state->lcb != chan_info->lc_class_b ) )
     {
       if ( chan_state->lcb != 0 )
       {
        dl_dereg_cmd_buf.service->cmd_data.dl_dereg.rlc_id[nchan] = chan_state->lcb;
        nchan++;
        chan_state->lcb = 0;
       }
    
     }
    
     /* deregister class C channel if necessary */
     if ( ( chan_state->has_chan_c == FALSE ) || 
          ( chan_state->lcc != chan_info->lc_class_c ) )
     {
       if ( chan_state->lcc != 0 )
       {
         dl_dereg_cmd_buf.service->cmd_data.dl_dereg.rlc_id[nchan] = chan_state->lcc;
         nchan++;
         chan_state->lcc = 0;
       }
    
     }

     if( nchan > 0 )
     {
       ( void ) voice_dsm_amr_q_empty( &session_obj->dl_queue );
         MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "DSM DL queue empty during de-registration" );
     }

     MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
            "wva_wcdma_deregister_dl_logical_channels(): Before: lca=(%d), "
            "lcb=(%d), lcc=(%d)", chan_state->lca, chan_state->lcb,
            chan_state->lcc );

     dl_dereg_cmd_buf.service->cmd_data.dl_dereg.nchan = nchan;
     rc  = wcdma_ext_audio_api( WCDMA_PUT_L2_DL_CMD, &dl_dereg_cmd_buf,
                                sizeof( dl_dereg_cmd_buf ) );
     if( rc == WCDMA_STATUS_GOOD )
     {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "WCDMA L2 DL De-registration successful. Number of WCDMA L2 "
              "downlink logical channels, nchan = (%d).", nchan );
       MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, 
              "wva_wcdma_deregister_dl_logical_channels(): After: lca=(%d), "
              "lcb=(%d), lcc=(%d)", chan_state->lca, chan_state->lcb,
              chan_state->lcc );
     }
     else
     {
       MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
              "WCDMA L2 DL De-registration failed, rc = (0x%08X). Number of "
              "WCDMA L2 downlink logical channels, nchan = (%d).", rc, nchan );
     }
#endif

    break;
  }

  return rc;
}

static uint32_t wva_wcdma_register_dl_logical_channels (
  wcdma_ivoice_event_set_logical_channels_t* chan_info,
  wva_modem_subs_object_t* subs_obj,
  wva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  uint8_t nchan = 0;
  voice_amr_chan_state_t* chan_state;
  voice_amr_dsm_queue_t* dl_queue = NULL;
  wcdma_cmd_l2_dl_cmd_buffer_t dl_reg_cmd_buf;

  for( ;; )
  {
    chan_state = &session_obj->dl_chan_state;
    dl_queue = &session_obj->dl_queue;
#ifndef WINSIM

    dl_reg_cmd_buf.service = NULL;
    rc  = wcdma_ext_audio_api( WCDMA_GET_L2_DL_CMD_BUFFER, &dl_reg_cmd_buf,
                               sizeof( dl_reg_cmd_buf ) );
    if( ( rc != WCDMA_STATUS_GOOD ) || ( dl_reg_cmd_buf.service == NULL ) )
     {
       MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
              "Couldn't get WCMDA L2 downlink command buffer for reg. "
              "rc = (0x%08X)", rc );
       return APR_EFAILED;
    }
    
    /*<- command id for Register these channels */
    dl_reg_cmd_buf.service->cmd_hdr.cmd_id = RLC_DL_REGISTER_SRVC_REQ;
    dl_reg_cmd_buf.service->cmd_hdr.as_id = subs_obj->asid;
    
    /* Register class A channel if necessary */
    if ( ( chan_state->has_chan_a == TRUE ) && ( chan_info->lc_class_a > 0 ) )
    {
      chan_state->lca = chan_info->lc_class_a;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].lc_id = chan_state->lca;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].dl_wm_ptr = &dl_queue->wm_a;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].context= TRUE;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].rlc_post_rx_func_ptr_para
        = (void*)session_obj;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].rlc_post_rx_proc_func_ptr 
        = wva_downlink_channel_data_available;
      nchan++;
    }
    
    /* Register class B channel if necessary */
    if ( ( chan_state->has_chan_b == TRUE ) && ( chan_info->lc_class_b > 0 ) )
    {
      chan_state->lcb = chan_info->lc_class_b;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].lc_id = chan_state->lcb;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].dl_wm_ptr = &dl_queue->wm_b;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].context= TRUE;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].rlc_post_rx_func_ptr_para
        = (void*)session_obj;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].rlc_post_rx_proc_func_ptr 
        = wva_downlink_channel_data_available;
      nchan++;
    }
    
    /* Register class C channel if necessary */
    if ( ( chan_state->has_chan_c == TRUE ) && ( chan_info->lc_class_c > 0 ) )
    {
      chan_state->lcc = chan_info->lc_class_c;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].lc_id = chan_state->lcc;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].dl_wm_ptr = &dl_queue->wm_c;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].context= TRUE;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].rlc_post_rx_func_ptr_para
        = (void*)session_obj;
      dl_reg_cmd_buf.service->cmd_data.dl_reg.rlc_data[nchan].rlc_post_rx_proc_func_ptr 
        = wva_downlink_channel_data_available;
      nchan++;
    }
    
    if( nchan > 0 )
    {
      ( void ) voice_dsm_amr_q_empty( &session_obj->dl_queue );
         MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "DSM DL queue empty during registration" );
    }

    dl_reg_cmd_buf.service->cmd_data.dl_dereg.nchan = nchan;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "wva_wcdma_register_dl_logical_channels(): BEFORE: lca=(%d), lcb=(%d), "
           "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );

    rc  = wcdma_ext_audio_api( WCDMA_PUT_L2_DL_CMD, &dl_reg_cmd_buf,
                               sizeof( dl_reg_cmd_buf ) );
    if( rc == WCDMA_STATUS_GOOD )
    {
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "WCDMA L2 DL Registration successful. Number of WCDMA L2 "
            "downlink logical channels, nchan = (%d).", nchan );
     MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "wva_wcdma_register_dl_logical_channels(): AFTER: lca=(%d), lcb=(%d), "
            "lcc=(%d)", chan_state->lca, chan_state->lcb, chan_state->lcc );
    }
    else
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "WCDMA L2 DL Registration failed, rc = (0x%08X). Number of "
             "WCDMA L2 downlink logical channels, nchan = (%d).", rc, nchan );
    }
#endif

    break;
  }

  return rc;
}


/****************************************************************************
 * WVA CMDs/EVENTs HANDLING ROUTINES                                        *
 ****************************************************************************/

static uint32_t wva_process_vs_cmd_response_event( 
 wva_event_packet_t* event_pkt
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  wva_simple_job_object_t* simple_obj = NULL;
  vs_common_event_cmd_response_t* evt_params = NULL;

  for ( ;; )
  {
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( ( vs_common_event_cmd_response_t* ) event_pkt->params );
    if( evt_params == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "wva_process_vs_cmd_response_event(): cmd response event recieved for "
           "VS cmd_id=(0x%08x), client_context=(0x%08x), status=(0x%08X)",
           evt_params->cmd_id, evt_params->client_context, evt_params->status_id );

    simple_obj = ( wva_simple_job_object_t* ) evt_params->client_context;
    if ( simple_obj == NULL ) break;

    simple_obj->is_completed = TRUE;
    simple_obj->status = evt_params->status_id;

    break;
  }

  wva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t wva_process_vs_ready_event( 
 wva_gating_control_t* ctrl
)
{
  uint32_t rc = APR_EOK;
  wva_event_packet_t* event_pkt = NULL;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( wva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
    
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    subs_obj = session_obj->active_subs_obj;
    if( NULL != subs_obj )
    {
      /* Publish vocoder state for voice call on a subscription. */
      session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_READY;
      voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                             session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_WCDMA,
                             wva_map_vocoder_type_wcdma_to_vs( session_obj->vocoder_type ) );
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );
    
    session_obj->is_vs_ready = TRUE;

    /* Prime the Ready buffer with VS for UL packets. */
    rc = wva_vs_prime_read_buffer( session_obj );
    
    WVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  wva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t wva_process_vs_not_ready_event( 
 wva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  wva_event_packet_t* event_pkt = NULL;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  wva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( wva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
  
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    subs_obj = session_obj->active_subs_obj;
    if( NULL != subs_obj )
    {
      /* Publish vocoder state for voice call on a subscription.
       * This state shall be publshed only during device switch scenrio's.
       * During call end RELEASED is published.
       */
      session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_NOT_READY;
      voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                             session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_WCDMA,
                             wva_map_vocoder_type_wcdma_to_vs( session_obj->vocoder_type ) );
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );
    
    if ( ctrl->state == WVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG_1 ( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wva_process_vs_not_ready_event(): for session with vsid=(0x%08x) ", 
             session_obj->vsid );

      session_obj->is_vs_ready = FALSE;

      /* FLUSH BUFFERS when voice services are not ready for vocoder 
         packet exchnage. */
      rc = wva_create_simple_job_object( session_obj->header.handle,
             ( wva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj= &ctrl->rootjob_obj->simple_job;

      rc = wva_vs_flush_buffers( session_obj, (void*)simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
      }
      else
      {
        /* Restore the read buffer reference if not returned from VS. */
        session_obj->vs_read_buf = session_obj->primed_read_buf;
      }
    }

    break;
  }

  if ( session_obj != NULL )
  {
    WVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc == APR_EOK )
  {
   ( void ) wva_mem_free_object ( ( wva_object_t*) simple_obj );
   ( void ) wva_free_event_packet ( event_pkt );
  }

  return rc;
}

static uint32_t wva_process_vs_open_event ( 
 wva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  wva_event_packet_t* event_pkt = NULL;
  wva_session_object_t* session_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( wva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
    
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    rc = wva_vs_open_session( session_obj );

    WVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  wva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t wva_process_vs_close_event ( 
 wva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  wva_event_packet_t* event_pkt = NULL;
  wva_session_object_t* session_obj = NULL;
  wva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( wva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
    
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == WVA_GATING_CMD_STATE_EXECUTE )
    {

      rc = wva_create_simple_job_object( session_obj->header.handle,
             ( wva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj = &ctrl->rootjob_obj->simple_job;

      rc = wva_vs_close_session( session_obj, (void*) simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
      }
      else
      {
        session_obj->vs_handle = APR_NULL_V;
      }
    }

    break;
  }

  if ( session_obj != NULL )
  {
    WVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc != APR_EPENDING )
  {
    ( void ) wva_mem_free_object ( ( wva_object_t*) simple_obj );
    ( void ) wva_free_event_packet ( event_pkt );
    rc = APR_EOK;
  }

  return rc;
}


static uint32_t wva_process_vs_eamr_rate_change_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  vs_common_event_eamr_mode_t* evt_params = NULL;
  wcdma_ivoice_cmd_send_sample_rate_t sample_rate_cmd;

  for ( ;; )
  {
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( ( vs_common_event_eamr_mode_t* ) event_pkt->params );
    if( evt_params == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    if( session_obj->vocoder_type != WCDMA_IVOCODER_ID_AMR )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wva_process_vs_eamr_rate_change_event(): Not Applicable for "
             "vocoder_type=0x%08x", session_obj->vocoder_type );
      break;
    }

    switch ( evt_params->mode )
    {
      case VS_COMMON_EAMR_MODE_NARROWBAND:
        sample_rate_cmd.sample_rate = WVA_VOICE_SAMPLE_RATE_NB_V;
        break;

      case VS_COMMON_EAMR_MODE_WIDEBAND:
        sample_rate_cmd.sample_rate = WVA_VOICE_SAMPLE_RATE_WB_V;
        break;

      default:
        sample_rate_cmd.sample_rate = WVA_VOICE_SAMPLE_RATE_UNDEFINED_V;
        break;
    }

    if( ( evt_params->mode != VS_COMMON_EAMR_MODE_NARROWBAND ) ||
        ( evt_params->mode != VS_COMMON_EAMR_MODE_WIDEBAND ) )
    {
      subs_obj = session_obj->active_subs_obj;
      sample_rate_cmd.handle = subs_obj->wcdma_handle;
      sample_rate_cmd.vocoder_id = session_obj->vocoder_type;

#ifndef WINSIM
      rc = wcdma_ext_audio_api( WCDMA_VOICE_CMD_SEND_SAMPLE_RATE,
                                &sample_rate_cmd, sizeof( sample_rate_cmd ) );
#endif
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "wva_process_vs_eamr_rate_change_event(): for subs_obj asid=%d "
           "eamr mode=%d, sample_rate=%d", subs_obj->asid, evt_params->mode,
           sample_rate_cmd.sample_rate );
    break;
  }

  wva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t wva_process_vs_read_buf_available_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  voicelog_ipacket_cmd_commit_data_t log_cmd_param;

  for ( ;; )
  {
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    subs_obj = session_obj->active_subs_obj;
    if ( ( NULL != subs_obj ) &&
         ( VOICELOG_IEVENT_VOCODER_STATE_RUNNING != session_obj->vocoder_state ) )
    {
      /* Publish vocoder state for voice call on a subscription. */
      session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_RUNNING;
      voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                             session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_WCDMA,
                             wva_map_vocoder_type_wcdma_to_vs( session_obj->vocoder_type ) );
    }

    /* Read the vocoder buffer from VS has returned. */
    ( void ) wva_vs_read_buffer ( session_obj );
    if( ( APR_EOK != rc ) || ( NULL == session_obj->vs_read_buf ) )
    {
      WVA_REPORT_FATAL_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    /* Deliver the vocoder data to W-RLC via DSM queue. */
    ( void ) wva_deliver_ul_vocoder_packet( session_obj );

    { /* Log vocoder Packet Data. */
      rc = voicelog_get_frame_header( session_obj->vs_read_buf->media_id,
                                      session_obj->vs_read_buf->frame_info,
                                      &log_cmd_param.frame_header );

      if( APR_EOK != rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_process_vs_read_buf_available_event(): Invalid Frame header " );
        rc = APR_EOK;
        break;
      }

      log_cmd_param.version = VOICELOG_IPACKET_VOCODER_DATA_VERSION_V;
      log_cmd_param.log_code = VOICELOG_IPACKET_CODE_UL_VOCODER_PACKET;
      log_cmd_param.vsid = session_obj->vsid;
      log_cmd_param.network_id = VOICELOG_ICOMMON_NETWORK_ID_WCDMA;
      log_cmd_param.timestamp = session_obj->vs_read_buf->timestamp;
      log_cmd_param.tap_point_id = VOICELOG_IPACKET_TAP_POINT_ID_VOICE_ADAPTER;
      log_cmd_param.media_id = session_obj->vs_read_buf->media_id;
      log_cmd_param.frame = session_obj->vs_read_buf->frame;
      log_cmd_param.frame_size = session_obj->vs_read_buf->size;
      log_cmd_param.data_extension = NULL;
      
      /* Log data. */
      ( void ) voicelog_call ( VOICELOG_IPACKET_CMD_COMMIT_DATA,
                               ( void* )&log_cmd_param, sizeof( log_cmd_param ) );
    }

    /* Prime the read buffer again to recieve next UL vs buffer. */
    ( void ) wva_vs_prime_read_buffer( session_obj );

    break;
  }

  ( void ) wva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t wva_process_vs_write_buf_returned_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  vs_voc_event_write_buffer_returned_t* evt_params = NULL;

  for ( ;; )
  {
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( vs_voc_event_write_buffer_returned_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_vs_write_buf_returned_event(): evt_paramsr is NULL" );
      break;
    }

    if ( evt_params->buffer == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_vs_write_buf_returned_event(): ret_buffer is NULL" );
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );
    
    /* Indicates that the vocoder buffer written to voice services has been 
     * successfully rendered to DSP.
     * Write buffer returned for next downlink vocoder packet.
     */
    session_obj->vs_write_buf = evt_params->buffer;

    WVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  wva_free_event_packet ( event_pkt );

  return rc;
}



static uint32_t wva_process_wcdma_scr_mode_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  wcdma_ivoice_event_request_scr_mode_t* evt_params = NULL;

  for ( ;; )
  {
    subs_obj = ( wva_modem_subs_object_t* ) event_pkt->session_context;
    if ( subs_obj == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_scr_mode_event(): subs_obj is NULL" );
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( wcdma_ivoice_event_request_scr_mode_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_scr_mode_event(): evt_params are NULL" );
      break;
    }

    session_obj = subs_obj->session_obj;
    if ( session_obj == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_scr_mode_event(): session_obj are NULL" );
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    session_obj->dtx_mode = evt_params->enable_flag;
    ( void ) wva_set_voc_dtx_mode ( session_obj );

    WVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  ( void ) wva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t wva_process_wcdma_select_owner_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    subs_obj = ( wva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = subs_obj->session_obj;
    if ( session_obj == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_select_owner_event(): session_obj are NULL" );
      break;
    }


    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    /* FOR IRAT-HO Scenrios,request voice agent for resource grant. */
    if ( ( session_obj->is_resource_granted == FALSE ) &&
         ( session_obj->va_wva_event_cb != NULL ) )
    {
      session_obj->va_wva_event_cb( session_obj->va_session_context,
                                    WVA_IRESOURCE_EVENT_REQUEST, NULL, 0 );
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "wva_process_wcdma_select_owner_event() - Resource requested " );
    }

    WVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  wva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t wva_process_wcdma_codec_mode_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  wcdma_ivoice_event_request_codec_mode_t* evt_params = NULL;

  for ( ;; )
  {
    subs_obj = ( wva_modem_subs_object_t* ) event_pkt->session_context;
    if ( subs_obj == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_codec_mode_event(): subs_obj is NULL" );
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( wcdma_ivoice_event_request_codec_mode_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_codec_mode_event(): evt_params are NULL" );
      break;
    }

    session_obj = subs_obj->session_obj;
    if ( session_obj == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_codec_mode_event(): session_obj are NULL" );
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    session_obj->codec_mode = evt_params->codec_mode;
    ( void ) wva_set_voc_codec_mode ( session_obj );

    WVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  ( void ) wva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t wva_process_wcdma_l1_vfr_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  int8_t tx_timetick_diff = 0;
  uint64_t tx_timing_offset_us = 0;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  wcdma_ivoicel1_event_vfr_notification_t* evt_params = NULL;

  for ( ;; )
  {
    subs_obj = ( wva_modem_subs_object_t* ) event_pkt->session_context;
    if ( subs_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( wcdma_ivoicel1_event_vfr_notification_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_l1_vfr_event(): evt_params are NULL" );
      break;
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "wva_process_wcdma_l1_vfr_event(): vfr_state=(%d), cfn_value=(%d), "
           "stmr_tick_count=(%d)", evt_params->vfr_state, evt_params->cfn_value,
           evt_params->stmr_tick_count );

    /* Ignore the Vfr event with even cfn_value, as per design WCDMA picks up 
     * the UL packet from DSM queue only during vfr events with odd cfn_value.
     */
    if ( ( evt_params->cfn_value % 2 ) == 0 ) break;

    /* Calculate the timing offset in stmr timer tick count.*/
    tx_timetick_diff = ( int8_t ) ( WVA_WCDMA_READS_UL_PACKET_AT_STMR_TICK_COUNT_V  - 
                                    evt_params->stmr_tick_count );

    /* Converting ticks to microsec. 150 ticks equals 10 milli-sec, or 150 ticks 
     * equals 10000 micro-sec or 1 tick equals 1000/15 or 200/3 microsec or 
     * 66.666 microsec .
     */
    tx_timing_offset_us = ( int64_t )( ( 1.0 * tx_timetick_diff ) * 66.66 );

    tx_timing_offset_us = tx_timing_offset_us - ( int64_t )WVA_BUF_TIME_FOR_PUTTING_UL_PACKET_IN_DSM_V;
    
    if ( tx_timing_offset_us <= 0 )
    {
      if ( ( ( -1 ) * tx_timing_offset_us ) >= ( int64_t )( WVA_VOICE_FRAME_SIZE_US_V ) )
      {
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                "mva_deliver_wcdma_tx_frame(): cfn = (0x%08X) is not valid ",
                evt_params->cfn_value );
         return APR_EFAILED;
      }
      tx_timing_offset_us = tx_timing_offset_us + WVA_VOICE_FRAME_SIZE_US_V;
    }
    
    /* Convert time in nano-sec, apr_timer expects time in nano-sec. */
    session_obj = subs_obj->session_obj;
    rc = apr_timer_start( session_obj->sfg_timer, (tx_timing_offset_us*1000) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_process_wcdma_l1_vfr_event(): Failed to start timer, "
             "rc = (0x%08x)", rc );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "wva_process_wcdma_l1_vfr_event(): Start timer, to be fired in "
             "(%d)us", tx_timing_offset_us );
    }

    break;
  }

  ( void ) wva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t wva_process_wcdma_set_logical_channels_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  uint32_t chan_class;
  voice_amr_chan_state_t* chan_state = NULL;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  wcdma_ivoice_event_set_logical_channels_t* evt_params = NULL;

  for ( ;; )
  {
    subs_obj = ( wva_modem_subs_object_t* ) event_pkt->session_context;
    if ( subs_obj == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_set_logical_channels_event(): subs_obj is NULL" );
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( wcdma_ivoice_event_set_logical_channels_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_set_logical_channels_event(): evt_params are NULL" );
      break;
    }

    session_obj = subs_obj->session_obj;
    if ( session_obj == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_set_logical_channels_event(): session_obj are NULL" );
      break;
    }

    switch ( evt_params->direction )
    {
     /* Register/Dereg Downlink logical channels. */
     case FALSE:
      {
        /* Update the channle state for DSM queue. */
        chan_class = evt_params->class_type;
        chan_state = &session_obj->dl_chan_state;
        rc = wva_update_chan_state( chan_state, chan_class );
        if( rc ) break;

        ( void ) wva_wcdma_deregister_dl_logical_channels( evt_params, subs_obj,
                                                           session_obj );
        ( void ) wva_wcdma_register_dl_logical_channels( evt_params, subs_obj,
                                                         session_obj );
      }
      break;
     
     /* Register/Dereg Downlink logical channels. */
     case TRUE:
      {
        /* Update the channle state for DSM queue. */
        chan_class = evt_params->class_type;
        chan_state = &session_obj->ul_chan_state;
        rc = wva_update_chan_state( chan_state, chan_class );
        if( rc ) break;

        ( void ) wva_wcdma_deregister_ul_logical_channels( evt_params, subs_obj,
                                                           session_obj );
        ( void ) wva_wcdma_register_ul_logical_channels( evt_params, subs_obj,
                                                         session_obj );
      }
      break;
    }

    break;
  }

  ( void ) wva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t wva_process_wcdma_dl_buf_available_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;

  for ( ;; )
  {
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_dl_buf_available_event(): session_obj is NULL" );
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    if( session_obj->vs_write_buf == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_dl_buf_available_event(): vs_write_buf is NULL" );
      break;
    }

    if( session_obj->is_vs_ready == FALSE )
    {
      ( void ) voice_dsm_amr_q_empty( &session_obj->dl_queue );
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_wcdma_dl_buf_available_event(): VS not ready dropping "
           "DL vocoder channel data" );
      break;
    }


    /* Get the vocoder data from the DSM queue. */
    rc = wva_get_dl_vocoder_packet( session_obj );
    if ( rc ) break;
    
    /* Pass the vocoder buffer to VS from rendering. */
    rc = wva_vs_write_buffer( session_obj );

    break;
  }

  ( void ) wva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t wva_process_send_silence_frame_event( 
 wva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  vs_voc_buffer_t* vs_buffer = NULL;
  vs_vocamr_frame_info_t* vocamr_info = NULL;
  vs_vocamrwb_frame_info_t* vocamrwb_info = NULL;
  uint32_t vs_media_id = WVA_VOCODER_ID_UNDEFINED_V;
  uint32_t codec_mode = WVA_CODEC_MODE_UNDEFINED;
  voicelog_ipacket_cmd_commit_data_t log_cmd_param;

  for ( ;; )
  {
    session_obj = ( wva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    if( ( session_obj->is_vs_ready != FALSE ) ||
        ( session_obj->vs_read_buf == NULL ) )
    {
      break;
    }

    vs_media_id = wva_map_vocoder_type_wcdma_to_vs( session_obj->vocoder_type );
    vs_buffer = session_obj->vs_read_buf;
    vs_buffer->media_id = vs_media_id;

    if( vs_media_id == VS_COMMON_MEDIA_ID_AMR )
    {
      ( void ) wva_map_vocamr_codec_mode_wcdma_to_vs( session_obj->codec_mode,
                                                      &codec_mode );
       vocamr_info = ( vs_vocamr_frame_info_t* ) vs_buffer->frame_info;
       vocamr_info->frame_type = VS_VOCAMR_FRAME_TYPE_SPEECH_GOOD;
       vocamr_info->codec_mode = ( vs_vocamr_codec_mode_t ) codec_mode;
    }
    else if( vs_media_id == VS_COMMON_MEDIA_ID_AMRWB )
    {
      ( void ) wva_map_vocamrwb_codec_mode_wcdma_to_vs( session_obj->codec_mode,
                                                        &codec_mode );
      vocamrwb_info = ( vs_vocamrwb_frame_info_t* ) vs_buffer->frame_info;
      vocamrwb_info->frame_type = VS_VOCAMRWB_FRAME_TYPE_SPEECH_GOOD;
      vocamrwb_info->codec_mode = ( vs_vocamrwb_codec_mode_t ) codec_mode;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_send_silence_frame_event(): Invalid vocoder type!!!" );
      break;
    }

    /* Get the silence frame vectors from voice utils. */
    rc = voice_util_get_homing_frame( vs_media_id, vs_buffer->frame_info,
                                      vs_buffer->frame, &vs_buffer->size );
    if ( rc ) break;

    /* Deliver the vocoder data to W-RLC via DSM queue. */
    ( void ) wva_deliver_ul_vocoder_packet( session_obj );

    { /* Log vocoder Packet Data. */
      rc = voicelog_get_frame_header( session_obj->vs_read_buf->media_id,
                                      session_obj->vs_read_buf->frame_info,
                                      &log_cmd_param.frame_header );
      
      if( APR_EOK != rc )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_process_send_silence_frame_event(): Invalid Frame header " );
        rc = APR_EOK;
        break;
      }

      log_cmd_param.version = VOICELOG_IPACKET_VOCODER_DATA_VERSION_V;
      log_cmd_param.log_code = VOICELOG_IPACKET_CODE_UL_VOCODER_PACKET;
      log_cmd_param.vsid = session_obj->vsid;
      log_cmd_param.network_id = VOICELOG_ICOMMON_NETWORK_ID_WCDMA;
      log_cmd_param.timestamp = session_obj->vs_read_buf->timestamp;
      log_cmd_param.tap_point_id = VOICELOG_IPACKET_TAP_POINT_ID_VOICE_ADAPTER;
      log_cmd_param.media_id =  session_obj->vs_read_buf->media_id;
      log_cmd_param.frame = session_obj->vs_read_buf->frame;
      log_cmd_param.frame_size = session_obj->vs_read_buf->size;
      log_cmd_param.data_extension = NULL;
      
      /* Log data. */
      ( void ) voicelog_call ( VOICELOG_IPACKET_CMD_COMMIT_DATA,
                               ( void* )&log_cmd_param, sizeof( log_cmd_param ) );
    }

    break;
  }

  if( session_obj != NULL )
  {
    WVA_RELEASE_LOCK( session_obj->data_lock );
  }

  ( void ) wva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t wva_process_wcdma_vocoder_start_event( 
 wva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  wva_event_packet_t* event_pkt = NULL;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  wcdma_ivoice_event_request_start_t* evt_params = NULL;
  wva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( wva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      rc = APR_EUNEXPECTED;
      WVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    evt_params = ( wcdma_ivoice_event_request_start_t* ) event_pkt->params;
    if( evt_params == NULL )
    { 
      rc = APR_EUNEXPECTED;
      WVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    subs_obj= ( wva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      WVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    session_obj = subs_obj->session_obj;
    if( session_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      WVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == WVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "wva_process_wcdma_vocoder_start_event() - configured vocoder "
             "id = (0x%08x), requested vocoder id = (0x%08x)",
             session_obj->vocoder_type, evt_params->vocoder_id );

      if ( subs_obj->is_wcdma_ready == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wva_process_wcdma_vocoder_start_event() - WCDMA already requested");
        break;
      }

      if ( evt_params->vocoder_id != WCDMA_IVOCODER_ID_NONE )
      {
        /* During Inter-frequency HHO scenrios, reuse the vocoder_type 
           set initially by WRRC. */
        session_obj->vocoder_type = evt_params->vocoder_id;
      }

      /* Vaildate the vocoder_type and check if the Logical channel are 
         configured to adapter by WRRC, this is to avoid multiple start/stop 
         from WL1 during add/drop state machine. */
      if ( ( session_obj->vocoder_type == WVA_VOCODER_ID_UNDEFINED_V ) ||
           ( session_obj->ul_chan_state.has_chan_a == FALSE ) ||
           ( session_obj->dl_chan_state.has_chan_a == FALSE ) )
      {
         MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                "wva_process_wcdma_vocoder_start_event() - Invalid vocoder "
                "type = (0x%08x) configurred, ul_dsm_state=%d, dl_dsm_state=%d",
                session_obj->vocoder_type, session_obj->ul_chan_state.has_chan_a,
                session_obj->dl_chan_state.has_chan_a );
         break;
      }

      /* Update the WCDMA state. */
      subs_obj->is_wcdma_ready = TRUE;
      session_obj->active_subs_obj = subs_obj;

      /* Update Vocoder Session Number for vocoder logging purpose. */
      ( void ) voicelog_session_update ( session_obj->vsid,
                                         VOICELOG_INFO_ENUM_VOCODER_SESSION_NUMBER);

      if ( ( session_obj->is_resource_granted == FALSE ) &&
           ( session_obj->va_wva_event_cb != NULL ) )
      {
        session_obj->va_wva_event_cb( session_obj->va_session_context,
                                      WVA_IRESOURCE_EVENT_REQUEST, NULL, 0 );

        /* Publish vocoder state for voice call on a subscription. */
        session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_REQUESTED;
        voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                               session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_WCDMA,
                               wva_map_vocoder_type_wcdma_to_vs( session_obj->vocoder_type ) );
        break;
      }

      /* Set cached vocoder properties. */
      ( void ) wva_set_voc_dtx_mode ( session_obj );
      ( void ) wva_set_voc_codec_mode ( session_obj );
      
      /* Create the Simple job object to track VS_ENABLE. */
      rc = wva_create_simple_job_object( session_obj->header.handle,
             ( wva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj = &ctrl->rootjob_obj->simple_job;
      
      rc = wva_vs_enable_vocoder( session_obj, simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }

      ( void ) wva_wcdma_start_session( subs_obj );
      ( void ) wva_wcdma_set_vfr_notification( subs_obj );
    }

    break;
  }

  /* If the return code is not APR_EPENDING it confirm that nothing is left for
   * hence memory associated to CMD packet, EVENT packet and JOB object created
   * shall be relesed. Control shall not return to this handler for a perticular
   * Insance of event. */
  if ( APR_EPENDING != rc ) 
  {
    rc = APR_EOK;

    /* Free CMD/EVT packet memory. */
    ( void ) wva_mem_free_object ( ( wva_object_t*) simple_obj );
    ( void ) wva_free_event_packet ( event_pkt );
  }

  if ( session_obj != NULL )
  {
    WVA_RELEASE_LOCK( session_obj->data_lock );
  }

  return rc;
}


static uint32_t wva_process_wcdma_vocoder_stop_event( 
 wva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  wva_event_packet_t* event_pkt = NULL;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  wva_simple_job_object_t* simple_obj = NULL;
  wva_icommon_cmd_set_asid_vsid_mapping_t wva_map_cmd;

  for ( ;; )
  {
    event_pkt = ( wva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      rc = APR_EUNEXPECTED;
      WVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    subs_obj = ( wva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      WVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    session_obj = subs_obj->session_obj;
    if( session_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      WVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == WVA_GATING_CMD_STATE_EXECUTE )
    {
      /* If the WRRC de-registeres the logical channles, update the session
         vocoder type as undefined, to ignore multiple start/stopn from WL1
         during add/drop state machine. */
      if ( ( session_obj->ul_chan_state.has_chan_a == FALSE ) &&
           ( session_obj->dl_chan_state.has_chan_a == FALSE ) )
      {
        session_obj->vocoder_type = WVA_VOCODER_ID_UNDEFINED_V;
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "wva_process_wcdma_vocoder_stop_event(): Invalidate configured "
               "type = (0x%08x), ul_dsm_state=%d, dl_dsm_state=%d",
               session_obj->vocoder_type, session_obj->ul_chan_state.has_chan_a,
               session_obj->dl_chan_state.has_chan_a );
      }

      if ( FALSE == subs_obj->is_wcdma_ready )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wcdma_vocoder_stop_event(PROCESS): Vocoder not started yet" );
        break;
      }

      /* Update the WCDMA state. */
      subs_obj->is_wcdma_ready = FALSE;
      /* Invalidate the active subscrition object. */
      session_obj->active_subs_obj = NULL;

      if ( TRUE == session_obj->is_resource_granted )
      {
        /* Create the Simple job object to track CVD setup. */
        rc = wva_create_simple_job_object( session_obj->header.handle,
               ( wva_simple_job_object_t** ) &ctrl->rootjob_obj );
        simple_obj = &ctrl->rootjob_obj->simple_job;

        rc = wva_vs_disable_vocoder( session_obj, simple_obj );
      }
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }
    }

    break;
  }

  if ( APR_EOK == rc )
  {
    /* Send voice resource released event to voice agent.
     * "REQUEST_STOP sent by W-RRC without sending REQUEST_START" 
     * This can happen if W-RRC called SELECT_OWNER during interRAT 
     * handover start. However the handover failed afterwards. So, W-RRC 
     * did not call REQUEST_START, instead directly called REQUEST_STOP 
     * to indicate WVA that it no longer required vocoder. 
     */
    if ( session_obj->va_wva_event_cb != NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wcdma_vocoder_stop_event(PROCESS) - RELEASED event sent to VA. "
             "grant_status=(%d)", session_obj->is_resource_granted );
      session_obj->is_resource_granted = FALSE;
      session_obj->va_wva_event_cb( session_obj->va_session_context, 
                                    WVA_IRESOURCE_EVENT_RELEASED, NULL, 0 );
    }

    /* Queue a cmd if a ASID-VSID mapping is waiting to be processed. */
    if ( WVA_VSID_UNDEFINED_V != subs_obj->pending_vsid )
    {
      wva_map_cmd.asid = subs_obj->asid;
      wva_map_cmd.vsid = subs_obj->pending_vsid;
      subs_obj->pending_vsid = WVA_VSID_UNDEFINED_V;
      wva_prepare_and_dispatch_cmd_packet ( WVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING, 
                                            (void*)&wva_map_cmd,  sizeof( wva_map_cmd ) );
    }

    /* Publish vocoder state for voice call on a subscription. */
    if(  VOICELOG_IEVENT_VOCODER_STATE_RELEASED != session_obj->vocoder_state )
    {
      /* Publish vocoder state for voice call on a subscription. */
      session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_RELEASED;
      voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                             session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_WCDMA,
                             wva_map_vocoder_type_wcdma_to_vs( session_obj->vocoder_type ) );
    }
  }

  /* If the return code is not APR_EPENDING it confirm that nothing is left for
   * hence memory associated to CMD packet, EVENT packet and JOB object created
   * shall be relesed. Control shall not return to this handler for a perticular
   * Insance of event. */
  if ( APR_EPENDING != rc ) 
  {
    rc = APR_EOK;
    /* Send STOP and VFR Disable notfication to WCDMA. */
    ( void ) wva_wcdma_stop_session ( subs_obj );
    ( void ) wva_wcdma_set_vfr_notification ( subs_obj );

    /* Free CMD/EVT packet memory. */
    ( void ) wva_mem_free_object ( ( wva_object_t*) simple_obj );
    ( void ) wva_free_event_packet ( event_pkt );
  }

  if ( session_obj != NULL )
  {
    WVA_RELEASE_LOCK( session_obj->data_lock );
  }

  return rc;
}


static uint32_t wva_process_resource_grant_cmd( 
 wva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  wva_cmd_packet_t* cmd_pkt = NULL;
  wva_session_object_t* session_obj = NULL;
  wva_modem_subs_object_t* subs_obj = NULL;
  wva_iresource_cmd_grant_t* cmd_params = NULL;
  wva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    cmd_pkt = ( wva_cmd_packet_t* ) ctrl->packet;
    if( cmd_pkt == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    cmd_params = ( wva_iresource_cmd_grant_t* ) cmd_pkt->params;
    if( cmd_params == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = wva_get_object ( cmd_params->handle, ( wva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_process_resource_grant_cmd(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == WVA_GATING_CMD_STATE_EXECUTE )
    {
      session_obj->is_resource_granted = TRUE;
      subs_obj = session_obj->active_subs_obj;
      if ( ( subs_obj == NULL ) || ( subs_obj->is_wcdma_ready == FALSE ) )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wva_process_resource_grant_cmd() - Traffic start request not "
             "available from WCDMA" );
        break;
      }

      /* Publish vocoder state for voice call on a subscription. */
      session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_GRANTED;
      voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                             session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_WCDMA,
                             wva_map_vocoder_type_wcdma_to_vs( session_obj->vocoder_type ) );

      /* Set cached vocoder properties. */
      ( void ) wva_set_voc_dtx_mode ( session_obj );
      ( void ) wva_set_voc_codec_mode ( session_obj );

      /* Create the Simple job object to track VS_ENABLE. */
      rc = wva_create_simple_job_object( session_obj->header.handle,
             ( wva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj = &ctrl->rootjob_obj->simple_job;

      rc = wva_vs_enable_vocoder( session_obj, simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }

      ( void ) wva_wcdma_start_session( session_obj->active_subs_obj );
      ( void ) wva_wcdma_set_vfr_notification( session_obj->active_subs_obj );
    }

    break;
  }

  if ( session_obj != NULL )
  {
    WVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc == APR_EOK )
  {
    ( void ) wva_mem_free_object ( ( wva_object_t*) simple_obj );
    ( void ) wva_free_cmd_packet ( cmd_pkt );
  }

  return rc;
}


static uint32_t wva_process_resource_revoke_cmd( 
 wva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  wva_cmd_packet_t* cmd_pkt = NULL;
  wva_session_object_t* session_obj = NULL;
  wva_iresource_cmd_grant_t* cmd_params = NULL;
  wva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    cmd_pkt = ( wva_cmd_packet_t* ) ctrl->packet;
    if( cmd_pkt == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    cmd_params = ( wva_iresource_cmd_grant_t* ) cmd_pkt->params;
    if( cmd_params == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = wva_get_object ( cmd_params->handle, ( wva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_process_resource_revoke_cmd(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    WVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == WVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "wva_process_resource_revoke_cmd() - vocoder revoked" );

      /* Create the Simple job object to track VS_DISABLE. */
      rc = wva_create_simple_job_object( session_obj->header.handle,
             ( wva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj= &ctrl->rootjob_obj->simple_job;

      rc = wva_vs_disable_vocoder( session_obj, simple_obj );
    }
    else
    {
      simple_obj= &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }

      /* Send a vocoder release event to voice agent. */
      if( session_obj->va_wva_event_cb!= NULL )
      {
        session_obj->is_resource_granted = FALSE;
        session_obj->va_wva_event_cb( session_obj->va_session_context,
                                      WVA_IRESOURCE_EVENT_RELEASED, NULL, 0 );
      }
    }

    break;
  }

  if ( session_obj != NULL )
  {
    WVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc == APR_EOK ) 
  {
    ( void ) wva_mem_free_object ( ( wva_object_t*) simple_obj );
    ( void ) wva_free_cmd_packet ( cmd_pkt );
  }

  return rc;
}

static uint32_t wva_process_set_asid_vsid_mapping_cmd ( 
  wva_cmd_packet_t* cmd_pkt 
)
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  wva_modem_subs_object_t* subs_obj = NULL;
  wva_session_object_t* session_obj = NULL;
  wva_icommon_cmd_set_asid_vsid_mapping_t* cmd_params = NULL;

  for ( ;; )
  {
    if( cmd_pkt == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    cmd_params = ( wva_icommon_cmd_set_asid_vsid_mapping_t* ) cmd_pkt->params;
    if( cmd_params == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    WVA_ACQUIRE_LOCK( wva_global_lock );

    subs_obj = wva_subs_obj_list[cmd_params->asid];
    if( subs_obj->vsid == cmd_params->vsid )
    {
      subs_obj->pending_vsid = WVA_VSID_UNDEFINED_V;
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "wva_process_set_asid_vsid_mapping_cmd(): no change to mapping" );
      WVA_RELEASE_LOCK ( wva_global_lock );
      break;
    }

    if( TRUE == subs_obj->is_wcdma_ready ) 
    {
      /* Store the VSID and apply the mapping at the end of the current voice call. */
      subs_obj->pending_vsid = cmd_params->vsid;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "wva_process_set_asid_vsid_mapping_cmd(): call in-progress"
           "new mapping is stored in pending-set: ASID=(%d) VSID=(0X%08X)",
           cmd_params->asid, cmd_params->vsid );
      WVA_RELEASE_LOCK ( wva_global_lock );
      break;
    }
    
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "wva_process_set_asid_vsid_mapping_cmd(): ASID=(%d) VSID=(0X%08X)",
           cmd_params->asid, cmd_params->vsid );

    

    /* Update the ASID-VSID mapping and the session object as per the VSID. */
    subs_obj->vsid = cmd_params->vsid;
    subs_obj->session_obj = NULL;

    for( index = 0; index < WVA_MAX_NUM_OF_SESSIONS_V; ++index )
    {
      session_obj = wva_session_obj_list[index];

      if( session_obj->vsid == subs_obj->vsid )
      {
        subs_obj->session_obj = session_obj;
        break;
      }
      else
      {
        session_obj = NULL;
      }
    }

    if( session_obj != NULL )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "wva_process_set_asid_vsid_mapping_cmd() - asid=%d mapped to "
             "session_obj=(0x%08x) with vsid=(0x%08x)", subs_obj->asid,
             subs_obj->session_obj, subs_obj->vsid );
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_process_set_asid_vsid_mapping_cmd(): Cannot find WVA session" );
    }

    WVA_RELEASE_LOCK ( wva_global_lock );

    break;
  }

  ( void ) wva_free_cmd_packet ( cmd_pkt );

  return rc;
}

/****************************************************************************
 * NONGATING REQUEST( CMDs/EVENTs ) PROCESSING FUNCTIONS
 ****************************************************************************/

static void wva_task_process_nongating_work_items ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t request_id = 0;
  wva_work_item_packet_type_t pkt_type;
  wva_work_item_t* work_item = NULL;
  void* packet = NULL;

  while( apr_list_remove_head( &wva_nongating_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) ) == APR_EOK )
  {
    pkt_type = work_item->pkt_type;
    packet = work_item->packet;
    
    if ( pkt_type == WVA_WORK_ITEM_PKT_TYPE_EVENT )
    {
      request_id = ( ( wva_event_packet_t* ) packet )->event_id ;
    }
    else if ( pkt_type ==  WVA_WORK_ITEM_PKT_TYPE_CMD )
    {
      request_id = ( ( wva_cmd_packet_t* ) packet )->cmd_id;
    }
    else
    {
      WVA_PANIC_ON_ERROR ( APR_EUNEXPECTED );
    }

    /* Add back to vs_free_work_pkt_q. */
    work_item->pkt_type = WVA_WORK_ITEM_PKT_TYPE_NONE;
    work_item->packet = NULL;
    ( void ) apr_list_add_tail( &wva_free_work_pkt_q, &work_item->link );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "wva_task_process_nongating_work_items(): Processing "
           "request_id=(0X%08X)", request_id );

    switch( request_id )
    {
     /**
      * Handling routine for nongating work-items should take of release the
      * memory allocated for the CMD/EVENT packets.
      */
     case WCDMA_IVOICE_EVENT_REQUEST_CODEC_MODE:
       rc = wva_process_wcdma_codec_mode_event( ( wva_event_packet_t* ) packet );
       break;

     case WCDMA_IVOICE_EVENT_REQUEST_SCR_MODE:
       rc = wva_process_wcdma_scr_mode_event( ( wva_event_packet_t* ) packet );
       break;

     case WCDMA_IVOICE_EVENT_SELECT_OWNER:
       rc = wva_process_wcdma_select_owner_event( ( wva_event_packet_t* ) packet );
       break;

     case WCDMA_IVOICE_EVENT_SET_LOGICAL_CHANNELS:
       rc = wva_process_wcdma_set_logical_channels_event( ( wva_event_packet_t* ) packet );
       break;

     case WCDMA_IVOICEL1_EVENT_VFR_NOTIFICATION:
       rc = wva_process_wcdma_l1_vfr_event( ( wva_event_packet_t* ) packet );
       break;

     case WVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE:
       rc = wva_process_wcdma_dl_buf_available_event( ( wva_event_packet_t* ) packet );
       break;

     case WVA_INTERNAL_EVENT_SEND_SILENCE_FRAME:
       rc = wva_process_send_silence_frame_event( ( wva_event_packet_t* ) packet );
       break;

     case WVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING:
       rc = wva_process_set_asid_vsid_mapping_cmd( ( wva_cmd_packet_t* ) packet  );
       break;

     case VS_COMMON_EVENT_CMD_RESPONSE:
       rc = wva_process_vs_cmd_response_event( ( wva_event_packet_t* ) packet );
       break;

     case VS_COMMOM_EVENT_EAMR_MODE_CHANGE:
       rc = wva_process_vs_eamr_rate_change_event( ( wva_event_packet_t* ) packet );
       break;

     case VS_VOC_EVENT_READ_AVAILABLE:
       rc = wva_process_vs_read_buf_available_event( ( wva_event_packet_t* ) packet );
       break;

     case VS_VOC_EVENT_WRITE_BUFFER_RETURNED:
       rc = wva_process_vs_write_buf_returned_event( ( wva_event_packet_t* ) packet );
       break;

     default:
       /* Add remaining work items to the gating work queue. */
       rc = wva_queue_work_packet ( WVA_WORK_ITEM_QUEUE_TYPE_GATING,
                                    pkt_type, packet );
       if ( rc )
       {
         if ( pkt_type == WVA_WORK_ITEM_PKT_TYPE_CMD )
         {
           wva_free_cmd_packet( ( wva_cmd_packet_t* ) packet );
         }
         else
         {
          wva_free_event_packet( ( wva_event_packet_t* ) packet );
         }

       }
       break;
    }
  }

  return;
}

/****************************************************************************
 * GATING REQUEST( CMDs/EVENTs ) PROCESSING FUNCTIONS
 ****************************************************************************/
 
static void wva_task_process_gating_work_items ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t request_id = 0;
  wva_work_item_t* work_item;
  wva_cmd_packet_t* cmd_pkt;
  wva_event_packet_t* event_pkt;
  wva_gating_control_t* ctrl = &wva_gating_work_pkt_q;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
     case WVA_GATING_CMD_STATE_FETCH:
       {
          /* Fetch the next gating command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc ) return;

          if ( ( work_item->packet == NULL ) ||
               ( work_item->pkt_type == WVA_WORK_ITEM_PKT_TYPE_NONE ) )
          {
            WVA_PANIC_ON_ERROR ( APR_EUNEXPECTED );
          }

          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "wva_task_process_gating_work_items(): "
               "VS_GATING_CMD_STATE_ENUM_FETCH" );

          ctrl->packet = work_item->packet;
          ctrl->pkt_type = work_item->pkt_type;
          ctrl->state = WVA_GATING_CMD_STATE_EXECUTE;

          /* Add the vs_work_item_t back to vs_free_work_pkt_q. */
          work_item->packet = NULL;
          work_item->pkt_type = WVA_WORK_ITEM_PKT_TYPE_NONE;
          ( void ) apr_list_add_tail( &wva_free_work_pkt_q, &work_item->link );
       }
       break;

     case WVA_GATING_CMD_STATE_EXECUTE:
     case WVA_GATING_CMD_STATE_CONTINUE:
       {
         if ( ctrl->pkt_type == WVA_WORK_ITEM_PKT_TYPE_CMD )
         {
           cmd_pkt = ( ( wva_cmd_packet_t* ) ctrl->packet );
           request_id = cmd_pkt->cmd_id;
         }
         else if ( ctrl->pkt_type == WVA_WORK_ITEM_PKT_TYPE_EVENT )
         {
           event_pkt = ( ( wva_event_packet_t* ) ctrl->packet );
           request_id = event_pkt->event_id;
         }
         else
         {
           WVA_PANIC_ON_ERROR ( APR_EUNEXPECTED );
         }

         /**
          * For Supported request_id, handler should take care of releasing 
          * memory allocated for packets.
          */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
                 "wva_task_process_gating_work_items(): request_id=(0x%08x)",
                 request_id );

         switch ( request_id )
         {
          case WCDMA_IVOICE_EVENT_REQUEST_START:
            rc = wva_process_wcdma_vocoder_start_event( ctrl );
            break;

          case WCDMA_IVOICE_EVENT_REQUEST_STOP:
            rc = wva_process_wcdma_vocoder_stop_event( ctrl );
            break;

          case VS_COMMON_EVENT_READY:
            rc = wva_process_vs_ready_event( ctrl );
            break;

          case VS_COMMON_EVENT_NOT_READY:
            rc = wva_process_vs_not_ready_event( ctrl );
            break;

          case WVA_INTERNAL_EVENT_VS_OPEN:
            rc = wva_process_vs_open_event( ctrl );
            break;

          case WVA_INTERNAL_EVENT_VS_CLOSE:
            rc = wva_process_vs_close_event( ctrl );
            break;

          case WVA_IRESOURCE_CMD_GRANT:
            rc = wva_process_resource_grant_cmd( ctrl );
            break;

          case WVA_IRESOURCE_CMD_REVOKE:
            rc = wva_process_resource_revoke_cmd( ctrl );
            break;

          default:
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                     "wva_task_process_gating_work_items(): Unsupported "
                     "request_id=(0X%08X)", request_id );

              /** For unsupported request_id, memory cleanup required for
               *  CMD/EVENT packets. */
              if( ctrl->pkt_type == WVA_WORK_ITEM_PKT_TYPE_CMD )
              {
                ( void ) wva_free_cmd_packet ( cmd_pkt );
              }

              if( ctrl->pkt_type == WVA_WORK_ITEM_PKT_TYPE_EVENT )
              {
                ( void ) wva_free_event_packet ( event_pkt );
              }

              /* set to VS_EOK to fetch the next command in queue. */
              rc = APR_EOK;
            }
           break;
         }

         /* Evaluate the pending command completion status. */
         switch ( rc )
         {
          case APR_EOK:
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                     "wva_task_process_gating_work_items(): request_id = "
                     "(0X%08X) processed successfully ", request_id );
              ctrl->packet = NULL;
              ctrl->pkt_type = WVA_WORK_ITEM_PKT_TYPE_NONE;
              /* The current command is finished so fetch the next command. */
              ctrl->state = WVA_GATING_CMD_STATE_FETCH;
            }
            break;
         
          case APR_EPENDING:
            /**
             * Assuming the current pending command control routine returns
             * APR_EPENDING the overall progress stalls until one or more
             * external events or responses are received.
             */
            ctrl->state = WVA_GATING_CMD_STATE_CONTINUE;
            /**
             * Return from here so as to avoid unecessary looping till reponse
             * is recived.
             */
            return;
         
          default:
            WVA_PANIC_ON_ERROR( APR_EUNEXPECTED );
            break;
         }
       }
       break;

     default:
      WVA_PANIC_ON_ERROR( rc );
      break;
    }/* switch case ends. */
  }/* for loop ends. */

  return;
}

/****************************************************************************
 * WVA TASK ROUTINES                                                        *
 ****************************************************************************/

WVA_INTERNAL void wva_signal_run ( void )
{
  apr_event_signal( wva_work_event );
}

static int32_t wva_run ( void )
{
  wva_task_process_nongating_work_items( );
  wva_task_process_gating_work_items( );

  return APR_EOK;
}

static int32_t wva_worker_thread_fn (
  void* param
)
{
  int32_t rc;

  apr_event_create( &wva_work_event );
  apr_event_signal( wva_control_event );

  for ( ;; )
  {
    rc = apr_event_wait( wva_work_event );
    if ( rc ) break;

    wva_run( );
  }

  apr_event_destroy( wva_work_event );
  apr_event_signal( wva_control_event );

  return APR_EOK;
}


/****************************************************************************
 * WVA BOOT-UP and POWER-DOWN ROUTINES                                      *
 ****************************************************************************/

static uint32_t wva_gating_control_init (
  wva_gating_control_t* p_ctrl
)
{
  uint32_t rc = APR_EOK;

  if ( p_ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_list_init_v2( &p_ctrl->cmd_q,
                         wva_thread_lock_fn, wva_thread_unlock_fn );
  if ( rc )
  {
    return APR_EFAILED;
  }

  p_ctrl->state = WVA_GATING_CMD_STATE_FETCH;
  p_ctrl->pkt_type = WVA_WORK_ITEM_PKT_TYPE_NONE;
  p_ctrl->packet = NULL;
  p_ctrl->rootjob_obj = NULL;

  return APR_EOK;
}  /* end of wva_gating_control_init () */

static uint32_t wva_gating_control_destroy (
  wva_gating_control_t* p_ctrl
)
{
  if ( p_ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  ( void ) apr_list_destroy( &p_ctrl->cmd_q );

  return APR_EOK;
}  /* end of vs_gating_control_destroy () */


/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/

static uint32_t wva_resource_cmd_register_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  wva_session_object_t* session_obj = NULL;
  wva_iresource_cmd_register_t* cmd_params = NULL;
  
  for ( ;; )
  {
    if ( size != sizeof ( wva_iresource_cmd_register_t ) )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }
  
    cmd_params = ( wva_iresource_cmd_register_t* ) params;
    if( cmd_params == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }
  
    WVA_ACQUIRE_LOCK ( wva_global_lock );
  
    /* Find a session with WVA_VSID_UNDEFINED_V vsid. */
    for( index = 0; index < WVA_MAX_NUM_OF_SESSIONS_V; ++index )
    {
      session_obj = wva_session_obj_list[index];
  
      if( ( session_obj->vsid == cmd_params->vsid ) ||
          ( session_obj->vsid == WVA_VSID_UNDEFINED_V ) )
      {
        break;
      }
      else
      {
        session_obj = NULL;
      }
    }
  
    if( session_obj != NULL )
    {
      session_obj->va_session_context = cmd_params->session_context;
      session_obj->va_wva_event_cb = cmd_params->event_cb;
      session_obj->vsid = cmd_params->vsid;
      ( void ) wva_vs_open_session( session_obj );
  
      *(cmd_params->ret_handle) = session_obj->header.handle;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_resource_cmd_register_proc(): Cannot find  WVA session instance" );
    }
  
    WVA_RELEASE_LOCK ( wva_global_lock );
  
    break;
  }

  return rc;
}


static uint32_t wva_resource_cmd_deregister_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  wva_session_object_t* session_obj = NULL;
  wva_iresource_cmd_deregister_t* cmd_params = NULL;
  
  for ( ;; )
  {
    if ( size != sizeof ( wva_iresource_cmd_deregister_t ) )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }
  
    cmd_params = ( wva_iresource_cmd_deregister_t* ) params;
    if( cmd_params == NULL )
    {
      WVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }
  
    rc = wva_get_object ( cmd_params->handle, ( wva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "wva_resource_cmd_deregister_proc(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      break;
    }
  
    WVA_ACQUIRE_LOCK ( wva_global_lock );
  
    session_obj->vsid = WVA_VSID_UNDEFINED_V;
    session_obj->va_session_context = NULL;
    session_obj->va_wva_event_cb = NULL;
    ( void ) wva_vs_close_session( session_obj, NULL );
  
    WVA_RELEASE_LOCK ( wva_global_lock );
  
    break;
  }

  return rc;
}

/****************************************************************************
 * POWER UP/DOWN ROUTINES                                                    *
 ****************************************************************************/

static int32_t wva_init ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index;
  RCINIT_INFO info_handle = NULL;
  RCINIT_PRIO priority = 0;
  unsigned long stack_size = 0;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
         "wva_init(): Date %s Time %s", __DATE__, __TIME__ );

  {  /* Initialize the locks. */
    rc = apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &wva_int_lock );
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &wva_thread_lock );
    apr_event_create( &wva_control_event );
  }

  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &wva_heapmgr, ( ( void* ) &wva_heap_pool ),
                          sizeof( wva_heap_pool ), NULL, NULL );
  }

  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;
  
    params.table = wva_object_table;
    params.total_bits = WVA_HANDLE_TOTAL_BITS_V;
    params.index_bits = WVA_HANDLE_INDEX_BITS_V;
    params.lock_fn = wva_int_lock_fn;
    params.unlock_fn = wva_int_unlock_fn;
    rc = apr_objmgr_construct( &wva_objmgr, &params );
  }

  { /* Initialize free and nongating work pkt queues. */
    rc = apr_list_init_v2( &wva_free_work_pkt_q, 
                           wva_int_lock_fn, wva_int_unlock_fn );
    for ( index = 0; index < WVA_NUM_WORK_PKTS_V; ++index )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &wva_work_pkts[index] );
      wva_work_pkts[index].pkt_type = WVA_WORK_ITEM_PKT_TYPE_NONE;
      wva_work_pkts[index].packet = NULL;
      rc = apr_list_add_tail( &wva_free_work_pkt_q,
                              ( ( apr_list_node_t* ) &wva_work_pkts[index] ) );
    }

    rc = apr_list_init_v2( &wva_nongating_work_pkt_q,
                           wva_int_lock_fn, wva_int_unlock_fn );
  }

  { /* Initialize gating work pkt queue. */
    rc = wva_gating_control_init( &wva_gating_work_pkt_q );
  }

  { /* Initialize the global session lock. */
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &wva_global_lock );
    WVA_PANIC_ON_ERROR( rc );
  }

  {
    mmstd_memset ( wva_task_stack, 0xFA, WVA_TASK_STACK_SIZE );
  }
  { /* Create the wva task worker thread. */
    info_handle = rcinit_lookup( WVA_TASK_NAME );
    if ( info_handle == NULL ) 
    {
      /* Use the default priority & stack_size*/
      MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "wva_init(): WVA task not registered with RCINIT" );
      priority = WVA_TASK_PRIORITY;
      stack_size = WVA_TASK_STACK_SIZE;
    }
    else
    {
      priority = rcinit_lookup_prio_info( info_handle );
      stack_size = rcinit_lookup_stksz_info( info_handle );
    }

    if ( ( priority > 255 ) || ( stack_size == 0 ) ) 
    {
      ERR_FATAL( "wva_init(): Invalid priority: %d or stack size: %d",
                 priority, stack_size, 0 );
    }

    rc = apr_thread_create( &wva_thread, WVA_TASK_NAME, TASK_PRIORITY(priority),
                            wva_task_stack, stack_size, 
                            wva_worker_thread_fn , NULL );
    WVA_PANIC_ON_ERROR( rc );

    apr_event_wait( wva_control_event );
  }

  wva_is_initialized = TRUE;

  return rc;
}


static int32_t wva_postinit ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index =0;

  /* Initialize the mapping info and open onex voice instance. */
  for( index = 0; index < WVA_MAX_NUM_OF_SESSIONS_V; ++index )
  {
    /* Create and Initialize modem subscription object. */
    rc = wva_create_modem_subs_object( &wva_subs_obj_list[index] );;
    WVA_PANIC_ON_ERROR (rc);

    /* Open CDMA voice session instance. */
    wva_subs_obj_list[index]->asid = ( sys_modem_as_id_e_type ) index;
    rc = wva_wcdma_open_session( wva_subs_obj_list[index] );
    WVA_PANIC_ON_ERROR (rc);

    /* Create and initialize WVA session object. */
    rc =  wva_create_session_object ( &wva_session_obj_list[index] );
    WVA_PANIC_ON_ERROR( rc );
  }

  return rc;
}


static int32_t wva_predeinit ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index =0;

  /* Close onex Session instance. */
  for( index = 0; index < WVA_MAX_NUM_OF_SESSIONS_V; ++index )
  {
    /* WVA CLOSE's VS session instance for all available VSID. */
    ( void ) wva_vs_close_session( wva_session_obj_list[index], NULL );

    /* WVA CLOSE's CDMA session instance for all available ASID. */
    ( void ) wva_wcdma_close_session( wva_subs_obj_list[index] );

    /* Free WVA session object for all VSID. */
    ( void ) apr_timer_destroy ( wva_session_obj_list[index]->sfg_timer );
    ( void ) apr_lock_destroy( wva_session_obj_list[index]->data_lock );
    ( void ) wva_mem_free_object( (wva_object_t*)wva_session_obj_list[index] );
    
    /* Free WVA subscription object for all ASID. */
    ( void ) wva_mem_free_object( (wva_object_t*)wva_subs_obj_list[index] );
  }

  return rc;
}


static int32_t wva_deinit ( void )
{
  uint32_t rc = APR_EOK;

  wva_is_initialized = FALSE;

  apr_event_signal_abortall( wva_work_event );
  apr_event_wait( wva_control_event );


  /* Release gating control structures */
  ( void ) wva_gating_control_destroy( &wva_gating_work_pkt_q );

  /* Release work queue */
  ( void ) apr_list_destroy( &wva_free_work_pkt_q );
  ( void ) apr_list_destroy( &wva_nongating_work_pkt_q );


  /* Deinitialize the object handle table. */
  ( void ) apr_objmgr_destruct( &wva_objmgr );

  /* Deinitialize basic OS resources for staging the setup. */
  ( void ) apr_event_destroy( wva_control_event );
  ( void ) apr_lock_destroy( wva_int_lock );
  ( void ) apr_lock_destroy( wva_thread_lock );
  ( void ) apr_lock_destroy( wva_global_lock );

  return rc;
}


WVA_EXTERNAL uint32_t wva_call (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc;

  switch ( cmd_id )
  {
  case DRV_CMDID_INIT:
    rc = wva_init( );
    break;

  case DRV_CMDID_POSTINIT:
    rc = wva_postinit( );
    break;

  case DRV_CMDID_PREDEINIT:
    rc = wva_predeinit( );
    break;

  case DRV_CMDID_DEINIT:
    rc = wva_deinit( );
    break;

  case WVA_IRESOURCE_CMD_REGISTER:
    rc =  wva_resource_cmd_register_proc( params, size );
    break;

  case WVA_IRESOURCE_CMD_DEREGISTER:
    rc =  wva_resource_cmd_deregister_proc( params, size );
    break;

  case WVA_IRESOURCE_CMD_GRANT:
      rc = wva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    break;

  case WVA_IRESOURCE_CMD_REVOKE:
      rc = wva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    break;

  case WVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING:
    rc = wva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "wva_call(): Unsupported cmd ID (0x%08x)", cmd_id );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

