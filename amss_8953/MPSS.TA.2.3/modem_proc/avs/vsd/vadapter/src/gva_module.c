/*
   Copyright (C) 2015-2016 QUALCOMM Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

   $Header: //components/rel/avs.mpss/6.2.1/vsd/vadapter/src/gva_module.c#10 $
   $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

/* CORE APIs. */
#include <stddef.h>
#include <string.h>
#include "err.h"
#include "msg.h"
#include "rcinit.h"
#include "mmstd.h"

/* APR APIs. */
#include "apr_errcodes.h"
#include "apr_list.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_event.h"
#include "apr_thread.h"
#include "apr_memmgr.h"

/* GSM APIs. */
#include "gsm_voice_server_if.h"

/* VSD APIs. */
#include "drv_api.h"
#include "vs_task.h"
#include "voice_util_api.h"
#include "vs.h"
#include "voicelog_if.h"
#include "voicelog_utils.h"

/* SELF APIs. */
#include "gva_if.h"
#include "gva_iresource_if.h"
#include "gva_i.h"

/*****************************************************************************
 * Defines                                                                   *
 ****************************************************************************/

/* Currently voice agent support max two subscription and max two VSID. */
#define GVA_MAX_NUM_OF_SESSIONS_V ( 2 )

#define GVA_MAX_VOC_FRAME_LENGTH ( 65 )


/*****************************************************************************
 * Global Variables                                                          *
 ****************************************************************************/

static apr_lock_t gva_int_lock;
static apr_lock_t gva_thread_lock;
static apr_event_t gva_control_event;

static apr_memmgr_type gva_heapmgr;
static uint8_t gva_heap_pool[ GVA_HEAP_SIZE_V ];

static apr_objmgr_t gva_objmgr;
static apr_objmgr_object_t gva_object_table[ GVA_MAX_OBJECTS_V ];

static gva_gating_control_t gva_gating_work_pkt_q;
static apr_list_t gva_nongating_work_pkt_q;
static apr_list_t gva_free_work_pkt_q;
static gva_work_item_t gva_work_pkts[ GVA_NUM_WORK_PKTS_V ];


static apr_event_t gva_work_event;
static apr_thread_t gva_thread;
static uint8_t gva_task_stack[ GVA_TASK_STACK_SIZE ];

static gva_modem_subs_object_t* gva_subs_obj_list[GVA_MAX_NUM_OF_SESSIONS_V];
static gva_session_object_t* gva_session_obj_list[GVA_MAX_NUM_OF_SESSIONS_V];
static apr_lock_t gva_global_lock;

static bool_t gva_is_initialized = FALSE; 

/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

static void gva_int_lock_fn ( void )
{
  apr_lock_enter( gva_int_lock );
}

static void gva_int_unlock_fn ( void )
{
  apr_lock_leave( gva_int_lock );
}

static void gva_thread_lock_fn ( void )
{
  apr_lock_enter( gva_thread_lock );
}

static void gva_thread_unlock_fn ( void )
{
  apr_lock_leave( gva_thread_lock );
}


/****************************************************************************
 * GVA CMDs & EVENTs PACKET QUEUING FUNCTIONS                               *
 ****************************************************************************/

/**
 * Queues the gva_cmd_packet_t and gva_event_packet_t. In
 * case of failure to queue a apr packet, packet shall be
 * freed by the caller.
 */
GVA_INTERNAL uint32_t gva_queue_work_packet (
  gva_work_item_queue_type_t queue_type,
  gva_work_item_packet_type_t pkt_type,
  void* packet
)
{
  uint32_t rc = APR_EOK;
  gva_work_item_t* work_item = NULL;
  apr_list_t* work_queue = NULL;

  switch ( queue_type )
  {
   case GVA_WORK_ITEM_QUEUE_TYPE_NONGATING:
     work_queue = &gva_nongating_work_pkt_q;
     break;

   case GVA_WORK_ITEM_QUEUE_TYPE_GATING:
     work_queue = &gva_gating_work_pkt_q.cmd_q;
     break;

   default:
     rc = APR_EUNSUPPORTED;
     break;
  }

  for ( ;; )
  {
    /* Get a free command structure. */
    rc = apr_list_remove_head( &gva_free_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) );
    if ( rc )
    {
      rc = APR_ENORESOURCE;
      /* No free WORK packet structure is available. */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
             "gva_queue_work_packet(): Ran out of WORK packets, rc=0x%08x, "
             "gva_state=%d",   rc, gva_is_initialized );
      break;
    }

    if ( pkt_type == GVA_WORK_ITEM_PKT_TYPE_CMD )
    {
      work_item->pkt_type = GVA_WORK_ITEM_PKT_TYPE_CMD;
    }
    else if ( pkt_type == GVA_WORK_ITEM_PKT_TYPE_EVENT )
    {
     work_item->pkt_type = GVA_WORK_ITEM_PKT_TYPE_EVENT;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
           "gva_queue_work_packet(): Invalid packet type!!!" );
      GVA_PANIC_ON_ERROR ( APR_ENOTEXIST );
    }

    work_item->packet = packet;


    /* Add to incoming request work queue */
    rc = apr_list_add_tail( work_queue, &work_item->link );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_queue_work_packet() - ERROR: rc=0x%08x", rc );
      /* Add back to gva_free_work_pkt_q */
      work_item->pkt_type = GVA_WORK_ITEM_PKT_TYPE_NONE;
      work_item->packet = NULL;
      ( void ) apr_list_add_tail( &gva_free_work_pkt_q, &work_item->link );
    }
    else
    {
      /**
       * Signal appropriate thread.
       */
      gva_signal_run();
    }

    break;
  } /* for loop ends. */

  return rc;
}  /* gva_queue_work_packet() ends. */


/****************************************************************************
 * GVA CMDs/EVENTs PACKET PREPARE/DISPATCHER/FREE ROUTINES                  *
 ****************************************************************************/

GVA_INTERNAL uint32_t gva_free_cmd_packet (
  gva_cmd_packet_t* packet
)
{
  uint32_t rc = VS_EOK;

  if ( packet != NULL )
  {
    if( packet->params != NULL )
    {
      /* Free the memory - p_cmd_packet->params. */
      apr_memmgr_free( &gva_heapmgr, packet->params );
      packet->params = NULL;
    }

    /* Free the memeory - p_cmd_packet. */
    apr_memmgr_free( &gva_heapmgr, packet );
    packet = NULL;
  }

  return rc;
}

/**
 * This is a common routine facilitating to prepare and
 * dispatches a CMD PKT.
 */
GVA_INTERNAL uint32_t gva_prepare_and_dispatch_cmd_packet (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  gva_cmd_packet_t* packet = NULL;

  for ( ;; )
  {
    packet = ( ( gva_cmd_packet_t* ) apr_memmgr_malloc( &gva_heapmgr,
                                           sizeof( gva_cmd_packet_t ) ) );
    if ( packet == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR( APR_ENORESOURCE );
      rc = APR_ENORESOURCE;
      break;
    }

    packet->cmd_id = cmd_id;
    packet->params = NULL;

    if ( ( size > 0 ) && ( params != NULL ) )
    {
      packet->params = apr_memmgr_malloc(  &gva_heapmgr, size );

      if ( packet->params == NULL )
      {
        rc = APR_ENORESOURCE;
        GVA_REPORT_FATAL_ON_ERROR( rc );
        ( void ) gva_free_cmd_packet( packet );
        break;
      }
      mmstd_memcpy( packet->params, size, params, size );
    }

    /* Queue the command packet for processing. */
    rc = gva_queue_work_packet( GVA_WORK_ITEM_QUEUE_TYPE_NONGATING,
                                GVA_WORK_ITEM_PKT_TYPE_CMD, ( void*) packet );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_prepare_and_dispatch_cmd_packet() - cmd pkt queuing failed. "
             "rc=(0x%08x)", rc );
      ( void ) gva_free_cmd_packet( packet );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "gva_prepare_and_dispatch_cmd_packet() cmd pkt queued with "
             "cmd_id=(0x%08x)", cmd_id );
    }

    break;
  }

  return rc;
}


GVA_INTERNAL uint32_t gva_free_event_packet (
  gva_event_packet_t* packet
)
{
  uint32_t rc = VS_EOK;

  if ( packet != NULL )
  {
    if( packet->params != NULL )
    {
      /* Free the memory - p_cmd_packet->params. */
      apr_memmgr_free( &gva_heapmgr, packet->params );
      packet->params = NULL;
    }

    /* Free the memeory - p_cmd_packet. */
    apr_memmgr_free( &gva_heapmgr, packet );
    packet= NULL;
  }

  return rc;
}

/**
 * This is a common routine facilitating to prepare and
 * dispatches a CMD PKT.
 */
GVA_INTERNAL uint32_t gva_prepare_and_dispatch_event_packet (
  void* session_context,
  uint32_t event_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  gva_event_packet_t* packet = NULL;

  for ( ;; )
  {
    packet = ( ( gva_event_packet_t* ) apr_memmgr_malloc( &gva_heapmgr,
                                         sizeof( gva_event_packet_t ) ) );
    if ( packet == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR( APR_ENORESOURCE );
      rc = APR_ENORESOURCE;
      break;
    }

    packet->session_context = session_context;
    packet->event_id = event_id;
    packet->params = NULL;

    if ( ( size > 0 ) && ( params != NULL ) )
    {
      packet->params = apr_memmgr_malloc(  &gva_heapmgr, size );

      if ( packet->params == NULL )
      {
        rc = APR_ENORESOURCE;
        GVA_REPORT_FATAL_ON_ERROR( rc );
        ( void ) gva_free_event_packet( packet );
        break;
      }
      mmstd_memcpy( packet->params, size, params, size );
    }

    /* Queue the command packet for processing. */
    rc = gva_queue_work_packet( GVA_WORK_ITEM_QUEUE_TYPE_NONGATING,
                                GVA_WORK_ITEM_PKT_TYPE_EVENT, ( void*) packet );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_prepare_and_dispatch_event_packet()-event pkt queuing failed "
             "rc=(0x%08x)", rc );
      ( void ) gva_free_event_packet( packet );
    }
    else
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "gva_prepare_and_dispatch_event_packet()-event pkt queued with "
             "event_id=(0x%08x)", event_id );
    }

    break;
  }

  return rc;
}


/****************************************************************************
 * GVA OBJECT CREATION, DESTRUCTION AND INITIALISATION ROUTINES             *
 ****************************************************************************/

static int32_t gva_get_object (
  uint32_t handle,
  gva_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_objmgr_find_object( &gva_objmgr, handle, &objmgr_obj );
  if ( rc )
  {
    return APR_EFAILED;
  }

  *ret_obj = ( ( gva_object_t* ) objmgr_obj->any.ptr );

  return APR_EOK;
}

static uint32_t gva_mem_alloc_object (
  uint32_t size,
  gva_object_t** ret_object
)
{
  int32_t rc;
  gva_object_t* gva_obj;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_object == NULL )
  {
    return APR_EBADPARAM;
  }

  { /* Allocate memory for the GVA object. */
    gva_obj = apr_memmgr_malloc( &gva_heapmgr, size );
    if ( gva_obj == NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_mem_alloc_object(): Out of memory, requested size (%d)", size );
      return APR_ENORESOURCE;
    }

    /* Allocate a new handle for the MVS object. */
    rc = apr_objmgr_alloc_object( &gva_objmgr, &objmgr_obj );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "gva_mem_alloc_object(): Out of objects, rc = (0x%08X)", rc );
      apr_memmgr_free( &gva_heapmgr, gva_obj );
      return APR_ENORESOURCE;
    }

    /* Use the custom object type. */
    objmgr_obj->any.ptr = gva_obj;

    /* Initialize the base MVS object header. */
    gva_obj->header.handle = objmgr_obj->handle;
    gva_obj->header.type = GVA_OBJECT_TYPE_ENUM_UNINITIALIZED;
  }

  *ret_object = gva_obj;

  return APR_EOK;
}

static uint32_t gva_mem_free_object (
  gva_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &gva_objmgr, object->header.handle );
  apr_memmgr_free( &gva_heapmgr, object );

  return APR_EOK;
}

static uint32_t gva_create_modem_subs_object ( 
  gva_modem_subs_object_t** ret_subs_obj )
{
  uint32_t rc = APR_EOK;
  gva_modem_subs_object_t* subs_obj = NULL;

  if ( ret_subs_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = gva_mem_alloc_object( sizeof( gva_modem_subs_object_t ),
                             ( ( gva_object_t** ) &subs_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the Object. */
    subs_obj->header.type = GVA_OBJECT_TYPE_ENUM_MODEM_SUBSCRIPTION;

    subs_obj->asid = SYS_MODEM_AS_ID_NONE;
    subs_obj->vsid = GVA_VSID_UNDEFINED_V;
    subs_obj->pending_vsid = GVA_VSID_UNDEFINED_V;

    subs_obj->gsm_handle = APR_NULL_V;
    subs_obj->is_gsm_ready = FALSE;
    subs_obj->session_obj = NULL;;
  }

  *ret_subs_obj = subs_obj;

  return APR_EOK;
}


static uint32_t gva_create_session_object ( 
  gva_session_object_t** ret_session_obj )
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;

  if ( ret_session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = gva_mem_alloc_object( sizeof( gva_session_object_t ),
                             ( ( gva_object_t** ) &session_obj ) );
  if ( rc )
  {
    *ret_session_obj = NULL;
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    session_obj->header.type = GVA_OBJECT_TYPE_ENUM_SESSION;

    session_obj->active_subs_obj = NULL;
    session_obj->vsid = GVA_VSID_UNDEFINED_V;
    session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_RELEASED;
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &session_obj->data_lock );
    GVA_PANIC_ON_ERROR(rc);

    session_obj->va_gva_event_cb = NULL;
    session_obj->va_session_context =  NULL;
    session_obj->is_resource_granted = FALSE;

    session_obj->vocoder_type = GVA_VOCODER_ID_UNDEFINED_V;
    session_obj->codec_mode = GVA_CODEC_MODE_UNDEFINED;
    session_obj->dtx_mode = FALSE;

    session_obj->vs_handle = APR_NULL_V;
    session_obj->vs_read_buf = NULL;
    session_obj->vs_write_buf = NULL;
    session_obj->is_vs_ready = FALSE;
    session_obj->primed_read_buf = NULL;
  }


  *ret_session_obj = session_obj;

  return APR_EOK;
}

static uint32_t gva_create_simple_job_object (
  uint32_t parentjob_handle,
  gva_simple_job_object_t** ret_job_obj
)
{
  int32_t rc;
  gva_simple_job_object_t* gva_obj = NULL;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = gva_mem_alloc_object( sizeof( gva_simple_job_object_t ),
                             ( ( gva_object_t** ) &gva_obj ) );
  if ( rc )
  {
    *ret_job_obj = NULL;
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    gva_obj->header.type = GVA_OBJECT_TYPE_ENUM_SIMPLE_JOB;
    gva_obj->context_handle = parentjob_handle;
    gva_obj->is_completed = 0;
  }

  *ret_job_obj = gva_obj;

  return APR_EOK;
}

GVA_INTERNAL int32_t gva_create_sequencer_job_object (
  gva_sequencer_job_object_t** ret_job_obj
)
{
  int32_t rc;
  gva_sequencer_job_object_t* job_obj = NULL;

  if ( ret_job_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = gva_mem_alloc_object( sizeof( gva_sequencer_job_object_t ),
                             ( ( gva_object_t** ) &job_obj ) );
  if ( rc )
  {
    *ret_job_obj = NULL;
    return APR_ENORESOURCE;
  }

  { /* Initialize the pending job object. */
    job_obj->header.type = GVA_OBJECT_TYPE_ENUM_SEQUENCER_JOB;

    job_obj->state = APR_NULL_V;
    job_obj->subjob_obj = NULL;
    job_obj->status = APR_UNDEFINED_ID_V;
  }

  *ret_job_obj = job_obj;

  return APR_EOK;
}


/****************************************************************************
 * GVA GSM <> VS MAPPING  ROUTINES                                          *
 ****************************************************************************/

static uint32_t gva_map_vocamr_codec_mode_gsm_to_vs( 
  uint32_t gsm_codec_mode,
  uint32_t* vs_codec_mode
)
{
  uint32_t rc = APR_EOK;

  switch ( gsm_codec_mode )
  {
   case GSM_IVOCAMR_CODEC_MODE_0475:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0475;
     break;

   case GSM_IVOCAMR_CODEC_MODE_0515:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0515;
     break;

   case GSM_IVOCAMR_CODEC_MODE_0590:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0590;
     break;

   case GSM_IVOCAMR_CODEC_MODE_0670:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0670;
     break;

   case GSM_IVOCAMR_CODEC_MODE_0740:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0740;
     break;

   case GSM_IVOCAMR_CODEC_MODE_0795:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_0795;
     break;

   case GSM_IVOCAMR_CODEC_MODE_1020:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_1020;
     break;

   case GSM_IVOCAMR_CODEC_MODE_1220:
     *vs_codec_mode = VS_VOCAMR_CODEC_MODE_1220;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *vs_codec_mode = 0xFFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t gva_map_vocamr_codec_mode_vs_to_gsm( 
  uint32_t vs_codec_mode,
  uint32_t* gsm_codec_mode
)
{
  uint32_t rc = APR_EOK;

  switch ( vs_codec_mode )
  {
   case VS_VOCAMR_CODEC_MODE_0475:
     *gsm_codec_mode = GSM_IVOCAMR_CODEC_MODE_0475;
     break;

   case VS_VOCAMR_CODEC_MODE_0515:
     *gsm_codec_mode = GSM_IVOCAMR_CODEC_MODE_0515;
     break;

   case VS_VOCAMR_CODEC_MODE_0590:
     *gsm_codec_mode = GSM_IVOCAMR_CODEC_MODE_0590;
     break;

   case VS_VOCAMR_CODEC_MODE_0670:
     *gsm_codec_mode = GSM_IVOCAMR_CODEC_MODE_0670;
     break;

   case VS_VOCAMR_CODEC_MODE_0740:
     *gsm_codec_mode = GSM_IVOCAMR_CODEC_MODE_0740;
     break;

   case VS_VOCAMR_CODEC_MODE_0795:
     *gsm_codec_mode = GSM_IVOCAMR_CODEC_MODE_0795;
     break;

   case VS_VOCAMR_CODEC_MODE_1020:
     *gsm_codec_mode = GSM_IVOCAMR_CODEC_MODE_1020;
     break;

   case VS_VOCAMR_CODEC_MODE_1220:
     *gsm_codec_mode = GSM_IVOCAMR_CODEC_MODE_1220;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *gsm_codec_mode = 0xFFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t gva_map_vocamrwb_codec_mode_gsm_to_vs( 
  uint32_t gsm_codec_mode,
  uint32_t* vs_codec_mode
)
{
  uint32_t rc = APR_EOK;

  switch ( gsm_codec_mode )
  {
   case GSM_IVOCAMRWB_CODEC_MODE_0660:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_0660;
     break;
  
   case GSM_IVOCAMRWB_CODEC_MODE_0885:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_0885;
     break;
  
   case GSM_IVOCAMRWB_CODEC_MODE_1265:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1265;
     break;
  
   case GSM_IVOCAMRWB_CODEC_MODE_1425:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1425;
     break;
    
   case GSM_IVOCAMRWB_CODEC_MODE_1585:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1585;
     break;
    
   case GSM_IVOCAMRWB_CODEC_MODE_1825:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1825;
     break;
    
   case GSM_IVOCAMRWB_CODEC_MODE_1985:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_1985;
     break;
   
   case GSM_IVOCAMRWB_CODEC_MODE_2305:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_2305;
     break;
  
   case GSM_IVOCAMRWB_CODEC_MODE_2385:
     *vs_codec_mode = VS_VOCAMRWB_CODEC_MODE_2385;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *vs_codec_mode = 0xFFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t gva_map_vocamrwb_codec_mode_vs_to_gsm( 
  uint32_t vs_codec_mode,
  uint32_t* gsm_codec_mode
)
{
  uint32_t rc = APR_EOK;

  switch ( vs_codec_mode )
  {
   case VS_VOCAMRWB_CODEC_MODE_0660:
     *gsm_codec_mode =GSM_IVOCAMRWB_CODEC_MODE_0660;
     break;
  
   case VS_VOCAMRWB_CODEC_MODE_0885:
     *gsm_codec_mode = GSM_IVOCAMRWB_CODEC_MODE_0885;
     break;
  
   case VS_VOCAMRWB_CODEC_MODE_1265:
     *gsm_codec_mode = GSM_IVOCAMRWB_CODEC_MODE_1265;
     break;
  
   case VS_VOCAMRWB_CODEC_MODE_1425:
     *gsm_codec_mode = GSM_IVOCAMRWB_CODEC_MODE_1425;
     break;
    
   case VS_VOCAMRWB_CODEC_MODE_1585:
     *gsm_codec_mode = GSM_IVOCAMRWB_CODEC_MODE_1585;
     break;
    
   case VS_VOCAMRWB_CODEC_MODE_1825:
     *gsm_codec_mode = GSM_IVOCAMRWB_CODEC_MODE_1825;
     break;
    
   case VS_VOCAMRWB_CODEC_MODE_1985:
     *gsm_codec_mode = GSM_IVOCAMRWB_CODEC_MODE_1985;
     break;
   
   case VS_VOCAMRWB_CODEC_MODE_2305:
     *gsm_codec_mode = GSM_IVOCAMRWB_CODEC_MODE_2305;
     break;
  
   case VS_VOCAMRWB_CODEC_MODE_2385:
     *gsm_codec_mode = GSM_IVOCAMRWB_CODEC_MODE_2385;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *gsm_codec_mode = 0xFFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t gva_map_vocamr_frame_type_vs_to_gsm(
  uint32_t vs_frame_type,
  uint32_t* gsm_frame_type
)
{
  uint32_t rc = APR_EOK;

  switch( vs_frame_type )
  {
   case VS_VOCAMR_FRAME_TYPE_NO_DATA:
     *gsm_frame_type = GSM_IVOCAMR_FRAME_TYPE_NO_DATA;
     break;
  
   case VS_VOCAMR_FRAME_TYPE_SID_BAD:
     *gsm_frame_type = GSM_IVOCAMR_FRAME_TYPE_SID_BAD;
     break;
  
   case VS_VOCAMR_FRAME_TYPE_SID_UPDATE:
     *gsm_frame_type = GSM_IVOCAMR_FRAME_TYPE_SID_UPDATE;
     break;
  
   case VS_VOCAMR_FRAME_TYPE_SID_FIRST:
     *gsm_frame_type = GSM_IVOCAMR_FRAME_TYPE_SID_FIRST;
     break;
  
   case VS_VOCAMR_FRAME_TYPE_SPEECH_BAD:
     *gsm_frame_type = GSM_IVOCAMR_FRAME_TYPE_SPEECH_BAD;
     break;
  
   case VS_VOCAMR_FRAME_TYPE_ONSET:
     *gsm_frame_type = GSM_IVOCAMR_FRAME_TYPE_SPEECH_ONSET;
     break;
  
   case VS_VOCAMR_FRAME_TYPE_SPEECH_DEGRADED:
     *gsm_frame_type = GSM_IVOCAMR_FRAME_TYPE_SPEECH_DEGRADED;
     break;
  
   case VS_VOCAMR_FRAME_TYPE_SPEECH_GOOD:
     *gsm_frame_type = GSM_IVOCAMR_FRAME_TYPE_SPEECH_GOOD;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *gsm_frame_type = 0xFFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t gva_map_vocamr_frame_type_gsm_to_vs(
  uint32_t gsm_frame_type,
  uint32_t* vs_frame_type
)
{
  uint32_t rc = APR_EOK;

  switch( gsm_frame_type )
  {
   case GSM_IVOCAMR_FRAME_TYPE_NO_DATA:
     *vs_frame_type = VS_VOCAMR_FRAME_TYPE_NO_DATA;
     break;
  
   case GSM_IVOCAMR_FRAME_TYPE_SID_BAD:
     *vs_frame_type = VS_VOCAMR_FRAME_TYPE_SID_BAD;
     break;
  
   case GSM_IVOCAMR_FRAME_TYPE_SID_UPDATE:
     *vs_frame_type = VS_VOCAMR_FRAME_TYPE_SID_UPDATE;
     break;
  
   case GSM_IVOCAMR_FRAME_TYPE_SID_FIRST:
     *vs_frame_type = VS_VOCAMR_FRAME_TYPE_SID_FIRST;
     break;
  
   case GSM_IVOCAMR_FRAME_TYPE_SPEECH_BAD:
     *vs_frame_type = VS_VOCAMR_FRAME_TYPE_SPEECH_BAD;
     break;
  
   case GSM_IVOCAMR_FRAME_TYPE_SPEECH_ONSET:
     *vs_frame_type = VS_VOCAMR_FRAME_TYPE_ONSET;
     break;
  
   case GSM_IVOCAMR_FRAME_TYPE_SPEECH_DEGRADED:
     *vs_frame_type = VS_VOCAMR_FRAME_TYPE_SPEECH_DEGRADED;
     break;
  
   case GSM_IVOCAMR_FRAME_TYPE_SPEECH_GOOD:
     *vs_frame_type = VS_VOCAMR_FRAME_TYPE_SPEECH_GOOD;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *vs_frame_type = 0xFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t gva_map_vocamrwb_frame_type_vs_to_gsm(
  uint32_t vs_frame_type,
  uint32_t* gsm_frame_type
)
{
  uint32_t rc = APR_EOK;
  
  switch( vs_frame_type )
  {
   case VS_VOCAMRWB_FRAME_TYPE_NO_DATA:
     *gsm_frame_type = GSM_IVOCAMRWB_FRAME_TYPE_NO_DATA;
     break;
  
   case VS_VOCAMRWB_FRAME_TYPE_SID_BAD:
     *gsm_frame_type = GSM_IVOCAMRWB_FRAME_TYPE_SID_BAD;
     break;
  
   case VS_VOCAMRWB_FRAME_TYPE_SID_UPDATE:
     *gsm_frame_type = GSM_IVOCAMRWB_FRAME_TYPE_SID_UPDATE;
     break;
  
   case VS_VOCAMRWB_FRAME_TYPE_SID_FIRST:
     *gsm_frame_type = GSM_IVOCAMRWB_FRAME_TYPE_SID_FIRST;
     break;
  
   case VS_VOCAMRWB_FRAME_TYPE_SPEECH_BAD:
     *gsm_frame_type = GSM_IVOCAMRWB_FRAME_TYPE_SPEECH_BAD;
     break;
  
   case VS_VOCAMRWB_FRAME_TYPE_SPEECH_LOST:
     *gsm_frame_type = GSM_IVOCAMRWB_FRAME_TYPE_SPEECH_LOST;
     break;
  
   case VS_VOCAMRWB_FRAME_TYPE_SPEECH_PROBABLY_DEGRADED:
     *gsm_frame_type = GSM_IVOCAMRWB_FRAME_TYPE_SPEECH_DEGRADED;
     break;
  
   case VS_VOCAMRWB_FRAME_TYPE_SPEECH_GOOD:
     *gsm_frame_type = GSM_IVOCAMRWB_FRAME_TYPE_SPEECH_GOOD;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *gsm_frame_type = 0xFFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t gva_map_vocamrwb_frame_type_gsm_to_vs(
  uint32_t gsm_frame_type,
  uint32_t* vs_frame_type
)
{
  uint32_t rc = APR_EOK;
  
  switch( gsm_frame_type )
  {
   case GSM_IVOCAMRWB_FRAME_TYPE_NO_DATA:
     *vs_frame_type = VS_VOCAMRWB_FRAME_TYPE_NO_DATA;
     break;
  
   case GSM_IVOCAMRWB_FRAME_TYPE_SID_BAD:
     *vs_frame_type = VS_VOCAMRWB_FRAME_TYPE_SID_BAD;
     break;
  
   case GSM_IVOCAMRWB_FRAME_TYPE_SID_UPDATE:
     *vs_frame_type = VS_VOCAMRWB_FRAME_TYPE_SID_UPDATE;
     break;
  
   case GSM_IVOCAMRWB_FRAME_TYPE_SID_FIRST:
     *vs_frame_type = VS_VOCAMRWB_FRAME_TYPE_SID_FIRST;
     break;
  
   case GSM_IVOCAMRWB_FRAME_TYPE_SPEECH_BAD:
     *vs_frame_type = VS_VOCAMRWB_FRAME_TYPE_SPEECH_BAD;
     break;
  
   case GSM_IVOCAMRWB_FRAME_TYPE_SPEECH_LOST:
     *vs_frame_type = VS_VOCAMRWB_FRAME_TYPE_SPEECH_LOST;
     break;
  
   case GSM_IVOCAMRWB_FRAME_TYPE_SPEECH_DEGRADED:
     *vs_frame_type = VS_VOCAMRWB_FRAME_TYPE_SPEECH_PROBABLY_DEGRADED;
     break;
  
   case GSM_IVOCAMRWB_FRAME_TYPE_SPEECH_GOOD:
     *vs_frame_type = VS_VOCAMRWB_FRAME_TYPE_SPEECH_GOOD;
     break;
  
   default:
     rc = APR_EBADPARAM;
     *vs_frame_type = 0xFFFFFFFF;
     break;
  }

  return rc;
}

static uint32_t gva_map_vocoder_type_gsm_to_vs(
  uint32_t gsm_vocoder_type
)
{
  uint32_t vs_media_id = 0xFFFFFFFF;

  switch ( gsm_vocoder_type )
  {
   case GSM_IVOCODER_ID_AMR:
     vs_media_id = VS_COMMON_MEDIA_ID_AMR;
     break;

   case GSM_IVOCODER_ID_AMRWB:
     vs_media_id = VS_COMMON_MEDIA_ID_AMRWB;
     break;

   case GSM_IVOCODER_ID_EFR:
    vs_media_id = VS_COMMON_MEDIA_ID_EFR;
    break;

   case GSM_IVOCODER_ID_FR:
    vs_media_id = VS_COMMON_MEDIA_ID_FR;
    break;

   case GSM_IVOCODER_ID_HR:
     vs_media_id = VS_COMMON_MEDIA_ID_HR;
     break;
  }

  return vs_media_id;
}

static bool_t gva_validate_gsm_vocoder_id (
  uint32_t vocoder_id
)
{
  bool_t rc = FALSE;

  switch ( vocoder_id )
  {
   case GSM_IVOCODER_ID_AMR:
   case GSM_IVOCODER_ID_AMRWB:
   case GSM_IVOCODER_ID_EFR:
   case GSM_IVOCODER_ID_FR:
   case GSM_IVOCODER_ID_HR:
     rc = TRUE;
     break;

   default:
     break;
  }

  return rc;
}
/****************************************************************************
 * GVA COMMON ROUTINES                                                      *
 ****************************************************************************/

static uint32_t gva_set_voc_codec_mode (
  gva_session_object_t* session_obj
)
{

  uint32_t rc = APR_EOK;
  vs_vocamr_cmd_set_codec_mode_t vocamr_codec_cmd;
  vs_vocamrwb_cmd_set_codec_mode_t vocamrwb_codec_cmd;
  uint32_t codec_mode;

  switch ( session_obj->vocoder_type )
  {
    case GSM_IVOCODER_ID_AMR:
    {
      rc = gva_map_vocamr_codec_mode_gsm_to_vs ( session_obj->codec_mode, &codec_mode );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "GVA: AMR: Unsupported CodecMode=(0x%08x)",
               session_obj->codec_mode );
      }
      else
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "GVA: AMR: CodecMode=(%d) sent to VS", codec_mode );
        vocamr_codec_cmd.handle = session_obj->vs_handle;
        vocamr_codec_cmd.codec_mode = codec_mode;
        rc  = vs_call( VS_VOCAMR_CMD_SET_CODEC_MODE, &vocamr_codec_cmd,
                       sizeof( vocamr_codec_cmd ) );
      }
    }
    break;

    case GSM_IVOCODER_ID_AMRWB:
    {
      rc = gva_map_vocamrwb_codec_mode_gsm_to_vs ( session_obj->codec_mode, &codec_mode );
      if ( rc )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "GVA: AMR-WB: Unsupported CodecMode=(0x%08x)",
               session_obj->codec_mode );
      }
      else
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "GVA: AMR-WB: CodecMode=(%d) sent to VS", codec_mode );
        vocamrwb_codec_cmd.handle = session_obj->vs_handle;
        vocamrwb_codec_cmd.codec_mode  = codec_mode;
        rc  = vs_call( VS_VOCAMRWB_CMD_SET_CODEC_MODE, &vocamrwb_codec_cmd,
                       sizeof( vocamrwb_codec_cmd ) );
      }
    }
    break;

    case GSM_IVOCODER_ID_EFR:
    case GSM_IVOCODER_ID_FR:
    case GSM_IVOCODER_ID_HR:
    default:
     rc = APR_EUNSUPPORTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_set_voc_codec_mode(): Unsupported VocoderId=(0x%08x)",
             session_obj->vocoder_type );
      break;
  }

  return rc;

}


static uint32_t gva_set_voc_dtx_mode (
  gva_session_object_t* session_obj
)
{

  uint32_t rc = APR_EOK;
  vs_vocamr_cmd_set_dtx_mode_t vocamr_dtx_cmd;
  vs_vocamrwb_cmd_set_dtx_mode_t vocamrwb_dtx_cmd;
  vs_vocefr_cmd_set_dtx_mode_t vocefr_dtx_cmd;
  vs_vocfr_cmd_set_dtx_mode_t vocfr_dtx_cmd;
  vs_vochr_cmd_set_dtx_mode_t vochr_dtx_cmd;

  switch ( session_obj->vocoder_type )
  {
   case GSM_IVOCODER_ID_AMR:
     vocamr_dtx_cmd.handle  = session_obj->vs_handle;
     vocamr_dtx_cmd.enable_flag  = session_obj->dtx_mode;
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "GVA: AMR dtx_mode=(%d) sent to VS", session_obj->dtx_mode );
     rc  = vs_call( VS_VOCAMR_CMD_SET_DTX_MODE, ( void* )&vocamr_dtx_cmd,
                    sizeof( vocamr_dtx_cmd ) );
     break;

   case GSM_IVOCODER_ID_AMRWB:
    vocamrwb_dtx_cmd.handle  = session_obj->vs_handle;
    vocamrwb_dtx_cmd.enable_flag  = session_obj->dtx_mode;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: AMR-WB dtx_mode=(%d) sent to VS", session_obj->dtx_mode );
    rc  = vs_call( VS_VOCAMRWB_CMD_SET_DTX_MODE, ( void* )&vocamrwb_dtx_cmd,
                   sizeof( vocamrwb_dtx_cmd ) );
    break;

   case GSM_IVOCODER_ID_EFR:
    vocefr_dtx_cmd.handle  = session_obj->vs_handle;
    vocefr_dtx_cmd.enable_flag  = session_obj->dtx_mode;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: EFR dtx_mode=(%d) sent to VS", session_obj->dtx_mode );
    rc  = vs_call( VS_VOCEFR_CMD_SET_DTX_MODE, ( void*)&vocefr_dtx_cmd,
                   sizeof( vocefr_dtx_cmd ) );
    break;

   case GSM_IVOCODER_ID_FR:
    vocfr_dtx_cmd.handle  = session_obj->vs_handle;
    vocfr_dtx_cmd.enable_flag  = session_obj->dtx_mode;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: FR dtx_mode=(%d) sent to VS", session_obj->dtx_mode );
    rc  = vs_call( VS_VOCFR_CMD_SET_DTX_MODE, ( void* )&vocfr_dtx_cmd,
                   sizeof( vocfr_dtx_cmd ) );
    break;

   case GSM_IVOCODER_ID_HR:
    vochr_dtx_cmd.handle  = session_obj->vs_handle;
    vochr_dtx_cmd.enable_flag  = session_obj->dtx_mode;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: HR dtx_mode=(%d) sent to VS", session_obj->dtx_mode );
    rc  = vs_call( VS_VOCHR_CMD_SET_DTX_MODE, ( void* )&vochr_dtx_cmd,
                   sizeof( vochr_dtx_cmd ) );
    break;

   default:
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_set_voc_dtx_mode(): Unsupported vocoder=(0x%08x)",
           session_obj->vocoder_type );
     break;
  }

  if ( rc != APR_EOK )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "GVA: failed to set dtx_mode=(%d), vocoder=(0x%08x)",
           session_obj->dtx_mode, session_obj->vocoder_type );
  }
  return rc;

}

static uint32_t gva_process_ul_packet_amr( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;
  vs_vocamr_frame_info_t* vs_vocamr_frame_info = NULL;
  gsm_ivocamr_frame_info_t* gsm_vocamr_frame_info = NULL;
  uint32_t frame_type;
  uint32_t codec_mode;

  for( ;; )
  {
    if ( vs_buffer->media_id != VS_COMMON_MEDIA_ID_AMR )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR: PROCESS_UL_PACKET: Invalid MediaId=(0x%08x)",
             vs_buffer->media_id );
      break;
    }

    vs_vocamr_frame_info = ( vs_vocamr_frame_info_t* ) vs_buffer->frame_info;
    gsm_vocamr_frame_info = ( gsm_ivocamr_frame_info_t* ) gsm_buffer->frame_info;

    /* Map VS to GSM frame type. */
    rc = gva_map_vocamr_frame_type_vs_to_gsm ( vs_vocamr_frame_info->frame_type,
                                               &frame_type );

    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR: PROCESS_UL_PACKET:(): Unsupported FrameType=(%d)",
             vs_vocamr_frame_info->frame_type );
      break;
    }

    /* Map VS to GSM codec mode. */
    rc = gva_map_vocamr_codec_mode_vs_to_gsm ( vs_vocamr_frame_info->codec_mode,
                                               &codec_mode);
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR: PROCESS_UL_PACKET: Unsupported FrameType=(%d)",
             vs_vocamr_frame_info->codec_mode );
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: AMR: PROCESS_UL_PACKET: FrameType=(%d), CodecMode=(%d)",
           vs_vocamr_frame_info->frame_type, vs_vocamr_frame_info->codec_mode );

    /* Update the vocoder id to UL GSM buffer. */
    gsm_buffer->vocoder_id = GSM_IVOCODER_ID_AMR;
    /* Update the vocoder frame type to UL GSM buffer. */
    gsm_vocamr_frame_info->frame_type = frame_type;
    /* Update the vocoder codec mode to UL GSM buffer. */
    gsm_vocamr_frame_info->codec_mode = codec_mode;
    /* Update the vocoder frame size to UL GSM buffer. */
    gsm_buffer->size = vs_buffer->size;
    /* Update the vocoder frame size to UL GSM buffer. */
    mmstd_memcpy( gsm_buffer->frame, vs_buffer->size,
                  vs_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}

static uint32_t gva_process_ul_packet_amrwb( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;
  vs_vocamrwb_frame_info_t* vs_vocamrwb_frame_info = NULL;
  gsm_ivocamrwb_frame_info_t* gsm_vocamrwb_frame_info = NULL;
  uint32_t frame_type;
  uint32_t codec_mode;

  for( ;; )
  {
    if ( vs_buffer->media_id != VS_COMMON_MEDIA_ID_AMRWB )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR-WB: PROCESS_UL_PACKET: Invalid MediaId=(0x%08x)",
             vs_buffer->media_id );
      break;
    }

    vs_vocamrwb_frame_info = ( vs_vocamrwb_frame_info_t* ) vs_buffer->frame_info;
    gsm_vocamrwb_frame_info = ( gsm_ivocamrwb_frame_info_t* ) gsm_buffer->frame_info;

    /* Map VS to GSM frame type. */
    rc = gva_map_vocamrwb_frame_type_vs_to_gsm( vs_vocamrwb_frame_info->frame_type,
                                                &frame_type );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR-WB: PROCESS_UL_PACKET:  Unsupported FrameType=(%d),",
             vs_vocamrwb_frame_info->frame_type );
      break;
    }

    /* Map VS to GSM codec mode. */
    rc = gva_map_vocamrwb_codec_mode_vs_to_gsm ( vs_vocamrwb_frame_info->codec_mode,
                                                 &codec_mode);
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR-WB: PROCESS_UL_PACKET: Unsupported FrameType=(%d),",
             vs_vocamrwb_frame_info->codec_mode );
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: AMR-WB: PROCESS_UL_PACKET: FrameType=(%d), CodecMode=(%d)",
           vs_vocamrwb_frame_info->frame_type, vs_vocamrwb_frame_info->codec_mode );

    /* Update the vocoder id to UL GSM buffer. */
    gsm_buffer->vocoder_id = GSM_IVOCODER_ID_AMRWB;
    /* Update the vocoder frame type to UL GSM buffer. */
    gsm_vocamrwb_frame_info->frame_type = frame_type;
    /* Update the vocoder codec mode to UL GSM buffer. */
    gsm_vocamrwb_frame_info->codec_mode = codec_mode;
    /* Update the vocoder frame size to UL GSM buffer. */
    gsm_buffer->size = vs_buffer->size;
    /* Update the vocoder frame size to UL GSM buffer. */
    mmstd_memcpy( gsm_buffer->frame, vs_buffer->size,
                  vs_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}

static uint32_t gva_process_ul_packet_efr( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;

  for( ;; )
  {
    if ( vs_buffer->media_id != VS_COMMON_MEDIA_ID_EFR )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: EFR: PROCESS_UL_PACKET: Invalid MediaId=(0x%08x)",
             vs_buffer->media_id );
      break;
    }

    /* Update the vocoder id to UL GSM buffer. */
    gsm_buffer->vocoder_id = GSM_IVOCODER_ID_EFR;
    /* Update the vocoder frame size to UL GSM buffer. */
    mmstd_memcpy( gsm_buffer->frame_info, sizeof( gsm_ivocefr_frame_info_t ),
                  vs_buffer->frame_info, sizeof( gsm_ivocefr_frame_info_t ) );
    /* Update the vocoder frame size to UL GSM buffer. */
    gsm_buffer->size = vs_buffer->size;
    /* Update the vocoder frame size to UL GSM buffer. */
    mmstd_memcpy( gsm_buffer->frame, vs_buffer->size,
                  vs_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}

static uint32_t gva_process_ul_packet_fr( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;

  for( ;; )
  {
    if ( vs_buffer->media_id != VS_COMMON_MEDIA_ID_FR )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: FR: PROCESS_UL_PACKET: Invalid MediaId=(0x%08x)",
             vs_buffer->media_id );
      break;
    }

    /* Update the vocoder id to UL GSM buffer. */
    gsm_buffer->vocoder_id = GSM_IVOCODER_ID_FR;
    /* Update the vocoder frame size to UL GSM buffer. */
    mmstd_memcpy( gsm_buffer->frame_info, sizeof( gsm_ivocfr_frame_info_t ),
                  vs_buffer->frame_info, sizeof( gsm_ivocfr_frame_info_t ) );
    /* Update the vocoder frame size to UL GSM buffer. */
    gsm_buffer->size = vs_buffer->size;
    /* Update the vocoder frame size to UL GSM buffer. */
    mmstd_memcpy( gsm_buffer->frame, vs_buffer->size,
                  vs_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}

static uint32_t gva_process_ul_packet_hr( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;

  for( ;; )
  {
    if ( vs_buffer->media_id != VS_COMMON_MEDIA_ID_HR )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: HR: PROCESS_UL_PACKET: Invalid MediaId=(0x%08x)",
             vs_buffer->media_id );
      break;
    }
  
    /* Update the vocoder id to UL GSM buffer. */
    gsm_buffer->vocoder_id = GSM_IVOCODER_ID_HR;
    /* Update the vocoder frame size to UL GSM buffer. */
    mmstd_memcpy( gsm_buffer->frame_info, sizeof( gsm_ivochr_frame_info_t ),
                  vs_buffer->frame_info, sizeof( gsm_ivochr_frame_info_t ) );
    /* Update the vocoder frame size to UL GSM buffer. */
    gsm_buffer->size = vs_buffer->size;
    /* Update the vocoder frame size to UL GSM buffer. */
    mmstd_memcpy( gsm_buffer->frame, vs_buffer->size,
                  vs_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}

static uint32_t gva_process_dl_packet_amr( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;
  gsm_ivocamr_frame_info_t* gsm_vocamr_frame_info = NULL;
  vs_vocamr_frame_info_t* vs_vocamr_frame_info = NULL;
  uint32_t frame_type;
  uint32_t codec_mode;

  for( ;; )
  {
    if ( gsm_buffer->vocoder_id != GSM_IVOCODER_ID_AMR )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR: PROCESS_DL_PACKET: Invalid MediaId=(0x%08x)",
             gsm_buffer->vocoder_id );
      break;
    }

    gsm_vocamr_frame_info = ( gsm_ivocamr_frame_info_t* ) gsm_buffer->frame_info;
    vs_vocamr_frame_info = ( vs_vocamr_frame_info_t* ) vs_buffer->frame_info;

    rc = gva_map_vocamr_frame_type_gsm_to_vs ( gsm_vocamr_frame_info->frame_type,
                                               &frame_type );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR: PROCESS_DL_PACKET: Unsupported FrameType=(%d),",
             gsm_vocamr_frame_info->frame_type );
      break;
    }

    rc = gva_map_vocamr_codec_mode_gsm_to_vs ( gsm_vocamr_frame_info->codec_mode,
                                               &codec_mode );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR: PROCESS_DL_PACKET: Unsupported CodecMode = (%d),",
             gsm_vocamr_frame_info->codec_mode );
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: AMR: PROCESS_DL_PACKET: FrameType=(%d), CodecMode=(%d)",
           frame_type, codec_mode );

    /* Update the media_id to VS buffer. */
    vs_buffer->media_id = VS_COMMON_MEDIA_ID_AMR;
    /* Update the vocoder frame type to VS buffer. */
    vs_vocamr_frame_info->frame_type = ( vs_vocamr_frame_type_t ) frame_type;
    /* Update the vocoder codec mode to VS  buffer. */
    vs_vocamr_frame_info->codec_mode = ( vs_vocamr_codec_mode_t ) codec_mode;
    /* Update the vocoder frame size to VS buffer. */
    vs_buffer->size = gsm_buffer->size;
    /* Update the vocoder frame size to VS buffer. */
    mmstd_memcpy( vs_buffer->frame, vs_buffer->size,
                  gsm_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}

static uint32_t gva_process_dl_packet_amrwb( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;

  gsm_ivocamrwb_frame_info_t* gsm_vocamrwb_frame_info = NULL;
  vs_vocamrwb_frame_info_t* vs_vocamrwb_frame_info = NULL;
  uint32_t frame_type;
  uint32_t codec_mode;

  for( ;; )
  {
    if ( gsm_buffer->vocoder_id != GSM_IVOCODER_ID_AMRWB )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR-WB: PROCESS_DL_PACKET: Invalid MediaId=(0x%08x)",
             gsm_buffer->vocoder_id );
      break;
    }

    gsm_vocamrwb_frame_info = ( gsm_ivocamrwb_frame_info_t* ) gsm_buffer->frame_info;
    vs_vocamrwb_frame_info = ( vs_vocamrwb_frame_info_t* ) vs_buffer->frame_info;

    rc = gva_map_vocamrwb_frame_type_gsm_to_vs ( gsm_vocamrwb_frame_info->frame_type,
                                             
   &frame_type );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR-WB: PROCESS_DL_PACKET: Unsupported FrameType=(%d),",
             gsm_vocamrwb_frame_info->frame_type );
      break;
    }

    rc = gva_map_vocamrwb_codec_mode_gsm_to_vs ( gsm_vocamrwb_frame_info->codec_mode,
                                                 &codec_mode );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: AMR-WB: PROCESS_DL_PACKET: Unsupported CodecMode=(%d),",
             gsm_vocamrwb_frame_info->codec_mode );
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: AMR-WB: PROCESS_DL_PACKET: FrameType=(%d), CodecMode=(%d)",
           frame_type, codec_mode );

    /* Update the media_id to VS buffer. */
    vs_buffer->media_id = VS_COMMON_MEDIA_ID_AMRWB;
    /* Update the vocoder frame type to VS buffer. */
    vs_vocamrwb_frame_info->frame_type = ( vs_vocamrwb_frame_type_t ) frame_type;
    /* Update the vocoder codec mode to VS  buffer. */
    vs_vocamrwb_frame_info->codec_mode = ( vs_vocamrwb_codec_mode_t ) codec_mode;
    /* Update the vocoder frame size to VS buffer. */
    vs_buffer->size = gsm_buffer->size;
    /* Update the vocoder frame size to VS buffer. */
    mmstd_memcpy( vs_buffer->frame, vs_buffer->size,
                  gsm_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}

static uint32_t gva_process_dl_packet_efr( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;

  for( ;; )
  {
    if ( gsm_buffer->vocoder_id != GSM_IVOCODER_ID_EFR )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: EFR: PROCESS_DL_PACKET: Invalid MediaId=(0x%08x)",
             gsm_buffer->vocoder_id );
      break;
    }

    /* Update the media_id to VS buffer. */
    vs_buffer->media_id = VS_COMMON_MEDIA_ID_EFR;
    /* Update the vocoder frame size to VS buffer. */
    mmstd_memcpy( vs_buffer->frame_info, sizeof( vs_vocefr_frame_info_t ),
                  gsm_buffer->frame_info, sizeof( vs_vocefr_frame_info_t ) );
    /* Update the vocoder frame size to VS buffer. */
    vs_buffer->size = gsm_buffer->size;
    /* Update the vocoder frame size to VS buffer. */
    mmstd_memcpy( vs_buffer->frame, vs_buffer->size,
                  gsm_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}

static uint32_t gva_process_dl_packet_fr( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;

  for( ;; )
  {
    if ( gsm_buffer->vocoder_id != GSM_IVOCODER_ID_FR )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: FR: PROCESS_DL_PACKET: Invalid MediaId=(0x%08x)",
             gsm_buffer->vocoder_id );
      break;
    }

    /* Update the media_id to VS buffer. */
    vs_buffer->media_id = VS_COMMON_MEDIA_ID_FR;
    /* Update the vocoder frame size to VS buffer. */
    mmstd_memcpy( vs_buffer->frame_info, sizeof( vs_vocfr_frame_info_t ),
                  gsm_buffer->frame_info, sizeof( vs_vocfr_frame_info_t ) );
    /* Update the vocoder frame size to VS buffer. */
    vs_buffer->size = gsm_buffer->size;
    /* Update the vocoder frame size to VS buffer. */
    mmstd_memcpy( vs_buffer->frame, vs_buffer->size,
                  gsm_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}

static uint32_t gva_process_dl_packet_hr( 
  gsm_ivocoder_buffer_t* gsm_buffer,
  vs_voc_buffer_t* vs_buffer
)
{
  uint32_t rc = APR_EOK;

  for( ;; )
  {
    if ( gsm_buffer->vocoder_id != GSM_IVOCODER_ID_HR )
    {
      rc = APR_EUNEXPECTED;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "GVA: HR: PROCESS_DL_PACKET: Invalid MediaId=(0x%08x)",
             gsm_buffer->vocoder_id );
      break;
    }

    /* Update the media_id to VS buffer. */
    vs_buffer->media_id = VS_COMMON_MEDIA_ID_HR;
    /* Update the vocoder frame size to VS buffer. */
    mmstd_memcpy( vs_buffer->frame_info, sizeof( vs_vochr_frame_info_t ),
                  gsm_buffer->frame_info, sizeof( vs_vochr_frame_info_t ) );
    /* Update the vocoder frame size to VS buffer. */
    vs_buffer->size = gsm_buffer->size;
    /* Update the vocoder frame size to VS buffer. */
    mmstd_memcpy( vs_buffer->frame, vs_buffer->size,
                  gsm_buffer->frame, vs_buffer->size );

    break;
  }

  return rc;
}


/****************************************************************************
 * GVA VS SESSION ROUTINES                                                  *
 ****************************************************************************/

static void gva_log_event_info(
  void* session_context,
  uint32_t event_id
)
{
  gva_modem_subs_object_t* subs_obj = ( gva_modem_subs_object_t* ) session_context;
  gva_session_object_t* session_obj = ( gva_session_object_t* ) session_context;

  if ( session_context == NULL ) return;

  switch( event_id )
  {
   case VS_COMMON_EVENT_CMD_RESPONSE:
   {
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "GVA: VSID=(0x%08x): VS_COMMON_EVENT_CMD_RESPONSE recieved",
            session_obj->vsid );
   }
   break;

   case VS_COMMON_EVENT_NOT_READY:
   {
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "GVA: VSID=(0x%08x): VS_COMMON_EVENT_NOT_READY recieved",
            session_obj->vsid );
   }
   break;

   case VS_COMMON_EVENT_READY:
   {
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "GVA: VSID=(0x%08x): VS_COMMON_EVENT_READY recieved",
            session_obj->vsid );
   }
   break;

   case VS_VOC_EVENT_READ_AVAILABLE:
   {
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
            "GVA: VSID=(0x%08x): VS_VOC_EVENT_READ_AVAILABLE recieved",
            session_obj->vsid );
   }
   break;

   case VS_VOC_EVENT_WRITE_BUFFER_RETURNED:
   {
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
            "GVA: VSID=(0x%08x): VS_VOC_EVENT_WRITE_BUFFER_RETURNED recieved",
            session_obj->vsid );
   }
   break;

   case VS_COMMOM_EVENT_EAMR_MODE_CHANGE:
   {
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "GVA: VSID=(0x%08x): VS_COMMOM_EVENT_EAMR_MODE_CHANGE recieved",
            session_obj->vsid );
   }
   break;

   case GSM_IVOICE_EVENT_START_REQUEST:
   {
     MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
            "GVA: ASID=(%d), VSID=(0x%08x): GSM_IVOICE_EVENT_REQUEST_START recieved",
            subs_obj->asid, subs_obj->vsid );
   }
   break;

   case GSM_IVOICE_EVENT_STOP_REQUEST:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "GVA: ASID=(%d), VSID=(0x%08x): GSM_IVOICE_EVENT_STOP_REQUEST recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

   case GSM_IVOICE_EVENT_SELECT_OWNER:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "GVA: ASID=(%d), VSID=(0x%08x): GSM_IVOICE_EVENT_SELECT_OWNER recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

   case GSM_IVOICE_EVENT_REQUEST_CODEC_MODE:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "GVA: ASID=(%d), VSID=(0x%08x): GSM_IVOICE_EVENT_REQUEST_CODEC_MODE recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

   case GSM_IVOICE_EVENT_REQUEST_DTX_MODE:
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "GVA: ASID=(%d), VSID=(0x%08x): GSM_IVOICE_EVENT_REQUEST_DTX_MODE recieved",
             subs_obj->asid, subs_obj->vsid );
    }
    break;

   default:
     break;
  }

  return;
}

static uint32_t gva_vs_event_cb(
 uint32_t event_id,
 void* params,
 uint32_t size,
 void* session_context
)
{
  uint32_t rc = APR_EOK;

  if ( gva_is_initialized == FALSE ) return APR_EOK;

  switch ( event_id )
  {
   case VS_COMMON_EVENT_CMD_RESPONSE:
   case VS_COMMON_EVENT_NOT_READY:
   case VS_COMMON_EVENT_READY:
   case VS_VOC_EVENT_READ_AVAILABLE:
   case VS_VOC_EVENT_WRITE_BUFFER_RETURNED:
   case VS_COMMOM_EVENT_EAMR_MODE_CHANGE:
    {
      (void) gva_log_event_info( ( gva_session_object_t* ) session_context,
                                 event_id );
      rc = gva_prepare_and_dispatch_event_packet( session_context, event_id,
                                                  params, size );
    }
    break;

   default:
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
            "gva_vs_event_cb(): Unsupported event (%d)", event_id );
     rc = APR_EFAILED;
  }

  return rc;
}

static uint32_t gva_vs_prime_read_buffer (
  gva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_prime_read_buffer_t prime_read_buf_cmd;

  prime_read_buf_cmd.handle = session_obj->vs_handle;
  prime_read_buf_cmd.buffer = session_obj->vs_read_buf;

  rc = vs_call( VS_VOC_CMD_PRIME_READ_BUFFER, ( void* ) &prime_read_buf_cmd,
                sizeof( prime_read_buf_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_vs_prime_read_buffer(): Failed to prime vs_read_buf, "
           "vs_read_buf=(0x%08x), rc=(0x%08x)", session_obj->vs_read_buf, rc );
  }
  else
  {
    session_obj->vs_read_buf = NULL;
  }

  return APR_EOK;
}

static uint32_t gva_vs_read_buffer (
  gva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_read_buffer_t read_buf_cmd;

  read_buf_cmd.handle = session_obj->vs_handle;
  read_buf_cmd.ret_buffer = &session_obj->vs_read_buf;

  rc = vs_call( VS_VOC_CMD_READ_BUFFER, ( void* )&read_buf_cmd,
                sizeof( read_buf_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_vs_read_buffer(): Failed to read vs_buffer, "
           "vs_read_buf=(0x%08x), rc=(0x%08x)", session_obj->vs_read_buf, rc );
  }
  else
  {
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "gva_vs_read_buffer(): read buffer available vs_read_buf=(0x%08x) ",
           session_obj->vs_read_buf );
  }

  return rc;
}

static uint32_t gva_vs_write_buffer (
  gva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_write_buffer_t write_buf_cmd;

  write_buf_cmd.handle = session_obj->vs_handle;
  write_buf_cmd.buffer = session_obj->vs_write_buf;

  rc = vs_call( VS_VOC_CMD_WRITE_BUFFER, ( void* )&write_buf_cmd,
                sizeof( write_buf_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_vs_write_buffer(): Failed to pass vs_write_buf, "
           "vs_write_buf=(0x%08x), rc=(0x%08x)", session_obj->vs_write_buf, rc );
  }
  else
  {
    session_obj->vs_write_buf = NULL;
  }

  return APR_EOK;
}

static uint32_t gva_vs_free_buffer (
  gva_session_object_t* session_obj
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_free_buffer_t free_buf_cmd;

  if ( session_obj->vs_read_buf != NULL )
  {
    free_buf_cmd.handle = session_obj->vs_handle;
    free_buf_cmd.buffer = session_obj->vs_read_buf;

    rc = vs_call( VS_VOC_CMD_FREE_BUFFER, ( void* )&free_buf_cmd,
                  sizeof( free_buf_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_free_buffer(): Failed to free vs_read_buf = (0x%08x), " 
             "rc = (0x%08x)", session_obj->vs_read_buf, rc );
    }
    session_obj->vs_read_buf = NULL;
    session_obj->primed_read_buf = NULL;
  }

  if ( session_obj->vs_write_buf != NULL )
  {
    free_buf_cmd.handle = session_obj->vs_handle;
    free_buf_cmd.buffer = session_obj->vs_write_buf;

    rc = vs_call( VS_VOC_CMD_FREE_BUFFER, ( void* )&free_buf_cmd,
                  sizeof( free_buf_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_free_buffer(): Failed to free vs_write_buf = (0x%08x), " 
             "rc = (0x%08x)", session_obj->vs_write_buf, rc );
    }
    session_obj->vs_write_buf = NULL;
  }

  return rc;
}

static uint32_t gva_vs_alloc_buffer (
  gva_session_object_t* session_obj
)
{
  uint32_t rc;
  vs_voc_cmd_alloc_buffer_t alloc_buf_cmd;

  for ( ;; )
  {
    /* Allocate read buffer. */
    session_obj->vs_read_buf = NULL;
    alloc_buf_cmd.handle = session_obj->vs_handle;
    alloc_buf_cmd.ret_buffer = &session_obj->vs_read_buf;
    alloc_buf_cmd.req_max_frame_size = GVA_MAX_VOC_FRAME_LENGTH;//322; 
    rc = vs_call( VS_VOC_CMD_ALLOC_BUFFER, ( void* )&alloc_buf_cmd,
                  sizeof( alloc_buf_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_alloc_buffer(): Failed to allocate read buffer, "
             "rc = (0x%08x)", rc );
      break;
    }
    session_obj->primed_read_buf = session_obj->vs_read_buf;

    /* Allocate write buffer. */
    session_obj->vs_write_buf = NULL;
    alloc_buf_cmd.handle = session_obj->vs_handle;
    alloc_buf_cmd.ret_buffer = &session_obj->vs_write_buf;
    alloc_buf_cmd.req_max_frame_size = GVA_MAX_VOC_FRAME_LENGTH;//322; 
    rc = vs_call( VS_VOC_CMD_ALLOC_BUFFER, ( void* )&alloc_buf_cmd,
                  sizeof( alloc_buf_cmd ) );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_alloc_buffer(): Failed to allocate write buffer, "
             "rc = (0x%08x)", rc );
      ( void ) gva_vs_free_buffer ( session_obj );
      break;
    }

    break;
  }

  return rc;
}

static uint32_t gva_vs_close_session (
  gva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_close_t close_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }

    /* Free read and write buffers. */
    ( void ) gva_vs_free_buffer ( session_obj );

    close_cmd.handle = session_obj->vs_handle;
    close_cmd.client_context = client_context;

    rc = vs_call( VS_VOC_CMD_CLOSE, (void*)&close_cmd, sizeof( close_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_close_session(): Failed to close VS session, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      session_obj->vs_handle = APR_NULL_V;
      rc = APR_EPENDING;
    }

    break;
  }

  return rc;
}


static uint32_t gva_vs_open_session (
 gva_session_object_t* session_obj
)
{
  uint32_t rc;
  vs_voc_cmd_open_t open_cmd;

  for ( ;; )
  {
    /* Open VS session. */
    open_cmd.ret_handle = &session_obj->vs_handle;
    open_cmd.vsid = session_obj->vsid;
    open_cmd.client_id = VS_VOC_CLIENT_GSM;
    open_cmd.session_context = ( void* )session_obj;
    open_cmd.event_cb = gva_vs_event_cb;

    rc = vs_call( VS_VOC_CMD_OPEN, (void*)&open_cmd, sizeof( open_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_open_session(): failed to open VS session, "
             "client = (0x%08x), rc = (0x%08x)", open_cmd.client_id, rc );
      break;
    }

    rc = gva_vs_alloc_buffer( session_obj );
    if ( rc )
    {
      ( void ) gva_vs_close_session ( session_obj, NULL );
      
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_open_session(): failed to alloc VS buffers, "
             "client = (0x%08x), rc = (0x%08x)", open_cmd.client_id, rc );
      break;
    }

    break;
  }

  return rc;
}


static uint32_t gva_vs_flush_buffers (
  gva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_flush_buffers_t vs_flush_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }
  
    vs_flush_cmd.handle = session_obj->vs_handle;
    vs_flush_cmd.client_context = client_context;
    rc = vs_call( VS_VOC_CMD_FLUSH_BUFFERS, &vs_flush_cmd,
                  sizeof( vs_flush_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_flush_buffers(): Failed to flush VS buffers, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      rc = APR_EPENDING;
    }
  
    break;
  } /* For loop ends here. */
  
  return rc;
}


static uint32_t gva_vs_disable_vocoder (
  gva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  vs_voc_cmd_disable_t vs_disable_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }

    vs_disable_cmd.handle = session_obj->vs_handle;
    vs_disable_cmd.client_context = client_context;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: VS_VOC_CMD_DISABLE(): VocoderId=(0x%08x)",
           gva_map_vocoder_type_gsm_to_vs( session_obj->vocoder_type ) );
    rc = vs_call( VS_VOC_CMD_DISABLE, &vs_disable_cmd,
                  sizeof( vs_disable_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_disable_vocoder(): Failed to disable VS session, " 
             "handle = (0x%08x), rc = (0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      rc = APR_EPENDING;
    }
   
    break;
  }/* For loop ends here. */

  return rc;
}


static uint32_t gva_vs_enable_vocoder (
  gva_session_object_t* session_obj,
  void* client_context
)
{
  uint32_t rc = APR_EOK;
  uint32_t media_id;
  vs_voc_cmd_enable_t vs_enable_cmd;

  for ( ;; )
  {
    if ( session_obj->vs_handle == APR_NULL_V )
    {
      rc = APR_EOK;
      break;
    }

    media_id = gva_map_vocoder_type_gsm_to_vs ( session_obj->vocoder_type );
    vs_enable_cmd.handle = session_obj->vs_handle;
    vs_enable_cmd.media_id = media_id;
    vs_enable_cmd.client_context = ( void* ) client_context;
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: VS_VOC_CMD_ENABLE(): vocoder=(0x%08x)", vs_enable_cmd.media_id );
    rc = vs_call( VS_VOC_CMD_ENABLE, &vs_enable_cmd,
                  sizeof( vs_enable_cmd ) );
    if ( rc )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_vs_enable_vocoder(): Failed to enable VS session, " 
             "handle=(0x%08x), rc=(0x%08x)", session_obj->vs_handle, rc );
    }
    else
    {
      rc = APR_EPENDING;
    }

    break;
  }/* For loop ends here. */

  return rc;
}


/****************************************************************************
 * GVA GSM SESSION ROUTINES                                                 *
 ****************************************************************************/

static uint32_t gva_gsm_event_cb(
 void* session_context,
 uint32_t event_id,
 void* params,
 uint32_t size
)
{
  uint32_t rc = APR_EOK;

  switch ( event_id )
  {
   case GSM_IVOICE_EVENT_START_REQUEST:
   case GSM_IVOICE_EVENT_STOP_REQUEST:
   case GSM_IVOICE_EVENT_SELECT_OWNER:
   case GSM_IVOICE_EVENT_REQUEST_CODEC_MODE:
   case GSM_IVOICE_EVENT_REQUEST_DTX_MODE:
    (void) gva_log_event_info( session_context, event_id );
     rc = gva_prepare_and_dispatch_event_packet( session_context, event_id,
                                                 params, size );
     break;

   default:
     MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
            "GVA: Unsupported EventId=(%d)", event_id );
     rc = APR_EFAILED;
  }

  return rc;
}

static void gva_gsm_vocoder_ul_cb(
 void* session_context,
 gsm_ivocoder_buffer_t* gsm_buffer
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;

  subs_obj = ( gva_modem_subs_object_t* )session_context;
  if( subs_obj == NULL ) return;

  session_obj = ( gva_session_object_t* ) subs_obj->session_obj;
  if( session_obj == NULL ) return;

  gsm_buffer->flags = FALSE;

  GVA_ACQUIRE_LOCK( session_obj->data_lock );

  if ( ( session_obj->is_vs_ready == TRUE ) &&
       ( session_obj->vs_read_buf != NULL ) )
  {
    switch ( session_obj->vocoder_type )
    {
      case GSM_IVOCODER_ID_AMR:
        rc = gva_process_ul_packet_amr( gsm_buffer, session_obj->vs_read_buf );
        break;

      case GSM_IVOCODER_ID_AMRWB:
        rc = gva_process_ul_packet_amrwb( gsm_buffer, session_obj->vs_read_buf );
        break;

      case GSM_IVOCODER_ID_EFR:
        rc = gva_process_ul_packet_efr( gsm_buffer, session_obj->vs_read_buf );
        break;

      case GSM_IVOCODER_ID_FR:
        rc = gva_process_ul_packet_fr( gsm_buffer, session_obj->vs_read_buf );
        break;

      case GSM_IVOCODER_ID_HR:
        rc = gva_process_ul_packet_hr( gsm_buffer, session_obj->vs_read_buf );
        break;

      default:
        rc = APR_EUNEXPECTED;
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "GVA: Unsupported VocoderID=(0x%08X)", session_obj->vocoder_type );
        break;
    }

    if ( rc == APR_EOK )
    {
      /* UL GSM buffer successfully populated. */
      gsm_buffer->flags = TRUE;
    }

    /* Prime vs read buffer with voice services for next uplink packet. */
    rc = gva_vs_prime_read_buffer( session_obj );
  }
  else
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "GVA: UL vocoder Packet not transfered to GSM, is_vs_ready=(%d), "
           "vs_read_buf=(0x%08x)", session_obj->is_vs_ready, session_obj->vs_read_buf );
  }

  GVA_RELEASE_LOCK( session_obj->data_lock );

  return;
}

static void gva_gsm_vocoder_dl_cb (
  void* session_context,
  gsm_ivocoder_buffer_t* gsm_buffer
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;

  subs_obj = ( gva_modem_subs_object_t* )session_context;
  if( subs_obj == NULL ) return;

  session_obj = ( gva_session_object_t* ) subs_obj->session_obj;
  if( session_obj == NULL ) return;

  GVA_ACQUIRE_LOCK( session_obj->data_lock );

  if ( ( session_obj->is_vs_ready == TRUE ) && ( gsm_buffer->flags == TRUE ) &&
       ( session_obj->vs_write_buf != NULL ) )
  {
    /**
     * TODO: Pass the vocoder packet content avaialble form GSM protocol 
     * software to VS.
     */
    switch ( session_obj->vocoder_type )
    {
      case GSM_IVOCODER_ID_AMR:
        rc = gva_process_dl_packet_amr( gsm_buffer, session_obj->vs_write_buf );
        break;

      case GSM_IVOCODER_ID_AMRWB:
        rc = gva_process_dl_packet_amrwb( gsm_buffer, session_obj->vs_write_buf );
        break;

      case GSM_IVOCODER_ID_EFR:
        rc = gva_process_dl_packet_efr( gsm_buffer, session_obj->vs_write_buf );
        break;

      case GSM_IVOCODER_ID_FR:
        rc = gva_process_dl_packet_fr( gsm_buffer, session_obj->vs_write_buf );
        break;

      case GSM_IVOCODER_ID_HR:
        rc = gva_process_dl_packet_hr( gsm_buffer, session_obj->vs_write_buf );
        break;

      default:
        rc = APR_EUNEXPECTED;
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "GVA: Unsupported VocoderID=(0x%08X)", session_obj->vocoder_type );
        break;
    }

    /**
     * Pass write buffer to VS, if dl packet processed successfully.
     *
     * TODO: Add internal event to write the buffer to VS/log the vocoder 
     *       packet.
     */
    if ( rc == APR_EOK )
    {
      rc = gva_vs_write_buffer( session_obj );
    }
  }
  else
  {
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "GVA: DL vocoder buffer not tranfered to VS, is_vs_ready = (%d), "
           "gsm_buffer->flags=(0X%08X), vs_write_buf=(0X%08X)",
           session_obj->is_vs_ready, gsm_buffer->flags, session_obj->vs_write_buf );
  }

  GVA_RELEASE_LOCK( session_obj->data_lock );

  return;
}

static uint32_t gva_gsm_open_session (
  gva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  gsm_ivoice_cmd_open_t open_cmd;

  if ( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "gva_gsm_open_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  open_cmd.ret_handle = &subs_obj->gsm_handle;
  open_cmd.asid = subs_obj->asid;
  open_cmd.event_cb = gva_gsm_event_cb;
  open_cmd.ul_cb = gva_gsm_vocoder_ul_cb;
  open_cmd.dl_cb = gva_gsm_vocoder_dl_cb;  
  open_cmd.session_data = ( void* ) subs_obj;

#ifndef WINSIM
  rc = gsm_call ( GSM_IVOICE_CMD_OPEN, &open_cmd, sizeof( open_cmd ) );
  if ( APR_EUNSUPPORTED == rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_gsm_open_session(): GERAN is UNSUPPORTED, asid = (0x%08x), "
           "rc = (0x%08x)", open_cmd.asid, rc );
    rc = APR_EOK;
  }
  else if ( APR_EOK != rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_gsm_open_session(): Failed to OPEN GERAN session "
           "asid = (0x%08x), rc = (0x%08x)", open_cmd.asid, rc );    
  }
  else
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "gva_gsm_open_session(): GERAN session successfully opened, "
           "asid = (0x%08x), rc = (0x%08x)", open_cmd.asid, rc );
  }
#endif

  return rc;
}

static uint32_t gva_gsm_close_session (
  gva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  gsm_ivoice_cmd_close_t close_cmd;

  if ( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "gva_gsm_close_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  close_cmd.handle = subs_obj->gsm_handle;

#ifndef WINSIM
  rc = gsm_call( GSM_IVOICE_CMD_CLOSE, &close_cmd, sizeof( close_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_gsm_close_session(): failed to close GSM session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}

static uint32_t gva_gsm_start_session (
  gva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  gsm_ivoice_cmd_start_t start_cmd;

  if ( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "gva_gsm_start_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  start_cmd.handle = subs_obj->gsm_handle;

#ifndef WINSIM
  rc = gsm_call ( GSM_IVOICE_CMD_START, &start_cmd, sizeof( start_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_gsm_start_session(): failed to start GSM session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}

static uint32_t gva_gsm_stop_session (
  gva_modem_subs_object_t* subs_obj
)
{
  uint32_t rc = APR_EOK;
  gsm_ivoice_cmd_stop_t stop_cmd;

  if ( NULL == subs_obj )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
         "gva_gsm_stop_session(): subs_obj is NULL" );
    return APR_EBADPARAM;
  }

  stop_cmd.handle = subs_obj->gsm_handle;

#ifndef WINSIM
  rc = gsm_call ( GSM_IVOICE_CMD_STOP, &stop_cmd, sizeof( stop_cmd ) );
  if ( rc )
  {
    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_gsm_stop_session(): failed to stop GSM session, "
           "asid = (0x%08x), rc = (0x%08x)", subs_obj->asid, rc );
  }
#endif

  return rc;
}

/****************************************************************************
 * GVA CMDs/EVENTs HANDLING ROUTINES                                        *
 ****************************************************************************/

static uint32_t gva_process_vs_cmd_response_event( 
 gva_event_packet_t* packet
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  gva_simple_job_object_t* simple_obj = NULL;
  vs_common_event_cmd_response_t* evt_params = NULL;

  for ( ;; )
  {
    session_obj = ( gva_session_object_t* ) packet->session_context;
    if( session_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( ( vs_common_event_cmd_response_t* ) packet->params );
    if( evt_params == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "gva_process_vs_cmd_response_event(): cmd response event recieved for "
           "VS cmd_id=(0x%08x), client_context=(0x%08x), status=(0x%08X)",
           evt_params->cmd_id, evt_params->client_context, evt_params->status_id );

    simple_obj = ( gva_simple_job_object_t* ) evt_params->client_context;
    if ( simple_obj == NULL ) break;

      simple_obj->is_completed = TRUE;
      simple_obj->status = evt_params->status_id;

    break;
  }

  gva_free_event_packet( packet );

  return rc;
}


static uint32_t gva_process_vs_ready_event( 
 gva_gating_control_t* ctrl
)
{
  uint32_t rc = APR_EOK;
  gva_event_packet_t* event_pkt = NULL;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( gva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
    
    session_obj = ( gva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    subs_obj = session_obj->active_subs_obj;
    if( NULL != subs_obj )
    {
      /* Publish vocoder state for voice call on a subscription. */
      session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_READY;
      voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                             session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_GSM,
                             gva_map_vocoder_type_gsm_to_vs( session_obj->vocoder_type ) );
    }

    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "gva_process_vs_ready_event(): vocoder session ready for packet "
         "exchnage" );

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    session_obj->is_vs_ready = TRUE;

    /* Prime the Ready buffer with VS for UL packets. */
    rc = gva_vs_prime_read_buffer( session_obj );
    
    GVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  gva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t gva_process_vs_not_ready_event( 
 gva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  gva_event_packet_t* event_pkt = NULL;
  gva_session_object_t* session_obj = NULL;
  gva_simple_job_object_t* simple_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( gva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
  
    session_obj = ( gva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    subs_obj = session_obj->active_subs_obj;
    if( NULL != subs_obj )
    {
      /* Publish vocoder state for voice call on a subscription.
       * This state shall be publshed only during device switch scenrio's.
       * During call end RELEASED is published.
       */
      session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_NOT_READY;
      voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                             session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_GSM,
                             gva_map_vocoder_type_gsm_to_vs( session_obj->vocoder_type ) );
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );
    
    if ( ctrl->state == GVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "gva_process_vs_not_ready_event(): for vsid=(0x%08x) ", 
             session_obj->vsid );

      session_obj->is_vs_ready = FALSE;

      /* FLUSH BUFFERS when voice services are not ready for vocoder 
         packet exchnage. */
      rc = gva_create_simple_job_object( session_obj->header.handle,
             ( gva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj= &ctrl->rootjob_obj->simple_job;

      rc = gva_vs_flush_buffers( session_obj, (void*)simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
      }
      else
      {
        /* Restore the read buffer reference if not returned from VS. */
        session_obj->vs_read_buf = session_obj->primed_read_buf;
      }
    }

    break;
  }

  if( session_obj != NULL )
  {
   GVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc == APR_EOK )
  {
   ( void ) gva_mem_free_object ( ( gva_object_t*) simple_obj );
   ( void ) gva_free_event_packet ( event_pkt );
  }

  return rc;
}


static uint32_t gva_process_vs_open_event ( 
 gva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  gva_event_packet_t* event_pkt = NULL;
  gva_session_object_t* session_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( gva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
    
    session_obj = ( gva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "gva_process_vs_open_event(): for session vsid = (0x%08X)", 
           session_obj->vsid );

    rc = gva_vs_open_session( session_obj );

    GVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  gva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t gva_process_vs_close_event ( 
 gva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  gva_event_packet_t* event_pkt = NULL;
  gva_session_object_t* session_obj = NULL;
  gva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( gva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }
    
    session_obj = ( gva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == GVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "gva_process_vs_close_event(): for session vsid = (0x%08x)", 
             session_obj->vsid );

      rc = gva_create_simple_job_object( session_obj->header.handle,
             ( gva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj = &ctrl->rootjob_obj->simple_job;

      rc = gva_vs_close_session( session_obj, (void*) simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
      }
      else
      {
        session_obj->vs_handle = APR_NULL_V;
      }
    }

    break;
  }

  if( session_obj != NULL )
  {
   GVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc != APR_EPENDING )
  {
    ( void ) gva_mem_free_object ( ( gva_object_t*) simple_obj );
    ( void ) gva_free_event_packet ( event_pkt );
    rc = APR_EOK;
  }

  return rc;
}


static uint32_t gva_process_vs_eamr_rate_change_event( 
 gva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  vs_common_event_eamr_mode_t* evt_params = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;
  gsm_ivoice_cmd_send_sample_rate_t sample_rate_cmd;

  for( ;; )
  {
    session_obj = ( gva_session_object_t* ) event_pkt->session_context;
    if( session_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( ( vs_common_event_eamr_mode_t* ) event_pkt->params );
    if( evt_params == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    if( session_obj->vocoder_type != GSM_IVOCODER_ID_AMR )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "gva_process_vs_eamr_rate_change_event(): Not Applicable for "
             "vocoder_type=0x%08x", session_obj->vocoder_type );
      break;
    }

    switch ( evt_params->mode )
    {
      case VS_COMMON_EAMR_MODE_NARROWBAND:
        sample_rate_cmd.sample_rate = 8000;
        break;

      case VS_COMMON_EAMR_MODE_WIDEBAND:
        sample_rate_cmd.sample_rate = 16000;
        break;

      default:
        sample_rate_cmd.sample_rate = 0;
        break;
    }

    if( ( evt_params->mode != VS_COMMON_EAMR_MODE_NARROWBAND ) ||
        ( evt_params->mode != VS_COMMON_EAMR_MODE_WIDEBAND ) )
    {
      subs_obj = session_obj->active_subs_obj;
      sample_rate_cmd.handle = subs_obj->gsm_handle;
      sample_rate_cmd.vocoder_id = session_obj->vocoder_type;

#ifndef WINSIM
      rc = gsm_call( GSM_IVOICE_CMD_SEND_SAMPLE_RATE,
                     &sample_rate_cmd, sizeof( sample_rate_cmd ) );
#endif
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "gva_process_vs_eamr_rate_change_event(): for session index=%d "
           "eamr mode=%d, sample_rate=%d", subs_obj->asid, evt_params->mode,
           sample_rate_cmd.sample_rate );

    break;
  }

  gva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t gva_process_vs_read_buf_available_event( 
 gva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;
  voicelog_ipacket_cmd_commit_data_t log_cmd_param;

  for ( ;; )
  {
    session_obj = ( gva_session_object_t* ) event_pkt->session_context;
    if ( NULL == session_obj )
    {
      GVA_REPORT_FATAL_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    subs_obj = session_obj->active_subs_obj;
    if ( ( NULL != subs_obj ) &&
         ( VOICELOG_IEVENT_VOCODER_STATE_RUNNING != session_obj->vocoder_state ) )
    {
      /* Publish vocoder state for voice call on a subscription. */
      session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_RUNNING;
      voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                             session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_GSM,
                             gva_map_vocoder_type_gsm_to_vs( session_obj->vocoder_type ) );
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    /* Read the vocoder buffer from VS has returned. */
    ( void ) gva_vs_read_buffer ( session_obj );
    if( ( APR_EOK != rc ) || ( NULL == session_obj->vs_read_buf ) )
    {
      GVA_REPORT_FATAL_ON_ERROR( APR_EUNEXPECTED );
      break;
    }

    /* Convert frame header info*/
    rc = voicelog_get_frame_header( session_obj->vs_read_buf->media_id,
                                    session_obj->vs_read_buf->frame_info,
                                    &log_cmd_param.frame_header );

    if( APR_EOK != rc )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_process_vs_read_buf_available_event(): Invalid Frame header " );
      rc = APR_EOK;
      break;
    }
 
    log_cmd_param.version = VOICELOG_IPACKET_VOCODER_DATA_VERSION_V;
    log_cmd_param.log_code = VOICELOG_IPACKET_CODE_UL_VOCODER_PACKET;
    log_cmd_param.vsid = session_obj->vsid;
    log_cmd_param.network_id = VOICELOG_ICOMMON_NETWORK_ID_GSM;
    log_cmd_param.timestamp = session_obj->vs_read_buf->timestamp;
    log_cmd_param.tap_point_id = VOICELOG_IPACKET_TAP_POINT_ID_VOICE_ADAPTER;
    log_cmd_param.media_id = session_obj->vs_read_buf->media_id;
    log_cmd_param.frame = session_obj->vs_read_buf->frame;
    log_cmd_param.frame_size = session_obj->vs_read_buf->size;
    log_cmd_param.data_extension = NULL;
  
    /* Log data. */
    ( void ) voicelog_call ( VOICELOG_IPACKET_CMD_COMMIT_DATA,
                             ( void* )&log_cmd_param, sizeof( log_cmd_param ) );

    break;
  }

  if( NULL != session_obj )  GVA_RELEASE_LOCK( session_obj->data_lock );

  gva_free_event_packet( event_pkt );

  return rc;
}


static uint32_t gva_process_vs_write_buf_returned_event( 
 gva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  vs_voc_event_write_buffer_returned_t* evt_params = NULL;

  for ( ;; )
  {
    session_obj = ( gva_session_object_t* ) event_pkt->session_context;

    evt_params = ( vs_voc_event_write_buffer_returned_t* ) event_pkt->params;
    if ( evt_params == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_process_vs_write_buf_returned_event(): evt_paramsr is NULL" );
      break;
    }

    if ( evt_params->buffer == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_process_vs_write_buf_returned_event(): ret_buffer is NULL" );
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );
    
    /* Indicates that the vocoder buffer written to voice services has been 
     * successfully rendered to DSP.
     * Write buffer returned for next downlink vocoder packet.
     */
    session_obj->vs_write_buf = evt_params->buffer;

    GVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  gva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t gva_process_gsm_dtx_mode_event( 
 gva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;
  gsm_ivoice_event_request_dtx_mode_t* evt_params = NULL;

  for( ;; )
  {
    evt_params = ( gsm_ivoice_event_request_dtx_mode_t* ) event_pkt->params;
    if( evt_params == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    subs_obj = ( gva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = ( gva_session_object_t* ) subs_obj->session_obj;
    if( session_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    session_obj->dtx_mode = evt_params->enable_flag;
    ( void ) gva_set_voc_dtx_mode ( session_obj );

    GVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  gva_free_event_packet ( event_pkt );

  return rc;
}

static uint32_t gva_process_gsm_select_owner_event( 
 gva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;

  for ( ;; )
  {
    subs_obj = ( gva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = ( gva_session_object_t* ) subs_obj->session_obj;
    if( session_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    /* FOR IRAT-HO Scenrios,request voice agent for resource grant. */
    if ( ( session_obj->is_resource_granted == FALSE ) &&
         ( session_obj->va_gva_event_cb != NULL ) )
    {
      session_obj->va_gva_event_cb( session_obj->va_session_context,
                                    GVA_IRESOURCE_EVENT_REQUEST, NULL, 0 );
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "gva_process_gsm_select_owner_event() - Resource requested from "
           "voice agent" );
    }

    GVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  gva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t gva_process_gsm_codec_mode_event( 
 gva_event_packet_t* event_pkt 
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;
  gsm_ivoice_event_request_codec_mode_t* evt_params = NULL;

  for( ;; )
  {
    subs_obj = ( gva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    evt_params = ( gsm_ivoice_event_request_codec_mode_t* ) event_pkt->params;
    if( evt_params == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    session_obj = ( gva_session_object_t* ) subs_obj->session_obj;
    if( session_obj == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    session_obj->codec_mode = evt_params->codec_mode;
    ( void ) gva_set_voc_codec_mode ( session_obj );

    GVA_RELEASE_LOCK( session_obj->data_lock );

    break;
  }

  gva_free_event_packet ( event_pkt );

  return rc;
}


static uint32_t gva_process_gsm_vocoder_start_event( 
 gva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  gva_event_packet_t* event_pkt = NULL;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;
  gsm_ivoice_event_start_request_t* evt_params = NULL;
  gva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    event_pkt = ( gva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      rc = APR_EUNEXPECTED;
      GVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    evt_params = ( gsm_ivoice_event_start_request_t* ) event_pkt->params;
    if( evt_params == NULL )
    {
      rc = APR_EUNEXPECTED;
      GVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    subs_obj = ( gva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      GVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    session_obj = ( gva_session_object_t* ) subs_obj->session_obj;
    if( session_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      GVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == GVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "gva_process_gsm_vocoder_start_event() - GSM configured with "
             "vocoder_id = (0x%08x), new vocoder id = (0x%08x)",
             session_obj->vocoder_type, evt_params->vocoder_id );

      if ( subs_obj->is_gsm_ready == TRUE )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "gva_process_gsm_vocoder_start_event() - GSM already requested " );
        break;
      }

      if( gva_validate_gsm_vocoder_id( evt_params->vocoder_id ) == FALSE )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
               "gva_process_gsm_vocoder_start_event() - Invalid vocoder Id",
               evt_params->vocoder_id );
        break;
      }

      /* Update the GVA call state. */
      subs_obj->is_gsm_ready = TRUE;
      session_obj->vocoder_type = evt_params->vocoder_id;
      session_obj->active_subs_obj = subs_obj;

      /* Update Vocoder Session Number for vocoder logging purpose. */
      ( void ) voicelog_session_update ( session_obj->vsid,
                                         VOICELOG_INFO_ENUM_VOCODER_SESSION_NUMBER);


      if ( ( session_obj->is_resource_granted == FALSE ) &&
           ( session_obj->va_gva_event_cb != NULL ) )
      {
        /* Request for voice resource. */
        session_obj->va_gva_event_cb( session_obj->va_session_context,
                                      GVA_IRESOURCE_EVENT_REQUEST, NULL, 0 );

        /* Publish vocoder state for voice call on a subscription. */
        session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_REQUESTED;
        voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                               session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_GSM,
                               gva_map_vocoder_type_gsm_to_vs( session_obj->vocoder_type ) );
        rc = APR_EOK;
        break;
      }

      /* Create the Simple job object to track VS_ENABLE. */
      rc = gva_create_simple_job_object( session_obj->header.handle,
             ( gva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj = &ctrl->rootjob_obj->simple_job;
      
      /* Set cached vocoder properties. */
      ( void ) gva_set_voc_dtx_mode( session_obj );
      ( void ) gva_set_voc_codec_mode( session_obj );
      
      rc = gva_vs_enable_vocoder( session_obj, simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }

      /* Send a START CMD notification to GSM. */
      ( void ) gva_gsm_start_session ( subs_obj );
    }

    break;
  }

  /* If the return code is not APR_EPENDING it confirm that nothing is left for
   * hence memory associated to CMD packet, EVENT packet and JOB object created
   * shall be relesed. Control shall not return to this handler for a perticular
   * Insance of event. */
  if ( APR_EPENDING != rc ) 
  {
    rc = APR_EOK;

    /* Free CMD/EVT packet memory. */
    ( void ) gva_mem_free_object ( ( gva_object_t*) simple_obj );
    ( void ) gva_free_event_packet ( event_pkt );
  }

  if ( session_obj != NULL )
  {
    GVA_RELEASE_LOCK( session_obj->data_lock );
  }

  return rc;
}


static uint32_t gva_process_gsm_vocoder_stop_event( 
 gva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  gva_event_packet_t* event_pkt = NULL;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;
  gva_simple_job_object_t* simple_obj = NULL;
  gva_icommon_cmd_set_asid_vsid_mapping_t gva_map_cmd;

  for ( ;; )
  {
    event_pkt = ( gva_event_packet_t* ) ctrl->packet;
    if( event_pkt == NULL )
    {
      rc = APR_EUNEXPECTED;
      GVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    subs_obj = ( gva_modem_subs_object_t* ) event_pkt->session_context;
    if( subs_obj == NULL )
    {
      rc  = APR_EUNEXPECTED;
      GVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    session_obj = ( gva_session_object_t* ) subs_obj->session_obj;
    if( session_obj == NULL )
    {
      rc = APR_EUNEXPECTED;
      GVA_REPORT_FATAL_ON_ERROR ( rc );
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == GVA_GATING_CMD_STATE_EXECUTE )
    {
      if ( FALSE == subs_obj->is_gsm_ready )
      {
        MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "gsm_vocoder_stop_event(PROCESS) - vocoder not started yet." );
        break;
      }

      /* Update the GVA call state. */
      subs_obj->is_gsm_ready = FALSE;
      /* Invalidate the active subscrition object. */
      session_obj->active_subs_obj = NULL;

      if ( TRUE == session_obj->is_resource_granted )
      {
        /* Create the Simple job object to track CVD setup. */
        rc = gva_create_simple_job_object( session_obj->header.handle,
               ( gva_simple_job_object_t** ) &ctrl->rootjob_obj );
        simple_obj = &ctrl->rootjob_obj->simple_job;

        rc = gva_vs_disable_vocoder( session_obj, simple_obj );
      }
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }
    }

    break;
  }

  if ( APR_EOK == rc )
  {
    /* Send voice resource released event to voice agent.
     * "REQUEST_STOP sent by GERAN-L1 without sending REQUEST_START" 
     * This can happen if GERAN-L1 called SELECT_OWNER during interRAT 
     * handover start. However the handover failed afterwards. So, GERAN-L1 
     * did not call REQUEST_START, instead directly called REQUEST_STOP 
     * to indicate GVA that it no longer required vocoder.
     */
    if( session_obj->va_gva_event_cb != NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "gsm_vocoder_stop_event(PROCESS) - RELEASED event sent to VA. "
             "grant_status=(%d)", session_obj->is_resource_granted );
      session_obj->is_resource_granted = FALSE;
      session_obj->va_gva_event_cb( session_obj->va_session_context, 
                                    GVA_IRESOURCE_EVENT_RELEASED, NULL, 0 );
    }

    /* Queue a cmd if a ASID-VSID mapping is waiting to be processed. */
    if ( GVA_VSID_UNDEFINED_V != subs_obj->pending_vsid )
    {
      gva_map_cmd.asid = subs_obj->asid;
      gva_map_cmd.vsid = subs_obj->pending_vsid;
      subs_obj->pending_vsid = GVA_VSID_UNDEFINED_V;
      gva_prepare_and_dispatch_cmd_packet ( GVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING, 
                                            (void*)&gva_map_cmd,  sizeof( gva_map_cmd ) );
    }

    /* Publish vocoder state for voice call on a subscription. */
    if(  VOICELOG_IEVENT_VOCODER_STATE_RELEASED != session_obj->vocoder_state )
    {
      session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_RELEASED;
      voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                             session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_GSM,
                             gva_map_vocoder_type_gsm_to_vs( session_obj->vocoder_type ) );
    }
  }

  /* If the return code is not APR_EPENDING it confirm that nothing is left for
   * hence memory associated to CMD packet, EVENT packet and JOB object created
   * shall be relesed. Control shall not return to this handler for a perticular
   * Insance of event. */
  if ( APR_EPENDING != rc )
  {
    rc = APR_EOK;

    /* Send stop notification to GSM. */ 
    ( void ) gva_gsm_stop_session ( subs_obj );

    /* Free CMD/EVT packet memory. */
    ( void ) gva_mem_free_object ( ( gva_object_t*) simple_obj );
    ( void ) gva_free_event_packet ( event_pkt );
  }

  if( session_obj != NULL )
  {
    GVA_RELEASE_LOCK( session_obj->data_lock );
  }

  return rc;
}


static uint32_t gva_process_resource_grant_cmd( 
 gva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  gva_cmd_packet_t* cmd_pkt = NULL;
  gva_session_object_t* session_obj = NULL;
  gva_modem_subs_object_t* subs_obj = NULL;
  gva_iresource_cmd_grant_t* cmd_params = NULL;
  gva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    cmd_pkt = ( gva_cmd_packet_t* ) ctrl->packet;
    if( cmd_pkt == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    cmd_params = ( gva_iresource_cmd_grant_t* ) cmd_pkt->params;
    if( cmd_params == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = gva_get_object ( cmd_params->handle, ( gva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_process_resource_grant_cmd(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == GVA_GATING_CMD_STATE_EXECUTE )
    {
      session_obj->is_resource_granted = TRUE;
      subs_obj = session_obj->active_subs_obj;

      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "gva_process_resource_grant_cmd() - vocoder granted" );

      if ( ( subs_obj == NULL ) || ( subs_obj->is_gsm_ready == FALSE ) )
       {
         MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
              "gva_process_resource_grant_cmd() - Traffic start request not "
              "available from GSM" );
         break;
       }

       /* Publish vocoder state for voice call on a subscription. */
       session_obj->vocoder_state = VOICELOG_IEVENT_VOCODER_STATE_GRANTED;
       voicelog_event_report( subs_obj->asid, subs_obj->vsid,
                              session_obj->vocoder_state, VOICELOG_ICOMMON_NETWORK_ID_GSM,
                              gva_map_vocoder_type_gsm_to_vs( session_obj->vocoder_type ) );

       /* Set cached vocoder properties. */
       ( void ) gva_set_voc_dtx_mode( session_obj );
       ( void ) gva_set_voc_codec_mode( session_obj );

       /* Create the Simple job object to track VS_ENABLE. */
       rc = gva_create_simple_job_object( session_obj->header.handle,
              ( gva_simple_job_object_t** ) &ctrl->rootjob_obj );
       simple_obj = &ctrl->rootjob_obj->simple_job;

       rc = gva_vs_enable_vocoder( session_obj, simple_obj );
    }
    else
    {
      simple_obj = &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }

      /* Send a START CMD notification to GSM. */
      ( void ) gva_gsm_start_session ( session_obj->active_subs_obj );
    }

    break;
  }

  if( session_obj != NULL )
  {
    GVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc == APR_EOK )
  {
    ( void ) gva_mem_free_object ( ( gva_object_t*) simple_obj );
    ( void ) gva_free_cmd_packet ( cmd_pkt );
  }

  return rc;
}


static uint32_t gva_process_resource_revoke_cmd( 
 gva_gating_control_t* ctrl 
)
{
  uint32_t rc = APR_EOK;
  gva_cmd_packet_t* cmd_pkt = NULL;
  gva_session_object_t* session_obj = NULL;
  gva_iresource_cmd_grant_t* cmd_params = NULL;
  gva_simple_job_object_t* simple_obj = NULL;

  for ( ;; )
  {
    cmd_pkt = ( gva_cmd_packet_t* ) ctrl->packet;
    if( cmd_pkt == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    cmd_params = ( gva_iresource_cmd_grant_t* ) cmd_pkt->params;
    if( cmd_params == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = gva_get_object ( cmd_params->handle, ( gva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_process_resource_revoke_cmd(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    GVA_ACQUIRE_LOCK( session_obj->data_lock );

    if ( ctrl->state == GVA_GATING_CMD_STATE_EXECUTE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
           "gva_process_resource_revoke_cmd() - vocoder revoked" );

      /* Create a simple job object to track VS_DISABLE. */
      rc = gva_create_simple_job_object( session_obj->header.handle,
             ( gva_simple_job_object_t** ) &ctrl->rootjob_obj );
      simple_obj= &ctrl->rootjob_obj->simple_job;

      rc = gva_vs_disable_vocoder( session_obj, simple_obj );
    }
    else
    {
      simple_obj= &ctrl->rootjob_obj->simple_job;
      if( simple_obj->is_completed != TRUE )
      {
        rc = APR_EPENDING;
        break;
      }

      /* Send a vocoder release event to voice agent. */
      if( session_obj->va_gva_event_cb!= NULL )
      {
        session_obj->is_resource_granted = FALSE;
        session_obj->va_gva_event_cb( session_obj->va_session_context,
                                      GVA_IRESOURCE_EVENT_RELEASED, NULL, 0 );
      }
    }

    break;
  }

  if( session_obj != NULL )
  {
   GVA_RELEASE_LOCK( session_obj->data_lock );
  }

  if ( rc == APR_EOK ) 
  {
    ( void ) gva_mem_free_object ( ( gva_object_t*) simple_obj );
    ( void ) gva_free_cmd_packet ( cmd_pkt );
  }

  return rc;
}

static uint32_t gva_process_set_asid_vsid_mapping_cmd ( 
  gva_cmd_packet_t* cmd_pkt  
)
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  gva_modem_subs_object_t* subs_obj = NULL;
  gva_session_object_t* session_obj = NULL;
  gva_icommon_cmd_set_asid_vsid_mapping_t* cmd_params = NULL;

  for ( ;; )
  {
    if( cmd_pkt == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    cmd_params = ( gva_icommon_cmd_set_asid_vsid_mapping_t* ) cmd_pkt->params;
    if( cmd_params == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    GVA_ACQUIRE_LOCK( gva_global_lock );

    subs_obj = gva_subs_obj_list[cmd_params->asid];
    if( subs_obj->vsid == cmd_params->vsid )
    {
      subs_obj->pending_vsid = GVA_VSID_UNDEFINED_V;
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "gva_process_set_asid_vsid_mapping_cmd(): no change to mapping" );
      GVA_RELEASE_LOCK ( gva_global_lock );
      break;
    }

    if( TRUE == subs_obj->is_gsm_ready ) 
    {
      /* Store the VSID and apply the mapping at the end of the current voice call. */
      subs_obj->pending_vsid = cmd_params->vsid;
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "gva_process_set_asid_vsid_mapping_cmd(): call in-progress"
           "new mapping is stored in pending-set: ASID=(%d) VSID=(0X%08X)",
           cmd_params->asid, cmd_params->vsid );
      GVA_RELEASE_LOCK ( gva_global_lock );
      break;
    }

    MSG_2( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "gva_process_set_asid_vsid_mapping_cmd(): ASID=(%d) VSID=(0X%08X)",
           cmd_params->asid, cmd_params->vsid );

    /* Update the ASID-VSID mapping and the session object as per the VSID. */
    subs_obj->vsid = cmd_params->vsid;
    subs_obj->session_obj = NULL;

    for( index = 0; index < GVA_MAX_NUM_OF_SESSIONS_V; ++index )
    {
      session_obj = gva_session_obj_list[index];

      if( session_obj->vsid == subs_obj->vsid )
      {
        subs_obj->session_obj = session_obj;
        break;
      }
      else
      {
        session_obj = NULL;
      }
    }

    if( session_obj != NULL )
    {
      MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
             "gva_process_set_asid_vsid_mapping_cmd() - asid=%d mapped to "
             "session_obj=(0x%08x) with vsid=(0x%08x)", subs_obj->asid,
             subs_obj->session_obj, subs_obj->vsid );
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_process_set_asid_vsid_mapping_cmd(): Cannot find GVA session" );
    }

    GVA_RELEASE_LOCK ( gva_global_lock );

    break;
  }

  ( void ) gva_free_cmd_packet ( cmd_pkt );

  return rc;
}

/****************************************************************************
 * NONGATING REQUEST( CMDs/EVENTs ) PROCESSING FUNCTIONS
 ****************************************************************************/

static void gva_task_process_nongating_work_items ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t request_id = 0;
  gva_work_item_packet_type_t pkt_type;
  gva_work_item_t* work_item = NULL;
  void* packet = NULL;

  while( apr_list_remove_head( &gva_nongating_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) ) == APR_EOK )
  {
    pkt_type = work_item->pkt_type;
    packet = work_item->packet;
    
    if ( pkt_type == GVA_WORK_ITEM_PKT_TYPE_EVENT )
    {
      request_id = ( ( gva_event_packet_t* ) packet )->event_id ;
    }
    else if ( pkt_type ==  GVA_WORK_ITEM_PKT_TYPE_CMD )
    {
      request_id = ( ( gva_cmd_packet_t* ) packet )->cmd_id;
    }
    else
    {
      GVA_PANIC_ON_ERROR ( APR_EUNEXPECTED );
    }

    /* Add back to vs_free_work_pkt_q. */
    work_item->pkt_type = GVA_WORK_ITEM_PKT_TYPE_NONE;
    work_item->packet = NULL;
    ( void ) apr_list_add_tail( &gva_free_work_pkt_q, &work_item->link );

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "gva_task_process_nongating_work_items(): Processing "
           "request_id=(0X%08X)", request_id );

    switch( request_id )
    {
     /**
      * Handling routine for nongating work-items should take of release the
      * memory allocated for the CMD/EVENT packets.
      */
     case GSM_IVOICE_EVENT_REQUEST_CODEC_MODE:
       rc = gva_process_gsm_codec_mode_event( ( gva_event_packet_t* ) packet );
       break;

     case GSM_IVOICE_EVENT_REQUEST_DTX_MODE:
       rc = gva_process_gsm_dtx_mode_event( ( gva_event_packet_t* ) packet );
       break;

     case GSM_IVOICE_EVENT_SELECT_OWNER:
       rc = gva_process_gsm_select_owner_event( ( gva_event_packet_t* ) packet );
       break;

     case GVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING:
       rc = gva_process_set_asid_vsid_mapping_cmd( ( gva_cmd_packet_t* ) packet  );
       break;

     case VS_COMMON_EVENT_CMD_RESPONSE:
       rc = gva_process_vs_cmd_response_event( ( gva_event_packet_t* ) packet );
       break;

     case VS_COMMOM_EVENT_EAMR_MODE_CHANGE:
       rc = gva_process_vs_eamr_rate_change_event( ( gva_event_packet_t* ) packet );
       break;

     case VS_VOC_EVENT_READ_AVAILABLE:
       rc = gva_process_vs_read_buf_available_event( ( gva_event_packet_t* ) packet );
       break;

     case VS_VOC_EVENT_WRITE_BUFFER_RETURNED:
       rc = gva_process_vs_write_buf_returned_event( ( gva_event_packet_t* ) packet );
       break;

     default:
       /* Add remaining work items to the gating work queue. */
       rc = gva_queue_work_packet ( GVA_WORK_ITEM_QUEUE_TYPE_GATING,
                                    pkt_type, packet );
       if ( rc )
       {
         if ( pkt_type == GVA_WORK_ITEM_PKT_TYPE_CMD )
         {
           gva_free_cmd_packet( ( gva_cmd_packet_t* ) packet );
         }
         else
         {
          gva_free_event_packet( ( gva_event_packet_t* ) packet );
         }

       }
       break;
    }
  }

  return;
}

/****************************************************************************
 * GATING REQUEST( CMDs/EVENTs ) PROCESSING FUNCTIONS
 ****************************************************************************/
 
static void gva_task_process_gating_work_items ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t request_id = 0;
  gva_work_item_t* work_item;
  gva_cmd_packet_t* cmd_pkt;
  gva_event_packet_t* event_pkt;
  gva_gating_control_t* ctrl = &gva_gating_work_pkt_q;

  for ( ;; )
  {
    switch ( ctrl->state )
    {
     case GVA_GATING_CMD_STATE_FETCH:
       {
          /* Fetch the next gating command to execute. */
          rc = apr_list_remove_head( &ctrl->cmd_q,
                                     ( ( apr_list_node_t** ) &work_item ) );
          if ( rc ) return;

          if ( ( work_item->packet == NULL ) ||
               ( work_item->pkt_type == GVA_WORK_ITEM_PKT_TYPE_NONE ) )
          {
            GVA_PANIC_ON_ERROR ( APR_EUNEXPECTED );
          }

          MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "gva_task_process_gating_work_items(): "
               "VS_GATING_CMD_STATE_ENUM_FETCH" );

          ctrl->packet = work_item->packet;
          ctrl->pkt_type = work_item->pkt_type;
          ctrl->state = GVA_GATING_CMD_STATE_EXECUTE;

          /* Add the vs_work_item_t back to vs_free_work_pkt_q. */
          work_item->packet = NULL;
          work_item->pkt_type = GVA_WORK_ITEM_PKT_TYPE_NONE;
          ( void ) apr_list_add_tail( &gva_free_work_pkt_q, &work_item->link );
       }
       break;

     case GVA_GATING_CMD_STATE_EXECUTE:
     case GVA_GATING_CMD_STATE_CONTINUE:
       {
         if ( ctrl->pkt_type == GVA_WORK_ITEM_PKT_TYPE_CMD )
         {
           cmd_pkt = ( ( gva_cmd_packet_t* ) ctrl->packet );
           request_id = cmd_pkt->cmd_id;
         }
         else if ( ctrl->pkt_type == GVA_WORK_ITEM_PKT_TYPE_EVENT )
         {
           event_pkt = ( ( gva_event_packet_t* ) ctrl->packet );
           request_id = event_pkt->event_id;
         }
         else
         {
           GVA_PANIC_ON_ERROR ( APR_EUNEXPECTED );
         }

         /**
          * For Supported request_id, handler should take care of releasing 
          * memory allocated for packets.
          */
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
                 "gva_task_process_gating_work_items(): request_id=(0x%08x)",
                 request_id );

         switch ( request_id )
         {
          case GSM_IVOICE_EVENT_START_REQUEST:
            rc = gva_process_gsm_vocoder_start_event( ctrl );
            break;

          case GSM_IVOICE_EVENT_STOP_REQUEST:
            rc = gva_process_gsm_vocoder_stop_event( ctrl );
            break;

          case VS_COMMON_EVENT_READY:
            rc = gva_process_vs_ready_event( ctrl );
            break;

          case VS_COMMON_EVENT_NOT_READY:
            rc = gva_process_vs_not_ready_event( ctrl );
            break;

          case GVA_INTERNAL_EVENT_VS_OPEN:
            rc = gva_process_vs_open_event( ctrl );
            break;

          case GVA_INTERNAL_EVENT_VS_CLOSE:
            rc = gva_process_vs_close_event( ctrl );
            break;

          case GVA_IRESOURCE_CMD_GRANT:
            rc = gva_process_resource_grant_cmd( ctrl );
            break;

          case GVA_IRESOURCE_CMD_REVOKE:
            rc = gva_process_resource_revoke_cmd( ctrl );
            break;

          default:
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
                     "gva_task_process_gating_work_items(): Unsupported "
                     "request_id=(0X%08X)", request_id );

              /** For unsupported request_id, memory cleanup required for
               *  CMD/EVENT packets. */
              if( ctrl->pkt_type == GVA_WORK_ITEM_PKT_TYPE_CMD )
              {
                ( void ) gva_free_cmd_packet ( cmd_pkt );
              }

              if( ctrl->pkt_type == GVA_WORK_ITEM_PKT_TYPE_EVENT )
              {
                ( void ) gva_free_event_packet ( event_pkt );
              }

              /* set to VS_EOK to fetch the next command in queue. */
              rc = APR_EOK;
            }
           break;
         }

         /* Evaluate the pending command completion status. */
         switch ( rc )
         {
          case APR_EOK:
            {
              MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH,
                     "gva_task_process_gating_work_items(): request_id = "
                     "(0X%08X) processed successfully ", request_id );
              ctrl->packet = NULL;
              ctrl->pkt_type = GVA_WORK_ITEM_PKT_TYPE_NONE;
              /* The current command is finished so fetch the next command. */
              ctrl->state = GVA_GATING_CMD_STATE_FETCH;
            }
            break;
         
          case APR_EPENDING:
            /**
             * Assuming the current pending command control routine returns
             * APR_EPENDING the overall progress stalls until one or more
             * external events or responses are received.
             */
            ctrl->state = GVA_GATING_CMD_STATE_CONTINUE;
            /**
             * Return from here so as to avoid unecessary looping till reponse
             * is recived.
             */
            return;
         
          default:
            GVA_PANIC_ON_ERROR( APR_EUNEXPECTED );
            break;
         }
       }
       break;

     default:
      GVA_PANIC_ON_ERROR( rc );
      break;
    }/* switch case ends. */
  }/* for loop ends. */

  return;
}

/****************************************************************************
 * GVA TASK ROUTINES                                                        *
 ****************************************************************************/

GVA_INTERNAL void gva_signal_run ( void )
{
  apr_event_signal( gva_work_event );
}

static int32_t gva_run ( void )
{
  gva_task_process_nongating_work_items( );
  gva_task_process_gating_work_items( );

  return APR_EOK;
}

static int32_t gva_worker_thread_fn (
  void* param
)
{
  int32_t rc;

  apr_event_create( &gva_work_event );
  apr_event_signal( gva_control_event );

  for ( ;; )
  {
    rc = apr_event_wait( gva_work_event );
    if ( rc ) break;

    gva_run( );
  }

  apr_event_destroy( gva_work_event );
  apr_event_signal( gva_control_event );

  return APR_EOK;
}


/****************************************************************************
 * GVA BOOT-UP and POWER-DOWN ROUTINES                                      *
 ****************************************************************************/

static uint32_t gva_gating_control_init (
  gva_gating_control_t* p_ctrl
)
{
  uint32_t rc = APR_EOK;

  if ( p_ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_list_init_v2( &p_ctrl->cmd_q,
                         gva_thread_lock_fn, gva_thread_unlock_fn );
  if ( rc )
  {
    return APR_EFAILED;
  }

  p_ctrl->state = GVA_GATING_CMD_STATE_FETCH;
  p_ctrl->pkt_type = GVA_WORK_ITEM_PKT_TYPE_NONE;
  p_ctrl->packet = NULL;
  p_ctrl->rootjob_obj = NULL;

  return APR_EOK;
}  /* end of gva_gating_control_init () */

static uint32_t gva_gating_control_destroy (
  gva_gating_control_t* p_ctrl
)
{
  if ( p_ctrl == NULL )
  {
    return APR_EBADPARAM;
  }

  ( void ) apr_list_destroy( &p_ctrl->cmd_q );

  return APR_EOK;
}  /* end of vs_gating_control_destroy () */


/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/

static uint32_t gva_resource_cmd_register_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  gva_session_object_t* session_obj = NULL;
  gva_iresource_cmd_register_t* cmd_params = NULL;
  
  for ( ;; )
  {
    if ( size != sizeof ( gva_iresource_cmd_register_t ) )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }
  
    cmd_params = ( gva_iresource_cmd_register_t* ) params;
    if( cmd_params == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }
  
    GVA_ACQUIRE_LOCK ( gva_global_lock );
  
    /* Find a session with GVA_VSID_UNDEFINED_V vsid. */
    for( index = 0; index < GVA_MAX_NUM_OF_SESSIONS_V; ++index )
    {
      session_obj = gva_session_obj_list[index];
  
      if( ( session_obj->vsid == cmd_params->vsid ) ||
          ( session_obj->vsid == GVA_VSID_UNDEFINED_V ) )
      {
        break;
      }
      else
      {
        session_obj = NULL;
      }
    }
  
    if( session_obj != NULL )
    {
      session_obj->va_session_context = cmd_params->session_context;
      session_obj->va_gva_event_cb = cmd_params->event_cb;
      session_obj->vsid = cmd_params->vsid;
      ( void ) gva_vs_open_session( session_obj );
  
      *(cmd_params->ret_handle) = session_obj->header.handle;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "gva_resource_cmd_register_proc(): Cannot find  GVA session instance" );
    }
  
    GVA_RELEASE_LOCK ( gva_global_lock );
  
    break;
  }

  return rc;
}


static uint32_t gva_resource_cmd_deregister_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  gva_session_object_t* session_obj = NULL;
  gva_iresource_cmd_deregister_t* cmd_params = NULL;
  
  for ( ;; )
  {
    if ( size != sizeof ( gva_iresource_cmd_deregister_t ) )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }
  
    cmd_params = ( gva_iresource_cmd_deregister_t* ) params;
    if( cmd_params == NULL )
    {
      GVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }
  
    rc = gva_get_object ( cmd_params->handle, ( gva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "gva_resource_cmd_deregister_proc(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      break;
    }
  
    GVA_ACQUIRE_LOCK ( gva_global_lock );
  
    session_obj->vsid = GVA_VSID_UNDEFINED_V;
    session_obj->va_session_context = NULL;
    session_obj->va_gva_event_cb = NULL;
    ( void ) gva_vs_close_session( session_obj, NULL );
  
    GVA_RELEASE_LOCK ( gva_global_lock );
  
    break;
  }

  return rc;
}

/****************************************************************************
 * POWER UP/DOWN ROUTINES                                                    *
 ****************************************************************************/

static int32_t gva_init ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index;
  RCINIT_INFO info_handle = NULL;
  RCINIT_PRIO priority = 0;
  unsigned long stack_size = 0;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
         "gva_init(): Date %s Time %s", __DATE__, __TIME__ );

  {  /* Initialize the locks. */
    rc = apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &gva_int_lock );
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &gva_thread_lock );
    apr_event_create( &gva_control_event );
  }

  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &gva_heapmgr, ( ( void* ) &gva_heap_pool ),
                          sizeof( gva_heap_pool ), NULL, NULL );
  }

  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;
  
    params.table = gva_object_table;
    params.total_bits = GVA_HANDLE_TOTAL_BITS_V;
    params.index_bits = GVA_HANDLE_INDEX_BITS_V;
    params.lock_fn = gva_int_lock_fn;
    params.unlock_fn = gva_int_unlock_fn;
    rc = apr_objmgr_construct( &gva_objmgr, &params );
  }

  { /* Initialize free and nongating work pkt queues. */
    rc = apr_list_init_v2( &gva_free_work_pkt_q, 
                           gva_int_lock_fn, gva_int_unlock_fn );
    for ( index = 0; index < GVA_NUM_WORK_PKTS_V; ++index )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &gva_work_pkts[index] );
      gva_work_pkts[index].pkt_type = GVA_WORK_ITEM_PKT_TYPE_NONE;
      gva_work_pkts[index].packet = NULL;
      rc = apr_list_add_tail( &gva_free_work_pkt_q,
                              ( ( apr_list_node_t* ) &gva_work_pkts[index] ) );
    }
    rc = apr_list_init_v2( &gva_nongating_work_pkt_q,
                           gva_int_lock_fn, gva_int_unlock_fn );
  }

  { /* Initialize gating work pkt queue. */
    rc = gva_gating_control_init( &gva_gating_work_pkt_q );
  }

  { /* Initialize the global session lock. */
    rc = apr_lock_create( APR_LOCK_TYPE_MUTEX, &gva_global_lock );
    GVA_PANIC_ON_ERROR ( rc );
  }

  { /* Create the GVA task worker thread. */
    info_handle = rcinit_lookup( GVA_TASK_NAME );
    if ( info_handle == NULL ) 
    {
      /* Use the default priority & stack_size*/
      MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "gva_init(): GVA task not registered with RCINIT" );
      priority = GVA_TASK_PRIORITY;
      stack_size = GVA_TASK_STACK_SIZE;
    }
    else
    {
      priority = rcinit_lookup_prio_info( info_handle );
      stack_size = rcinit_lookup_stksz_info( info_handle );
    }

    if ( ( priority > 255 ) || ( stack_size == 0 ) ) 
    {
      ERR_FATAL( "gva_init(): Invalid priority: %d or stack size: %d",
                 priority, stack_size, 0 );
    }

    rc = apr_thread_create( &gva_thread, GVA_TASK_NAME, TASK_PRIORITY(priority),
                            gva_task_stack, stack_size, 
                            gva_worker_thread_fn , NULL );
    GVA_PANIC_ON_ERROR( rc );

    apr_event_wait( gva_control_event );
  }

  gva_is_initialized = TRUE;

  return rc;
}


static int32_t gva_postinit ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index =0;

  /* Initialize the mapping info and open onex voice instance. */
  for( index = 0; index < GVA_MAX_NUM_OF_SESSIONS_V; ++index )
  {
    /* Create and Initialize modem subscription object. */
    rc = gva_create_modem_subs_object( &gva_subs_obj_list[index] );;
    GVA_PANIC_ON_ERROR (rc);

    /* Open CDMA voice session instance. */
    gva_subs_obj_list[index]->asid = ( sys_modem_as_id_e_type ) index;
    rc = gva_gsm_open_session( gva_subs_obj_list[index] );
    GVA_PANIC_ON_ERROR (rc);

    /* Create and initialize GVA session object. */
    rc =  gva_create_session_object ( &gva_session_obj_list[index] );
    GVA_PANIC_ON_ERROR( rc );
  }

  return rc;
}


static int32_t gva_predeinit ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index =0;

  /* Close onex Session instance. */
  for( index = 0; index < GVA_MAX_NUM_OF_SESSIONS_V; ++index )
  {
    /* GVA CLOSE's VS session instance for all available VSID. */
    ( void ) gva_vs_close_session( gva_session_obj_list[index], NULL );

    /* GVA CLOSE's CDMA session instance for all available ASID. */
    ( void ) gva_gsm_close_session( gva_subs_obj_list[index] );

    /* Free GVA session object for all VSID. */
    ( void ) apr_lock_destroy( gva_session_obj_list[index]->data_lock );
    ( void ) gva_mem_free_object( (gva_object_t*)gva_session_obj_list[ index ] );
    
    /* Free GVA subscription object for all ASID. */
    ( void ) gva_mem_free_object( (gva_object_t*)gva_subs_obj_list[ index ] );
  }

  return rc;
}


static int32_t gva_deinit ( void )
{
  uint32_t rc = APR_EOK;

  gva_is_initialized = FALSE;

  apr_event_signal_abortall( gva_work_event );
  apr_event_wait( gva_control_event );

  /* Release gating control structures */
  ( void ) gva_gating_control_destroy( &gva_gating_work_pkt_q );

  /* Release work queue */
  ( void ) apr_list_destroy( &gva_free_work_pkt_q );
  ( void ) apr_list_destroy( &gva_nongating_work_pkt_q );

  /* Deinitialize the object handle table. */
  apr_objmgr_destruct( &gva_objmgr );

  /* Deinitialize basic OS resources for staging the setup. */
  ( void ) apr_event_destroy( gva_control_event );
  ( void ) apr_lock_destroy( gva_int_lock );
  ( void ) apr_lock_destroy( gva_thread_lock );
  ( void ) apr_lock_destroy( gva_global_lock );

  return rc;
}


GVA_EXTERNAL uint32_t gva_call (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc;

  switch ( cmd_id )
  {
  case DRV_CMDID_INIT:
    rc = gva_init( );
    break;

  case DRV_CMDID_POSTINIT:
    rc = gva_postinit( );
    break;

  case DRV_CMDID_PREDEINIT:
    rc = gva_predeinit( );
    break;

  case DRV_CMDID_DEINIT:
    rc = gva_deinit( );
    break;

  case GVA_IRESOURCE_CMD_REGISTER:
    rc =  gva_resource_cmd_register_proc( params, size );
    break;

  case GVA_IRESOURCE_CMD_DEREGISTER:
    rc =  gva_resource_cmd_deregister_proc( params, size );
    break;

  case GVA_IRESOURCE_CMD_GRANT:
    rc = gva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    break;

  case GVA_IRESOURCE_CMD_REVOKE:
    rc = gva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    break;

  case GVA_ICOMMON_CMD_SET_ASID_VSID_MAPPING:
    rc = gva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    break;

  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "gva_call(): Unsupported cmd ID (0x%08x)", cmd_id );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

