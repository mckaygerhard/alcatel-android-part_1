/*
   Copyright (C) 2015-2016 QUALCOMM Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc

   $Header: //components/rel/avs.mpss/6.2.1/vsd/vadapter/src/iva_module.c#3 $
   $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

/* CORE APIs. */
#include <stddef.h>
#include <string.h>
#include "err.h"
#include "msg.h"
#include "rcinit.h"
#include "mmstd.h"

/* APR APIs. */
#include "apr_errcodes.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_memmgr.h"
#include "apr_event.h"
#include "apr_thread.h"

/* VSD APIs. */
#include "drv_api.h"
#include "vs_task.h"

/* SELF APIs. */
#include "iva_if.h"
#include "iva_iresource_if.h"
#include "iva_i.h"

/*****************************************************************************
 * Defines                                                                   *
 ****************************************************************************/

#define IVA_MAX_NUM_OF_SESSIONS_V ( 2 )


/*****************************************************************************
 * Definitions                                                               *
 ****************************************************************************/


/*****************************************************************************
 * Global Variables                                                          *
 ****************************************************************************/

static apr_lock_t iva_int_lock;
static apr_event_t iva_control_event;

static apr_memmgr_type iva_heapmgr;
static uint8_t iva_heap_pool[ IVA_HEAP_SIZE_V ];

static apr_objmgr_t iva_objmgr;
static apr_objmgr_object_t iva_object_table[ IVA_MAX_OBJECTS_V ];

static apr_list_t iva_nongating_work_pkt_q;
static apr_list_t iva_free_work_pkt_q;
static iva_work_item_t iva_work_pkts[ IVA_NUM_WORK_PKTS_V ];

static apr_event_t iva_work_event;
static apr_thread_t iva_thread;
static uint8_t iva_task_stack[ IVA_TASK_STACK_SIZE ];

static iva_session_object_t *iva_session_list[ IVA_MAX_NUM_OF_SESSIONS_V ];
static apr_lock_t iva_global_thread_lock;

static bool_t iva_is_initialized = FALSE; 


/****************************************************************************
 * COMMON INTERNAL ROUTINES                                                 *
 ****************************************************************************/

static void iva_int_lock_fn ( void )
{
  apr_lock_enter( iva_int_lock );
}

static void iva_int_unlock_fn ( void )
{
  apr_lock_leave( iva_int_lock );
}


/****************************************************************************
 * GVA CMDs/EVENTs PACKET PREPARE/DISPATCHER/FREE ROUTINES                  *
 ****************************************************************************/

IVA_INTERNAL uint32_t iva_free_cmd_packet (
  iva_cmd_packet_t* packet
)
{
  uint32_t rc = APR_EOK;

  if ( packet != NULL )
  {
    if( packet->params != NULL )
    {
      /* Free the memory - p_cmd_packet->params. */
      apr_memmgr_free( &iva_heapmgr, packet->params );
      packet->params = NULL;
    }

    /* Free the memeory - p_cmd_packet. */
    apr_memmgr_free( &iva_heapmgr, packet );
    packet = NULL;
  }

  return rc;
}

/**
 * This is a common routine facilitating to prepare and
 * dispatches a CMD PKT.
 */
IVA_INTERNAL uint32_t iva_prepare_and_dispatch_cmd_packet (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  iva_cmd_packet_t* packet = NULL;
  iva_work_item_t* work_item = NULL;

  for ( ;; )
  {
    packet = ( ( iva_cmd_packet_t* ) apr_memmgr_malloc( &iva_heapmgr,
                                           sizeof( iva_cmd_packet_t ) ) );
    if ( packet == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR( APR_ENORESOURCE );
      rc = APR_ENORESOURCE;
      break;
    }

    packet->cmd_id = cmd_id;
    packet->params = NULL;

    if ( ( size > 0 ) && ( params != NULL ) )
    {
      packet->params = apr_memmgr_malloc(  &iva_heapmgr, size );

      if ( packet->params == NULL )
      {
        rc = APR_ENORESOURCE;
        IVA_REPORT_FATAL_ON_ERROR( rc );
        ( void ) iva_free_cmd_packet( packet );
        break;
      }
      mmstd_memcpy( packet->params, size, params, size );
    }

    /* Get a free command structure. */
    rc = apr_list_remove_head( &iva_free_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) );
    if ( rc )
    {
      rc = APR_ENORESOURCE;
      /* No free WORK packet structure is available. */
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL,
             "iva_prepare_and_dispatch_cmd_packet(): Ran out of WORK packets, "
             "rc=0x%08x, gva_state=%d",   rc, iva_is_initialized );
      break;
    }

    work_item->packet = packet;
    /* Add to incoming request work queue */
    rc = apr_list_add_tail( &iva_nongating_work_pkt_q, &work_item->link );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "iva_prepare_and_dispatch_cmd_packet() - ERROR: rc=0x%08x", rc );
      /* Add back to iva_free_work_pkt_q */
      work_item->packet = NULL;
      ( void ) apr_list_add_tail( &iva_free_work_pkt_q, &work_item->link );
    }
    else
    {
      /**
       * Signal appropriate thread.
       */
      iva_signal_run();
    }

    break;
  }

  return rc;
}

/****************************************************************************
 * IVA OBJECT CREATION, DESTRUCTION AND INITIALISATION ROUTINES             *
 ****************************************************************************/

static int32_t iva_get_object (
  uint32_t handle,
  iva_object_t** ret_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = apr_objmgr_find_object( &iva_objmgr, handle, &objmgr_obj );
  if ( rc )
  {
    return APR_EFAILED;
  }

  *ret_obj = ( ( iva_object_t* ) objmgr_obj->any.ptr );

  return APR_EOK;
}

static uint32_t iva_mem_alloc_object (
  uint32_t size,
  iva_object_t** ret_object
)
{
  int32_t rc;
  iva_object_t* iva_obj;
  apr_objmgr_object_t* objmgr_obj;

  if ( ret_object == NULL )
  {
    return APR_EBADPARAM;
  }

  { /* Allocate memory for the IVA object. */
    iva_obj = apr_memmgr_malloc( &iva_heapmgr, size );
    if ( iva_obj == NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "iva_mem_alloc_object(): Out of memory, requested size (%d)", size );
      return APR_ENORESOURCE;
    }

    /* Allocate a new handle for the IVA object. */
    rc = apr_objmgr_alloc_object( &iva_objmgr, &objmgr_obj );
    if ( rc )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, 
             "iva_mem_alloc_object(): Out of objects, rc = (0x%08X)", rc );
      apr_memmgr_free( &iva_heapmgr, iva_obj );
      return APR_ENORESOURCE;
    }

    /* Use the custom object type. */
    objmgr_obj->any.ptr = iva_obj;

    /* Initialize the base IVA object header. */
    iva_obj->header.handle = objmgr_obj->handle;
    iva_obj->header.type = IVA_OBJECT_TYPE_ENUM_UNINITIALIZED;
  }

  *ret_object = iva_obj;

  return APR_EOK;
}

static uint32_t iva_mem_free_object (
  iva_object_t* object
)
{
  if ( object == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &iva_objmgr, object->header.handle );
  apr_memmgr_free( &iva_heapmgr, object );

  return APR_EOK;
}


static uint32_t iva_create_session_object ( 
  iva_session_object_t** ret_session_obj )
{
  uint32_t rc = APR_EOK;
  iva_session_object_t* session_obj = NULL;

  if ( ret_session_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  rc = iva_mem_alloc_object( sizeof( iva_session_object_t ),
                             ( ( iva_object_t** ) &session_obj ) );
  if ( rc )
  {
    return APR_ENORESOURCE;
  }

  { /* Initialize the simple job object. */
    session_obj->header.type = IVA_OBJECT_TYPE_ENUM_SESSION;

    session_obj->vsid = IVA_VSID_UNDEFINED_V;

    session_obj->is_resource_granted = FALSE;
    session_obj->va_event_cb = NULL;
    session_obj->va_session_context =  NULL;

    session_obj->is_ims_closed = TRUE;
    session_obj->ims_event_cb = NULL;
    session_obj->ims_session_context =  NULL;
  }

  *ret_session_obj = session_obj;

  return APR_EOK;
}


/****************************************************************************
 * EXTERNAL API ROUTINES                                                    *
 ****************************************************************************/

static uint32_t iva_resource_cmd_register_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  iva_session_object_t* session_obj = NULL;
  iva_iresource_cmd_register_t* cmd_params = NULL;

  for ( ;; )
  {
    if ( size != sizeof ( iva_iresource_cmd_register_t ) )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }

    cmd_params = ( iva_iresource_cmd_register_t* ) params;
    if( cmd_params == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }

    IVA_ACQUIRE_LOCK ( iva_global_thread_lock );

    /* Find the pre-created session object with asid passed by client. */
    for( index = 0; index < IVA_MAX_NUM_OF_SESSIONS_V; ++index )
    {
      session_obj = iva_session_list[index];

      if( ( session_obj->vsid == cmd_params->vsid ) ||
          ( session_obj->vsid == IVA_VSID_UNDEFINED_V ) )
      {
        break;
      }
      else
      {
        session_obj = NULL;
      }
    }

    if( session_obj != NULL )
    {
      session_obj->va_session_context = cmd_params->session_context;
      session_obj->va_event_cb = cmd_params->event_cb;
      if( session_obj->vsid == IVA_VSID_UNDEFINED_V )
      {
        session_obj->vsid = cmd_params->vsid;
      }
      session_obj->is_resource_granted = FALSE;

      *(cmd_params->ret_handle) = session_obj->header.handle;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "iva_resource_cmd_register_proc(): Cannot find IVA session instance "
           "for Voice agent to register" );
    }

    IVA_RELEASE_LOCK ( iva_global_thread_lock );

    break;
  }

  return rc;
}


static uint32_t iva_resource_cmd_deregister_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  iva_session_object_t* session_obj = NULL;
  iva_iresource_cmd_deregister_t* cmd_params = NULL;

  for ( ;; )
  {
    if ( size != sizeof ( iva_iresource_cmd_deregister_t ) )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }

    cmd_params = ( iva_iresource_cmd_deregister_t* ) params;
    if( cmd_params == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }

    rc = iva_get_object ( cmd_params->handle, ( iva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "iva_resource_cmd_deregister_proc(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EHANDLE;
      break;
    }

    IVA_ACQUIRE_LOCK ( iva_global_thread_lock );

    session_obj->va_session_context = NULL;
    session_obj->va_event_cb = NULL;

    IVA_RELEASE_LOCK ( iva_global_thread_lock );

    break;
  }

  return rc;
}


static uint32_t iva_voice_cmd_open_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  iva_session_object_t* session_obj = NULL;
  iva_ivoice_cmd_open_t* cmd_params = NULL;

  for ( ;; )
  {
    if ( size != sizeof ( iva_ivoice_cmd_open_t ) )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }

    cmd_params = ( iva_ivoice_cmd_open_t* ) params;
    if( cmd_params == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }

    IVA_ACQUIRE_LOCK ( iva_global_thread_lock );

    /* Find the pre-created session object with asid passed by client. */
    for( index = 0; index < IVA_MAX_NUM_OF_SESSIONS_V; ++index )
    {
      session_obj = iva_session_list[index];

      if( ( session_obj->vsid == cmd_params->vsid ) ||
          ( session_obj->vsid == IVA_VSID_UNDEFINED_V ) )
      {
        break;
      }
      else
      {
        session_obj = NULL;
      }
    }

    if( session_obj != NULL )
    {
      session_obj->is_ims_closed = FALSE;
      session_obj->ims_session_context = cmd_params->session_context;
      if( session_obj->vsid == IVA_VSID_UNDEFINED_V )
      {
        session_obj->vsid = cmd_params->vsid;
      }
      session_obj->ims_event_cb = cmd_params->event_cb;

      *(cmd_params->ret_handle) = session_obj->header.handle;
    }
    else
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "iva_voice_cmd_open_proc(): Cannot find IVA session instance!!!" );
    }

    IVA_RELEASE_LOCK ( iva_global_thread_lock );

    break;
  }

  return rc;
}


static uint32_t iva_voice_cmd_close_proc ( 
  void* params,
  uint32_t size
)
{
  uint32_t rc = APR_EOK;
  iva_session_object_t* session_obj = NULL;
  iva_ivoice_cmd_close_t* cmd_params = NULL;

  for ( ;; )
  {
    if ( size != sizeof ( iva_ivoice_cmd_close_t ) )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EBADPARAM );
      rc = APR_EBADPARAM;
      break;
    }

    cmd_params = ( iva_ivoice_cmd_close_t* ) params;
    if( cmd_params == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      rc = APR_EBADPARAM;
      break;
    }

    rc = iva_get_object ( cmd_params->handle, ( iva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "iva_voice_cmd_close_proc(): Invalid handle=0X%08X, rc=0X%08X",
             cmd_params->handle, rc );
      rc = APR_EHANDLE;
      break;
    }

    IVA_ACQUIRE_LOCK ( iva_global_thread_lock );

    if( session_obj->is_ims_closed == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "iva_voice_cmd_close_proc(): Invalid request session already closed");
      rc = APR_EFAILED;
      break;
    }

    session_obj->ims_session_context = NULL;
    session_obj->ims_event_cb = NULL;
    session_obj->is_ims_closed = TRUE;

    break;
  }

  if( session_obj != NULL )
  {
    IVA_RELEASE_LOCK ( iva_global_thread_lock );
  }

  return rc;
}


static uint32_t iva_voice_cmd_request_vocoder_proc ( 
  iva_cmd_packet_t* packet
)
{
  uint32_t rc = APR_EOK;
  iva_session_object_t* session_obj = NULL;
  iva_ivoice_cmd_request_vocoder_t* cmd_params = NULL;

  for ( ;; )
  {
    cmd_params = ( iva_ivoice_cmd_request_vocoder_t* ) packet->params;
    if( cmd_params == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = iva_get_object ( cmd_params->handle, ( iva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "iva_voice_cmd_request_vocoder_proc(): Invalid handle=0X%08X, "
             "rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    IVA_ACQUIRE_LOCK ( iva_global_thread_lock );

    if( session_obj->is_ims_closed == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "iva_voice_cmd_request_vocoder_proc(): Invalid request session "
           "already closed");
      break;
    }


    /* Notify resource request to Voice agent for IMS. */
    if ( session_obj->is_resource_granted == TRUE ) 
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "IVA: REQUEST_VOCODER(PROCESS): VSID=(0x%08x): "
             "EVENT_GRANT_VOCODER sent to IMS immediately.", session_obj->vsid );
      session_obj->ims_event_cb( session_obj->va_session_context,
                                IVA_IVOICE_EVENT_GRANT_VOCODER, NULL, 0 );
    }
    else
    {
      /* Notify resource request to Voice agent for IMS. */
      if (  session_obj->va_event_cb != NULL )
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
               "IVA: REQUEST_VOCODER(PROCESS): VSID=(0x%08x): "
               "EVENT_REQUEST sent to voice agent ", session_obj->vsid );
        session_obj->va_event_cb( session_obj->va_session_context,
                                  IVA_IRESOURCE_EVENT_REQUEST, NULL, 0 );
      }
      else
      {
        MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
               "IVA: REQUEST_VOCODER(PROCESS): VSID=(0x%08x): Voice Agent not "
               "registered", session_obj->vsid );
      }
    }

    break;
  }

  if( session_obj != NULL )
  {
    IVA_RELEASE_LOCK ( iva_global_thread_lock );
  }

  ( void ) iva_free_cmd_packet( packet );

  return rc;
}



static uint32_t iva_resource_cmd_grant_proc ( 
 iva_cmd_packet_t* packet
)
{
  uint32_t rc = APR_EOK;
  iva_session_object_t* session_obj = NULL;
  iva_iresource_cmd_grant_t* cmd_params = NULL;

  for ( ;; )
  {
    cmd_params = ( iva_iresource_cmd_grant_t* ) packet->params;
    if( cmd_params == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = iva_get_object ( cmd_params->handle, ( iva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "iva_resource_cmd_grant_proc(): Invalid handle=0X%08X, rc=0X%08X",
             cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    IVA_ACQUIRE_LOCK ( iva_global_thread_lock );

    session_obj->is_resource_granted = TRUE;

    if ( session_obj->ims_event_cb != NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "IVA: GRANT_VOCODER(PROCESS): VSID=(0x%08x): "
             "EVENT_GRANT_VOCODER sent to IMS ", session_obj->vsid );
      session_obj->ims_event_cb( session_obj->ims_session_context,
                                 IVA_IVOICE_EVENT_GRANT_VOCODER, NULL, 0 );
    }
    
    IVA_RELEASE_LOCK ( iva_global_thread_lock );

    break;
  }

  ( void ) iva_free_cmd_packet( packet );

  return rc;
}


static uint32_t iva_resource_cmd_revoke_proc ( 
 iva_cmd_packet_t* packet
)
{
  uint32_t rc = APR_EOK;
  iva_session_object_t* session_obj = NULL;
  iva_iresource_cmd_revoke_t* cmd_params = NULL;
  
  for ( ;; )
  {
    cmd_params = ( iva_iresource_cmd_revoke_t* ) packet->params;
    if( cmd_params == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = iva_get_object ( cmd_params->handle, ( iva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "iva_resource_cmd_revoke_proc(): Invalid handle=0X%08X, rc=0X%08X",
             cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    IVA_ACQUIRE_LOCK ( iva_global_thread_lock );

    session_obj->is_resource_granted = FALSE;

    if ( session_obj->ims_event_cb != NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "IVA: REVOKE_VOCODER(PROCESS): VSID=(0x%08x): "
             "EVENT_REVOKE_VOCODER sent to IMS ", session_obj->vsid );
      session_obj->ims_event_cb( session_obj->ims_session_context,
                                 IVA_IVOICE_EVENT_REVOKE_VOCODER, NULL, 0 );
    }

    IVA_RELEASE_LOCK ( iva_global_thread_lock );

    break;
  }

  ( void ) iva_free_cmd_packet( packet );

  return rc;
}


static uint32_t iva_voice_cmd_grant_vocoder_accepted_proc ( 
 iva_cmd_packet_t* packet
)
{
  uint32_t rc = APR_EOK;
  iva_session_object_t* session_obj = NULL;
  iva_ivoice_cmd_set_grant_vocoder_accepted_t* cmd_params = NULL;

  for ( ;; )
  {
    cmd_params = ( iva_ivoice_cmd_set_grant_vocoder_accepted_t* ) packet->params;
    if( cmd_params == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = iva_get_object ( cmd_params->handle, ( iva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "iva_voice_cmd_grant_vocoder_accepted_proc(): Invalid "
             "handle=0X%08X, rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    IVA_ACQUIRE_LOCK ( iva_global_thread_lock );

    if( session_obj->is_ims_closed == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "iva_voice_cmd_grant_vocoder_accepted_proc(): Invalid request "
           "session already closed");
      rc = APR_EFAILED;
      break;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "iva_voice_cmd_grant_vocoder_accepted_proc(): vsid = (0x%08x) "
           "CMD_SET_GRANT_VOCODER_ACCEPTED recieved from IMS", 
           session_obj->vsid );

    break;
  }

  if( session_obj != NULL )
  {
    IVA_RELEASE_LOCK ( iva_global_thread_lock );
  }

  ( void ) iva_free_cmd_packet( packet );

  return rc;
}


static uint32_t iva_voice_cmd_release_vocoder_done_proc ( 
 iva_cmd_packet_t* packet
)
{
  uint32_t rc = APR_EOK;
  iva_session_object_t* session_obj = NULL;
  iva_ivoice_cmd_set_release_vocoder_done_t* cmd_params = NULL;

  for ( ;; )
  {
    cmd_params = ( iva_ivoice_cmd_set_release_vocoder_done_t* ) packet->params;
    if( cmd_params == NULL )
    {
      IVA_REPORT_FATAL_ON_ERROR ( APR_EUNEXPECTED );
      break;
    }

    rc = iva_get_object ( cmd_params->handle, ( iva_object_t** )&session_obj );
    if( session_obj == NULL )
    {
      MSG_2( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
             "iva_voice_cmd_release_vocoder_done_proc(): Invalid "
             "handle=0X%08X, rc=0X%08X", cmd_params->handle, rc );
      rc = APR_EOK;
      break;
    }

    IVA_ACQUIRE_LOCK ( iva_global_thread_lock );

    if( session_obj->is_ims_closed == TRUE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "iva_voice_cmd_release_vocoder_done_proc(): Invalid request session "
           "already closed");
      rc = APR_EFAILED;
      break;
    }

    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "iva_voice_cmd_release_vocoder_done_proc(): vsid=(0x%08x) "
           "CMD_SET_RELEASE_VOCODER_DONE recieved from IMS", 
           session_obj->vsid );

    /* Notify Voice agent that resource is released by IMS. */
    if ( session_obj->va_event_cb != NULL )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "IVA: RELEASE_VOCODER_DONE(PROCESS): VSID=(0x%08x): "
             "EVENT_RELEASED sent to voice agent ", session_obj->vsid );
      session_obj->va_event_cb( session_obj->va_session_context,
                                IVA_IRESOURCE_EVENT_RELEASED, NULL, 0 );
      session_obj->is_resource_granted = FALSE;
    }

    break;
  }

  if( session_obj != NULL )
  {
    IVA_RELEASE_LOCK ( iva_global_thread_lock );
  }

  ( void ) iva_free_cmd_packet( packet );

  return rc;
}


/****************************************************************************
 * NONGATING REQUEST( CMDs ) PROCESSING FUNCTIONS
 ****************************************************************************/

static void iva_task_process_nongating_work_items ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t request_id = 0;
  iva_work_item_t* work_item = NULL;
  iva_cmd_packet_t* packet = NULL;

  while( apr_list_remove_head( &iva_nongating_work_pkt_q,
                               ( ( apr_list_node_t** ) &work_item ) ) == APR_EOK )
  {
    packet = work_item->packet;

    /* Add back to vs_free_work_pkt_q. */
    work_item->packet = NULL;
    ( void ) apr_list_add_tail( &iva_free_work_pkt_q, &work_item->link );

    if ( packet == NULL )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR,
           "iva_task_process_nongating_work_items(): work packet NULL!! " );
      continue;
    }
    else
    {
      request_id = packet->cmd_id;
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
             "iva_task_process_nongating_work_items(): Processing "
             "request_id=(0X%08X", request_id );
    }

    switch( request_id )
    {
     /**
      * Handling routine for nongating work-items should take of release the
      * memory allocated for the CMD/EVENT packets.
      */
     case IVA_IVOICE_CMD_REQUEST_VOCODER:
       rc = iva_voice_cmd_request_vocoder_proc ( packet );
       break;

     case IVA_IVOICE_CMD_SET_GRANT_VOCODER_ACCEPTED:
       rc = iva_voice_cmd_grant_vocoder_accepted_proc( packet );
       break;

     case IVA_IVOICE_CMD_SET_RELEASE_VOCODER_DONE:
       rc = iva_voice_cmd_release_vocoder_done_proc ( packet );
       break;

     case IVA_IRESOURCE_CMD_GRANT:
       rc = iva_resource_cmd_grant_proc ( packet );
       break;

     case IVA_IRESOURCE_CMD_REVOKE:
       rc = iva_resource_cmd_revoke_proc ( packet );
       break;

     default:
       {
         /* Handle unsupported request. */
         MSG_1( MSG_SSID_DFLT, MSG_LEGACY_MED,
                "iva_task_process_nongating_work_items(): Unsupported "
                "request_id=(0X%08X)", request_id );
         iva_free_cmd_packet( packet );
       }
       break;
    } /* switch case ends. */

  } /* while loop ends. */

  return;
}

/****************************************************************************
 * GVA TASK ROUTINES                                                        *
 ****************************************************************************/

IVA_INTERNAL void iva_signal_run ( void )
{
  apr_event_signal( iva_work_event );
}

static int32_t iva_run ( void )
{
  iva_task_process_nongating_work_items( );

  return APR_EOK;
}

static int32_t iva_worker_thread_fn (
  void* param
)
{
  int32_t rc;

  apr_event_create( &iva_work_event );
  apr_event_signal( iva_control_event );

  for ( ;; )
  {
    rc = apr_event_wait( iva_work_event );
    if ( rc ) break;

    iva_run( );
  }

  apr_event_destroy( iva_work_event );
  apr_event_signal( iva_control_event );

  return APR_EOK;
}

/****************************************************************************
 * POWER UP/DOWN ROUTINES                                                   *
 ****************************************************************************/

static int32_t iva_init ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  RCINIT_INFO info_handle = NULL;
  RCINIT_PRIO priority = 0;
  unsigned long stack_size = 0;

  MSG_2( MSG_SSID_DFLT, MSG_LEGACY_LOW,
         "iva_init(): Date %s Time %s", __DATE__, __TIME__ );

  {  /* Initialize the locks. */
    rc |= apr_lock_create( APR_LOCK_TYPE_INTERRUPT, &iva_int_lock );
    rc |= apr_lock_create( APR_LOCK_TYPE_MUTEX, &iva_global_thread_lock );
    IVA_PANIC_ON_ERROR( rc );
    apr_event_create( &iva_control_event );
  }

  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &iva_heapmgr, ( ( void* ) &iva_heap_pool ),
                          sizeof( iva_heap_pool ), NULL, NULL );
  }

  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;
  
    params.table = iva_object_table;
    params.total_bits = IVA_HANDLE_TOTAL_BITS_V;
    params.index_bits = IVA_HANDLE_INDEX_BITS_V;
    params.lock_fn = iva_int_lock_fn;
    params.unlock_fn = iva_int_unlock_fn;
    rc = apr_objmgr_construct( &iva_objmgr, &params );
    IVA_PANIC_ON_ERROR( rc );
  }

  { /* Initialize free and nongating work pkt queues. */
    rc = apr_list_init_v2( &iva_free_work_pkt_q, 
                           iva_int_lock_fn, iva_int_unlock_fn );
    for ( index = 0; index < IVA_NUM_WORK_PKTS_V; ++index )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &iva_work_pkts[index] );
      iva_work_pkts[index].packet = NULL;
      rc = apr_list_add_tail( &iva_free_work_pkt_q,
                              ( ( apr_list_node_t* ) &iva_work_pkts[index] ) );
    }

    rc = apr_list_init_v2( &iva_nongating_work_pkt_q,
                           iva_int_lock_fn, iva_int_unlock_fn );
  }

  { /* Create the GVA task worker thread. */
    info_handle = rcinit_lookup( IVA_TASK_NAME );
    if ( info_handle == NULL ) 
    {
      /* Use the default priority & stack_size*/
      MSG( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "iva_init(): IVA task not registered with RCINIT" );
      priority = IVA_TASK_PRIORITY;
      stack_size = IVA_TASK_STACK_SIZE;
    }
    else
    {
      priority = rcinit_lookup_prio_info( info_handle );
      stack_size = rcinit_lookup_stksz_info( info_handle );
    }

    if ( ( priority > 255 ) || ( stack_size == 0 ) ) 
    {
      ERR_FATAL( "iva_init(): Invalid priority: %d or stack size: %d",
                 priority, stack_size, 0 );
    }

    rc = apr_thread_create( &iva_thread, IVA_TASK_NAME, TASK_PRIORITY(priority),
                            iva_task_stack, stack_size, 
                            iva_worker_thread_fn , NULL );
    IVA_PANIC_ON_ERROR( rc );

    apr_event_wait( iva_control_event );
  }

  iva_is_initialized = TRUE;

  return rc;
}


static int32_t iva_postinit ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index =0;

  for ( index =0; index < IVA_MAX_NUM_OF_SESSIONS_V; index++ )
  {
    /* Initialize the IVA session object. */
    rc = iva_create_session_object( &iva_session_list[ index ] );
    IVA_PANIC_ON_ERROR( rc );
  }

  return rc;
}


static int32_t iva_predeinit ( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index =0;

  for ( index =0; index < IVA_MAX_NUM_OF_SESSIONS_V; index++ )
  {
    /* Free all the IVA session object. */
    ( void ) iva_mem_free_object( ( iva_object_t* ) iva_session_list[ index ] );
  }

  return rc;
}


static int32_t iva_deinit ( void )
{
  uint32_t rc = APR_EOK;

  iva_is_initialized = FALSE;

  apr_event_signal_abortall( iva_work_event );
  apr_event_wait( iva_control_event );

  /* Release work queue */
  ( void ) apr_list_destroy( &iva_free_work_pkt_q );
  ( void ) apr_list_destroy( &iva_nongating_work_pkt_q );

  /* Deinitialize the object handle table. */
  apr_objmgr_destruct( &iva_objmgr );

  /* Deinitialize basic OS resources for staging the setup. */
  apr_event_destroy( iva_control_event );
  apr_lock_destroy( iva_int_lock );
  apr_lock_destroy( iva_global_thread_lock );

  return rc;
}


/****************************************************************************
 * SINGLE ENTRY POINT                                                       *
 ****************************************************************************/

IVA_EXTERNAL uint32_t iva_call (
  uint32_t cmd_id,
  void* params,
  uint32_t size
)
{
  uint32_t rc;

  switch ( cmd_id )
  {
  case DRV_CMDID_INIT:
    rc = iva_init( );
    break;

  case DRV_CMDID_POSTINIT:
    rc = iva_postinit( );
    break;

  case DRV_CMDID_PREDEINIT:
    rc = iva_predeinit( );
    break;

  case DRV_CMDID_DEINIT:
    rc = iva_deinit( );
    break;

  case IVA_IVOICE_CMD_OPEN:
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "iva_call() CMD_OPEN received from IMS" );
    rc =  iva_voice_cmd_open_proc( params, size );
    break;

  case IVA_IVOICE_CMD_CLOSE:
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "iva_call() CMD_CLOSE received from IMS" );
    rc =  iva_voice_cmd_close_proc( params, size );
    break;

  case IVA_IVOICE_CMD_REQUEST_VOCODER:
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "iva_call() CMD_REQUEST_VOCODER received from IMS" );
      rc = iva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    }
    break;
    
  case IVA_IVOICE_CMD_SET_GRANT_VOCODER_ACCEPTED:
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "iva_call(): CMD_SET_GRANT_VOCODER_ACCEPTED received from IMS" );
      rc = iva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    }
    break;

  case IVA_IVOICE_CMD_SET_RELEASE_VOCODER_DONE:
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "iva_call(): CMD_SET_RELEASE_VOCODER_DONE received from IMS" );
      rc = iva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    }
    break;

  case IVA_IRESOURCE_CMD_REGISTER:
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "iva_call() CMD_REGISTER received from voice agent" );
    rc =  iva_resource_cmd_register_proc( params, size );
    break;

  case IVA_IRESOURCE_CMD_DEREGISTER:
    MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
         "iva_call() CMD_DEREGISTER received from voice agent" );
    rc =  iva_resource_cmd_deregister_proc( params, size );
    break;

  case IVA_IRESOURCE_CMD_GRANT:
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "iva_call(): CMD_GRANT recieved from voice agent" );
      rc = iva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    }
    break;

  case IVA_IRESOURCE_CMD_REVOKE:
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_MED,
           "iva_call(): CMD_REVOKE recieved from voice agent" );
      rc = iva_prepare_and_dispatch_cmd_packet ( cmd_id, params,  size );
    }
    break;


  default:
    MSG_1( MSG_SSID_DFLT, MSG_LEGACY_LOW,
           "iva_call(): Unsupported cmd ID (0x%08x)", cmd_id );
    rc = APR_EUNSUPPORTED;
    break;
  }

  return rc;
}

void iva_task
(
  dword ignored
    /* Parameter received from Main Control task - ignored */
    /*lint -esym(715,ignored)
    ** Have lint not complain about the ignored parameter 'ignored' which is
    ** specified to make this routine match the template for rex_def_task().
    */
)
{
  ( void ) iva_call( DRV_CMDID_INIT, NULL, 0 );
  ( void ) iva_call( DRV_CMDID_POSTINIT, NULL, 0 );

  return;
}


