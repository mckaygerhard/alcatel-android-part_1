#ifndef __TVA_IRESOURCE_IF_H__
#define __TVA_IRESOURCE_IF_H__

/**
  @file  tva_iresource_if.h
  @brief This file contains resource interface definitions of the TDSCDMA Voice
         Adapter.
*/

/*
  ============================================================================

   Copyright (C) 2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //components/rel/avs.mpss/6.2.1/vsd/vadapter/inc/protected/tva_iresource_if.h#1 $
  $Author: pwbldsvc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------


  ============================================================================
*/


/*----------------------------------------------------------------------------
  Include files for Module
----------------------------------------------------------------------------*/

#include "tva_icommon_if.h"


/****************************************************************************
 * TVA VOICE INTERFACE DEFINITION                                           *
 ****************************************************************************/

/** Register command (synchronous API).
 *
 * Voice agent shall use this command to acquire a handle to a TVA instance.
 *
 * Voice agent must use the returned handle (ret_handle) provided by the Open 
 * command to execute any subsequent commands.
 *
 * TVA shall be requesting resource token to voice agent in response of traffic 
 * channel start request from access stratum.
 *
 * If resource is available voice agent grants the token immediately.
 * If resource are not available, voice agent caches the request (LIFO based)
 * for TVA, as grants the voice resource to TVA as soon as its available. 
 *
 * Upon a successful register, the command shall return TVA_EOK to the client.
 *
 */
#define TVA_IRESOURCE_CMD_REGISTER ( 0x0001318B )

typedef struct tva_iresource_cmd_register_t tva_iresource_cmd_register_t;

struct tva_iresource_cmd_register_t {

  uint32_t* ret_handle;
   /**<
     * Returns the handle that the client must use when making subsequent
     * commands. */

  uint32_t vsid;
    /**< Voice System ID as defined by DCN 80-NF711-1 Rev E. */

  tva_icommon_event_callback_fn_t event_cb;
    /**<
      * Central event callback function, which receives asynchronous events
      * from the server.
      *
      * Operational contract:
      *
      * - The client may only queue the incoming event and signal a worker
      * thread to process the event. The client must not perform any other
      * processing in the callback context.
      *
      * - The client may not call any APIs on the TDSCDMA voice adapter in the
      * callback context. This will cause synchronization issues for the driver
      * and may lead to a system failure or deadlock.
      *
      * - The client may not perform any blocking operations or acquire any
      * locks in the event callback context that lead to a system deadlock.
      *
      * - The client may spend no more than 5 us while in the callback
      * context.
      *
      * - It is highly recommended to use atomic operations for
      * synchronization needs.
      *
      * Failure to meet the operational contract may lead to an undefined
      * state with system stability and performance issues.
      */

  void* session_context;
    /**<
      * Pointer to the session data. The client stores its session
      * context pointer here to retrieve its session control data structure,
      * which the client uses to queue and signal events into its worker
      * thread.
      *
      * The session_data is returned to the client each time an event
      * callback is triggered.
      */

};


/** De-register command (synchronous blocking API.)
 *
 * This command deregisters from TDSCDMA Voice Adapter instance.
 *
 * Voice agent shall not use the open handle after receiving successful close
 * indication.
 *
 * Upon a successful deregister, the command shall return TVA_EOK to the caller.
 */
#define TVA_IRESOURCE_CMD_DEREGISTER ( 0x0001318C )

typedef struct tva_iresource_cmd_deregister_t tva_iresource_cmd_deregister_t;

struct tva_iresource_cmd_deregister_t {

  uint32_t handle;
    /**< Registration Handle. */

};


/**
 * TVA sends this event to the client requesting to enable the voice 
 * resource created using TVA_IRESOURCE_CMD_REGISTER.
 *
 * This event does not have any parameters.
 */
#define TVA_IRESOURCE_EVENT_REQUEST ( 0x0001318D )


/**
 * Voice agent sends this command to the TVA for vocoder session set-up.
 *
 * Voice agent shall grant voice resource provided TVA has requested for
 * the resource token via #TVA_IRESOURCE_EVENT_REQUEST. 
 */
#define TVA_IRESOURCE_CMD_GRANT ( 0x0001318E )

typedef struct tva_iresource_cmd_grant_t tva_iresource_cmd_grant_t;

struct tva_iresource_cmd_grant_t {

  uint32_t handle;
    /**< Registration Handle. */

};


/**
 * Voice agent shall issue this command to TVA, to tear down the vocoder 
 * session and release the voice resource.
 *
 * TVA shall send TVA_IRESOURCE_EVENT_RELEASED notifying voice agent that
 * the voice resource is successfully released. 
 */
#define TVA_IRESOURCE_CMD_REVOKE ( 0x0001318F )

typedef struct tva_iresource_cmd_revoke_t tva_iresource_cmd_revoke_t;

struct tva_iresource_cmd_revoke_t {

  uint32_t handle;
    /**< Registration Handle. */

};


/**
 * This events is an indication that the voice resource has been
 * successfully released.
 *
 * TVA shall notify this event to voice agent: Either Voice agent sends 
 * TVA_IRESOURCE_CMD_REVOKE to TVA or TVA tears down the vocoder and releases 
 * the voice resource without any revoke from voice agent.
 *
 * This does not have any parameter list.
 */
#define TVA_IRESOURCE_EVENT_RELEASED ( 0x00013190 )

#endif /* __TVA_IRESOURCE_IF_H__ */

