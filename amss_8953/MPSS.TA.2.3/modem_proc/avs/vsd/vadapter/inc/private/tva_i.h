#ifndef __TVA_I_H__
#define __TVA_I_H__

/**
  @file  tva_i.h
  @brief This file contains internal definitions of TDSCDMA voice adapter.
*/

/*
  ============================================================================

   Copyright (C) 2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //components/rel/avs.mpss/6.2.1/vsd/vadapter/inc/private/tva_i.h#2 $
  $Author: pwbldsvc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------

  ============================================================================
*/

/****************************************************************************
  Include files for Module
****************************************************************************/

/* MM APIs*/
#include "mmdefs.h"

/* WCDMA APIs. */
#include "tds_ext_api.h"

/* VOICE UTILS APIs. */
#include "voice_amr_if.h"
#include "voice_dsm_if.h"


/****************************************************************************
  TVA DEFINES
****************************************************************************/

/* Defined 4500 bytes Considering max 3 session/subscriptions .
 *
 * Note: one session consistes of one tva_session_object_t and one 
 *       tva_modem_subs_object_t
 */
#define TVA_HEAP_SIZE_V ( 4500 )

/* Size of VS work packet queue. */
#define TVA_NUM_WORK_PKTS_V ( 20 )

#define TVA_HANDLE_TOTAL_BITS_V ( 16 )
#define TVA_HANDLE_INDEX_BITS_V ( 4 ) /**< 4 bits = 16 handles. */
#define TVA_MAX_OBJECTS_V ( 1 << TVA_HANDLE_INDEX_BITS_V )

#define TVA_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[0x%08x], tva_state=%d", \
                            rc, tva_is_initialized, 0 ); } }

#define TVA_REPORT_FATAL_ON_ERROR( rc ) \
  { if ( rc ) { MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL, "Error[0x%08x], "\
                       "tva_state=%d", rc, tva_is_initialized ); } }

#define TVA_ACQUIRE_LOCK( lock ) \
  apr_lock_enter( lock );

#define TVA_RELEASE_LOCK( lock ) \
  apr_lock_leave( lock );

/* Definitions for error checking */
#define TVA_VOCODER_ID_UNDEFINED_V (0xFFFFFFFF)
#define TVA_VSID_UNDEFINED_V (0xFFFFFFFF)
#define TVA_CODEC_MODE_UNDEFINED ( 0xFFFFFFFF )

/**
 * Internal event triggered W-RLC queues channel data to AMR DSM queues.
 */
#define TVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE ( 0x00013189 )

/**
 * Internal event triggered to send silence frame if UL vocoder packet
 * is not available from DSP.
 */
#define TVA_INTERNAL_EVENT_SEND_SILENCE_FRAME ( 0x000131AE )

#define TVA_INTERNAL_EVENT_VS_OPEN ( 0x000131AB )

#define TVA_INTERNAL_EVENT_VS_CLOSE ( 0x000131AA )

#define TVA_AMR_DSM_Q_LEN ( 1 )

/****************************************************************************
  TVA thread states
****************************************************************************/

typedef enum tva_thread_state_enum_t
{
  TVA_THREAD_STATE_ENUM_INIT,
  TVA_THREAD_STATE_ENUM_READY,
  TVA_THREAD_STATE_ENUM_EXIT
}
  tva_thread_state_enum_t;

/***************************************************************************
  TVA WORK REQUEST ( TYPE / ITEM ) DEFINITIONS                              *
****************************************************************************/

typedef struct tva_cmd_packet_t {
  
  uint32_t cmd_id;
    /**< Command id issued from client. */
  void* params;
    /**< Structure associated to each cmd_id. */
} tva_cmd_packet_t;

typedef struct tva_event_packet_t {
  
  void* session_context;
    /**< Reference to vocie adapter session object. */
  uint32_t event_id;
    /**< Event id issued from client. */
  void* params;
    /**< Structure associated to each event_id. */
} tva_event_packet_t;

typedef enum tva_work_item_packet_type_t
{
  TVA_WORK_ITEM_PKT_TYPE_NONE,
  TVA_WORK_ITEM_PKT_TYPE_CMD,
  TVA_WORK_ITEM_PKT_TYPE_EVENT

} tva_work_item_packet_type_t;

typedef enum tva_work_item_queue_type_t
{
  TVA_WORK_ITEM_QUEUE_TYPE_NONE,
  TVA_WORK_ITEM_QUEUE_TYPE_NONGATING,
  TVA_WORK_ITEM_QUEUE_TYPE_GATING,

} tva_work_item_queue_type_t;



typedef struct tva_work_item_t {
  
  apr_list_node_t link;
  
  tva_work_item_packet_type_t pkt_type;
  
  void* packet;
   /**<
     * This is generic work packet, based on the pkt_type it can
     * to tva_cmd_packet_t or tva_event_packet_t.
     */
} tva_work_item_t;


/****************************************************************************
   THE COMMON OBJECT DEFINITIONS
****************************************************************************/

typedef enum tva_object_type_enum_t
{
   TVA_OBJECT_TYPE_ENUM_UNINITIALIZED,
   TVA_OBJECT_TYPE_ENUM_SIMPLE_JOB,
   TVA_OBJECT_TYPE_ENUM_SEQUENCER_JOB,
   TVA_OBJECT_TYPE_ENUM_MODEM_SUBSCRIPTION,
   TVA_OBJECT_TYPE_ENUM_SESSION,
   TVA_OBJECT_TYPE_ENUM_INVALID

} tva_object_type_enum_t;

typedef struct tva_object_header_t
{
  uint32_t handle;
   /**< The handle to the associated apr_objmgr_object_t instance. */
  tva_object_type_enum_t type;
   /**<
    * The object type defines the actual derived object.
    *
    * The derived object can be any custom object type. A session or a
    * command are two such custom object types. A free object entry is set
    * to VS_OBJECT_TYPE_ENUM_FREE.
    */
} tva_object_header_t;

/* forward declaration */
typedef struct tva_modem_subs_object_t tva_modem_subs_object_t;

/****************************************************************************
  THE SIMPLE JOB OBJECT
****************************************************************************/

typedef struct tva_simple_job_object_t tva_simple_job_object_t;

struct tva_simple_job_object_t {

  tva_object_header_t header;

  uint32_t context_handle;
   /**<
    * The parent-job handle indicates this sub-job is part of a batch-job.
    *
    * Set this value to -1 when it is unused or when there is no parent.
    */
  bool_t is_completed;
   /**< The command completed response flag. 0 is false and 1 is true. */
  uint32_t status;
   /**< The status returned by the command completion. */
};


/****************************************************************************
  THE SEQUENCER JOB OBJECT
****************************************************************************/

typedef struct tva_sequencer_job_object_t tva_sequencer_job_object_t;

struct tva_sequencer_job_object_t {

  tva_object_header_t header;

  uint32_t state;
   /**< The generic state variable. */
  union tva_object_t* subjob_obj;
   /**< The current sub-job object. */
  uint32_t status;
   /**< A status value. */
};


/****************************************************************************
  THE SESSION OBJECT
****************************************************************************/

typedef struct tva_session_object_t tva_session_object_t;

struct tva_session_object_t {

  tva_object_header_t header;

  uint32_t vsid;
    /**< System level published/documented Voice System ID. */

  tva_modem_subs_object_t* active_subs_obj;
    /**< 
      * Reference to the active modem subscription object that is currenlty
      * mapped to VSID.
      * Multiple subscription may map to same VSID, hence Adapter vsid session
      * needs to cache the active subscription context for sending command like
      * start/stop/sample_rate.
      */

  uint8_t vocoder_state;
    /**< This represents possible vocoder activites/states.*/

  apr_lock_t data_lock;
    /**< Lock to synchornisze data access. */

  /**<
    * Refers to logical data channel as per AMRIF1 frame format.
   .*/
  voice_amr_chan_state_t ul_chan_state;
    /**< Indicates actice AMR logical channels. */
  voice_amr_dsm_queue_t ul_queue;
    /**< AMR DSM queue for transmitting uplink AMR channels data. */
  voice_amr_chan_state_t dl_chan_state;
    /**< Indicates actice AMR logical channels. */
  voice_amr_dsm_queue_t dl_queue;
    /**< AMR DSM queue for retrieving downlink AMR channels data. */

  apr_timer_t sfg_timer;
	/**< Timer for silence frame generation. */

  /**<
    * Refers to Voice agent registeration for managing resource arbitration.
    */
  tva_icommon_event_callback_fn_t va_tva_event_cb;
    /**<
      * Callback function registered by voice agent to recieve command
      * responses and events.
      */
  void* va_session_context;
    /**< Session context provided voice agent during TVA_IRESOURCE_CMD_OPEN. */

  bool_t is_resource_granted;
    /**< Indicates that vocoder resource access is granted. */

  /**<
    * Reference to vocoder properties.
    */
  uint32_t vocoder_type;
    /**< Indicates the active vocoder type for the call. */

  uint32_t codec_mode;
    /**< Indicates the vocoder type requested for the call. */

  bool_t dtx_mode;
    /**< Indicates the active dtx mode for the call. */

  /**<
    * Reference to VS session control.
    * Applicable if adapter is configured with asid to vsid mapping.
    */
  uint32_t vs_handle;
    /**< Return handle for vocoder session. */

  vs_voc_buffer_t* vs_read_buf;

  vs_voc_buffer_t* primed_read_buf;

  vs_voc_buffer_t* vs_write_buf;

  bool_t is_vs_ready;
    /**< Inidcates vocoder session is running. */
};

/****************************************************************************
  THE MODEM SUBSCRIPTION INFO OBJECT
*****************************************************************************/

struct tva_modem_subs_object_t {

  tva_object_header_t header;

  sys_modem_as_id_e_type asid;
    /**< Modem Active Subcription ID. */

  uint32_t vsid;
    /**< System level published/documented voice system ID. */

  uint32_t pending_vsid;
    /**< System level published/documented voice system ID. */

  uint32_t tds_handle;
    /**< Return handle for GSM voice instance. */

  bool_t is_tds_ready;
    /**< Indicates the readiness of GSM protocol software for voice 
      * traffic exchange.
      */

  tva_session_object_t* session_obj;
    /**< Reference to wva session object having VSID as per the
      *  mapping available from voice agent. */
};

/****************************************************************************
  THE GENERIC TVA OBJECT
****************************************************************************/

typedef union tva_object_t {
  
  tva_object_header_t header;
  tva_simple_job_object_t simple_job;
  tva_sequencer_job_object_t sequencer_job;
  tva_modem_subs_object_t modem_subs_obj;
  tva_session_object_t session_obj;

} tva_object_t;


/****************************************************************************
 * PENDING MVS COMMAND DEFINITIONS                                          *
 ****************************************************************************/

typedef enum tva_gating_cmd_state_t
{
  TVA_GATING_CMD_STATE_FETCH,
  TVA_GATING_CMD_STATE_EXECUTE,
  TVA_GATING_CMD_STATE_CONTINUE

} tva_gating_cmd_state_t;

typedef struct tva_gating_control_t {
  
  apr_list_t cmd_q;
  /**< The gating (tva_work_item_t) queue. */
  tva_gating_cmd_state_t state;
  /**<
   * The current state of the gating command control.
   *
   * This variable is managed by the gating command processor. The
   * individual gating command controls indicates to the gating command
   * processor to complete or to delay the completion of the current
   * gating command.
   */
  tva_work_item_packet_type_t pkt_type;
  /**<
   * This is required for Gating commands to identify the packet type
   * below mentioned.
   */
  void* packet;
  /**<
   * The current (command) packet being processed.
   * This is generic reference to command packet, which could be
   * tva_cmd_packet_t or tva_event_packet_t.
   */
  tva_object_t* rootjob_obj;
  /**<
   * The rootjob_obj is a temporary storage for the current gating
   * command.
   */
} tva_gating_control_t;


/****************************************************************************
 * TVA INTERNAL ROUTINES                                                    *
 ****************************************************************************/

TVA_INTERNAL uint32_t tva_queue_work_packet (
  tva_work_item_queue_type_t queue_type,
  tva_work_item_packet_type_t pkt_type,
  void* packet
);


TVA_INTERNAL uint32_t tva_free_cmd_packet (
  tva_cmd_packet_t* cmd_packet
);


TVA_INTERNAL uint32_t tva_prepare_and_dispatch_cmd_packet (
  uint32_t cmd_id,
  void* params,
  uint32_t size
);


TVA_INTERNAL uint32_t tva_free_event_packet (
  tva_event_packet_t* event_packet
);


TVA_INTERNAL uint32_t tva_prepare_and_dispatch_event_packet (
  void* session_context,
  uint32_t event_id,
  void* params,
  uint32_t size
);


TVA_INTERNAL void tva_signal_run ( 
  void
);


#endif  /* __TVA_I_H__ */
