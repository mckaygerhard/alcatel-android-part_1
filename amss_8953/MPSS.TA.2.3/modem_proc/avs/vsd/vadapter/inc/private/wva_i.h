#ifndef __WVA_I_H__
#define __WVA_I_H__

/**
  @file  wva_i.h
  @brief This file contains internal definitions of WCDMA voice adapter.
*/

/*
  ============================================================================

   Copyright (C) 2015 Qualcomm Technologies, Inc.
   All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================

                             Edit History

  $Header: //components/rel/avs.mpss/6.2.1/vsd/vadapter/inc/private/wva_i.h#2 $
  $Author: pwbldsvc $

  when      who   what, where, why
  --------  ---   ------------------------------------------------------------

  ============================================================================
*/

/****************************************************************************
  Include files for Module
****************************************************************************/

/* MM APIs*/
#include "mmdefs.h"

/* WCDMA APIs. */
#include "wcdmamvsif.h"

/* VOICE UTILS APIs. */
#include "voice_amr_if.h"
#include "voice_dsm_if.h"


/****************************************************************************
  WVA DEFINES
****************************************************************************/

/* Defined 4500 bytes Considering max 3 session/subscriptions .
 *
 * Note: one session consistes of one wva_session_object_t and one 
 *       wva_modem_subs_object_t
 */
#define WVA_HEAP_SIZE_V ( 4500 )

/* Size of VS work packet queue. */
#define WVA_NUM_WORK_PKTS_V ( 20 )

#define WVA_HANDLE_TOTAL_BITS_V ( 16 )
#define WVA_HANDLE_INDEX_BITS_V ( 4 ) /**< 4 bits = 16 handles. */
#define WVA_MAX_OBJECTS_V ( 1 << WVA_HANDLE_INDEX_BITS_V )

#define WVA_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[0x%08x], wva_state=%d", \
                            rc, wva_is_initialized, 0 ); } }

#define WVA_REPORT_FATAL_ON_ERROR( rc ) \
  { if ( rc ) { MSG_2( MSG_SSID_DFLT, MSG_LEGACY_FATAL, "Error[0x%08x], "\
                       "wva_state=%d", rc, wva_is_initialized ); } }

#define WVA_ACQUIRE_LOCK( lock ) \
  apr_lock_enter( lock );

#define WVA_RELEASE_LOCK( lock ) \
  apr_lock_leave( lock );

/* Definitions for error checking */
#define WVA_VOCODER_ID_UNDEFINED_V (0xFFFFFFFF)
#define WVA_VSID_UNDEFINED_V (0xFFFFFFFF)
#define WVA_CODEC_MODE_UNDEFINED ( 0xFFFFFFFF )

/**
 * Internal event triggered W-RLC queues channel data to AMR DSM queues.
 */
#define WVA_INTERNAL_EVENT_DL_BUFFER_AVAILABLE ( 0x00013189 )

/**
 * Internal event triggered to send silence frame if UL vocoder packet
 * is not available from DSP.
 */
#define WVA_INTERNAL_EVENT_SEND_SILENCE_FRAME ( 0x000131AE )

#define WVA_INTERNAL_EVENT_VS_OPEN ( 0x000131AC )

#define WVA_INTERNAL_EVENT_VS_CLOSE ( 0x000131AD )

#define WVA_AMR_DSM_Q_LEN ( 1 )

/****************************************************************************
  WVA thread states
****************************************************************************/

typedef enum wva_thread_state_enum_t
{
  WVA_THREAD_STATE_ENUM_INIT,
  WVA_THREAD_STATE_ENUM_READY,
  WVA_THREAD_STATE_ENUM_EXIT
}
  wva_thread_state_enum_t;

/***************************************************************************
  WVA WORK REQUEST ( TYPE / ITEM ) DEFINITIONS                              *
****************************************************************************/

typedef struct wva_cmd_packet_t {
  
  uint32_t cmd_id;
    /**< Command id issued from client. */
  void* params;
    /**< Structure associated to each cmd_id. */
} wva_cmd_packet_t;

typedef struct wva_event_packet_t {
  
  void* session_context;
    /**< Reference to vocie adapter session object. */
  uint32_t event_id;
    /**< Event id issued from client. */
  void* params;
    /**< Structure associated to each event_id. */
} wva_event_packet_t;

typedef enum wva_work_item_packet_type_t
{
  WVA_WORK_ITEM_PKT_TYPE_NONE,
  WVA_WORK_ITEM_PKT_TYPE_CMD,
  WVA_WORK_ITEM_PKT_TYPE_EVENT

} wva_work_item_packet_type_t;

typedef enum wva_work_item_queue_type_t
{
  WVA_WORK_ITEM_QUEUE_TYPE_NONE,
  WVA_WORK_ITEM_QUEUE_TYPE_NONGATING,
  WVA_WORK_ITEM_QUEUE_TYPE_GATING,

} wva_work_item_queue_type_t;



typedef struct wva_work_item_t {
  
  apr_list_node_t link;
  
  wva_work_item_packet_type_t pkt_type;
  
  void* packet;
   /**<
     * This is generic work packet, based on the pkt_type it can
     * to wva_cmd_packet_t or wva_event_packet_t.
     */
} wva_work_item_t;


/****************************************************************************
   THE COMMON OBJECT DEFINITIONS
****************************************************************************/

typedef enum wva_object_type_enum_t
{
   WVA_OBJECT_TYPE_ENUM_UNINITIALIZED,
   WVA_OBJECT_TYPE_ENUM_SIMPLE_JOB,
   WVA_OBJECT_TYPE_ENUM_SEQUENCER_JOB,
   WVA_OBJECT_TYPE_ENUM_MODEM_SUBSCRIPTION,
   WVA_OBJECT_TYPE_ENUM_SESSION,
   WVA_OBJECT_TYPE_ENUM_INVALID

} wva_object_type_enum_t;

typedef struct wva_object_header_t
{
  uint32_t handle;
   /**< The handle to the associated apr_objmgr_object_t instance. */
  wva_object_type_enum_t type;
   /**<
    * The object type defines the actual derived object.
    *
    * The derived object can be any custom object type. A session or a
    * command are two such custom object types. A free object entry is set
    * to VS_OBJECT_TYPE_ENUM_FREE.
    */
} wva_object_header_t;

/* forward declaration */
typedef struct wva_modem_subs_object_t wva_modem_subs_object_t;

/****************************************************************************
  THE SIMPLE JOB OBJECT
****************************************************************************/

typedef struct wva_simple_job_object_t wva_simple_job_object_t;

struct wva_simple_job_object_t {

  wva_object_header_t header;

  uint32_t context_handle;
   /**<
    * The parent-job handle indicates this sub-job is part of a batch-job.
    *
    * Set this value to -1 when it is unused or when there is no parent.
    */
  bool_t is_completed;
   /**< The command completed response flag. 0 is false and 1 is true. */
  uint32_t status;
   /**< The status returned by the command completion. */
};


/****************************************************************************
  THE SEQUENCER JOB OBJECT
****************************************************************************/

typedef struct wva_sequencer_job_object_t wva_sequencer_job_object_t;

struct wva_sequencer_job_object_t {

  wva_object_header_t header;

  uint32_t state;
   /**< The generic state variable. */
  union wva_object_t* subjob_obj;
   /**< The current sub-job object. */
  uint32_t status;
   /**< A status value. */
};


/****************************************************************************
  THE SESSION OBJECT
****************************************************************************/

typedef struct wva_session_object_t wva_session_object_t;

struct wva_session_object_t {

  wva_object_header_t header;

  uint32_t vsid;
    /**< System level published/documented Voice System ID. */

  wva_modem_subs_object_t* active_subs_obj;
    /**< 
      * Reference to the active modem subscription object that is currenlty
      * mapped to VSID.
      * Multiple subscription may map to same VSID, hence Adapter vsid session
      * needs to cache the active subscription context for sending command like
      * start/stop/sample_rate.
      */

  uint8_t vocoder_state;
    /**< This represents possible vocoder activites/states.*/

  apr_lock_t data_lock;
    /**< Lock to synchornisze data access. */

  /**<
    * Refers to logical data channel as per AMRIF1 frame format.
   .*/
  voice_amr_chan_state_t ul_chan_state;
    /**< Indicates actice AMR logical channels. */
  voice_amr_dsm_queue_t ul_queue;
    /**< AMR DSM queue for transmitting uplink AMR channels data. */
  voice_amr_chan_state_t dl_chan_state;
    /**< Indicates actice AMR logical channels. */
  voice_amr_dsm_queue_t dl_queue;
    /**< AMR DSM queue for retrieving downlink AMR channels data. */

  apr_timer_t sfg_timer;
    /**< Timer for silence frame generation. */

  /**<
    * Refers to Voice agent registeration for managing resource arbitration.
    */
  wva_icommon_event_callback_fn_t va_wva_event_cb;
    /**<
      * Callback function registered by voice agent to recieve command
      * responses and events.
      */
  void* va_session_context;
    /**< Session context provided voice agent during WVA_IRESOURCE_CMD_OPEN. */

  bool_t is_resource_granted;
    /**< Indicates that vocoder resource access is granted. */

  /**<
    * Reference to vocoder properties.
    */
  uint32_t vocoder_type;
    /**< Indicates the active vocoder type for the call. */

  uint32_t codec_mode;
    /**< Indicates the vocoder type requested for the call. */

  bool_t dtx_mode;
    /**< Indicates the active dtx mode for the call. */

  /**<
    * Reference to VS session control.
    * Applicable if adapter is configured with asid to vsid mapping.
    */
  uint32_t vs_handle;
    /**< Return handle for vocoder session. */

  vs_voc_buffer_t* vs_read_buf;

  vs_voc_buffer_t* primed_read_buf;

  vs_voc_buffer_t* vs_write_buf;

  bool_t is_vs_ready;
    /**< Inidcates vocoder session is running. */
};

/****************************************************************************
  THE MODEM SUBSCRIPTION INFO OBJECT
*****************************************************************************/

struct wva_modem_subs_object_t {

  wva_object_header_t header;

  sys_modem_as_id_e_type asid;
    /**< Modem Active Subcription ID. */

  uint32_t vsid;
    /**< System level published/documented voice system ID. */

  uint32_t pending_vsid;
    /**< System level published/documented voice system ID. */

  uint32_t wcdma_handle;
    /**< Return handle for GSM voice instance. */

  bool_t is_wcdma_ready;
    /**< Indicates the readiness of GSM protocol software for voice 
      * traffic exchange.
      */

  wva_session_object_t* session_obj;
    /**< Reference to wva session object having VSID as per the
      *  mapping available from voice agent. */
};

/****************************************************************************
  THE GENERIC WVA OBJECT
****************************************************************************/

typedef union wva_object_t {
  
  wva_object_header_t header;
  wva_simple_job_object_t simple_job;
  wva_sequencer_job_object_t sequencer_job;
  wva_modem_subs_object_t modem_subs_obj;
  wva_session_object_t session_obj;

} wva_object_t;


/****************************************************************************
 * PENDING MVS COMMAND DEFINITIONS                                          *
 ****************************************************************************/

typedef enum wva_gating_cmd_state_t
{
  WVA_GATING_CMD_STATE_FETCH,
  WVA_GATING_CMD_STATE_EXECUTE,
  WVA_GATING_CMD_STATE_CONTINUE

} wva_gating_cmd_state_t;

typedef struct wva_gating_control_t {
  
  apr_list_t cmd_q;
  /**< The gating (wva_work_item_t) queue. */
  wva_gating_cmd_state_t state;
  /**<
   * The current state of the gating command control.
   *
   * This variable is managed by the gating command processor. The
   * individual gating command controls indicates to the gating command
   * processor to complete or to delay the completion of the current
   * gating command.
   */
  wva_work_item_packet_type_t pkt_type;
  /**<
   * This is required for Gating commands to identify the packet type
   * below mentioned.
   */
  void* packet;
  /**<
   * The current (command) packet being processed.
   * This is generic reference to command packet, which could be
   * wva_cmd_packet_t or wva_event_packet_t.
   */
  wva_object_t* rootjob_obj;
  /**<
   * The rootjob_obj is a temporary storage for the current gating
   * command.
   */
} wva_gating_control_t;


/****************************************************************************
 * WVA INTERNAL ROUTINES                                                    *
 ****************************************************************************/

WVA_INTERNAL uint32_t wva_queue_work_packet (
  wva_work_item_queue_type_t queue_type,
  wva_work_item_packet_type_t pkt_type,
  void* packet
);


WVA_INTERNAL uint32_t wva_free_cmd_packet (
  wva_cmd_packet_t* cmd_packet
);


WVA_INTERNAL uint32_t wva_prepare_and_dispatch_cmd_packet (
  uint32_t cmd_id,
  void* params,
  uint32_t size
);


WVA_INTERNAL uint32_t wva_free_event_packet (
  wva_event_packet_t* event_packet
);


WVA_INTERNAL uint32_t wva_prepare_and_dispatch_event_packet (
  void* session_context,
  uint32_t event_id,
  void* params,
  uint32_t size
);


WVA_INTERNAL void wva_signal_run ( 
  void
);


#endif  /* __WVA_I_H__ */
