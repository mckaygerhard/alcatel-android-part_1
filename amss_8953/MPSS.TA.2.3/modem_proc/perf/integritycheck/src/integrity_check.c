/*==========================================================================
 * FILE:         integrity_xor.c
 *
 * SERVICES:     INTEGRITY XOR
 *
 * DESCRIPTION:  This file provides the implementation of checking corruption in
 *               in the RO_compressed region using XOR Checksum
 *
 * Copyright (c) 2015 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
=============================================================================*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/30/15   rr      Created file
===========================================================================*/
#include <stdlib.h>
#include <qurt.h>
#include <hexagon_protos.h>
#include "ULogFront.h"
#include "err.h"

#define SZ (14)
#define BLOCK_SIZE	 (1 << SZ)
#define CHECKSUM_SIZE    (4)
#define MAX_CHECKSUM_BLOCKS (1 << (SZ - 2))
#define MAX_SIZE (MAX_CHECKSUM_BLOCKS << (SZ))
#define CACHE_LINE_SHIFT (5)
#define CACHE_LINE_SZ  (1 << CACHE_LINE_SHIFT)
#define PREFETCH_DIST ((BLOCK_SIZE + CACHE_LINE_SZ - 1)/CACHE_LINE_SZ)


static ULogHandle ulog_handle;

unsigned int __attribute__((section (".data"))) perf_integritycheck_compress_section_begin = 0;
unsigned int __attribute__((section (".data"))) perf_integritycheck_compress_section_size = 0;

unsigned int __attribute__((section (".data"))) perf_integritycheck_checksum_buf[MAX_CHECKSUM_BLOCKS];
unsigned int perf_integritycheck_first_corrupt_page_addr = 0;
unsigned int perf_integritycheck_num_corrupted = 0;
/************************************************************************************//**
*\brief checks for corruption in the compressed region when there is a crash using Fletcher's checksum		
*\param input_arg: none
*\return none
****************************************************************************************/

void integrity_fletcher()
{
	ULOG_RT_PRINTF_0(ulog_handle, "Running Integrity Check on candidate_compress");
	
	unsigned int sa, sb, cycles1, cycles2, step_end, start , ii, result, end;
	unsigned int count = 0;
	end = perf_integritycheck_compress_section_begin + perf_integritycheck_compress_section_size;
	
	cycles1 = qurt_sysclock_get_hw_ticks();
	
	for(start = perf_integritycheck_compress_section_begin; start < end; start = start + BLOCK_SIZE)
	{
		asm volatile ("l2fetch(%[addr],%[dim])" : : 
			[addr] "r" (start + BLOCK_SIZE), 
			[dim] "r" ( Q6_P_combine_IR(CACHE_LINE_SZ, Q6_R_combine_RlRl(CACHE_LINE_SZ, PREFETCH_DIST)))
			: "memory");
		 sa = 0;
		 sb = 0;
		 step_end = start + BLOCK_SIZE;
		 if(step_end > end)
			 step_end = end;
		 for( ii = start;ii < step_end; ii = ii + CHECKSUM_SIZE)
		 {	
			sa = sa + (*((uint32_t*)ii));
			sb = sb + sa;
		 }
		 result = sb ^ sa;
		 if(perf_integritycheck_checksum_buf[count] != result)
		{
			if(perf_integritycheck_num_corrupted == 0)
			  perf_integritycheck_first_corrupt_page_addr  = start; /*get the address of the first corrupted page*/
			perf_integritycheck_num_corrupted++;
			
		}
		count++;
	}
	
	cycles2 = qurt_sysclock_get_hw_ticks();
	ULOG_RT_PRINTF_3(ulog_handle, "Duration(ticks): %u, Duration (us): %u, blocks:%u", cycles2 - cycles1, ((cycles2 - cycles1)*13) >> 8,count);
	if(perf_integritycheck_num_corrupted != 0){
		/*save a copy of the first corrupted page before FW overlay*/
		ULOG_RT_PRINTF_0(ulog_handle, "Integrity Check Failed");
		memcpy((void*)perf_integritycheck_checksum_buf,(void*)perf_integritycheck_first_corrupt_page_addr , BLOCK_SIZE); 
		ULOG_RT_PRINTF_2(ulog_handle, "Address of first corrupt block: %u, number of corrupted blocks: %u", perf_integritycheck_first_corrupt_page_addr, perf_integritycheck_num_corrupted);
	}
	else
		ULOG_RT_PRINTF_0(ulog_handle, "Integrity Check Pass");
	
}

/************************************************************************************//**
*\brief registers integrity_xor with the crash handler
*\param input_arg: none
*\return void
****************************************************************************************/

void init_integrity(void)
{

	if( perf_integritycheck_compress_section_size != 0) /*check if integrity_check.py was executed*/
	{
		ULogFront_RealTimeInit(&ulog_handle, "Integrity Check", 128, ULOG_MEMORY_LOCAL, ULOG_LOCK_OS);
		if(!(err_crash_cb_register(integrity_fletcher)))
			ULOG_RT_PRINTF_0(ulog_handle, "unable to attach to crash handler");
	}

}
