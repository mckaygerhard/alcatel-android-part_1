
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               DL PAGER TOP LEVEL HEADER FILE

GENERAL DESCRIPTION

  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All Rights Reserved. QUALCOMM Proprietary and Confidential.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/


/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/perf.mpss/1.0.4/q6zip/core/inc/dlpager_types.h#1 $ $DateTime: 2016/02/09 16:18:42 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/09/14   cp      Header file for dlpager types
===========================================================================*/

#ifndef __DLPAGER_TYPES_H__
#define __DLPAGER_TYPES_H__

#include "qurt.h"

#define THREAD_NULL (0xffffffff)
typedef enum
{
  TLB_MISS_X,  
  TLB_MISS_R,  
  TLB_MISS_W,  
  EVICT_PAGE,  
  DECOMPRESSION_COMPLETE,
  COMPRESSION_COMPLETE,
  SOFT_CLEAN_PAGE,
  HARD_CLEAN_PAGE,
  SOFT_CLEAN_FAILED,
  INVALID_EVENT,
  MAX_EVENTS,
  EVENT_ENUM_SIZE = 0xf /* force enum to be atleast 4 bits */
}dlpager_event_t;

/* enum for page states */
typedef enum
{
  UNMAPPED_CLEAN,
  UNMAPPED_BSS,
  UNMAPPED_CLEAN_DECOMPRESSING,
  UNMAPPED_DIRTY_DECOMPRESSING,
  UNMAPPED_DIRTY_COMPRESSING_ALLOCATED,
  UNMAPPED_CLEAN_ALLOCATED,
  UNMAPPED_HARD_CLEAN_COMPRESSING,
  MAPPED_CLEAN,  
  MAPPED_DIRTY,
  MAPPED_DIRTY_COMPRESSING,
  INVALID_STATE,
  MAX_STATES,
  PAGE_STATE_ENUM_SIZE = 0xf /* force enum to be atleast 4 bits*/
}dlpager_page_state_t;

typedef enum
{
  SOFT_CLEAN = SOFT_CLEAN_PAGE,
  HARD_CLEAN = HARD_CLEAN_PAGE,
  NO_CLEAN = INVALID_EVENT,
  CLEAN_ENUM_SIZE = 0xf /* force same size as dlpager_event_t */
}dlpager_clean_t;

typedef enum
{
  LOW_PRIORITY = SOFT_CLEAN_PAGE,
  HIGH_PRIORITY = HARD_CLEAN_PAGE,
}dlpager_priority_t;

/** @brief Type for an I/O vector - something that has address and length */
typedef struct
{
    /** @brief Address associated with this I/O vector */
    unsigned int addr;

    /** @brief Length (bytes) of this I/O vector */
    unsigned int len;

} dlpager_iovec_t;

typedef void (*dlpager_handler_fptr_t)(unsigned int, qurt_thread_t);

#endif /* __DLPAGER_TYPES_H__ */

