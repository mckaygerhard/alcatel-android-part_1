/**
   @file q6zip_ipa_conf.h 
   @brief Q6Zip IPA configuration module 
    
   Copyright (c) 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.
   Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#pragma once

/** @brief Datatype for the various flags/knobs */
typedef struct
{
    /** @brief Flag to control de-touring request to Q6Zip SW if clock boost is
               in progress */
    boolean q6zip_sw_during_clock_boost;

    /** @brief Flag to control de-touring request to Q6Zip SW if firmware is in
               danger */
    boolean q6zip_sw_during_firmware_danger;

} q6zip_ipa_conf_flags_t;

/** @brief Data type that holds Q6Zip IPA configuration */
typedef struct
{
    q6zip_ipa_conf_flags_t flags;

} q6zip_ipa_conf_t;

extern q6zip_ipa_conf_t q6zip_ipa_conf_singleton;

/**
 * @brief Initialize Q6Zip IPA configuration
 * @param [in] conf Configuration data 
 * @warning Must be invoked only *AFTER* EFS has initialized 
 */
extern void q6zip_ipa_conf_init (q6zip_ipa_conf_t * conf);

/**
 * @brief RC init function to initialize Q6Zip IPA configuration 
 * @warning Intended to be used via RC init path 
 */
extern void q6zip_ipa_conf_rcinit (void);

#if defined( Q6ZIP_UNIT_TEST )
/**
 * @brief Print Q6Zip IPA configuration
 * @param [in] conf Pointer to configuration data
 */
extern void q6zip_ipa_conf_print (q6zip_ipa_conf_t const * conf);
#endif
