#ifndef DLPAGER_LOG_VERSION_2_H
#define DLPAGER_LOG_VERSION_2_H
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

               DL PAGER LOG (version 2) HEADER

GENERAL DESCRIPTION
  This header exists to fullfil a compilation dependency between Q6Zip in
  perf/ (perf.mpss) and DL pager in core/kernel/dlpager (core.mpss). Version
  2 mirrors changes to its original in core.mpss for AT.1.X. Since perf.mpss
  is shared across multiple PLs (TA, AT etc... circa Oct 2015), a separate
  copy (ie dlpager_log_version_2.h) is required for coexistence

  Copyright (c) 2015 by Qualcomm Technologies, Inc.  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

            EDIT HISTORY FOR MODULE

$Header: //components/rel/perf.mpss/1.0.4/q6zip/core/inc/dlpager_log_version_2.h#1 $ $DateTime: 2016/02/09 16:18:42 $ $Author: pwbldsvc $

when       who       what, where, why
---------- --------- ----------------------------------------------------------
10/06/2015 anandj    Created
===========================================================================*/
#pragma once
#include <stdio.h>
#include <qurt.h>
#include <stdlib.h>
#include <dlpager_types.h>

#define DLPAGER_LOG_ENTRIES_BITS  11
#define DLPAGER_LOG_ENTRIES_MASK  ((1<<DLPAGER_LOG_ENTRIES_BITS)-1)
#define DLPAGER_LOG_ENTRIES_MAX   (1<<DLPAGER_LOG_ENTRIES_BITS) /**< No. of log entries */

/** @brief DL pager task-fault related events */
typedef enum
{
    DLP_FAULT,            /**< Pagefault occured       */
    DLP_REMOVE_CLK_VOTE,  /**< Clocks were devoted     */
    DLP_FIRST_CLK_VOTE,   /**< Initial clock vote      */
    DLP_MAX_CLK_VOTE,     /**< Vote for maximum clocks */

    DLP_RESTART_TIMER,
    DLP_FIRST_CLK_REQ,
    DLP_MAX_CLK_REQ,
    DLP_TIMER_REQ,

    DLP_EVENT_UNUSED_8,
    DLP_EVENT_UNUSED_9,
    DLP_EVENT_UNUSED_A,
    DLP_EVENT_UNUSED_B,
    DLP_EVENT_UNUSED_C,
    DLP_EVENT_UNUSED_D,
    DLP_EVENT_UNUSED_E,
    DLP_EVENT_UNUSED_F,

    DLP_EVENT_MAX = 0xF

    /* @warning Only 4 bits are allocated for dlpager_tasklog_event_t in
       dlpager_tasklog_entry_t (below). */
} dlpager_tasklog_event_t;

/** @brief Loader used for decompression/compression */
typedef enum
{
    DLP_LOADER_Q6ZIP_SW,  /**< SW  */
    DLP_LOADER_Q6ZIP_IPA, /**< IPA */
    DLP_LOADER_NA         /**< Not applicable. Shows up as 0xFE in Trace32! */
} dlpager_loader_t;

/** Log entry that logs the pager activity. The entries are logged when 
    when event_t happens. */
typedef struct {
/* 07-00 */ uint64_t start_ticks;   /**< XO ticks */
/* 0F-08 */ uint64_t end_ticks;     /**< XO ticks */

/* 13-10 */ uint32_t fault_addr;    /**< Faulting address */
/* 17-14 */ uint32_t evicted_va;    /**< Evicted VA page */
/* 1B-18 */ uint32_t evicted_pa;    /**< Evicted PA page */

/* 1E-1C */ uint32_t thread_id:22;   /**< Fault thread Id */
/* 1E */    dlpager_loader_t loader:2;       /**< Loader used  */
/* 1F */    dlpager_event_t page_event:4;    /**< Event passed by QuRT */
/* 1F */    dlpager_tasklog_event_t log_event:4;   /**< dlpager_tasklog_event_t, extra bit since enum may be signed */

} dlpager_tasklog_entry_t;

extern dlpager_tasklog_entry_t dlpager_tasklog[DLPAGER_LOG_ENTRIES_MAX]  __attribute__((aligned(32)));
extern unsigned long long dlpager_last_tick; /* Last 64 bit tick logged */

/*===========================================================================

             FUNCTION DEFINITIONS

===========================================================================*/
extern unsigned dlpager_pagelog_start(dlpager_page_state_t start_state, dlpager_page_state_t end_state, dlpager_event_t event, void* addr, qurt_thread_t thread_id);

extern unsigned dlpager_tasklog_start( dlpager_tasklog_event_t log_event, unsigned fault_addr, dlpager_event_t page_event, qurt_thread_t thread_id, unsigned evict_va, unsigned evict_pa );

//extern void dlpager_tasklog_set_loader( unsigned int idx, dlpager_loader_t loader );
//extern void dlpager_tasklog_set_clk( unsigned int idx, unsigned clk );
//extern void dlpager_tasklog_stop( unsigned log_idx );

static inline unsigned long long get_qtimer(void)
{
  unsigned long long ret;
  asm volatile (" %0 = c31:30 " : "=r"(ret));
  return ret;
}


static inline void dlpager_tasklog_set_clk( unsigned int idx, unsigned clk )
{
  dlpager_tasklog[idx].fault_addr = clk;
}

static inline void dlpager_tasklog_set_loader( unsigned int idx, dlpager_loader_t loader )
{
  dlpager_tasklog[ idx ].loader = loader;
}

static inline void dlpager_tasklog_stop( unsigned log_idx )
{
  dlpager_tasklog[log_idx].end_ticks = get_qtimer();
  dlpager_last_tick = dlpager_tasklog[log_idx].end_ticks;
}

#endif /* DLPAGER_LOG_MAIN_H */


