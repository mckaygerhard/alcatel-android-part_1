#include "assert.h"
#include "fs_public.h"
#include "msg_diag_service.h"
#if !defined( Q6ZIP_ENABLE_ASSERTS )
#define Q6ZIP_ENABLE_ASSERTS
#endif
#include "q6zip_assert.h"
#include "q6zip_types.h"
#include "q6zip_ipa.h"
#include "q6zip_test.h"
#include "q6zip_test_registry.h"
#include "q6zip_test_scenario.h"
#include "q6zip_types.h"
#include "q6zip_worker.h"
#include <stdio.h>

struct q6zip_test_scenario_global_s
{
    q6zip_worker_t worker;
    qurt_timer_t timer;
    qurt_signal_t signal;

    /** @brief Scenario being run */
    q6zip_test_scenario_t * scenario;

    q6zip_iface_t const * q6zip;

};

static uint8 buffer[ 4096 ];
static q6zip_buffer_t dict_buffer;

static q6zip_test_scenario_global_t global;

q6zip_test_scenario_global_t * q6zip_test_global;

static void * work (void * argument)
{
    unsigned int signals;
    q6zip_test_scenario_global_t * global = (q6zip_test_scenario_global_t *)argument;

    qurt_timer_restart( global->timer, 250000 );
    signals = qurt_signal_wait_any( &global->signal, Q6ZIP_TEST_SIGNAL_TMO );
    qurt_signal_clear( &global->signal, signals );

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "[q6zip/test] worker started", 0, 0, 0 );

    #if 0
    dict_buffer.ptr = buffer;
    dict_buffer.cap = sizeof( buffer );

    /* @todo anandj Hack to wait on efs_initialize() */

    ASSERT( !q6zip_test_load_file( "/q6zip/ro/tb_q6zip_v3md2/dict.bin", &dict_buffer ) );
    #else
    (void)buffer;
    (void)dict_buffer;
    #endif

    while ( TRUE )
    {
        signals = qurt_signal_wait_any( &global->signal, Q6ZIP_TEST_SIGNALS );
        qurt_signal_clear( &global->signal, signals );

        if ( signals & Q6ZIP_TEST_SIGNAL_QUIT )
        {
            break;
        }

        if ( signals & Q6ZIP_TEST_SIGNAL_SCENARIO_START )
        {
            if ( global->scenario )
            {
                global->scenario->ops.start( global->scenario );
            }
        }
    }

    #if defined( Q6ZIP_UNIT_TEST )
    /* Wait enough time for last request to be processed
       @todo anandj This is hacky. Need a clear handshake here! */
    qurt_timer_sleep( 250000 );

    #endif


    return NULL;
}

void q6zip_test_init (void)
{
    unsigned int      i;
    qurt_timer_attr_t timer_attr;

    (void)q6zip_test_registry;
    (void)i;
    q6zip_test_global = &global;

    /* Select Q6ZIP implementation to use */
    global.q6zip = &Q6ZIP_IPA;

    qurt_signal_init( &global.signal );

    qurt_timer_attr_init( &timer_attr);
    qurt_timer_attr_set_type( &timer_attr, QURT_TIMER_ONESHOT );
    qurt_timer_attr_set_duration( &timer_attr, 50000 );

    qurt_timer_create( &global.timer, &timer_attr, &global.signal, Q6ZIP_TEST_SIGNAL_TMO );
    qurt_timer_stop( global.timer );

    /* Initialize tests */
    for ( i = 0; i < Q6ZIP_ARRAY_SIZE( q6zip_test_registry ); i++ )
    {
        if ( q6zip_test_registry[ i ]->ops.init )
        {
            q6zip_test_registry[ i ]->ops.init( q6zip_test_registry[ i ] );
        }
    }

    q6zip_worker_fork( &global.worker, "q6zip_test_worker", 4096, work, 100, &global );
}

void q6zip_test_adapter_response (q6zip_request_t * request, q6zip_response_t const * response)
{
    /* Forward response to the scenario for handling */
    if ( global.scenario )
    {
        global.scenario->ops.response( global.scenario, request, response );
    }

    q6zip_request_free( &request );
}

q6zip_iface_t const * q6zip_test_get_implementation (void)
{
    return global.q6zip;
}

void q6zip_test_fini (void)
{
    qurt_signal_set( &global.signal, Q6ZIP_TEST_SIGNAL_QUIT );
    q6zip_worker_join( &global.worker );
}

boolean q6zip_test_handle_diag_msg (
    uint8 const * msg,
    uint16        len
)
{
    boolean processed = FALSE;

    /* First stop whatever is going on */
    if ( global.scenario )
    {
        qurt_signal_set( &global.signal, Q6ZIP_TEST_SIGNAL_STOP );
        global.scenario->ops.stop( global.scenario );
    }

    if ( len == 0 )
    {
        return FALSE;
    }

    /* @todo anandj Ugly hack to ensure that STOP gets processed */
    qurt_timer_sleep( 500000 );

    /* Start? */
    if ( msg [ 0 ] & 0x80 )
    {
        /* Which scenario? */
        switch ( msg[ 0 ] & 0x7F )
        {
            case 120: /* 0xF8 */
                global.scenario = &q6zip_test_scenario_copy;
                processed = TRUE;
                break;

            case 121: /* 0xF9 */
                global.scenario = &q6zip_test_scenario_mixed;
                processed = TRUE;
                break;

            case 122: /* 0xFA */
                global.scenario = &q6zip_test_scenario_error_ro;
                processed = TRUE;
                break;

            case 123: /* 0xFB */
                global.scenario = &q6zip_test_scenario_zip_rw;
                processed = TRUE;
                break;

            case 124: /* 0xFC */
                global.scenario = &q6zip_test_scenario_unzip_rw;
                processed = TRUE;
                break;

            case 125: /* 0xFD */
                global.scenario = &q6zip_test_scenario_unzip_ro;
                processed = TRUE;
                break;

            case 126: /* 0xFE */
                global.scenario = &q6zip_test_scenario_init;
                processed = TRUE;
                break;

            case 127:
                qurt_signal_set( &global.signal, Q6ZIP_TEST_SIGNAL_QUIT );

                processed = TRUE;
                /* Fall-through intended */

            default:
                global.scenario = NULL;
                break;
        }

        if ( global.scenario )
        {
            /* Have the scenario configure itself */
            global.scenario->ops.conf( global.scenario, NULL, msg, len );

            /* Notify worker thread to run the selected scenario */
            qurt_signal_set( &global.signal, Q6ZIP_TEST_SIGNAL_SCENARIO_START );
        }
    }
    else
    {
        /* Running test/scenario was stopped */
        processed = TRUE;
    }

    return processed;
}

int q6zip_test_load_file (char const * filename, q6zip_buffer_t *buffer)
{
    int fd;
    int error = 0;
   
    buffer->len = 0;

    if ( !buffer->ptr )
    {
        return EINVAL;
    }

    fd = efs_open( filename, O_RDONLY );

    if ( fd >= 0 )
    {
        /*static char temp[ 4400 ];  Worstcase sized buffer */
        boolean completed = TRUE;   /* Successfully read file? */
        unsigned int offset;
        unsigned int nbytes;
        unsigned int bytes_to_read;

        offset = 0;

        while ( TRUE )
        {
            bytes_to_read = Q6ZIP_MIN( 128, buffer->cap - offset );

            nbytes = efs_read( fd, ((uint8 * )buffer->ptr) + offset, bytes_to_read );

            offset += nbytes;

            if ( nbytes == 0 )
            {
                /* EOF reached OR buffer exhausted */
                break;
            }
        }

        if ( completed )
        {
            buffer->len = offset;
        }
        else
        {
            error = ENOMEM;
        }

        efs_close( fd );
    }
    else
    {
        error = ENOENT;
    }

    return error;
}

void q6zip_test_unzip_ro (
    void *           dst,
    unsigned int     dst_len,
    void *           src,
    unsigned int     src_len,
    void *           context,
    q6zip_waiter_t * waiter
)
{
    q6zip_iface_t const * q6zip = q6zip_test_get_implementation();
    q6zip_request_t * request;
    q6zip_error_t error;

    request = q6zip->get_request();
    Q6ZIP_ASSERT( request );

    request->src.ptr  = (void *)src;
    request->src.len  = src_len;
    request->dst.ptr  = dst;
    request->dst.len  = dst_len;
    request->algo     = Q6ZIP_ALGO_UNZIP_RO;
    request->priority = Q6ZIP_FIXED_PRIORITY_HIGH;
    request->user.other = context;

    error = q6zip->submit( &request );
    Q6ZIP_ASSERT( !error );

    if ( waiter )
    {
        q6zip_waiter_wait( waiter );

    }
}

void q6zip_test_unzip_rw (
    void *           dst,
    unsigned int     dst_len,
    void *           src,
    unsigned int     src_len,
    void *           context,
    q6zip_waiter_t * waiter
)
{
    q6zip_iface_t const * q6zip = q6zip_test_get_implementation();
    q6zip_request_t * request;
    q6zip_error_t error;

    request = q6zip->get_request();
    Q6ZIP_ASSERT( request );

    request->src.ptr    = (void *)src;
    request->src.len    = src_len;
    request->dst.ptr    = dst;
    request->dst.len    = dst_len;
    request->algo       = Q6ZIP_ALGO_UNZIP_RW;
    request->priority   = Q6ZIP_FIXED_PRIORITY_HIGH;
    request->user.other = context;

    error = q6zip->submit( &request );
    Q6ZIP_ASSERT( !error );

    if ( waiter )
    {
        q6zip_waiter_wait( waiter );

    }
}

void q6zip_test_zip_rw (
    void *           dst,
    unsigned int     dst_len,
    void *           src,
    unsigned int     src_len,
    q6zip_priority_t priority,
    void *           context,
    q6zip_waiter_t * waiter
)
{
    q6zip_iface_t const * q6zip = q6zip_test_get_implementation();
    q6zip_request_t * request;
    q6zip_error_t error;

    request = q6zip->get_request();
    Q6ZIP_ASSERT( request );

    request->src.ptr    = (void *)src;
    request->src.len    = src_len;
    request->dst.ptr    = dst;
    request->dst.len    = dst_len;
    request->algo       = Q6ZIP_ALGO_ZIP_RW;
    request->priority   = priority;
    request->user.other = context;

    error = q6zip->submit( &request );
    Q6ZIP_ASSERT( !error );

    if ( waiter )
    {
        q6zip_waiter_wait( waiter );

    }
}

void q6zip_test_submit_dma (
    q6zip_iovec_t const * dst,
    q6zip_iovec_t const * src,
    q6zip_waiter_t *      waiter 
)
{
    q6zip_iface_t const * q6zip = q6zip_test_get_implementation();
    q6zip_request_t * request;
    q6zip_error_t error;

    request = q6zip->get_request();
    Q6ZIP_ASSERT( request );

    request->src.ptr  = src->ptr;
    request->src.len  = src->len;
    request->dst.ptr  = dst->ptr;
    request->dst.len  = dst->len;
    request->algo     = Q6ZIP_ALGO_UNZIP_RW;
    request->priority = Q6ZIP_ALGO_MEMCPY;

    error = q6zip->submit( &request );
    Q6ZIP_ASSERT( !error );

    if ( waiter )
    {
        q6zip_waiter_wait( waiter );

    }
}

boolean q6zip_test_dma_verify (
    q6zip_iovec_t const * dst,
    q6zip_iovec_t const * src,
    q6zip_iovec_t const * expected,
    q6zip_waiter_t *      waiter 
)
{
    /* @warning waiter *cannot* be NULL */
    Q6ZIP_ASSERT( waiter );

    q6zip_test_submit_dma( dst, src, waiter );

    return memcmp( expected->ptr, dst->ptr, expected->len ) == 0;
}

void q6zip_test_wait (qurt_timer_duration_t duration)
{
    unsigned int signals;

    qurt_timer_restart( global.timer, duration );
    signals = qurt_signal_wait_any( &global.signal, Q6ZIP_TEST_SIGNAL_TMO );
    qurt_signal_clear( &global.signal, signals );
}
