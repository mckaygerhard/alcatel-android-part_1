#include "err.h"
#include "msg_diag_service.h"
#if !defined( Q6ZIP_ENABLE_ASSERTS )
#define Q6ZIP_ENABLE_ASSERTS
#endif
#include "q6zip_assert.h"
#include "q6zip_test.h"
#include "q6zip_test_scenario_unzip_ro.h"
#include "q6zip_test_vectors_ro.h"
#include "q6zip_waiter.h"

/** @brief Destination/uncompressed page
    @warning *MUST* be aligned on cache line */
static uint8 unzipped_page[ 4096 ] __attribute__(( aligned( 64 ) ));

q6zip_waiter_t waiter;

static void initialize (q6zip_test_scenario_t * scenario)
{
    q6zip_waiter_init( &waiter );
    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "initialized", 0, 0, 0 );
}

static void configure (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs,
    uint8 const * msg,
    uint16 len
)
{
}

static void start (q6zip_test_scenario_t * scenario)
{
    unsigned int i;
    boolean bit_exact_match = FALSE;

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "ENTER: RO unzip tests. dst=0x%x", unzipped_page, 0, 0 );

    for ( i = 0; i < Q6ZIP_TEST_VECTORS_RO_MAX; i++ )
    {
        q6zip_test_unzip_ro(
            ( void * ) unzipped_page,
            sizeof( unzipped_page ),
            ( void * ) RO_VECTORS_INDEX[ i ].zipped,
            RO_VECTORS_INDEX[ i ].zipped_size,
            ( void * )&RO_VECTORS_INDEX[ i ],
            &waiter );

        if ( memcmp( RO_VECTORS_INDEX[ i ].unzipped, unzipped_page, RO_VECTORS_INDEX[ i ].unzipped_size ) == 0 )
        {
            /* @warning MSG_3() emissions here may get dropped */
            bit_exact_match = TRUE;
        }
        else
        {
            /* On IPAv2.6, comp/dcmp engine is stuck. Abort! */
            bit_exact_match = FALSE;
            break;
        }
    }

    if ( bit_exact_match )
    {
        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "PASS: %u RO unzip vectors", i, 0, 0 );
    }
    else
    {
        ERR_FATAL( "FAIL: RO unzip of vector %u dst_ptr=0x%x", i, unzipped_page, 0 );
    }

    MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "LEAVE: RO unzip tests", 0, 0, 0 );
}

static void response (
    q6zip_test_scenario_t * scenario,
    q6zip_request_t const * request,
    q6zip_response_t const * response
)
{
    q6zip_test_vectors_index_t * test_vector;

    Q6ZIP_ASSERT( !response->error );

    test_vector = (q6zip_test_vectors_index_t *) request->user.other;
    Q6ZIP_ASSERT( test_vector );

    Q6ZIP_ASSERT( response->len == test_vector->unzipped_size );
    q6zip_waiter_wake( &waiter );
}

static void stop (q6zip_test_scenario_t * scenario)
{
}

q6zip_test_scenario_t q6zip_test_scenario_unzip_ro =
{
    .ops = 
    {
        initialize,
        configure,
        start,
        response,
        stop
    }
};
