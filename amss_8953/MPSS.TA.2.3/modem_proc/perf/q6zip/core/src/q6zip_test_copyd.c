/**
   @file q6zip_test_copyd.c 
   @brief Q6ZIP copy daemon 
   @detail A worker thread that performs 4K copies from DDR to DDR using IPA 
 */
#if !defined( Q6ZIP_ENABLE_ASSERTS )
#define Q6ZIP_ENABLE_ASSERTS
#endif
#include "q6zip_assert.h"
#include "q6zip_iface.h"
#include "q6zip_ipa.h"
#include "q6zip_params.h"
#include "q6zip_request.h"
#include "q6zip_test_copyd.h"
#include "q6zip_waiter.h"
#include "q6zip_worker.h"
#include "qurt.h"

#define PAGE_SHIFT 12
#define PAGE_SIZE ( 1 << PAGE_SHIFT )
#define NUM_PAGES 16

#if defined( Q6ZIP_UNIT_TEST )
#define MAX_COPIES 32
#define MAX_REQUESTS_PER_BURST 4
#define INTER_BURST_INTERVAL 4000

#define LOW_FLOOD_MAX_HIGH_COPIES 16
#define LOW_FLOOD_INTERVAL 250000

#else
#define MAX_COPIES 8192
#define MAX_REQUESTS_PER_BURST 2
#define INTER_BURST_INTERVAL 50000

#define LOW_FLOOD_MAX_HIGH_COPIES 8192
#define LOW_FLOOD_INTERVAL 50000

#endif

#if MAX_REQUESTS_PER_BURST > NUM_PAGES
#error MAX_REQUEST_PER_BURST <= NUM_PAGES
#endif

typedef enum
{
    COPYD_WORKER_STATE_UNKNOWN,
    COPYD_WORKER_STATE_STARTED,
    COPYD_WORKER_STATE_WORKING,
    COPYD_WORKER_STATE_EXITED

} copyd_worker_state_t;

typedef enum
{
    Q6ZIP_TEST_SIGNAL_QUIT  = (1 << 0),
    Q6ZIP_TEST_SIGNAL_STOP  = (1 << 1),
    Q6ZIP_TEST_SIGNAL_SCENARIO_START = (1 << 2),
    Q6ZIP_TEST_SIGNAL_TMO   = (1 << 3),

} q6zip_test_signal_t;

#define Q6ZIP_TEST_SIGNALS \
    ( Q6ZIP_TEST_SIGNAL_QUIT | \
      Q6ZIP_TEST_SIGNAL_SCENARIO_START )

#define Q6ZIP_TEST_SIGNALS_SCENARIO_ONLY \
    ( Q6ZIP_TEST_SIGNAL_STOP | \
      Q6ZIP_TEST_SIGNAL_TMO )
/* Source pages for IPA DMA */
unsigned char src_pages[ NUM_PAGES * PAGE_SIZE ] __attribute__(( aligned( 4 ) ));

/* Destination pages for IPA DMA */
unsigned char dst_pages[ NUM_PAGES * PAGE_SIZE ] __attribute__(( aligned( 4 ) ));

typedef struct
{
    /** @brief Maximum number of copies at high priority */
    unsigned int max_high_copies;

    /** @brief Interval between two high priority copies */
    unsigned int interval;

} q6zip_test_low_flood_knobs_t;

typedef struct
{
    /** @brief Maximum (approximate) number of copies to perform. Actual would be
               a bit higher */
    unsigned int max_copies;

    /** @brief Maximum number of Q6ZIP_ALGO_MEMCPY requests in a burst */
    unsigned int max_requests_per_burst;

    /** @brief Time (microseconds) between bursts */
    unsigned int inter_burst_interval;
} q6zip_test_interleaved_knobs_t;

/** @brief Various knobs for this test */
typedef struct
{
    
    /** @brief Knobs for testing response on high priority requests while keeping
               low priority queue flooded */
    q6zip_test_low_flood_knobs_t low_flood;

    q6zip_test_interleaved_knobs_t interleaved;

} q6zip_test_copyd_knobs_t;

typedef struct
{
    qurt_timer_t timer;
    qurt_signal_t signal;

} q6zip_test_scenario_global_t;

typedef struct
{
    q_link_type   link;
    q6zip_iovec_t src;
    q6zip_iovec_t dst;
    boolean       compare;

} node_t;
typedef struct q6zip_test_scenario_s q6zip_test_scenario_t;

typedef struct
{
    q6zip_waiter_t        waiter;
    q6zip_worker_t        worker;
    q6zip_iface_t const * q6zip;
    q6zip_test_scenario_t * scenario;

    struct
    {
        q_type queue;
        node_t nodes[ NUM_PAGES ];
    } buffer;

    struct
    {
        unsigned int dropped;

    } stats;
    copyd_worker_state_t state;

} q6zip_test_copyd_t;

typedef PACK( struct )
{
    /** @brief Control byte. Here is its decomposition ...
        7 ... 7 : 1 = start, 0 = stop
        6 ... 0 : scenario */
    uint8 control;

    /** @brief Number of (interleaved) requests per burst */
    uint8 num_requests_per_burst;

    uint16 num_bursts;

} q6zip_test_scenario_interleaved_conf_msg_t;

typedef void (*q6zip_test_scenario_conf_fn_t)(q6zip_test_scenario_t * scenario, q6zip_test_copyd_knobs_t const * knobs);
typedef void (*q6zip_test_scenario_start_fn_t)(q6zip_test_scenario_t * scenario);
typedef void (*q6zip_test_scenario_stop_fn_t)(q6zip_test_scenario_t * scenario);

typedef struct
{
    q6zip_test_scenario_conf_fn_t conf;
    q6zip_test_scenario_start_fn_t start;
    q6zip_test_scenario_stop_fn_t  stop;

} q6zip_test_scenario_operations_t;

typedef struct
{
    /** @brief Configuration for this scenario */
    q6zip_test_copyd_knobs_t knobs;

    /** @brief Has this scenario been stopped? */
    volatile boolean stopped;

    /** @brief Global (ie across test scenarios) data */
    q6zip_test_scenario_global_t * global;

    qurt_signal_t signal;

} q6zip_test_scenario_data_t;

struct q6zip_test_scenario_s
{
    q6zip_test_scenario_operations_t ops;
    q6zip_test_scenario_data_t data;
};
static q6zip_test_scenario_global_t global;

static void q6zip_test_scenario_conf_one_low_one_high (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs
);

static void q6zip_test_scenario_start_one_low_one_high (q6zip_test_scenario_t * scenario);

static void q6zip_test_scenario_stop_one_low_one_high (q6zip_test_scenario_t * scenario);

static q6zip_test_scenario_t one_low_one_high = {
    .ops =
    {
        q6zip_test_scenario_conf_one_low_one_high,
        q6zip_test_scenario_start_one_low_one_high,
        q6zip_test_scenario_stop_one_low_one_high
    },
    .data.global = &global
};

static void q6zip_test_scenario_conf_low_flood_one_high (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs
);

static void q6zip_test_scenario_start_low_flood_one_high (q6zip_test_scenario_t * scenario);

static void q6zip_test_scenario_stop_low_flood_one_high (q6zip_test_scenario_t * scenario);

static q6zip_test_scenario_t low_flood_one_high = {
    .ops =
    {
        q6zip_test_scenario_conf_low_flood_one_high,
        q6zip_test_scenario_start_low_flood_one_high,
        q6zip_test_scenario_stop_low_flood_one_high
    },
    .data.global = &global
};
static q6zip_test_copyd_t q6zip_test_copyd;

/** @brief Knobs and its CRT-initialized defaults */
static q6zip_test_copyd_knobs_t q6zip_test_copyd_knobs = {
    .interleaved = {
        MAX_COPIES,            /* max_copies             */
        MAX_REQUESTS_PER_BURST,/* max_requests_per_burst */
        INTER_BURST_INTERVAL   /* inter_burst_interval   */
    },
    .low_flood = {
        LOW_FLOOD_MAX_HIGH_COPIES,
        LOW_FLOOD_INTERVAL
    }
};

static void q6zip_initialized (boolean initialized)
{
    if ( initialized )
        q6zip_waiter_wake( &q6zip_test_copyd.waiter );
}

static inline void q6zip_test_copyd_free (
   q6zip_request_t ** request,
   node_t *           node
)
{
    q_put( &q6zip_test_copyd.buffer.queue, &node->link );
    q6zip_request_free( request );
}    

static void q6zip_request_completed (
    q6zip_request_t * request,
    q6zip_response_t const * response
)
{
    node_t * node = request->user.other;

    Q6ZIP_ASSERT( !response->error );

    if ( node->compare )
    {
        Q6ZIP_ASSERT( !memcmp( request->dst.ptr, request->src.ptr, request->dst.len ) );
    }

    q6zip_test_copyd_free( &request, node );
}

/**
 * @brief Test to keep the low priority queue flooded and inject a high to 
 *        measure its processing time
 * @param [in] knobs 
 */
void q6zip_test_low_flood (
    q6zip_test_scenario_t *              scenario,
    q6zip_test_low_flood_knobs_t const * knobs
)
{
    unsigned int      copies = 0;
    unsigned int      signals;
    node_t *          node;
    q6zip_error_t     error = Q6ZIP_ERROR_NONE;
    unsigned int      i, j;
    q6zip_request_t * requests[ Q6ZIP_PARAM_IPA_MAX_DEF_PRIO_REQS + 1 ];

    while ( !scenario->data.stopped && ( copies < knobs->max_high_copies ) )
    {
        /* Restart timer */
        qurt_timer_stop( scenario->data.global->timer );
        qurt_timer_restart( scenario->data.global->timer, knobs->interval );

        /* Wait for something to happen */
        signals = qurt_signal_wait_any( &scenario->data.global->signal, Q6ZIP_TEST_SIGNALS_SCENARIO_ONLY );
        qurt_signal_clear( &scenario->data.global->signal, signals );

        if ( signals & Q6ZIP_TEST_SIGNAL_STOP )
        {
            qurt_timer_stop( scenario->data.global->timer );
            break;
        }

        /* Assuming Q6ZIP_TEST_SIGNAL_TMO. Prepare low priority requests */
        for ( i = 0; i < Q6ZIP_PARAM_IPA_MAX_DEF_PRIO_REQS; i++ )
        {
            requests[ i ] = q6zip_test_copyd.q6zip->get_request();
            Q6ZIP_ASSERT( requests[ i ] );

            node = (node_t *)q_get( &q6zip_test_copyd.buffer.queue );
            Q6ZIP_ASSERT( node );
            node->compare = FALSE;

            requests[ i ]->src.ptr    = node->src.ptr;
            requests[ i ]->src.len    = node->src.len;
            requests[ i ]->dst.ptr    = node->dst.ptr;
            requests[ i ]->dst.len    = node->dst.len;
            requests[ i ]->algo       = Q6ZIP_ALGO_MEMCPY;
            requests[ i ]->priority   = Q6ZIP_FIXED_PRIORITY_LOW;
            requests[ i ]->user.other = node;
        }

        /* Prepare a high priority request */
        requests[ i ] = q6zip_test_copyd.q6zip->get_request();
        Q6ZIP_ASSERT( requests[ i ] );

        node = (node_t *)q_get( &q6zip_test_copyd.buffer.queue );
        Q6ZIP_ASSERT( node );
        node->compare = FALSE;

        requests[ i ]->src.ptr    = node->src.ptr;
        requests[ i ]->src.len    = node->src.len;
        requests[ i ]->dst.ptr    = node->dst.ptr;
        requests[ i ]->dst.len    = node->dst.len;
        requests[ i ]->algo       = Q6ZIP_ALGO_MEMCPY;
        requests[ i ]->priority   = Q6ZIP_FIXED_PRIORITY_HIGH;
        requests[ i ]->user.other = node;

        for ( j = 0; j <= i; j++ )
        {
            error = q6zip_test_copyd.q6zip->submit( &requests[ j ] );

            if ( error )
            {
                /* If there was an error, ensure that associated request and node 
                   are cleaned up*/
                q6zip_test_copyd_free( &requests[ j ], node );
            }
            else
            {
                /* Last request is the high priority one. If that doesnt fail,
                   count it as a successfull copy attempt */
                if ( j == i )
                {
                    copies++;
                }
            }
        }
    }
}

static void q6zip_test_scenario_conf_low_flood_one_high (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs
)
{
    scenario->data.knobs.low_flood = knobs->low_flood;
}

static void q6zip_test_scenario_start_low_flood_one_high (q6zip_test_scenario_t * scenario)
{
    scenario->data.stopped = FALSE;
    q6zip_test_low_flood( scenario, &scenario->data.knobs.low_flood );
}

static void q6zip_test_scenario_stop_low_flood_one_high (q6zip_test_scenario_t * scenario)
{
    scenario->data.stopped = TRUE;
}
void q6zip_test_interleaved (
    q6zip_test_scenario_t  *               scenario,
    q6zip_test_interleaved_knobs_t const * knobs
)
{
    unsigned int copies = 0;
    unsigned int num_requests;
    unsigned int signals;
    node_t * node;
    q6zip_error_t error;
    q6zip_request_t * request;
    q6zip_priority_t priority;

    priority = Q6ZIP_FIXED_PRIORITY_LOW;

    while ( !scenario->data.stopped  && ( copies < knobs->max_copies ) )
    {
        /* Restart timer */
        qurt_timer_stop( scenario->data.global->timer );
        qurt_timer_restart( scenario->data.global->timer, knobs->inter_burst_interval );

        /* Wait for something to happen */
        signals = qurt_signal_wait_any( &scenario->data.global->signal, Q6ZIP_TEST_SIGNALS_SCENARIO_ONLY );
        qurt_signal_clear( &scenario->data.global->signal, signals );

        if ( signals & Q6ZIP_TEST_SIGNAL_STOP )
        {
            qurt_timer_stop( scenario->data.global->timer );
            break;
        }

        /* Assuming Q6ZIP_TEST_SIGNAL_TMO */
        num_requests = 0;
        while ( num_requests < knobs->max_requests_per_burst )
        {
            request = q6zip_test_copyd.q6zip->get_request();
            Q6ZIP_ASSERT( request );

            node = (node_t *)q_get( &q6zip_test_copyd.buffer.queue );
            Q6ZIP_ASSERT( node );

            node->compare = FALSE;

            request->src.ptr  = node->src.ptr;
            request->src.len  = node->src.len;
            request->dst.ptr  = node->dst.ptr;
            request->dst.len  = node->dst.len;
            request->algo     = Q6ZIP_ALGO_MEMCPY;
            request->priority = priority;
            request->user.other = node;

            error = q6zip_test_copyd.q6zip->submit( &request );
            /* @warning It is possible for .submit() to return an error */

            num_requests++;
            copies++;
            if ( error )
            {
                q6zip_test_copyd_free( &request, node );
                q6zip_test_copyd.stats.dropped++;
                break;
            }

            priority = ( priority == Q6ZIP_FIXED_PRIORITY_LOW ) ? 
                Q6ZIP_FIXED_PRIORITY_HIGH : Q6ZIP_FIXED_PRIORITY_LOW;
        }
    }
}

static void q6zip_test_scenario_conf_one_low_one_high (
    q6zip_test_scenario_t * scenario,
    q6zip_test_copyd_knobs_t const * knobs
)
{
    scenario->data.knobs.interleaved = knobs->interleaved;
}

static void q6zip_test_scenario_start_one_low_one_high (q6zip_test_scenario_t * scenario)
{
    scenario->data.stopped = FALSE;
    q6zip_test_interleaved( scenario, &scenario->data.knobs.interleaved );
}

static void q6zip_test_scenario_stop_one_low_one_high (q6zip_test_scenario_t * scenario)
{
    scenario->data.stopped = TRUE;
}

void * work (void * work)
{
    unsigned int signals;

    (void) one_low_one_high;
    (void) low_flood_one_high;

    q6zip_test_copyd.state = COPYD_WORKER_STATE_STARTED;

    #if 0
    /* Initialize Q6ZIP */
    q6zip_test_copyd.q6zip->init( NULL, 0, q6zip_initialized, q6zip_request_completed );

    /* Wait for Q6ZIP initialization to complete */
    q6zip_waiter_wait( &q6zip_test_copyd.waiter );

    #endif

    while ( TRUE )
    {
        signals = qurt_signal_wait_any( &global.signal, Q6ZIP_TEST_SIGNALS );
        qurt_signal_clear( &global.signal, signals );

        if ( signals & Q6ZIP_TEST_SIGNAL_QUIT )
        {
            break;
        }

        if ( signals & Q6ZIP_TEST_SIGNAL_SCENARIO_START )
        {
            if ( q6zip_test_copyd.scenario )
            {
                q6zip_test_copyd.state = COPYD_WORKER_STATE_WORKING;
                q6zip_test_copyd.scenario->ops.start( q6zip_test_copyd.scenario );
            }
        }
    }

    #if defined( Q6ZIP_UNIT_TEST )
    /* Wait enough time for last request to be processed
       @todo anandj This is hacky. Need a clear handshake here! */
    qurt_timer_sleep( 250000 );

    #endif

    q6zip_test_copyd.state = COPYD_WORKER_STATE_EXITED;

    return NULL;
}

boolean q6zip_test_copyd_handle_diag_msg (
    uint8 const * msg,
    uint16        len
)
{
    boolean processed = FALSE;

    /* First stop whatever is going on */
    if ( q6zip_test_copyd.scenario )
    {
        qurt_signal_set( &global.signal, Q6ZIP_TEST_SIGNAL_STOP );
        q6zip_test_copyd.scenario->ops.stop( q6zip_test_copyd.scenario );
    }

    if ( len == 0 )
    {
        return FALSE;
    }
    /* @todo anandj Ugly hack to ensure that STOP gets processed */
    qurt_timer_sleep( 500000 );

    /* Start? */
    if ( msg [ 0 ] & 0x80 )
    {
        q6zip_test_copyd_knobs_t knobs;

        /* Local copy of knobs */
        knobs = q6zip_test_copyd_knobs;
        /* Which scenario? */
        switch ( msg[ 0 ] & 0x7F )
        {
            case 0:
                {
                    q6zip_test_scenario_interleaved_conf_msg_t const * conf;
                    conf = (q6zip_test_scenario_interleaved_conf_msg_t const *)msg;

                    knobs.interleaved.max_requests_per_burst = 
                        ( ( conf->num_requests_per_burst > 0 ) && ( conf->num_requests_per_burst <= ( Q6ZIP_PARAM_IPA_MAX_DEF_PRIO_REQS + Q6ZIP_PARAM_IPA_MAX_HI_PRIO_REQS ) ) ) ?
                         conf->num_requests_per_burst: MAX_REQUESTS_PER_BURST;

                    knobs.interleaved.max_copies = conf->num_bursts + 1;

                    one_low_one_high.ops.conf( &one_low_one_high, &knobs );

                    q6zip_test_copyd.scenario = &one_low_one_high;
                    processed = TRUE;
                }
                break;

            case 1:
                low_flood_one_high.ops.conf( &low_flood_one_high, &q6zip_test_copyd_knobs );
                q6zip_test_copyd.scenario = &low_flood_one_high;
                processed = TRUE;
                break;

            case 126:
                /* Initialize Q6ZIP */
                q6zip_test_copyd.q6zip->init( NULL, 0, q6zip_initialized, q6zip_request_completed );

                /* Wait for Q6ZIP initialization to complete */
                q6zip_waiter_wait( &q6zip_test_copyd.waiter );

                processed = TRUE;
                break;

            case 127:
                qurt_signal_set( &global.signal, Q6ZIP_TEST_SIGNAL_QUIT );

                processed = TRUE;
                /* Fall-through intended */

            default:
                q6zip_test_copyd.scenario = NULL;
                break;
        }

        if ( q6zip_test_copyd.scenario )
        {
            /* Notify worker thread to run the selected scenario */
            qurt_signal_set( &global.signal, Q6ZIP_TEST_SIGNAL_SCENARIO_START );
        }
    }
    else
    {
        /* Running test/scenario was stopped */
        processed = TRUE;
    }

    return processed;
}

void q6zip_test_copyd_init (void)
{
    unsigned int i;
    qurt_timer_attr_t timer_attr;

    q6zip_test_copyd.q6zip = &Q6ZIP_IPA;

    q6zip_waiter_init( &q6zip_test_copyd.waiter );

    qurt_signal_init( &global.signal );

    qurt_timer_attr_init( &timer_attr);
    qurt_timer_attr_set_type( &timer_attr, QURT_TIMER_ONESHOT );
    qurt_timer_attr_set_duration( &timer_attr, 50000 );

    qurt_timer_create( &global.timer, &timer_attr, &global.signal, Q6ZIP_TEST_SIGNAL_TMO );
    qurt_timer_stop( global.timer );
    q_init( &q6zip_test_copyd.buffer.queue );
    for ( i = 0; i < Q6ZIP_ARRAY_SIZE( q6zip_test_copyd.buffer.nodes ); i++ )
    {
        /* Associate source and destination pages */
        q6zip_test_copyd.buffer.nodes[ i ].src.ptr = &src_pages[ i * PAGE_SIZE ];
        q6zip_test_copyd.buffer.nodes[ i ].src.len = PAGE_SIZE;

        memset( q6zip_test_copyd.buffer.nodes[ i ].src.ptr, i, PAGE_SIZE );

        q6zip_test_copyd.buffer.nodes[ i ].dst.ptr = &dst_pages[ i * PAGE_SIZE ];
        q6zip_test_copyd.buffer.nodes[ i ].dst.len = PAGE_SIZE;

        memset( q6zip_test_copyd.buffer.nodes[ i ].dst.ptr, 0, PAGE_SIZE );

        q_put( &q6zip_test_copyd.buffer.queue, &q6zip_test_copyd.buffer.nodes[ i ].link );
    }

    q6zip_worker_fork( &q6zip_test_copyd.worker, "q6zip_test_copyd_worker", 4096, work, 100, NULL );
}

void q6zip_test_copyd_fini (void)
{
    q6zip_worker_join( &q6zip_test_copyd.worker );
    Q6ZIP_ASSERT( q_cnt( &q6zip_test_copyd.buffer.queue ) == NUM_PAGES );

    q6zip_test_copyd.q6zip->fini();
}
