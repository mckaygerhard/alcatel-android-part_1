#include "fs_lib.h"
#include "q6zip_ipa_conf.h"
#include "q6zip_latency_meas.h"
#include "q6zip_types.h"
#include <stdio.h>

typedef struct
{
    char const * path;
    unsigned int offset;
    boolean      default_value;

} q6zip_ipa_conf_flag_load_info_t;

q6zip_ipa_conf_t q6zip_ipa_conf_singleton;

/** @brief Table to load the various flag type configuration */
q6zip_ipa_conf_flag_load_info_t const Q6ZIP_IPA_CONF_FLAG_LOAD_INFO[] = {
    {
        "/nv/item_files/modem/q6zip/q6zip_sw_during_clock_boost",
        Q6ZIP_OFFSETOF( q6zip_ipa_conf_flags_t, q6zip_sw_during_clock_boost ),
        FALSE,
    },
    {
        "/nv/item_files/modem/q6zip/q6zip_sw_during_firmware_danger",
        Q6ZIP_OFFSETOF( q6zip_ipa_conf_flags_t, q6zip_sw_during_firmware_danger ),
        TRUE,
    }
};

static void q6zip_ipa_conf_load_flags (q6zip_ipa_conf_flags_t * flags)
{
    int i;
    int bytes_read;
    uint8 * dst;

    for ( i = 0; i < Q6ZIP_ARRAY_SIZE( Q6ZIP_IPA_CONF_FLAG_LOAD_INFO ); i++ )
    {
        dst = ( (uint8 *)flags ) + Q6ZIP_IPA_CONF_FLAG_LOAD_INFO[ i ].offset;

        bytes_read = efs_get( 
            Q6ZIP_IPA_CONF_FLAG_LOAD_INFO[ i ].path,
            (void *)dst,
            1U );

        if ( bytes_read != 1U )
        {
            /* Apply default */
            *dst = Q6ZIP_IPA_CONF_FLAG_LOAD_INFO[ i ].default_value;
        }
    }
}

void q6zip_ipa_conf_init (q6zip_ipa_conf_t * conf)
{
    q6zip_ipa_conf_load_flags( &conf->flags );
    q6zip_latency_meas_rcinit();
}

void q6zip_ipa_conf_rcinit (void)
{
    q6zip_ipa_conf_init( &q6zip_ipa_conf_singleton );
}

#if defined( Q6ZIP_UNIT_TEST )
void q6zip_ipa_conf_print (q6zip_ipa_conf_t const * conf)
{
    int i;

    fprintf( stdout, "flags:\n" );
    for ( i = 0; i < Q6ZIP_ARRAY_SIZE( Q6ZIP_IPA_CONF_FLAG_LOAD_INFO ); i++ )
    {
        fprintf( 
            stdout, 
            "\t%s, %u\n", Q6ZIP_IPA_CONF_FLAG_LOAD_INFO[ i ].path, *( (uint8 *)&conf->flags + Q6ZIP_IPA_CONF_FLAG_LOAD_INFO[ i ].offset ) ); 

    }
}
#endif
