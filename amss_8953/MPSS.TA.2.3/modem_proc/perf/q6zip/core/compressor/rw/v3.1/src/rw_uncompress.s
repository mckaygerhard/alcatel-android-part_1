	.file	"rw_uncompress.c"
	.text
.p2align 2
.p2align 4,,15
.globl deltaUncompressTest0
.type	deltaUncompressTest0, @function
deltaUncompressTest0:
	{ loop0(.L21,#1023) } 
	.falign
	.L21:
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
	{ jumpr r31 }

.globl deltaUncompressTest1
.type	deltaUncompressTest1, @function
deltaUncompressTest1:
	{ loop0(.L210,#1023) } 
    { R2=##.L210}
	{ SA0 = R2 }
	.falign
	.L210:
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
	{ jumpr r31 }

.globl deltaUncompressTest2
.type	deltaUncompressTest2, @function
deltaUncompressTest2:
	{ loop0(.L22,#1023) }
    { R2=##.L22}
	.falign
	.L22:
	{ SA0 = R2 ; r1 = add(r1,#32)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
	{ jumpr r31 }

.globl deltaUncompressTest3
.type	deltaUncompressTest3, @function
deltaUncompressTest3:
	{ loop0(.L23,#1023) }
    { R2=##.L24}
	{ R3=##.L23 }
	.falign
	.L23:
	{ SA0 = R2 ; r1 = add(r1,#32)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
    { jumpr r31 }
	.falign
	.L24:
	{ SA0 = R3 ; r1 = add(r1,#32)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }:endloop0 
	{ jumpr r31 }


.globl deltaUncompressTest4
.type	deltaUncompressTest4, @function
deltaUncompressTest4:
	{ r0=#1023; R2=##.L25}
	.falign
	.L25:
	{ r1 = add(r1,#32);r0 = add(r0,#-1)}
	{ r1 = add(r1,#32);p0=cmp.gt(r0,#0)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32); if (p0) jumpr r2} 
	{ jumpr r31 }


.globl deltaUncompressTest5
.type	deltaUncompressTest5, @function
deltaUncompressTest5:
	{ r0=#1023; R2=##.L26}
	{ R3=##.L27 }
	.falign
	.L26:
	{ r1 = add(r1,#32);r0 = add(r0,#-1)}
	{ r1 = add(r1,#32);p0=cmp.gt(r0,#0)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32); if (p0) jumpr r3} 
	{ jumpr r31 }
	.falign
	.L27:
	{ r1 = add(r1,#32);r0 = add(r0,#-1)}
	{ r1 = add(r1,#32);p0=cmp.gt(r0,#0)}
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32) }
	{ r1 = add(r1,#32); if (p0) jumpr r2} 
	{ jumpr r31 }



	.p2align 2
	.p2align 4,,15
	.globl deltaUncompress
	.type	deltaUncompress, @function
deltaUncompress:
	// saved LR + FP regs size (bytes) = 0
	// callee saved regs size (bytes) = 48
	// local vars size (bytes) = 32
	// fixed args size (bytes) = 0

        // R16 to R27 are saved on the stack (12 regs * 4 bytes each  = 48 total bytes)
        // Function is called with (*compressed, in_len, *uncompressed, out_len)
        // must change to          (*compressed, *uncompressed, out_len)
	{
		r29 = add(r29,#-80)             // update SP 
		r9:8 = memd(r6=##C.6.3689)      // r6 = jump table add; r9 = CASE01, r8 = CASE00
                r28 = r0                        // r28 = compressed
        }
        {
		r24=#1024                       // r24 = 1024
		memd(r29+#0) = r9:8             // stack: save CASE00 and CASE01 addresses
                r0  = memw(r28)                   // r0 = size-of-encoded-stream, size-of-raw-stream
                r2 = #0
	}
	{
		r9:8 = memd(r6+#8)              // r9 = CASE11, r8 = CASE10
                r0 = combine(r2.h, r0.l)         // r0 = size-of-raw-stream
		r28 = add(r28,#4)                // r28 = compressed + 4
                r2 = r1                         // r2 = uncompressed
	}
	{
		memd(r29+#72) = r17:16          // save stack
		memd(r29+#64) = r19:18         // save stack
                r0  = asl(r0,#2)               //  r28 = size-of-raw-stream * 4
	}
	{
		memd(r29+#56) = r21:20          // save stack
		memd(r29+#48) = r23:22          // save stack
                r0 = add(r28, r0)              // r0 = compressed_encoded
		r25 = #0                        // r25 = 0
	}
	{
		memd(r29+#40) = r25:24          // save stack
		memd(r29+#32) = r27:26          // save stack
		r10=##.CASE11                   // r24 = CASE11
	}
	{
		memd(r29+#8) = r9:8             // stack: save CASE11, CASE10 addresses
		loop0(.DCZERO,#128)
		r19 = #0                        // r19 = 0
		r5 = #0                         // r5  = 0
	}
.falign
.DCZERO:
        {
		dczeroa(r1)
		r1  = add(r1,#32)
		r21 = add(r0,#4)                // sneak this in, r21 = compressed_encoded + 4
		r16 = r2                         // sneak this in, r16 = uncompressed
	}:endloop0 // start=.DCZERO

        // by this point,
        //  r0 is pointing to compressed_encded
        //  r28 is pointing to compressed_raw
        //  r2 is point to uncompressed
	{
		memw(r29+#16) = #0              // stack: save 0
		r1 = #0                         // r1 = 0
		r22 = add(r29,#16)              // r22 = ptr into stack
		r23 = r29                       // r23 = SP
	}
	{
		r18 = memw(r21+#4)              // r18 = start of holding register (i.e. beyond first two jump codes)
		r4 = memw(r0+#0)                // r4  = code of first jump-target
		r21 = add(r21,#4)               // r21 = compressed_encoded + 8 (i.e. beyond first two jump codes)
		r17 = #60                       // r17 = 60
	}
        // by this point,
        //  r21 is pointing into compressed_encoded
        //   (technically it's 2 words up from the start, since first two jump targets have been grabbed)
	{
		r0 = memw(r0+#4)                // r0 = code of next-jump-target
		memw(r22+#12) = #0              // stack: save 0
		r8=add(r16,#4096)               // r8 = end of uncompressed buffer
		r26 = #-1024                    // r26 = -1024
	}
	{
		r5:4 |= asl(r1:0,#32)           // before:
                                                //   r5 is 0; r4 is code of jump-target.
                                                //   r1 is 0; r0 is code of next-jump-target.
                                                // after:
                                                //   r5 = code of next-jump-target. r4 = code of jump-target
		r1:0 = asl(r19:18,#62)          // before:
                                                //   r1 is 0; r0 is code of next-jump-target.
                                                //   r19 is 0; r18 is compressed_encoded data beyond first 2 jump codes
                                                // after:
                                                //   r1 = all 0s except MSBs are first 2 bits of compressed_encoded data beyond the first 2 jump codes; r0 = #0
		memw(r22+#8) = #0               // stack: save 0
		memw(r22+#4) = #0               // stack: save 0
	}
	{
		r1:0 |= lsr(r5:4,#2)            // r1 maintains all 0s except MSBs, which are first 2 bits of compressed_encoded beyond the first 2 jump codes; r0 is similar. but it's all 0s except MSBs, which are codes for next-jump-target
		r19:18 = asl(r19:18,#60)        // r19 = all 0s except MSBs are first 4 bits of compressed_encoded; r18 = 0
		r4 = and(r4,#3)                 // r4  = code of jump-target
		r27 = #1023                     // r27  = 1023
	}
	{
		r3 = and(r0,#3)                 // r3 = code of next-jump-target
		r19:18 |= lsr(r1:0,#2)          // r19:18 |= r1:0 >> 2
		r0 = memw(r29+r4<<#2)           // r0 = jump target
		lc0=r24                         // loop count = 1024
	}
	{
		r20 = memw(r29+r3<<#2)          // r20 = next jump target
		r24=##.CASE00                   // r24 = CASE00
		jumpr r0                        // jump to target
	}

.p2align 5
.SUPER_ZERO:
	{
                r0 = add(r17,#-32)      ;       // bitcount -=32
                r3 = #0             ;
                r16 = add(r16,#64) ;            // uncompressed += 16 words
                r2 = memw(r21++#4)              // r2 = next word of compressed-encoded data, compressed++
        }
	{
                r5:4 = asl(r3:2,r0)     ;       // shift out compressed data and store in r5 and r4
                r0=sub(r8,r16)      ;           // r0 = uncompressed_end - uncompressed
                r2=add(r8,#-4)                  // r2 = uncompressed_end - 4
        }
	{
                r5:4 |= lsr(r19:18,#32) ;       // hold |= *compresedInPtr
                r0=asr(r0,#2)       ;           // set r0 to number of words left til end
                p0=cmp.gt(r16,r2)  ;            // check if at the end
                dcfetch(r21+#128)               // prefetch compressed_encoded data
        }
	{
                r19:18 = r5:4           ;       // set holding register
                LC0=r0 ;                        // update loop count reg
                if (!p0) jump .CASE00           // if not at end of buffer, go to case00
        }
	{
                jump .EXIT_LBL                 // else exit
        }

.p2align 5
.SUPER_RAW:
	{
                r0 = add(r17,#-32)      ;       // bitcount -=32
                r3 = #0             ;
                r2 = memw(r21++#4)              // r2 = next word of compressed-encoded data, compressed-encoded++
                r11 = add(r16, #64)             // r11 = uncompressed_soon_to_be ptr
        }
	{
                r5:4 = asl(r3:2,r0)     ;       // shift out compressed-encoded data and store in r5 and r4
                r12=sub(r8,r11)      ;           // r12 = uncompressed_end - uncompressed_soon_to_be
                r2=add(r8,#-4)                  // r2 = uncompressed_end - 4
        }
	{
                r5:4 |= lsr(r19:18,#32) ;       // hold |= *compresedInPtr
                r12=asr(r12,#2)       ;           // set r12 to number of words left til end
                p0=cmp.gt(r11,r2)  ;            // check if (soon_to_be) at end
        }
        {
                r19:18 = r5:4           ;       // set holding register
                loop0(.SUPER_RAW_LOOP,#16)
        }
        .falign
.SUPER_RAW_LOOP:
        {
                r0 = memw(r28++#4)              // r0 = compressed-raw data. compressed-raw++
                memw(r16++#4) = r0.new              // store raw data into uncompressed buffer; uncompressed++
        }:endloop0 // start=.SUPER_RAW_LOOP
        {
                loop0(.ANCHOR_LOOP,#4)
                r28 = add(r28, #-16)
                r3  = add(r25, #1)
        }
        .falign
.ANCHOR_LOOP:
        {
                r0 = memw(r28++#4)             // r0 = compressed-raw data. compressed-raw++
                memw(r22+r25<<#2) = r0.new      // anchors[anchor_index] = compressed-raw data
                r3   = add(r3, #1)
                r25  = and(r3, #3)
        }:endloop0
	{
                dcfetch(r28+#128)               // prefetch compressed_raw data
                LC0=r12 ;                        // update loop count reg
                if (!p0) jump .CASE11           // if not at end of buffer, go to case11
        }
	{
                jump .EXIT_LBL                 // else exit
        }

.p2align 5
.CASE00:
	{
                SA0=r20 ;                        // set the jump-target
                r0 = add(r17,#-2) ;              // mark that we've taken off 2 bits from compressed-encoded
                p0=cmp.eq(R20,r24)          ;    //  r24 is set to CASE00.  so if r20 (jump target) is to case00
                p0=cmp.eq(r18,#0) ;              //  ... and if r18 (holding reg) is 0
                if (p0.new) jump:nt .SUPER_ZERO  // then go go gadget super zero
        }
	{
                p0 = cmp.gtu(r0,#32)        ;   // check if num-bits <= 32
                r3 = #0                     ;   //
                r2 = memw(r21+#0)               // r2 = compressed[i]
        }
	{
                if (p0) r17 = r0            ;    // bitcount -= 2
                r5:4 = asl(r3:2,r0)         ;    // tmp = compressedInPtr << bitCount
                r1 = and(r18,#3)                 // r1 = anchor code
        }
	{
                if (!p0) r21 = add(r21,#4)  ;    // compressed++
                if (!p0) r17 = add(r17,#30) ;   //  bitCount += word size in bits
                r5:4 |= lsr(r19:18,#2)          //  hold >>=  bitCount; hold |= tmp
        }
	{
                r20 = memw(r23+r1<<#2)      ;   //  r20 <- next jump target
                r19:18 = r5:4               ;   // r19:18 = hold
                r16 = add(r16,#4)      ;        // uncompressed++
                dcfetch(r21+#64)                // prefetch compressed data
        }:endloop0
	{
                jump .EXIT_LBL
        }
.p2align 5
.CASE01:
        // The if-check tells us what's happening inside SKIP BITS W CHECK.
        //   r0 holds the bitcount.
        //   if the bit count <= 32, then r21 (compressed ptr) increases by 4 ***** r21 is the compressed_encoded
        //                            and r17 +=  28 ??? 

	{
                r3:2 = lsr(r19:18,#2)  ;        // shift off 2 bits of holding reg, store in r3 and r2
                r0 = add(r17,#-4)     ;         // bitCount -= 4  , (num anchor bits + num code bits = 4)
                r4 = memw(r21+#0)           ;   // r4 = compressed data
                SA0=r20                         // set jump-target
        }
	{
                r1 = and(r2,#3)   	 ;      // r1 holds some next anchor code
                r2 = and(r18,#3)      ;         // r2 holds current anchor code
                p0 = cmp.gtu(r0,#32)        ;   // check if bitCount <= 32
                r5:4 = combine(#0,r4)           // r5:4 = compressed data
        }
	{
                r2 = memw(r22+r2<<#2)  ;
                if (p0) r17 = r0      ;         // set bitCount (was >32)
                if (!p0) r17 = add(r17,#28) ;   // set bitCount (was <=32)
                r5:4 = asl(r5:4,r0 )            // shift bits off holding reg
        }
	{
                memw(r16++#4) = r2     ;        // uncompressed++
                r20 = memw(r23+r1<<#2);         // update next-jump-target
                if (!p0) r21 = add(r21,#4)  ;   // compressed++ (bitcount was <=32)
                r5:4 |= lsr(r19:18,#4)          // shift off the the code (case01) and anchor code
        }
	{
                r19:18 = r5:4          ;        // set holding reg
                dcfetch(r21+#64)                // prefetch compressed data
        }:endloop0
	{
                jump .EXIT_LBL
        }
.p2align 5
.CASE10:
	{
                r1:0 = lsr(r19:18,#12) ;        // partial match, so shift holding register by 12
                r3 = and(r18,#3)      ;         // get the next anchor code
                r2 = add(r17,#-14)      ;       // bitCount -= 14
                SA0=r20
        }
	{
                r0 = and(r0,#3)        ;        // bitmask out the anchor code
                r4 = memw(r22+r3<<#2) ;         // grab the next target address
                p0 = cmp.gtu(r2,#32)    ;       // check if bitcount <= 32
                r7:6 = lsr(r19:18,#2)           // chop off 2 bits from hold. store in holding reg in r7 and r6
        }
	{
                r20 = memw(r23+r0<<#2) ;        // grab the next-jump-target, store in r20
                r0 = and(r6,r27)      ;         // mask part of the holding reg. (trying to get the delta here)
                r5 = memw(r21+#0)               // load compressed data into r5
        }
	{
                r5:4 = combine(#0,r5)  ;        // load compressed data into r5 and r4
                r0 |= and(r4,r26)               // next-jump-target & r26
        }
	{
                memw(r22+r3<<#2) = r0  ; 
                r3:2 = asl(r5:4,r2)   ;         // shift compressed data left, store in r3, r2
                if (p0) r17 = r2                // set bitCount
        }
	{
                r3:2 |= lsr(r19:18,#14);        // finish touching up ohlding reg
                if (!p0) r17 = add(r17,#18)   ; // update bitcount if needed
                dcfetch(r21+#64)                // start prefetching compressed data
        }
	{
                r19:18 = r3:2          ;        // store holding reg back into r19:18
                if (!p0) r21 = add(r21,#4);     // if needed, compressed++
                memw(r16++#4) = r0              // store into uncompressed; uncompressed++
        }:endloop0
	{
                jump .EXIT_LBL
        }
.p2align 5
.CASE11:
        // for this case, we want to pull from the compressed_raw buffer
        // and we want to update that pointer. (For us, that's going to be r28)
        // We have to touch r21 just for SKIP BITS W CHECK
        // r16 is pointing to uncompressed buffer. want to grab from compressed_raw
        // r19:18 is the holding register
        {
                SA0=r20                    ;    // set the jump target
                p0=cmp.eq(r20,r10)          ;    //  r10 is set to CASE11.  so if r20 (jump target) is to case11
                p0=cmp.eq(r18,#-1) ;              //  ... and if r18 (holding reg) is 0xFFFFFFFF
                if (p0.new) jump:nt .SUPER_RAW  // then go go gadget super raw
                if (!p0.new) r2 = memw(r28++#4)      ;       // load compressed-raw data into r2; compressed-raw++
        }
        {
                r0 = add(r17,#-2)          ;    // subtract 2 from bitcount
                r3 = #0                     ;   //
                memw(r16++#4) = r2     ;       // store compressed-raw data into uncompressed; uncompressed++
                memw(r22+r25<<#2) = r2 ;        // store new anchor
        }
        {
                r2 = memw(r21+#0)               // r2 = compressed_encoded[i]
                r1 = and(r18,#3)                // r1 = code of next-jump-target
                dcfetch(r28+#64)                // prefetch compressed_raw
        }
        {
                r5:4 = asl(r3:2,r0)            // tmp = compressedInPtr << bitCount
                r20 = memw(r23+r1<<#2)  ;       // store next-jump-target into r20
        }
	{
                r5:4 |= lsr(r19:18,#2)          // hold >>= 2; hold |= tmp;
                p0 = cmp.gtu(r0,#32)    ;       // check bitcount <= 32
                if (p0.new) r17 = r0                // update bitcount
                r0 = add(r25,#1)        ;       // update anchor index
        }
	{
                if (!p0) r17 = add(r17,#30)     // if it is, update it
                if (!p0) r21 = add(r21,#4) ;    // if needed, compressed_encoded++
                r19:18 = r5:4
                r25 = and(r0,#3)                // update anchor index. store into r25
        }:endloop0
.EXIT_LBL:
	{
		r0 = #4096                      // uncompressed 4096 bytes
		r27:26 = memd(r29+#32)          // restore stack
		r25:24 = memd(r29+#40)
	}
	{
		r23:22 = memd(r29+#48)
		r21:20 = memd(r29+#56)
	}
	{
		r19:18 = memd(r29+#64)
		r17:16 = memd(r29+#72)
		r29 = add(r29,#80)
		jumpr r31                       // return
	}
	.size	deltaUncompress, .-deltaUncompress
	.section	.rodata
	.p2align 3
	.type	C.6.3689, @object
	.size	C.6.3689, 16
C.6.3689:
	.word	.CASE00
	.word	.CASE01
	.word	.CASE10
	.word	.CASE11
	.ident	"GCC: (Sourcery QuIC Lite 5.0-413) 4.4.0"
