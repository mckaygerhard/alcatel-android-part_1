#include "rw_compress.h"
#include <string.h>
#include <stdint.h>
#include <hexagon_protos.h>

#define NUM_ANCHORS 4 // must be a power of 2
#if (NUM_ANCHORS != 4)
#error("NUM_ANCHORS not 4")
#endif
#define NUM_ANCHOR_BITS 2
#define NUM_CODE_BITS 2
#define WORD_SIZE_IN_BITS (sizeof(unsigned int) << 3) // 32

#define GET_BITS(hold,n) (unsigned int)(hold & ((1UL << n) - 1))
 
#ifdef __hexagon__
#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)
#else
#define LIKELY(x) (x)
#define UNLIKELY(x) (x)
#endif


/*********************************************************************************************************/        



#define SKIP_BITS_W_CHECK(compressedInPtr,bitCount,hold,n)      \
  bitCount -= n;                                    \
  hold >>= n;                                                   \
  hold |= (uint64_t)(*(compressedInPtr)) << bitCount;     \
  if (UNLIKELY(bitCount <= WORD_SIZE_IN_BITS)) compressedInPtr++;       \
  if (UNLIKELY(bitCount <= WORD_SIZE_IN_BITS)) bitCount += WORD_SIZE_IN_BITS; 



#define CACHE_LINE_SHIFT (5)
#define CACHE_LINE_SZ  (1 << CACHE_LINE_SHIFT)

static inline void l2fetch_buffer(void *addr,unsigned int len)
{
  /* Cache-align starting address and length. */
  unsigned int ofs = ((unsigned int) addr) & (CACHE_LINE_SZ-1);
  addr = (void *) ((unsigned int) addr - ofs);
  len  = (len+ofs+CACHE_LINE_SZ-1) / CACHE_LINE_SZ;

  /* Width=cache line, height=# cache lines, stride=cache line */
  asm volatile ("l2fetch(%[addr],%[dim])" : : 
     [addr] "r" (addr), 
     [dim] "r" ( Q6_P_combine_IR(CACHE_LINE_SZ, Q6_R_combine_RlRl(CACHE_LINE_SZ, len)) )
     : "memory");

}

/* DCZERO the output buffer, 32 bytes at a time.
   Assume page size is a multiple of 32
   Assume dest addr is multiple of 32
*/
/* linked in from dczero assembly version in dlpager/src */
void dczero(unsigned int addr, unsigned int size);
/*static inline void dczero(uint32_t addr,uint32_t size)
{
    uint32_t lines, i;
    lines = 4096 >> CACHE_LINE_SHIFT;
    for (i = 0; i < lines; i++)
    {
        asm("dczeroa(%0)" : : "r" (addr));
        addr += CACHE_LINE_SZ;
    }
}*/

//uncompress a word-aligned buffer compressed of length in_len words into a word-aligned buffer uncompressed of of lengh out_len words
//unsigned int deltaUncompress(unsigned int *compressed,unsigned int in_len,unsigned int *uncompressed,unsigned int out_len)
unsigned int deltaUncompress(unsigned int *compressed,unsigned int *uncompressed,unsigned int out_len)
{
  int i;
  unsigned int delta;
  unsigned int code,val,size,anchor,size_of_raw_stream;
  unsigned int anchors[NUM_ANCHORS];
  unsigned int anchorIndex = 0;
  unsigned int numAvailBits = 0;
  unsigned int bufIndex = 0;
  unsigned long long int hold = 0;
  void* jump_table[] = {
    &&CODE0, //0
    &&CODE1, //1
    &&CODE2, //2
    &&CODE3  //3
  };
  unsigned int *lastWord = &uncompressed[out_len-1];
  unsigned int *compressed_raw;
  unsigned int *compressed_encoded;

  void* jumpTarget,*nextJumpTarget;
  int debug_i = 0;
  int code3_i = 0;
  int code11_i = 0;


  anchors[0] = anchors[1] = anchors[2] = anchors[3] = 0;
  //l2fetch_buffer((void *)compressed, in_len);
  dczero((unsigned int)uncompressed, 4096);
  //dczero((unsigned int)uncompressed, 4096);

  size = 4096;

  size_of_raw_stream = compressed[0] & 0x0000FFFF; // mask out size-of-encoded-section, if there
  compressed_raw     = &(compressed[1]);
  compressed_encoded = &(compressed[size_of_raw_stream + 1]); // +1 to skip size/first entry

//#define DEBUG 1
  
#ifdef DEBUG
  printf("\nuncompressed: 0x%x\n", uncompressed);
  printf("compressed: 0x%x\n", compressed);
  printf("compressed_encoded: 0x%x\n", compressed_encoded);
  printf("compressed_raw: 0x%x\n", compressed_raw);
  //printf("length of compressed buffer: %d\n", in_len);
  printf("size_of_raw_stream: %d\n", size_of_raw_stream);
  printf("size_of_raw_stream in hex: 0x%x\n", size_of_raw_stream);
  printf("lastWord : 0x%x\n", lastWord);
#endif

  //load up 64bits into the holding register
  SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,0);

  //process the input
  jumpTarget=jump_table[GET_BITS(hold,NUM_CODE_BITS)];
  SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,NUM_CODE_BITS);
  nextJumpTarget=jump_table[GET_BITS(hold,NUM_CODE_BITS)];
  SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,NUM_CODE_BITS);
  goto *jumpTarget;

#define SUPER_ZERO_WORDS 16
#define SUPER_RAW_WORDS  16


CODE00:
    //value was 0
    //jumpTarget=nextJumpTarget;
    //nextJumpTarget=jump_table[GET_BITS(hold,NUM_CODE_BITS)];
    SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,NUM_CODE_BITS*SUPER_ZERO_WORDS);
    uncompressed+=SUPER_ZERO_WORDS;
#ifdef DEBUG
    if(debug_i > 1000){
            printf("CODE00\n");
            printf("\tdebug_i: %d\n", debug_i);
            printf("\tval: super 0\n");
            printf("\thold (32 bits): 0x%x\n", (unsigned int) hold);
    }
#endif
    debug_i+=16;
    if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
    //goto *jumpTarget;
    goto CODE0;
CODE11:
    // all raw words
    //jumpTarget=nextJumpTarget;
    //nextJumpTarget=jump_table[GET_BITS(hold,NUM_CODE_BITS)];
    SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,NUM_CODE_BITS*SUPER_RAW_WORDS);
    for(i = 0; i < SUPER_RAW_WORDS; ++i){
        val = *compressed_raw++;
        *uncompressed++ = val;
        //add new anchor
        anchors[anchorIndex++] = val;
        anchorIndex &= (NUM_ANCHORS - 1);
    }
#ifdef DEBUG
    if(debug_i > 1000){
              printf("CODE11\n");
              printf("\tdebug_i: %d\n", debug_i);
              printf("\tval: (raws)\n");
              printf("\thold (32 bits): 0x%x\n", (unsigned int) hold);
    }
    code11_i++;
#endif
    debug_i+=16;
    if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
    //goto *jumpTarget;
    goto CODE3;

CODE0:
      //value was 0
      if ((nextJumpTarget==&&CODE0)&&((unsigned int)hold==0)) goto CODE00;
      jumpTarget=nextJumpTarget;
      nextJumpTarget=jump_table[GET_BITS(hold,NUM_CODE_BITS)];
      SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,NUM_CODE_BITS);
      uncompressed++;
#ifdef DEBUG
      if(debug_i > 1000){
              printf("CODE0\n");
              printf("\tdebug_i: %d\n", debug_i);
              printf("\tval: 0\n");
              printf("\thold (32 bits): 0x%x\n", (unsigned int) hold);
      }
#endif
      debug_i++;
      if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
      goto *jumpTarget;
CODE1:
      //get anchor number which has exact match
      anchor = GET_BITS(hold,NUM_ANCHOR_BITS);
      jumpTarget=nextJumpTarget;
      nextJumpTarget=jump_table[GET_BITS(hold>>NUM_ANCHOR_BITS,NUM_CODE_BITS)];

      SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,NUM_CODE_BITS+NUM_ANCHOR_BITS);
      *uncompressed++ = anchors[anchor];
#ifdef DEBUG
      if(debug_i > 1000){
              printf("CODE1\n");
              printf("\tdebug_i: %d\n", debug_i);
              printf("\tval: 0x%x\n", anchors[anchor]);
              printf("\thold (32 bits): 0x%x\n", (unsigned int) hold);
      }
#endif
      debug_i++;
      if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
      goto *jumpTarget;

CODE2:
      //get anchor number which has partial match
      anchor = GET_BITS(hold,NUM_ANCHOR_BITS);
      //get the delta and construct output word
      delta = GET_BITS(hold>>NUM_ANCHOR_BITS,10);
      jumpTarget=nextJumpTarget;
      nextJumpTarget=jump_table[GET_BITS(hold>>(NUM_ANCHOR_BITS+10),NUM_CODE_BITS)];

      SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,NUM_CODE_BITS+NUM_ANCHOR_BITS+10);
      val = *uncompressed++ = (anchors[anchor] & 0xFFFFFC00) | delta;
#ifdef DEBUG
      if(debug_i > 1000){
              printf("CODE2\n");
              printf("\tdebug_i: %d\n", debug_i);
              printf("\tval: 0x%x\n", val);
              printf("\thold (32 bits): 0x%x\n", (unsigned int) hold);
      }
#endif
      debug_i++;
      if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;
      //update existing anchor
      anchors[anchor] = val;
      goto *jumpTarget;

CODE3:
      if ((nextJumpTarget==&&CODE3)&&((unsigned int)hold==0xFFFFFFFF)) goto CODE11;
      //no exact or partial match
      //val = (unsigned int)hold;
      val = *compressed_raw++;
      *uncompressed++ = val;
#ifdef DEBUG
      if(debug_i > 1000){
              printf("CODE3\n");
              printf("\tdebug_i: %d\n", debug_i);
              printf("\tval: 0x%x\n", val);
              printf("\thold (32 bits): 0x%x\n", (unsigned int) hold);
      }
      code3_i++;
#endif
      debug_i++;

      if (UNLIKELY(uncompressed>lastWord)) goto RETLAB;

      jumpTarget=nextJumpTarget;
      nextJumpTarget=jump_table[GET_BITS(hold,NUM_CODE_BITS)];
      SKIP_BITS_W_CHECK(compressed_encoded,numAvailBits,hold,NUM_CODE_BITS);
      //add new anchor
      anchors[anchorIndex++] = val;
      anchorIndex &= (NUM_ANCHORS - 1);
      goto *jumpTarget;

RETLAB:
#ifdef DEBUG
      printf("Exiting\n");
      printf("\tdebug_i: %d\n", debug_i);

      printf("\tcode3_i: %d\n", code3_i);
      printf("\tcode11_i: %d\n", code11_i);
#endif
      return (size);
}
