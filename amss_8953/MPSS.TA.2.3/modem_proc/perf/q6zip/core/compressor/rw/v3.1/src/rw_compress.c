#include "rw_compress.h"
#include <string.h>
#include <stdint.h>
#include <hexagon_protos.h>

typedef union {
  unsigned long long ud;
  unsigned int       uw[2];
} vect64;

#define MAX_BUF_SIZE 5664 // need space for worst-case all raw + worst-case all partial match


#define NUM_ANCHORS 4 // must be a power of 2
#if (NUM_ANCHORS != 4)
#error("NUM_ANCHORS not 4")
#endif
#define NUM_ANCHOR_BITS 2
#define NUM_CODE_BITS 2
#define WORD_SIZE_IN_BITS (sizeof(unsigned int) << 3) // 32

#define GET_BITS(hold,n) (unsigned int)(hold & ((1UL << n) - 1))
 
#ifdef __hexagon__
#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)
#else
#define LIKELY(x) (x)
#define UNLIKELY(x) (x)
#endif


#define CACHE_LINE_SHIFT (5)
#define CACHE_LINE_SZ  (1 << CACHE_LINE_SHIFT)

static inline void l2fetch_buffer(void *addr,unsigned int len)
{
  /* Cache-align starting address and length. */
  unsigned int ofs = ((unsigned int) addr) & (CACHE_LINE_SZ-1);
  addr = (void *) ((unsigned int) addr - ofs);
  len  = (len+ofs+CACHE_LINE_SZ-1) / CACHE_LINE_SZ;

  /* Width=cache line, height=# cache lines, stride=cache line */
  asm volatile ("l2fetch(%[addr],%[dim])" : : 
     [addr] "r" (addr), 
     [dim] "r" ( Q6_P_combine_IR(CACHE_LINE_SZ, Q6_R_combine_RlRl(CACHE_LINE_SZ, len)) )
     : "memory");

}

/* DCZERO the output buffer, 32 bytes at a time.
   Assume page size is a multiple of 32
   Assume dest addr is multiple of 32
*/
static inline void dczero(uint32_t addr,uint32_t size)
{
    uint32_t lines, i;
    lines = 4096 >> CACHE_LINE_SHIFT;
    for (i = 0; i < lines; i++)
    {
        asm("dczeroa(%0)" : : "r" (addr));
        addr += CACHE_LINE_SZ;
    }
}


#define pushCompressedBits(bits,numBits,compressedWordStream,compressedPartialBits,compressedPartialWord) \
  { \
    unsigned long long int temp = ((unsigned long long int) bits) << compressedPartialBits;                                  \
    compressedPartialBits += numBits;                                                                                        \
    compressedPartialWord = compressedPartialWord|temp;                                                                      \
    *compressedWordStream = compressedPartialWord;      \
    if (compressedPartialBits >= WORD_SIZE_IN_BITS) compressedWordStream++ ; \
    if (compressedPartialBits >= WORD_SIZE_IN_BITS) shift=WORD_SIZE_IN_BITS; \
    compressedPartialWord = compressedPartialWord >> shift;      \
    if (compressedPartialBits >= WORD_SIZE_IN_BITS) compressedPartialBits -= WORD_SIZE_IN_BITS;                              \
    shift=0;\
  }

  
#define pushHeader(bits,numBits,compressedWordStream,compressedPartialBits,compressedPartialWord) \
   pushCompressedBits(bits,numBits,compressedWordStream,compressedPartialBits,compressedPartialWord) \
   if (UNLIKELY(payloadHoldBits!=0)) pushCompressedBits(payloadHoldVal,payloadHoldBits,compressedWordStream,compressedPartialBits,compressedPartialWord) 
   

#define finalizeCompressedBits(compressedWordStream,compressedPartialBits,compressedPartialWord) \
  *compressedWordStream = compressedPartialWord;          \
  if (compressedPartialBits>0) compressedWordStream++;   

/*********************************************************************************************************/        
//compress a word-aligned buffer uncompressed of length in_len words into a word-aligned buffer compressed
unsigned int deltaCompress(unsigned int *uncompressed,unsigned int *compressed,unsigned int in_len)
{
  unsigned int i,val;
  unsigned int anchors[NUM_ANCHORS];
  vect64 vanchors[2];
  unsigned int *anchorPtr=&anchors[0];/* @todo anandj What the?,anchor_count*/;
  unsigned int *anchorLast=&anchors[NUM_ANCHORS-1];/* @todo anandj What the?,anchor_count*/;
  unsigned int *vanchorPtr=&vanchors[0].uw[0];
  unsigned int *vanchorLast=&vanchors[1].uw[1];
  unsigned int numCompressedPartialBits = 0;
  unsigned long long compressedPartialWord;
  unsigned int payloadHoldBits=0;
  unsigned int payloadHoldVal;
  unsigned int shift=0;
  unsigned int *compressed_raw           = compressed + 1; // 0th word will hold size of compressed raw
  unsigned int *compressed_raw_start     = compressed_raw;
  unsigned int *compressed_encoded       = compressed_raw + 1024; // compressed buffer, worst case, still has space beyond EOB
  unsigned int *compressed_encoded_start = compressed_encoded;
  unsigned int num_compressed_words_raw = 0;
  unsigned int num_compressed_words_encoded = 0;
  

  vect64 valval;
  vect64 val12;
  vect64 val34;


  //l2fetch_buffer((void *)uncompressed, in_len*4);
  //dczero((unsigned int)compressed,4352); // max size of rwbuffer
  //dczero((unsigned int)compressed,5664); // FIXME FIXME FIXME this requires support from rwbuffer
                                           // 4096 + 1536 (worst case bytes for raw + worst case bytes for anchor)
                                           // + 1 for size of compressed raw stream
                                           // round up to 5664 for nearest 32-byte alignment

  anchors[0] = anchors[1] = anchors[2] = anchors[3] = 0;

  compressedPartialWord=0ULL;
  payloadHoldVal=0;
  vanchors[0].ud=0;
  vanchors[1].ud=0;
  
  
  //loop over input
  for (i = 0; i < 1024; i++)
  {
    val = *uncompressed++;
    //printf("val_%d: 0x%x, ", i, val);
    if (val==0 ) goto CODE0;

    valval.uw[0]=val;
    valval.uw[1]=val;

    val12.ud =valval.ud ^ vanchors[0].ud;
    val34.ud =valval.ud ^ vanchors[1].ud;

    if ( val12.uw[0]==0) goto ANCHOR_MATCH0;
    if ( val12.uw[1]==0) goto ANCHOR_MATCH1;
    if ( val34.uw[0]==0) goto ANCHOR_MATCH2;
    if ( val34.uw[1]==0) goto ANCHOR_MATCH3;

    val12.ud &= 0xFFFFFC00FFFFFC00ULL;
    val34.ud &= 0xFFFFFC00FFFFFC00ULL;

    if (val12.uw[0]==0) goto PARTIAL_MATCH0;
    if (val12.uw[1]==0) goto PARTIAL_MATCH1;
    if (val34.uw[0]==0) goto PARTIAL_MATCH2;
    if (val34.uw[1]==0) goto PARTIAL_MATCH3;
    goto RAW;

CODE0:
    {
      //code is 00
      pushHeader(0,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);
      payloadHoldVal=0;
      payloadHoldBits=0;
      
      continue;
    }
ANCHOR_MATCH0:
    {
          pushHeader(1,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);
          payloadHoldVal=0;
          payloadHoldBits=NUM_ANCHOR_BITS;
         
          continue;
     } 
ANCHOR_MATCH1:
    {
          pushHeader(1,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);
          payloadHoldVal=1;
          payloadHoldBits=NUM_ANCHOR_BITS;
          
          continue;
     } 
ANCHOR_MATCH2:
    {
          pushHeader(1,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);
          payloadHoldVal=2;
          payloadHoldBits=NUM_ANCHOR_BITS;
          
          continue;
     } 
ANCHOR_MATCH3:
    {
          pushHeader(1,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);
          payloadHoldVal=3;
          payloadHoldBits=NUM_ANCHOR_BITS;
         
          continue;
     } 
PARTIAL_MATCH0:
    {
        pushHeader(2,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);  
        payloadHoldVal=((val&0x3ff)<<NUM_ANCHOR_BITS)+0; 
        payloadHoldBits=10+NUM_ANCHOR_BITS; 
        vanchors[0].uw[0]=val;
        
        continue;
    }
PARTIAL_MATCH1:
    {
        pushHeader(2,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);  
        payloadHoldVal=((val&0x3ff)<<NUM_ANCHOR_BITS)+1; 
        payloadHoldBits=10+NUM_ANCHOR_BITS; 
        vanchors[0].uw[1]=val;
       
        continue;
    }
PARTIAL_MATCH2:
    {
        pushHeader(2,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);  
        payloadHoldVal=((val&0x3ff)<<NUM_ANCHOR_BITS)+2; 
        payloadHoldBits=10+NUM_ANCHOR_BITS; 
        vanchors[1].uw[0]=val;
        
        continue;
    }
PARTIAL_MATCH3:
    {
        pushHeader(2,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);  
        payloadHoldVal=((val&0x3ff)<<NUM_ANCHOR_BITS)+3; 
        payloadHoldBits=10+NUM_ANCHOR_BITS; 
        vanchors[1].uw[1]=val;
        
        continue;
    }
RAW:
    {
      //if full or partial match above, control would have hit continue statement
      //no full or partial match, leave the word uncompressed, add it as an anchor
      *vanchorPtr++=val;
      if (vanchorPtr>vanchorLast) vanchorPtr=&vanchors[0].uw[0];
      pushHeader(3,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord); 
      *compressed_raw++ = val;
      ++num_compressed_words_raw;
      payloadHoldVal=0;
      payloadHoldBits=0;
      continue;
    }
  } //for loop
  //take care of the remaining bits
  pushHeader(0,NUM_CODE_BITS,compressed_encoded,numCompressedPartialBits,compressedPartialWord);
  finalizeCompressedBits(compressed_encoded,numCompressedPartialBits,compressedPartialWord);
  
  
  // attach the encoded portion to the end of the raw portion
  /*while(compressed_encoded_start < compressed_encoded){
        *compressed_raw++ = *compressed_encoded_start++;
  }*/
  // FIXME: reinstate
  num_compressed_words_encoded = compressed_encoded - compressed_encoded_start;
  
  //printf("num compressed words raw:     %d\n" , num_compressed_words_raw);
  compressed[0] = (num_compressed_words_encoded << 16) | num_compressed_words_raw;
  //compressed[0] = num_compressed_words_raw;

  //return (((compressed_raw-compressed) << 2)); // return size of the compressed buffer in bytes
  // FIXME: reinstate
  return 5664; // return size of the compressed buffer in bytes
}

