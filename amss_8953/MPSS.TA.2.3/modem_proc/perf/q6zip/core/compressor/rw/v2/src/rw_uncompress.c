#include "rw_compress.h"
#include <string.h>
#include <stdint.h>

/* linked in from dczero assembly version in dlpager/src */
void dczero(unsigned int addr, unsigned int size);

#define NUM_ANCHORS 4 // must be a power of 2
#if (NUM_ANCHORS != 4)
#error("NUM_ANCHORS not 4")
#endif
#define NUM_ANCHOR_BITS 2
#define NUM_CODE_BITS 2
#define WORD_SIZE_IN_BITS (sizeof(unsigned int) << 3) // 32

#define GET_BITS(hold,n) (unsigned int)(hold & ((1UL << n) - 1))
 
#ifdef __hexagon__
#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)
#else
#define LIKELY(x) (x)
#define UNLIKELY(x) (x)
#endif

#define SKIP_BITS_W_CHECK(compressedInPtr,bitCount,hold,n)      \
  bitCount -= n;                                    \
  hold >>= n;                                                   \
  if (UNLIKELY(bitCount <= WORD_SIZE_IN_BITS))                  \
  {                                                     \
    hold |= (uint64_t)(*(compressedInPtr++)) << bitCount;     \
    bitCount += WORD_SIZE_IN_BITS;                    \
  }


//check for end and if not, execute the corresponding case
#define JUMP_NEXT_CASE \
{ \
code = GET_BITS(hold,NUM_CODE_BITS); \
void *jump_ptr = jump_table[code]; \
goto *jump_ptr; \
} \

#define JUMP_NEXT_CASE2 \
{ \
code = GET_BITS(hold,NUM_CODE_BITS); \
void *jump_ptr = jump_table[code]; \
if (LIKELY(out_index < out_len) )\
goto *jump_ptr; \
goto RETLAB; \
} \

/*********************************************************************************************************/        
//uncompress a word-aligned buffer compressed of length in_len words into a word-aligned buffer uncompressed of of lengh out_len words
unsigned int deltaUncompress(unsigned int *compressed,unsigned int *uncompressed,unsigned int out_len)
{
  unsigned int delta;
  unsigned int code,val,size,anchor;
  unsigned int anchors[NUM_ANCHORS];
  unsigned int anchorIndex = NUM_ANCHORS - 1;
  unsigned int out_index = 0,currentWord = 0,numAvailBits = 0,bufIndex = 0;
  unsigned long long int hold = 0;
  void* jump_table[] = {
    &&CODE0, //0
    &&CODE1, //1
    &&CODE2, //2
    &&CODE3  //3
  };
  
  dczero((unsigned int)uncompressed, out_len);

  anchors[0] = anchors[1] = anchors[2] = anchors[3] = 0;
  //read the original uncompressed size
  //size = compressed[bufIndex++];
  //if (size == 0) return 0;
  //load up 64bits into the holding register
  //compressed++;
  SKIP_BITS_W_CHECK(compressed,numAvailBits,hold,0);
  //if (size != 1) SKIP_BITS_W_CHECK(compressed,numAvailBits,hold,0);

  //process the input
      JUMP_NEXT_CASE
CODE0:
      //value was 0
      SKIP_BITS_W_CHECK(compressed,numAvailBits,hold,NUM_CODE_BITS);
      uncompressed[out_index++] = 0;
      JUMP_NEXT_CASE
CODE1:
      //get anchor number which has exact match
      anchor = GET_BITS(hold>>NUM_CODE_BITS,NUM_ANCHOR_BITS);
      SKIP_BITS_W_CHECK(compressed,numAvailBits,hold,NUM_CODE_BITS+NUM_ANCHOR_BITS);
      uncompressed[out_index++] = anchors[anchor];
      JUMP_NEXT_CASE
CODE2:
      //get anchor number which has partial match
      anchor = GET_BITS(hold>>NUM_CODE_BITS,NUM_ANCHOR_BITS);
      //get the delta and construct output word
      delta = GET_BITS(hold>>(NUM_CODE_BITS+NUM_ANCHOR_BITS),10);
      SKIP_BITS_W_CHECK(compressed,numAvailBits,hold,NUM_CODE_BITS+NUM_ANCHOR_BITS+10);
      val = uncompressed[out_index++] = (anchors[anchor] & 0xFFFFFC00) | delta;
      //update existing anchor
      anchors[anchor] = val;
      JUMP_NEXT_CASE
CODE3:
      //no exact or partial match
      SKIP_BITS_W_CHECK(compressed,numAvailBits,hold,NUM_CODE_BITS);
      val = (unsigned int)hold;
      SKIP_BITS_W_CHECK(compressed,numAvailBits,hold,32);
      uncompressed[out_index++] = val;
      //add new anchor
      anchorIndex = (anchorIndex + 1) & (NUM_ANCHORS - 1);
      anchors[anchorIndex] = val; 
      JUMP_NEXT_CASE2
RETLAB: return (out_index << 2);
}
