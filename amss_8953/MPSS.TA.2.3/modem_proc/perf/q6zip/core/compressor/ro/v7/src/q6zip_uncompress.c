/*===========================================================================
* FILE:         q6zip_uncompress.c
*
* SERVICES:     DL PAGER
*
* DESCRIPTION:  q6zip uncompressor
*
* Copyright (c) 2014 Qualcomm Technologies Incorporated.
* All Rights Reserved. QUALCOMM Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

  EDIT HISTORY FOR MODULE
  
  $Header: //components/rel/perf.mpss/1.0.4/q6zip/core/compressor/ro/v7/src/q6zip_uncompress.c#2 $ $DateTime: 2016/02/22 22:01:09 $ $Author: pwbldsvc $
  
when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/19/16   hz      Fix compilation errors when hexagon tool switching to Clang.
01/17/14   ao      Initial revision (collaboration with Ric Senior)
03/18/14   rs      Updated to version q6zip3v3
04/25/14   rs      Updated to version q6zip3v4
08/21/14   hz      Made compatible with gcc 3.4.4 for Windows and Linux
8/28/14    rr      Updated to version q6zip3v5, added version info.
===========================================================================*/
volatile unsigned int version = 6;

#include "assert.h"
#include <stdint.h>
#include <stdlib.h>
#include "q6zip_uncompress.h"
#if __hexagon__ /*on-target env.*/
#include <hexagon_protos.h>
#else/*off-target env.*/
#include "q6_intrinsic_alter.h"
#include <string.h>//for memset.
#endif/*__hexagon__*/

//#define DEBUG

#ifdef DEBUG
#include <stdio.h>
#endif

#if defined DEBUG
#define debug printf
#else
#define debug(...)
#endif

#define GET_WORD_FROM(ci)           (*((unsigned int*)ci))

typedef struct 
{
   uint64_t reg;
   int num_bits;
}hold_reg_t;

void init_hold(hold_reg_t *hold, unsigned int **input)
{
  
  hold->reg = (uint64_t)**input;
  *input = *input + 1;  
  hold->reg |= ((uint64_t)**input << 32);
  *input = *input + 1;
  hold->num_bits = 64;
}

inline void skip(hold_reg_t *hold, unsigned int n)
{
  ASSERT(hold->num_bits >= n);
  hold->num_bits -= n;
  hold->reg >>= n;
}

inline void reload(hold_reg_t *hold, unsigned int **input_ptr)
{
  unsigned int *in = *input_ptr;
  if (hold->num_bits <=32)
  {
	hold->reg |= (uint64_t)(*in) << (hold->num_bits);
	hold->num_bits += 32;
	*input_ptr = (unsigned int *)*input_ptr + 1;
  }
}

inline void skip_reload(hold_reg_t *hold, unsigned int n, unsigned int **input_ptr)
{
  skip(hold, n);
  reload(hold, input_ptr);
}

inline void transfer(hold_reg_t *hold_lo, hold_reg_t *hold_hi)
{
  uint32_t bits;
  /* figure out num bits to transger */
  char n_bits = 64 - hold_lo->num_bits;
  ASSERT(n_bits <= 32);

  /* get the bits*/
  bits = hold_hi->reg & ((1 << n_bits) - 1);
  
  /* update hold_lo*/
  hold_lo->reg |= (uint64_t)bits << hold_lo->num_bits;
  hold_lo->num_bits += n_bits;
  ASSERT(hold_lo->num_bits == 64);

  /*update hold_hi*/
  hold_hi->reg >>= n_bits;
  hold_hi->num_bits -= n_bits;
  ASSERT(hold_hi->num_bits >= 0);

}

#if __hexagon__ /*on-target env.*/
#define GET_RAW_WORD() (Q6_dcfetch_A(raw+1), *raw++)	
#else /*off-target env.*/
#define GET_RAW_WORD() (*raw++)
#endif /*__hexagon__*/

#define CACHE_LINE_SHIFT (5)
#define CACHE_LINE_SZ  (1 << CACHE_LINE_SHIFT)

#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)

#define JUMP_NEXT_DECODE  {void *jumpPtr=jump_table[op1]; if (LIKELY(op1==1)) goto MATCH_8N_SQ0;goto *jumpPtr;}

//Q6_R_extractu_RII.
#define EXTRACTU_BITS(src,n_bits,n_offset)        \
    (((src)>>(n_offset))&((1UL<<(n_bits))-1))

//Q6_R_tableidxb_RII.
#define TABLEIDXB_RII(dest,src,n_bits,n_offset)       \
    (((dest)&~((1UL<<(n_bits))-1))|(((src)>>(n_offset))&((1UL<<(n_bits))-1)))

//Q6_R_insert_RII.
#define INSERT_BITS(dest,src,n_bits,n_offset)       \
    (((dest)&~(((1UL<<(n_bits))-1)<<(n_offset)))|(((src)&((1UL<<(n_bits))-1))<<(n_offset)))

/*
 *@warning unused function.
 *static inline void l2fetch_buffer
 *(
 *    void *addr,
 *  unsigned int len
 *)
 *{
 *    [> Cache-align starting address and length. <]
 *  unsigned int ofs = ((unsigned long) addr) & (CACHE_LINE_SZ-1);
 *  addr = (void *) ((unsigned long) addr - ofs);
 *    len  = (len+ofs+CACHE_LINE_SZ-1) / CACHE_LINE_SZ;
 *
 *    [> Width=cache line, height=# cache lines, stride=cache line <]
 *    asm volatile ("l2fetch(%[addr],%[dim])" : :
 *                  [addr] "r" (addr),
 *                  [dim] "r" ( Q6_P_combine_IR(CACHE_LINE_SZ, Q6_R_combine_RlRl(CACHE_LINE_SZ, len)) )
 *                  : "memory");
 *
 *}
 */

/* DCZERO the output buffer, 32 bytes at a time.
   Assume page size is a multiple of 32
   Assume dest addr is multiple of 32
*/
static inline void dczero(uint32_t addr,uint32_t size)
{
#if __hexagon__ /*on-target env.*/
    uint32_t lines, i;
    lines = 4096 >> CACHE_LINE_SHIFT;
    for (i = 0; i < lines; i++)
    {
        asm("dczeroa(%0)" : : "r" (addr));
        addr += CACHE_LINE_SZ;
    }
#else/*off-target env.*/
    memset((void *)((unsigned long)addr), 0, size);
#endif/*__hexagon__*/
}

static unsigned int* input;
static int* q6zip_out_buf_size;
#define MAX_CODES                 16
  
unsigned int  header_length[MAX_CODES];

#define OP1_BITS 3
#define OP2_BITS 3
#define OP3_BITS 2
#define LB_BITS 9

#define MATCH_6N_2x0_SQ0_BITS 3
#define MATCH_8N_SQ0_BITS     3
#define MATCH_5N_3x0_SQ0_BITS 4
#define NO_MATCH_BITS         3
#define DICT1_MATCH_BITS      3
#define DICT2_MATCH_BITS      4
#define MATCH_6N_2x0_SQ1_BITS 3
#define MATCH_8N_SQ1_BITS     3
#define MATCH_4N_4x0_SQ1_BITS 6
#define MATCH_4N_4x0_SQ0_BITS 5
#define MATCH_5N_3x0_SQ1_BITS 5
#define MATCH_6N_2x2_SQ0_BITS 6
#define MATCH_6N_2x4_SQ0_BITS 6
#define MATCH_6N_2x2_SQ1_BITS 7
#define MATCH_6N_2x4_SQ1_BITS 7

void q6zip_header_length_table_init()
{

  /* Real decodes */
  header_length[0b0000] = 3;
  header_length[0b0001] = 3;
  header_length[0b0010] = 4;    /*5N3x0*/
  header_length[0b0011] = 3;
  header_length[0b0100] = 3;
  header_length[0b0101] = 4;    /*DICT2*/
  header_length[0b0110] = 3;
  header_length[0b0111] = 3;
  header_length[0b1000] = 3;
  header_length[0b1001] = 3;
  header_length[0b1010] = 4;    /*LEVEL A*/
  header_length[0b1011] = 3;
  header_length[0b1100] = 3;
  header_length[0b1101] = 4;    /*LEVEL B*/
  header_length[0b1110] = 3;
  header_length[0b1111] = 3;  

}

int q6zip_uncompress(char* out_buf, int* out_buf_size,
                     char* in_buf,  int in_buf_size,
                                     char* dict)
{
  input = (unsigned int*)in_buf;
  q6zip_out_buf_size = out_buf_size;
  unsigned int* out = (unsigned int*)out_buf;

  unsigned int* raw = input;
  /* first word of input provides the number of raw words.
     point "in" past list of raw words */
  unsigned int* in = input + 1 + (*raw++);

  hold_reg_t hold;
  init_hold(&hold, &in);

  unsigned int* dictionary1 = (unsigned int*)dict;
  unsigned int* dictionary2 = &dictionary1[1<<DICT1_BITS];
  unsigned int entry;
  unsigned int masked;
  
  unsigned int op1 = 0, next_header_offset = 0, next_header_length = 0;  
  int lastOut=0xFFFFFFFF;
  
  void * jumpPtr;  
  /*
   *@warning unused variable.
   *unsigned int count = 0;
   */
  static void *jump_table[] = 
	{       
	        &&MATCH_FFFFFF00_SQ0,//0
            &&MATCH_FFFFFFFF_SQ0,//1
            &&MATCH_FFFFF000_SQ0,//2
            &&NO_MATCH,          //3
            &&DICT1_MATCH,       //4
            &&DICT2_MATCH,       //5
            &&MATCH_FFFFFF00_SQ1,//6
            &&MATCH_FFFFFFFF_SQ1,//7
            &&MATCH_FFFFFF00_SQ0,//8
            &&MATCH_FFFFFFFF_SQ0,//9
            &&LEVEL_A,           //10 =0xA==0x1010 
            &&NO_MATCH,          //11
            &&DICT1_MATCH,       //12
            &&LEVEL_B,           //13 0xD==1101 MATCH_6N_2x2_SQ1  = ( 0b1011010, 7, 0xFFFF00FF ,"MATCH_6N_2x2_SQ1"),     #MATCH_6Nx1
            &&MATCH_FFFFFF00_SQ1,//14
            &&MATCH_FFFFFFFF_SQ1,//15
  }; 
  static int initialized = 0;

  if(! initialized)
  {    
    q6zip_header_length_table_init();
    initialized = 1;
  }

  /* l2fetch does not matter for correctness
    l2fetch_buffer((void *)in_buf, in_buf_size);  
  */
  dczero((unsigned long)out_buf, 4096);    

  op1 = Q6_R_extractu_RII((unsigned int)hold.reg,OP1_BITS+1,0);
  jumpPtr=jump_table[op1];
  next_header_offset = header_length[op1];
  op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, next_header_offset);
  skip_reload(&hold, next_header_offset, &in);  
  goto *jumpPtr;
   
  NO_MATCH:	   
	debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		  hold.reg, hold.num_bits, op1);    
	jumpPtr = jump_table[op1];
	*out = *raw++; out++;	
	next_header_length = header_length[op1];
	op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, next_header_length);           
	skip_reload(&hold, next_header_length, &in);  
	goto *jumpPtr;

   DICT1_MATCH:
	debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		  hold.reg, hold.num_bits, op1);   
	 jumpPtr = jump_table[op1];
	 next_header_length = header_length[op1];	 
	 entry = EXTRACTU_BITS((unsigned int)hold.reg,DICT1_BITS, next_header_length);
	 *out = GET_WORD_FROM(dictionary1 + entry);	out++;
	 next_header_offset = next_header_length + DICT1_BITS;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;
	    		  
   DICT2_MATCH:
	debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		  hold.reg, hold.num_bits, op1);   
	 jumpPtr = jump_table[op1];
	 next_header_length = header_length[op1];	 
	 entry = EXTRACTU_BITS((unsigned int)hold.reg,DICT2_BITS, next_header_length);
	 *out = GET_WORD_FROM(dictionary2 + entry);	out++;
	 next_header_offset = next_header_length + DICT2_BITS;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;
	    
  MATCH_FFFFFFFF_SQ0:
	 
	debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		  hold.reg, hold.num_bits, op1);   

	 jumpPtr = jump_table[op1];
	 next_header_length = header_length[op1];	 
	 lastOut=TABLEIDXB_RII(lastOut,(unsigned int)hold.reg,LB_BITS,next_header_length);
	 *out = *(out + lastOut); out++;
	 next_header_offset = next_header_length + LB_BITS;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;

   MATCH_FFFFFFFF_SQ1:	
	
	debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		  hold.reg, hold.num_bits, op1);   

	 jumpPtr = jump_table[op1];
	 next_header_length = header_length[op1];	 	 
	 *out = *(out + lastOut); out++;
	 skip_reload(&hold, next_header_length, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;
	 
	MATCH_FFFFFF00_SQ0:    
	debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		  hold.reg, hold.num_bits, op1);   

	 jumpPtr = jump_table[op1];
	 next_header_length = header_length[op1];	 
	 lastOut=TABLEIDXB_RII(lastOut,(unsigned int)hold.reg,LB_BITS,next_header_length);
	 masked = EXTRACTU_BITS((unsigned int)hold.reg,8,next_header_length+LB_BITS);
	 *out = INSERT_BITS(*(out + lastOut), masked, 8,0); out++;
	 next_header_offset = next_header_length + LB_BITS + 8;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;

    MATCH_FFFFF000_SQ0:
	 debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		  hold.reg, hold.num_bits, op1);   

	 jumpPtr = jump_table[op1];
	 next_header_length = header_length[op1];	 
	 lastOut=TABLEIDXB_RII(lastOut,(unsigned int)hold.reg,LB_BITS,next_header_length);
	 masked = EXTRACTU_BITS((unsigned int)hold.reg,12,next_header_length+LB_BITS);
	 *out = INSERT_BITS(*(out + lastOut), masked, 12,0); out++;
	 next_header_offset = next_header_length + LB_BITS + 12;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;

    MATCH_FFFFFF00_SQ1:
	debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		  hold.reg, hold.num_bits, op1);   

	 jumpPtr = jump_table[op1];
	 next_header_length = header_length[op1];	 	 
	 masked = EXTRACTU_BITS((unsigned int)hold.reg,8,next_header_length);
	 *out = INSERT_BITS(*(out + lastOut), masked, 8,0); out++;
	 next_header_offset = next_header_length + 8;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;

    LEVEL_B:            
      debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		  hold.reg, hold.num_bits, op1);   
     	          
     jumpPtr = jump_table[op1];
	   next_header_length = header_length[op1];	 
     skip_reload(&hold, next_header_length, &in); 
     if(hold.reg & 0x1)
       goto MATCH_FFFFF000_SQ1;

    /*
     *@warning unused label.
     *MATCH_FFFF0000_SQ0:
     */
	 lastOut=TABLEIDXB_RII(lastOut,(unsigned int)hold.reg,LB_BITS,1);
	 masked = EXTRACTU_BITS((unsigned int)hold.reg,16,LB_BITS+1);
	 *out = INSERT_BITS(*(out + lastOut), masked, 16,0); out++;
	 next_header_offset = LB_BITS + 16 + 1;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;  
     
   MATCH_FFFFF000_SQ1:      
	 masked = EXTRACTU_BITS((unsigned int)hold.reg,12,1);
	 *out = INSERT_BITS(*(out + lastOut), masked, 12,0); out++;
	 next_header_offset = 12 + 1;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;

   LEVEL_A:
      debug("hold: %016llX  bits: %2d  nxt_hdr: %2x \n", 
		    hold.reg, hold.num_bits, op1);   
     if((out - (unsigned int*)out_buf) >=1024)
     {
       *q6zip_out_buf_size = (out - (unsigned int*)out_buf) * sizeof(unsigned int) ;		
       return 0;
     }


     jumpPtr = jump_table[op1];
	   next_header_length = header_length[op1];	 
     skip_reload(&hold, next_header_length, &in); 
     if((hold.reg & 3) == 0)
       goto MATCH_FFFF0000_SQ1;     
     if((hold.reg & 3) == 3)
       goto MATCH_FFFF00FF_SQ0;
     if((hold.reg & 3) == 2)
       goto MATCH_FF00FFFF_SQ0;
     if((hold.reg & 7) == 5)
       goto MATCH_FFFF00FF_SQ1;
 
   /*
    * @warning unused label.
    *MATCH_FF00FFFF_SQ1:
    */
   masked = EXTRACTU_BITS((unsigned int)hold.reg,8,3);
	 *out = INSERT_BITS(*(out + lastOut), masked, 8,16); out++;
	 next_header_offset = 8 + 3;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr; 
    
   MATCH_FFFF00FF_SQ0:
   lastOut=TABLEIDXB_RII(lastOut,(unsigned int)hold.reg,LB_BITS,2);
	 masked = EXTRACTU_BITS((unsigned int)hold.reg,8,LB_BITS+2);
	 *out = INSERT_BITS(*(out + lastOut), masked, 8,8); out++;
	 next_header_offset = LB_BITS + 8 + 2;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr; 

   MATCH_FFFF0000_SQ1:
	 masked = EXTRACTU_BITS((unsigned int)hold.reg,16,2);
	 *out = INSERT_BITS(*(out + lastOut), masked, 16,0); out++;
	 next_header_offset = 16 + 2;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;     
	  
   MATCH_FF00FFFF_SQ0:
   lastOut=TABLEIDXB_RII(lastOut,(unsigned int)hold.reg,LB_BITS,2);
	 masked = EXTRACTU_BITS((unsigned int)hold.reg,8,LB_BITS+2);
	 *out = INSERT_BITS(*(out + lastOut), masked, 8,16); out++;
	 next_header_offset = LB_BITS + 8 + 2;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;       
   
   MATCH_FFFF00FF_SQ1: 
   masked = EXTRACTU_BITS((unsigned int)hold.reg,8,3);
	 *out = INSERT_BITS(*(out + lastOut), masked, 8,8); out++;
	 next_header_offset = 8 + 3;
	 skip_reload(&hold, next_header_offset, &in);  
	 op1 = EXTRACTU_BITS((unsigned int)hold.reg, OP1_BITS+1, 0);	 
	 goto *jumpPtr;   


}