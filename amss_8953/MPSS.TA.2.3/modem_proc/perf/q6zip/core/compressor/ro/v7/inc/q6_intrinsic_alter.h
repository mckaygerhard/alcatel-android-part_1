#ifndef __Q6_INTRINSIC_ALTER_H__
#define __Q6_INTRINSIC_ALTER_H__

/*===========================================================================
 * FILE:         q6_intrinsic_alter.h
 *
 * SERVICES:     DL PAGER
 *
 * DESCRIPTION:  q6zip uncompressor header
 *
 * Copyright (c) 2016 Qualcomm Technologies Incorporated.
 * All Rights Reserved. QUALCOMM Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

  EDIT HISTORY FOR MODULE
  
when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/14/16   hzhi    Initial revision.
===========================================================================*/

/*==================================================================*
  * added by hzhi for new macros                                     *
  *==================================================================*/
//Q6_R_insert_RII.
#define Q6_R_insert_RII(dest,src,n_bits,n_offset)       \
    (((dest)&~(((1UL<<(n_bits))-1)<<(n_offset)))|(((src)&((1UL<<(n_bits))-1))<<(n_offset)))

//Q6_R_extractu_RII.
#define Q6_R_extractu_RII(src,n_bits,n_offset)        \
    (((src)>>(n_offset))&((1UL<<(n_bits))-1))

//Q6_R_tableidxb_RII.
#define Q6_R_tableidxb_RII(dest,src,n_bits,n_offset)       \
    (((dest)&~((1UL<<(n_bits))-1))|(((src)>>(n_offset))&((1UL<<(n_bits))-1)))

//Q6_dcfetch_A
 
 /*==================================================================*/
#endif /*__Q6_INTRINSIC_ALTER_H__*/
