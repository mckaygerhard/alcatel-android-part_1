#!/bin/sh
#===============================================================================
#
# Image Top-Level Build Script
#
# General Description
#    Image Top-Level Build Script
#
# Copyright (c) 2009-2012 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
# $Header: //components/rel/build.qc/1.0/ms/build.sh#1 $
# $DateTime: 2015/01/13 21:27:22 $
# $Change: 7275381 $
#
#===============================================================================

cd `dirname $0`

# Call script to setup build environment, if it exists.
#if [ -e setenv.sh ]; then
#source ./setenv.sh
#fi

# Call the main build command
python build_variant.py $*
build_result=$?
if [ "${build_result}" != "0" ] ; then
    exit ${build_result}
fi

