/*!
  @file
  qsh_clt_dsm.h

  @brief
  Header file for DSM client implementation.
*/

/*==============================================================================

  Copyright (c) 2014 QUALCOMM Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  QUALCOMM Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of QUALCOMM Technologies Incorporated.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/qsh/src/image_qsh.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------- 
        
==============================================================================*/

#ifndef IMAGE_QSH_H
#define IMAGE_QSH_H
#include <qsh.h>
/*==============================================================================

                           INCLUDE FILES

==============================================================================*/


/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

  FUNCTION:  image_qsh_register

==============================================================================*/
/*!
  @brief
  QSH image register

  @return
  None
*/
/*============================================================================*/
void image_qsh_register
(
  void
);


/*==============================================================================

  FUNCTION:  image_qsh_dump_meta_hdr_constants_get

==============================================================================*/
/*!
  @brief
  Function called during initialization to get the dump meta header constants

  @return
  None
*/
/*============================================================================*/
void image_qsh_dump_meta_hdr_constants_get
( 
  void
);


#endif /* IMAGE_QSH_H */
