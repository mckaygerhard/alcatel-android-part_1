/*!
  @file
  prose_data_manager.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_data_manager.h#8 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_DATA_MANAGER_H
#define PROSE_DATA_MANAGER_H

#include "hashmap.h"

#include "prose_defs.h"

class ProseDataManager
{
  public:
    static ProseDataManager* get_instance();

    virtual ~ProseDataManager();

    static void destroy();

    void init();

    void read_publish_data_from_efs();

    void write_publish_data_to_efs();

    void read_subscribe_data_from_efs();

    void write_subscribe_data_to_efs();

    void read_match_data_from_efs();

    void write_match_data_to_efs();

    struct PacInfo
    {
      prose_app_code_s pac_;
      prose_disc_key_s disc_key_;
      uint64           t4000_timestamp_;
      int              de_id_;
      sys_plmn_id_s_type announcing_plmn_;      
      PacInfo() : pac_(), disc_key_(), t4000_timestamp_(0), de_id_(0), announcing_plmn_() {}
    };

    void add_publish_info(const pair<prose_pa_id_s, prose_os_app_id_s>& id, const PacInfo& pac_info);

    const PacInfo* lookup_publish_info(const pair<prose_pa_id_s, prose_os_app_id_s>& id);
    
    void remove_publish_info(const pair<prose_pa_id_s, prose_os_app_id_s>& id);

    struct FilterElem
    {
      prose_app_code_s       filter_pac_;
      list<prose_app_code_s> filter_mask_;
      uint64                 t4002_timestamp_;
      FilterElem() : filter_pac_(), filter_mask_(), t4002_timestamp_(0) {}
    };

    struct FilterInfo
    {
      list<FilterElem>      filter_list_;
      int                   de_id_;
      FilterInfo() : filter_list_(), de_id_(0) {}
    };

    void add_subscribe_info(const pair<prose_pa_id_s, prose_os_app_id_s>& id, const FilterInfo& filter_info);

    void add_subscribe_info(const pair<prose_pa_id_s, prose_os_app_id_s>& id, const FilterElem& filter_elem);

    void add_subscribe_info(const pair<prose_pa_id_s, prose_os_app_id_s>& id, int de_id);

    const FilterInfo* lookup_subscribe_info(const pair<prose_pa_id_s, prose_os_app_id_s>& id);

    const FilterInfo* remove_subscribe_info(const pair<prose_pa_id_s, prose_os_app_id_s>& id, const FilterElem& filter_elem);

    void remove_subscribe_info(const pair<prose_pa_id_s, prose_os_app_id_s>& id);

    struct MatchInfo
    {
      prose_pa_id_s    pa_id_;
      uint64           t4004_timestamp_;
      prose_app_code_s metadata_index_mask_;
      MatchInfo() : pa_id_(), t4004_timestamp_(0), metadata_index_mask_(), blocked_(false) {}
      bool is_blocked() const { return blocked_; }
      private:
      bool             blocked_;
      friend class ProseDataManager;
    };

    void add_match_info(const prose_app_code_s& id);

    bool add_match_info(const prose_app_code_s& id, const MatchInfo& match_info);

    const MatchInfo* lookup_match_info(const prose_app_code_s& id, const prose_app_code_s** ptr = 0);

    void block_match_info(const prose_app_code_s& id);

    void remove_match_info(const prose_app_code_s& id);

    static const prose_app_code_s PROSE_APP_CODE_DEFAULT_MASK;

    PacInfo& get_pac_info();
    
    FilterElem& get_filter_elem();

    MatchInfo& get_match_info();

  private:
    ProseDataManager();
    ProseDataManager(const ProseDataManager&);
    ProseDataManager& operator=(const ProseDataManager&);

    static const uint8 MAX_DATA_RECORDS;
    static const uint8 EFS_DATA_VERSION;
    static const char* PROSE_DATA_PATH;
    static const char* PUBLISH_DATA_FILENAME;
    static const char* SUBSCRIBE_DATA_FILENAME;
    static const char* MATCH_DATA_FILENAME;
    static ProseDataManager *instance_;

#ifdef _WIN32
#pragma pack(push,1) // Save previous, and turn on 1 byte alignment
#endif

    PACK(struct) PublishDataRecord
    {
      prose_pa_id_s     pa_id_;
      prose_os_app_id_s os_app_id_;
      int               de_id_;
      prose_app_code_s  pac_;
      prose_disc_key_s  disc_key_;
      sys_plmn_id_s_type announcing_plmn_;
      uint64            t4000_timestamp_;
    } publish_record_;

    PACK(struct) SubscribeDataRecord
    {
      prose_pa_id_s     pa_id_;
      prose_os_app_id_s os_app_id_;
      int               de_id_;
      uint32            num_filters_;
      PACK(struct) FilterInfoRecord
      {
        prose_app_code_s       filter_pac_;
        uint32                 num_masks_;
        prose_app_code_s       filter_mask_[10];
        uint64                 t4002_timestamp_;
      } filter_list_[10];
    } subscribe_record_;

    PACK(struct) MatchDataRecord
    {
      prose_app_code_s matched_pac_;
      prose_pa_id_s    pa_id_;
      prose_app_code_s metadata_index_mask_;
      uint64           t4004_timestamp_;
    } match_record_;

#ifdef _WIN32
#pragma pack(pop) // Revert alignment to what it was previously
#endif

    typedef hashmap<pair<prose_pa_id_s, prose_os_app_id_s>, PacInfo> PublishInfoMap;
    PublishInfoMap publish_info_map_;
    PacInfo pac_info_;

    typedef hashmap<pair<prose_pa_id_s, prose_os_app_id_s>, FilterInfo> SubscribeInfoMap;
    SubscribeInfoMap subscribe_info_map_;
    FilterElem filter_elem_;

    typedef hashmap<prose_app_code_s, MatchInfo> MatchInfoMap;
    MatchInfoMap match_info_map_;
    MatchInfo match_info_;

    bool publish_data_dirty_;
    bool subscribe_data_dirty_;
    bool match_data_dirty_;
};

inline bool operator==(const prose_pa_id_s& id1, const prose_pa_id_s& id2)
{
  return !memcmp(&id1, &id2, sizeof(prose_pa_id_s));
}

inline bool operator==(const prose_os_app_id_s& id1, const prose_os_app_id_s& id2)
{
  return !memcmp(&id1, &id2, sizeof(prose_os_app_id_s));
}

inline bool operator==(const prose_app_code_s& id1, const prose_app_code_s& id2)
{
  return !memcmp(&id1, &id2, sizeof(prose_app_code_s));
}

inline bool operator!=(const prose_app_code_s& id1, const prose_app_code_s& id2)
{
  return !(id1 == id2);
}

inline prose_app_code_s operator~(const prose_app_code_s& id1)
{
  prose_app_code_s pac;
  const uint32 *id1_ptr = reinterpret_cast<const uint32*>(id1.byte);
  uint32 *result = reinterpret_cast<uint32*>(pac.byte);
  uint32 i, j;
  for (i = 0; i < PROSE_APP_CODE_SIZE / sizeof(uint32); i += sizeof(uint32))
    *result++ = ~(*id1_ptr++);
  for (j = 0; j < PROSE_APP_CODE_SIZE % sizeof(uint32); j++, i++)
    pac.byte[i] = ~id1.byte[i];
  return pac;
}

inline prose_app_code_s operator&(const prose_app_code_s& id1, const prose_app_code_s& id2)
{
  prose_app_code_s pac;
  const uint32 *id1_ptr = reinterpret_cast<const uint32*>(id1.byte);
  const uint32 *id2_ptr = reinterpret_cast<const uint32*>(id2.byte);
  uint32 *result = reinterpret_cast<uint32*>(pac.byte);
  uint32 i, j;
  for (i = 0; i < PROSE_APP_CODE_SIZE / sizeof(uint32); i += sizeof(uint32))
    *result++ = *id1_ptr++ & *id2_ptr++;
  for (j = 0; j < PROSE_APP_CODE_SIZE % sizeof(uint32); j++, i++)
    pac.byte[i] = id1.byte[i] & id2.byte[i];
  return pac;
}

inline bool operator==(const ProseDataManager::FilterElem& id1, const ProseDataManager::FilterElem& id2)
{
  return (id1.filter_pac_ == id2.filter_pac_ && id1.filter_mask_ == id2.filter_mask_); 
}

inline bool operator==(const ProseDataManager::MatchInfo& id1, const ProseDataManager::MatchInfo& id2)
{
  return id1.pa_id_ == id2.pa_id_;
}

#endif /* PROSE_DATA_MANAGER_H */
