/*!
  @file
  prose_rex_task.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_rex_task.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_REX_TASK_H
#define PROSE_REX_TASK_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <msgcfg.h>
#include <msg.h>
#include <err.h>
#include <comdef.h>
#include <rex.h>
#include <IxErrno.h>

/*=============================================================================

                   INTERNAL DEFINITIONS AND TYPES

=============================================================================*/
/*! @brief PROSE task rex control block
 * On target these declarations reside in service/task/task.c */
extern rex_tcb_type prose_tcb;

extern void prose_task(unsigned long);

/*=============================================================================

                         OS Abstraction Layer.

=============================================================================*/
typedef rex_sigs_type prose_signal_mask_t;

typedef rex_sigs_type prose_signal_result_t;


/*===========================================================================*/
/*  The signals that can be sent to the prose task */
/*---------------------------------------------------------------------------*/
/*  to be set for PROSE's MSGR REX client & associated queue */
#define PROSE_MSGR_Q_SIG      0x00000001
/*---------------------------------------------------------------------------*/
/*  PROSE's watchdog signal */
#define PROSE_WDOG_SIG        0x00000002

#define PROSE_TIMER_POOL_SIG      0x00000004

/*---------------------------------------------------------------------------*/
/*  to be set when the task is stopped during power down */
#define PROSE_TASK_STOP_SIG   TASK_STOP_SIG

/*===========================================================================*/
/* All the signals related to PROSE task */
#define PROSE_SIGS ( PROSE_MSGR_Q_SIG           |\
                     PROSE_WDOG_SIG             |\
                     PROSE_TIMER_POOL_SIG       |\
                     PROSE_TASK_STOP_SIG)


/******************************************************************************
                 Prose Task prototypes.
******************************************************************************/
/*===========================================================================

FUNCTION PROSE_SET_SIGS

DESCRIPTION    This function sets the signals for prose Manager task.

DEPENDENCIES   None.

RETURN VALUE   None.

SIDE EFFECTS   None.

===========================================================================*/
void prose_set_sigs ( rex_sigs_type sigs );

/*=============================================================================

  FUNCTION:  prose_clr_sigs

=============================================================================*/
/*!
    @brief
    Clears the required signals

    @return
    None
*/
/*===========================================================================*/
void prose_clr_sigs ( prose_signal_mask_t sigs );

/*=============================================================================

  FUNCTION:  prose_task_check_dog

=============================================================================*/
/*!
    @brief
    Checks is wdog signal is set and pets it and clears the wdog signal

    @return
    None
*/
/*===========================================================================*/
void prose_task_check_dog (void);


#ifdef __cplusplus
}
#endif


class ProseRexTask
{
  private:
    static void init();
    static void deinit();
  public:
    static void main(dword dummy);
};


#endif /* PROSE_TASK_H */
