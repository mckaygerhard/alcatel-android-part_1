/*!
  @file
  prose_dispatcher.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/main/inc/prose_dispatcher.h#5 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_DISPATCHER_H
#define PROSE_DISPATCHER_H

#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "msgr_rex.h"
#include "ds_lted_ext_msg.h"
#include "lte_mac_msg.h"
#include "lte_rrc_ext_msg.h"
#include "dsm_item.h"

#ifdef __cplusplus
}
#endif

#include "prose_ext_msg.h"
#include "prose_diag.h"

class ProseDispatcher
{
  public:
    static ProseDispatcher *get_instance();

    ~ProseDispatcher() {}

    static void destroy();

    bool init(void);

    void deinit(void);

    void loop(void);

    template<class T> void send_msgr_msg(T& t, msgr_umid_type umid);

    template<class T> void send_msgr_attach_msg(T& msg, dsm_item_type** dsm_ptr,
        msgr_umid_type umid);

  private:
    ProseDispatcher();
    ProseDispatcher(const ProseDispatcher&);
    ProseDispatcher& operator=(const ProseDispatcher&);

    errno_enum_type dispatch(msgr_hdr_s *msg_ptr);

    template<class T> errno_enum_type handle_msg(const T* msg_ptr);

    bool validate_disc_req_params(prose_disc_type_e disc_type, const prose_os_app_id_s& os_app_id, const prose_pa_id_s& pa_id, uint32 duration, uint32 request_time);

    static ProseDispatcher *instance_;

    typedef union
    {
      prose_disc_publish_req_s publish_req;
      prose_disc_publish_cancel_req_s pub_cancel_req;
      prose_disc_subscribe_req_s subs_req;
      prose_disc_subscribe_cancel_req_s subs_cancel_req;
      prose_disc_set_config_req_s set_config;
      prose_disc_get_config_req_s get_config;
      prose_disc_set_category_req_s set_cat;
      prose_disc_get_category_req_s get_cat;
      prose_disc_terminate_req_s terminate_req;
      ds_appsrv_lte_d_post_req_id_ind_s post_req_ind;
      ds_appsrv_lte_d_post_result_ind_s post_result;
      ds_appsrv_srvreq_result_ind_s srvreq_ind;
      ds_appsrv_imsi_data_changed_ind_s imsi_ind;
      lte_mac_pac_add_cnf_s pac_add;
      lte_mac_filter_add_cnf_s filter_add;
      lte_mac_pac_match_rpt_ind_s match_rpt;
      lte_mac_pac_tx_status_rpt_ind_s tx_status_rpt;
      lte_rrc_lted_available_ind_s lted_avail;
      lte_rrc_lted_not_available_ind_s lted_not_avail;
      lte_rrc_conn_rel_ind_s conn_rel_ind;
      lte_rrc_lted_plmn_earfcn_ind_s plmn_earfcn_ind;
    } prose_msg_u;

    static msgr_umid_type external_umid_list_[];
    static msgr_umid_type internal_umid_list_[];

    uint32                rcvd_cnt_;  /*!< Total number of received msgs */

    typedef struct
    {
      /* qlink for the REX Q associated to this task */
      q_link_type  qlink;
      prose_msg_u  msg;

    } prose_msg_t;
    
    prose_msg_t          *rcv_msg_ptr_;      /*! Ptr to rcvd msgs*/

    q_type                msgr_rcv_q_;
    msgr_client_t         msgr_client_;
    msgr_id_t             msgr_q_id_;
};



template<class T>
void ProseDispatcher::send_msgr_msg(T& msg, msgr_umid_type umid)
{
  uint8 inst_id = 0;
  msgr_init_hdr_inst(&msg.msg_hdr, MSGR_PROSE_DISC, umid, inst_id);
  errno_enum_type status = msgr_send(&msg.msg_hdr, sizeof(msg));
  PROSE_MSG_2(PROSE_MAIN_LOW, "Sent umid 0x%x with status %d", umid, status);
  ASSERT(status == E_SUCCESS);
}

template<class T>
void ProseDispatcher::send_msgr_attach_msg(T& msg, dsm_item_type** dsm_ptr, msgr_umid_type umid)
{
  uint8 inst_id = 0;
  msgr_attach_struct_type *att_ptr = NULL;
  
  if (*dsm_ptr)
  {
    msgr_init_hdr_attach(&msg.msg_hdr, MSGR_PROSE_DISC, umid, 0, 1);
    att_ptr = msgr_get_attach(&msg.msg_hdr, 0);
    ASSERT(att_ptr != NULL);
    msgr_set_dsm_attach(att_ptr, *dsm_ptr);
    ASSERT(*dsm_ptr != NULL);
  }
  else
  {
    msgr_init_hdr_attach(&msg.msg_hdr, MSGR_PROSE_DISC, umid, 0, 0);
  }
  msgr_set_hdr_inst(&msg.msg_hdr, inst_id);

  errno_enum_type status = msgr_send(&msg.msg_hdr, sizeof(msg));
  if(status != E_SUCCESS)
  {
    dsm_free_packet(dsm_ptr);
  }
  PROSE_MSG_2(PROSE_MAIN_LOW, "Sent umid 0x%x with status %d", umid, status);
  ASSERT(status == E_SUCCESS);
}


#endif /* PROSE_DISPATCHER_H */
