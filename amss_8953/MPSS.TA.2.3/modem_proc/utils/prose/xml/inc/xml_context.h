/*!
  @file
  xml_context.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/xml_context.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef XmlContext_H
#define XmlContext_H

#include "mem.h"
#include "xml_unmarshaller.h"
#include "xml_marshaller.h"

class XmlContext
{
  public:
    XmlContext() {
      m_pXMLMarshaller = 0;
      m_pXMLUnMarshaller = 0;
    }

    virtual ~XmlContext()
    {
      if (m_pXMLMarshaller)
        mem_free<XmlMarshaller>(m_pXMLMarshaller);

      if (m_pXMLUnMarshaller)
        mem_free<XmlUnMarshaller>(m_pXMLUnMarshaller);
    }

    virtual XmlMarshaller* createMarshaller() = 0;

    virtual XmlUnMarshaller* createUnMarshaller() = 0;

    virtual bool SetNamespaceBinder(XmlObject* pNameSpaceinfo)
    { return false; }

    virtual bool setNamespacePrefix(char*, char* )
    { return false; }

    virtual bool setNamespacePrefix(unsigned int, char* )
    { return false; }

  protected:
    XmlMarshaller* m_pXMLMarshaller;
    XmlUnMarshaller* m_pXMLUnMarshaller;
};

#endif /* XmlContext_H */
