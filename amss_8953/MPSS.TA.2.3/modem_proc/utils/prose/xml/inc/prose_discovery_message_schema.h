/*!
  @file
  prose_discovery_message_schema.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/prose_discovery_message_schema.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef prose_discovery_message_schema_H
#define prose_discovery_message_schema_H

#include "../../utils/inc/list.h"
#include "xml_decoder_schema.h"

class XmlElement;
class prose_discovery_message;

class prose_discovery_message_schema : public XmlDecoderSchema
{

  public:
    prose_discovery_message_schema();

    virtual ~prose_discovery_message_schema();

    int ProcessElement(char* n_strElement, char* n_strVal);

    int ProcessAttribute(char* n_strElement, char* n_strAttr, char* n_strVal);

    int Validate() { return 0; }

    XmlElement* ProcessAnyElement(char* n_strElement, char* n_strVal)
    { return NULL; }

    int ProcessState(char *n_strElement)
    { return 0; }

    void* GetSessionData();

  private:
    prose_discovery_message_schema(const prose_discovery_message_schema&);
    prose_discovery_message_schema& operator=(const prose_discovery_message_schema&);

    list<XmlElement*> code_stack_;
    prose_discovery_message *discovery_message_;
}; 

#endif /* prose_discovery_message_schema_H */

