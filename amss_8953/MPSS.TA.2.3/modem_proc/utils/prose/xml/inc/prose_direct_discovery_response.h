/*!
  @file
  prose_direct_discovery_response.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/prose_direct_discovery_response.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef prose_direct_discovery_response_H
#define prose_direct_discovery_response_H

#include "xml_list.h"
#include "xml_element.h"

class AnnounceRsp_info; 
class MonitorRsp_info;
class RejectRsp_info;
class anyExtType;

class prose_direct_discovery_response : public XmlElement
{
  private:
    char         *current_time_;
    int          max_offset_;
    list<XmlElement*> *response_announce_;
    list<XmlElement*> *response_monitor_;
    list<XmlElement*> *response_reject_;
    anyExtType   *anyExt_;
    slist        *any_;
    slist        *anyAttribute_;

    prose_direct_discovery_response(const prose_direct_discovery_response&);

    void clear_current_time();
    void clear_max_offset();
    void clear_response_announce();
    void clear_response_monitor();
    void clear_response_reject();
    void clear_anyExt();
    void clear_any();
    void clear_anyAttribute();

  public:
    prose_direct_discovery_response()
    {
      current_time_ = NULL;
      max_offset_ = 0;
      response_announce_ = NULL;
      response_monitor_ = NULL;
      response_reject_ = NULL;
      anyExt_ = NULL;
      any_ = NULL;
      anyAttribute_ = NULL;
    }

    virtual ~prose_direct_discovery_response();

    prose_direct_discovery_response& operator=(const prose_direct_discovery_response&);

    bool set_current_time(const char* current_time_val)
    {
      if (current_time_val != NULL)
      {
        current_time_ = PROSE_STRDUP(current_time_val);
      }
      return true;
    }

    char* get_current_time() const
    {
      return current_time_;
    }

    bool set_max_offset(int max_offset_val)
    {
      max_offset_ = max_offset_val;
      return true;
    }

    int get_max_offset() const
    {
      return max_offset_;
    }

    bool set_response_announce(AnnounceRsp_info *response_announce_val);

    bool set_response_announce(list<XmlElement*> *response_announce_val)
    {
      if (response_announce_val != NULL)
      {
        response_announce_ = response_announce_val;
      }
      return true;
    }

    list<XmlElement*>* get_response_announce() const
    {
      return response_announce_;
    }

    bool set_response_monitor(MonitorRsp_info *response_monitor_val);

    bool set_response_monitor(list<XmlElement*> *response_monitor_val)
    {
      if (response_monitor_val != NULL)
      {
        response_monitor_ = response_monitor_val;
      }
      return true;
    }

    list<XmlElement*>* get_response_monitor() const
    {
      return response_monitor_;
    }

    bool set_response_reject(RejectRsp_info *response_reject_val);

    bool set_response_reject(list<XmlElement*> *response_reject_val)
    {
      if (response_reject_val != NULL)
      {
        response_reject_ = response_reject_val;
      }
      return true;
    }

    list<XmlElement*>* get_response_reject() const
    {
      return response_reject_;
    }

    bool set_anyExt(anyExtType* anyExt_val)
    {
      if (anyExt_val != NULL)
      {
        anyExt_ = anyExt_val;
      }
      return true;
    }

    anyExtType* get_anyExt() const
    {
      return anyExt_;
    }

    bool set_any(slist *any_val)
    {
      any_ = any_val;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute_val)
    {
      anyAttribute_ = anyAttribute_val;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"prose_direct_discovery_response"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"prose-direct-discovery-response"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* prose_direct_discovery_response_H */

