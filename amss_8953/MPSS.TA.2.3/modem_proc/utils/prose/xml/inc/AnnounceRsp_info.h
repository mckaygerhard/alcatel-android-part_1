/*!
  @file
  AnnounceRsp_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/AnnounceRsp_info.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef AnnounceRsp_info_H
#define AnnounceRsp_info_H

#include "xml_list.h"
#include "xml_element.h"

class AnnounceRsp_info : public XmlElement
{
  private:
    int        transaction_id_;
    char       *prose_application_code_;
    int        validity_timer_t4000_;
    char       *discovery_key_;
    int        discovery_entry_id_;
    slist      *any_;
    slist      *anyAttribute_;

    void clear_transaction_id();
    void clear_prose_application_code();
    void clear_validity_timer_t4000();
    void clear_discovery_type();
    void clear_discovery_key();
    void clear_discovery_entry_id();
    void clear_any();
    void clear_anyAttribute();

  public:
    AnnounceRsp_info()
    {
      transaction_id_ = 0;
      prose_application_code_ = NULL;
      validity_timer_t4000_ = 0;
      discovery_key_ = NULL;
      discovery_entry_id_ = 0;
      any_ = NULL;
      anyAttribute_ = NULL;
    }

    virtual ~AnnounceRsp_info();

    AnnounceRsp_info(const AnnounceRsp_info&);

    AnnounceRsp_info& operator=(const AnnounceRsp_info&);

    bool set_transaction_id(int transaction_id_val)
    {
      transaction_id_ = transaction_id_val;
      return true;
    }

    int get_transaction_id() const
    {
      return transaction_id_;
    }

    bool set_prose_application_code(const char* prose_application_code_val)
    {
      if (prose_application_code_val != NULL)
      {
        prose_application_code_ = PROSE_STRDUP(prose_application_code_val);
      }
      return true;
    }

    char* get_prose_application_code() const
    {
      return prose_application_code_;
    }

    bool set_validity_timer_t4000(int validity_timer_t4000_val)
    {
      validity_timer_t4000_ = validity_timer_t4000_val;
      return true;
    }

    int get_validity_timer_t4000() const
    {
      return validity_timer_t4000_;
    }

    bool set_discovery_key(const char* discovery_key_val)
    {
      if (discovery_key_val != NULL)
      {
        discovery_key_ = PROSE_STRDUP(discovery_key_val);
      }
      return true;
    }

    char* get_discovery_key() const
    {
      return discovery_key_;
    }

    bool set_discovery_entry_id(int discovery_entry_id_val)
    {
      discovery_entry_id_ = discovery_entry_id_val;
      return true;
    }

    int get_discovery_entry_id() const
    {
      return discovery_entry_id_;
    }

    bool set_any(slist *any_val)
    {
      any_ = any_val;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute_val)
    {
      anyAttribute_ = anyAttribute_val;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"AnnounceRsp_info"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"AnnounceRsp-info"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* AnnounceRsp_info_H */

