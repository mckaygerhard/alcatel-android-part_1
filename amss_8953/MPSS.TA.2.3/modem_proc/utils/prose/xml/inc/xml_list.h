/*!
  @file
  xml_list.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/xml_list.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef XmlList_H
#define XmlList_H

struct node
{
  void*         m_pBuffer;
  node* m_pNext;
};

class slist
{
  private:

    node*  m_pHead;
    node*  m_pTail;
    unsigned short m_iListCount;

    slist(const slist&);
    slist& operator=(const slist&);

    void* get_key_at_index(unsigned short) const;

  public:
    class iterator
    {
      public:
        iterator(node* ptr) : ptr(ptr) { }
        iterator operator++() { ptr = ptr->m_pNext; return *this; }
        iterator operator++(int) { iterator i = *this; ptr = ptr->m_pNext; return i; }
        void* operator*() { return ptr->m_pBuffer; }
        //void* operator->() { return ptr->m_pBuffer; }
        bool operator==(const iterator& rhs) { return ptr == rhs.ptr; }
        bool operator!=(const iterator& rhs) { return ptr != rhs.ptr; }
      private:
        node* ptr;
    };

    class const_iterator
    {
      public:
        const_iterator(node* ptr) : ptr(ptr) { }
        const_iterator operator++() { ptr = ptr->m_pNext; return *this; }
        const_iterator operator++(int) { const_iterator i = *this; ptr = ptr->m_pNext; return i; }
        const void* operator*() { return ptr->m_pBuffer; }
        //const void* operator->() { return ptr->m_pBuffer; }
        bool operator==(const const_iterator& rhs) { return ptr == rhs.ptr; }
        bool operator!=(const const_iterator& rhs) { return ptr != rhs.ptr; }
      private:
        node* ptr;
    };

    void* operator[](unsigned short index) {
      return get_key_at_index(index);
    }

    const void* operator[](unsigned short index) const {
      return get_key_at_index(index);
    }

    iterator begin() {
      return iterator(m_pHead);
    }

    const_iterator begin() const {
      return const_iterator(m_pHead);
    }

    iterator end() {
      return iterator(m_pTail->m_pNext);
    }

    const_iterator end() const {
      return const_iterator(m_pTail->m_pNext);
    }

    bool is_last(iterator iter) {
      return (iter == m_pTail);
    }

    bool is_last(const_iterator iter) const {
      return (iter == m_pTail);
    }

    bool empty() const {
      return (m_pHead == 0);
    }

    void* top() const {
      return (m_pHead == 0) ? 0 : m_pHead->m_pBuffer;
    }

    slist();

    virtual ~slist();

    bool           append(void*);

    bool           Add(void* x) { return append(x); }

    bool           Remove(void*);

    unsigned short size() const { return m_iListCount; }

    node*          GetPtrToListHead() { return m_pHead; }

    bool           push(void*);

    void           pop();

};

#endif /* XmlList_H */

