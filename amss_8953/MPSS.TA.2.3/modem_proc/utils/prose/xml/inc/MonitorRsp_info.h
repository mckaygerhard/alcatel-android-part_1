/*!
  @file
  MatchRsp_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/MonitorRsp_info.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef MonitorRsp_info_H
#define MonitorRsp_info_H

#include "xml_list.h"
#include "xml_element.h"

class DiscFilter_info;
class anyExtType;

class MonitorRsp_info : public XmlElement
{
  private:
    int              transaction_id_;
    list<XmlElement*> *discovery_filter_;
    int              discovery_entry_id_;
    anyExtType       *anyExt_;
    slist            *any_;
    slist            *anyAttribute_;

    void clear_transaction_id();
    void clear_discovery_filter();
    void clear_discovery_entry_id();
    void clear_anyExt();
    void clear_any();
    void clear_anyAttribute();

  public:
    MonitorRsp_info()
    {
      transaction_id_ = 0;
      discovery_filter_ = 0;
      discovery_entry_id_ = 0;
      anyExt_ = NULL;
      any_ = NULL;
      anyAttribute_ = NULL;
    }

    virtual ~MonitorRsp_info();

    MonitorRsp_info(const MonitorRsp_info&);

    MonitorRsp_info& operator=(const MonitorRsp_info&);

    bool set_transaction_id(int transaction_id_val)
    {
      transaction_id_ = transaction_id_val;
      return true;
    }

    int get_transaction_id() const
    {
      return transaction_id_;
    }

    bool set_discovery_filter(DiscFilter_info *discovery_filter_val);

    bool set_discovery_filter(list<XmlElement*> *discovery_filter_val)
    {
      if (discovery_filter_val != NULL)
      {
        discovery_filter_ = discovery_filter_val;
      }
      return true;
    }

    list<XmlElement*>* get_discovery_filter() const
    {
      return discovery_filter_;
    }

    bool set_discovery_entry_id(int discovery_entry_id_val)
    {
      discovery_entry_id_ = discovery_entry_id_val;
      return true;
    }

    int get_discovery_entry_id() const
    {
      return discovery_entry_id_;
    }

    bool set_anyExt(anyExtType* anyExt_val)
    {
      if (anyExt_val != NULL)
      {
        anyExt_ = anyExt_val;
      }
      return true;
    }

    anyExtType* get_anyExt() const
    {
      return anyExt_;
    }

    bool set_any(slist *any_val)
    {
      any_ = any_val;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute_val)
    {
      anyAttribute_ = anyAttribute_val;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"MonitorRsp_info"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"MonitorRsp-info"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* MonitorRsp_info_H */

