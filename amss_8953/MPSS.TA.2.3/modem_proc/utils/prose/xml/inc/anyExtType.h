/*!
  @file
  anyExtType.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/anyExtType.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef anyExtType_H
#define anyExtType_H

#include "xml_list.h"
#include "xml_element.h"

class anyExtType : public XmlElement
{
  private:
    slist *any_; 

    anyExtType(const anyExtType&);

    void clear_any();

  public:
    anyExtType()
    {
      any_ = NULL;
    }

    virtual ~anyExtType();

    anyExtType& operator=(const anyExtType&);

    bool set_any(slist *any)
    {
      any_ = any;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"anyExtType"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)""; }
    unsigned char Valiadate() { return 0; }
};

#endif /* anyExtType_H */

