/*!
  @file
  IMSI_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/IMSI_info.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef IMSI_info_H
#define IMSI_info_H

#include "xml_list.h"
#include "xml_element.h"

class IMSI_info : public XmlElement
{
  private:
    int    mcc_;
    int    mnc_;
    int    msin_;
    slist *any_; 
    slist *anyAttribute_;

    IMSI_info(const IMSI_info&);

    void clear_mcc();
    void clear_mnc();
    void clear_msin();
    void clear_any();
    void clear_anyAttribute();

  public:
    IMSI_info()
    {
      mcc_ = 0;
      mnc_ = 0;
      msin_ = 0;
      any_ = 0;
      anyAttribute_ = 0;
    }

    virtual ~IMSI_info();

    IMSI_info& operator=(const IMSI_info&);

    bool set_mcc(int mcc_val)
    {
      mcc_ = mcc_val;
      return true;
    }

    int get_mcc() const
    {
      return mcc_;
    }

    bool set_mnc(int mnc_val)
    {
      mnc_ = mnc_val;
      return true;
    }

    int get_mnc() const
    {
      return mnc_;
    }

    bool set_msin(int msin_val)
    {
      msin_ = msin_val;
      return true;
    }

    int get_msin() const
    {
      return msin_;
    }

    bool set_any(slist *any)
    {
      any_ = any;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute)
    {
      anyAttribute_ = anyAttribute;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"IMSI_info"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"IMSI-info"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* IMSI_info_H */

