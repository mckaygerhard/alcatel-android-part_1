/*!
  @file
  AppID_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/AppID_info.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef AppID_info_H
#define AppID_info_H

#include "xml_list.h"
#include "xml_element.h"

class AppID_info : public XmlElement
{
  private:
    char  *os_id_;
    char  *os_app_id_;
    slist *any_; 
    slist *anyAttribute_;

    AppID_info(const AppID_info&);

    void clear_os_id();
    void clear_os_app_id();
    void clear_any();
    void clear_anyAttribute();

  public:
    AppID_info()
    {
      os_id_ = NULL;
      os_app_id_ = NULL;
      any_ = NULL;
      anyAttribute_ = NULL;
    }

    virtual ~AppID_info();

    AppID_info& operator=(const AppID_info&);

    bool set_os_id(const char* os_id_val)
    {
      if (os_id_val != NULL)
      {
        os_id_ = PROSE_STRDUP(os_id_val);
      }
      return true;
    }

    char* get_os_id() const
    {
      return os_id_;
    }

    bool set_os_app_id(const char* os_app_id)
    {
      if (os_app_id != NULL)
      {
        os_app_id_ = PROSE_STRDUP(os_app_id);
      }
      return true;
    }

    char* get_os_app_id() const
    {
      return os_app_id_;
    }

    bool set_any(slist *any)
    {
      any_ = any;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute)
    {
      anyAttribute_ = anyAttribute;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"AppID_info"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"AppID-info"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* AppID_info_H */

