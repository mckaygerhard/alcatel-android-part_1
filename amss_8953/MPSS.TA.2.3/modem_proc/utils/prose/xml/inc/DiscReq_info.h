/*!
  @file
  DiscReq_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/DiscReq_info.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef DiscReq_info_H
#define DiscReq_info_H

#include "xml_list.h"
#include "xml_element.h"

class IMSI_info;
class AppID_info;

class DiscReq_info : public XmlElement
{
  private:
    int        transaction_id_;
    int        command_;
    IMSI_info  *ue_identity_;
    char       *prose_application_id_;
    AppID_info *application_identity_;
    int        discovery_entry_id_;
    int        requested_timer_;
    char       *metadata_;
    slist      *any_;
    slist      *anyAttribute_;

    void clear_transaction_id();
    void clear_command();
    void clear_ue_identity();
    void clear_prose_application_id();
    void clear_application_identity();
    void clear_discovery_entry_id();
    void clear_requested_timer();
    void clear_metadata();
    void clear_any();
    void clear_anyAttribute();

  public:
    DiscReq_info()
    {
      transaction_id_ = 0;
      command_ = 0;
      ue_identity_ = NULL;
      prose_application_id_ = NULL;
      application_identity_ = NULL;
      discovery_entry_id_ = 0;
      requested_timer_ = 0;
      metadata_ = NULL;
      any_ = NULL;
      anyAttribute_ = NULL;
    }

    virtual ~DiscReq_info();

    DiscReq_info(const DiscReq_info&);
    DiscReq_info& operator=(const DiscReq_info&);

    bool set_transaction_id(int transaction_id_val)
    {
      transaction_id_ = transaction_id_val;
      return true;
    }

    int get_transaction_id() const
    {
      return transaction_id_;
    }

    bool set_command(int command_val)
    {
      command_ = command_val;
      return true;
    }

    int get_command() const
    {
      return command_;
    }

    bool set_ue_identity(IMSI_info *ue_identity_val)
    {
      if (ue_identity_val != NULL)
      {
        ue_identity_ = ue_identity_val;
      }
      return true;
    }

    IMSI_info* get_ue_identity() const
    {
      return ue_identity_;
    }

    bool set_prose_application_id(const char* prose_application_id_val)
    {
      if (prose_application_id_val != NULL)
      {
        prose_application_id_ = PROSE_STRDUP(prose_application_id_val);
      }
      return true;
    }

    char* get_prose_application_id() const
    {
      return prose_application_id_;
    }

    bool set_application_identity(AppID_info *application_identity_val)
    {
      if (application_identity_val != NULL)
      {
        application_identity_ = application_identity_val;
      }
      return true;
    }

    AppID_info* get_application_identity() const
    {
      return application_identity_;
    }

    bool set_discovery_entry_id(int discovery_entry_id_val)
    {
      discovery_entry_id_ = discovery_entry_id_val;
      return true;
    }

    int get_discovery_entry_id() const
    {
      return discovery_entry_id_;
    }

    bool set_requested_timer(int requested_timer_val)
    {
      requested_timer_ = requested_timer_val;
      return true;
    }

    int get_requested_timer() const
    {
      return requested_timer_;
    }

    bool set_metadata(const char* metadata_val)
    {
      if (metadata_val != NULL)
      {
        metadata_ = PROSE_STRDUP(metadata_val);
      }
      return true;
    }

    char* get_metadata() const
    {
      return metadata_;
    }

    bool set_any(slist *any_val)
    {
      any_ = any_val;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute_val)
    {
      anyAttribute_ = anyAttribute_val;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"DiscReq_info"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"DiscReq-info"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* DiscReq_info_H */

