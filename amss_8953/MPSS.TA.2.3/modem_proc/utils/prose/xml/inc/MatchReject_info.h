/*!
  @file
  MatchReject_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/MatchReject_info.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef MatchReject_info_H
#define MatchReject_info_H

#include "xml_list.h"
#include "xml_element.h"

class MatchReject_info : public XmlElement
{
  private:
    int        transaction_id_;
    int        pc3_control_protocol_cause_value_;
    slist      *any_;
    slist      *anyAttribute_;

    void clear_transaction_id();
    void clear_pc3_control_protocol_cause_value();
    void clear_any();
    void clear_anyAttribute();

  public:
    MatchReject_info()
    {
      transaction_id_ = 0;
      pc3_control_protocol_cause_value_ = 0;
      any_ = NULL;
      anyAttribute_ = NULL;
    }

    virtual ~MatchReject_info();

    MatchReject_info(const MatchReject_info&);
    MatchReject_info& operator=(const MatchReject_info&);

    bool set_transaction_id(int transaction_id_val)
    {
      transaction_id_ = transaction_id_val;
      return true;
    }

    int get_transaction_id() const
    {
      return transaction_id_;
    }

    bool set_pc3_control_protocol_cause_value(int pc3_control_protocol_cause_value_val)
    {
      pc3_control_protocol_cause_value_ = pc3_control_protocol_cause_value_val;
      return true;
    }

    int get_pc3_control_protocol_cause_value() const
    {
      return pc3_control_protocol_cause_value_;
    }

    bool set_any(slist *any_val)
    {
      any_ = any_val;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute_val)
    {
      anyAttribute_ = anyAttribute_val;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"MatchReject_info"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"DiscReq-info"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* MatchReject_info_H */

