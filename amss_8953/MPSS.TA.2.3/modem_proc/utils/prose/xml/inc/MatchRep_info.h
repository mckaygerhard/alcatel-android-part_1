/*!
  @file
  MatchRep_info.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/MatchRep_info.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef MatchRep_info_H
#define MatchRep_info_H

#include "xml_list.h"
#include "xml_element.h"

class IMSI_info;
class PLMN_info;

class MatchRep_info : public XmlElement
{
  private:
    int        transaction_id_;
    char       *prose_application_code_;
    IMSI_info  *ue_identity_;
    PLMN_info  *monitored_plmn_id_;
    PLMN_info  *vplmn_id_;
    char       *mic_;
    char       *utc_based_counter_;
    bool       metadata_flag_;
    slist      *any_;
    slist      *anyAttribute_;

    void clear_transaction_id();
    void clear_prose_application_code();
    void clear_ue_identity();
    void clear_monitored_plmn_id();
    void clear_vplmn_id();
    void clear_mic();
    void clear_utc_based_counter();
    void clear_metadata_flag();
    void clear_any();
    void clear_anyAttribute();

  public:
    MatchRep_info()
    {
      transaction_id_ = 0;
      prose_application_code_ = NULL;
      ue_identity_ = NULL;
      monitored_plmn_id_ = NULL;
      vplmn_id_ = NULL;
      mic_ = NULL;
      utc_based_counter_ = NULL;
      metadata_flag_ = false;
      any_ = NULL;
      anyAttribute_ = NULL;
    }

    virtual ~MatchRep_info();

    MatchRep_info(const MatchRep_info&);
    MatchRep_info& operator=(const MatchRep_info&);

    bool set_transaction_id(int transaction_id_val)
    {
      transaction_id_ = transaction_id_val;
      return true;
    }

    int get_transaction_id() const
    {
      return transaction_id_;
    }

    bool set_prose_application_code(const char* prose_application_code_val)
    {
      if (prose_application_code_val != NULL)
      {
        prose_application_code_ = PROSE_STRDUP(prose_application_code_val);
      }
      return true;
    }

    char* get_prose_application_code() const
    {
      return prose_application_code_;
    }

    bool set_ue_identity(IMSI_info *ue_identity_val)
    {
      if (ue_identity_val != NULL)
      {
        ue_identity_ = ue_identity_val;
      }
      return true;
    }

    IMSI_info* get_ue_identity() const
    {
      return ue_identity_;
    }

    bool set_monitored_plmn_id(PLMN_info *monitored_plmn_id_val)
    {
      if (monitored_plmn_id_val != NULL)
      {
        monitored_plmn_id_ = monitored_plmn_id_val;
      }
      return true;
    }

    PLMN_info* get_monitored_plmn_id() const
    {
      return monitored_plmn_id_;
    }

    bool set_vplmn_id(PLMN_info *vplmn_id_val)
    {
      if (vplmn_id_val != NULL)
      {
        vplmn_id_ = vplmn_id_val;
      }
      return true;
    }

    PLMN_info* get_vplmn_id() const
    {
      return vplmn_id_;
    }

    bool set_mic(const char* mic_val)
    {
      if (mic_val != NULL)
      {
        mic_ = PROSE_STRDUP(mic_val);
      }
      return true;
    }

    char* get_mic() const
    {
      return mic_;
    }

    bool set_utc_based_counter(const char* utc_based_counter_val)
    {
      if (utc_based_counter_val != NULL)
      {
        utc_based_counter_ = PROSE_STRDUP(utc_based_counter_val);
      }
      return true;
    }

    char* get_utc_based_counter() const
    {
      return utc_based_counter_;
    }

    bool set_metadata_flag(bool metadata_flag_val)
    {
      metadata_flag_ = metadata_flag_val;
      return true;
    }

    bool get_metadata_flag() const
    {
      return metadata_flag_;
    }

    bool set_any(slist *any_val)
    {
      any_ = any_val;
      return true;
    }

    slist* get_any() const
    {
      return any_;
    }

    bool set_anyAttribute(slist* anyAttribute_val)
    {
      anyAttribute_ = anyAttribute_val;
      return true;
    }

    slist* get_anyAttribute() const
    {
      return anyAttribute_;
    }

    bool ProcessUnMarshall(char* n_Element, char* n, XmlElement* n_Object);
    bool ProcessUnMarshallAttribute(char* n_Element, char* n, XmlAttribute* n_Object);

    char* ClassName() { return (char*)"MatchRep_info"; }
    char* ClassType() { return (char*)"XmlElement"; }
    char* TagName()   { return (char*)"DiscReq-info"; }
    unsigned char Valiadate() { return 0; }
};

#endif /* MatchRep_info_H */

