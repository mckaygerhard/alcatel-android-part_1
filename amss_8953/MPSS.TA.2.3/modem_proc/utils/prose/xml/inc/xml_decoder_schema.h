/*!
  @file
  xml_decoder_schema.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/xml_decoder_schema.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef XmlDecoderSchema_H
#define XmlDecoderSchema_H

#include "xml_common.h"

/**
 * Base class
 * Before intiating the parser, first set the parser to receive a response of type
 * based on the expected response. i.e. Use the appropriate response instance for
 * receiving the parsed data.
 */
class XmlDecoderSchema
{
  public:
    XmlDecoderSchema() {}

    virtual ~XmlDecoderSchema() {}

    /**
     * Validation function to see if you obtained all the necessary data when parsing is
     * complete
     */
    virtual int Validate() = 0;
    /**
     * Attribute parsing for tag format <tag attr="value" attr2="value2">
     */
    virtual int ProcessAttribute(char *n_strElement, char *n_strAttr, char *n_strVal) = 0;
    /**
     * Parsing a tag value of the form <tag>value</tag>
     */
    virtual int ProcessElement(char *n_strElement, char *n_strValue) = 0;
    /**
     * Change state of Parser when pushElement.
     */
    virtual int ProcessState(char *n_strElement) = 0;

    virtual void* GetSessionData() = 0;

    virtual void SetSchemaListener(void* pObj) { if (pObj) {} };
};

#endif /* XmlDecoderSchema_H */
