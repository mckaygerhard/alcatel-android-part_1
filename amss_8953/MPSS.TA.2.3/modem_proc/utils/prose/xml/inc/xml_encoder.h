/*!
  @file
  xml_encoder.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/xml/inc/xml_encoder.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef Xml_Encoder_H
#define Xml_Encoder_H

class XmlEncoder
{
  private:
    bool inited_;

    bool header_added_;

    bool from_get_xml_doc_;

    bool has_attribute_;

    char* xml_doc_;

    unsigned int xml_doc_length_;

    char* str_xml_namespace_set_;

    char* escape_string(const char* attr_value);

    inline void xml_append(const char *src);

  public:

    XmlEncoder(void);

    virtual ~XmlEncoder(void);

    void init_xml_doc(void);

    void deinit_xml_doc(void);

    void add_comment(const char* comment);

    void add_header(const char* version, 
        const char* encoding);

    void reset_header();

    void set_xml_data(const char* data);

    void set_xml_namespace(const char* namespace_name);

    void start_element(const char* element_name, 
        bool has_attribute);

    void start_element_self_ending(const char* element_name);

    void start_element_ns(const char* element_name, 
        const char* ns_name, 
        const char* ns_val, 
        bool has_attribute);

    void start_attribute(const char* attr_name, 
        const char* attr_value, 
        bool set_ns, 
        bool self_ending,
        bool end_element);

    void start_attribute_ns(const char* attr_name, 
        const char* attr_value, 
        const char* ns_name,
        bool self_ending,
        bool end_element);

    char* get_xml_doc(void);

    unsigned int get_xml_doc_length() const { return xml_doc_length_; }

    void end_xml_element(const char* element_name);

    void end_xml_element_ns(const char* element_name, 
        const char* ns_name);

    void add_namespace_prefix_to_xml(const char* attr_name, 
        const char* attr_value, 
        const char* prefix,
        bool self_ending,
        bool end_element);
};

#endif /* Xml_Encoder_H */

