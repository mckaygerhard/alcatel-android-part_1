/*!
  @file
  list.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/utils/inc/list.h#2 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef LIST_H
#define LIST_H

#include "mem.h"
#include "assert.h"

template<typename T>
class list
{
  private:
    struct node
    {
      T data_;
      node *next_;
      node() : next_(0) {}
      node(const T& data) : data_(data), next_(0) {}
    };
    node *head_;
    node *tail_;
    unsigned short count_;

    node* create_node()
    {
      return new(mem_alloc<node>()) node;
    }

    node* create_node(const T& x)
    {
      return new(mem_alloc<node>()) node(x);
    }

  public:
    list<T>() : head_(0), tail_(0), count_(0) {}

    ~list<T>() 
    {
      node *cur = 0;
      while (head_ != 0)
      {
        cur = head_;
        head_ = head_->next_;
        mem_free<node>(cur);
        cur = 0;
        --count_;
      }
      tail_ = 0;
    }

    list<T>(const list<T>& src)
      : head_(0), tail_(0), count_(0)
    { 
      count_ = src.count_;
      node *cur = src.head_;
      while (cur)
      {
        if (!head_)
        {
          head_ = create_node(cur->data_);
          ASSERT(head_);
          tail_ = head_;
        }
        else
        {
          tail_->next_ = create_node(cur->data_);
          ASSERT(tail_->next_);
          tail_ = tail_->next_;
        }
        cur = cur->next_;
      }
    }

    list<T>& operator=(const list<T>& rhs)
    {
      if (this != &rhs)
      {
        node *cur = 0;
        while (head_ != 0)
        {
          cur = head_;
          head_ = head_->next_;
          mem_free<node>(cur);
          cur = 0;
          --count_;
        }
        tail_ = 0;
        count_ = rhs.count_;
        cur = rhs.head_;
        while (cur)
        {
          if (!head_)
          {
            head_ = create_node(cur->data_);
            ASSERT(head_);
            tail_ = head_;
          }
          else
          {
            tail_->next_ = create_node(cur->data_);
            ASSERT(tail_->next_);
            tail_ = tail_->next_;
          }
          cur = cur->next_;
        }
      }
      return *this;
    }

    T& front() { return head_->data_; }
    const T& front() const { return head_->data_; }

    void push_front(const T& x)
    {
      node *cur = create_node(x);
      ASSERT(cur);
      cur->next_ = head_;
      head_ = cur;
      if (!tail_) tail_ = cur;
      ++count_;
    }

    void pop_front()
    {
      if (head_)
      {
        node *cur = head_;
        if (tail_ == head_)
          tail_ = 0;
        head_ = cur->next_;
        mem_free<node>(cur);
        cur = 0;
        --count_;
      }
    }

    void remove(const T& x)
    {
      node *cur = 0;
      while (head_ && head_->data_ == x)
      {
        cur = head_;
        if (tail_ == head_)
          tail_ = 0;
        head_ = cur->next_;
        mem_free<node>(cur);
        cur = 0;
        --count_;
      }
      if (head_)
      {
        node *pre = head_;
        cur = head_->next_;
        while (cur)
        {
          if (cur->data_ == x)
          {
            if (tail_ == cur)
              tail_ = pre;
            pre->next_ = cur->next_;
            mem_free<node>(cur);
            --count_;
            cur = pre->next_;
          }
          else
          {
            pre = cur;
            cur = cur->next_;
          }
        }
      }
    }

    void clear()
    {
      node *cur = 0;
      while (head_)
      {
        cur = head_;
        head_ = head_->next_;
        mem_free<node>(cur);
        cur = 0;
        --count_;
      }
      tail_ = 0;
    }

    class const_iterator
    {
      public:
        const_iterator() : ptr_(0) {}
        const_iterator(node* ptr) : ptr_(ptr) { }
        const_iterator operator++() { ptr_ = ptr_->next_; return *this; }
        const_iterator operator++(int) { const_iterator i = *this; ptr_ = ptr_->next_; return i; }
        const T& operator*() const { return ptr_->data_; }
        const T* operator->() const { return &(ptr_->data_); }
        bool operator==(const const_iterator& rhs) const { return ptr_ == rhs.ptr_; }
        bool operator!=(const const_iterator& rhs) const { return ptr_ != rhs.ptr_; }
      protected:
        void incr() { ptr_ = ptr_->next_; }
        node *ptr_;
        friend class list<T>;
    };

    class iterator : public const_iterator
  {
    public:
      iterator() : const_iterator() {}
      iterator(node* ptr) : const_iterator(ptr) { }
      iterator operator++() { const_iterator::incr(); return *this; }
      iterator operator++(int) { iterator i = *this; const_iterator::incr(); return i; }
      T& operator*() { return const_iterator::ptr_->data_; }
      T* operator->() { return &(const_iterator::ptr_->data_); }
  };

    // check empty() is not true before using []
    T& operator[](unsigned short index) {
      node *cur = head_;
      for (unsigned short i = 0; i < index && cur; i++)
        cur = cur->next_;
      return cur->data_;
    }

    // check empty() is not true before using []
    const T& operator[](unsigned short index) const {
      node *cur = head_;
      for (unsigned short i = 0; i < index && cur; i++)
        cur = cur->next_;
      return cur->data_;
    }

    iterator begin() {
      return iterator(head_);
    }

    const_iterator begin() const {
      return const_iterator(head_);
    }

    iterator end() {
      return tail_ ? iterator(tail_->next_) : 0;
    }

    const_iterator end() const {
      return tail_ ? const_iterator(tail_->next_) : 0;
    }

    bool is_last(iterator iter) {
      return (iter == tail_);
    }

    bool is_last(const_iterator iter) const {
      return (iter == tail_);
    }

    bool empty() const {
      return (head_ == 0);
    }

    iterator insert(iterator pos, const T& x)
    {
      node* n = create_node(x);
      ASSERT(n);
      if (!head_)
      {
        head_ = n;
        tail_ = n;
        ++count_;
      }
      else if (head_ == pos.ptr_)
      {
        n->next_ = head_;
        head_ = n;
        ++count_;
      }
      else if (tail_->next_ == pos.ptr_)
      {
        tail_->next_ = n;
        tail_ = n;
        ++count_;
      }
      else
      {
        node *cur = head_;
        while (cur && cur->next_ != pos.ptr_)
          cur = cur->next_;
        if (cur)
        {
          n->next_ = cur->next_;
          cur->next_ = n;
          ++count_;
        }
      }
      return iterator(n);
    }

    void push_back(const T& x)
    {
      insert(end(), x);
    }

    iterator erase(const_iterator pos)
    {
      node *n = pos.ptr_;
      if (head_ == n)
      {
        if (tail_ == head_)
          tail_ = tail_->next_;
        head_ = head_->next_;
        mem_free<node>(n);
        --count_;
        return iterator(head_);
      }
      else
      {
        if (tail_->next_ == pos.ptr_)
          n = tail_;
        node* cur = head_;
        while (cur && cur->next_ != n)
          cur = cur->next_;
        if (cur)
        {
          if (cur->next_ == tail_)
            tail_ = cur;
          cur->next_ = n->next_;
          mem_free<node>(n);
          --count_;
          return iterator(cur->next_);
        }
      }
      return 0;
    }

    unsigned short size() const { return count_; }
};

template<typename T>
inline bool operator==(const list<T>& a, const list<T>& b)
{
  if (a.size() != b.size())
    return false;
  for (typename list<T>::const_iterator iter = a.begin(), jter = b.begin();
      iter != a.end(); ++iter, ++jter)
    if (*iter != *jter)
      return false;
  return true;
}

#endif /* LIST_H */

