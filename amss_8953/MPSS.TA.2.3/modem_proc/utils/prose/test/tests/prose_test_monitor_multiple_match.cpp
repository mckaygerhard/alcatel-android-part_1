/*!
  @file
  prose_test_monitor_basic.cpp

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/test/tests/prose_test_monitor_multiple_match.cpp#5 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

extern "C"
{
#include "prose_ext_msg.h"
#include "dsm_item.h"
#include "dsm_init.h"
}

#include "tf_stub.h"
#include "TestFramework.h"
#include "prose_qtf.h"

#include <sstream>

static msgr_umid_type umid_list[] =
{
  LTE_RRC_PROSE_CFG_REQ,
  PROSE_DISC_SET_CONFIG_RSP,
  PROSE_DISC_BROADCAST_NOTIFICATION_IND,
  PROSE_DISC_SUBSCRIBE_RSP,
  DS_APPSRV_LTE_D_POST_START_REQ,
  LTE_MAC_FILTER_ADD_REQ,
  PROSE_DISC_MATCH_EVENT_IND,
  PROSE_DISC_SUBSCRIBE_CANCEL_RSP,
  LTE_MAC_FILTER_DEL_REQ,
  PROSE_DISC_NOTIFICATION_IND
};

static int trans_id = 0;

int initialize_monitor_session(const string& app_id, const string& pa_id)
{
  prose_disc_subscribe_req_s subscribe_req;
  prose_disc_subscribe_rsp_s *subscribe_rsp_ptr;
  lte_mac_filter_add_req_s *filter_add_req_ptr;
  ds_appsrv_lte_d_post_req_s *req_ptr;

  byte *buf_ptr;
  uint32 buf_len;
  msgr_hdr_s attach_ind;
  dsm_item_type *dsm_ptr = NULL;
  msgr_attach_struct_type *att_ptr;
  uint16 dsm_packet_len;
  ds_appsrv_lte_d_post_req_id_ind_s post_req_id;
  ds_appsrv_lte_d_post_result_ind_s post_result_ind;
  char *buffer;
  uint16 num_bytes;

  msgr_init_hdr(&subscribe_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_SUBSCRIBE_REQ);
  memcpy(subscribe_req.os_app_id.byte, app_id.c_str(), app_id.length());
  subscribe_req.os_app_id.length = app_id.length();
  memcpy(subscribe_req.pa_id.byte, pa_id.c_str(), pa_id.length());
  subscribe_req.pa_id.length = pa_id.length();
  subscribe_req.duration = 300;
  subscribe_req.disc_type = PROSE_DISC_TYPE_OPEN;
  subscribe_req.request_time = 300;
  prose_qtf_send_msg((byte*)&subscribe_req, sizeof(subscribe_req));

  TF_MSG("Checking for DS_APPSRV_LTE_D_POST_START_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(ds_appsrv_lte_d_post_req_s) == buf_len);
  req_ptr = (ds_appsrv_lte_d_post_req_s *)(void*) buf_ptr;
  TF_ASSERT(DS_APPSRV_LTE_D_POST_START_REQ == req_ptr->msg_hdr.id);
  TF_ASSERT(strcmp(req_ptr->apn_str, "TEST_APN") == 0);

  uint8 num_attach = 0;

  num_attach = msgr_get_num_attach(&req_ptr->msg_hdr);
  TF_ASSERT(num_attach == 1);
  att_ptr = msgr_get_attach((msgr_hdr_struct_type *)req_ptr, 0);
  TF_ASSERT(att_ptr != NULL);
  msgr_get_dsm_attach(att_ptr, &dsm_ptr);
  TF_ASSERT(dsm_ptr != NULL);

  dsm_packet_len = dsm_length_packet(dsm_ptr);

  ostringstream req_oss;
  req_oss << "<?xml version=\"1.0\" encoding=\"UTF-8\"?><prose-discovery-message xmlns=\"urn:3GPP:ns:ProSe:Discovery:2014\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:cp=\"urn:ietf:params:xml:ns:copycontrol\">"
    << "<DISCOVERY_REQUEST>"
    << "<discovery-request>"
    << "<transaction-ID>" << trans_id << "</transaction-ID>"
    << "<command>2</command>"
    << "<UE-identity><MCC>1</MCC><MNC>2</MNC><MSIN>1234</MSIN></UE-identity>"
    << "<ProSe-Application-ID>" << pa_id << "</ProSe-Application-ID>"
    << "<application-identity><OS-ID>00000000000000000000000000000fb7</OS-ID>"
    << "<OS-App-ID>" << app_id << "</OS-App-ID>"
    << "</application-identity>"
    << "<discovery-entry-ID>0</discovery-entry-ID>"
    << "<Requested-Timer>300</Requested-Timer>"
    << "</discovery-request>"
    << "</DISCOVERY_REQUEST>"
    << "</prose-discovery-message>";
  string xml_string(req_oss.str());

  TF_ASSERT(dsm_packet_len == xml_string.length() + 1);
  buffer = (char*)malloc(dsm_packet_len);
  TF_ASSERT(buffer);
  dsm_pullup(&dsm_ptr, buffer, dsm_packet_len);
  TF_ASSERT(dsm_ptr == NULL);
  TF_ASSERT(memcmp(buffer, xml_string.c_str(), xml_string.length()) == 0);
  free(buffer);

  prose_qtf_wait_for_done();

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_subscribe_rsp_s) == buf_len);
  subscribe_rsp_ptr = (prose_disc_subscribe_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_SUBSCRIBE_RSP == subscribe_rsp_ptr->msg_hdr.id);

  msgr_init_hdr(&post_req_id.msg_hdr, MSGR_PROSE_DISC, DS_APPSRV_LTE_D_REQ_ID_IND);
  post_req_id.request_id = 10;
  prose_qtf_send_msg((byte*)&post_req_id, sizeof(post_req_id));

  ostringstream rsp_oss;
  rsp_oss << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    << "<prose-discovery-message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"urn:3GPP:ns:ProSe:Discovery:2014\" xsi:noNamespaceSchemaLocation=\"lted_prose_discovery_schema.xsd\">"
    << "<DISCOVERY_RESPONSE>"
    << "<Current-Time>1900-06-19T19:21:14</Current-Time>"
    << "<Max-Offset>16</Max-Offset>"
    << "<response-monitor>"
    << "<transaction-ID>" << trans_id++ << "</transaction-ID>"
    << "<discovery-filter>"
    << "<filter-ID>1</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011040002001200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>ffffffffffffffffffffc003e007e00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<discovery-filter>"
    << "<filter-ID>2</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011040000101200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>ffffffffffffffffffffc0001f07e00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<discovery-filter>"
    << "<filter-ID>3</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011040000009200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>ffffffffffffffffffffc00000ffe00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<discovery-filter>"
    << "<filter-ID>4</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011040006001200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>ffffffffffffffffffffc003e007e00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<discovery-filter>"
    << "<filter-ID>5</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011040000301200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>ffffffffffffffffffffc0001f07e00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<discovery-filter>"
    << "<filter-ID>6</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011040000019200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>ffffffffffffffffffffc00000ffe00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<discovery-filter>"
    << "<filter-ID>7</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011001002001200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>fffffffffffffffffff03f03e007e00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<discovery-filter>"
    << "<filter-ID>8</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011001000101200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>fffffffffffffffffff03f001f07e00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<discovery-filter>"
    << "<filter-ID>9</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011001000009200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>fffffffffffffffffff03f0000ffe00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<discovery-filter>"
    << "<filter-ID>10</filter-ID>"
    << "<ProSe-Application-Code>e008024321123408011001006001200000000000000000</ProSe-Application-Code>"
    << "<ProSe-Application-Mask>fffffffffffffffffff03f03e007e00000000000000000</ProSe-Application-Mask>"
    << "<TTL-timer-T4002>2</TTL-timer-T4002>"
    << "</discovery-filter>"
    << "<anyExt>"
    << "<qualcomm-rel12-extensions>"
    << "<UE-log-ID>111111-8888-1222-6888-654321</UE-log-ID>"
    << "<discovery-entry-ID>90</discovery-entry-ID>"
    << "</qualcomm-rel12-extensions>"
    << "</anyExt>"
    << "</response-monitor>"
    << "</DISCOVERY_RESPONSE>"
    << "</prose-discovery-message>";
  string xml_monitor(rsp_oss.str()); 

  msgr_init_hdr_attach(&post_result_ind.msg_hdr, MSGR_PROSE_DISC, DS_APPSRV_LTE_D_POST_RESULT_IND, 0, 1);
  msgr_set_hdr_inst(&post_result_ind.msg_hdr, 1);

  dsm_ptr = NULL;
  att_ptr = NULL;

  num_bytes = dsm_pushdown(&dsm_ptr, (void *)xml_monitor.c_str(), xml_monitor.length(), DSM_DS_LARGE_ITEM_POOL);
  TF_ASSERT(num_bytes == xml_monitor.length());
  att_ptr = msgr_get_attach(&post_result_ind.msg_hdr, 0);
  TF_ASSERT(att_ptr != NULL);
  msgr_set_dsm_attach(att_ptr, dsm_ptr);
  TF_ASSERT(dsm_ptr != NULL);

  post_result_ind.err_code = DS_APPSRV_LTE_D_ERR_NONE;
  post_result_ind.http_status_code = 200;
  post_result_ind.req_id = 10;
  prose_qtf_send_msg((byte*)&post_result_ind, sizeof(post_result_ind));

  TF_MSG("Checking for LTE_MAC_FILTER_ADD_REQ");

  uint32 session_id = 0;

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_filter_add_req_s) == buf_len);
  filter_add_req_ptr = (lte_mac_filter_add_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_FILTER_ADD_REQ == filter_add_req_ptr->msg_hdr.id);
  session_id = filter_add_req_ptr->session_id;
  TF_ASSERT(filter_add_req_ptr->num_active_filter == 3);

  TF_MSG("Checking for LTE_MAC_FILTER_ADD_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_filter_add_req_s) == buf_len);
  filter_add_req_ptr = (lte_mac_filter_add_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_FILTER_ADD_REQ == filter_add_req_ptr->msg_hdr.id);
  TF_ASSERT(filter_add_req_ptr->session_id == session_id);
  TF_ASSERT(filter_add_req_ptr->num_active_filter == 3);

  TF_MSG("Checking for LTE_MAC_FILTER_ADD_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_filter_add_req_s) == buf_len);
  filter_add_req_ptr = (lte_mac_filter_add_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_FILTER_ADD_REQ == filter_add_req_ptr->msg_hdr.id);
  TF_ASSERT(filter_add_req_ptr->session_id == session_id);
  TF_ASSERT(filter_add_req_ptr->num_active_filter == 3);

  TF_MSG("Checking for LTE_MAC_FILTER_ADD_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_filter_add_req_s) == buf_len);
  filter_add_req_ptr = (lte_mac_filter_add_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_FILTER_ADD_REQ == filter_add_req_ptr->msg_hdr.id);
  TF_ASSERT(filter_add_req_ptr->session_id == session_id);
  TF_ASSERT(filter_add_req_ptr->num_active_filter == 1);

  lte_mac_filter_add_cnf_s filter_add_cnf;
  msgr_init_hdr(&filter_add_cnf.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_FILTER_ADD_CNF);
  filter_add_cnf.session_id = session_id;
  filter_add_cnf.active_num = 3;
  filter_add_cnf.mac_handler_id_arr[0] = 1;
  filter_add_cnf.mac_handler_id_arr[1] = 2;
  filter_add_cnf.mac_handler_id_arr[2] = 3;
  prose_qtf_send_msg((byte*)&filter_add_cnf, sizeof(filter_add_cnf));

  msgr_init_hdr(&filter_add_cnf.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_FILTER_ADD_CNF);
  filter_add_cnf.session_id = session_id;
  filter_add_cnf.active_num = 3;
  filter_add_cnf.mac_handler_id_arr[0] = 4;
  filter_add_cnf.mac_handler_id_arr[1] = 5;
  filter_add_cnf.mac_handler_id_arr[2] = 6;
  prose_qtf_send_msg((byte*)&filter_add_cnf, sizeof(filter_add_cnf));

  msgr_init_hdr(&filter_add_cnf.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_FILTER_ADD_CNF);
  filter_add_cnf.session_id = session_id;
  filter_add_cnf.active_num = 3;
  filter_add_cnf.mac_handler_id_arr[0] = 7;
  filter_add_cnf.mac_handler_id_arr[1] = 8;
  filter_add_cnf.mac_handler_id_arr[2] = 9;
  prose_qtf_send_msg((byte*)&filter_add_cnf, sizeof(filter_add_cnf));

  msgr_init_hdr(&filter_add_cnf.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_FILTER_ADD_CNF);
  filter_add_cnf.session_id = session_id;
  filter_add_cnf.active_num = 1;
  filter_add_cnf.mac_handler_id_arr[0] = 10;
  prose_qtf_send_msg((byte*)&filter_add_cnf, sizeof(filter_add_cnf));

  return session_id;
}

void get_match_information(uint8 *pac)
{
  byte *buf_ptr;
  uint32 buf_len;

  uint8 num_attach = 0;
  ds_appsrv_lte_d_post_req_s *req_ptr;
  ds_appsrv_lte_d_post_req_id_ind_s post_req_id;
  ds_appsrv_lte_d_post_result_ind_s post_result_ind;
  lte_mac_pac_match_rpt_ind_s match_rpt_ind;

  dsm_item_type *dsm_ptr = NULL;
  msgr_attach_struct_type *att_ptr;
  uint16 dsm_packet_len;
  char *buffer;
  uint16 num_bytes;

  TF_MSG("Checking for DS_APPSRV_LTE_D_POST_START_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(ds_appsrv_lte_d_post_req_s) == buf_len);
  req_ptr = (ds_appsrv_lte_d_post_req_s *)(void*) buf_ptr;
  TF_ASSERT(DS_APPSRV_LTE_D_POST_START_REQ == req_ptr->msg_hdr.id);
  TF_ASSERT(strcmp(req_ptr->apn_str, "TEST_APN") == 0);

  num_attach = msgr_get_num_attach(&req_ptr->msg_hdr);
  TF_ASSERT(num_attach == 1);
  att_ptr = msgr_get_attach((msgr_hdr_struct_type *)req_ptr, 0);
  TF_ASSERT(att_ptr != NULL);
  msgr_get_dsm_attach(att_ptr, &dsm_ptr);
  TF_ASSERT(dsm_ptr != NULL);

  dsm_packet_len = dsm_length_packet(dsm_ptr);

  ostringstream req_oss;
  req_oss << "<?xml version=\"1.0\" encoding=\"UTF-8\"?><prose-discovery-message xmlns=\"urn:3GPP:ns:ProSe:Discovery:2014\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:cp=\"urn:ietf:params:xml:ns:copycontrol\">"
    << "<MATCH_REPORT>"
    << "<match-report>"
    << "<transaction-ID>" << trans_id << "</transaction-ID>"
    << "<ProSe-Application-Code>e008024321123408011040000101200000000000000000</ProSe-Application-Code>"
    << "<UE-identity><MCC>1</MCC><MNC>2</MNC><MSIN>1234</MSIN></UE-identity>"
    << "<Monitored-PLMN-ID><mcc>1</mcc><mnc>1</mnc></Monitored-PLMN-ID>"
    << "<VPLMN-ID><mcc>1</mcc><mnc>1</mnc></VPLMN-ID>"
    << "<MIC>7b000000</MIC>"
    << "<UTC-based-counter>00001a8c</UTC-based-counter>"
    << "<Metadata-flag>true</Metadata-flag>"
    << "</match-report>"
    << "</MATCH_REPORT>"
    << "</prose-discovery-message>";
  string match_rpt(req_oss.str()); 

  TF_ASSERT(dsm_packet_len == match_rpt.length() + 1);
  buffer = (char*)malloc(dsm_packet_len);
  TF_ASSERT(buffer);
  dsm_pullup(&dsm_ptr, buffer, dsm_packet_len);
  TF_ASSERT(dsm_ptr == NULL);
  TF_ASSERT(memcmp(buffer, match_rpt.c_str(), match_rpt.length()) == 0);
  free(buffer);

  prose_qtf_wait_for_done();

  // send the same match report while waiting for match_report_ack
  msgr_init_hdr(&match_rpt_ind.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_MATCH_RPT_IND);
  match_rpt_ind.pac_match_rpt.active_handler_id_num = 1;
  match_rpt_ind.pac_match_rpt.mac_handler_id_arr[0] = 2;
  memcpy(match_rpt_ind.pac_match_rpt.pac, pac, LTE_MAC_PAC_CODE_LEN);
  match_rpt_ind.pac_match_rpt.mic = 123;
  match_rpt_ind.pac_match_rpt.utc_time = 6789;
  match_rpt_ind.pac_match_rpt.freq = 300;
  prose_qtf_send_msg((byte*)&match_rpt_ind, sizeof(match_rpt_ind));

  msgr_init_hdr(&post_req_id.msg_hdr, MSGR_PROSE_DISC, DS_APPSRV_LTE_D_REQ_ID_IND);
  post_req_id.request_id = 11;
  prose_qtf_send_msg((byte*)&post_req_id, sizeof(post_req_id));

  prose_qtf_wait_for_done();

  msgr_init_hdr_attach(&post_result_ind.msg_hdr, MSGR_PROSE_DISC, DS_APPSRV_LTE_D_POST_RESULT_IND, 0, 1);
  msgr_set_hdr_inst(&post_result_ind.msg_hdr, 1);

  dsm_ptr = NULL;
  att_ptr = NULL;

  ostringstream rsp_oss;
  rsp_oss << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    << "<prose-discovery-message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"urn:3GPP:ns:ProSe:Discovery:2014\" xsi:noNamespaceSchemaLocation=\"lted_prose_discovery_schema.xsd\">"
    << "<MATCH_REPORT_ACK>"
    << "<Current-Time>1900-06-19T19:21:14</Current-Time>"
    << "<match-ack match-report-refresh-timer-T4006=\"1\">"
    << "<transaction-ID>" << trans_id++ << "</transaction-ID>"
    << "<ProSe-Application-ID>MATCHED_PA_ID</ProSe-Application-ID>"
    << "<validity-timer-T4004>5</validity-timer-T4004>"
    << "<metadata>ABCD</metadata>"
    << "</match-ack>"
    << "</MATCH_REPORT_ACK>"
    << "</prose-discovery-message>";
  string match_ack(rsp_oss.str());

  num_bytes = dsm_pushdown(&dsm_ptr, (void *)match_ack.c_str(), match_ack.length() + 1, DSM_DS_LARGE_ITEM_POOL);
  TF_ASSERT(num_bytes == match_ack.length() + 1);
  att_ptr = msgr_get_attach(&post_result_ind.msg_hdr, 0);
  TF_ASSERT(att_ptr != NULL);
  msgr_set_dsm_attach(att_ptr, dsm_ptr);
  TF_ASSERT(dsm_ptr != NULL);

  post_result_ind.err_code = DS_APPSRV_LTE_D_ERR_NONE;
  post_result_ind.http_status_code = 200;
  post_result_ind.req_id = 11;
  prose_qtf_send_msg((byte*)&post_result_ind, sizeof(post_result_ind));

  prose_qtf_wait_for_done();
}

void verify_match_event(const string& app_id, const string& pa_id, bool end_event)
{
  byte *buf_ptr;
  uint32 buf_len;

  TF_MSG("Checking for PROSE_DISC_MATCH_EVENT_IND");

  prose_disc_match_event_ind_s *match_event_ind_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_match_event_ind_s) == buf_len);
  match_event_ind_ptr = (prose_disc_match_event_ind_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_MATCH_EVENT_IND == match_event_ind_ptr->msg_hdr.id);
  TF_ASSERT(memcmp(match_event_ind_ptr->os_app_id.byte, app_id.c_str(), app_id.length()) == 0);
  TF_ASSERT(memcmp(match_event_ind_ptr->pa_id.byte, pa_id.c_str(), pa_id.length()) == 0);
  TF_ASSERT(match_event_ind_ptr->match_event_status == (end_event ? TRUE : FALSE));
  TF_ASSERT(memcmp(match_event_ind_ptr->matched_pa_id.byte, "MATCHED_PA_ID", 13) == 0);
}

void finalize_monitor_session(const string& app_id, const string& pa_id, bool is_last_session)
{
  byte *buf_ptr;
  uint32 buf_len;

  prose_disc_subscribe_cancel_req_s cancel_req;
  msgr_init_hdr(&cancel_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_SUBSCRIBE_CANCEL_REQ);
  memcpy(cancel_req.os_app_id.byte, app_id.c_str(), app_id.length());
  cancel_req.os_app_id.length = app_id.length();
  memcpy(cancel_req.pa_id.byte, pa_id.c_str(), pa_id.length());
  cancel_req.pa_id.length = pa_id.length();
  prose_qtf_send_msg((byte*)&cancel_req, sizeof(cancel_req));

  prose_qtf_wait_for_done();

  TF_MSG("Checking for PROSE_DISC_SUBSCRIBE_CANCEL_RSP");

  prose_disc_subscribe_cancel_rsp_s *subscribe_cancel_rsp_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_subscribe_cancel_rsp_s) == buf_len);
  subscribe_cancel_rsp_ptr = (prose_disc_subscribe_cancel_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_SUBSCRIBE_CANCEL_RSP == subscribe_cancel_rsp_ptr->msg_hdr.id);

  if (is_last_session)
  {
    TF_MSG("Checking for LTE_MAC_FILTER_DEL_REQ");

    lte_mac_filter_del_req_s *filter_del_req_ptr;
    prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
    TF_ASSERT(sizeof(lte_mac_filter_del_req_s) == buf_len);
    filter_del_req_ptr = (lte_mac_filter_del_req_s *)(void*) buf_ptr;
    TF_ASSERT(LTE_MAC_FILTER_DEL_REQ == filter_del_req_ptr->msg_hdr.id);
    TF_ASSERT(filter_del_req_ptr->active_num == 5);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[0] == 1);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[1] == 2);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[2] == 3);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[3] == 4);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[4] == 5);

    prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
    TF_ASSERT(sizeof(lte_mac_filter_del_req_s) == buf_len);
    filter_del_req_ptr = (lte_mac_filter_del_req_s *)(void*) buf_ptr;
    TF_ASSERT(LTE_MAC_FILTER_DEL_REQ == filter_del_req_ptr->msg_hdr.id);
    TF_ASSERT(filter_del_req_ptr->active_num == 5);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[0] == 6);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[1] == 7);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[2] == 8);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[3] == 9);
    TF_ASSERT(filter_del_req_ptr->mac_handler_id_arr[4] == 10);
  }
#if 0
  else
  {
    // the match start indication was not sent for the second session.
    // so we don't receive the match end indication.
    verify_match_event(app_id, pa_id, true);
  }
#endif

  TF_MSG("Checking for PROSE_DISC_NOTIFICATION_IND");

  prose_disc_notification_ind_s *notification_ind_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_notification_ind_s) == buf_len);
  notification_ind_ptr = (prose_disc_notification_ind_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_NOTIFICATION_IND == notification_ind_ptr->msg_hdr.id);
  TF_ASSERT(memcmp(notification_ind_ptr->os_app_id.byte, app_id.c_str(), app_id.length()) == 0);
  TF_ASSERT(memcmp(notification_ind_ptr->pa_id.byte, pa_id.c_str(), pa_id.length()) == 0);
  TF_ASSERT(notification_ind_ptr->result == PROSE_QMI_RESULT_ABORTED);
}

TF_DEFINE_TEST_CASE(COMPONENT, ProseTestMonitorMultipleMatch);

void ProseTestMonitorMultipleMatch::Setup()
{
  prose_qtf_setup(umid_list, sizeof(umid_list)/sizeof(umid_list[0]));  
}

void ProseTestMonitorMultipleMatch::Test()
{
  byte *buf_ptr;
  uint32 buf_len;

  prose_disc_set_config_req_s config_req;
  lte_rrc_prose_cfg_req_s *cfg_req_ptr;
  prose_disc_set_config_rsp_s *cfg_rsp_ptr;
  prose_disc_match_event_ind_s *match_event_ind_ptr;
  msgr_hdr_s attach_ind;
  lte_mac_pac_match_rpt_ind_s match_rpt_ind;

  TF_MSG("Start of test");

  sys_plmn_id_s_type plmn1 = { { 0x00, 0xF1, 0x10 } };
  lte_rrc_plmn_s camped_plmn = { 0x00, 0x00, 0x01, 0x02, 0x00, 0x01, 0xFF };

  msgr_init_hdr(&config_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_SET_CONFIG_REQ);
  config_req.announcing_policy_list_size = 1;
  config_req.announcing_policy_list[0].plmn = plmn1;
  config_req.announcing_policy_list[0].t4005 = 500;
  config_req.monitoring_policy_list_size = 1;
  config_req.monitoring_policy_list[0].plmn = plmn1;
  config_req.monitoring_policy_list[0].t4005 = 500;
  memcpy(config_req.dedicated_apn_name, "TEST_APN", 9);
  memset(&config_req.os_id, 0, sizeof(config_req.os_id));
  *(uint16*)(&config_req.os_id.byte) = 4023;
  prose_qtf_send_msg((byte*)&config_req, sizeof(config_req));

  TF_MSG("Checking for LTE_RRC_PROSE_CFG_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_rrc_prose_cfg_req_s) == buf_len);
  cfg_req_ptr = (lte_rrc_prose_cfg_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_RRC_PROSE_CFG_REQ == cfg_req_ptr->msg_hdr.id);
  
  TF_MSG("Checking for PROSE_DISC_SET_CONFIG_RSP");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_set_config_rsp_s) == buf_len);
  cfg_rsp_ptr = (prose_disc_set_config_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_SET_CONFIG_RSP == cfg_rsp_ptr->msg_hdr.id);
  
  lte_rrc_lted_available_ind_s lted_avail;
  msgr_init_hdr(&lted_avail.msg_hdr, MSGR_PROSE_DISC, LTE_RRC_LTED_AVAILABLE_IND);
  lted_avail.tx_enabled = TRUE;
  lted_avail.rx_enabled = TRUE;
  lted_avail.tx_on_serving = TRUE;
  lted_avail.camped_plmn = camped_plmn;
  prose_qtf_send_msg((byte*)&lted_avail, sizeof(lted_avail));

  msgr_init_hdr(&attach_ind, MSGR_PROSE_DISC, DS_APPSRV_LTE_ATTACHED_IND);
  prose_qtf_send_msg((byte*)&attach_ind, sizeof(attach_ind));

  TF_MSG("Checking for PROSE_DISC_BROADCAST_NOTIFICATION_IND");

  prose_disc_broadcast_notification_ind_s *notif_ind_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_broadcast_notification_ind_s) == buf_len);
  notif_ind_ptr = (prose_disc_broadcast_notification_ind_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_BROADCAST_NOTIFICATION_IND == notif_ind_ptr->msg_hdr.id);
  TF_ASSERT(notif_ind_ptr->publish_allowed == TRUE);
  TF_ASSERT(notif_ind_ptr->subscribe_allowed == TRUE);

  //--------------------------------------------------------------------------

  int session1 = initialize_monitor_session("TEST_APP_01", "mcc001.mnc001.ProSeApp.TEST_PA_01");
  int session2 = initialize_monitor_session("TEST_APP_02", "mcc001.mnc001.ProSeApp.TEST_PA_02");

  uint8 pac[] = {0xe0,0x08,0x02,0x43,0x21,0x12,0x34,0x08,0x01,0x10,0x40,0x00,0x01,0x01,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

  msgr_init_hdr(&match_rpt_ind.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_MATCH_RPT_IND);
  match_rpt_ind.pac_match_rpt.active_handler_id_num = 1;
  match_rpt_ind.pac_match_rpt.mac_handler_id_arr[0] = 2;
  memcpy(match_rpt_ind.pac_match_rpt.pac, pac, LTE_MAC_PAC_CODE_LEN);
  match_rpt_ind.pac_match_rpt.mic = 123;
  match_rpt_ind.pac_match_rpt.utc_time = 6789;
  match_rpt_ind.pac_match_rpt.freq = 300;
  prose_qtf_send_msg((byte*)&match_rpt_ind, sizeof(match_rpt_ind));

  get_match_information(pac);

  verify_match_event("TEST_APP_01", "mcc001.mnc001.ProSeApp.TEST_PA_01", false);

  TF_SLEEP(5000);

  msgr_init_hdr(&match_rpt_ind.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_MATCH_RPT_IND);
  match_rpt_ind.pac_match_rpt.active_handler_id_num = 1;
  match_rpt_ind.pac_match_rpt.mac_handler_id_arr[0] = 2;
  memcpy(match_rpt_ind.pac_match_rpt.pac, pac, LTE_MAC_PAC_CODE_LEN);
  match_rpt_ind.pac_match_rpt.mic = 123;
  match_rpt_ind.pac_match_rpt.utc_time = 6789;
  match_rpt_ind.pac_match_rpt.freq = 300;
  prose_qtf_send_msg((byte*)&match_rpt_ind, sizeof(match_rpt_ind));
  
  prose_qtf_wait_for_done();

  verify_match_event("TEST_APP_02", "mcc001.mnc001.ProSeApp.TEST_PA_02", false);

  TF_SLEEP(30000);

  verify_match_event("TEST_APP_01", "mcc001.mnc001.ProSeApp.TEST_PA_01", true);

  TF_SLEEP(5000);

  verify_match_event("TEST_APP_02", "mcc001.mnc001.ProSeApp.TEST_PA_02", true);

  msgr_init_hdr(&match_rpt_ind.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_MATCH_RPT_IND);
  match_rpt_ind.pac_match_rpt.active_handler_id_num = 1;
  match_rpt_ind.pac_match_rpt.mac_handler_id_arr[0] = 2;
  memcpy(match_rpt_ind.pac_match_rpt.pac, pac, LTE_MAC_PAC_CODE_LEN);
  match_rpt_ind.pac_match_rpt.mic = 123;
  match_rpt_ind.pac_match_rpt.utc_time = 6789;
  match_rpt_ind.pac_match_rpt.freq = 300;
  prose_qtf_send_msg((byte*)&match_rpt_ind, sizeof(match_rpt_ind));
  
  prose_qtf_wait_for_done();
  
  verify_match_event("TEST_APP_01", "mcc001.mnc001.ProSeApp.TEST_PA_01", false);
  verify_match_event("TEST_APP_02", "mcc001.mnc001.ProSeApp.TEST_PA_02", false);

  TF_SLEEP(19000);

  verify_match_event("TEST_APP_01", "mcc001.mnc001.ProSeApp.TEST_PA_01", true);
  verify_match_event("TEST_APP_02", "mcc001.mnc001.ProSeApp.TEST_PA_02", true);

  TF_SLEEP(1000);

  msgr_init_hdr(&match_rpt_ind.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_MATCH_RPT_IND);
  match_rpt_ind.pac_match_rpt.active_handler_id_num = 1;
  match_rpt_ind.pac_match_rpt.mac_handler_id_arr[0] = 2;
  memcpy(match_rpt_ind.pac_match_rpt.pac, pac, LTE_MAC_PAC_CODE_LEN);
  match_rpt_ind.pac_match_rpt.mic = 123;
  match_rpt_ind.pac_match_rpt.utc_time = 6789;
  match_rpt_ind.pac_match_rpt.freq = 300;
  prose_qtf_send_msg((byte*)&match_rpt_ind, sizeof(match_rpt_ind));

  get_match_information(pac);

  verify_match_event("TEST_APP_01", "mcc001.mnc001.ProSeApp.TEST_PA_01", false);
  // verify_match_event("TEST_APP_02", "mcc001.mnc001.ProSeApp.TEST_PA_02", false);

  finalize_monitor_session("TEST_APP_01", "mcc001.mnc001.ProSeApp.TEST_PA_01", false);
  finalize_monitor_session("TEST_APP_02", "mcc001.mnc001.ProSeApp.TEST_PA_02", true);

  prose_qtf_check_no_more_msgs();
}

void ProseTestMonitorMultipleMatch::Teardown()
{
  prose_qtf_teardown();
}

