/*!
  @file
  prose_test_announce_timeout.cpp

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/test/tests/prose_test_announce_timeout.cpp#6 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

extern "C"
{
#include "prose_ext_msg.h"
#include "dsm_item.h"
#include "dsm_init.h"
}

#include "tf_stub.h"
#include "TestFramework.h"
#include "prose_qtf.h"

static msgr_umid_type umid_list[] =
{
  LTE_RRC_PROSE_CFG_REQ,
  PROSE_DISC_SET_CONFIG_RSP,
  PROSE_DISC_BROADCAST_NOTIFICATION_IND,
  PROSE_DISC_PUBLISH_RSP,
  DS_APPSRV_LTE_D_POST_START_REQ,
  LTE_MAC_PAC_ADD_REQ,
  PROSE_DISC_PUBLISH_CANCEL_RSP,
  LTE_MAC_PAC_DEL_REQ,
  PROSE_DISC_NOTIFICATION_IND
};

TF_DEFINE_TEST_CASE(COMPONENT, ProseTestAnnounceTimeout);


void ProseTestAnnounceTimeout::Setup()
{
  prose_qtf_setup(umid_list, sizeof(umid_list)/sizeof(umid_list[0]));  
}

void ProseTestAnnounceTimeout::Test()
{
  byte *buf_ptr;
  uint32 buf_len;

  prose_disc_set_config_req_s config_req;
  msgr_hdr_s attach_ind;
  prose_disc_publish_req_s publish_req;
  ds_appsrv_lte_d_post_req_s *req_ptr;
  prose_disc_publish_rsp_s *publish_rsp_ptr;
  lte_rrc_prose_cfg_req_s *cfg_req_ptr;
  prose_disc_set_config_rsp_s *cfg_rsp_ptr;

  dsm_item_type *dsm_ptr = NULL;
  msgr_attach_struct_type *att_ptr;

  TF_MSG("Start of test");

  sys_plmn_id_s_type plmn1 = { { 0x00, 0xF1, 0x10 } };

  msgr_init_hdr(&config_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_SET_CONFIG_REQ);
  config_req.announcing_policy_list_size = 1;
  config_req.announcing_policy_list[0].plmn = plmn1;
  config_req.announcing_policy_list[0].t4005 = 500;
  config_req.monitoring_policy_list_size = 1;
  config_req.monitoring_policy_list[0].plmn = plmn1;
  config_req.monitoring_policy_list[0].t4005 = 500;
  memcpy(config_req.dedicated_apn_name, "TEST_APN", 9);
  memset(&config_req.os_id, 0, sizeof(config_req.os_id));
  *(uint16*)(&config_req.os_id.byte) = 4023;
  prose_qtf_send_msg((byte*)&config_req, sizeof(config_req));

  TF_MSG("Checking for LTE_RRC_PROSE_CFG_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_rrc_prose_cfg_req_s) == buf_len);
  cfg_req_ptr = (lte_rrc_prose_cfg_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_RRC_PROSE_CFG_REQ == cfg_req_ptr->msg_hdr.id);
  
  TF_MSG("Checking for PROSE_DISC_SET_CONFIG_RSP");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_set_config_rsp_s) == buf_len);
  cfg_rsp_ptr = (prose_disc_set_config_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_SET_CONFIG_RSP == cfg_rsp_ptr->msg_hdr.id);
  
  msgr_init_hdr(&attach_ind, MSGR_PROSE_DISC, DS_APPSRV_LTE_ATTACHED_IND);
  prose_qtf_send_msg((byte*)&attach_ind, sizeof(attach_ind));

  lte_rrc_lted_available_ind_s lted_avail;
  msgr_init_hdr(&lted_avail.msg_hdr, MSGR_PROSE_DISC, LTE_RRC_LTED_AVAILABLE_IND);
  lted_avail.tx_enabled = TRUE;
  lted_avail.rx_enabled = TRUE;
  lted_avail.tx_on_serving = TRUE;
  prose_qtf_send_msg((byte*)&lted_avail, sizeof(lted_avail));

  TF_MSG("Checking for PROSE_DISC_BROADCAST_NOTIFICATION_IND");

  prose_disc_broadcast_notification_ind_s *notif_ind_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_broadcast_notification_ind_s) == buf_len);
  notif_ind_ptr = (prose_disc_broadcast_notification_ind_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_BROADCAST_NOTIFICATION_IND == notif_ind_ptr->msg_hdr.id);
  TF_ASSERT(notif_ind_ptr->publish_allowed == TRUE);
  TF_ASSERT(notif_ind_ptr->subscribe_allowed == TRUE);

  //--------------------------------------------------------------------------

  msgr_init_hdr(&publish_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_PUBLISH_REQ);
  memcpy(publish_req.os_app_id.byte, "TEST_APP_ID", 12);
  publish_req.os_app_id.length = 11;
  memcpy(publish_req.pa_id.byte, "mcc001.mnc001.ProSeApp.TEST_PA_ID", 34);
  publish_req.pa_id.length = 33;
  publish_req.duration = 300;
  publish_req.disc_type = PROSE_DISC_TYPE_OPEN;
  publish_req.request_time = 300;
  prose_qtf_send_msg((byte*)&publish_req, sizeof(publish_req));

  TF_MSG("Checking for DS_APPSRV_LTE_D_POST_START_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(ds_appsrv_lte_d_post_req_s) == buf_len);
  req_ptr = (ds_appsrv_lte_d_post_req_s *)(void*) buf_ptr;
  TF_ASSERT(DS_APPSRV_LTE_D_POST_START_REQ == req_ptr->msg_hdr.id);
  TF_ASSERT(strcmp(req_ptr->apn_str, "TEST_APN") == 0);

  uint8 num_attach = 0;

  num_attach = msgr_get_num_attach(&req_ptr->msg_hdr);
  TF_ASSERT(num_attach == 1);
  att_ptr = msgr_get_attach((msgr_hdr_struct_type *)req_ptr, 0);
  TF_ASSERT(att_ptr != NULL);
  msgr_get_dsm_attach(att_ptr, &dsm_ptr);
  TF_ASSERT(dsm_ptr != NULL);

  uint16 dsm_packet_len = dsm_length_packet(dsm_ptr);

    char xml_string[] = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><prose-discovery-message xmlns=\"urn:3GPP:ns:ProSe:Discovery:2014\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:cp=\"urn:ietf:params:xml:ns:copycontrol\"><DISCOVERY_REQUEST><discovery-request><transaction-ID>0</transaction-ID><command>1</command><UE-identity><MCC>1</MCC><MNC>2</MNC><MSIN>1234</MSIN></UE-identity><ProSe-Application-ID>mcc001.mnc001.ProSeApp.TEST_PA_ID</ProSe-Application-ID><application-identity><OS-ID>00000000000000000000000000000fb7</OS-ID><OS-App-ID>TEST_APP_ID</OS-App-ID></application-identity><discovery-entry-ID>0</discovery-entry-ID><Requested-Timer>300</Requested-Timer></discovery-request></DISCOVERY_REQUEST></prose-discovery-message>";

  TF_ASSERT(dsm_packet_len == strlen(xml_string)+1);
  char *buffer = (char*)malloc(dsm_packet_len);
  TF_ASSERT(buffer);
  dsm_pullup(&dsm_ptr, buffer, dsm_packet_len);
  TF_ASSERT(dsm_ptr == NULL);
  TF_ASSERT(memcmp(buffer, xml_string, strlen(xml_string)) == 0);
  free(buffer);

  prose_qtf_wait_for_done();

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_publish_rsp_s) == buf_len);
  publish_rsp_ptr = (prose_disc_publish_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_PUBLISH_RSP == publish_rsp_ptr->msg_hdr.id);

  ds_appsrv_lte_d_post_req_id_ind_s post_req_id;
  msgr_init_hdr(&post_req_id.msg_hdr, MSGR_PROSE_DISC, DS_APPSRV_LTE_D_REQ_ID_IND);
  post_req_id.request_id = 10;
  prose_qtf_send_msg((byte*)&post_req_id, sizeof(post_req_id));

  char xml_announce[] = 
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
    <prose-discovery-message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"urn:3GPP:ns:ProSe:Discovery:2014\" xsi:noNamespaceSchemaLocation=\"lted_prose_discovery_schema.xsd\">\
    <DISCOVERY_RESPONSE>\
    <Current-Time>1900-06-19T19:21:14</Current-Time>\
    <Max-Offset>16</Max-Offset>\
    <response-announce>\
    <transaction-ID>0</transaction-ID>\
    <ProSe-Application-Code>e008024321123408011042002201200000e00000000000</ProSe-Application-Code>\
    <validity-timer-T4000>2</validity-timer-T4000>\
    <discovery-type>65</discovery-type>\
    <discovery-key>00112233445566778899aabbccddeeff</discovery-key>\
    <discovery-entry-ID>89</discovery-entry-ID>\
    </response-announce>\
    </DISCOVERY_RESPONSE>\
    </prose-discovery-message>";

  /*
    <anyExt>\
    <qualcomm-rel12-extensions>\
    <UE-log-ID>abcd12-8888-1222-6888-654321</UE-log-ID>\
    <discovery-entry-ID>89</discovery-entry-ID>\
    </qualcomm-rel12-extensions>\
    </anyExt>\
*/

    /*
    <response-announce>\
    <transaction-ID>4</transaction-ID>\
    <ProSe-Application-Code>e008024321123408011040004001200001000000000000</ProSe-Application-Code>\
    <validity-timer-T4000>2</validity-timer-T4000>\
    <discovery-type>65</discovery-type>\
    <discovery-key>00112233445566778899aabbccddeeff</discovery-key>\
    <anyExt>\
    <qualcomm-rel12-extensions>\
    <discovery-entry-ID>91</discovery-entry-ID>\
    </qualcomm-rel12-extensions>\
    </anyExt>\
    </response-announce>\
    */
  
  ds_appsrv_lte_d_post_result_ind_s post_result_ind;
  msgr_init_hdr_attach(&post_result_ind.msg_hdr, MSGR_PROSE_DISC, DS_APPSRV_LTE_D_POST_RESULT_IND, 0, 1);
  msgr_set_hdr_inst(&post_result_ind.msg_hdr, 1);

  dsm_ptr = NULL;
  att_ptr = NULL;

  uint16 num_bytes = dsm_pushdown(&dsm_ptr, (void *)xml_announce, strlen(xml_announce), DSM_DS_LARGE_ITEM_POOL);
  TF_ASSERT(num_bytes == strlen(xml_announce));
  att_ptr = msgr_get_attach(&post_result_ind.msg_hdr, 0);
  TF_ASSERT(att_ptr != NULL);
  msgr_set_dsm_attach(att_ptr, dsm_ptr);
  TF_ASSERT(dsm_ptr != NULL);

  post_result_ind.err_code = DS_APPSRV_LTE_D_ERR_NONE;
  post_result_ind.http_status_code = 200;
  post_result_ind.req_id = 10;
  prose_qtf_send_msg((byte*)&post_result_ind, sizeof(post_result_ind));

  TF_MSG("Checking for LTE_MAC_PAC_ADD_REQ");

  lte_mac_pac_add_req_s *pac_add_req_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_pac_add_req_s) == buf_len);
  pac_add_req_ptr = (lte_mac_pac_add_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_PAC_ADD_REQ == pac_add_req_ptr->msg_hdr.id);
  TF_ASSERT(pac_add_req_ptr->session_id == 1);

  lte_mac_pac_add_cnf_s pac_add_cnf;
  msgr_init_hdr(&pac_add_cnf.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_ADD_CNF);
  pac_add_cnf.session_id = 1;
  pac_add_cnf.mac_handler_id = 1;
  prose_qtf_send_msg((byte*)&pac_add_cnf, sizeof(pac_add_cnf));
  
  prose_qtf_wait_for_done();
    
  lte_mac_pac_tx_status_rpt_ind_s tx_status_rpt;
  msgr_init_hdr(&tx_status_rpt.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_TX_STATUS_RPT_IND);
  tx_status_rpt.active_num = 1;
  tx_status_rpt.mac_handler_id_arr[0] = 1;
  prose_qtf_send_msg((byte*)&tx_status_rpt, sizeof(tx_status_rpt));

  prose_qtf_wait_for_done();

  prose_disc_publish_cancel_req_s cancel_req;
  msgr_init_hdr(&cancel_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_PUBLISH_CANCEL_REQ);
  memcpy(cancel_req.os_app_id.byte, "TEST_APP_ID", 12);
  cancel_req.os_app_id.length = 11;
  memcpy(cancel_req.pa_id.byte, "mcc001.mnc001.ProSeApp.TEST_PA_ID", 34);
  cancel_req.pa_id.length = 33;
  prose_qtf_send_msg((byte*)&cancel_req, sizeof(cancel_req));

  prose_qtf_wait_for_done();

  TF_MSG("Checking for PROSE_DISC_PUBLISH_CANCEL_RSP");

  prose_disc_publish_cancel_rsp_s *cancel_rsp_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_publish_cancel_rsp_s) == buf_len);
  cancel_rsp_ptr = (prose_disc_publish_cancel_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_PUBLISH_CANCEL_RSP == cancel_rsp_ptr->msg_hdr.id);

  TF_MSG("Checking for LTE_MAC_PAC_DEL_REQ");

  lte_mac_pac_del_req_s *pac_del_req_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_pac_del_req_s) == buf_len);
  pac_del_req_ptr = (lte_mac_pac_del_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_PAC_DEL_REQ == pac_del_req_ptr->msg_hdr.id);
  TF_ASSERT(pac_del_req_ptr->active_num == 1);
  TF_ASSERT(pac_del_req_ptr->mac_handler_id_arr[0] == 1);

  TF_MSG("Checking for PROSE_DISC_NOTIFICATION_IND");

  prose_disc_notification_ind_s *notification_ind_ptr;
  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_notification_ind_s) == buf_len);
  notification_ind_ptr = (prose_disc_notification_ind_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_NOTIFICATION_IND == notification_ind_ptr->msg_hdr.id);
  TF_ASSERT(notification_ind_ptr->result == PROSE_QMI_RESULT_ABORTED);

  //-----------------------------------------------------------------------

  msgr_init_hdr(&publish_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_PUBLISH_REQ);
  memcpy(publish_req.os_app_id.byte, "TEST_APP_ID", 12);
  publish_req.os_app_id.length = 11;
  memcpy(publish_req.pa_id.byte, "mcc001.mnc001.ProSeApp.TEST_PA_ID", 34);
  publish_req.pa_id.length = 33;
  publish_req.duration = 300;
  publish_req.disc_type = PROSE_DISC_TYPE_OPEN;
  publish_req.request_time = 300;
  prose_qtf_send_msg((byte*)&publish_req, sizeof(publish_req));

  prose_qtf_wait_for_done();

  TF_MSG("Checking for LTE_MAC_PAC_ADD_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_pac_add_req_s) == buf_len);
  pac_add_req_ptr = (lte_mac_pac_add_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_PAC_ADD_REQ == pac_add_req_ptr->msg_hdr.id);
  TF_ASSERT(pac_add_req_ptr->session_id == 2);

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_publish_rsp_s) == buf_len);
  publish_rsp_ptr = (prose_disc_publish_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_PUBLISH_RSP == publish_rsp_ptr->msg_hdr.id);

  prose_qtf_wait_for_done();

  msgr_init_hdr(&pac_add_cnf.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_ADD_CNF);
  pac_add_cnf.session_id = 2;
  pac_add_cnf.mac_handler_id = 1;
  prose_qtf_send_msg((byte*)&pac_add_cnf, sizeof(pac_add_cnf));
  
  prose_qtf_wait_for_done();
    
  msgr_init_hdr(&tx_status_rpt.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_TX_STATUS_RPT_IND);
  tx_status_rpt.active_num = 1;
  tx_status_rpt.mac_handler_id_arr[0] = 1;
  prose_qtf_send_msg((byte*)&tx_status_rpt, sizeof(tx_status_rpt));

  prose_qtf_wait_for_done();

  TF_SLEEP(61000);

  TF_MSG("Checking for LTE_MAC_PAC_DEL_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_pac_del_req_s) == buf_len);
  pac_del_req_ptr = (lte_mac_pac_del_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_PAC_DEL_REQ == pac_del_req_ptr->msg_hdr.id);
  TF_ASSERT(pac_del_req_ptr->active_num == 1);
  TF_ASSERT(pac_del_req_ptr->mac_handler_id_arr[0] == 1);

  TF_MSG("Checking for DS_APPSRV_LTE_D_POST_START_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(ds_appsrv_lte_d_post_req_s) == buf_len);
  req_ptr = (ds_appsrv_lte_d_post_req_s *)(void*) buf_ptr;
  TF_ASSERT(DS_APPSRV_LTE_D_POST_START_REQ == req_ptr->msg_hdr.id);
  TF_ASSERT(strcmp(req_ptr->apn_str, "TEST_APN") == 0);

  num_attach = 0;

  num_attach = msgr_get_num_attach(&req_ptr->msg_hdr);
  TF_ASSERT(num_attach == 1);
  att_ptr = msgr_get_attach((msgr_hdr_struct_type *)req_ptr, 0);
  TF_ASSERT(att_ptr != NULL);
  msgr_get_dsm_attach(att_ptr, &dsm_ptr);
  TF_ASSERT(dsm_ptr != NULL);

  dsm_packet_len = dsm_length_packet(dsm_ptr);

  char xml_string2[] = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><prose-discovery-message xmlns=\"urn:3GPP:ns:ProSe:Discovery:2014\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:cp=\"urn:ietf:params:xml:ns:copycontrol\"><DISCOVERY_REQUEST><discovery-request><transaction-ID>1</transaction-ID><command>1</command><UE-identity><MCC>1</MCC><MNC>2</MNC><MSIN>1234</MSIN></UE-identity><ProSe-Application-ID>mcc001.mnc001.ProSeApp.TEST_PA_ID</ProSe-Application-ID><application-identity><OS-ID>00000000000000000000000000000fb7</OS-ID><OS-App-ID>TEST_APP_ID</OS-App-ID></application-identity><discovery-entry-ID>0</discovery-entry-ID><Requested-Timer>300</Requested-Timer></discovery-request></DISCOVERY_REQUEST></prose-discovery-message>";
  
  TF_ASSERT(dsm_packet_len == strlen(xml_string2)+1);
  buffer = (char*)malloc(dsm_packet_len);
  TF_ASSERT(buffer);
  dsm_pullup(&dsm_ptr, buffer, dsm_packet_len);
  TF_ASSERT(dsm_ptr == NULL);
  TF_ASSERT(memcmp(buffer, xml_string2, strlen(xml_string2)) == 0);
  free(buffer);

  prose_qtf_wait_for_done();

  msgr_init_hdr(&post_req_id.msg_hdr, MSGR_PROSE_DISC, DS_APPSRV_LTE_D_REQ_ID_IND);
  post_req_id.request_id = 11;
  prose_qtf_send_msg((byte*)&post_req_id, sizeof(post_req_id));

  char xml_announce2[] = 
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\
    <prose-discovery-message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"urn:3GPP:ns:ProSe:Discovery:2014\" xsi:noNamespaceSchemaLocation=\"lted_prose_discovery_schema.xsd\">\
    <DISCOVERY_RESPONSE>\
    <Current-Time>1900-06-19T19:21:14</Current-Time>\
    <Max-Offset>16</Max-Offset>\
    <response-announce>\
    <transaction-ID>1</transaction-ID>\
    <ProSe-Application-Code>e008024321123408011042002201200000e00000000000</ProSe-Application-Code>\
    <validity-timer-T4000>2</validity-timer-T4000>\
    <discovery-type>65</discovery-type>\
    <discovery-key>00112233445566778899aabbccddeeff</discovery-key>\
    </response-announce>\
    </DISCOVERY_RESPONSE>\
    </prose-discovery-message>";

  msgr_init_hdr_attach(&post_result_ind.msg_hdr, MSGR_PROSE_DISC, DS_APPSRV_LTE_D_POST_RESULT_IND, 0, 1);
  msgr_set_hdr_inst(&post_result_ind.msg_hdr, 1);

  dsm_ptr = NULL;
  att_ptr = NULL;

  num_bytes = dsm_pushdown(&dsm_ptr, (void *)xml_announce2, strlen(xml_announce2), DSM_DS_LARGE_ITEM_POOL);
  TF_ASSERT(num_bytes == strlen(xml_announce2));
  att_ptr = msgr_get_attach(&post_result_ind.msg_hdr, 0);
  TF_ASSERT(att_ptr != NULL);
  msgr_set_dsm_attach(att_ptr, dsm_ptr);
  TF_ASSERT(dsm_ptr != NULL);

  post_result_ind.err_code = DS_APPSRV_LTE_D_ERR_NONE;
  post_result_ind.http_status_code = 200;
  post_result_ind.req_id = 11;
  prose_qtf_send_msg((byte*)&post_result_ind, sizeof(post_result_ind));

  prose_qtf_wait_for_done();

  TF_MSG("Checking for LTE_MAC_PAC_ADD_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_pac_add_req_s) == buf_len);
  pac_add_req_ptr = (lte_mac_pac_add_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_PAC_ADD_REQ == pac_add_req_ptr->msg_hdr.id);
  TF_ASSERT(pac_add_req_ptr->session_id == 2);

  msgr_init_hdr(&pac_add_cnf.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_ADD_CNF);
  pac_add_cnf.session_id = 2;
  pac_add_cnf.mac_handler_id = 3;
  prose_qtf_send_msg((byte*)&pac_add_cnf, sizeof(pac_add_cnf));
  
  prose_qtf_wait_for_done();
    
  msgr_init_hdr(&tx_status_rpt.msg_hdr, MSGR_PROSE_DISC, LTE_MAC_PAC_TX_STATUS_RPT_IND);
  tx_status_rpt.active_num = 1;
  tx_status_rpt.mac_handler_id_arr[0] = 3;
  prose_qtf_send_msg((byte*)&tx_status_rpt, sizeof(tx_status_rpt));

  prose_qtf_wait_for_done();

  msgr_init_hdr(&cancel_req.msg_hdr, MSGR_PROSE_DISC, PROSE_DISC_PUBLISH_CANCEL_REQ);
  memcpy(cancel_req.os_app_id.byte, "TEST_APP_ID", 12);
  cancel_req.os_app_id.length = 11;
  memcpy(cancel_req.pa_id.byte, "mcc001.mnc001.ProSeApp.TEST_PA_ID", 34);
  cancel_req.pa_id.length = 33;
  prose_qtf_send_msg((byte*)&cancel_req, sizeof(cancel_req));

  prose_qtf_wait_for_done();
  TF_MSG("Checking for PROSE_DISC_PUBLISH_CANCEL_RSP");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_publish_cancel_rsp_s) == buf_len);
  cancel_rsp_ptr = (prose_disc_publish_cancel_rsp_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_PUBLISH_CANCEL_RSP == cancel_rsp_ptr->msg_hdr.id);

  TF_MSG("Checking for LTE_MAC_PAC_DEL_REQ");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(lte_mac_pac_del_req_s) == buf_len);
  pac_del_req_ptr = (lte_mac_pac_del_req_s *)(void*) buf_ptr;
  TF_ASSERT(LTE_MAC_PAC_DEL_REQ == pac_del_req_ptr->msg_hdr.id);
  TF_ASSERT(pac_del_req_ptr->active_num == 1);
  TF_ASSERT(pac_del_req_ptr->mac_handler_id_arr[0] == 3);

  TF_MSG("Checking for PROSE_DISC_NOTIFICATION_IND");

  prose_qtf_get_next_ext_msg(&buf_ptr, &buf_len);
  TF_ASSERT(sizeof(prose_disc_notification_ind_s) == buf_len);
  notification_ind_ptr = (prose_disc_notification_ind_s *)(void*) buf_ptr;
  TF_ASSERT(PROSE_DISC_NOTIFICATION_IND == notification_ind_ptr->msg_hdr.id);
  TF_ASSERT(notification_ind_ptr->result == PROSE_QMI_RESULT_ABORTED);

  //-----------------------------------------------------------------------

  prose_qtf_check_no_more_msgs();
}

void ProseTestAnnounceTimeout::Teardown()
{
  prose_qtf_teardown();
}

