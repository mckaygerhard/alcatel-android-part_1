/*!
  @file
  prose_qtfi.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/test/common/prose_qtfi.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_QTFI_H
#define PROSE_QTFI_H

#include <msgr.h>
#include "prose_dispatcher.h"

typedef union
{
  msgr_hdr_s        msgr_hdr;
  prose_disc_publish_rsp_s  publish_rsp;
} prose_qtf_test_msg_u;

typedef union
{
  prose_ext_msg_u         ext_msg;
  prose_qtf_test_msg_u    test_msg;
} prose_qtf_ext_msg_u;

typedef union
{
  msgr_hdr_s          msgr_hdr;
  prose_qtf_ext_msg_u ext_msg;

} prose_qtf_msg_u;

// Todo: Change lte_rrc_ext_msg_u to an enum of ext msgs RRC throws
#define LTE_RRC_UTF_QUEUE_SLOT_SIZE                          \
 LTE_RRC_UTF_MAX(sizeof(prose_qtf_ext_msg_u), sizeof(lte_rrc_int_msg_u))


/*! @brief Static circular queue item
*/
typedef struct
{
  // Note we define a uint32 array to get the data buffer word-aligned
  uint32 data[(LTE_RRC_UTF_QUEUE_SLOT_SIZE / 4) + 1];
  uint32 len;

} prose_qtf_queue_item_s;


/*! @brief Static circular queue
*/
typedef struct
{
  /*!< Number of items in the queue */
  uint8 num_items;

  /*!< Number of queue items still being used (not yet freed) */
  uint8 num_outstanding;

  /*!< Array of ptrs to queue items */
  prose_qtf_queue_item_s items[LTE_RRC_UTF_QUEUE_LEN];

  /*!< Index to one more than the last item (index to push next item) */
  uint8 tail_index;

  /*!< Index to the next item to pop */
  uint8 head_index;

} prose_qtf_queue_s;


/*! @brief Encapsulates elements required for buffering received messages
*/
typedef struct
{
  /*!< Signal queue for rcvd msg events */
  msgr_client_t client;

  /*!< Mutex to synchronize access to message queue */
  pthread_mutex_t mutex;

  /*!< Queue of received messages */
  prose_qtf_queue_s queue;

  /*!< Mailbox id */
  msgr_id_t mb_id;

} prose_qtf_msgq_s;


/*! @brief Typedef of variables internal to module prose_qtf.cpp
*/
typedef struct
{
  /*!< Critical section to synch access to stimuli list */
  pthread_mutex_t   input_proc_mutex;

  /*!< Queue for storing list of stimuli already sent */
  msgr_client_t     input_proc_client;
  uint32            stimuli_list[LTE_RRC_UTF_MAX_NUM_STIMULI];
  uint8             num_stimuli;            /*!< Number of stimuli already sent */
  
  prose_qtf_msgq_s int_msgq;      /*!< Queue of received internal messages */
  prose_qtf_msgq_s ext_msgq;      /*!< Queue of received external messages */

  msgr_client_t    ext_msgq_client;    /*!< Mailbox to receive external messages */
  msgr_id_t        ext_msgq_mb_id;     /*!< Mailbox id for external msgq */
  boolean          ext_msgq_mb_exists; /*!< Flag to indicate if ext msg mb was allocated */

  /*!< Static space for storing dispatch payload for direct calls to transition funcs */
  lte_rrc_dispatcher_message_s dispatch_payload;

} prose_qtf_s;

/*===========================================================================

                    EXTERNAL FUNCTION PROTOTYPES

===========================================================================*/

void prose_qtf_queue_put
(
  prose_qtf_msgq_s *msgq,   /*!< Ptr to the msgq to initialize */  
  byte    *buf_ptr,                  /*!< Ptr to the msg buffer */
  uint32  buf_len                    /*!< Length of the buffer */
);

void prose_qtf_queue_get
(
  prose_qtf_msgq_s *msgq, /*!< Ptr to the msgq to initialize */  
  byte   **buf_ptr,         /*!< <OUT> Ptr to the msg buffer, NULL if queue is empty */
  uint32 *buf_len           /*!< <OUT> Length of buffer in bytes */
);

void prose_qtf_queue_reset
(
  prose_qtf_msgq_s *msgq   /*!< Ptr to the msgq to initialize */  
);

#endif
