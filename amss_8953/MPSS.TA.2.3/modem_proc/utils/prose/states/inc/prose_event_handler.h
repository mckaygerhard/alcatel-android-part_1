/*!
  @file
  prose_event_handler.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/states/inc/prose_event_handler.h#5 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef PROSE_EVENT_HANDLER_H
#define PROSE_EVENT_HANDLER_H

#include "state_machine.h"
#include "prose_ext_msg.h"
#include "lte_mac_msg.h"

#include "AnnounceRsp_info.h"
#include "MonitorRsp_info.h"
#include "RejectRsp_info.h"
#include "MatchAck_info.h"
#include "MatchReject_info.h"

#include "prose_diag.h"

class ProseEventHandler
{
  protected:
    virtual prose_event_status_e on_disc_publish_req(const prose_disc_publish_req_s *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "PublishReq dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_disc_publish_cancel_req(const prose_disc_publish_cancel_req_s *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "PublishCancelReq dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_disc_subscribe_req(const prose_disc_subscribe_req_s *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "SubscribeReq dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_disc_subscribe_cancel_req(const prose_disc_subscribe_cancel_req_s *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "SubscribeCancelReq dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_pc3_announce_rsp(const AnnounceRsp_info *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "AnnounceRsp dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_pc3_monitor_rsp(const MonitorRsp_info *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "MonitorRsp dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_pc3_reject_rsp(const RejectRsp_info *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "RejectRsp dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_pc3_match_ack(const MatchAck_info *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "MatchAck dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_pc3_match_rej(const MatchReject_info *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "MatchReject dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_pac_add_cnf(const lte_mac_pac_add_cnf_s *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "PacAddCnf dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_filter_add_cnf(const lte_mac_filter_add_cnf_s *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "FilterAddCnf dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_pac_match_rpt_ind(const lte_mac_pac_match_rpt_ind_s *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "MatchRptInd dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_pac_tx_status_rpt_ind(const lte_mac_pac_tx_status_rpt_ind_s *msg_ptr, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "TxStatusRptInd dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_conn(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "Conn dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_auth_change(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "AuthChange dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_xml_failure(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "XmlFailure dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }
    
    virtual prose_event_status_e on_pre_t4000_expiry(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "PreT4000 dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_t4000_expiry(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "T4000 dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_pre_t4002_expiry(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "PreT4002 dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_t4002_expiry(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "T4002 dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_t4004_expiry(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "T4004 dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_t4006_expiry(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "T4006 dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_t_hangover_expiry(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "T_HANGOVER dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

    virtual prose_event_status_e on_t_session_expiry(const void*, StateMachine *sm)
    {
      PROSE_MSG_SPRINTF_1(PROSE_MAIN_ERROR, "T_SESSION dropped in state %s", sm->get_current_state()->get_state_name());
      return EVENT_NOT_HANDLED;
    }

  private:

    friend class ProseStateMachine;
};

#endif /* PROSE_EVENT_HANDLER_H */
