/*!
  @file
  state_machine.h

  @brief
  This file implements the core functions of PROSE module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/prose/states/inc/state_machine.h#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
04/22/16   sc      Initial Revision

=============================================================================*/

#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "state.h"

class StateMachine
{
  public:

    StateMachine(State *initial_state) : current_state_(initial_state)
    { start(); }
  
    virtual ~StateMachine() {}

    State* get_current_state() const
    {
      return current_state_;
    }

    void transition_to_state(State *target)
    {
      State *source = get_current_state();
      if (source == target)
      {
        target->on_exit(this);
        target->on_entry(this);
      }
      else
        transition_from_state(source, target);
      set_current_state(target);
      target->on_initialize(this);
    }

  protected:
    template <typename EVENT_HANDLER, typename EVENT>
      void process_event(const EVENT* event, prose_event_status_e (EVENT_HANDLER::* event_handler)(const EVENT*, StateMachine*))
      {
        State *target = get_current_state();
        for (bool handled = false; !handled; target = target->parent_)
        {
          ASSERT(target != 0);
          handled = target->process_event(event, event_handler, this);
        }
      }

    void start()
    {
      transition_to_state(get_root());
    }

  private:
    StateMachine(const StateMachine&);
    StateMachine& operator=(const StateMachine&);

    State* get_root() const
    {
      State *root = get_current_state();
      for (; root->parent_; root = root->parent_) {}
      return root;
    }

    void transition_from_state(State *source, State *target)
    {
      for ( ; source && source->level_ > target->level_; source = source->parent_)
      {
        source->on_exit(this);
      }
      if (source != target)
      {
        if (!target->parent_) 
        {
          ASSERT(!source);
        }
        else
          transition_from_state(source, target->parent_);

        target->on_entry(this);
      }
    }

    void set_current_state(State *state)
    {
      current_state_ = state;
    }

    State *current_state_;
};

#endif /* STATE_MACHINE_H */
