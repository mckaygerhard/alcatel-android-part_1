/*!
  @file
  a2_ul_seci.h

  @brief
  The internal interface to the A2 UL SEC Driver Module

  @author
  araina
*/

/*==============================================================================

  Copyright (c) 2014 Qualcomm Technologies, Inc. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies, Inc. and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies, Inc.

==============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/utils.mpss/5.2/a2/driver/src/a2_ul_seci.h#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------------
08/13/14   mm      Updated prototype for a2_ul_seci_write_key_to_int_mem()
08/12/14   mm      Changed sec key storage to uint32 to avoid possible 
                   alignment issues
05/09/14   mm      Naming cleanup
04/07/14   ca      Replaced DMA task and ciph set up task with 
                   DMA_ciph task to save taskq memory
03/28/14   mm      Moved structs from a2_ul_sec.c for QSH
03/13/14   ca      CR:631667, fixed race conditon b/w A2 thread and tech
                   in case of a2_ul_sec_write_keys(). Added 
                   a2_ul_sec_process_write_keys_req() fn.
01/13/14   ca      DSDA feature is implemented.
10/09/13   ar      initial version
==============================================================================*/

#ifndef A2_UL_SECI_H
#define A2_UL_SECI_H

/*==============================================================================

                           INCLUDE FILES

==============================================================================*/
#include <comdef.h>
#include <a2_dbg.h>
#include <a2_common.h>
#include <a2_hw_constants.h>
#include <a2_hw_task_defi.h>
#include <a2_util.h>
#include <a2_ul_sec.h>
#include <a2_poweri.h>
#include <a2_taskq.h>
#include <a2_msg.h>

/*==============================================================================

                   EXTERNAL DEFINITIONS AND TYPES

==============================================================================*/

/*!
  @brief
  macro used to indicate that A2 UL SEC is already initialized as
  part of A2 driver initialization
*/
#define A2_UL_SEC_INITIALIZED                    0xFEEDC0FE

/*!
  @brief Tag extn mask for tag task type
*/
#define A2_UL_SEC_LTE_TAG_TASK_TYPE_MASK         0x001F

/*!
  @brief Maximum number of ciphering and integrity keys
*/
#define A2_UL_SEC_LTE_NUM_KEYS_MAX               16

/*!
  @brief maximum number of bytes pulled up from PDCPDL source packet
         2bytes(max pdcp hdr size) + 1byte(data) = 3bytes
*/
#define A2_UL_SEC_LTE_MAX_PULLUP_DATA_SIZE       3

/*!
  @brief number of bytes that needs to be pulled up from the tail of
         PDCPDL source packet to get the MAC-I
*/
#define A2_UL_SEC_LTE_MACI_SIZE_IN_BYTES       4

/*! 
  @brief This macro used to get the inst id from the sub id.
         This is a two step process. one is to get the inst bf from sub id
         and then get the inst id from inst bf */

#define A2_UL_SEC_GET_HW_INST_ID(sub_id)          \
  a2_ul_sec.common.hw_inst_bf_to_inst_id_tbl      \
  [a2_ul_sec.common.sub_id_info_tbl[sub_id].hw_inst_bf]

/*!
  @brief Structure to hold a2 sub id information such as,
         the inst bf and registered call backs.
*/
typedef struct
{
  /*! storage for ciphering and integrity keys, needed for register interface
      based ciphering */
  uint32            keys[A2_CIPHER_MAX_NUM_KEY_IDX][
                      A2_BYTES_TO_WORDS(A2_UL_SEC_KEY_SIZE)];
  /*! a bitmask of which keys have been written to hw (LSB represents key 0) */
  uint32            keys_valid_bmsk;

  /*! variable to store handle to the instance bf for a given sub id */
  a2_hw_inst_bf_t   hw_inst_bf;
} a2_ul_sec_sub_id_info_s;

/*!
  @brief
  Tag Ids to be used in TAG TASK (For internal A2 driver usage only) tag id is
  5 bits long
*/
typedef enum
{
  A2_UL_SEC_TAG_HSPA_GATHER_AND_DECIPHER                    = 0,
  A2_UL_SEC_TAG_LTE_GATHER_DECIPHER_AND_INTEGRITY_SRC_ADDR  = 1,
  A2_UL_SEC_TAG_LTE_GATHER_DECIPHER_AND_INTEGRITY_DEST_ADDR = 2,
  A2_UL_SEC_TAG_LTE_COMPUTE_INTEGRITY                       = 3,
  A2_UL_SEC_TAG_DSM_SRC_TO_DSM_DEST_GATHER                  = 4,
  A2_UL_SEC_TAG_MAX                                         = 31
} a2_ul_sec_tag_e;

/*!
  @brief
  Structure to hold cached info of all the tasks written by this module
*/
typedef struct
{
  /*! cached tasks for api a2_ul_seci_write_keys() */
  hwtask_a2_ts_dma_fill_t         dma_fill_for_keys;

  /*! cached tasks for api a2_ul_seci_hspa_gather_and_decipher() */
  hwtask_a2_ts_dma_t              dma_for_hspa_gather;
  hwtask_a2_ts_dma_ciph_t         dma_ciph_for_hspa_gather;  
  hwtask_a2_ts_umts_ciph_init_t   ciph_init_for_hspa_gather;
  hwtask_a2_ts_dma_tag_t          tag_for_hspa_gather;

  /*! cached tasks for api a2_ul_seci_dma_src_dsm_chain_to_dest_dsm_chain() */
  hwtask_a2_ts_dma_t              dma_for_dsm_gather;
  hwtask_a2_ts_dma_tag_t          tag_for_dsm_gather;

  hwtask_a2_ts_dma_irq_t          dma_irq;

} a2_ul_sec_cached_tasks_s;

/*!
  @brief
  Structure to hold call back functions registered during initialization
*/
typedef struct
{
  a2_ul_sec_hspa_gather_and_decipher_cb          hspa_gather_cb_ptr;
  a2_ul_sec_lte_gather_decipher_and_integrity_cb lte_gather_cb_ptr;
  a2_ul_sec_lte_compute_integrity_cb             lte_integrity_cb_ptr;
  a2_ul_sec_dma_src_dsm_to_dest_dsm_cb           dma_dsm_chain_cb_ptr;
} a2_ul_sec_registered_cbs_s;

typedef struct
{
  uint32    num_dma_irq_rcvd;
} a2_ul_sec_stats_s;

/*!
  @brief Structure to hold all global variables used in this file
*/
typedef struct
{
  /*! variable to keep track whether a2_ul_sec structure is initialized or
      not. This strcuture cannot be initialized more than once after power up.
  */
  uint32                      initialized;
  /*! variable to hold all the cached tasks in which fields are set to static
      information which is not expected to change. */
  a2_ul_sec_cached_tasks_s    cached_tasks;

  /*! A2 UL SEC Task queue */
  a2_taskq_s                  taskq;

  /*! A2 UL SEC Status queue */
  a2_taskq_status_s           statusq;

  /*! Mutex lock to guarantee write consistency to UL SEC task queue */
  a2_crit_sect_s              taskq_lock;

  a2_iovec_t                  iovec_list[A2_UL_SEC_MAX_SRC_IOVEC_COUNT];

  /*! iovec list to store addr/len info for source dsm item chain */
  a2_iovec_t                  src_dsm_iovec_list[A2_UL_SEC_MAX_SRC_IOVEC_COUNT];

  /*! iovec list to store addr/len info for programming common tasks apis */
  a2_iovec_t                  temp_iovec_list[A2_UL_SEC_MAX_SRC_IOVEC_COUNT];


  /*! a2 power collapse related debug info */
  a2_pc_dbg_s                  pc_dbg;

  a2_ul_sec_stats_s            stats;

    /*! reverse look up from thread is to system sub id*/
  a2_sub_id_t                  sub_id;

} a2_ul_sec_inst_s;

typedef struct
{
  /*! A table to map hw instance bit field to hw instance id */
  a2_hw_inst_id_e            hw_inst_bf_to_inst_id_tbl[A2_HW_INST_BF_MAX+1];

  /*! instance look up from system sub id */
  a2_ul_sec_sub_id_info_s    sub_id_info_tbl[A2_SUB_ID_MAX];
  
  /*! Registered call back(s) list */
  a2_ul_sec_registered_cbs_s  registered_cb_list;
  
} a2_ul_sec_common_s;

typedef struct
{
  /*! a2 ul sec instances */
  a2_ul_sec_common_s   common;
  a2_ul_sec_inst_s     inst[A2_HW_INST_ID_MAX];  
} a2_ul_sec_s;

/*==============================================================================

                    EXTERNAL FUNCTION PROTOTYPES

==============================================================================*/

/*==============================================================================

  FUNCTION:  a2_ul_sec_init

==============================================================================*/
/*!
  @brief
  Initializes SW structures associated with a2 ul sec block

  @return
  None
*/
/*============================================================================*/
extern void a2_ul_sec_init
(
  void
);
/* a2_ul_sec_init() */

/*==============================================================================

  FUNCTION:  a2_ul_sec_get_top_lvl_ptr

==============================================================================*/
/*!
  @brief
  Returns a pointer to the top-level data structure.
*/
/*============================================================================*/
extern a2_ul_sec_s * a2_ul_sec_get_top_lvl_ptr
(
  void
);
/* a2_ul_sec_get_top_lvl_ptr() */

/*==============================================================================

  FUNCTION:  a2_ul_sec_deinit

==============================================================================*/
/*!
  @brief
  De-inits variables associated with a2 ul sec block

  @return
  None
*/
/*============================================================================*/
extern void a2_ul_sec_deinit
(
  void
);
/* a2_ul_sec_deinit() */

/*==============================================================================

  FUNCTION:  a2_ul_sec_ist_cb

==============================================================================*/
/*!
  @brief
  Interrupt handler for A2 UL SEC module.
*/
/*============================================================================*/
extern int a2_ul_sec_ist_cb
(
  void* arg
);
/* a2_ul_sec_ist_cb() */

/* #ifdef FEATURE_A2_UL_SEC_TEST */

/*==============================================================================

  FUNCTION:  a2_ul_sec_dma_from_a2_internal_mem

==============================================================================*/
/*!
  @brief
  Function writes a dma task for gathering keys from A2 internal memory

  @detail
  none

*/
/*============================================================================*/
extern void a2_ul_sec_dma_from_a2_internal_mem
(
  a2_sub_id_t  sub_id,  /* sub id in the system */
  uint32       src_addr,         /*!< source a2 internal memory address */
  uint16       src_len_in_bytes, /*!< source length in bytes */
  uint32       dest_addr         /*!< address where dma-ed data is stored */
);
/* a2_ul_sec_dma_from_a2_internal_mem() */

/* #endif FEATURE_A2_UL_SEC_TEST */

/*==============================================================================

  FUNCTION:  a2_ul_sec_hw_init

==============================================================================*/
/*!
  @brief
  initializes the HW related registers for UL SEC block

  @return
  None
*/
/*============================================================================*/
extern void a2_ul_sec_hw_init
(
  a2_sub_id_t      sub_id, /* subscriber id*/ 
  a2_technology_e  tech, /* technology */
  a2_hw_inst_bf_t  inst_bf /* memory region/a2 instance*/
);
/* a2_ul_sec_hw_init() */

/*==============================================================================

  FUNCTION:  a2_ul_sec_add_sub_id

==============================================================================*/
/*!
  @brief
  updates the sub id to instance bf.
  This fucniton is called when power_proc_req() is being processed.

  @return
  returns void
*/
/*============================================================================*/


void a2_ul_sec_add_sub_id
(
a2_sub_id_t       sub_id,     /*!< sub id in the system */
a2_hw_inst_bf_t   a2_inst_bf /*!< a2 instacne id  */
);

/*==============================================================================

  FUNCTION:  a2_ul_sec_unset_technology

==============================================================================*/
/*!
  @brief
  calls the hw deinit 
  @return
  returns void
*/
/*============================================================================*/
void a2_ul_sec_unset_technology
(
  a2_sub_id_t       sub_id     /*!< sub id in the system */
);

/*==============================================================================

  FUNCTION:  a2_ul_sec_hw_deinit

==============================================================================*/
/*!
  @brief
  disables the a2 threads.
  @return
  returns void
*/
/*============================================================================*/

void a2_ul_sec_hw_deinit
(
  a2_sub_id_t       sub_id     /*!< sub id in the system */
);

/*==============================================================================

  FUNCTION:  a2_ul_sec_set_technology

==============================================================================*/
/*!
  @brief
  this fucntion calls a2_ul_sec_hw_init() fn and intializes a2_ul_sec for
  a particular tech.
  @return
  None
*/
/*============================================================================*/
void a2_ul_sec_set_technology
(
  a2_sub_id_t      sub_id, /* subscriber id*/ 
  a2_technology_e  tech, /* technology */
  a2_hw_inst_bf_t  inst_bf /* memory region/a2 instance*/
);

/*==============================================================================

  FUNCTION:  a2_ul_sec_get_sub_id_to_inst_bf_tbl_ptr
==============================================================================*/
/*!
  @brief
  To get the handle to the sub_id_to_inst_bf table
*/
/*============================================================================*/
a2_ul_sec_sub_id_info_s* a2_ul_sec_get_sub_id_to_inst_bf_tbl_ptr
(
  void
);

/*==============================================================================

  FUNCTION:  a2_ul_sec_set_boot_up_state
==============================================================================*/
/*!
  @brief
  To set the boot up state for taskqs
*/
/*============================================================================*/
void a2_ul_sec_set_boot_up_state
(
  boolean boot_up_state
);

/*==============================================================================

  FUNCTION:  a2_ul_sec_process_write_keys_req

==============================================================================*/
/*!
  @brief
  Processes the write keys message in A2 task context. The keys are expected to
  already have been written to the internal data structure.
*/
/*============================================================================*/
EXTERN void a2_ul_sec_process_write_keys_req
(
    a2_ul_seci_write_keys_req_s *   write_keys_req_ptr
);

/*==============================================================================

  FUNCTION:  a2_ul_seci_get_key_ptr_from_int_mem

==============================================================================*/
/*!
  @brief
  Returns a pointer to the first word of the key given a pointer to internal
  memory (or a physical memory dump of internal memory).
*/
/*============================================================================*/
uint32 * a2_ul_seci_get_key_ptr_from_int_mem
(
  a2_hw_inst_id_t   inst_id,
  uint32            key_index,
  uint32 *          int_mem_ptr
);

/*==============================================================================

  FUNCTION:  a2_ul_seci_check_key

==============================================================================*/
/*!
  @brief
  Returns whether or not the key specified has been sent to A2 task to be 
  written into A2 HW.
*/
/*============================================================================*/
boolean a2_ul_seci_check_key
(
  a2_sub_id_t   sub_id, 
  uint32        key_idx
);

/*==============================================================================

  FUNCTION:  a2_ul_seci_write_key_to_int_mem

==============================================================================*/
/*!
  @brief
  Writes a single key to internal memory. The key should have already been
  set in physical memory by a2_ul_sec_write_keys().
*/
/*============================================================================*/
EXTERN void a2_ul_seci_write_key_to_int_mem
(
  a2_sub_id_t       sub_id,
  a2_hw_inst_id_t   inst_id,
  uint32            key_idx
);

#endif /* A2_UL_SEC_H */
