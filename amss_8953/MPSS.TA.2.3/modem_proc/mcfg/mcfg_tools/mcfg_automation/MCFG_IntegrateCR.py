#******************************************************************************
#* $Header: //components/rel/mcfg.mpss/7.0/mcfg_tools/mcfg_automation/MCFG_IntegrateCR.py#2 $
#* $DateTime: 2016/02/11 12:16:01 $
#*
#* 
#******************************************************************************
#*
#* Copyright (c) 2015 Qualcomm Technologies, Inc.
#* All rights reserved.
#* Qualcomm Technologies, Inc. Confidential and Proprietary.
#*
#******************************************************************************
from PrismClient import *
import pdb
import sys
from optparse import OptionParser
from MCFG_Automation_Functions import *
changeRequest = None

integrateParser = OptionParser()
integrateParser.add_option("--cr", dest="crNumber", help="The CR number that will be integrated")
integrateParser.add_option("--pause", dest="pause", action="store_true", default=False,help="The pause flag will allow the user to verify changes before continuing to promote the workspace")


(options, args) = integrateParser.parse_args()
if options.crNumber == None:
		print "Error! No CR number was specified...\n"
		integrateParser.print_help()
		sys.exit(0)

def integrate():
	


	jira = None
	user,pw = login()
	try:
		jira = JIRA(user,pw)
	except:
		print "Failed to create JIRA object"
	
	prism = PrismClient(pw)
	
	readXMLs()
	changeRequest = prism.get_changerequest_by_id(int(options.crNumber))
	pdb.set_trace()

	PackageWarehouses = set()
	ParsedItems = []
	parseCR(changeRequest['Description'],set(),ParsedItems,jira)

	for changeTask in changeRequest['ChangeTasks'][0]:
		PackageWarehouses.add(changeTask['ChangeTaskName'])

	processCR(int(options.crNumber), PackageWarehouses,ParsedItems,jira)


integrate()
