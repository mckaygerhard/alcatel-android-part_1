#ifndef __MCPM_RUMI_STUBS_H__
#define __MCPM_RUMI_STUBS_H__

/*=========================================================================

        M O D E M   C L O C K   A N D   P O W E R   M A N A G E R
        
                   U T I L S   H E A D E R    F I L E


  GENERAL DESCRIPTION
    This file contains STUBS REQUIRED for the MCPM driver until full clock driver changes are supported.
  
  EXTERNALIZED FUNCTIONS
    
  INITIALIZATION AND SEQUENCING REQUIREMENTS
    
  
        Copyright (c) 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.


==========================================================================*/

/*==========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.
 
$Header: //components/rel/mpower.mpss/7.3/mcpm/inc/mcpm_rumi_stubs.h#1 $

when         who     what, where, why 
--------     ---     ---------------------------------------------------------
08/17/15     pc      Initial version for JACALA RUMI



==========================================================================*/ 


/*==========================================================================

                     INCLUDE FILES FOR MODULE

==========================================================================*/
#include <mcpm_utils.h>

/*
 * Includes not in the local directory.
 */
#ifdef MCPM_JACALA
#define MCPM_TARUMI_RET return //remove stubs once resource is available
#define MCPM_TARUMI_RETX(...) return (__VA_ARGS__); //remove stubs once resource is available
#define MCPM_TARUMI_ERR(...) MCPM_MSG_HIGH_3(__VA_ARGS__); //remove stubs once resource is available
#else
#define MCPM_TARUMI_RET //remove stubs once resource is available
#define MCPM_TARUMI_RETX(...) //remove stubs once resource is available
#define MCPM_TARUMI_ERR(...) MCPM_ERR_FATAL(__VA_ARGS__); //remove stubs once resource is available
#endif


#endif /* __MCPM_UTILS_H__ */






