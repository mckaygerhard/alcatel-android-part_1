
#ifndef FTM_RF_DEBUG_INTERFACE_H
#define FTM_RF_DEBUG_INTERFACE_H
/*!
  @file
  ftm_rf_debug_interface.h

  @brief
  Common framework to perform RF Calibration commands.
*/

/*======================================================================================================================

  Copyright (c) 2015 - 2016 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document are confidential 
  and proprietary information of Qualcomm Technologies Incorporated and all rights therein are 
  expressly reserved. By accepting this material the recipient agrees that this material and the 
  information contained therein are held in confidence and in trust and will not be used, copied, 
  reproduced in whole or in part, nor its contents revealed in any manner to others without the 
  express written permission of Qualcomm Technologies Incorporated.

======================================================================================================================*/

/*======================================================================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rffactory.mpss/1.0/intf/ftm_rf_debug_interface.h#8 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------------------------------------------------
08/22/16   br      Radio config- Updated property enums
07/26/16   br      Radio config- Updated property enums
07/25/16   mpt     Added tx override properties
07/14/16   br      Initial Release

======================================================================================================================*/

#include "comdef.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef T_WINNT
#error code not present
#endif

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the reserved field in FTM test command packet.  */
typedef uint16 ftm_rf_debug_command_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the reserved field in FTM test command packet.  */
typedef uint32 ftm_rf_debug_field_reserved_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the version field in FTM test command packet.  */
typedef uint32 ftm_rf_debug_field_version_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the size of the technology field in FTM test command packet.  */
typedef uint32 ftm_rf_debug_field_technology_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Type definition to define the error code mask for response packet. Each bit indicates a type of error. */ 
typedef uint32 ftm_rf_debug_field_error_code_mask_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM command request packet */
typedef PACK(struct)
{
  uint8 diag_cmd;
  /*!< Command Identification */

  uint8 diag_subsys_id;
  /*!< Sub-system Identification. For instance - 11 for FTM */

  uint16 rf_subsys_id;
  /*!< RF Mode Identification for FTM_RF_DEBUG_C */

  ftm_rf_debug_command_type debug_command;
  /*!< FTM test command defined by #ftm_rf_test_command_enum_type */

  uint16 req_len;
  /*!< Defines the request length of the FTM diag packet, <b>when the packet is uncompressed</b>. This includes 
  the summation of the size of ftm_rf_test_diag_header_type and size of remaning paylaod (when it is uncompressed) */

  uint16 rsp_len;
  /*!< Defines the response length of the FTM diag packet, <b>when the packet is uncompressed</b>. This includes 
  the summation of the size of ftm_rf_test_diag_header_type and size of remaning paylaod (when it is uncompressed) */

} ftm_rf_debug_diag_header_type;


/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM command request packet */
typedef PACK(struct)
{
  uint8 diag_cmd;
  /*!< Command Identification */

  uint8 diag_subsys_id;
  /*!< Sub-system Identification. For instance - 11 for FTM */

  uint16 rf_subsys_id;
  /*!< RF Mode Identification. 101 for FTM_RF_DEBUG_C */

  uint32 status;
  /*!< Status field. Not currently used */

  uint16 delayed_rsp_id;
  /*!< Delayed response ID. 0 if only 1 response packet. >0 for more than 1 responses. This is a common
  ID for all packets belonging to the same request  */

  uint16 rsp_cnt;
  /*!< Response count. 0 for the first response packet, 1 for the second response packet, etc. If more than
  two responses, the MSB (0x8000) will be set for all responses except for the last response. (e.g. 0x8000,
  0x8001, 0x8002, 0x0003). The MSB can be thought of as the "more to come" bit. The astute will wonder why 
  the "two response" case doesn't set the MSB of the first response...good question, that's just the way 
  the diag team defined this field. */

  ftm_rf_debug_command_type debug_command;
  /*!< FTM test command defined by #ftm_rf_debug_command_enum_type */

  uint16 req_len;
  /*!< Defines the request length of the FTM diag packet, <b>when the packet is uncompressed</b>. This includes 
  the summation of the size of ftm_rf_debug_diag_header_type and size of remaning paylaod (when it is uncompressed) */

  uint16 rsp_len;
  /*!< Defines the response length of the FTM diag packet, <b>when the packet is uncompressed</b>. This includes 
  the summation of the size of ftm_rf_debug_diag_header_type and size of remaning paylaod (when it is uncompressed) */

} ftm_rf_debug_diag_header_subsys128_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Debug command request packet */
typedef PACK(struct)
{
  ftm_rf_debug_field_reserved_type reserved;
  /*!< Reserved Field */

  ftm_rf_debug_field_version_type version;
  /*!< Version of packet */

  ftm_rf_debug_field_technology_type technology;
  /*!< Indicates the technology (defined by ftm_rf_technology_type) for which the command is sent */

} ftm_rf_debug_command_header_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Debug command request packet 
  V2 moves  technology field out of test command header and into payload */
typedef PACK(struct)
{
  ftm_rf_debug_field_reserved_type reserved;
  /*!< Reserved Field */

  ftm_rf_debug_field_version_type version;
  /*!< Version of packet */

} ftm_rf_debug_command_header_type_v2;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Debug command request packet */
typedef PACK(struct)
{
  ftm_rf_debug_diag_header_type diag_header;
  /*!<  Structure defining the header for FTM request packet */

  ftm_rf_debug_command_header_type command_header;
  /*!<  Structure defining the command header for RF Debug command packet */

} ftm_rf_debug_req_header_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Debug command request packet 
  V2 moves technology fields out of test command header and into payload */
typedef PACK(struct)
{
  ftm_rf_debug_diag_header_type diag_header;
  /*!<  Structure defining the header for FTM request packet */

  ftm_rf_debug_command_header_type_v2 command_header;
  /*!<  Structure defining the command header for RF Debug command packet */

} ftm_rf_debug_req_header_type_v2;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Debug command response packet */
typedef PACK(struct)
{
  ftm_rf_debug_field_reserved_type reserved;
  /*!< Reserved Field */

  ftm_rf_debug_field_version_type version;
  /*!< Version of packet */

  ftm_rf_debug_field_error_code_mask_type common_error_code;
  /*!< Error code from the common framework */

} ftm_rf_debug_command_rsp_header_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Debug command response packet */
typedef PACK(struct)
{
  ftm_rf_debug_diag_header_type diag_header;
  /*!<  Structure defining the header for FTM response packet */

  ftm_rf_debug_command_rsp_header_type command_header;
  /*!<  Structure defining the command header for RF Debug response packet */

} ftm_rf_debug_header_rsp_type;

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Defintion of command header for FTM RF Debug command response packet, Diag subsys 128 */
typedef PACK(struct)
{
  ftm_rf_debug_diag_header_subsys128_type diag_header;
  /*!<  Structure defining the header for FTM response packet using subsys128 */

  ftm_rf_debug_command_rsp_header_type command_header;
  /*!<  Structure defining the command header for RF Debug response packet */

} ftm_rf_debug_header_rsp_subsys128_type;

/*====================================================================================================================*/
/*!
  @addtogroup FTM_TEST_TOOLS_CID
  @{
*/

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of commands available for FTM test command */
typedef enum
{
  FTM_RF_DEBUG_CMD_UNASSIGNED = 0, /*!< 0 : Unassigned command */

  FTM_RF_DEBUG_CMD_RADIO_CONFIGURE = 1, /*!< 1 : Radio Configure command */

  FTM_RF_DEBUG_CMD_TX_OVERRIDE = 2, /*!< 2 : Tx override command */

  FTM_RF_DEBUG_CMD_RX_OVERRIDE = 3, /*!< 3 : Rx override command */

  FTM_RF_DEBUG_CMD_DISCOVERY = 4, /*!< 4 : RF Radio Allocation command */

  FTM_RF_DEBUG_CMD_IQ_CAPTURE = 5, /*!< 6 : IQ Capture command */

  FTM_RF_DEBUG_CMD_NUM  /*!< Max : Defines maximum number of command IDs */

} ftm_rf_debug_command_enum_type;

/*! @} */ /* FTM_RF_DEBUG_TOOLS_CID */

/*====================================================================================================================*/
/*!
  @addtogroup FTM_RF_DEBUG_ERROR_CODE
  @{
*/

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of error */
typedef enum
{
  FTM_RF_DEBUG_CMD_HANDLER_FAIL = 1, /*!< 1 : Indicates command handlder failure */

  FTM_RF_DEBUG_EC_NULL_PARAM = 2, /*!< 2 : Indicates NULL parameter */

  FTM_RF_DEBUG_EC_RSP_CREATION = 3, /*!< 3 : Failure during response creation */

  FTM_RF_DEBUG_EC_BAD_CMD = 4, /*!< 4 : Bad Command */

  FTM_RF_DEBUG_EC_BAD_CMD_HANDLER = 5, /*!< 5 : Indicates bad command hander */

  FTM_RF_DEBUG_EC_UMCOMPRESS_FAIL = 6, /*!< 6: Uncompress failure */

  FTM_RF_DEBUG_EC_MALLOC_FAILRE = 7, /*!< 7 : Malloc failure */

  FTM_RF_DEBUG_EC_NUM  /*!< Max : Defines maximum number of error codes */

} ftm_rf_debug_error_code_enum_type;

/*! @} */ /* FTM_RF_DEBUG_ERROR_CODE */

/*====================================================================================================================*/
/*!
  @addtogroup FTM_TEST_TOOLS_COMMAND Property IDs
  @{
*/

/*====================================================================================================================*/
/*!
  @name Radio Config Property ID

  @brief
  Radio Config Property ID list
*/
/*! @{ */

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for FTM Radio Config command. These enumeration are used to define 
the content of #ftm_properties_framework_field_property_type in FTM test command packet. 
radio_config_property_names Must be updated when this list is updated.*/ 
typedef enum
{
  FTM_RF_DEBUG_RADIO_CFG_PROP_UNASSIGNED = 0,       /*!< 0 : Unassigned property */

  FTM_RF_DEBUG_RADIO_CFG_PROP_IS_TEARDOWN = 1,      /*!< 1 : If teardown needs to be done */

  FTM_RF_DEBUG_RADIO_CFG_PROP_RADIO_SETUP_TYPE = 2, /*!< 2 : Type of Radio setup needed */

  FTM_RF_DEBUG_RADIO_CFG_PROP_RFM_DEVICE = 3,       /*!< 3 : RFM Device*/

  FTM_RF_DEBUG_RADIO_CFG_PROP_SIG_PATH = 4,        /*!< 4 : Signal Path for AT forward */

  FTM_RF_DEBUG_RADIO_CFG_PROP_ANT_PATH = 5,        /*!< 5 : Antenna Path */

  FTM_RF_DEBUG_RADIO_CFG_PROP_RFM_PATH_TYPE = 6,   /*!< 6 : RFM Path type - Tx/PRx/DRx */

  FTM_RF_DEBUG_RADIO_CFG_PROP_BAND = 7,            /*!< 7 : Operating Tech Band */

  FTM_RF_DEBUG_RADIO_CFG_PROP_SUBBAND = 8,         /*!< 8: Radio Bandwidth */

  FTM_RF_DEBUG_RADIO_CFG_PROP_RESERVED = 9,       /*!< 9: Reserved for future use */

  FTM_RF_DEBUG_RADIO_CFG_PROP_CHANNEL = 10,        /*!< 10 : Operating Tech channel */

  FTM_RF_DEBUG_RADIO_CFG_PROP_WAVEFORM = 11,       /*!< 11 : Tx waveform type */

  FTM_RF_DEBUG_RADIO_CFG_PROP_BANDWIDTH = 12,      /*!< 12 : Radio Bandwidth */

  FTM_RF_DEBUG_RADIO_CFG_PROP_NUM_RB = 13,         /*!< 13 : Number of RBs */

  FTM_RF_DEBUG_RADIO_CFG_PROP_START_RB = 14,       /*!< 14 : Start RB Index */

  FTM_RF_DEBUG_RADIO_CFG_PROP_CW_OFFSET = 15,      /*!< 15 : Offset frequency wrt channel to be tuned to*/

  FTM_RF_DEBUG_RADIO_CFG_PROP_NUM                  /*!< Max : Defines maximum number of properties */

} ftm_rf_debug_radio_config_property_type;
      
extern const char *rf_debug_radio_config_property_names[];
      
/*! @} */

/*====================================================================================================================*/
/*!
  @name Tx Control Property ID

  @brief
  Tx Control Property ID list
*/
/*! @{ */
      
/*====================================================================================================================*/
/*!
  @name Rx Override Property ID

  @brief
  Rx OverrideProperty ID list
*/
/*! @{ */
      
/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for FTM Rx Override Command. 
  These enumeration are used to define the content of #ftm_properties_framework_field_property_type in FTM RF Debug command packet. 
rx_override_property_names Must be updated when this list is updated.*/
typedef enum
{
  FTM_RF_DEBUG_RX_OVERRIDE_PROP_UNASSIGNED = 0, /*!< 0 : Unassigned property */

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_PATH_INDEX = 1,  /*!< 1 : RF path index */

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_SIG_PATH = 2,  /*!< 2 : Signal Path for AT forward */ 

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_RFM_DEVICE = 3, /*!< 3 : RFM device for this measurement*/

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_ANTENNA_PORT = 4, /*!< 4 : RFM device for this measurement*/

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_EXPECTED_AGC = 5, /*!< 5 : Expected AGC Value */

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_LNA_GAIN_STATE = 6, /*!< 6 : LNA Gain State */

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_AGC = 7, /*!< 7 : RxAGC value */ 

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_NBEE = 8, /*!< 8 : NBEE value */

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_SIGNAL_TYPE = 9,

  FTM_RF_DEBUG_RX_OVERRIDE_PROP_NUM  /*!< Max : Defines maximum number of properties */

} ftm_rf_debug_rx_override_property_type;

/*! Enumeration to indicate the type of the IQ capture data format */


extern const char *rf_debug_rx_override_property_names[];

/*! @} */

/*====================================================================================================================*/
/*!
  @name Tx override Property ID

  @brief
  Tx Override Property ID list
*/
/*! @{ */

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for FTM TX Override Command. 
  These enumeration are used to define the content of #ftm_properties_framework_field_property_type in FTM RF debug command packet
tx_override_property_names Must be updated when this list is updated.*/
typedef enum
{
  FTM_RF_DEBUG_TX_OVERRIDE_PROP_UNASSIGNED = 0, /*!< 0 : Unassigned property */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_TX_ENABLE = 1, /*!< 1 : Tx Enable */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_PATH_INDEX = 2, /*!< 2 : Path Index */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_SIG_PATH = 3, /*!< 3 : Signal Path*/

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_RFM_DEVICE = 4, /*!< 4 : RFM Device */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_ANT_PATH = 5, /*!< 5 : Antenna Path */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_XPT_MODE = 6, /*!< 6 : XPT Mode */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_IQ_GAIN_ACTION = 7,  /*!< 7 : IQ Gain Action Type */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_IQ_GAIN = 8,  /*!< 8 : IQ Gain */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_TXFE_GAIN_ACTION= 9,  /*!< 9 : TxFE Gain Action Type */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_TXFE_GAIN = 10,  /*!< 10 : TxFE Gain */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_ENV_SCALE_ACTION = 11,  /*!< 11 : Envelope Scale Action Type */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_ENV_SCALE = 12,  /*!< 12 : Envelope Scale */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_RGI = 13,  /*!< 13 : RGI */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_PA_BIAS = 14,  /*!< 14 : PA Bias */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_PA_STATE = 15,  /*!< 15 : PA State*/

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_PA_CURRENT = 16,  /*!< 16 : PA Current */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_DELAY_ACTION = 17,  /*!< 17 : Delay Action Type */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_DELAY = 18,  /*!< 18 : Delay */

  FTM_RF_DEBUG_TX_OVERRIDE_PROP_NUM  /*!< Max : Defines maximum number of properties */

} ftm_rf_debug_tx_override_property_type;

extern const char *rf_debug_tx_override_property_names[];

/*! Enumeration to indicate the type of xpt mode */
typedef enum
{
  FTM_RF_DEBUG_XPT_MODE_APT    = 0, /*!< 0 : APT Mode  */ 
  FTM_RF_DEBUG_XPT_MODE_EPT    = 1, /*!< 1 : EPT Mode  */ 
  FTM_RF_DEBUG_XPT_MODE_ET     = 2, /*!< 1 : ET Mode  */ 
  FTM_RF_DEBUG_XPT_MODE_MAX         /*!< Max */

} ftm_rf_debug_xpt_mode_type;

/*! Enumeration to indicate the type of IQ Gain Action */
typedef enum
{
  FTM_RF_DEBUG_IQ_GAIN_ACTION_SKIP        = 0, /*!< 0 : Skip IQ override  */ 
  FTM_RF_DEBUG_IQ_GAIN_ACTION_OVERRIDE    = 1, /*!< 1 : override with param value  */ 
  FTM_RF_DEBUG_IQ_GAIN_ACTION_SW_DEFAULT  = 2, /*!< 1 : override with sw default  */ 
  FTM_RF_DEBUG_IQ_GAIN_ACTION_NV          = 3, /*!< 1 : override with NV value  */ 
  FTM_RF_DEBUG_IQ_GAIN_ACTION_MAX              /*!< Max */

} ftm_rf_debug_iq_gain_action_type;


/*! Enumeration to indicate the type of TxFe Gain Action */
typedef enum
{
  FTM_RF_DEBUG_TXFE_GAIN_ACTION_SKIP        = 0, /*!< 0 : Skip TxFE override  */ 
  FTM_RF_DEBUG_TXFE_GAIN_ACTION_OVERRIDE    = 1, /*!< 1 : override with param value  */ 
  FTM_RF_DEBUG_TXFE_GAIN_ACTION_SW_DEFAULT  = 2, /*!< 1 : override with sw default  */ 
  FTM_RF_DEBUG_TXFE_GAIN_ACTION_NV          = 3, /*!< 1 : override with NV value  */ 
  FTM_RF_DEBUG_TXFE_GAIN_ACTION_MAX              /*!< Max */

} ftm_rf_debug_txfe_gain_action_type;

/*! Enumeration to indicate the type of Env Scale Action */
typedef enum
{
  FTM_RF_DEBUG_ENV_SCALE_ACTION_SKIP        = 0, /*!< 0 : Skip env scale override  */ 
  FTM_RF_DEBUG_ENV_SCALE_ACTION_OVERRIDE    = 1, /*!< 1 : override with param value  */ 
  FTM_RF_DEBUG_ENV_SCALE_ACTION_SW_DEFAULT  = 2, /*!< 1 : override with sw default  */ 
  FTM_RF_DEBUG_ENV_SCALE_ACTION_NV          = 3, /*!< 1 : override with NV value  */ 
  FTM_RF_DEBUG_ENV_SCALE_ACTION_MAX              /*!< Max */

} ftm_rf_debug_env_scale_action_type;

/*! Enumeration to indicate the type of Delay Action */
typedef enum
{
  FTM_RF_DEBUG_DELAY_ACTION_SKIP        = 0, /*!< 0 : Skip delay override  */ 
  FTM_RF_DEBUG_DELAY_ACTION_OVERRIDE    = 1, /*!< 1 : override with param value  */ 
  FTM_RF_DEBUG_DELAY_ACTION_SW_DEFAULT  = 2, /*!< 1 : override with sw default  */ 
  FTM_RF_DEBUG_DELAY_ACTION_NV          = 3, /*!< 1 : override with NV value  */ 
  FTM_RF_DEBUG_DELAY_ACTION_MAX              /*!< Max */

} ftm_rf_debug_delay_action_type;

/*! Enumeration to indicate the type of tx functionality to perform */
typedef enum
{
  FTM_RF_DEBUG_TX_FUNCTION_SET_TX_STATE     = 0, /*!< 0 : perform set tx state  */ 
  FTM_RF_DEBUG_TX_FUNCTION_SET_TX_OVERRIDE  = 1, /*!< 1 : perform tx override */ 
  FTM_RF_DEBUG_TX_FUNCTION_MAX              /*!< Max */

} ftm_rf_debug_tx_function_type;

/*! @} */


/*====================================================================================================================*/
/*!
  @name IQ Capture Property ID

  @brief
  IQ Capture Property ID list
*/
/*! @{ */

/*--------------------------------------------------------------------------------------------------------------------*/
/*! Enumeration to indicate the type of properties for FTM IQ Capture Command. 
  These enumeration are used to define the content of #ftm_properties_framework_field_property_type in FTM RF Debug command packet
iq_capture_property_names Must be updated when this list is updated.*/
typedef enum
{
  FTM_RF_DEBUG_IQ_CAPTURE_PROP_UNASSIGNED = 0, /*!< 0 : Unassigned property */

  FTM_RF_DEBUG_IQ_CAPTURE_PROP_NUM,  /*!< Max : Defines maximum number of properties */

} ftm_rf_debug_iq_capture_property_type;

extern const char *rf_debug_iq_capture_property_names[];
       


/*--------------------------------------------------------------------------------------------------------------------*/

#ifdef T_WINNT
#error code not present
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* ftm_rf_debug_INTERFACE_H */

