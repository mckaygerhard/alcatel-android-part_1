/*!
  @file
  ftm_rf_debug_interface.c

  @brief
  Common framework to perform radio test in FTM mode
*/

/*======================================================================================================================

  Copyright (c) 2015 - 2016 Qualcomm Technologies Incorporated. All Rights Reserved

  Qualcomm Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document are confidential 
  and proprietary information of Qualcomm Technologies Incorporated and all rights therein are 
  expressly reserved. By accepting this material the recipient agrees that this material and the 
  information contained therein are held in confidence and in trust and will not be used, copied, 
  reproduced in whole or in part, nor its contents revealed in any manner to others without the 
  express written permission of Qualcomm Technologies Incorporated.

======================================================================================================================*/

/*======================================================================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rffactory.mpss/1.0/src/ftm_rf_debug_interface.c#6 $

when       who     what, where, why
--------   ---     -----------------------------------------------------------------------------------------------------
08/22/16   br      Update property names for Radio config
07/27/16   br      Update property names for Radio config
07/25/16   mpt     Initial Release

======================================================================================================================*/

#include "comdef.h"
#include "rflm_defs.h"
#include "ftm_rf_debug_interface.h"
/*--------------------------------------------------------------------------------------------------------------------*/
/*! 
 
 @brief
  Radio Config Property Name Display Info
*/
const char *rf_debug_radio_config_property_names[] = 
{
  "UNASSIGNED",        /* FTM_RF_DEBUG_RADIO_CFG_PROP_UNASSIGNED */
  "IS_TEARDOWN",       /* FTM_RF_DEBUG_RADIO_CFG_PROP_IS_TEARDOWN */
  "RADIO_SETUP_TYPE",  /* FTM_RF_DEBUG_RADIO_CFG_PROP_RADIO_SETUP_TYPE */
  "RFM_DEVICE",        /* FTM_RF_DEBUG_RADIO_CFG_PROP_RFM_DEVICE*/
  "SIG_PATH",          /* FTM_RF_DEBUG_RADIO_CFG_PROP_SIG_PATH */
  "ANT_PATH",          /* FTM_RF_DEBUG_RADIO_CFG_PROP_ANT_PATH */
  "RFM_PATH_TYPE",     /* FTM_RF_DEBUG_RADIO_CFG_PROP_RFM_PATH_TYPE*/
  "BAND",              /* FTM_RF_DEBUG_RADIO_CFG_PROP_BAND */
  "SUBBAND",           /* FTM_RF_DEBUG_RADIO_CFG_PROP_SUBBAND */
  "RESERVED",          /* FTM_RF_DEBUG_RADIO_CFG_PROP_RESERVED */
  "CHANNEL",           /* FTM_RF_DEBUG_RADIO_CFG_PROP_CHANNEL */
  "WAVEFORM",          /* FTM_RF_DEBUG_RADIO_CFG_PROP_WAVEFORM*/
  "BANDWIDTH",         /* FTM_RF_DEBUG_RADIO_CFG_PROP_BANDWIDTH */
  "NUM_RB",            /* FTM_RF_DEBUG_RADIO_CFG_PROP_NUM_RB */
  "START_RB",          /* FTM_RF_DEBUG_RADIO_CFG_PROP_START_RB */
  "CW_OFFSET",         /* FTM_RF_DEBUG_RADIO_CFG_PROP_CW_OFFSET */
};

COMPILE_ASSERT(sizeof(rf_debug_radio_config_property_names)/sizeof(rf_debug_radio_config_property_names[0]) == FTM_RF_DEBUG_RADIO_CFG_PROP_NUM );


/*--------------------------------------------------------------------------------------------------------------------*/
/*!
  @brief
  Rx Override Property Name Display Info
*/
const char *rf_debug_rx_override_property_names[] = 
{
  "UNASSIGNED",           /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_UNASSIGNED */
  "PATH_INDEX",           /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_PATH_INDEX */
  "SIG_PATH",             /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_SIG_PATH */
  "RFM_DEVICE",           /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_RFM_DEVICE */
  "ANTENNA_PORT",         /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_ANTENNA_PORT */
  "EXPECTED_AGC",         /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_EXPECTED_AGC */
  "LNA_GAIN_STATE",       /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_LNA_GAIN_STATE */
  "AGC",                  /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_AGC */
  "NBEE",                 /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_NBEE */
  "SIGNAL_TYPE"           /* FTM_RF_DEBUG_RX_OVERRIDE_PROP_SIGNAL_TYPE */
};

COMPILE_ASSERT(sizeof(rf_debug_rx_override_property_names)/sizeof(rf_debug_rx_override_property_names[0]) == FTM_RF_DEBUG_RX_OVERRIDE_PROP_NUM);

/*--------------------------------------------------------------------------------------------------------------------*/
/*!
  @brief
  Tx Override Property Name Display Info
*/
const char *rf_debug_tx_override_property_names[] = 
{
  "UNASSIGNED",           /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_UNASSIGNED */
  "TX_ENABLE",           /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_TX_ENABLE */
  "PATH_INDEX",           /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_PATH_INDEX */
  "SIG_PATH",             /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_SIG_PATH */
  "RFM_DEVICE",           /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_RFM_DEVICE */
  "ANT_PATH",             /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_ANT_PATH */    
  "XPT_MODE",             /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_XPT_MODE */    
  "IQ_GAIN_ACTION",       /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_IQ_GAIN_ACTION */      
  "IQ_GAIN",              /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_IQ_GAIN */   
  "TXFE_GAIN_ACTION",     /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_TXFE_GAIN_ACTION */ 
  "TXFE_GAIN",            /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_TXFE_GAIN */
  "ENV_SCALE_ACTION",     /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_ENV_SCALE_ACTION */
  "ENV_SCALE",            /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_ENV_SCALE */
  "RGI",                  /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_RGI */
  "PA_BIAS",              /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_PA_BIAS */
  "PA_STATE",             /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_PA_STATE */
  "PA_CURRENT",           /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_PA_CURRENT */
  "DELAY_ACTION",         /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_DELAY_ACTION */    
  "DELAY",                /* FTM_RF_DEBUG_TX_OVERRIDE_PROP_DELAY */   
};

COMPILE_ASSERT(sizeof(rf_debug_tx_override_property_names)/sizeof(rf_debug_tx_override_property_names[0]) == FTM_RF_DEBUG_TX_OVERRIDE_PROP_NUM);


/*--------------------------------------------------------------------------------------------------------------------*/
/*!
  @brief
  IQ Capture Property Name Display Info
*/
const char *rf_debug_iq_capture_property_names[] = 
{
  "UNASSIGNED",           /* FTM_RF_DEBUG_IQ_CAPTURE_PROP_UNASSIGNED */ 
};

COMPILE_ASSERT(sizeof(rf_debug_iq_capture_property_names)/sizeof(rf_debug_iq_capture_property_names[0]) == FTM_RF_DEBUG_IQ_CAPTURE_PROP_NUM);
