#ifndef TRM_DEVICE_ORDERING_H
#define TRM_DEVICE_ORDERING_H

/*===========================================================================

                   T R M    S T R U C T   H E A D E R    F I L E

DESCRIPTION
   This file contains the declaration of TRM data structure.

  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/trm/inc/trm_device_ordering.h#15 $

when         who     what, where, why
--------     ---     ----------------------------------------------------------
10/23/2015   sr      Changes for PRx Hopping on SCELL Deletion(CR: 925022).
08/21/2015   mn      Changes for LTE CA device hopping (CR: 894455).
06/26/2015   rj      Added support for W CA Device Ordering
06/10/2015   ag      Added support for autogen of device order tables
06/10/2015   mn      Device order for HO RxD clients (CR: 851615).
06/10/2015   mn      Added hard-coded device order tables in TRM to support 
                      the new Chile v2 RF card. (CR: 851458).
04/24/2015   ag      Added support for new table format and TDD UL CA card 
02/26/2015   mn      Initial version.
===========================================================================*/

/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include "customer.h"
#include "trm_config_handler.h"

/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typesdefs,
and other items needed by this module.

============================================================================*/

#define TRM_DEVICE_ORDER_SIZE 8
#define TRM_MAX_CONFLICTS_IN_DOT 2
#define TRM_MAX_CA_CONFLICTS_IN_DOT 3
#define TRM_MAX_CA_SUPPORTED 3

#define TRM_DEVICE_ORDER_HO_RX_COMBOS                             ((SYS_BAND_LTE_EUTRAN_BAND255 - SYS_BAND_LTE_EUTRAN_BAND1) + 1)

#define TRM_DEVICE_ORDER_FIXED_DEVICE(device_order, group_val)    (device_order)->groups[0] = group_val; \
                                                                  (device_order)->groups[1] = NO_GROUP;

/* Supported RF HW IDs */
#define RFC_WTR3925_TDD_ULCA_DSDA_QFE4X_HWID 182
#define RFC_WTR3925_TDD_ULCA_V2_HWID 189
#define RFC_WTR2955_CHILECA_V2_HWID  126
//#define RFC_WTR2955_NACA_V2_HWID  124
#define RFC_WTR3925_3DLCA_NAEUKR_1100_HWID  174

#define DDS_SUB TRUE
#define NON_DDS_SUB FALSE
#define DR_ALLOWED TRUE
#define DR_NOT_ALLOWED FALSE
#define ANY_Rx TRUE
#define BEST_Rx FALSE

/* Enumerating the different band groups required for this RFC */
typedef enum
{
  TRM_BG0,
  TRM_BG1,
  TRM_BG2,  
  TRM_BG_MAX
} trm_band_group_enum;

typedef enum
{
  TRM_DEV_ORDER_SET_0,
  TRM_DEV_ORDER_SET_1,
  TRM_DEV_ORDER_SET_2,
  TRM_DEV_ORDER_SET_3,
  TRM_DEV_ORDER_SET_4,
  TRM_DEV_ORDER_SET_5,
  TRM_DEV_ORDER_SET_6,
  TRM_DEV_ORDER_SET_7,
  TRM_DEV_ORDER_SET_8,
  TRM_DEV_ORDER_SET_9,
  TRM_DEV_ORDER_SET_10,
  TRM_DEV_ORDER_SET_GPS,  
  TRM_DEV_ORDER_SET_WLAN,  
  TRM_DEV_ORDER_SET_ALL_TX, 
  TRM_DEV_ORDER_SET_ALL_RX,
  TRM_DEV_ORDER_SET_HO_SECONDARY,
  TRM_DEV_ORDER_SET_MAX
} trm_device_order_set_enum_type;

/*----------------------------------------------------------------------------
  TRM DOT CA Associated Enum Type Info
----------------------------------------------------------------------------*/
typedef enum
{
  TRM_DOT_CA_ASSOCIATED_INVALID = -1,

  /* Not Applicable - Inter Band and Intra-Band (Non-Contiguous) */
  TRM_DOT_CA_ASSOCIATED_NA,

  /* PCell + SCell1 Contiguous */
  TRM_DOT_CA_PCELL_SCELL1_CONTIGUOUS,

  /* PCell + SCell2 Contiguous */
  TRM_DOT_CA_PCELL_SCELL2_CONTIGUOUS,

  /* SCell1 + SCell2 Contiguous */
  TRM_DOT_CA_SCELL1_SCELL2_CONTIGUOUS,

  /* PCell + SCell1 + SCell2 Contiguous */
  TRM_DOT_CA_PCELL_SCELL1_SCELL2_CONTIGUOUS,

  TRM_DOT_CA_ASSOCIATED_MAX
}trm_dot_ca_assoc_enum_type;

typedef struct
{
  trm_group                   groups[TRM_DEVICE_ORDER_SIZE];
  uint32                      set_num;
} trm_device_order_set_list_type;

/* Types for input to the device order tables */
typedef uint16 tech_info_mask_type;
typedef uint64 conc_dot_input_mask_type;

/* Structure for an element in the standalone Device Order Table (DOT) */
typedef struct 
{
  tech_info_mask_type  tech_info_mask;
  trm_group  best_tx_group;
  trm_group best_rx_group;
  uint8 any_rx_set;
} trm_standalone_dot_type;

/* Structure for an element in the concurrency Device Order Table (DOT) */
typedef struct
{
  conc_dot_input_mask_type conc_dot_input_mask; /* contains info mask for conc techs */
  uint8 tech1_any_rx_set;
  uint8 tech2_any_rx_set;
} trm_concurrency_dot_type;

typedef struct
{
  uint32       mask;
  trm_band_t   pcell_band;
  trm_band_t   scell1_band;
  trm_band_t   scell2_band;
  trm_dot_ca_assoc_enum_type   asso_enum;
  boolean is_dr_mode;

  trm_group    pcell_tx_group;
  trm_group    pcell_rx_group;
  trm_group    scell1_tx_group;
  trm_group    scell1_rx_group;
  trm_group    scell2_rx_group;
} trm_ca_dot_type;

typedef struct
{
  trm_client_enum_t  client_id;
  trm_band_t         band;
  boolean            is_ul;
  boolean            is_ho_rx;
}trm_dot_ca_conflict_entry_type;

typedef struct
{
  trm_dot_ca_conflict_entry_type cell[TRM_MAX_CA_CONFLICTS_IN_DOT];
  trm_dot_ca_assoc_enum_type   asso_enum;
  boolean is_dr_mode;
  trm_band_t                     lte_d_band;
}trm_dot_ca_conflict_input_type;

typedef struct
{
  trm_group    pcell_tx_group;
  trm_group    pcell_rx_group;
  trm_group    ho_rx1_group;
  trm_group    ho_rx2_group;
  trm_group    scell1_tx_group;
  trm_group    scell1_rx_group;
  trm_group    scell2_rx_group;
  trm_group    scell2_tx_group;
  trm_group    lte_d_rx_group;
  trm_group    lte_d_tx_group;
}trm_dot_ca_conflict_output_type;

/* Macro for setting a tech info mask */
#define SET_TECH_INFO_MASK(is_dds, client, is_tdd, is_dr_allowed, band_group) ((((band_group) &0xF) << 12)|(((client) &0xF) << 8)|(((is_tdd) &0x3) << 4)|(((is_dr_allowed) & 0x3) << 2) | ((is_dds) & 0x3))
#define SET_CONC_DOT_INPUT_MASK(is_dds1, client1, is_tdd1, band_group1,is_var_resource1, is_dds2, client2, is_tdd2, band_group2, dglna_flag) ( (((uint64)dglna_flag & 0x3 ) << 36) | (((uint64)is_var_resource1 & 0x3 ) << 32) | (SET_TECH_INFO_MASK(is_dds1, client1, is_tdd1, TRUE, band_group1) << 16) | (SET_TECH_INFO_MASK(is_dds2, client2, is_tdd2, TRUE, band_group2)))


typedef struct
{
  boolean is_dds;
  boolean is_tdd;
  trm_rat_group_type rat_group;
  boolean is_dr_allowed;
  uint32 band_group;
  trm_resource_enum_t  resource;
  trm_client_enum_t  client_id;
  trm_band_t         band;
} trm_dot_tech_input_type;

typedef enum
{
  TRM_DOT_STANDALONE_TBL,
  TRM_DOT_CONCURRENCY_TBL,
  TRM_DOT_CA_TBL,
  TRM_DOT_MAX_TBL
}trm_dot_table_type;

typedef struct
{
  trm_group   pcell_ho_rx1_group;
  trm_group   pcell_ho_rx2_group;
} trm_dot_ho_rx_dev_type;
/*============================================================================

                            FUNCTION DECLARATIONS

============================================================================*/

class TrmDeviceOrdering
{
private:

  /* static bootup variables */
  boolean  dot_ran;
  rfm_bands_bitmask *trm_band_mask_per_group;
  trm_device_order_set_list_type *trm_device_order_sets;
  trm_standalone_dot_type *trm_standalone_dot;
  trm_concurrency_dot_type *trm_concurrency_dot;
  trm_ca_dot_type *trm_ca_dot;
  uint32 num_standalone_dot_entries;
  uint32 num_concurrency_dot_entries;
  uint32 num_ca_dot_entries;
  uint8 max_device_order_sets;
  uint8 gps_dos_idx;
  uint8 wlan_dos_idx;
  uint8 all_tx_dos_idx;
  uint8 all_rx_dos_idx;
  uint8 ho_rxd_sec_idx;
  
  /* runtime variables */
  trm_dot_tech_input_type trm_dot_conflict_list[TRM_MAX_CONFLICTS_IN_DOT];
  trm_dot_ca_conflict_input_type trm_dot_ca_conflict_list;
  uint8 num_pri_conflicts;
  uint8 num_ca_conflicts;

  trm_dot_ho_rx_dev_type  ho_rx_combos[TRM_DEVICE_ORDER_HO_RX_COMBOS];
public:
  
  void setup
  (
    uint32 hwid,
    boolean dot_ag_enabled
  );

   void deinit ( void );

  trm_group get_best_group
  (
    trm_client_enum_t             client_id,
    trm_resource_enum_t           resource,
    trm_band_t                    band
  );
  
  trm_band_group_enum get_band_group_for_band
  (
    trm_band_t       sys_band
  );
  
  trm_group  get_best_rx_group
  (
    tech_info_mask_type  tech_info_mask
  );
  
  trm_group get_best_tx_group
  (
    tech_info_mask_type   tech_info_mask
  );
  
  void get_order_list 
  (
    trm_client_enum_t   client,
    trm_device_order_set_list_type* order_set    
  );

  void form_conc_mask_based_on_dds 
  (
    uint64 *conc_mask,
    uint8 *requesting_tech_index
  );

  void get_order_list_from_standalone_dot
  (
    trm_dot_tech_input_type  *client_info,
    boolean mode_concurrent,
    trm_device_order_set_list_type *order_list
  );

  void get_order_list_from_concurrency_dot
  (
    conc_dot_input_mask_type conc_dot_input_mask,
    uint8                    requesting_tech_index,
    trm_device_order_set_list_type* order_list
  );

  void get_order_list_from_tbl
  ( 
    trm_device_order_set_list_type* order_list,
    trm_dot_table_type  tbl_type,
    trm_client_enum_t   client,
    boolean mode_concurrent
  );
  
  void get_order_list_from_ca_dot
  (
    trm_client_enum_t client,
    trm_device_order_set_list_type* order_list
  );

  void get_order_list_for_unsupported_hwid
  (
    trm_client_enum_t   client_id,
    trm_band_t          client_band,
    trm_device_order_set_list_type* order_list 
  );

  trm_dot_ca_assoc_enum_type dev_order_get_assoc_ca_info
  (
    trm_client_enum_t  my_client,
	trm_client_enum_t  shared_client
  );

  boolean get_device_for_ca_combo
  (
    trm_dot_ca_conflict_input_type *ca_combo_in, 
    trm_dot_ca_conflict_output_type *ca_dev_out
  );

  boolean get_device_for_ho_rx
  (
    trm_band_t band,
    trm_dot_ho_rx_dev_type* dev_info
  );

  void populate_ho_rx_combos( void );
};

#endif /* TRM_DEVICE_ORDERING_H */
