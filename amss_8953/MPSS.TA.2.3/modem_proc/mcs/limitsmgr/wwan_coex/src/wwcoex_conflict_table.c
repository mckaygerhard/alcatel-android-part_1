
/*!
  @file
  wwcoex_conflict_table.c

  @brief
  This file implements the conflict table management functions of LIMTSMGR COEX module.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2013-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/limitsmgr/wwan_coex/src/wwcoex_conflict_table.c#21 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
11/18/15   sg      Removed rex call to set signal from FW context
10/16/15   sg      Reset freq IDs before copying new values
10/07/15   jm      Immediately flush out FreqIDs during FW conflict check logging
08/27/15   jm      Featurize out lmtsmgr signaling in FW Standalone Build
08/26/15   mb      Added logging for fw query log packets
08/04/15   sg      Fix for getting the right freq ID for QCTA conflicts
07/20/15   rj      Add history buffer when sending CRAT info to FW
07/14/15   mb      Fixed bug causing incorrect freq ID to be used in RX
07/10/15   rj      Added support for Wwancoex-FW CRAT mode info
06/29/15   jm      IRAT conflict check support
06/16/15   jm      Add history buffer for conflict checks
06/11/15   sg      Fixed bug in bit shifting and added support for
                   providing legacy conflict without a valid table
05/20/15   sg      FW action is alligned to 64 bit boundary 
04/15/15   sg      QXTA support
04/03/15   jm      Allow coex disable for all MSIM modes
01/02/15   sg      EFS based spur simulation support
10/30/14   jm      Interface change to support 4-bit action
10/27/14   jm      KW Fixes
08/22/14   sg      Spur mitigation support
08/18/14   sg      Slot level timeline simulator support
07/17/14   sg      efs based coex simulation support
07/03/14   jm      Support for desense calculation in DR-DSDS
01/14/14   jm      Ensure freqID is 16-bit value
11/23/13   rj      Handle Tech Rx Power Ind after Rx OFF indication
11/22/13   jm      Resolution of LLVM Warnings
07/29/13   ag      Fixed query highest action returning UNKNOWN for hybrid mode
03/23/13   ag      Initial Revision

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/
#include "comdef.h"
#include <stringl/stringl.h>
#include "wwcoex_action_iface.h"
#include "wwcoex_conflict_table.h"
#include "stringl.h"

/*=============================================================================

                            TYPEDEFS

=============================================================================*/


/*=============================================================================

                         INTERNAL VARIABLES

=============================================================================*/
/*! conflict tables */
wwcoex_conflict_tbls_type  wwcoex_conflict_tbls[COEX_MAX_CONCURRENT_SCENARIOS];

/*! Channel conflict tables */
wwcoex_chnl_cflt_table_type wwcoex_chnl_cflt_tbls[WWCOEX_MAX_CHNL_CFLT_TBLS];

/* History buffer of all SW-CXM queries */
wwcoex_query_cflt_hist_type wwcoex_cflt_chk_hist[WWCOEX_MAX_CHNL_QUERY_HIST_SIZE];
wwcoex_query_cflt_hist_type wwcoex_cflt_chk_hist_for_log[WWCOEX_MAX_CHNL_QUERY_HIST_SIZE];
uint8 wwcoex_chnl_hist_index = 0;
uint8 wwcoex_chnl_hist_cnt = 0;

cxm_fw_crat_mode_t wwcoex_crat_mode_hist[WWCOEX_MAX_CHNL_QUERY_HIST_SIZE];
uint8 wwcoex_crat_hist_index = 0;

/*! Spur related globals */
cxm_spur_info_s            wwcoex_spur_info[WWCOEX_MAX_SPUR_TABLE_SIZE];
uint16                     wwcoex_spur_info_idx;

/*! number of active/standby stacks */
uint32   wwcoex_num_standby_stacks    = 0;
uint32   wwcoex_num_active_stacks     = 0;
boolean  wwcoex_is_drdsds_mode        = FALSE;
boolean  wwcoex_is_lmts_task_running  = FALSE;
volatile boolean  wwcoex_is_spur_mitigation_en = FALSE;

/*! simulation input */
volatile uint32   wwcoex_simulation_input      = 0;

/*! hard coded spurs for testing */
cxm_spur_info_s   wwcoex_sim_spur_info[WWCOEX_MAX_CONCURRENT_SPURS];
uint32            wwcoex_sim_spur_count;

/*! SUBs Modes and Callback for FW */
cxm_fw_crat_mode_t       wwcoex_crat_mode;
wwcoexSubsModeCallback   fw_cb    = NULL;

/* channels per channel grouping */
#define WWCOEX_MAX_CHNL_PER_GRP 2

/* defines the incrament we are copying to logs */
#define CHNL_HIST_NEW_ENTRY_PER_LOG 5

/* WWAN COEX chain holder */
cxm_fw_crat_mode_t  wwcoex_rf_crat_mode;

/* Number of freqids packed */
#define WWCOEX_MAX_NUM_PACKED_FREQIDS 4

/*=============================================================================

                                FUNCTIONS

=============================================================================*/
/*===========================================================================
FUNCTION wwcoex_set_sub_state

DESCRIPTION
  This API will update the subscription state

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_set_sub_state
(
  uint32 num_standby_stacks,
  uint32 num_active_stacks
)
{
  wwcoex_num_standby_stacks = num_standby_stacks;
  wwcoex_num_active_stacks = num_active_stacks; 
}

/*===========================================================================
FUNCTION wwcoex_get_sub_state_standby

DESCRIPTION
  This API will retrieve the standby stack value

DEPENDENCIES 
  None

RETURN VALUE  
  uint32

SIDE EFFECTS
  None
  
===========================================================================*/
uint32 wwcoex_get_sub_state_standby()
{
  return wwcoex_num_standby_stacks;
}

/*===========================================================================
FUNCTION wwcoex_set_rf_mode

DESCRIPTION
  This API will update the RF mode

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_set_rf_mode
(
  boolean is_drdsds_enabled
)
{
  wwcoex_is_drdsds_mode = is_drdsds_enabled; 
}

/*===========================================================================
FUNCTION wwcoex_set_spur_mitigation_mask

DESCRIPTION
  This API will update the RF mode

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_set_spur_mitigation_mask
(
  boolean is_spur_mitigation_enabled
)
{
  wwcoex_is_spur_mitigation_en = is_spur_mitigation_enabled; 
}

/*===========================================================================
FUNCTION wwcoex_init_spur_sim

DESCRIPTION
  This API will init the spur simulator with preloaded values

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_init_spur_sim
(
  uint32 sim_input
)
{
  uint32 i;

   /* If the spur EFS is set then use that to override the spur info table */
  if(wwcoex_sim_spur_count > 0)
  {
    for(i=0;i<wwcoex_sim_spur_count;i++)
    {
      wwcoex_spur_info[i].spur_freq_offset      = wwcoex_sim_spur_info[i].spur_freq_offset;
      wwcoex_spur_info[i].spur_freq_uncertainty = 500;
      wwcoex_spur_info[i].victim_freq_id        = 1;
      wwcoex_spur_info[i].spur_level            = wwcoex_sim_spur_info[i].spur_level;
      wwcoex_spur_info[i].notch_depth           = wwcoex_sim_spur_info[i].notch_depth;
      wwcoex_spur_info[i].spur_type             = 2;
      
      wwcoex_spur_info_idx += 1;
    }
  }
  /* This condition is used by FW for VP test.
     FW-CXM overrides MCS behavior using this API for testing spur */
  else if(sim_input > 0)
  {
    /* If SIM is needed populate a dummy entry in the array */
    wwcoex_spur_info[0].spur_freq_offset = -9000;
    wwcoex_spur_info[0].spur_freq_uncertainty = 500;
    wwcoex_spur_info[0].victim_freq_id = 49136;
    wwcoex_spur_info[0].spur_level = -630;
    wwcoex_spur_info[0].notch_depth = 0;
    wwcoex_spur_info[0].spur_type = 2;

    wwcoex_spur_info_idx += 1;

    /* Enable spur mitigation */
    wwcoex_set_spur_mitigation_mask(TRUE);

    /* Override the SIM handle */
    wwcoex_sim_spur_count = 1;
  }
}

/*===========================================================================
FUNCTION wwcoex_init_chnl_fltr_sim

DESCRIPTION
  This API will init the channel and filter conflict

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_init_chnl_fltr_sim
(
  uint32 sim_input,
  uint8  data_tech,
  uint8  idle_tech
)
{
   /* If the spur EFS is set then use that to override the spur info table */
  if(sim_input == 1)
  {
    wwcoex_chnl_cflt_tbls[0].is_valid = TRUE;
    wwcoex_chnl_cflt_tbls[0].tech1 = data_tech;
    wwcoex_chnl_cflt_tbls[0].tech2 = idle_tech;
  }
  else if(sim_input == 2)
  {
    wwcoex_chnl_cflt_tbls[0].is_valid = TRUE;
    wwcoex_chnl_cflt_tbls[0].tech1 = data_tech;
    wwcoex_chnl_cflt_tbls[0].tech2 = idle_tech;
    wwcoex_chnl_cflt_tbls[0].arr[1][2].cflt_type = WWCOEX_CHNL_CFLT_FILTER;
    wwcoex_chnl_cflt_tbls[0].arr[2][1].cflt_type = WWCOEX_CHNL_CFLT_FILTER;
  }
  else if(sim_input == 0)
  {
    memset((void *)wwcoex_chnl_cflt_tbls,0,sizeof(wwcoex_chnl_cflt_tbls));
  }
}


/*===========================================================================
FUNCTION wwcoex_init_tables

DESCRIPTION
  This API will init the wwcoex conflict tables

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_init_tables
(
  uint32 num_standby_stacks,
  uint32 num_active_stacks
)
{
  uint32 i;

  /* Allocate memory for conflict tables */
  for (i=0; i <COEX_MAX_CONCURRENT_SCENARIOS; i++)
  {
    memset(&wwcoex_conflict_tbls[i], 0, sizeof(wwcoex_conflict_tbls_type));

    wwcoex_conflict_tbls[i].current_tbl = WWCOEX_TBL_INVALID;
  }
  
  wwcoex_num_active_stacks = num_active_stacks;
  wwcoex_num_standby_stacks = num_standby_stacks;
}

/*===========================================================================
FUNCTION wwcoex_check_task_state

DESCRIPTION
  This API will inform the caller whether lmtsmgr task is up

DEPENDENCIES 
  None

RETURN VALUE  
  TRUE  - if the lmtsmgr task is up
  FALSE - if the lmtsmgr task is not running

SIDE EFFECTS
  None
  
===========================================================================*/
boolean wwcoex_check_task_state()
{
  return wwcoex_is_lmts_task_running;
}

/*===========================================================================
FUNCTION wwcoex_set_task_state

DESCRIPTION
  This API will set lmtsmgr task up flag

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_set_task_state(boolean task_state)
{
  wwcoex_is_lmts_task_running = task_state;
}

/*===========================================================================
FUNCTION wwcoex_set_rf_chain_holders

DESCRIPTION
  This API will the RF chain holders

DEPENDENCIES 
  None

RETURN VALUE  
  None

SIDE EFFECTS
  None
  
===========================================================================*/
void wwcoex_set_rf_chain_holders
(
  cxm_fw_crat_mode_t  *rf_crat_mode
)
{
  memscpy(&wwcoex_rf_crat_mode, sizeof(cxm_fw_crat_mode_t),
          rf_crat_mode, sizeof(cxm_fw_crat_mode_t));
}

/*=============================================================================

  FUNCTION:  wwcoex_find_match_node

=============================================================================*/
/*!
    @brief
    Find a node with matching techs
 
    @return
    index to the matching node node, MAX otherwise

*/
/*===========================================================================*/
uint32 wwcoex_find_match_node
(
  uint8 tech1,
  uint8 tech2
)
{
  uint32 index;

  /* Check if this tech combination already has a node */
  for (index=0; index<COEX_MAX_CONCURRENT_SCENARIOS; index++)
  {
    if ( wwcoex_conflict_tbls[index].current_tbl == WWCOEX_TBL_INVALID )
    {
      continue;
    }

    if ( ( (wwcoex_conflict_tbls[index].tech1 == tech1) && 
           (wwcoex_conflict_tbls[index].tech2 == tech2) ) ||
         ( (wwcoex_conflict_tbls[index].tech1 == tech2) &&
           (wwcoex_conflict_tbls[index].tech2 == tech1) ) )
    {
      /* Found a match */
      break;
    }
  }

  return index;
}

/*=============================================================================

  FUNCTION:  wwcoex_get_table_node

=============================================================================*/
/*!
    @brief
    Get the conflict table node corresponding to the two tech pair. If there
    is no table for the pair, then return an unused node.
 
    @return
    conflict table node

*/
/*===========================================================================*/
wwcoex_conflict_tbls_type* wwcoex_get_table_node
(
  uint8   tech1,
  uint8   tech2
)
{
  uint32 index;
  wwcoex_conflict_tbls_type* tbl_node=NULL;

  /* Check if there already exists a node for this tech combination */
  index = wwcoex_find_match_node(tech1, tech2);

  if (index >= COEX_MAX_CONCURRENT_SCENARIOS)
  {
    /* Could not find a match */

    /* Check if there is anything unused */
    for (index = 0; index < COEX_MAX_CONCURRENT_SCENARIOS; index++ )
    {
      if ( wwcoex_conflict_tbls[index].current_tbl == WWCOEX_TBL_INVALID )
      {
        tbl_node = &wwcoex_conflict_tbls[index];
        tbl_node->tech1 = tech1;
        tbl_node->tech2 = tech2;
        break; 
      }
    }
  }
  else
  {
    tbl_node = &wwcoex_conflict_tbls[index];
  }

  return tbl_node;
}

/*=============================================================================

  FUNCTION:  wwcoex_invalidate_tables

=============================================================================*/
/*!
    @brief
    Invalidates the conflict tables containing the specified tech
 
    @return
    NONE

*/
/*===========================================================================*/
void wwcoex_invalidate_tables
(
  uint8  tech
)
{
  uint8 i,j;

  /* Find nodes that have this tech and invalidate them */
  for (i=0; i<COEX_MAX_CONCURRENT_SCENARIOS; i++)
  {
    if (wwcoex_conflict_tbls[i].tech1 == tech || 
        wwcoex_conflict_tbls[i].tech2 == tech)
    {
      wwcoex_conflict_tbls[i].current_tbl = WWCOEX_TBL_INVALID;

      for (j=0; j<WWCOEX_MAX_TABLES_PER_SCENARIO; j++)
      {
        wwcoex_conflict_tbls[i].tables[j].tech1_num_entries = 0;
        wwcoex_conflict_tbls[i].tables[j].tech2_num_entries = 0;
      }
    }
  }
}

/*=============================================================================

  FUNCTION:  wwcoex_get_oldest_tbl_index

=============================================================================*/
/*!
    @brief
    Returns index to the oldest table
 
    @return
    NONE

*/
/*===========================================================================*/
int8 wwcoex_get_oldest_tbl_index
(
  wwcoex_conflict_tbls_type*  conflict_tbl
)
{
  int8 index;

  if (conflict_tbl->current_tbl < 0)
  {
    /* Table node is not valid... so use the first one */
    index = 0;
  }
  else
  {
    index = (conflict_tbl->current_tbl + 1) % WWCOEX_MAX_TABLES_PER_SCENARIO ;
  }

  return index;
}

/*=============================================================================

  FUNCTION:  wwcoex_tbl_freqid_pair_lookup

=============================================================================*/
/*!
    @brief
    Search for a freq id pair in the conflict tables and return the action
 
    @return
    NONE

*/
/*===========================================================================*/
cxm_action_type wwcoex_tbl_freqid_pair_lookup
(
  uint32 freqid1,
  uint32 freqid2
)
{
  cxm_action_type action = ACTION_UNKNOWN;
  int index1, index2;
  uint32 i,j;
  wwcoex_tbl_type* tbl_ptr;
  boolean match_found = FALSE;

  for (i=0; i<COEX_MAX_CONCURRENT_SCENARIOS; i++)
  {
    /* Check if node is valid */
    if (wwcoex_conflict_tbls[i].current_tbl == WWCOEX_TBL_INVALID)
    {
      continue;
    }

    /* Node is valid so scan both tables. Start for current table first since
       that has more chances of finding a match */
    j = wwcoex_conflict_tbls[i].current_tbl;
    do
    {
      tbl_ptr =  &wwcoex_conflict_tbls[i].tables[j];

      /* Is the table valid ? */
      if ( (tbl_ptr->tech1_num_entries > 0) &&
           (tbl_ptr->tech2_num_entries > 0) )
      {
        index1 = freqid1 - tbl_ptr->tech1_fid_offset;
        index2 = freqid2 - tbl_ptr->tech2_fid_offset;

        if ( (index1 < 0) || (index1 >= tbl_ptr->tech1_num_entries) || 
             (index2 < 0) || (index2 >= tbl_ptr->tech2_num_entries)  )
        {
          /* Try the other way around */
          index1 = freqid2 - tbl_ptr->tech1_fid_offset;
          index2 = freqid1 - tbl_ptr->tech2_fid_offset;

          if ( (index1 >= 0) && (index1 < tbl_ptr->tech1_num_entries) &&
               (index2 >= 0) && (index2 < tbl_ptr->tech2_num_entries)  )
          {
            /* Found a match... */
            action = tbl_ptr->arr[index1][index2].action;
            match_found = TRUE;
            break;
          }
        }
        else
        {
          /* Found a match... */
          action = tbl_ptr->arr[index1][index2].action;
          match_found = TRUE;
          break;
        }
      }
      j = (j + 1) % WWCOEX_MAX_TABLES_PER_SCENARIO;
    }while (j != wwcoex_conflict_tbls[i].current_tbl);

    if (match_found == TRUE)
    {
      break;
    }
  }

  return action;
}


/*=============================================================================

  FUNCTION:  wwcoex_tbl_freqid_pair_spur_lookup

=============================================================================*/
/*!
    @brief
    Search for a freq id pair in the conflict tables and return the spur 
    If the spur exists then the spur_info will be populated with the 
    right values
 
    @return
    NONE

*/
/*===========================================================================*/
cxm_action_type wwcoex_tbl_freqid_pair_spur_lookup
(
  uint32            freqid1,
  uint32            freqid2,
  uint16           *handle
)
{
  cxm_action_type spur_action = ACTION_SPUR_UNKNOWN;
  int index1, index2;
  uint32 i,j;
  wwcoex_tbl_type* tbl_ptr;
  boolean match_found = FALSE;

  /* initialize handle to unknown */
  *handle = WWCOEX_SPUR_HANDLE_UNKNOWN;

  for (i=0; i<COEX_MAX_CONCURRENT_SCENARIOS; i++)
  {
    /* Check if node is valid */
    if (wwcoex_conflict_tbls[i].current_tbl == WWCOEX_TBL_INVALID)
    {
      continue;
    }

    /* Node is valid so scan both tables. Start for current table first since
       that has more chances of finding a match */
    j = wwcoex_conflict_tbls[i].current_tbl;
    do
    {
      tbl_ptr =  &wwcoex_conflict_tbls[i].tables[j];

      /* Is the table valid ? */
      if ( (tbl_ptr->tech1_num_entries > 0) &&
           (tbl_ptr->tech2_num_entries > 0) )
      {
        index1 = freqid1 - tbl_ptr->tech1_fid_offset;
        index2 = freqid2 - tbl_ptr->tech2_fid_offset;

        if ( (index1 < 0) || (index1 >= tbl_ptr->tech1_num_entries) || 
             (index2 < 0) || (index2 >= tbl_ptr->tech2_num_entries)  )
        {
          /* Try the other way around */
          index1 = freqid2 - tbl_ptr->tech1_fid_offset;
          index2 = freqid1 - tbl_ptr->tech2_fid_offset;

          if ( (index1 >= 0) && (index1 < tbl_ptr->tech1_num_entries) &&
               (index2 >= 0) && (index2 < tbl_ptr->tech2_num_entries)  )
          {
            /* Found a match... */
            *handle = tbl_ptr->arr[index1][index2].spur_handle_2;
            if(*handle == WWCOEX_SPUR_HANDLE_NONE)
            {
              spur_action = ACTION_NONE;
            }
            else if(*handle == WWCOEX_SPUR_HANDLE_UNKNOWN)
            {
              spur_action = ACTION_SPUR_UNKNOWN;
            }
            else
            {
              spur_action = ACTION_SPUR;
            }
            match_found = TRUE;
            break;
          }
        }
        else
        {
          /* Found a match... */
          *handle = tbl_ptr->arr[index1][index2].spur_handle_1;
          if(*handle == WWCOEX_SPUR_HANDLE_NONE)
          {
            spur_action = ACTION_NONE;
          }
          else if(*handle == WWCOEX_SPUR_HANDLE_UNKNOWN)
          {
            spur_action = ACTION_SPUR_UNKNOWN;
          }
          else
          {
            spur_action = ACTION_SPUR;
          }
          match_found = TRUE;
          break;
        }
      }
      j = (j + 1) % WWCOEX_MAX_TABLES_PER_SCENARIO;
    }while (j != wwcoex_conflict_tbls[i].current_tbl);

    if (match_found == TRUE)
    {
      break;
    }
  }

  return spur_action;
}

/*=============================================================================

  FUNCTION:  wwcoex_tbl_channel_conflict_lookup

=============================================================================*/
/*!
    @brief
    Search for a freq id pair in the channel conflict tables and 
    return the action
 
    @return
    cxm_action_type

*/
/*===========================================================================*/
boolean wwcoex_is_chnl_cflt_table_valid
(
  uint32 tech1,
  uint32 tech2,
  uint32 *index
)
{
  uint32 i;
  wwcoex_chnl_cflt_table_type *cflt_tbl;
  boolean result = FALSE;

  /* Check if input is valid */
  if((tech1 >= (uint32)CXM_FW_INTERNAL) ||
     (tech2 >= (uint32)CXM_FW_INTERNAL))
  {
    return FALSE;
  }

  /* Find a valid table:
       - To support IRAT, the table should be tech agnostic
       - The assumption is that there can only be one QTA at a time
         So there will alway be one active table only
 */
  for(i=0;i<WWCOEX_MAX_CHNL_CFLT_TBLS;i++)
  {
    cflt_tbl = &(wwcoex_chnl_cflt_tbls[i]);

    /* check if the table is valid */
    if(cflt_tbl->is_valid == FALSE)
    {
      continue;
    }

    *index = i;
    result = TRUE;
    break;
  }

  return result;

}


/*=============================================================================

  FUNCTION:  wwcoex_cflt_tbl_lookup

=============================================================================*/
/*!
    @brief
    Search for a freq id pair in the channel conflict tables and 
    desense tables and return the action
 
    @return
    cxm_action_type

*/
/*===========================================================================*/
static cxm_action_type wwcoex_cflt_tbl_lookup
(
  uint32 index,
  uint32 freqid1,
  uint32 freqid2,
  uint32 ch_id1,
  uint32 ch_id2,
  wwcoex_cflt_type  cflt_type
)
{
  cxm_action_type action = ACTION_NONE;
  wwcoex_chnl_cflt_table_type *cflt_tbl;
  wwcoex_chnl_cflt_result_type *arr;
  uint32 i;

  switch (cflt_type)
  {
    case WWCOEX_CFLT_FILTER:
    {
      if(index < WWCOEX_MAX_CHNL_CFLT_TBLS)
      {
        cflt_tbl = &(wwcoex_chnl_cflt_tbls[index]);
        arr = &(cflt_tbl->arr[ch_id1][ch_id2]);
      
        if(arr->cflt_type == WWCOEX_CHNL_CFLT_FILTER)
        {
          action = ACTION_FILTER_CONFLICT;
        }
      }
      break;
    }

    case WWCOEX_CFLT_CHANNEL:
    {
      if(ch_id1 == ch_id2)
      {
        action = ACTION_CHANNEL_CONFLICT;
      }
      else
      {
        if(index < WWCOEX_MAX_CHNL_CFLT_TBLS)
        {
          cflt_tbl = &(wwcoex_chnl_cflt_tbls[index]);
          arr = &(cflt_tbl->arr[ch_id1][ch_id2]);
          
          if(arr->cflt_type == WWCOEX_CHNL_CFLT_LEGACY)
          {
            action = ACTION_CHANNEL_CONFLICT;
          }
          else if(arr->cflt_type == WWCOEX_CHNL_CFLT_CARRIER)
          {
            if((arr->freq_id == freqid1) ||
               (arr->freq_id == freqid2))
            {
              action = ACTION_CHANNEL_CONFLICT;
              break;
            }
          }
        }
      }
      break;
    }

    case WWCOEX_CFLT_DESENSE:
    {
      if((freqid1 == WWCOEX_UNKNOWN_FREQID) ||
         (freqid2 == WWCOEX_UNKNOWN_FREQID))
      {
        action = ACTION_UNKNOWN;
      }
      else
      {
        action = wwcoex_tbl_freqid_pair_lookup(
                   freqid1,
                   freqid2
                 );
      }
      break;
    }

    default:
      break;
  }

  return action;

}

/*=============================================================================

  FUNCTION:  wwcoex_tbl_freqid_lookup

=============================================================================*/
/*!
    @brief
    Search for a freq id in the conflict tables and return the MAX action
 
    @return
    NONE

*/
/*===========================================================================*/
cxm_action_type wwcoex_tbl_freqid_lookup
(
  uint32 freqid1
)
{
  cxm_action_type action = ACTION_NONE;
  int index1, index2;
  uint32 i,j,num_scenarios=COEX_MAX_CONCURRENT_SCENARIOS;
  wwcoex_tbl_type* tbl_ptr;
  boolean found = FALSE;

  for (i=0; i<COEX_MAX_CONCURRENT_SCENARIOS; i++)
  {
    /* Check if node is valid */
    if (wwcoex_conflict_tbls[i].current_tbl == WWCOEX_TBL_INVALID)
    {
      num_scenarios--;
      continue;
    }

    /* Node is valid so scan both tables. Start for current table first since
       that has more chances of finding a match */
    j = wwcoex_conflict_tbls[i].current_tbl;
    do
    {
      tbl_ptr =  &wwcoex_conflict_tbls[i].tables[j];

      /* Is the table valid ? */
      if ( (tbl_ptr->tech1_num_entries > 0) &&
           (tbl_ptr->tech2_num_entries > 0) )
      {
        /* Check first which columns does the index map to */
        index1 = freqid1 - tbl_ptr->tech1_fid_offset;
        if ( (index1 < 0) || (index1 >= tbl_ptr->tech1_num_entries) )
        {
          /* Try the other way around */
          index2 = freqid1 - tbl_ptr->tech2_fid_offset;

          if ( (index2 >= 0) && (index2 < tbl_ptr->tech2_num_entries) )
          {
            /* Its the secondary index.. So go through all the entries in first column. */
            for (index1 = 0; index1 < tbl_ptr->tech1_num_entries; index1++)
            {
              action = MAX((uint8)action, (uint8)(tbl_ptr->arr[index1][index2].action));
            }

            /* found id in this table so no need to check other table */
            found = TRUE;
            break;
          }
          //else cannot find the freqId in this table.. do nothing.
        }
        else
        {
          /* Its the primary index.. So go through all the entries in second column. */
          for (index2 = 0; index2 < tbl_ptr->tech2_num_entries; index2++)
          {
            action = MAX((uint8)action, (uint8)(tbl_ptr->arr[index1][index2].action));
          }

          /* found id in this table so no need to check other table */
          found = TRUE;
          break;
        }
      }
      j = (j + 1) % WWCOEX_MAX_TABLES_PER_SCENARIO;
    }while (j != wwcoex_conflict_tbls[i].current_tbl);
  }

  /* if not found in any tables and there is table for atleast one scenario, then return UNKNOWN... */
  if ((found==FALSE) && (num_scenarios > 0))
  {
    action = ACTION_UNKNOWN;
  }

  return action;
}



/*============================================================================

FUNCTION WWCOEX_SET_CRAT_MODE

DESCRIPTION
  Query CRAT Mode to get what system is capable of
  
DEPENDENCIES
  None

RETURN VALUE
  Returns BitMask for Sub1+Sub2 techs

   Single SIM Capability
    L only  = (crat_info.instance0_lte = 0x1) 
    1X only = (crat_info.instance0_1x = 0x1) 
    W only  = (crat_info.instance0_wcdma = 0x1) 
    SLTE (L+1X) = ((crat_info.instance0_lte = 0x1) | (crat_info.instance1_1x = 0x1))
    SRLTE (L+1X QTA) = ((crat_info.instance0_lte = 0x1) | (crat_info.instance1_1x = 0x1))
  
  Multi SIM Capability
    L+W = ((crat_info.instance0_lte = 0x1) | (crat_info.instance1_wcdma = 0x1))
    L+1X = ((crat_info.instance0_lte = 0x1) | (crat_info.instance1_1x = 0x1))


============================================================================*/
void wwcoex_set_crat_mode( cxm_fw_crat_mode_t crat_mode )
{

  memscpy(&wwcoex_crat_mode, sizeof(cxm_fw_crat_mode_t),
          &crat_mode, sizeof(cxm_fw_crat_mode_t));
}


/*============================================================================

FUNCTION CXM_QUERY_HIGHEST_ACTION

DESCRIPTION
  Query the highest mitigation action for one freqId
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_highest_action
(
  cxm_highest_action_query_s* query_st
)
{
  int i;

  /* Check input... */
  if ( query_st == NULL || query_st->actions == NULL ||
       query_st->num_requesting_ids == 0 || query_st->requesting_ids == NULL || 
       query_st->num_requesting_ids > WWCOEX_MAX_REQUESTING_IDS )
  {
    return FALSE;
  }

  /* SW Coex mitigation is disabled */
  if (wwcoex_num_standby_stacks == 0)
  {
    memset((void *)(query_st->actions), 0, 
           query_st->num_requesting_ids * sizeof(cxm_action_type));
    return TRUE;
  }

  /* Scan through each entry in the table */
  for (i=0; i < query_st->num_requesting_ids; i++)
  {
    /* If any of the requesting Ids are unknown then return action as unknown */
    if ( (query_st->requesting_ids[i]) == WWCOEX_UNKNOWN_FREQID )
    {
      query_st->actions[i] = ACTION_UNKNOWN;
    }
    else
    {
       /* Lookup conflict table for freqids*/
       query_st->actions[i] = wwcoex_tbl_freqid_lookup(query_st->requesting_ids[i]);
    }
  }

  return TRUE;
}

/*============================================================================

FUNCTION CXM_PROCESS_FREQ_ID_FOR_SIMULATOR

DESCRIPTION
  Provides the selective blanking decision based on the freq ID of requesting
  and conflicting tech.
  
DEPENDENCIES
  None

RETURN VALUE
  cxm_action_type

SIDE EFFECTS
  None

============================================================================*/
static cxm_action_type cxm_process_freq_id_for_simulator
(
  uint32 req_id,
  uint32 conflict_id,
  cxm_action_query_s* query_st
)
{
  wwan_coex_activity_type  x_activity = ACTIVITY_INVALID;
  wwan_coex_activity_type  g_activity = ACTIVITY_INVALID;
  cxm_action_type action = ACTION_NONE;

  /* Since GSM is simulated, there is no query from GSM
     So the requesting tech is always 'X' and conflicting tech is GSM 
  */
  x_activity = query_st->requesting_activity;
  g_activity = query_st->conflicting_activity;

  switch(wwcoex_simulation_input)
  {
    case 1: /*X Rx - G Tx */
      if((x_activity == ACTIVITY_RX) &&
         (g_activity == ACTIVITY_TX))
      {
        action = ACTION_BLANK;
      }
      break;
    
    case 2: /*X Tx - G Rx */
      if((x_activity == ACTIVITY_TX) &&
         (g_activity == ACTIVITY_RX))
      {
        action = ACTION_BLANK;
      }
      break;

    case 3: /*X Tx - G Tx */
      if((x_activity == ACTIVITY_TX) &&
         (g_activity == ACTIVITY_TX))
      {
        action = ACTION_BLANK;
      }
      break;
      
    case 4:
      if(((x_activity == ACTIVITY_RX) &&
          (g_activity == ACTIVITY_TX)) ||
         ((x_activity == ACTIVITY_TX) &&
          (g_activity == ACTIVITY_TX)))
      {
        action = ACTION_BLANK;
      }
      break;
        
    default:
      action = ACTION_UNKNOWN;
  }

  return action;
}


/*============================================================================

FUNCTION CXM_QUERY_ACTION_FOR_SIMULATOR

DESCRIPTION
  Query the mitigation action corresponding to the two techs freq combination.
  The result provided by this API is based on simulation EFS input instead
  of the conflict table for the 2 techs.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
static boolean cxm_query_action_for_simulator
(
  cxm_action_query_s* query_st
)
{
  int i,j;
  boolean blanket_decision = FALSE;
  cxm_action_type action = ACTION_NONE;
  cxm_action_type action_func = ACTION_NONE;

  /*-------------------------------------
    Tech1   Tech2   Result  EFS entry
    -------------------------------------
    X RX    G TX    Blank   0X01
    X TX    G RX    Blank   0X02
    X TX    G TX    Blank   0X03
    
    X TX    X TX    Blank   0X10
    X TX    X RX    Blank   0X11
    X TX    X X     Blank   0X12

    X X     X X     Blank   0X100
  -------------------------------------*/ 

  /* Invalid input to simulator */
  if((wwcoex_simulation_input == 0) ||
     (wwcoex_simulation_input > 0x100))
  {
    action = ACTION_UNKNOWN;
    blanket_decision = TRUE;
  }

  /* Always blank inout to simulator */
  else if (wwcoex_simulation_input == 0x100)
  {
    action = ACTION_BLANK;
    blanket_decision = TRUE;
  }

  /* Selective blanking but not tech based */
  else if (wwcoex_simulation_input >= 0x10)
  {
    blanket_decision = TRUE;
    
    switch (wwcoex_simulation_input)
    {
      case 0x10: /* TX - TX blanking */
        if((query_st->requesting_activity == ACTIVITY_TX) &&
           (query_st->conflicting_activity == ACTIVITY_TX))
        {
          action = ACTION_BLANK;
        }
        break;

      case 0x11: /* TX - RX blanking */
        if(((query_st->requesting_activity == ACTIVITY_TX) &&
            (query_st->conflicting_activity == ACTIVITY_RX)) ||
           ((query_st->conflicting_activity == ACTIVITY_TX) &&
            (query_st->requesting_activity == ACTIVITY_RX)))
        {
          action = ACTION_BLANK;
        }
        break;

      case 0x12: /* TX - XX blanking */
        if((query_st->requesting_activity == ACTIVITY_TX) ||
           (query_st->conflicting_activity == ACTIVITY_TX) )
        {
          action = ACTION_BLANK;
        }
        break;

      default:
        action = ACTION_UNKNOWN;
        break;
    }
  }

  /* Selective blanking based on GSM state */
  else
  {
    for (i=0; i < query_st->num_requesting_ids; i++)
    {
      for (j=0; j < query_st->num_conflicting_ids; j++)
      {
        action_func = cxm_process_freq_id_for_simulator
                      (query_st->requesting_ids[i],
                       query_st->conflicting_ids[j],
                       query_st);
        
        action = MAX(action, action_func);
      }
      /* Store the action into the mask */
      query_st->actions |= (action << (WWCOEX_ACTION_BIT_SIZE*i));
    } 
  }

  /* If the decision is not frequency ID specific then 
     apply the blanket decision to all */
  if(blanket_decision)
  {
    for (i=0; i < query_st->num_requesting_ids; i++)
    {
      query_st->actions |= (action << (WWCOEX_ACTION_BIT_SIZE*i));
    }
  }

  return TRUE;
}


/*============================================================================

FUNCTION CXM_COPY_CONFLICT_CHECK_HIST

DESCRIPTION
  Copies the query information over to a history buffer
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
static void cxm_copy_conflict_check_hist
(
  cxm_conflict_check_s   *query_st,
  wwcoex_cflt_type        cflt_type
)
{
  uint16 *req_id_ptr, *conf_id_ptr;
  uint8  j;

  /* Check input */
  if (query_st == NULL)
  {
    return;
  }

  /* Copy parameters to history buffer */
  memscpy(&wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].input_params, sizeof(cxm_conflict_check_s),
          query_st, sizeof(cxm_conflict_check_s));
  wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].cflt_type = cflt_type;

  req_id_ptr = query_st->req_ids;
  conf_id_ptr = query_st->conf_ids;

  /* Reset the freq ID before copying the new 64 bit value */
  wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].req_ids_packed = 0;
  wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].conf_ids_packed = 0;
  
  /* Collect and pack the freqids before TCM overlay occurs */
  for (j = 0; j<WWCOEX_MAX_NUM_PACKED_FREQIDS; j++)
  {
    if ((req_id_ptr == NULL) || (conf_id_ptr == NULL))
    {
      break;
    }

    /* Retrieve the freqid */
    wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].req_ids_packed |= (*req_id_ptr & 0xFFFF);
    wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].conf_ids_packed |= (*conf_id_ptr & 0xFFFF);

    /* Shift the value to re-pack into itself */
    if (j < (WWCOEX_MAX_NUM_PACKED_FREQIDS-1))
    {
      wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].req_ids_packed = 
        wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].req_ids_packed << 16;
      wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].conf_ids_packed = 
        wwcoex_cflt_chk_hist[wwcoex_chnl_hist_index].conf_ids_packed << 16;
    }

    /* Shift the ptrs */
    req_id_ptr++;
    conf_id_ptr++;
  }

  /* update the chnl hist cnt */
  wwcoex_chnl_hist_cnt++;

  /* After we have populated the entire wwcoex_cflt_chk_hist array, we copy it for logging, and signal for the fw query log */ 
  /* We also copy every CHNL_HIST_NEW_ENTRY_PER_LOG entries until the first full packet is sent. */
  if ((wwcoex_chnl_hist_index == WWCOEX_MAX_CHNL_QUERY_HIST_SIZE -1) || 
    (wwcoex_chnl_hist_cnt<WWCOEX_MAX_CHNL_QUERY_HIST_SIZE && wwcoex_chnl_hist_cnt>0 && wwcoex_chnl_hist_cnt%CHNL_HIST_NEW_ENTRY_PER_LOG == 0)) 
  { 
    memscpy(&wwcoex_cflt_chk_hist_for_log, sizeof(wwcoex_cflt_chk_hist_for_log) , &wwcoex_cflt_chk_hist, sizeof(wwcoex_cflt_chk_hist_for_log)); 
  } 

  /* Circular buffer reset */
  wwcoex_chnl_hist_index = ((wwcoex_chnl_hist_index + 1) % WWCOEX_MAX_CHNL_QUERY_HIST_SIZE);

}

/*============================================================================

FUNCTION CXM_CHECK_CONFLICT

DESCRIPTION
  This API checks for channel or filter conflicts from a FW query.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_check_conflict
(
  cxm_conflict_check_s   *query_st,
  wwcoex_cflt_type        cflt_type
)
{
  uint32 req_ch_id, conf_ch_id;
  uint64 shift,temp_result;
  boolean is_vaid_table;
  uint32 i = 0,j = 0;
  cxm_action_type action = ACTION_NONE, action_func = ACTION_NONE;
  uint32 req_loop  =0,  req_car_per_ch =0,  req_chnl =0,  req_car =0;
  uint32 conf_loop =0, conf_car_per_ch =0, conf_chnl =0, conf_car =0;
  uint32 req_chnl_idx =0, req_car_idx =0, total_req_car =0, total_conf_car =0;
  int32  conf_chnl_idx = 0, conf_car_idx = 0;
  int32 req_freq_idx = 0, conf_freq_idx = 0;
  int32 req_chnl_grp_car_sum = 0, conf_chnl_grp_car_sum = 0;
  uint32 index = 0xFF;

   /* Check input */
  if ( query_st == NULL                   ||
       query_st->num_conf_ch == 0  || query_st->conf_ids == NULL   ||
       query_st->num_req_ch == 0   || query_st->req_ids == NULL
     )
  {
    return FALSE;
  }

  /*Reset the mask and handle*/
  query_st->action  = ACTION_NONE;

  /* Check if there is any QTA going on between the tech pair */
  is_vaid_table = wwcoex_is_chnl_cflt_table_valid(
                    query_st->req_tech,
                    query_st->conf_tech,
                    &index
                    );

  /* If the requesting and conflicting types are not RXRX then
     there is no filter conflict */
  if(((query_st->req_act != ACTIVITY_RX)||
        (query_st->conf_act != ACTIVITY_RX)) &&
        (cflt_type == WWCOEX_CFLT_FILTER))
  {
    return TRUE;
  }
  /* If the requesting and conflicting types are both RX then
     there is no desense */
  if((query_st->req_act == ACTIVITY_RX)&&
     (query_st->conf_act == ACTIVITY_RX) &&
     (cflt_type == WWCOEX_CFLT_DESENSE))
  {
    return TRUE;
  }

  /* The input from FW is vectorized and asymetric 
     num_req_ch -> This field is a bitmask. bit is set of each
                   active channel
     num_carr_per_req_ch -> This field is for a channel pair.
                            Informs number of channel for that pair.
                            Uses 2 bits per channel pair.
     req_ch_id -> This field stores the channel ID.
                  Uses 3 bits for each channel ID
     action -> Result is stored in action.
               Action is per channel per carrier.
               Each action is 4 bits                            
  */

  /* Copy FW entries to local variables */
  req_chnl = query_st->num_req_ch;
  conf_chnl = query_st->num_conf_ch;

  req_car = query_st->num_carr_per_req_ch;
  conf_car = query_st->num_carr_per_conf_ch;

  /* Extract the number of times to loop through request and 
     conflict entries */
  while(req_chnl)
  {
    if(req_chnl & WWCOEX_CHNL_BIT_MASK)
    {
      req_loop = 
        req_loop +(req_car & WWCOEX_CAR_PER_CHNL_BIT_MASK);
      req_car_per_ch |= 
        ((req_car & WWCOEX_CAR_PER_CHNL_BIT_MASK)<<
         (i*WWCOEX_CAR_PER_CHNL_BIT_SIZE));
    }
    i+=1;
    req_chnl = req_chnl >> WWCOEX_CHNL_BIT_SIZE;
    
    if(i%2 == 0)
    {
      req_car  = 
        req_car >> WWCOEX_CAR_PER_CHNL_BIT_SIZE;
    }

  }
  while(conf_chnl)
  {
    if(conf_chnl & WWCOEX_CHNL_BIT_MASK)
    {
      conf_loop = 
        conf_loop +(conf_car & WWCOEX_CAR_PER_CHNL_BIT_MASK);
      conf_car_per_ch |= 
        ((conf_car & WWCOEX_CAR_PER_CHNL_BIT_MASK)<<
         (j*WWCOEX_CAR_PER_CHNL_BIT_SIZE));
    }
    j+=1;
    conf_chnl = conf_chnl >> WWCOEX_CHNL_BIT_SIZE;

    if(j%2 == 0)
    {
      conf_car  = 
        conf_car >> 
        (WWCOEX_CAR_PER_CHNL_BIT_SIZE*(WWCOEX_CAR_PER_CHNL_BIT_SIZE/2));
    }
  }

  /* Copy FW entries to local variables */
  req_chnl = query_st->num_req_ch;
  conf_chnl = query_st->num_conf_ch;

  req_car = query_st->num_carr_per_req_ch;
  conf_car = query_st->num_carr_per_conf_ch;

  /* Start with channel ID of 0 and carr of -1 */
  req_chnl_idx = 0;
  req_car_idx = -1;

  /* Start with number of carriers on the first channel */
  total_req_car  = req_car & WWCOEX_CAR_PER_CHNL_BIT_MASK;
  
  for(i=0;i<req_loop;i++)
  {
    /* Start with no action */
    action = ACTION_NONE;

    /* Extract the channel index and carrier index
       each time the loop is run */
    if(i<total_req_car)
    {
      req_car_idx += 1;
    }
    else
    {
      req_chnl_idx += 1;
      req_car_idx = 0;
      total_req_car += 
        (req_car_per_ch>>(WWCOEX_CAR_PER_CHNL_BIT_SIZE*req_chnl_idx))&
        WWCOEX_CAR_PER_CHNL_BIT_MASK;
    }

    /* Get the channel ID based on the channel Index */
    req_ch_id = 
      (query_st->req_ch_id >> (req_chnl_idx*WWCOEX_CHNL_ID_BIT_SIZE))&
      WWCOEX_CHNL_ID_BIT_MASK;

    /* Start with channel ID of 0 and carr of -1 */
    conf_chnl_idx = 0;
    conf_car_idx = -1;
  
    /* total_conf_car needs to be reset each iteration */
    total_conf_car = conf_car & WWCOEX_CAR_PER_CHNL_BIT_MASK;

    /* reset the chnl grp sum for conf for each req iteration */
    conf_chnl_grp_car_sum = 0;
    
    /* Determine the frequency indexes to use, for the req */
    /* Restricted to RX because PRX and DRX use same freq ID in FW. No diversity in TX. */   
    if (query_st->req_act == ACTIVITY_RX)
    {
      // If we have moved onto a new channel grouping, we need to update our previous total. 
      if ((req_chnl_idx % WWCOEX_MAX_CHNL_PER_GRP == 0) && (req_chnl_idx != 0) && (req_car_idx == 0))
      {
        req_chnl_grp_car_sum += (req_car_per_ch>>(WWCOEX_CAR_PER_CHNL_BIT_SIZE*(req_chnl_idx -1))) & WWCOEX_CAR_PER_CHNL_BIT_MASK;
      }
      // Calculate the new frequency index by using the previous carrier total + the current carrier index
      req_freq_idx = req_chnl_grp_car_sum + req_car_idx; 
    }
    else
    {
      req_freq_idx = i;
    }


    for(j=0;j<conf_loop;j++)
    {
      /* Extract the channel index and carrier index
         each time the loop is run */
      if(j<total_conf_car)
      {
        conf_car_idx += 1;
      }
      else
      {
        conf_chnl_idx += 1;
        conf_car_idx = 0;
        total_conf_car += 
          (conf_car_per_ch>>(WWCOEX_CAR_PER_CHNL_BIT_SIZE*conf_chnl_idx))&
          WWCOEX_CAR_PER_CHNL_BIT_MASK;
      }

      /* Get the channel ID based on the channel Index */
      conf_ch_id = 
        (query_st->conf_ch_id >>(conf_chnl_idx*WWCOEX_CHNL_ID_BIT_SIZE))&
        WWCOEX_CHNL_ID_BIT_MASK;

    /* Determine the frequency indexes to use, for the conf */
    /* Restricted to RX because PRX and DRX use same freq ID in FW. No diversity in TX. */   
    if (query_st->conf_act == ACTIVITY_RX)
    {   
      // If we have moved onto a new channel grouping, we need to update our previous total. 
      if ((conf_chnl_idx % WWCOEX_MAX_CHNL_PER_GRP == 0) && (conf_chnl_idx != 0) && (conf_car_idx == 0))
      {
        conf_chnl_grp_car_sum += (conf_car_per_ch>>(WWCOEX_CAR_PER_CHNL_BIT_SIZE*(conf_chnl_idx -1))) & WWCOEX_CAR_PER_CHNL_BIT_MASK;
      }
      // Calculate the new frequency index by using the previous carrier total + the current carrier index
      conf_freq_idx = conf_chnl_grp_car_sum + conf_car_idx;
    }
    else
    {
      conf_freq_idx = j;
    }
    
    /* Lookup conflict table for freqids*/
    action_func =  wwcoex_cflt_tbl_lookup( 
                     index,
                     query_st->req_ids[req_freq_idx],
                     query_st->conf_ids[conf_freq_idx],
                     req_ch_id,
                     conf_ch_id,
                     cflt_type
                   );
              
      /* Store the max of action after checking with all conflicting
         tech freq IDs */
      action = MAX(action, action_func);
    }
    
    /* FW action is stored per carrier per channel. 
       The max carriers per channel is 2 
       For each channel the results are stored in 8 consecutive bits 
       The results for first channel start at bit 0
       the results for second channel start at bit 8 
    */
    shift =  ((WWCOEX_ACTION_BIT_SIZE*req_chnl_idx*WWCOEX_MAX_CARRIER_PER_CHANNEL)+
              (WWCOEX_ACTION_BIT_SIZE *req_car_idx));
    temp_result = ((uint64)action << (uint64)shift);
    query_st->action |= temp_result;
                        
  }

  return TRUE;
}
    
/*============================================================================

FUNCTION CXM_QUERY_ACTION

DESCRIPTION
  Query the mitigation action corresponding to the two techs freq combination.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_action
(
  cxm_action_query_s* query_st
)
{
  int i,j;
  cxm_action_type action = ACTION_NONE;
  cxm_action_type action_func = ACTION_NONE;

  /* Check input... */
  if ( query_st == NULL ||
       query_st->num_conflicting_ids == 0 || query_st->conflicting_ids == NULL ||
       query_st->num_requesting_ids == 0 || query_st->requesting_ids == NULL || 
       query_st->num_requesting_ids > WWCOEX_MAX_REQUESTING_IDS )
  {
    return FALSE;
  }
  
  /*Reset the mask */
  query_st->actions = 0;

  /* If SW mitigation is disabled then num standby tasks will be 0; 
     Also if its DSDA, then Rx-Rx conflict should have action as NONE */
  if ( (wwcoex_num_standby_stacks == 0) ||
       ( ((wwcoex_is_drdsds_mode == TRUE)||
         (wwcoex_num_standby_stacks == wwcoex_num_active_stacks)) &&
         (query_st->requesting_activity == ACTIVITY_RX) && 
         (query_st->conflicting_activity == ACTIVITY_RX) ) ) 
  {
    return TRUE;
  }

  if(wwcoex_simulation_input != 0)  
  {
    return cxm_query_action_for_simulator(query_st);
  }

  /* Scan through each entry in the table */
  for (i=0; i < query_st->num_requesting_ids; i++)
  {
    if (query_st->requesting_ids[i] != WWCOEX_UNKNOWN_FREQID)
    {
      /* Start with no action */
      action = ACTION_NONE;
      
      for (j=0; j < query_st->num_conflicting_ids; j++)
      {
        /* If any of the Ids are unknown then return action as unknown */
        if ( query_st->conflicting_ids[j] != WWCOEX_UNKNOWN_FREQID )
        {
          /* Lookup conflict table for freqids*/
          action_func =  wwcoex_tbl_freqid_pair_lookup( 
                                            query_st->requesting_ids[i],
                                            query_st->conflicting_ids[j]);

          action = MAX(action, action_func);
        }
        else
        {
          action = MAX(action, ACTION_UNKNOWN);
        }
      }
    }
    else
    {
      /* Use unknown... */
      action = ACTION_UNKNOWN;
    }

    /* Store the action into the mask */
    query_st->actions |= (action << (WWCOEX_ACTION_BIT_SIZE*i));
  }

  return TRUE;
}

/*============================================================================

FUNCTION CXM_QUERY_SPUR_ACTION

DESCRIPTION
  Query the mitigation action corresponding to the two techs freq combination.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_spur_action_for_simulator
(
  cxm_spur_query_s* query_st
)
{
  uint32 i,j,shift;
  uint32 num_request_ids, num_conflict_ids;
  cxm_action_type action = ACTION_NONE;
  cxm_action_type action_func = ACTION_NONE;
  uint32 req_car_per_ch =0, curr_req_channel = 0, curr_req_carrier = 0;
  uint32 conf_car_per_ch =0, curr_conf_channel = 0;
  uint16 handle = 0;


  /* Number of requesting IDs and conflicting IDs are a function of 
     number of requesting channel and carriers per channel */
  req_car_per_ch = query_st->num_carr_per_requesting_ch;
  conf_car_per_ch = query_st->num_carr_per_conflicting_ch;
  
  num_request_ids  = query_st->num_requesting_ch*req_car_per_ch;
  num_conflict_ids = query_st->num_conflicting_ch*conf_car_per_ch;

  /* Scan through each entry in the table to get the handle */
  for (i=0; i < num_request_ids; i++)
  {
    curr_req_channel = i/req_car_per_ch;
    
    if (query_st->requesting_ids[i] != WWCOEX_UNKNOWN_FREQID)
    {
      /* Start with no action */
      action = ACTION_NONE;
      
      for (j=0; j < num_conflict_ids; j++)
      {
        curr_conf_channel = j/conf_car_per_ch;
        
        /* If any of the Ids are unknown then return action as unknown */
        if ( query_st->conflicting_ids[j] != WWCOEX_UNKNOWN_FREQID )
        {
          /* Do a look up only if channel ID does not match */
          if(query_st->requesting_ch_id[curr_req_channel] != 
             query_st->conflicting_ch_id[curr_conf_channel])
          {
            action_func = ACTION_SPUR;
            handle = wwcoex_sim_spur_count;
          }
          else
          {
            action_func = ACTION_NONE;
          }

          action = MAX(action, action_func);
        }
        else
        {
          action = MAX(action, ACTION_SPUR_UNKNOWN);
        }
      }
    }
    else
    {
      /* Use unknown... */
      action = ACTION_SPUR_UNKNOWN;
    }

    /* Find out which nibble needs to be set */
    curr_req_carrier = i%req_car_per_ch;
    shift = (curr_req_channel*WWCOEX_MAX_CARRIER_PER_CHANNEL)+curr_req_carrier;
    
    /* Store the action into the nibble */
    query_st->action |= (action << (WWCOEX_ACTION_BIT_SIZE*shift));
  }

  /* Contruct the handle to return to common FW */
  query_st->handle = handle;

  return TRUE;
}

/*============================================================================

FUNCTION CXM_QUERY_SPUR_ACTION

DESCRIPTION
  Query the mitigation action corresponding to the two techs freq combination.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_spur_action
(
  cxm_spur_query_s* query_st
)
{
  uint32 i,j,shift;
  uint32 num_request_ids, num_conflict_ids;
  cxm_action_type action = ACTION_NONE;
  cxm_action_type action_func = ACTION_NONE;
  uint32 req_car_per_ch =0, curr_req_channel = 0, curr_req_carrier = 0;
  uint32 conf_car_per_ch =0, curr_conf_channel = 0;
  uint16 temp_handle = 0, handle = 0, count=0;

   /* Check input */
  if ( query_st == NULL ||
       query_st->num_conflicting_ch == 0  || query_st->conflicting_ids == NULL   ||
       query_st->num_requesting_ch == 0   || query_st->requesting_ids == NULL    ||
       query_st->requesting_ch_id == NULL || query_st->conflicting_ch_id == NULL
     )
  {
    return FALSE;
  }

  /*Reset the mask and handle*/
  query_st->action  = ACTION_NONE;
  query_st->handle  = WWCOEX_INVALID_SPUR_HANDLE;

  /* TX is not a victim of Spur */
  if((wwcoex_is_spur_mitigation_en  == FALSE) ||
     (query_st->requesting_activity == ACTIVITY_TX))
  {
    return TRUE;
  }

  /* Valid input to simulator so use simulator */
  if(wwcoex_sim_spur_count > 0)
  {
    return cxm_query_spur_action_for_simulator(query_st);
  }

  /* Number of requesting IDs and conflicting IDs are a function of 
     number of requesting channel and carriers per channel */
  req_car_per_ch = query_st->num_carr_per_requesting_ch;
  conf_car_per_ch = query_st->num_carr_per_conflicting_ch;
  
  num_request_ids  = query_st->num_requesting_ch*req_car_per_ch;
  num_conflict_ids = query_st->num_conflicting_ch*conf_car_per_ch;

  /* Scan through each entry in the table to get the handle */
  for (i=0; i < num_request_ids; i++)
  {
    curr_req_channel = i/req_car_per_ch;
    
    if (query_st->requesting_ids[i] != WWCOEX_UNKNOWN_FREQID)
    {
      /* Start with no action */
      action = ACTION_NONE;
      
      for (j=0; j < num_conflict_ids; j++)
      {
        curr_conf_channel = j/conf_car_per_ch;
        
        /* If any of the Ids are unknown then return action as unknown */
        if ( query_st->conflicting_ids[j] != WWCOEX_UNKNOWN_FREQID )
        {
          /* Do a look up only if channel ID does not match */
          if(query_st->requesting_ch_id[curr_req_channel] != 
             query_st->conflicting_ch_id[curr_conf_channel])
          {
            /* Lookup conflict table for freqids*/
            action_func =  wwcoex_tbl_freqid_pair_spur_lookup( 
                                            query_st->requesting_ids[i],
                                            query_st->conflicting_ids[j],
                                            &temp_handle);
          }
          else
          {
            action_func = ACTION_NONE;
          }
          
          /* Multi carrier spurs are stored in contigous memory location
             Action is the MAX action for multi carriers comparission
             Handle is the MIN index for muti carrier comparission 
             count is the sum of number of spurs founds 
          */

          /* TODO: Overflow in mc cases is not handled */
          action = MAX(action, action_func);
          handle = MIN(handle,temp_handle);
          count  = count + (temp_handle & 0xF);
        }
        else
        {
          action = MAX(action, ACTION_SPUR_UNKNOWN);
        }
      }
    }
    else
    {
      /* Use unknown... */
      action = ACTION_SPUR_UNKNOWN;
    }

    /* Find out which nibble needs to be set */
    curr_req_carrier = i%req_car_per_ch;
    shift = (curr_req_channel*WWCOEX_MAX_CARRIER_PER_CHANNEL)+curr_req_carrier;
    
    /* FW action is stored per carrier per channel. 
       The max carriers per channel is 3 
       For each channel the results are stored in 12 consequitive bits or 3 nibbles
       The results for first channel start at bit 0
       the results for second channel start at bit 12 
    */
    query_st->action |= (action << (WWCOEX_ACTION_BIT_SIZE*shift));
  }

  /* Contruct the handle to return to common FW */
  query_st->handle = (handle&0xFFF0)+(count&0xF);

  return TRUE;
}

/*============================================================================

FUNCTION CXM_QUERY_SPUR_INFO

DESCRIPTION
  Query the spur setting corresponding to a handle.
  This API is called from Tech FW to MCS to get spur informaton
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer or invalid handle
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_get_spur_info_from_handle 
(
  uint16               spur_handle,
  cxm_spur_info_s      *spur_info,
  uint32               *num_spurs
)
{
  int i;
  uint16 index, count;
  
  /* check if the input is valid */
  if((spur_info == NULL)||
     (num_spurs == NULL)||
     (spur_handle == WWCOEX_SPUR_HANDLE_UNKNOWN)||
     (spur_handle == WWCOEX_SPUR_HANDLE_NONE))
  {
    return FALSE;
  }

  /* MSB 12 bits of handle is array index
     LSB 4 bits of the handle is count */
  count = spur_handle&0xF;
  index = (spur_handle>>4)&0xFFF;
  index = index % WWCOEX_MAX_SPUR_TABLE_SIZE;

  /* check for invalid count or overflow */
  if(count > WWCOEX_MAX_CONCURRENT_SPURS)
  {
    count = WWCOEX_MAX_CONCURRENT_SPURS;
  }
  
  /* Copy the spur information and count */
  for(i=0;i<count;i++)
  {
    spur_info[i] = wwcoex_spur_info[index];
    index = (index+1)%WWCOEX_MAX_SPUR_TABLE_SIZE;
  }
  *num_spurs = count;

  return TRUE;
}

/*============================================================================

FUNCTION CXM_QUERY_CHANNEL_CONFLICT

DESCRIPTION
  Query if there are any channel conflict between the given freq IDs.
  This API will be used in QTA and QCTA.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_channel_conflict
(
  cxm_conflict_check_s *query_st
)
{
  boolean result;
  
  result = cxm_check_conflict(
             query_st,
             WWCOEX_CFLT_CHANNEL
           );

  /* Add to history buffer */    
  cxm_copy_conflict_check_hist(query_st, WWCOEX_CFLT_CHANNEL);

  return result;
}


/*============================================================================

FUNCTION CXM_QUERY_FILTER_CONFLICT

DESCRIPTION
  Query if there is any filter conflict between the given freq IDs.
  This API will be used in QDTA.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_filter_conflict
(
  cxm_conflict_check_s* query_st
)
{

  boolean result;
  
  result = cxm_check_conflict(
             query_st,
             WWCOEX_CFLT_FILTER
           );

  /* Add to history buffer */    
  cxm_copy_conflict_check_hist(query_st, WWCOEX_CFLT_FILTER);
   
  return result;

}


/*============================================================================

FUNCTION CXM_QUERY_DESENSE_ACTION

DESCRIPTION
  Query if there is any desense between the frequency combinations.
  
DEPENDENCIES
  None

RETURN VALUE
  FALSE for incorrect arguments like NULL pointer, 0 entries in the list, 
  incorrect freqId
  TRUE otherwise

SIDE EFFECTS
  None

============================================================================*/
boolean cxm_query_desense_action
(
  cxm_conflict_check_s* query_st
)
{

  boolean result;
  
  result = cxm_check_conflict(
             query_st,
             WWCOEX_CFLT_DESENSE
           );

  /* Add to history buffer */    
  cxm_copy_conflict_check_hist(query_st, WWCOEX_CFLT_DESENSE);
   
  return result;

}

/*============================================================================

FUNCTION CXM_REGISTER_MODE_UPDATE_CALLBACK

DESCRIPTION
  Register CallBack with CXM for getting Mode Updates 
  
DEPENDENCIES
  None

RETURN VALUE
None

SIDE EFFECTS
  None

============================================================================*/
void cxm_register_mode_update_callback( wwcoexSubsModeCallback reg_cb )
{
  if (reg_cb != NULL)
  {
    fw_cb = reg_cb;
  }

  return;
}

/*============================================================================

FUNCTION CXM_UPDATE_SUBS_STATE

DESCRIPTION
  This function calls FW Callback which was registered by FW to get Mode change updates
  
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void cxm_update_subs_state( void )
{

  if (fw_cb != NULL)
  {
    fw_cb(wwcoex_crat_mode);
  }
  return;
}



/*============================================================================

FUNCTION CXM_QUERY_CRAT_MODE

DESCRIPTION
  Query CRAT Mode to get what system is capable of
  
DEPENDENCIES
  None

RETURN VALUE
  Returns BitMask for Sub1+Sub2 techs

   Single SIM Capability
    L only  = (crat_info.instance0_lte = 0x1) 
    1X only = (crat_info.instance0_1x = 0x1) 
    W only  = (crat_info.instance0_wcdma = 0x1) 
    SLTE (L+1X) = ((crat_info.instance0_lte = 0x1) | (crat_info.instance1_1x = 0x1))
    SRLTE (L+1X QTA) = ((crat_info.instance0_lte = 0x1) | (crat_info.instance1_1x = 0x1))
  
  Multi SIM Capability
    L+W = ((crat_info.instance0_lte = 0x1) | (crat_info.instance1_wcdma = 0x1))
    L+1X = ((crat_info.instance0_lte = 0x1) | (crat_info.instance1_1x = 0x1))

SIDE EFFECTS
  None

============================================================================*/
cxm_fw_crat_mode_t cxm_query_crat_mode( void )
{
  uint8               i;
  cxm_fw_crat_mode_t  rf_dev_owners;
  cxm_fw_crat_mode_t  retVal;
  boolean             is_diff = FALSE;

  /* Check if the RF chain holders are a part of 
     MMCP active tech list 
     If 1X,WCDMA or LTE is active chain holder
     but if these clients are not a part of MMCP info
     then use TRM RF chain holder based active techs
     to notify FW
  */
  if((wwcoex_rf_crat_mode.crat_info.instance1_1x == 0x1) &&
     (wwcoex_crat_mode.crat_info.instance1_1x == 0x0))
  {
    is_diff = TRUE;
  }
  if((wwcoex_rf_crat_mode.crat_info.instance1_wcdma == 0x1) &&
     (wwcoex_crat_mode.crat_info.instance1_wcdma == 0x0))
  {
    is_diff = TRUE;
  }
  if((wwcoex_rf_crat_mode.crat_info.instance0_lte == 0x1) &&
     (wwcoex_crat_mode.crat_info.instance0_lte == 0x0))
  {
    is_diff = TRUE;
  }
  if((wwcoex_crat_mode.crat_info.instance1_wcdma == 0x1) &&
     (wwcoex_crat_mode.crat_info.instance1_1x == 0x1))
  {
    is_diff = TRUE;
  }
  
  if(is_diff == FALSE)
  {
    memscpy(&retVal, sizeof(cxm_fw_crat_mode_t),
          &wwcoex_crat_mode, sizeof(cxm_fw_crat_mode_t));
  }
  else
  {
    memscpy(&retVal, sizeof(cxm_fw_crat_mode_t),
          &wwcoex_rf_crat_mode, sizeof(cxm_fw_crat_mode_t));
  }

  /* Copy parameters to history buffer */
  memscpy(&wwcoex_crat_mode_hist[wwcoex_crat_hist_index], sizeof(cxm_fw_crat_mode_t),
          &retVal, sizeof(cxm_fw_crat_mode_t));

  /* Circular buffer reset */
  wwcoex_crat_hist_index = ((wwcoex_crat_hist_index + 1) % WWCOEX_MAX_CHNL_QUERY_HIST_SIZE);

  return retVal;
}