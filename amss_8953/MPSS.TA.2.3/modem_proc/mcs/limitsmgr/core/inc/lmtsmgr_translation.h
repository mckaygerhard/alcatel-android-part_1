#ifndef __LMTSMGR_TRANSLATION_H__
#define __LMTSMGR_TRANSLATION_H__
/*!
  @file lmtsmgr_translation.h

  @brief
   Translations functions exposed by the LMTSMGR Layer

*/

/*=============================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/limitsmgr/core/inc/lmtsmgr_translation.h#6 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
05/29/15   tl      Remove #ifdefs around lmtsmgr_trans_rf_bands_to_sys_bands()
05/15/15   sg      Support for TRM to FWCXM translation
05/15/15   jm      Support for band group algorithm for RxBN desense; clean up
                   tables for SHDR and RxBN only cases
04/22/15   jm      Support inst id for WCDMA
01/06/15   rj      Initial Revision

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/
#include "cxm.h"
#include "comdef.h"
#include "tcxomgr.h"
#include "wwcoex_action_iface.h"

/*=============================================================================

                       CONSTANTS AND DEFINES

=============================================================================*/

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_rf_bands_to_sys_bands

=============================================================================*/
uint32 lmtsmgr_trans_rf_bands_to_sys_bands 
(
  cxm_tech_type tech_id,
  uint32 tech_band
);

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_lmts_tech_to_xo_tech

=============================================================================*/
/*!
    @brief
    Get the XO tech ID corresponding to the limitsmgr tech ID
 
    @return
    None
*/
/*===========================================================================*/
void lmtsmgr_trans_lmts_tech_to_xo_tech
(
  cxm_tech_type   tech,
  tcxomgr_client_info_struct_type *xo_rgs_client
);

#ifdef FEATURE_LMTSMGR_SIMULATOR_SUPPORT
#error code not present
#endif /* FEATURE_LMTSMGR_SIMULATOR_SUPPORT */

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_cxm_to_sys_id

=============================================================================*/
/*!
    @brief
    Converts CXM tech IDs to MMCP tech IDs
 
    @return
    MMCP SYS tech ID
*/
/*=============================================================================*/
sys_sys_mode_e_type lmtsmgr_trans_cxm_to_sys_id 
(
  cxm_tech_type tech
);

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_trm_to_cxm_client

=============================================================================*/
/*!
    @brief
    This is a TRM to CXM client mapping 
 
    @return
    None
*/
/*===========================================================================*/
cxm_tech_type lmtsmgr_trans_trm_to_cxm_client
(
  trm_client_enum_t trm_clid
);

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_trm_to_cxm_client

=============================================================================*/
/*!
    @brief
    This is a TRM to Sys client mapping 
 
    @return
    None
*/
/*===========================================================================*/
sys_sys_mode_e_type lmtsmgr_trans_trm_to_sys_client
(
  trm_client_enum_t trm_clid
);


#ifdef FEATURE_MODEM_ANTENNA_SWITCH_DIVERSITY
/*=============================================================================

  FUNCTION:  lmtsmgr_trans_cxm_to_trm_client

=============================================================================*/
/*!
    @brief
    Converts CXM tech IDs to TRM client IDs
 
    @return
    TRM client ID
*/
/*=============================================================================*/
trm_client_enum_t lmtsmgr_trans_cxm_to_trm_client 
(
  cxm_tech_type tech    
);
#endif /* Antenna Switch Div feature */

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_sys_bands_to_rf_bands

=============================================================================*/
/*!
    @brief
    Maps sys bands to RF bands 
 
    @return
    None
*/
/*===========================================================================*/
uint32 lmtsmgr_trans_sys_bands_to_rf_bands 
(
  uint32 tech_band,
  cxm_tech_type tech_id
);

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_cxm_tech_to_rf_mode

=============================================================================*/
/*!
    @brief
    Maps tech type to RF mode
 
    @return
    None
*/
rfm_mode_enum_type lmtsmgr_trans_cxm_tech_to_rf_mode
(
  cxm_tech_type tech_id
);

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_sys_id_to_cxm_client

=============================================================================*/
/*!
    @brief
    Converts MMCP SYS Tech IDs to CXM client IDs
 
    @return
    CXM client ID
*/
/*=============================================================================*/
cxm_tech_type lmtsmgr_trans_sys_id_to_cxm_client
(
  sys_sys_mode_e_type  mode,
  sys_modem_as_id_e_type asid
);

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_cxm_client_to_sys_id

=============================================================================*/
/*!
    @brief
    Converts CXM client IDs to MMCP SYS Tech IDs
 
    @return
    MMCP SYS Tech IDs
*/
/*=============================================================================*/
sys_modem_as_id_e_type lmtsmgr_trans_cxm_client_to_sys_id
(
  cxm_tech_type cxm_client
);

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_sys_band_to_band_group

=============================================================================*/
/*!
    @brief
    Converts Sys Band to band Group
 
    @return
    Band Group
*/
/*=============================================================================*/
lmtsmgr_band_group_type lmtsmgr_trans_sys_band_to_band_group
(
  cxm_tech_type tech,
  uint32 sys_band
);

/*=============================================================================

  FUNCTION:  lmtsmgr_trans_trm_client_to_fwcxm_client

=============================================================================*/
/*!
    @brief
    This is a TRM to FW CXM client mapping 
 
    @return
    None
*/
/*===========================================================================*/
cxmfw_rat_type lmtsmgr_trans_trm_client_to_fwcxm_client
(
  trm_client_enum_t trm_clid
);

#endif /* __LMTSMGR_TRANSLATION_H__ */
