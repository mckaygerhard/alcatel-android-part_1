#ifndef __LMTSMGR_DIAG_H__
#define __LMTSMGR_DIAG_H__
/*!
  @file lmtsmgr_diag.h

  @brief
   Diag interface used by LMTSMGR module

*/

/*=============================================================================

  Copyright (c) 2014-2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by this document
  are confidential and proprietary information of Qualcomm Technologies
  Incorporated and all rights therein are expressly reserved.  By accepting
  this material the recipient agrees that this material and the information
  contained therein are held in confidence and in trust and will not be used,
  copied, reproduced in whole or in part, nor its contents revealed in any
  manner to others without the express written permission of Qualcomm
  Technologies Incorporated.

=============================================================================*/

/*==============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/limitsmgr/core/inc/lmtsmgr_diag.h#5 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
08/26/15   mb      Add macro/structs for QxTA and chnl cflt log packets. 
05/29/15   tl      Add log packets for Vbatt power limiting
02/17/15   jm      Added frame type to broadcast state info
02/02/15   jm      Force filter path selection based on EFS
10/10/14   jm      Slot Level activity log support for frame boundary
08/25/14   jm      Circular buffer for slot level activity tl
08/12/14   jm      Slot level activity timeline support
04/28/14   jm      L+G Band Avoidance support
03/02/14   ag      Added type to the power info log packet
04/12/13   ag      Initial Revision

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/

#include "comdef.h"
#include "log_codes.h"
#include "log.h"
#include "wwan_coex_mgr.h"
#include "wwcoex_action_iface.h"
#include "wwcoex_conflict_table.h"
#include "vbatt_i.h"
#include "diagpkt.h"
/*=============================================================================

                       CONSTANTS AND DEFINES

=============================================================================*/
#define FEATURE_LOG_TECH_STATE
#define FEATURE_LOG_SCENARIO_TBL

#define WWCOEX_LOG_POWER_SUBCODE  2
#define WWCOEX_LOG_FREQUENCY_SUBCODE 3
#define WWCOEX_LOG_BLIST_RSP_SUBCODE 4

#define WWCOEX_LOG_MAX_TL_ENTRIES ((CXM_MAX_NUM_TIMING_SLOTS*LMTSMGR_TIMING_HIST_RECS)+LMTSMGR_TIMING_HIST_RECS)

#define CXM_QTA_MAX_DEVICE_PAIR     8
/*=============================================================================

                             TYPEDEFS

=============================================================================*/
/*---------------------------------------------------------------------------
  Header Type (AFC logging)
---------------------------------------------------------------------------*/
#ifdef TEST_FRAMEWORK
#error code not present
#endif

typedef PACK(struct)
{
  cxm_action_type action;
  wwan_coex_desense_type desense;
  uint16 spur_handle1;
  uint16 spur_handle2;
}wwcoex_log_arr_entry_type;

typedef PACK(struct)
{
  wwcoex_log_arr_entry_type current_tbl[CXM_MAX_SUPPORTED_LINK_SETS * CXM_MAX_SUPPORTED_LINK_SETS];
//  wwcoex_log_arr_entry_type old_tbl[CXM_MAX_SUPPORTED_LINK_SETS*CXM_MAX_SUPPORTED_LINK_SETS];
//  uint32  num_old_entries;
}wwcoex_log_tbl_type;

typedef PACK(struct)
{
  uint32                  freqId;
  uint32                  frequency; /*!< Tech operating frequency in KHz */
  uint32                  bandwidth; /*!< Tech operating bandwidth in Hz */
  int16                   power;     /*!< Power associated with this */
  uint8                   band;      /*!< Operating Sys Band */
  uint8                   direction; /*!< Direction (Up-link/Down-Link/Both) */
  uint16                  type;      /*!< Type (CA1, CA2, PM, Div, or Unused) */
}wwcoex_log_freq_link_type;

typedef PACK(struct)
{
  wwcoex_log_freq_link_type   links[CXM_MAX_SUPPORTED_LINK_SETS];
}wwcoex_log_tech_freq_state;


typedef PACK(struct)
{
  uint8  subcode;
  uint8  num_t1entries;
  uint8  num_t2entries;
  uint8  tech1;
  uint8  tech2;
  uint32 tech1_curr_fid_offset;
  uint32 tech2_curr_fid_offset;
}wwcoex_state_log_hdr_type;

typedef PACK(struct)
{
  uint32 sim_mode;
  uint32 spur_sim_mode;
}wwcoex_sim_info_type;

typedef PACK(struct)
{
  uint8 filter_path;
  uint8 gsm_tx_filter_path;
  uint8 force_filter_flag;
}wwcoex_filter_path_info_type;

typedef PACK(struct)
{
  int16 tech1_sar_plimit;
  int16 tech1_backoff_plimit;
  int16 tech2_sar_plimit;
  int16 tech2_backoff_plimit;
}wwcoex_pwr_backoff_info_type;

typedef PACK(struct)
{
  log_hdr_type hdr;
  uint8  version;
  wwcoex_state_log_hdr_type state_hdr;
  wwcoex_sim_info_type sim_info;
  wwcoex_filter_path_info_type filter_info;
  wwcoex_pwr_backoff_info_type backoff_info;
  wwcoex_log_tbl_type scenario_tbls;
} wwcoex_state_log_type;

typedef PACK(struct)
{
  uint8  subcode;
  uint8  tech_id;
  uint8  num_entries;
}wwcoex_power_log_hdr_type;

typedef PACK(struct)
{
  log_hdr_type hdr;
  uint8  version;
  wwcoex_power_log_hdr_type power_hdr;
  wwcoex_log_tech_freq_state tech_list;
}wwcoex_power_log_type;


typedef PACK(struct)
{
  uint8  bl_id;
  uint8  response;
}wwcoex_log_tech_blist_rsp_type;

typedef PACK(struct)
{
  uint32  frequency; /*!< Tech operating frequency in KHz */
  uint32  bandwidth; /*!< Tech operating bandwidth in Hz */
  int16   power;     /*!< Power associated with this (dBm*10) */
  uint8   band;      /*!< Operating Sys Band */
  uint8   direction; /*!< Direction (Up-link/Down-Link/Both) */
}wwcoex_log_serv_freq_entry_type;

typedef PACK(struct)
{
  wwcoex_log_serv_freq_entry_type entries[CXM_MAX_SUPP_FREQ_LINK_BA];
}wwcoex_log_tech_serv_freq_state;

typedef PACK(struct)
{
  log_hdr_type                                hdr;
  uint8                                   version;
  wwcoex_power_log_hdr_type             power_hdr;
  union wwcoex_serv_subcode
  {
    wwcoex_log_tech_serv_freq_state  serv_freq_list;
    wwcoex_log_tech_blist_rsp_type   bl_rsp;
  }u;
}wwcoex_ba_serv_power_log_type;


typedef PACK(struct)
{
  uint8  blacklist_id;
  uint8  tech_id;
  uint8  serv_freq_state;
  uint8  serv_freq_mode;
  uint8  num_entries;
}wwcoex_ba_blist_hdr_type;

typedef PACK(struct)
{
  uint8                   band;         /*!< Operating Sys Band */
  uint32                  freq_lower;   /*!< Lower bounds (KHz) */
  uint32                  freq_upper;   /*!< Upper bounds (KHz) */
  uint8                   desense_mode; /*!< Victim (DL) or Aggressor (UL) */
  int16                   rxpwr_thresh; /*!< Rx Power in dBm*10 format; filled when desense mode is Victim;
                                               CXM_UNKNOWN_POWER if no threshold */
  int16                   txpwr_thresh; /*!< Tx Power in dBm*10 format; filled when desense mode is Aggr;
                                               CXM_UNKNOWN_POWER if no threshold */
}wwcoex_log_ba_list_entry_type;

typedef PACK(struct)
{
  wwcoex_log_ba_list_entry_type entries[CXM_MAX_SUPP_FREQ_LINK_BA];
}wwcoex_log_blist_state;

typedef PACK(struct)
{
  log_hdr_type hdr;
  uint8  version;
  wwcoex_ba_blist_hdr_type        blist_hdr;
  wwcoex_log_blist_state          blacklist;
}wwcoex_ba_blist_log_type;

typedef PACK(struct)
{
  /*! GSM Call type */
  uint8 call_type;
  
  /*! Start time (in USTMR units) of a new block, which indicates
      any activity reported after this time is NOT accurate.
      Macro CXM_INVALID_USTMR_TIME indicates all activity comes
      from same block */ 
  uint32 new_block_time;
    
  /*! Bit-wise enable validity indicator for GSM specific
      conditional metrics (defined below).  */
  uint8 metrics_valid_mask;

  /*! Start frame boundary (in USTMR units) of each
      frame in the message  */ 
  uint32 frame_start_time[CXM_MAX_GSM_LOOK_AHEAD_FRAMES];  
  
  /*! GSM Specific Conditional Metrics.
  
    Position 0: Max sustainable conecutive GSM UL DTx-ed frames after
    current GSM frame, measured in time (ms)
	
    Position 1: Max cluster of GSM UL DTx-ed frames with 1 intermittent
    Tx burst (SACCH) counted after current GSM frame,
    measured in time (ms)
	
    Position 2: Max cluster of GSM UL DTx-ed frames with 2 intermittent
    Tx bursts (SACCH) counted after current GSM frame,
    measured in time (ms)
  */
  uint32 cond_metics[CXM_MAX_GSM_METRIC_TYPES];
}wwcoex_gsm_metrics_type;

typedef PACK(union)
{
  wwcoex_gsm_metrics_type gsm_metrics; /*!< GSM specific parameters */ 
}wwcoex_timing_info_tech_info_type;

typedef PACK(struct)
{
  uint8  send_tech_id;
  uint8  rec_tech_id;
  uint8  in_dtx_state;
  uint32 dtx_switch_time;
  uint32 dtx_time;
  uint8  num_entries;
  wwcoex_timing_info_tech_info_type tech_metrics;
}wwcoex_timing_info_hdr_type;

typedef PACK(struct)
{
  uint32 freqid;        /*! Frequency ID */
  uint32 frequency;     /*! Frequency (kHz) */
  uint8  band;          /*!< Tech operating band (sys_band_class_e_type) */
  uint8  direction;     /*! Type of activity being registered, Uplink/Downlink */ 
  uint8  link_type;      /*! Link Type of slot (Normal/Diversity/PowerMonitor) */
  uint32 start_time;    /*! Start time (in USTMR units) of the activity in the next slot (unpadded)*/ 
  uint32 end_time;      /*! End time (in USTMR units) of the activity in the next slot (unpadded)*/ 
  uint8  micro_prio;    /*! Micro priority which maps to ACTIVITY_TIER */ 
  uint16 conflict_mask; /*! Bitwise mask based off of cxm_tl_conflict_type */
}wwcoex_act_tl_list_entry_type;

typedef PACK(struct)
{
  wwcoex_act_tl_list_entry_type entries[WWCOEX_LOG_MAX_TL_ENTRIES];
}wwcoex_act_tl_list_type;

typedef PACK(struct)
{
  log_hdr_type hdr;
  uint8  version;
  wwcoex_timing_info_hdr_type     timing_hdr[LMTSMGR_TIMING_HIST_RECS];
  wwcoex_act_tl_list_type         sl_act_list;
}wwcoex_timing_info_log_type;

/** Log packet used to report the contents of a vbatt record, using the
 * LOG_MCS_VBATT_INFO_C log packet. */
typedef PACK(struct)
{
  log_hdr_type  hdr;
  /** The log version; this version of the structure is
   * MCS_VBATT_INFO_LOG_VERSION. */
  uint8         version;
  /** The log subcode indicating the vbatt record,
   * MCS_VBATT_INFO_LOG_RECORD_SUBCODE. */
  uint8         subcode;

  /* Flag indicating whether the record passed the record validation: 1 if the
   * record passed and will be used by the vbatt algorithm; 0 if the record
   * failed validation. */
  int16         valid;
  /** The tech and band used for this record, using the values in
   * sys_band_class_e_type */
  int16         band;
  /** The stage 1 maximum tx power limit, in dBm * 10 */
  int16         stage1_plimit;
  /** The stage 1 voltage up threshold, in mV */
  int16         stage1_voltage_up;
  /** The stage 2 voltage down threshold, in mV */
  int16         stage1_voltage_down;
  /** The stage 2 maximum tx power limit, in dBm * 10 */
  int16         stage2_plimit;
  /** The stage 2 voltage up threshold, in mV */
  int16         stage2_voltage_up;
  /** The stage 2 voltage down threshold, in mV */
  int16         stage2_voltage_down;
  /** The time hysteresis, in milliseconds */
  uint16        time_hysteresis;

} vbatt_record_log_type;

/** Log packet used to report the current vbatt state, using the
 * LOG_MCS_VBATT_INFO_C log packet. */
typedef PACK(struct)
{
  log_hdr_type  hdr;
  /** The log version; this version of the structure is
   * MCS_VBATT_INFO_LOG_VERSION. */
  uint8         version;
  /** The log subcode indicating the vbatt state,
   * MCS_VBATT_INFO_LOG_STATE_SUBCODE. */
  uint8         subcode;

  /** The current vbatt value, or -1 if the current vbatt value is unavailable
   */
  int16         current_vbatt;
  /** The value used to override vbatt, or -1 if not set */
  int16         vbatt_test_override;
  /** The current active tech and band, using the values in
   * sys_band_class_e_type */
  int16         active_band;
  /** The current stage, where -1 indicates no limits, 0 indicates stage 1,
   * and 1 indicates stage 2 */
  int16         current_stage;
  /** The time remaining, in milliseconds, before the hysteresis timer
   * expires; or 0 if the hysteresis timer is not currently set */
  uint16        hysteresis_timer_expires;
  /** The stage from which the hysteresis timer will prevent transitioning
   * back into, or -2 if the hysteresis timer is not currently set */
  int16         hysteresis_stage;
  /** The current power limit for the active tech */
  int16         plimit;
  /** The current low threshold set with ADC */
  int16         vbatt_threshold_low;
  /** The current high threshold set with ADC */
  int16         vbatt_threshold_high;
} vbatt_state_log_type;

/*! Structure to store QTA information for a pair of devices */
typedef PACK(struct)
{
  uint8       data_dev; // originally: rfm_device_enum_type
  uint8       idle_dev; // originally: rfm_device_enum_type
  uint8       data_client_type; // originally: trm_client_type
  uint8       rstr_type; // originally: trm_restriction_enum_type
  boolean                    is_partial_conflict;
} cxm_qta_association_type_log;

/*! Structure for the QTA gap */
typedef PACK(struct)
{
  uint8         data_clid; //originally: trm_client_enum_t
  uint8         idle_clid; //originally: trm_client_enum_t
  uint8         num_entries;
  boolean       is_qta_start;
  cxm_qta_association_type_log  qta_params[CXM_QTA_MAX_DEVICE_PAIR];
} cxm_qta_info_type_log;

/* Structure used for storing channel conflict information 
   along with freq IDs for which there is channel conflict */
typedef PACK(struct)
{
  uint8         cflt_type; //originally: wwcoex_chnl_cflt_type
  uint16        freq_id;
}wwcoex_chnl_cflt_result_type_log;

/* Stucture used for storing channel conflict tables */
typedef PACK(struct)
{
  /* stores channel conflict result */
  wwcoex_chnl_cflt_result_type_log arr[WWCOEX_MAX_RF_DEV][WWCOEX_MAX_RF_DEV];

  /* tech IDs for which the table is created */
  uint8 tech1;
  uint8 tech2;

  /* indicates if the current table is valid */
  boolean is_valid;
}wwcoex_chnl_cflt_table_type_log;

/* Struct containing data for channel conflict logs */
typedef PACK(struct)
{
  /* TRM params */
  cxm_qta_info_type_log trm_params;
  /* cflt table */
  wwcoex_chnl_cflt_table_type_log cflt_table;

} cxm_qta_update_info_log;

/* Struct containig header info for channel conflict logs */
typedef PACK(struct)
{
  uint8 subcode;
  uint8 tech1;
  uint8 tech2;
  uint8 num_entries;
} wwcoex_cflt_log_hdr_type;

/* Struct for channel conflict logs */
typedef PACK(struct)
{
  log_hdr_type hdr;
  uint8 version;
  wwcoex_cflt_log_hdr_type sub_hdr;
  cxm_qta_update_info_log data;
} wwcoex_cflt_log_type;

/*! @brief Struct for providing results to FW CXM 
    Almost all of the fields are packed vectors 
    The same structure is used for desense, channel and filter conflict */
typedef PACK(struct)
{
  /* Requesting Freq IDs for a channel pair 
     diversity does not have a unique freq ID */
  uint64 req_ids; // mod by bubernak

  /* Conflicting Freq IDs for a channel pair 
     diversity does not have a unique freq ID */
  uint64 conf_ids; // mod by bubernak

  /* Action is a packed field. CXM stores the result in this field
     Action is stored per carrier per chnl and uses 4 bits each */
  uint64 action;
  
  /* Requesting Channel IDs is a packed field
     Channel IDs are stored using 3 bits for each channel */
  uint32 req_ch_id;

  /* Number of requesting channels is a bitmask
     Bit is set to indicate each channel that is requested */
  uint8  num_req_ch;

  /* Number of carrier per req channel is a packed field
     This stores the number of carrier for a channel pair */
  uint8  num_carr_per_req_ch;            

  /* Conflicting Channel IDs is a packed field
     Channel IDs are stored using 3 bits for each channel */
  uint32 conf_ch_id;

  /* Number of conflicting channels is a bitmask
     Bit is set to indicate each channel that is conflicting */
  uint8  num_conf_ch;

  /* Number of carrier per conf channel is a packed field
     This stores the number of carrier for a channel pair */
  uint8  num_carr_per_conf_ch;

  /* Requesting activity */
  uint8 req_act; //originally: wwan_coex_activity_type

  /* Conflicting activity */
  uint8 conf_act; //originally: wwan_coex_activity_type

  /* Requesting 
  FW tech Id */
  uint8  req_tech;

  /* Conflicting FW tech Id */
  uint8  conf_tech;
}cxm_conflict_check_s_log;

typedef PACK(struct)
{
  /* Parameters passed by FW-CXM to MCS during conflict check */
  cxm_conflict_check_s_log  input_params;
  
  /* Type of conflict FW-CXM requested */
  uint8      cflt_type; // originally: wwcoex_cflt_type
  
}wwcoex_query_cflt_hist_type_log;

/* Struct for the header of the fw query log */
typedef PACK(struct)
{
  uint8 subcode;
} wwcoex_fw_query_log_hdr_type;

/* Struct containing the data of the fw query log */
typedef PACK(struct)
{
  wwcoex_query_cflt_hist_type_log cflt_hist[WWCOEX_MAX_CHNL_QUERY_HIST_SIZE];
} wwcoex_fw_query_log;

/* Struct for fw query logs */
typedef PACK(struct)
{
  log_hdr_type hdr;
  uint8 version;
  wwcoex_fw_query_log_hdr_type sub_hdr;
  wwcoex_fw_query_log data;
} wwcoex_fw_query_type;

/*---------------------------------------------------------------------------
  Definitions for Diag commands sent to Limits Manager from QXDM
---------------------------------------------------------------------------*/

/*! 
  @brief
    diag command enum, it contains the list of diag commands
    with command code
*/
typedef enum
{
  
  SAR_SET_DSI   = 0x01,
  VBATT_SET     = 0x02,
  
  LIMITSMGR_DIAG_MAX_ID
} limitsmgr_diag_table_id_type;

/*--------------------------------------------------------------------------
                             SAR SET DSI

This interface provides a way for a test tool to override the current
sar dsi value.
--------------------------------------------------------------------------*/

DIAGPKT_SUBSYS_REQ_DEFINE(LIMITSMGR, SAR_SET_DSI)
  uint8 dsi_val;    
DIAGPKT_REQ_END

DIAGPKT_SUBSYS_RSP_DEFINE(LIMITSMGR, SAR_SET_DSI)
DIAGPKT_RSP_END

/*--------------------------------------------------------------------------
                               VBATT SET LEVEL

This interface provides a way for a test tool to override the current
vbatt value.
--------------------------------------------------------------------------*/

/* Diag command request to set the vbatt override value, to test the vbatt
 * core algorithm in cases where the actual vbatt level can't be easily
 * manipulated */
DIAGPKT_SUBSYS_REQ_DEFINE(LIMITSMGR, VBATT_SET)
  int16 vbatt_value;    
DIAGPKT_REQ_END

/* Diag command response to set the vbatt override value */
DIAGPKT_SUBSYS_RSP_DEFINE(LIMITSMGR, VBATT_SET)
DIAGPKT_RSP_END

#ifdef TEST_FRAMEWORK
#error code not present
#endif

/*=============================================================================

                        FUNCTION DECLARATIONS

=============================================================================*/
/*=============================================================================

  FUNCTION:  wwcoex_log_power

=============================================================================*/
/*!
    @brief
    Logs the power and frequency information for a particular tech
 
    @return
    None
*/
/*===========================================================================*/
void wwcoex_log_power
(
  cxm_tech_type tech_id,
  uint8 subcode
);

/*=============================================================================

  FUNCTION:  wwcoex_log_state

=============================================================================*/
/*!
    @brief
    Logs all the WWAN Coex state information
 
    @return
    None
*/
/*===========================================================================*/
void wwcoex_log_state
(
  cxm_tech_type tech1,
  cxm_tech_type tech2,
  wwcoex_tbl_type* result_tbl
);

/*=============================================================================

  FUNCTION:  wwcoex_log_serv_power

=============================================================================*/
/*!
    @brief
    Logs the serving power and frequency information for a particular tech
    (ie for Band Avoidance)
 
    @return
    None
*/
/*===========================================================================*/
void wwcoex_log_serv_power
(
  cxm_tech_type tech_id,
  uint8 subcode
);

/*=============================================================================

  FUNCTION:  wwcoex_log_blacklist

=============================================================================*/
/*!
    @brief
    Logs the blacklist information (ie for Band Avoidance)
 
    @return
    None
*/
/*===========================================================================*/
void wwcoex_log_blacklist
(
  cxm_tech_type tech_id
);

/*=============================================================================

  FUNCTION:  wwcoex_log_sl_act_tl

=============================================================================*/
/*!
    @brief
    Logs the slot level activity timeline
 
    @details
    L1->MCS sends the bulk of this information but MCS adds
    potential conflict type to each activity slot
 
    @return
    None
*/
/*===========================================================================*/
void wwcoex_log_sl_act_tl
(
  cxm_tech_type rec_tech
);

/*=============================================================================

  FUNCTION:  wwcoex_log_fw_query

=============================================================================*/
/*!
    @brief
    Logs FW Query Information
 
    @details
    
      
    @return
     void

*/
/*===========================================================================*/
void wwcoex_log_fw_query(void);

/*=============================================================================

  FUNCTION:  wwcoex_log_qta_update_info

=============================================================================*/
/*!
    @brief
    Logs QTA Information sent from TRM
    @details
      
    @return
     void

*/
/*===========================================================================*/
void wwcoex_log_qta_update_info(void);


/*=============================================================================

  FUNCTION:  vbatt_log_record

=============================================================================*/
/*!
    @brief
    Send a log packet containing the log record

    @param[in]  record  The record from EFS to log
    @param[in]  valid   Flag indicating whether the record is valid

    @return
    None
*/
/*===========================================================================*/
void vbatt_log_record
(
  const vbatt_record_type      *record,
  boolean                       valid
);

/*=============================================================================

  FUNCTION:  vbatt_log_state

=============================================================================*/
/*!
    @brief
    Send a log packet containing the current vbatt state

    @return
    None
*/
/*===========================================================================*/
void vbatt_log_state(void);

#endif /* __LMTSMGR_DIAG_H__ */

