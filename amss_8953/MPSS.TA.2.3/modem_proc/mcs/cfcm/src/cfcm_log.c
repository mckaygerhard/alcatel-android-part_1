/*!
  @file
  cfcm_log.c

  @brief
  This file implements the Log related data for CFCM task.

  @ingroup per_implementation
*/

/*=============================================================================

  Copyright (c) 2016 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

=============================================================================*/

/*=============================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/mcs.mpss/5.0/cfcm/src/cfcm_log.c#4 $

=============================================================================*/

/*=============================================================================

                           INCLUDE FILES

=============================================================================*/
#include "mcs_variation.h"
#include "cfcmi.h"
#include "cfcm_log.h"
#include "cfcm_client.h"
/*=============================================================================

                         INTERNAL VARIABLES

=============================================================================*/
cfcm_log_cpu_thrshld_type cpu_thrshld[CFCM_CLIENT_MAX];

#define CFCM_LOG_MONITOR_INFO_VERSION 2 /* v2 */
#define CFCM_LOG_COMMAND_INFO_VERSION 2 /* v2  */

/*! @brief whether the monitor is turned on in the mask
*/
#define CFCM_MONITOR_IN_MASK(mask, monitor)    (((mask) & (1 << (monitor))) != 0)
/*=============================================================================

                                FUNCTIONS

=============================================================================*/
/*=============================================================================

  FUNCTION:  cfcm_log_monitor_input

=============================================================================*/
/*!
    @brief
    Logs the power and frequency information for a particular tech
 
    @return
    None
*/
/*===========================================================================*/
void cfcm_log_monitor_input
(
  cfcm_monitor_e monitor_id,
  cfcm_log_monitor_type* monitor_data
)
{
#ifdef LOG_MCS_CFCM_MONITOR_INFO_C
  cfcm_log_monitor_info_type *log_ptr=NULL;
  uint32 log_size = 0;

    /* Check if tech_id is valid */
  if (!CFCM_IS_MONITOR_ID_VALID(monitor_id))
  {
    CFCM_MSG_1(ERROR, "Invalid monitor ID %d sent for logging", monitor_id);
    return;
  }

  /* Find out how much size we need to allocate */
  log_size = sizeof(cfcm_log_monitor_info_type);

  log_ptr = (cfcm_log_monitor_info_type *)log_alloc_ex( 
                                       (log_code_type) LOG_MCS_CFCM_MONITOR_INFO_C, log_size );

  if ( log_ptr != NULL )
  {
    log_ptr->version = CFCM_LOG_MONITOR_INFO_VERSION;
    /* Update monitor data to log packet */
    log_ptr->monitor_id = (uint8)monitor_id;
    memscpy(&log_ptr->monitor, sizeof(cfcm_log_monitor_type),
              monitor_data, sizeof(cfcm_log_monitor_type));

    /* submit the log to DIAG */
    log_commit( log_ptr );
  }
  else
  {
    CFCM_MSG_1(MED, "Could not allocate cfcm_log_monitor_input pkt of size %d",
                          log_size );
  }
#else
  CFCM_UNUSED(monitor_data);
#endif /* LOG_MCS_CFCM_MONITOR_INFO_C */
}

/*=============================================================================

  FUNCTION:  cfcm_log_command_output

=============================================================================*/
/*!
    @brief
    Logs all CFCM Output Command
 
    @return
    None
*/
/*===========================================================================*/
void cfcm_log_command_output
( 
  uint32* hist_idx,
  cfcm_monitor_e monitor_id,
  cfcm_log_monitor_type* monitor_data
)
{
#ifdef LOG_MCS_CFCM_COMMAND_INFO_C
  cfcm_log_cmd_info_output_type *log_ptr=NULL;
  uint32 index = 0, latest_hist_idx = 0, monitor_idx = 0, i= 0;
  uint32 log_size = 0;
  cfcm_client_info_s*   client_ptr;
  
  if (hist_idx == NULL)
  {
    CFCM_MSG_0(ERROR, "INVALID arguments to cfcm_log_command_output");
    return ;
  }

  if (cfcm_client_check_history_idxs_change(hist_idx))
  {
    /* Find out how much size we need to allocate */
    log_size = FPOS(cfcm_log_cmd_info_output_type, output.client_cmd);
    log_size += (CFCM_CLIENT_MAX * sizeof(cfcm_log_client_cmd_data_type));
  
    log_ptr = (cfcm_log_cmd_info_output_type *)log_alloc_ex( 
                                         (log_code_type) LOG_MCS_CFCM_COMMAND_INFO_C,
                                                 log_size );
  
    if ( log_ptr != NULL )
    {
      log_ptr->version = CFCM_LOG_COMMAND_INFO_VERSION;
  
      /* Update monitor data to log packet */
      log_ptr->output.monitor_id = (uint8)monitor_id;
      memscpy(&log_ptr->output.monitor, sizeof(cfcm_log_monitor_type),
                monitor_data, sizeof(cfcm_log_monitor_type));
  
      //check each client
      for (index=0; index < CFCM_CLIENT_MAX; index++)
      {
        client_ptr = &cfcm_client.clients[index];
        if (hist_idx[index] != client_ptr->latest_cmd_hist_idx)
        {
          //check each monitor for the client
          for (monitor_idx=0; monitor_idx < CFCM_MONITOR_LOG_MAX; monitor_idx++)
          {
            //need to make sure that the monitor is a valid monitor for the client. skip if it isnt
            if( CFCM_MONITOR_IN_MASK(client_ptr->monitor_mask, monitor_idx))
            {
              if(client_ptr->latest_cmd_hist_idx != 0)
              {
                //scan through the command history to see if we find a command for our current monitor
                for(i = client_ptr->latest_cmd_hist_idx; i> 0 ; i--)
                {
                  if((cfcm_monitor_e)monitor_idx == client_ptr->cmd_hist[i].monitor_id)
                  {
                    latest_hist_idx = i;
                    break; //we have found the correct monitor, stop checking and break to add it to the log packet
                  }
                }
              }
              else 
              {
                latest_hist_idx =i;
                if( (cfcm_monitor_e)monitor_idx != client_ptr->cmd_hist[i].monitor_id)
                {
                  break; // the monitor doesn't match the latest command, so break to check the next monitor
                } 
              }
              // add the command to the log packet
              log_ptr->output.client_cmd[index].client_id = (uint8)client_ptr->cmd_hist[latest_hist_idx].client_id;
              log_ptr->output.client_cmd[index].cmd_info[monitor_idx].cmd = (uint32)client_ptr->cmd_hist[latest_hist_idx].cmd;
              log_ptr->output.client_cmd[index].cmd_info[monitor_idx].step_timer = client_ptr->cmd_hist[latest_hist_idx].step_timer;
              log_ptr->output.client_cmd[index].cmd_info[monitor_idx].monitor = monitor_idx;
              log_ptr->output.client_cmd[index].cmd_info[monitor_idx].data_rate = client_ptr->cmd_hist[latest_hist_idx].data_rate;

              // the level/state info varies by Monitor, so determine monitor to add the correcto info
              if (log_ptr->output.client_cmd[index].cmd_info[monitor_idx].monitor == CFCM_MONITOR_CPU)
              {
                log_ptr->output.client_cmd[index].cmd_info[monitor_idx].cpu_thrshld = cpu_thrshld[index];
                log_ptr->output.client_cmd[index].cmd_info[monitor_idx].level.cpu_load = client_ptr->cmd_hist[latest_hist_idx].level.cpu_load;
              }
              else if (log_ptr->output.client_cmd[index].cmd_info[monitor_idx].monitor == CFCM_MONITOR_BW_THROTTLING)
              {
               log_ptr->output.client_cmd[index].cmd_info[monitor_idx].level.bw_level = client_ptr->cmd_hist[latest_hist_idx].level.bw_level;
              }
              else if (log_ptr->output.client_cmd[index].cmd_info[monitor_idx].monitor == CFCM_MONITOR_THERMAL_PA) 
              {
                log_ptr->output.client_cmd[index].cmd_info[monitor_idx].level.npa_state = client_ptr->cmd_hist[latest_hist_idx].level.npa_state;
              }
              else if (log_ptr->output.client_cmd[index].cmd_info[monitor_idx].monitor == CFCM_MONITOR_THERMAL_CX ) 
              {
                log_ptr->output.client_cmd[index].cmd_info[monitor_idx].level.npa_state = client_ptr->cmd_hist[latest_hist_idx].level.npa_state;
              }
              else if (log_ptr->output.client_cmd[index].cmd_info[monitor_idx].monitor ==  CFCM_MONITOR_MDM_TEMP ) 
              {
                log_ptr->output.client_cmd[index].cmd_info[monitor_idx].level.npa_state = client_ptr->cmd_hist[latest_hist_idx].level.npa_state;
              }
              else if (log_ptr->output.client_cmd[index].cmd_info[monitor_idx].monitor == CFCM_MONITOR_VDD_PEAK_CURR_EST) 
              {
                log_ptr->output.client_cmd[index].cmd_info[monitor_idx].level.npa_state = client_ptr->cmd_hist[latest_hist_idx].level.npa_state;
              }
              else
              {
                log_ptr->output.client_cmd[index].cmd_info[monitor_idx].level.dsm_level = client_ptr->cmd_hist[latest_hist_idx].level.dsm_level;
              }
            }//if monitor in mask
          }//for each monitor
        }
        else
        {
          log_ptr->output.client_cmd[index].client_id = CFCM_CLIENT_MAX;
        }
      }   
  
      /* submit the log to DIAG */
      log_commit( log_ptr );
    }
    else
    {
      CFCM_MSG_1(MED, "Could not allocate log pkt of size %d", log_size);
    }
  }
#else
  if (hist_idx == NULL)
  {
    CFCM_MSG_0(ERROR, "INVALID arguments to cfcm_log_command_output");
    return ;
  }
  CFCM_UNUSED(hist_idx);
  cfcm_client_check_history_idxs_change(hist_idx);
#endif /* LOG_MCS_CFCM_COMMAND_INFO_C */
}

/*=============================================================================

  FUNCTION:  cfcm_log_update_cpu_thrshlds

=============================================================================*/
/*!
    @brief
    Updates CPU thresholds for logging purpose
 
    @return
    None
*/
/*===========================================================================*/
void cfcm_log_update_cpu_thrshlds
(
  cfcm_log_cpu_thrshld_type* thrshld
)
{
  uint32 size = 0; 
  size = (CFCM_CLIENT_MAX * sizeof(cfcm_log_cpu_thrshld_type));
  if (thrshld != NULL)
  {
    memscpy(cpu_thrshld, size, thrshld, size);
  }
}

