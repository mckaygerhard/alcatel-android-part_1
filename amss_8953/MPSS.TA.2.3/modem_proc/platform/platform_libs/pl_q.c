#include "platform_libs.h"
#include "platform_libs.c"
#include <assert.h>
#include <stdio.h>

int foo_init(void) {
   static int ii = 0;
   ii++;
   assert(ii = 1);
   return 0;
}


PL_DEFINE(foo, foo_init, 0)

int bar_init(void) {
   return PL_INIT(foo);
}

void bar_deinit(void) {
   PL_DEINIT(foo);
}

PL_DEFINE(bar, bar_init, bar_deinit)

int fail_init(void) {
   return -2;
}

void fail_deinit(void) {
   assert(0);
}

PL_DEP(fail)
PL_DEFINE(fail, fail_init, fail_deinit)
int ifirst = 0;
int ilast = 0;
int first_init(void) {
   ifirst++;
   assert(ilast == 0);
   return 0;
}
void first_deinit(void) {
   assert(ilast == 0);
   ifirst--;
}
int last_init(void) {
   assert(ifirst == 1);
   ilast++;
   return 0;
}

void last_deinit(void) {
   assert(ifirst == 1);
   ilast--;
}

PL_DEFINE(first, first_init, first_deinit)
PL_DEFINE(last, last_init, last_deinit)

struct platform_lib* (*pl_list[])(void) = {
   PL_ENTRY(first),
   PL_ENTRY(foo),
   PL_ENTRY(bar),
   PL_ENTRY(last),
   0
};

struct platform_lib* (*pl_list_cust[])(void) = { 
   PL_ENTRY(fail),
   0 
};


int main(void) {
   assert(0 == pl_init());
   assert(PL_ENTRY(foo)()->uRefs == 2);
   assert(PL_ENTRY(bar)()->uRefs == 1);
   assert(PL_INIT(fail) == -2);
   assert(PL_ENTRY(fail)()->uRefs == 1);
   pl_deinit();
   assert(PL_ENTRY(foo)()->uRefs == 0);
   assert(PL_ENTRY(bar)()->uRefs == 0);
   assert(PL_ENTRY(fail)()->uRefs == 1);
   assert(0 != pl_init_lst(pl_list_cust));
   return 0;
}
