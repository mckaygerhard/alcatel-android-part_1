#define QTEST
#include "qtest_stdlib.h"
#include "QTestEnv.h"
#include "version.h"
#include <assert.h>
#include <stdlib.h>

const char qtest_malloc_version[] = VERSION_STRING ;
static QTestEnv * gpqm = 0;

static __inline IEnv*  qtest_IEnv(void) {
   if(0 == gpqm)  {
      gpqm = QTestEnv_New(0);
   }
   assert(0 != gpqm);
   return QTestEnv_ToIEnv(gpqm);
}

void qtest_set_failure_mask(uint32 mask) {
   (void)qtest_IEnv();
   QTestEnv_SetFailureMask(gpqm, mask);
}
extern void QTestEnv_SetPassCount(QTestEnv *me, int nCount);
void qtest_set_pass_count(int count) {
   (void)qtest_IEnv();
   QTestEnv_SetPassCount(gpqm, count);
}

void qtest_get_failure_mask(uint32* pmask) {
   (void)qtest_IEnv();
   QTestEnv_GetFailureMask(gpqm, pmask);
}


int qtest_test_failure(void) {
   (void)qtest_IEnv();
   return QTestEnv_TestFailure(gpqm);
}

void qtest_done(void) {
   if(0 != gpqm)  {
      QTestEnv_Done(gpqm);
      gpqm = 0;
   }
}

void* qtest_malloc(const char* name, char* stack, int sz) {
   void* rv = 0;
   if(0 != IEnv_ErrMallocNameNoZI(qtest_IEnv(), sz, &rv, stack == 0 ? name : stack)) { 
      rv = 0;
   }
   if(stack) {
      free(stack);
   }
   return rv;
}

void* qtest_calloc(const char* name, char* stack, int cnt, int sz) {
   void* rv = 0;
   assert(cnt * sz > 0);
   if(0 != IEnv_ErrMallocName(qtest_IEnv(), cnt * sz, &rv, stack == 0 ? name : stack)) { 
      rv = 0;
   }
   if(stack) {
      free(stack);
   }
   return rv;
}

void* qtest_realloc(const char* name, char* stack, void* ptr, int sz) {
   if(0 != IEnv_ErrReallocNameNoZI(qtest_IEnv(), sz, &ptr, stack == 0 ? name : stack)) { 
      ptr = 0;
   }
   if(stack) {
      free(stack);
   }
   return ptr;
}

void qtest_free(const char* name, char* stack, void* rv) {
   int nErr = IEnv_ErrReallocName(qtest_IEnv(), 0, &rv, stack == 0 ? name : stack);
   (void)nErr;
   assert(0 == nErr);
   if(stack) {
      free(stack);
   }
}

void qtest_atexit( void (*pfnAtExit)(void* pCtx), void* pvCxt) {
   int nErr = IEnv_AtExit(qtest_IEnv(), pfnAtExit, pvCxt);
   (void)nErr;
   assert(0 == nErr);
}
