/*
=======================================================================

FILE:         std_mem.c

SERVICES:     apiOne std lib memory operations stuff

=======================================================================
Copyright (c) 2008,2013 QUALCOMM Technologies Inc. All Rights Reserved.
Qualcomm Technologies Confidential and Proprietary
=======================================================================
*/

#include <string.h>
#include "AEEstd.h"

void* std_memset(void* p, int c, int nLen)
{
   if (nLen < 0) {
      return p;
   }
   return memset(p, c, (size_t)nLen);
//
//   this code commented out and libc version used for performance
//
//   char* pc = p;
//   while (nLen-- > 0) {
//      *pc++ = c;
//   }
//   return p;
}

void* std_memmove(void* pTo, const void* cpFrom, int nLen)
{
   if (nLen <= 0) {
      return pTo;
   }
   return memmove(pTo, cpFrom, (size_t)nLen);
//
//   this code commented out and libc version used for performance
//
//   char* pcTo = pTo;
//   const char* cpcFrom = cpFrom;
//
//   if (pcTo < cpcFrom) {
//      /* run forwards */
//      while (nLen-- > 0) {
//         *pcTo++ = *cpcFrom++;
//      }
//   } else if (pcTo > cpcFrom) {
//      /* run backwards */
//
//      pcTo += nLen;
//      cpcFrom += nLen;
//
//      while (nLen-- > 0) {
//         *--pcTo = *--cpcFrom;
//      }
//   }
//   return pTo;
}

