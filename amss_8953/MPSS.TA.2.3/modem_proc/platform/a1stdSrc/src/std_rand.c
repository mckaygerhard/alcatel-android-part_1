/*======================================================================
        Copyright (c) 2005-2007 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary and Confidential
======================================================================*/
#include "AEEstd.h"

// Defines the 'a' value used by the generator
#define  RAN_A_VAL  16807

// Defines the 'm' value used by the generator
// Do not modify this value. It is used as a bit mask and thus depends
// on the bit pattern.
#define  RAN_M_VAL  2147483647

unsigned std_rand_next(unsigned uRand)
{
   return uRand * 214013L + 2531011L;
}

static uint32 std_rand_next_new( uint32* puSeed )
{
   /*------------------------------------------------------------------------
    This function calculates the next random number using the formula:
    
          z(n) = a * z(n-1) mod( 2^31 - 1 )
          
    The value of z(n) is calculated as follows using a 32-bit wordsize:
    
    1. Shift z(n-1) left by one bit so it is right justified.
    
    2. Multiply a*z(n-1) and place the result into two 32-bit words called
       uAzHi and uAzLo.
       
       At this point, we have:   a*z(n-1) = uAzHi*(2^32) + uAzLo
       
    3. Keep the lower 31 bits of the product in the 32-bit word uAzLo, and
       place the remaining upper 15 bits in the 32-bit word uAzHi.
       
       At this point, we have:   a*z(n-1) = uAzHi*(2^31) + uAzLo
       which can be rewritten:   a*z(n-1) = uAzHi*(2^31-1) + (uAzLo+uAzHi)
       
    4. To get a*z(n-1)mod(2^31-1), add uAzHi to uAzLo and check to see if
       the result is equal to or greater than 2^31-1.  If it is, subtract
       off 2^31-1. (This is why z(n-1) was right justified in step 1)
    
    5. Finally, shift the result left by one bit to make z(n) a left
       justified binary fraction again.        
  ------------------------------------------------------------------------*/
  
  uint32 uAzHi = 0;
  uint32 uAzLo = 0;
  uint32 uSeed = 0;

  // Shift ran_reg left one bit so we can use MSB as an overflow bit
  (*puSeed) >>= 1;
  uSeed = *puSeed;

  // Guard against zero seed
  if( 0 == uSeed )
  {
     uSeed = 2531011L;
  }

  // Multiply a*z(n-1), putting the lower 31 bits into uAzLo, and the
  // remaining upper bits into uAzHi

  uAzLo = RAN_A_VAL * (uSeed & 0xFFFF);
  uAzHi = RAN_A_VAL * (uSeed >> 16) + (uAzLo >> 16);

  uAzLo = ( (uAzHi << 16) | (uAzLo & 0xFFFF) ) & RAN_M_VAL;
  uAzHi >>= 15;

  // Add the upper bits to the lower bits to get a*z(n-1)mod(2^31-1), if the
  // result is not less than the modulus, subtract the modulus from the result

  uAzLo += uAzHi;

  if( uAzLo >= RAN_M_VAL )
  { 
    uAzLo -= RAN_M_VAL;
  }

  // Place the result into ran_reg and shift it back to a left justified
  // binary fraction

  *puSeed = uAzLo << 1;

  return ((*puSeed) >> 1);

}

uint32 std_rand( uint32 uSeed, byte* pDest, int nSize )
{
   uint32 uRandData = 0;

   while (nSize-- > 0) 
   {
      if (uRandData < 0x100)
      {
            uRandData = std_rand_next_new( &uSeed ) | 0x80000000;
      }

      *pDest++ = (byte)uRandData;
      uRandData >>= 8;
   }

   return uSeed;
}

