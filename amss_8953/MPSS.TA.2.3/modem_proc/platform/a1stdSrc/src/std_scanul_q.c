/*
=======================================================================
        Copyright (c) 2005 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary and Confidential
=======================================================================
*/

#include <stdio.h>
#include "AEEstd.h"
#include "AEEStdErr.h"

#define TEST_CASE_BOTH(args) \
   Test_Case args; \
   Test_Case_1 args

#define TEST_CASE_UL(args) Test_Case args
#define TEST_CASE_ULL(args) Test_Case_1 args

typedef struct {
   int err;
} Test;

static void Test_Case(Test *pt, int nRadix, const char *psz, int errExpected, unsigned long ulExpected, int cntExpected)
{
   static int nCase = 0;

   const char *pszEnd = 0;
   unsigned long ul = 0x99119;
   int bFail = 0;
   int err;
   
   ++nCase;

   //   err = std_scanul(psz, nRadix, &pszEnd, &ul);

   ul = std_scanul(psz, nRadix, &pszEnd, &err);
   
   if (err != errExpected) {
      printf("  Return value %d != %d\n", err, errExpected);
      bFail = 1;
   }

   if (ul != ulExpected) {
      printf("  Value %lu != %lu\n", ul, ulExpected);
      bFail = 1;
   }

   if ((pszEnd - psz) != cntExpected) {
      printf("  Count %d != %d\n", (int)(pszEnd - psz), cntExpected);
      bFail = 1;
   }

   if (bFail) {
      printf("std_scanul() Error: case %d (%d, \"%s\", %d, %lu, %d)\n", nCase, nRadix, psz, errExpected, ulExpected, cntExpected);
      ++ pt->err;
   }
}

static void Test_Case_1(Test *pt, int nRadix, const char *psz, int errExpected, 
                        unsigned long long ullExpected, int cntExpected)
{
   static int nCase = 0;

   const char *pszEnd = 0;
   unsigned long long ull = 0;
   int bFail = 0;
   int err;
   
   ++nCase;

   ull = std_scanull(psz, nRadix, &pszEnd, &err);
   
   if (err != errExpected) {
      printf("  Return value %d != %d\n", err, errExpected);
      bFail = 1;
   }

   if (ull != ullExpected) {
      printf("  Value %llu != %llu\n", ull, ullExpected);
      bFail = 1;
   }

   if ((pszEnd - psz) != cntExpected) {
      printf("  Count %d != %d\n", (int)(pszEnd - psz), cntExpected);
      bFail = 1;
   }

   if (bFail) {
      printf("std_scanull() Error: case %d (%d, \"%s\", %d, %llu, %d)\n", 
             nCase, nRadix, psz, errExpected, ullExpected, cntExpected);
      ++ pt->err;
   }
}

static int Test_Suite(void)
{
   Test t;

   t.err = 0;

   // radix, string, radix      -> err, value, size

#define SUC AEE_SUCCESS
#define NEG STD_NEGATIVE
#define NOD STD_NODIGITS
#define OVR STD_OVERFLOW
#define BAD STD_BADPARAM

   // Radix validation
   
   TEST_CASE_BOTH((&t, -1, "12",          BAD,   0,   0));
   TEST_CASE_BOTH((&t,  1, "12",          BAD,   0,   0));
   TEST_CASE_BOTH((&t, 37, "12",          BAD,   0,   0));
                                      
   // Prefixes                        
                                      
   TEST_CASE_BOTH((&t,  0, "12",          SUC,  12,   2));
   TEST_CASE_BOTH((&t,  0, " 12",         SUC,  12,   3));
   TEST_CASE_BOTH((&t,  0, "+12",         SUC,  12,   3));
   TEST_CASE_BOTH((&t,  0, " +12",        SUC,  12,   4));
   TEST_CASE_BOTH((&t,  0, "--12",        NOD,   0,   0));
   TEST_CASE_BOTH((&t,  0, "+-12",        NOD,   0,   0));
   TEST_CASE_BOTH((&t,  0, "-+12",        NOD,   0,   0));
                                      
   TEST_CASE_BOTH((&t,  0, "012",         SUC,  10,   3));
   TEST_CASE_BOTH((&t,  8, "012",         SUC,  10,   3));
   TEST_CASE_BOTH((&t, 10, "012",         SUC,  12,   3));
   TEST_CASE_BOTH((&t,  0, "0x12",        SUC,  18,   4));
   TEST_CASE_BOTH((&t, 10, "0x12",        SUC,   0,   1));
   TEST_CASE_BOTH((&t, 10, "0X12",        SUC,   0,   1));
   TEST_CASE_BOTH((&t,  0, "0x0xc",       SUC,   0,   3)),
   TEST_CASE_BOTH((&t,  0, " +0x123fg",   SUC, 0x123f,8)),
                                      
   // Digits                          
                                      
   TEST_CASE_BOTH((&t,  0, "@12",         NOD,   0,   0));
   TEST_CASE_BOTH((&t, 36, "1/2",         SUC,   1,   1));
   TEST_CASE_BOTH((&t, 36, "1:2",         SUC,   1,   1));
   TEST_CASE_BOTH((&t, 36, "0@2",         SUC,   0,   1));
   TEST_CASE_BOTH((&t, 36, "9[2",         SUC,   9,   1));
   TEST_CASE_BOTH((&t, 36, "a`2",         SUC,  10,   1));
   TEST_CASE_BOTH((&t, 36, "z{2",         SUC,  35,   1));
   TEST_CASE_BOTH((&t, 35, "az2",         SUC,  10,   1));
   TEST_CASE_BOTH((&t, 11, "ab2",         SUC,  10,   1));
   TEST_CASE_BOTH((&t, 10, "9a2",         SUC,   9,   1));
   TEST_CASE_BOTH((&t,  9, "892",         SUC,   8,   1));
   TEST_CASE_BOTH((&t, 36, "1\377",       SUC,   1,   1));
                                      
   TEST_CASE_BOTH((&t, 36, "A",           SUC,  10,   1));
   TEST_CASE_BOTH((&t, 36, "Z",           SUC,  35,   1));

   // Overflow

   TEST_CASE_UL((&t,  0, "4294967295",  SUC, 4294967295U,  10));
   TEST_CASE_UL((&t,  0, "4294967296",  OVR,  MAX_UINT32,  10));
   TEST_CASE_UL((&t,  0, "42949672950", OVR,  MAX_UINT32,  11));
   TEST_CASE_UL((&t, 16, "FFFFFFFF",    SUC,  0xFFFFFFFF,   8));
   TEST_CASE_UL((&t, 16, "EFFFFFFF0",   OVR,  MAX_UINT32,   9));

   // "Negative zero" is non-negative
   TEST_CASE_UL((&t,  0, "-0",          SUC,                  0, 2));
   TEST_CASE_UL((&t,  0, "-12",         NEG, (unsigned long)-12, 3));
   TEST_CASE_UL((&t,  0, "-4294967295", NEG,                  1, 11));
   TEST_CASE_UL((&t,  0, "-2147483648", NEG,         0x80000000, 11));
   TEST_CASE_UL((&t,  0,  "2147483647", SUC,         0x7fffffff, 10));
   TEST_CASE_UL((&t,  0,  "2147483648", SUC,         0x80000000, 10));
   TEST_CASE_UL((&t,  0, "-4294967296", OVR,         MAX_UINT32, 11));
                                      
   // string length & NODIGITS        
                                      
   TEST_CASE_BOTH((&t,  0, "-",           NOD,   0,   0));
   TEST_CASE_BOTH((&t,  0, " ",           NOD,   0,   0));
   
   TEST_CASE_BOTH((&t,  0, "0xy",         SUC,   0,   1));
   TEST_CASE_BOTH((&t,  8, "0xy",         SUC,   0,   1));
   TEST_CASE_BOTH((&t, 16, "0xy",         SUC,   0,   1));
   TEST_CASE_BOTH((&t, 10, "0xy",         SUC,   0,   1));

   // Try null ptrs

   (void) std_scanul("123", 0, 0, 0);
   (void) std_scanull("123", 0, 0, 0);

   // scanull() specific tests
   TEST_CASE_ULL((&t, 10, "1844674407370955167", SUC, 1844674407370955167uLL, 19));
   TEST_CASE_ULL((&t, 10, "9223372036854775807", SUC, 9223372036854775807uLL, 19));
   TEST_CASE_ULL((&t, 10, "18446744073709551615", SUC, MAX_UINT64, 20));
   TEST_CASE_ULL((&t, 10, "18446744073709551616", OVR, MAX_UINT64, 20));
   TEST_CASE_ULL((&t, 10, "1844674407370955161699", OVR, MAX_UINT64, 20));
   TEST_CASE_ULL((&t, 10, "1844674407370955161500", OVR, MAX_UINT64, 21));
   TEST_CASE_ULL((&t, 16, "0xFFFFFFFFFFFFFFFF", SUC, MAX_UINT64, 18)); 
   TEST_CASE_ULL((&t, 16, "0xFFFFFFFFFFFFFFFF1", OVR, MAX_UINT64, 19)); 
   TEST_CASE_ULL((&t, 8, "1777777777777777777777", SUC, MAX_UINT64, 22));
   TEST_CASE_ULL((&t, 0, "02000000000000000000000", OVR, MAX_UINT64, 23));

   TEST_CASE_ULL((&t, 0, "-2147483648", NEG, (uint64)-2147483648ll, 11));
   TEST_CASE_ULL((&t, 0, "-18446744073709551615", NEG, 1, 21));
   TEST_CASE_ULL((&t, 0, "-18446744073709551616", OVR, MAX_UINT64, 21));

   return t.err;
}


int main(int argc, char **argv)
{
   return Test_Suite();
}
