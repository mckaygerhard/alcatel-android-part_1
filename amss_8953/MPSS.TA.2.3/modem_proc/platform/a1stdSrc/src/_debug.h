#ifndef _DEBUG_H
#define _DEBUG_H
/*
=======================================================================

FILE:         _debug.h

SERVICES:     @@@TODO: fill me in!

DESCRIPTION:  @@@TODO: fill me in!

=======================================================================
        Copyright � 2005 Qualcomm Technologies Incorporated.
               All Rights Reserved.
            QUALCOMM Proprietary and Confidential
=======================================================================
*/
#include "AEEdbg.h"

#define FARF_ALWAYS        1    /* 0 turns me off */
#define FARF_ALWAYS_LEVEL  DBG_MSG_LEVEL_LOW

#define FARF_LOW           1    /* 0 turns me off */
#define FARF_LOW_LEVEL     DBG_MSG_LEVEL_LOW

#define FARF_MEDIUM        1    /* 0 turns me off */
#define FARF_MEDIUM_LEVEL  DBG_MSG_LEVEL_MEDIUM

#define FARF_HIGH          1    /* 0 turns me off */
#define FARF_HIGH_LEVEL    DBG_MSG_LEVEL_HIGH

#define FARF_ERROR         1    /* 0 turns me off */
#define FARF_ERROR_LEVEL   DBG_MSG_LEVEL_ERROR

#define FARF_FATAL         1    /* 0 turns me off */
#define FARF_FATAL_LEVEL   DBG_MSG_LEVEL_FATAL


#if defined(_DEBUG)

#include "AEEstd.h"

typedef struct farf_args {
   int         nLevel;
   const char* cpszFile;
   int         nLine;
} farf_args;


static __inline void farf(const farf_args* pfa, const char* cpszFormat, ...)
{
   char buf[256];
   AEEVaList args;

   AEEVA_START(args, cpszFormat);
   
   std_vstrlprintf(buf, sizeof(buf), cpszFormat, args);

   dbg_Message(buf, pfa->nLevel, pfa->cpszFile, pfa->nLine);

   AEEVA_END(args);
}

# define EVENT(e,pcv,n) dbg_Event(e,pcv,n)
# define BREAKPOINT()   dbg_Break()
# define VERIFY(x)      ((x) || (dbg_Break(), FALSE))
# define ASSERT(x)      (void)VERIFY(x)
# define FARF(x, p)     if (1 == FARF_##x)\
   {\
      farf_args F = {\
       FARF_##x##_LEVEL,\
       __FILENAME__,\
       __LINE__,\
      };\
      farf p ; \
   }

#else  /* #if defined(_DEBUG) */

# define EVENT(e,pcv,n) (void)0
# define BREAKPOINT()   (void)0
# define ASSERT(x)      (void)0
# define VERIFY(x)      (x) /* wanna evaluate (x), used in conditionals */
# define FARF(x, p)     (void)0

#endif /* #if defined(_DEBUG) */

#endif /* #ifndef _DEBUG_H */

