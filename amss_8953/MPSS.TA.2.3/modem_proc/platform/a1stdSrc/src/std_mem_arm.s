;=======================================================================
;        Copyright � 2008 Qualcomm Technologies Incorporated.
;               All Rights Reserved.
;            QUALCOMM Proprietary/GTDR
;=======================================================================

   EXPORT std_memset
   EXPORT std_memmove
   
   AREA STD_MEMOPS, CODE, READONLY
   CODE32

;=======================================================================
; void* std_memset(void* p, int c, int nLen)
;
; Aligns the memory block to 16 bytes before performing store multiple
; for optimal performance.
;=======================================================================

std_memset
   CMP      r2, #0
   BXLE     r14
   AND      r3, r1, #0xff
   STMFD    r13!, {r0, r14}
   MOV      r1, r2
   ORR      r2,r3,r3,lsl #8
   ORR      r2,r2,r2,lsl #16

   CMP      r1, #16
   BCC      _std_memset_lt_16
   ANDS     r12, r0, #0x0f               
   BEQ      _std_memset_align
   RSB      r12, r12, #16                 ; align to 16 byte boundary
   SUB      r1, r1, r12
   MOVS     r12, r12, lsr #1
   STRCSB   r2, [r0], #1
   TST      r12, #0x01
   STRNEH   r2, [r0], #2
   MOVS     r12, r12, lsl #30
   STRCS    r2, [r0], #4
   STRCS    r2, [r0], #4
   STRMI    r2, [r0], #4
   B        _std_memset_align

_std_memset_lt_16
   SUBS     r1, r1, #1                    ; nLen < 16
   STRPLB   r2, [r0], #1
   BPL      _std_memset_lt_16
   B        _std_memset_ret         

_std_memset_align
   SUBS     r1, r1, #0x20                 ; nLen >= 16 and memory is 16 byte 
   MOV      r14, r2                       ; aligned
   MOV      r3, r2
   MOV      r12, r2
_std_memset_loop
   STMCSIA  r0!, {r2, r3, r12, r14}
   STMCSIA  r0!, {r2, r3, r12, r14}
   SUBCSS   r1, r1, #0x20
   BCS      _std_memset_loop
   MOVS     r1, r1, lsl #28               ; nLen < 32 and memory is 16 byte 
   STMCSIA  r0!, {r2, r3, r12, r14}       ; aligned
   STMMIIA  r0!, {r2, r3}
   MOVS     r1, r1, lsl #2
   STRCS    r2, [r0], #4
   BEQ      _std_memset_ret
   STRMIH   r2, [r0], #2
   TST      r1, #0x40000000
   STRNEB   r2, [r0], #1    

_std_memset_ret
   LDMFD    r13!, {r0, pc}

;=======================================================================
; void *std_memmove(void *pTo, const void *cpFrom, int nLen)
;
; Aligns the memory block to 16 bytes before performing the copy. 
; Non-overlapped case: Aligns destination buffer to 16 bytes and performs
;                      multiple load / store operations if the source is
;                      aligned at 16 byte boundary.               
; Overlapped case: Aligns destination buffer to 16 bytes and copies one
;                  word at a time.
;=======================================================================

std_memmove
   CMP      r2, #0
   BXLE     r14
   STMDB    r13!, {r0, r14}
   SUBS     r3, r0, r1                     ; check for overlapped buffer
   CMPCS    r2, r3
   BHI      _std_memmove

_std_memcpy
   CMP      r2, #16                        ; src and dest are not overlapped
   BLS      _std_memcpy_last_few
   ANDS     r12, r0, #0x0f
   BEQ      _std_memcpy_dest_aligned
   RSB      r12, r12, #16                  ; align dest to 16 byte boundary
   SUB      r2, r2, r12

_std_memcpy_align_dest
   SUBS     r12, r12, #1
   LDRPLB   r3, [r1], #1
   STRPLB   r3, [r0], #1
   BPL      _std_memcpy_align_dest

_std_memcpy_dest_aligned
   ANDS     r3, r1, #0x03
   BEQ      _std_memcpy_aligned
   LDR      r14, [r1, -r3]!                ; check for src alignment
   CMP      r3, #0x2
   BEQ      _std_memcpy_src2
   BHI      _std_memcpy_src3

_std_memcpy_src1
   SUBS     r2, r2, #0x10                  ; src is 1 byte aligned and 
   BCC      _std_memcpy_src1_last_few      ; dest is 16 byte aligned
   STMDB    r13!, {r4, r5}
_std_memcpy_src1_loop
   MOV      r3, r14, lsr #0x8
   LDMIB    r1!, {r4-r5, r12, r14}         ; shift as below to perform
   ORR      r3, r3, r4, lsl #0x18          ; aligned loads and stores:
   MOV      r4, r4, lsr #0x8               ; (a1 >> 8) | (a2 << 24)
   ORR      r4, r4, r5, lsl #0x18
   MOV      r5, r5, lsr #0x8
   ORR      r5, r5, r12, lsl #0x18
   MOV      r12, r12, lsr #0x8
   ORR      r12, r12, r14, lsl #0x18
   STMIA    r0!, {r3-r5, r12}
   SUBS     r2, r2, #0x10
   BCS      _std_memcpy_src1_loop
   LDMFD    r13!, {r4, r5}

_std_memcpy_src1_last_few
   MOVS     r2, r2, lsl #0x1D              ; nLen < 16, dest is 16 byte aligned
   MOVCS    r3, r14, lsr #0x8              ; src is 1 byte aligned
   LDMCSIB  r1!, {r12, r14}
   ORRCS    r3, r3, r12, lsl #0x18
   MOVCS    r12, r12, lsr #0x8
   ORRCS    r12, r12, r14, lsl #0x18
   STMCSIA  r0!, {r3, r12}
   MOVMI    r3, r14, lsr #0x8
   LDRMI    r14, [r1, #0x4]!
   ORRMI    r3, r3, r14, lsl #0x18
   STRMI    r3, [r0], #0x4
   MOVS     r2, r2, lsl #0x2
   MOVCS    r3, r14, lsr #0x8
   STRCSH   r3, [r0], #2
   MOVCS    r14, r14, lsr #0x10
   MOVMI    r3, r14, lsr #0x8
   STRMIB   r3, [r0], #1
   B        _std_memcpy_ret

_std_memcpy_src2
   SUBS     r2, r2, #0x10                  ; src is 2 byte aligned and
   BCC      _std_memcpy_src2_last_few      ; dest is 16 byte aligned
   STMDB    r13!, {r4, r5}
_std_memcpy_src2_loop
   MOV      r3, r14, lsr #0x10
   LDMIB    r1!, {r4-r5, r12, r14}         ; shift as below to perform
   ORR      r3, r3, r4, lsl #0x10          ; aligned loads and stores:
   MOV      r4, r4, lsr #0x10              ; (a1 >> 10) | (a2 << 10)
   ORR      r4, r4, r5, lsl #0x10
   MOV      r5, r5, lsr #0x10
   ORR      r5, r5, r12, lsl #0x10
   MOV      r12, r12, lsr #0x10
   ORR      r12, r12, r14, lsl #0x10
   STMIA    r0!, {r3-r5, r12}
   SUBS     r2, r2, #0x10
   BCS      _std_memcpy_src2_loop
   LDMFD    r13!, {r4, r5}

_std_memcpy_src2_last_few
   MOVS     r2, r2, lsl #0x1D              ; nLen < 16, dest is 16 byte aligned
   MOVCS    r3, r14, lsr #0x10             ; src is 2 byte aligned
   LDMCSIB  r1!, {r12, r14}
   ORRCS    r3, r3, r12, lsl #0x10
   MOVCS    r12, r12, lsr #0x10
   ORRCS    r12, r12, r14, lsl #0x10
   STMCSIA  r0!, {r3, r12}
   MOVMI    r3, r14, lsr #0x10
   LDRMI    r14, [r1, #0x4]!
   ORRMI    r3, r3, r14, lsl #0x10
   STRMI    r3, [r0], #4
   MOVS     r2, r2, lsl #0x2
   MOVCS    r3, r14, lsr #0x10
   STRCSH   r3, [r0], #2
   BPL      _std_memcpy_ret
   LDRCS    r14, [r1, #0x4]!
   MOVCS    r14, r14, lsl #0x10
   MOVMI    r3, r14, lsr #0x10
   STRMIB   r3, [r0], #1
   B        _std_memcpy_ret

_std_memcpy_src3
   SUBS     r2, r2, #0x10                  ; src is 3 byte aligned and
   BCC      _std_memcpy_src3_last_few      ; dest is 16 byte aligned
   STMDB    r13!, {r4, r5}
_std_memcpy_src3_loop
   MOV      r3, r14, lsr #0x18
   LDMIB    r1!, {r4-r5, r12, r14}         ; shift as below to perform
   ORR      r3, r3, r4, lsl #0x8           ; aligned loads and stores:
   MOV      r4, r4, lsr #0x18              ; (a1 >> 24) | (a2 << 8)
   ORR      r4, r4, r5, lsl #0x8
   MOV      r5, r5, lsr #0x18
   ORR      r5, r5, r12, lsl #0x8
   MOV      r12, r12, lsr #0x18
   ORR      r12, r12, r14, lsl #0x8
   STMIA    r0!, {r3-r5, r12}
   SUBS     r2, r2, #0x10
   BCS      _std_memcpy_src3_loop
   LDMFD    r13!, {r4, r5}

_std_memcpy_src3_last_few
   MOVS     r2, r2, lsl #0x1D              ; nLen < 16, dest is 16 byte aligned
   MOVCS    r3, r14, lsr #0x18             ; src is 3 byte aligned
   LDMCSIB  r1!, {r12, r14}
   ORRCS    r3, r3, r12, lsl #0x8
   MOVCS    r12, r12, lsr #0x18
   ORRCS    r12, r12, r14, lsl #0x8
   STMCSIA  r0!, {r3, r12}
   MOVMI    r3, r14, lsr #0x18
   LDRMI    r14, [r1, #0x4]!
   ORRMI    r3, r3, r14, lsl #0x8
   STRMI    r3, [r0], #4
   MOVS     r2, r2, lsl #0x2
   MOVCS    r3, r14, lsr #0x18
   LDRCS    r14, [r1, #0x4]!
   ORRCS    r3, r3, r14, lsl #0x8
   STRCSH   r3, [r0], #2
   MOVCS    r14, r14, lsl #0x10
   MOVMI    r3, r14, lsr #0x18
   STRMIB   r3, [r0], #1
   B        _std_memcpy_ret

_std_memcpy_aligned
   SUBS     r2, r2, #0x20                  ; dest is 16 byte aligned and 
   STMDB    r13!, {r4, r14}                ; src is 4 byte aligned
   BCC      _std_memcpy_aligned_last_few
   
_std_memcpy_aligned_loop
   LDMCSIA  r1!, {r3-r4, r12, r14}
   STMCSIA  r0!, {r3-r4, r12, r14}
   LDMCSIA  r1!, {r3-r4, r12, r14}
   STMCSIA  r0!, {r3-r4, r12, r14}
   SUBCSS   r2, r2, #0x20
   BCS      _std_memcpy_aligned_loop

_std_memcpy_aligned_last_few
   MOVS     r12, r2, lsl #0x1C             ; nLen < 32, dest is 16 byte aligned
   LDMCSIA  r1!, {r3-r4, r12, r14}         ; and src is 4 byte aligned
   STMCSIA  r0!, {r3-r4, r12, r14}
   LDMMIIA  r1!, {r3-r4}
   STMMIIA  r0!, {r3-r4}
   LDMFD    r13!, {r4, r14}
   MOVS     r12, r2, lsl #0x1E
   LDRCS    r3, [r1], #4
   STRCS    r3, [r0], #4
   BEQ      _std_memcpy_ret
   MOVS     r2, r2, lsl #0x1F
   LDRMIB   r2, [r1], #0x1
   LDRCSB   r3, [r1], #0x1
   LDRCSB   r12, [r1], #0x1
   STRMIB   r2, [r0], #0x1
   STRCSB   r3, [r0], #0x1
   STRCSB   r12, [r0], #0x1

_std_memcpy_ret
   LDMFD    r13!, {r0, pc}

_std_memcpy_last_few
   SUBS     r2, r2, #1                    ; nLen < 16
   LDRPLB   r3, [r1], #1
   STRPLB   r3, [r0], #1
   BPL      _std_memcpy_last_few
   B        _std_memcpy_ret

_std_memmove                              ; src and dest are overlapped
   CMP      r0, r1                        ; skip if src and dest are the same
   BEQ      _std_memmove_ret
   CMP      r2, #16
   ADD      r0, r0, r2
   ADD      r1, r1, r2
   BLS      _std_memmove_last_few
   ANDS     r12, r0, #0x0f
   BEQ      _std_memmove_dest_aligned
   SUB      r2, r2, r12

_std_memmove_align_dest
   SUBS     r12, r12, #1                  ; align dest to 16 byte boundary
   LDRPLB   r3, [r1, #-0x1]!
   STRPLB   r3, [r0, #-0x1]!
   BPL      _std_memmove_align_dest

_std_memmove_dest_aligned
   ANDS     r14, r1, #0x03
   BEQ      _std_memmove_aligned
   LDR      r3, [r1, -r14]!               ; check for src alignment
   CMP      r14, #0x2
   BEQ      _std_memmove_src2
   BHI      _std_memmove_src3

_std_memmove_src1
   SUBS     r2, r2, #0x10                 ; src is 1 byte aligned and 
   BCC      _std_memmove_src1_last_few    ; dest is 16 byte aligned
   STMDB    r13!, {r4, r5}
_std_memmove_src1_loop
   MOV      r14, r3, lsl #0x18
   LDMDB    r1!, {r3-r5, r12}             ; shift as below to perform
   ORR      r14, r14, r12, lsr #0x8       ; aligned loads and stores:
   MOV      r12, r12, lsl #0x18           ; (a1 << 24) | (a2 >> 8)
   ORR      r12, r12, r5, lsr #0x8
   MOV      r5, r5, lsl #0x18
   ORR      r5, r5, r4, lsr #0x8
   MOV      r4, r4, lsl #0x18
   ORR      r4, r4, r3, lsr #0x8
   STMDB    r0!, {r4, r5, r12, r14}
   SUBS     r2, r2, #0x10
   BCS      _std_memmove_src1_loop
   LDMFD    r13!, {r4, r5}

_std_memmove_src1_last_few
   MOVS     r2, r2, lsl #0x1D             ; nLen < 16, dest is 16 byte aligned
   MOVCS    r14, r3, lsl #0x18            ; and src is 1 byte aligned
   LDMCSDB  r1!, {r3, r12}
   ORRCS    r14, r14, r12, lsr #0x8
   MOVCS    r12, r12, lsl #0x18
   ORRCS    r12, r12, r3, lsr #0x8
   STMCSDB  r0!, {r12, r14}
   MOVMI    r12, r3, lsl #0x18
   LDRMI    r3, [r1, #-0x4]!
   ORRMI    r12, r12, r3, lsr #0x8
   STRMI    r12, [r0, #-0x4]!
   MOVS     r2, r2, lsl #0x2
   MOVCS    r12, r3, lsl #0x8
   LDRCS    r3, [r1, #-0x4]!
   ORRCS    r12, r12, r3, lsr #0x18
   STRCSH   r12, [r0, #-0x2]!
   MOVCS    r3, r3, lsr #0x10
   STRMIB   r3, [r0, #-0x1]!
   B        _std_memmove_ret

_std_memmove_src2
   SUBS     r2, r2, #0x10                 ; src is 2 byte aligned and
   BCC      _std_memmove_src2_last_few    ; dest is 16 byte aligned
   STMDB    r13!, {r4, r5}
_std_memmove_src2_loop
   MOV      r14, r3, lsl #0x10
   LDMDB    r1!, {r3-r5, r12}             ; shift as below to perform
   ORR      r14, r14, r12, lsr #0x10      ; aligned loads and stores:
   MOV      r12, r12, lsl #0x10           ; (a1 << 10) | (a2 >> 10)
   ORR      r12, r12, r5, lsr #0x10
   MOV      r5, r5, lsl #0x10
   ORR      r5, r5, r4, lsr #0x10
   MOV      r4, r4, lsl #0x10
   ORR      r4, r4, r3, lsr #0x10
   STMDB    r0!, {r4, r5, r12, r14}
   SUBS     r2, r2, #0x10
   BCS      _std_memmove_src2_loop
   LDMFD    r13!, {r4, r5}

_std_memmove_src2_last_few
   MOVS     r2, r2, lsl #0x1D             ; nLen < 16, dest is 16 byte aligned
   MOVCS    r14, r3, lsl #0x10            ; and src is 2 byte aligned
   LDMCSDB  r1!, {r3, r12}
   ORRCS    r14, r14, r12, lsr #0x10
   MOVCS    r12, r12, lsl #0x10
   ORRCS    r12, r12, r3, lsr #0x10
   STMCSDB  r0!, {r12, r14}
   MOVMI    r12, r3, lsl #0x10
   LDRMI    r3, [r1, #-0x4]!
   ORRMI    r12, r12, r3, lsr #0x10
   STRMI    r12, [r0, #-0x4]!
   MOVS     r2, r2, lsl #0x2
   STRCSH   r3, [r0, #-0x2]!
   BPL      _std_memmove_ret
   LDRCS    r3, [r1, #-0x4]!
   MOVCS    r3, r3, lsr #0x10
   MOVMI    r3, r3, lsr #0x8
   STRMIB   r3, [r0, #-0x1]!
   B        _std_memmove_ret

_std_memmove_src3
   SUBS     r2, r2, #0x10                 ; src is 3 byte aligned and
   BCC      _std_memmove_src3_last_few    ; dest is 16 byte aligned
   STMDB    r13!, {r4, r5}
_std_memmove_src3_loop
   MOV      r14, r3, lsl #0x8
   LDMDB    r1!, {r3-r5, r12}             ; shift as below to perform
   ORR      r14, r14, r12, lsr #0x18      ; aligned loads and stores:
   MOV      r12, r12, lsl #0x8            ; (a1 << 8) | (a2 >> 24)
   ORR      r12, r12, r5, lsr #0x18
   MOV      r5, r5, lsl #0x8
   ORR      r5, r5, r4, lsr #0x18
   MOV      r4, r4, lsl #0x8
   ORR      r4, r4, r3, lsr #0x18
   STMDB    r0!, {r4, r5, r12, r14}
   SUBS     r2, r2, #0x10
   BCS      _std_memmove_src3_loop
   LDMFD    r13!, {r4, r5}

_std_memmove_src3_last_few
   MOVS     r2, r2, lsl #0x1D             ; nLen < 16, dest is 16 byte aligned
   MOVCS    r14, r3, lsl #0x8             ; and src is 3 byte aligned
   LDMCSDB  r1!, {r3, r12}
   ORRCS    r14, r14, r12, lsr #0x18
   MOVCS    r12, r12, lsl #0x8
   ORRCS    r12, r12, r3, lsr #0x18
   STMCSDB  r0!, {r12, r14}
   MOVMI    r12, r3, lsl #0x8
   LDRMI    r3, [r1, #-0x4]!
   ORRMI    r12, r12, r3, lsr #0x18
   STRMI    r12, [r0, #-0x4]!
   MOVS     r2, r2, lsl #0x2
   MOVCS    r12, r3, lsr #0x8
   STRCSH   r12, [r0, #-0x2]!
   MOVCS    r3, r3, lsl #0x10
   MOVMI    r3, r3, lsr #0x10
   STRMIB   r3, [r0, #-0x1]!
   B        _std_memmove_ret

_std_memmove_aligned
   SUBS     r2,r2,#0x20                   ; dest is 16 byte aligned and
   STMDB    r13!, {r4, r14}               ; src is 4 byte aligned
   BCC      _std_memmove_aligned_last_few

_std_memmove_aligned_loop 
   LDMDB    r1!, {r3-r4, r12, r14}
   STMDB    r0!, {r3-r4, r12, r14}
   LDMDB    r1!, {r3-r4, r12, r14}
   STMDB    r0!, {r3-r4, r12, r14}
   SUBS     r2, r2, #0x20
   BCS     _std_memmove_aligned_loop

_std_memmove_aligned_last_few
   MOVS     r12, r2, lsl #0x1C            ; nLen < 32, dest is 16 byte aligned
   LDMCSDB  r1!, {r3-r4, r12, r14}        ; and src is 4 byte aligned
   STMCSDB  r0!, {r3-r4, r12, r14}
   LDMMIDB  r1!, {r3-r4}
   STMMIDB  r0!, {r3-r4}
   LDMFD    r13!, {r4, r14}
   MOVS     r12, r2, lsl #0x1E
   LDRCS    r3, [r1, #-0x4]!
   STRCS    r3, [r0, #-0x4]!
   BEQ      _std_memmove_ret
   MOVS     r2, r2, lsl #0x1F
   LDRCSB   r3, [r1, #-0x1]!
   LDRCSB   r12, [r1, #-0x1]!
   LDRMIB   r2, [r1, #-0x1]!
   STRCSB   r3, [r0, #-0x1]!
   STRCSB   r12, [r0, #-0x1]!
   STRMIB   r2, [r0, #-0x1]!

_std_memmove_ret
   LDMFD    r13!, {r0, pc}

_std_memmove_last_few
   SUBS     r2, r2, #1                    ; nLen < 16
   LDRPLB   r3, [r1, #-0x1]!
   STRPLB   r3, [r0, #-0x1]!
   BPL      _std_memmove_last_few
   B        _std_memmove_ret
  
   END
