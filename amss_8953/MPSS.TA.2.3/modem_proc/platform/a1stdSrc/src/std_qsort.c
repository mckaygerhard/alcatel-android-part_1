#include "AEEstd.h"

typedef struct {
   int nElemWidth;
   int (*pfnCompare)(void*, const void*, const void*);
   void* pCompareCx;
   byte* pPivot;
} __qsort_args;

/* TODO:
   the below sucks, but I wasn't able to get there with token pasting
 */
#undef __QSORT
#define __QSORT                      __qsort_byte
#undef __QSORT_VALMOVE
#define __QSORT_VALMOVE(_dst, _src)  (*(byte*)(_dst) = *(byte*)(_src))
#undef __QSORT_VALSWAP
#define __QSORT_VALSWAP(_dst, _src)  do { if ((_dst) != (_src)) {\
                                          byte _tv = *(byte*)(_dst); \
                                         *(byte*)(_dst) = *(byte*)(_src);\
                                         *(byte*)(_src) = _tv; } } while (0)
#undef __QSORT_WIDTH
#define __QSORT_WIDTH                sizeof(byte)

#include "__qsort_template.c"

#undef __QSORT
#define __QSORT                      __qsort_uint16
#undef __QSORT_VALMOVE
#define __QSORT_VALMOVE(_dst, _src)  (*(uint16*)(_dst) = *(uint16*)(_src))
#undef __QSORT_VALSWAP
#define __QSORT_VALSWAP(_dst, _src)  do { if ((_dst) != (_src)) {\
                                          uint16 _tv = *(uint16*)(_dst); \
                                         *(uint16*)(_dst) = *(uint16*)(_src);\
                                         *(uint16*)(_src) = _tv; } } while (0)
#undef __QSORT_WIDTH
#define __QSORT_WIDTH                sizeof(uint16)

#include "__qsort_template.c"

#undef __QSORT
#define __QSORT                      __qsort_uint32
#undef __QSORT_VALMOVE
#define __QSORT_VALMOVE(_dst, _src)  (*(uint32*)(_dst) = *(uint32*)(_src))
#undef __QSORT_VALSWAP
#define __QSORT_VALSWAP(_dst, _src)  do { if ((_dst) != (_src)) {\
                                          uint32 _tv = *(uint32*)(_dst); \
                                         *(uint32*)(_dst) = *(uint32*)(_src);\
                                         *(uint32*)(_src) = _tv; } } while (0)
#undef __QSORT_WIDTH
#define __QSORT_WIDTH                sizeof(uint32)

#include "__qsort_template.c"

#undef __QSORT
#define __QSORT                      __qsort_uint64
#undef __QSORT_VALMOVE
#define __QSORT_VALMOVE(_dst, _src)  (*(uint64*)(_dst) = *(uint64*)(_src))
#undef __QSORT_VALSWAP
#define __QSORT_VALSWAP(_dst, _src)  do { if ((_dst) != (_src)) {\
                                          uint64 _tv = *(uint64*)(_dst);\
                                         *(uint64*)(_dst) = *(uint64*)(_src);\
                                         *(uint64*)(_src) = _tv; } } while (0)
#undef __QSORT_WIDTH
#define __QSORT_WIDTH                sizeof(uint64)

#include "__qsort_template.c"

#undef __QSORT
#define __QSORT                      __qsort_arbitrary
#undef __QSORT_VALMOVE
#define __QSORT_VALMOVE(_dst,_src)   std_memmove((_dst), (_src), args->nElemWidth)
#undef __QSORT_VALSWAP
#undef __QSORT_WIDTH

#include "__qsort_template.c"

#undef __QSORT
#define __QSORT                      __qsort_arbitrary_inplace
#undef __QSORT_VALMOVE
#undef __QSORT_VALSWAP
#undef __QSORT_WIDTH
#define __QSORT_INPLACE

#include "__qsort_template.c"

void std_qsort(void* pElems, int nNumElems, int nElemWidth,
               int (*pfnCompare)(void*, const void*, const void*),
               void* pCompareCx)
{
   __qsort_args args;
   uint64       uPivot; /* enough memory for all the uint cases */

   if (nNumElems < 2) {
      return;
   }
   if (nElemWidth < 1) {
      return;
   }

   args.nElemWidth = nElemWidth;
   args.pfnCompare = pfnCompare;
   args.pCompareCx = pCompareCx;
   args.pPivot     = (byte*)(void*)&uPivot;

   if (1 == nElemWidth) {

      __qsort_byte(pElems, nNumElems, &args);

   } else if (2 == nElemWidth && 0 == ((uint32)pElems & 1)) {

      __qsort_uint16(pElems, nNumElems, &args);

   } else if (4 == nElemWidth && 0 == ((uint32)pElems & 3)) {

      __qsort_uint32(pElems, nNumElems, &args);

   } else if (8 == nElemWidth && 0 == ((uint32)pElems & 7)) {

      __qsort_uint64(pElems, nNumElems, &args);

   } else {
#ifndef STD_QSORT_BIG_PIVOT /* my Q test overrides this */
#define STD_QSORT_BIG_PIVOT 128
#endif 
      if (nElemWidth <= STD_QSORT_BIG_PIVOT) {
         byte abPivot[STD_QSORT_BIG_PIVOT];
         args.pPivot = abPivot;
         __qsort_arbitrary(pElems, nNumElems, &args);
      } else {
         __qsort_arbitrary_inplace(pElems, nNumElems, &args);
      }
   }

}



