
static void __TEST_PRINT(void* p, int num)
{
   int i;
   __TEST_TYPE* vals = (__TEST_TYPE*)p;
   for (i = 0; i < num; i++) {
      printf(__TEST_PRINT_FORMAT, vals[i]);
   }
   printf("\n");
}

static int __TEST_COMPARE(void* p, const void* u1, const void* u2)
{
   return (int)(*(__TEST_TYPE*)u1 - *(__TEST_TYPE*)u2);
}

static struct {
   __TEST_TYPE test[10];
   __TEST_TYPE expect[10];
   int         num;
} __TESTS[] = {
   {
      { 0, 1, },
      { 0, 1, },
      2,
   },
   {
      { 1, 0, },
      { 0, 1, },
      2,
   },
   {
      { 1, 0, 1, },
      { 0, 1, 1, },
      3,
   },
   {
      { 1, 1, 0, },
      { 0, 1, 1, },
      3,
   },
   {
      { 0, 1, 1, },
      { 0, 1, 1, },
      3,
   },
   {
      { 1, 2, 3, 4, 5 },
      { 1, 2, 3, 4, 5 },
      5,
   },
   {
      { 5, 4, 3, 2, 1 },
      { 1, 2, 3, 4, 5 },
      5,
   },
   {
      { 3, 4, 5, 2, 1 },
      { 1, 2, 3, 4, 5 },
      5,
   },

};
