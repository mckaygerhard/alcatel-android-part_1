#include "AEEstd.h"
#include "stdio.h"

#ifndef __FILENAME__
#define __FILENAME__ __FILE__
#endif

#define TEST(x) \
   do { if (!(x)) { \
      printf(__FILENAME__ ":%d: test failed: %s\n", __LINE__, #x); \
      errs++; \
   } } while (0)


#define TESTBITS(ctx,size) \
{\
   int ii; \
   for(ii = 1; ii < size - 1; ++ii) { \
      STD_BIT_SET(ctx.bits, ii); \
      TEST(ctx.post == 0x0); \
      TEST(ctx.pre == 0x0); \
      TEST(STD_BIT_TEST(ctx.bits, ii) != 0x0);\
      TEST(STD_BIT_TEST(ctx.bits, ii - 1) == 0x0);\
      TEST(STD_BIT_TEST(ctx.bits, ii + 1) == 0x0);\
      STD_BIT_CLEAR(ctx.bits, ii); \
      TEST(STD_BIT_TEST(ctx.bits, ii) == 0x0);\
      TEST(ctx.post == 0x0); \
      TEST(ctx.pre == 0x0); \
   }\
}

int bitclear(void) {
   int errs = 0;
   int ii;
   struct {
      uint32 pre;
      uint32 bits;
      uint32 post;
   } ctx = {0};
   for(ii = 0; ii < 32; ++ii) {
      STD_BIT_SET(&ctx.bits, ii);
   }
   TEST(ctx.pre == 0x0);
   TEST(ctx.post == 0x0);
   TEST(ctx.bits == 0xffffffff);
   for(ii = 0; ii < 32; ++ii) {
      STD_BIT_CLEAR(&ctx.bits, ii);
   }
   TEST(ctx.pre == 0x0);
   TEST(ctx.post == 0x0);
   TEST(ctx.bits == 0x0);
   return errs;
}


int main(void)
{
   int errs = 0;
   struct {
      int pre;
      int bits[1];
      int post; 
   } intbits = {0};
   struct {
      int pre;
      char bits[64];
      int post;
   } charbits = {0};
   TEST(0 == bitclear());
   TESTBITS(intbits, sizeof(intbits.bits) * 8);
   TESTBITS(charbits, sizeof(charbits.bits) * 8);
   return errs;
}
