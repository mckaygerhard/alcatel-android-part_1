#===============================================================================
# Copyrigha (c) 2012 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#===============================================================================

import SCons.Action
from SCons.Script import *

def exists(env):
   return env.Detect('qaic')

def add_qaic_header_dep(env, deps, idlname):
    pass

def generate(env):
    #public methods
    env.AddMethod(add_qaic_header_dep, "AddQaicHeaderDep")
