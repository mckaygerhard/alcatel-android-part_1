/*==============================================================================

Copyright (c) 2011 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "dlw.h"
#include "platform_libs.h"
#include "dlw_dlfcn.h"

#include "AEEatomic.h"

//static uint32 dlw_ref = 0;

// if two threads contend for the first call to dlinit assume success for
// thread that loses
//static int dlw_state = 1;

int dlw_Init(void)
{
  return 1;
}

void* dlw_Open(const char* file, int mode)
{
  return 0;
}

int dlw_Close(void* h)
{
  return -1;
}

void* dlw_Sym(void* __restrict h, const char* __restrict name)
{
  return 0;
}

int dlw_Addr(const void* __restrict h, dlw_info* __restrict info)
{
  return -1;
}

char* dlw_Error(void)
{
  const char* pszNone = "dlerror returned NULL";
  char* psz = 0; //dlerror();

  if (psz) {
    return psz;
  }
  return (char*)pszNone;
}

static int pl_dlw_Init(void) {
   return dlw_Init() == 1 ? 0 : -1;
}
PL_DEFINE(dlw, pl_dlw_Init, 0)
