/*==============================================================================

Copyright (c) 2011 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define VERIFY_PRINT_ERROR
#include "verify.h"

typedef struct _dl_info {
	const char	*dli_fname;	/* File defining the symbol */
	void		*dli_fbase;	/* Base address */
	const char	*dli_sname;	/* Symbol name */
	const void	*dli_saddr;	/* Symbol address */
} Dl_info;

int ret_val_dlinit = 1;
void* ret_val_dlopen = 0;
int ret_val_dlclose = 0;
void* ret_val_dlsym = 0;
int ret_val_dladdr = 0;
char ret_val_dlerror[] = "dlerror";

#define dlinit test_dlinit
#define dlopen test_dlopen
#define dlclose test_dlclose
#define dlsym test_dlsym
#define dladdr test_dladdr
#define dlerror test_dlerror

int dlinit(int n, char** libs)
{
  return ret_val_dlinit;
}

void* dlopen(const char* file, int mode)
{
  return ret_val_dlopen;
}

int dlclose(void* h)
{
  return ret_val_dlclose;
}

void* dlsym(void* __restrict h, const char* __restrict name)
{
  return ret_val_dlsym;
}

int dladdr(const void* __restrict h, Dl_info* __restrict info)
{
  return ret_val_dladdr;
}

char* dlerror(void)
{
  return ret_val_dlerror;
}

#define DLW_DLFCN_H
#include "dlw.h"
#include "dlw.c"


int test_dlw_Init(void)
{
  int nErr = 0;

  VERIFY(0 == dlw_ref);
  VERIFY(1 == dlw_state);

  ret_val_dlinit = 0;
  VERIFY(0 == dlw_Init());
  VERIFY(0 == dlw_Init());
  ret_val_dlinit = 1;
  VERIFY(0 == dlw_Init());
  VERIFY(3 == dlw_ref);
  VERIFY(0 == dlw_state);

bail:
  return nErr;
}

int dlw_Init(void);
void* dlw_Open(const char*, int);
int dlw_Close(void*);
void* dlw_Sym(void* __restrict, const char* __restrict);
int dlw_Addr(const void* __restrict, dlw_info* __restrict);
char* dlw_Error(void);


int test_dlw_Open(void)
{
  const char* file = 0;
  int mode = 0;
  int nErr = 0;

  VERIFY(ret_val_dlopen == dlw_Open(file, mode));

bail:
  return nErr;
}

int test_dlw_Close(void)
{
  void* h = 0;
  int nErr = 0;

  VERIFY(ret_val_dlclose == dlw_Close(h));

bail:
  return nErr;
}

int test_dlw_Sym(void)
{
  void* h = 0;
  const char* name = 0;
  int nErr = 0;

  VERIFY(ret_val_dlsym == dlw_Sym(h, name));

bail:
  return nErr;
}

int test_dlw_Addr(void)
{
  void* h = 0;
  dlw_info* info = 0;
  int nErr = 0;

  VERIFY(ret_val_dladdr == dlw_Addr(h, info));

bail:
  return nErr;
}

int test_dlw_Error(void)
{
  char* err = dlw_Error();
  int nErr = 0;

  VERIFY(!strcmp(err, ret_val_dlerror));

bail:
  return nErr;
}

int main(void)
{
  int nErr = 0;

  nErr += 
    test_dlw_Init() +
    test_dlw_Open() +
    test_dlw_Close() +
    test_dlw_Sym() +
    test_dlw_Addr() +
    test_dlw_Error();

  if (0 != nErr) {
    return 1;
  }
  return 0;
}

int pl_lib_init(struct platform_lib* (*plf)(void)) {
   return 0;
}
void pl_lib_deinit(struct platform_lib* (*plf)(void)) {
   return;
}
PL_DEFINE(svfs, 0, 0)
