#ifndef DLW_DLFCN_H
#define DLW_DLFCN_H
/*==============================================================================

Copyright (c) 2011 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.

==============================================================================*/
#ifndef _GNU_SOURCE 
#define _GNU_SOURCE 
#endif //_GNU_SOURCE 

#include <dlfcn.h>

#if !defined(__hexagon__) && !defined(hexagon)

static __inline int dlinit(int a, void* names) {
   return 1;
}

#endif // !defined(__hexagon__) && !defined(hexagon)

#endif // DLW_DLFCN_H

