/*==============================================================================
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include <stdlib.h>
#include <stdio.h>

// C externs
// TODO remove all these
#ifdef __cplusplus
extern "C"
{
#endif

#include "HAP_mem.h"
#include "HAP_debug.h"

#include "qurt_cycles.h"
#include "qurt_alloc.h"
#include "msg_diag_service.h"
#include "qurt_anysignal.h"
#include "qurt_sclk.h"
#include "qurt_thread.h"

#include "platform_libs.h"

/* TODO from ptrace.h, replace with #include when header becomes available */
enum __ptrace_request {
  /**
      @brief Indicate that the process making this request is requesting to be traced.
  */
  PTRACE_TRACEME = 0
#define PT_TRACE_ME PTRACE_TRACEME
};

/* TODO from ptrace.h, replace with #include when header becomes available */
long ptrace(enum __ptrace_request request, unsigned int pid, void* addr, void* data);
/* end ptrace.h */

unsigned long long HAP_perf_get_time_us(void);
unsigned long long HAP_perf_get_pcycles(void);

#ifdef __cplusplus
}
#endif

int HAP_cx_malloc(void* pctx, uint32 bytes, void** pptr)
{
  (void)pctx;

  *pptr = qurt_malloc(bytes);
  if (*pptr) {
    return 0;
  }
  return 1;
}

int HAP_cx_free(void* pctx, void* ptr)
{
  (void)pctx;

  qurt_free(ptr);
  return 0;
}

void HAP_debug(const char* msg, int level,
               const char* filename, int line)
{
  msg_const_type diag_msg;
  memset(&diag_msg, 0, sizeof(diag_msg));

  switch(level) {
    case HAP_LEVEL_LOW:
      diag_msg.desc.ss_mask = MSG_LEGACY_LOW;
      break;
    case HAP_LEVEL_MEDIUM:
      diag_msg.desc.ss_mask = MSG_LEGACY_MED;
      break;
    case HAP_LEVEL_HIGH:
      diag_msg.desc.ss_mask = MSG_LEGACY_HIGH;
      break;
    case HAP_LEVEL_FATAL:
      diag_msg.desc.ss_mask = MSG_LEGACY_FATAL;
      break;
    case HAP_LEVEL_ERROR:
    default:
      diag_msg.desc.ss_mask = MSG_LEGACY_ERROR;
      break;
  }

  if (diag_msg.desc.ss_mask & (MSG_BUILD_MASK_MSG_SSID_QDSP6)) {

    diag_msg.fmt = "HAP:%d:%s";
    {
      const char* str = strrchr(filename, '/');
      if (str == 0) {
        diag_msg.fname = filename;
      } else {
        diag_msg.fname = str + 1;
      }
    }
    diag_msg.desc.ss_id = MSG_SSID_QDSP6;
    diag_msg.desc.line = line;

    msg_sprintf((const msg_const_type*)&(diag_msg), (int)qurt_thread_get_id(), msg);
  }
}

uint64 HAP_perf_get_time_us(void)
{
  /* Converts ticks into microseconds
   1 tick = 1/19.2MHz seconds
   Micro Seconds = Ticks * 10ULL/192ULL */
  return (uint64)((qurt_sysclock_get_hw_ticks()) * 10ull / 192ull);
}

uint64 HAP_perf_get_pcycles(void)
{
  return (uint64)qurt_get_core_pcycles();
}

long HAP_debug_ptrace(int req, unsigned int pid, void* addr, void* data)
{
  return ptrace((__ptrace_request)req, pid, addr, data);
}

int HAP_utils_init(void)
{
  return 0;
}

void HAP_utils_deinit(void)
{
}

PL_DEFINE(HAP_utils, HAP_utils_init, HAP_utils_deinit);

