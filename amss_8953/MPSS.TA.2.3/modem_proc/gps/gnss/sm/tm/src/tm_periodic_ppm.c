/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=======*

            TM Periodic PPM Sub-module

General Description
  This file contains implementations for TM Periodic PPM Module

  Copyright (c) 2010 - 2012 Qualcomm Technologies Incorporated.
  Qualcomm Confidential and Proprietary. All Rights Reserved.
  Copyright (c) 2013 - 2014 Qualcomm Atheros, Inc.
  Qualcomm Atheros Confidential and Proprietary. All Rights Reserved. 
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=======*/

/*==============================================================================

                           Edit History
  $Header: //components/rel/gnss8.mpss/7.5.4/gnss/sm/tm/src/tm_periodic_ppm.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
==============================================================================*/
#include "gps_variation.h"
#include "comdef.h"
#include "customer.h"

#ifdef FEATURE_GNSS_PERIODIC_PPM
#error code not present
#endif /* FEATURE_GNSS_PERIODIC_PPM */
