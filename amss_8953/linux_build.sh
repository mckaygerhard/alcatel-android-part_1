#!/bin/bash
#===============================================================================
#
# Copyright (C) 2015 TCL Communication Technology Holdings Limited.
#
# build shell script file.
#
#     Author: Fan Yi
#     E-mail: yi.fan@tcl.com
#     Date  : 2015/07/21
#
#-------------------------------------------------------------------------------
#
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to the module.
# Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     -----------------------------------------------------------
# 07/21/2015 Fan Yi  Create modem build script
#===============================================================================

usage() {
echo "
usage : linux_build [-h] [-b] [-m] [-r] [-t] [-d] [-a] [--clean] <project name> <operator name> <build_id> <simlock> <secEfuse> <miniBuild>


 parameters :
 <project name>  : name of the project to build.
 <operator name> : name of the operator to build.
 <build_id>      : configure build id if need.

optional arguments:
  -h, --help        print this help and exits
  -b, --boot        build boot only
  -m, --modem       build modem only
  -r, --rpm         build rpm only
  -t, --trustzone   build trustzone only
  -d, --adsp        build adsp only
  -a, --all         build boot modem rpm
  --clean           clean selected build modules
"
    base="vendor/tct"
    if [ ! -d $base ]; then
        return
    fi
    echo -e "\033[31mYou can try below instead:\033[0m"
    for entry in `ls $base`
    do
        files=`ls $base/$entry`
            for file in $files
            do
                #just filter out the generated header,excluding "build" dir
                if (echo $file | grep -q ".h") ; then
                    a=`echo $file |cut -d'.' -f1| cut -d'_' -f2`
                    echo -e "\033[33m$0 $entry $a \033[0m"
                fi
            done
    done
}

getargs() {
    index=0
    boot=0
    modem=1
    rpm=0
    tz=0
    adsp=1
    append=
    non_version=K3C10000BU00
    simlock=false
    secEfuse=false
    miniBuild=false
    export MP_MACRO=false
    for parameter in $* ;do
        start=$(expr match "${parameter}" '-\|--')
        option=${parameter:$start}
        if [[ $start -gt 0 ]];then
            if [[ "$option" == "h" || "$option" == "help" ]];then
                usage && exit 0
            elif [[ "$option" == "b" || "$option" == "boot" ]];then
                boot=1
                modem=0
                rpm=0
                tz=0
                adsp=0
            elif [[ "$option" == "m" || "$option" == "modem" ]];then
                boot=0
                modem=1
                rpm=0
                tz=0
                adsp=1
            elif [[ "$option" == "r" || "$option" == "rpm" ]];then
                boot=0
                modem=0
                rpm=1
                tz=0
                adsp=0
            elif [[ "$option" == "t" || "$option" == "trustzone" ]];then
                boot=0
                modem=0
                rpm=0
                tz=1
                adsp=0
            elif [[ "$option" == "d" || "$option" == "adsp" ]];then
                boot=0
                modem=1
                rpm=0
                tz=0
                adsp=1
            elif [[ "$option" == "a" || "$option" == "all" ]];then
                boot=1
                modem=1
                rpm=1
                tz=1
                adsp=1
            elif [[ "$option" == "clean" ]];then
                append=-c
            else
                echo "unvalid option $parameter. try --help"
            fi
        elif [[ ${parameter:0:1} != '-' ]];then
            if [[ $index -eq 0 ]];then project=$parameter;fi
            if [[ $index -eq 1 ]];then operator=$parameter;fi
            if [[ $index -eq 2 ]];then non_version=$parameter;fi
            if [[ $index -eq 3 ]];then simlock=$parameter;fi
	    if [[ $index -eq 4 ]];then secEfuse=$parameter;fi
            if [[ $index -eq 5 ]];then miniBuild=$parameter;fi
            ((index++))
        else
            echo "!!unvalid parameter '$parameter' !!\n"
        fi
    done

    if [[ $index == 0 ]];then
        usage && exit 0
    fi
}

#===============================================================================
# fail msg
#===============================================================================
fail ()
{
    if [ ! -z "$@" ]
    then
        echo -e "\033[31mERROR: $@\033[0m" >&2
    fi
    echo -e "\033[31mERROR: failed.\033[0m" >&2
    usage
    exit 1
}

#===============================================================================
# set up Hexagon environment
#===============================================================================
export_hexagon64()
{
    version=6.4.06
    if [ -d "$project_dir/vendor/tct/buildtools/HEXAGON_Tools/" ]; then
        export HEXAGON_ROOT=$project_dir/vendor/tct/buildtools/HEXAGON_Tools/
        export HEXAGON_RTOS_RELEASE=${version}       
        export HEXAGON_Q6VERSION=v55
        export HEXAGON_IMAGE_ENTRY=0x86C00000
    elif [ -d "$project_dir/buildtools/HEXAGON_Tools/" ]; then
        export HEXAGON_ROOT=$project_dir/buildtools/HEXAGON_Tools/
        export HEXAGON_RTOS_RELEASE=${version}       
        export HEXAGON_Q6VERSION=v55
        export HEXAGON_IMAGE_ENTRY=0x86C00000
    else
        echo -e "\033[31mPlease clone vendor/tct/buildtools for HEXAGON_Tools ${version}\033[0m"
        exit
    fi
}

export_hexagon32()
{
    version=5.1.05
    if [ -d "$project_dir/vendor/tct/buildtools/HEXAGON_Tools/" ]; then
        export HEXAGON_ROOT=$project_dir/vendor/tct/buildtools/HEXAGON_Tools/
        export HEXAGON_RTOS_RELEASE=${version}
        export PATH=$PATH:$project_dir/vendor/tct/buildtools/HEXAGON_Tools/${version}/gnu/bin
    elif [ -d "$project_dir/buildtools/HEXAGON_Tools/" ]; then
        export HEXAGON_ROOT=$project_dir/buildtools/HEXAGON_Tools/
        export HEXAGON_RTOS_RELEASE=${version}
        export PATH=$PATH:$project_dir/buildtools/HEXAGON_Tools/${version}/gnu/bin
    else
        echo -e "\033[31mPlease clone vendor/tct/buildtools for HEXAGON_Tools ${version}\033[0m"
        exit
    fi
}

#===============================================================================
# set python environment
#===============================================================================
export_python()
{
    python_ver=2.7.6
    git_server=$(dirname `git remote -v | awk '{print $2}' | head -n 1 `)
    git_branch=$(git branch | cut -d ' ' -f2)
    if [ -d "$project_dir/vendor/tct/buildtools/python-${python_ver}/bin" ]; then
        export MAKE_PATH=$project_dir/vendor/tct/buildtools/python-${python_ver}/bin
    elif [ -d "$project_dir/buildtools/python-${python_ver}/bin" ]; then
        export MAKE_PATH=$project_dir/buildtools/python-${python_ver}/bin 
    else
        echo -e "\033[31mPlease clone vendor/tct/buildtools for python ${python_ver}\033[0m"
        echo -e "With below commands:"
        echo -e "\033[33mcd ..   #(put buildtools and amss codes under the same folder)\ngit clone $git_server/vendor/tct-source/buildtools -b ${git_branch}\033[0m"
        exit
    fi
    export PATH=$MAKE_PATH:$PATH
}

#===============================================================================
# set up LLVM environment
#===============================================================================
export_llvm35210()
{
    export LLVMROOT=$project_dir/vendor/tct/buildtools/llvm/3.5.2.1
    export LLVMBIN=$LLVMROOT/bin
    export LLVMLIB=$LLVMROOT/lib/clang/3.5.2/lib/linux
    export MUSLPATH=$LLVMROOT/tools/lib64
    export MUSL32PATH=$LLVMROOT/tools/lib32
    export LLVMINC=$MUSLPATH/include
    export LLVM32INC=$MUSL32PATH/include
    export LLVMTOOLPATH=$LLVMROOT/tools/bin
    export LLVMLINUX_TOOLS_PATH=$project_dir/vendor/tct/buildtools/llvm/3.5.2.1/bin
}

export_llvm35250()
{
    export LLVMROOT=$project_dir/vendor/tct/buildtools/llvm/3.5.2.5
    export LLVMBIN=$LLVMROOT/bin
    export LLVMLIB=$LLVMROOT/lib/clang/3.5.2/lib/linux
    export MUSLPATH=$LLVMROOT/tools/lib64
    export MUSL32PATH=$LLVMROOT/tools/lib32
    export LLVMINC=$MUSLPATH/include
    export LLVM32INC=$MUSL32PATH/include
    export LLVMTOOLPATH=$LLVMROOT/tools/bin
    export LLVMLINUX_TOOLS_PATH=$project_dir/vendor/tct/buildtools/llvm/3.5.2.5/bin
}

#===============================================================================
# set up LINARO environment
#===============================================================================
export_linaro()
{
    export LINAROGCCLINUX_TOOLS_PATH=$project_dir/vendor/tct/buildtools/linaro-toolchain/aarch64-none-elf/4.9-2014.07/bin
    export GNUROOT=$project_dir/vendor/tct/buildtools/linaro-toolchain/aarch64-none-elf/4.9-2014.07
    export GNUARM7=$project_dir/vendor/tct/buildtools/linaro-toolchain/gcc-linaro-arm-linux-gnueabihf/4.8-2014.02
}

#===============================================================================
# set up ARM600 environment
#===============================================================================
export_ARM600()
{
    export COMPILER=6.01bld48
    export COMPILER_VER=6.01
    export MAKE_PATH=/opt/bin:/usr/bin
    if [ -d "/opt/TCTNBTools/${COMPILER}/bin" ]; then
        export ARM_COMPILER_PATH=/opt/TCTNBTools/${COMPILER}/bin
        export ARMROOT=/opt/TCTNBTools/${COMPILER}
    elif [ -d "$project_dir/vendor/tct/buildtools/${COMPILER}" ]; then
        export ARM_COMPILER_PATH=$project_dir/vendor/tct/buildtools/${COMPILER}/bin
        export ARMROOT=$project_dir/vendor/tct/buildtools/${COMPILER}
    elif [ -d "$project_dir/buildtools/${COMPILER}" ]; then
        export ARM_COMPILER_PATH=$project_dir/buildtools/${COMPILER}/bin
        export ARMROOT=$project_dir/buildtools/${COMPILER}
    else
        echo -e "\033[31mPlease install ARM compiler tool ${COMPILER}\033[0m"
        exit 1
    fi
    export ARMTOOLS=ARMCT6
    export ARMLIB=$ARMROOT/lib
    export ARMINCLUDE=$ARMROOT/include
    export ARMINC=$ARMINCLUDE
    export ARMBIN=$ARMROOT/bin
    export ARMHOME=$ARMROOT
    export LLVMROOT=$ARMROOT
    export LLVMBIN=$ARMROOT/bin
    export PATH=$ARMBIN:$MAKE_PATH:$PATH
    export ARMLMD_LICENSE_FILE=8225@armls2
    export_python
    export_hexagon64
    export_llvm35250
    export_linaro
}

#===============================================================================
# set up ARM501 environment
#===============================================================================
export_ARM501()
{
    export MAKE_PATH=/opt/bin:/usr/bin
    export COMPILER=5.01bld94
    if [ -d "/opt/TCTNBTools/5.01bld94" ]; then
        export ARM_COMPILER_PATH=/opt/TCTNBTools/5.01bld94/bin64
        export ARMROOT=/opt/TCTNBTools/5.01bld94
    elif [ -d "$project_dir/vendor/tct/buildtools/${COMPILER}" ]; then
        export ARM_COMPILER_PATH=$project_dir/vendor/tct/buildtools/${COMPILER}/bin64
        export ARMROOT=$project_dir/vendor/tct/buildtools/${COMPILER}
    elif [ -d "$project_dir/buildtools/${COMPILER}" ]; then
        export ARM_COMPILER_PATH=$project_dir/buildtools/${COMPILER}/bin64
        export ARMROOT=$project_dir/buildtools/${COMPILER}
    else
        echo -e "\033[31mPlease install ARM compiler tool ${COMPILER}\033[0m"
        exit 1
    fi
    export ARMTOOLS=ARMCT5.01
    export ARMLIB=$ARMROOT/lib
    export ARMINCLUDE=$ARMROOT/include
    export ARMINC=$ARMINCLUDE
    export ARMBIN=$ARMROOT/bin64
    export ARMHOME=$ARMROOT
    export COMPILER=
    export COMPILER_VER=
    export PATH=$ARMBIN:$MAKE_PATH:$PATH
    export ARMLMD_LICENSE_FILE=8225@armls2
    export_python
    export_hexagon64
    export_llvm35210
    export_linaro
}

#===============================================================================
# set up RVCT41 environment
#===============================================================================
export_RVCT41()
{
    export MAKE_PATH=/opt/bin:/usr/bin
    ARM_COMPILER_PATH=/opt/TCTNBTools/4.1BLD713/RVCT/Programs/4.1/462/linux-pentium
    export ARMROOT=/opt/TCTNBTools/4.1BLD713
    export ARMTOOLS=RVCT41
    export ARMLIB=$ARMROOT/RVCT/Data/4.1/462/lib
    export ARMINCLUDE=$ARMROOT/RVCT/Data/4.1/462/include/unix
    export ARMINC=$ARMINCLUDE
    export ARMCONF=$ARMROOT/RVCT/Programs/4.1/462/linux-pentium
    export ARMDLL=$ARMROOT/RVCT/Programs/4.1/462/linux-pentium
    export ARMBIN=$ARMROOT/RVCT/Programs/4.1/462/linux-pentium
    export PATH=$ARM_COMPILER_PATH:$PATH
    export ARMHOME=$ARMROOT
    export ARMLMD_LICENSE_FILE=8225@armls1
    export_python
    export_hexagon64
}

#===============================================================================
# build boot image
#===============================================================================
build_boot()
{
    #echo $1
    chmod +x $1/BOOT.BF.3.3/boot_images/build/ms/build.sh
    export_ARM501
    cd $1/BOOT.BF.3.3/boot_images/build/ms
    ./build.sh --prod TARGET_FAMILY=$CPU_CHIP_TYPE $append
    local result=$?
    if [ $result -ne 0 ]; then
	echo "TCL error: boot img build fail"
        exit $result
    fi
    echo "" >> $1/BOOT.BF.3.3/build-log.txt
    cd $1
}

#===============================================================================
# build modem image
#===============================================================================
build_modem()
{
    export_ARM501
    cd $1/MPSS.TA.2.3/modem_proc/build/ms
    bash build.sh ${CPU_CHIP_TYPE}.genw3k.prod bparams=-k OEM_BUILD_VER=$non_version
    local result=$?
    if [ "$result" == "0" ]; then
        pushd $1/MSM8953.LA.2.0/common/build > /dev/null
        python build.py --nonhlos $non_version
        popd > /dev/null
    else
	echo "TCL error: modem img build fail"
        exit $result
    fi
    echo "" >> $1/MPSS.TA.2.3/build-log.txt
    cd $1
}

#===============================================================================
# build rpm image
#===============================================================================
build_rpm()
{
    export_ARM501
    cd $1/RPM.BF.2.4/rpm_proc/build
    chmod +x build_$CPU_CHIP_TYPE.sh
    #modify by gtguo for defect 1401273,add OEM_BUILD_VER=TCT
    ./build_$CPU_CHIP_TYPE.sh OEM_BUILD_VER=TCT $append
    local result=$?
    if [ $result -ne 0 ]; then
	echo "TCL error: rpm img build fail"
        exit $result
    fi
    echo "" >> $1/RPM.BF.2.4/build-log.txt
    cd $1
}

#===============================================================================
# build trustzone image
#===============================================================================
build_tz()
{
    export_ARM600
    if [ ! -z "$PYTHONPATH" ]; then
        echo -e "\033[31mfind PYTHONPATH variable, unset it\033[0m"
        unset PYTHONPATH
    fi
    cd $1/TZ.BF.4.0.5/trustzone_images/build/ms
    baseline=MSM8953_LA2.0
    dest_dir=TZ.BF.4.0.5/trustzone_images/build/ms/bin/SANAANAA/
    bash build.sh CHIPSET=msm${CPU_CHIP_TYPE} MAPREPORT=1 devcfg sampleapp goodixfp alipay tclapp
    local result=$?
    if [ $result -ne 0 ]; then
        echo "TCL error: tz img build fail"
        exit $result
    fi
    cd $1
}

#===============================================================================
# build adsp
#===============================================================================
build_adsp()
{
    export_hexagon32
    export_python
    cd $1/ADSP.8953.2.8.2/adsp_proc/build
    python ./build.py -c msm$CPU_CHIP_TYPE -o all
    local result=$?
    if [ $result -ne 0 ]; then
	echo "TCL error: adsp img build fail"
        exit $result
    fi
    cd $1
}

#===============================================================================
# efuse sign
#===============================================================================
efuse_sign()
{
    if [[ "$CPU_CHIP_TYPE" == "8976" && "$project" == "idol4s" && "$operator" == "vdf" ]]; then
        if [ -d "$project_dir/build/tools/sign_tool" ]; then
            cd $project_dir/build/tools/sign_tool/idol4s_vdf
            ./st.sh $project_dir idol4s_vdf $1
            if test $? -ne 0 ; then
              echo "ERROR: efuse sign amss error"
              echo "ERROR: device cannot poweron without modem efuse sign"
							exit 1
            fi
        else
            echo "ERROR: cannot find efuse sign tool"
            echo "ERROR: device cannot poweron without modem efuse sign"
            exit 1
        fi
        #pack NON-HLOS.bin image
        cd $build_dir/common/build
        python build.py --nonhlos $non_version
        cd $build_dir

        else 
	if [[ "$CPU_CHIP_TYPE" == "8976" && "$project" == "idol4s" ]]; then
             if [ -d "$project_dir/sign_tool" ]; then
                 cd $project_dir/sign_tool
                 ./st.sh $project_dir $project $1
                 if test $? -ne 0 ; then
                   echo "ERROR: efuse sign amss error"
                   echo "ERROR: device cannot poweron without modem efuse sign"
									 exit 1
                 fi
             else
                 echo "ERROR: cannot find efuse sign tool"
                 echo "ERROR: device cannot poweron without modem efuse sign"
                 exit 1
             fi
             #pack NON-HLOS.bin image
             cd $build_dir/common/build
             python build.py --nonhlos $non_version
             cd $build_dir
         fi
    fi
    if [[ "$CPU_CHIP_TYPE" == "8952" && "$project" == "idol4" && "$operator" == "global" ]] ; then
	if [ "$secEfuse" == "true" ] ; then
		cp -f $project_dir/build/tools/sign_tool/idol4/efuse/sec.dat $project_dir/amss_8976/common/sectools/resources/build/sec.dat
	fi 
        if [ -d "$project_dir/build/tools/sign_tool" ]; then
            cd $project_dir/build/tools/sign_tool/idol4
            ./st.sh $project_dir $project $1
            if test $? -ne 0 ; then
              echo "ERROR: efuse sign amss error"
              echo "ERROR: device cannot poweron without modem efuse sign"
							exit 1
            fi
        else
            echo "ERROR: cannot find efuse sign tool"
            echo "ERROR: device cannot poweron without modem efuse sign"
            exit 1
        fi
        #pack NON-HLOS.bin image
        cd $build_dir/common/build
        python build.py --nonhlos $non_version
        cd $build_dir
    fi
}

#===============================================================================
# remove tct.h
#===============================================================================
rm_tct_h()
{
    if [[ -e "$1/adsp_proc/adsp_proc/build/cust/tct.h" ]];then
        echo "remove old tct.h under adsp_proc/adsp_proc/build/cust"
        rm $1/ADSP.8953.2.8.2/adsp_proc/build/cust/tct.h
    fi

    if [[ -e "$1/boot_images/build/ms/tct.h" ]];then
        echo "remove old tct.h under boot_images/build/ms"
        rm $1/BOOT.BF.3.3/boot_images/build/ms/tct.h
    fi

    if [[ -e "$1/modem_proc/build/ms/tct.h" ]];then
        echo "remove old tct.h under modem_proc/build/ms"
        rm $1/MPSS.TA.2.3/modem_proc/build/ms/tct.h
    fi

    if [[ -e "$1/rpm_proc/build/ms/tct.h" ]];then
        echo "remove old tct.h under rpm_proc/build/ms"
        rm $1/RPM.BF.2.4/rpm_proc/build/ms/tct.h
    fi

    if [[ -e "$1/trustzone_images/build/cust/tct.h" ]];then
        echo "remove old tct.h under trustzone_images/build/cust/cust"
        rm $1/TZ.BF.4.0.5/trustzone_images/build/cust/tct.h
    fi
}

#===============================================================================
# link tct.h
#===============================================================================
link_tct_h()
{
    if [[ -e "$1/vendor/tct/$2/$2"_"$3.h" ]];then
        ln -s $1/vendor/tct/$2/$2\_$3.h $1/ADSP.8953.2.8.2/adsp_proc/build/cust/tct.h
        echo "vendor/tct/$2/$2"_"$3.h to adsp_proc/adsp_proc/build/cust/tct.h"
        ln -s $1/vendor/tct/$2/$2\_$3.h $1/BOOT.BF.3.3/boot_images/build/ms/tct.h
        echo "vendor/tct/$2/$2"_"$3.h to boot_images/build/ms/tct.h"
        ln -s $1/vendor/tct/$2/$2\_$3.h $1/MPSS.TA.2.3/modem_proc/build/ms/tct.h
        echo "vendor/tct/$2/$2"_"$3.h to modem_proc/build/ms/tct.h"
        ln -s $1/vendor/tct/$2/$2\_$3.h $1/RPM.BF.2.4/rpm_proc/build/ms/tct.h
        echo "vendor/tct/$2/$2"_"$3.h to rpm_proc/build/ms/tct.h"
        ln -s $1/vendor/tct/$2/$2\_$3.h $1/TZ.BF.4.0.5/trustzone_images/build/cust/tct.h
        echo "vendor/tct/$2/$2"_"$3.h to trustzone_images/build/cust/tct.h"
    else
        echo "# Unknow project name: $2 or operator name: $3 " >> build-log.txt
        fail "Unknow project name: \033[32m$2\033[31m or operator name: \033[32m$3\033[0m "
    fi
}

#===============================================================================
# set project partition
#===============================================================================
set_partition()
{
    if [[ -e "$1/vendor/tct/$2/build/partition_load_pt/partition_$3.xml" ]]; then
        echo "#----------------------------------------------------------------------"
        echo "# copy $1/vendor/tct/$2/build/partition_load_pt/partition_$3.xml to $1/common/config/partition.xml"
        cp -r $1/vendor/tct/$2/build/partition_load_pt/partition_$3.xml $1/MSM8953.LA.2.0/common/config/partition.xml
    else
        if [[ -e "$1/vendor/tct/$2/build/partition_load_pt/partition.xml" ]];then
            echo "#----------------------------------------------------------------------"
            echo "# copy $1/vendor/tct/$2/build/partition_load_pt/partition.xml to $1/common/config/partition.xml"
            cp -r $1/vendor/tct/$2/build/partition_load_pt/partition.xml $1/MSM8953.LA.2.0/common/config/partition.xml
        fi
    fi
}

#===============================================================================
# disable FEATURE_BOOT_RAMDUMPS_TO_SD_CARD
#===============================================================================
disable_dump_ram_to_sdcard()
{
echo "disable FEATURE_BOOT_RAMDUMPS_TO_SD_CARD for mp sw" >> build-log.txt
if (! grep -q "#define FEATURE_BOOT_RAMDUMPS_TO_SD_CARD" $build_dir/boot_images/build/ms/8976.target.builds); then
    sed -i "s/define FEATURE_BOOT_RAMDUMPS_TO_SD_CARD/#define FEATURE_BOOT_RAMDUMPS_TO_SD_CARD/g" $build_dir/boot_images/build/ms/8976.target.builds
fi

if (! grep -q "#define FEATURE_BOOT_RAMDUMPS_TO_SD_CARD" $build_dir/boot_images/build/ms/8952.target.builds); then
    sed -i "s/define FEATURE_BOOT_RAMDUMPS_TO_SD_CARD/#define FEATURE_BOOT_RAMDUMPS_TO_SD_CARD/g" $build_dir/boot_images/build/ms/8952.target.builds
fi
}

#===============================================================================
# main
#===============================================================================
getargs $*


setenv=`export`
starttime="$(date +%s)"
starttimefmt=`date --date='@'$starttime`

#===============================================================================
# Setup Paths
#===============================================================================
build_dir=$(dirname $0)
build_dir=$(readlink -e $build_dir)
project_dir=$(dirname $build_dir)

export BOOT_ROOT=$build_dir/MPSS.TA.2.3/modem_proc/build/ms
VENDOR_ROOT=$build_dir/vendor

#===============================================================================
# Set target enviroment
#===============================================================================

echo "Start Time = $starttimefmt" > build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# ENVIRONMENT BEGIN" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
export >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# ENVIRONMENT END" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# H HEADER FILE GENERATE" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
chmod +x $VENDOR_ROOT/tools/TCTHeaderGen.py
$VENDOR_ROOT/tools/TCTHeaderGen.py $VENDOR_ROOT/Macro_Desc.csv $VENDOR_ROOT/tct 2>&1 | tee -a build-log.txt
echo "" >> build-log.txt

#clean link files
rm_tct_h $build_dir 2>&1 | tee -a build-log.txt
#here link project_operator.h to tct.h
echo "# Now linking $project"_"$operator.h to tct.h" >> build-log.txt
#link_tct_h $build_dir $project $operator 2>&1 | tee -a build-log.txt
link_tct_h $build_dir $project $operator

#check cpu chip type
CPU_CHIP_TYPE=$(cat "$build_dir/vendor/tct/$project/$project"_"$operator.h" | grep PROJECT_TCTNB_CPU_CHIP_TYPE | awk '{print $3}')
export PROJECT_TCT_RF_CARD=$(cat "$build_dir/vendor/tct/$project/$project"_"$operator.h" | grep PROJECT_TCT_RF_CARD | awk '{print $3}')
export TCT_AVS_TYPE=$(cat "$build_dir/vendor/tct/$project/$project"_"$operator.h" | grep PROJECT_TCT_AVS | awk '{print $3}')

#set partition
set_partition $build_dir $project $operator

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# BUILD BEGIN" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
if [[ $boot -eq 1 ]];then
    echo "# Build boot image" >> build-log.txt
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    build_boot $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build boot error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build boot image" >> build-log.txt
fi

if [[ $rpm -eq 1 ]];then
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    echo "# Build rpm image" >> build-log.txt
    build_rpm $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build rpm error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build rpm image" >> build-log.txt
fi

if [[ $tz -eq 1 ]];then
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    echo "# Build tz image" >> build-log.txt
    build_tz $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build tz error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build trustzone image" >> build-log.txt
fi

if [[ $adsp -eq 1 ]];then
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    echo "# Build adsp image" >> build-log.txt
    build_adsp $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build adsp error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build adsp image" >> build-log.txt
fi

if [[ $modem -eq 1 ]];then
    echo "#-------------------------------------------------------------------------------" >> build-log.txt
    echo "# Build modem image" >> build-log.txt
    build_modem $build_dir 2>&1 | tee -a build-log.txt
    result=$(grep "** Build errors..." -rn build-log.txt)
    if [ "$result" != "" ]; then
        echo "Build modem error! exit!"
        exit 1
    fi
else
    echo "###IGNORE build modem image" >> build-log.txt
fi
#efuse_sign amss

echo "#-------------------------------------------------------------------------------" >> build-log.txt
echo "# BUILD END" >> build-log.txt
echo "#-------------------------------------------------------------------------------" >> build-log.txt
endtime="$(date +%s)"
endtimefmt=`date --date='@'$endtime`
elapsedtime=$(expr $endtime - $starttime)
echo
echo "Start Time = $starttimefmt - End Time = $endtimefmt" >> build-log.txt
echo "Elapsed Time = $elapsedtime seconds" >> build-log.txt

echo "Start Time = $starttimefmt - End Time = $endtimefmt"
echo "Elapsed Time = $elapsedtime seconds"

# Copy image files to common/build
$build_dir/vendor/script/modembin_copy.sh $CPU_CHIP_TYPE
