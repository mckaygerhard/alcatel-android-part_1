/*============================================================================
  @file sns_sam_prov.c

  @brief
  This here is the only file that may be changes while adding new SAM
  Sensor Providers into the SAM Framework.

  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
  ===========================================================================*/

/*===========================================================================
  Include Files
  ===========================================================================*/
#include "sns_sam_prov_api.h"
#include "sns_sam_prov.h"
#include "sns_sam_client.h"
#include "sns_sam.h"
#include "sns_sam_dep.h"
#include "sns_sam_resp.h"
#include "sns_sam_init.h"
#include "sns_sam_error.h"

/*============================================================================
  Provider Registration Function Declarations
  ===========================================================================*/

sns_sam_err sns_sam_prov_loc_register(
      sns_sam_prov_api **provAPI );

/*============================================================================
  Type Definitions
  ===========================================================================*/

struct sns_sam_prov
{
  /* The registration function for this Provider */
  sns_sam_prov_register reg;
  /* Provider API as retrieved from reg */
  sns_sam_prov_api *provAPI;
  /* Service object associated with this provider */
  qmi_idl_service_object_type serviceObj;
  /* Sensor Req object allocated for initilization */
  sns_sam_sensor sensor;
};
typedef struct sns_sam_prov sns_sam_prov;

/*============================================================================
  Global Data
  ===========================================================================*/

/* Array of all known Providers */
STATIC sns_sam_prov samProviders[] =
{
  { sns_sam_prov_loc_register, NULL, NULL }
};

const uint32_t samProvidersSize =
  sizeof(samProviders) / sizeof(samProviders[0]);

/* Callback functions available for all Providers.  This object
 * is shared by all, and the value of sensorReq will be reset before each
 * Provider API function call passing it as an argument. */
sns_sam_prov_callback provCB SNS_SAM_UIMAGE_DATA;

/*============================================================================
  Static Functions
  ===========================================================================*/

/* See sns_sam_prov_cb_send_req. */
STATIC sns_sam_err
sns_sam_prov_cb_send_req( sns_sam_sensor_req const *sensorReq,
    sns_sam_enable_req *enableReqMsg )
{
  return sns_sam_client_send( sensorReq, enableReqMsg );
}

/* See sns_sam_prov_add_sensor */
STATIC sns_sam_sensor*
sns_sam_prov_get_sensor( sns_sam_sensor_uid sensorUID )
{
  sns_sam_sensor *sensor = sns_sam_lookup_sensor_from_suid( &sensorUID );
  sns_sam_err err;

  SNS_PRINTF_STRING_MEDIUM_1( samModule, "Adding new sensor %x", sensorUID );

  if( NULL == sensor )
  {
    err = sns_sam_init_sensor( &sensor );
    if( SAM_ENONE != err )
    {
      return NULL;
    }
    else
    {
      return sensor;
    }
  }
  else
  {
    SNS_PRINTF_STRING_MEDIUM_1( samModule, "Added sensor already exists %x",
        sensorUID );
    return sensor;
  }
}

/* See sns_sam_prov_sensor_available. */
STATIC sns_sam_err
sns_sam_prov_sensor_available( sns_sam_sensor const *sensor )
{
  sns_sam_dep_available( sensor );
  return SAM_ENONE;
}

/* See sns_sam_prov_remove_sensor. */
STATIC SNS_SAM_UIMAGE_CODE sns_sam_err
sns_sam_prov_remove_sensor( sns_sam_sensor_uid sensorUID )
{
  // PEND: To be implemented later
  // Remove active algorithm instances
  // Remove sensor from sensorQ and unregister from SMR
  // Call smr_client_init for the applicable service object

  return SAM_ENONE;
}

/* See sns_sam_prov_stop_stream. */
STATIC SNS_SAM_UIMAGE_CODE sns_sam_err
sns_sam_prov_stop_stream( sns_sam_sensor_req const *sensorReq )
{
  SNS_PRINTF_STRING_ERROR_1( samModule, "sns_sam_prov_stop_stream %x",
      *sensorReq->sensor->sensorUID );
  sns_sam_uimage_exit();
  sns_sam_remove_algo_instances( (sns_sam_sensor_req*)sensorReq, SENSOR1_EFAILED );

  return SAM_ENONE;
}

/*============================================================================
  Public Functions
  ===========================================================================*/

bool
sns_sam_prov_init_client( sns_sam_client_init_data *initData )
{
  uint32_t i;
  bool found = false;
  uint32_t serviceID;

  qmi_idl_get_service_id( initData->serviceObj, &serviceID );
  for( i = 0; i < samProvidersSize; i++ )
  {
    if( serviceID == samProviders[ i ].sensor.sensorReq.serviceID )
    {
      found = true;
      if( initData->timeoutExpired )
      {
        SNS_PRINTF_STRING_ERROR_1( samModule,
            "Timeout expired for %i", serviceID );
      }
      else if( NULL != samProviders[ i ].provAPI->sns_sam_prov_sensor_list )
      {
        sns_sam_err err =
          sns_sam_client_init( &samProviders[ i ].sensor.sensorReq, 0, false );
        if( SAM_ENONE != err )
        {
          SNS_PRINTF_STRING_ERROR_2( samModule,
            "Error %i initializing PROV connection %i", err, serviceID );
          // PEND: Do we have any cleanup to perform here?
        }
        else
        {
          provCB.sensorReq = &samProviders[ i ].sensor.sensorReq;
          samProviders[ i ].provAPI->sns_sam_prov_sensor_list( &provCB );
        }
      }
    }
  }

  return found;
}

sns_sam_err
sns_sam_prov_init_fw( void )
{
  uint32_t i;
  sns_sam_err err;

  provCB.structSize = sizeof(sns_sam_prov_callback);
  provCB.sensorReq = NULL;

  provCB.sns_sam_prov_cb_send_req = &sns_sam_prov_cb_send_req;
  provCB.sns_sam_prov_get_sensor = &sns_sam_prov_get_sensor;
  provCB.sns_sam_prov_remove_sensor = &sns_sam_prov_remove_sensor;
  provCB.sns_sam_prov_stop_stream = &sns_sam_prov_stop_stream;
  provCB.sns_sam_prov_sensor_available = &sns_sam_prov_sensor_available;

  for( i = 0; i < samProvidersSize; i++ )
  {
    err = samProviders[ i ].reg( &samProviders[ i ].provAPI );

    if( SAM_ENONE == err )
    {
      int32_t errQMI = QMI_IDL_LIB_NO_ERR;

      provCB.sensorReq = NULL;
      samProviders[ i ].serviceObj =
        samProviders[ i ].provAPI->sns_sam_prov_service( &provCB );

      samProviders[ i ].sensor.serviceObj = samProviders[ i ].serviceObj;
      samProviders[ i ].sensor.sensorReq.sensor = &samProviders[ i ].sensor;

      errQMI = qmi_idl_get_service_id( samProviders[ i ].serviceObj,
          &samProviders[ i ].sensor.sensorReq.serviceID );
      if( QMI_IDL_LIB_NO_ERR != errQMI )
      {
        SNS_PRINTF_STRING_ERROR_1( samModule,
            "QMI Service ID Lookup error %i", errQMI );
      }
      else
      {
        err = sns_sam_client_check( samProviders[ i ].serviceObj,
            SMR_CLIENT_INSTANCE_ANY, SNS_SAM_INIT_CLIENT_TIMEOUT_US );
        if( SAM_ENONE != err )
        {
          SNS_PRINTF_STRING_ERROR_1( samModule, "SAM client check failed %i", err );
          return SAM_EFAILED;
        }
      }
    }
    else
    {
      SNS_PRINTF_STRING_ERROR_1( samModule, "Error registering provider %i", err );
    }
  }

  return SAM_ENONE;
}
