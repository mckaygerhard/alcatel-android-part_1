#ifndef SNS_SAM_RESP_H
#define SNS_SAM_RESP_H

/*============================================================================
  @file sns_sam_resp.h

  @brief
  Handles processing of all incoming response messages to the SAM Framework.

  Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
  ===========================================================================*/

/*===========================================================================
  Include Files
  ===========================================================================*/
#include <stdint.h>
#include "qmi_idl_lib.h"
#include "fixed_point.h"
#include "sns_queue.h"
#include "sns_sam.h"
#include "sns_usmr.h"

/*============================================================================
  Function Declarations
  ===========================================================================*/

/**
 * Handle an incoming response message to the SAM framework.  The request
 * message associated with respMsg will be freed unless it is associated
 * with an active sensor request.
 *
 * @param[i] respMsg Message to be processed
 */
void sns_sam_handle_resp( sns_sam_resp *respMsg );

/**
 * Form and send all batch response messages for the given client request.
 * One response will be sent for each flush request in the client's queue.
 *
 * @param[io] clientReq Process and clear its flushReqQ
 */
void sns_sam_send_batch_resp( sam_client_req *clientReq );

#endif /* SNS_SAM_RESP_H */
