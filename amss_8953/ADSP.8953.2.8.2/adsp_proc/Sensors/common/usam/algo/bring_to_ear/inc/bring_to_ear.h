#ifndef BRING_TO_EAR_H
#define BRING_TO_EAR_H

/*=============================================================================
  @file bring_to_ear.h

  Bring to ear gesture recognition header file 

  Copyright (c) 2011, 2015-2016 Qualcomm Technologies Inc.  All Rights Reserved.
  Qualcomm Technologies Inc. Proprietary and Confidential.
=============================================================================*/

/*===========================================================================
  Include Files
  ===========================================================================*/

#include "axis_direction.h"
#include "rel_rest.h"
#include "comdef.h"
#include "sns_sam_algo_api.h"
#include "sns_sam_bte_v01.h"

#define BRING_TO_EAR_SAMPLE_RATE_DEF_HZ         (30)
#define BRING_TO_EAR_SAMPLE_RATE_DEF_Q16        (FX_CONV_Q16(30, 0))
#define BRING_TO_EAR_HORIZ_ANG                  (FX_FLTTOFIX_Q16(20.0*PI/180.0)) // radians, Q16
#define BRING_TO_EAR_VERT_ANG                   (FX_FLTTOFIX_Q16(20.0*PI/180.0)) // radians, Q16
#define BRING_TO_EAR_FACING_ANGLE_THRESH        (FX_FLTTOFIX_Q16(65.0*PI/180.0)) // radians, Q16
#define BRING_TO_EAR_DATA_COLS                  (3)

#define BRING_TO_EAR_DEF_INT_CFG_PARAM1         (FX_FLTTOFIX_Q16(0.3*G))
#define BRING_TO_EAR_DEF_INT_CFG_PARAM2         (FX_FLTTOFIX_Q16(0.2))
#define BRING_TO_EAR_DEF_INT_CFG_PARAM3         (FX_FLTTOFIX_Q16(15.0*PI/180.0))
#define BRING_TO_EAR_DEF_INT_CFG_PARAM4         (FX_FLTTOFIX_Q16(0.2))

#define BRING_TO_EAR_NEAR                       (1)
#define BRING_TO_EAR_FAR                        (0)

enum BRING_TO_EAR_EVENTS
{
  BRING_TO_EAR_EVENT = 1
};

typedef struct
{
  int32_t sample_rate;                // sample rate in Hz, Q16
  int32_t facing_angle_threshold;     // radians, Q16
  int32_t horiz_angle_threshold;      // radians, Q16
  int32_t vert_angle_threshold;       // radians, Q16
  int32_t prox_enabled;               // boolean, 1=TRUE, 0=FALSE
  int32_t internal_config_param1;
  int32_t internal_config_param2;
  int32_t internal_config_param3;
  int32_t internal_config_param4;
} bte_config_s;

typedef struct
{
  int32_t a[BRING_TO_EAR_DATA_COLS]; // accel, m/s/s, Q16
  int32_t proximity;
  int32_t unused[2];   // to give enough memory to copy sensor data item  
} bte_input_s;

typedef struct
{
  int32_t bring_to_ear_event;   // event type
  int32_t event;                // boolean, true if new event detected
} bte_output_s;

typedef struct
{
  bte_config_s config;  
  int32_t bte_state;
  axis_state_s *axis_algo;
  rel_rest_state_s *rel_rest_algo;
  int32_t horiz_angle_threshold;
  int32_t vert_angle_threshold;
  int32_t prox_enabled;
} bte_state_s;


/* BTE persistant data */
typedef struct {
  bool regDataRecv; /* If the registry data has been received*/
  uint64_t power; /* Consumption for the sensor as reported by SMGR */
  q16_t defSampleRate; /* Default sampling rate if none specified */
  q16_t minSampleRate; /* Minimum sampling rate */
  q16_t maxSampleRate; /* Maximum sampling rate */
  sns_sam_sensor_uid const *accelSUID; /* SUID saved in dep_sensors_met() */
  int32_t facing_angle_threshold;     // radians, Q16
  int32_t horiz_angle_threshold;      // radians, Q16
  int32_t vert_angle_threshold;       // radians, Q16
  int32_t prox_enabled;               // boolean, 1=TRUE, 0=FALSE
  int32_t internal_config_param1;
  int32_t internal_config_param2;
  int32_t internal_config_param3;
  int32_t internal_config_param4;
} bte_persist_s;


#endif /* BRING_TO_EAR_H */

