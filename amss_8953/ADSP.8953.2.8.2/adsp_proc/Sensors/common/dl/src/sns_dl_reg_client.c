/*==============================================================================
  @file sns_dl_reg_client.c

  @brief
  Implementation of the SNS registry communication required for the reading
  of the list of boot critical shared object based SNS DDF based drivers
  and SAM based SNS algorithms from the registry. 

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/

#include <stringl.h>
#include <string.h>
#include <stdbool.h>

#include "sns_reg_api_v02.h"
#include "sns_dl_priv.h"

//------------------------------------------------------------------------------
// DATA DEFINITIONS
//------------------------------------------------------------------------------

/* Used for outgoing request messages to the SAM Framework */
struct sns_dl_reg_msg_s
{
  /* request message id required by the SNS registry service */
  unsigned int  msg_id;
  /* request message payload */
  void          *buffer;
  /* request message payload size */
  uint32_t      buffer_sz;  
};
typedef struct sns_dl_reg_msg_s sns_dl_reg_msg_t;

/* For incoming response messages to the SAM Framework */
struct sns_dl_reg_resp_msg_s
{
  /* The actual request message and its meta data (details about the msg) */
  sns_dl_reg_msg_t msg;
  /* pointer to req buffer */
  void *p_req_buffer;
  /* the command associated with this response message */
  sns_dl_reg_cmd_t *p_reg_cmd;
  /* free function pointer required for clean up */
  pfn_sns_dl_free pfn_free;
};
typedef struct sns_dl_reg_resp_msg_s sns_dl_reg_resp_msg_t;

//------------------------------------------------------------------------------
// DATA DECLARATIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// FORWARD DECLARATIONS
//------------------------------------------------------------------------------
extern void sns_dl_handle_reg_cmd_resp
(
  sns_dl_reg_cmd_t *p_reg_cmd,
  uint32_t  group_id,
  void      *data,
  uint32_t  data_size,
  bool      is_err,
  void      *ptr_to_free,
  bool      *user_callback_to_free_mem
);

//------------------------------------------------------------------------------
// IMPLEMENTATION 
//------------------------------------------------------------------------------
/**
  @brief  Callback API called by the USMR FWK whenever an indication is received

  @param[in] reg_client_handle  -- USMR client handle for which we have an error
  @param[in] msg_id             -- Msg id for which the indication was received
  @param[in] ind_buffer         -- Buffer containing the indication payload
  @param[in] ind_buffer_len     -- Len of buffer containing the payload
  @param[in] ind_cb_data        -- Data passed in when the indication callback
                                   is issued

  @return
    None
*/
STATIC
void sns_dl_reg_client_ind_cb
(
  smr_client_hndl reg_client_handle,
  unsigned int msg_id,
  void *ind_buffer,
  unsigned int ind_buffer_len,
  void *ind_cb_data
)
{
  SNS_DL_PRINTF(ERROR,
                  "Unexpected Indication Seen For MSGID: 0x%x Len:%u SVC:0x%p",
                  msg_id, ind_buffer_len, (sns_dl_svc_t*)ind_cb_data);
}

/**
  @brief  Callback API called by the USMR FWK whenever a remote service has an
          error it has to report to its clients

  @param[in] reg_client_handle  -- USMR client handle for which we have an error
  @param[in] error              -- Error indication from the USMR FWK
  @param[in] err_cb_data        -- Data passed in when the error callback is
                                   issued

  @return
    None
*/
STATIC
void sns_dl_reg_client_err_cb
(
  smr_client_hndl reg_client_handle,
  smr_err error,
  void *err_cb_data
)
{
  sns_err_code_e err_code = SNS_ERR_FAILED;
  sns_dl_svc_t *p_svc = (sns_dl_svc_t*)err_cb_data;
  SNS_DL_PRINTF(FATAL, "Client Error 0x%x Seen For SVC:0x%p", error, p_svc);
  sns_dl_svc_handle_error(p_svc, err_code);
}

/**
  @brief  Callback API called by the USMR FWK whenever a client is destroyed

  @param[in]  release_cb_data --  Data passed in when the destroy callback is
                                  issued
  @return
    None
*/
STATIC
void sns_dl_reg_client_release_cb(void *release_cb_data)
{
  SNS_DL_PRINTF(LOW, "sns_dl_reg_client_release_cb SVC:0x%p",
                      (sns_dl_svc_t*)release_cb_data);
}

/**
  @brief  API to create a client handle to the SNS registry using USMR for the
          supplied DL SVC obj

  @param[in]  p_svc       --  Pointer to the SVC object
  @param[in]  timeout_us  --  Time out to block before returning back to the
                              caller. If the timeout is 0, the caller is
                              blocked until the registry search operation
                              completes.

  @return
    SNS_SUCCESS         - Call successful
    SNS_ERR_FAILED      - General failure
*/
sns_err_code_e sns_dl_reg_client_create(sns_dl_svc_t *p_svc, uint32_t timeout)
{
  smr_err err;
  sns_err_code_e ret_val = SNS_SUCCESS;

  if (!p_svc->reg_client_handle_valid) {
    err = smr_client_init(GET_DL_REG_REF(p_svc)->reg_svc_obj,
                            SMR_CLIENT_INSTANCE_ANY,
                            sns_dl_reg_client_ind_cb, (void*)p_svc,
                            timeout, sns_dl_reg_client_err_cb, (void*)p_svc,
                            &(p_svc->reg_client_handle));

    if (SMR_NO_ERR != err) {
      SNS_DL_PRINTF(ERROR, "smr_client_init error 0x%x", err);
      ret_val = SNS_ERR_FAILED;
    } else {
      p_svc->reg_client_handle_valid = true;
    }
  }
  return ret_val;
}

/**
  @brief  API to destroy a client handle to the SNS registry using USMR for the
          supplied DL SVC obj

  @param[in]  p_svc       --  Pointer to the SVC object

  @return
    SNS_SUCCESS         - Call successful
    SNS_ERR_FAILED      - General failure
*/
sns_err_code_e sns_dl_reg_client_destroy(sns_dl_svc_t *p_svc)
{
  smr_err err;
  sns_err_code_e ret_val = SNS_SUCCESS;

  if (p_svc->reg_client_handle_valid) {
    p_svc->reg_client_handle_valid = false;
    err = smr_client_release(p_svc->reg_client_handle,
                              sns_dl_reg_client_release_cb,
                              (void*)p_svc);
    if (SMR_NO_ERR != err) {
      SNS_DL_PRINTF(ERROR, "sns_sl_reg_client_destroy error 0x%x", err);
      ret_val = SNS_ERR_FAILED;
    }
  }
  return ret_val;
}

/**
  @brief  API to handle responses to the from the SNS registry service.

  @param[in] client_handle  -- USMR client handle for which we have a resp
  @param[in] msg_id         -- Msg ID corresponding to the response
  @param[in] resp_c_struct  -- Actual response message payload
  @param[in] resp_c_struct_len -- Size of response message payload
  @param[in] resp_cb_data   -- Callback data passed in when the request was sent
  @param[in] transp_err     -- USMR FWK indication if there was any error during
                               communication
  @return
    None
*/
STATIC
void sns_dl_reg_client_resp_cb(smr_client_hndl client_handle,
                                unsigned int msg_id,
                                void *resp_c_struct,
                                unsigned int resp_c_struct_len,
                                void *resp_cb_data,
                                smr_err transp_err)
{
  sns_reg_group_read_resp_msg_v02 *p_read_resp = NULL;
  sns_reg_group_search_resp_msg_v02 *p_srch_resp = NULL;
  sns_dl_reg_cmd_t *p_reg_cmd = NULL;
  void *data = NULL;
  sns_common_resp_s_v01 resp_err;
  uint32_t data_size = 0, group_id = 0; //this is the default value
  bool user_callback_to_free_mem = false, is_err = true;
  sns_dl_reg_resp_msg_t *p_dl_resp_msg = (sns_dl_reg_resp_msg_t*)resp_cb_data;
  pfn_sns_dl_free pfn_free = NULL;

  if (NULL == p_dl_resp_msg) {
    SNS_DL_PRINTF(FATAL,
                  "sns_dl_reg_client_resp_cb NULL Resp CallBack For MSG ID:%d",
                  msg_id);
    goto bail;
  }
  pfn_free = p_dl_resp_msg->pfn_free;
  /* if the free pointer is NULL, it implies that the callback parameter somehow
     has been corrupted, something is terribly wrong here */
  SNS_ASSERT(NULL != pfn_free);
  
  p_reg_cmd = p_dl_resp_msg->p_reg_cmd;
  /* if the reg_cmd ptr is NULL, it implies that the callback parameter somehow
     has been corrupted, something is terribly wrong here */
  SNS_ASSERT(NULL != p_dl_resp_msg->p_reg_cmd);

  /* check the actual response message payload */
  if (NULL == resp_c_struct) {
    SNS_DL_PRINTF(ERROR,
                  "NULL Resp Struct resp_c_struct For MSG ID:%d", msg_id);
    goto bail;
  }  
  /* check if there were any transport errors */
  if (SMR_NO_ERR != transp_err) {
    SNS_DL_PRINTF(HIGH,
                  "Transport Error 0x%x For MSG ID:%d", transp_err, msg_id);
    goto bail;
  }

  /*
    we have a valid message and callback param, now do the work of obtaining
    the payload and dispatching it to the callback function in the reg cmd
  */
  switch (msg_id) {
    case SNS_REG_GROUP_READ_RESP_V02:
      p_read_resp = (sns_reg_group_read_resp_msg_v02*)resp_c_struct;
      memcpy(&resp_err, &(p_read_resp->resp), sizeof(sns_common_resp_s_v01));
      group_id  = p_read_resp->group_id;
      data      = p_read_resp->data;
      data_size = p_read_resp->data_len;
      break;
    case SNS_REG_GROUP_SEARCH_RESP_V02:
      p_srch_resp = (sns_reg_group_search_resp_msg_v02*)resp_c_struct;
      memcpy(&resp_err, &(p_srch_resp->resp), sizeof(sns_common_resp_s_v01));
      if ((p_srch_resp->group_id_valid) && (p_srch_resp->data_valid)) {
        group_id  = p_srch_resp->group_id;
        data      = p_srch_resp->data;
        data_size = p_srch_resp->data_len;
      }
      break;
    default:
      SNS_DL_PRINTF(ERROR, "Unsupported MSG ID:%d", msg_id);
      goto bail;
  };
  SNS_DL_PRINTF(LOW, "reg_client_resp_cb MSGID%d GroupID:%d", msg_id, group_id);

  if (SNS_RESULT_SUCCESS_V01 != resp_err.sns_result_t) {
    SNS_DL_PRINTF(ERROR, "Registry Error:0x%x MSGID:%d Group:%d",
                          resp_err.sns_err_t, msg_id, group_id);
  } else {
    is_err = false;
  }
bail:
  /*
    If we have valid data, callback param and resp payload dispatch it to
    the cmd. We dispatch even if we have an error just incase the callback
    uses it for book-keeping purposes etc. The cmd callback API indicates
    whether it handled free'ing of the resp payload in variable 
    user_callback_to_free_mem 
  */
  if ((p_reg_cmd) && (resp_c_struct))
    sns_dl_handle_reg_cmd_resp(p_reg_cmd, group_id, data, data_size, is_err,
                                resp_c_struct, &user_callback_to_free_mem);
  if ((p_dl_resp_msg) && (pfn_free)) {
    /* did the user free the resp paylod, of not we free it */
    if (!user_callback_to_free_mem) {
      FREEIFPFN(pfn_free, p_dl_resp_msg->msg.buffer);
    }
    FREEIFPFN(pfn_free, p_dl_resp_msg->p_req_buffer);
    FREEIFPFN(pfn_free, p_dl_resp_msg);
  }
}

/**
  @brief  API to send a message across to the SNS registry service depending
          on the operation specified in the command
          
  @param[in] p_reg_cmd -- Pointer to the registry command
  
  @return
    SNS_SUCCESS         - Call successful
    SNS_ERR_BAD_PARM    - Bad parameter was passed in
    SNS_ERR_NOMEM       - Out of memory error
    SNS_ERR_FAILED      - General failure
*/
sns_err_code_e
sns_dl_reg_client_perform_ops
(
  sns_dl_reg_cmd_t *p_reg_cmd
)
{
  sns_dl_reg_msg_t      dl_req_msg;
  sns_dl_reg_resp_msg_t *p_dl_resp_msg = NULL;
  sns_reg_group_read_req_msg_v02 *p_read_req_msg;
  sns_reg_group_search_req_msg_v02 *p_srch_req_msg;
  smr_txn_handle txn_handle = NULL;
  smr_err err;
  int32_t qmi_err;
  uint32_t resp_msg_size = 0;
  void *ptr = NULL;
  sns_err_code_e ret_val;
  unsigned int req_msg_id, resp_msg_id;
  sns_dl_svc_t *p_svc;

  /* init the request msg data before any error checking etc. */
  dl_req_msg.buffer = NULL;
  dl_req_msg.buffer_sz = 0;

  p_svc = p_reg_cmd->p_svc;
  /* fill in the msg_ids depending on the cmd operation requested */
  switch (p_reg_cmd->cmd_type) {
    case DL_REG_CMD_ITERATE:
      req_msg_id = SNS_REG_GROUP_READ_REQ_V02;
      resp_msg_id = SNS_REG_GROUP_READ_RESP_V02;
      break;
    case DL_REG_CMD_SEARCH:
      req_msg_id = SNS_REG_GROUP_SEARCH_REQ_V02;
      resp_msg_id = SNS_REG_GROUP_SEARCH_RESP_V02;
      break;
    default:
      ret_val = SNS_ERR_BAD_PARM;
      goto bail;
  };

  /* obtain details of the request message size for the req_msg_id */
  qmi_err = qmi_idl_get_message_c_struct_len(GET_DL_REG_REF(p_svc)->reg_svc_obj,
                                              QMI_IDL_REQUEST,
                                              req_msg_id,
                                              &(dl_req_msg.buffer_sz));
  if (QMI_IDL_LIB_NO_ERR != qmi_err) {
    ret_val = SNS_ERR_FAILED;
    goto bail;
  }
  /* obtain details of the response message size for the resp_msg_id */
  qmi_err = qmi_idl_get_message_c_struct_len(GET_DL_REG_REF(p_svc)->reg_svc_obj,
                                              QMI_IDL_RESPONSE,
                                              resp_msg_id,
                                              &resp_msg_size);
  if (QMI_IDL_LIB_NO_ERR != qmi_err) {
    ret_val = SNS_ERR_FAILED;
    goto bail;
  }
  /* allocate the request msg buffer which will contain the actual payload */
  dl_req_msg.buffer = p_svc->pfn_malloc(dl_req_msg.buffer_sz);
  if (NULL == dl_req_msg.buffer) {
    ret_val = SNS_ERR_NOMEM;
    goto bail;  
  }
  /* allocate response msg struct which will contain details of the response */
  p_dl_resp_msg = (sns_dl_reg_resp_msg_t*)
                    p_svc->pfn_malloc(sizeof(sns_dl_reg_resp_msg_t));
  if (NULL == p_dl_resp_msg) {
    ret_val = SNS_ERR_NOMEM;
    goto bail;
  }
  /* allocate the response msg buffer which will contain the actual payload */
  ptr = p_svc->pfn_malloc(resp_msg_size);
  if (NULL == ptr) {
    ret_val = SNS_ERR_NOMEM;
    goto bail;
  }
  /* file in the req msg id */
  dl_req_msg.msg_id = req_msg_id;
  /* initialize the read or search req msg */
  if (DL_REG_CMD_ITERATE == p_reg_cmd->cmd_type) {
    p_read_req_msg = (sns_reg_group_read_req_msg_v02*)dl_req_msg.buffer;
    p_read_req_msg->group_id = p_reg_cmd->group_id_single;
  } else {
    p_srch_req_msg = (sns_reg_group_search_req_msg_v02*)dl_req_msg.buffer;
    p_srch_req_msg->group_id_start_valid  = 1;
    p_srch_req_msg->group_id_start        = p_reg_cmd->group_id_start;
    p_srch_req_msg->group_id_end_valid    = 1;
    p_srch_req_msg->group_id_end          = p_reg_cmd->group_id_stop;
    p_srch_req_msg->search_position_valid = 1;
    p_srch_req_msg->search_position       = p_reg_cmd->srch_pos;
    p_srch_req_msg->data_len              = p_reg_cmd->srch_data_len;
    memcpy(p_srch_req_msg->data,
            p_reg_cmd->srch_data,
            p_reg_cmd->srch_data_len);    
  }
  /* initialize the resp msg */
  p_dl_resp_msg->pfn_free       = p_svc->pfn_free;
  p_dl_resp_msg->p_reg_cmd      = p_reg_cmd;
  p_dl_resp_msg->p_req_buffer   = dl_req_msg.buffer;
  p_dl_resp_msg->msg.msg_id     = dl_req_msg.msg_id;
  p_dl_resp_msg->msg.buffer     = ptr;
  p_dl_resp_msg->msg.buffer_sz  = resp_msg_size;

  SNS_DL_PRINTF(LOW,
            "sns_dl_reg_client_resp_cb Allocating %#x %#x %#x %d FreePtr:0x%p",
              p_dl_resp_msg->msg.buffer,
              p_dl_resp_msg->p_req_buffer,
              p_dl_resp_msg,
              p_dl_resp_msg->msg.buffer_sz,
              p_dl_resp_msg->pfn_free);

  err = smr_client_send_req(p_svc->reg_client_handle,
                            dl_req_msg.msg_id,
                            (void*)dl_req_msg.buffer,         //ptr to request
                            dl_req_msg.buffer_sz,             //len of request 
                            (void*)p_dl_resp_msg->msg.buffer, //ptr to resp buf
                            p_dl_resp_msg->msg.buffer_sz,     //len of resp buf
                            sns_dl_reg_client_resp_cb,        //callback func
                            p_dl_resp_msg, &txn_handle);
  if( SMR_NO_ERR != err ) {
    SNS_DL_PRINTF(ERROR, "smr_client_send_req error 0x%x", err);
    ret_val = SNS_ERR_FAILED;
  } else {
    ret_val = SNS_SUCCESS;
  }
bail:
  if (SNS_SUCCESS != ret_val) {
    FREEIFPFN(p_svc->pfn_free, ptr);
    FREEIFPFN(p_svc->pfn_free, dl_req_msg.buffer);
    FREEIFPFN(p_svc->pfn_free, p_dl_resp_msg);
  }
  return ret_val;
}
