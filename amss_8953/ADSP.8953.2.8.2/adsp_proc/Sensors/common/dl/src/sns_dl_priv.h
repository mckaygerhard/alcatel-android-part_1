/*==============================================================================
  @file sns_dl_priv.h

  @brief
  Internal header containing all the private methods and data structures
  required for the implementation of the DL SVC.

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/
#ifndef SNS_DL_PRIV_H
#define SNS_DL_PRIV_H

/* Compile in and always log these messages */
#define FARF_ERROR    1
#define FARF_FATAL    1

/* only in debug builds compile in and always log these messages */
#ifdef FEATURE_SNS_DL_DEBUG
  #define FARF_LOW      1
  #define FARF_MEDIUM   1
  #define FARF_HIGH     1
#endif

/* define these before including the HAP header */
#include "HAP_farf.h"

#define SNS_DL_PRINTF FARF
//#define SNS_DL_PRINTF(x, ...)

#include "sns_reg_common.h"
#include "sns_em.h"
#include "sns_usmr.h"
#include "sns_queue.h"
#include "sns_dl.h"
#include "sns_dl_reg.h"

/* prio required by mutex api, basically ignored by the SNS OSA */
#define SNS_DL_MODULE_MTX_PRIO  0

#define SNS_DL_REG_CTXT_SIG_CHK   0x1
#define SNS_DL_REG_CTXT_SIG_SRCH  0x2
#define SNS_DL_REG_CTXT_SIG_ITER  0x4

#define FREEIFPFN(pfn, ptr) \
  do {                      \
    if ((ptr))              \
      (pfn)((ptr));         \
  } while(0)

#define SNS_DL_FUNC_NAME_SZ 16

typedef enum
{
  STATE_UNINIT = 0,
  STATE_READY_TO_INIT,
  STATE_READY,
  STATE_ERROR,
} sns_dl_svc_state_t;

typedef enum
{
  EVENT_INIT = 0,
  EVENT_STOP,
  EVENT_READY,
} sns_dl_reg_event_t;

typedef enum
{
  DL_REG_CMD_ITERATE = 0,
  DL_REG_CMD_SEARCH,
} sns_dl_reg_cmd_type_t;


typedef enum
{
  SNS_DL_REG_START = 0,
  SNS_DL_REG_UUID = SNS_DL_REG_START,
  
  SNS_DL_REG_STOP,
} sns_dl_reg_identifier_t;

/** Forward Declarations */
struct sns_dl_svc_s;
struct sns_dl_reg_s;
struct sns_dl_svc_mgr_s;

/**
  @brief Function type to compare a field such as UUID in the cfg against id
*/
typedef bool
(*pfn_sns_reg_match)(void* id, uint32_t id_sz, sns_reg_dyn_cfg_t *p_cfg);

/**
  @brief Function type to create a handle to DL SVC.
*/
typedef
sns_err_code_e (*pfn_sns_dl_svc_ctor)(struct sns_dl_svc_s*, sns_dl_attr_t*);

/**
  @brief Function type to create a handle to DL SVC.
*/
typedef sns_err_code_e (*pfn_sns_dl_svc_dtor)(struct sns_dl_svc_s*);

/**
  @brief API to check if the DL SVC is fully initialized.
*/
typedef
sns_err_code_e (*pfn_sns_dl_svc_is_initialized)(struct sns_dl_svc_s*,
                                                pfn_is_initialized_cb,
                                                void *,
                                                uint32_t timeout_us);
/**
  @brief  API to iterate over all the boot critical SNS algos and drivers.
*/
typedef
sns_err_code_e (*pfn_sns_dl_reg_for_each)
(
  struct sns_dl_svc_s   *p_svc,
  pfn_sns_reg_for_each  pfn,
  sns_reg_dyn_cfg_t     *p_cfg_param,
  uint32_t              group_id_start,
  uint32_t              group_id_stop,
  uint32_t              timeout_us,
  bool                  timing_profile_flag
);

/**
  @brief  API to search the DL registry.
*/
typedef
sns_err_code_e (*pfn_sns_dl_reg_search)
(
  struct sns_dl_svc_s     *p_svc,
  sns_reg_dyn_cfg_t       *p_cfg,
  void                    *id,
  size_t                  id_sz,
  sns_dl_reg_identifier_t id_type,
  uint32_t                group_id_start,
  uint32_t                group_id_stop,
  uint32_t                timeout_us,
  bool                    timing_profile_flag
);

/**
  @brief  Print diagnostic information pertaining to the specified handle.
*/
typedef void (*pfn_sns_dl_svc_print_state)(struct sns_dl_svc_s*);

struct sns_dl_svc_vtbl
{
  pfn_sns_dl_svc_ctor           ctor;
  pfn_sns_dl_svc_dtor           dtor;
  pfn_sns_dl_svc_is_initialized is_init;
  pfn_sns_dl_reg_for_each       for_each;
  pfn_sns_dl_reg_search         search;
  pfn_sns_dl_svc_print_state    print;
};
typedef struct sns_dl_svc_vtbl sns_dl_svc_vtbl_t;

struct sns_dl_reg_ctxt_s
{
  sns_q_link_s        q_link; // link to queue callers for different operations
  struct sns_dl_svc_s *p_svc; // reference to svc parent
};
typedef struct sns_dl_reg_ctxt_s sns_dl_reg_ctxt_t;

struct sns_dl_svc_s
{
  sns_q_link_s        q_link; // link to queue clients of the DL svc
  char                name[SNS_DL_CLIENT_NAME_SZ];
  pfn_sns_dl_malloc   pfn_malloc;
  pfn_sns_dl_free     pfn_free;
  int                 init_magic;
  sns_dl_svc_state_t  init_status;
  sns_err_code_e      err_code;
  smr_client_hndl     reg_client_handle;
  bool                reg_client_handle_valid;
  sig_node*           sig;    // sig to block and wakeup clients
  OS_EVENT            *mutex;
  sns_q_s             request_q;
  int                 line;
  char                func_name[SNS_DL_FUNC_NAME_SZ];
  struct sns_dl_svc_mgr_s *p_mgr; // reference to svc mgr parent
  sns_dl_svc_vtbl_t   vtbl;
};
typedef struct sns_dl_svc_s sns_dl_svc_t;

struct sns_dl_reg_s
{
  qmi_idl_service_object_type reg_svc_obj;
  qmi_service_instance reg_instance_id;
};
typedef struct sns_dl_reg_s sns_dl_reg_t;

struct sns_dl_svc_mgr_s
{
  sns_q_s       sns_dl_svc_q; // queue for all SVC allocations
  OS_EVENT      *mutex;       // mtx for queue
  sns_dl_reg_t  reg;          // registry obj used by all clients
};

struct sns_dl_reg_cmd_resp_s
{
  sns_q_link_s  q_link; // link to queue clients of the DL svc
  void          *reg_data;
  uint32_t      reg_data_size;
  uint32_t      group_id;
  void          *ptr_to_free;
};
typedef struct sns_dl_reg_cmd_resp_s sns_dl_reg_cmd_resp_t;

typedef struct sns_dl_svc_mgr_s sns_dl_svc_mgr_t;

struct sns_dl_reg_cmd_s;
typedef sns_err_code_e (*pfn_sns_dl_reg_cmd)(struct sns_dl_reg_cmd_s *,
                                              void* param);

typedef void (*pfn_sns_dl_reg_cmd_resp)(struct sns_dl_reg_cmd_s *p_reg_cmd,
                                          uint32_t  group_id,
                                          void      *data,
                                          uint32_t  data_size,
                                          bool      is_err,
                                          void      *ptr_to_free,
                                          bool      *user_callback_to_free_mem);
struct sns_dl_reg_cmd_s
{
  sns_q_link_s      q_link;
  uint32_t          group_id_start;
  uint32_t          group_id_stop;
  uint32_t          group_id_single;
  uint32_t          batch_size;
  uint32_t          err_responses;
  uint32_t          msg_responses;
  void              *srch_data;
  uint32_t          srch_data_len;
  uint8_t           srch_pos;
  sns_em_timer_obj_t timeout_timer;
  uint32_t          timeout_us;
  sns_dl_reg_cmd_type_t  cmd_type;
  pfn_sns_dl_reg_cmd  pfn;
  void              *pfn_cb_data;
  pfn_sns_dl_reg_cmd_resp pfn_reg_cmd_resp;
  OS_FLAG_GRP       *sig;
  sns_dl_svc_t      *p_svc;
  sns_q_s           reg_response_q;
  OS_EVENT          *mutex;       // mtx for queue
  bool              timing_profile_flag;
};
typedef struct sns_dl_reg_cmd_s sns_dl_reg_cmd_t;
  
#define GET_SVC_FROM_HANDLE(handle, p_svc)      \
  do {                                          \
    if (handle != SNS_DL_HANDLE_INITIALIZER) {  \
      p_svc = (sns_dl_svc_t*)handle;            \
      p_svc = ((SNS_DL_INIT_MAGIC == p_svc->init_magic)?p_svc:NULL);  \
    } else {                                    \
      p_svc = NULL;                             \
    }                                           \
  } while(0)

#define GET_DL_REG_REF(p_svc) (&(p_svc->p_mgr->reg))

/**
  @brief SNS MUTEX Lock/Unlock helper macros
*/
#define SNS_DL_LOCK(mutex)                          \
  do                                                \
  {                                                 \
    uint8_t __os_err = 0;                           \
    sns_os_mutex_pend((mutex), 0, &__os_err);       \
    SNS_ASSERT(OS_ERR_NONE == __os_err)

#define SNS_DL_UNLOCK(mutex)                        \
    __os_err = sns_os_mutex_post((mutex));          \
    SNS_ASSERT(OS_ERR_NONE == __os_err);            \
  } while(0)

#define SNS_DL_LOCK_VAR(mutex, __os_err)            \
    sns_os_mutex_pend((mutex), 0, &__os_err);       \
    SNS_ASSERT(OS_ERR_NONE == __os_err)

#define SNS_DL_UNLOCK_VAR(mutex, __os_err)          \
    __os_err = sns_os_mutex_post((mutex));          \
    SNS_ASSERT(OS_ERR_NONE == __os_err)

sns_err_code_e sns_dl_reg_ctor(sns_dl_reg_t *p_reg);
sns_err_code_e sns_dl_reg_dtor(sns_dl_reg_t *p_reg);
sns_err_code_e sns_dl_reg_client_create(sns_dl_svc_t *p_svc, uint32_t timeout);
sns_err_code_e sns_dl_reg_client_destroy(sns_dl_svc_t *p_svc);

sns_err_code_e sns_dl_reg_init_connection
(
  sns_dl_svc_t *p_svc,
  uint32_t timeout_us,
  bool *is_initialized
);

sns_err_code_e
sns_dl_reg_deinit_connection(sns_dl_svc_t *p_svc);

sns_err_code_e sns_dl_reg_is_initialized_cb
(
  sns_dl_svc_t    *p_svc,
  sns_dl_reg_t    *p_reg,
  pfn_is_initialized_cb pfn,
  void            *param,
  uint32_t        timeout_us
);

sns_err_code_e
sns_dl_reg_client_perform_ops
(
  sns_dl_reg_cmd_t *p_reg_cmd
);

extern sns_dl_svc_mgr_t sns_dl_mgr;

void sns_dl_svc_update_init_state
(
  sns_dl_svc_t        *p_svc,
  sns_dl_svc_state_t  state,
  sns_err_code_e      err_code,
  const char          *func_name,
  int                 line
);

#define SNS_DL_SVC_UPDATE_STATE(p_svc, state, err_code)   \
  sns_dl_svc_update_init_state((p_svc), (state), (err_code), __func__, __LINE__)

void sns_dl_svc_get_init_state
(
  sns_dl_svc_t        *p_svc,
  sns_dl_svc_state_t  *p_state,
  sns_err_code_e      *p_err_code
);

sns_err_code_e sns_dl_svc_check_and_initialize
(
  sns_dl_svc_t *p_svc
);

void sns_dl_svc_enqueue_command
(
  sns_dl_svc_t *p_svc,
  sns_dl_reg_cmd_t *p_reg_cmd
);

void sns_dl_svc_dequeue_command
(
  sns_dl_svc_t *p_svc,
  sns_dl_reg_cmd_t *p_reg_cmd
);

bool sns_dl_svc_validate_reg_cmd_and_lock_svc
(
  sns_dl_reg_cmd_t *p_reg_cmd
);

void sns_dl_svc_reg_cmd_unlock_svc
(
  sns_dl_svc_t *p_svc
);

void sns_dl_svc_handle_error
(
  sns_dl_svc_t *p_svc,
  sns_err_code_e err_code
);

void sns_dl_reg_cmd_force_exit(sns_dl_reg_cmd_t *p_reg_cmd);

uint32_t sns_dl_read_platform_endian_uint32(uint32_t rev);
void sns_dl_memcpy_v01(sns_reg_dyn_cfg_t *p_dest, sns_reg_dyn_cfg_v01_t *p_src);
bool sns_dl_is_in_algo_window(uint32_t grp_id_start, uint32_t grp_id_stop);
bool sns_dl_is_in_driver_window(uint32_t grp_id_start, uint32_t grp_id_stop);
bool sns_dl_is_in_window(uint32_t group_id_start, uint32_t group_id_stop);

bool sns_dl_cmp_uuid_cb(void* id, uint32_t id_sz, sns_reg_dyn_cfg_t *p_cfg);

sns_err_code_e memcpy_reg_data
(
  sns_reg_dyn_cfg_t *p_cfg,
  void *src,
  uint32_t src_sz
);

sns_err_code_e sns_dl_svc_stub_ctor(sns_dl_svc_t *p_svc, sns_dl_attr_t *p_attr);
sns_err_code_e sns_dl_svc_stub_dtor(sns_dl_svc_t *p_svc);
sns_err_code_e sns_dl_svc_stub_is_init(sns_dl_svc_t *p_svc,
                                        pfn_is_initialized_cb pfn,
                                        void *param,
                                        uint32_t timeout_us);
sns_err_code_e sns_dl_svc_stub_for_each
(
  sns_dl_svc_t          *p_svc,
  pfn_sns_reg_for_each  pfn,
  sns_reg_dyn_cfg_t     *p_cfg_param,
  uint32_t              group_id_start,
  uint32_t              group_id_stop,
  uint32_t              timeout_us,
  bool                  timing_profile_flag
);

sns_err_code_e sns_dl_svc_stub_search_identifier
(
  sns_dl_svc_t      *p_svc,
  sns_reg_dyn_cfg_t *p_cfg,
  void              *id,
  size_t            id_sz,
  sns_dl_reg_identifier_t id_type,
  uint32_t          group_id_start,
  uint32_t          group_id_stop,
  uint32_t          timeout_us,
  bool              timing_profile_flag
);

void sns_dl_svc_stub_print(sns_dl_svc_t *p_svc);

sns_err_code_e sns_dl_svc_reg_search_identifier
(
  sns_dl_svc_t      *p_svc,
  sns_reg_dyn_cfg_t *p_cfg,
  void              *id,
  size_t            id_sz,
  sns_dl_reg_identifier_t id_type,
  uint32_t          group_id_start,
  uint32_t          group_id_stop,
  uint32_t          timeout_us,
  bool              timing_profile_flag
);

sns_err_code_e sns_dl_svc_reg_for_each
(
  sns_dl_svc_t          *p_svc,
  pfn_sns_reg_for_each  pfn,
  sns_reg_dyn_cfg_t     *p_cfg_param,
  uint32_t              group_id_start,
  uint32_t              group_id_stop,
  uint32_t              timeout_us,
  bool                  timing_profile_flag
);

#endif //SNS_DL_PRIV_H
