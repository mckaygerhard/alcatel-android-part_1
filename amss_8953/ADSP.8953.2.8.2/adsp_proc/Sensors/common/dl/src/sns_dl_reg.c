#include <stringl.h>
#include <string.h>
#include <stdbool.h>

#include "sns_reg_api_v02.h"
#include "sns_dl_priv.h"

//-----------------------------------------------------------------------------
// DATA DEFINITIONS
//-----------------------------------------------------------------------------

/* QMI instance id for REG V02 service */
#define SNS_DL_REG_INSTANCE_ANY 0xFFFF

/**
  @brief Data structure we pass into USMR to determine if the registry service
  is available and running.
*/
struct sns_dl_reg_init_check_s
{
  /**< -- Pointer to the DL SVC object on whose behalf we want to determine
          availability of the registry service */
  sns_dl_svc_t          *p_svc;
  /**< -- Call back function parameter */
  void                  *init_cb_param;
  /**< -- Call back function to be issued after determining whether or not
          the registry service is available or not */
  pfn_is_initialized_cb pfn_init_cb;
};
typedef struct sns_dl_reg_init_check_s sns_dl_reg_init_check_t;

//-----------------------------------------------------------------------------
// DATA DECLARATIONS
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// FORWARD DECLARATIONS
//-----------------------------------------------------------------------------
STATIC
sns_err_code_e sns_dl_reg_check_client
(
  sns_dl_svc_t            *p_svc,
  sns_dl_reg_t            *p_reg,
  sns_dl_reg_init_check_t *p_init_check_data,
  uint32_t                timeout
);

STATIC
void sns_dl_reg_client_init_cb
(
  qmi_idl_service_object_type service_obj,
  qmi_service_instance instance_id,
  bool timeout_expired,
  void *init_cb_param
);

//-----------------------------------------------------------------------------
// IMPLEMENTATION 
//-----------------------------------------------------------------------------
/**
  @brief Initialize the SNS registry parameters.
  
  @param[in] p_reg -- Pointer to a SNS reg obj
  
  @return
    SNS_SUCCESS -- Call is successful
    SNS_ERR_FAILED -- Otherwise
*/
sns_err_code_e sns_dl_reg_ctor(sns_dl_reg_t *p_reg)
{
  sns_err_code_e ret_val = SNS_SUCCESS;
  p_reg->reg_svc_obj = SNS_REG2_SVC_get_service_object_v02();
  p_reg->reg_instance_id = SNS_DL_REG_INSTANCE_ANY;
  return ret_val;
}

/**
  @brief De-initialize the SNS registry parameters.
  
  @param[in] p_reg -- Pointer to a SNS reg obj
  
  @return
    SNS_SUCCESS -- Call is successful
    SNS_ERR_FAILED -- Otherwise
*/
sns_err_code_e sns_dl_reg_dtor(sns_dl_reg_t *p_reg)
{
  sns_err_code_e ret_val = SNS_SUCCESS;
  memset(&p_reg->reg_svc_obj, 0, sizeof(qmi_idl_service_object_type));
  p_reg->reg_instance_id = 0;
  return ret_val;
}

/**
  @brief  Attempt to initialize a connection to the SNS registry for
          the provided DL SVC obj.
  
  @param[in]  p_svc -- Pointer to the SVC object
  
  @return
    SNS_SUCCESS         -- Call is successful
    SNS_ERR_FAILED      -- General Failure
    SNS_ERR_NOMEM       -- Out of memory
*/
sns_err_code_e sns_dl_reg_init_connection
(
  sns_dl_svc_t *p_svc,
  uint32_t timeout_us,
  bool *is_initialized
)
{
  sns_err_code_e ret_val;
  sns_dl_svc_state_t state;
  /* this call will block (depending on the timeout) until USMR returns */
  ret_val = sns_dl_reg_client_create(p_svc, timeout_us);
  if (SNS_SUCCESS == ret_val) {
    state = STATE_READY;
    *is_initialized = true;
  } else {
    SNS_DL_PRINTF(ERROR, "sns_dl_reg_init_connection Error 0x%x", ret_val);
    state = STATE_ERROR;
    *is_initialized = false;
  }
  /* this call can update svc init state to READY or ERROR */
  SNS_DL_SVC_UPDATE_STATE(p_svc, state, ret_val);
  return ret_val;
}

/**
  @brief  Attempt to de-initialize a connection to the SNS registry for
          the provided DL SVC obj.
  
  @param[in]  p_svc -- Pointer to the SVC object
  
  @return
    SNS_SUCCESS         -- Call is successful
    SNS_ERR_FAILED      -- General Failure
*/
sns_err_code_e sns_dl_reg_deinit_connection(sns_dl_svc_t *p_svc)
{
  sns_err_code_e ret_val, err_code;
  sns_dl_svc_state_t state;
  ret_val = sns_dl_reg_client_destroy(p_svc);  
  if (SNS_SUCCESS == ret_val) {
    state = STATE_UNINIT;
    err_code = SNS_ERR_WOULDBLOCK;
  } else {
    SNS_DL_PRINTF(ERROR, "sns_dl_reg_deinit_connection Error 0x%x", ret_val);
    state = STATE_ERROR;
    err_code = ret_val;
  }
  /* this call can update svc init state to UNINIT or ERROR */
  SNS_DL_SVC_UPDATE_STATE(p_svc, state, err_code);
  return ret_val;
}

/**
  @brief  Attempt to check if a connection to the SNS registry is possible
          for the provided DL SVC obj.
  
  @param[in]  p_svc  -- Pointer to the SVC object
  @param[in]  p_reg  -- Pointer to the DL registry object which has the details
                        of the registry to connect to
  @param[in]  pfn_init_cb  -- Callback function to be invoked after the check
  @param[in]  init_cb_param  -- Callback function parameter
  @param[in]  timeout_us  - Blocking timeout supplied by caller

  @note   sns_dl_reg_client_init_cb is where the clean up of the allocated
          structures to perform this query is done

  @return
    SNS_SUCCESS         -- Call is successful
    SNS_ERR_FAILED      -- General Failure
    SNS_ERR_NOMEM       -- Out of memory
*/
sns_err_code_e sns_dl_reg_is_initialized_cb
(
  sns_dl_svc_t          *p_svc,
  sns_dl_reg_t          *p_reg,
  pfn_is_initialized_cb pfn_init_cb,
  void                  *init_cb_param,
  uint32_t              timeout_us
)
{
  sns_err_code_e ret_val;
  sns_dl_reg_init_check_t *p_init_check_data;

  p_init_check_data = p_svc->pfn_malloc(sizeof(sns_dl_reg_init_check_t));
  if (NULL != p_init_check_data) {
    p_init_check_data->p_svc = p_svc;
    p_init_check_data->pfn_init_cb = pfn_init_cb;
    p_init_check_data->init_cb_param = init_cb_param;
    ret_val = sns_dl_reg_check_client(p_svc, p_reg,
                                        p_init_check_data, timeout_us);
  } else {
    ret_val = SNS_ERR_NOMEM;
  }
  return ret_val;
}

/**
  @brief  Implementation based on USMR to check if a connection to the 
          SNS registry is possible for the provided DL SVC obj.
  
  @param[in]  p_svc  -- Pointer to the SVC object
  @param[in]  p_reg  -- Pointer to the DL registry object which has the details
                        of the registry to connect to
  @param[in]  pfn_init_cb  -- Callback function to be invoked after the check
  @param[in]  init_cb_param  -- Callback function parameter
  @param[in]  timeout_us  - Blocking timeout supplied by caller
  
  @return
    SNS_SUCCESS         -- Call is successful
    SNS_ERR_FAILED      -- General Failure
    SNS_ERR_NOMEM       -- Out of memory
*/
STATIC
sns_err_code_e sns_dl_reg_check_client
(
  sns_dl_svc_t            *p_svc,
  sns_dl_reg_t            *p_reg,
  sns_dl_reg_init_check_t *p_init_check_data,
  uint32_t                timeout_us
)
{
  smr_err err = SMR_NO_ERR;
  sns_err_code_e ret_val;

  err = smr_client_check_ext(p_reg->reg_svc_obj,
                              p_reg->reg_instance_id,
                              timeout_us,
                              sns_dl_reg_client_init_cb,
                              (void*)p_init_check_data);
  if (SMR_NO_ERR == err) {
    ret_val = SNS_SUCCESS;
    SNS_DL_PRINTF(LOW, "SNS DL Check Success 0x%x", err);
  } else {
    ret_val = SNS_ERR_FAILED;
    SNS_DL_PRINTF(ERROR, "SNS DL Check Failed 0x%x", err);
    /* this call can update svc init state to ERROR */
    SNS_DL_SVC_UPDATE_STATE(p_svc, STATE_ERROR, ret_val);
  }
  return ret_val;
}

/**
 * @brief
 * This callback function is called by the SMR infrastructure when a service
 * becomes available, or if the client-specified timeout passes.
 *
 * @param[in] service_obj       -- QMI service object
 * @param[in] instance_id       -- Instance ID of the service found
 * @param[in] timeout_expired   -- Whether the timeout expired
 * @param[in] init_cb_param     -- User passed in parameter
 */
STATIC
void sns_dl_reg_client_init_cb
(
  qmi_idl_service_object_type service_obj,
  qmi_service_instance instance_id,
  bool timeout_expired,
  void *init_cb_param
)
{
  sns_dl_svc_t *p_svc;
  sns_dl_svc_state_t state;
  sns_err_code_e ret_val;
  sns_dl_reg_init_check_t *p_init_check_data;
  bool is_ready;

  p_init_check_data = (sns_dl_reg_init_check_t*)(init_cb_param);
  if (p_init_check_data) {
    SNS_DL_PRINTF(LOW, "Found a Ready Check Client");
    p_svc = p_init_check_data->p_svc;
    if (false == timeout_expired) {
      /* service is up and ready to connect to */
      state = STATE_READY_TO_INIT;
      ret_val = SNS_SUCCESS;
      SNS_DL_PRINTF(LOW, "SNS DL Check CB Passed 0x%x %d",
                      instance_id, timeout_expired);
      /* this call can update svc init state to STATE_READY_TO_INIT */
      SNS_DL_SVC_UPDATE_STATE(p_svc, state, ret_val);
      is_ready = true;
    } else {
      /* service is unavailable at this time */
      ret_val = SNS_ERR_WOULDBLOCK;
      SNS_DL_PRINTF(ERROR, "SNS DL Check CB Failed 0x%x %d",
                            instance_id, timeout_expired);
      is_ready = false;
    }
    /* invoke the user supplied callback and inform them of availability */
    p_init_check_data->pfn_init_cb(p_init_check_data->init_cb_param,
                                    is_ready,
                                    ret_val);
    p_svc->pfn_free(p_init_check_data);
  }
}
