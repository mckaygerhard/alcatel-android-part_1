/*==============================================================================
  @file sns_dl_test.c

  @brief
  Implementation of the DL Service Test

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/
#ifndef _DEBUG
#define _DEBUG 1
#endif /*_DEBUG*/

#include "HAP_farf.h"

#include <string.h>
#include "rcinit.h"
#include "sns_reg_common.h"
#include "sns_memmgr.h"
#include "sns_dl.h"

#ifdef FEATURE_SNS_DL_TEST

#define DL_SVC_TEST_TIMEOUT_US 100000

#define SNS_DL_PRINTF FARF

#define FEATURE_SNS_DL_TEST_TIMING_PROFILE
#define FEATURE_SNS_DL_TEST_NEGATIVE_TESTS

typedef struct
{
  uint8_t uuid[SNS_DRIVER_UUID_SZ];
} sns_dl_test_driver_props_t;

sns_dl_test_driver_props_t sns_dl_test_driver_props[] =
{
  {SNS_REG_UUID_BMG160},
  {SNS_REG_UUID_BMA2X2},
  {SNS_REG_UUID_HSCD008}
};

int sns_dl_test_num_driver_objs = (sizeof(sns_dl_test_driver_props)/
                        sizeof(sns_dl_test_driver_props[0]));

static sns_dl_svc_handle_t dl_svc_test_handle = SNS_DL_HANDLE_INITIALIZER;
static sns_err_code_e g_dl_svc_test_err_code = SNS_ERR_NOTSUPPORTED;
static bool g_dl_svc_test_is_ready = false;
static sns_reg_dyn_cfg_t g_dl_svc_test_cfg;

static sns_err_code_e dl_svc_init(sns_dl_svc_handle_t *p_handle);
static sns_err_code_e dl_svc_deinit(sns_dl_svc_handle_t *p_handle);
static sns_err_code_e dl_svc_timing(sns_dl_svc_handle_t handle);
static sns_err_code_e dl_svc_basic(sns_dl_svc_handle_t handle);
static sns_err_code_e dl_svc_negative_test_timeout(sns_dl_svc_handle_t handle);
static sns_err_code_e dl_svc_negative_test_error(sns_dl_svc_handle_t handle);
static void dl_svc_main(void);

extern sns_err_code_e sns_dl_reg_test_hook_timing_for_each_algo
(
  sns_dl_svc_handle_t svc_handle,
  pfn_sns_reg_for_each pfn,
  sns_reg_dyn_cfg_t *p_cfg,
  uint32_t timeout_us
);

extern sns_err_code_e sns_dl_reg_test_hook_timing_for_each_driver
(
  sns_dl_svc_handle_t svc_handle,
  pfn_sns_reg_for_each pfn,
  sns_reg_dyn_cfg_t *p_cfg,
  uint32_t timeout_us
);

extern sns_err_code_e sns_dl_reg_test_hook_timing_search_uuid
(
  sns_dl_svc_handle_t svc_handle,
  void* id,
  size_t id_sz,
  sns_reg_dyn_cfg_t *p_cfg,
  uint32_t timeout_us
);

extern sns_err_code_e sns_dl_reg_test_hook_timeout
(
  sns_dl_svc_handle_t svc_handle,
  uint32_t timeout_us
);

extern sns_err_code_e sns_dl_reg_test_hook_error
(
  sns_dl_svc_handle_t svc_handle,
  uint32_t timeout_us
);

void dl_svc_test_entry(unsigned long int unused_param)
{
  rcinit_handshake_startup(); // required rcinit handshake
  do {
    // Fire every 30 seconds
    qurt_timer_sleep(60000000);
    dl_svc_main();
  } while (1);
}

static void* dl_svc_test_malloc(size_t num_bytes)
{
  return SNS_OS_ANY_MALLOC(SNS_DBG_MOD_DSPS_SMGR, num_bytes);
}

static void dl_svc_test_free(void* p)
{
  SNS_OS_ANY_FREE(p);
}
  
static void dl_svc_test_is_initialized_cb
(
  void *param,
  bool is_initialized,
  sns_err_code_e err_code
)
{
  int x = (int)param;
  SNS_DL_PRINTF(HIGH, "dl_svc_test_is_initialized_cb Init CB. Param:%d,"
              " Is Init:%d Err Code %#x", x , is_initialized, err_code);
  g_dl_svc_test_is_ready = is_initialized;
  g_dl_svc_test_err_code = err_code;
}

static
void sns_dl_test_print_cfg(sns_reg_dyn_cfg_t *p_cfg)
{
  static int test_ctr = 0;
  int i;
  static char uuid_string[(SNS_DRIVER_UUID_SZ * 5) + 1];
  char *ptr = uuid_string;
  test_ctr++;
  for (i=0; i<SNS_DRIVER_UUID_SZ; i++) {
    snprintf(ptr, 6, "0x%2.2x,", p_cfg->uuid[i]);
    ptr += 5;
  }
  uuid_string[sizeof(uuid_string) - 1] = 0;
  //FARF has a problem with emitting strings reliably so for now we just log
  //a very minimal set of data
  SNS_DL_PRINTF(HIGH, "CTR:%d DLCFG Load Attr:[0x%x], UUID:[%c]",
                                                      test_ctr,
                                                      p_cfg->load_attrib,
                                                      uuid_string[0]);

// SNS_DL_PRINTF(HIGH,
              // "CTR:%d DLCFG Path:[%s], Sym[%s] Load Attr:[0x%x], UUID:[%s]",
                                                    // test_ctr,
                                                    // p_cfg->full_file_path,
                                                    // p_cfg->entry_symbol,
                                                    // p_cfg->load_attrib,
                                                    // uuid_string);
}

static void dl_svc_main(void)
{
  bool init_successful = false;
  sns_err_code_e ret_val, destr_ret_val;
  ret_val = dl_svc_init(&dl_svc_test_handle);
  if (SNS_SUCCESS != ret_val)
    goto bail;
  init_successful = true;
  ret_val = dl_svc_basic(dl_svc_test_handle);
  if (SNS_SUCCESS != ret_val)
    goto bail;
  ret_val = dl_svc_timing(dl_svc_test_handle);
  if (SNS_SUCCESS != ret_val)
    goto bail;
  ret_val = dl_svc_negative_test_timeout(dl_svc_test_handle);
  if (SNS_SUCCESS != ret_val)
    goto bail;
  ret_val = dl_svc_negative_test_error(dl_svc_test_handle);
  if (SNS_SUCCESS != ret_val)
    goto bail;
bail:
  if (init_successful) {
    destr_ret_val = dl_svc_deinit(&dl_svc_test_handle);
    if (SNS_SUCCESS != destr_ret_val) {
      SNS_DL_PRINTF(ERROR, "dl_svc_deinit Failed! %#x", destr_ret_val);
    }
  }
  if (SNS_SUCCESS == ret_val)
    SNS_DL_PRINTF(FATAL, "dl_svc_main Passed!");
  else
    SNS_DL_PRINTF(FATAL, "dl_svc_main Test Failed!");
};

static
sns_err_code_e dl_svc_init(sns_dl_svc_handle_t *p_handle)
{
  sns_err_code_e ret_val;
  sns_dl_attr_t attr;
  static char dl_svc_tst[] = "DLSVCTEST";

  ret_val = sns_dl_attr_init(&attr);
  if (SNS_SUCCESS == ret_val) {
    sns_dl_set_attr_name(&attr, dl_svc_tst, sizeof(dl_svc_tst));
    sns_dl_set_attr_allocator(&attr, dl_svc_test_malloc, dl_svc_test_free);
    sns_dl_set_attr_blocking_info(&attr, true, DL_SVC_TEST_TIMEOUT_US,
                                    NULL, NULL);
    ret_val = sns_dl_svc_create(p_handle, &attr);
    if (SNS_SUCCESS == ret_val)
      SNS_DL_PRINTF(HIGH, "Handle Created Successfully!");
    else
      SNS_DL_PRINTF(ERROR, "Handle Create Error %#x", ret_val);
    sns_dl_attr_deinit(&attr);
  }
  return ret_val;
}

static
sns_err_code_e dl_svc_deinit(sns_dl_svc_handle_t *p_handle)
{
  sns_err_code_e ret_val;
  ret_val = sns_dl_svc_destroy(*p_handle);
  if (SNS_SUCCESS == ret_val) {
    SNS_DL_PRINTF(HIGH, "sns_dl_svc_test Destroy Handle Success");
    *p_handle = SNS_DL_HANDLE_INITIALIZER;
  } else {
    SNS_DL_PRINTF(ERROR, "sns_dl_svc_test Destroy Handle Failed %#x", ret_val);
  }
  return ret_val;
}

static
sns_err_code_e dl_svc_basic(sns_dl_svc_handle_t handle)
{
  int i;
  sns_err_code_e ret_val;
  ret_val = sns_dl_svc_is_initialized(handle,
                            dl_svc_test_is_initialized_cb,
                            (void*)(int)1000, DL_SVC_TEST_TIMEOUT_US);
  SNS_DL_PRINTF(HIGH, "sns_dl_svc_is_initialized ret_val %#x", ret_val);
  if (SNS_SUCCESS == ret_val) {
    ret_val = sns_dl_reg_for_each_algorithm(handle,
                                            sns_dl_test_print_cfg,
                                            &g_dl_svc_test_cfg,
                                            DL_SVC_TEST_TIMEOUT_US);
    SNS_DL_PRINTF(HIGH, "sns_dl_svc_test for_each_alg ret_val %#x", ret_val);
    
    for (i = 0; i<sns_dl_test_num_driver_objs; i++) {
      ret_val = sns_dl_reg_search_uuid(handle,
                                        sns_dl_test_driver_props[i].uuid,
                                        SNS_DRIVER_UUID_SZ,
                                        &g_dl_svc_test_cfg,
                                        DL_SVC_TEST_TIMEOUT_US);
      SNS_DL_PRINTF(HIGH, "UUID SRCH Ret Val %#x", ret_val);
      if (SNS_SUCCESS == ret_val)
        sns_dl_test_print_cfg(&g_dl_svc_test_cfg);
    }
  }
  return ret_val;
}

static
sns_err_code_e dl_svc_timing(sns_dl_svc_handle_t handle)
{
  sns_err_code_e ret_val = SNS_SUCCESS;
#ifdef FEATURE_SNS_DL_TEST_TIMING_PROFILE
  static uint8_t uuid[SNS_DRIVER_UUID_SZ] = SNS_REG_UUID_BMG160;
  
  SNS_DL_PRINTF(HIGH, "Algo Timing Start");
  sns_dl_reg_test_hook_timing_for_each_algo(handle,
                                            sns_dl_test_print_cfg,
                                            &g_dl_svc_test_cfg,
                                            0);
  SNS_DL_PRINTF(HIGH, "Algo Timing Stop");
  SNS_DL_PRINTF(HIGH, "Driver Timing Start");
  sns_dl_reg_test_hook_timing_for_each_algo(handle,
                                            sns_dl_test_print_cfg,
                                            &g_dl_svc_test_cfg,
                                            0);
  SNS_DL_PRINTF(HIGH, "Driver Timing Stop");
  SNS_DL_PRINTF(HIGH, "Driver SRCH Timing Start");
  ret_val = sns_dl_reg_search_uuid(handle, uuid, SNS_DRIVER_UUID_SZ,
                                    &g_dl_svc_test_cfg, 0);
  SNS_DL_PRINTF(HIGH, "Driver SRCH Timing Stop %#x", ret_val);
#endif
  return ret_val;
}

static
sns_err_code_e dl_svc_negative_test_timeout(sns_dl_svc_handle_t handle)
{
  sns_err_code_e ret_val = SNS_SUCCESS;

#ifdef FEATURE_SNS_DL_TEST_NEGATIVE_TESTS
  /* timeout for 3 seconds */
  ret_val = sns_dl_reg_test_hook_timeout(handle, 3000000);
  SNS_DL_PRINTF(HIGH, "negative_test_timeout Expected %#x Got:%#x",
                          SNS_ERR_TIMEOUT, ret_val);

  if (SNS_ERR_TIMEOUT == ret_val) {
    ret_val = SNS_SUCCESS;
  }
#endif

  return ret_val;
}

static
sns_err_code_e dl_svc_negative_test_error(sns_dl_svc_handle_t handle)
{
  sns_err_code_e ret_val = SNS_SUCCESS;

#ifdef FEATURE_SNS_DL_TEST_NEGATIVE_TESTS
  /* timeout for 10 seconds */
  ret_val = sns_dl_reg_test_hook_error(handle, 10000000);
  SNS_DL_PRINTF(HIGH, "negative_test_error Expected %#x Got:%#x",
                      SNS_ERR_FAILED, ret_val);
  if (SNS_ERR_FAILED == ret_val) {
    ret_val = SNS_SUCCESS;
  }
#endif

  return ret_val;
}

#endif //FEATURE_SNS_DL_TEST
