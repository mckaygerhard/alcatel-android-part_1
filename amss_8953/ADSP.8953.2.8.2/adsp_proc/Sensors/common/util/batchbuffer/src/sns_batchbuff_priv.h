#ifndef SNS_BATCHBUFFER_PRIV_H
#define SNS_BATCHBUFFER_PRIV_H

/*============================================================================
  @file sns_batchbuffer.h

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.

  ============================================================================*/

/*=====================================================================
                       INCLUDES
=======================================================================*/
#include "sns_common.h"
#include "sns_memmgr.h"
#include "sns_queue.h"
#include "sns_batchbuffer.h"

/*=====================================================================
                    INTERNAL DEFINITIONS AND TYPES
=======================================================================*/

#ifdef SNS_USES_ISLAND
#define SNS_BATCHBUFF_UIMAGE_CODE __attribute__((section (".text.BATCHBUFF")))
#define SNS_BATCHBUFF_UIMAGE_DATA __attribute__((section (".data.BATCHBUFF")))
#define SNS_BATCHBUFF_UIMAGE_BSS __attribute__((section (".bss.BATCHBUFF")))
#else
#define SNS_BATCHBUFF_UIMAGE_CODE
#define SNS_BATCHBUFF_UIMAGE_DATA
#define SNS_BATCHBUFF_UIMAGE_BSS
#endif /* USES_ISLAND */

typedef struct
{
  sns_q_s   protected_q;
  OS_EVENT* mutex_ptr;
} sns_batchbuff_q_s;

typedef struct
{
  sns_q_link_s q_link;
  intptr_t     ptr;
}sns_batchbuff_blk_s;

typedef struct
{
  sns_batchbuff_threshold_cb threshold_cb;
  uint32_t                   free_blk_cnt;
}sns_batchbuff_threshold_cb_s;


/*=====================================================================
                          FUNCTIONS
=======================================================================*/



#endif /* SNS_BATCHBUFFER_PRIV_H */
