/*============================================================================
  @file sns_batchbuff.c

  @brief
  Contains implementation batching buffer utilities for SAM and SMGR.

  Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
  ============================================================================*/

#include "sns_batchbuff_priv.h"
#include "sns_batchbuffer.h"
#include "sns_common.h"
#include "sns_uimg_utils.h"
#include "sns_pm.h"
#include "sns_init.h"

#include "sns_debug_str.h"

SNS_BATCHBUFF_UIMAGE_DATA STATIC sns_batchbuff_q_s sns_batchbuff_blk_uimg_free_q;
SNS_BATCHBUFF_UIMAGE_DATA STATIC sns_batchbuff_q_s sns_batchbuff_blk_uimg_used_q;
SNS_BATCHBUFF_UIMAGE_DATA STATIC sns_batchbuff_threshold_cb_s sns_batchbuff_u_threshold_cb[SNS_BATCHBUFF_CLIENT_CNT][SNS_BATCHBUFF_MAX_CB_CNT];
SNS_BATCHBUFF_UIMAGE_DATA STATIC intptr_t sns_batchbuff_blk_uimg_start_addr;
SNS_BATCHBUFF_UIMAGE_DATA STATIC sns_batchbuff_blk_s sns_batchbuff_header_uimg[SNS_BATCHBUFF_BLK_MAX_UIMG];
SNS_BATCHBUFF_UIMAGE_DATA STATIC uint16_t sns_batchbuff_blk_usage_uimg[SNS_BATCHBUFF_CLIENT_CNT]; 


SNS_BATCHBUFF_UIMAGE_DATA STATIC sns_batchbuff_q_s sns_batchbuff_blk_bimg_free_q;
SNS_BATCHBUFF_UIMAGE_DATA STATIC sns_batchbuff_q_s sns_batchbuff_blk_bimg_used_q;
STATIC sns_batchbuff_threshold_cb_s sns_batchbuff_bimg_threshold_cb[SNS_BATCHBUFF_CLIENT_CNT][SNS_BATCHBUFF_MAX_CB_CNT];
SNS_BATCHBUFF_UIMAGE_DATA STATIC intptr_t sns_batchbuff_blk_bimg_start_addr;
STATIC sns_batchbuff_blk_s sns_batchbuff_header_bimg[SNS_BATCHBUFF_BLK_MAX_BIMG];
SNS_BATCHBUFF_UIMAGE_DATA STATIC uint16_t sns_batchbuff_blk_usage_bimg[SNS_BATCHBUFF_CLIENT_CNT];



/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
 
//#define SNS_BATCHBUFF_DEBUG
#ifdef SNS_BATCHBUFF_DEBUG
#define SNS_BATCHBUFF_PRINTF0(xx_fmt)                            SNS_PRINTF_STRING_LOW_0(SNS_DBG_MOD_DSPS_BATCHBUFF, xx_fmt)
#define SNS_BATCHBUFF_PRINTF1(xx_fmt, xx_arg1)                   SNS_PRINTF_STRING_LOW_1(SNS_DBG_MOD_DSPS_BATCHBUFF, xx_fmt, xx_arg1)
#define SNS_BATCHBUFF_PRINTF2(xx_fmt, xx_arg1, xx_arg2)          SNS_PRINTF_STRING_LOW_2(SNS_DBG_MOD_DSPS_BATCHBUFF, xx_fmt, xx_arg1, xx_arg2)
#define SNS_BATCHBUFF_PRINTF3(xx_fmt, xx_arg1, xx_arg2, xx_arg3) SNS_PRINTF_STRING_LOW_3(SNS_DBG_MOD_DSPS_BATCHBUFF, xx_fmt, xx_arg1, xx_arg2, xx_arg3)
#else
#define SNS_BATCHBUFF_PRINTF0(xx_fmt)
#define SNS_BATCHBUFF_PRINTF1(xx_fmt, xx_arg1)
#define SNS_BATCHBUFF_PRINTF2(xx_fmt, xx_arg1, xx_arg2)
#define SNS_BATCHBUFF_PRINTF3(xx_fmt, xx_arg1, xx_arg2, xx_arg3)
#endif //SNS_BATCHBUFF_DEBUG

SNS_BATCHBUFF_UIMAGE_CODE
void sns_batchbuff_print(void)
{
#ifdef SNS_BATCHBUFF_DEBUG
  uint16_t i=0;
  sns_batchbuff_blk_s* blk_ptr = NULL;

  sns_os_mutex_pend(sns_batchbuff_blk_uimg_used_q.mutex_ptr, 0,NULL);
  blk_ptr = sns_q_check(&sns_batchbuff_blk_uimg_used_q.protected_q);
  for(i=0;i<sns_q_cnt(&sns_batchbuff_blk_uimg_used_q.protected_q);i++)
  {
    SNS_BATCHBUFF_PRINTF2( "uimg used addr %u %x", i, blk_ptr->ptr);
    blk_ptr = sns_q_next(&sns_batchbuff_blk_uimg_used_q.protected_q, &blk_ptr->q_link);
  }
  sns_os_mutex_post(sns_batchbuff_blk_uimg_used_q.mutex_ptr);

  sns_os_mutex_pend(sns_batchbuff_blk_uimg_free_q.mutex_ptr, 0,NULL);
  blk_ptr = sns_q_check(&sns_batchbuff_blk_uimg_free_q.protected_q);
  for(i=0;i<sns_q_cnt(&sns_batchbuff_blk_uimg_free_q.protected_q);i++)
  {
    SNS_BATCHBUFF_PRINTF2( "uimg free addr %u %x", i, blk_ptr->ptr);
    blk_ptr = sns_q_next(&sns_batchbuff_blk_uimg_free_q.protected_q, &blk_ptr->q_link);
  }
  sns_os_mutex_post(sns_batchbuff_blk_uimg_free_q.mutex_ptr);
#endif
}



/*=========================================================================
  FUNCTION:  sns_batchbuff_init
  =======================================================================*/
sns_err_code_e
sns_batchbuff_init(void)
{
  uint8_t err;
  int i;
  intptr_t batch_buffer =  (intptr_t)NULL;
  sns_batchbuff_blk_s* blk_ptr = NULL;

  if ( sns_batchbuff_blk_uimg_free_q.mutex_ptr != NULL )
  {
    SNS_BATCHBUFF_PRINTF0("batch buffer already initialized"); 
    return SNS_ERR_FAILED;
  }
  SNS_BATCHBUFF_PRINTF0( "batch buffer initialize"); 
  sns_q_init(&sns_batchbuff_blk_uimg_free_q.protected_q);
  sns_batchbuff_blk_uimg_free_q.mutex_ptr = 
    sns_os_mutex_create_uimg(SNS_BATCHBUFF_QUE_MUTEX, &err);
  SNS_ASSERT(err == OS_ERR_NONE);

  sns_q_init(&sns_batchbuff_blk_uimg_used_q.protected_q);
  sns_batchbuff_blk_uimg_used_q.mutex_ptr = 
    sns_os_mutex_create_uimg(SNS_BATCHBUFF_QUE_MUTEX, &err);
  SNS_ASSERT(err == OS_ERR_NONE);

  sns_q_init(&sns_batchbuff_blk_bimg_free_q.protected_q);
  sns_batchbuff_blk_bimg_free_q.mutex_ptr = 
    sns_os_mutex_create_uimg(SNS_BATCHBUFF_QUE_MUTEX, &err);
  SNS_ASSERT(err == OS_ERR_NONE);

  sns_q_init(&sns_batchbuff_blk_bimg_used_q.protected_q);
  sns_batchbuff_blk_bimg_used_q.mutex_ptr = 
    sns_os_mutex_create_uimg(SNS_BATCHBUFF_QUE_MUTEX, &err);
  SNS_ASSERT(err == OS_ERR_NONE);
  
  batch_buffer = (intptr_t)SNS_OS_U_MALLOC(SNS_DBG_MOD_DSPS_BATCHBUFF, 
     SNS_BATCHBUFF_BLK_MAX_UIMG*SNS_BATCHBUFF_MAX_BLK_SIZE);
  SNS_BATCHBUFF_PRINTF1( "batchbuff uimg blk allocate %x", batch_buffer);
  SNS_ASSERT(batch_buffer != (intptr_t)NULL);
  sns_batchbuff_blk_uimg_start_addr = batch_buffer;

  for ( i=0; i<SNS_BATCHBUFF_BLK_MAX_UIMG;i++ )
  {
     blk_ptr = &sns_batchbuff_header_uimg[i];
     sns_q_link(blk_ptr, &blk_ptr->q_link);
     blk_ptr->ptr = (intptr_t)(batch_buffer + i*SNS_BATCHBUFF_MAX_BLK_SIZE);
     sns_os_mutex_pend(sns_batchbuff_blk_uimg_free_q.mutex_ptr, 0,&err);
     sns_q_put(&sns_batchbuff_blk_uimg_free_q.protected_q, &blk_ptr->q_link);
     sns_os_mutex_post(sns_batchbuff_blk_uimg_free_q.mutex_ptr);
     SNS_BATCHBUFF_PRINTF2( "batchbuff init uimg %u %x", i, blk_ptr->ptr);
  }

  batch_buffer = (intptr_t)NULL;
  batch_buffer = (intptr_t)SNS_OS_SENSOR_HEAP_MALLOC(SNS_DBG_MOD_DSPS_BATCHBUFF, 
    SNS_BATCHBUFF_BLK_MAX_BIMG*SNS_BATCHBUFF_MAX_BLK_SIZE);
  SNS_BATCHBUFF_PRINTF1( "batchbuff bimg blk allocate %x", batch_buffer);
  SNS_ASSERT(batch_buffer != (intptr_t)NULL);
  sns_batchbuff_blk_bimg_start_addr = batch_buffer;

  for ( i=0; i<SNS_BATCHBUFF_BLK_MAX_BIMG;i++ )
  {
     blk_ptr = &sns_batchbuff_header_bimg[i];
     sns_q_link(blk_ptr, &blk_ptr->q_link);
     blk_ptr->ptr = (intptr_t)(batch_buffer + i*SNS_BATCHBUFF_MAX_BLK_SIZE);
     sns_os_mutex_pend(sns_batchbuff_blk_bimg_free_q.mutex_ptr, 0,&err);
     sns_q_put(&sns_batchbuff_blk_bimg_free_q.protected_q, &blk_ptr->q_link);
     sns_os_mutex_post(sns_batchbuff_blk_bimg_free_q.mutex_ptr);
     SNS_BATCHBUFF_PRINTF2( "batchbuff init bimg %u %x", i, blk_ptr->ptr);
  }
  SNS_OS_MEMZERO(&sns_batchbuff_blk_usage_uimg, sizeof(sns_batchbuff_blk_usage_uimg));
  SNS_OS_MEMZERO(&sns_batchbuff_blk_usage_bimg, sizeof(sns_batchbuff_blk_usage_bimg));
  sns_init_done();
  return SNS_SUCCESS;
}

/*=========================================================================
  FUNCTION:  sns_batchbuff_cb_register
  =======================================================================*/
bool sns_batchbuff_cb_register(sns_batchbuff_client_e client,
  sns_batchbuff_mem_type_e mem_type,
  sns_batchbuff_threshold_cb cb_func,
  uint32_t free_blk_cnt)
{
  int i;
  sns_batchbuff_threshold_cb_s *ptr= (mem_type == SNS_BATCHBUFF_UIMG_MEM ? 
    sns_batchbuff_u_threshold_cb[client] : sns_batchbuff_bimg_threshold_cb[client] ); 

  for ( i=0;i<SNS_BATCHBUFF_MAX_CB_CNT;i++ )
  {
    if ( NULL == ptr[i].threshold_cb )
    {
      ptr[i].threshold_cb = cb_func;
      ptr[i].free_blk_cnt = free_blk_cnt;
      return true;
    }
  }
  return false;
}

/*=========================================================================
  FUNCTION:  sns_batchbuff_in_uimg
  =======================================================================*/
SNS_BATCHBUFF_UIMAGE_CODE
bool sns_batchbuff_in_uimg(void* ptr)
{
  if ( ((intptr_t)ptr)>=sns_batchbuff_blk_uimg_start_addr &&
       ((intptr_t)ptr)<sns_batchbuff_blk_uimg_start_addr + 
       SNS_BATCHBUFF_BLK_MAX_UIMG*SNS_BATCHBUFF_MAX_BLK_SIZE)
  {
    return true;
  }
  return false;
}

/*=========================================================================
  FUNCTION:  sns_batchbuff_in_bimg
  =======================================================================*/
SNS_BATCHBUFF_UIMAGE_CODE
bool sns_batchbuff_in_bimg(void* ptr)
{
  if ( ((intptr_t)ptr)>=sns_batchbuff_blk_bimg_start_addr &&
       ((intptr_t)ptr)<sns_batchbuff_blk_bimg_start_addr + 
       SNS_BATCHBUFF_BLK_MAX_BIMG*SNS_BATCHBUFF_MAX_BLK_SIZE)
  {
    return true;
  }
  return false;
}


/*=========================================================================
  FUNCTION:  sns_batchbuff_u_malloc
  =======================================================================*/
SNS_BATCHBUFF_UIMAGE_CODE 
void * sns_batchbuff_malloc(
  sns_batchbuff_client_e client, sns_batchbuff_mem_type_e mem_type)
{
  sns_batchbuff_q_s *free_q=NULL, *used_q=NULL;
  sns_batchbuff_blk_s* blk_ptr = NULL;
  uint16_t *used_cnt_ptr = NULL;
  uint8_t err;
  sns_batchbuff_threshold_cb_s (*cb_ptr)[SNS_BATCHBUFF_MAX_CB_CNT]= 
    (mem_type == SNS_BATCHBUFF_UIMG_MEM ? 
    sns_batchbuff_u_threshold_cb : sns_batchbuff_bimg_threshold_cb ); 
  
  if ( SNS_BATCHBUFF_UIMG_MEM == mem_type)
  {
    free_q = &sns_batchbuff_blk_uimg_free_q;
    used_q = &sns_batchbuff_blk_uimg_used_q;
    used_cnt_ptr = &sns_batchbuff_blk_usage_uimg[client];
  }else
  {
    free_q = &sns_batchbuff_blk_bimg_free_q;
    used_q = &sns_batchbuff_blk_bimg_used_q;
    used_cnt_ptr = &sns_batchbuff_blk_usage_bimg[client];
  }
  sns_os_mutex_pend(free_q->mutex_ptr, 0,&err);
  blk_ptr = sns_q_get(&free_q->protected_q);
  sns_os_mutex_post(free_q->mutex_ptr);
  if ( blk_ptr != NULL )
  {
    int i, j;
    
    sns_os_mutex_pend(used_q->mutex_ptr, 0,&err);
    sns_q_put(&used_q->protected_q, &blk_ptr->q_link);
    (*used_cnt_ptr) += 1;
    sns_os_mutex_post(used_q->mutex_ptr);
    
    SNS_OS_MEMZERO((void *)blk_ptr->ptr,SNS_BATCHBUFF_MAX_BLK_SIZE);  

    SNS_BATCHBUFF_PRINTF3("batchbuff malloc type: %u, %u available %x",
                          mem_type,
                          sns_q_cnt(&free_q->protected_q),
                          blk_ptr->ptr);
    for ( i=0; i<SNS_BATCHBUFF_CLIENT_CNT; i++ )
    {
      for ( j=0; j<SNS_BATCHBUFF_MAX_CB_CNT; j++)
      {
        if ( NULL != cb_ptr[i][j].threshold_cb &&
             cb_ptr[i][j].free_blk_cnt == sns_q_cnt(&free_q->protected_q) )
        {
          bool isIsland = SNS_OS_IS_ISLAND_FUNC(cb_ptr[i][j].threshold_cb);
  
          if ( isIsland )
          {
            cb_ptr[i][j].threshold_cb(mem_type, cb_ptr[i][j].free_blk_cnt);
          }
          else
          {
            sns_utils_place_uimge_hold( SNS_UIMG_BB );
            cb_ptr[i][j].threshold_cb(mem_type, cb_ptr[i][j].free_blk_cnt);
            sns_utils_remove_uimage_hold( SNS_UIMG_BB );
          }
        }
      }
    }

    return (void *)blk_ptr->ptr;
  }
  
  SNS_BATCHBUFF_PRINTF1("batchbuff malloc mem depleted mode: %u",
                          mem_type);
  return NULL;
}


SNS_BATCHBUFF_UIMAGE_CODE
void sns_batchbuff_free(sns_batchbuff_client_e client, void* ptr)
{
  sns_batchbuff_blk_s* blk_ptr = NULL;
  sns_batchbuff_q_s* free_q=NULL;
  sns_batchbuff_q_s* used_q = NULL;
  uint8_t err;
  uint16_t *used_cnt_ptr = NULL;
  
  if ( sns_batchbuff_in_uimg(ptr) )
  {
    free_q = &sns_batchbuff_blk_uimg_free_q;
    used_q = &sns_batchbuff_blk_uimg_used_q;
    used_cnt_ptr = &sns_batchbuff_blk_usage_uimg[client];
  }
  else if ( sns_batchbuff_in_bimg(ptr) )
  {
    free_q = &sns_batchbuff_blk_bimg_free_q;
    used_q = &sns_batchbuff_blk_bimg_used_q;
    used_cnt_ptr = &sns_batchbuff_blk_usage_bimg[client];
  }
  else
  {
    SNS_PRINTF_STRING_ERROR_1(SNS_DBG_MOD_DSPS_BATCHBUFF, 
      "batchbuff free ptr error %x", ptr); 
    ASSERT(0);
  }
  sns_batchbuff_print();
  sns_os_mutex_pend(used_q->mutex_ptr, 0,&err);
  blk_ptr = sns_q_check(&used_q->protected_q);
  //sns_os_mutex_post(used_q->mutex_ptr);

  while ( blk_ptr != NULL )
  {
    if ( (intptr_t)ptr == blk_ptr->ptr )
    {
      sns_q_delete(&blk_ptr->q_link);
      sns_os_mutex_pend(free_q->mutex_ptr, 0,&err);
      sns_q_put(&free_q->protected_q, &blk_ptr->q_link);
      (*used_cnt_ptr) -= 1;
      sns_os_mutex_post(free_q->mutex_ptr);
      sns_os_mutex_post(used_q->mutex_ptr);
      return;
    }
    //sns_os_mutex_pend(used_q->mutex_ptr, 0,&err);
    blk_ptr = sns_q_next(&used_q->protected_q,&blk_ptr->q_link);
    //sns_os_mutex_post(used_q->mutex_ptr);
  }
  SNS_PRINTF_STRING_ERROR_1(SNS_DBG_MOD_DSPS_BATCHBUFF, 
    "batchbuff free ptr not found %x", ptr); 
  ASSERT(0);
}

SNS_BATCHBUFF_UIMAGE_CODE
uint16_t sns_batchbuff_blocksize(void)
{
  return SNS_BATCHBUFF_MAX_BLK_SIZE;
}

SNS_BATCHBUFF_UIMAGE_CODE
uint16_t sns_batchbuff_available_block_cnt(sns_batchbuff_mem_type_e mem_type)
{
  sns_batchbuff_q_s* free_q=NULL;
  uint16_t cnt=0;
  uint8_t err;
  
  if ( SNS_BATCHBUFF_UIMG_MEM == mem_type)
  {
    free_q = &sns_batchbuff_blk_uimg_free_q;
  }else
  {
    free_q = &sns_batchbuff_blk_bimg_free_q;
  }
  sns_os_mutex_pend(free_q->mutex_ptr, 0,&err);
  cnt = sns_q_cnt(&free_q->protected_q);
  sns_os_mutex_post(free_q->mutex_ptr);
  return cnt;
}

