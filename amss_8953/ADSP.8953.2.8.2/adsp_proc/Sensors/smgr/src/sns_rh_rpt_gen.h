#ifndef SNS_RH_RPT_GEN_H
#define SNS_RH_RPT_GEN_H
/*============================================================================

  @file sns_rh_rpt_gen.h

  @brief
  This file contains definitions for the Report Generation functions
  of the Request Handler

  Copyright (c) 2014-2015 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.

============================================================================*/
/* $Header: //components/rel/ssc.adsp/2.6/smgr/src/sns_rh_rpt_gen.h#5 $ */
/* $DateTime: 2015/06/25 22:38:38 $*/
/* $Author: pwbldsvc $ */

/*============================================================================
  EDIT HISTORY FOR FILE

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-06-15  pn    Updated sns_rh_rpt_gen_save_indication() signature
  2015-05-27  bd    SMGR flow control feature
  2014-04-23  pn    Initial version
============================================================================*/
#include "sns_rh.h"

/*===========================================================================

                   SMGR MACRO

===========================================================================*/

/*===========================================================================

                   EXTERNAL DEFINITIONS AND TYPES

===========================================================================*/

/*===========================================================================

                          FUNCTIONS

===========================================================================*/
extern bool sns_rh_rpt_gen_send_query_indication(
  sns_rh_rpt_spec_s* rpt_ptr);

extern bool sns_rh_rpt_gen_send_empty_query_indication(
  const sns_rh_query_s* query_ptr);

extern bool sns_rh_rpt_gen_generate_periodic_report(
  sns_rh_rpt_spec_s* rpt_spec_ptr);

extern bool sns_rh_rpt_gen_send_buffering_indications(
  sns_rh_rpt_spec_s* rpt_spec_ptr);

extern bool sns_rh_rpt_gen_send_report_indication(
   sns_rh_rpt_spec_s* rpt_spec_ptr,
   void*              ind_msg_ptr,
   uint16_t           msg_id, 
   uint16_t           len,
   bool               free_me);
extern void sns_rh_rpt_gen_save_indication(
   sns_rh_rpt_spec_s* rpt_spec_ptr,
   void*              ind_msg_ptr,
   uint16_t           len);

extern void sns_rh_rpt_gen_send_saved_ind( 
  sns_rh_rpt_spec_s* rpt_spec_ptr);

extern void sns_rh_process_conn_resume(void);

#endif /* SNS_RH_RPT_GEN_H */

