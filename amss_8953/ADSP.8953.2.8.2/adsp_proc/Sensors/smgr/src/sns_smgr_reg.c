/*=============================================================================
  @file sns_smgr_reg.c

  This file contains the logic for handling registry in Sensor Manager

*******************************************************************************
*   Copyright (c) 2014-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
*   Qualcomm Technologies Proprietary and Confidential.
*
********************************************************************************/

/* $Header: //components/rel/ssc.adsp/2.6/smgr/src/sns_smgr_reg.c#30 $ */
/* $DateTime: 2016/02/16 21:47:11 $ */
/* $Author: pwbldsvc $ */

/*============================================================================
  EDIT HISTORY FOR FILE

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-12-24  dq   Enable GPIO configuration with registry
  2015-06-25  yh   Added MMC3530
  2015-06-17  AH   Support to request SNS_REG_GROUP_SSI_SMGR_CFG_3
  2015-04-13  MW   Added LSM6DS3, HTS221, LIS3MDL
  2015-04-09  pn   Added support for 56-bit timestamp
  2015-01-15  SH   Added DAF Playback Accel Driver
  2015-01-14  MW   Added APDS9960,BMP280
  2014-12-10  MW   Reshuffled smgr_sensor_fn_ptr_map for HD22
  2014-21-14  MW   Added AKM099xx FIFO
  2014-10-30  MW   Added BMI160
  2014-10-14  MW   Added call to sns_hw_pnoc_vote() before probing
  2014-10-08  MW   Added ZPA2326
  2014-09-03  jms  Added SPI support
  2014-09-03  MW   Use sns_em_convert_usec_to_dspstick() for idle_to_ready and
                   off_to_idle conversion
  2014-08-26  MW   Fixed autodetect - enable then restore power rail and I2C
                   clk before and after probe is called
  2014-08-18  pn   Removed KW error
  2014-07-31  pn   Obsoleted FEATURE_TEST_DRI
  2014-07-14  VY   Fixed compiler warnings
  2014-06-25  MW   Added ISL29033
  2014-05-28  MW   Added HSPPAD038A
  2014-05-15  sc   Added LPS25H
  2014-04-23  pn   Initial version

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "dlfcn.h"
#include "sns_osa.h"
#include "sns_memmgr.h"
#include "sns_smgr_main.h"
#include "sns_smgr_hw.h"
#include "sns_reg_api_v02.h"
#include "sns_dd.h"
#include "sns_reg_common.h"
#include "sns_smgr_reg.h"
#include "sns_smgr_mr.h"
#include "sns_smgr_util.h"
#include "sns_smgr_ddf_priv.h"
#include "sns_dl.h"
#include "sns_dl_dbg.h"
#include "stringl.h"

extern sns_hw_gpio_config sns_smgr_hw_ssc_gpio_config[SNS_REG_DDF_GPIO_CONFIG_MAX];

/*----------------------------------------------------------------------------
 * Structure Definitions
 * -------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define SMGR_SENSOR_FN_PTR_MAP_TBLE_SIZE sizeof(smgr_sensor_fn_ptr_map)/sizeof(smgr_sensor_fn_ptr_map[0])

#define SNS_SMGR_SSI_GET_FIRST_DEVINFO_ID()         ( smgr_ssi_devinfo[0] )
#define SNS_SMGR_SSI_DEVINFO_IDX_TO_CFG_IDX(d_idx)  ( (d_idx)/SNS_REG_SSI_SMGR_CFG_NUM_SENSORS )
#define SNS_SMGR_SSI_IS_LAST_DEVINFO(id)    ( smgr_ssi_devinfo[ARR_SIZE(smgr_ssi_devinfo)-1] == (id) )

/*----------------------------------------------------------------------------
 * Structure Definitions
 * -------------------------------------------------------------------------*/

/** 
  The code here hard codes 16 as the UUID size and is used everywhere 
  This is a sanity check to ensure that SNS_DRIVER_UUID_SZ is consistent.
*/
#if SNS_DRIVER_UUID_SZ != 16
  #error Inconsistent UUID size specified
#endif

typedef struct
{
  uint8_t drvuuid[16];
  sns_ddf_driver_if_s * fun_ptr;
}smgr_sensor_config_fn_ptr_map_s;

typedef struct
{
  uint16 last_requested_sensor_dep_reg_group_id;
} sns_smgr_resp_cb_data_s;

typedef enum 
{
  SSC_DRV_STATIC = 0,   /**< Type to indicate a static driver */
  SSC_DRV_DYNAMIC = 1,  /**< Type to indicate a dynamically loaded driver */
} smgr_sensor_drv_t;

/**
  Handle to describe a driver loaded in the DDF, this is used for both
  statically loaded and dynamically loaded drivers.
*/
typedef void*                   smgr_sensor_drv_handle_t;
/**
  Static initializer for the smgr_sensor_drv_handle_t
*/
#define INVALID_DRV_HANDLE_T    (NULL)
/**
  Helper macro to determine if a handle is valid or not
*/
#define IS_VALID_DRV_HANDLE(h)  ((INVALID_DRV_HANDLE_T == (h))?false:true)

/**
  Macro defining the size of the error string when dynamically loading
  a SNS driver
*/
#define SNS_DDF_DL_ERROR_STR_SIZE  32

/**
  Type define describing the characteristics of a dynamic shared object based
  SNS driver that is to be loaded and initialized into SNS DDF.
*/
typedef struct
{
  /**< Data type to link this structure in a queue */
  sns_q_link_s  q_link;
  /**< Driver UUID */
  uint8_t       drvuuid[SNS_DRIVER_UUID_SZ];
  /**< File path of the driver for both static and dyn.
       For static drivers a default name should be used. */
  char          drv_full_file_path[SNS_DSO_FILEPATH_SZ];
  /**< Load symbol for a dynamically loaded SNS driver */
  char          entry_symbol[SNS_DSO_LOAD_SYM_NAME_SZ];
  /**< Error string containing reason of failure if any when dynamically 
       loading a SNS driver */
  char          load_error[SNS_DDF_DL_ERROR_STR_SIZE];
  /**< Reference count that keeps track of how many times a UUID
    is loaded and unloaded. Used in initialization and clean up. */
  int           ref_cnt;
  /**< Handle associated with the loaded driver that DDF uses */
  smgr_sensor_drv_handle_t drv_handle;
} smgr_sensor_drv_info_s;

/**
  Type define describing the characteristics of a driver loaded into DDF.
*/
typedef struct
{
  /**< Data type to link this structure in a queue */
  sns_q_link_s            q_link;
  /**< Data type indicating whether this is a dynamic or static DDF driver */
  smgr_sensor_drv_t       type;
  /**< Pointer to the drivers VTBL */
  sns_ddf_driver_if_s     *fun_ptr;
  /**< File handle of the driver. For static DDF driver this should be NULL */
  void                    *file_handle;
  /**< Reference to the info object that contains characteristics
    of this driver */
  smgr_sensor_drv_info_s  *p_drv_info_ref;
} smgr_sensor_drv_handle_obj_t;

/* How long to wait for for the DL service before determining that it is
 * unattainable.  In microseconds. */
#define SNS_DDF_DL_TIMEOUT_US     100000

/* 
    How long to wait in microseconds for for the DL service to return
  if a driver by the given UUID was found in the DL registry. If 0 wait
  until we hear back from the search function.
 */
#define SNS_DDF_DL_SRCH_TIMEOUT   0

/*----------------------------------------------------------------------------
 *  Variables
 * -------------------------------------------------------------------------*/
/**
  Global variable to hold a list of DDF loaded drivers both dynamic and static
*/
static sns_q_s sns_drv_infoQ;

/**
  This is a default "file path" for statically loaded drivers.
*/
static char drv_fpath_static[SNS_DSO_FILEPATH_SZ] = "STATIC";

/**
  Global err code to indicating status regarding SMGRs use of DL services
*/
static sns_err_code_e sns_ddf_dl_err_code = SNS_ERR_UNKNOWN;

/**
  Global handle for the DL service
*/
static sns_dl_svc_handle_t sns_ddf_dl_handle = SNS_DL_HANDLE_INITIALIZER;

/**
  Count of the number of dynamic drivers loaded
*/
static int num_dynamic_drivers_loaded = 0;

static uint8_t null_uuid[SNS_DRIVER_UUID_SZ] = {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0};

/* the retry counts for a REG request for DD initialization */
static const smgr_sensor_config_fn_ptr_map_s smgr_sensor_fn_ptr_map[] = {

#ifdef CONFIG_SUPPORT_LSM303D
 { SNS_REG_UUID_LSM303D, &sns_dd_lsm303d_if},
#endif
#ifdef CONFIG_SUPPORT_LPS25H
	{ SNS_REG_UUID_LPS25H, &sns_dd_press_lps25h_if},	
#endif
#ifdef CONFIG_SUPPORT_L3GD20
  { SNS_REG_UUID_L3GD20, &sns_dd_gyr_if},
#endif
#ifdef CONFIG_SUPPORT_LSM6DS3
	{ SNS_REG_UUID_LSM6DS3, &sns_dd_lsm6ds3_if},	
#endif
#ifdef CONFIG_SUPPORT_HTS221
	{ SNS_REG_UUID_HTS221, &sns_dd_hts221_if},	
#endif
#ifdef CONFIG_SUPPORT_LIS3MDL
	{ SNS_REG_UUID_LIS3MDL, &sns_dd_lis3mdl_if},	
#endif
#ifndef SENSORS_DD_DEV_FLAG
#ifdef CONFIG_SUPPORT_MPU6050
  { SNS_REG_UUID_MPU6050, &sns_dd_mpu6xxx_if },
#endif
#ifdef CONFIG_SUPPORT_QDSP_SIM_PLAYBACK
  { SNS_REG_UUID_QDSP_SIM_PLAYBACK, &sns_dd_qdsp_playback_if },
#endif
#ifdef CONFIG_SUPPORT_DAF_PLAYBACK_ACCEL
  { SNS_REG_UUID_DAF_PLAYBACK_ACCEL, &sns_dd_daf_playback_accel_if },
#endif
#ifdef CONFIG_SUPPORT_MPU6500
  { SNS_REG_UUID_MPU6500, &sns_dd_mpu6xxx_if },
#endif
#ifdef CONFIG_SUPPORT_MPU6515
  { SNS_REG_UUID_MPU6515 , &sns_dd_mpu6515_if },
  { SNS_REG_UUID_MPU6515_AKM8963 , &sns_dd_mpu6515_if },
  { SNS_REG_UUID_MPU6515_BMP280 , &sns_dd_mpu6515_if },
#endif
#ifdef CONFIG_SUPPORT_ADXL
  { SNS_REG_UUID_ADXL350, &sns_accel_adxl350_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_LIS3DH
  { SNS_REG_UUID_LIS3DH, &sns_dd_acc_lis3dh_if},
#endif
#ifdef CONFIG_SUPPORT_LIS3DSH
  { SNS_REG_UUID_LIS3DSH, &sns_dd_acc_lis3dsh_if},
#endif
#ifdef CONFIG_SUPPORT_BMA150
  { SNS_REG_UUID_BMA150, &sns_accel_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_BMA250
  { SNS_REG_UUID_BMA250, &sns_accel_bma250_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_BMG160
  { SNS_REG_UUID_BMG160,&sns_bmg160_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_MPU3050
  { SNS_REG_UUID_MPU3050, &sns_gyro_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_L3G4200D
  { SNS_REG_UUID_L3G4200D, &sns_dd_gyr_l3g4200d_if},
#endif
#ifdef CONFIG_SUPPORT_AKM8963
  { SNS_REG_UUID_AKM8963, &sns_mag_akm8963_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_AKM8975
  { SNS_REG_UUID_AKM8975, &sns_mag_akm8975_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_AKM8973
  { SNS_REG_UUID_AKM8973, 0},
#endif
#ifdef CONFIG_SUPPORT_AMI306
  { SNS_REG_UUID_AMI306, &sns_mag_ami306_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_BMP085
  { SNS_REG_UUID_BMP085, &sns_alt_bmp085_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_BMP180
  { SNS_REG_UUID_BMP180, &sns_alt_bmp085_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_LPS331AP
  { SNS_REG_UUID_LPS331AP, &sns_dd_press_lps331ap_if},
#endif
#ifdef CONFIG_SUPPORT_APDS99XX
  { SNS_REG_UUID_APDS99XX, &sns_dd_apds99xx_driver_if},
#endif
#ifdef CONFIG_SUPPORT_ISL29028
  { SNS_REG_UUID_ISL29028, &sns_alsprx_isl29028_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_ISL29147
  { SNS_REG_UUID_ISL29147, &sns_alsprx_isl29147_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_ISL29011
  { SNS_REG_UUID_LPS331AP, &sns_alsprx_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_LSM303DLHC
  { SNS_REG_UUID_LSM303DLHC, &sns_dd_mag_lsm303dlhc_if},
#endif
#ifdef CONFIG_SUPPORT_YAS530
  { SNS_REG_UUID_YAS530, &sns_mag_yas_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_MAX44009
  { SNS_REG_UUID_MAX44009, &sns_als_max44009_driver_if},
#endif
#ifdef CONFIG_SUPPORT_BMA2X2
  { SNS_REG_UUID_BMA2X2, &sns_accel_bma2x2_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_LIS3DH
  { SNS_REG_UUID_LIS3DH, &sns_dd_acc_lis3dh_if},
#endif
#ifdef CONFIG_SUPPORT_BMA150
  { SNS_REG_UUID_BMA150, &sns_accel_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_MPU3050
  { SNS_REG_UUID_MPU3050, &sns_gyro_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_HSCD008
  { SNS_REG_UUID_HSCD008, &sns_dd_mag_hscdtd_if},
#endif
#ifdef CONFIG_SUPPORT_AKM8975
  { SNS_REG_UUID_AKM8975, &sns_mag_akm8975_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_APDS99XX
  { SNS_REG_UUID_APDS99XX, &sns_dd_apds99xx_driver_if},
#endif
#ifdef CONFIG_SUPPORT_TMD277X
  { SNS_REG_UUID_TMD277X, &sns_alsprx_tmd277x_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_LTR55X
  { SNS_REG_UUID_LTR55X, &sns_alsprx_ltr55x_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_AL3320B
	{ SNS_REG_UUID_AL3320B, &sns_alsprx_al3320b_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_ISL29028
  { SNS_REG_UUID_ISL29028, &sns_alsprx_isl29028_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_SHTC1
  { SNS_REG_UUID_SHTC1, &sns_rht_shtc1_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_APDS9950
  { SNS_REG_UUID_APDS9950, &sns_dd_apds9950_driver_if},
#endif
#ifdef CONFIG_SUPPORT_MAX88120
  { SNS_REG_UUID_MAX88120, &sns_ges_max88120_driver_if},
#endif
#ifdef CONFIG_SUPPORT_TMG399X
  { SNS_REG_UUID_TMG399X, &sns_ams_tmg399x_alsprx_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_KXCJK
  { SNS_REG_UUID_KXCJK, &sns_accel_kxcjk_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_MMA8452
  { SNS_REG_UUID_MMA8452, &sns_dd_acc_mma8452_if},
#endif
#ifdef CONFIG_SUPPORT_AP3216C
  { SNS_REG_UUID_AP3216C, &sns_alsprx_ap3216c_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_CM36283
	{ SNS_REG_UUID_CM36283, &sns_alsprx_cm36283_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_ISL29044A
	{ SNS_REG_UUID_ISL29044A, &sns_alsprx_isl29044a_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_AKM09912
  {SNS_REG_UUID_AKM09912, &sns_mag_akm09912_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_BH1721
  { SNS_REG_UUID_BH1721, &sns_als_bh1721_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_M34160PJ
	{ SNS_REG_UUID_M34160PJ, &sns_mmc3xxx_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_KXTIK
  { SNS_REG_UUID_KXTIK, &sns_accel_kxtik_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_AD7146
  { SNS_REG_UUID_ADI7146, &sns_dd_sar_ad7146_fn_list},
#endif
#ifdef CONFIG_SUPPORT_MAX44006
 { SNS_REG_UUID_MAX44006, &sns_als_rgb_max44006_driver_if},
#endif
#ifdef CONFIG_SUPPORT_BU52061NVX
 { SNS_REG_UUID_BU52061NVX, &sns_hall_bu52061_driver_fn_list},
#endif
#ifdef CONFIG_SUPPORT_MC3410
    { SNS_REG_UUID_MC3410, &sns_dd_acc_mc3410_if},
#endif	
#ifdef CONFIG_SUPPORT_AKM09911
	{ SNS_REG_UUID_AKM09911, &sns_mag_akm_driver_fn_list},	
#endif
#ifdef CONFIG_SUPPORT_HSPPAD038A
	{ SNS_REG_UUID_HSPPAD038A, &sns_dd_prs_hsppad_if},	
#endif
#ifdef CONFIG_SUPPORT_ISL29033
	{ SNS_REG_UUID_ISL29033, &sns_als_isl29033_driver_fn_list},	
#endif
#ifdef CONFIG_SUPPORT_ZPA2326
	{ SNS_REG_UUID_ZPA2326, &sns_dd_press_zpa2326_if},	
#endif
#ifdef CONFIG_SUPPORT_BMI160
	{ SNS_REG_UUID_BMI160, &sns_dd_fn_list_bmi160},	
#endif
#ifdef CONFIG_SUPPORT_AKM099xx_FIFO
	{ SNS_REG_UUID_AKM099xx_FIFO, &sns_dd_mag_akm09914_akm09915_fn_list},	
#endif
#ifdef CONFIG_SUPPORT_APDS9960
	{ SNS_REG_UUID_APDS9960, &sns_dd_apds9960_driver_if},	
#endif
#ifdef CONFIG_SUPPORT_BMP280
	{ SNS_REG_UUID_BMP280, &sns_alt_bmp280_driver_fn_list},	
#endif
#ifdef CONFIG_SUPPORT_MMC3530
	{ SNS_REG_UUID_MMC3530, &sns_mmc3530_driver_fn_list},	
#endif
#ifdef CONFIG_SUPPORT_LIS2HH
       { SNS_REG_UUID_LIS2HH, &sns_dd_acc_lis2hh_if},
#endif
#ifdef CONFIG_SUPPORT_LIS2MDL
       { SNS_REG_UUID_LIS2MDL, &sns_dd_lis2mdl_if},
#endif
/*[FEATURE]-MODIFY-BEGIN by TCTNB.ZXZ ,2588460,2016/07/26,  link UUID and fn list interface for akm09916/rpr0521 */
#ifdef CONFIG_SUPPORT_RPR0521
	{ SNS_REG_UUID_RPR0521, &sns_dd_rpr0521_driver_fn_list},
#endif

#ifdef CONFIG_SUPPORT_AKM09916
	{ SNS_REG_UUID_AKM09916, &sns_mag_akm09916_driver_fn_list},
#endif
/*[FEATURE]-MODIFY-END by TCTNB.ZXZ*/

/*[FEATURE]-MODIFY-BEGIN by TCTNB.ZXZ ,2855626,2016/09/08,  link UUID and fn list interface for tmd2725 */
#ifdef CONFIG_SUPPORT_TMD2725
	{ SNS_REG_UUID_TMD2725, &sns_tmd2725_driver_fn_list},
#endif
/*[FEATURE]-MODIFY-END by TCTNB.ZXZ*/

#endif // SENSORS_DD_DEV_FLAG

/* Generic configs for vendor driver development*/
#ifdef CONFIG_SUPPORT_VENDOR_1
  { SNS_REG_UUID_VENDOR_1, &sns_dd_vendor_if_1},
#endif
#ifdef CONFIG_SUPPORT_VENDOR_2
  { SNS_REG_UUID_VENDOR_2, &sns_dd_vendor_if_2},
#endif
};

/* SSI: SMGR groups */
static const uint16_t smgr_ssi_cfg[] = {
  SNS_REG_GROUP_SSI_SMGR_CFG_V02,
  SNS_REG_GROUP_SSI_SMGR_CFG_2_V02,
  SNS_REG_GROUP_SSI_SMGR_CFG_3_V02
};

/* SSI: Device Info (auto-detect) */
static const uint16_t smgr_ssi_devinfo[] = {
  SNS_REG_GROUP_SSI_DEVINFO_ACCEL_V02,
  SNS_REG_GROUP_SSI_DEVINFO_GYRO_V02,
  SNS_REG_GROUP_SSI_DEVINFO_MAG_V02,
  SNS_REG_GROUP_SSI_DEVINFO_PROX_LIGHT_V02,
  SNS_REG_GROUP_SSI_DEVINFO_PRESSURE_V02,
  SNS_REG_GROUP_SSI_DEVINFO_TAP_V02,
  SNS_REG_GROUP_SSI_DEVINFO_HUMIDITY_V02,
  SNS_REG_GROUP_SSI_DEVINFO_RGB_V02,
  SNS_REG_GROUP_SSI_DEVINFO_SAR_V02,
  SNS_REG_GROUP_SSI_DEVINFO_HALL_EFFECT_V02
};

static uint8_t smgr_sensor_cfg_cnt = 0;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

extern smgr_sensor_drv_handle_t sns_smgr_create_if_handle(const uint8_t * uuid);

/**
  @brief Helper API to get the DDF VTBL from a driver handle.
*/
STATIC sns_ddf_driver_if_s* sns_ddf_get_driver_vtbl_ptr
(
  smgr_sensor_drv_handle_t handle
)
{
  return ((smgr_sensor_drv_handle_obj_t*)(handle))->fun_ptr;
}

/**
  Function to determine if DL svc is available and fully initialized
  @ return true: Is available and fully initialized.
           false: Unavailable. 
*/
STATIC
bool sns_ddf_is_dynamic_loading_enabled(void)
{
  bool ret_val;
  if (SNS_DL_HANDLE_INITIALIZER == sns_ddf_dl_handle)
  {
    ret_val = false;
  }
  else
  {
    ret_val = true;
  }
  return ret_val;
}

/**
  Function to print relevant debug information regarding DDF's use of DL SVC.
  @ return None
*/
void sns_ddf_print_dynamic_info(void)
{
  bool is_enabled = sns_ddf_is_dynamic_loading_enabled();
  if (true == is_enabled)
    sns_dl_svc_print_state(sns_ddf_dl_handle);
  MSG_3(MSG_SSID_SNS, DBG_ERROR_PRIO,
          "DDF is DL Enabled:%d, Err:%#x Num Dyn Loaded Drivers:%d",
          is_enabled, sns_ddf_dl_err_code, num_dynamic_drivers_loaded);
}

/**
  Function to pass into the DL SVC. Since we will load/unload drivers only when
  running in big image, the allocator provided will allocate from big img heap.
*/
void* sns_ddf_dl_malloc(size_t num_bytes)
{
  return SNS_OS_MALLOC( SNS_DBG_MOD_DSPS_SMGR, num_bytes );
}

/**
  Function to pass into the DL SVC. Since we will load/unload drivers only when
  running in big image, the deallocator provided will free from big img heap.
*/
void sns_ddf_dl_free(void* p)
{
  SNS_OS_FREE(p);
}

/**
  @brief Function to initialize required data structures and a handle
  to the DL service.
  
  @note: It is expected that this is called once on init during SMGR init.
*/
sns_err_code_e sns_smgr_dl_init(void)
{
  static char sns_ddf_dl_name[] = "SNS_DDF";
  static bool is_drv_initialized = false;
  sns_err_code_e err_code, ret_val = SNS_SUCCESS;
  sns_dl_attr_t attr;
  
  if (false == is_drv_initialized)
  {
    is_drv_initialized = true;
    sns_q_init(&sns_drv_infoQ);
    err_code = sns_dl_attr_init(&attr);
    if (SNS_SUCCESS == err_code)
    {
      sns_dl_set_attr_name(&attr, sns_ddf_dl_name, sizeof(sns_ddf_dl_name));
      sns_dl_set_attr_allocator(&attr, sns_ddf_dl_malloc, sns_ddf_dl_free);
      sns_dl_set_attr_blocking_info(&attr, true,
                                      SNS_DDF_DL_TIMEOUT_US, NULL, NULL);
      /*  enable this if you want to test on platforms where DL 
          registry is unsupported. */
      //sns_dl_attr_use_local_registry(&attr);
      err_code = sns_dl_svc_create(&sns_ddf_dl_handle, &attr);
      /** we should return error codes only on no memory type failures
          as this is an optional feature at this time.
      */
      if (SNS_ERR_NOMEM == err_code)
      {
        ret_val = SNS_ERR_NOMEM;
      }
      sns_dl_attr_deinit(&attr);
    }
    sns_ddf_dl_err_code = err_code;
  }
  return ret_val;
}

/**
  @brief Function to de-initialize required data structures and a handle
  to the DL service.
*/
void sns_smgr_dl_deinit(void)
{
  if (sns_ddf_is_dynamic_loading_enabled()) {
    sns_ddf_dl_err_code = sns_dl_svc_destroy(sns_ddf_dl_handle);
    sns_ddf_dl_handle = SNS_DL_HANDLE_INITIALIZER;
  }
}

/**
  @brief Linear search CB function to compare the UUID in the list of 
  registered device drivers.

  @return 1 Driver UUID found, 0 otherwise.
*/
SMGR_STATIC int sns_cfg_cmp_uuid(void* p_item, void* p_compare_val)
{
  smgr_sensor_drv_info_s *p_drv_info = (smgr_sensor_drv_info_s*)p_item;
  uint8_t *uuid = (uint8_t*)p_compare_val;
  int found = 0;
  
  if(SNS_OS_MEMCMP(uuid, p_drv_info->drvuuid, SNS_DRIVER_UUID_SZ) == 0)
  {
    found = 1;
  }

  return found;
}

/**
  @brief Linear search CB function to compare the VTBL pointer in the 
  of driver info queue.

  @return 1 Driver VTBL found, 0 otherwise.
*/
SMGR_STATIC int sns_cfg_cmp_drv_fun(void* p_item, void* p_compare_val)
{
  smgr_sensor_drv_info_s *p_drv_info = (smgr_sensor_drv_info_s*)p_item;
  sns_ddf_driver_if_s *fn_ptr = (sns_ddf_driver_if_s*)p_compare_val;
  sns_ddf_driver_if_s *fn_ptr_cmp;
  int found = 0;
  
  fn_ptr_cmp = sns_ddf_get_driver_vtbl_ptr(p_drv_info->drv_handle);
  if (fn_ptr_cmp == fn_ptr)
  {
    found = 1;
  }
  return found;
}

/**
  @brief Create a new Driver Info object and enqueue on the 
  info queue
  
  @param[in] uuid: Valid pointer to the UUID
  @param[in] drv_full_file_path: Valid pointer to the driver file path.
  
  @return NULL Could not create the object
          Non NULL, valid pointer to the newly created object.
*/
SMGR_STATIC smgr_sensor_drv_info_s* create_drv_info_entry
(
  const uint8_t * uuid,
  const char *drv_full_file_path
)
{
  smgr_sensor_drv_info_s *p_drv_info;
  p_drv_info = SNS_OS_ANY_MALLOC(SNS_DBG_MOD_DSPS_SMGR, 
                                    sizeof(smgr_sensor_drv_info_s));
  if (NULL != p_drv_info)
  {
    SNS_OS_MEMZERO(p_drv_info, sizeof(smgr_sensor_drv_info_s));
    SNS_OS_MEMSCPY(p_drv_info->drvuuid, SNS_DRIVER_UUID_SZ,
                    uuid, SNS_DRIVER_UUID_SZ);
    SNS_OS_MEMSCPY(p_drv_info->drv_full_file_path, SNS_DSO_FILEPATH_SZ,
                    drv_full_file_path, SNS_DSO_FILEPATH_SZ);
    p_drv_info->ref_cnt = 0;
    p_drv_info->drv_handle = INVALID_DRV_HANDLE_T;
    sns_q_link(p_drv_info, (sns_q_link_s*)p_drv_info);
    sns_q_put(&sns_drv_infoQ, (sns_q_link_s*)p_drv_info);
  }

  return p_drv_info;
}

/**
  @brief Destroy a new Driver Info object and dequeue from the 
  info queue
  
  @param[in] p_drv_info: Valid pointer to the driver info object.  
*/
SMGR_STATIC void destroy_drv_info_entry(smgr_sensor_drv_info_s *p_drv_info)
{
  sns_q_link_s *q_drv_info;
  smgr_sensor_drv_info_s *p_drv_info_val;
  if (p_drv_info)
  {
    for ( q_drv_info = sns_q_check(&sns_drv_infoQ); NULL != q_drv_info;
          q_drv_info = sns_q_next(&sns_drv_infoQ, q_drv_info) ) {
      p_drv_info_val = (smgr_sensor_drv_info_s*)q_drv_info;
      if (p_drv_info_val == p_drv_info) {
        sns_q_delete(&(p_drv_info->q_link));
        SNS_OS_ANY_FREE(p_drv_info);
        break;
      }
    }
  }
}

/**
  @brief API to search the info queue by UUID.
  
  @param[in] uuid: Valid pointer to the UUID
  
  @return NULL Could not find the object
          Non NULL, valid pointer to a driver info object.
*/
SMGR_STATIC
smgr_sensor_drv_info_s * sns_smgr_get_drv_info_uuid(const uint8_t * uuid)
{
  smgr_sensor_drv_info_s *p_drv_info;
  p_drv_info = (smgr_sensor_drv_info_s*)sns_q_linear_search(&sns_drv_infoQ,
                                                            sns_cfg_cmp_uuid,
                                                            (void*)uuid);
  return p_drv_info;
}

/**
  @brief API to search the info queue by driver VTBL
  
  @param[in] fn_ptr: Valid pointer to the VTBL
  
  @return NULL Could not find the object
          Non NULL, valid pointer to a driver info object.
*/
SMGR_STATIC smgr_sensor_drv_info_s*
sns_smgr_get_drv_info_drv_fun(const sns_ddf_driver_if_s *fn_ptr)
{
  smgr_sensor_drv_info_s *p_drv_info;
  p_drv_info = (smgr_sensor_drv_info_s*)sns_q_linear_search(&sns_drv_infoQ,
                                                            sns_cfg_cmp_drv_fun,
                                                            (void*)fn_ptr);
  return p_drv_info;
}

/**
  @brief API to destroy a DDF registered driver by a handle.
  
  @param[in] handle: Valid handle to the dynamic/static driver.
*/
SMGR_STATIC void sns_smgr_destroy_if_handle(smgr_sensor_drv_handle_t handle)
{
  smgr_sensor_drv_handle_obj_t *p_drv;
  smgr_sensor_drv_info_s *p_drv_info;

  if (IS_VALID_DRV_HANDLE(handle))
  {
    p_drv = (smgr_sensor_drv_handle_obj_t*)(handle);
    p_drv_info = p_drv->p_drv_info_ref;
    if (p_drv_info->ref_cnt > 0)
    {
      p_drv_info->ref_cnt--;
      /* no more outstanding requests on this handle so delete */
      if (0 == p_drv_info->ref_cnt)
      {
        if (SSC_DRV_DYNAMIC == p_drv->type)
        {
          dlclose(p_drv->file_handle);
          num_dynamic_drivers_loaded--;
        }
        SNS_OS_ANY_FREE(p_drv);
        destroy_drv_info_entry(p_drv_info);
      }
    }
  }
}

/**
  @brief API to validate if the VTBL information pertaining to a dynamically
         loaded driver is valid and compliant with the SNS DDF FWK supported
         on this platform.

  @param[in]  p_vtbl_info --  Valid pointer to the VTBL info structure that was
                              obtained from the dynamically loaded driver
  @param[out] err_str     --  String buffer to contain the reason for
                              non compliance
  @param[in]  err_str_size -- Size of the err_str buffer

  @return
    true - VTBL info is valid and compliant with the DDF
    false - otherwise
*/
STATIC bool sns_ddf_validate_vtbl_info
(
  sns_ddf_driver_vtbl_info  *p_vtbl_info,
  char                      *err_str,
  size_t                    err_str_size
)
{
  bool ret_val = false;
  if (p_vtbl_info)
  {
    if (SNS_DDF_DRIVER_VTBL_REVISION != p_vtbl_info->vtbl_revision)
    {
      strlcpy(err_str, "UNSUPPORTED VTBL REVISION", err_str_size);
    }
    else
    {
      if (sizeof(sns_ddf_driver_if_s) != p_vtbl_info->vtbl_size)
      {
        strlcpy(err_str, "SNS DDF VTBL SIZE MISMATCH", err_str_size);
      }
      else
      {
        ret_val = true;
      }
    }
  }
  else
  {
    strlcpy(err_str, "NULL SNS DDF VTBL INFO", err_str_size);
  }

  return ret_val;
}

/**
  @brief API to load a dynamic or static version of a driver given its UUID.
  
  @param[in] uuid: Valid pointer to the UUID
  
  @note: In this implementation we first determine if there is a config
  present in the registry indicating that there is a dynamic version of a
  driver present. The search 'key' in the registry is UUID.
  If there exits such a driver and it has a valid file path and symbol to
  obtain the VTBL the driver is loaded and it VTBL registered with the DDF.
  For static drivers, the operation is as before where the UUID is used to
  search the smgr_sensor_fn_ptr_map table to obtain the driver's VTBL.

  @return Handle to the driver if successful else a invalid handle.
*/
smgr_sensor_drv_handle_t sns_smgr_create_if_handle(const uint8_t * uuid)
{
  uint8_t i, found = 0, new_instance = 0;
  smgr_sensor_drv_handle_obj_t *p_drv = NULL;
  smgr_sensor_drv_handle_t drv_handle;
  smgr_sensor_drv_info_s *p_drv_info = NULL;
  char *full_file_path;
  char *dlerr_str;
  int cmp;
  char err_str[SNS_DDF_DL_ERROR_STR_SIZE];
  char *entry_sym = NULL;
  sns_err_code_e err_code;
  sns_ddf_driver_get_vtbl_info pfn;
  sns_ddf_driver_vtbl_info *p_vtbl_info;
  int len;
  sns_reg_dyn_cfg_t *p_cfg = NULL;
  bool is_valid_vtbl;
  void *file_handle = 0, *load_sym = 0;
  
  cmp = SNS_OS_MEMCMP(uuid, null_uuid, SNS_DRIVER_UUID_SZ);
  if (0 == cmp)
  {
    goto bail;
  }
  /* check by UUID if an instance of the driver was already loaded */
  p_drv_info = sns_smgr_get_drv_info_uuid(uuid);
  if (NULL != p_drv_info)
  {
    /* there exists a driver, check its validity and incr ref count */
    if (IS_VALID_DRV_HANDLE(p_drv_info->drv_handle))
    {
      p_drv = (smgr_sensor_drv_handle_obj_t*)(p_drv_info->drv_handle);
      p_drv_info->ref_cnt++;
      found = 1;
    }
  }
  else
  {
    /* new UUID driver instance requested */
    new_instance = 1;
    /* create and init a driver obj */
    p_drv = SNS_OS_ANY_MALLOC(SNS_DBG_MOD_DSPS_SMGR,
                              sizeof(smgr_sensor_drv_handle_obj_t));
    if (NULL == p_drv)
    {
      goto bail;
    }
    p_drv->fun_ptr = NULL;
    p_drv->file_handle = NULL;
    SNS_OS_MEMZERO(err_str, SNS_DDF_DL_ERROR_STR_SIZE);
    /** 
      create if dyn loading is available else proceed to consult the
      smgr_sensor_fn_ptr_map  table
    */
    if (sns_ddf_is_dynamic_loading_enabled())
    {
      do
      {
        /* create a DL reg obj to be used by the DL service to find a driver */
        p_cfg = SNS_OS_ANY_MALLOC(SNS_DBG_MOD_DSPS_SMGR,
                                    sizeof(sns_reg_dyn_cfg_t));
        if (p_cfg)
        {
          /* search the registry by UUID */
          SNS_OS_MEMSET(p_cfg, 0, sizeof(sns_reg_dyn_cfg_t));
          err_code = sns_dl_reg_search_uuid(sns_ddf_dl_handle,
                                              (void*)uuid,
                                              SNS_DRIVER_UUID_SZ,
                                              p_cfg,
                                              SNS_DDF_DL_SRCH_TIMEOUT);
          if (SNS_SUCCESS != err_code)
          {
            /* not found exit the loop and try static config */
            strlcpy(err_str, "UUID Not Found in DL Reg",
                    SNS_DDF_DL_ERROR_STR_SIZE);
            break;
          }
          /* sanity UUID check */
          cmp = SNS_OS_MEMCMP(uuid, p_cfg->uuid, SNS_DRIVER_UUID_SZ);
          if ( 0 != cmp )
          {
            strlcpy(err_str, "UUID Not Matched", SNS_DDF_DL_ERROR_STR_SIZE);
            break;
          }
          else
          {
            /* check if the path is valid */
            if ((len = strlen(p_cfg->full_file_path)) > 0)
            {
              full_file_path = p_cfg->full_file_path;
              /* dynamically load it */
              file_handle = dlopen(full_file_path, RTLD_NOW);
              MSG_1(MSG_SSID_SNS, DBG_HIGH_PRIO, "DDF dlopen returned %x",
                    file_handle);
              if (0 == file_handle)
              {
                dlerr_str = dlerror();
                if (NULL == dlerr_str)
                {
                  dlerr_str = "UNKNOWN DL ERROR"; 
                }
                strlcpy(err_str, dlerr_str, SNS_DDF_DL_ERROR_STR_SIZE);
                MSG(MSG_SSID_SNS, DBG_HIGH_PRIO, "DDF dlopen failed");
                break;
              }
              /* check if the registry contained a entry sym to lookup */
              if ((len = strlen(p_cfg->entry_symbol)) > 0)
              {
                load_sym = dlsym(file_handle, p_cfg->entry_symbol);
                entry_sym = p_cfg->entry_symbol;
              }
              if (0 == load_sym)
              {
                strlcpy(err_str, "SNS DDF dlsym failed",
                          SNS_DDF_DL_ERROR_STR_SIZE);
                MSG(MSG_SSID_SNS, DBG_HIGH_PRIO, "SNS DDF dlsym failed");
                dlclose(file_handle);
                break;
              }
              /** 
                if the returned symbol is valid and it returns a valid 
                pointer to the VTBL we are successful else we unload/close the 
                DSO and proceed to try the static route.
              */
              pfn = (sns_ddf_driver_get_vtbl_info)load_sym;
              p_vtbl_info = pfn();
              is_valid_vtbl = sns_ddf_validate_vtbl_info(p_vtbl_info, err_str,
                                                    SNS_DDF_DL_ERROR_STR_SIZE);
              if (false == is_valid_vtbl)
              {
                dlclose(file_handle);
                break;
              }
              p_drv->fun_ptr = p_vtbl_info->vtbl;
              strlcpy(err_str, "Success", SNS_DDF_DL_ERROR_STR_SIZE);
              num_dynamic_drivers_loaded++;
              p_drv->file_handle = file_handle;
              p_drv->type = SSC_DRV_DYNAMIC;
              found = 1;
              break;
            }
          }
        }
      } while(0);
    }
    if (0 == found)
    {
      /* no dynamic versions of the UUID found consult the static table */
      for (i=0; i<SMGR_SENSOR_FN_PTR_MAP_TBLE_SIZE; i++)
      {
        if (SNS_OS_MEMCMP(uuid, smgr_sensor_fn_ptr_map[i].drvuuid, SNS_DRIVER_UUID_SZ)==0)
        {
          p_drv->type = SSC_DRV_STATIC;
          full_file_path = drv_fpath_static;
          p_drv->fun_ptr = smgr_sensor_fn_ptr_map[i].fun_ptr;
          found = 1;
          break;
        }
      }
    }
    
    /* create the driver info object representing this driver */
    if (1 == found)
    {
      p_drv_info = create_drv_info_entry(uuid, full_file_path);
      if (NULL == p_drv_info)
      {
        /* no memory to create the info object */
        found = 0;
      }
      else
      {
        /* ref cnt after successful create is 0 therefore incr by 1 */
        p_drv_info->drv_handle = (smgr_sensor_drv_handle_t)(p_drv);
        p_drv_info->ref_cnt++;
        strlcpy(p_drv_info->load_error, err_str, SNS_DDF_DL_ERROR_STR_SIZE);
        if (0 != entry_sym)
        {
          strlcpy(p_drv_info->entry_symbol, entry_sym,SNS_DSO_LOAD_SYM_NAME_SZ);
        }
        p_drv->p_drv_info_ref = p_drv_info;
      }
    }
  }

bail:
  if (0 == found)
  {
    if (1 == new_instance)
    {
      if (p_drv_info)
      {
        destroy_drv_info_entry(p_drv_info);
      }
      if (p_drv)
      {
        SNS_OS_ANY_FREE(p_drv);
      }
    }
    drv_handle = INVALID_DRV_HANDLE_T;
  }
  else
  {
    drv_handle = (smgr_sensor_drv_handle_t)(p_drv);
  }
  if (p_cfg)
  {
    SNS_OS_ANY_FREE(p_cfg);
  }
  return drv_handle;
}

/**
  @brief API to uninstall a driver by UUID. This is useful to unload
  dynamically loaded drivers in case there are errors. This is to be 
  used for both static and dynamic drivers.
*/
void sns_smgr_uninstall_driver(const uint8_t *uuid)
{
  smgr_sensor_drv_info_s *p_drv_info;
  p_drv_info = sns_smgr_get_drv_info_uuid(uuid);
  if (NULL != p_drv_info)
  {
    sns_smgr_destroy_if_handle(p_drv_info->drv_handle);
  }
}

/**
  @brief API to iterate through all loaded drivers. This can be used
  for debug purposes for example.
*/
void sns_ddf_for_each_driver
(
  pfn_sns_dl_get_info pfn
)
{
  sns_q_link_s *q_drv_info;
  smgr_sensor_drv_info_s *p_drv_info;
  sns_dl_info_t ddf_info;
  smgr_sensor_drv_handle_obj_t *p_drv;
  if (pfn)
  {
    for ( q_drv_info = sns_q_check(&sns_drv_infoQ); NULL != q_drv_info;
          q_drv_info = sns_q_next(&sns_drv_infoQ, q_drv_info) ) {
      p_drv_info = (smgr_sensor_drv_info_s*)q_drv_info;
      SNS_OS_MEMZERO(&ddf_info, sizeof(sns_dl_info_t));
      ddf_info.uuid           = p_drv_info->drvuuid;
      ddf_info.entry_symbol   = p_drv_info->entry_symbol;
      ddf_info.full_file_path = p_drv_info->drv_full_file_path;
      ddf_info.err_str        = p_drv_info->load_error;
      p_drv = (smgr_sensor_drv_handle_obj_t*)(p_drv_info->drv_handle);
      ddf_info.file_handle    = p_drv->file_handle;
      pfn(&ddf_info);
    }
  }
}

/*===========================================================================

  FUNCTION:   sns_smgr_uuid_to_fn_ptr

===========================================================================*/
/*!
  @brief find driver function pointer from UUID.

  @detail
  @param
   UUId - driver UUID

  @return
    driver function pointer
 */
/*=========================================================================*/
sns_ddf_driver_if_s * sns_smgr_uuid_to_fn_ptr(const uint8_t * uuid)
{
  sns_ddf_driver_if_s *p_drv_fun_ptr = NULL;
  smgr_sensor_drv_info_s *p_drv_info;
  p_drv_info = sns_smgr_get_drv_info_uuid(uuid);
  if (NULL != p_drv_info)
  {
    p_drv_fun_ptr = sns_ddf_get_driver_vtbl_ptr(p_drv_info->drv_handle);
  }
  return p_drv_fun_ptr;
}

/*===========================================================================

  FUNCTION:   sns_smgr_fn_ptr_to_uuid

===========================================================================*/
/*!
  @brief find driver function pointer from UUID.

  @detail
  @param
   fn_ptr - Pointer to driver function interface

  @return
    Pointer to static const UUID
 */
/*=========================================================================*/
SMGR_STATIC
const uint8_t * sns_smgr_fn_ptr_to_uuid(const sns_ddf_driver_if_s *fn_ptr)
{
  smgr_sensor_drv_info_s *p_drv_info;
  if (NULL != fn_ptr)
  {
    p_drv_info = sns_smgr_get_drv_info_drv_fun(fn_ptr);
    if (NULL != p_drv_info)
    {
      return p_drv_info->drvuuid;
    }
  }
  return NULL ;
}

/*===========================================================================

  FUNCTION:   smgr_is_valid_fac_cal

===========================================================================*/
/*!
  @brief check if factory calibration data is within the valid range

  @detail treat the value invalid when all biases values are 0
  @param
  @return
 */
/*=========================================================================*/
SMGR_STATIC bool smgr_is_valid_fac_cal(q16_t *cal_data_ptr)
{
  /* biases data validity */
  if ((0==cal_data_ptr[0]) && (0==cal_data_ptr[1]) &&  (0==cal_data_ptr[2]))
  {
    return false;
  }
  else
  {
    return true;
  }
}

/*===========================================================================

  FUNCTION:   sns_smgr_ssi_get_cfg_idx

===========================================================================*/
/*!
  @brief  Checks if 'id' is a valid SSI CFG ID

  @param  id  : CFG ID
  @return   -1  : if 'id' does not exist in smgr_ssi_cfg[]
          >= 0  : 'id' exists, and value corresponds to the column index.
 */
/*=========================================================================*/
int sns_smgr_ssi_get_cfg_idx(uint16_t id)
{
  int i, rv = -1;

  for(i = 0; i < ARR_SIZE(smgr_ssi_cfg); i++)
  {
    if(smgr_ssi_cfg[i] == id)
    {
      rv = i;
      break;
    }
  }

  return rv;
}

/*===========================================================================

  FUNCTION:   sns_smgr_ssi_is_last_cfg

===========================================================================*/
/*!
  @brief  Checks if id is the last SMGR CFG Id expected.

  @param  id  : SMGR CFG ID
  @return 'true' if so, 'false' otherwise
 */
/*=========================================================================*/
bool sns_smgr_ssi_is_last_cfg(uint16_t id)
{
  return ( smgr_ssi_cfg[ARR_SIZE(smgr_ssi_cfg)-1] == (id) );
}

/*===========================================================================

  FUNCTION:   sns_smgr_ssi_get_cfg_id

===========================================================================*/
/*!
  @brief  Returns the 'idx'-th SMGR CFG Id in smgr_ssi_cfg table.

  @param  idx  : index
  @return   >= 0 : a valid Id
              -1 : otherwise
 */
/*=========================================================================*/
int32_t sns_smgr_ssi_get_cfg_id(uint8_t idx)
{
  return ( (idx < ARR_SIZE(smgr_ssi_cfg)) ? (int32_t)smgr_ssi_cfg[idx] : -1 );
}

/*===========================================================================

  FUNCTION:   sns_smgr_ssi_get_devinfo_idx

===========================================================================*/
/*!
  @brief  Checks if 'id' is a valid SSI device info ID

  @param  id  : device info ID

  @return   -1  : if 'id' does not exist in smgr_ssi_devinfo[]
          >= 0  : 'id' exists, and value corresponds to the column index
 */
/*=========================================================================*/
SMGR_STATIC int sns_smgr_ssi_get_devinfo_idx(uint16_t id)
{
  int i, rv = -1;

  for(i = 0; i < ARR_SIZE(smgr_ssi_devinfo); i++)
  {
    if(smgr_ssi_devinfo[i] == id)
    {
      rv = i;
      break;
    }
  }

  return rv;
}

/*===========================================================================

  FUNCTION:   sns_smgr_ssi_get_next_devinfo_id

===========================================================================*/
/*!
  @brief  Gets the next DEVINFO group ID

  @param  curr_id  : the current DEVINFO ID
  @return   -1  : if 'id' is the last ID, or 'id' does not exist.
          >= 0  : the next CFG group ID
 */
/*=========================================================================*/
SMGR_STATIC int32_t sns_smgr_ssi_get_next_devinfo_id(uint16_t curr_id)
{
  int32_t next_id = -1;
  int idx = sns_smgr_ssi_get_devinfo_idx(curr_id);

  if(!SNS_SMGR_SSI_IS_LAST_DEVINFO(curr_id) && (idx >= 0) &&
     ((idx+1) < ARR_SIZE(smgr_ssi_devinfo)))
  {
    next_id = (int32_t) smgr_ssi_devinfo[idx+1];
  }

  return next_id;
}

/*===========================================================================

  FUNCTION:   sns_smgr_populate_cfg_from_devinfo

===========================================================================*/
/*!
  @brief Fills in a smgr_sensor_cfg_s based on registry device info

  @detail
  @param
   Id - Registry DEVINFO group ID.
   sensor_cfg_ptr - pointer to entry in smgr_sensor_cfg
   devinfo - pointer to sns_reg_ssi_devinfo_group_s
   Ix - index into devinfo
   num_sensors - number of sensors in the sensors_list
   sensors_list - Array of sensors supported by this device
   device_select - device_select value returned by probe function
  @return
   none
 */
/*=========================================================================*/
SMGR_STATIC void sns_smgr_populate_cfg_from_devinfo ( 
  uint16_t                           Id,
  smgr_sensor_cfg_s*                 sensor_cfg_ptr,
  const sns_reg_ssi_devinfo_group_s* devinfo,
  uint8_t                            Ix,
  uint32_t                           num_sensors,
  const sns_ddf_sensor_e*            sensors_list,
  uint8_t                            device_select,
  sns_ddf_driver_if_s                *drv_fn_ptr)
{
  int i;
  sensor_cfg_ptr->drv_fn_ptr = drv_fn_ptr;
  SNS_OS_MEMCOPY(sensor_cfg_ptr->uuid, devinfo->uuid_cfg[Ix].drvuuid, 
                 sizeof(sensor_cfg_ptr->uuid));
  sensor_cfg_ptr->off_to_idle_time =
    (uint16_t)TIMETICK_SCLK_FROM_US(devinfo->uuid_cfg[Ix].off_to_idle);
  sensor_cfg_ptr->idle_to_ready_time =
    (uint16_t)TIMETICK_SCLK_FROM_US(devinfo->uuid_cfg[Ix].idle_to_ready);
  sensor_cfg_ptr->bus_instance = devinfo->uuid_cfg[Ix].i2c_bus;
  sensor_cfg_ptr->device_select = device_select;
  if(  devinfo->uuid_cfg[Ix].reg_group_id == 0xFFFF ) {
    sensor_cfg_ptr->driver_reg_type = SNS_SMGR_REG_ITEM_TYPE_NONE;
    sensor_cfg_ptr->driver_reg_id = 0;
  }
  else
  {
    sensor_cfg_ptr->driver_reg_type = SNS_SMGR_REG_ITEM_TYPE_GROUP;
    sensor_cfg_ptr->driver_reg_id = devinfo->uuid_cfg[Ix].reg_group_id;
  }
  if( devinfo->uuid_cfg[Ix].cal_pri_group_id == 0xFFFF ) {
    sensor_cfg_ptr->primary_cal_reg_type = SNS_SMGR_REG_ITEM_TYPE_NONE;
    sensor_cfg_ptr->primary_cal_reg_id = 0;
  } else {
    sensor_cfg_ptr->primary_cal_reg_type = SNS_SMGR_REG_ITEM_TYPE_GROUP;
    sensor_cfg_ptr->primary_cal_reg_id = devinfo->uuid_cfg[Ix].cal_pri_group_id;
  }
  sensor_cfg_ptr->first_gpio = devinfo->uuid_cfg[Ix].gpio1;
  sensor_cfg_ptr->second_gpio = -1;
  sensor_cfg_ptr->bus_slave_addr = devinfo->uuid_cfg[Ix].i2c_address;
  sensor_cfg_ptr->sensitivity_default = devinfo->uuid_cfg[Ix].sensitivity_default;
  sensor_cfg_ptr->flags = devinfo->uuid_cfg[Ix].flags;

  sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_NONE;
  sensor_cfg_ptr->data_types[1] = SNS_REG_SSI_DATA_TYPE_NONE;
  sensor_cfg_ptr->range_sensor = 0;
  switch( Id ) {
    case SNS_REG_GROUP_SSI_DEVINFO_ACCEL_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_ACCEL_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] == (int)SNS_REG_SSI_DATA_TYPE_ACCEL ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_ACCEL;
        }
        if( (int)sensors_list[i] == (int)SNS_REG_SSI_DATA_TYPE_TEMP) {
          sensor_cfg_ptr->data_types[1] = SNS_REG_SSI_DATA_TYPE_TEMP;
        }
      }
      break;
    case SNS_REG_GROUP_SSI_DEVINFO_GYRO_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_GYRO_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] == (int)SNS_REG_SSI_DATA_TYPE_GYRO ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_GYRO;
        }
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_TEMP ) {
          sensor_cfg_ptr->data_types[1] = SNS_REG_SSI_DATA_TYPE_TEMP;
        }
      }
      break;
    case SNS_REG_GROUP_SSI_DEVINFO_MAG_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_MAG_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_MAG ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_MAG;
        }
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_TEMP ) {
          sensor_cfg_ptr->data_types[1] = SNS_REG_SSI_DATA_TYPE_TEMP;
        }
      }
      break;
    case SNS_REG_GROUP_SSI_DEVINFO_PROX_LIGHT_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_PROX_LIGHT_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_PROXIMITY ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_PROXIMITY;
        }
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_AMBIENT ) {
          sensor_cfg_ptr->range_sensor = 1;
          sensor_cfg_ptr->data_types[1] = SNS_REG_SSI_DATA_TYPE_AMBIENT;
        }
      }
      SNS_SMGR_PRINTF1(HIGH, "range_sensor=%d", sensor_cfg_ptr->range_sensor);
      break;
    case SNS_REG_GROUP_SSI_DEVINFO_PRESSURE_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_PRESSURE_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_PRESSURE ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_PRESSURE;
        }
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_TEMP ) {
          sensor_cfg_ptr->data_types[1] = SNS_REG_SSI_DATA_TYPE_TEMP;
        }
      }
      break;
    case SNS_REG_GROUP_SSI_DEVINFO_IR_GESTURE_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_IR_GESTURE_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_IR_GESTURE ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_IR_GESTURE;
        }
      }
      break;
    case SNS_REG_GROUP_SSI_DEVINFO_TAP_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_TAP_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_DOUBLETAP ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_DOUBLETAP;
        }
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_SINGLETAP ) {
          sensor_cfg_ptr->data_types[1] = SNS_REG_SSI_DATA_TYPE_SINGLETAP;
        }
      }
      break;
    case SNS_REG_GROUP_SSI_DEVINFO_HUMIDITY_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_HUMIDITY_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_HUMIDITY ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_HUMIDITY;
        }
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_AMBIENT_TEMP ) {
          sensor_cfg_ptr->data_types[1] = SNS_REG_SSI_DATA_TYPE_AMBIENT_TEMP;
        }
      }
      break;

    case SNS_REG_GROUP_SSI_DEVINFO_RGB_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_RGB_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_RGB ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_RGB;
        }
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_CT_C ) {
          sensor_cfg_ptr->data_types[1] = SNS_REG_SSI_DATA_TYPE_CT_C;
        }
      }
      break;

    case SNS_REG_GROUP_SSI_DEVINFO_SAR_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_SAR_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_SAR ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_SAR;
        }
      }
      break;

    case SNS_REG_GROUP_SSI_DEVINFO_HALL_EFFECT_V02:
      sensor_cfg_ptr->sensor_id = SNS_SMGR_ID_HALL_EFFECT_V01;
      for( i = 0; i < num_sensors; i++ ) {
        if( (int)sensors_list[i] ==  (int)SNS_REG_SSI_DATA_TYPE_HALL_EFFECT ) {
          sensor_cfg_ptr->data_types[0] = SNS_REG_SSI_DATA_TYPE_HALL_EFFECT;
        }
      }
      break;

    default:
      break;
  }

}

/*===========================================================================

  FUNCTION:   sns_smgr_parse_reg_devinfo_resp

===========================================================================*/
/*!
  @brief Process SSI devinfo response for sensor autodetect

  @detail
  @param
   Id - Group ID or single item ID
   devinfo - pointer to sns_reg_ssi_devinfo_group_s
  @return
   none
 */
/*=========================================================================*/
SMGR_STATIC void sns_smgr_parse_reg_devinfo_resp(
  uint16_t                           Id,
  const sns_reg_ssi_devinfo_group_s* devinfo)
{
  int i;
  uint16_t devinfo_idx;

  devinfo_idx = sns_smgr_ssi_get_devinfo_idx(Id);

  if ( devinfo->min_ver_no != 1 ||
       SNS_SMGR_NUM_SENSORS_DEFINED <= devinfo_idx )
  {
    SNS_SMGR_PRINTF2(
      ERROR, "ssi: wrong min_ver_no: %u, devinfo_idx: %u", 
      devinfo->min_ver_no, devinfo_idx);
    return;
  }

  for ( i = 0; i < SNS_REG_SSI_DEVINFO_NUM_CFGS &&
               i < devinfo->num_uuid_dev_info_valid; i++ )
  {
    sns_ddf_driver_if_s    *drv_fn_ptr;
    sns_ddf_device_access_s dev_access;
    uint32_t                num_sensors;
    sns_ddf_sensor_e       *sensor_list;
    sns_ddf_memhandler_s    memhandler;
    sns_ddf_status_e        status;
    smgr_sensor_cfg_s      *sensor_cfg_ptr;
    smgr_sensor_drv_handle_t drv_handle;

    drv_fn_ptr = NULL;
    drv_handle = sns_smgr_create_if_handle(devinfo->uuid_cfg[i].drvuuid);
    if (IS_VALID_DRV_HANDLE(drv_handle))
    {
      drv_fn_ptr = sns_ddf_get_driver_vtbl_ptr(drv_handle);
    if( drv_fn_ptr == NULL )
    {
        SNS_SMGR_PRINTF2(ERROR, "ssi: Invalid Func Ptr for devinfo_idx[i]: %u[%u]", devinfo_idx, i);
        sns_smgr_destroy_if_handle(drv_handle);
        continue;
      }
    }
    else
    {
      SNS_SMGR_PRINTF2(ERROR, "ssi: No UUID for devinfo_idx[i]: %u[%u]", devinfo_idx, i);
      continue;
    }

    if( drv_fn_ptr->probe != NULL )
    {

      sns_reg_ssi_smgr_cfg_group_drvcfg_s drv_cfg_ptr;
      sns_pm_pwr_rail_e  power_state = sns_hw_powerrail_status();
      bool qup_state = sns_hw_qup_clck_status();

      drv_cfg_ptr.i2c_bus = devinfo->uuid_cfg[i].i2c_bus;

      if ( SNS_DDF_COMM_BUS_TYPE_IS_I2C( drv_cfg_ptr.i2c_bus ) )
      {
        /* Populate bus instance in SMGR's HW state */
        sns_get_i2c_bus_instance(&drv_cfg_ptr);
      }

      /* Turn on power rail and I2C clock */
      sns_hw_power_rail_config(SNS_PWR_RAIL_ON_NPM);
      sns_hw_set_qup_clk(true);
      // Vote for pnoc ON here in case there was a new bus added to SMGR HW
      sns_hw_pnoc_vote(true);

      SMGR_DELAY_US(devinfo->uuid_cfg[i].off_to_idle);


      //---------- Init bus configuration for communication interface ----------
      dev_access.device_select   = 0;
      dev_access.port_config.bus = SNS_DDF_BUS_NONE;
      dev_access.first_gpio      = (uint32_t)devinfo->uuid_cfg[i].gpio1;
      dev_access.second_gpio     = 0;

      status = sns_ddf_comm_bus_init_cfg( &dev_access,
                                          devinfo->uuid_cfg[i].i2c_bus,
                                          devinfo->uuid_cfg[i].i2c_address );
      if (  SNS_DDF_SUCCESS != status )
      {
        SNS_SMGR_PRINTF1( HIGH, "ddf probe init bus failed status=%d", status );
        sns_smgr_destroy_if_handle(drv_handle);
        // for future safety deinit drv_fn_ptr
        drv_fn_ptr = NULL;
        continue;
      }
      //------------------------------------------------------------------------

      sns_ddf_memhandler_init( &memhandler );
      SNS_SMGR_PRINTF2(LOW, "ssi: probing devinfo_idx[i]: %u[%u]", devinfo_idx, i);
      SNS_SMGR_PRINTF3(LOW, "ssi: bus_instance:%u gpio1:%u slave_addr:0x%x",
                       devinfo->uuid_cfg[i].i2c_bus, devinfo->uuid_cfg[i].gpio1,
                       devinfo->uuid_cfg[i].i2c_address );

      status = drv_fn_ptr->probe( &dev_access, &memhandler,
                                  &num_sensors, &sensor_list );

      sns_ddf_comm_bus_free_cfg( &dev_access ); //clean mem after bus configuration

      if( status == SNS_DDF_SUCCESS  && num_sensors != 0 )
      {
        SNS_SMGR_PRINTF2(HIGH, "ssi: devinfo_idx[i]: %u[%u] probe success", devinfo_idx, i);
        /* Populate the smgr_sensor_cfg[] array in the same order as the DEVINFO
           entries in the registry */
        sensor_cfg_ptr = &smgr_sensor_cfg[devinfo_idx];
        sns_smgr_populate_cfg_from_devinfo( Id, sensor_cfg_ptr, devinfo, i,
                                            num_sensors, sensor_list,
                                            dev_access.device_select, drv_fn_ptr );
        sns_ddf_memhandler_free( &memhandler );
        /* Only one sensor per type is currently supported, so return here now
           that one has been found */

        /* Restore power rail and I2C clock state */
        sns_hw_power_rail_config(power_state);
        sns_hw_set_qup_clk(qup_state);

        return;
      }
      SNS_SMGR_PRINTF3(HIGH, "ssi: devinfo_idx[i]: %u[%u] probe failed error=%d",
                       devinfo_idx, i, status);
      sns_smgr_destroy_if_handle(drv_handle);
      // for future safety deinit drv_fn_ptr
      drv_fn_ptr = NULL;
      sns_ddf_memhandler_free( &memhandler );

      sns_hw_power_rail_config(power_state);
      sns_hw_set_qup_clk(qup_state);

    } /* end if( drv_fn_ptr->probe != NULL ) block */
    else
    {
      /* Device does not support the probe function. Assume it's connected */
      const uint8_t default_device_select = 0;
      sns_ddf_sensor_e sensor_type[SMGR_MAX_DATA_TYPES_PER_DEVICE];

      SNS_SMGR_PRINTF2(MED, "ssi: devinfo_idx[i]: %u[%u] probe function not supported", devinfo_idx, i);

      sensor_cfg_ptr = &smgr_sensor_cfg[devinfo_idx];
      num_sensors = 1;
      sensor_list = sensor_type;
      switch( Id )
      {
        case SNS_REG_GROUP_SSI_DEVINFO_ACCEL_V02:
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_ACCEL;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_GYRO_V02:
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_GYRO;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_MAG_V02:
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_MAG;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_PROX_LIGHT_V02:
          num_sensors = 2;
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_PROXIMITY;
          sensor_type[1] = SNS_REG_SSI_DATA_TYPE_AMBIENT;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_PRESSURE_V02:
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_PRESSURE;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_IR_GESTURE_V02:
          num_sensors = 1;
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_IR_GESTURE;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_TAP_V02:
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_DOUBLETAP;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_HUMIDITY_V02:
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_HUMIDITY;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_RGB_V02:
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_RGB;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_SAR_V02:
          num_sensors = 1;
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_SAR;
          break;
        case SNS_REG_GROUP_SSI_DEVINFO_HALL_EFFECT_V02:
          num_sensors = 1;
          sensor_type[0] = SNS_REG_SSI_DATA_TYPE_HALL_EFFECT;
          break;

        default:
          num_sensors = 0;
          break;
      }
      if (0 == num_sensors)
      {
        sns_smgr_destroy_if_handle(drv_handle);
        // for future safety deinit drv_fn_ptr
        drv_fn_ptr = NULL;
      }
      else
      {
        sns_smgr_populate_cfg_from_devinfo( Id, sensor_cfg_ptr, devinfo, i,
                                            num_sensors, sensor_list,
                                            default_device_select, drv_fn_ptr );
      }
      /* Only one sensor per type is currently supported, so return here now
         that one has been found */
      return;
    } /* end if( drv_fn_ptr->probe != NULL ) else block*/
  } /* end for */
}

/*===========================================================================

  FUNCTION:   sns_smgr_populate_ssi_cfg_from_smgr_cfg

===========================================================================*/
/*!
  @brief Fills in a sns_reg_ssi_smgr_cfg_group_s from a SMGR configuration

  @detail
  @param
   ssi_cfg_ptr - Pointer to an SSI SMGR configuration
   sensor_cfg_ptr - pointer to entry in smgr_sensor_cfg
  @return
   none
 */
/*=========================================================================*/
SMGR_STATIC void sns_smgr_populate_ssi_cfg_from_smgr_cfg(
  sns_reg_ssi_smgr_cfg_group_s *ssi_cfg_ptr,
  smgr_sensor_cfg_s            *sensors_cfg_ptr,
  int                          cfg_index)
{
  int i, os;
  const uint8_t *uuid;

  SNS_OS_MEMSET(ssi_cfg_ptr,
                0,
                sizeof(sns_reg_ssi_smgr_cfg_group_s));

  ssi_cfg_ptr->maj_ver_no = 1;
  ssi_cfg_ptr->min_ver_no = 1;
  ssi_cfg_ptr->reserved1 = 0;
  ssi_cfg_ptr->reserved2 = 0;
  ssi_cfg_ptr->reserved3 = 0;
  ssi_cfg_ptr->reserved4 = 0;


  os = 0;
  i = cfg_index * SNS_REG_SSI_SMGR_CFG_NUM_SENSORS;

  while(  os < SNS_REG_SSI_SMGR_CFG_NUM_SENSORS &&
          i < SNS_SMGR_NUM_SENSORS_DEFINED)
  {
    sns_reg_ssi_smgr_cfg_group_drvcfg_s *drv_ptr = &ssi_cfg_ptr->drv_cfg[os];

    uuid = sns_smgr_fn_ptr_to_uuid(sensors_cfg_ptr[i].drv_fn_ptr);
    if( uuid != NULL )
    {
      SNS_OS_MEMCOPY(drv_ptr->drvuuid, uuid, 16);
      drv_ptr->off_to_idle =
        sns_em_convert_dspstick_to_usec( sensors_cfg_ptr[i].off_to_idle_time );
      drv_ptr->idle_to_ready =
        sns_em_convert_dspstick_to_usec( sensors_cfg_ptr[i].idle_to_ready_time );
      drv_ptr->i2c_bus = sensors_cfg_ptr[i].bus_instance;
      if( sensors_cfg_ptr[i].driver_reg_type == SNS_SMGR_REG_ITEM_TYPE_NONE ) {
        drv_ptr->reg_group_id = 0xFFFF;
      } else {
        drv_ptr->reg_group_id = sensors_cfg_ptr[i].driver_reg_id;
      }
      if( sensors_cfg_ptr[i].primary_cal_reg_type == SNS_SMGR_REG_ITEM_TYPE_NONE ) {
        drv_ptr->cal_pri_group_id = 0xFFFF;
      } else {
        drv_ptr->cal_pri_group_id = sensors_cfg_ptr[i].primary_cal_reg_id;
      }
      drv_ptr->gpio1 = sensors_cfg_ptr[i].first_gpio;
      drv_ptr->gpio2 = sensors_cfg_ptr[i].second_gpio;
      drv_ptr->sensor_id = sensors_cfg_ptr[i].sensor_id;
      drv_ptr->i2c_address = sensors_cfg_ptr[i].bus_slave_addr;
      drv_ptr->data_type1 = sensors_cfg_ptr[i].data_types[0];
      drv_ptr->data_type2 = sensors_cfg_ptr[i].data_types[1];
      drv_ptr->related_sensor_index = -1;
      drv_ptr->sensitivity_default = sensors_cfg_ptr[i].sensitivity_default;
      drv_ptr->flags = sensors_cfg_ptr[i].flags;
      drv_ptr->device_select = sensors_cfg_ptr[i].device_select;
      drv_ptr->reserved2 = 0;
      drv_ptr->reserved3 = 0;
    }

    os++;
    i++;
  }
}

/*===========================================================================

  FUNCTION:   sns_smgr_process_sensor_dep_reg_data

===========================================================================*/
/*!
  @brief Configures default values for sensor dependent registry items after
   autodetect is complete
  @detail
  @param
   cfg_group_ptr - Pointer to a sns_reg_ssi_sensor_dep_reg_group_s struct
  @return
   none
 */
/*=========================================================================*/
SMGR_STATIC void sns_smgr_process_sensor_dep_reg_data(
  sns_reg_ssi_sensor_dep_reg_group_s * cfg_group_ptr)
{
  uint8_t sensor_type = cfg_group_ptr->sensor_type - 1;
  uint8_t i, j;
  if( sensor_type < SNS_SMGR_NUM_SENSORS_DEFINED )
  {
    // Get autodetected sensor of this type
    const uint8_t * uuid = sns_smgr_fn_ptr_to_uuid(smgr_sensor_cfg[sensor_type].drv_fn_ptr);
    if( uuid != NULL )
    {
      // Find default values for this sensor
      for( i = 0; i < SNS_REG_MAX_SENSORS_WITH_DEP_REG_ITEMS; ++i )
      {
        if( SNS_OS_MEMCMP( cfg_group_ptr->uuid_reg_cfg[i].drvuuid, uuid, 
                           sizeof(cfg_group_ptr->uuid_reg_cfg[i].drvuuid) ) == 0 )
        {
          for( j = 0; j < SNS_REG_MAX_SENSOR_DEP_REG_ITEMS; ++j )
          {
            if( cfg_group_ptr->uuid_reg_cfg[i].reg_items[j].reg_item_id != 0 &&
                cfg_group_ptr->uuid_reg_cfg[i].reg_items[j].size != 0 )
            {
              // Update reg item with new value
              sns_smgr_update_reg_data( 
                cfg_group_ptr->uuid_reg_cfg[i].reg_items[j].reg_item_id,
                SNS_SMGR_REG_ITEM_TYPE_SINGLE,
                cfg_group_ptr->uuid_reg_cfg[i].reg_items[j].size,
                (uint8_t*)cfg_group_ptr->uuid_reg_cfg[i].reg_items[j].value );
            }
          }
        }
      }
    }
  }
}

/*===========================================================================

  FUNCTION:   sns_smgr_process_reg_devinfo

===========================================================================*/
/*!
  @brief Process a retrieved registry devinfo group.

  @detail
  @param
   Type - Group or Single item registry
   Id - Group ID or single item ID
   Length - bytes
   data_ptr - pointer to received data
   sns_resp - response error
  @return
   none
 */
/*=========================================================================*/
SMGR_STATIC void sns_smgr_process_reg_devinfo( 
  sns_smgr_RegItemType_e  item_type,
  uint16_t                item_id,
  uint32_t                item_len, 
  uint8_t*                data_ptr, 
  sns_common_resp_s_v01   sns_resp)
{
  uint8_t         ix;
  sns_err_code_e  err;

  if( item_id == 0 )
  {
    item_id = sns_smgr.last_received_reg_group_id;
  }

  if(sns_resp.sns_err_t == SENSOR1_SUCCESS)
  {
    sns_smgr_parse_reg_devinfo_resp( item_id, (sns_reg_ssi_devinfo_group_s*)data_ptr );
  }
  else
  {
    SNS_SMGR_PRINTF3(HIGH, "ssi: resp_err=%u, id=%u, last_id=%u", 
                     sns_resp.sns_err_t, item_id, sns_smgr.last_received_reg_group_id);
  }

  if( SNS_SMGR_SSI_IS_LAST_DEVINFO(item_id) )
  {
    sns_reg_ssi_smgr_cfg_group_s ssi_cfg;

    SNS_SMGR_PRINTF0(HIGH, "ssi: populating SSI config.");
      
    for( ix = 0; ix < ARR_SIZE(smgr_ssi_cfg); ix++ )
    {
      sns_smgr_populate_ssi_cfg_from_smgr_cfg( &ssi_cfg,
                                               smgr_sensor_cfg,
                                               ix );

      err = sns_smgr_update_reg_data( (uint16_t) sns_smgr_ssi_get_cfg_id(ix),
                                      SNS_SMGR_REG_ITEM_TYPE_GROUP,
                                      sizeof( ssi_cfg ), (uint8_t*)&ssi_cfg );
      SNS_SMGR_PRINTF3(MED, "ssi: populating SSI config. ix:%u cfg_id:%u err:%u", 
                       ix, (unsigned)sns_smgr_ssi_get_cfg_id(ix), (unsigned)err);
      sns_hw_update_ssi_reg_items(sns_smgr_ssi_get_cfg_id(ix), (uint8_t*)&ssi_cfg);
    }
    
    SNS_SMGR_PRINTF0(LOW, "ssi: last devinfo. state set to ALL_INIT_AUTODETECT_DONE");
    sns_smgr.all_init_state = SENSOR_ALL_INIT_AUTODETECT_DONE;
    err = sns_smgr_req_reg_data(SNS_REG_GROUP_SSI_SENSOR_DEP_CFG0_V02, 
                                SNS_SMGR_REG_ITEM_TYPE_GROUP);
    sns_smgr.last_requested_sensor_dep_reg_group_id = 
      SNS_REG_GROUP_SSI_SENSOR_DEP_CFG0_V02;
  }
}

/*===========================================================================

  FUNCTION:   sns_smgr_ssi_get_next_cfg_id

===========================================================================*/
/*!
  @brief  Gets the next CFG group ID

  @param  curr_id  : the current CFG ID
  @return    -1 : if 'id' is the last CFG ID, or 'id' does not exist.
          >= 0  : the next CFG group ID
 */
/*=========================================================================*/
SMGR_STATIC int32_t sns_smgr_ssi_get_next_cfg_id(uint16_t curr_id)
{
  int32_t next_id = -1;
  int idx = sns_smgr_ssi_get_cfg_idx(curr_id);

  if( (idx>=0) && ((idx+1) < ARR_SIZE(smgr_ssi_cfg) ) )
  {
    next_id = (int32_t) smgr_ssi_cfg[idx+1];
  }

  return next_id;
}

/*===========================================================================

  FUNCTION:   sns_smgr_process_reg_ssi_config

===========================================================================*/
/*!
  @brief Process an SSI SMGR Config registry group.

  @detail
  @param
   Type - Group or Single item registry
   Id - Group ID or single item ID
   Length - bytes
   data_ptr - pointer to received data
   sns_resp - response error
  @return
   none
 */
/*=========================================================================*/
SMGR_STATIC void sns_smgr_process_reg_ssi_config( 
  sns_smgr_RegItemType_e item_type, 
  uint16_t               item_id,
  uint32_t               item_len, 
  uint8_t*               data_ptr, 
  sns_common_resp_s_v01  sns_resp)
{
  int                           cfg_idx;
  uint16_t                      curr_id;
  uint32_t                      ix;
  sns_reg_ssi_smgr_cfg_group_s* cfg_group_ptr = (sns_reg_ssi_smgr_cfg_group_s*)data_ptr;
  smgr_sensor_cfg_s           * sensor_cfg_ptr;

  static bool                   valid_cfg = false;

  if( item_id == 0 )
  {
    item_id = sns_smgr.last_received_reg_group_id;
  }

  cfg_idx = sns_smgr_ssi_get_cfg_idx(item_id);

  SNS_SMGR_PRINTF2(HIGH, "ssi: proc ssi id %i cfg %i", item_id, cfg_idx);
  if(sns_resp.sns_err_t != SENSOR1_SUCCESS)
  {
    SNS_SMGR_PRINTF3(LOW, "ssi: resp_err=%u, id=%u, last_id=%u", 
                     sns_resp.sns_err_t, item_id, sns_smgr.last_received_reg_group_id);
    cfg_idx = sns_smgr_ssi_get_cfg_idx(sns_smgr.last_received_reg_group_id);
    if ( sns_smgr_ssi_is_last_cfg(sns_smgr.last_received_reg_group_id) )
    {
      SNS_SMGR_PRINTF0(LOW, "ssi: Setting init state to ALL_INIT_CONFIGURED");
      sns_smgr.all_init_state = SENSOR_ALL_INIT_CONFIGURED;
    }
  }
  else if ( (cfg_group_ptr->maj_ver_no != 1) && !valid_cfg )
  {
    /* Only use the configuration if the major version is 1. 
    Otherwise autodetect sensors */
    SNS_SMGR_PRINTF1(MED, "ssi: proc ssi maj ver no = %i", cfg_group_ptr->maj_ver_no);
    sns_smgr.all_init_state = SENSOR_ALL_INIT_WAITING_AUTODETECT;

    curr_id = SNS_SMGR_SSI_GET_FIRST_DEVINFO_ID();
    (void) sns_smgr_req_reg_data(curr_id, SNS_SMGR_REG_ITEM_TYPE_GROUP);
    do
    {
      curr_id = (uint16_t) sns_smgr_ssi_get_next_devinfo_id(curr_id);
      (void) sns_smgr_req_reg_data(curr_id, SNS_SMGR_REG_ITEM_TYPE_GROUP);
    } while( ! SNS_SMGR_SSI_IS_LAST_DEVINFO(curr_id) );
  }
  else
  {
    sns_hw_update_ssi_reg_items(item_id, data_ptr);

    for ( ix = 0; smgr_sensor_cfg_cnt < SNS_SMGR_NUM_SENSORS_DEFINED &&
                  ix < SNS_REG_SSI_SMGR_CFG_NUM_SENSORS; ix++ )
    {
      smgr_sensor_drv_handle_t drv_handle;
      sensor_cfg_ptr = &smgr_sensor_cfg[smgr_sensor_cfg_cnt];
      smgr_sensor_cfg_cnt++;

      sensor_cfg_ptr->drv_fn_ptr = NULL;
      drv_handle = sns_smgr_create_if_handle(cfg_group_ptr->drv_cfg[ix].drvuuid);
      if (IS_VALID_DRV_HANDLE(drv_handle))
      {
        sensor_cfg_ptr->drv_fn_ptr = sns_ddf_get_driver_vtbl_ptr(drv_handle);
        if( NULL == sensor_cfg_ptr->drv_fn_ptr )
        {
          SNS_SMGR_PRINTF2(ERROR, "ssi: Invalid Func Ptr for smgr_sensor_cfg[i]: %u[%u]", cfg_idx, ix);
          sns_smgr_destroy_if_handle(drv_handle);
        }
      }
      SNS_OS_MEMCOPY(sensor_cfg_ptr->uuid, cfg_group_ptr->drv_cfg[ix].drvuuid, 
                     sizeof(sensor_cfg_ptr->uuid));
      sensor_cfg_ptr->off_to_idle_time = 
        (uint16_t)TIMETICK_SCLK_FROM_US(cfg_group_ptr->drv_cfg[ix].off_to_idle);
      sensor_cfg_ptr->idle_to_ready_time = 
        (uint16_t)TIMETICK_SCLK_FROM_US(cfg_group_ptr->drv_cfg[ix].idle_to_ready);
      sensor_cfg_ptr->bus_instance = cfg_group_ptr->drv_cfg[ix].i2c_bus;
      if(  cfg_group_ptr->drv_cfg[ix].reg_group_id == 0xFFFF ) 
      {
        sensor_cfg_ptr->driver_reg_type = SNS_SMGR_REG_ITEM_TYPE_NONE;
        sensor_cfg_ptr->driver_reg_id = 0;
      }
      else 
      {
        sensor_cfg_ptr->driver_reg_type = SNS_SMGR_REG_ITEM_TYPE_GROUP;
        sensor_cfg_ptr->driver_reg_id = cfg_group_ptr->drv_cfg[ix].reg_group_id;
      }

      if( cfg_group_ptr->drv_cfg[ix].cal_pri_group_id == 0xFFFF )
      {
        sensor_cfg_ptr->primary_cal_reg_type = SNS_SMGR_REG_ITEM_TYPE_NONE;
        sensor_cfg_ptr->primary_cal_reg_id = 0;
      }
      else
      {
        sensor_cfg_ptr->primary_cal_reg_type = SNS_SMGR_REG_ITEM_TYPE_GROUP;
        sensor_cfg_ptr->primary_cal_reg_id = cfg_group_ptr->drv_cfg[ix].cal_pri_group_id;
      }
      sensor_cfg_ptr->first_gpio = cfg_group_ptr->drv_cfg[ix].gpio1;
      sensor_cfg_ptr->second_gpio = cfg_group_ptr->drv_cfg[ix].gpio2;
      sensor_cfg_ptr->sensor_id = cfg_group_ptr->drv_cfg[ix].sensor_id;
      sensor_cfg_ptr->bus_slave_addr = cfg_group_ptr->drv_cfg[ix].i2c_address;
      sensor_cfg_ptr->data_types[0] = cfg_group_ptr->drv_cfg[ix].data_type1;
      sensor_cfg_ptr->data_types[1] = cfg_group_ptr->drv_cfg[ix].data_type2;
      sensor_cfg_ptr->sensitivity_default = cfg_group_ptr->drv_cfg[ix].sensitivity_default;
      sensor_cfg_ptr->flags = cfg_group_ptr->drv_cfg[ix].flags;

      if( cfg_group_ptr->min_ver_no > 0 )
      {
        sensor_cfg_ptr->device_select = cfg_group_ptr->drv_cfg[ix].device_select;
      }
      if( sensor_cfg_ptr->drv_fn_ptr != NULL )
      {
        valid_cfg = true;
      }
    }

    if ( !sns_smgr_ssi_is_last_cfg(item_id) )
    {
      curr_id = (uint16_t) sns_smgr_ssi_get_next_cfg_id(item_id);
      (void) sns_smgr_req_reg_data( curr_id, SNS_SMGR_REG_ITEM_TYPE_GROUP );
    }
    else
    {
      sns_smgr.all_init_state = SENSOR_ALL_INIT_CONFIGURED;
    }
  }  // else (no auto-detect)
}

/*===========================================================================

  FUNCTION:   sns_smgr_process_reg_data

===========================================================================*/
/*!
  @brief Receive registry data. Find where it was requested and apply it.

  @detail
  @param
   Type - Group or Single item registry
   Id - Group ID or single item ID
   Length - bytes
   data_ptr - pointer to received data
   sns_resp - response error
  @return
   none
 */
/*=========================================================================*/
SMGR_STATIC void sns_smgr_process_reg_data(
  sns_smgr_RegItemType_e  item_type, 
  uint16_t                item_id,
  uint32_t                item_len, 
  uint8_t*                data_ptr, 
  sns_common_resp_s_v01   sns_resp)
{
  sns_smgr_sensor_s*        sensor_ptr;
  const smgr_sensor_cfg_s*  sensor_cfg_ptr;
  uint32_t                  ix, iy;

  SNS_SMGR_PRINTF1(LOW, "proc reg data: Id:%u", item_id );

  if ( (sns_smgr_ssi_get_devinfo_idx(item_id) != -1) ||
       ((sns_resp.sns_err_t != SENSOR1_SUCCESS) && 
        (sns_smgr_ssi_get_devinfo_idx(sns_smgr.last_received_reg_group_id) != -1)) )
  {
    sns_smgr_process_reg_devinfo(item_type, item_id, item_len, data_ptr, sns_resp);
    return;
  }

  if ( (sns_smgr_ssi_get_cfg_idx(item_id) != -1) ||
       ((sns_resp.sns_err_t != SENSOR1_SUCCESS) && 
        (sns_smgr_ssi_get_cfg_idx(sns_smgr.last_received_reg_group_id) != -1)) )
  {
    sns_smgr_process_reg_ssi_config(item_type, item_id, item_len, data_ptr, sns_resp);
    return;
  }

  if ( sns_resp.sns_err_t != SENSOR1_SUCCESS )
  {
    SNS_SMGR_PRINTF3(ERROR, "resp_err=%u, id=%u, last_id=%u", 
                     sns_resp.sns_err_t, item_id, sns_smgr.last_received_reg_group_id);

    if ( item_id == sns_smgr.last_requested_sensor_dep_reg_group_id )
    {
      SNS_SMGR_PRINTF1(ERROR, "invalid DEP reg id=%u", 
                       sns_smgr.last_requested_sensor_dep_reg_group_id);
    }
    else
    {
      SNS_SMGR_PRINTF1(ERROR, "Unknown reg id=%u", sns_smgr.last_received_reg_group_id);
    }
  }
  else if ( item_id == sns_smgr.last_requested_sensor_dep_reg_group_id )
  {
    sns_reg_ssi_sensor_dep_reg_group_s* cfg_group_ptr = 
      (sns_reg_ssi_sensor_dep_reg_group_s*)data_ptr;

    SNS_SMGR_PRINTF1(LOW, "SMGR processing sensor dep reg item group %d", item_id);

    if ( cfg_group_ptr->ver_no == 0 )
    {
      SNS_SMGR_PRINTF0(HIGH, "Registry group is invalid");
      sns_smgr.last_requested_sensor_dep_reg_group_id = 0xFFFF;
      sns_smgr.all_init_state = SENSOR_ALL_INIT_CONFIGURED;
    }
    else
    {
      sns_smgr_process_sensor_dep_reg_data( cfg_group_ptr );
      if ( cfg_group_ptr->next_group_id != 0 )
      {
        sns_smgr_req_reg_data(cfg_group_ptr->next_group_id, 
                              SNS_SMGR_REG_ITEM_TYPE_GROUP);
        sns_smgr.last_requested_sensor_dep_reg_group_id = cfg_group_ptr->next_group_id;
      }
      else
      {
        SNS_SMGR_PRINTF0(HIGH, "Setting init_state to ALL_INIT_CONFIGURED");
        sns_smgr.last_requested_sensor_dep_reg_group_id = 0xFFFF;
        sns_smgr.all_init_state = SENSOR_ALL_INIT_CONFIGURED;
      }
    }
  }
  else if ( (item_id == SNS_REG_GROUP_SSI_GPIO_CFG_V02) &&
            (item_type == SNS_SMGR_REG_ITEM_TYPE_GROUP) )
  {
    sns_hw_update_ssi_reg_items(item_id, data_ptr);
  }
  else if ( ( (item_id >= SNS_REG_GROUP_SSC_GPIO_CFG_01_V02) &&
              (item_id <= SNS_REG_GROUP_SSC_GPIO_CFG_30_V02) ) &&
            (item_type == SNS_SMGR_REG_ITEM_TYPE_GROUP) )
  {
    int i = 0;
    bool found_empty_slot = false;
    sns_reg_group_ssc_gpio_config_group_s* reg_gpio_conf =
        (sns_reg_group_ssc_gpio_config_group_s*) data_ptr;

    // Set the GPIO configuration if the group is valid
    if ( (reg_gpio_conf->version > 0) && (SNS_SMGR_HW_INVALID_GPIO != reg_gpio_conf->gpio_num) )
    {
      uint32_t gpio_num = (uint32_t) (reg_gpio_conf->gpio_num & 0x00FF);

      // Find the next available gpio config slot or find a slot that's already
      // used by this gpio
      while ( (!found_empty_slot) && (i < SNS_REG_DDF_GPIO_CONFIG_MAX) )
      {
        if ( SNS_SMGR_HW_INVALID_GPIO == sns_smgr_hw_ssc_gpio_config[i].gpio_num ||
             ( (gpio_num == sns_smgr_hw_ssc_gpio_config[i].gpio_num) &&
               (false == sns_smgr_hw_ssc_gpio_config[i].is_slpi_gpio) ) )
        {
          // Found an appropriate gpio config slot, filling it in now
          sns_smgr_hw_ssc_gpio_config[i].is_slpi_gpio      = false;
          sns_smgr_hw_ssc_gpio_config[i].gpio_num          = gpio_num;
          sns_smgr_hw_ssc_gpio_config[i].active.is_valid   = true;
          sns_smgr_hw_ssc_gpio_config[i].active.func_sel   = reg_gpio_conf->active_func_sel;
          sns_smgr_hw_ssc_gpio_config[i].active.dir        = reg_gpio_conf->active_dir;
          sns_smgr_hw_ssc_gpio_config[i].active.pull       = reg_gpio_conf->active_pull;
          sns_smgr_hw_ssc_gpio_config[i].active.drive      = reg_gpio_conf->active_drive;
          sns_smgr_hw_ssc_gpio_config[i].inactive.is_valid = true;
          sns_smgr_hw_ssc_gpio_config[i].inactive.func_sel = reg_gpio_conf->inactive_func_sel;
          sns_smgr_hw_ssc_gpio_config[i].inactive.dir      = reg_gpio_conf->inactive_dir;
          sns_smgr_hw_ssc_gpio_config[i].inactive.pull     = reg_gpio_conf->inactive_pull;
          sns_smgr_hw_ssc_gpio_config[i].inactive.drive    = reg_gpio_conf->inactive_drive;

          found_empty_slot = true;
        }
        i++;
      }
      if (!found_empty_slot)
      {
        SNS_SMGR_PRINTF1(ERROR,
            "proc_reg_data - unable to find empty gpio_config slot for gpio_num %u",
            reg_gpio_conf->gpio_num);
      }
    }
  }
  else
  {
    for ( ix = 0; ix < SNS_SMGR_NUM_SENSORS_DEFINED; ix++ )
    {
      sensor_ptr = &sns_smgr.sensor[ix];
      sensor_cfg_ptr = sensor_ptr->const_ptr;

      /* skip any non-existent sensor */
      if ( NULL == SMGR_DRV_FN_PTR(sensor_ptr) )
      {
        continue;
      }

      if ( sensor_cfg_ptr->driver_reg_type == item_type &&
           sensor_cfg_ptr->driver_reg_id == item_id &&
           SENSOR_INIT_WAITING_REG == sensor_ptr->init_state)
      {
        /* Need to copy data for driver because response message will be freed
           before init can happen */
        if ( NULL != (sensor_ptr->reg_item_param.data = 
                      SNS_OS_MALLOC(SNS_DBG_MOD_DSPS_SMGR, item_len )))
        {
          for ( iy = 0; iy < item_len; iy++ )
          {
            sensor_ptr->reg_item_param.data[iy] = data_ptr[iy];
          }
          sensor_ptr->reg_item_param.nvitem_grp = item_type;
          sensor_ptr->reg_item_param.nvitem_id = item_id;
          sensor_ptr->reg_item_param.status = SNS_DDF_SUCCESS;
          sensor_ptr->reg_item_param.data_len = item_len;
          sensor_ptr->init_state = SENSOR_INIT_REG_READY;
        }
        else
        {
          /* No memory. At timeout we will discover registry request failed,
             thus init will fail. Very unlikely */
          SNS_SMGR_PRINTF2(
            HIGH, "ix=%d reg_id=%u", ix, (unsigned)sensor_cfg_ptr->driver_reg_id);
        }
      }
      else if ( (sensor_cfg_ptr->primary_cal_reg_type == item_type) &&
                (sensor_cfg_ptr->primary_cal_reg_id == item_id) &&
                (sensor_ptr->ddf_sensor_ptr[SNS_SMGR_DATA_TYPE_PRIMARY_V01] != NULL) &&
                (sensor_ptr->ddf_sensor_ptr[SNS_SMGR_DATA_TYPE_PRIMARY_V01]->
				all_cal_ptr != NULL) )
      {
        q16_t *reg_data_ptr = (q16_t *)data_ptr;
        sns_smgr_all_cal_s* all_cal_ptr = 
          sensor_ptr->ddf_sensor_ptr[SNS_SMGR_DATA_TYPE_PRIMARY_V01]->all_cal_ptr;
        SNS_ASSERT(item_len >= (sizeof(q16_t) * SNS_SMGR_SENSOR_DIMENSION_V01 * 2));    /* scale factors and biases */

        if ( item_id == SNS_REG_SCM_GROUP_MAG_FAC_CAL_PARAMS_V02 )
        {
          sns_smgr_cal_s* cal_db_ptr;
          sns_reg_mag_fac_cal_params_data_group_s* mag_reg_data_ptr =
            (sns_reg_mag_fac_cal_params_data_group_s*)data_ptr;

          /* Reset all autocal data */
          all_cal_ptr->auto_cal.used = false;
          sns_smgr_load_default_cal( &(all_cal_ptr->auto_cal) );

          cal_db_ptr = &all_cal_ptr->factory_cal;
          cal_db_ptr->used = true;
          if ( mag_reg_data_ptr->bias_valid )
          {
            cal_db_ptr->zero_bias[0] = mag_reg_data_ptr->x_fac_bias;
            cal_db_ptr->zero_bias[1] = mag_reg_data_ptr->y_fac_bias;
            cal_db_ptr->zero_bias[2] = mag_reg_data_ptr->z_fac_bias;

            SNS_SMGR_PRINTF3(
              LOW, "mag fac cal zero_bias [0]:%d, [1]:%d, [2]:%d",
              (int)cal_db_ptr->zero_bias[0],
              (int)cal_db_ptr->zero_bias[1],
              (int)cal_db_ptr->zero_bias[2]);
          }

          if ( mag_reg_data_ptr->cal_mat_valid )
          {
            cal_db_ptr->compensation_matrix_valid = true;

            cal_db_ptr->compensation_matrix[0] = mag_reg_data_ptr->compensation_matrix[0][0];
            cal_db_ptr->compensation_matrix[1] = mag_reg_data_ptr->compensation_matrix[0][1];
            cal_db_ptr->compensation_matrix[2] = mag_reg_data_ptr->compensation_matrix[0][2];
            cal_db_ptr->compensation_matrix[3] = mag_reg_data_ptr->compensation_matrix[1][0];
            cal_db_ptr->compensation_matrix[4] = mag_reg_data_ptr->compensation_matrix[1][1];
            cal_db_ptr->compensation_matrix[5] = mag_reg_data_ptr->compensation_matrix[1][2];
            cal_db_ptr->compensation_matrix[6] = mag_reg_data_ptr->compensation_matrix[2][0];
            cal_db_ptr->compensation_matrix[7] = mag_reg_data_ptr->compensation_matrix[2][1];
            cal_db_ptr->compensation_matrix[8] = mag_reg_data_ptr->compensation_matrix[2][2];

            SNS_SMGR_PRINTF3(
              LOW, "mag fac cal compensation_matrix: [0]:%u, [1]:%u, [2]:%u",
              (unsigned)cal_db_ptr->compensation_matrix[0],
              (unsigned)cal_db_ptr->compensation_matrix[1],
              (unsigned)cal_db_ptr->compensation_matrix[2]);
            SNS_SMGR_PRINTF3(
              LOW, "mag fac cal compensation_matrix: [0]:%u, [1]:%u, [2]:%u",
              (unsigned)cal_db_ptr->compensation_matrix[3],
              (unsigned)cal_db_ptr->compensation_matrix[4],
              (unsigned)cal_db_ptr->compensation_matrix[5]);
            SNS_SMGR_PRINTF3(
              LOW, "mag fac cal compensation_matrix: [0]:%u, [1]:%u, [2]:%u",
              (unsigned)cal_db_ptr->compensation_matrix[6],
              (unsigned)cal_db_ptr->compensation_matrix[7],
              (unsigned)cal_db_ptr->compensation_matrix[8]);
          }          
          all_cal_ptr->full_cal = *cal_db_ptr;
        }
        else if ( smgr_is_valid_fac_cal(reg_data_ptr) )
        {
          sns_smgr_cal_s* cal_db_ptr;
          uint32_t i;

          /* Reset all autocal data */
          all_cal_ptr->auto_cal.used = false;
          sns_smgr_load_default_cal(&all_cal_ptr->auto_cal);

          cal_db_ptr = &all_cal_ptr->factory_cal;
          cal_db_ptr->used = true;
          for ( i = 0; i < ARR_SIZE(cal_db_ptr->zero_bias); i++)
          {
            cal_db_ptr->zero_bias[i]    = reg_data_ptr[i];
            cal_db_ptr->scale_factor[i] = reg_data_ptr[i+SNS_SMGR_SENSOR_DIMENSION_V01];
          }
          all_cal_ptr->full_cal = *cal_db_ptr;
        }
      }
    }
  }
}

/*===========================================================================

  FUNCTION:   sns_smgr_process_reg_resp_msg

===========================================================================*/
/*!
  @brief Receive registry data. Find where it was requested and apply it.

  @param item_ptr - the response message
 
  @return
   none
 */
/*=========================================================================*/
void sns_smgr_process_reg_resp_msg(sns_smgr_mr_msg_s* item_ptr)
{
  /* Response from register read request of group or single item */
  if ( SNS_REG_GROUP_READ_RESP_V02 == item_ptr->header.msg_id )
  {
    sns_reg_group_read_resp_msg_v02* resp_ptr =
      (sns_reg_group_read_resp_msg_v02*)item_ptr->body_ptr;

    sns_smgr_process_reg_data(SNS_SMGR_REG_ITEM_TYPE_GROUP,
                              resp_ptr->group_id,
                              resp_ptr->data_len,
                              resp_ptr->data,
                              resp_ptr->resp);
  }
  else if ( SNS_REG_SINGLE_READ_RESP_V02 == item_ptr->header.msg_id )
  {
    sns_reg_single_read_resp_msg_v02* resp_ptr =
      (sns_reg_single_read_resp_msg_v02*)item_ptr->body_ptr;
    sns_smgr_process_reg_data(SNS_SMGR_REG_ITEM_TYPE_SINGLE,
                              resp_ptr->item_id,
                              resp_ptr->data_len,
                              resp_ptr->data,
                              resp_ptr->resp);
  }
  else  /* response from register write */
  {
    SNS_SMGR_PRINTF2(MED, "process_msg - resp result=%d err=%d",
                     ((sns_common_resp_s_v01*)item_ptr->body_ptr)->sns_result_t,
                     ((sns_common_resp_s_v01*)item_ptr->body_ptr)->sns_err_t);
  }
  SNS_OS_FREE(item_ptr->body_ptr);
  SNS_OS_FREE(item_ptr);
}

/*===========================================================================

  FUNCTION:   sns_smgr_req_reg_data

===========================================================================*/
/*!
  @brief This function requests data from registry

  @detail
  @param
   Id - identifier of single item or group
   Type - option to request single item or group
  @return
   error code
 */
/*=========================================================================*/
sns_err_code_e sns_smgr_req_reg_data( 
  uint16_t item_id, 
  uint8_t  item_type)
{
  void*                 read_req_ptr = NULL;
  void*                 read_resp_ptr = NULL;
  uint16_t*             resp_cb_data_ptr = NULL;
  sns_smgr_mr_header_s  req_hdr;
  uint16_t              resp_len = 0;

  SMGR_ASSERT_AT_COMPILE(ARR_SIZE(smgr_ssi_cfg)*SNS_REG_SSI_SMGR_CFG_NUM_SENSORS >=
                         ARR_SIZE(smgr_ssi_devinfo));

  SNS_SMGR_PRINTF2(MED, "req_reg_data - item(%d) type(%d)", item_id, item_type);

  if ( SNS_SMGR_REG_ITEM_TYPE_GROUP == item_type )
  {
    req_hdr.msg_id = SNS_REG_GROUP_READ_REQ_V02;
    req_hdr.body_len = sizeof( sns_reg_group_read_req_msg_v02 );
    resp_len = sizeof(sns_reg_group_read_resp_msg_v02);
    resp_cb_data_ptr = SNS_OS_MALLOC(SNS_DBG_MOD_DSPS_SMGR, sizeof(uint16));
    if( resp_cb_data_ptr )
    {
      *resp_cb_data_ptr = item_id;
    }
  }
  else if ( SNS_SMGR_REG_ITEM_TYPE_SINGLE == item_type )
  {
    req_hdr.msg_id = SNS_REG_SINGLE_READ_REQ_V02;
    req_hdr.body_len = sizeof( sns_reg_single_read_req_msg_v02 );
    resp_len = sizeof(sns_reg_single_read_resp_msg_v02);
  }
  else
  {
    return SNS_ERR_FAILED;
  }

  read_req_ptr = SNS_OS_MALLOC(SNS_DBG_MOD_DSPS_SMGR, req_hdr.body_len);
  read_resp_ptr = SNS_OS_MALLOC(SNS_DBG_MOD_DSPS_SMGR, resp_len);

  if ( (NULL == read_req_ptr) || (NULL == read_resp_ptr) )
  {
    if (NULL != read_req_ptr)
    {
      SNS_OS_FREE(read_req_ptr);
    }
    if (NULL != read_resp_ptr)
    {
      SNS_OS_FREE(read_resp_ptr);
    }
    if (NULL != resp_cb_data_ptr)
    {
      SNS_OS_FREE(resp_cb_data_ptr);
    }
    return SNS_ERR_NOMEM;
  }

  if ( SNS_SMGR_REG_ITEM_TYPE_GROUP == item_type )
  {
    ((sns_reg_group_read_req_msg_v02*)read_req_ptr)->group_id = item_id;
  }
  else
  {
    ((sns_reg_single_read_req_msg_v02*)read_req_ptr)->item_id = item_id;
  }

  req_hdr.svc_num = SNS_REG2_SVC_ID_V01;
  return sns_smgr_mr_send_req(&req_hdr, read_req_ptr, read_resp_ptr, resp_len, 
                              (void*)resp_cb_data_ptr);
}

/*===========================================================================

  FUNCTION:   sns_smgr_update_reg_data

===========================================================================*/
/*!
  @brief This function updates the registry data

  @detail
  @param
   item_id   - identifier of single item or group
   item_type - option to request single item or group
   item_len  - bytes
   data_ptr  - pointer to data that will update the registry
  @return
   error code
 */
/*=========================================================================*/
sns_err_code_e sns_smgr_update_reg_data( 
  const uint16_t item_id,
  const uint8_t  item_type,
  uint32_t       item_len,
  uint8_t*       data_ptr)
{
  sns_err_code_e        sns_err = SNS_ERR_FAILED;

#ifndef SNS_PCSIM
  void*                 write_req_ptr = NULL;
  void*                 write_resp_ptr = NULL;
  uint16_t              resp_len;
  sns_smgr_mr_header_s  req_hdr;

  req_hdr.svc_num = SNS_REG2_SVC_ID_V01;
  if ( SNS_SMGR_REG_ITEM_TYPE_GROUP == item_type )
  {
    if (item_len > SNS_REG_MAX_GROUP_BYTE_COUNT_V02)
    {
      return SNS_ERR_BAD_PARM;
    }
    req_hdr.msg_id = SNS_REG_GROUP_WRITE_REQ_V02;
    req_hdr.body_len = sizeof(sns_reg_group_write_req_msg_v02);
    resp_len = sizeof(sns_reg_group_write_resp_msg_v02);
  }
  else if ( SNS_SMGR_REG_ITEM_TYPE_SINGLE == item_type )
  {
    if (item_len >= SNS_REG_MAX_ITEM_BYTE_COUNT_V02)
    {
      return SNS_ERR_BAD_PARM;
    }
    req_hdr.msg_id = SNS_REG_SINGLE_WRITE_REQ_V02;
    req_hdr.body_len = sizeof( sns_reg_single_write_req_msg_v02 );
    resp_len = sizeof(sns_reg_single_write_resp_msg_v02);
  }
  else
  {
    return SNS_ERR_FAILED;
  }

  write_req_ptr = SNS_OS_MALLOC(SNS_DBG_MOD_DSPS_SMGR, req_hdr.body_len);
  write_resp_ptr = SNS_OS_MALLOC(SNS_DBG_MOD_DSPS_SMGR, resp_len);
  if ( (NULL == write_req_ptr) || (NULL == write_resp_ptr) )
  {
    if (NULL != write_req_ptr)
    {
      SNS_OS_FREE(write_req_ptr);
    }
    if (NULL != write_resp_ptr)
    {
      SNS_OS_FREE(write_resp_ptr);
    }
    return SNS_ERR_NOMEM;
  }

  if ( SNS_SMGR_REG_ITEM_TYPE_GROUP == item_type )
  {
    sns_reg_group_write_req_msg_v02* req_ptr = 
      (sns_reg_group_write_req_msg_v02*)write_req_ptr;
    req_ptr->group_id = item_id;
    req_ptr->data_len = item_len;
    SNS_OS_MEMSCPY(req_ptr->data, sizeof(req_ptr->data), data_ptr, item_len);
  }
  else
  {
    sns_reg_single_write_req_msg_v02* req_ptr =
      (sns_reg_single_write_req_msg_v02*)write_req_ptr;
    req_ptr->item_id  = item_id;
    req_ptr->data_len = item_len;
    SNS_OS_MEMSCPY(req_ptr->data, sizeof(req_ptr->data), data_ptr, item_len);
  }

  sns_err = sns_smgr_mr_send_req(&req_hdr, write_req_ptr, write_resp_ptr, 
                                 resp_len, NULL);
#endif

  return sns_err;
}

/*===========================================================================

  FUNCTION:   smgr_send_reg_message_for_fac_cal

===========================================================================*/
/*!
  @brief Send registry message for factory calibration

  @Detail

  @param[i] NONE

  @return
   NONE
*/
/*=========================================================================*/
void smgr_send_reg_message_for_fac_cal(void)
{
#ifndef ADSP_STANDALONE
  sns_smgr_sensor_s*        sensor_ptr;
  const smgr_sensor_cfg_s*  sensor_cfg_ptr;
  uint32_t                  ix;

  for ( ix = 0; ix < ARR_SIZE(sns_smgr.sensor); ix++ )
  {
    sensor_ptr = &sns_smgr.sensor[ix];
    sensor_cfg_ptr = sensor_ptr->const_ptr;

    /* skip any non-existent sensor */
    if ( NULL == SMGR_DRV_FN_PTR(sensor_ptr) )
    {
      continue;
    }

    if ( SNS_SMGR_REG_ITEM_TYPE_NONE != sensor_cfg_ptr->primary_cal_reg_type )
    {
      sns_err_code_e  err;
      err = sns_smgr_req_reg_data(sensor_cfg_ptr->primary_cal_reg_id,
                                  sensor_cfg_ptr->primary_cal_reg_type);
      SNS_ASSERT( SNS_SUCCESS == err);
    }
  }
#endif
}

/*===========================================================================

  FUNCTION:   sns_smgr_save_last_rcvd_reg_group_id

===========================================================================*/
/*!
  @brief Saves the last received registry group ID

  @detail
  @param[i] group_id - the last registry group ID received
  @return   none
 */
/*=========================================================================*/
void sns_smgr_save_last_rcvd_reg_group_id(uint16_t group_id)
{
  sns_smgr.last_received_reg_group_id = group_id;
}

