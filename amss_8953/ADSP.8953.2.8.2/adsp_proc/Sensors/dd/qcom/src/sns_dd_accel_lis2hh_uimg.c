/******************** (C) COPYRIGHT 2014 STMicroelectronics ********************
 *
 * File Name         : sns_dd_accel_lis2hh.c
 * Authors           : Karimuddin Sayed
 * Version           : V 3.0.0
 * Date              : 04/02/2015
 * Description       : LIS2HH Accelerometer driver source file
 *
 ********************************************************************************
 * Copyright (c) 2014, STMicroelectronics.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     3. Neither the name of the STMicroelectronics nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

#include "sns_dd_accel_lis2hh.h"

extern sns_ddf_status_e sns_dd_acc_lis2hh_init(
    sns_ddf_handle_t* dd_handle_ptr,
    sns_ddf_handle_t smgr_handle,
    sns_ddf_nv_params_s* nv_params,
    sns_ddf_device_access_s device_info[],
    uint32_t num_devices,
    sns_ddf_memhandler_s* memhandler,
    sns_ddf_sensor_e* sensors[],
    uint32_t* num_sensors);

extern sns_ddf_status_e sns_dd_acc_lis2hh_get_attr(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensor,
    sns_ddf_attribute_e attrib,
    sns_ddf_memhandler_s* memhandler,
    void** value,
    uint32_t* num_elems);

extern sns_ddf_status_e sns_dd_acc_lis2hh_set_attr(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensor,
    sns_ddf_attribute_e attrib,
    void* value);

extern sns_ddf_status_e sns_dd_acc_lis2hh_enable_sched(
    sns_ddf_handle_t  dd_handle,
    sns_ddf_sensor_e  sensor,
    bool              enable);

extern sns_ddf_status_e sns_dd_acc_lis2hh_reset(sns_ddf_handle_t dd_handle);

extern sns_ddf_status_e sns_dd_acc_lis2hh_run_test(
    sns_ddf_handle_t  dd_handle,
    sns_ddf_sensor_e  sensor,
    sns_ddf_test_e    test,
    uint32_t*         err);
/**
 * Accelerometer LIS2HH sampling frequencies array for attribute(ODR)
 */
const sns_ddf_odr_t lis2hh_odr[STM_LIS2HH_ODR_NUM] = {
  10,
  50,
  100,
  200,
  400,
  800
};

#if DUMP_REGISTER_DATA
void dump_register_single(sns_ddf_handle_t  dd_handle, uint8_t reg, uint8_t size)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer[32] = {0};
  uint8_t rw_bytes = 0;
  uint8_t i = 0;
  if(state->g_value == 1)
    return;

  status = sns_ddf_read_port(
      state->port_handle,
      reg,
      &rw_buffer[i],
      size,
      &rw_bytes);
  if(status != SNS_DDF_SUCCESS) {
    LIS2HH_MSG_0(HIGH, "returned error");
    return;
  }
  if(rw_bytes != size) {
    LIS2HH_MSG_0(HIGH, "bytes doesn't match");
    return;
  }
  for(i=0;i<size;i++)
    LIS2HH_MSG_2(HIGH, "UPDATED - Reg 0x%x = 0x%x ", reg, rw_buffer[i]);
}

void dump_registers(sns_ddf_handle_t  dd_handle)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer[32] = {0};
  uint8_t reg_map[] = {
    STM_LIS2HH_CTRL_REG1_A,
    STM_LIS2HH_CTRL_REG2_A,
    STM_LIS2HH_CTRL_REG3_A,
    STM_LIS2HH_CTRL_REG4_A,
    STM_LIS2HH_CTRL_REG5_A,
    STM_LIS2HH_CTRL_REG6_A,
    STM_LIS2HH_CTRL_REG7_A,
    STM_LIS2HH_FIFO_MODE,
    STM_LIS2HH_INT1_CFG
  };

  uint8_t rw_bytes = 0;
  uint8_t i = 0;
  uint16_t n = sizeof(reg_map)/sizeof(reg_map[0]);
  if(state->g_value == 1)
    return;

  LIS2HH_MSG_0(HIGH, "Register conf::");
  for(i=0; i<n;i++) {
    status = sns_ddf_read_port(
        state->port_handle,
        reg_map[i],
        &rw_buffer[i],
        1,
        &rw_bytes);
    if(status != SNS_DDF_SUCCESS) {
      LIS2HH_MSG_0(HIGH, "returned error");
      return;
    }
    if(rw_bytes != 1) {
      LIS2HH_MSG_0(HIGH, "bytes doesn't match");
      return;
    }
    LIS2HH_MSG_2(HIGH, "Reg 0x%x = 0x%x ", reg_map[i], rw_buffer[i]);
  }
}
#endif


#if 0
/**
 * @brief Check ZYXDA in STATUS register, return data ready flag.
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 * @param[out] data_flag      data ready flag to set.
 *                            true:  a new set of data is available.
 *                            false: a new set of data is not yet available.
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
static sns_ddf_status_e sns_dd_acc_lis2hh_is_dataready(
    sns_ddf_handle_t dd_handle,
    bool *data_flag)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer = 0;
  uint8_t rw_bytes = 0;

  // read status register
  status = sns_ddf_read_port(
      state->port_handle,
      STM_LIS2HH_STATUS_REG_A,
      &rw_buffer,
      1,
      &rw_bytes);
  if(status != SNS_DDF_SUCCESS)
    return status;
  if(rw_bytes != 1)
    return SNS_DDF_EBUS;

  if((rw_buffer&0x08) > 0)
    *data_flag = true;
  else
    *data_flag = false;

  return SNS_DDF_SUCCESS;
}
#endif


#if STM_LIS2HH_LOGDATA
/*===========================================================================
FUNCTION:   sns_dd_acc_lis2hh_log_data
===========================================================================*/
/*!
  @brief log accel sensor data

  @detail
  - Logs latest set of sensor data sampled from the sensor.

  @param[in] accel_data_ptr: ptr to the driver data
  */
/*=========================================================================*/
void sns_dd_acc_lis2hh_log_data(sns_ddf_sensor_data_s *accel_data_ptr)
{
  sns_err_code_e err_code;
  sns_log_sensor_data_pkt_s* log_struct_ptr;

  //Allocate log packet
  err_code = sns_logpkt_malloc(SNS_LOG_CONVERTED_SENSOR_DATA,
      sizeof(sns_log_sensor_data_pkt_s),
      (void**)&log_struct_ptr);

  if ((err_code == SNS_SUCCESS) && (log_struct_ptr != NULL))
  {
    log_struct_ptr->version = SNS_LOG_SENSOR_DATA_PKT_VERSION;
    log_struct_ptr->sensor_id = SNS_DDF_SENSOR_ACCEL;
    log_struct_ptr->vendor_id = SNS_DDF_VENDOR_STMICRO;

    //Timestamp the log with sample time
    log_struct_ptr->timestamp = accel_data_ptr->timestamp;

    //Log the sensor data
    log_struct_ptr->num_data_types = STM_LIS2HH_NUM_AXES;
    log_struct_ptr->data[0]  = accel_data_ptr->samples[0].sample;
    log_struct_ptr->data[1]  = accel_data_ptr->samples[1].sample;
    log_struct_ptr->data[2]  = accel_data_ptr->samples[2].sample;

    //Commit log (also frees up the log packet memory)
    (void) sns_logpkt_commit(SNS_LOG_CONVERTED_SENSOR_DATA,
        log_struct_ptr);
  }
}
#endif
#if STM_LIS2HH_LOGDATA
/*===========================================================================
FUNCTION:   sns_dd_acc_log_fifo
===========================================================================*/
/*!
  @brief log acc sensor data in fifo

  @detail
  - Logs latest set of sensor data sampled from the sensor.

  @param[in] acc_data_ptr: ptr to the driver data
  */
/*=========================================================================*/
void sns_dd_acc_lis2hh_log_fifo(sns_ddf_sensor_data_s *acc_data_ptr)
{
  sns_err_code_e err_code;
  sns_log_sensor_data_pkt_s* log_struct_ptr;
  uint16 idx =0 ;

  //Allocate log packet
  err_code = sns_logpkt_malloc(SNS_LOG_CONVERTED_SENSOR_DATA,
      sizeof(sns_log_sensor_data_pkt_s) + (acc_data_ptr->num_samples -1)*sizeof(int32_t),
      (void**)&log_struct_ptr);

  if ((err_code == SNS_SUCCESS) && (log_struct_ptr != NULL))
  {
    log_struct_ptr->version = SNS_LOG_SENSOR_DATA_PKT_VERSION;
    log_struct_ptr->sensor_id = SNS_DDF_SENSOR_ACCEL;
    log_struct_ptr->vendor_id = SNS_DDF_VENDOR_STMICRO;

    //Timestamp the log with sample time
    log_struct_ptr->timestamp = acc_data_ptr->timestamp;
    log_struct_ptr->end_timestamp = acc_data_ptr->end_timestamp;

    log_struct_ptr->num_data_types = STM_LIS2HH_NUM_AXES;
    log_struct_ptr->num_samples = acc_data_ptr->num_samples / STM_LIS2HH_NUM_AXES;

    //Log the sensor fifo data
    log_struct_ptr->data[0]  = acc_data_ptr->samples[0].sample;
    log_struct_ptr->data[1]  = acc_data_ptr->samples[1].sample;
    log_struct_ptr->data[2]  = acc_data_ptr->samples[2].sample;

    for(idx=0; idx<acc_data_ptr->num_samples; idx++)
    {
      log_struct_ptr->samples[idx]  = acc_data_ptr->samples[idx].sample;
    }

    //Commit log (also frees up the log packet memory)
    (void) sns_logpkt_commit(SNS_LOG_CONVERTED_SENSOR_DATA,
        log_struct_ptr);
  }
}
#endif

/**
 * @brief Retrieves a single set of sensor data from lis2hh.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
static sns_ddf_status_e sns_dd_acc_lis2hh_get_data(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensors[],
    uint32_t num_sensors,
    sns_ddf_memhandler_s* memhandler,
    sns_ddf_sensor_data_s** data)
{
  uint8_t read_buffer[STM_LIS2HH_NUM_READ_BYTES] = { 0 };
  uint8_t read_count = 0;
  sns_ddf_time_t timestamp;
  sns_ddf_status_e status;
  sns_ddf_sensor_data_s *data_ptr;
  sns_dd_acc_lis2hh_state_t *state = dd_handle;
  // sensor coordinate x,y,z axis raw hardware data
  int16_t hw_d[STM_LIS2HH_NUM_AXES] = { 0 };
  // SAE coordinate x,y,z axis data
  q16_t sae_d[STM_LIS2HH_NUM_AXES] = { 0 };

  //if current power mode is LOWPOWER , return error.
  if((SNS_DDF_POWERSTATE_LOWPOWER == state->power_state)||(num_sensors!=1))
    return SNS_DDF_EDEVICE;

  // This is a synchronous driver, so try to read data now.
  status = sns_ddf_read_port(
      state->port_handle,
      STM_LIS2HH_OUT_X_L_A,
      read_buffer,
      STM_LIS2HH_NUM_READ_BYTES,
      &read_count);
  if(status != SNS_DDF_SUCCESS)
    return status;
  if(read_count != STM_LIS2HH_NUM_READ_BYTES)
    return SNS_DDF_EBUS;

  // get current time stamp
  timestamp = sns_ddf_get_timestamp();

  //convert the raw data in read_buffer to X/Y/Z axis sensor data
  hw_d[0] = (int16_t) (((read_buffer[1]) << 8) | read_buffer[0]);
  hw_d[1] = (int16_t) (((read_buffer[3]) << 8) | read_buffer[2]);
  hw_d[2] = (int16_t) (((read_buffer[5]) << 8) | read_buffer[4]);

  //adjust sensor data per current sensitivity, and convert to Q16
  sae_d[0] = FX_FLTTOFIX_Q16(hw_d[0] * state->sstvt_adj * G/1000000);
  sae_d[1] = FX_FLTTOFIX_Q16(hw_d[1] * state->sstvt_adj * G/1000000);
  sae_d[2] = FX_FLTTOFIX_Q16(hw_d[2] * state->sstvt_adj * G/1000000);

  //map the sensor data to the phone's coordinates
  sns_ddf_map_axes(&state->axes_map, sae_d);

  //allocate memory for sns_ddf_sensor_data_s data structure
  data_ptr =sns_dd_lis2hh_memhandler_malloc(memhandler, sizeof(sns_ddf_sensor_data_s), state->smgr_handle);
  if(NULL == data_ptr)
  {
    return SNS_DDF_ENOMEM;
  }
  *data = data_ptr;

  data_ptr->sensor = sensors[0];
  data_ptr->status = SNS_DDF_SUCCESS;
  data_ptr->timestamp = timestamp;

  //allocate memory for data samples.
  data_ptr->samples = sns_dd_lis2hh_memhandler_malloc(memhandler, STM_LIS2HH_NUM_AXES * sizeof(sns_ddf_sensor_sample_s), state->smgr_handle);
  if(NULL == data_ptr->samples)
  {
    return SNS_DDF_ENOMEM;
  }

  //Fill out sensor output data and status
  //Axis and polarity conversion are configured in the header file.
  data_ptr->samples[0].sample = sae_d[0];
  data_ptr->samples[0].status = SNS_DDF_SUCCESS;
  data_ptr->samples[1].sample = sae_d[1];
  data_ptr->samples[1].status = SNS_DDF_SUCCESS;
  data_ptr->samples[2].sample = sae_d[2];
  data_ptr->samples[2].status = SNS_DDF_SUCCESS;
  data_ptr->num_samples = STM_LIS2HH_NUM_AXES;

#if STM_LIS2HH_LOGDATA
  sns_dd_acc_lis2hh_log_data(data_ptr);
#endif

  return SNS_DDF_SUCCESS;
}
/**
 * @brief Reads FIFO_SRC register to extract the FIFO sample count
 *
 * @param[in]  state      The driver instance
 *
 * @return number of samples in FIFO
 */
static uint8_t sns_dd_acc_lis2hh_get_fifo_sample_count(
    sns_dd_acc_lis2hh_state_t* state)
{
  uint8_t fifo_count;
  uint8_t rw_buffer = 0;
  uint8_t rw_bytes = 0;
  sns_ddf_status_e status;
  sns_ddf_time_t cur_time, odr_interval;

  if(1 == state->fifo_wmk)
  {
    cur_time = sns_ddf_get_timestamp();
    odr_interval = sns_ddf_convert_usec_to_tick(1000000 / lis2hh_odr[state->cur_rate_idx]);

    if((cur_time - state->last_timestamp) < odr_interval)
    {
      fifo_count = 0;
    }
    else
    {
      fifo_count = 1;
    }
  }
  else
  {
    status  = sns_ddf_read_port(state->port_handle,
        STM_LIS2HH_FIFO_SRC,
        &rw_buffer,
        1,
        &rw_bytes);
    if((status != SNS_DDF_SUCCESS) || (rw_bytes != 1))
    {
      fifo_count = 0;
    }
    else
    {
      fifo_count = rw_buffer & 0x1F;

      // if it's overflow, add one more sample.
      if(rw_buffer & 0x40)
      {
        fifo_count++;
      }
    }
  }
  return fifo_count;
}

/*===========================================================================
FUNCTION:   sns_dd_acc_parse_and_send_fifo_data
===========================================================================*/
/*!
  @brief Parses FIFO data and sends to SMGR
  */
/*=========================================================================*/
static sns_ddf_status_e sns_dd_acc_lis2hh_parse_and_send_fifo_data(
    sns_dd_acc_lis2hh_state_t* state,
    sns_ddf_handle_t            smgr_handle,
    const uint8_t*              raw_buf_ptr,
    uint16_t                    num_samples
    )
{
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  uint8_t idx;
  // sensor coordinate x,y,z axis raw data sample
  static int16_t hw_d[STM_LIS2HH_NUM_AXES];
  // SAE coordinate x,y,z axis data sample
  static q16_t sae_d[STM_LIS2HH_NUM_AXES];

  for(idx=0; idx<num_samples; idx++)
  {
    //convert the raw data in read_buffer to X/Y/Z axis sensor data
    hw_d[0] = (int16_t) (((raw_buf_ptr[1 + idx*6]) << 8) |
        raw_buf_ptr[0 + idx*6]);
    hw_d[1] = (int16_t) (((raw_buf_ptr[3 + idx*6]) << 8) |
        raw_buf_ptr[2 + idx*6]);
    hw_d[2] = (int16_t) (((raw_buf_ptr[5 + idx*6]) << 8) |
        raw_buf_ptr[4 + idx*6]);

    //adjust sensor data per current sensitivity, and convert to Q16
    sae_d[0] = FX_FLTTOFIX_Q16(hw_d[0] * state->sstvt_adj * G/1000000);
    sae_d[1] = FX_FLTTOFIX_Q16(hw_d[1] * state->sstvt_adj * G/1000000);
    sae_d[2] = FX_FLTTOFIX_Q16(hw_d[2] * state->sstvt_adj * G/1000000);

    //map the sensor data to the phone's coordinates
    sns_ddf_map_axes(&state->axes_map, sae_d);

    //Fill out sensor output data and status
    state->fifo_data.samples[0 + idx*3].sample = sae_d[0];
    state->fifo_data.samples[0 + idx*3].status = SNS_DDF_SUCCESS;
    state->fifo_data.samples[1 + idx*3].sample = sae_d[1];
    state->fifo_data.samples[1 + idx*3].status = SNS_DDF_SUCCESS;
    state->fifo_data.samples[2 + idx*3].sample = sae_d[2];
    state->fifo_data.samples[2 + idx*3].status = SNS_DDF_SUCCESS;

  }

  state->fifo_data.num_samples = STM_LIS2HH_NUM_AXES * num_samples;

  // send fifo data to SMGR
  status = sns_ddf_smgr_notify_data(
      state->smgr_handle,
      &state->fifo_data,
      1);
  if(status != SNS_DDF_SUCCESS)
    return status;

  LIS2HH_MSG_1(HIGH, "parse_and_send: sent data successfully. fifo_data.num_samples=%u",
      state->fifo_data.num_samples);

  LIS2HH_MSG_1(HIGH, "parse_and_send: end time=%u", sns_ddf_get_timestamp());

#if STM_LIS2HH_LOGDATA
  sns_dd_acc_lis2hh_log_fifo(&state->fifo_data);
#endif

  return SNS_DDF_SUCCESS;
}


/**
 * @brief To send FIFO data to SMGR, numbers of sample up to watermark level.
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 * @param[in]  num_samples Num of data samples in FIFO to read.
 * @param[in]  timestamp      The end timestamp.
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
static sns_ddf_status_e sns_dd_acc_lis2hh_send_fifo_data(
    sns_ddf_handle_t dd_handle,
    uint16_t num_samples,
    sns_ddf_time_t    start_time,
    sns_ddf_time_t    end_time)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status = SNS_DDF_SUCCESS;
  uint8_t read_count = 0;
  uint16_t num_invalid_samples;
  uint8_t* raw_buf_ptr = state->fifo_raw_buffer;

  // read all FIFO data up to num_samples.
  status = sns_ddf_read_port(
      state->port_handle,
      STM_LIS2HH_OUT_X_L_A,
      state->fifo_raw_buffer,
      STM_LIS2HH_NUM_READ_BYTES * num_samples,
      &read_count);
  if(status != SNS_DDF_SUCCESS)
    return status;
  if(read_count != STM_LIS2HH_NUM_READ_BYTES * num_samples)
  {
    LIS2HH_MSG_2(HIGH, "send_fifo_data: raw data reading fails, num_samples_to_read=%u, actual_read_bytes=%u",
        num_samples, read_count);
    return SNS_DDF_EBUS;
  }

  // fill out fifo_data structure
  state->fifo_data.sensor = SNS_DDF_SENSOR_ACCEL;
  state->fifo_data.end_timestamp = end_time;
  state->fifo_data.timestamp = start_time;

  LIS2HH_MSG_2(HIGH, "send_fifo_data: start_timestamp=%u, end_timestamp=%u",
      state->fifo_data.timestamp, state->fifo_data.end_timestamp);

  if ( (int32_t)(state->odr_settled_ts - state->fifo_data.timestamp) <= 0 )
  {
    /* all samples in FIFO produced after ODR change settled */
    num_invalid_samples = 0;
  }
  else if ( (int32_t)(state->odr_settled_ts - state->fifo_data.end_timestamp) > 0 )
  {
    /* all samples in FIFO produced before ODR change settled */
    num_invalid_samples = num_samples;
  }
  else /* some samples in FIFO produced before ODR change settled */
  {
    //last_timestamp could be zero or not reliable. Use input parameters of this function.
    uint32_t time_interval = (end_time - start_time)/(num_samples-1);
    num_invalid_samples =
      ((state->odr_settled_ts - state->fifo_data.timestamp) / time_interval) + 1;
    state->fifo_data.end_timestamp =
      start_time + (time_interval * (num_invalid_samples-1));
    //set temporary last_timestamp within this function.
    state->last_timestamp = state->fifo_data.end_timestamp;
  }

  if ( num_invalid_samples > 0 )
  {
    state->fifo_data.status      = SNS_DDF_EINVALID_DATA;
    state->fifo_data.num_samples = STM_LIS2HH_NUM_AXES * num_invalid_samples;
    status = sns_dd_acc_lis2hh_parse_and_send_fifo_data(
        state, state->smgr_handle, raw_buf_ptr, num_invalid_samples);
    MSG_3(MSG_SSID_SNS, DBG_HIGH_PRIO,
        "send_fifo_data: #samples=%d #invalid=%d end_ts=%u",
        num_samples, num_invalid_samples, state->fifo_data.end_timestamp);

    /* prepares to send remaining samples */
    num_samples -= num_invalid_samples;
    if ( num_samples > 0 )
    {
      raw_buf_ptr = &state->fifo_raw_buffer[state->fifo_data.num_samples<<1];
      state->fifo_data.end_timestamp = end_time;
      state->fifo_data.timestamp     = state->last_timestamp +
        (end_time - state->last_timestamp)/num_samples;
      MSG_3(MSG_SSID_SNS, DBG_HIGH_PRIO,
          "send_fifo_data: #valid=%d ts=%u end_ts=%u",
          num_samples, state->fifo_data.timestamp, state->fifo_data.end_timestamp);
    }
  }
  if ( num_samples > 0 )
  {
    state->fifo_data.status = SNS_DDF_SUCCESS;
    state->fifo_data.num_samples = STM_LIS2HH_NUM_AXES * num_samples;
    status |= sns_dd_acc_lis2hh_parse_and_send_fifo_data(
        state, state->smgr_handle, raw_buf_ptr, num_samples);
  }

  //set final last_timestamp value.
  state->last_timestamp = end_time;

  return status;
}

/**
 * @brief Called in response to an motion detection interrupt for this driver.
 *
 * @param[in]  dd_handle    Handle to a driver instance.
 * @param[in]  gpio_num     GPIO number that generated the interrupt.
 * @param[in]  timestamp    Time at which ISR is called
 *
 * @return none
 * */
void sns_dd_acc_lis2hh_handle_irq(
    sns_ddf_handle_t  handle,
    uint32_t          gpio_num,
    sns_ddf_time_t    timestamp)
{
  sns_dd_acc_lis2hh_state_t* state = handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer[2] = {0, 0};
  uint8_t rw_bytes = 0;
  uint8_t total_in_fifo = 0;
  //flag for the while loop to read out any new data samples during previous INT handling.
  bool run_again = true;
  //how many times fifo_read happens
  uint8_t fifo_run_cnt = 0;
  sns_ddf_time_t end_time=0, cal_end=0, start_time=0, cal_odr=0, est_odr=0;

  if(handle==NULL)
  {
    LIS2HH_MSG_0(HIGH, "handle_irq: Received Null Pointer.");
    return;
  }
  LIS2HH_MSG_1(HIGH, "handle_irq: CPU IRQ time=%u", timestamp);
  LIS2HH_MSG_1(HIGH, "handle_irq: DD ISR start time=%u", sns_ddf_get_timestamp());

  if (gpio_num != state->gpio_num)
    return;

  est_odr = sns_ddf_convert_usec_to_tick(1000000 / lis2hh_odr[state->cur_rate_idx]);

  while(run_again)
  {
    run_again = false;

    // verify source of interrupts by reading FIFO_SRC.
    status = sns_ddf_read_port(
        state->port_handle,
        STM_LIS2HH_FIFO_SRC,
        rw_buffer+1,
        1,
        &rw_bytes);
    if(status != SNS_DDF_SUCCESS)
      return;
    if(rw_bytes != 1)
      return;

    //get new end_time.
    end_time = sns_ddf_get_timestamp();

    LIS2HH_MSG_2(HIGH, "handle_irq: STATUS_REG=%u, FIFO_SRC=%u", rw_buffer[0], rw_buffer[1]);

    //FIFO overflow interrupt handling.
    if((rw_buffer[1] & 0x40) && (fifo_run_cnt==0))
    {
      LIS2HH_MSG_2(HIGH, "handle_irq: to send overflow event, watermark=%u, timestamp=%u",
          state->fifo_wmk, timestamp);

      // send a FIFO overflow event to SMGR
      status = sns_ddf_smgr_notify_event(
          state->smgr_handle,
          SNS_DDF_SENSOR_ACCEL,
          SNS_DDF_EVENT_FIFO_OVERFLOW);
      if(status != SNS_DDF_SUCCESS)
        return;

      fifo_run_cnt ++;
      run_again = true;
    }

    //FIFO watermark interrupt(without overflow flag) handling
    if((0x0 == (rw_buffer[1] & 0x40)) && (rw_buffer[1] & 0x80))
    {
      total_in_fifo = rw_buffer[1] & 0x1F;
      LIS2HH_MSG_1(HIGH, "handle_irq: WM INT happenes. total_in_fifo=%u", total_in_fifo);

      //if there is no data in FIFO.
      if(0 == total_in_fifo)
        return;

      // send a FIFO Watermark event to SMGR
      status = sns_ddf_smgr_notify_event(
          state->smgr_handle,
          SNS_DDF_SENSOR_ACCEL,
          SNS_DDF_EVENT_FIFO_WM_INT);
      if(status != SNS_DDF_SUCCESS)
        return;

      //calculate start_time and cal_end for three different cases.
      if(0 == fifo_run_cnt)
      {
        if(timestamp > end_time)
          timestamp = end_time;

        //1.if more samples than fifo_wmk for the first fifo irq run, recalcuate the end timestamp of last sample.
        if(total_in_fifo > state->fifo_wmk)
        {
          if((timestamp > state->last_timestamp)&&(0 != state->last_timestamp))
          {
            //use state->last_timestamp as reliable source.
            cal_odr = (end_time - state->last_timestamp) / total_in_fifo;
            if((cal_odr < 0.5*est_odr) || (cal_odr > 1.5*est_odr))
              cal_odr = est_odr;
            start_time = state->last_timestamp + cal_odr;
          }
          else
          {
            //use timestamp as reliable source.
            cal_odr = est_odr;
            start_time = end_time - cal_odr * (total_in_fifo - 1);
          }
          cal_end = end_time;
        }
        //2.if fifo has fifo_wmk for the first fifo irq run
        else
        {
          if((timestamp > state->last_timestamp)&&(0 != state->last_timestamp))
          {
            //use state->last_timestamp as reliable source.
            cal_odr = (timestamp - state->last_timestamp) / total_in_fifo;
            if((cal_odr < 0.5*est_odr) || (cal_odr > 1.5*est_odr))
              cal_odr = est_odr;
            start_time = state->last_timestamp + cal_odr;
          }
          else
          {
            //use timestamp as reliable source.
            cal_odr = est_odr;
            start_time = timestamp - cal_odr * (total_in_fifo - 1);
          }
          cal_end = timestamp;
        }
      }
      else
      {
        //3. read whatever has in FIFO for 2nd irq run. use state->last_timestamp as reliable source.
        start_time = state->last_timestamp + cal_odr;
        cal_end = state->last_timestamp + cal_odr * total_in_fifo;
      }


      end_time = cal_end;

      //update last_timestamp
      //state->last_timestamp = end_time;

      LIS2HH_MSG_3(HIGH, "handle_irq: to call send_fifo_data, total_to_read=%u, starttime=%u, real_endtime=%u",
          total_in_fifo, start_time, end_time);

      // send all data samples in FIFO to SMGR.
      status = sns_dd_acc_lis2hh_send_fifo_data(state,
          total_in_fifo,
          start_time,
          end_time);
      if(status != SNS_DDF_SUCCESS)
        return;

      fifo_run_cnt ++;
      run_again = true;
    }

    //special case, FIFO DRI interrupt handling for watermark 1.
    if((state->fifo_enabled) && (state->fifo_int_enabled) && (1==state->fifo_wmk))
    {
      // send a FIFO Watermark event to SMGR
      status = sns_ddf_smgr_notify_event(
          state->smgr_handle,
          SNS_DDF_SENSOR_ACCEL,
          SNS_DDF_EVENT_FIFO_WM_INT);
      if(status != SNS_DDF_SUCCESS)
        return;

      LIS2HH_MSG_2(HIGH, "handle_irq: to call send_fifo_data, watermark=%u, timestamp=%u",
          state->fifo_wmk, timestamp);

      //update last_timestamp
      //state->last_timestamp = timestamp;

      status = sns_dd_acc_lis2hh_send_fifo_data(state,
          state->fifo_wmk,
          timestamp,
          timestamp);
      if(status != SNS_DDF_SUCCESS)
        return;

      //run_again = true;
    }

  }

  LIS2HH_MSG_1(HIGH, "handle_irq: DD ISR end time=%u", sns_ddf_get_timestamp());
}

/**
 * @brief Implement trigger_fifo_data API.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_trigger_fifo(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensor,
    uint16_t num_samples,
    bool trigger_now)
{
  sns_dd_acc_lis2hh_state_t *state = dd_handle;
  uint8_t total_in_fifo = 0;
  uint8_t total_to_read = 0;
  sns_ddf_time_t start_time, end_time, est_odr;

  LIS2HH_MSG_2(HIGH, "trigger_fifo: get called, num_samples=%u, trigger_now=%u",
      num_samples, trigger_now);

  if((SNS_DDF_SENSOR_GYRO != sensor) || (num_samples > STM_LIS2HH_MAX_FIFO))
  {
    return SNS_DDF_EINVALID_PARAM;
  }

  if(!trigger_now || !state->fifo_enabled)
  {
    return SNS_DDF_SUCCESS;
  }

  total_in_fifo = sns_dd_acc_lis2hh_get_fifo_sample_count(state);

  //to avoid that there is no sample in FIFO.
  if(0 == total_in_fifo)
  {
    return SNS_DDF_SUCCESS;
  }

  end_time = sns_ddf_get_timestamp();
  est_odr = sns_ddf_convert_usec_to_tick(1000000 / lis2hh_odr[state->cur_rate_idx]);

  LIS2HH_MSG_2(HIGH, "trigger_fifo: total_in_fifo=%d, end_time=%d", total_in_fifo, end_time);

  if(0 == state->last_timestamp)
    state->last_timestamp = end_time - total_in_fifo * est_odr;

  //TODO: check if possible start time is bigger than end_time.
  if(0 == num_samples)
  {
    total_to_read = total_in_fifo;

    if(total_in_fifo<32)
    {
      start_time = state->last_timestamp +
        (end_time - state->last_timestamp)/total_in_fifo;
    }
    else
    {
      start_time = end_time - 31 * est_odr;
    }
  }
  else
  {
    //calculate how many samples driver needs to provide
    total_to_read = MIN(num_samples, total_in_fifo);
    total_to_read = MIN(total_to_read, state->fifo_wmk);

    if(total_in_fifo<32)
    {
      //use state->last_timestamp as more accurate source.
      start_time = state->last_timestamp +
        (end_time - state->last_timestamp)/total_in_fifo;

      //rare case: fifo has more data samples than what SMGR wants,
      //then recalculate end_timestamp
      if(total_to_read < total_in_fifo)
      {
        end_time = state->last_timestamp +
          total_to_read * (end_time - state->last_timestamp)/total_in_fifo;
      }
    }
    else
    {
      //use end_time as more accurate source.
      start_time = end_time - 31 * est_odr;

      //rare case: fifo has more data samples than what SMGR wants,
      //then recalculate end_timestamp
      if(total_to_read < total_in_fifo)
      {
        end_time = start_time + (total_to_read - 1) * est_odr;
      }
    }
  }

  LIS2HH_MSG_3(HIGH, "trigger_fifo: to call send_fifo_data, total_to_read=%u, starttime=%u, endtime=%u",
      total_to_read, start_time, end_time);

  //if total_to_read == 1, here start_time will be same as end_time.
  return sns_dd_acc_lis2hh_send_fifo_data(state, total_to_read, start_time, end_time);
}
/**
 * LIS2HH device driver interface.
 */
sns_ddf_driver_if_s sns_dd_acc_lis2hh_if =
{
  &sns_dd_acc_lis2hh_init,
  &sns_dd_acc_lis2hh_get_data,
  &sns_dd_acc_lis2hh_set_attr,
  &sns_dd_acc_lis2hh_get_attr,
  NULL,   // handle timer
  &sns_dd_acc_lis2hh_handle_irq,
  &sns_dd_acc_lis2hh_reset,
  &sns_dd_acc_lis2hh_run_test,
  &sns_dd_acc_lis2hh_enable_sched,
  NULL,
  &sns_dd_acc_lis2hh_trigger_fifo
};
