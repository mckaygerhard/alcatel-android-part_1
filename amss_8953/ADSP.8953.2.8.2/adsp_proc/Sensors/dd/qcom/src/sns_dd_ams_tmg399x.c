/********************************************************************************
* Copyright (c) 2014, "ams AG"
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*     2. Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     3. Neither the name of "ams AG" nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/
/******************************************************************************
* Copyright (c) 2014, "ams AG"
* All rights reserved.
* THIS SOFTWARE IS PROVIDED FOR USE ONLY IN CONJUNCTION WITH AMS PRODUCTS.
* USE OF THE SOFTWARE IN CONJUNCTION WITH NON-AMS-PRODUCTS IS EXPLICITLY
* EXCLUDED.
*******************************************************************************/
/*============================================================================
 Copyright (c) 2014-2015 Qualcomm Technologies, Inc.  All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential
 ============================================================================*/
/*==============================================================================

    S E N S O R S   AMBIENT LIGHT AND PROXIMITY  D R I V E R

DESCRIPTION

   Implements the driver for the AMS ALS(Light) and Proximity Sensor
   This driver has 3 sub-modules:
   1. The common handler that provides the driver interface
   2. The light(ALS) driver that handles light data type
   3. The proximity driver that handles proximity data type

==============================================================================*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.



when         who     what, where, why
----------   ---     -----------------------------------------------------------
09/18/15     PS      Fix samples with zero lux
06/08/15     AH      Fix zero lux for initial few samples in polling mode
07/10/15     AH      Add Proxi Invalid data fix for polling mode from Vendor.
06/17/15     AH      Add fixes : Polling streaming issue, als_factor
06/08/15     FV      Add changes from QCM to fix streaming issue.
06/04/15     FV      Check ALS saturation flag, if saturated, dec gain.
06/04/15     FV      Turn off ALS when adjusting gain.
02/12/15     sh      Fix proximity alternating states when raw value is between detect
                     and release (in polling mode)
12/04/14     sd      Updated prox default threshold to include support of certain V2 MTP
09/11/14     FV      Update prox binary event detection
09/03/14     MW      Use interrupt timestamp when reporting samples
08/15/14     MW      Added auto gain control for RGB, use of als_factor, fix in
                     gain assignment in ams_set_als_gain()
02/15/14     fv      Convert tmd37x2 driver to tmg399x
11/14/13     fv      Convert tmd377x driver to tmd37x2
06/10/13     fv      Convert tmd277x driver to tmd377x
03/21/13     fv      Clean up code, change vendor name to "ams AG", add new copyright text.
12/11/09     fv      Convert Intersil driver to drive AMS TSL/TMD 377x ALS/PROX sensor
==============================================================================*/

/*============================================================================
                                INCLUDE FILES
============================================================================*/
#include "fixed_point.h"
#include "sns_ddf_attrib.h"
#include "sns_ddf_comm.h"
#include "sns_ddf_common.h"
#include "sns_ddf_driver_if.h"
#include "sns_ddf_memhandler.h"
#include "sns_ddf_signal.h"
#include "sns_ddf_smgr_if.h"
#include "sns_ddf_util.h"
#include "stdbool.h"
#include <string.h>
#if COMPILE_AT_QUALCOMM
#include "sns_log_api.h"
#include "sns_log_types.h"
#else
#include "sns_log_api_public.h"
#include "sns_log_types_public.h"
#include "qurt_elite_diag.h"
#endif

#include "ams_tmg399x.h"
#include "sns_dd_ams_tmg399x_priv.h"
#include "ams_i2c.h"

#ifdef AMS_USE_GESTURE
  #include "cal_offsets.h"
#endif

//#define MAX_ODR  40

//
//++ TMG399x ++
//
enum state_machine_sensors_e currentSensor;
bool     currentlyActiveSensors[MAX_NUM_OF_SENSORS];  // Als, Prox, Gesture, RGB, CT

// There should be a slot for each possible active DAF request
uint32_t tmg399x_dd_daf_active_reqs[MAX_DAF_REQS]
                                     = {SNS_DAF_PROX_POLLING_START_V01,
                                        SNS_DAF_GENERAL_REVERB_START_V01};

#if 0
static uint8_t const restorable_regs[] = {
    AMS_REG_ATIME,    // TMG399x_ALS_TIME,
    AMS_REG_PERS,     // TMG399x_PERSISTENCE,
    AMS_REG_PPULSE,   // TMG399x_PRX_PULSE_COUNT,
    AMS_REG_CONTROL,  // TMG399x_GAIN,
                      // TMG399x_POFFSET_NE,
};
#endif

enum numParamsToReturn_e {
    AMBIENT_PARAMS,
    PROX_PARAMS,
    RGB_PARAMS,
    CTEMP_CLR_PARAMS
};


#define NUM_ALS_SAMPLES_RET       2
#define NUM_PROX_SAMPLES_RET      2
#define NUM_RGB_SAMPLES_RET       3
#define NUM_CTEMP_CLR_SAMPLES_RET 2

static uint16_t numParamsToReturn[] = {
    NUM_ALS_SAMPLES_RET,
    NUM_PROX_SAMPLES_RET,
    NUM_RGB_SAMPLES_RET,
    NUM_CTEMP_CLR_SAMPLES_RET
};

#define AGAIN1   0
#define AGAIN4   1
#define AGAIN16  2
#define AGAIN64  3

//                                  0  1   2   3
/* static uint8_t ams_gain_value[] = { 1, 4, 16, 60 }; */

int32_t subDevId_AlsProx = -1;
int32_t subDevId_RgbCt   = -1;
int32_t subDevId_Gesture = -1;

#ifdef AMS_USE_GESTURE
#define GESTURE_FIFO_DATA_MAX_ELEMENTS  128
static uint8_t   gestureFifoData[GESTURE_FIFO_DATA_MAX_ELEMENTS];
static uint32_t  sumOfFifoBytesRead;

static bool      doCalibration;
static uint8_t   enableRegCal;
static uint8_t   gconfig4RegCal;
#endif  // AMS_USE_GESTURE

bool      firstProx;
bool      firstAls;
bool      firstGest;

#define AMS_APERS_VAL   2 // 5
#define AMS_MAX_LUX     ((32 * 1024) - 1)

/*============================================================================
                           STATIC FUNCTION PROTOTYPES
============================================================================*/
#if COMPILE_AT_QUALCOMM
sns_ddf_driver_if_s sns_ams_tmg399x_alsprx_driver_fn_list =
#else
sns_ddf_driver_if_s sns_dd_vendor_if_1 =
#endif
{
  .init              = &sns_dd_ams_tmg399x_init,
  .get_data          = &sns_dd_ams_tmg399x_get_data,
  .set_attrib        = &sns_dd_ams_tmg399x_set_attrib,
  .get_attrib        = &sns_dd_ams_tmg399x_get_attrib,
  .handle_timer      = &sns_dd_ams_tmg399x_handle_timer,
  .handle_irq        = &sns_dd_ams_tmg399x_handle_irq,
  .reset             = &sns_dd_ams_tmg399x_reset,
  .run_test          = &sns_dd_ams_tmg399x_run_test,          // NULL,  self test
  .enable_sched_data = &sns_dd_ams_tmg399x_enable_sched_data,
  .probe             = &sns_dd_ams_tmg399x_probe,             // New for combo driver
  .process_daf_req   = &sns_dd_ams_tmg399x_process_daf_req,
  .cancel_daf_trans  = &sns_dd_ams_tmg399x_cancel_daf_trans
};

#if !COMPILE_AT_QUALCOMM
sns_ddf_driver_if_s sns_dd_vendor_if_2 =
{
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL
};
#endif


//
// +++++++++++++++++++  AMS-TAOS USA Code   +++++++++++++++++++
//

//
// ------------------  AMS-TAOS USA Code   ------------------
//
// The following functions based on Sample code v 2.0.6
//

void ams_update_als_thresh
(
 sns_dd_state_t* state,
 uint16_t        als_low,
 uint16_t        als_high,
 uint8_t         apers
)
{
    sns_ddf_status_e status = SNS_DDF_SUCCESS;

    DD_MSG_3(HIGH, "ams_update_als_thresh low=%d high=%d apers=%d", als_low, als_high, apers);

    // Set ALS Interrupt Threshold low
    status = ams_i2cWriteField_r(state, AILTH, als_low);
    if ( status != SNS_DDF_SUCCESS ) {
        return;
    }

    // Set ALS Interrupt Threshold high
    status = ams_i2cWriteField_r(state, AIHTH, als_high);
    if ( status != SNS_DDF_SUCCESS ) {
        return;
    }

    ams_i2cWriteField_r(state, APERS, apers);
}

bool tmg399x_is_daf_active
(
    sns_dd_state_t*     dd_ptr
)
{
    bool is_daf_active = FALSE;
    int i;

    for (i = 0; i < MAX_DAF_REQS; i++)
    {
        if ( SNS_DAF_NO_MSG != dd_ptr->daf_reqs[i].daf_active_req )
        {
            is_daf_active = TRUE;
            break;
        }
    }
    
    return is_daf_active;
}

/*===========================================================================
FUNCTION      sns_dd_ams_tmg399x__alsprx_log

DESCRIPTION   Log the latest sensor data

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None
===========================================================================*/
static void sns_dd_ams_tmg399x__alsprx_log(
   sns_dd_state_t*  state,
   sns_ddf_sensor_e sensor_type,
   uint32_t         data1,
   uint32_t         data1_q16,
   uint32_t         data2,
   uint32_t         data2_q16,
   uint32_t         raw_data )
{
    sns_err_code_e err_code;
    sns_log_sensor_data_pkt_s* log_struct_ptr;
    uint32_t  cnvtSensorData;


    DD_MSG_0(HIGH, "LogData: ENTER");

    /* Allocate log packet */
#if COMPILE_AT_QUALCOMM
    cnvtSensorData = SNS_LOG_CONVERTED_SENSOR_DATA;
#else
    cnvtSensorData = SNS_LOG_CONVERTED_SENSOR_DATA_PUBLIC;
#endif
    err_code = sns_logpkt_malloc(cnvtSensorData,
                                 sizeof(sns_log_sensor_data_pkt_s),
                                 (void**)&log_struct_ptr);

    if ((err_code == SNS_SUCCESS) && (log_struct_ptr != NULL))
    {
        log_struct_ptr->version   = SNS_LOG_SENSOR_DATA_PKT_VERSION;
        log_struct_ptr->sensor_id = sensor_type;
        log_struct_ptr->vendor_id = SNS_DDF_VENDOR_AMS;

        /* Timestamp the log with sample time */
        log_struct_ptr->timestamp = sns_ddf_get_timestamp();

        /* Log the sensor data */
        if (sensor_type == SNS_DDF_SENSOR_AMBIENT)
        {
            log_struct_ptr->num_data_types = 3;
            log_struct_ptr->data[0] = data1;
            log_struct_ptr->data[1] = data1_q16;
            log_struct_ptr->data[2] = raw_data;
        }
        else if (SNS_DDF_SENSOR_PROXIMITY == sensor_type)
        {
            log_struct_ptr->num_data_types = 5;
            log_struct_ptr->data[0] = data1;
            log_struct_ptr->data[1] = data1_q16;
            log_struct_ptr->data[2] = data2;
            log_struct_ptr->data[3] = data2_q16;
            log_struct_ptr->data[4] = raw_data;
        }
        else if (SNS_DDF_SENSOR_RGB == sensor_type)
        {
            log_struct_ptr->num_data_types = 3;
            log_struct_ptr->data[0] = data1;
            log_struct_ptr->data[1] = data1_q16;
            log_struct_ptr->data[2] = data2;
        }
        else if (SNS_DDF_SENSOR_CT_C == sensor_type)
        {
            log_struct_ptr->num_data_types = 2;
            log_struct_ptr->data[0] = data1;
            log_struct_ptr->data[1] = data1_q16;
        }
        /* else if (SNS_DDF_SENSOR_RGB == sensor_type) */
        /* { */
        /*   log_struct_ptr->num_data_types = 5; */
        /*   log_struct_ptr->data[0] = data1; */
        /*   log_struct_ptr->data[1] = data1_q16; */
        /*   log_struct_ptr->data[2] = data2; */
        /*   log_struct_ptr->data[3] = data2_q16; */
        /*   log_struct_ptr->data[4] = raw_data; */
        /* } */
        /* else if (SNS_DDF_SENSOR_CT_C == sensor_type) */
        /* { */
        /*   log_struct_ptr->num_data_types = 5; */
        /*   log_struct_ptr->data[0] = data1; */
        /*   log_struct_ptr->data[1] = data1_q16; */
        /*   log_struct_ptr->data[2] = data2; */
        /*   log_struct_ptr->data[3] = data2_q16; */
        /*   log_struct_ptr->data[4] = raw_data; */
        /* } */

        /* Commit log (also frees up the log packet memory) */
#if COMPILE_AT_QUALCOMM
        cnvtSensorData = SNS_LOG_CONVERTED_SENSOR_DATA;
#else
        cnvtSensorData = SNS_LOG_CONVERTED_SENSOR_DATA_PUBLIC;
#endif
        err_code = sns_logpkt_commit(cnvtSensorData,
                                     log_struct_ptr);
    }
    else
    {
        DD_MSG_1(HIGH, "LogData: - logpkt_malloc failed with err: %d", err_code);
        state->dropped_logs++;
    }

    DD_MSG_0(HIGH, "LogData: EXIT");
}


/*===========================================================================

FUNCTION      sns_dd_ams_tmg399x__alsprx_prx_binary

DESCRIPTION   This function is called by the proximity common handler when proximity data
              is available

DEPENDENCIES  None

RETURN VALUE  None

SIDE EFFECT   None

===========================================================================*/
sns_ddf_status_e sns_dd_ams_tmg399x__alsprx_prx_binary
(
  sns_dd_state_t* state
)
{
    uint16_t pdata;
    uint16_t detect;
    uint16_t release;
    uint8_t from;
    uint8_t to;
    sns_ddf_status_e status = SNS_DDF_SUCCESS;

    DD_MSG_0(HIGH, "PrxBinary:");

    pdata   = state->chip.prx_info.pdata;
    DD_MSG_1(MED, "PrxBinary: pdata %d",  pdata );

    detect  = state->chip.setup.prox.detect;
    DD_MSG_1(MED, "PrxBinary: detect %d",  detect );

    release = state->chip.setup.prox.release;
    DD_MSG_1(MED, "PrxBinary: release %d",  release );

    if(state->chip.prx_info.event) // Detected
    {
        if(pdata < release)
        {
            state->chip.prx_info.event = false;
        }
    }
    else // Not detected
    {
        if(pdata > detect)
        {
            state->chip.prx_info.event = true;
        }
    }

    if(state->chip.prx_info.event)
    {
        from = state->chip.setup.prox.release;
        to   = 0xFF;
    }
    else
    {
        from = 0x00;
        to   = state->chip.setup.prox.detect;
    }

    status = ams_i2cWriteField_s(state, PITHL, from);
    status = ams_i2cWriteField_s(state, PITHH, to);

    return status;
}


static uint16_t ams_get_num_params_to_ret
(
 sns_ddf_sensor_e sensor
)
{
    uint16_t paramsToRet;


    switch(sensor)
    {
    case SNS_DDF_SENSOR_AMBIENT:
        paramsToRet = numParamsToReturn[AMBIENT_PARAMS];
        break;

    case SNS_DDF_SENSOR_PROXIMITY:
        paramsToRet = numParamsToReturn[PROX_PARAMS];
        break;

    case SNS_DDF_SENSOR_RGB:
        paramsToRet = numParamsToReturn[RGB_PARAMS];
        break;

    case SNS_DDF_SENSOR_CT_C:
        paramsToRet = numParamsToReturn[CTEMP_CLR_PARAMS];
        break;

    default:
        return SNS_DDF_EDATA_BOUND;
        break;
    }

    return paramsToRet;
}


#if 0
uint32_t ams_prox_time_us(sns_dd_state_t* state)
{
    // Prox enabled
    DD_MSG_1(MED, "PrxTime_us: %d", 2 * state->chip.Min_Integ_Time_us +
             16 * state->chip.setup.prox.pulse_count  +
             state->chip.setup.prox.ptime_us);

    return ( 2 * state->chip.Min_Integ_Time_us +
             16 * state->chip.setup.prox.pulse_count  +
             state->chip.setup.prox.ptime_us);
}
#endif


statusReg_t parseStatusReg(sns_dd_state_t* state)
{
    statusReg_t  statusReg;

    statusReg.val = state->chip.shadow.val[AMS_REG_STATUS];
    state->chip.als_info.is_saturated = statusReg.CPSAT;

    return statusReg;
}


sns_ddf_status_e ams_readDataRegisters
(
 sns_dd_state_t* state
)
{
    sns_ddf_status_e status  = SNS_DDF_SUCCESS;
    sns_ddf_status_e statusa = SNS_DDF_SUCCESS;
    sns_ddf_status_e statusc = SNS_DDF_SUCCESS;
    uint8_t          startReg;
    statusReg_t      statusReg;
    //
    // Temp store REG_STATUS .. REG_PDATA
    // 0x93 .. 0x9C
    // 0x9C - 0x93 + 1 = 10
    uint8_t  buf[10] = {0};


    DD_MSG_0(HIGH, "RdDataRegs: ENTER");

    startReg = AMS_REG_STATUS;
    status = ams_i2c_read_buf(state,
                              startReg,
                              AMS_REG_PDATA - startReg + 1,
                              &buf[0]);
    if(status != SNS_DDF_SUCCESS)
    {
        DD_MSG_0(HIGH, "RdDataRegs: could not read to buf");
        return status;
    }
    state->chip.shadow.val[AMS_REG_STATUS] = buf[0];
    statusReg = parseStatusReg(state);

    DD_MSG_1(HIGH, "RdDataRegs: statusReg: 0x%02X", statusReg.val);

    //
    // If ALS and Prox are valid, then and only then,
    // copy data read to the shadow memory
    // else
    // use the previous values in the shadow memory.
    if(statusReg.AVALID)
    {
        DD_MSG_0(HIGH, "RdDataRegs: AVALID");
#if COMPILE_AT_QUALCOMM
        memscpy(&state->chip.shadow.val[AMS_REG_CDATAL], 8, &buf[1], 8);
#else
        memcpy(&state->chip.shadow.val[AMS_REG_CDATAL], &buf[1], 8);
#endif
        statusa = ams_i2c_read_byte(state, AMS_REG_ATIME,   &state->chip.shadow.val[AMS_REG_ATIME]);
        statusc = ams_i2c_read_byte(state, AMS_REG_CONTROL, &state->chip.shadow.val[AMS_REG_CONTROL]);  // for gain
        if(statusa != SNS_DDF_SUCCESS) status = statusa;
        if(statusc != SNS_DDF_SUCCESS) status = statusc;
    }
    else
    {
        DD_MSG_0(HIGH, "RdDataRegs: ALS not valid, use previous data");
    }

    if(statusReg.PVALID)
    {
        DD_MSG_0(HIGH, "RdDataRegs: PVALID");
#if COMPILE_AT_QUALCOMM
        memscpy(&state->chip.shadow.val[AMS_REG_PDATA], 1, &buf[9], 1);
#else
        memcpy(&state->chip.shadow.val[AMS_REG_PDATA], &buf[9], 1);
#endif
        statusa = ams_i2c_read_byte(state, AMS_REG_ATIME,   &state->chip.shadow.val[AMS_REG_ATIME]);
        statusc = ams_i2c_read_byte(state, AMS_REG_CONTROL, &state->chip.shadow.val[AMS_REG_CONTROL]);  // for gain
        if(statusa != SNS_DDF_SUCCESS) status = statusa;
        if(statusc != SNS_DDF_SUCCESS) status = statusc;
    }
    else
    {
        DD_MSG_0(HIGH, "RdDataRegs: PROX not valid, use previous data");
    }

    DD_MSG_0(HIGH, "RdDataRegs: EXIT");

    return status;
}


sns_ddf_status_e ams_get_prox_data
(
 sns_dd_state_t* state
)
{
    uint8_t* tBuf;

    tBuf = &state->chip.shadow.val[AMS_REG_PDATA];

    //
    // Get Prox data
    //
    state->chip.prx_info.pdata = getWord(tBuf, 0);
    state->chip.prx_info.pdata = state->chip.shadow.val[AMS_REG_PDATA];

    DD_MSG_1(MED, "Get prox data: %d", state->chip.prx_info.pdata);

    return SNS_DDF_SUCCESS;
}


sns_ddf_status_e ams_get_als_data
(
 sns_dd_state_t* state
)
{
    double denom;
    double IRed;
    double IGrn;
    double IBlu;
    double IClr;

    //
    // Get Color data
    //
    state->chip.als_info.clear_raw = ams_getField_s(state, CDATA);
    state->chip.als_info.red_raw   = ams_getField_s(state, RDATA);
    state->chip.als_info.green_raw = ams_getField_s(state, GDATA);
    state->chip.als_info.blue_raw  = ams_getField_s(state, BDATA);
    state->chip.als_info.atime     = ams_getField_s(state, ATIME);
    state->chip.als_info.again     = ams_getField_s(state, AGAIN);

    //
    // Compute IR
    // ir = (R + G + B - C + 1) / 2
    //
    state->chip.als_info.ir =
        (state->chip.als_info.red_raw  + state->chip.als_info.green_raw +
         state->chip.als_info.blue_raw - state->chip.als_info.clear_raw + 1) >> 1;

    if (state->chip.als_info.ir < 0)
    {
        state->chip.als_info.ir = 0;
    }

    //
    // Compute irradiances in uW/cm^2
    //
    denom = (REG_TO_TIME_uS(state->chip.als_info.atime, state->chip.Min_Integ_Time_us) / 1000.0)
             * ams_tmg399x_alsGainVal[state->chip.als_info.again]; // gain * time in msec

    IClr = ((31.94/denom) * state->chip.als_info.clear_raw);
    IRed = ((30.33/denom) * state->chip.als_info.red_raw);
    IGrn = ((46.38/denom) * state->chip.als_info.green_raw);
    IBlu = ((48.63/denom) * state->chip.als_info.blue_raw);

    state->chip.als_info.clear_irradiance = FX_FLTTOFIX_Q16(IClr);
    state->chip.als_info.red_irradiance   = FX_FLTTOFIX_Q16(IRed);
    state->chip.als_info.green_irradiance = FX_FLTTOFIX_Q16(IGrn);
    state->chip.als_info.blue_irradiance  = FX_FLTTOFIX_Q16(IBlu);

    DD_MSG_1(MED, "get_als_data: clr: 0x%04X", state->chip.als_info.clear_raw);
    DD_MSG_1(MED, "get_als_data: red: 0x%04X", state->chip.als_info.red_raw);
    DD_MSG_1(MED, "get_als_data: grn: 0x%04X", state->chip.als_info.green_raw);
    DD_MSG_1(MED, "get_als_data: blu: 0x%04X", state->chip.als_info.blue_raw);

    denom *= 1000;
    DD_MSG_1(MED, "get_als_data: denom: %d", (uint32_t)denom);
    DD_MSG_1(MED, "get_als_data: ATIME: 0x%02X", state->chip.als_info.atime);
    DD_MSG_2(MED, "get_als_data: AGAIN: 0x%02X, val = %d", state->chip.als_info.again, ams_tmg399x_alsGainVal[state->chip.als_info.again]);

    DD_MSG_1(MED, "get_als_data: IrrClear: %08X", IClr*1000);
    DD_MSG_1(MED, "get_als_data: IrrRed:   %08X", IRed*1000);
    DD_MSG_1(MED, "get_als_data: IrrGreen: %08X", IGrn*1000);
    DD_MSG_1(MED, "get_als_data: IrrBlue:  %08X", IBlu*1000);

    DD_MSG_1(MED, "get_als_data: Q16:IrrClear: %08X", state->chip.als_info.clear_irradiance);
    DD_MSG_1(MED, "get_als_data: Q16:IrrRed:   %08X", state->chip.als_info.red_irradiance);
    DD_MSG_1(MED, "get_als_data: Q16:IrrGreen: %08X", state->chip.als_info.green_irradiance);
    DD_MSG_1(MED, "get_als_data: Q16:IrrBlue:  %08X", state->chip.als_info.blue_irradiance);

    return SNS_DDF_SUCCESS;
}


static sns_ddf_status_e ams_set_als_gain
(
 sns_dd_state_t* state,
 int             gain
)
{
    uint8_t gVal = 0;
    sns_ddf_status_e status = SNS_DDF_SUCCESS;

    uint8_t enableReg;

    switch (gain) {
    case 1:
        gVal = AGAIN1;
        break;
    case 4:
        gVal = AGAIN4;
        break;
    case 16:
        gVal = AGAIN16;
        break;
    case 60:
    case 64:
        gVal = AGAIN64;
        break;
    default:
        return SNS_DDF_EINVALID_PARAM;
    }

    /* Auto gain */
    /* Disable ALS when changing ALS gain, */
    /* then re-enable so that the new gain settings */
    /* take effect on the new integration cycle. */
    ams_i2c_read_byte(state,  AMS_REG_ENABLE, &enableReg);
    ams_i2c_write_byte(state, AMS_REG_ENABLE, 0);

    ams_i2c_write_spec_func(state->port_handle, regAddr[AMS_REG_AICLEAR]);
    if ( status != SNS_DDF_SUCCESS )
    {
        return status;
    }

    status = ams_i2cWriteField_s(state, AGAIN, gVal);
    if ( status != SNS_DDF_SUCCESS )
    {
        return status;
    }

    state->chip.als_info.again = gVal;

    ams_i2c_write_byte(state, AMS_REG_ENABLE, enableReg);

    return status;
}


void ams_calc_cpl
(
 tmg399x_chip *chip
)
{
    uint32_t cpl;
    uint32_t sat;
    uint8_t  atime;
    uint8_t  again;

    DD_MSG_1(MED, "calc_cpl: old: %d", chip->als_info.cpl);

    atime = chip->als_info.atime;
    again = chip->als_info.again;

    DD_MSG_1(MED, "calc_cpl:  atime: 0x%02X", chip->als_info.atime);
    DD_MSG_1(MED, "calc_cpl:  again: 0x%02X", chip->als_info.again);
    DD_MSG_1(MED, "calc_cpl: I time: %d", chip->Min_Integ_Time_us);

    cpl  = 256 - atime;
    cpl *= chip->Min_Integ_Time_us;
    cpl *= ams_tmg399x_alsGainVal[again];
    cpl /= chip->lux_coef[chip->device_index].d_factor;

    sat = min((uint32_t)MAX_ALS_VALUE, (uint32_t)(256 - atime) << 10);
    sat = sat * 8 / 10;

    chip->als_info.cpl        = cpl;
    chip->als_info.saturation = sat;

    DD_MSG_1(MED, "calc_cpl: new: %d", chip->als_info.cpl);
}

/*!
 * @brief Increment als gain
 *
 * @param[in] state - DD state handle.
 */
static void ams_inc_gain
(
 sns_dd_state_t* state
)
{
    int32_t rc;
    tmg399x_chip *chip = &state->chip;
    uint8_t       gain = chip->als_info.again;

    DD_MSG_1(MED, "inc_gain: old: %d", gain);

    if (gain > AGAIN16)
    {
        return;
    }

    if (gain < AGAIN4)
    {
        rc = ams_set_als_gain(state, ams_tmg399x_alsGainVal[AGAIN4]);
    }
    else if (gain < AGAIN16)
    {
        rc = ams_set_als_gain(state, ams_tmg399x_alsGainVal[AGAIN16]);
    }
    else
    {
        rc = ams_set_als_gain(state, ams_tmg399x_alsGainVal[AGAIN64]);
    }

    if (!rc)
    {
        ams_calc_cpl(chip);
    }
}


/*!
 * @brief Decrement als gain
 *
 * @param[in] state - DD state handle.
 */
static void ams_dec_gain
(
 sns_dd_state_t* state
)
{
    int rc;
    tmg399x_chip *chip = &state->chip;
    uint8_t       gain = chip->als_info.again;

    DD_MSG_0(MED, "dec_gain: ");

    if (gain == AGAIN1)
    {
        return;
    }

    if (gain > AGAIN16)
    {
        rc = ams_set_als_gain(state, ams_tmg399x_alsGainVal[AGAIN16]);
    }
    else if (gain > AGAIN4)
    {
        rc = ams_set_als_gain(state, ams_tmg399x_alsGainVal[AGAIN4]);
    }
    else
    {
        rc = ams_set_als_gain(state, ams_tmg399x_alsGainVal[AGAIN1]);
    }

    if (!rc)
    {
        ams_calc_cpl(chip);
    }
}

static int32_t ams_max_colorcount(tmg399x_chip *chip)
{
    int32_t t;

    /* t = 256 - chip->shadow.val[AMS_REG_ATIME]; */
    t = 256 - chip->als_info.atime;

    if (t > 63)
    {
        return 65535;
    }
    else
    {
        return ((t * 1024) - 1);
    }
}

static void ams_auto_gain_control
(
    sns_dd_state_t* state
)
{

    int quarter  = (ams_max_colorcount(&state->chip) / 4);

    if(state == NULL)
    {
        return;
    }

    /* AutoGain */
    DD_MSG_0(HIGH, "als auto gain");
    if (state->chip.als_info.clear_raw < 100)
    {
        ams_inc_gain(state);
    }

    //
    // Check to see if als is saturated or we are getting close to the upper limit
    // if so, decrement the als gain
    // When decrementing, als will be turned off so then back on after teh gain is set,
    // so that the integration cycle will use the new als gain value.
    //
    if (state->chip.als_info.is_saturated ||
    (state->chip.als_info.clear_raw > (ams_max_colorcount(&state->chip) - quarter)))
    {
        if(state->chip.als_info.is_saturated)
        {
            DD_MSG_0(MED, "auto_gain_control: SATURATED, dec ALS gain.");
            state->chip.als_info.lux = AMS_MAX_LUX;
        }
        ams_dec_gain(state);
    }
}

sns_ddf_status_e ams_get_lux
(
 sns_dd_state_t* state
)
{
    sns_ddf_status_e status = SNS_DDF_SUCCESS;
    tmg399x_chip *chip = &state->chip;
    int32_t rp1, gp1, bp1;    //, cp1;
    int32_t lux = 0;
    int32_t cct;
    uint32_t sat = chip->als_info.saturation;


    DD_MSG_1(MED, "get_lux: Clear 0x%02X", chip->als_info.clear_raw);
    DD_MSG_1(MED, "get_lux: Red   0x%02X", chip->als_info.red_raw);
    DD_MSG_1(MED, "get_lux: Green 0x%02X", chip->als_info.green_raw);
    DD_MSG_1(MED, "get_lux: Blue  0x%02X", chip->als_info.blue_raw);
    DD_MSG_1(MED, "get_lux: IR    0x%02X", chip->als_info.ir);
    DD_MSG_1(MED, "get_lux: ATIME 0x%02X", chip->als_info.atime);
    DD_MSG_1(MED, "get_lux: AGAIN 0x%02X", chip->als_info.again);
    DD_MSG_1(MED, "get_lux: CPL   %d",     chip->als_info.cpl);

    /* use time in ms get scaling factor */
    ams_calc_cpl(chip);

    /* remove ir from counts*/
    rp1 = chip->als_info.red_raw   - chip->als_info.ir;
    gp1 = chip->als_info.green_raw - chip->als_info.ir;
    bp1 = chip->als_info.blue_raw  - chip->als_info.ir;

    DD_MSG_1(MED, "get_lux: rp1   %d",     rp1);
    DD_MSG_1(MED, "get_lux: gp1   %d",     gp1);
    DD_MSG_1(MED, "get_lux: bp1   %d",     bp1);


    if (!chip->als_info.cpl)
    {
        chip->als_info.cpl = 1;
    }

    if (chip->als_info.red_raw > chip->als_info.ir)
    {
        lux += chip->lux_coef[chip->device_index].r_coef * rp1;
    }

    if (chip->als_info.green_raw > chip->als_info.ir)
    {
        lux += chip->lux_coef[chip->device_index].g_coef * gp1;
    }

    if (chip->als_info.blue_raw > chip->als_info.ir)
    {
        lux += chip->lux_coef[chip->device_index].b_coef * bp1;
    }

    //
    // Round up the lux:
    // lux *= 16
    // lux /= cpl
    // lux += (16/2)
    // lux /= 16
    //
    lux <<= ROUND_SHFT_VAL;     // *= 16
    lux /=  chip->als_info.cpl;
    lux +=  ROUND_ADD_VAL;      // += 8
    lux >>= ROUND_SHFT_VAL;     // /= 16

    if (lux < 0)
    {
        lux = 0;
    }

    chip->als_info.lux = (uint32_t) ((lux * state->nv_db.als_factor)/100);

    cct = ((chip->lux_coef[chip->device_index].ct_coef * bp1) / rp1) +
        chip->lux_coef[chip->device_index].ct_offset;

    chip->als_info.cct = (uint32_t) cct;

    DD_MSG_2(MED, "get_lux: Lux       %d  get_lux: Lux_before   %d", chip->als_info.lux, (uint32_t)lux);
    DD_MSG_1(MED, "get_lux: cct       %d", chip->als_info.cct);
    DD_MSG_1(MED, "get_lux: devIdx    %d", chip->device_index);
    DD_MSG_1(MED, "get_lux: ct_coef   %d", chip->lux_coef[chip->device_index].ct_coef);
    DD_MSG_1(MED, "get_lux: ct_offset %d", chip->lux_coef[chip->device_index].ct_offset);
    DD_MSG_1(MED, "get_lux: als_factor%d", state->nv_db.als_factor);
    DD_MSG_1(MED, "get_lux: atime 0x%02X", chip->als_info.atime);
    DD_MSG_2(MED, "get_lux: again 0x%02X, val = %d", chip->als_info.again, ams_tmg399x_alsGainVal[chip->als_info.again]);

    if (!chip->als_gain_auto)
    {
        if (chip->als_info.clear_raw <= MIN_ALS_VALUE)
        {
            lux = 0;
            chip->als_info.lux = (uint32_t) lux;  //fv ???
        }
        else if (chip->als_info.clear_raw >= sat)
        {
            lux = chip->als_info.lux;  //fv ???
        }
    }
    else
    {
        /* AutoGain */
        ams_auto_gain_control(state);
    }

    return status;
}


sns_ddf_status_e ams_prox_sensor_samples(sns_dd_state_t          *state,
                                         sns_ddf_sensor_data_s   *data_ptr,
                                         sns_ddf_memhandler_s    *memhandler,
                                         sns_ddf_sensor_sample_s *sample_data)
{
    uint32_t num_samples = NUM_PROX_SAMPLES_RET;

    DD_MSG_0(MED, "PrxSensorSmpls: ENTER");

    if(sample_data != NULL)
    {
        data_ptr->samples = sample_data;
    }
    else
    {
        DD_MSG_0(MED, "PrxSensorSmpls: Invalid Param");
        return SNS_DDF_EINVALID_PARAM;
    }

    DD_MSG_1(MED, "PrxSensorSmpls: Faraway event num: %d", SNS_PRX_FAR_AWAY);
    DD_MSG_1(MED, "PrxSensorSmpls: nearby event num: %d",  SNS_PRX_NEAR_BY);

    data_ptr->samples[0].sample = FX_CONV_Q16(state->chip.prx_info.event, 0);
    data_ptr->samples[0].status = SNS_DDF_SUCCESS;
    data_ptr->samples[1].sample = state->chip.prx_info.pdata;
    data_ptr->samples[1].status = SNS_DDF_SUCCESS;
    data_ptr->num_samples       = num_samples;

    DD_MSG_0(HIGH, "PrxSensorSmpls: PRX Data...");
    DD_MSG_3(FATAL, "PrxSensorSmpls: Data: Event:%d  Reported Data: %d  Raw Data: %d",
             state->chip.prx_info.event, data_ptr->samples[0].sample, state->chip.prx_info.pdata);
    sns_dd_ams_tmg399x__alsprx_log(state, SNS_DDF_SENSOR_PROXIMITY,
                                   state->chip.prx_info.event, data_ptr->samples[0].sample,
                                   0, 0, (uint32_t) state->chip.prx_info.pdata);

    DD_MSG_0(MED, "PrxSensorSmpls: EXIT");

    return SNS_DDF_SUCCESS;
}


static sns_ddf_status_e ams_rgb_sensor_samples
(
 sns_dd_state_t          *state,
 sns_ddf_sensor_data_s   *data_ptr,
 sns_ddf_memhandler_s    *memhandler,
 sns_ddf_sensor_sample_s *sample_data
)
{
    uint32_t num_samples = NUM_RGB_SAMPLES_RET;

    DD_MSG_1(MED, "rgb_sensor_samples: RGB var1: %d", 1161);

    if(sample_data != NULL)
    {
        data_ptr->samples = sample_data;
    }
    else
    {
        DD_MSG_1(MED, "rgb_sensor_samples: RGB var1: %d", 1163);
        return SNS_DDF_EINVALID_PARAM;
    }

    DD_MSG_1(MED, "rgb_sensor_samples: RED:   %d", state->chip.als_info.red_irradiance);
    DD_MSG_1(MED, "rgb_sensor_samples: GREEN: %d", state->chip.als_info.green_irradiance);
    DD_MSG_1(MED, "rgb_sensor_samples: BLUE:  %d", state->chip.als_info.blue_irradiance);

    data_ptr->samples[0].sample = state->chip.als_info.red_irradiance;
    data_ptr->samples[0].status = SNS_DDF_SUCCESS;
    data_ptr->samples[1].sample = state->chip.als_info.green_irradiance;
    data_ptr->samples[1].status = SNS_DDF_SUCCESS;
    data_ptr->samples[2].sample = state->chip.als_info.blue_irradiance;
    data_ptr->samples[2].status = SNS_DDF_SUCCESS;
    data_ptr->num_samples       = num_samples;

    DD_MSG_0(HIGH, "RGB Data...");
    DD_MSG_3(FATAL, "RGB Data:  Red:%d Green:%d  Blue: %d", state->chip.als_info.red_raw, state->chip.als_info.green_raw, state->chip.als_info.blue_raw);
    sns_dd_ams_tmg399x__alsprx_log(state, SNS_DDF_SENSOR_RGB,
                                   data_ptr->samples[0].sample, // should be R in irradiance
                                   data_ptr->samples[1].sample, // should be G in irradiance
                                   data_ptr->samples[2].sample, // should be B in irradiance
                                   0, 0);

    return SNS_DDF_SUCCESS;
}


static sns_ddf_status_e ams_cct_clear_sensor_samples
(
 sns_dd_state_t          *state,
 sns_ddf_sensor_data_s   *data_ptr,
 sns_ddf_memhandler_s    *memhandler,
 sns_ddf_sensor_sample_s *sample_data
)
{
    uint32_t num_samples = NUM_CTEMP_CLR_SAMPLES_RET;

    DD_MSG_0(MED, "cctSmpl: CCT_CLR");

    if(sample_data != NULL)
    {
        data_ptr->samples = sample_data;
    }
    else
    {
        DD_MSG_0(MED, "cctSmpl: CCT_CLR EXIT EINVALID_PARAM");
        return SNS_DDF_EINVALID_PARAM;
    }

    DD_MSG_1(MED, "cctSmpl: CCT: %d", state->chip.als_info.cct);
    DD_MSG_1(MED, "cctSmpl: IRR: %d", state->chip.als_info.clear_irradiance);

    data_ptr->samples[0].sample = FX_FLTTOFIX_Q16(state->chip.als_info.cct);
    data_ptr->samples[0].status = SNS_DDF_SUCCESS;
    data_ptr->samples[1].sample = state->chip.als_info.clear_irradiance;
    data_ptr->samples[1].status = SNS_DDF_SUCCESS;
    data_ptr->num_samples       = num_samples;

    DD_MSG_1(MED, "cctSmpl: CCT: %08X", data_ptr->samples[0].sample);
    DD_MSG_1(MED, "cctSmpl: IRR: %d", data_ptr->samples[1].sample);

    sns_dd_ams_tmg399x__alsprx_log(state, SNS_DDF_SENSOR_CT_C,
                                   data_ptr->samples[0].sample,
                                   data_ptr->samples[1].sample, // should be clear in irradiance
                                   0, 0, 0);

    return SNS_DDF_SUCCESS;
}


static sns_ddf_status_e ams_als_sensor_samples
(
 sns_dd_state_t           *state,
 sns_ddf_sensor_data_s    *data_ptr,
 sns_ddf_memhandler_s     *memhandler,
 sns_ddf_sensor_sample_s  *sample_data
)
{
    // Return Lux, Red, Green, Blue and Clear data
    uint32_t num_samples = NUM_ALS_SAMPLES_RET;

    DD_MSG_0(MED, "SnsrSmpls: ENTER");

    if(sample_data != NULL)
    {
        data_ptr->samples = sample_data;
    }
    else
    {
        DD_MSG_0(MED, "SnsrSmpls: EINVALID_PARAM");
        return SNS_DDF_EINVALID_PARAM;
    }

    data_ptr->samples[0].sample = FX_FLTTOFIX_Q16(state->chip.als_info.lux);
    data_ptr->samples[0].status = SNS_DDF_SUCCESS;
    data_ptr->samples[1].sample = (uint32_t)state->chip.als_info.clear_raw;
    data_ptr->samples[1].status = SNS_DDF_SUCCESS;
    data_ptr->num_samples       = num_samples;

    DD_MSG_0(HIGH, "SnsrSmpls: ALS Data...");
    DD_MSG_2(FATAL, "SnsrSmpls: TMG LIGHT Data: MLux:%d  Reported Data: %d", state->chip.als_info.lux, state->chip.als_info.clear_raw);
    DD_MSG_3(FATAL, "SnsrSmpls: TMG LIGHT Data: R:G:B: %d:%d:%d ", state->chip.als_info.red_raw, state->chip.als_info.green_raw, state->chip.als_info.blue_raw);

    sns_dd_ams_tmg399x__alsprx_log(state, SNS_DDF_SENSOR_AMBIENT,
                                   (1000 * state->chip.als_info.lux),  //state->chip.als_info.lux
                                   data_ptr->samples[0].sample,
                                   0,
                                   0,
                                   state->chip.als_info.clear_raw);

    DD_MSG_0 (HIGH, "SnsrSmpls: EXIT");

    return SNS_DDF_SUCCESS;
}


static sns_ddf_status_e ams_build_smgr_message
(
 sns_dd_state_t*        state,
 sns_ddf_memhandler_s*  memhandler,
 sns_ddf_sensor_data_s* data[],
 uint32_t               sub_dev_id,
 sns_ddf_time_t         timestamp
)
{
    uint8_t          numOfSensors = 0;
    uint16_t         paramsToRet;
    uint16_t         i;
    sns_ddf_status_e status = SNS_DDF_SUCCESS;
    uint32_t         subDevIndex = 0;
    statusReg_t      statusReg;

    /* basic sanity check */
    if(state == NULL)
    {
        DD_MSG_0(MED, "BldSmgrMsg: state NULL");

        return SNS_DDF_EINVALID_PARAM;
    }

    numOfSensors  = state->sub_dev[sub_dev_id].num_sensors;

    DD_MSG_2(HIGH, "BldSmgrMsg: numOfSensors = %d sub_dev_id = %d", numOfSensors, sub_dev_id);

    sns_ddf_sensor_sample_s* sample_ptr[2];
    sns_ddf_sensor_data_s*   sensor_data;

    if(numOfSensors == 0)
    {
        DD_MSG_0(MED, "BldSmgrMsg: numOfSensors ZERO");
        return SNS_DDF_EDATA_BOUND;
    }

    if(memhandler == NULL)
    {
        DD_MSG_0(MED, "BldSmgrMsg: memhandler = NULL, ddf_malloc");

        status = sns_ddf_malloc((void **)&sensor_data, (numOfSensors * sizeof(sns_ddf_sensor_data_s)));
        if (status != SNS_DDF_SUCCESS )
        {
            DD_MSG_0(MED, "BldSmgrMsg: Failed miserably. Can't even notify SMGR");
            /* Failed miserably. Can't even notify SMGR */
            return SNS_DDF_EFAIL;
        }
    }
    else
    {
        DD_MSG_0(MED, "BldSmgrMsg: memhandler defined, using ddf_memhandler_malloc");

        sensor_data  = sns_ddf_memhandler_malloc(memhandler, (numOfSensors * sizeof(sns_ddf_sensor_data_s)));
        if(sensor_data == NULL)
        {
            DD_MSG_1(HIGH, "BldSmgrMsg: Get Data malloc error, Size: %d", (numOfSensors * sizeof(sns_ddf_sensor_data_s)));
            return SNS_DDF_ENOMEM;
        }
        *data = sensor_data;
    }

    if((status = ams_readDataRegisters(state)) != SNS_DDF_SUCCESS)
    {
        DD_MSG_0(MED, "BldSmgrMsg: could not read data registers!");
        return status;
    }
    statusReg.val = state->chip.shadow.val[AMS_REG_STATUS];
    DD_MSG_1(MED, "BldSmgrMsg: statusReg: 0x%02X", statusReg.val);
    DD_MSG_1(MED, "BldSmgrMsg: numOfSensors: %d", numOfSensors);

    for(i = 0; i < numOfSensors; i++)
    {
        DD_MSG_2(HIGH, "BldSmgrMsg: chip.sensors[%d] = %d", i, state->chip.sensors[i]);
        DD_MSG_3(HIGH, "BldSmgrMsg: sub_dev[%d].sensors[%d] = %d", sub_dev_id, i, state->sub_dev[sub_dev_id].sensors[i]);

        sensor_data[i].sensor        = state->sub_dev[sub_dev_id].sensors[i];//state->chip.sensors[i];
        sensor_data[i].status        = SNS_DDF_SUCCESS;
        sensor_data[i].timestamp     = timestamp;
        sensor_data[i].end_timestamp = timestamp;

        paramsToRet = ams_get_num_params_to_ret(state->sub_dev[sub_dev_id].sensors[i]);//state->chip.sensors[i]);
        DD_MSG_1(HIGH, "BldSmgrMsg: paramsToRet %d", paramsToRet);
        if(memhandler == NULL)
        {
            DD_MSG_0(MED, "BldSmgrMsg: memhandler == NULL, use ddf_malloc");
            status = sns_ddf_malloc((void **)&sample_ptr[i], (paramsToRet * sizeof(sns_ddf_sensor_sample_s)));
            if(status != SNS_DDF_SUCCESS )
            {
                DD_MSG_0(MED, "BldSmgrMsg: TMG399x cannot notify smgr prx:");
                return SNS_DDF_EFAIL;
            }
        }
        else
        {
            sample_ptr[i] = sns_ddf_memhandler_malloc(memhandler, (paramsToRet * sizeof(sns_ddf_sensor_sample_s)));
            if(sample_ptr[i] == NULL)
            {
                DD_MSG_0(MED, "BldSmgrMsg: sample_ptr == NULL");
                return SNS_DDF_ENOMEM;
            }
        }

        switch(sensor_data[i].sensor)
        {
        case SNS_DDF_SENSOR_AMBIENT:
            DD_MSG_0(MED, "BldSmgrMsg: AMBIENT");

            // if AVALID NOT valid, use previous data.
            if(!statusReg.AVALID)
            {
                DD_MSG_1(HIGH, "BldSmgrMsg: ALS not valid: 0x%02X", statusReg.val);
            }
            else
            {
                status = ams_get_als_data(state);
                if(status != SNS_DDF_SUCCESS)
                {
                    DD_MSG_1(HIGH, "BldSmgrMsg: Get Data als err1: %d", status);
                    return status;
                }

                status = ams_get_lux(state);
                if(status != SNS_DDF_SUCCESS)
                {
                    DD_MSG_1(HIGH, "BldSmgrMsg: Build SMGR msg, Get Data als err2: %d", status);
                    return status;
                }
            }

            subDevIndex = subDevId_AlsProx;

            DD_MSG_0(HIGH, "BldSmgrMsg: Get Data, returning als data to smgr");

            if(currentlyActiveSensors[AMS_SENSOR_ALS])
            {
                status = ams_als_sensor_samples(state, &sensor_data[i], NULL, sample_ptr[i]);
                if(status != SNS_DDF_SUCCESS)
                {
                    DD_MSG_0(HIGH, "BldSmgrMsg: Get Data, ERROR returning als data to smgr");
                    return status;
                }
            }
        break;

        case SNS_DDF_SENSOR_PROXIMITY:
            DD_MSG_0(MED, "BldSmgrMsg: PROX");

            // if PVALID NOT valid, use previous data.
            if(!statusReg.PVALID)
            {
                DD_MSG_1(HIGH, "BldSmgrMsg: PROX not valid: 0x%02X", statusReg.val);
            }
            else
            {
                status = ams_get_prox_data(state);
                if(status != SNS_DDF_SUCCESS)
                {
                    DD_MSG_1(HIGH, "BldSmgrMsg: Get Data prx err1: %d", status);
                    return status;
                }

                status = sns_dd_ams_tmg399x__alsprx_prx_binary(state);
                if(status != SNS_DDF_SUCCESS)
                {
                    DD_MSG_1(HIGH, "BldSmgrMsg: Get Data prx err2: %d", status);
                    return status;
                }
            }

            subDevIndex = subDevId_AlsProx;

            if(currentlyActiveSensors[AMS_SENSOR_PROX])
            {
                status = ams_prox_sensor_samples(state, &sensor_data[i], NULL, sample_ptr[i]);
                if(status != SNS_DDF_SUCCESS)
                {
                    DD_MSG_0(HIGH, "BldSmgrMsg: Get Data, ERROR returning prx data to smgr");
                    return status;
                }
            }

        break;

        case SNS_DDF_SENSOR_RGB:
            // Get RGB and clear data
            status = ams_get_als_data(state);
            if(status != SNS_DDF_SUCCESS)
            {
                DD_MSG_1(HIGH, "BldSmgrMsg: Get Data als err1: %d", status);
                return status;
            }

            // Call this function for auto gain control
            ams_auto_gain_control(state);

            subDevIndex = subDevId_RgbCt;

            if(currentlyActiveSensors[AMS_SENSOR_RGB])
            {
                DD_MSG_0(HIGH, "BldSmgrMsg: returning RGB data to smgr");
                status = ams_rgb_sensor_samples(state, &sensor_data[i], NULL, sample_ptr[i]);
                if(status != SNS_DDF_SUCCESS)
                {
                    DD_MSG_0(HIGH, "BldSmgrMsg: Get Data, ERROR returning als data to smgr");
                    return status;
                }
            }

            break;

        case SNS_DDF_SENSOR_CT_C:
            // Get RGB and clear data

            status = ams_get_als_data(state);
            if(status != SNS_DDF_SUCCESS)
            {
                DD_MSG_1(HIGH, "BldSmgrMsg: Get Data als err1: %d", status);
                return status;
            }

            // Compute color temp (by-product of lux computation)
            status = ams_get_lux(state);
            if(status != SNS_DDF_SUCCESS)
            {
                DD_MSG_1(HIGH, "BldSmgrMsg: Get Data als err2: %d", status);
                return status;
            }

            subDevIndex = subDevId_RgbCt;

            if(currentlyActiveSensors[AMS_SENSOR_CT])
            {
                DD_MSG_0(HIGH, "BldSmgrMsg: Get Data, returning color temp and clear data to smgr");
                status = ams_cct_clear_sensor_samples(state, &sensor_data[i], NULL, sample_ptr[i]);
                if(status != SNS_DDF_SUCCESS)
                {
                    DD_MSG_0(HIGH, "BldSmgrMsg: Get Data, ERROR returning als data to smgr");
                    return status;
                }
            }

            break;

#ifdef AMS_USE_GESTURE
        case SNS_DDF_SENSOR_IR_GESTURE:
            DD_MSG_0(MED, "BldSmgrMsg: GES");

            subDevIndex = subDevId_Gesture;

            status = ams_gesture_sensor_samples(state, &sensor_data[i], NULL, sample_ptr[i]);
            if(status != SNS_DDF_SUCCESS)
            {
                DD_MSG_0(HIGH, "BldSmgrMsg: Get Data, ERROR returning ges data to smgr");
                return status;
            }
            break;
#endif

        default:
            break;
        }  /* // switch(sensor_data[i].sensor) */
    }  /* for(i = 0; i < numOfSensors; i++) */

    if(memhandler == NULL)
    {
        DD_MSG_0(MED, "BldSmgrMsg: exit memhandler = NULL");
        DD_MSG_0(MED, "BldSmgrMsg: exit before smgr_notify_data()");

        status = sns_ddf_smgr_notify_data(state->sub_dev[subDevIndex].smgr_handle, sensor_data, numOfSensors);

        DD_MSG_1(MED, "BldSmgrMsg: exit after smgr_notify_data() %d", status);

        for(i = 0; i < numOfSensors; i++)
        {
            DD_MSG_1(MED, "BldSmgrMsg: exit mfree(sample_ptr[%d])", i);

            sns_ddf_mfree(sample_ptr[i]);
        }

        DD_MSG_0(MED, "BldSmgrMsg: exit mfree(sensor_data)");

        sns_ddf_mfree(sensor_data);
    }

    DD_MSG_0(MED, "BldSmgrMsg: EXIT");

    return status;
}


#ifdef AMS_USE_GESTURE
void ams_setupCalibrateOffset
(
 sns_dd_state_t*  state
)
{
    ams_i2c_read_byte(state, AMS_REG_ENABLE,    &enableRegCal);
    ams_i2c_read_byte(state, AMS_REG_CONFIG_AB, &gconfig4RegCal);

    // Turn off ALS and GES
    ams_clrBits_r(state, AMS_REG_ENABLE, (AMS_MSK_AEN | AMS_MSK_GEN));
    ams_setBits_r(state, AMS_REG_ENABLE, (AMS_MSK_PEN | AMS_MSK_PIEN));

    calInitialize(state);

    doCalibration = TRUE;
    firstProx     = TRUE;
}


static sns_ddf_status_e ams_readGestureData
(
 sns_dd_state_t*  state
)
{
    sns_ddf_status_e status = SNS_DDF_SUCCESS;
    uint8_t     numOfDataSets;
    uint16_t    numOfBytes;
    uint8_t    *pFifoData;

    //
    // Read gesture data from Scorpion 6 FIFO until empty
    //

    sumOfFifoBytesRead = 0;
    numOfDataSets      = 1;
    pFifoData          = &gestureFifoData[0];

    do
    {
        // Have a Gesture interrupt, read four bytes from FIFO, then read the
        // FIFO level register to find out if ther are more data in the FIFO.

        numOfBytes = numOfDataSets << 2;   // numOfDataSets * 4;
        ams_i2c_read_buf(state, AMS_REG_GFIFO_N, numOfBytes, pFifoData);
        sumOfFifoBytesRead += numOfBytes;

        ams_i2c_read_byte(state, AMS_REG_GFLVL, &numOfDataSets);
    } while( numOfDataSets != 0 );

    // Do Calibration
    if(doCalibration)
    {
        if(! calIsGestureFinished())
        {
            ams_setField_s(state, GMODE, 1);
        }
        else  // Gesture is finished
        {
            doCalibration = false;

            ams_i2c_write_byte(state, AMS_REG_CONFIG_AB, gconfig4RegCal);
            ams_i2c_write_byte(state, AMS_REG_ENABLE,    enableRegCal);
        }
    }
    else  // if(!doCalibration)
    {
        // Send data to be processed and generate a gesture.
    }

    return status;
}
#endif  // AMS_USE_GESTURE


static void ams_read_status_reg
(
 sns_dd_state_t* state,
 sns_ddf_time_t  timestamp,
 AMS_SIG_TYPE    sig_type,
 uint32_t        sub_dev_id
)
{
    statusReg_t  statusReg;
    uint8_t  numOfSensors;
    bool     hadAlsInt;
    bool     hadProxInt;
#ifdef AMS_USE_GESTURE
    bool     hasGesInt;
    bool     gestSensor    = true;
#endif
    /* sns_ddf_sensor_e *sensors = &state->chip.sensors[0]; */
    sns_ddf_sensor_e *sensors = &state->sub_dev[sub_dev_id].sensors[0];

    bool ambientSensor = true;
    bool proxSensor    = true;


    DD_MSG_0(HIGH, "RdStatusReg: ENTER");
    DD_MSG_1(HIGH, "RdStatusReg: sig_type:   %d", sig_type);
    DD_MSG_1(HIGH, "RdStatusReg: sub_dev_id: %d", sub_dev_id);

    numOfSensors  = 0;

    ams_i2c_read_byte(state, AMS_REG_STATUS, &state->chip.shadow.val[AMS_REG_STATUS]);
    statusReg = parseStatusReg(state);

    hadAlsInt  = statusReg.AINT;
    hadProxInt = statusReg.PINT;

    DD_MSG_1(MED, "RdStatusReg: status_reg is: 0x%02X", statusReg.val);
    DD_MSG_2(MED, "RdStatusReg: has interrupts A:%d, P:%d", statusReg.AINT, statusReg.PINT);

#ifdef AMS_USE_GESTURE
    hasGesInt  = statusReg.GINT;
    DD_MSG_3(MED, "RdStatusReg: has interrupts G:%d", hasGesInt);
    DD_MSG_1(MED, "RdStatusReg: doCalibration:%d", doCalibration);
#endif

    DD_MSG_1(MED, "RdStatusReg: ALS  active: %d", currentlyActiveSensors[AMS_SENSOR_ALS]);
    DD_MSG_1(MED, "RdStatusReg: Prox active: %d", currentlyActiveSensors[AMS_SENSOR_PROX]);
    DD_MSG_1(MED, "RdStatusReg: RGB  active: %d", currentlyActiveSensors[AMS_SENSOR_RGB]);
    DD_MSG_1(MED, "RdStatusReg: CT   active: %d", currentlyActiveSensors[AMS_SENSOR_CT]);

    do
    {
        if(statusReg.AINT /* && currentlyActiveSensors[AMS_SENSOR_ALS] */)
        {
            //
            // ALS interrupt and RGB CLEAR
            //

            DD_MSG_0(MED, "RdStatusReg: in ALS");

            if(firstAls)
            {
                DD_MSG_0(MED, "RdStatusReg: firstAls");

                firstAls  = false;
                hadAlsInt = false;
            }
            else
            {
                if(ambientSensor)
                {
                    ambientSensor = false;
                    sensors[numOfSensors++] = SNS_DDF_SENSOR_AMBIENT;

                    DD_MSG_1(MED, "RdStatusReg: Als ambientSensor, numOfSensors: %d", numOfSensors);
                }
            }

            ams_i2c_write_spec_func(state->port_handle, regAddr[AMS_REG_CICLEAR]);

            DD_MSG_1(MED, "RdStatusReg: ALS int numOfSensors: %d", numOfSensors);
        }

        if(statusReg.PINT /* && currentlyActiveSensors[AMS_SENSOR_PROX] */)
        {
            //
            // PROX interrupt
            //

            DD_MSG_0(MED, "RdStatusReg: in PROX");

            if(firstProx)
            {
                DD_MSG_0(MED, "RdStatusReg: firstProx");

                firstProx = false;
                ams_i2cWriteField_r(state, PPERS,  2);
                ams_i2cWriteField_r(state, PITHL, 0x00);
                ams_i2cWriteField_r(state, PITHH, state->chip.setup.prox.detect);
            }

            if(proxSensor)
            {
                proxSensor = false;
                sensors[numOfSensors] = SNS_DDF_SENSOR_PROXIMITY;
                numOfSensors++;

                DD_MSG_1(MED, "RdStatusReg: Prox, numOfSensors: %d", numOfSensors);
            }

            ams_i2c_write_spec_func(state->port_handle, regAddr[AMS_REG_PICLEAR]);

            DD_MSG_0(MED, "RdStatusReg: out of prox");
        }

#ifdef AMS_USE_GESTURE
        if(hasGesInt)
        {
            // Read gesture data
            DD_MSG_0(MED, "RdStatusReg: in Gesture");

            ams_readGestureData(state);
        }
#endif  // AMS_USE_GESTURE

        ams_i2c_read_byte(state, AMS_REG_STATUS, &state->chip.shadow.val[AMS_REG_STATUS]);
        statusReg = parseStatusReg(state);

        hadAlsInt  |= statusReg.AINT;
        hadProxInt |= statusReg.PINT;
    } while(statusReg.AINT | statusReg.PINT);

    /* state->chip.numOfSensors  = numOfSensors; */
    state->sub_dev[sub_dev_id].num_sensors = numOfSensors;

    if(numOfSensors > 0)
    {
        ams_build_smgr_message(state, NULL, NULL, sub_dev_id, timestamp);
    }

    /* sensors = &state->chip.sensors[0]; */
    sensors = &state->sub_dev[sub_dev_id].sensors[0];

    numOfSensors  = 0;

    if(hadAlsInt && (currentlyActiveSensors[AMS_SENSOR_RGB] || currentlyActiveSensors[AMS_SENSOR_CT]))
    {
        //
        // ALS interrupt and RGB CLEAR
        //

        DD_MSG_1(MED, "RdStatusReg: ALS Interrupt  (RGB/CT): 0x%02X", AMS_INT_AINT);

        //
        // On an ALS interrupt, will read the ALS data and store it.
        // Will NOT return data (lux).
        // Lux will be returned in the timer.
        //
        if(currentlyActiveSensors[AMS_SENSOR_RGB])
        {
            sensors[numOfSensors] = SNS_DDF_SENSOR_RGB;
            DD_MSG_1(MED, "RdStatusReg: Read Stat Reg: ALS Interrupt sensor: 0x%02X", sensors[numOfSensors]);
            numOfSensors++;
            DD_MSG_1(MED, "RdStatusReg: Read Stat Reg: ALS int numOfSensors: %d", numOfSensors);
        }

        if(currentlyActiveSensors[AMS_SENSOR_CT])
        {
            sensors[numOfSensors] = SNS_DDF_SENSOR_CT_C;
            DD_MSG_1(MED, "RdStatusReg: Read Stat Reg: ALS Interrupt sensor: 0x%02X", sensors[numOfSensors]);
            numOfSensors++;
            DD_MSG_1(MED, "RdStatusReg: Read Stat Reg: ALS int numOfSensors: %d", numOfSensors);
        }
    }

    /* state->chip.numOfSensors  = numOfSensors; */
    state->sub_dev[sub_dev_id].num_sensors = numOfSensors;

    if(numOfSensors > 0)
    {
        ams_build_smgr_message(state, NULL, NULL, sub_dev_id, timestamp);
    }

    /* If ALS interrupt triggered then update ALS threholds */
    if(hadAlsInt)
    {
        uint16_t  clear_data = state->chip.als_info.clear_raw;
        uint16_t  als_low  = clear_data - (clear_data / 20);   // 20 percent of clear data
        uint16_t  als_high = clear_data + (clear_data / 20);   // 20 percent of clear data

        DD_MSG_2(MED, "RdStatusReg: clear_data: 0x%02X (%d)", clear_data, clear_data);
        DD_MSG_2(MED, "RdStatusReg: als_low:    0x%02X (%d)", als_low,    als_low);
        DD_MSG_2(MED, "RdStatusReg: als_high:   0x%02X (%d)", als_high,   als_high);

        // When als_low == als_high (low == high = 0), set the thresh to 1, 2 so it will get an interrupt.
        if(als_low == als_high)
        {
            als_low  = 0;
            als_high = 2;
        }

        DD_MSG_1(MED, "RdStatusReg: Als odr: %d", state->chip.setup.als.odr);
        DD_MSG_1(MED, "RdStatusReg: dri flg: %d", state->sub_dev[sub_dev_id].dri_enable);

        // Write new threshold values only if in dri mode
        if(state->sub_dev[sub_dev_id].dri_enable)
        {
            DD_MSG_0(MED, "RdStatusReg: dri enabled, set threshold and apers");
            ams_update_als_thresh(state, als_low, als_high, AMS_APERS_VAL);
        }
    }

    DD_MSG_0(MED, "RdStatusReg: EXIT");
}


sns_ddf_status_e sns_dd_ams_tmg399x_enable_sched_data
(
 sns_ddf_handle_t dd_handle,
 sns_ddf_sensor_e sensor,
 bool             enable
)
{
    sns_dd_state_t* state      = (sns_dd_state_t*)(((uint32_t)dd_handle) & (uint32_t)(~DD_HANDLE_ALIGN));
    uint32_t        sub_dev_id = (uint32_t)dd_handle & ((uint32_t)DD_HANDLE_ALIGN);

    uint8_t byteVal;

    sns_ddf_status_e status = SNS_DDF_SUCCESS;

    DD_MSG_0(MED, "schedData: ENTER");

    if ( (state == NULL) || (sub_dev_id >= DD_NUM_SUB_DEV) ) {
        DD_MSG_0(MED, "schedData: EXIT 1");
        return SNS_DDF_EINVALID_PARAM;
    }

    //
    // Clear interrupts
    // Clear any pending interrupts
    byteVal = regAddr[AMS_REG_AICLEAR];
    status = ams_i2c_write_spec_func(state->port_handle, byteVal);
    if ( status != SNS_DDF_SUCCESS ) {
        DD_MSG_0(MED, "schedData: EXIT 2");
        return status;
    }

    DD_MSG_2(MED, "TMG399x enable: %d var2: %d", 1100, enable);

    // set up for run.
    //

    if(enable)
    {
        if(state->sub_dev[sub_dev_id].dri_enable)
        {
            // IRQ's are enabled, register the irq
            status = sns_ddf_signal_register(state->sub_dev[sub_dev_id].interrupt_gpio,
                      dd_handle,
#if COMPILE_AT_QUALCOMM
                      &sns_ams_tmg399x_alsprx_driver_fn_list,
#else
                      &sns_dd_vendor_if_1,
#endif
                      SNS_DDF_SIGNAL_IRQ_FALLING);

            if( status != SNS_DDF_SUCCESS )
            {
                DD_MSG_0(MED, "schedData: EXIT 3");
                return status;
            }

            // Set APERS to interrupt when two values are out of range.
            //ams_i2cWriteField_s(state, APERS, AMS_APERS_VAL);
        }
        else
        {
            status =  SNS_DDF_EFAIL;
        }
    }
    else
    {
        uint16_t i;
        bool     activeSensors = false;

        for(i = 0; i < MAX_NUM_OF_SENSORS; ++i)
        {
            activeSensors |= currentlyActiveSensors[i];
        }

        if(activeSensors == false)
        {
            // If NO sensors are active then deregister irq
            sns_ddf_signal_deregister(state->sub_dev[sub_dev_id].interrupt_gpio);
        }
    }

    DD_MSG_0(MED, "schedData: EXIT 4");
    return status;
}


/*===========================================================================

  FUNCTION:   sns_dd_ams_tmg399x_get_data

===========================================================================*/
/*!
  @brief Called by the SMGR to get data

  @detail
  Requests a single sample of sensor data from each of the specified
  sensors. Data is returned immediately after being read from the
  sensor, in which case data[] is populated in the same order it was
  requested

  This driver is a pure asynchronous one, so no data will be written to buffer.
  As a result, the return value will be always PENDING if the process does
  not fail.  This driver will notify the Sensors Manager via asynchronous
  notification when data is available.

  @param[in]  dd_handle    Handle to a driver instance.
  @param[in]  sensors      List of sensors for which data is requested.
  @param[in]  num_sensors  Length of @a sensors.
  @param[in]  memhandler   Memory handler used to dynamically allocate
                           output parameters, if applicable.
  @param[out] data         Sampled sensor data. The number of elements must
                           match @a num_sensors.

  @return SNS_DDF_SUCCESS if data was populated successfully. If any of the
          sensors queried are to be read asynchronously SNS_DDF_PENDING is
          returned and data is via @a sns_ddf_smgr_data_notify() when
          available. Otherwise a specific error code is returned.

*/
/*=========================================================================*/
sns_ddf_status_e sns_dd_ams_tmg399x_get_data
(
  sns_ddf_handle_t        dd_handle,
  sns_ddf_sensor_e        sensors[],
  uint32_t                num_sensors,
  sns_ddf_memhandler_s*   memhandler,
  sns_ddf_sensor_data_s*  data[] /* ignored by this async driver */
)
{
    sns_ddf_status_e status = SNS_DDF_PENDING;
    sns_dd_state_t*  state      = (sns_dd_state_t*)(((uint32_t)dd_handle) & (uint32_t)(~DD_HANDLE_ALIGN));
    uint32_t         sub_dev_id = (uint32_t)dd_handle & ((uint32_t)DD_HANDLE_ALIGN);
//    sns_ddf_timer_s  timer_obj = 0;
    uint8_t          i = 0;
    statusReg_t      statusReg;


    DD_MSG_0(HIGH, "GetData: ENTER");
    DD_MSG_2(MED,  "GetData: num of sensors: %d, sub_dev_id: %d", num_sensors, sub_dev_id);

    if(num_sensors == 0)
    {
        DD_MSG_0(HIGH, "GetData: num_sensors == 0");
        DD_MSG_0(HIGH, "GetData: EXIT, EINVALID");

        return SNS_DDF_EINVALID_PARAM;
    }

    if ( (state == NULL) || (sub_dev_id >= DD_NUM_SUB_DEV) )
    {
        DD_MSG_2(HIGH, "GetData: state == NULL or sub_dev_id(%d) >= NUM_SUB_DEV(%d)", sub_dev_id, DD_NUM_SUB_DEV);
        DD_MSG_0(HIGH, "GetData: EXIT, EINVALID");

        return SNS_DDF_EINVALID_PARAM;
    }

    state->sub_dev[sub_dev_id].num_sensors = num_sensors;
    //chip.numOfSensors = num_sensors;

    for( i = 0; i < num_sensors; ++i)
    {
        //state->chip.sensors[i] = sensors[i];
        state->sub_dev[sub_dev_id].sensors[i] = sensors[i];
        DD_MSG_2(MED,  "GetData: sensor[%d]: %d", i, sensors[i]);
#if 0
        //fv+
        //ams_tmg399x_state_machine(state, sensors[i], (odrVal != 0));   // ((*(uint32_t*)value) != 0));

        switch(sensors[i])
        {
        case SNS_DDF_SENSOR_PROXIMITY:
            //odrVal    = state->chip.setup.prox.odr;
            timer_obj = state->sub_dev[sub_dev_id].timer_obj[0];
            break;

        case SNS_DDF_SENSOR_AMBIENT:
            //odrVal    = state->chip.setup.als.odr;
            timer_obj = state->sub_dev[sub_dev_id].timer_obj[1];
            break;

        case SNS_DDF_SENSOR_RGB:
            //odrVal    = state->chip.setup.rgb.odr;
            timer_obj = state->sub_dev[sub_dev_id].timer_obj[0];
            break;

        case SNS_DDF_SENSOR_CT_C:
            //odrVal    = state->chip.setup.ct_c.odr;
            timer_obj = state->sub_dev[sub_dev_id].timer_obj[1];
            break;

            /* case SNS_DDF_SENSOR_IR_GESTURE: */
            /*     odrVal = state->chip.setup.gesture.odr; */
            /*     break; */

        default:
            break;
        }
#endif
    }

    //fv+ DEBUG REMOVE
    ams_i2c_read_byte(state, AMS_REG_STATUS, &statusReg.val);
    DD_MSG_1(HIGH, "GetData: statusReg: 0x%02X", statusReg.val);
    //fv-

    ams_i2c_read_byte(state, AMS_REG_STATUS, &state->chip.shadow.val[AMS_REG_STATUS]);
    statusReg = parseStatusReg(state);

    DD_MSG_1(HIGH, "GetData: shadow statusReg: 0x%02X", state->chip.shadow.val[AMS_REG_STATUS]);
    DD_MSG_1(HIGH, "GetData: statusReg.CPSAT:  %d", statusReg.CPSAT);
    DD_MSG_1(HIGH, "GetData: statusReg.PGSAT:  %d", statusReg.PGSAT);
    DD_MSG_1(HIGH, "GetData: statusReg.PINT:   %d", statusReg.PINT);
    DD_MSG_1(HIGH, "GetData: statusReg.AINT:   %d", statusReg.AINT);
    DD_MSG_1(HIGH, "GetData: statusReg.PBINT:  %d", statusReg.PBINT);
    DD_MSG_1(HIGH, "GetData: statusReg.GINT:   %d", statusReg.GINT);
    DD_MSG_1(HIGH, "GetData: statusReg.PVALID: %d", statusReg.PVALID);
    DD_MSG_1(HIGH, "GetData: statusReg.AVALID: %d", statusReg.AVALID);
    DD_MSG_1(HIGH, "GetData: is_saturated:     %d", state->chip.als_info.is_saturated);

    DD_MSG_1(HIGH, "GetData: activeSensor[ALS]  = %d", currentlyActiveSensors[AMS_SENSOR_ALS]);
    DD_MSG_1(HIGH, "GetData: activeSensor[PROX] = %d", currentlyActiveSensors[AMS_SENSOR_PROX]);
    DD_MSG_1(HIGH, "GetData: activeSensor[RGB]  = %d", currentlyActiveSensors[AMS_SENSOR_RGB]);
    DD_MSG_1(HIGH, "GetData: activeSensor[RT]   = %d", currentlyActiveSensors[AMS_SENSOR_CT]);

    if(currentlyActiveSensors[AMS_SENSOR_ALS] || currentlyActiveSensors[AMS_SENSOR_PROX] ||
       currentlyActiveSensors[AMS_SENSOR_RGB] || currentlyActiveSensors[AMS_SENSOR_CT]
       )
    {
        if(firstAls)
        {
            if (statusReg.AVALID)
            {
                DD_MSG_0(MED, "GetData: first valid data");
                firstAls = false;
            }
            else
            {
                DD_MSG_0(MED, "GetData: Still no valid data to re-use");
                return SNS_DDF_EINVALID_DATA;
            }
        }

        if(firstProx)
        {
            DD_MSG_0(MED, "GetData: firstProx");

            firstProx = false;
            ams_i2cWriteField_r(state, PPERS,  2);
            ams_i2cWriteField_r(state, PITHL, 0x00);
            ams_i2cWriteField_r(state, PITHH, state->chip.setup.prox.detect);

            DD_MSG_0(MED, "GetData: EXIT");

            return SNS_DDF_EINVALID_DATA;
        }

        //
        // Return data to SMGR even if data is not valid.
        // If data is not valid it will be the same as the previous read.
        //
        status = ams_build_smgr_message(state, memhandler, data, sub_dev_id, sns_ddf_get_timestamp());
        DD_MSG_1(MED,  "GetData: build_smgr_message() status: %d", status);
    }
    else
    {
        DD_MSG_0(HIGH, "GetData: no currently active sensors");
        DD_MSG_0(HIGH, "GetData: EXIT, EINVALID");
        status = SNS_DDF_EINVALID_PARAM;
    }

    /* if(timer_obj != 0) */
    /* { */
    /*     DD_MSG_1(HIGH, "GetData: Start timer for %d ms", MAX_CYCLE_TIME_ms); */

    /*     status = sns_ddf_timer_start(timer_obj, MS_TO_US(MAX_CYCLE_TIME_ms)); */
    /*     if(status != SNS_DDF_SUCCESS) */
    /*     { */
    /*         return status; */
    /*     } */
    /* } */
    /* else */
    /* { */
    /*     return SNS_DDF_EINVALID_PARAM; */
    /* } */

    DD_MSG_1(HIGH, "GetData: EXIT, status: %X", status);
    return status;
}

static void tmg399x_handle_daf_req_timer
(
  sns_dd_state_t* dd_ptr,
  sns_ddf_timer_s* timer
)
{
  sns_ddf_status_e err_code;
  dd_daf_s* daf_req = NULL;
  int i;
  
  // Use the timer pointer to find the accompanying DAF request
  for (i = 0; i < MAX_DAF_REQS; i++)
  {
    if ( timer == &dd_ptr->daf_reqs[i].daf_timer )
    {
      daf_req = &dd_ptr->daf_reqs[i];
      break;
    }
  }
  
  // Accompanying DAF request was not found for this timer, exit out
  if ( NULL == daf_req )
  {
    DD_MSG_1(ERROR, "handle_daf_req_timer: unable to service timer 0x%x", timer);
    return;
  }

  switch ( daf_req->daf_active_req )
  {
    case SNS_DAF_GENERAL_REVERB_START_V01:
    {
      sns_daf_general_reverb_ind_v01 ind_msg;

      uint8_t *trans_id_ptr = NULL;
      if ( daf_req->daf_trans_id_valid )
      {
        trans_id_ptr = &daf_req->daf_trans_id;
      }

      // Create the DAF indication
      ind_msg.echo = daf_req->daf_echo;

      // Send the DAF indication through the SMGR
      err_code = sns_ddf_smgr_notify_daf_ind(
          SNS_DAF_GENERAL_REVERB_IND_V01,
          &ind_msg,
          sizeof(ind_msg),
          trans_id_ptr,
          daf_req->daf_conn_handle);
      if ( err_code != SNS_DDF_SUCCESS )
      {
          DD_MSG_1(ERROR, "Error sending DAF ind %d", err_code);
      }

      break;
    }

    case SNS_DAF_PROX_POLLING_START_V01:
    {
      sns_daf_prox_polling_report_v01 ind_msg;

      uint8_t *trans_id_ptr = NULL;
      if ( daf_req->daf_trans_id_valid )
      {
        trans_id_ptr = &daf_req->daf_trans_id;
      }

      // Read the Proximity Data register
      if((err_code = ams_readDataRegisters(dd_ptr)) != SNS_DDF_SUCCESS)
      {
        DD_MSG_0(MED, "could not read data registers!");
        return;
      }
      ams_get_prox_data(dd_ptr);
      
      // Create the DAF indication
      ind_msg.prox_adc = (uint32_t) dd_ptr->chip.prx_info.pdata;

      // Send the DAF indication through the SMGR
      err_code = sns_ddf_smgr_notify_daf_ind(
          SNS_DAF_PROX_POLLING_REPORT_V01,
          &ind_msg,
          sizeof(ind_msg),
          trans_id_ptr,
          daf_req->daf_conn_handle);
      if ( err_code != SNS_DDF_SUCCESS )
      {
          DD_MSG_1(ERROR, "Error sending DAF ind %d", err_code);
      }

      break;
    }

    default:
    {
      // Unknown DAF request
      DD_MSG_1(ERROR, "handle_daf_req_timer - unknown DAF request %d", daf_req->daf_active_req);
      break;
    }
  }
}


/*===========================================================================

  FUNCTION:   sns_dd_ams_tmg399x_handle_timer

===========================================================================*/
/*!
  @brief Called when the timer set by this driver has expired. This is
         the callback function submitted when initializing a timer.

  @detail
  This will be called within the context of the Sensors Manager task.
  It indicates that sensor data is ready

  @param[in] dd_handle  Handle to a driver instance.
  @param[in] arg        The argument submitted when the timer was set.

  @return None
*/
/*=========================================================================*/
void sns_dd_ams_tmg399x_handle_timer
(
  sns_ddf_handle_t dd_handle,
  void*            arg
)
{
    sns_ddf_time_t   timestamp;
    sns_dd_state_t*  state      = (sns_dd_state_t*)(((uint32_t)dd_handle) & (uint32_t)(~DD_HANDLE_ALIGN));
    uint32_t         sub_dev_id = (uint32_t)dd_handle & ((uint32_t)DD_HANDLE_ALIGN);

    DD_MSG_0(LOW, "HndlTimer: ENTER");

    timestamp = sns_ddf_get_timestamp();
    
    // Only DAF timers have non-NULL arguments
    if (arg != NULL)
    {
      tmg399x_handle_daf_req_timer(state, arg);
    }
    else
    {
      //ams_read_status_reg(state, timestamp, TIMER_TYPE, sub_dev_id);
      ams_build_smgr_message(state, NULL, NULL, sub_dev_id, timestamp);
      /* ams_build_smgr_message(state, NULL, NULL, sub_dev_id); */
    }
}


/*===========================================================================

  FUNCTION:   sns_dd_ams_tmg399x_handle_irq

===========================================================================*/
/*!
  @brief AMS Tmg399x interrupt handler

  @detail
  This function will be called within the context of the Interrupt
  Service Task (IST), *not* the ISR.

  @param[in] dd_handle  Handle to a driver instance.
  @param[in] irq        The IRQ representing the interrupt that occured.

  @return None
*/
/*=========================================================================*/
void sns_dd_ams_tmg399x_handle_irq
(
  sns_ddf_handle_t  dd_handle,
  uint32_t          gpio_num,
  sns_ddf_time_t    timestamp
)
{
    sns_dd_state_t* state      = (sns_dd_state_t*)(((uint32_t)dd_handle) & (uint32_t)(~DD_HANDLE_ALIGN));
    uint32_t        sub_dev_id = (uint32_t)dd_handle & ((uint32_t)DD_HANDLE_ALIGN);

    DD_MSG_0(MED, "IRQ: ENTER");
    DD_MSG_2(MED, "IRQ: gpio:%d, sub_dev_id:%d", gpio_num, sub_dev_id);

    ams_read_status_reg(state, timestamp, IRQ_TYPE, sub_dev_id);

    DD_MSG_0(MED, "IRQ: EXIT");
}

dd_daf_s* sns_dd_ams_tmg399x_get_active_daf_req(
    sns_dd_state_t*        dd_ptr,
    uint32_t               req_id
)
{
    dd_daf_s* daf_req = NULL;
    int i;

    for (i = 0; i < MAX_DAF_REQS; i++)
    {
        if ( req_id == tmg399x_dd_daf_active_reqs[i] )
        {
            daf_req = &dd_ptr->daf_reqs[i];
            break;
        }
    }

    return daf_req;
}

sns_ddf_status_e sns_dd_ams_tmg399x_process_daf_req(
    sns_ddf_handle_t       dd_handle,
    uint32_t               req_id,
    const void*            req_msg,
    uint32_t               req_size,
    sns_ddf_memhandler_s*  memhandler,
    void**                 resp_msg,
    uint32_t*              resp_size,
    const uint8_t*         trans_id_ptr,
    void*                  conn_handle)
{
    sns_ddf_status_e   err_code = SNS_DDF_SUCCESS;
    sns_dd_state_t*    dd_ptr;
    uint32_t           sub_dev_idx;

    if ( (dd_handle == NULL) || (memhandler == NULL) )
    {
         DD_MSG_0(ERROR, "process_daf_req handle is NULL");
         return SNS_DDF_EINVALID_PARAM;
    }
    
    sub_dev_idx = (uint32_t)dd_handle & ((uint32_t)DD_HANDLE_ALIGN);
    dd_ptr = (sns_dd_state_t*)(((uint32_t)dd_handle) & (uint32_t)(~DD_HANDLE_ALIGN));

    if (sub_dev_idx >= DD_NUM_SUB_DEV)
    {
       return SNS_DDF_EINVALID_PARAM;
    }

    DD_MSG_1(HIGH, "process_daf_req: req_id %d", req_id);

    // Set default values
    *resp_size = 0;

    // All of the DAF message processing happens in this switch statement
    switch (req_id)
    {
        case SNS_DAF_GENERAL_WHO_AM_I_V01:
        {
            DD_MSG_0(HIGH, "SNS_DAF_GENERAL_WHO_AM_I_V01");

            uint16_t deviceId = 0;
            sns_daf_general_who_am_i_resp_v01* resp_msg_ptr;

            // conn_handle is not used for this message since this message does not make use of indications
            (void) conn_handle;

            // No need to validate the request message. The message should be empty.

            // Read the WHO AM I (ID) register and return the information in the response.
            deviceId = ams_getField_r(dd_ptr, ID, &err_code);
            if ( err_code != SNS_DDF_SUCCESS )
            {
                DD_MSG_1(ERROR, "Error reading port %d", err_code);
                break;
            }

            // Allocate the response message and fill it in
            resp_msg_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_daf_general_who_am_i_resp_v01));
            if (resp_msg_ptr == NULL)
            {
              err_code = SNS_DDF_ENOMEM;
              DD_MSG_1(ERROR, "daf error - memhandler %d", err_code);
              break;
            }

            resp_msg_ptr->hardware_id = (uint32_t) deviceId;
            resp_msg_ptr->firmware_id = (uint32_t) 0x1000;

            DD_MSG_2(HIGH, "DD - hardware ID %d (0x%08x)", resp_msg_ptr->hardware_id, resp_msg_ptr->hardware_id);
            DD_MSG_2(HIGH, "DD - firmware ID %d (0x%08x)", resp_msg_ptr->firmware_id, resp_msg_ptr->firmware_id);

            *resp_msg = resp_msg_ptr;
            *resp_size = sizeof(sns_daf_general_who_am_i_resp_v01);

            err_code = SNS_DDF_SUCCESS;
            break;
        }
        case SNS_DAF_GENERAL_ECHO_V01:
        {
            DD_MSG_0(HIGH, "SNS_DAF_GENERAL_ECHO_V01");

            const sns_daf_general_echo_req_v01 *req_msg_ptr = req_msg;
            sns_daf_general_echo_resp_v01 *resp_msg_ptr;

            // Allocate the response message and fill it in
            resp_msg_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_daf_general_echo_resp_v01));
            if (resp_msg_ptr == NULL)
            {
              err_code = SNS_DDF_ENOMEM;
              DD_MSG_1(ERROR, "daf error - memhandler %d", err_code);
              break;
            }

            resp_msg_ptr->echo = req_msg_ptr->echo;

            *resp_msg = resp_msg_ptr;
            *resp_size = sizeof(sns_daf_general_echo_resp_v01);

            err_code = SNS_DDF_SUCCESS;
            break;
        }
        case SNS_DAF_GENERAL_REVERB_START_V01:
        {
            DD_MSG_0(HIGH, "SNS_DAF_GENERAL_REVERB_START_V01");

            const sns_daf_general_reverb_start_req_v01 *req_msg_ptr = req_msg;
            sns_daf_general_reverb_start_resp_v01 *resp_msg_ptr;

            dd_daf_s* daf_req_ptr = sns_dd_ams_tmg399x_get_active_daf_req(dd_ptr, req_id);
            // Make sure there's a slot for the active DAF request and that it is not currently occupied
            if ( NULL == daf_req_ptr || SNS_DAF_NO_MSG != daf_req_ptr->daf_active_req )
            {
              return SNS_DDF_EINVALID_PARAM;
            }

            // Save the DAF request ID, this marks the DAF request as "active"
            daf_req_ptr->daf_active_req = req_id;

            // Save the conn_handle
            daf_req_ptr->daf_conn_handle = conn_handle;

            // Save the trans_id, if it exists
            if (NULL != trans_id_ptr)
            {
              daf_req_ptr->daf_trans_id_valid = TRUE;
              daf_req_ptr->daf_trans_id = *trans_id_ptr;
            }
            else
            {
              daf_req_ptr->daf_trans_id_valid = FALSE;
            }

            daf_req_ptr->daf_echo = req_msg_ptr->echo;
            
            // If the DAF timer hasn't been initialized, initialize it
            if ( daf_req_ptr->daf_timer == NULL )
            {
                err_code = sns_ddf_timer_init(&daf_req_ptr->daf_timer,
                              (sns_ddf_handle_t)((uint32_t)dd_ptr | sub_dev_idx),
                              &sns_ams_tmg399x_alsprx_driver_fn_list,
                              (void *)&daf_req_ptr->daf_timer,
                              TRUE);
                if ( err_code != SNS_DDF_SUCCESS )
                {
                  return err_code;
                }
            }

            // Start a periodic timer
            err_code = sns_ddf_timer_start(daf_req_ptr->daf_timer, req_msg_ptr->reverb_period);
            if ( err_code != SNS_DDF_SUCCESS )
            {
              DD_MSG_1(ERROR, "daf error - timer_start %d", err_code);
              break;
            }

            // PEND: Consider removing this call. The DAF General Reverb request
            //       doesn't require the power rails to be on.
            // Notify the SMGR of a DAF Request becoming active
            err_code = sns_ddf_smgr_notify_event(
                dd_ptr->sub_dev[sub_dev_idx].smgr_handle,
                dd_ptr->sub_dev[sub_dev_idx].sensors[0],
                SNS_DDF_EVENT_DAF_ACTIVE);
            if ( err_code != SNS_DDF_SUCCESS )
            {
              DD_MSG_1(ERROR, "daf error - smgr_notify_event %d", err_code);
              break;
            }

            // Allocate the response message and fill it in
            resp_msg_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_daf_general_reverb_start_resp_v01));
            if (resp_msg_ptr == NULL)
            {
              err_code = SNS_DDF_ENOMEM;
              DD_MSG_1(ERROR, "daf error - memhandler %d", err_code);
              break;
            }

            resp_msg_ptr->echo = req_msg_ptr->echo;

            DD_MSG_2(HIGH, "DD - echo %d reverb_period %d", req_msg_ptr->echo, req_msg_ptr->reverb_period);

            *resp_msg = resp_msg_ptr;
            *resp_size = sizeof(sns_daf_general_reverb_start_resp_v01);

            err_code = SNS_DDF_SUCCESS;
            break;
        }
        case SNS_DAF_GENERAL_REVERB_STOP_V01:
        {
            DD_MSG_0(HIGH, "SNS_DAF_GENERAL_REVERB_STOP_V01");

            uint32_t active_req_id = SNS_DAF_GENERAL_REVERB_START_V01;

            dd_daf_s* daf_req_ptr = sns_dd_ams_tmg399x_get_active_daf_req(dd_ptr, active_req_id);
            // Make sure there's a slot for the DAF request and that it is currently active
            if ( NULL == daf_req_ptr || active_req_id != daf_req_ptr->daf_active_req )
            {
              return SNS_DDF_EINVALID_PARAM;
            }
            
            // Cancel the DAF request timer
            err_code = sns_ddf_timer_cancel(daf_req_ptr->daf_timer);
            if ( err_code != SNS_DDF_SUCCESS )
            {
              DD_MSG_1(ERROR, "daf error - timer cancel %d", err_code);
              break;
            }
            
            // Mark the DAF request as disabled
            daf_req_ptr->daf_active_req = SNS_DAF_NO_MSG;

            // Notify the SMGR if all DAF Requests are inactive
            if ( FALSE == tmg399x_is_daf_active(dd_ptr) )
            {
              err_code = sns_ddf_smgr_notify_event(
                  dd_ptr->sub_dev[sub_dev_idx].smgr_handle,
                  dd_ptr->sub_dev[sub_dev_idx].sensors[0],
                  SNS_DDF_EVENT_DAF_INACTIVE);
              if ( err_code != SNS_DDF_SUCCESS )
              {
                DD_MSG_1(ERROR, "daf error - smgr_notify_event %d", err_code);
                break;
              }
            }

            err_code = SNS_DDF_SUCCESS;
            break;
        }
        case SNS_DAF_PROX_POLLING_START_V01:
        {
          DD_MSG_0(HIGH, "SNS_DAF_PROX_POLLING_START_V01");

          uint32_t sampling_period;
          const sns_daf_prox_polling_start_req_v01 *req_msg_ptr = req_msg;
          sns_daf_prox_polling_start_resp_v01 *resp_msg_ptr;

          dd_daf_s* daf_req_ptr = sns_dd_ams_tmg399x_get_active_daf_req(dd_ptr, req_id);
          // Make sure there's a slot for the active DAF request and that it is not currently occupied
          if ( NULL == daf_req_ptr || SNS_DAF_NO_MSG != daf_req_ptr->daf_active_req )
          {
            // If daf_active_req is not 0, then that means there's currently an active DAF request
            // so this one can't be serviced
            return SNS_DDF_EINVALID_PARAM;
          }

          // Save the DAF request ID
          daf_req_ptr->daf_active_req = req_id;

          // Save the conn_handle
          daf_req_ptr->daf_conn_handle = conn_handle;

          // Save the trans_id, if it exists
          if (NULL != trans_id_ptr)
          {
            daf_req_ptr->daf_trans_id_valid = TRUE;
            daf_req_ptr->daf_trans_id = *trans_id_ptr;
          }
          else
          {
            daf_req_ptr->daf_trans_id_valid = FALSE;
          }

          // Verify the sampling period and adjust it, if necessary
          sampling_period = req_msg_ptr->sampling_period_us;

          // If the DAF timer hasn't been initialized, initialize it
          if ( daf_req_ptr->daf_timer == NULL )
          {
              err_code = sns_ddf_timer_init(&daf_req_ptr->daf_timer,
                            (sns_ddf_handle_t)((uint32_t)dd_ptr | sub_dev_idx),
                            &sns_ams_tmg399x_alsprx_driver_fn_list,
                            (void *)&daf_req_ptr->daf_timer,
                            TRUE);
              if ( err_code != SNS_DDF_SUCCESS )
              {
                return err_code;
              }
          }

          // Start a periodic timer
          err_code = sns_ddf_timer_start(daf_req_ptr->daf_timer, sampling_period);
          if ( err_code != SNS_DDF_SUCCESS )
          {
            DD_MSG_1(ERROR, "daf error - timer_start %d", err_code);
            break;
          }

          // Notify the SMGR of a DAF Request becoming active
          err_code = sns_ddf_smgr_notify_event(
              dd_ptr->sub_dev[sub_dev_idx].smgr_handle,
              dd_ptr->sub_dev[sub_dev_idx].sensors[0],
              SNS_DDF_EVENT_DAF_ACTIVE);
          if ( err_code != SNS_DDF_SUCCESS )
          {
            DD_MSG_1(ERROR, "daf error - smgr_notify_event %d", err_code);
            break;
          }

          // Enable the sensor
          err_code = ams_tmg399x_state_machine(dd_ptr, SNS_DDF_SENSOR_PROXIMITY, true);
          if ( err_code != SNS_DDF_SUCCESS )
          {
            DD_MSG_1(ERROR, "daf error - enabling sensor %d", err_code);
            break;
          }

          // Allocate the response message and fill it in
          resp_msg_ptr = sns_ddf_memhandler_malloc(memhandler, sizeof(sns_daf_prox_polling_start_resp_v01));
          if (resp_msg_ptr == NULL)
          {
            err_code = SNS_DDF_ENOMEM;
            DD_MSG_1(ERROR, "daf error - memhandler %d", err_code);
            break;
          }

          resp_msg_ptr->actual_sampling_period_us = sampling_period;

          *resp_msg = resp_msg_ptr;
          *resp_size = sizeof(sns_daf_prox_polling_start_resp_v01);

          err_code = SNS_DDF_SUCCESS;
          break;
        }
        case SNS_DAF_PROX_POLLING_STOP_V01:
        {
            DD_MSG_0(HIGH, "SNS_DAF_PROX_POLLING_STOP_V01");
            
            uint32_t active_req_id = SNS_DAF_NO_MSG;
            if ( SNS_DAF_PROX_POLLING_STOP_V01 == req_id )
            {
              active_req_id = SNS_DAF_PROX_POLLING_START_V01;
            }

            dd_daf_s* daf_req_ptr = sns_dd_ams_tmg399x_get_active_daf_req(dd_ptr, active_req_id);
            // Make sure there's a slot for the DAF request and that it is currently active
            if ( NULL == daf_req_ptr || SNS_DAF_NO_MSG == daf_req_ptr->daf_active_req )
            {
              return SNS_DDF_EINVALID_PARAM;
            }
            
            // Cancel the DAF request timer
            err_code = sns_ddf_timer_cancel(daf_req_ptr->daf_timer);
            if ( err_code != SNS_DDF_SUCCESS )
            {
              DD_MSG_1(ERROR, "daf error - timer cancel %d", err_code);
              break;
            }
            
            // Mark the DAF request as disabled
            daf_req_ptr->daf_active_req = SNS_DAF_NO_MSG;

            // Notify the SMGR if all DAF Requests are inactive
            if ( FALSE == tmg399x_is_daf_active(dd_ptr) )
            {
              err_code = sns_ddf_smgr_notify_event(
                  dd_ptr->sub_dev[sub_dev_idx].smgr_handle,
                  dd_ptr->sub_dev[sub_dev_idx].sensors[0],
                  SNS_DDF_EVENT_DAF_INACTIVE);
              if ( err_code != SNS_DDF_SUCCESS )
              {
                DD_MSG_1(ERROR, "daf error - smgr_notify_event %d", err_code);
                break;
              }
            }

            err_code = SNS_DDF_SUCCESS;
            break;
        }
        default:
        {
            // Unsupported DAF request
            err_code = SNS_DDF_EINVALID_DAF_REQ;
            break;
        }
    }

    return err_code;
}

void sns_dd_ams_tmg399x_cancel_daf_trans(
    sns_ddf_handle_t       dd_handle,
    void*                  conn_handle)
{
  sns_ddf_status_e   err_code = SNS_DDF_SUCCESS;
  sns_dd_state_t*    dd_ptr;
  uint32_t           sub_dev_idx;
  dd_daf_s*          daf_req_ptr;
  int i;

  if ( dd_handle == NULL )
  {
       DD_MSG_0(ERROR, "cancel_daf_trans handle is NULL");
       return;
  }

  sub_dev_idx = (uint32_t)dd_handle & ((uint32_t)DD_HANDLE_ALIGN);
  dd_ptr = (sns_dd_state_t*)(((uint32_t)dd_handle) & (uint32_t)(~DD_HANDLE_ALIGN));

  if (sub_dev_idx >= DD_NUM_SUB_DEV)
  {
     return;
  }

  DD_MSG_1(HIGH, "cancel_daf_trans: conn_handle %d", conn_handle);

  // Iterate through the array of DAF requests and disable the ones with matching conn_handles
  for ( i = 0; i < MAX_DAF_REQS; i++ )
  {
    daf_req_ptr = &dd_ptr->daf_reqs[i];
  
    // If there is a DAF request that is active for the given conn_handle
    if ( (SNS_DAF_NO_MSG != daf_req_ptr->daf_active_req) &&
         (daf_req_ptr->daf_conn_handle == conn_handle) )
    {
      // Mark the DAF Request as deactivated
      daf_req_ptr->daf_active_req = SNS_DAF_NO_MSG;
      
      // Cancel the DAF request timer
      err_code = sns_ddf_timer_cancel(daf_req_ptr->daf_timer);
      if ( err_code != SNS_DDF_SUCCESS )
      {
        DD_MSG_1(ERROR, "cancel_daf_trans: error canceling timer %d", err_code);
      }
    }
  }
  
  // Notify the SMGR if all DAF Requests are inactive
  if ( FALSE == tmg399x_is_daf_active(dd_ptr) )
  {
    // Notify the SMGR of a DAF Request becoming inactive
    err_code = sns_ddf_smgr_notify_event(
        dd_ptr->sub_dev[sub_dev_idx].smgr_handle,
        dd_ptr->sub_dev[sub_dev_idx].sensors[0],
        SNS_DDF_EVENT_DAF_INACTIVE);
    if ( err_code != SNS_DDF_SUCCESS )
    {
      DD_MSG_1(ERROR, "cancel_daf_trans: error notifying SMGR %d", err_code);
    }
  }
}