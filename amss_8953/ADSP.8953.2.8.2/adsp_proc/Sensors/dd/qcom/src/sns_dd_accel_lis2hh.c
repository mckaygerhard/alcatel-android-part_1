/******************** (C) COPYRIGHT 2014 STMicroelectronics ********************
 *
 * File Name         : sns_dd_accel_lis2hh.c
 * Authors           : Karimuddin Sayed
 * Version           : V 3.0.1
 * Date              : 10/28/2015
 * Description       : LIS2HH Accelerometer driver source file
 *
 ********************************************************************************
 * Copyright (c) 2014, STMicroelectronics.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *     2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     3. Neither the name of the STMicroelectronics nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************
 * REVISON HISTORY
 *
 * VERSION | DATE          | DESCRIPTION
 * 3.0.1   | 10/28/2015    | Always update range bits when requested
 * 3.0.0   | 04/02/2015    | uImage supported changes
 * 2.0.0   | 12/10/2014    | FIFO mode driver created.
 * 1.1.0   | 07/23/2014    | Deleted obsolete lowpass frequency interface, use attribute ODR.
 * 1.0.0   | 08/21/2013    | Created.
 *******************************************************************************/

#include "sns_dd_accel_lis2hh.h"
#include "sns_dd_inertial_test.h"

/*------------------------------------------------------
  Access to externalized functions.
  ------------------------------------------------------*/
extern sns_ddf_status_e sns_dd_acc_lis2hh_enable_fifo(
    sns_ddf_handle_t dd_handle,
    uint16_t watermark);
extern sns_ddf_status_e sns_dd_acc_lis2hh_disable_fifo(
    sns_ddf_handle_t dd_handle);


#if DUMP_REGISTER_DATA
extern void dump_register_single(sns_ddf_handle_t  dd_handle, uint8_t reg, uint8_t size);
extern void dump_registers(sns_ddf_handle_t  dd_handle);
#endif
extern const sns_ddf_odr_t lis2hh_odr[STM_LIS2HH_ODR_NUM];

extern sns_ddf_driver_if_s sns_dd_acc_lis2hh_if;
/**
 * Accelerometer LIS2HH ranges array for setting attribute
 */
stm_lis2hh_range lis2hh_ranges[STM_LIS2HH_RANGE_NUM]={
  STM_LIS2HH_RANGE_2G,
  STM_LIS2HH_RANGE_4G,
  STM_LIS2HH_RANGE_8G
};

/**
 * Accelerometer LIS2HH sampling frequencies array for getting attribute
 */
sns_ddf_lowpass_freq_t lis2hh_freqs[STM_LIS2HH_ODR_NUM] = {
  FX_FLTTOFIX_Q16(5),
  FX_FLTTOFIX_Q16(25),
  FX_FLTTOFIX_Q16(50),
  FX_FLTTOFIX_Q16(100),
  FX_FLTTOFIX_Q16(200),
  FX_FLTTOFIX_Q16(400)
};


/**
 * Accelerometer LIS2HH sampling frequencies array for setting attribute
 */
stm_lis2hh_odr lis2hh_odr_reg_values[STM_LIS2HH_ODR_NUM] = {
  STM_LIS2HH_ODR10,
  STM_LIS2HH_ODR50,
  STM_LIS2HH_ODR100,
  STM_LIS2HH_ODR200,
  STM_LIS2HH_ODR400,
  STM_LIS2HH_ODR800
};

/**
 * @brief write reg wrapper for write_port
 *
 * @param[in]  dd_handle   Handle to a driver instance.
 * @param[in]  reg_addr    The array of address
 * @param[in]  reg_value   The araay of values to update
 * @param[in]  bytes       The number of bytes in a array
 * @param[in]  mask        The mask of this register to specify which part to be updated
 *                         To be used only to update single register
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
sns_ddf_status_e sns_dd_lis2hh_write_reg(
    sns_ddf_handle_t dd_handle,
    uint8_t reg_addr,
    uint8_t* reg_value,
    uint8_t size,
    uint8_t mask)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer = 0;
  uint8_t rw_bytes = 0;

  if((size > 1) || (mask == 0xFF) || (mask == 0x00)) {
    status = sns_ddf_write_port(
        state->port_handle,
        reg_addr,
        reg_value,
        size,
        &rw_bytes);
    if(status != SNS_DDF_SUCCESS) {
      LIS2HH_MSG_E_1(ERROR, "write_port failed- status (%d)",status);
      return status;
    }
    if(rw_bytes!= size) {
      LIS2HH_MSG_E_2(ERROR, "write_port failed- write bytes not match - rw_byest (%d) size (%d)",rw_bytes, size);
      return SNS_DDF_EBUS;
    }
  } else {
    // read current value from this register
    status = sns_ddf_read_port(
        state->port_handle,
        reg_addr,
        &rw_buffer,
        1,
        &rw_bytes);
    if(status != SNS_DDF_SUCCESS) {
      LIS2HH_MSG_E_1(ERROR, "read_port failed- status (%d)",status);
      return status;
    }
    if(rw_bytes != 1) {
      LIS2HH_MSG_E_1(ERROR, "read_port failed-  read_count  does not match - read_count (%d)",rw_bytes);
      return SNS_DDF_EBUS;
    }

    // generate new value
    rw_buffer = (rw_buffer & (~mask)) | (*reg_value & mask);

    // write new value to this register
    status = sns_ddf_write_port(
        state->port_handle,
        reg_addr,
        &rw_buffer,
        1,
        &rw_bytes);
    if(status != SNS_DDF_SUCCESS) {
      LIS2HH_MSG_E_1(ERROR, "write_port failed- status (%d)",status);
      return status;
    }
    if(rw_bytes != 1) {
      LIS2HH_MSG_E_1(ERROR, "write_port failed- write bytes not match - rw_byest (%d)",rw_bytes);
      return SNS_DDF_EBUS;
    }
  }
#if  DUMP_REGISTER_DATA
  LIS2HH_MSG_3(HIGH, "reg_addr (0x%x) reg_val (0x%x) mask (0x%x)", reg_addr, reg_value[0], mask);
  dump_register_single(dd_handle, reg_addr, size);
#endif
  return SNS_DDF_SUCCESS;
}
#if 0
/**
 * @brief Check ZYXDA in STATUS register, return data ready flag.
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 * @param[out] data_flag      data ready flag to set.
 *                            true:  a new set of data is available.
 *                            false: a new set of data is not yet available.
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
static sns_ddf_status_e sns_dd_acc_lis2hh_is_dataready(
    sns_ddf_handle_t dd_handle,
    bool *data_flag)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer = 0;
  uint8_t rw_bytes = 0;

  // read status register
  status = sns_ddf_read_port(
      state->port_handle,
      STM_LIS2HH_STATUS_REG_A,
      &rw_buffer,
      1,
      &rw_bytes);
  if(status != SNS_DDF_SUCCESS)
    return status;
  if(rw_bytes != 1)
    return SNS_DDF_EBUS;

  if((rw_buffer&0x08) > 0)
    *data_flag = true;
  else
    *data_flag = false;

  return SNS_DDF_SUCCESS;
}
#endif

/**
 * @brief Find the matched internal ODR for desired ODR.

 *
 * @param[in]  desired_rate      New desired ODR.
 * @param[out] new_rate          Matched internal ODR.
 * @param[out] new_index         The index of matched internal ODR in lis3dsh_odr.
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
sns_ddf_status_e  sns_dd_acc_lis2hh_match_odr(
    sns_ddf_odr_t desired_rate,
    uint32_t *new_index)
{
  uint8_t idx;

  if((0 == desired_rate) || (desired_rate > STM_LIS2HH_MAX_ODR))
    return SNS_DDF_EINVALID_PARAM;

  for(idx=0; idx<STM_LIS2HH_ODR_NUM; idx++)
  {
    if(desired_rate <= lis2hh_odr[idx])
      break;
  }

  if (idx >= STM_LIS2HH_ODR_NUM)
    return SNS_DDF_EINVALID_PARAM;

  *new_index = idx;

  return SNS_DDF_SUCCESS;
}

/**
 * @brief Performs a set data sampling rate operation
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 * @param[in]  data_rate       Data sampling rate settings for LIS2HH
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
static sns_ddf_status_e sns_dd_acc_lis2hh_set_datarate(
    sns_ddf_handle_t dd_handle,
    sns_ddf_odr_t data_rate)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t write_buffer = 0;
  uint32_t new_rate_index;
  uint32_t samples_to_discard;
  uint32_t delay = 0;

  if(data_rate > 0) {
    status =  sns_dd_acc_lis2hh_match_odr(data_rate,
        &new_rate_index);
    if(SNS_DDF_SUCCESS != status)
      return SNS_DDF_EINVALID_PARAM;

    if(state->cur_rate_idx == new_rate_index)
      return SNS_DDF_SUCCESS;
    // Configure ODR in CTRL_REG1
    write_buffer = lis2hh_odr_reg_values[new_rate_index];
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG1_A,
        &write_buffer,
        1,
        0xFF);
    if(status != SNS_DDF_SUCCESS)
      return status;

    state->cur_rate_idx = new_rate_index;
    samples_to_discard = 2;
    delay = samples_to_discard *
      (sns_ddf_convert_usec_to_tick ( 1000000 / lis2hh_odr[new_rate_index])); /* in microseconds */

    state->odr_settled_ts =
      sns_ddf_get_timestamp() + delay;
  } else {
    if(state->cur_rate_idx == -1)
      return SNS_DDF_SUCCESS;
    write_buffer = STM_LIS2HH_ODR_OFF;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG1_A,
        &write_buffer,
        1,
        0x70);
    if(status != SNS_DDF_SUCCESS)
      return status;
    state->cur_rate_idx = -1;
  }

  return SNS_DDF_SUCCESS;
}

/**
 * @brief Performs a set dynamic range operation
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 * @param[in]  range          Dynamic range settings for LIS2HH
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
static sns_ddf_status_e sns_dd_acc_lis2hh_set_range(
    sns_ddf_handle_t dd_handle,
    int8_t range_idx)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t write_buffer = 0;
  stm_lis2hh_sstvt sstvt;
  int8_t temp_rate_idx = -1;

  if((range_idx < 0) || (range_idx >= STM_LIS2HH_RANGE_NUM))  {
    LIS2HH_MSG_E_1(ERROR, "SET_GYR_RANGE - invalid range_idx - (%d)",range_idx);
    return SNS_DDF_EINVALID_PARAM;
  }
  LIS2HH_MSG_1(LOW, "SET_ACC_RANGE - set range_idx - (%d)",range_idx);

  //set sensor sensitivity for data conversion
  switch (lis2hh_ranges[range_idx]) {
    case STM_LIS2HH_RANGE_2G:
      sstvt = STM_LIS2HH_SSTVT_2G;
      break;
    case STM_LIS2HH_RANGE_4G:
      sstvt = STM_LIS2HH_SSTVT_4G;
      break;
    case STM_LIS2HH_RANGE_8G:
      sstvt = STM_LIS2HH_SSTVT_8G;
      break;
    default:
      return SNS_DDF_EINVALID_PARAM;
  }

  // Configure CTRL_REG4
  write_buffer = lis2hh_ranges[range_idx];
  status = sns_dd_lis2hh_write_reg(
      dd_handle,
      STM_LIS2HH_CTRL_REG4_A,
      &write_buffer,
      1,
      0xFF);
  if(status != SNS_DDF_SUCCESS)
    return status;

  state->range_idx = range_idx;
  state->sstvt_adj = sstvt;

  temp_rate_idx = state->cur_rate_idx;

  status = sns_dd_acc_lis2hh_set_datarate(state,
      200);
  if(status != SNS_DDF_SUCCESS) {
    LIS2HH_MSG_E_1(ERROR, "set_datarate failed  status=%u", status);
    return status;
  }

  //20ms around 20 ODR
  sns_ddf_delay(20000);

  if(temp_rate_idx < 0)
    status = sns_dd_acc_lis2hh_set_datarate(state,
        0);
  else
    status = sns_dd_acc_lis2hh_set_datarate(state,
        lis2hh_odr[temp_rate_idx]);
  if(status != SNS_DDF_SUCCESS) {
    LIS2HH_MSG_E_1(ERROR, "set_datarate failed  status=%u", status);
    return status;
  }
  return SNS_DDF_SUCCESS;
}

/**
 * @brief Resets the driver and device so they return to the state they were
 *        in after init() was called.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_reset(sns_ddf_handle_t dd_handle)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;

  // Reset sensor device and driver state.

  status = sns_dd_acc_lis2hh_set_datarate(state,
      0);
  if(status != SNS_DDF_SUCCESS) {
    LIS2HH_MSG_E_1(ERROR, "set_datarate failed  status=%u", status);
    return status;
  }
  if(state->range_idx < 0) {
    status = sns_dd_acc_lis2hh_set_range(dd_handle, 0);
    if(status != SNS_DDF_SUCCESS) {
      LIS2HH_MSG_E_1(ERROR, "set range failed - status (%d)",status);
      return status;
    }
  }
  state->power_state = SNS_DDF_POWERSTATE_LOWPOWER;

  return SNS_DDF_SUCCESS;
}



/**
 * @brief Performs a set power state operation
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 * @param[in]  power_state    Power state to be set.
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
static sns_ddf_status_e sns_dd_acc_lis2hh_set_power(
    sns_ddf_handle_t dd_handle,
    sns_ddf_powerstate_e power_state)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;

  if((SNS_DDF_POWERSTATE_LOWPOWER == power_state)
      &&(SNS_DDF_POWERSTATE_ACTIVE == state->power_state))
  {
    // Configure ODR of control register 1 to enter power down mode.
    status = sns_dd_acc_lis2hh_set_datarate(state,
        0);
    if(status != SNS_DDF_SUCCESS) {
      LIS2HH_MSG_E_1(ERROR, "set_datarate failed  status=%u", status);
      return status;
    }
    state->power_state = SNS_DDF_POWERSTATE_LOWPOWER;
  }
  else if((SNS_DDF_POWERSTATE_ACTIVE == power_state)
      &&(SNS_DDF_POWERSTATE_LOWPOWER == state->power_state))
  {
    state->power_state= SNS_DDF_POWERSTATE_ACTIVE;
  }

  return SNS_DDF_SUCCESS;
}




/**
 * @brief Sets a lis2hh sensor attribute to a specific value.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_set_attr(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensor,
    sns_ddf_attribute_e attrib,
    void* value)
{
  sns_ddf_status_e status;
  sns_dd_acc_lis2hh_state_t *state = dd_handle;

  if((sensor != SNS_DDF_SENSOR_ACCEL) && (sensor != SNS_DDF_SENSOR__ALL))
    return SNS_DDF_EINVALID_PARAM;

  switch(attrib)
  {
    case SNS_DDF_ATTRIB_POWER_STATE:
      {
        sns_ddf_powerstate_e* power_state = value;
        return sns_dd_acc_lis2hh_set_power(dd_handle, *power_state);
      }

    case SNS_DDF_ATTRIB_RANGE:
      {
        uint32_t* range_idx = value;
        return sns_dd_acc_lis2hh_set_range(dd_handle, *range_idx);
      }
      /*
         case SNS_DDF_ATTRIB_LOWPASS:
         {
         uint32_t* freqs_idx = value;
         stm_lis2hh_odr freqs = lis2hh_odr_reg_values[*freqs_idx];
         if(freqs > STM_LIS2HH_MAX_ODR)
         return SNS_DDF_EINVALID_PARAM;

         return sns_dd_acc_lis2hh_set_datarate(dd_handle, freqs);
         }
         */
    case SNS_DDF_ATTRIB_ODR:
      {

        if((SNS_DDF_SENSOR_ACCEL == sensor)||(SNS_DDF_SENSOR__ALL == sensor))
        {
          if (*(sns_ddf_odr_t*)value > 0)
          {
            status = sns_dd_acc_lis2hh_set_datarate(state,
                *(sns_ddf_odr_t*)value);
            return status;
          }
          else
          {
            return sns_dd_acc_lis2hh_set_power(dd_handle, SNS_DDF_POWERSTATE_LOWPOWER);
          }
        }
        else
        {
          return SNS_DDF_EINVALID_PARAM;
        }
        break;
      } // ATTRIB_ODR

    case SNS_DDF_ATTRIB_FIFO:
      {
        sns_ddf_fifo_set_s* fifo = value;

        if(0 == fifo->watermark)
        {
          return sns_dd_acc_lis2hh_disable_fifo(state);
        }else if(fifo->watermark <= STM_LIS2HH_MAX_FIFO){
          //accel should be up and running.
          if(state->cur_rate_idx < 0)
            return SNS_DDF_EFAIL;

          return sns_dd_acc_lis2hh_enable_fifo(state, fifo->watermark);
        }else{
          return SNS_DDF_EINVALID_ATTR;
        }

        break;
      }

    default:
      return SNS_DDF_EINVALID_ATTR;
  }
}

/**
 * @brief Callback function to report inertial SW self test and bias calibration results.
 *
 * @param[in]  handle    Handle to a driver instance.
 * @param[in]  sensor    Sensor type.
 * @param[in]  status    Inertial SW self test result.
 * @param[in]  err       Device error code.
 * @param[in]  biases    Sensor biases after offset calibration.
 *
 * @return No return;
 */
static void sns_dd_acc_lis2hh_SWtest_notify(
    sns_ddf_handle_t handle,
    sns_ddf_sensor_e sensor,
    sns_ddf_status_e status,
    uint32_t err,
    q16_t* biases)
{
  sns_dd_acc_lis2hh_state_t* state = handle;
  q16_t* biases_ptr;
  uint8_t i;

  sns_ddf_smgr_notify_test_complete(
      state->smgr_handle,
      sensor,
      status, err);

  if(status == SNS_DDF_SUCCESS)
  {
    biases_ptr = state->biases;

    for(i = 0; i < STM_LIS2HH_NUM_AXES; i++)
      biases_ptr[i] = biases[i];

    sns_ddf_smgr_notify_event(
        state->smgr_handle,
        sensor,
        SNS_DDF_EVENT_BIAS_READY);
  }

}

/**
 * @brief Runs lis2hh inertial software self test and bias calibration.
 *
 * @param[in]  dd_handle   Handle to a driver instance.
 * @param[in]  sensor      Sensor type.
 *
 * @return SNS_DDF_PENDING if this SW self test is prepared to be performed later; SNS_DDF_EFAIL
 *         if SW self test fails. Otherwise SNS_DDF_EDEVICE, SNS_DDF_EBUS,
 *         SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to indicate an error has occurred.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_SW_selftest(
    sns_ddf_handle_t  dd_handle,
    sns_ddf_sensor_e  sensor)
{
  sns_ddf_status_e status;
  sns_dd_inertial_test_config_s test_config;

  test_config.sample_rate = 15;
  test_config.num_samples = 64;
  test_config.max_variance = FX_FLTTOFIX_Q16(383);                                     // max variance
  test_config.bias_thresholds[0] = FX_FLTTOFIX_Q16(STM_LIS2HH_SWST_MAX_BIAS * G/1000); // x-axis max bias
  test_config.bias_thresholds[1] = FX_FLTTOFIX_Q16(STM_LIS2HH_SWST_MAX_BIAS * G/1000); // y-axis max bias
  test_config.bias_thresholds[2] = FX_FLTTOFIX_Q16(STM_LIS2HH_SWST_MAX_BIAS * G/1000); // z-axis max bias

  status = sns_dd_acc_lis2hh_reset(dd_handle);
  if(SNS_DDF_SUCCESS != status)
    return status;

  status = sns_dd_acc_lis2hh_set_power(dd_handle, SNS_DDF_POWERSTATE_ACTIVE);
  if(SNS_DDF_SUCCESS != status)
    return status;

  status = sns_dd_acc_lis2hh_set_datarate(dd_handle, 100);
  if(SNS_DDF_SUCCESS != status)
    return status;

  status = sns_dd_inertial_test_run(
      sensor,
      dd_handle,
      &sns_dd_acc_lis2hh_if,
      &test_config,
      &sns_dd_acc_lis2hh_SWtest_notify);

  if(SNS_DDF_SUCCESS != status)
    return status;

  return SNS_DDF_PENDING;
}

/**
 * @brief Runs lis2hh factory self test and returns the test result.
 *        Supports SNS_DDF_TEST_SELF_SW.
 *        SNS_DDF_TEST_SELF_SW is the inertial software self test and bias calibration.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_run_test(
    sns_ddf_handle_t  dd_handle,
    sns_ddf_sensor_e  sensor,
    sns_ddf_test_e    test,
    uint32_t*         err)
{
  sns_ddf_status_e status;

  //verify all the parameters
  if((sensor != SNS_DDF_SENSOR_ACCEL) && (sensor != SNS_DDF_SENSOR__ALL))
    return SNS_DDF_EINVALID_PARAM;

  switch(test)
  {
    case SNS_DDF_TEST_SELF_SW:
      status = sns_dd_acc_lis2hh_SW_selftest(dd_handle, sensor);
      break;
    default:
      return SNS_DDF_EINVALID_TEST;
  }

  return status;
}

/**
 * @brief Retrieves the value of an attribute for a lis2hh sensor.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_get_attr(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensor,
    sns_ddf_attribute_e attrib,
    sns_ddf_memhandler_s* memhandler,
    void** value,
    uint32_t* num_elems)
{
  sns_dd_acc_lis2hh_state_t *state = dd_handle;

  if(sensor != SNS_DDF_SENSOR_ACCEL)
    return SNS_DDF_EINVALID_PARAM;

  switch(attrib)
  {
    case SNS_DDF_ATTRIB_POWER_INFO:
      {
        sns_ddf_power_info_s* power = sns_dd_lis2hh_memhandler_malloc(
            memhandler, sizeof(sns_ddf_power_info_s), state->smgr_handle);
        if(NULL == power)
          return SNS_DDF_ENOMEM;

        //current consumption, unit uA
        power->active_current = 50;
        power->lowpower_current = 6;
        *value = power;
        *num_elems = 1;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_RANGE:
      {
        sns_ddf_range_s* ranges = sns_dd_lis2hh_memhandler_malloc(
            memhandler, STM_LIS2HH_RANGE_NUM * sizeof(sns_ddf_range_s), state->smgr_handle);
        if(NULL == ranges)
          return SNS_DDF_ENOMEM;

        ranges[0].min = STM_LIS2HH_RANGE_2G_MIN;
        ranges[0].max = STM_LIS2HH_RANGE_2G_MAX;
        ranges[1].min = STM_LIS2HH_RANGE_4G_MIN;
        ranges[1].max = STM_LIS2HH_RANGE_4G_MAX;
        ranges[2].min = STM_LIS2HH_RANGE_8G_MIN;
        ranges[2].max = STM_LIS2HH_RANGE_8G_MAX;

        *num_elems = STM_LIS2HH_RANGE_NUM;
        *value = ranges;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_RESOLUTION:
      {
        sns_ddf_resolution_t *res = sns_dd_lis2hh_memhandler_malloc(
            memhandler ,sizeof(sns_ddf_resolution_t), state->smgr_handle);
        if(NULL == res)
          return SNS_DDF_ENOMEM;

        *res = FX_FLTTOFIX_Q16(state->sstvt_adj * G / 1000000);
        *value = res;
        *num_elems = 1;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_RESOLUTION_ADC:
      {
        sns_ddf_resolution_adc_s *res = sns_dd_lis2hh_memhandler_malloc(
            memhandler ,sizeof(sns_ddf_resolution_adc_s), state->smgr_handle);
        if(NULL == res)
          return SNS_DDF_ENOMEM;

        res->bit_len = 16;
        res->max_freq = 200;
        *value = res;
        *num_elems = 1;

        return SNS_DDF_SUCCESS;

      }

    case SNS_DDF_ATTRIB_LOWPASS:
      {
        *value = lis2hh_freqs;
        *num_elems = STM_LIS2HH_ODR_NUM;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_ODR:
      {
        sns_ddf_odr_t *res = sns_dd_lis2hh_memhandler_malloc(
            memhandler ,sizeof(sns_ddf_odr_t), state->smgr_handle);
        if(NULL == res)
          return SNS_DDF_ENOMEM;

        //always return the current ODR to SMGR.
        if((SNS_DDF_SENSOR_ACCEL == sensor)||(SNS_DDF_SENSOR__ALL == sensor)) {
          if(state->cur_rate_idx >= 0)
            *res = lis2hh_odr[state->cur_rate_idx];
          else
            *res = 0;
        }
        else
          return SNS_DDF_EINVALID_PARAM;

        *value = res;
        *num_elems = 1;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_DELAYS:
      {
        sns_ddf_delays_s *lis2hh_delays = sns_dd_lis2hh_memhandler_malloc(
            memhandler, sizeof(sns_ddf_delays_s), state->smgr_handle);
        if(NULL == lis2hh_delays)
          return SNS_DDF_ENOMEM;

        lis2hh_delays->time_to_active = 5;
        lis2hh_delays->time_to_data = 1;
        *value = lis2hh_delays;
        *num_elems = 1;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_BIAS:
      {
        *value = state->biases;
        *num_elems = STM_LIS2HH_NUM_AXES;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_FIFO:
      {
        sns_ddf_fifo_attrib_get_s *res = sns_dd_lis2hh_memhandler_malloc(
            memhandler ,sizeof(sns_ddf_fifo_attrib_get_s), state->smgr_handle);
        if(NULL == res)
          return SNS_DDF_ENOMEM;

        res->is_supported = true;
        res->is_sw_watermark = false;
        res->max_watermark = STM_LIS2HH_MAX_FIFO;
        res->share_sensor_cnt = 0;
        res->share_sensors[0] =  NULL;
        *value = res;
        *num_elems = 1;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_ODR_TOLERANCE:
      {
        q16_t *res = sns_dd_lis2hh_memhandler_malloc(
            memhandler ,sizeof(q16_t), state->smgr_handle);
        if(NULL == res)
          return SNS_DDF_ENOMEM;

        *res = FX_FLTTOFIX_Q16(0.05);
        *value = res;
        *num_elems = 1;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_DRIVER_INFO:
      {
        sns_ddf_driver_info_s *info = sns_dd_lis2hh_memhandler_malloc(
            memhandler, sizeof(sns_ddf_driver_info_s), state->smgr_handle);
        if(NULL == info)
          return SNS_DDF_ENOMEM;

        info->name = "STMicroelectronics LIS2HH Driver";
        info->version = 1;
        *value = info;
        *num_elems = 1;

        return SNS_DDF_SUCCESS;
      }

    case SNS_DDF_ATTRIB_DEVICE_INFO:
      {
        sns_ddf_device_info_s *info = sns_dd_lis2hh_memhandler_malloc(
            memhandler, sizeof(sns_ddf_device_info_s), state->smgr_handle);
        if(NULL == info)
          return SNS_DDF_ENOMEM;

        info->name = "Accelerometer";
        info->vendor = "STMicroelectronics";
        info->model = "LIS2HH";
        info->version = 1;
        *value = info;
        *num_elems = 1;

        return SNS_DDF_SUCCESS;
      }
    default:
      return SNS_DDF_EINVALID_ATTR;
  }
}
/**
 * @brief Initializes the lis2hh driver and sets up sensor.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_init(
    sns_ddf_handle_t* dd_handle_ptr,
    sns_ddf_handle_t smgr_handle,
    sns_ddf_nv_params_s* nv_params,
    sns_ddf_device_access_s device_info[],
    uint32_t num_devices,
    sns_ddf_memhandler_s* memhandler,
    sns_ddf_sensor_e* sensors[],
    uint32_t* num_sensors)
{
  sns_ddf_status_e status;
  sns_dd_acc_lis2hh_state_t* state;
  static sns_ddf_sensor_e my_sensors[] = {
    SNS_DDF_SENSOR_ACCEL};
  uint8_t rw_buffer = 0;
  uint8_t rw_bytes = 0;
LIS2HH_MSG_E_0(ERROR, " Justin sns_dd_acc_lis2hh_init : Enter");
#if UIMAGE_SUPPORT
  /* Update smgr sensor data for the driver to indicate uImage support */
  sns_ddf_smgr_set_uimg_refac(smgr_handle);

#endif

  // Allocate a driver instance.
  status = sns_dd_lis2hh_malloc((void**)&state, sizeof(sns_dd_acc_lis2hh_state_t), smgr_handle);
  if(status != SNS_DDF_SUCCESS)
  {
    return SNS_DDF_ENOMEM;
  }
  *dd_handle_ptr = state;
  state->smgr_handle = smgr_handle;
  state->gpio_num = device_info->first_gpio;

  // Open communication port to the device.
  status = sns_ddf_open_port(&state->port_handle, &device_info->port_config);
  if(status != SNS_DDF_SUCCESS)
  {
    sns_dd_lis2hh_mfree(state, smgr_handle);
    return status;
  }

  // wait sensor powering up to be ready by delaying 5 ms.
  sns_ddf_delay(5000);

  // confirm WHO_AM_I register
  status = sns_ddf_read_port(
      state->port_handle,
      STM_LIS2HH_WHO_AM_I_A,
      &rw_buffer,
      1,
      &rw_bytes);
  if(status != SNS_DDF_SUCCESS) {
    sns_ddf_close_port(state->port_handle);
    sns_dd_lis2hh_mfree(state, smgr_handle);
    return status;
  }
  if((rw_bytes!=1)||(rw_buffer!=0x41)) {
    sns_ddf_close_port(state->port_handle);
    sns_dd_lis2hh_mfree(state, smgr_handle);
    return SNS_DDF_EFAIL;
  }

  // Resets the lis2hh
  status = sns_dd_acc_lis2hh_reset(state);
  if(status != SNS_DDF_SUCCESS)
  {
    sns_ddf_close_port(state->port_handle);
    sns_dd_lis2hh_mfree(state, smgr_handle);
    return status;
  }

  sns_ddf_axes_map_init(
      &state->axes_map, ((nv_params != NULL) ? nv_params->data : NULL));

  //allocate memory for fifo data sample.
  state->fifo_data.samples = NULL;
  state->fifo_raw_buffer = NULL;
  state->range_idx = -1;
  state->cur_rate_idx = -1;

  status =  sns_ddf_signal_register(
      state->gpio_num,
      state,
      &sns_dd_acc_lis2hh_if,
      SNS_DDF_SIGNAL_IRQ_RISING);

  if(status != SNS_DDF_SUCCESS)
    return status;
  // Fill out output parameters.
  *num_sensors = 1;
  *sensors = my_sensors;
LIS2HH_MSG_E_0(ERROR, " Justin sns_dd_acc_lis2hh_init : Exit");
  return SNS_DDF_SUCCESS;
}
/**
 * @brief To enable LIS2HH FIFO.
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 * @param[in] watermark      Watermark level.
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_enable_fifo(
    sns_ddf_handle_t dd_handle,
    uint16_t watermark)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer = 0;

  LIS2HH_MSG_1(HIGH, "acc_enable_fifo: get called. watermark=%u", watermark);

  //malloc memory for state->fifo_data.samples
  if(NULL == state->fifo_data.samples)
  {
    status = sns_dd_lis2hh_malloc((void**)&(state->fifo_data.samples),
        STM_LIS2HH_MAX_FIFO * STM_LIS2HH_NUM_AXES * sizeof(sns_ddf_sensor_sample_s), state->smgr_handle);

    if(status != SNS_DDF_SUCCESS)
    {
      LIS2HH_MSG_1(HIGH, "lis2hh_enable_fifo: malloc fifo_data samples failed. status=%u", status);

      return status;
    }
  }

  //malloc memory for state->fifo_raw_buffer
  if(NULL == state->fifo_raw_buffer)
  {
    status = sns_dd_lis2hh_malloc((void**)&(state->fifo_raw_buffer),
        STM_LIS2HH_NUM_READ_BYTES * STM_LIS2HH_MAX_FIFO * sizeof(uint8_t), state->smgr_handle);
    if(status != SNS_DDF_SUCCESS)
    {
      LIS2HH_MSG_1(HIGH, "lis2hh_enable_fifo: malloc fifo_raw_buffer failed. status=%u", status);

      return status;
    }
  }

  if(watermark > 1)
  {
    //disable BDU in REG41.
    rw_buffer = 0x0;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG1_A,
        &rw_buffer,
        1,
        0x08);
    if(status != SNS_DDF_SUCCESS)
    {
      return status;
    }

    // Configure accelscope FIFO control registers to enable FIFO
    rw_buffer = 0x40 | (uint8_t)(watermark - 1);
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_FIFO_MODE,
        &rw_buffer,
        1,
        0xFF);
    if(status != SNS_DDF_SUCCESS)
      return status;

    // enable FIFO
    rw_buffer = 0x80;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG3_A,
        &rw_buffer,
        1,
        0x80);
    if(status != SNS_DDF_SUCCESS)
    {
      return status;
    }
  }
  else if(1 == watermark)
  {
    if(state->fifo_enabled)
    {
      // Configure accel FIFO control registers to disable FIFO
      rw_buffer = 0x0;
      status = sns_dd_lis2hh_write_reg(
          dd_handle,
          STM_LIS2HH_FIFO_MODE,
          &rw_buffer,
          1,
          0xFF);
      if(status != SNS_DDF_SUCCESS)
        return status;

      // disable FIFO
      rw_buffer = 0x0;
      status = sns_dd_lis2hh_write_reg(
          dd_handle,
          STM_LIS2HH_CTRL_REG3_A,
          &rw_buffer,
          1,
          0x80);
      if(status != SNS_DDF_SUCCESS)
      {
        return status;
      }
    }

    //enable BDU in REG4.
    rw_buffer = 0x08;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG1_A,
        &rw_buffer,
        1,
        0x08);
    if(status != SNS_DDF_SUCCESS)
    {
      return status;
    }
  }

  state->fifo_wmk = watermark;
  state->fifo_enabled = true;

  LIS2HH_MSG_1(HIGH, "lis2hh_enable_fifo: finished. watermark=%u", watermark);

  return SNS_DDF_SUCCESS;
}

/**
 * @brief To disable LIS2HH FIFO.
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_disable_fifo(
    sns_ddf_handle_t dd_handle)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer = 0;

  LIS2HH_MSG_1(HIGH, "lis2hh_disable_fifo: get called. current_watermark=%u", state->fifo_wmk);

  if(state->fifo_wmk > 1)
  {
    // Configure accel FIFO control registers
    rw_buffer = 0x0;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_FIFO_MODE,
        &rw_buffer,
        1,
        0xFF);
    if(status != SNS_DDF_SUCCESS)
      return status;

    // disable FIFO
    rw_buffer = 0x0;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG3_A,
        &rw_buffer,
        1,
        0x80);
    if(status != SNS_DDF_SUCCESS)
    {
      return status;
    }
  }else if(1 == state->fifo_wmk)
  {
    //disable BDU in REG1.
    rw_buffer = 0x0;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG1_A,
        &rw_buffer,
        1,
        0x08);
    if(status != SNS_DDF_SUCCESS)
    {
      return status;
    }
  }

  //state->fifo_wmk = 0;  //will be cleared in disable_fifo_int().
  state->fifo_enabled = false;

  //only reset last_timestamp after reset or after fifo disabled.
  state->last_timestamp = 0;

  //free memory for state->fifo_data.samples
  if(NULL != state->fifo_data.samples)
  {
    status = sns_dd_lis2hh_mfree(state->fifo_data.samples, state->smgr_handle);
    if(status != SNS_DDF_SUCCESS)
    {
      LIS2HH_MSG_1(HIGH, "lis2hh_disable_fifo: mfree fifo_data samples failed. status=%u", status);

      return status;
    }

    state->fifo_data.samples = NULL;
  }

  //free memory for state->fifo_raw_buffer
  if(NULL != state->fifo_raw_buffer)
  {
    status = sns_dd_lis2hh_mfree(state->fifo_raw_buffer, state->smgr_handle);
    if(status != SNS_DDF_SUCCESS)
    {
      LIS2HH_MSG_1(HIGH, "lis2hh_disable_fifo: mfree fifo_raw_buffer failed. status=%u", status);

      return status;
    }

    state->fifo_raw_buffer = NULL;
  }

  LIS2HH_MSG_1(HIGH, "lis2hh_disable_fifo: finished. current_watermark=%u", state->fifo_wmk);

  return SNS_DDF_SUCCESS;
}

/**
 * @brief To enable LIS2HH FIFO interrupt.
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
static sns_ddf_status_e sns_dd_acc_lis2hh_enable_fifo_int(
    sns_ddf_handle_t dd_handle)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer = 0;

  if(false == state->fifo_enabled)
    return SNS_DDF_SUCCESS;

  if(state->fifo_enabled && state->fifo_int_enabled)
    return SNS_DDF_SUCCESS;

  if(state->fifo_wmk > 1)
  {
    // Configure accel FIFO control registers
    rw_buffer = 0x02;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG3_A,
        &rw_buffer,
        1,
        0x02);
    if(status != SNS_DDF_SUCCESS)
    {
      return status;
    }
  }else  if(1 == state->fifo_wmk)
    //special case, watermark is 1, enable DRI instead of FIFO.
  {
    // Configure accel FIFO control registers
    rw_buffer = 0x01;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG3_A,
        &rw_buffer,
        1,
        0x01);
    if(status != SNS_DDF_SUCCESS)
    {
      return status;
    }
  }else
  {
    return SNS_DDF_EINVALID_PARAM;
  }

#if 0
  status =  sns_ddf_signal_register(
      state->gpio_num,
      state,
      &sns_dd_acc_lis2hh_if,
      SNS_DDF_SIGNAL_IRQ_RISING);

  if(status != SNS_DDF_SUCCESS)
    return status;
#endif

  state->fifo_int_enabled = true;

  LIS2HH_MSG_2(HIGH, "fifo int enabled: gpio_num=%u, status=%u",
      state->gpio_num, status);

  return SNS_DDF_SUCCESS;
}

/**
 * @brief To disable LIS2HH FIFO interrupt.
 *
 * @param[in]  dd_handle      Handle to a driver instance.
 *
 * @return SNS_DDF_SUCCESS if this operation was done successfully. Otherwise
 *         SNS_DDF_EDEVICE, SNS_DDF_EBUS, SNS_DDF_EINVALID_PARAM, or SNS_DDF_EFAIL to
 *         indicate and error has occurred.
 */
static sns_ddf_status_e sns_dd_acc_lis2hh_disable_fifo_int(
    sns_ddf_handle_t dd_handle)
{
  sns_dd_acc_lis2hh_state_t* state = dd_handle;
  sns_ddf_status_e status;
  uint8_t rw_buffer = 0;

  if(false == state->fifo_int_enabled)
    return SNS_DDF_SUCCESS;

  if(state->fifo_wmk > 1)
  {
    // Configure accel FIFO control registers
    rw_buffer = 0x0;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG3_A,
        &rw_buffer,
        1,
        0x02);
    if(status != SNS_DDF_SUCCESS)
    {
      return status;
    }
  }
  else if(1 == state->fifo_wmk)
  {
    //special case, if watermark is 1, clear accel DRI INT.
    // Configure accel FIFO control registers
    rw_buffer = 0x0;
    status = sns_dd_lis2hh_write_reg(
        dd_handle,
        STM_LIS2HH_CTRL_REG3_A,
        &rw_buffer,
        1,
        0x01);
    if(status != SNS_DDF_SUCCESS)
    {
      return status;
    }
  }else
  {
    return SNS_DDF_EINVALID_PARAM;
  }

  state->fifo_int_enabled = false;

  //status = sns_ddf_signal_deregister(state->gpio_num);
  //if(SNS_DDF_SUCCESS != status)
  //return status;

  return SNS_DDF_SUCCESS;
}
/**
 * @brief Implement enable_sched_data() DDF API.
 *
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_acc_lis2hh_enable_sched(
    sns_ddf_handle_t  dd_handle,
    sns_ddf_sensor_e  sensor,
    bool              enable)
{
  sns_dd_acc_lis2hh_state_t *state = dd_handle;

  LIS2HH_MSG_2(HIGH, "enable_sched_data: sensor=%u, enable=%u", sensor, enable);

  //each called function will handle concurrencies when to enable/disable FIFO int.

  switch(sensor)
  {
    case SNS_DDF_SENSOR_ACCEL:
      {
        if(enable)
        {
          return sns_dd_acc_lis2hh_enable_fifo_int(state);

        }else{

          return sns_dd_acc_lis2hh_disable_fifo_int(state);
        }

        break;
      }
    default:
      return SNS_DDF_EINVALID_ATTR;
  }

  return SNS_DDF_SUCCESS;
}
