/******************** (C) COPYRIGHT 2015 STMicroelectronics ********************
*
* File Name         : sns_dd_hts221_uimg.c
* Description       : HTS221 humidity sensor driver source file
*
********************************************************************************
* Copyright (c) 2015, STMicroelectronics.
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*     2. Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     3. Neither the name of the STMicroelectronics nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXHUMID OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

/*==============================================================================
Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential
==============================================================================*/
#include "sns_dd_hts221.h"

static sns_ddf_status_e hts221_get_humid(sns_dd_hts221_state_t* state, q16_t* humid)
{
    sns_ddf_status_e status;
    uint8_t reg[2] = {0};
    uint8_t rw_bytes;
    int16_t humid_adc;

    // Read humidity.
    status=sns_dd_hts221_read_regs(AUTO_INCREMENT|STM_HTS221_HUMID_OUT_L, reg, 2);
    if (status!=SNS_DDF_SUCCESS) return status;
    humid_adc=(int16_t)((reg[1]<<8)|reg[0]);

    *humid=((int64_t)(state->HY1_HY0*humid_adc+state->H1001)<<16)/state->HX1_HX0/2;    // RH%
    return status;
}

static sns_ddf_status_e hts221_get_tmpt(sns_dd_hts221_state_t* state, q16_t* tmpt)
{
    sns_ddf_status_e status;
    uint8_t reg[2] = {0};
    uint8_t rw_bytes;
    int16_t tmpt_adc;

    // Read temperature.
    status=sns_dd_hts221_read_regs(AUTO_INCREMENT|STM_HTS221_TEMP_OUT_L, reg, 2);
    if (status!=SNS_DDF_SUCCESS) return status;
    tmpt_adc=(int16_t)((reg[1]<<8)|reg[0]);
    *tmpt= ((int64_t)(state->TY1_TY0*tmpt_adc+state->T1001)<<16)/state->TX1_TX0/8;    // C
    return status;
}

sns_ddf_status_e hts221_get_ht(sns_dd_hts221_state_t* state, q16_t* humid, q16_t* tmpt)
{
    sns_ddf_status_e status;
    uint8_t reg[4] = {0};
    uint8_t rw_bytes;
    int16_t humid_adc, tmpt_adc;

    // Read both humidity and temperature.
    status=sns_dd_hts221_read_regs(AUTO_INCREMENT|STM_HTS221_HUMID_OUT_L, reg, 4);
    if (status!=SNS_DDF_SUCCESS) return status;
    humid_adc=(int16_t)((reg[1]<<8)|reg[0]);
    tmpt_adc=(int16_t)((reg[3]<<8)|reg[2]);
    *humid=((int64_t)(state->HY1_HY0*humid_adc+state->H1001)<<16)/state->HX1_HX0/2;    // RH%
    *tmpt= ((int64_t)(state->TY1_TY0*tmpt_adc+state->T1001)<<16)/state->TX1_TX0/8;    // C
    return status;
}

static void sns_dd_hts221_log_data(sns_ddf_sensor_e sensor, uint32_t data, sns_ddf_time_t timestamp)
{
    sns_err_code_e status;
    sns_log_sensor_data_pkt_s* log;

    status = sns_logpkt_malloc(SNS_LOG_CONVERTED_SENSOR_DATA, sizeof(sns_log_sensor_data_pkt_s), (void**)&log);
    if ( (status==SNS_SUCCESS) && (log!=NULL) )
    {
        log->version = SNS_LOG_SENSOR_DATA_PKT_VERSION;
        log->sensor_id = sensor;
        log->vendor_id = SNS_DDF_VENDOR_STMICRO;
        log->timestamp = timestamp;
        log->num_data_types = 1;
        log->num_samples = 1;
        log->data[0] = data;
        sns_logpkt_commit(SNS_LOG_CONVERTED_SENSOR_DATA, log);
    }
}

/** 
 * @brief Retrieves a single set of humidity and/or temperature data from hts221
 *  
 * Refer to sns_ddf_driver_if.h for definition.
 */
sns_ddf_status_e sns_dd_hts221_get_data(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensors[],
    uint32_t num_sensors,
    sns_ddf_memhandler_s* memhandler,
    sns_ddf_sensor_data_s** data)
{
    uint8_t i;
    q16_t sample=0;
    sns_ddf_time_t timestamp;
    sns_ddf_status_e status;
    sns_ddf_sensor_data_s *data_ptr;
    sns_dd_hts221_state_t *state;

    if (dd_handle==NULL || data==NULL)
    {
        DD_MSG_0(ERROR, "GetData: dd_handle is NULL");
        return SNS_DDF_EINVALID_PARAM;
    }
    state = (sns_dd_hts221_state_t*)dd_handle;

    //if current power mode is LOWPOWER , return error.
    if (!(state->driver_mode&HTS221_DRVMODE_bPowerAll)||(num_sensors>2))
        return SNS_DDF_EDEVICE;

    //allocate memory for sns_ddf_sensor_data_s data structure
    data_ptr = sns_dd_memhandler_malloc(memhandler, num_sensors * sizeof(sns_ddf_sensor_data_s), state->smgr_handle);
    if(NULL == data_ptr) return SNS_DDF_ENOMEM;

    *data = data_ptr;

    for (i=0; i<num_sensors; i++)
    {
        // get current time stamp
        timestamp = sns_ddf_get_timestamp();
    
        // This is a synchronous driver, so try to read data now.
        if (sensors[i]==SNS_DDF_SENSOR_HUMIDITY)
        {
            status=hts221_get_humid(state, &sample);
            DD_MSG_1(MED, "GetData: RH=%dm%%", sample*1000/65536);
        }
        else if (sensors[i]==SNS_DDF_SENSOR_AMBIENT_TEMP)
        {
            status=hts221_get_tmpt(state, &sample);
            DD_MSG_1(MED, "GetData: T=%dmC", sample*1000/65536);
        }
        else
        {
            status=SNS_DDF_EINVALID_PARAM;
        }
        if (status!=SNS_DDF_SUCCESS) return status;
        
        data_ptr[i].sensor = sensors[i];
        data_ptr[i].status = SNS_DDF_SUCCESS;
        data_ptr[i].timestamp = timestamp;
    
        //allocate memory for data samples.
        data_ptr[i].samples = sns_dd_memhandler_malloc(memhandler, sizeof(sns_ddf_sensor_sample_s), state->smgr_handle);
        if(NULL == data_ptr[i].samples) return SNS_DDF_ENOMEM;
        data_ptr[i].samples->sample = sample;
        data_ptr[i].samples->status = SNS_DDF_SUCCESS;
        data_ptr[i].num_samples = 1;

        // Log sensor data
        sns_dd_hts221_log_data(data_ptr[i].sensor, data_ptr[i].samples->sample, data_ptr[i].timestamp);
    }
    
    return SNS_DDF_SUCCESS;
}

void sns_dd_hts221_handle_irq(
    sns_ddf_handle_t  handle,
    uint32_t          gpio_num,
    sns_ddf_time_t    timestamp)
{
    sns_dd_hts221_state_t* state;
    state = (sns_dd_hts221_state_t*)handle;

    if ((handle==NULL)||(gpio_num!=state->gpio_num)||!(state->driver_mode&HTS221_DRVMODE_bScheduleAll) )
    {
        DD_MSG_0(MED, "HandleIrq: Received Null Pointer.");
        return;
    }

    sns_ddf_status_e status;
    sns_ddf_sensor_data_s ddf_data[2];
    sns_ddf_sensor_sample_s ddf_sample[2];
    sns_ddf_time_t isr_timestamp;
    q16_t humid, tmpt;
    uint32 data_num;
    sns_ddf_odr_t current_dd_odr;

    current_dd_odr=(state->dd_humid_odr>=state->dd_temp_odr)?state->dd_humid_odr:state->dd_temp_odr;
    if (current_dd_odr==0) return;
    isr_timestamp=sns_ddf_get_timestamp();
    if ((timestamp-state->last_irq_timestamp)<(TIMESTAMP_TICK_FREQ/2/current_dd_odr))
    // If IRQ interval is less than 1/2 ODR, treat this IRQ as false IRQ.
    {
        DD_MSG_2(MED, "DRI: IrqTime=%u, IsrTime=%u. False IRQ.", timestamp, isr_timestamp);
        return;
    }

    data_num=0;
    state->last_irq_timestamp=timestamp;
    status=hts221_get_ht(state, &humid, &tmpt); //DRDY returns inactive after both HUMIDITY_OUT_H and TEMP_OUT_H are read.
    if (status!=SNS_DDF_SUCCESS) return;
    if (state->dd_humid_odr!=0)
    {
        // 1. Create notification for humidure sensor.
        ddf_sample[data_num].status=status;
        ddf_sample[data_num].sample=humid;
        ddf_data[data_num].sensor=SNS_DDF_SENSOR_HUMIDITY;
        ddf_data[data_num].status=status;
        ddf_data[data_num].timestamp=timestamp;
        ddf_data[data_num].num_samples=1;
        ddf_data[data_num].samples=&ddf_sample[data_num];
        sns_dd_hts221_log_data(ddf_data[data_num].sensor, ddf_data[data_num].samples->sample, ddf_data[data_num].timestamp);
        DD_MSG_3(MED, "DRI: IrqTime=%u, IsrTime=%u, RH=%dm%%", timestamp, isr_timestamp, humid*1000/65536);
        data_num++;
    }

    if (state->dd_temp_odr!=0)
    {
        // 2. Create notification for temperature sensor.
        if (status!=SNS_DDF_SUCCESS) return;
        ddf_sample[data_num].status=status;
        ddf_sample[data_num].sample=tmpt;
        ddf_data[data_num].sensor=SNS_DDF_SENSOR_AMBIENT_TEMP;
        ddf_data[data_num].status=status;
        ddf_data[data_num].timestamp=timestamp;
        ddf_data[data_num].num_samples=1;
        ddf_data[data_num].samples=&ddf_sample[data_num];
        sns_dd_hts221_log_data(ddf_data[data_num].sensor, ddf_data[data_num].samples->sample, ddf_data[data_num].timestamp);
        DD_MSG_3(MED, "DRI: IrqTime=%u, IsrTime=%u, T=%dmC", timestamp, isr_timestamp, tmpt*1000/65536);
        data_num++;
    }

    // 3. Notify SMGR.
    status = sns_ddf_smgr_notify_data(state->smgr_handle, ddf_data, data_num);
    if (SNS_DDF_SUCCESS!=status) DD_MSG_0(ERROR, "DRI: notice error.");
}

/**
 * HTS221 humidity sensor device driver interface.
 */
sns_ddf_driver_if_s sns_dd_hts221_if=
{
    .init   = &sns_dd_hts221_init,
    .get_data   = &sns_dd_hts221_get_data,
    .set_attrib = &sns_dd_hts221_set_attr,
    .get_attrib = &sns_dd_hts221_get_attr,
    .handle_timer = &sns_dd_hts221_handle_timer,
    .handle_irq = &sns_dd_hts221_handle_irq,
    .reset      = &sns_dd_hts221_reset,
    .run_test   = &sns_dd_hts221_run_test,
    .enable_sched_data = &sns_dd_hts221_enable_sched_data,
    .probe      = &sns_dd_hts221_probe,
};
