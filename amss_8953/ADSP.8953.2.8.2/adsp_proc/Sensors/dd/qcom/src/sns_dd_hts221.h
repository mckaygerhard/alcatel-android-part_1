/******************** (C) COPYRIGHT 2015 STMicroelectronics ********************
*
* File Name         : sns_dd_hts221.h
* Description       : HTS221 humidity sensor driver header file
*
********************************************************************************
* Copyright (c) 2015, STMicroelectronics.
* All rights reserved.
* 
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     1. Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*     2. Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     3. Neither the name of the STMicroelectronics nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
* 
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXHUMID OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#ifndef __HTS221__
#define __HTS221__

#include <stdio.h>
#include "sns_ddf_attrib.h"
#include "sns_ddf_comm.h"
#include "sns_ddf_common.h"
#include "sns_ddf_driver_if.h"
#include "sns_ddf_memhandler.h"
#include "sns_ddf_smgr_if.h"
#include "sns_ddf_util.h"
#include "sns_ddf_signal.h"

// driver configuration switches.
#ifdef SENSORS_DD_DEV_FLAG
#define BUILD_DRAGON_BOARD      // defined: build in HD22 build; undefined: build in customer build.
#endif
//#define BUILD_UIMAGE            // defined: run driver in uImage mode; undefined: in big image mode.
#define BUILD_DEBUG_LOG         // defined: enable debug message; undefined: disable debug message.

#define DEVICE_NAME "HTS221"

#if defined(BUILD_DRAGON_BOARD)
#   include "sns_log_types_public.h"
#   include "sns_log_api_public.h"
#   define SNS_LOG_CONVERTED_SENSOR_DATA    SNS_LOG_CONVERTED_SENSOR_DATA_PUBLIC
#   include "qurt_elite_diag.h"
// Commenting out the line below to allow using in HD22 package
//#   define sns_dd_hts221_if    sns_dd_vendor_if_1
#else    // Regular Builds
#   include "log_codes.h"
#   include "sns_log_types.h"
#   include "sns_log_api.h"
#endif

#if defined(BUILD_DEBUG_LOG) && !defined(BUILD_UIMAGE)
#   define DD_MSG_0(level,msg)             MSG(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg)
#   define DD_MSG_1(level,msg,p1)          MSG_1(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1)
#   define DD_MSG_2(level,msg,p1,p2)       MSG_2(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2)
#   define DD_MSG_3(level,msg,p1,p2,p3)    MSG_3(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2,p3)
#   define DD_MSG_4(level,msg,p1,p2,p3,p4) MSG_4(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2,p3,p4)
#   define DD_MSG_5(level,msg,p1,p2,p3,p4,p5)       MSG_5(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2,p3,p4,p5)
#   define DD_MSG_6(level,msg,p1,p2,p3,p4,p5,p6)    MSG_6(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2,p3,p4,p5,p6)
#elif defined(BUILD_DEBUG_LOG) && defined(BUILD_UIMAGE)
#   define DD_MSG_0(level,msg)             UMSG(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg)
#   define DD_MSG_1(level,msg,p1)          UMSG_1(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1)
#   define DD_MSG_2(level,msg,p1,p2)       UMSG_2(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2)
#   define DD_MSG_3(level,msg,p1,p2,p3)    UMSG_3(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2,p3)
#   define DD_MSG_4(level,msg,p1,p2,p3,p4) UMSG_4(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2,p3,p4)
#   define DD_MSG_5(level,msg,p1,p2,p3,p4,p5)    UMSG_5(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2,p3,p4,p5)
#   define DD_MSG_6(level,msg,p1,p2,p3,p4,p5,p6) UMSG_6(MSG_SSID_QDSP6,DBG_##level##_PRIO, DEVICE_NAME"-"msg,p1,p2,p3,p4,p5,p6)
#else    // Regular Builds
#   define DD_MSG_0(level,msg)
#   define DD_MSG_1(level,msg,p1)
#   define DD_MSG_2(level,msg,p1,p2)
#   define DD_MSG_3(level,msg,p1,p2,p3)
#   define DD_MSG_4(level,msg,p1,p2,p3,p4)
#   define DD_MSG_5(level,msg,p1,p2,p3,p4,p5)
#   define DD_MSG_6(level,msg,p1,p2,p3,p4,p5,p6)
#endif

#define STM_HTS221_HUMID_RANGE_MIN  FX_CONV_Q16(0,0)
#define STM_HTS221_HUMID_RANGE_MAX  FX_CONV_Q16(100,0)
#define STM_HTS221_TEMP_RANGE_MIN   FX_CONV_Q16(-40,0)
#define STM_HTS221_TEMP_RANGE_MAX   FX_CONV_Q16(120,0)

#define STM_HTS221_MAX_RES_HUMID    FX_FLTTOFIX_Q16(0.1)	//AV_CONF.AVGH=4
#define STM_HTS221_MAX_RES_TEMP     FX_FLTTOFIX_Q16(0.02)	//AV_CONF.AVGT=4

// Self Test Range. Assume test in normal room condition.
#define STM_HTS221_HUMID_ST_MIN     FX_CONV_Q16(0,0)
#define STM_HTS221_HUMID_ST_MAX     FX_CONV_Q16(100,0)
#define STM_HTS221_TEMP_ST_MIN      FX_CONV_Q16(0,0)
#define STM_HTS221_TEMP_ST_MAX      FX_CONV_Q16(50,0)

/**
 * Humidity sensor HTS221 I2C address
 */
#define STM_HTS221_I2C_ADDR         0x5F    //1011111
#define AUTO_INCREMENT 0x80

/**
 * Humidity sensor HTS221 WHO AM I register
 */
#define STM_HTS221_WHO_AM_I_VALUE   0xBC

/**
 * Humidity sensor HTS221 register addresses
 */
#define STM_HTS221_WHO_AM_I         0x0F
#define STM_HTS221_AV_CONF          0x10
#define STM_HTS221_CTRL_REG1        0x20
#define STM_HTS221_CTRL_REG2        0x21
#define STM_HTS221_CTRL_REG3        0x22
#define STM_HTS221_STATUS_REG       0x27
#define STM_HTS221_HUMID_OUT_L      0x28
#define STM_HTS221_HUMID_OUT_H      0x29
#define STM_HTS221_TEMP_OUT_L       0x2A
#define STM_HTS221_TEMP_OUT_H       0x2B
#define STM_HTS221_CALIB0           0x30

#define STMERR_TEST_OK              0
#define STMERR_TEST_UNKNOWN         1
#define STMERR_TEST_BUS_ERROR       2
#define STMERR_TEST_SENSOR1_ST_FAIL 0x10
#define STMERR_TEST_SENSOR2_ST_FAIL 0x20

#define HTS221_DRVMODE_bPowerHumid           0x01
#define HTS221_DRVMODE_bPowerTemp            0x02
#define HTS221_DRVMODE_bScheduleHumid        0x04
#define HTS221_DRVMODE_bScheduleTemp         0x08
#define HTS221_DRVMODE_bPowerAll             0x03
#define HTS221_DRVMODE_bScheduleAll          0x0C

/**
 * Humidity sensor HTS221 Driver State Information Structure
 */
typedef struct
{
    sns_ddf_handle_t smgr_handle;	// Handle used with sns_ddf_notify_data.
    sns_ddf_handle_t port_handle;	// Handle used to access the I2C bus.
    uint32_t gpio_num;              // GPIO number used for DRI.
    uint8_t ctrl_reg1, ctrl_reg3;	// sensor register mirror.
    int32_t TX1_TX0, TY1_TY0, T1001;
    int32_t HX1_HX0, HY1_HY0, H1001;
    bool self_test_enable;			// Self-test status: 0 for disabled; 1 for enabled.
    sns_ddf_timer_s timer_obj;		// Timer object for self-test.
    sns_ddf_odr_t dd_humid_odr;     // Current driver data rate. 0Hz and physical sensor odr.
    sns_ddf_odr_t dd_temp_odr;
    int sensor_odr_code;
    sns_ddf_time_t last_irq_timestamp;
    int32_t driver_mode;
} sns_dd_hts221_state_t;

#define sns_dd_hts221_read_regs(reg_addr, reg_buf, count)    \
    ( (status=sns_ddf_read_port(state->port_handle, reg_addr, reg_buf, count, &rw_bytes)) \
    !=SNS_DDF_SUCCESS? status : (rw_bytes!=count)?SNS_DDF_EBUS:SNS_DDF_SUCCESS )

#define sns_dd_hts221_write_regs(reg_addr, reg_buf, count) \
    ( (status=sns_ddf_write_port(state->port_handle, reg_addr, reg_buf, count, &rw_bytes)) \
    !=SNS_DDF_SUCCESS? status : (rw_bytes!=count)?SNS_DDF_EBUS:SNS_DDF_SUCCESS )

#define TIMESTAMP_TICK_FREQ     32768

#ifndef BUILD_UIMAGE
#   define 	sns_dd_malloc(ptr, size, smgr)				sns_ddf_malloc(ptr, size)
#   define	sns_dd_mfree(ptr, smgr) 					sns_ddf_mfree(ptr)
#   define 	sns_dd_memhandler_malloc(ptr, size, smgr)	sns_ddf_memhandler_malloc(ptr, size)
#else
#   define 	sns_dd_malloc(ptr, size, smgr)				sns_ddf_malloc_ex(ptr, size, smgr)
#   define	sns_dd_mfree(ptr, smgr)						sns_ddf_mfree_ex(ptr, smgr)
#   define 	sns_dd_memhandler_malloc(ptr, size, smgr) 	sns_ddf_memhandler_malloc_ex(ptr, size, smgr)
#endif

//Extract integer and fractional part of q16_t variable.
#define Q16INT(a)     ((a)>>16)
#define Q16DEC2(a)    ((((a)>=0?(a):-(a))&0x0000FFFF)*100/65536)

//***** Interface Functions ****************************************************************
sns_ddf_status_e sns_dd_hts221_reset(
    sns_ddf_handle_t dd_handle);

sns_ddf_status_e sns_dd_hts221_init(
    sns_ddf_handle_t* dd_handle_ptr,
    sns_ddf_handle_t smgr_handle,
    sns_ddf_nv_params_s* nv_params,
    sns_ddf_device_access_s device_info[],
    uint32_t num_devices,
    sns_ddf_memhandler_s* memhandler,
    sns_ddf_sensor_e* sensors[],
    uint32_t* num_sensors);

sns_ddf_status_e sns_dd_hts221_get_attr(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensor,
    sns_ddf_attribute_e attrib,
    sns_ddf_memhandler_s* memhandler,
    void** value,
    uint32_t* num_elems);

sns_ddf_status_e sns_dd_hts221_set_attr(
    sns_ddf_handle_t dd_handle,
    sns_ddf_sensor_e sensor,
    sns_ddf_attribute_e attrib,
    void* value);

sns_ddf_status_e sns_dd_hts221_enable_sched_data(
    sns_ddf_handle_t  dd_handle,
    sns_ddf_sensor_e  sensor,
    bool              enable);

sns_ddf_status_e sns_dd_hts221_run_test(
    sns_ddf_handle_t    dd_handle,
    sns_ddf_sensor_e    sensor_type,
    sns_ddf_test_e      test,
    uint32_t*           err);

void sns_dd_hts221_handle_timer(
    sns_ddf_handle_t dd_handle,
    void* arg);

sns_ddf_status_e sns_dd_hts221_probe(
    sns_ddf_device_access_s* device_info,
    sns_ddf_memhandler_s*    memhandler,
    uint32_t*                num_sensors,
    sns_ddf_sensor_e**       sensors);

sns_ddf_status_e hts221_get_ht(sns_dd_hts221_state_t* state, q16_t* humid, q16_t* tmpt);

#endif  /* __HTS221__ */
