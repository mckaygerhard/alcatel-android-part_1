/*
 * Copyright (c) 2016, ams AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when                 who              what, where, why
------------    --------      --------------------------------------------------
28/04/2016      Byron Shi      Make tmd2725 driver for Qualcomm aDSP
08/05/2016      Byron Shi      ALS use interrupt
09/05/2016      Byron Shi      Add NV params, ALS SELF TEST, get_data() support
13/06/2016      Byron Shi      Add ALS polling in DRI mode, update Prox algorithm
23/06/2016      Byron Shi      Add Prox SELF TEST, checklist for QCOM, update BSD header
16/08/2016      Byron Shi      Support OpenSSC v3.4/v3.5
05/09/2016      Byron Shi      Support Data log Packets

==============================================================================*/

#include <stdio.h>
#include "sns_ddf_attrib.h"
#include "sns_ddf_common.h"
#include "sns_ddf_comm.h"
#include "sns_ddf_driver_if.h"
#include "sns_ddf_smgr_if.h"
#include "sns_ddf_util.h"
#include "sns_ddf_memhandler.h"
#include "sns_ddf_signal.h"
#include "sns_dd_tmd2725_priv.h"

#include "sns_log_types_public.h"
#include "sns_log_api_public.h"
#include "qurt_elite_diag.h"

/*===========================================================================*/
/*
*  Global variable
*/
/*===========================================================================*/
bool first_prox = false;
bool in_calib = false;
uint8_t calib_count = 0;

#ifdef TMD2725_ALS_POLLING_IN_DRI
bool als_timer_stop = true;
#endif
bool first_als = false;
bool ready_for_report = false;

uint8_t data_requested = 0;

bool in_timer_handler = false;

/*===========================================================================*/
/*
*  Parameters
*/
/*===========================================================================*/
static uint8_t const als_gains[] = {
	1,
	4,
	16,
	64
};

/*===========================================================================*/
/*
*  External functions and function declaration
*/
/*===========================================================================*/
extern sns_ddf_status_e sns_dd_tmd2725_init(
	sns_ddf_handle_t*        dd_ptr,
	sns_ddf_handle_t         smgr_handle,
	sns_ddf_nv_params_s*     nv_params,
	sns_ddf_device_access_s  device_info[],
	uint32_t                 num_devices,
	sns_ddf_memhandler_s*    memhandler,
	sns_ddf_sensor_e*        sensors[],
	uint32_t*                num_sensors
);

extern sns_ddf_status_e sns_dd_tmd2725_set_attrib(
	sns_ddf_handle_t         handle,
	sns_ddf_sensor_e         sensor,
	sns_ddf_attribute_e      attrib,
	void*                    value
);

extern sns_ddf_status_e sns_dd_tmd2725_get_attrib(
	sns_ddf_handle_t         handle,
	sns_ddf_sensor_e         sensor,
	sns_ddf_attribute_e      attrib,
	sns_ddf_memhandler_s*    memhandler,
	void**                   value,
	uint32_t*                num_elems
);

extern sns_ddf_status_e sns_dd_tmd2725_reset(
	sns_ddf_handle_t         handle
);

extern sns_ddf_status_e sns_dd_tmd2725_run_test(
	sns_ddf_handle_t         handle,
	sns_ddf_sensor_e         sensor,
	sns_ddf_test_e           test,
	uint32_t*                err
);

extern sns_ddf_status_e sns_dd_tmd2725_enable_sched_data(
	sns_ddf_handle_t         state,
	sns_ddf_sensor_e         sensor,
	bool                     enable
);

extern sns_ddf_status_e sns_dd_tmd2725_probe(
	sns_ddf_device_access_s* device_info,
	sns_ddf_memhandler_s*    memhandler,
	uint32_t*                num_sensors,
	sns_ddf_sensor_e**       sensors
);

sns_ddf_status_e sns_dd_tmd2725_get_data(
	sns_ddf_handle_t         handle,
	sns_ddf_sensor_e         sensors[],
	uint32_t                 num_sensors,
	sns_ddf_memhandler_s*    memhandler,
	sns_ddf_sensor_data_s*   data[]
);

void sns_dd_tmd2725_handle_timer(
	sns_ddf_handle_t         handle,
	void*                    arg
);

void sns_dd_tmd2725_handle_irq(
	sns_ddf_handle_t         handle,
	uint32_t                 gpio_num,
	sns_ddf_time_t           timestamp
);

sns_ddf_driver_if_s sns_tmd2725_driver_fn_list = {
	&sns_dd_tmd2725_init,
	&sns_dd_tmd2725_get_data,
	&sns_dd_tmd2725_set_attrib,
	&sns_dd_tmd2725_get_attrib,
	&sns_dd_tmd2725_handle_timer,
	&sns_dd_tmd2725_handle_irq,
	&sns_dd_tmd2725_reset,
	&sns_dd_tmd2725_run_test,
	&sns_dd_tmd2725_enable_sched_data,
	&sns_dd_tmd2725_probe,
	NULL,
	NULL,
	NULL,
};

sns_ddf_driver_if_s sns_dd_vendor_if_2 = {
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};

/*===========================================================================*/
/*
*  Local functions
*/
/*===========================================================================*/
int32_t tmd2725_hub_i2c_read_byte(sns_dd_tmd2725_state_t* state, uint8_t reg, uint8_t *val)
{
	int32_t ret = -1;
	sns_ddf_status_e status;
	uint8_t read_count;

	if (NULL == state->port_handle)
	{
		TMD2725_DD_UIMG_MSG_0(ERROR, "i2c read byte fail: port handle is null\n");
		return ret;
	}

	status = sns_ddf_read_port(state->port_handle,
								reg,
								val,
								1,
								&read_count);
	if (SNS_DDF_SUCCESS == status)
	{
		ret = read_count;
	}
	else
	{
		TMD2725_DD_UIMG_MSG_1(ERROR, "i2c read byte fail at reg:%x\n", reg);
	}

	return ret;
}

static int32_t tmd2725_hub_i2c_read_byte_block(sns_dd_tmd2725_state_t* state, uint8_t reg, uint8_t *val, int32_t len)
{
	int32_t ret = -1;
	sns_ddf_status_e status;
	uint8_t read_count;

	if (NULL == state->port_handle)
	{
		TMD2725_DD_UIMG_MSG_0(ERROR, "i2c read block fail: port handle is null\n");
		return ret;
	}

	status = sns_ddf_read_port(state->port_handle,
								reg,
								val,
								len,
								&read_count);
	if (SNS_DDF_SUCCESS == status)
	{
		ret = read_count;
	}
	else
	{
		TMD2725_DD_UIMG_MSG_1(ERROR, "i2c read block fail at reg:%x\n", reg);
	}

	return ret;
}

int32_t tmd2725_hub_i2c_write_byte(sns_dd_tmd2725_state_t* state, uint8_t reg, uint8_t val)
{
	int32_t ret = -1;
	sns_ddf_status_e status;
	uint8_t write_count;

	if (NULL == state->port_handle)
	{
		TMD2725_DD_UIMG_MSG_0(ERROR, "i2c write byte fail: port handle is null\n");
		return ret;
	}

	status = sns_ddf_write_port(state->port_handle,
								reg,
								&val,
								1,
								&write_count);
	if (SNS_DDF_SUCCESS == status)
	{
		ret = write_count;
	}
	else
	{
		TMD2725_DD_UIMG_MSG_1(ERROR, "i2c write byte fail at reg:%x\n", reg);
	}

	return ret;
}

int32_t tmd2725_hub_i2c_modify(sns_dd_tmd2725_state_t* state, uint8_t reg, uint8_t mask, uint8_t val)
{
	int32_t ret;
	uint8_t temp;

	ret = tmd2725_hub_i2c_read_byte(state, reg, &temp);
	if (ret <= 0)
	{
		TMD2725_DD_UIMG_MSG_1(ERROR, "i2c modify byte read fail at reg:%x\n", reg);
		return ret;
	}
	temp &= ~mask;
	temp |= val;
	ret = tmd2725_hub_i2c_write_byte(state, reg, temp);
	if (ret <= 0)
	{
		TMD2725_DD_UIMG_MSG_1(ERROR, "i2c modify byte write fail at reg:%x\n", reg);
	}

	return ret;
}

void tmd2725_hub_read_prox_data(sns_dd_tmd2725_state_t* state)
{
	uint8_t pdata;
	int32_t ret;

	ret = tmd2725_hub_i2c_read_byte(state, TMD2725_PDATA_REG, &pdata);
TMD2725_DD_UIMG_MSG_1(ERROR, "tmd2725_hub_read_prox_data : pdata =%d\n",pdata);
	if (ret > 0)
	{
		state->chip.prox_inf.raw = pdata;
	}
}

bool tmd2725_hub_get_prox_event(sns_dd_tmd2725_state_t* state, bool int_trigger, bool *first)
{
	static uint8_t prox_th_low = 0, prox_th_high = 0;
	bool ret = false;

	if (*first)
	{
		/* First triggered proximity event */
		*first = false;

		if (state->chip.prox_inf.raw > state->chip.params.prox_th_high)
		{
			state->chip.prox_inf.detected = true;
			if (state->chip.prox_inf.raw < state->chip.params.prox_th_verynear)
			{
				prox_th_low = state->chip.params.prox_th_low;
				prox_th_high = state->chip.params.prox_th_verynear;
			}
			else
			{
				prox_th_low = state->chip.params.prox_th_contaminated;
				prox_th_high = 0xFF;
			}
		}
		else
		{
			state->chip.prox_inf.detected = false;
			prox_th_low = 0;
			prox_th_high = state->chip.params.prox_th_high;
		}

		if (int_trigger)
		{
			tmd2725_hub_i2c_modify(state, TMD2725_PERS_REG, PPERS, PROX_PERSIST(1));
			tmd2725_hub_i2c_write_byte(state, TMD2725_PILT_REG, prox_th_low);
			tmd2725_hub_i2c_write_byte(state, TMD2725_PIHT_REG, prox_th_high);
		}

		ret = true;
	}
	else
	{
		if (state->chip.prox_inf.detected)
		{
			if (state->chip.prox_inf.raw < prox_th_low)
			{
				state->chip.prox_inf.detected = false;
				if (prox_th_low > state->chip.params.prox_th_high)
				{
					prox_th_low = state->chip.params.prox_th_high - 10;
					prox_th_high = state->chip.params.prox_th_contaminated + 10;
				}
				else
				{
					prox_th_low = 0;
					prox_th_high = state->chip.params.prox_th_high;
				}

				ret = true;

				tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, 0); 
				tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, PON);
				sns_ddf_delay(DELAY_MS_AFTER_PON * 1000); 
				tmd2725_hub_i2c_write_byte(state, TMD2725_CALIBCFG_REG, (BINSRCH_TARGET_0|0x09));
				tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, (PIEN|CIEN), CIEN);
				tmd2725_hub_i2c_write_byte(state, TMD2725_CALIB_REG, START_OFFSET_CALIB);
				in_calib = true;
				calib_count = 0;
				/* Start timer */
				sns_ddf_timer_start(state->calib_timer, 1000*10);
			}
			else if (state->chip.prox_inf.raw > state->chip.params.prox_th_verynear)
			{
				prox_th_low = state->chip.params.prox_th_contaminated;
				prox_th_high = 0xFF;
			}

			if (int_trigger)
			{
				tmd2725_hub_i2c_write_byte(state, TMD2725_PILT_REG, prox_th_low);
				tmd2725_hub_i2c_write_byte(state, TMD2725_PIHT_REG, prox_th_high);
			}
		}
		else
		{
			if (state->chip.prox_inf.raw > prox_th_high)
			{
				state->chip.prox_inf.detected = true;
				if (prox_th_high > state->chip.params.prox_th_high)
				{
					prox_th_low = state->chip.params.prox_th_contaminated;
					prox_th_high = 0xFF;
				}
				else
				{
					prox_th_low = state->chip.params.prox_th_low;
					prox_th_high = state->chip.params.prox_th_verynear;
				}

				ret = true;
			}
			else if (state->chip.prox_inf.raw < prox_th_low)
			{
				prox_th_low = 0;
				prox_th_high = state->chip.params.prox_th_high;
				tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, 0); 
				tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, PON);
				sns_ddf_delay(DELAY_MS_AFTER_PON * 1000); 
				tmd2725_hub_i2c_write_byte(state, TMD2725_CALIBCFG_REG, (BINSRCH_TARGET_0|0x09));
				tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, (PIEN|CIEN), CIEN);
				tmd2725_hub_i2c_write_byte(state, TMD2725_CALIB_REG, START_OFFSET_CALIB);
				in_calib = true;
				calib_count = 0;
				/* Start timer */
				sns_ddf_timer_start(state->calib_timer, 1000*10);
			}

			if (int_trigger)
			{
				tmd2725_hub_i2c_write_byte(state, TMD2725_PILT_REG, prox_th_low);
				tmd2725_hub_i2c_write_byte(state, TMD2725_PIHT_REG, prox_th_high);
			}
		}
	}

	return ret;
}

void tmd2725_hub_read_als(sns_dd_tmd2725_state_t* state)
{
	uint8_t data[4];
	int32_t ret;

	memset((void*)data, 0, sizeof(data));
	ret = tmd2725_hub_i2c_read_byte_block(state, TMD2725_PHOTOPICL_REG, data, 4);
	if (ret > 0)
	{
		state->chip.als_inf.photopic = (uint16_t)(((uint16_t)data[1]<<8)|data[0]);
		state->chip.als_inf.ir = (uint16_t)(((uint16_t)data[3]<<8)|data[2]);
	}
}

#ifndef TMD2725_ALS_POLLING_IN_DRI
static void tmd2725_hub_update_als_thresh(sns_dd_tmd2725_state_t* state)
{
	uint8_t data[4];
	uint32_t high_thresh = 0;
	uint32_t low_thresh = 0;
	uint32_t max_count = 0;
	uint8_t atime = state->chip.params.als_time + 1;

	memset((void*)data, 0, sizeof(data));

	/* Calculate new thresholds with current photopic channel and delta percentage setting */
	max_count = ((uint32_t)(atime) << 10) - 1;
	high_thresh = (state->chip.als_inf.photopic * (100 + state->chip.als_inf.deltaP) / 100);
	high_thresh = high_thresh > max_count ? max_count:high_thresh;
	state->chip.params.als_th_high = (uint16_t)high_thresh;

	low_thresh = (state->chip.als_inf.photopic * (100 - state->chip.als_inf.deltaP) / 100);
	state->chip.params.als_th_low = (uint16_t)low_thresh;

	/* Write the threshold to registers */
	data[0] = (uint8_t)(state->chip.params.als_th_low & 0xFF);
	data[1] = (uint8_t)((state->chip.params.als_th_low>>8) & 0xFF);
	data[2] = (uint8_t)(state->chip.params.als_th_high & 0xFF);
	data[3] = (uint8_t)((state->chip.params.als_th_high>>8) & 0xFF);

	tmd2725_hub_i2c_write_byte(state, TMD2725_AILTL_REG, data[0]);
	tmd2725_hub_i2c_write_byte(state, TMD2725_AILTH_REG, data[1]);
	tmd2725_hub_i2c_write_byte(state, TMD2725_AIHTL_REG, data[2]);
	tmd2725_hub_i2c_write_byte(state, TMD2725_AIHTH_REG, data[3]);
}
#endif

static int32_t tmd2725_hub_set_als_gain(sns_dd_tmd2725_state_t* state, int32_t gain)
{
	int32_t ret;
	uint8_t ctrl_reg = 0;

	switch (gain)
	{
	case 1:
		ctrl_reg = AGAIN_1;
		break;
	case 4:
		ctrl_reg = AGAIN_4;
		break;
	case 16:
		ctrl_reg = AGAIN_16;
		break;
	case 64:
		ctrl_reg = AGAIN_64;
		break;
	default:
		break;
	}

	ret = tmd2725_hub_i2c_write_byte(state, TMD2725_CFG1_REG, ctrl_reg);
	if (ret > 0)
	{
		state->chip.params.cfg1 = ctrl_reg;
	}
	return ret;
}

static void tmd2725_hub_calc_cpl(sns_dd_tmd2725_state_t* state)
{
	uint32_t cpl;
	uint32_t sat;
	uint32_t tmp1, tmp2;
	uint8_t atime = state->chip.params.als_time + 1;

	/* Calculate ALS time and gain */
	cpl = atime;
	cpl *= TMD2725_ATIME_PER_100;
	cpl /= 100;
	cpl *= als_gains[state->chip.params.cfg1];

	/* Calculate the saturation level */
	tmp1= MAX_ALS_VALUE;
	tmp2= ((uint32_t)(atime) << 10) - 1;
	sat = tmp1 < tmp2 ? tmp1:tmp2;
	sat = sat * 8 / 10;
	state->chip.als_inf.cpl = cpl;

	state->chip.als_inf.saturation = sat;
}

int32_t tmd2725_hub_get_lux(sns_dd_tmd2725_state_t* state)
{
	int32_t lux = 0, lux1 = 0, lux2 = 0;
	uint32_t sat;
	uint32_t sf;

	/* Use time in ms and get scaling factor */
	tmd2725_hub_calc_cpl(state);
	sat = state->chip.als_inf.saturation;

	if (!state->chip.als_gain_auto)
	{
		ready_for_report = true;
		if (state->chip.als_inf.photopic <= MIN_ALS_VALUE)
		{
			lux = 0;
			goto exit;
		}
		else if (state->chip.als_inf.photopic >= sat)
		{
			lux = SNS_DD_ALS_RANGE_MAX;
			goto exit;
		}
	}
	else
	{
		/* Auto ALS gain setting is enabled */
		uint8_t gain = als_gains[state->chip.params.cfg1];
		int32_t ret = -1;

		if (gain == 16 && (state->chip.als_inf.photopic >= sat || state->chip.als_inf.ir >= sat))
		{
			ret = tmd2725_hub_set_als_gain(state, 1);
		}
		else if (gain == 16 && state->chip.als_inf.photopic < GAIN_SWITCH_LEVEL)
		{
			ret = tmd2725_hub_set_als_gain(state, 64);
		}
		else if ((gain == 64 && (state->chip.als_inf.photopic >= sat || state->chip.als_inf.ir >= sat))
			|| (gain == 1 && state->chip.als_inf.photopic < GAIN_SWITCH_LEVEL))
		{
			ret = tmd2725_hub_set_als_gain(state, 16);
		}
		if (ret > 0)
		{
			/* ALS gain is changed, give up current lux calculation */
			tmd2725_hub_calc_cpl(state);
			lux = state->chip.als_inf.lux;
			goto exit;
		}

		/* ALS gain is stable, ALS data is available now */
		ready_for_report = true;

		if (state->chip.als_inf.photopic <= MIN_ALS_VALUE)
		{
			lux = 0;
			goto exit;
		}
		else if (state->chip.als_inf.photopic >= sat)
		{
			lux = SNS_DD_ALS_RANGE_MAX;
			goto exit;
		}
	}

TMD2725_DD_UIMG_MSG_1(ERROR, "tmd2725_hub_get_lux : chip.als_inf.photopic =%d \n",state->chip.als_inf.photopic);
TMD2725_DD_UIMG_MSG_1(ERROR, "tmd2725_hub_get_lux : chip.als_inf.ir =%d \n",state->chip.als_inf.ir);
TMD2725_DD_UIMG_MSG_1(ERROR, "tmd2725_hub_get_lux : als_gains[state->chip.params.cfg1] =%d \n",als_gains[state->chip.params.cfg1]);


	lux1 = state->chip.als_inf.photopic * 1000 - state->chip.als_inf.ir * B_Coef;
	lux2 = state->chip.als_inf.photopic * C_Coef - state->chip.als_inf.ir * D_Coef;
	lux = lux1 > lux2 ? lux1:lux2;

	if (lux < 0)
	{
		lux = 0;
		goto exit;
	}

	sf = state->chip.als_inf.cpl;
	if (lux < (100*sf))
	{
		lux *= D_Factor;
		lux /= sf;
	}
	else
	{
		lux /= sf;
		lux *= D_Factor;
	}

	lux += 500;

	/* Scalling */
	//lux *= state->chip.als_inf.lux_scale_1000;
	//lux /= 1000;
       /* Scalling */

       if ( lux > 1000 * 1000 )
       {
         lux /= 1000;
         lux *= state->chip.als_inf.lux_scale_1000;
       }
       else
       {
         lux *= state->chip.als_inf.lux_scale_1000;
         lux /= 1000;
       }


	state->chip.als_inf.mlux = lux;
	lux /= 1000;

	if (lux > SNS_DD_ALS_RANGE_MAX)
	{
		lux = SNS_DD_ALS_RANGE_MAX;
	}

exit:
	state->chip.als_inf.lux = (uint16_t)lux;
	if (lux == 0 || lux == SNS_DD_ALS_RANGE_MAX)
	{
		state->chip.als_inf.mlux = lux * 1000;
	}
	return 0;
}


#if 0
void sns_dd_tmd2725_log_sensor_data(
	sns_dd_tmd2725_state_t *state,
	sns_ddf_sensor_data_s  *sensor_data)
{
	sns_log_sensor_data_pkt_s *log_struct_ptr;
	sns_err_code_e err_code;

	/* Allocate log packet */
	err_code = sns_logpkt_malloc(
				SNS_LOG_CONVERTED_SENSOR_DATA_PUBLIC,
				sizeof(sns_log_sensor_data_pkt_s),
				(void**)&log_struct_ptr);
	if(err_code != SNS_SUCCESS)
	{
		TMD2725_DD_UIMG_MSG_1(ERROR, "sns_logpkt_malloc - error:%d\n", err_code);
	}
	else if ((err_code == SNS_SUCCESS) && (log_struct_ptr != NULL))
	{
		log_struct_ptr->version   = SNS_LOG_SENSOR_DATA_PKT_VERSION;
		log_struct_ptr->sensor_id = sensor_data->sensor;
		log_struct_ptr->vendor_id = SNS_DDF_VENDOR_AMS;

		log_struct_ptr->timestamp = sensor_data->timestamp;

		if (sensor_data->sensor == SNS_DDF_SENSOR_AMBIENT)
		{
			log_struct_ptr->num_data_types = 3;
			log_struct_ptr->data[0] = state->chip.als_inf.mlux;
			log_struct_ptr->data[1] = sensor_data->samples[0].sample;
			log_struct_ptr->data[2] = sensor_data->samples[1].sample;
		}
		else if (sensor_data->sensor == SNS_DDF_SENSOR_PROXIMITY)
		{
			log_struct_ptr->num_data_types = 5;
			log_struct_ptr->data[0] = state->chip.prox_inf.detected;
			log_struct_ptr->data[1] = sensor_data->samples[0].sample;
			log_struct_ptr->data[2] = 0;
			log_struct_ptr->data[3] = 0;
			log_struct_ptr->data[4] = sensor_data->samples[1].sample;
		}

		/* Commit log (also frees up the log packet memory) */
		err_code = sns_logpkt_commit(SNS_LOG_CONVERTED_SENSOR_DATA_PUBLIC, log_struct_ptr);
		if(err_code != SNS_SUCCESS)
        {
            TMD2725_DD_UIMG_MSG_1(ERROR, "sns_logpkt_commit - error:d\n", err_code);
        }
	}
}
#endif

static sns_ddf_status_e tmd2725_hub_prox_sensor_samples(
	sns_dd_tmd2725_state_t   *state,
	sns_ddf_sensor_data_s    *data_ptr,
	sns_ddf_memhandler_s     *memhandler,
	sns_ddf_sensor_sample_s  *sample_data) 
{
	uint32_t num_samples = 2;
	uint32_t prox_data = state->chip.prox_inf.raw;

	if (memhandler != NULL)
	{
		data_ptr->samples = sns_ddf_memhandler_malloc_ex(memhandler,
								num_samples * sizeof(sns_ddf_sensor_sample_s),
								state->smgr_handle);
		if (data_ptr->samples == NULL)
		{
			return SNS_DDF_ENOMEM;
		}
	}
	else if (sample_data != NULL)
	{
		data_ptr->samples = sample_data;
	}
	else
	{
		return SNS_DDF_EINVALID_PARAM;
	}

	data_ptr->samples[0].sample = FX_CONV_Q16(state->chip.prox_inf.detected, 0);
	data_ptr->samples[0].status = SNS_DDF_SUCCESS;
	data_ptr->samples[1].sample = prox_data;
	data_ptr->samples[1].status = SNS_DDF_SUCCESS;
	data_ptr->num_samples       = num_samples;
	//sns_dd_tmd2725_log_sensor_data(state, data_ptr);

	return SNS_DDF_SUCCESS;
}

static void tmd2725_hub_report_prox(sns_dd_tmd2725_state_t* state)
{
	static sns_ddf_sensor_data_s   *sensor_data;
	static sns_ddf_sensor_sample_s *buf_ptr;

	TMD2725_DD_UIMG_MSG_2(HIGH, "tmd2725_hub_report_prox(): pdata:%d detected=%d\n",
							state->chip.prox_inf.raw, state->chip.prox_inf.detected);

	if ((sns_ddf_malloc_ex((void **)&sensor_data, sizeof(sns_ddf_sensor_data_s),
							state->smgr_handle)) != SNS_DDF_SUCCESS)
	{
		TMD2725_DD_UIMG_MSG_0(ERROR, "report_prox():sns_ddf_malloc_ex()-- sensor_data error!\n");
	}
	else
	{
		sensor_data->sensor    = SNS_DDF_SENSOR_PROXIMITY;
		sensor_data->status    = SNS_DDF_SUCCESS;
		sensor_data->timestamp = sns_ddf_get_timestamp();

		if ((sns_ddf_malloc_ex((void **)&buf_ptr, 2 * sizeof(sns_ddf_sensor_sample_s),
								state->smgr_handle)) != SNS_DDF_SUCCESS)
		{
			TMD2725_DD_UIMG_MSG_0(ERROR, "report_prox():sns_ddf_malloc_ex()--sensor_sample error!\n");
			sns_ddf_mfree_ex(sensor_data, state->smgr_handle);
		}
		else
		{
			tmd2725_hub_prox_sensor_samples(state, sensor_data, NULL, buf_ptr);
			sns_ddf_smgr_notify_data(state->smgr_handle, sensor_data, 1);

			sns_ddf_mfree_ex(buf_ptr, state->smgr_handle);
			sns_ddf_mfree_ex(sensor_data, state->smgr_handle);
		}
	}
}

static sns_ddf_status_e tmd2725_hub_als_sensor_samples(
	sns_dd_tmd2725_state_t   *state,
	sns_ddf_sensor_data_s    *data_ptr,
	sns_ddf_memhandler_s     *memhandler,
	sns_ddf_sensor_sample_s  *sample_data) 
{
	uint32_t num_samples = 2;
	uint32_t luxtoq16 = state->chip.als_inf.lux;
	uint32_t photopic = state->chip.als_inf.photopic;

	if (memhandler != NULL)
	{
		data_ptr->samples = sns_ddf_memhandler_malloc_ex(memhandler,
								num_samples * sizeof(sns_ddf_sensor_sample_s),
								state->smgr_handle);
		if (data_ptr->samples == NULL)
		{
			return SNS_DDF_ENOMEM;
		}
	}
	else if (sample_data != NULL)
	{
		data_ptr->samples = sample_data;
	}
	else
	{
		return SNS_DDF_EINVALID_PARAM;
	}

	data_ptr->samples[0].sample = FX_FLTTOFIX_Q16(luxtoq16);
	data_ptr->samples[0].status = SNS_DDF_SUCCESS;
	data_ptr->samples[1].sample = photopic;
	data_ptr->samples[1].status = SNS_DDF_SUCCESS;
	data_ptr->num_samples       = num_samples;
	//sns_dd_tmd2725_log_sensor_data(state, data_ptr);

	return SNS_DDF_SUCCESS;
}

static void tmd2725_hub_report_als(sns_dd_tmd2725_state_t* state)
{
	static sns_ddf_sensor_data_s *sensor_data;
	static sns_ddf_sensor_sample_s *buf_ptr;

	TMD2725_DD_UIMG_MSG_1(HIGH, "tmd2725_hub_report_als():lux=%d\n", state->chip.als_inf.lux);

	if ((sns_ddf_malloc_ex((void **)&sensor_data, sizeof(sns_ddf_sensor_data_s),
							state->smgr_handle)) != SNS_DDF_SUCCESS)
	{
		TMD2725_DD_UIMG_MSG_0(ERROR, "report_als():sns_ddf_malloc()-- sensor_data error!\n");
	}
	else
	{
		sensor_data->sensor    = SNS_DDF_SENSOR_AMBIENT;
		sensor_data->status    = SNS_DDF_SUCCESS;
		sensor_data->timestamp = sns_ddf_get_timestamp();

		if ((sns_ddf_malloc_ex((void **)&buf_ptr, 2 * sizeof(sns_ddf_sensor_sample_s),
								state->smgr_handle)) != SNS_DDF_SUCCESS)
		{
			TMD2725_DD_UIMG_MSG_0(ERROR, "report_als():sns_ddf_malloc()-- sensor_sample error!\n");
			sns_ddf_mfree_ex(sensor_data, state->smgr_handle);
		}
		else
		{
			tmd2725_hub_als_sensor_samples(state, sensor_data, NULL, buf_ptr);
			sns_ddf_smgr_notify_data(state->smgr_handle, sensor_data, 1);

			sns_ddf_mfree_ex(buf_ptr, state->smgr_handle);
			sns_ddf_mfree_ex(sensor_data, state->smgr_handle);
		}
	}
}

/*===========================================================================*/
/*
* @brief Requests a single sample of sensor data. 
*        Data is returned immediately after being read from the sensor.
*
* @param[in]  handle       Handle to driver instance.
* @param[in]  sensors      List of sensors for which data is requested.
* @param[in]  num_sensors  Length of @a sensors.
* @param[in]  memhandler   Memory handler used to dynamically allocate 
*                          output parameters, if applicable.
* @param[out] data         Sampled sensor data.

* @return SNS_DDF_SUCCESS if data was populated successfully, error code 
*         otherwise.
*/
/*===========================================================================*/
sns_ddf_status_e sns_dd_tmd2725_get_data(
	sns_ddf_handle_t         handle,
	sns_ddf_sensor_e         sensors[],
	uint32_t                 num_sensors,
	sns_ddf_memhandler_s*    memhandler,
	sns_ddf_sensor_data_s**  data)
{
	sns_dd_tmd2725_state_t *state = (sns_dd_tmd2725_state_t *)handle;
	sns_ddf_status_e status = SNS_DDF_SUCCESS;
	sns_ddf_time_t timestamp;
	sns_ddf_sensor_data_s *data_ptr;
	uint8_t i = 0;

	uint8_t datavalid = 0;

	TMD2725_DD_UIMG_MSG_1(HIGH, "tmd2725 get_data() num_sensors:%d\n", num_sensors);
	/* Basic sanity check */
	if (handle == NULL || sensors == NULL) 
	{
		return SNS_DDF_EINVALID_PARAM;
	}

	for(i = 0; i < num_sensors; i++)
	{
		switch(sensors[i])
		{
		case SNS_DDF_SENSOR_PROXIMITY:
			TMD2725_DD_UIMG_MSG_0(HIGH, "tmd2725 get_data() request prox data\n");
			data_requested |= PINT;
			break;

		case SNS_DDF_SENSOR_AMBIENT:
			TMD2725_DD_UIMG_MSG_0(HIGH, "tmd2725 get_data() request als data\n");
			data_requested |= AINT;
			break;

		default:
			break;
		}
	}

	if (first_prox || first_als)
	{
		tmd2725_hub_i2c_read_byte(state, TMD2725_STATUS_REG, &datavalid);
		if ((datavalid&data_requested) != data_requested)
		{
			TMD2725_DD_UIMG_MSG_0(HIGH, "data requested has not been valid yet. return pending...\n");
			status = sns_ddf_timer_start(state->polling_timer, 1000*10);
			if (status != SNS_DDF_SUCCESS)
			{
				return status;
			}
			return SNS_DDF_PENDING;
		}
	}

	if (data_requested & PINT)
	{
		/* Read proximity */
		tmd2725_hub_read_prox_data(state);
		tmd2725_hub_get_prox_event(state, false, &first_prox);
		TMD2725_DD_UIMG_MSG_1(HIGH, "tmd2725 get_data() pdata=%d\n", state->chip.prox_inf.raw);
	}

	if (data_requested & AINT)
	{
		/* Read als */
		tmd2725_hub_read_als(state);
		tmd2725_hub_get_lux(state);
		TMD2725_DD_UIMG_MSG_1(HIGH, "tmd2725 get_data() lux=%d\n", state->chip.als_inf.lux);
	}

	timestamp = sns_ddf_get_timestamp();
	data_ptr = sns_ddf_memhandler_malloc_ex(memhandler, 
							num_sensors * sizeof(sns_ddf_sensor_data_s), state->smgr_handle);
	if (data_ptr == NULL)
	{
		return SNS_DDF_ENOMEM;
	}
	*data = data_ptr;

	for(i = 0; i < num_sensors; i++)
	{
		data_ptr[i].sensor    = sensors[i];
		data_ptr[i].status    = SNS_DDF_SUCCESS;
		data_ptr[i].timestamp = timestamp;

		switch(sensors[i])
		{
		case SNS_DDF_SENSOR_PROXIMITY:
			TMD2725_DD_UIMG_MSG_0(HIGH, "tmd2725 get_data() report prox\n");
			data_requested &= ~PINT;
			tmd2725_hub_prox_sensor_samples(state, &data_ptr[i], memhandler, NULL);
			break;

		case SNS_DDF_SENSOR_AMBIENT:
			if (ready_for_report)
			{
				if (first_als)
				{
					first_als = false;
				}
				TMD2725_DD_UIMG_MSG_0(HIGH, "tmd2725 get_data() report als\n");
				data_requested &= ~AINT;
				tmd2725_hub_als_sensor_samples(state, &data_ptr[i], memhandler, NULL);
			}
			break;

		default:
			break;
		}
	}

	return status;
}

void sns_dd_tmd2725_handle_timer(sns_ddf_handle_t handle, void* arg)
{
	uint8_t status = 0;
	sns_dd_tmd2725_state_t *state = (sns_dd_tmd2725_state_t *)handle;
	sns_ddf_sensor_e sensor = *(sns_ddf_sensor_e*)arg;

	in_timer_handler = true;
	if (sensor == CALIB_TIMER_ARG)
	{
		if (in_calib)
		{
			if (!state->dri_enabled)
			{
				tmd2725_hub_i2c_read_byte(state, TMD2725_STATUS_REG, &status);
			}
			calib_count++;
			TMD2725_DD_UIMG_MSG_2(HIGH, "tmd2725 handle timer() calib_count:%d status:0x%x\n", calib_count, status);
			if ((status & CINT) || calib_count == 10)
			{
				in_calib = false;
				if (status & CINT)
				{
					tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, CINT, CINT);
				}
				else
				{
					tmd2725_hub_i2c_modify(state, TMD2725_PERS_REG, PPERS, PROX_PERSIST(0));
					tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, (PIEN|CIEN), PIEN);
				}

				if (state->chip.als_enabled)
				{
					tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, AINT, AINT);
					tmd2725_hub_i2c_modify(state, TMD2725_ENABLE_REG, (PEN|AEN), (PEN|AEN));
				}
				else
				{
					tmd2725_hub_i2c_modify(state, TMD2725_ENABLE_REG, (PEN|WEN), (PEN|WEN));
				}
			}
			else
			{
				sns_ddf_timer_start(state->calib_timer, 1000*10);
			}
		}
	}
	else if (sensor == POLLING_TIMER_ARG)
	{
		if (state->dri_enabled)
		{
#ifdef TMD2725_ALS_POLLING_IN_DRI
			tmd2725_hub_read_als(state);
			tmd2725_hub_get_lux(state);
			if (ready_for_report)
			{
				tmd2725_hub_report_als(state);
			}
			if (als_timer_stop == false)
			{
				sns_ddf_timer_start(state->polling_timer, 1000*100);
			}
#endif
		}
		else
		{
			tmd2725_hub_i2c_read_byte(state, TMD2725_STATUS_REG, &status);
			if ((status&data_requested) == data_requested)
			{
				if (data_requested & PINT)
				{
					/* Read proximity */
					tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, PINT, PINT);
					tmd2725_hub_read_prox_data(state);
					if (tmd2725_hub_get_prox_event(state, false, &first_prox))
					{
						tmd2725_hub_report_prox(state);
					}
					data_requested &= ~PINT;
				}
				if (data_requested & AINT)
				{
					/* Read als */
					tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, AINT, AINT);
					tmd2725_hub_read_als(state);
					tmd2725_hub_get_lux(state);
					if (ready_for_report)
					{
						tmd2725_hub_report_als(state);
						data_requested &= ~AINT;
						if (first_als)
						{
							first_als = false;
						}
					}
					else
					{
						sns_ddf_timer_start(state->polling_timer, 1000*10);
					}
				}
			}
			else
			{
				if (state->chip.prox_enabled || state->chip.als_enabled)
				{
					sns_ddf_timer_start(state->polling_timer, 1000*10);
				}
			}
		}
	}

	in_timer_handler = false;
}

static void tmd2725_hub_check_and_report(sns_dd_tmd2725_state_t* state)
{
	uint8_t status;

	tmd2725_hub_i2c_read_byte(state, TMD2725_STATUS_REG, &status);

	if (status & PINT)
	{
		/* Proximity interrupt, read Pdata and get prox event */
		tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, PINT, PINT);
		tmd2725_hub_read_prox_data(state);
		if (tmd2725_hub_get_prox_event(state, true, &first_prox))
		{
			tmd2725_hub_report_prox(state);
		}
	}
#ifndef TMD2725_ALS_POLLING_IN_DRI
	if (status & AINT)
	{
		/* ALS interrupt, read ALS and get lux data */
		tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, AINT, AINT);
		tmd2725_hub_read_als(state);
		tmd2725_hub_get_lux(state);
		if (ready_for_report)
		{
			/* Lux changing level */
			state->chip.als_inf.deltaP = 10;
			if (first_als)
			{
				first_als = false;
				tmd2725_hub_i2c_modify(state, TMD2725_PERS_REG, APERS, ALS_PERSIST(1));
			}
			tmd2725_hub_update_als_thresh(state);
			tmd2725_hub_report_als(state);
		}
	}
#endif
	if (status & CINT)
	{
		/* Offset calibration completed */
		in_calib = false;
		tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, CINT, CINT);
		tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, (PIEN|CIEN), PIEN);

		if (first_prox)
		{
			tmd2725_hub_i2c_modify(state, TMD2725_PERS_REG, PPERS, PROX_PERSIST(0));
		}
		if (state->chip.als_enabled)
		{
			tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, AINT, AINT);
			tmd2725_hub_i2c_modify(state, TMD2725_ENABLE_REG, (PEN|AEN), (PEN|AEN));
		}
		else
		{
			tmd2725_hub_i2c_modify(state, TMD2725_ENABLE_REG, (PEN|WEN), (PEN|WEN));
		}
	}
}

/*===========================================================================*/
/*
* @brief Called in response to an interrupt for this driver.
*  
* @note This function will be called within the context of the SMGR task, 
*       *not* the ISR. 
*
* @param[in] handle     Handle to a driver instance. 
* @param[in] gpio_num   GPIO number that triggered this interrupt.
* @param[in] timestamp  Time at which interrupt happened.
*/
/*===========================================================================*/
void sns_dd_tmd2725_handle_irq(
	sns_ddf_handle_t         handle,
	uint32_t                 gpio_num,
	sns_ddf_time_t           timestamp)
{
	sns_dd_tmd2725_state_t* state = (sns_dd_tmd2725_state_t*)handle;

	tmd2725_hub_check_and_report(state);
}
