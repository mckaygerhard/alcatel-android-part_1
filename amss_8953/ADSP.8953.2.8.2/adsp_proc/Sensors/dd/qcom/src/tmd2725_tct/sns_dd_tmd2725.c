/*
 * Copyright (c) 2016, ams AG
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*==============================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

when                 who              what, where, why
------------    --------      --------------------------------------------------
28/04/2016      Byron Shi      Make tmd2725 driver for Qualcomm aDSP
08/05/2016      Byron Shi      ALS use interrupt
09/05/2016      Byron Shi      Add NV params, ALS SELF TEST, get_data() support
13/06/2016      Byron Shi      Add ALS polling in DRI mode, update Prox algorithm
23/06/2016      Byron Shi      Add Prox SELF TEST, checklist for QCOM, update BSD header
16/08/2016      Byron Shi      Support OpenSSC v3.4/v3.5
05/09/2016      Byron Shi      Support Data log Packets

==============================================================================*/

#include <stdio.h>
#include "sns_ddf_attrib.h"
#include "sns_ddf_common.h"
#include "sns_ddf_comm.h"
#include "sns_ddf_driver_if.h"
#include "sns_ddf_smgr_if.h"
#include "sns_ddf_util.h"
#include "sns_ddf_memhandler.h"
#include "sns_ddf_signal.h"
#include "sns_dd_tmd2725_priv.h"

#include "sns_log_types_public.h"
#include "sns_log_api_public.h"
#include "qurt_elite_diag.h"

/*===========================================================================*/
/*
*  External variable
*/
/*===========================================================================*/
extern bool first_prox;
extern bool in_calib;
extern uint8_t calib_count;

#ifdef TMD2725_ALS_POLLING_IN_DRI
extern bool als_timer_stop;
#endif
extern bool first_als;
extern bool ready_for_report;

extern uint8_t data_requested;

extern bool in_timer_handler;

extern sns_ddf_driver_if_s sns_tmd2725_driver_fn_list;

/*===========================================================================*/
/*
*  Parameters
*/
/*===========================================================================*/
static sns_ddf_sensor_e dd_tmd2725_sensors[2] = { SNS_DDF_SENSOR_PROXIMITY, SNS_DDF_SENSOR_AMBIENT };

static sns_ddf_odr_t tmd2725_odr_list[] = {
	SNS_DD_PROX_FREQ
};

static uint32_t tmd2725_odr_list_size = sizeof(tmd2725_odr_list)/sizeof(tmd2725_odr_list[0]);

static uint8_t const tmd2725_ids[] = {
	0xE4, /* TMD2725 */
};

static tmd2725_parameters_s param_default = {
	.als_time     = ATIME_MS(50),
	.prox_time    = PTIME_US(4000),
	.wait_time    = WTIME_MS(50),
	.als_th_low   = 0x00,
	.als_th_high  = 0xFFFF,
	.prox_th_low  = SNS_DD_PROX_THRESH_FAR,
	.prox_th_high = SNS_DD_PROX_THRESH_NEAR,
	.persistence  = PROX_PERSIST(0) | ALS_PERSIST(0),
	.cfg0         = 0x80,
	.prox_cfg0    = (PPLEN_8US | PROX_PULSE_CNT(16)),
	.prox_cfg1    = (PGAIN_4 | PROX_LDRIVE_MA(150)),
	.cfg1         = AGAIN_16,
	.cfg3         = 0x4C,
};

/*===========================================================================*/
/*
*  External functions
*/
/*===========================================================================*/
extern int32_t tmd2725_hub_i2c_read_byte(sns_dd_tmd2725_state_t* state, uint8_t reg, uint8_t *val);
extern int32_t tmd2725_hub_i2c_write_byte(sns_dd_tmd2725_state_t* state, uint8_t reg, uint8_t val);
extern int32_t tmd2725_hub_i2c_modify(sns_dd_tmd2725_state_t* state, uint8_t reg, uint8_t mask, uint8_t val);

extern void tmd2725_hub_read_als(sns_dd_tmd2725_state_t* state);
extern int32_t tmd2725_hub_get_lux(sns_dd_tmd2725_state_t* state);

/*===========================================================================*/
/*
*  Local functions
*/
/*===========================================================================*/
static sns_ddf_status_e tmd2725_hub_prox_enable(sns_dd_tmd2725_state_t* state, int32_t on)
{
	sns_ddf_status_e status = SNS_DDF_SUCCESS;

	if (on)
	{
		/* Start offset calibration and clear PINT before PEN */
		tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, PON);
		tmd2725_hub_i2c_modify(state, TMD2725_PERS_REG, PPERS, PROX_PERSIST(0));
		tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, (PINT|CINT), (PINT|CINT));
		tmd2725_hub_i2c_write_byte(state, TMD2725_CALIBCFG_REG, (BINSRCH_TARGET_0|0x09));
		tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, (PIEN|CIEN), CIEN);
		tmd2725_hub_i2c_write_byte(state, TMD2725_CALIB_REG, START_OFFSET_CALIB);
		first_prox = true;
		in_calib = true;
		calib_count = 0;

		/* Start one timer to limit calibration duration */
		status = sns_ddf_timer_start(state->calib_timer, 1000*10);
	}
	else
	{
		/* Cancle timer, clear raw data and calibration flag */
		status = sns_ddf_timer_cancel(state->calib_timer);
		in_calib = false;
		state->chip.prox_inf.raw = 0;

		/* Disable PEN */
		tmd2725_hub_i2c_modify(state, TMD2725_ENABLE_REG, (PEN|WEN), 0);
		data_requested &= ~PINT;
		first_prox = false;
	}

	state->chip.prox_enabled = on;

	return status;
}

static sns_ddf_status_e tmd2725_hub_als_enable(sns_dd_tmd2725_state_t* state, int32_t on)
{
	sns_ddf_status_e status = SNS_DDF_SUCCESS;

	if (on)
	{
		/* Clear AINT */
		tmd2725_hub_i2c_modify(state, TMD2725_PERS_REG, APERS, ALS_PERSIST(0));
		tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, AINT, AINT);
		/* Wait for calibration complete */
		if (!in_calib)
		{
			tmd2725_hub_i2c_modify(state, TMD2725_ENABLE_REG, (AEN|WEN), AEN);
		}

		first_als = true;
		ready_for_report = false;
	}
	else
	{
		/* Disable AEN, and enable WEN if proximity is on */
		if (state->chip.prox_enabled)
		{
			tmd2725_hub_i2c_modify(state, TMD2725_ENABLE_REG, (AEN|WEN), WEN);
		}
		else
		{
			tmd2725_hub_i2c_modify(state, TMD2725_ENABLE_REG, AEN, 0);
		}
		data_requested &= ~AINT;
		first_als = false;
	}

	state->chip.als_enabled = on;

	return status;
}

static int32_t tmd2725_hub_set_defaults(sns_dd_tmd2725_state_t* state)
{
	int32_t ret = 0;

	TMD2725_DD_MSG_0(HIGH, "tmd2725_hub_set_defaults: use defaults\n");
	/* Get default setting */
	state->chip.params.als_time     = param_default.als_time;
	state->chip.params.prox_time    = param_default.prox_time;
	state->chip.params.wait_time    = param_default.wait_time;
	state->chip.params.als_th_low   = param_default.als_th_low;
	state->chip.params.als_th_high  = param_default.als_th_high;
	state->chip.params.prox_th_low  = param_default.prox_th_low;
	state->chip.params.prox_th_high = param_default.prox_th_high;
	state->chip.params.persistence  = param_default.persistence;
	state->chip.params.cfg0         = param_default.cfg0;
	state->chip.params.prox_cfg0    = param_default.prox_cfg0;
	state->chip.params.prox_cfg1    = param_default.prox_cfg1;
	state->chip.params.cfg1         = param_default.cfg1;
	state->chip.params.cfg3         = param_default.cfg3;

	state->chip.params.prox_th_contaminated =
		state->chip.params.prox_th_low > SNS_DD_PROX_THRESH_CONTAMINATED ?
		state->chip.params.prox_th_low : SNS_DD_PROX_THRESH_CONTAMINATED;

	if (state->chip.params.prox_th_contaminated == state->chip.params.prox_th_low)
	{
		state->chip.params.prox_th_verynear = 0xFF;
	}
	else
	{
		state->chip.params.prox_th_verynear =
			state->chip.params.prox_th_high > SNS_DD_PROX_THRESH_VERY_NEAR ?
			state->chip.params.prox_th_high : SNS_DD_PROX_THRESH_VERY_NEAR;
	}

	state->chip.als_gain_auto = true;
	/* Disable sensor */
	ret = tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, 0x00);
	if (ret <= 0)
	{
		return ret;
	}

	/* Write registers */
	tmd2725_hub_i2c_write_byte(state, TMD2725_ATIME_REG, state->chip.params.als_time);
	tmd2725_hub_i2c_write_byte(state, TMD2725_PRATE_REG, state->chip.params.prox_time);
	tmd2725_hub_i2c_write_byte(state, TMD2725_WTIME_REG, state->chip.params.wait_time);
	tmd2725_hub_i2c_write_byte(state, TMD2725_AILTL_REG, (uint8_t)(state->chip.params.als_th_low&0xFF));
	tmd2725_hub_i2c_write_byte(state, TMD2725_AILTH_REG, (uint8_t)((state->chip.params.als_th_low&0xFF00)>>8));
	tmd2725_hub_i2c_write_byte(state, TMD2725_AIHTL_REG, (uint8_t)(state->chip.params.als_th_high&0xFF));
	tmd2725_hub_i2c_write_byte(state, TMD2725_AIHTH_REG, (uint8_t)((state->chip.params.als_th_high&0xFF00)>>8));
	tmd2725_hub_i2c_write_byte(state, TMD2725_PILT_REG, state->chip.params.prox_th_low);
	tmd2725_hub_i2c_write_byte(state, TMD2725_PIHT_REG, state->chip.params.prox_th_high);
	tmd2725_hub_i2c_write_byte(state, TMD2725_PERS_REG, state->chip.params.persistence);
	tmd2725_hub_i2c_write_byte(state, TMD2725_CFG0_REG, state->chip.params.cfg0);
	tmd2725_hub_i2c_write_byte(state, TMD2725_PCFG0_REG, state->chip.params.prox_cfg0);
	tmd2725_hub_i2c_write_byte(state, TMD2725_PCFG1_REG, state->chip.params.prox_cfg1);
	tmd2725_hub_i2c_write_byte(state, TMD2725_CFG1_REG, state->chip.params.cfg1);
	tmd2725_hub_i2c_write_byte(state, TMD2725_CFG3_REG, state->chip.params.cfg3);

	return ret;
}

/*===========================================================================*/
/*
* @brief Resets the driver and device so they return to the state they were 
*        in after init() was called.
*
* @param[in] handle  Handle to a driver instance.
* 
* @return Success if the driver was able to reset its state and the device.
*         Otherwise a specific error code is returned. 
*/
/*===========================================================================*/
sns_ddf_status_e sns_dd_tmd2725_reset(sns_ddf_handle_t handle)
{
	sns_dd_tmd2725_state_t* state = (sns_dd_tmd2725_state_t*)handle;
	sns_ddf_status_e status = SNS_DDF_SUCCESS;
	int32_t ret;

	TMD2725_DD_MSG_0(HIGH, "tmd2725 reset()\n");
	/* Basic sanity check */
	if (handle == NULL)
	{
		return SNS_DDF_EINVALID_PARAM;
	}

	/* Reset power state and etc. */
	state->powerstate = SNS_DDF_POWERSTATE_LOWPOWER;
	state->prox_odr = 0;
	state->als_odr = 0;
	state->chip.prox_enabled = false;
	state->chip.als_enabled = false;
	state->dri_enabled = false;

	/* Configure device registers - initiate device reset */
	ret = tmd2725_hub_set_defaults(state);
	if (ret <= 0)
	{
		status = SNS_DDF_EFAIL;
	}

	return status;
}

/*===========================================================================*/
/*
* @brief Initializes the driver and sets up devices.
*  
* Allocates a handle to a driver instance, opens a communication port to 
* associated devices, configures the driver and devices, and places 
* the devices in the default power state. Returns the instance handle along 
* with a list of supported sensors. This function will be called at init 
* time.
*  
* @param[out] handle_ptr     Pointer that this function must malloc and 
*                            populate. This is a handle to the driver
*                            instance that will be passed in to all other
*                            functions. NB: Do not use @a memhandler to
*                            allocate this memory.
* @param[in]  smgr_handle    Handle used to identify this driver when it 
*                            calls into Sensors Manager functions.
* @param[in]  nv_params      NV parameters retrieved for the driver.
* @param[in]  device_info    Access info for physical devices controlled by 
*                            this driver. Used to configure the bus
*                            and talk to the devices.
* @param[in]  num_devices    Number of elements in @a device_info. 
* @param[in]  memhandler     Memory handler used to dynamically allocate 
*                            output parameters, if applicable. NB: Do not
*                            use memhandler to allocate memory for
*                            @a handle_ptr.
* @param[in/out] sensors     List of supported sensors, allocated, 
*                            populated, and returned by this function.
* @param[in/out] num_sensors Number of elements in @a sensors.
*
* @return Success if @a handle_ptr was allocated and the driver was 
*         configured properly. Otherwise a specific error code is returned.
*/
/*===========================================================================*/
sns_ddf_status_e sns_dd_tmd2725_init(
	sns_ddf_handle_t*        dd_ptr,
	sns_ddf_handle_t         smgr_handle,
	sns_ddf_nv_params_s*     nv_params,
	sns_ddf_device_access_s  device_info[],
	uint32_t                 num_devices,
	sns_ddf_memhandler_s*    memhandler,
	sns_ddf_sensor_e*        sensors[],
	uint32_t*                num_sensors)
{
	sns_ddf_status_e status = SNS_DDF_SUCCESS;
	sns_dd_tmd2725_state_t* state;
	sns_ddf_handle_t handle;
	bool data_from_registry = false;


#ifdef TMD2725_LDO_GPIO_CONTROL
       DALResult           	    eResult  = DAL_ERROR;
#endif

#ifdef TMD2725_CONFIG_ENABLE_UIMAGE
	sns_ddf_smgr_set_uimg_refac(smgr_handle);
#endif

	TMD2725_DD_MSG_0(HIGH, "tmd2725 init()\n");

	if (dd_ptr == NULL || num_sensors == NULL || sensors == NULL)
	{
		TMD2725_DD_MSG_0(ERROR, "tmd2725 init():invalid param\n");
		return SNS_DDF_EINVALID_PARAM;
	}

	/* Allocate a driver instance */
	status = sns_ddf_malloc_ex((void**)&state, sizeof(sns_dd_tmd2725_state_t), smgr_handle);
	if (status != SNS_DDF_SUCCESS)
	{
		TMD2725_DD_MSG_0(ERROR, "tmd2725 init(): malloc state error!\n");
		return SNS_DDF_ENOMEM;
	}

	/* Init memoery */
	memset((void*)state, 0, sizeof(sns_dd_tmd2725_state_t));

	/* Assign the handle_ptr */
	handle = (sns_ddf_handle_t)state;
	*dd_ptr = handle;

	/* Open I2C port */
	status = sns_ddf_open_port(&state->port_handle, &device_info->port_config);
	if (status != SNS_DDF_SUCCESS) 
	{
		TMD2725_DD_MSG_0(ERROR, "I2C open failed\n");
		status = SNS_DDF_EBUS;
		goto fail_1;
	}

	/* Get first_gpio for IRQ register */
	state->smgr_handle = smgr_handle;
	state->gpio1       = device_info->first_gpio;

	/* Init timers */
	state->calib_timer_arg = CALIB_TIMER_ARG;
	status = sns_ddf_timer_init(&state->calib_timer,
								(sns_ddf_handle_t)state,
								&sns_tmd2725_driver_fn_list,
								(void*)&state->calib_timer_arg,
								false);
	if (status != SNS_DDF_SUCCESS)
	{
		TMD2725_DD_MSG_0(ERROR, "calibration timer init failed\n");
		status = SNS_DDF_EFAIL;
		goto fail_2;
	}

	state->polling_timer_arg = POLLING_TIMER_ARG;
	status = sns_ddf_timer_init(&state->polling_timer,
								(sns_ddf_handle_t)state,
								&sns_tmd2725_driver_fn_list,
								(void*)&state->polling_timer_arg,
								false);
	if (status != SNS_DDF_SUCCESS)
	{
		TMD2725_DD_MSG_0(ERROR, "polling timer init failed\n");
		status = SNS_DDF_EFAIL;
		goto fail_2;
	}


	/* Begin add  gpio control for ldo 3.3v */
	#ifdef TMD2725_LDO_GPIO_CONTROL
	// TLMM
	/* attach to GPIO to get gpio_handle */
	if( DAL_DeviceAttach(DALDEVICEID_TLMM, &state->gpio_handle )== DAL_ERROR)
	{
		TMD2725_DD_MSG_0(ERROR, "DAL_DeviceAttach failed in init\n");
	}
	
	/* configruing the pin as output, set to 1 */
	state->gpio_ldo_pin_cfg =DAL_GPIO_CFG_OUT( TMD2725_LDO_GPIO, 0, DAL_GPIO_OUTPUT, DAL_GPIO_NO_PULL, DAL_GPIO_2MA, DAL_GPIO_LOW_VALUE);
	
	/* enable it */
	eResult=DalTlmm_ConfigGpio(state->gpio_handle, state->gpio_ldo_pin_cfg, DAL_TLMM_GPIO_ENABLE );

	if( eResult != DAL_SUCCESS )
	{
		TMD2725_DD_MSG_1(ERROR, "Error enabling 4444 , eResult: %d", eResult);
		goto fail_2;
	}
	
		TMD2725_DD_MSG_0(ERROR, "HAD set GPIO in init\n");

	sns_ddf_delay(100*1000);

        DalTlmm_GpioOut( state->gpio_handle, state->gpio_ldo_pin_cfg, DAL_GPIO_HIGH_VALUE );
        
        sns_ddf_delay(1000);
        
	#endif
	/* End add  gpio control for ldo 3.3v */

	
	/* Reset the device, reset also initializes registers */
	status = sns_dd_tmd2725_reset(handle);
	if (status != SNS_DDF_SUCCESS)
	{
		TMD2725_DD_MSG_0(ERROR, "reset failed in init\n");
		status = SNS_DDF_EFAIL;
		goto fail_2;
	}

	/* Get parameters from NV if it is available */
	if (nv_params)
	{
		if (nv_params->status == SNS_DDF_SUCCESS
			&& nv_params->data_len >= sizeof(tmd2725_nv_db_s))
		{
			tmd2725_nv_db_s *nv_ptr = (tmd2725_nv_db_s *)nv_params->data;
			if (nv_ptr->magic == AMS_MAGIC_NUM)
			{
				data_from_registry = true;
				state->nv_db_size = nv_params->data_len;

				memcpy(&state->nv_db, nv_ptr, sizeof(tmd2725_nv_db_s));
			}
		}

		if (!data_from_registry)
		{
			state->nv_db_size = 128;
			state->nv_db.magic = AMS_MAGIC_NUM;
			state->nv_db.als_factor = 1000;
		}

		state->chip.als_inf.lux_scale_1000 = state->nv_db.als_factor;

		/* Notify to write back to registry in case defaults had to be loaded. */
		if (nv_params && !data_from_registry)
		{
			sns_ddf_smgr_notify_event(smgr_handle, SNS_DDF_SENSOR_AMBIENT, SNS_DDF_EVENT_UPDATE_REGISTRY_GROUP);
		}
	}

	/* Set the output and return */
	*num_sensors = 2;
	*sensors = dd_tmd2725_sensors;

	return status;

fail_2:
	sns_ddf_close_port(state->port_handle);
fail_1:
	sns_ddf_mfree_ex(state->samples, smgr_handle);

	return status;
}

static sns_ddf_status_e sns_dd_tmd2725_set_powerstate(
	sns_dd_tmd2725_state_t*  state,
	sns_ddf_powerstate_e     op_mode)
{
	int32_t ret = 0;

	TMD2725_DD_MSG_0(HIGH, "tmd2725 set_powerstate()\n");
	if (op_mode == SNS_DDF_POWERSTATE_ACTIVE)
	{
		/* Set PON */
		ret = tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, PON);
		if (ret <= 0)
		{
			return SNS_DDF_EBUS;
		}
		sns_ddf_delay(DELAY_MS_AFTER_PON * 1000);

		state->powerstate = SNS_DDF_POWERSTATE_ACTIVE;
	}
	else /* SNS_DDF_POWERSTATE_LOWPOWER */
	{
		/* Clear PON */
		ret = tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, 0);
		if (ret <= 0)
		{
			return SNS_DDF_EBUS;
		}

		state->powerstate = SNS_DDF_POWERSTATE_LOWPOWER;
	}

	return SNS_DDF_SUCCESS;
}

/*===========================================================================*/
/*
* @brief Sets a sensor attribute to a specific value.
*
* @param[in] handle     Handle to a driver instance.
* @param[in] sensor     Sensor for which this attribute is to be set. When 
*                       addressing an attribute that refers to the driver
*                       this value is set to SNS_DDF_SENSOR__ALL.
* @param[in] attrib     Attribute to be set.
* @param[in] value      Value to set this attribute.
*
* @return Success if the value of the attribute was set properly. Otherwise 
*         a specific error code is returned.
*/
/*===========================================================================*/
sns_ddf_status_e sns_dd_tmd2725_set_attrib(
	sns_ddf_handle_t         handle,
	sns_ddf_sensor_e         sensor,
	sns_ddf_attribute_e      attrib,
	void*                    value)
{
	sns_dd_tmd2725_state_t* state = (sns_dd_tmd2725_state_t*)handle;
	sns_ddf_status_e status = SNS_DDF_SUCCESS;

	TMD2725_DD_MSG_2(HIGH, "tmd2725 set_attrib(): sensor=%d, attrib=%d\n", sensor, attrib);

	if (handle == NULL || value == NULL)
	{
		return SNS_DDF_EINVALID_PARAM;
	}

	switch (attrib)
	{
	case SNS_DDF_ATTRIB_POWER_STATE:
		/* Set power mode */
		status = sns_dd_tmd2725_set_powerstate(state, *(sns_ddf_powerstate_e*)value);
		if (status != SNS_DDF_SUCCESS)
		{
			TMD2725_DD_MSG_0(ERROR, "set power fail!\n" );
			return status;
		}
		break;

	case SNS_DDF_ATTRIB_RANGE:
		/* Does not support different ranges */
		break;

	case SNS_DDF_ATTRIB_ODR:
		/* Enable/Disable sensor with ODR setting */
		switch (sensor)
		{
		case SNS_DDF_SENSOR_PROXIMITY:
			state->prox_odr = *(sns_ddf_odr_t *)value;
			if (*(uint32_t*)value != 0)
			{
				status = tmd2725_hub_prox_enable(state, 1);
			}
			else
			{
				status = tmd2725_hub_prox_enable(state, 0);
			}
			break;
		case SNS_DDF_SENSOR_AMBIENT:
			state->als_odr = *(sns_ddf_odr_t *)value;
			if (*(uint32_t*)value != 0)
			{
				status = tmd2725_hub_als_enable(state, 1);
			}
			else
			{
				status = tmd2725_hub_als_enable(state, 0);
			}
			break;
		default:
			break;
		}
		if (status != SNS_DDF_SUCCESS)
		{
			return status;
		}
		break;

	default:
		return SNS_DDF_EINVALID_PARAM;
	}

	return status;
}

sns_ddf_status_e sns_dd_tmd2725_prox_query(
	sns_dd_tmd2725_state_t*  state,
	sns_ddf_memhandler_s*    memhandler,
	sns_ddf_attribute_e      attrib,
	void**                   value,
	uint32_t*                num_elems)
{
	sns_ddf_range_s *device_ranges;
	sns_ddf_resolution_t *resolution;
	sns_ddf_resolution_adc_s *resolution_adc;
	sns_ddf_power_info_s *power;
	uint32_t *odr;

	switch (attrib)
	{
	case SNS_DDF_ATTRIB_RANGE:
		if ((device_ranges = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*device_ranges),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		device_ranges->min = FX_FLTTOFIX_Q16(0);
		device_ranges->max = FX_FLTTOFIX_Q16(SNS_DD_PROX_RANGE_MAX);
		*value = device_ranges;
		*num_elems = 1;       
		break;

	case SNS_DDF_ATTRIB_RESOLUTION_ADC:
		if ((resolution_adc = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*resolution_adc),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		resolution_adc->bit_len = SNS_DD_PROX_BITS;
		resolution_adc->max_freq = SNS_DD_PROX_FREQ;
		*value = resolution_adc;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_RESOLUTION:
		if ((resolution = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*resolution),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		*resolution = FX_FLTTOFIX_Q16(SNS_DD_PROX_RES);
		*value = resolution;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_POWER_INFO:
		if ((power = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*power),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		power->active_current = SNS_DD_PROX_PWR;
		power->lowpower_current = SNS_DD_PROX_LO_PWR;
		*value = power;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_ODR:
		if ((odr = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*odr),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		*odr = state->prox_odr;
		*value = odr;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_SUPPORTED_ODR_LIST:
		*value = tmd2725_odr_list;
		*num_elems = tmd2725_odr_list_size;
		break;

	default:
		return SNS_DDF_EINVALID_ATTR;
	}

	return SNS_DDF_SUCCESS;
}

sns_ddf_status_e sns_dd_tmd2725_als_query(
	sns_dd_tmd2725_state_t*  state,
	sns_ddf_memhandler_s*    memhandler,
	sns_ddf_attribute_e      attrib,
	void**                   value,
	uint32_t*                num_elems)
{
	sns_ddf_range_s *device_ranges;
	sns_ddf_resolution_t *resolution;
	sns_ddf_resolution_adc_s *resolution_adc;
	sns_ddf_power_info_s *power;
	uint32_t *odr;

	switch (attrib)
	{
	case SNS_DDF_ATTRIB_RANGE:
		if ((device_ranges = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*device_ranges),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		device_ranges->min = FX_FLTTOFIX_Q16(0);
		device_ranges->max = FX_FLTTOFIX_Q16(SNS_DD_ALS_RANGE_MAX);
		*value = device_ranges;
		*num_elems = 1;       
		break;

	case SNS_DDF_ATTRIB_RESOLUTION_ADC:
		if ((resolution_adc = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*resolution_adc),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		resolution_adc->bit_len = SNS_DD_ALS_BITS;
		resolution_adc->max_freq = SNS_DD_ALS_FREQ;
		*value = resolution_adc;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_RESOLUTION:
		if ((resolution = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*resolution),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		*resolution = FX_FLTTOFIX_Q16(SNS_DD_ALS_RES);
		*value = resolution;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_POWER_INFO:
		if ((power = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*power),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		power->active_current = SNS_DD_ALS_PWR;
		power->lowpower_current = SNS_DD_ALS_LO_PWR;
		*value = power;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_ODR:
		if ((odr = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(*odr),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		*odr = state->als_odr;
		*value = odr;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_SUPPORTED_ODR_LIST:
		*value = &tmd2725_odr_list;
		*num_elems = tmd2725_odr_list_size;
		break;

	default:
		return SNS_DDF_EINVALID_ATTR;
	}

	return SNS_DDF_SUCCESS;
}

/*===========================================================================*/
/*
* @brief Retrieves the value of an attribute for a sensor.
* 
* @param[in]  handle      Handle to a driver instance.
* @param[in]  sensor      Sensor whose attribute is to be retrieved. When 
*                         addressing an attribute that refers to the driver
*                         this value is set to SNS_DDF_SENSOR__ALL.
* @param[in]  attrib      Attribute to be retrieved.
* @param[in]  memhandler  Memory handler used to dynamically allocate 
*                         output parameters, if applicable.
* @param[out] value       Pointer that this function will allocate or set 
*                         to the attribute's value.
* @param[out] num_elems   Number of elements in @a value.
*  
* @return Success if the attribute was retrieved and the buffer was 
*         populated. Otherwise a specific error code is returned.
*/
/*===========================================================================*/
sns_ddf_status_e sns_dd_tmd2725_get_attrib(
	sns_ddf_handle_t         handle,
	sns_ddf_sensor_e         sensor,
	sns_ddf_attribute_e      attrib,
	sns_ddf_memhandler_s*    memhandler,
	void**                   value,
	uint32_t*                num_elems)
{ 
	sns_dd_tmd2725_state_t* state = (sns_dd_tmd2725_state_t*)handle;
	sns_ddf_status_e status = SNS_DDF_SUCCESS;
	bool generic_attrib = false;

	sns_ddf_registry_group_s *reg_group;
	uint8_t *reg_group_data;

	sns_ddf_driver_info_s *driver_info_ptr;
	sns_ddf_driver_info_s  driver_info = {
		"TMD2725_PROX_ALS", /* name */
		1                   /* version */
	};

	sns_ddf_device_info_s *device_info_ptr;
	sns_ddf_device_info_s  device_info = {
		"PROX_ALS",         /* name */
		"ams AG",           /* vendor */
		"TMD2725",          /* model */
		1                   /* version */
	};

	if (handle == NULL || memhandler == NULL)
	{
		return SNS_DDF_EINVALID_PARAM;
	}

	TMD2725_DD_MSG_2(HIGH, "tmd2725 get_attrib():sensor=%d, attrib=%d\n", sensor, attrib);

	/* Check first if the query is for generic attributes */
	generic_attrib = true;
	switch (attrib)
	{
	case SNS_DDF_ATTRIB_DRIVER_INFO:
		if ((driver_info_ptr = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(sns_ddf_driver_info_s),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		*driver_info_ptr = driver_info;
		*(sns_ddf_driver_info_s**)value = driver_info_ptr;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_DEVICE_INFO:
		if ((device_info_ptr = sns_ddf_memhandler_malloc_ex(memhandler, sizeof(sns_ddf_device_info_s),
							state->smgr_handle)) == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		*device_info_ptr = device_info;
		*(sns_ddf_device_info_s**)value = device_info_ptr;
		*num_elems = 1;
		break;

	case SNS_DDF_ATTRIB_REGISTRY_GROUP:
		reg_group = sns_ddf_memhandler_malloc(memhandler,sizeof(*reg_group));
		if (reg_group == NULL)
		{
			return SNS_DDF_ENOMEM;
		}

		if (state->nv_db_size == 0)
		{
			reg_group->group_data = NULL;
			reg_group->size = 0;
		}
		else
		{
			reg_group_data = sns_ddf_memhandler_malloc(memhandler, state->nv_db_size);
			if (reg_group_data == NULL)
			{
				return SNS_DDF_ENOMEM;
			}

			memcpy(reg_group_data, &state->nv_db, sizeof(tmd2725_nv_db_s));
			reg_group->group_data = reg_group_data;
			reg_group->size = state->nv_db_size;
		}

		*value = reg_group;
		*num_elems = 1;
		break;

	default:
		generic_attrib = false;
		break;
	}

	/* The query is not for generic attribute but is for specific data type */
	if (generic_attrib != true)
	{
		switch (sensor)
		{
		case SNS_DDF_SENSOR_PROXIMITY:
			status = sns_dd_tmd2725_prox_query(state, memhandler, attrib, value, num_elems);
			break;

		case SNS_DDF_SENSOR_AMBIENT:
			status = sns_dd_tmd2725_als_query(state, memhandler, attrib, value, num_elems);
			break;

		default:
			TMD2725_DD_MSG_1(HIGH, "tmd2725_get_attrib():sensor type %d not support\n", sensor);
			status = SNS_DDF_EINVALID_ATTR;
		}
	}

	return status;
}

#define NUM_OF_LUX_TO_AVE  8
static sns_ddf_status_e tmd2725_hub_get_avg_lux(sns_dd_tmd2725_state_t* state)
{
	uint32_t luxSum;
	uint8_t i;

	luxSum = 0;

	for (i = 0; i < NUM_OF_LUX_TO_AVE;)
	{
		/* Wait ALS integration time before reading */
		sns_ddf_delay(TMD2725_ATIME_PER_100 * 10 * (state->chip.params.als_time + 2));

		tmd2725_hub_read_als(state);
		tmd2725_hub_get_lux(state);

		if (ready_for_report)
		{
			luxSum += state->chip.als_inf.lux;
			i++;
		}
	}

	state->chip.als_inf.avg_lux = (luxSum / i);

	return SNS_DDF_SUCCESS;
}

/*===========================================================================*/
/*
* FUNCTION      tmd2725_hub_calibrate_als
*
* DESCRIPTION   Called from self-test. Compare avg lux received to avg lux
*               expected and calculates scaling factor.
*
* DEPENDENCIES  None
*
* RETURN VALUE  Status
*
* SIDE EFFECT   None
*/
/*===========================================================================*/
sns_ddf_status_e tmd2725_hub_calibrate_als(sns_dd_tmd2725_state_t* state)
{
	sns_ddf_status_e status;
	uint32_t tgtLux, avgLux, tgtLuxLowRange, tgtLuxHiRange;
	avgLux = 1;
	tgtLux = 1;

	/* Compare the average lux to the passed in calibration lux */
	status = tmd2725_hub_get_avg_lux(state);
	if (status != SNS_DDF_SUCCESS)
	{
		return status;
	}

	/* Find Lux mid way between calLuxLower and calLuxUpper */
	tgtLux = (CALIB_LUX_LOW + CALIB_LUX_HIGH) / 2;
	tgtLuxLowRange = tgtLux >> 1;
	tgtLuxHiRange = tgtLux + (tgtLux >> 1);
	avgLux = state->chip.als_inf.avg_lux;

	/* Check if avg lux is outside expected range of target lux. If so, return efail */
	if (avgLux <= tgtLuxLowRange || avgLux >= tgtLuxHiRange)
	{
		return SNS_DDF_EFAIL;
	}

	state->nv_db.als_factor = (state->nv_db.als_factor * tgtLux) / avgLux;
	state->chip.als_inf.lux_scale_1000 = state->nv_db.als_factor;
	return status;
}

/*===========================================================================*/
/*
* FUNCTION      sns_dd_tmd2725_run_test
*
* DESCRIPTION   OEM Self Test to calibrate ALS Lux. Takes several lux readings
*               and calculates scaling factor for ALS reading
*
* DEPENDENCIES  None
*
* RETURN VALUE  Status
*
* SIDE EFFECT   None
*/
/*===========================================================================*/
sns_ddf_status_e sns_dd_tmd2725_run_test(
	sns_ddf_handle_t         handle,
	sns_ddf_sensor_e         sensor,
	sns_ddf_test_e           test,
	uint32_t*                err)
{
	sns_dd_tmd2725_state_t* state = (sns_dd_tmd2725_state_t*)handle;
	sns_ddf_status_e status = SNS_DDF_SUCCESS;
	sns_ddf_handle_t smgr_handle;
	uint8_t data = 0;
	uint8_t offset_l = 0;

	if (sensor == SNS_DDF_SENSOR_AMBIENT)
	{
		/* ALS run test */
		if (test == SNS_DDF_TEST_OEM || test == SNS_DDF_TEST_SELF)
		{
			smgr_handle = state->smgr_handle;

			status = sns_dd_tmd2725_reset(state);
			if (status != SNS_DDF_SUCCESS)
			{
				sns_ddf_smgr_notify_test_complete(smgr_handle, SNS_DDF_SENSOR_AMBIENT, SNS_DDF_EFAIL, SNS_DD_TMD2725_CAL_FAILED_RST);
				return status;
			}

			/* Enable sensor */
			tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, PON);
			sns_ddf_delay(DELAY_MS_AFTER_PON * 1000);
			tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, (PON|AEN));
			/* update als scaling */
			status = tmd2725_hub_calibrate_als(state);
			tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, 0);
			if (status != SNS_DDF_SUCCESS)
			{
				sns_ddf_smgr_notify_test_complete(smgr_handle, SNS_DDF_SENSOR_AMBIENT, SNS_DDF_EFAIL, SNS_DD_TMD2725_CAL_FAILED_LUX);
				return status;
			}
			status = sns_ddf_smgr_notify_event(smgr_handle, SNS_DDF_SENSOR_AMBIENT, SNS_DDF_EVENT_UPDATE_REGISTRY_GROUP);
			if (status != SNS_DDF_SUCCESS)
			{
				sns_ddf_smgr_notify_test_complete(smgr_handle, SNS_DDF_SENSOR_AMBIENT, SNS_DDF_EFAIL, SNS_DD_TMD2725_CAL_FAILED_REG);
				return status;
			}
		}
		else
		{
			return SNS_DDF_EINVALID_TEST;
		}
	}
	else if (sensor == SNS_DDF_SENSOR_PROXIMITY)
	{
		/* Proximity run test */
		if (test == SNS_DDF_TEST_OEM || test == SNS_DDF_TEST_SELF)
		{
			smgr_handle = state->smgr_handle;

			status = sns_dd_tmd2725_reset(state);
			if (status != SNS_DDF_SUCCESS )
			{
				sns_ddf_smgr_notify_test_complete(smgr_handle, SNS_DDF_SENSOR_PROXIMITY, SNS_DDF_EFAIL, SNS_DD_TMD2725_CAL_FAILED_RST);
				return status;
			}

			/* Enable sensor */
			tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, PON);
			sns_ddf_delay(DELAY_MS_AFTER_PON * 1000);
			tmd2725_hub_i2c_modify(state, TMD2725_PERS_REG, PPERS, PROX_PERSIST(0));
			tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, (PINT|CINT), (PINT|CINT));
			tmd2725_hub_i2c_write_byte(state, TMD2725_CALIBCFG_REG, (BINSRCH_TARGET_0|0x09));
			tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, (PIEN|CIEN), CIEN);
			tmd2725_hub_i2c_write_byte(state, TMD2725_CALIB_REG, START_OFFSET_CALIB);
			sns_ddf_delay(100*1000);
			tmd2725_hub_i2c_write_byte(state, TMD2725_ENABLE_REG, 0);
			tmd2725_hub_i2c_read_byte(state, TMD2725_STATUS_REG, &data);
			if (data & CINT)
			{
				tmd2725_hub_i2c_modify(state, TMD2725_STATUS_REG, CINT, CINT);
				tmd2725_hub_i2c_read_byte(state, TMD2725_POFFSETL_REG, &offset_l);
				if (offset_l == 0xFF)
				{
					sns_ddf_smgr_notify_test_complete(smgr_handle, SNS_DDF_SENSOR_PROXIMITY, SNS_DDF_EFAIL, SNS_DD_TMD2725_CAL_FAILED_PROX);
					return SNS_DDF_EFAIL;
				}
			}
			else
			{
				sns_ddf_smgr_notify_test_complete(smgr_handle, SNS_DDF_SENSOR_PROXIMITY, SNS_DDF_EFAIL, SNS_DD_TMD2725_CAL_FAILED_PROX);
				return SNS_DDF_EFAIL;
			}
		}
		else
		{
			return SNS_DDF_EINVALID_TEST;
		}
	}
	else
	{
		return SNS_DDF_EINVALID_TEST;
	}

	return SNS_DDF_SUCCESS;
}

/*===========================================================================*/
/*
* @brief Begins device-scheduled sampling and enables notification via Data 
*        Ready Interrupts (DRI).
*
* The driver commands the device to begin sampling at the configured 
* ODR (@a SNS_DDF_ATTRIB_ODR) and enables DRI. When data is ready, the 
* driver's handle_irq() function is called and the driver notifies 
* SMGR of the event via @a sns_ddf_smgr_notify_event() and @a 
* SNS_DDF_EVENT_DATAREADY. 
*  
* @param[in] handle  Handle to the driver's instance.
* @param[in] sensor  Sensor to be sampled.
* @param[in] enable  True to enable or false to disable data stream.
* 
* @return SNS_DDF_SUCCESS if sensor was successfully configured and 
*         internal sampling has commenced or ceased. Otherwise an
*         appropriate error code.
*/
/*===========================================================================*/
sns_ddf_status_e sns_dd_tmd2725_enable_sched_data(
	sns_ddf_handle_t         handle,
	sns_ddf_sensor_e         sensor,
	bool                     enable)
{
	sns_dd_tmd2725_state_t* state = (sns_dd_tmd2725_state_t*)handle;
	sns_ddf_status_e status = SNS_DDF_SUCCESS;

	TMD2725_DD_MSG_0(HIGH, "tmd2725 enable_sched_data()\n");
	if (state == NULL)
	{
		return SNS_DDF_EINVALID_PARAM;
	}

	/* Check if the platform supports interrupts. */
	if (!sns_ddf_signal_irq_enabled() || state->gpio1 == 0xFFFF)
	{
		return SNS_DDF_EFAIL;
	}

	if (sensor == SNS_DDF_SENSOR_PROXIMITY)
	{
		if (enable)
		{
			/* Enable interrupt on proximity sensor */
			tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, PIEN, PIEN);
		}
		else
		{
			/* Disable interrupt on proximity sensor */
			tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, PIEN, 0);
		}
	}
	else if (sensor == SNS_DDF_SENSOR_AMBIENT)
	{
#ifdef TMD2725_ALS_POLLING_IN_DRI
		/* Start/cancel timer for ALS polling in DRI mode */
		if (enable)
		{
			als_timer_stop = false;	
			status = sns_ddf_timer_start(state->polling_timer, 1000*100);
			if (status != SNS_DDF_SUCCESS)
			{
				return status;
			}
		}
		else
		{
			als_timer_stop = true;
			status = sns_ddf_timer_cancel(state->polling_timer);
			if (status != SNS_DDF_SUCCESS)
			{
				return status;
			}
		}
#else
		if (enable)
		{
			/* Enable interrupt on ALS sensor */
			tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, AIEN, AIEN);
		}
		else
		{
			/* Disable interrupt on ALS sensor */
			tmd2725_hub_i2c_modify(state, TMD2725_INTENAB_REG, AIEN, 0);
		}
#endif
	}

	if (enable && ((state->chip.prox_enabled && !state->chip.als_enabled)
		|| (!state->chip.prox_enabled && state->chip.als_enabled)))
	{
		status = sns_ddf_signal_register(
							state->gpio1,
							state,
							&sns_tmd2725_driver_fn_list,
							SNS_DDF_SIGNAL_IRQ_FALLING);
		state->dri_enabled = true;
	}
	else if (!state->chip.prox_enabled && !state->chip.als_enabled)
	{
		if (state->dri_enabled)
		{
			status = sns_ddf_signal_deregister(state->gpio1);
		}
		state->dri_enabled = false;
	}

	return status;
}
	
/*===========================================================================*/
/*
* @brief Probes for the device with a given configuration
*
* @param[in]  dev_info    Access info for physicol devices controlled by 
*                         this driver. Used to determine if the device is
*                         physically present.
* @param[in]  memhandler  Memory handler used to dynamically allocate 
*                         output parameters, if applicable.
* @param[out] num_sensors Number of sensors supported. 0 if none.
* @param[out] sensor_type Array of sensor types supported, with num_sensor
*                         elements. Allocated by this function.
*
* @return SNS_DDF_SUCCESS if the part was probed function completed, even
*         if no device was found (in which case num_sensors will be set to
*         0).
*/
/*===========================================================================*/
sns_ddf_status_e sns_dd_tmd2725_probe(
	sns_ddf_device_access_s* device_info,
	sns_ddf_memhandler_s*    memhandler,
	uint32_t*                num_sensors,
	sns_ddf_sensor_e**       sensors)
{
	sns_ddf_status_e status = SNS_DDF_SUCCESS;
	sns_ddf_handle_t port_handle;
	uint8_t id, rev;
	uint8_t read_count;
	uint8_t i;

	/* Basic sanity check*/
	if (device_info == NULL || sensors == NULL || num_sensors == NULL)
	{
		return SNS_DDF_EINVALID_PARAM;
	}
	*num_sensors = 0;
	*sensors = NULL;

	status = sns_ddf_open_port(&port_handle, &(device_info->port_config));
	if (status != SNS_DDF_SUCCESS) 
	{
		return status;
	}

	/* Read & Verify Device ID */
	status = sns_ddf_read_port(port_handle,
								TMD2725_ID_REG,
								&id,
								1,
								&read_count);
	if (status != SNS_DDF_SUCCESS)
	{
		sns_ddf_close_port(port_handle);
		return status;
	}
	TMD2725_DD_MSG_1(HIGH, "tmd2725 Get chip id %x\n", id);

	status = sns_ddf_read_port(port_handle,
								TMD2725_REVID_REG,
								&rev,
								1,
								&read_count);
	if (status != SNS_DDF_SUCCESS)  
	{
		sns_ddf_close_port(port_handle);
		return status;
	}
	TMD2725_DD_MSG_1(HIGH, "tmd2725 Get chip rev %x\n", rev);

	/* Verify Device ID */
	for (i = 0; i < sizeof(tmd2725_ids); i++)
	{
		if ((id&0xFC) == tmd2725_ids[i])
		{
			status = SNS_DDF_SUCCESS;
			break;
		}
	}
	if (i >= sizeof(tmd2725_ids))
	{
		status = SNS_DDF_EDEVICE;
		TMD2725_DD_MSG_0(ERROR,"tmd2725 probe() not supported chip id\n");
		sns_ddf_close_port(port_handle);
		return status;
	}

	/* ID register is as expected */
	*num_sensors = 2;
	*sensors = sns_ddf_memhandler_malloc(memhandler,
						sizeof(sns_ddf_sensor_e) * *num_sensors);
	if (*sensors != NULL)
	{
		(*sensors)[0] = SNS_DDF_SENSOR_PROXIMITY; /* Proximity */
		(*sensors)[1] = SNS_DDF_SENSOR_AMBIENT; /* ALS */

		status = SNS_DDF_SUCCESS;
	}
	else
	{
		status = SNS_DDF_ENOMEM;
	}

	sns_ddf_close_port(port_handle);
	return status;
}
