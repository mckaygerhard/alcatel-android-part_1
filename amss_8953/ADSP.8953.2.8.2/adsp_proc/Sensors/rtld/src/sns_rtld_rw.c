/*==============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include "sns_memmgr.h"

#include "sns_rtld_rw.h"

#ifndef FARF_ERROR
#define FARF_ERROR 1
#endif
#include "HAP_farf.h"

int sns_rtld_rw_malloc(size_t length, void** rw, void** rw2)
{
  void* va = 0;

  va = SNS_OS_U_MALLOC(0, length);
  if (va){
    *rw = va;
    *rw2 = va;
    return 0;
  }
  return 1;
}

void sns_rtld_rw_free(void* va)
{
  SNS_OS_U_FREE(va);
}

int sns_rtld_rw_mprotect(void* va, size_t length, int prot, int ctx)
{
  // only RW allowed in this heap and pages already RW
  if (prot & PROT_EXEC) {
    // bail is execute privs requested
    FARF(ERROR, "Cannot mark RW heap as X");
    return -1;
  }
  return 0;
}

