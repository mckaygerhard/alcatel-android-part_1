#ifndef VOC_CAPI_LIB_H
#define VOC_CAPI_LIB_H
/*========================================================================

 *//** @file CVocoderCapiLib.h

     Copyright (c) 2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
     QUALCOMM Proprietary.  Export of this technology or software is regulated
     by the U.S. Government, Diversion contrary to U.S. law prohibited.
    *//*====================================================================== */

/*========================================================================
  Edit History

  $Header: //components/rel/avs.adsp/2.7/voice/algos/vocoder_capi/inc/CVocoderCapiLib.h#4 $


  when       who     what, where, why
  --------   ---     -------------------------------------------------------
  06/30/10    SJ      Created file.

  ========================================================================== */


/* =======================================================================
   INCLUDE FILES FOR MODULE
   ========================================================================== */

#ifdef __qdsp6__
#include "qurt_elite.h"
#else
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#endif
#include "Elite_CAPI.h"
#include "fourgv_dec_api.h"
#include "fourgv_enc_api.h"
#include "adsp_error_codes.h"
#include "adsp_media_fmt.h"
#include "stringl.h"
extern "C" {
#include "eamr_dec_api.h"
#include "eamr_enc_api.h"
#include "amrwb_dec_api.h"
#include "amrwb_enc_api.h"
#include "evrc_dec_api.h"
#include "evrc_enc_api.h"
#include "v13k_dec_api.h"
#include "v13k_enc_api.h"
#include "g711_api.h"
#include "fr_dec_api.h"
#include "fr_enc_api.h"
#include "tty_enc_api.h"
#include "tty_dec_api.h"
}

// using memscpy and memsmove instead memcpy and memmove
// d: dest  s: src  n : dest size and src size
#define capi_memscpy(d,s,n)   memscpy(d,n,s,n)
#define capi_memsmove(d,s,n)  memsmove(d,n,s,n)
//todo: consider removing the #define below
#ifndef __qdsp6__
#define MSG(a, b, c) \
   printf(c); \
printf("\n");

#define MSG_2(a, b, c,...) \
   printf(c,##__VA_ARGS__); \
printf("\n");

#define MSG_3(a, b, c,...) \
   printf(c,##__VA_ARGS__); \
printf("\n");
#endif


#define VOCODER_MAX_PACKET_SIZE 32 //12 for 4gv, 18 for AMR NB, 32 for AMR WB
// Number of Samples per Frame
#define VOC_NB_FRAME_SIZE_IN_BYTES 320
#define VOC_WB_FRAME_SIZE_IN_BYTES 640
#define VOC_INP_BUF_SIZE 2048
#define VOC_INP_BUF_SIZE_LINEAR_PCM 960
//#define VOC_INP_BUF_SIZE_LINEAR_PCM 4096
#define VOC_NB_OUT_BUF_SIZE 160*2
#define VOC_WB_OUT_BUF_SIZE 320*2

//GSM-FR constants
#define GSMFR_MAX_SRC_FRAME_LENGTH         33
#define GSMFR_TRANSLATED_FRAME_LENGTH      36
#define GSMFR_MAX_FRAMES_IN_TRANSBUFFER 30

#define GSMFR_DSP_HEADERLEN_IN_BYTES       2
#define GSMFR_MAX_SRC_FRAME_LENGTH_ODD    GSMFR_MAX_SRC_FRAME_LENGTH
#define GSMFR_MAX_SRC_FRAME_LENGTH_EVEN   (GSMFR_MAX_SRC_FRAME_LENGTH -1)
#define GSMFR_NIBBLE_SHIFT_BITS           4
#define GSM_FR_LSB_TO_MSB_UNPACK_PARAMS   1

enum VocParamIdx     ///< Vocoder param enum
{
   VOC_MAX_RATE = eIcapiMaxParamIndex,     ///< to set/get min_rate for 1x vocoders
   VOC_MIN_RATE,     ///< to set/get max_rate for 1x vocoders
   VOC_FRAME_SIZE,   ///< to set/get frame size
   VOC_DTX_ENABLE,   ///< to set/get DTX enable flag
   VOC_DTX_MODE,
   VOC_OP_MODE,      ///<rate or operating mode command
   VOC_RATE_CMD,        ///< rate modulation command
   VOC_PARAM_ID_MAX = 0x7fffffff ///< this is to overcome compiler depedent behaviour of enum size
};

//Structure to be used in INIT Params
typedef struct VoiceInitParamsStruct
{
   uint32 VocTypeID;        //codec ID
   uint32 VocConfigBlkSize; //size of structure for error check
   void* VocConfigBlk;      //Encoder COnfiguratoin block or Decoder format block

}VoiceInitParamsType;

typedef struct
{
   uint8 stored_nibble;     // stored nibble value
   uint8 odd_or_even_frame; // 0 : odd frame read 33 bytes for decoder wrapper
                            // 1 : even frame read 32 bytes for decoder wrapper
}gsm_fr_t;

typedef struct VoiceDecoderCapiType VoiceDecoderCapiType;

///< structure for Decoder CAPI
struct VoiceDecoderCapiType
{

   ICAPIVtbl *pVtbl;             ///< function pointer table
   uint8 fErasure;             ///< to save erasure status
   uint16 nFrameSize;          ///< max output size for decoder
   uint16  nInpPktSize;        ///< input packet size
   FourGVTW FgvTWStruct;                ///< FourGV timewarping structure
   uint32  nVocType;                     ///< Vocoder selection
   int16  aDecodeInputBuf[VOCODER_MAX_PACKET_SIZE];  ///<need to keep memory of the last
   ///<input packet so made it a
   //class member
   gsm_fr_t  gsm_fr;
   uint32  nCompanding;               ///< companding type for G711 FS only.  G711_A2 companding type in header
   uint16  nG711Mode;               ///< based on companding type for G711 FS only.
   int16  nPrevRate;                ///< used in AMR NB and WB erasure handling
   void*  pDecoder;                 ///< pointer for decoder
   uint32  nSamplingRate;           ///<sampling rate
   CAPI_ChannelMap_t out_chan_map;
   //some function pointer to reduce if/else for different vocoder types
   int (*VocProcess)(ICAPIVtbl* _pif, const CAPI_BufList_t* input,
         CAPI_BufList_t* output, CAPI_Buf_t* params);
   int (*VocReInit)(VoiceDecoderCapiType *pMe, void* pConfigBlk);
};

typedef struct VoiceEncoderCapiType VoiceEncoderCapiType;

struct VoiceEncoderCapiType
{

   ICAPIVtbl *pVtbl;               ///< function pointer table

   int16  fDTX;                   ///< DTX flag
   int16  nOpMode;                  ///< 4GV COP/operating mode for AMR,
   ///< reduced rate level for others
   int16  nMinRate;               ///< min rate setting
   int16  nMaxRate;               ///< max rate setting
   uint32  nVocType;               ///< Vocoder selection
   uint16 nFrameSize;                     ///< expected input size for encoder
   uint16  nMaxPktSize;                    ///<Max output size of encoder
   uint16  nOutPktSize;                    ///<Current output size of encoder
   int16  nDtxMode;                      ///< DTX mode
   int16  nVADType;                      ///< VAD type
   int16  nRateModCmd;           ///< rate modulation cmd
   int16  nBneState;                      ///< BNE state
   int16  nChannels;                      ///< number of channels
   void   *pEncoder;
   gsm_fr_t  gsm_fr;
   uint32  nSamplingRate;           ///<sampling rate
   CAPI_ChannelMap_t channelMap;    ///< channel mapping structure
   uint16_t nBitsPerSample;
   uint32  nCompanding;               ///< companding type for G711 FS only.  G711_A2 companding type in header
   //some function pointer to reduce if/else for different vocoder types
   //int (*VocSetParam)(ICAPIVtbl* _pif, int nParamIdx, int nParamVal);
   int (*VocProcess)(ICAPIVtbl* _pif, const CAPI_BufList_t* input,
         CAPI_BufList_t* output, CAPI_Buf_t* params);
   int (*VocInit)(ICAPIVtbl *_pif, VoiceEncoderCapiType *pMe, void* pConfigBlk);
   int (*VocReInit)(VoiceEncoderCapiType *pMe, void* pConfigBlk);

};

extern const ICAPIVtbl CVoiceComboEncoderLibVtbl;
extern const ICAPIVtbl CVoiceComboDecoderLibVtbl;

ADSPResult Voc_Create_Decoder_CAPI(ICAPI **Decoder);
ADSPResult Voc_Create_Encoder_CAPI(ICAPI **Encoder);

/**************************************************************************/
//vocoder specific configuration structures --TODO:: Have a discussion and
//freeze these structures
/**************************************************************************/
/** \brief AMR NB/WB encoder configuration structure */
struct Voice_AmrEncodeConfigType
{
   /** \brief Encoding Mode */

   uint16          ulEncodeMode;

   /** \brief DTX Mode */

   uint16          ulDtxMode;

};

struct Voice_G711EncodeConfigType
{
   /** \brief Sampling rate: 8000Hz o6 16000Hz */
      uint32_t            ulSampleRate;
};
struct Voice_G711FormatBlockType
{
   /** \brief Sampling rate: 8000Hz o6 16000Hz */
      uint32_t            ulSampleRate;
};

/** \brief V13k encoder configuration structure */
struct Voice_V13kEncodeConfigType
{
   /** \brief Max Rate */
      /**
        Use 1 to 4 values
        to set this member
       */
      uint16_t          usMaxRate;

      /** \brief Min Rate */
      /**
        Use 1 to 4 values
        to set this member
       */
      uint16_t          usMinRate;

   /** \brief Reduced Rate  command */
      /**
        Use 0 to 4 values
        to set this member
        0 = 14.4 kbps (default)
        1 = 12.2 kbps
        2 = 11.2 kbps
        3 = 9.0 kbps
        4 = 7.2 kbps
       */
      uint16_t          usReducedRateCmd;

      /** \brief Rate Modulation command */
      uint16_t          usRateModCmd;

};

/** \brief EVRC encoder configuration structure */
struct Voice_EvrcEncodeConfigType
{
   /** \brief Max Rate */
      /**
        Use 1 to 4 values
        to set this member
       */
      uint16_t          usMaxRate;

      /** \brief Min Rate */
      /**
        Use 1 to 4 values
        to set this member
       */
      uint16_t          usMinRate;

   /** \brief Rate Modulation command */
      uint16_t          usRateModCmd;

      /** \brief Reserved */

      uint16_t          usReserved;

};


/** \brief EVRC B and WB encoder configuration structure */
struct Voice_FourGVEncodeConfigType
{
   /** \brief Max Rate */
      /**
        Use 1 to 4 values
        to set this member
       */
      uint16_t          usMaxRate;

      /** \brief Min Rate */
      /**
        Use 1 to 4 values
        to set this member
       */
      uint16_t          usMinRate;

      /** \brief DTX Enable */

      uint16_t          usDtxEnable;

      /** \brief  reduced rate level */
      /* supported values:
         0 = 9.3 kbps (default)
         1 = 8.5 kbps
         2 = 7.5 kbps
         3 = 7.0 kbps
         4 = 6.6 kbps
         5 = 6.2 kbps
         6 = 5.8 kbps
         7 = 4.8 kbps
       */

      uint16_t          usReducedRateLevel;

};

#endif /* #ifndef Voc_CAPI_LIB_H */
