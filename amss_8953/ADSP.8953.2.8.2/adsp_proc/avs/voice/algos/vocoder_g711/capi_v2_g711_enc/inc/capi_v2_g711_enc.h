/* ======================================================================== */
/**
   @file capi_v2_g711_enc.h
   @brief CAPI_V2 wrapper for g711 encoder
*/

/* =========================================================================
   Copyright (c) 2012-2015 QUALCOMM Technologies Incorporated.
   All rights reserved. Qualcomm Technologies Proprietary and Confidential.
   ========================================================================== */

/* =========================================================================
   Edit History

   when         who        what, where, why
   --------     ---        --------------------------------------------------
   11/17/15   adeepak      Initial creation
   ========================================================================= */

#ifndef CAPI_V2_G711_ENC_H_
#define CAPI_V2_G711_ENC_H_

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "Elite_CAPI_V2.h"

#if defined(__cplusplus)
extern "C" {
#endif // __cplusplus

/*----------------------------------------------------------------------------
 * Function Declarations
 * -------------------------------------------------------------------------*/

/* ----- Entry functions for VSM_MEDIA_TYPE_G711_ALAW ----- */
capi_v2_err_t capi_v2_g711_alaw_enc_get_static_properties (
   capi_v2_proplist_t *init_set_properties,
   capi_v2_proplist_t *static_properties);


capi_v2_err_t capi_v2_g711_alaw_enc_init (
   capi_v2_t*              _pif,
   capi_v2_proplist_t      *init_set_properties);



/* ----- Entry functions for VSM_MEDIA_TYPE_G711_MLAW ----- */
capi_v2_err_t capi_v2_g711_mlaw_enc_get_static_properties (
   capi_v2_proplist_t *init_set_properties,
   capi_v2_proplist_t *static_properties);


capi_v2_err_t capi_v2_g711_mlaw_enc_init (
   capi_v2_t*              _pif,
   capi_v2_proplist_t      *init_set_properties);


/* ----- Entry functions for VSM_MEDIA_TYPE_G711_ALAW_V2 ----- */
capi_v2_err_t capi_v2_g711_alaw_v2_enc_get_static_properties (
   capi_v2_proplist_t *init_set_properties,
   capi_v2_proplist_t *static_properties);


capi_v2_err_t capi_v2_g711_alaw_v2_enc_init (
   capi_v2_t*              _pif,
   capi_v2_proplist_t      *init_set_properties);


/* ----- Entry functions for VSM_MEDIA_TYPE_G711_MLAW_V2 ----- */
capi_v2_err_t capi_v2_g711_mlaw_v2_enc_get_static_properties (
   capi_v2_proplist_t *init_set_properties,
   capi_v2_proplist_t *static_properties);


capi_v2_err_t capi_v2_g711_mlaw_v2_enc_init (
   capi_v2_t*              _pif,
   capi_v2_proplist_t      *init_set_properties);




#if defined(__cplusplus)
}
#endif // __cplusplus

#endif /* CAPI_V2_G711_ENC_H_ */
