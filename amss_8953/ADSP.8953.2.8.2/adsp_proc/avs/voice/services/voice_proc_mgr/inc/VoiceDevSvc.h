
/*========================================================================
Elite

This file contains an example service belonging to the Elite framework.

Copyright (c) 2009 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/voice/services/voice_proc_mgr/inc/VoiceDevSvc.h#5 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
9/27/09    vn       Created file.
4/08/10    ka       Refactoring of structures

========================================================================== */
#ifndef VOICEDEVSVC_H
#define VOICEDEVSVC_H

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */

#include "qurt_elite.h"
#include "Elite.h"
#include "VoiceMsgs.h"
#include "AFEDevService.h"
#include "apr_api.h"
#include "VoiceMixerSvc.h"
#include "VoiceCmnUtils.h"
#include "mmpm.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/* -----------------------------------------------------------------------
** Global definitions/forward declarations
** ----------------------------------------------------------------------- */

typedef enum
{
   VOICEPROC_CONNECT_DWN_STREAM_CMD = 0,
   /**< Connect to downstream peer */
   VOICEPROC_DISCONNECT_DWN_STREAM_CMD,
   /**< Disconnect from downstream peer */
   VOICEPROC_RECONFIG_CMD,
   /**< Reconfigure to a new topology/sampling rate/device */
   VOICEPROC_RUN_CMD,
   /**< Move to run state and start processing data */
   VOICEPROC_STOP_CMD,
   /**< Move to stop state and stop processing data */
   VOICEPROC_SET_PARAM_CMD,
   /**< Set a parameter */
   VOICEPROC_GET_PARAM_CMD,
   /**< Get a parameter */
   VOICEPROC_DESTROY_YOURSELF_CMD,
   /**< Destroy the service instance */
   VOICEPROC_CONNECT_RX_PEER,
   /**< Connect to rx path peer.

     Required for Automatic Volume Control/Receive voice enhancement algorithms
     to get feedback of rx path gain
     */
   VOICEPROC_DISCONNECT_RX_PEER,
   /**< Disconnect from rx path peer */
   VOICEPROC_SET_MUTE_CMD,
   /**< Control mute/unmute */
   VOICEPROC_CONFIG_HOST_PCM,
   /**< Configure Host PCM */
   VOICEPROC_SET_TIMING_CMD,
   /**< Set timing parameters */
   VOICEPROC_SET_TIMINGV2_CMD,
   /**< Set timing parameters with VPM specific API */
   VOICEPROC_GET_KPPS_CMD,
   /**< Command to get  KPPS */
   VOICEPROC_SET_TIMINGV3_CMD,
   /**< Set timing parameters with VPM specific API to support VSID */   
   VOICEPROC_GET_DELAY_CMD,
   /**< Command to get delay */
   VOICEPROC_SET_PARAM_V3_CMD,
   /**< Command to set compressed param */
   VOICEPROC_REGISTER_EVENT,
   /**< Command to register event */
   VOICEPROC_UNREGISTER_EVENT,
   /**< Command to unregister event */
   VOICEPROC_NUM_MSGS
   /**< Number of supported message */
} voiceproc_custom_msgs_t;

typedef enum
{
   VOICEDEV_SET_SHARED_OBJECTS = 0,
   /**< Sets the shared object handles */
   VOICEDEV_NUM_MSGS
   /**< Number of supported message */
} voicedev_custom_msgs_t;


typedef struct VoiceProcMgr_t VoiceProcMgr_t;
typedef struct voice_proc_session_t voice_proc_session_t;
typedef struct voice_proc_apr_info_t  voice_proc_apr_info_t;
/** @brief Structure to save the port/rate/topology configuration associated with a session */
typedef struct
{
   uint16_t tx_port;
   /**< tx port configuration of the session */
   uint32_t tx_topology_id;
   /**< tx topology configuration of the session */
   uint16_t tx_sampling_rate;
   /**< tx sampling rate configuration of the session */
   uint16_t rx_port;
   /**< rx port configuration of the session */
   uint32_t rx_topology_id;
   /**< rx topology configuration of the session */
   uint16_t rx_sampling_rate;
   /**< rx sampling rate configuration of the session */
   uint32_t ec_mode;
   /**< it tells whether ec is in async mode or sync mode */
   uint16_t far_port;
   /**< audio reference port in a case where audio is external */

} vpm_session_config_t;

typedef struct
{
   uint32_t                voice_activity_event_registered;
   /**< Indicates whether the client has registered for voice activity event on this session*/
}vpm_event_reg_info_t;
/** Enumeration to indicate the total number of simultaneous VPM sessions supported */
#define VOICE_MAX_VP_SESSIONS 8
/** Enumeration to indicate a particular VPM session is in the run state */
const uint32_t VPM_RUN_STATE=0xABCD3333;
/** Enumeration to indicate a particular VPM session is in the stop state */
const uint32_t VPM_STOP_STATE=0xABCD4444;

/** @brief Structure to define the state of a particular session.

    Tx or rx pointers may be NULL if this is not a paired session.  For example, in
    a tx only session, rx will be NULL.  One of rx or tx must be non-null.
*/
struct voice_proc_session_t
{
   VoiceProcMgr_t       *vpm_ptr;
   /**< owning VPM of this session */
   elite_svc_handle_t     *tx;
   /**< tx voice proc handle of this session, may be NULL if tx not active */
   elite_svc_handle_t     *rx;
   /**< tx far handle for Rx AFE service*/
   elite_svc_handle_t     *tx_far;
   /**< rx voice proc handle of this session, may be NULL if tx not active */
   vmx_inport_handle_t    *mixer_in_port;
   /**< Input port of mixer for tx voice proc */
   vmx_outport_handle_t   *mixer_out_port;
   /**< Output port of mixer for rx voice proc */
   async_struct_t          async_struct;
   uint16_t tx_num_channels;
   /**< Number of Mics. Number of VPTx near input channels */
   uint16_t rx_num_channels;
   /**< Number of speaker channels. Number of VPRx channels input/output  */
   /**< structure to manage asynchronous responses coming back from dynamic services.

        This structure will collect responses from the dynamic services, and match them
        to the original APR command, so that the APR response is sent when all the appropriate
        messages have been collected, and an appropriate response result has been generated
   */
   uint32_t               input_mapping_mask;
   /**< indicates mapping from the voice proc tx modules to encoders */
   uint32_t               output_mapping_mask;
   /**< indicates mapping from decoders to the voice proc rx modules */
   elite_svc_handle_t     *tx_afe_handle;
   /**< saved handle to Tx Near input AFE service (returned from AFE connect) */
   elite_svc_handle_t     *far_afe_handle;
   /**< saved handle to Tx Far input AFE service (returned from AFE connect) */
   elite_svc_handle_t     *rx_afe_handle;
   /**< structure used to pass in queue information to Tx Far AFE, so that far data is sent to desired Vptx gpQ */
   int16_t                my_handle;
   /**< saved session handle that was given to Voice SW for this session */
   uint32_t                state;
   /**< state of session, either run or stop state */
   vpm_session_config_t   session_config;
   /**< used to save the current port/sampling rate/topology configuration.  Used to cache values when AFE connect/disconnect done during run/stop */
   uint16_t               host_pcm_num_active_tap_points;     
   /**< used to track number of active host pcm tap points per session (can stay enabled across RUN->STOP->RUN state transitions) */
   vpm_event_reg_info_t  event_reg_info;
   /**< Event reg/unreg information */
   void                   *tx_afe_drift_ptr;
   /**< saved ptr to drift info struct (returned from Tx near-end AFE connect)  */
   void                   *rx_afe_drift_ptr;
   /**< saved ptr to drift info struct (returned from Rx AFE connect)  */
   void                   *far_src_drift_ptr;
   /**< saved ptr to drift info struct (returned from Tx AFE connect)  */
   uint16_t               timing_cmd_version;
   /**< keeps track of which version of timing command is used.
        needed for clocks requests for ADSP Test fwk.*/
   volatile uint32_t      tx_afe_delay;
   /**< Variable provided to AFE where it writes the tx port delay */
   volatile uint32_t      tx_afe_hw_delay;
   /**< Variable provided to AFE where it writes the tx hw delay */
   volatile uint32_t      rx_afe_delay;
   /**< Variable provided to AFE where it writes the rx port delay */
   volatile uint32_t      rx_afe_hw_delay;
   /**< Variable provided to AFE where it writes the rx hw delay */
};

/** @brief Structure to define the state of a the Voice Proc Manager.

    This contains information about the manager and the active voice proc sessions under control.  There
    may be 0 to VOICE_MAX_VP_SESSIONS active at any given time.
*/
struct VoiceProcMgr_t
{

   elite_svc_handle_t      svc_handle;
   /**< VPM handle to give out to others */
   voice_proc_session_t    *session[VOICE_MAX_VP_SESSIONS];
   /**< session pointers.  Any active session will be non-NULL */
   uint32_t  session_run_mask;
   /**< Session mask holds the sessions that are enabled (Populated in the order of creation LSB->MSB). */
   qurt_elite_queue_t           *vpm_respq;
   /**< queue for default acks. */
   qurt_elite_queue_t           *vpm_aferespq;
   /**< queue for responses from afe communication. */
   qurt_elite_queue_t           *afe_serv_cmdq;
   /**< saved AFE command Queue for sending AFE requests */
   qurt_elite_channel_t         channel;
   /**< channel for managing events/commands posted to this service */
   elite_apr_handle_t      apr_handle;
   /**< saved handle obtained during APR registration, required for processing APR packets */
   elite_apr_addr_t        apr_addr;
   /**< saved address obtained during APR registration, equal to VPM's APR address (domain/id pair) */

   voice_commom_mmpm_t          mmpm;
   /**< MMPM data for VPM on LPASS_ADSP core */
   uint32_t  mmpm_min_mips_per_thread;
   /**< This stores the last procured per thread mips, required for clock requests. */
   /**< Caching this is not currently required, as we do not have kpps based mmpm requests . */
   uint32_t  mmpm_mips_request;
   /**< This stores the last total mips request used for clock requests. */
   /**< Caching this is not currently required, as we do not have kpps based mmpm requests . */
   uint32_t cmd_mask;
   uint32_t wait_mask;
   uint32_t resp_mask;
   uint32_t afe_resp_mask;
   uint32_t output_mask;

   voice_so_handles_t   appi_tx;         // Handle to obtain Tx APPI. This is populated by VPM once the SO 
                                         // is successfully loaded and symbols acquired.
                                          
   voice_so_handles_t   appi_rx;         // Handle to obtain Rx APPI. This is populated by VPM once the SO 
                                         // is successfully loaded and symbols acquired.
   bool_t req_clock;                     // If any client is the Second Application Domain (APPS), then assume
                                         // end application to be ADSP Test framework on Target. If this flag is 
                                         // TRUE, then clock requests logic will be pursued.
   bool_t so_done;                       // Set to TRUE when the shared object handles are received.
};

/**
* This function creates an instance of the Elite example service. This
* function signature should be used by all services and objects, so that
* we can later adopt a registry that will enable link-time featurization.
*
* @param[in] inputParam This can be any value or pointer to structure required
*    by service help set up the instance. In the example service, this input is
*    not used.
* @param[out] handle This will be the service entry handle returned to the caller.
* @return Success/failure of instance creation.
*/
ADSPResult VoiceProcMgrCreate(uint32_t inputParam, void **handle);

/** @brief This strucutre is used for communication with client.
This structure is typically used as an argument passing to dynamic service for
   1. For service to directly raise an event with client if so required.
   2. For service to send information about the commands processing result.
*/

struct voice_proc_apr_info_t
{
   /*The following is necessary information for direct communication with client
    and for talking with APR*/
   uint32_t    apr_handle;
   /**< This is the apr handle that is required in sending ack. */
   uint16_t    self_addr;
   /**< self's address (16bit) */
   uint16_t    self_port;
   /**< slef's port (16bit) */
   uint16_t    client_addr;
   /**< Client's address (16bit)*/
   uint16_t    client_port;
   /**< Client's port (16bit) */
};

/** @brief Structure of primary payload used in vptx/vprx run custom msgs.
*/
typedef struct
{
    qurt_elite_queue_t      *pBufferReturnQ;
    /**< This is the queue to which this payload buffer needs to be returned */
    qurt_elite_queue_t      *pResponseQ;
    /**< This is the queue to send the ACK back. NULL indicates no response is required */
    uint32_t           unClientToken;
    /**< Token that should be given in the ACK. This is different than the
         unResponeResult and can be used to identify who send the ACK back by
         the server. */
    uint32_t           unResponseResult;
    /**< This is to be filled with ACK result by the client. */
    uint32_t           unSecOpCode;
    /**< This is the secondary opcode indicating the format for the rest of payload */
    void               *afe_drift_ptr;
    /**< Pointer to drift struct, return from AFE connect in VPM. */
    void               *far_src_drift_ptr;
    /**< Pointer to drift struct, return from AFE connect in VPM. */
    uint32_t           session_thread_clock_mhz;
    /**< Service may assume this to be the minimum thread clock while in Run state */
} elite_msg_custom_voiceproc_run_payload_t;

#if 0
typedef struct cvd_cal_param_t
    {
      uint32_t module_id;
      uint32_t param_id;
      uint32_t param_data_size;
      void* param_data;
    } cvd_cal_param_t;



#endif

#ifdef __cplusplus
}
#endif //__cplusplus

#endif // #ifndef VOICEDEVSVC_H

