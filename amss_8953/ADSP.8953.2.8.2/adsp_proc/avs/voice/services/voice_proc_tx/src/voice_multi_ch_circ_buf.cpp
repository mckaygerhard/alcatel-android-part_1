
#include "voice_multi_ch_circ_buf.h"

circbuf_status_enum voice_multi_ch_circ_buf_init(voice_multi_ch_circ_buf_t *structPtr, 
      int32_t num_channels, int32_t max_samples, int32_t bitsPerChannel)
{
   circbuf_status_enum  result = CIRCBUF_SUCCESS;
   if (MAX_CIRCBUF_CHANNELS >= num_channels)
   {
      structPtr->num_channels = num_channels;
      for (int32_t i = 0; i < num_channels; i++)
      {
         result = voice_circbuf_init((circbuf_struct *)&(structPtr->circ_buf[i]),
               (int8_t *)(structPtr->buf_ptr[i]), max_samples, MONO_VOICE,
               bitsPerChannel);
         if (CIRCBUF_SUCCESS != result)
         {
            break;
         }
      }
   }
   else
   {
      structPtr->num_channels = 0;
      return CIRCBUF_FAIL;
   }
   return result;
}

circbuf_status_enum voice_multi_ch_circ_buf_read(voice_multi_ch_circ_buf_t *structPtr,
      int8_t *(*toPtr)[MAX_CIRCBUF_CHANNELS], int32_t numSamples)
{
   circbuf_status_enum  result = CIRCBUF_SUCCESS;
   for (int32_t i = 0; i < structPtr->num_channels; i++)
   {
      result = voice_circbuf_read((circbuf_struct *)&(structPtr->circ_buf[i]),
            (int8_t *)(*toPtr)[i], numSamples, numSamples << 1);
      if (CIRCBUF_SUCCESS != result)
      {
         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_multi_ch_circ_buf_read error, ptr(%p) num_channels(%ld), index(%ld)",structPtr,structPtr->num_channels,i);
         break;
      }
   }
   return result;
}

circbuf_status_enum voice_multi_ch_circ_buf_block_read(voice_multi_ch_circ_buf_t *structPtr,
      int8_t *toPtr, int32_t numSamples)
{
   circbuf_status_enum  result = CIRCBUF_SUCCESS;
   int32_t num_samples_per_ch = numSamples / structPtr->num_channels; //TODO: avoid division 
   int32_t channel_offset = 0;
   for (int32_t i = 0; i < structPtr->num_channels; i++)
   {
      result = voice_circbuf_read((circbuf_struct *)&(structPtr->circ_buf[i]),
            (int8_t *)((int16_t*)toPtr+channel_offset), num_samples_per_ch, num_samples_per_ch << 1);
      channel_offset += num_samples_per_ch;
      if (CIRCBUF_SUCCESS != result)
      {
         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_multi_ch_circ_buf_block_read error, ptr(%p) num_channels(%ld), index(%ld)",structPtr,structPtr->num_channels,i);
         break;
      }
   }
   return result;
}

circbuf_status_enum voice_multi_ch_circ_buf_write(voice_multi_ch_circ_buf_t *structPtr,
      int8_t *(*fromPtr)[MAX_CIRCBUF_CHANNELS], int32_t numSamples)
{
   circbuf_status_enum  result = CIRCBUF_SUCCESS;

   for (int32_t i = 0; i < structPtr->num_channels; i++)
   {
      result = voice_circbuf_write((circbuf_struct *)&(structPtr->circ_buf[i]),
            (int8_t *)(*fromPtr)[i], numSamples); // becuase size of each sample is int16_t
      if (CIRCBUF_SUCCESS != result)
      {
         MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_multi_ch_circ_buf_write error, ptr(%p) num_channels(%ld), index(%ld), unread samples(%ld) , max samples(%ld) samples(%ld)",
               structPtr,structPtr->num_channels,i,structPtr->circ_buf[i].unread_samples, structPtr->circ_buf[i].max_samples, numSamples);
      }
      /* Keep writing to other channels even if one channel overflows */
      if (CIRCBUF_FAIL == result)
      {
         break;
      }
   }
   return result;
}

circbuf_status_enum voice_multi_ch_circ_buf_block_write(voice_multi_ch_circ_buf_t *structPtr,
      int8_t *fromPtr, int32_t numSamples)
{
   circbuf_status_enum  result = CIRCBUF_SUCCESS;
   int32_t num_samples_per_ch = numSamples / structPtr->num_channels; //TODO: avoid division 
   int32_t channel_offset = 0;

   for (int32_t i = 0; i < structPtr->num_channels; i++)
   {
      result = voice_circbuf_write((circbuf_struct *)&(structPtr->circ_buf[i]),
            (int8_t *)(((int16_t*)fromPtr)+channel_offset ), num_samples_per_ch); // becuase size of each sample is int16_t
      channel_offset += num_samples_per_ch;
      if (CIRCBUF_SUCCESS != result)
      {
         MSG_6(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"VCP: voice_multi_ch_circ_buf_write error, ptr(%p) num_channels(%ld), index(%ld), unread samples(%ld) , max samples(%ld) samples(%ld)",
               structPtr,structPtr->num_channels,i,structPtr->circ_buf[i].unread_samples, structPtr->circ_buf[i].max_samples, numSamples);
      }
      /* Keep writing to other channels even if one channel overflows */
      if (CIRCBUF_FAIL == result)
      {
         break;
      }
   }
   return result;
}
circbuf_status_enum voice_multi_ch_circ_buf_read_request(voice_multi_ch_circ_buf_t *structPtr,
      int32_t numSamples, uint32_t *availableSamplesPtr)
{
   circbuf_status_enum  result = CIRCBUF_SUCCESS;
   for (int32_t i = 0; i < structPtr->num_channels; i++)
   {
      result = voice_circbuf_read_request((circbuf_struct *)&(structPtr->circ_buf[i]),
            numSamples, availableSamplesPtr);
      if (CIRCBUF_SUCCESS != result)
      {
         break;
      }
   }
   return result;
}

circbuf_status_enum voice_multi_ch_circ_buf_reset(voice_multi_ch_circ_buf_t *structPtr)
{
   circbuf_status_enum  result = CIRCBUF_SUCCESS;
   for (int32_t i = 0; i < structPtr->num_channels; i++)
   {
      result = voice_circbuf_reset((circbuf_struct *)&(structPtr->circ_buf[i]));
      if (CIRCBUF_SUCCESS != result)
      {
         break;
      }
   }
   return result;
}

