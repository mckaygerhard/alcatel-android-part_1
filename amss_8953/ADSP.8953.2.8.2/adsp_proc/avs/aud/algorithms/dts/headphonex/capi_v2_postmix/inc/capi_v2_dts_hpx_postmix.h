/* =========================================================================
 * Copyright (c) 2015-2015 QUALCOMM Technologies Incorporated.
 * All rights reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 * =========================================================================*/

/**
 * @file capi_v2_dts_hpx_postmix.h
 *
 * Common Audio Processor Interface for dts hpx postmix.
 */

/* =========================================================================
 * Edit History:
 * when         who         what, where, why
 * ----------   -------     ------------------------------------------------
 * 2015/03/11   kgodara     CAPI_V2 Interface for dts hpx postmix
 * =========================================================================*/

#ifndef CAPI_V2_DTS_HPX_POSTMIX_H
#define CAPI_V2_DTS_HPX_POSTMIX_H

#include "Elite_CAPI_V2.h"
#include "mmdefs.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get static properties of dts hpx postmix module such as
 * memory, stack requirements etc.
 * See Elite_CAPI_V2.h for more details.
 */
capi_v2_err_t capi_v2_dts_hpx_postmix_get_static_properties(
        capi_v2_proplist_t *init_set_properties,
        capi_v2_proplist_t *static_properties);


/**
 * Instantiates(and allocates) the module memory.
 * See Elite_CAPI_V2.h for more details.
 */
capi_v2_err_t capi_v2_dts_hpx_postmix_init(
        capi_v2_t          *_pif,
        capi_v2_proplist_t *init_set_properties);

#ifdef __cplusplus
}
#endif

#endif /* CAPI_V2_DTS_HPX_POSTMIX_H */

