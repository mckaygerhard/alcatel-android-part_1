#ifndef C_DTS_ENCODER_LIB_H
#define C_DTS_ENCODER_LIB_H

/* ========================================================================
   DTS encoder library wrapper header file

  *//** @file CDTSEncoderlib.h
  This is a wrapper code for DTS Core encoder library.
  the function in this file are called by the CDTSEncoder media module.
  It is derived from CAudioProcLib class

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   23/04/12   SS     Created file.

   ========================================================================= */

#include "Elite_CAPI.h"

/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */
/* Sizes of DTS Encoder Input and Output buffers in bytes */
#define DTSTRANSENC_WINDOW	512     // encode samples window size
#define MAXNUMPRMCH			5       // Maximum number of primary audio channels.
#define UINT32_SIZE			4
#define DTSENC_INPUT_SIZE_PER_CHANNEL (DTSTRANSENC_WINDOW * UINT32_SIZE)
#define DTSENC_OUTPUT_SIZE (DTSTRANSENC_WINDOW * UINT32_SIZE)

class CDTSEncoderLib: public ICAPI
{
private:
#if ((defined __hexagon__) || (defined __qdsp6__))
	uint32 m_ui32InpBuffer[DTSTRANSENC_WINDOW*(MAXNUMPRMCH+1)] __attribute__ ((__aligned__(8)));
	uint32 m_ui32OutBuffer[DTSTRANSENC_WINDOW] __attribute__ ((__aligned__(8)));
#else
	uint32 m_ui32InpBuffer[DTSTRANSENC_WINDOW*(MAXNUMPRMCH+1)];
	uint32 m_ui32OutBuffer[DTSTRANSENC_WINDOW];
#endif

	int CreateDTSEncoder();
	int CopyInterleavedPCM();

public:

	void *m_pEncState;

	enum DTSEncParamIdx
	{
		eDTSnPCMR = eIcapiMaxParamIndex,									// Bits per sample
		eDTSnFS,									// Source sample rate
		eDTSnBitrate,  								// Output bit rate
		eDTSnActualOutputSize
	};
	/* =======================================================================
	*                          Public Function Declarations
	* ======================================================================= */

	/**
	* Constructor of CDTSEncoderLib
	*/
	CDTSEncoderLib ( );

	/**
	* Constructor of CDTSEncoderLib
	*/
	CDTSEncoderLib ( ADSPResult &result );

	/**
	* Destructor of CDTSEncoderLib
	*/
	virtual ~CDTSEncoderLib ( );

	/*************************************************************************
	* CAudioProcLib Methods
	*************************************************************************/

	/**
	* Initialize the core encoder library
	*
	* @return     success/failure is returned
	*/
	virtual int CDECL Init ( CAPI_Buf_t* pParams );

	/**
	* Re initialize the core encoder library in the case of repositioning or
	* when full initialization is not required
	*
	* @return     success/failure is returned
	*/
	virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

	/**
	* Gracefully exit the core encoder library
	*
	* @return     success/failure is returned
	*/
	virtual int CDECL End ( void );

	/**
	* Get the value of the DTS encoder parameters
	*
	* @param[in]   nParamIdx      Enum value of the parameter of interest
	* @param[out]  pnParamVal     Desired value of the parameter of interest
	*
	* @return   Success/fail
	*/
	virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );


	/**
	* Set the value of the DTS encoder parameters
	*
	* @param[in]   nParamIdx      Enum value of the parameter of interest
	* @param[out]  nPrarmVal      Desired value of the parameter of interest
	*
	* @return   Success/fail
	*/
	virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

	/**
	* Encode audio bitstream and produce one frame worth of samples
	*
	* @param[in]   pInBitStream     Pointer to input bit stream
	* @param[out]  pOutSamples      Pointer to output samples
	* @param[out]  pOutParams       Pointer to output parameters
	*
	* @return     Success/failure
	*/
	virtual int CDECL Process ( const CAPI_BufList_t* pInSamples,
		CAPI_BufList_t*       pOutBitStream,
		CAPI_Buf_t*       pOutParams );

};


#endif /* C_DTS_ENCODER_LIB_H */

