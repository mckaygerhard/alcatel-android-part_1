/*========================================================================

                                 O p e n M M
       S B C  E n c o d e r   -  I m p l e m e n t a t i o n

*//** @file CSbcEncoder.hpp
  CSbcEncoder is a c++ Component that encapsulate c-sim code


Copyright (c) 2007-2010 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
*//*====================================================================== */
/*========================================================================
                             Edit History

$Header: //components/rel/avs.adsp/2.7/aud/algorithms/audencdec/sbc/oi/enc/CSbcEncoderLib/inc/CSbcEncoderLib.h#3 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
11/20/07   rl          Created file.
5/13/10   SarithaJ     Make it compatible to AudioLite Enc Service.

========================================================================== */

#ifndef _CSBCENCODER_LIB_H_
#define _CSBCENCODER_LIB_H_

#include "Elite_CAPI.h"

    class CSbcEncoderLib : public ICAPI
    {

    public:

     void* m_pEncState;

     /*Enum declarations for SBC params*/
     enum SBCParamIdx
      {
         eSbcMode = eIcapiMaxParamIndex,
         eSbcBitRate,
         eSbcBands,
         eSbcBlocks,
         eSbcAlloc                 		   
      };

   /* =======================================================================
    *                          Public Function Declarations
    * ======================================================================= */

   /**
    * Constructor of CSbcEncoderLib
    */
        CSbcEncoderLib(ADSPResult &result);

   /**
    * Destructor of CSbcEncoderLib
    */
    virtual ~CSbcEncoderLib();

   /*************************************************************************
    * CAudioProcLib Methods
    *************************************************************************/

   /**
    * Initialize the core encoder library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL Init ( CAPI_Buf_t* pParams );

   /**
    * Re initialize the core encoder library in the case of repositioning or
    * when full initialization is not required
    *
    * @return     success/failure is returned
    */
   virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

   /**
    * Gracefully exit the core encoder library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL End ( void );

   /**
    * Get the value of the SBC encoder parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  pnParamVal     Desired value of the parameter of interest
    *
    * @return   Success/fail
    */
   virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );

   /**
    * Get the value of the SBC encoder parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  nPrarmVal      Desired value of the parameter of interest
    *
    * @return   Success/fail
    */
   virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

   /**
    * Encode audio bitstream and produce one frame worth of samples
    *
    * @param[in]   pInBitStream     Pointer to input bit stream
    * @param[out]  pOutSamples      Pointer to output samples
    * @param[out]  pOutParams       Pointer to output parameters
    *
    * @return     Success/failure
    */
   virtual int CDECL Process ( const CAPI_BufList_t* pInSamples,
                               CAPI_BufList_t*       pOutBitStream,
                               CAPI_Buf_t*       pOutParams );



    };

#endif  // end of _CSBCENCODER_LIB_H_
/**
@}
*/
