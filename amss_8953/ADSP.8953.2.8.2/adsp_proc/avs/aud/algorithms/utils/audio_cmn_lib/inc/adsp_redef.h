/*========================================================================
Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/12/2015 svutukur      Created file to get rid of redef link errors while integrating audio code in mpss builds.

========================================================================== */

#define log2table adsp_log2table
#define sqRootLut adsp_sqRootLut
#define invSqRootLut adsp_invSqRootLut
#define invTable adsp_invTable