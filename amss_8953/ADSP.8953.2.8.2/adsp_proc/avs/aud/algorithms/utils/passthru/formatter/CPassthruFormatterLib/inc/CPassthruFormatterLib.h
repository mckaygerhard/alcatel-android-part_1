#ifndef C_PASSTHRUFORMATTERLIB_H
#define C_PASSTHRUFORMATTERLIB_H

/* ========================================================================
   Pass-through formatter library wrapper header file

  *//** @file CPassthruFormatter.h
  This is a wrapper code for core Passthru library. 

  Copyright (c) 2008 Qualcomm Technologies Incorporated.
  All Rights Reserved. Qualcomm Proprietary and Confidential.
  *//*====================================================================== */

/* =========================================================================
                             Edit History

   when       who           what, where, why
   --------   ---           ------------------------------------------------
   09/16/11   RP            Created file.

   ========================================================================= */

#include "Elite_CAPI.h"

/* =======================================================================
 *                       DEFINITIONS AND DECLARATIONS
 * ====================================================================== */

class CPassthruFormatterLib : public ICAPI
{

   private:
   
   uint32_t m_frame_rate;
   uint32_t m_frame_number;   
   bool_t m_end_of_stream;

   /*Default constructor*/
   CPassthruFormatterLib ( );

   public:
   
   enum ePassthruFormatterParamIdx
   {
      ePassthruFormatterEndOfStream = eIcapiMaxParamIndex,         
   };

   /*channel mapping info for Passthru formatter*/
   CAPI_ChannelMap_t m_PassthruChannelMap;

   /* =======================================================================
    *                          Public Function Declarations
    * ======================================================================= */

   /**
    * Constructor of CPassthruFormatterLib
    */
   CPassthruFormatterLib (ADSPResult    &nRes);

   /**
    * Destructor of CPassthruFormatterLib
    */
   ~CPassthruFormatterLib ( );


   /*************************************************************************
    * CPassthruFormatterLib Methods
    *************************************************************************/

   /**
    * Initialize the core library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL Init ( CAPI_Buf_t* pParams );

   /**
    * Re initialize the core library in the case of repositioning or
    * when full initialization is not required
    *
    * @return     success/failure is returned
    */
   virtual int CDECL ReInit ( CAPI_Buf_t* pParams );

   /**
    * Gracefully exit the core library
    *
    * @return     success/failure is returned
    */
   virtual int CDECL End ( void );

   /**
    * Get the value of the passthru parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    *
    * @return  Value of the parameter of interest
    */
   virtual int CDECL GetParam ( int nParamIdx, int *pnParamVal );

   /**
    * Get the value of the passthru parameters
    *
    * @param[in]   nParamIdx      Enum value of the parameter of interest
    * @param[out]  nPrarmVal      Desired value of the parameter of interest
    *
    * @return  None
    */
   virtual int CDECL SetParam ( int nParamIdx, int nParamVal );

   /**
    * Decode audio bitstream and produce one frame worth of samples
    *
    * @param[in]   pInBitStream     Pointer to input bit stream
    * @param[out]  pOutSamples      Pointer to output samples
    * @param[out]  pOutParams       Pointer to output parameters
    *
    * @return     Success/failure
    */
   virtual int CDECL Process ( const CAPI_BufList_t* pInBitStream,
                               CAPI_BufList_t*       pOutSamples,
                               CAPI_Buf_t*       pOutParams );   

};


#endif /* C_PASSTHRUFORMATTERLIB_H */

