/*======================================================================*
 DTS, Inc.
 5220 Las Virgenes Road
 Calabasas, CA 91302  USA

 CONFIDENTIAL: CONTAINS CONFIDENTIAL PROPRIETARY INFORMATION OWNED BY
 DTS, INC. AND/OR ITS AFFILIATES ("DTS"), INCLUDING BUT NOT LIMITED TO
 TRADE SECRETS, KNOW-HOW, TECHNICAL AND BUSINESS INFORMATION. USE,
 DISCLOSURE OR DISTRIBUTION OF THE SOFTWARE IN ANY FORM IS LIMITED TO
 SPECIFICALLY AUTHORIZED LICENSEES OF DTS.  ANY UNAUTHORIZED
 DISCLOSURE IS A VIOLATION OF STATE, FEDERAL, AND INTERNATIONAL LAWS.
 BOTH CIVIL AND CRIMINAL PENALTIES APPLY.

 DO NOT DUPLICATE. COPYRIGHT 2014, DTS, INC. ALL RIGHTS RESERVED.
 UNAUTHORIZED DUPLICATION IS A VIOLATION OF STATE, FEDERAL AND
 INTERNATIONAL LAWS.

 ALGORITHMS, DATA STRUCTURES AND METHODS CONTAINED IN THIS SOFTWARE
 MAY BE PROTECTED BY ONE OR MORE PATENTS OR PATENT APPLICATIONS.
 UNLESS OTHERWISE PROVIDED UNDER THE TERMS OF A FULLY-EXECUTED WRITTEN
 AGREEMENT BY AND BETWEEN THE RECIPIENT HEREOF AND DTS, THE FOLLOWING
 TERMS SHALL APPLY TO ANY USE OF THE SOFTWARE (THE "PRODUCT") AND, AS
 APPLICABLE, ANY RELATED DOCUMENTATION:  (i) ANY USE OF THE PRODUCT
 AND ANY RELATED DOCUMENTATION IS AT THE RECIPIENT'S SOLE RISK:
 (ii) THE PRODUCT AND ANY RELATED DOCUMENTATION ARE PROVIDED "AS IS"
 AND WITHOUT WARRANTY OF ANY KIND AND DTS EXPRESSLY DISCLAIMS ALL
 WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE, REGARDLESS OF WHETHER DTS KNOWS OR HAS REASON TO KNOW OF THE
 USER'S PARTICULAR NEEDS; (iii) DTS DOES NOT WARRANT THAT THE PRODUCT
 OR ANY RELATED DOCUMENTATION WILL MEET USER'S REQUIREMENTS, OR THAT
 DEFECTS IN THE PRODUCT OR ANY RELATED DOCUMENTATION WILL BE
 CORRECTED; (iv) DTS DOES NOT WARRANT THAT THE OPERATION OF ANY
 HARDWARE OR SOFTWARE ASSOCIATED WITH THIS DOCUMENT WILL BE
 UNINTERRUPTED OR ERROR-FREE; AND (v) UNDER NO CIRCUMSTANCES,
 INCLUDING NEGLIGENCE, SHALL DTS OR THE DIRECTORS, OFFICERS, EMPLOYEES,
 OR AGENTS OF DTS, BE LIABLE TO USER FOR ANY INCIDENTAL, INDIRECT,
 SPECIAL, OR CONSEQUENTIAL DAMAGES (INCLUDING BUT NOT LIMITED TO
 DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, AND LOSS
 OF BUSINESS INFORMATION) ARISING OUT OF THE USE, MISUSE, OR INABILITY
 TO USE THE PRODUCT OR ANY RELATED DOCUMENTATION.
*======================================================================*/

#ifndef __SRS_TRUMEDIA_APPI_H__
#define __SRS_TRUMEDIA_APPI_H__

#include "shared_lib_api.h"
#include "adsp_audproc_api.h"

#define SRS_TECH_WOWHDX     1

#include "srs_common_ver_api.h"
#include "srs_geq_ver_api.h"
#include "srs_geq10b_api.h"
#include "srs_aeq_api.h"
#include "srs_misc_api.h"
#include "srs_iir_api.h"
#include "srs_cshp_api.h"
#include "srs_fft_api.h"
#ifdef SRS_TECH_WOWHDX
    #include "srs_wowhdx_api.h"
#else
#include "srs_wowhd_api.h"
#endif
#include "srs_hardlimiter_api.h"
#include "appi_srs_TruMedia_api.h"

enum SRS_Security_Mode {
	SRS_SM_INACTIVE = 0,
	SRS_SM_DEMO = 1,
	SRS_SM_ACTIVE = 2,
};
#define SRS_DEFAULT_MODE SRS_SM_INACTIVE

#define PARAMS_UPDATED_WOWHD				0x01
#define PARAMS_UPDATED_WOWHD_FILTERS		0x02
#define PARAMS_UPDATED_CSHP					0x04
#define PARAMS_UPDATED_CSHP_FILTERS			0x08
#define PARAMS_UPDATED_HPF					0x10
#define PARAMS_UPDATED_AEQ                  0x20
#define PARAMS_UPDATED_HL					0x40
#define PARAMS_UPDATED_GEQ                  0x80
#ifdef SRS_TECH_WOWHDX
    #define PARAMS_UPDATED_WOWHDX_FILTERS      0x100
#endif

#define XFADE

#ifdef XFADE	   
enum CrossFade {
      CF_NONE = 0,
	  CF_IN,			// global in
      CF_OUT,			// global out
	  CF_W2C,			// wowhd to cshp
	  CF_C2W,			// cshp to wowhd
	  CF_WOUT,			// wowhd out
	  CF_WIN,			// wowhd in
	  CF_COUT,			// cshp out
	  CF_CIN			// cshp in
};
#endif

typedef struct {
   const appi_vtbl_t        *vtbl;   
   
   // global
   uint32_t					num_channels;
   uint32_t					blk_size;
   uint32_t					sample_rate;
   uint32_t					interleaved;
   uint32_t                 bits_per_sample;
   uint32_t					enable_flags;   
   void*					ws;
   
    bool_t                  is_trumediahd_enabled;
#ifdef SRS_TECH_WOWHDX
    // wowhdx
    SRSWowhdxObj            wowhdobj;
    void*                   wowhd_fftplanbufptr;
    int16_t                 xhp_coefs[32];
    int16_t                 xlp_coefs[32];
    int16_t                 tbhp_coefs[32];
    uint32_t                tbhp_order;
#else
   // wowhd   
   SRSWowhdObj              wowhdobj;
#endif
   
   // cshp   
   SRSCshpObj				cshpobj;
   
   // hpf
   void*					hpfptr_l;
   void*					hpfptr_r;
   SRSIirObj				hpfobj_l;
   SRSIirObj				hpfobj_r;
   int32_t					hpfactive;
   uint32_t					hpf_coefs[26]; // space for hpf coefs is needed
   
    // geq
    void*                   geqptr_l;
    void*                   geqptr_r;
    SRSGeq10bObj            geqobj_l;
    SRSGeq10bObj            geqobj_r;
    int32_t                 geqactive;
    int16_t                 geqUserGains[10];

    // parametric eq
    void*                   aeqptr_l;
    void*                   aeqptr_r;
    SRSAeqObj               aeqobj_l;
    SRSAeqObj               aeqobj_r;
    int32_t                 max_nbands;
    int32_t                 aeqactive;
    uint32_t                l_coefs[74]; // space for aeq coefs is needed (6*(nBands)+2)
    uint32_t                r_coefs[74];
    void*                   aeq_fftplanbufptr;
    uint16_t                fir_coeffs[8206]; // (8 + 2 + 2 + 2 + (max length * 2)) usage for maximum size
   
    // hardlimiter
    SRSHdLmtObj             hardlmtobj;   
   
   int32_t*					cache_in;
   int32_t*					cache_out;
   uint32_t					cache_pos;
   
   // for handling interleaved/noninterleaved
   uint32_t					lchan_idx;
   uint32_t					lchan_off;
   uint32_t					rchan_idx;
   uint32_t					rchan_off;
   uint32_t					lr_inc;
   
   uint8_t*					ws2;
   
   SRS_Security_Mode		security_mode;
   uint32_t					security_mode_ever_active;
   uint32_t					security_demo_timer;
   uint32_t					security_process_flag;
   uint32_t					security_demo_time_next;
   uint32_t					security_process_cnt;
   
   // shadow parameters for cross fading support
   srs_TruMedia_wowhd_params_t	shadow_params_wowhd;
   srs_TruMedia_cshp_params_t	shadow_params_cshp;
   srs_TruMedia_hpf_params_t	shadow_params_hpf;
    srs_TruMedia_aeq_params_t   shadow_params_aeq;
   srs_TruMedia_hl_params_t		shadow_params_hl;
    srs_TruMedia_geq_params_t   shadow_params_geq;
   srs_TruMedia_params_t		shadow_params_globals;

#ifdef XFADE	   
   CrossFade					cross_fade;
   int32_t						cross_fade_needed;
   int32_t						cross_fade_out_only;
#endif
   
   int32_t						params_flag;
    
    int32_t                     igain;
} appi_srs_TruMedia_struct;

#endif /*__SRS_TRUMEDIA_APPI_H__*/
