/* ======================================================================== */
/**
@file appi_eans.h

   Header file to implement the Audio Post Processor Interface for Enhanced
   Audio Noise Supression (EANS).
*/

/* =========================================================================
  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   09/09/12   bvodapal  Rev C - Adding Multichannel Support
   11/17/10   ss      Introducing APPI Rev B
   10/20/10   ss      Introducing Audio Post Processor Interface for EANS
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_EANS_H
#define __APPI_EANS_H

#include "Elite_APPI.h"
#include "adsp_error_codes.h"
#include "adsp_adm_api.h"

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/
#define EANS_MAX_CHANNELS 8

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_eans_getsize(
      const appi_buf_t*     params_ptr,
      uint32_t*             size_ptr);

ADSPResult appi_eans_init(
      appi_t*              _pif,
      bool_t*              is_in_place_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

typedef struct _appi_eans_t
{
   const appi_vtbl_t*   vtbl;
   void*              m_pEansStaticMem[EANS_MAX_CHANNELS];  // Eans static memory and buffers
   void*              m_pEansScratchMem[EANS_MAX_CHANNELS];
   int16_t            sEnable;
   int16_t            sEnableState;       // Since EANS does not work for certain sampling rates,
   uint16_t           nSamplingRate;      // This is just a current Sampling rate indicator
   uint16_t           nChannels;      // This is just a current Sampling rate indicator
   int16_t            *local_in_buf_ptr[EANS_MAX_CHANNELS];
   uint32_t           samples_in_local_in_buf;
   int16_t            *local_out_buf_ptr[EANS_MAX_CHANNELS];
   uint32_t           samples_in_local_out_buf;
   uint16_t           num_channels_created;
   bool_t             fParamsChanged;
   QURT_ELITE_ALIGN(int16_t m_naEansParams[42], 8);
   // For both these arrays:
   // m_naEansParams[0]= reserved
   // m_naEansParams[1]= Frame size (depends on sampling rate)
   // m_naEansParams[2-39]= list of 38 params
   // The above arrangement is to fecilitate memmove (alignment)
} appi_eans_t;

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //__APPI_EANS_H

