/* ======================================================================== */
/**
@file appi_downmix.h

   Header file to implement the Audio Post Processor Interface for channel downmix.
   Supports 5.1 to stereo/mono and stereo to mono.
*/

/* =========================================================================
  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   2/24/2011  dg      Created the file using the soft volume controls appi as template.
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_DOWNMIX_H
#define __APPI_DOWNMIX_H

#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/

#include "Elite_APPI.h"
#include "adsp_error_codes.h"

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/
static const uint32_t AUDPROC_PARAM_ID_DOWNMIX_OUT_CHANNELS = 1;

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_downmix_getsize(
      const appi_buf_t*    params_ptr,
      uint32_t*            size_ptr);

ADSPResult appi_downmix_init( 
      appi_t*              _pif,
      bool_t*              is_in_place_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif //__APPI_DOWNMIX_H

