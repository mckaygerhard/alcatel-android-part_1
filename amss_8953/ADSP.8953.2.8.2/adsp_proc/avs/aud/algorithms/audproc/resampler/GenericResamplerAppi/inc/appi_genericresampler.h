/* ======================================================================== */
/**
@file appi_genericresampler.h

   Header file to implement the Audio Post Processor Interface for Generic
   Resampler
*/

/* =========================================================================
  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   11/17/10   ss      Introducing APPI Rev B
   10/29/10   ss      Introducing Audio Post Processor Interface for Generic
                      Resampler
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_CGENERICRESAMPLER_H
#define __APPI_CGENERICRESAMPLER_H

#include "Elite_APPI.h"
#include "adsp_error_codes.h"

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/
#define NUM_RESAMPLERS_NEEDED(numChannels) (((numChannels) + 1) / 2) // Round up to next multiple of 2, since each generic resampler can process upto 2 channels.

static const uint32_t MULTI_RESAMP_MAX_CHANNELS = 8;
static const uint32_t MAX_RESAMPLERS = NUM_RESAMPLERS_NEEDED(MULTI_RESAMP_MAX_CHANNELS);

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_genericresampler_getsize(
      const appi_buf_t*    params_ptr,
      uint32_t*            size_ptr);

ADSPResult appi_genericresampler_init(
      appi_t*              _pif,
      bool_t*              is_in_place_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);

#endif //__APPI_CGENERICRESAMPLER_H

