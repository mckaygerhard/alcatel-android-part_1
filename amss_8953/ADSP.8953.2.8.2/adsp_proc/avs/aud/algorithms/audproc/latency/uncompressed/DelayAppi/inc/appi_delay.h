/* ======================================================================== */
/**
@file appi_delay.h

   Header file for the delay module.
*/

/* =========================================================================
  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   04/02/13   dg      Created the delay module.
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_DELAY_H
#define __APPI_DELAY_H

#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/

#include "Elite_APPI.h"

/*------------------------------------------------------------------------
 * Macros, Defines, Type declarations
 * -----------------------------------------------------------------------*/

/*------------------------------------------------------------------------
 * Function declarations
 * -----------------------------------------------------------------------*/
ADSPResult appi_delay_getsize(
      const appi_buf_t*    params_ptr,
      uint32_t*            size_ptr);

ADSPResult appi_delay_init(
      appi_t*              _pif,
      bool_t*              is_in_place_ptr,
      const appi_format_t* in_format_ptr,
      appi_format_t*       out_format_ptr,
      appi_buf_t*          info_ptr);


#ifdef __cplusplus
}
#endif /*__cplusplus*/


#endif //__APPI_DELAY_H

