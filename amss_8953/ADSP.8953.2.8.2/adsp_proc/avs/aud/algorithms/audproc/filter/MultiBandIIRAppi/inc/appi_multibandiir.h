/* ======================================================================== */
/**
@file appi_multibandiir.h

   Header file to implement the Audio Post Processor Interface for Multi-Band
   IIR filter object (MBIIR).
*/

/* =========================================================================
  Copyright (c) 2010 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   10/26/10   c_liyenh      Introducing Audio Post Processor Interface for MBIIR
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_MULTIBANDIIR_H
#define __APPI_MULTIBANDIIR_H

#include "Elite_APPI.h"

   /**
    * This function get size of APPI definition structure to IIR filters
    * @param[in] params_ptr - meta data-interface pointer
    * @param[out] size_ptr - return appi data structure size
    *
    * @return
    * - ADSP_EOK - The initialization was successful.
    * - error code - There was an error which needs to propagate.
    */
ADSPResult appi_multibandiir_getsize(
      /*[in]*/ const appi_buf_t*    params_ptr,
      /*[out]*/ uint32_t*            size_ptr);
   /**
    * This function format APPI definitions to IIR filters
    * @param[in] _pif - IAPPI pseudo-interface pointer
    * @param[in] pInfo - setup information in metadata (custom) form
    * @param[in] pInFormat - input format
    * @param[in] pOutFormat - output format
    * @param[in] pfInPlace - in-place computation indicator
    *
    * @return
    * - ADSP_EOK - The initialization was successful.
    * - error code - There was an error which needs to propagate.
    */
ADSPResult appi_multibandiir_init(
      /*[in]*/ appi_t*                         _pif,
      /*[out]*/ bool_t*                    pfInPlace,
      /*[in]*/ const appi_format_t*   pInFormat,
      /*[in]*/ appi_format_t*         pOutFormat,
      /*[in]*/ appi_buf_t*                pInfo);

#endif //__APPI_MULTIBANDIIR_H

