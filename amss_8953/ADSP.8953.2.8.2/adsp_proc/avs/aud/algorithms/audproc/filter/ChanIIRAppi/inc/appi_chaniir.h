/* ======================================================================== */
/**
@file appi_chaniir.h

   Header file to implement the Audio Post Processor Interface for channel
   IIR filter object (ChanIIR).
*/

/* =========================================================================
  Copyright (c) 2011 Qualcomm Technologies Incorporated.
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   03/15/11   WJ      Initial creation
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_CHANIIR_H
#define __APPI_CHANIIR_H

#include "Elite_APPI.h"
#include "adsp_error_codes.h"
#include "qurt_elite.h"
#include "adsp_adm_api.h"
#include "adsp_asm_api.h"

#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/

typedef enum
{
   CHANIIR_LEFT_CHAN = 0,
   CHANIIR_RIGHT_CHAN,
   CHANIIR_CENTER_CHAN,
   CHANIIR_LEFT_SURROUND,
   CHANIIR_RIGHT_SURROUND,
   CHANIIR_LFE
} channel_index_enum;

typedef struct _chaniir_init_info
{
   uint32_t        num_stages;
   channel_index_enum    chan_index;
} chaniir_init_info;


   /**
    * This function get size of APPI definition structure to IIR filters
    * @param[in] params_ptr - meta data-interface pointer
    * @param[out] size_ptr - return appi data structure size
    *
    * @return
    * - ADSP_EOK - The initialization was successful.
    * - error code - There was an error which needs to propagate.
    */
ADSPResult appi_left_chaniir_getsize(
      /*[in]*/ const appi_buf_t*    params_ptr,
      /*[out]*/ uint32_t*            size_ptr);
   /**
    * This function format APPI definitions to IIR filters
    * @param[in] _pif - IAPPI pseudo-interface pointer
    * @param[in] pInfo - setup information in metadata (custom) form
    * @param[in] pInFormat - input format
    * @param[in] pOutFormat - output format
    * @param[in] pfInPlace - in-place computation indicator
    *
    * @return
    * - ADSP_EOK - The initialization was successful.
    * - error code - There was an error which needs to propagate.
    */
ADSPResult appi_left_chaniir_init(
		/*[in]*/ appi_t*                         _pif,
		/*[out]*/ bool_t*                    pfInPlace,
		/*[in]*/ const appi_format_t*   pInFormat,
		/*[in]*/ appi_format_t*         pOutFormat,
		/*[in]*/ appi_buf_t*                pInfo);


/**
 * This function get size of APPI definition structure to IIR filters
 * @param[in] params_ptr - meta data-interface pointer
 * @param[out] size_ptr - return appi data structure size
 *
 * @return
 * - ADSP_EOK - The initialization was successful.
 * - error code - There was an error which needs to propagate.
 */
ADSPResult appi_right_chaniir_getsize(
		/*[in]*/ const appi_buf_t*    params_ptr,
		/*[out]*/ uint32_t*            size_ptr);
/**
 * This function format APPI definitions to IIR filters
 * @param[in] _pif - IAPPI pseudo-interface pointer
 * @param[in] pInfo - setup information in metadata (custom) form
 * @param[in] pInFormat - input format
 * @param[in] pOutFormat - output format
 * @param[in] pfInPlace - in-place computation indicator
 *
 * @return
 * - ADSP_EOK - The initialization was successful.
 * - error code - There was an error which needs to propagate.
 */
ADSPResult appi_right_chaniir_init(
      /*[in]*/ appi_t*                         _pif,
      /*[out]*/ bool_t*                    pfInPlace,
      /*[in]*/ const appi_format_t*   pInFormat,
      /*[in]*/ appi_format_t*         pOutFormat,
      /*[in]*/ appi_buf_t*                pInfo);

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif //__APPI_CHANIIR_H

