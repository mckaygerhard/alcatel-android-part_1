/* ======================================================================== */
/**
@file appi_compressed_mute.h

   Header file to implement the Audio Post Processor Interface for Muting the
   compressed Stream
*/

/*===========================================================================
  Copyright (c) 2013 Qualcomm Technologies, Inc. All rights reserved.
  Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/* =========================================================================
                             Edit History

   when        who          what, where, why
   --------    ---         ------------------------------------------------------
   04/19/13    bvodapal    Introducing Audio Post Processor Interface for
   	   	   	   	   	   	   Compressed mute module
   ========================================================================= */

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#ifndef __APPI_COMPRESSED_MUTE_H
#define __APPI_COMPRESSED_MUTE_H

#ifdef __cplusplus
extern "C"{
#endif /*__cplusplus*/

#include "Elite_APPI.h"

#define COMPRESSED_MUTE_MAX_CHANNELS 8
#define COMPRESSED_MUTE_MIN_CHANNELS 2

   /**
    * This function get size of APPI definition structure of compressed stream mute module
    * @param[in] params_ptr - meta data-interface pointer
    * @param[out] size_ptr - return appi data structure size
    *
    * @return
    * - ADSP_EOK - The initialization was successful.
    * - error code - There was an error which needs to propagate.
    */
ADSPResult appi_compressed_mute_getsize(
      /*[in]*/ const appi_buf_t*    params_ptr,
      /*[out]*/ uint32_t*            size_ptr);

   /**
    * This function format APPI definitions to compressed stream mute module
    * @param[in] _pif - IAPPI pseudo-interface pointer
    * @param[in] pInfo - setup information in metadata (custom) form
    * @param[in] pInFormat - input format
    * @param[in] pOutFormat - output format
    * @param[in] pfInPlace - in-place computation indicator
    *
    * @return
    * - ADSP_EOK - The initialization was successful.
    * - error code - There was an error which needs to propagate.
    */
ADSPResult appi_compressed_mute_init(
      /*[in]*/ appi_t*                   _pif,
      /*[out]*/ bool_t*                    pfInPlace,
      /*[in]*/ const appi_format_t*   pInFormat,
      /*[in]*/ appi_format_t*         pOutFormat,
      /*[in]*/ appi_buf_t*                pInfo);

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif //__APPI_COMPRESSED_MUTE_H

