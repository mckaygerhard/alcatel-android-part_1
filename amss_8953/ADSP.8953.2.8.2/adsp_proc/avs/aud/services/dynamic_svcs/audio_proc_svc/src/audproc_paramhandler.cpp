
/*==========================================================================
ELite Source File

This file implements the functions for setting the parameters for the Audio Post
Processing Dynamic Service.

Copyright (c) 2013-2015 QUALCOMM Technologies, Incorporated.
All Rights Reserved. QUALCOMM Technologies Proprietary.
Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 */

/*===========================================================================
Edit History

when       who     what, where, why
--------   ---     ---------------------------------------------------------
09/20/10   DG      Added support for the case of 5.1 bypass, when the number
                   of input and output channels will be 6.
06/09/10   DG      Created file and copied functions from AudDynaPPSvc.cpp.
=========================================================================== */

/*---------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "audproc_comdef.h"
#include "audproc_appi_topo.h"
#include "EliteTopology_db_if.h"
#include "capi_v2_dynamic_resampler.h"
/*---------------------------------------------------------------------------
 * Definitions and Constants
 * -------------------------------------------------------------------------*/

#define KPPS_VOTE_MEDIA_FMT_CHANGE 50000 // 50 MPPS
#define BW_VOTE_MEDIA_FMT_CHANGE 20*1024*1024 //20 MBPS

/*---------------------------------------------------------------------------
 * Function Declarations
 * -------------------------------------------------------------------------*/
static ADSPResult AudPP_SetMediaFormat(ThisAudDynaPPSvc_t* me,
      const AdspAudioMediaFormatInfo_t *pInputMediaFormat,
      const AdspAudioMediaFormatInfo_t *pOutputMediaFormat);
static void AudPP_PrintMediaFormat(const ThisAudDynaPPSvc_t* me, const AdspAudioMediaFormatInfo_t *pMediaFormat, bool_t is_input);
static ADSPResult AudPP_sendMediaTypeSetParams(ThisAudDynaPPSvc_t* me, const AdspAudioMediaFormatInfo_t *pOutputMediaFormat);

static void AudPP_topo_KPPS_event(topo_event_client_t *obj_ptr, uint32_t new_KPPS);
static void AudPP_topo_bandwidth_event(topo_event_client_t *obj_ptr, uint32_t new_bandwidth);
static void AudPP_topo_output_media_format_event(topo_event_client_t *obj_ptr, const AdspAudioMediaFormatInfo_t *new_format);
static void AudPP_topo_algorithmic_delay_event(topo_event_client_t *obj_ptr, uint32_t new_delay);
/*---------------------------------------------------------------------------
 * Globals
 * -------------------------------------------------------------------------*/
const topo_event_vtable_t topo_event_vtable = {
      AudPP_topo_KPPS_event,
      AudPP_topo_bandwidth_event,
      AudPP_topo_output_media_format_event,
      AudPP_topo_algorithmic_delay_event
};

/*---------------------------------------------------------------------------
 * Function Definitions
 * -------------------------------------------------------------------------*/

static ADSPResult AudPP_SetMediaFormat(ThisAudDynaPPSvc_t* me,
      const AdspAudioMediaFormatInfo_t *pInputMediaFormat,
      const AdspAudioMediaFormatInfo_t *pOutputMediaFormat)
{
   ADSPResult result;

   //MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX: Setting media format", me->objId);
   AudPP_PrintMediaFormat(me, pInputMediaFormat, TRUE);

   me->is_processing_set_media_type = TRUE;

   result = AudPP_sendMediaTypeSetParams(me, pOutputMediaFormat);
   if (ADSP_EOK != result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Failed to send set params for media type\n", me->objId);
   }
   if (ADSP_ENOMEMORY == result)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Ran out of memory while trying to change media type.", me->objId);
      AudPP_GotoFatalState(me);
   }

   AdspAudioMediaFormatInfo_t oldOutputMediaFormat;
   oldOutputMediaFormat = me->outputMediaFormatInfo;

   AdspAudioMediaFormatInfo_t inputFormat = *pInputMediaFormat;
   if (!isCompressedFormat(pInputMediaFormat))
   {
      inputFormat.isInterleaved = FALSE; // Deinterleaving is done by the format converter
   }

   if (ADSP_SUCCEEDED(result))
   {
	   uint32_t prev_vote_kpps,prev_vote_bw;
	   prev_vote_kpps = me->topologyKpps;
	   prev_vote_bw = me->topologyBandwidth;

	   //Set KPPS and BW needed for reinit sequence if existing vote does not suffice
	   me->topologyKpps	= (me->topologyKpps < KPPS_VOTE_MEDIA_FMT_CHANGE) ? KPPS_VOTE_MEDIA_FMT_CHANGE : me->topologyKpps; // 50 MPPS Voting during media format update
	   me->topologyBandwidth = (me->topologyBandwidth < BW_VOTE_MEDIA_FMT_CHANGE ) ? BW_VOTE_MEDIA_FMT_CHANGE : me->topologyBandwidth; // 20 MBPS BW Voting during media format update

	   // Bypass the flag temporarily to ensure that BW vote is raised here
	   me->is_processing_set_media_type = FALSE;
	   // Vote for MIPS and BW needed for media format reinit sequence
	   (void)AudPP_ProcessKppsBw(me, FALSE, TRUE);
	   me->is_processing_set_media_type = TRUE;

	   //Restore KPPS and BW to previous values
	   me->topologyKpps	= prev_vote_kpps;
	   me->topologyBandwidth = prev_vote_bw;

       topo_change_input_media_type(me->pTopologyObj,
            &inputFormat);
   }

   if (ADSP_SUCCEEDED(result))
   {
      bool_t deinterleavingNeeded = pInputMediaFormat->isInterleaved && !isCompressedFormat(pInputMediaFormat);
      result = AudPP_SetupFormatConv(&me->formatConverter,
            pInputMediaFormat->channels,
            pInputMediaFormat->channels, // The format converter cannot change channels, this is done in the topology.
            deinterleavingNeeded,
            pInputMediaFormat->samplingRate);
      if (ADSP_EOK != result)
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Failed to set up the format converter.\n", me->objId);
         AudPP_GotoFatalState(me);
      }
   }

   if (ADSP_SUCCEEDED(result))
   {
      // All went well, so commit the media format.
      me->inputMediaFormatInfo = *pInputMediaFormat;
   }

   me->is_processing_set_media_type = FALSE;

   if (me->is_media_format_change_pending)
   {
      AudPP_UpdateOutputMediaFormat(me, &me->new_media_format);
   }

   AudPP_PrintMediaFormat(me, &me->outputMediaFormatInfo, FALSE);

   AudPP_VoteBw(me);

   return result;
}

ADSPResult AudPP_setInputMediaInfo(ThisAudDynaPPSvc_t* me, const AdspAudioMediaFormatInfo_t *pInputMediaFormat)
{
   AdspAudioMediaFormatInfo_t newOutputMediaFormat = *pInputMediaFormat;

   if (!isCompressedFormat(pInputMediaFormat))
   {
      newOutputMediaFormat.isInterleaved = FALSE; // PP will always deinterleave PCM data
      if (!me->ppCfgInfo.outputParams.keepInputSamplingRate)
      {
         newOutputMediaFormat.samplingRate = me->ppCfgInfo.outputParams.outputSamplingRate;
      }
      if (!me->ppCfgInfo.outputParams.useNativeNumChannels)
      {
         newOutputMediaFormat.channels = me->ppCfgInfo.outputParams.outputNumChannels;
         memscpy (newOutputMediaFormat.channelMapping,sizeof(newOutputMediaFormat.channelMapping), me->ppCfgInfo.outputParams.outChannelMap, newOutputMediaFormat.channels * sizeof(me->ppCfgInfo.outputParams.outChannelMap[0]));
      }
      if (!me->ppCfgInfo.outputParams.useNativeBitsPerSample)
      {
         newOutputMediaFormat.bitsPerSample = me->ppCfgInfo.outputParams.outputBitsPerSample;
      }
   }

   ADSPResult result;
   result = AudPP_SetMediaFormat(me, pInputMediaFormat, &newOutputMediaFormat);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Error in setting input media format.", me->objId);
      return result;
   }

   return ADSP_EOK;
}

/**
 * Function to set the output frequency and number of channels.
 *   - Dependencies: Must not be called when the data path is
 *     active.
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in, out] me
 *   Pointer to the instance structure.
 * @param[in] pParams
 *   The parameters for setting the output media format.
 *
 * @return ADSPResult
 *   Error code indicating status
 */
ADSPResult AudPP_setOutputMediaInfo(ThisAudDynaPPSvc_t* me, const AudPPSvcOutputMediaTypeParams_t *pParams)
{

   MSG_7( MSG_SSID_QDSP6, DBG_HIGH_PRIO,"P%hX audproc_svc: Set output media Begin: keepInputFreq: %d, outputFreq: %lu, useNativeNumChannels: %d, outputChannels: %lu, useNativeBitsPerSample: %d, outputBitsPerSample: %lu", me->objId, pParams->keepInputSamplingRate, pParams->outputSamplingRate, pParams->useNativeNumChannels, pParams->outputNumChannels, pParams->useNativeBitsPerSample, pParams->outputBitsPerSample);

   if (me->mediaFormatReceived)
   {
      AdspAudioMediaFormatInfo_t newOutputMediaFormat = me->inputMediaFormatInfo;
      newOutputMediaFormat.isInterleaved = FALSE; // PP will always deinterleave data
      if (!pParams->keepInputSamplingRate)
      {
         newOutputMediaFormat.samplingRate = pParams->outputSamplingRate;
      }
      if (!pParams->useNativeNumChannels)
      {
         newOutputMediaFormat.channels = pParams->outputNumChannels;
         memscpy (newOutputMediaFormat.channelMapping,sizeof(newOutputMediaFormat.channelMapping), pParams->outChannelMap, pParams->outputNumChannels*sizeof(pParams->outChannelMap[0]));
      }
      if (!pParams->useNativeBitsPerSample)
      {
         newOutputMediaFormat.bitsPerSample = pParams->outputBitsPerSample;
      }

      ADSPResult result;
      result = AudPP_SetMediaFormat(me, &me->inputMediaFormatInfo, &newOutputMediaFormat);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "P%hX audproc_svc: Error in setting output media format.", me->objId);
         return result;
      }

      // Sanity checks
      QURT_ELITE_ASSERT(pParams->keepInputSamplingRate ?
            (me->inputMediaFormatInfo.samplingRate == me->outputMediaFormatInfo.samplingRate) :
            (pParams->outputSamplingRate == me->outputMediaFormatInfo.samplingRate));
      QURT_ELITE_ASSERT(pParams->useNativeNumChannels ?
            (me->inputMediaFormatInfo.channels == me->outputMediaFormatInfo.channels) :
            (pParams->outputNumChannels == me->outputMediaFormatInfo.channels));
      QURT_ELITE_ASSERT(pParams->useNativeBitsPerSample ?
            (me->inputMediaFormatInfo.bitsPerSample == me->outputMediaFormatInfo.bitsPerSample) :
            (pParams->outputBitsPerSample == me->outputMediaFormatInfo.bitsPerSample));
   }

   me->ppCfgInfo.outputParams = *pParams;

   MSG( MSG_SSID_QDSP6, DBG_HIGH_PRIO,"audproc_svc: Setting output media End");

   return ADSP_EOK;
}

static void AudPP_PrintMediaFormat(const ThisAudDynaPPSvc_t* me, const AdspAudioMediaFormatInfo_t *pMediaFormat, bool_t is_input)
{
   if(is_input)
   {
      MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX: Input MediaFmt: Number of channels: %lu, Sampling rate: %lu ,Bits per sample: %lu, "
            "Is Interleaved: %lu, Is Signed: %lu, Bitstream format: %lu ",
            me->objId, pMediaFormat->channels,pMediaFormat->samplingRate,pMediaFormat->bitsPerSample,
            pMediaFormat->isInterleaved, pMediaFormat->isSigned, pMediaFormat->bitstreamFormat );
      MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX: Input MediaFmt: Channel mapping for channels 1 to 6: %d, %d, %d, %d, %d, %d,", me->objId,
            pMediaFormat->channelMapping[0], pMediaFormat->channelMapping[1], pMediaFormat->channelMapping[2],
            pMediaFormat->channelMapping[3], pMediaFormat->channelMapping[4], pMediaFormat->channelMapping[5]);
      if(pMediaFormat->channels > 6)
      {
         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX: Input MediaFmt: Channel mapping for channels 7 and 8 :: %d, %d,", me->objId,
               pMediaFormat->channelMapping[6], pMediaFormat->channelMapping[7]);
      }
   }
   else
   {
      MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX: Output MediaFmt: Number of channels: %lu, Sampling rate: %lu ,Bits per sample: %lu,"
            "Is Interleaved: %lu, Is Signed: %lu, Bitstream format: %lu ",
            me->objId, pMediaFormat->channels,pMediaFormat->samplingRate,pMediaFormat->bitsPerSample,
            pMediaFormat->isInterleaved, pMediaFormat->isSigned, pMediaFormat->bitstreamFormat );
      MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX: Output MediaFmt: Channel mapping for channels 1 to 6: %d, %d, %d, %d, %d, %d,", me->objId,
            pMediaFormat->channelMapping[0], pMediaFormat->channelMapping[1], pMediaFormat->channelMapping[2],
            pMediaFormat->channelMapping[3], pMediaFormat->channelMapping[4], pMediaFormat->channelMapping[5]);
      if(pMediaFormat->channels > 6)
      {
         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX: Output MediaFmt: Channel mapping for channels 7 and 8: %d, %d,", me->objId,
               pMediaFormat->channelMapping[6], pMediaFormat->channelMapping[7]);
      }
   }
}

/**
 * Compares two structures of type AdspAudioMediaFormatInfo_t
 * for equality, by checking the value of each member.
 *   - Dependencies: None
 *   - Side Effects: None
 *   - Re-entrant: Yes
 *
 * @param[in] pMF1
 *  Pointer to the first structure.
 * @param[in] pMF2
 *  Pointer to the second structure.
 *
 * @return bool_t
 *  TRUE if all the members of pMF1 are equal to those of pMF2.
 *  FALSE otherwise.
 */
bool_t AudPP_MediaFormatsEqual(const AdspAudioMediaFormatInfo_t *pMF1, const AdspAudioMediaFormatInfo_t *pMF2)
{
   bool_t returnVal = TRUE;

   returnVal = returnVal && (pMF1->channels == pMF2->channels);
   returnVal = returnVal && (pMF1->bitsPerSample == pMF2->bitsPerSample);
   returnVal = returnVal && (pMF1->samplingRate == pMF2->samplingRate);
   returnVal = returnVal && (pMF1->isSigned == pMF2->isSigned);
   returnVal = returnVal && (pMF1->isInterleaved == pMF2->isInterleaved);
   returnVal = returnVal && (pMF1->bitstreamFormat == pMF2->bitstreamFormat);
   for (uint32_t i = 0; i < pMF1->channels; i++)
   {
      returnVal = returnVal && (pMF1->channelMapping[i] == pMF2->channelMapping[i]);
   }

   return returnVal;
}

static ADSPResult AudPP_sendMediaTypeSetParams(ThisAudDynaPPSvc_t* me, const AdspAudioMediaFormatInfo_t *pOutputMediaFormat)
{
   ADSPResult result = ADSP_EOK;
   ADSPResult temp_result = ADSP_EOK;

   if (isDTSTopology(me))
   {
      temp_result |= ADSP_EOK;
   }

   if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_MFC))
   {
      temp_result |= topo_set_output_media_format (me->pTopologyObj, AUDPROC_MODULE_ID_MFC, pOutputMediaFormat);
   }

   if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_CHMIXER))
   {
      temp_result |= topo_set_output_media_format (me->pTopologyObj, AUDPROC_MODULE_ID_CHMIXER, pOutputMediaFormat);
   }

    if (topo_is_module_present(me->pTopologyObj, AUDPROCTST_MODULE_ID_BYTESHIFTER_CAPIV2))
    {
      temp_result |= topo_set_output_media_format (me->pTopologyObj, AUDPROCTST_MODULE_ID_BYTESHIFTER_CAPIV2, pOutputMediaFormat);
    }

   if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_DS1AP))
   {
      temp_result |= topo_set_output_media_format (me->pTopologyObj, AUDPROC_MODULE_ID_DS1AP, pOutputMediaFormat);
   }

   if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_DS2AP))
   {
      struct ds2ap_output_media_t
      {
         asm_stream_param_data_v2_t header;
         audproc_param_id_ds2ap_output_media_fmt_t ds2ap_out_format;
      } payload;
      payload.header.module_id   = AUDPROC_MODULE_ID_DS2AP;
      payload.header.param_id    = AUDPROC_PARAM_ID_DS2AP_OUTPUT_MEDIA_FMT;
      payload.header.param_size  = sizeof(payload.ds2ap_out_format);
      payload.header.reserved    = 0;
      payload.ds2ap_out_format.sample_rate = pOutputMediaFormat->samplingRate;
      payload.ds2ap_out_format.num_channels = pOutputMediaFormat->channels;
      memscpy(payload.ds2ap_out_format.channel_type,
              sizeof(payload.ds2ap_out_format.channel_type),
              pOutputMediaFormat->channelMapping,
              8 * sizeof(uint8_t));
      temp_result |= topo_set_param(me->pTopologyObj, &payload, sizeof(payload));
   }

   if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_RESAMPLER))
   {
      // Set the output sample rate for the Dynamic resampler module.
      (void) topo_set_output_sample_rate (me->pTopologyObj,
            AUDPROC_MODULE_ID_RESAMPLER,
            pOutputMediaFormat->samplingRate );
      // No need to check for return type. The resampler may not be present, so we can ignore this.
   }

   if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_DOWNMIX))
   {
      struct audproc_downmix_output_media_t
      {
         asm_stream_param_data_v2_t header;
         uint32_t out_ch;
         uint8_t  channel_map[8];
      } payload;

      payload.header.module_id    = AUDPROC_MODULE_ID_DOWNMIX;
      payload.header.param_id     = AUDPROC_PARAM_ID_DOWNMIX_OUT_CHANNELS;
      payload.header.param_size   = sizeof(payload.out_ch) + 8*sizeof(payload.channel_map[0]);
      payload.header.reserved     = 0;
      payload.out_ch = pOutputMediaFormat->channels;
      memscpy(payload.channel_map,
              sizeof(payload.channel_map),
              pOutputMediaFormat->channelMapping,
              payload.out_ch * sizeof(payload.channel_map[0]));

      // No need to check for return type. The downmix module may not be present, so we can ignore this.
      (void) topo_set_param(me->pTopologyObj, &payload, sizeof(payload));
   }
   if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_VIRTUALIZER))
   {
      bool_t set_default_virt_out = ((me->ppCfgInfo.outputParams.useNativeNumChannels) &&
            (me->ppCfgInfo.dynaPPSvcType == DYNA_SVC_PP_TYPE_POPP));
      AdspAudioMediaFormatInfo_t VirtualizerMediaFormat = *pOutputMediaFormat;

      if(set_default_virt_out)
      {
         VirtualizerMediaFormat.channels = STEREO;

         for (uint8_t ch = 0; ch < 8 ; ch++)
         {
            VirtualizerMediaFormat.channelMapping[ch] = 0;
         }
         VirtualizerMediaFormat.channelMapping[0] = PCM_CHANNEL_L;
         VirtualizerMediaFormat.channelMapping[1] = PCM_CHANNEL_R;
      }

      (void)topo_set_output_media_format (me->pTopologyObj, AUDPROC_MODULE_ID_VIRTUALIZER, &VirtualizerMediaFormat);

      if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_CHMIXER) && set_default_virt_out )
      {
         struct chmixer_ch_cfg_t
         {
            asm_stream_param_data_v2_t header;
            capi_v2_chmixer_enable_payload_t chmixer_enable;
         } payload;

         payload.header.module_id      = AUDPROC_MODULE_ID_CHMIXER;
         payload.header.param_id       = CAPI_V2_CHANNELMIXER_PARAM_ID_ENABLE;
         payload.header.param_size     = sizeof(payload.chmixer_enable);
         payload.header.reserved       = 0;
         payload.chmixer_enable.enable = 0;

         (void)topo_set_param(me->pTopologyObj, &payload, sizeof(payload));
      }
    }
    if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_PBE))
    {
    	bool_t set_chmixer_enable = ((me->ppCfgInfo.outputParams.useNativeNumChannels==TRUE) &&
    			(me->ppCfgInfo.dynaPPSvcType == DYNA_SVC_PP_TYPE_POPP));

    	if(set_chmixer_enable)
    	{
    		struct pbe_chmixer_enable_cfg_t
    		{
    			asm_stream_param_data_v2_t header;
    			capi_v2_pbe_chmixer_enable_t chmixer_enable;
    		} payload;

    		payload.header.module_id      = AUDPROC_MODULE_ID_PBE;
    		payload.header.param_id       = CAPI_V2_PBE_CHMIXER_PARAM_ID_ENABLE;
    		payload.header.param_size     = sizeof(payload.chmixer_enable);
    		payload.header.reserved       = 0;
    		payload.chmixer_enable.client_enable = 0;

    		(void)topo_set_param(me->pTopologyObj, &payload, sizeof(payload));
    	}
    }
    if (topo_is_module_present(me->pTopologyObj, AUDPROC_MODULE_ID_AUDIOSPHERE))
    {
      temp_result |= topo_set_output_media_format (me->pTopologyObj, AUDPROC_MODULE_ID_AUDIOSPHERE, pOutputMediaFormat);
    }

   if (ADSP_EOK != temp_result)
   {
      result = ADSP_EFAILED;
   }

   return result;
}

static ThisAudDynaPPSvc_t *AudPP_topo_cb_to_svc(topo_event_client_t *cb_obj_ptr)
{
   uint8_t *cb_obj_mem_ptr = reinterpret_cast<uint8_t*>(cb_obj_ptr);
   uint8_t *pp_obj_mem_ptr = cb_obj_mem_ptr - offsetof(ThisAudDynaPPSvc_t, topo_event_client);
   return reinterpret_cast<ThisAudDynaPPSvc_t*>(pp_obj_mem_ptr);
}

static void AudPP_topo_KPPS_event(topo_event_client_t *obj_ptr, uint32_t new_KPPS)
{
   ThisAudDynaPPSvc_t *me = AudPP_topo_cb_to_svc(obj_ptr);

   if (me->topologyKpps != new_KPPS)
   {
      me->topologyKpps = new_KPPS;
   }

   if (!qurt_elite_adsppm_wrapper_is_registered(me->ulAdsppmClientId))
   {
      return;
   }
   new_KPPS = AudPP_AggregateKpps(me);

   if (me->prev_kpps_vote !=  new_KPPS)
   {
      me->prev_kpps_vote =  new_KPPS;
#if (ADSPPM_INTEGRATION==1)
      ADSPResult result=ADSP_EOK;
      static const uint8_t NUM_REQUEST=1;
      MmpmRscParamType rscParam[NUM_REQUEST];
      MMPM_STATUS      retStats[NUM_REQUEST];
      MmpmRscExtParamType reqParam;
      uint8_t req_num=0;
      MmpmMppsReqType mmpmMppsParam;

      reqParam.apiType                    = MMPM_API_TYPE_SYNC;
      reqParam.pExt                       = NULL;       //for future
      reqParam.pReqArray                  = rscParam;
      reqParam.pStsArray                  = retStats;   //for most cases mmpmRes is good enough, need not check this array.
      reqParam.reqTag                     = 0;          //for async only

      uint32_t mpps = (new_KPPS > 0)   ? (new_KPPS+1000)/1000   : 0; //round up if not zero

      //Requesting 0 will be equivalent to releasing particular resource
      mmpmMppsParam.mppsTotal                  = mpps;
      mmpmMppsParam.adspFloorClock             = 0;
      rscParam[req_num].rscId                   = MMPM_RSC_ID_MPPS;
      rscParam[req_num].rscParam.pMppsReq       = &mmpmMppsParam;

      req_num++;

      reqParam.numOfReq                   = req_num;

      if (new_KPPS == 0)
      {
         result = qurt_elite_adsppm_wrapper_release(me->ulAdsppmClientId, &me->adsppmClientPtr, &reqParam);
         MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM release MPPS by P%hX  (%lu). Result %lu", me->objId, me->ulAdsppmClientId, result);
      }
      else
      {
         result = qurt_elite_adsppm_wrapper_request(me->ulAdsppmClientId, &me->adsppmClientPtr, &reqParam);
         MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "ADSPPM request MPPS %lu by P%hX (%lu). Result %lu",  mpps, me->objId, me->ulAdsppmClientId,result);
      }
#endif // #if (ADSPPM_INTEGRATION==1)
   }
}

static void AudPP_topo_bandwidth_event(topo_event_client_t *obj_ptr, uint32_t new_bandwidth)
{
   ThisAudDynaPPSvc_t *me = AudPP_topo_cb_to_svc(obj_ptr);

   me->topologyBandwidth = new_bandwidth;

   AudPP_VoteBw(me);
}

static void AudPP_topo_output_media_format_event(topo_event_client_t *obj_ptr, const AdspAudioMediaFormatInfo_t *new_format)
{
   ThisAudDynaPPSvc_t *me = AudPP_topo_cb_to_svc(obj_ptr);

   AudPP_UpdateOutputMediaFormat(me, new_format);
}

static void AudPP_topo_algorithmic_delay_event(topo_event_client_t *obj_ptr, uint32_t new_delay)
{
   ThisAudDynaPPSvc_t *me = AudPP_topo_cb_to_svc(obj_ptr);

   if (NULL != me->pAlgorithmicDelay)
   {
      *me->pAlgorithmicDelay = new_delay;
   }
}

void AudPP_UpdateOutputMediaFormat(ThisAudDynaPPSvc_t *me, const AdspAudioMediaFormatInfo_t *new_format)
{
   if (me->is_processing_data || me->is_processing_set_media_type)
   {
      me->is_media_format_change_pending = TRUE;
      me->new_media_format = *new_format;
      return;
   }

   AdspAudioMediaFormatInfo_t oldOutputMediaFormat = me->outputMediaFormatInfo;
   me->outputMediaFormatInfo = *new_format;

   me->is_media_format_change_pending = FALSE;

   // need to check whether currently there's data being processed
   // in output buffer.  If yes, need to deliver that buffer first
   // before sending media format setup command.
   if (me->audPPStatus.pOutDataMsgDataBuf != NULL)
   {
      // currently there's buffer being processed.
      // From program flow, this can ONLY be PCM buffer.
      // deliver current buffer
      MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "P%hX audproc_svc: MF Delivering out %p ", me->objId, me->audPPStatus.pOutDataMsgDataBuf);
      AudPP_deliverCurrentOutputBuffer(me, oldOutputMediaFormat.channels);
   }  // if (pOutDataMsgDataBuf !=NULL)

   // enqueue the input Media Type Msg to downstream bufQ.
   ADSPResult result = AudPP_sendMediaTypeMsg(me);
   if (ADSP_FAILED(result))
   {
      MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Couldn't send media type downstream!", me->objId);
   }

   // Allocate bigger output buffers if needed for the PCM use case. The buffers for the compressed use case
   // are allocated based on the input size.
   if (!isCompressedFormat(&me->outputMediaFormatInfo))
   {
      result = AudPP_CheckAndAdjustPCMBufSize(me);
      if (ADSP_FAILED(result))
      {
         MSG_1(MSG_SSID_QDSP6, DBG_FATAL_PRIO, "P%hX audproc_svc: Couldn't allocate output buffers!", me->objId);
      }
   }
   AudPP_VoteBw(me);
}
