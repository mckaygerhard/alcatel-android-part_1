/** @file AudioEncSvc_CapiV1Util.cpp
This file contains functions for Elite Encoder service.

Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/aud/services/dynamic_svcs/audio_enc_svc/src/AudioEncSvc_CapiV1Util.cpp#9 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
02/04/14    rbhatnk      Created file.

========================================================================== */

#include "qurt_elite.h"
#include "Elite.h"

#include "AudioEncSvc_CapiV1Wrapper.h"
#include "AudioEncSvc_CapiV1Util.h"

#include "CVocoderCapiLib.h"
#include "CSbcEncoderLib.h"
#include "CADPCMEncoderLib.h"
#include "CWmaEncoderLib.h"
#include "ComboDTSEncPacketizerlib.h"
#include "CDTSEncoderLib.h"
#include "CMp3EncoderLib.h"
#include "CDepacketizerLib.h"
#include "CeAc3PacketizerLib.h"
#include "CAc3PacketizerLib.h"
#include "CDtshdPacketizerLib.h"
#include "CDolbyPlusEncoderLib.h"
#include "CMatPacketizerLib.h"

#include "Elite_CAPI.h"
#include "audio_basic_op.h"
#include <audio_basic_op_ext.h>
#include "adsp_asm_api.h"

//secret hack: ideally the service shouldn't be aware of capi v1. however, for create we need to know.
//in order to hide details from the service structs, this layer is written.
//wrapper cannot do this because wrapper only complies to CAPI V2 APIs
#include "AudioEncSvc_CapiV1WrapperPrivate.h"

static void init_media_fmt(capi_v2_standard_data_format_t *std_media_fmt)
{
   std_media_fmt->bits_per_sample = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->bitstream_format = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->data_interleaving = CAPI_V2_INVALID_INTERLEAVING;
   std_media_fmt->data_is_signed = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->num_channels = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->q_factor = CAPI_V2_DATA_FORMAT_INVALID_VAL;
   std_media_fmt->sampling_rate = CAPI_V2_DATA_FORMAT_INVALID_VAL;

   for (uint32_t j=0; (j<CAPI_V2_MAX_CHANNELS); j++)
   {
      std_media_fmt->channel_type[j] = (uint16_t)CAPI_V2_DATA_FORMAT_INVALID_VAL;
   }
}

static ADSPResult validate_input(enc_CAPI_init_params_t *pCapiInitParams)
{
   return ADSP_EOK;
}

static ADSPResult allocate_memory(capi_v2_t **capi_v2_ptr_ptr)
{
   ADSPResult result = ADSP_EOK;
   *capi_v2_ptr_ptr = NULL;

   //Allocate memory for the wrapper
   capi_v2_proplist_t props_list;
   capi_v2_prop_t prop[1];

   capi_v2_init_memory_requirement_t init_mem;
   uint32_t i=0;
   prop[i].id = CAPI_V2_INIT_MEMORY_REQUIREMENT;
   prop[i].payload.actual_data_len = 0;
   prop[i].payload.max_data_len = sizeof(capi_v2_init_memory_requirement_t);
   prop[i].payload.data_ptr = (int8_t*)&init_mem;
   prop[i].port_info.is_valid = false;
   i++;

   props_list.prop_ptr = prop;
   props_list.props_num = i;

   result = audio_enc_svc_capi_v1_wrapper_get_static_properties(NULL, &props_list); //only init_mem can be queried for CAPI v1 wrapper as other things need instance
   if (ADSP_FAILED(result))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioEncSvc: Get Static Properties error");
      return result;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"AudioEncSvc: Get Static Properties done with init_mem_req=%lu", init_mem.size_in_bytes);

   capi_v2_t *capi_v2_ptr = (capi_v2_t*)qurt_elite_memory_malloc(init_mem.size_in_bytes, QURT_ELITE_HEAP_DEFAULT);
   if (!capi_v2_ptr)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioEncSvc: Insufficient memory to allocate to CapiV2 Module.It requires %lu bytes",init_mem.size_in_bytes);
      return ADSP_ENOMEMORY;
   }
   memset(capi_v2_ptr, 0, init_mem.size_in_bytes);

   MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "AudioEncSvc: Module allocated for %lu bytes of memory at location 0x%p.",
         init_mem.size_in_bytes, capi_v2_ptr);

   *capi_v2_ptr_ptr = capi_v2_ptr;

   return result;
}

static ADSPResult audio_enc_svc_destroy_capi_v1_amdb(capi_v2_t **capi_v2_ptr_ptr)
{
   if (NULL == *capi_v2_ptr_ptr) return ADSP_EFAILED;

   //only util layer knows about underlying capi wrapper struct.
   audio_enc_svc_capi_v1_wrapper_t *wrapper = (audio_enc_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   if (wrapper->capi_v1_ptr)
   {
      wrapper->capi_v1_ptr->~ICAPI();
      wrapper->capi_v1_ptr = NULL;
   }
   if (wrapper->amdb_capi_ptr)
   {
      adsp_amdb_module_handle_info_t module_handle_info;
      module_handle_info.interface_type = CAPI;
      module_handle_info.type = 0; // Ignored
      module_handle_info.id1 = 0; // Ignored
      module_handle_info.id2 = 0; // Ignored
      module_handle_info.h.capi_handle = wrapper->amdb_capi_ptr;
      module_handle_info.result = ADSP_EOK;
      adsp_amdb_release_handles(&module_handle_info, 1);
      wrapper->amdb_capi_ptr = NULL;
   }

   if (*capi_v2_ptr_ptr)
   {
      qurt_elite_memory_free (*capi_v2_ptr_ptr);
      *capi_v2_ptr_ptr = NULL;
   }

   return ADSP_EOK;
}

static ADSPResult audio_enc_svc_destroy_capi_v1(capi_v2_t **capi_v2_ptr_ptr)
{
   if (NULL == *capi_v2_ptr_ptr) return ADSP_EFAILED;

   //only util layer knows about underlying capi wrapper struct.
   audio_enc_svc_capi_v1_wrapper_t *wrapper = (audio_enc_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   if (wrapper->capi_v1_ptr)
   {
      qurt_elite_memory_delete(wrapper->capi_v1_ptr,ICAPI);
   }

   if (*capi_v2_ptr_ptr)
   {
      qurt_elite_memory_free (*capi_v2_ptr_ptr);
      *capi_v2_ptr_ptr = NULL;
   }

   return ADSP_EOK;
}

static ADSPResult audio_enc_svc_destroy_capi_v1_voice(capi_v2_t **capi_v2_ptr_ptr)
{
   if (NULL == *capi_v2_ptr_ptr) return ADSP_EFAILED;

   //only util layer knows about underlying capi wrapper struct.
   audio_enc_svc_capi_v1_wrapper_t *wrapper = (audio_enc_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   if (wrapper->capi_v1_ptr)
   {
      qurt_elite_memory_free(wrapper->capi_v1_ptr);
   }

   if (*capi_v2_ptr_ptr)
   {
      qurt_elite_memory_free (*capi_v2_ptr_ptr);
      *capi_v2_ptr_ptr = NULL;
   }

   return ADSP_EOK;
}

ADSPResult audio_enc_svc_create_init_auto_capi_v1(capi_v2_t **capi_v2_ptr_ptr, AudioEncSvcInitParams_t *pInitParams, enc_CAPI_init_params_t *pCapiInitParams)
{
   ADSPResult result = ADSP_EOK;

   result = validate_input(pCapiInitParams);
   if (ADSP_FAILED(result)) return result;

   result = allocate_memory(capi_v2_ptr_ptr);
   if (ADSP_FAILED(result)) return result;

   //only util layer knows about underlying capi wrapper struct.
   audio_enc_svc_capi_v1_wrapper_t *wrapper = (audio_enc_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   pCapiInitParams->enc_destroy_fn = audio_enc_svc_destroy_capi_v1;
   wrapper->in_data_fmt = CAPI_V2_FIXED_POINT;
   wrapper->out_data_fmt = CAPI_V2_RAW_COMPRESSED;

   // First check if we have any dynamic library with this format id.
   adsp_amdb_capi_t *pCapi = (adsp_amdb_capi_t*)pCapiInitParams->amdb_handle;
   if (pCapi)
   {
      result = adsp_amdb_capi_new_f(pCapi, pCapiInitParams->enc_cfg_id, pInitParams->bits_per_sample, &wrapper->capi_v1_ptr);
      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed in adsp_amdb_capi_new_f");
      }
      wrapper->amdb_capi_ptr = pCapi;
      pCapiInitParams->enc_destroy_fn = audio_enc_svc_destroy_capi_v1_amdb;
   }
   else
   {
      switch (pCapiInitParams->enc_cfg_id)
      {
      case ASM_MEDIA_FMT_G711_ALAW_FS:
      case ASM_MEDIA_FMT_G711_MLAW_FS:
      case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
      case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
      case ASM_MEDIA_FMT_AMRNB_FS:
      case ASM_MEDIA_FMT_AMRWB_FS:
      case ASM_MEDIA_FMT_EVRC_FS:
      case ASM_MEDIA_FMT_EVRCB_FS:
      case ASM_MEDIA_FMT_EVRCWB_FS:
      case ASM_MEDIA_FMT_V13K_FS:
      case ASM_MEDIA_FMT_FR_FS:
      {
         result = Voc_Create_Encoder_CAPI(&wrapper->capi_v1_ptr);

         if (result != ADSP_EOK)
         {
            MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "Failed to create voice encoder CAPI");
            return result; //return is necessary because of "For non-voice CAPI, arrive at this place if.." found below
         }
         pCapiInitParams->enc_destroy_fn = audio_enc_svc_destroy_capi_v1_voice;

         break;
      }
      case ASM_MEDIA_FMT_DTS:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CDTSEncoderLib, QURT_ELITE_HEAP_DEFAULT, result);
         break;
      }

      case ASM_MEDIA_FMT_AC3:
      case ASM_MEDIA_FMT_EAC3:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CDolbyplusEncoderLib, QURT_ELITE_HEAP_DEFAULT, pInitParams->bits_per_sample, result);
         if (NULL == wrapper->capi_v1_ptr)
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "DDP Encoder :encoder couldn't be created.");
            result = ADSP_ENOMEMORY;
         }
         else
         {
            result=  wrapper->capi_v1_ptr->SetParam(CDolbyplusEncoderLib::ddpEncMode,(pCapiInitParams->enc_cfg_id == ASM_MEDIA_FMT_AC3)?9:8);
            if(result != ENC_SUCCESS)
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "DDP Encoder :Unsupported Encoding mode ");
            }
         }
         break;
      }
      case ASM_MEDIA_FMT_SBC:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CSbcEncoderLib, QURT_ELITE_HEAP_DEFAULT, result);
         break;
      }
      case ASM_MEDIA_FMT_ADPCM:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CADPCMEncoderLib, QURT_ELITE_HEAP_DEFAULT, result);
         break;
      }
      case ASM_MEDIA_FMT_WMA_V8:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CWmaEncoderLib, QURT_ELITE_HEAP_DEFAULT, result);
         break;
      }
      case ASM_MEDIA_FMT_MP3:
      {
         qurt_elite_memory_new(wrapper->capi_v1_ptr, CMp3EncoderLib, QURT_ELITE_HEAP_DEFAULT, result);
         break;
      }
      default:
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "AudioEncSvc unsupported encoding format!\n");
         result = ADSP_EBADPARAM;
      }
      } /* switch(pCapiInitParams->enc_cfg_id) */
   }

   if (NULL == wrapper->capi_v1_ptr)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "No memory to create Encoder CAPI instance");
      result = ADSP_ENOMEMORY;
   }

   if (ADSP_EOK != result)
   {
      //clean-up
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot create Encoder CAPI %d", result);
      // Arrive at this place  if
      // in qurt_elite_memory_new(), the memoy allocation is successul but
      // the constructor fail. The constructuor is responsible for
      // cleaning up its own memory. Since construtor fail, shall not
      // call destrutor (which might access unitilize memory) via
      // qurt_elite_memory_delete. Use qurt_elite_memory_free instead.
      if (NULL != wrapper->capi_v1_ptr)
      {
         qurt_elite_memory_free(wrapper->capi_v1_ptr);
         wrapper->capi_v1_ptr = NULL;
      }
      result = ADSP_EFAILED;
   }

   if (ADSP_SUCCEEDED(result) && (NULL != wrapper->capi_v1_ptr))
   {
      wrapper->enc_cfg_id = pCapiInitParams->enc_cfg_id;

      capi_v2_proplist_t init_proplist;
      capi_v2_event_callback_info_t cb_info = pCapiInitParams->cb_info;
      capi_v2_prop_t props[2];
      props[0].id = CAPI_V2_EVENT_CALLBACK_INFO;
      props[0].payload.actual_data_len = props[0].payload.max_data_len = sizeof(cb_info);
      props[0].payload.data_ptr = reinterpret_cast<int8_t*>(&cb_info);
      props[0].port_info.is_valid = false;

      audio_enc_svc_capi_v2_media_fmt_t media_fmt;
      init_media_fmt(&media_fmt.std);

      media_fmt.main.format_header.data_format = CAPI_V2_FIXED_POINT;
      media_fmt.std.bits_per_sample = pInitParams->bits_per_sample;
      props[1].id = CAPI_V2_INPUT_MEDIA_FORMAT;
      props[1].payload.actual_data_len = props[1].payload.max_data_len = sizeof(media_fmt);
      props[1].payload.data_ptr = reinterpret_cast<int8_t*>(&media_fmt);
      props[1].port_info.is_valid = true;
      props[1].port_info.is_input_port = false;
      props[1].port_info.port_index = 0; //CAPI v1 only supports one port.

      init_proplist.props_num = 2;
      init_proplist.prop_ptr = props;

      result = audio_enc_svc_capi_v1_wrapper_init(*capi_v2_ptr_ptr, &init_proplist);

      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioEncSvc: Initialization error");
      }
   }

   if (ADSP_FAILED(result))
   {
      if (pCapiInitParams->enc_destroy_fn)
      {
         pCapiInitParams->enc_destroy_fn(capi_v2_ptr_ptr);
         pCapiInitParams->enc_destroy_fn = NULL;
      }
      else
      {
         if (*capi_v2_ptr_ptr)
         {
            qurt_elite_memory_free(*capi_v2_ptr_ptr);
            *capi_v2_ptr_ptr = NULL;
         }
      }
   }

   return result;
}

ADSPResult audio_enc_svc_create_init_depack_capi_v1(capi_v2_t **capi_v2_ptr_ptr, AudioEncSvcInitParams_t *pInitParams, enc_CAPI_init_params_t *pCapiInitParams)
{
   ADSPResult result = ADSP_EOK;

   result = validate_input(pCapiInitParams);
   if (ADSP_FAILED(result)) return result;

   result = allocate_memory(capi_v2_ptr_ptr);
   if (ADSP_FAILED(result)) return result;

   //only util layer knows about underlying capi wrapper struct.
   audio_enc_svc_capi_v1_wrapper_t *wrapper = (audio_enc_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   //doesn't depend on format id
   qurt_elite_memory_new(wrapper->capi_v1_ptr, CDepacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);

   if ( ADSP_FAILED(result) )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot create encoder CAPI %d", result);

      // For non-voice CAPI, arrive at this place only if
      // in qurt_elite_memory_new(), the memoy allocation is successul but
      // the constructor fail. The constructuor is responsible for
      // cleaning up its own memory. Since construtor fail, shall not
      // call destrutor (which might access unitilize memory) via
      // qurt_elite_memory_delete. Use qurt_elite_memory_free instead.
      if (NULL != wrapper->capi_v1_ptr)
      {
         qurt_elite_memory_free(wrapper->capi_v1_ptr);
         wrapper->capi_v1_ptr = NULL;
      }
      result = ADSP_EFAILED;
   }

   pCapiInitParams->enc_destroy_fn = audio_enc_svc_destroy_capi_v1;
   wrapper->in_data_fmt = CAPI_V2_IEC61937_PACKETIZED;
   wrapper->out_data_fmt = CAPI_V2_RAW_COMPRESSED;

   if (ADSP_SUCCEEDED(result) && (NULL != wrapper->capi_v1_ptr))
   {
      wrapper->enc_cfg_id = pCapiInitParams->enc_cfg_id;

      capi_v2_proplist_t init_proplist;
      capi_v2_event_callback_info_t cb_info = pCapiInitParams->cb_info;
      capi_v2_prop_t props[2];
      props[0].id = CAPI_V2_EVENT_CALLBACK_INFO;
      props[0].payload.actual_data_len = props[0].payload.max_data_len = sizeof(cb_info);
      props[0].payload.data_ptr = reinterpret_cast<int8_t*>(&cb_info);
      props[0].port_info.is_valid = false;

      //bits per sample not required for depacketizer

      init_proplist.props_num = 1;
      init_proplist.prop_ptr = props;

      result = audio_enc_svc_capi_v1_wrapper_init(*capi_v2_ptr_ptr, &init_proplist);

      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioEncSvc: Initialization error");
      }
   }

   if (ADSP_FAILED(result))
   {
      if (pCapiInitParams->enc_destroy_fn)
      {
         pCapiInitParams->enc_destroy_fn(capi_v2_ptr_ptr);
         pCapiInitParams->enc_destroy_fn = NULL;
      }
   }


   return result;
}


ADSPResult audio_enc_svc_create_init_pack_capi_v1(capi_v2_t **capi_v2_ptr_ptr, AudioEncSvcInitParams_t *pInitParams, enc_CAPI_init_params_t *pCapiInitParams)
{
   ADSPResult result = ADSP_EOK;

   result = validate_input(pCapiInitParams);
   if (ADSP_FAILED(result)) return result;

   result = allocate_memory(capi_v2_ptr_ptr);
   if (ADSP_FAILED(result)) return result;

   //only util layer knows about underlying capi wrapper struct.
   audio_enc_svc_capi_v1_wrapper_t *wrapper = (audio_enc_svc_capi_v1_wrapper_t*) (*capi_v2_ptr_ptr);

   pCapiInitParams->enc_destroy_fn = audio_enc_svc_destroy_capi_v1;
   wrapper->in_data_fmt = CAPI_V2_RAW_COMPRESSED;
   wrapper->out_data_fmt = CAPI_V2_IEC61937_PACKETIZED;

   //for compressed playback create packetizer instead of encoder
   switch(pCapiInitParams->enc_cfg_id)
   {
   case ASM_MEDIA_FMT_AC3:
      qurt_elite_memory_new(wrapper->capi_v1_ptr, CAc3PacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);
      break;
   case ASM_MEDIA_FMT_EAC3:
      qurt_elite_memory_new(wrapper->capi_v1_ptr, CeAc3PacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);
      break;
   case ASM_MEDIA_FMT_DTS:
      qurt_elite_memory_new(wrapper->capi_v1_ptr, CDtshdPacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);
      break;
   case ASM_MEDIA_FMT_MAT:
      qurt_elite_memory_new(wrapper->capi_v1_ptr, CMatPacketizerLib, QURT_ELITE_HEAP_DEFAULT, result);
      break;
   default:
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Unsupported media fmt 0x%x\n in compressed playback mode (packetization)",
            (int)pCapiInitParams->enc_cfg_id);
      result = ADSP_EBADPARAM;
   }

   if ( ADSP_FAILED(result) )
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Cannot create encoder CAPI %d", result);

      // For non-voice CAPI, arrive at this place only if
      // in qurt_elite_memory_new(), the memoy allocation is successul but
      // the constructor fail. The constructuor is responsible for
      // cleaning up its own memory. Since construtor fail, shall not
      // call destrutor (which might access unitilize memory) via
      // qurt_elite_memory_delete. Use qurt_elite_memory_free instead.
      if (NULL != wrapper->capi_v1_ptr)
      {
         qurt_elite_memory_free(wrapper->capi_v1_ptr);
         wrapper->capi_v1_ptr = NULL;
      }
      result = ADSP_EFAILED;
   }

   if (ADSP_SUCCEEDED(result) && (NULL != wrapper->capi_v1_ptr))
   {
      wrapper->enc_cfg_id = pCapiInitParams->enc_cfg_id;

      capi_v2_proplist_t init_proplist;
      capi_v2_event_callback_info_t cb_info = pCapiInitParams->cb_info;
      capi_v2_prop_t props[2];
      props[0].id = CAPI_V2_EVENT_CALLBACK_INFO;
      props[0].payload.actual_data_len = props[0].payload.max_data_len = sizeof(cb_info);
      props[0].payload.data_ptr = reinterpret_cast<int8_t*>(&cb_info);
      props[0].port_info.is_valid = false;

      //bits per sample need not be set on packetizer capi

      init_proplist.props_num = 1;
      init_proplist.prop_ptr = props;

      result = audio_enc_svc_capi_v1_wrapper_init(*capi_v2_ptr_ptr, &init_proplist);

      if (ADSP_FAILED(result))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioEncSvc: Initialization error");
      }
   }

   if (ADSP_FAILED(result))
   {
      if (pCapiInitParams->enc_destroy_fn)
      {
         pCapiInitParams->enc_destroy_fn(capi_v2_ptr_ptr);
         pCapiInitParams->enc_destroy_fn = NULL;
      }
   }


   return result;
}

/**
 * this is CAPI V1 util (instead of CAPI V2 util) because once we have only CAPI V2, we can remove this and have this code directly in create_init functions.
 */
enc_AMDB_presence audio_enc_svc_get_amdb_presence(Enc_CAPI_Type type, uint32_t id1, uint32_t id2, void **amdb_capi_handle_ptr)
{
   ADSPResult result = ADSP_EOK;
   enc_AMDB_presence ret = ENC_AMDB_PRESENCE_NOT_PRESENT;
   *amdb_capi_handle_ptr = NULL;

   uint32_t amdb_type;

   switch(type)
   {
   case ENC_CAPI_TYPE_AUTO:
      amdb_type = AMDB_MODULE_TYPE_ENCODER;
      break;
   case ENC_CAPI_TYPE_DEPACKETIZER: //depacketizer won't be in AMDB
      return ret;
   case ENC_CAPI_TYPE_PACKETIZER:
      amdb_type = AMDB_MODULE_TYPE_PACKETIZER;
      break;
   default:
      return ret;
   }

   //first check CAPI V2 (like PP)
   adsp_amdb_module_handle_info_t module_handle_info;
   module_handle_info.interface_type = CAPI_V2;
   module_handle_info.type = amdb_type;
   module_handle_info.id1 = id1;
   module_handle_info.id2 = id2;
   module_handle_info.h.capi_v2_handle = NULL;
   module_handle_info.result = ADSP_EFAILED;

   /*
    * Note: This call will block till all modules with 'preload = 0' are loaded by the AMDB. This loading
    * happens using a thread pool using threads of very low priority. This can cause the current thread
    * to be blocked because of a low priority thread. If this is not desired, a callback function
    * should be provided that can be used by the AMDB to signal when the modules are loaded. The current
    * thread can then handle other tasks in parallel.
    */
   adsp_amdb_get_modules_request(&module_handle_info, 1, NULL, NULL);

   result = module_handle_info.result;
   if (ADSP_SUCCEEDED(result))
   {
      switch(module_handle_info.interface_type)
      {
      case CAPI_V2:
         ret = ENC_AMDB_PRESENCE_PRESENT_AS_CAPI_V2;
         *amdb_capi_handle_ptr = (void*)module_handle_info.h.capi_v2_handle;
         break;
      case CAPI:
         if (ENC_CAPI_TYPE_AUTO == type)  // Only AUTO CAPIv1 modules are supported.
         {
            *amdb_capi_handle_ptr = (void*)module_handle_info.h.capi_handle;
            ret = ENC_AMDB_PRESENCE_PRESENT_AS_CAPI_V1;
         }
         else
         {
            adsp_amdb_release_handles(&module_handle_info, 1);
         }
         break;
      case STUB:
         {
            MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"AudioEncSvc: module present as stub.");
            adsp_amdb_release_handles(&module_handle_info, 1);
            ret = ENC_AMDB_PRESENCE_PRESENT_AS_STUB;
            break;
         }
      default:
         adsp_amdb_release_handles(&module_handle_info, 1);
         break;
      }
   }
   else
   {
      if (ENC_CAPI_TYPE_AUTO == type) //CAPI V1 is defined in AMDB only for auto type. By default, the type is set to decoder.
      {
         //then check CAPI V1
         module_handle_info.interface_type = CAPI;
         module_handle_info.type = AMDB_MODULE_TYPE_DECODER;
         module_handle_info.id1 = id1;
         module_handle_info.id2 = 0;
         module_handle_info.h.capi_handle = NULL;
         module_handle_info.result = ADSP_EFAILED;
         adsp_amdb_get_modules_request(&module_handle_info, 1, NULL, NULL);
         result = module_handle_info.result;

         if (ADSP_SUCCEEDED(result))
         {
            if (CAPI == module_handle_info.interface_type)
            {
               *amdb_capi_handle_ptr = (void*)module_handle_info.h.capi_handle;
               ret = ENC_AMDB_PRESENCE_PRESENT_AS_CAPI_V1;
            }
            else
            {
               adsp_amdb_release_handles(&module_handle_info, 1);
            }
         }
      }
   }
   return ret;
}
