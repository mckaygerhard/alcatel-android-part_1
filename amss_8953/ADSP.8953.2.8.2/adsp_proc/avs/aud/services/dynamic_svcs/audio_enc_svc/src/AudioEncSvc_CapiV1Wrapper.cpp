/** @file AudioEncSvc_CapiV1Wrapper.cpp
This file contains functions for Elite Encoder service.

Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/aud/services/dynamic_svcs/audio_enc_svc/src/AudioEncSvc_CapiV1Wrapper.cpp#8 $


when       who     what, where, why
--------   ---     -------------------------------------------------------
07/22/14    rbhatnk      Created file.

========================================================================== */

#include "AudioEncSvc_CapiV1Wrapper.h"
#include "AudioEncSvc_CapiV1WrapperPrivate.h"
#include "Elite_CAPI.h"
#include "adsp_media_fmt.h"

#include "CEtsiEaacPlusEncoderLib.h"
#include "CVocoderCapiLib.h"
#include "CSbcEncoderLib.h"
#include "CADPCMEncoderLib.h"
#include "CWmaEncoderLib.h"
#include "ComboDTSEncPacketizerlib.h"
#include "CDTSEncoderLib.h"
#include "CMp3EncoderLib.h"
#include "CDepacketizerLib.h"
#include "CeAc3PacketizerLib.h"
#include "CAc3PacketizerLib.h"
#include "CDtshdPacketizerLib.h"
#include "CDolbyPlusEncoderLib.h"

#include "adsp_asm_stream_commands.h"
#include "adsp_media_fmt.h"
#include "Elite_CAPI_V2_private_params.h"
#include "audio_basic_op_ext.h"

#define AUDIO_ENC_SVC_CAPI_V1_WRAPPER_STACK_SIZE 4096
#define AUDIO_ENC_SVC_CAPI_V1_WRAPPER_INPUT_SIZE 4096
// output buffer size. If encoder service needs to allocate out buffers by itself instead of
// using the client buffers, encoder service queries CAPI for output buffer size. Use this
// default size if the query to CAPI not succeed.
#define AUDIO_ENC_SVC_CAPI_V1_WRAPPER_OUTPUT_SIZE 4096

#define PCM_ENC_KPPS_FACTOR 6 // Assuming at max 6 buffer transfers
#define PCM_ENC_MIN_KPPS 4000 // Minimum PCM encoder KPPS vote

#define AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_KPPS 20000
#define AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_CODE_BW 0
#define AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_DATA_BW (2*1024*1024)

#define KCPS_WMA_V8        130000
#define KCPS_DDP_ENC       130000

#if 0
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_change_media_fmt(capi_v2_t* _pif,
      const capi_v2_data_format_t* in_format_ptr[],
      capi_v2_data_format_t* out_format_ptr[]);
#endif

static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_process(capi_v2_t* _pif,
      capi_v2_stream_data_t* input[],
      capi_v2_stream_data_t* output[]);
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_end(capi_v2_t* _pif);
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_set_param(capi_v2_t* _pif,
      uint32_t param_id,
      const capi_v2_port_info_t *port_info_ptr,
      capi_v2_buf_t *params_ptr);
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_get_param(capi_v2_t* _pif,
      uint32_t param_id,
      const capi_v2_port_info_t *port_info_ptr,
      capi_v2_buf_t *params_ptr);
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_set_properties(capi_v2_t* _pif,
      capi_v2_proplist_t *props_ptr);
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_get_properties(capi_v2_t* _pif,
      capi_v2_proplist_t *props_ptr);


static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_process_set_properties(audio_enc_svc_capi_v1_wrapper_t* me, capi_v2_proplist_t *props_ptr);
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_process_get_properties(audio_enc_svc_capi_v1_wrapper_t* me, capi_v2_proplist_t *props_ptr);

static capi_v2_vtbl_t vtbl =
{
      audio_enc_svc_capi_v1_wrapper_process,
      audio_enc_svc_capi_v1_wrapper_end,
      audio_enc_svc_capi_v1_wrapper_set_param,
      audio_enc_svc_capi_v1_wrapper_get_param,
      audio_enc_svc_capi_v1_wrapper_set_properties,
      audio_enc_svc_capi_v1_wrapper_get_properties
};

static int init_capi(audio_enc_svc_capi_v1_wrapper_t* me, CAPI_Buf_t *params);

static capi_v2_err_t conv_capi_v1_err_to_capi_v2_err(int res)
{
   switch (res)
   {
   case ENC_SUCCESS:
      return CAPI_V2_EOK;
   case ENC_NEED_MORE:
      return CAPI_V2_ENEEDMORE;
   case ENC_BADPARAM_FAILURE:
      return CAPI_V2_EBADPARAM;
   case ENC_BUFFERTOOSMALL_FAILURE:
      return CAPI_V2_EFAILED;
   case ENC_NOMEMORY_FAILURE:
      return CAPI_V2_ENOMEMORY;
   case ENC_NOTIMPL_FAILURE:
   case ENC_FAILURE:
   default:
   {
      switch(res)
      {
      case DEC_SUCCESS:
         return CAPI_V2_EOK;
      case DEC_NEED_MORE:
         return CAPI_V2_ENEEDMORE;
      case DEC_RECOVERY_SUCCESS:
         return CAPI_V2_EOK;
      case DEC_RECOVERY_FAILURE:
         return CAPI_V2_EFAILED;
      case DEC_BADPARAM_FAILURE:
         return CAPI_V2_EBADPARAM;
      case DEC_NOMEMORY_FAILURE:
         return CAPI_V2_ENOMEMORY;
      case DEC_NOTIMPL_FAILURE:
      case DEC_FAILURE:
      default:
         return CAPI_V2_EFAILED;
      }
   }
   }
}

static bool_t has_media_fmt_changed(audio_enc_svc_capi_v2_media_fmt_t *old_fmt, audio_enc_svc_capi_v2_media_fmt_t *new_fmt)
{
   if ((old_fmt->main.format_header.data_format != new_fmt->main.format_header.data_format))
   {
      return true;
   }

   if (  (old_fmt->main.format_header.data_format == CAPI_V2_FIXED_POINT) ||
         (old_fmt->main.format_header.data_format == CAPI_V2_IEC61937_PACKETIZED) )
   {
      if (  (old_fmt->std.bitstream_format != new_fmt->std.bitstream_format) ||
            (old_fmt->std.num_channels != new_fmt->std.num_channels) ||
            (old_fmt->std.bits_per_sample != new_fmt->std.bits_per_sample) ||
            (old_fmt->std.sampling_rate != new_fmt->std.sampling_rate) ||
            (old_fmt->std.data_is_signed != new_fmt->std.data_is_signed) ||
            (old_fmt->std.data_interleaving != new_fmt->std.data_interleaving) ||
            (old_fmt->std.q_factor != new_fmt->std.q_factor) )
      {
         return true;
      }

      for (uint32_t j=0; (j<new_fmt->std.num_channels) && (j<CAPI_V2_MAX_CHANNELS); j++)
      {
         if (old_fmt->std.channel_type[j] != new_fmt->std.channel_type[j])
         {
            return true;
         }
      }
   }
   else
   {
      if (   (old_fmt->raw_fmt.bitstream_format != new_fmt->raw_fmt.bitstream_format))
      {
         return true;
      }
      // CAPI outputs raw bit stream only for depacketizer and encoder mode.
   }

   return false;
}

static void init_media_fmt(audio_enc_svc_capi_v1_wrapper_t *me)
{
   audio_enc_svc_capi_v2_media_fmt_t *media_fmt = &(me->out_media_fmt[0]);

   media_fmt->main.format_header.data_format = me->out_data_fmt;
   switch(me->out_data_fmt)
   {
   case CAPI_V2_FIXED_POINT: //for PCM
   {
      media_fmt->std.bits_per_sample = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.bitstream_format = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.data_interleaving = CAPI_V2_DEINTERLEAVED_PACKED;
      media_fmt->std.data_is_signed = 1;
      media_fmt->std.num_channels = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.q_factor = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.sampling_rate = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      for (uint32_t j=0; (j<CAPI_V2_MAX_CHANNELS); j++)
      {
         media_fmt->std.channel_type[j] = (uint16_t)CAPI_V2_DATA_FORMAT_INVALID_VAL;
      }
   }
   break;
   case CAPI_V2_IEC61937_PACKETIZED: //for packetizer
   {
      media_fmt->std.bits_per_sample = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.bitstream_format = me->enc_cfg_id;
      media_fmt->std.data_interleaving = CAPI_V2_INVALID_INTERLEAVING;
      media_fmt->std.data_is_signed = 1;
      media_fmt->std.num_channels = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.q_factor = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      media_fmt->std.sampling_rate = CAPI_V2_DATA_FORMAT_INVALID_VAL;
      for (uint32_t j=0; (j<CAPI_V2_MAX_CHANNELS); j++)
      {
         media_fmt->std.channel_type[j] = (uint16_t)CAPI_V2_DATA_FORMAT_INVALID_VAL;
      }
   }
   break;
   case CAPI_V2_RAW_COMPRESSED: //for depacketizer and encoder
   {
      //in case of depacketizer, unless we depacketize we don't know the fmt.
      //init with enc_cfg_id so that for encoder case, it detects no change in fmt
      media_fmt->raw_fmt.bitstream_format = me->enc_cfg_id;
   }
   break;
   default:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Data format invalid %d", me->out_data_fmt);
      QURT_ELITE_ASSERT(0);
   }
   }
}

static void copy_media_fmt(audio_enc_svc_capi_v2_media_fmt_t *dest, const audio_enc_svc_capi_v2_media_fmt_t *src)
{
   memscpy(dest, sizeof(audio_enc_svc_capi_v2_media_fmt_t), src, sizeof(audio_enc_svc_capi_v2_media_fmt_t));
}

static capi_v2_err_t check_raise_kpps_event(audio_enc_svc_capi_v1_wrapper_t* me, uint32_t val)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   if (me->kpps != val)
   {
      capi_v2_event_KPPS_t evnt;
      capi_v2_event_info_t event_info;
      event_info.port_info.is_valid = false;

      evnt.KPPS = val;

      event_info.payload.actual_data_len = sizeof(capi_v2_event_KPPS_t);
      event_info.payload.data_ptr = (int8_t*)&evnt;
      event_info.payload.max_data_len = sizeof(capi_v2_event_KPPS_t);
      result = me->cb_info.event_cb(me->cb_info.event_context, CAPI_V2_EVENT_KPPS, &event_info );
      if (CAPI_V2_EOK != result)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Failed to raise event for KPPS change");
      }
      else
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper raised event for KPPS change %lu", val);
         me->kpps = val;
      }
   }

   return result;
}

static uint32_t determine_kpps(audio_enc_svc_capi_v1_wrapper_t* me)
{
   uint32_t kpps=AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_KPPS;
   switch( me->enc_cfg_id )
   {
   case ASM_MEDIA_FMT_G711_ALAW_FS:
   case ASM_MEDIA_FMT_G711_MLAW_FS:
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
   case ASM_MEDIA_FMT_AMRNB_FS:
   case ASM_MEDIA_FMT_AMRWB_FS:
   case ASM_MEDIA_FMT_EVRC_FS:
   case ASM_MEDIA_FMT_EVRCB_FS:
   case ASM_MEDIA_FMT_EVRCWB_FS:
   case ASM_MEDIA_FMT_V13K_FS:
   case ASM_MEDIA_FMT_FR_FS:
   {
      //TODO: no change yet
      break;
   }
   case ASM_MEDIA_FMT_DTS:
   {
      break;
   }
   case ASM_MEDIA_FMT_AC3:
   case ASM_MEDIA_FMT_EAC3:
   {
      break;
   }
   case ASM_MEDIA_FMT_SBC:
   {
      kpps=8000;
      break;
   }
   case ASM_MEDIA_FMT_ADPCM:
   {
      kpps=3000;
      break;
   }
   case ASM_MEDIA_FMT_WMA_V8:
   {
      break;
   }
   case ASM_MEDIA_FMT_MP3:
   {
      break;
   }
   default:
   {
      break;
   }
   }

   return kpps;
}


static uint32_t determine_data_bw(audio_enc_svc_capi_v1_wrapper_t* me)
{
   uint32_t bw=AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_DATA_BW;
   switch( me->enc_cfg_id )
   {
   case ASM_MEDIA_FMT_G711_ALAW_FS:
   case ASM_MEDIA_FMT_G711_MLAW_FS:
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
   case ASM_MEDIA_FMT_AMRNB_FS:
   case ASM_MEDIA_FMT_AMRWB_FS:
   case ASM_MEDIA_FMT_EVRC_FS:
   case ASM_MEDIA_FMT_EVRCB_FS:
   case ASM_MEDIA_FMT_EVRCWB_FS:
   case ASM_MEDIA_FMT_V13K_FS:
   case ASM_MEDIA_FMT_FR_FS:
   {
      //TODO: no change yet
      break;
   }
   case ASM_MEDIA_FMT_DTS:
   {
      break;
   }
   case ASM_MEDIA_FMT_AC3:
   case ASM_MEDIA_FMT_EAC3:
   {
      break;
   }
   case ASM_MEDIA_FMT_SBC:
   {
      break;
   }
   case ASM_MEDIA_FMT_ADPCM:
   {
      break;
   }
   case ASM_MEDIA_FMT_WMA_V8:
   {
      break;
   }
   case ASM_MEDIA_FMT_MP3:
   {
      break;
   }
   default:
   {
      break;
   }
   }

   return bw;
}

static capi_v2_err_t check_raise_bw_event(audio_enc_svc_capi_v1_wrapper_t* me, uint32_t code_bw, uint32_t data_bw)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   if ( (me->code_bw != code_bw) || (me->data_bw != data_bw) )
   {
      capi_v2_event_bandwidth_t evnt;
      capi_v2_event_info_t event_info;
      event_info.port_info.is_valid = false;

      evnt.code_bandwidth = code_bw;
      evnt.data_bandwidth = data_bw;

      event_info.payload.actual_data_len = sizeof(capi_v2_event_bandwidth_t);
      event_info.payload.data_ptr = (int8_t*)&evnt;
      event_info.payload.max_data_len = sizeof(capi_v2_event_bandwidth_t);
      result = me->cb_info.event_cb(me->cb_info.event_context, CAPI_V2_EVENT_BANDWIDTH, &event_info );
      if (CAPI_V2_EOK != result)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Failed to raise event for bandwidth change");
      }
      else
      {
         MSG_2(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper raised event for bw change %lu %lu", code_bw , data_bw);
         me->code_bw = code_bw;
         me->data_bw = data_bw;
      }
   }
   return result;
}

static capi_v2_err_t check_raise_output_media_fmt_updated_event(audio_enc_svc_capi_v1_wrapper_t* me, audio_enc_svc_capi_v2_media_fmt_t *new_fmt)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   if ( has_media_fmt_changed(&(me->out_media_fmt[0]), new_fmt ) )
   {
      copy_media_fmt(&(me->out_media_fmt[0]), new_fmt);

      capi_v2_event_info_t event_info;
      event_info.port_info.is_valid = true;
      event_info.port_info.is_input_port = false;
      event_info.port_info.port_index = 0;

      event_info.payload.actual_data_len = aud_enc_svc_media_fmt_size(new_fmt->main.format_header.data_format);
      event_info.payload.data_ptr = (int8_t*)&me->out_media_fmt[0];
      event_info.payload.max_data_len = event_info.payload.actual_data_len;
      result = me->cb_info.event_cb(me->cb_info.event_context, CAPI_V2_EVENT_OUTPUT_MEDIA_FORMAT_UPDATED, &event_info );
      if (CAPI_V2_EOK != result)
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Failed to raise event for out media fmt change");
      }
      else
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper raised event for out media fmt change");
      }
   }
   return result;
}

static void set_end_of_stream(audio_enc_svc_capi_v1_wrapper_t *me, bool_t endOfStreamFlag)
{
   switch(me->enc_cfg_id)
   {
   case ASM_MEDIA_FMT_WMA_V8:
      //WMA encoder need to know when end of stream has been
      //reached to process the last frames
      me->capi_v1_ptr->SetParam(CWmaEncoderLib::eWmaEndOfStream, 1);
      break;
   case ASM_MEDIA_FMT_EAC3:
   case ASM_MEDIA_FMT_AC3:
      //DDPencoder need to know when end of stream has been
      //reached to process the last frames
      me->capi_v1_ptr->SetParam(CDolbyplusEncoderLib::ddpEncEos, 1);
      break;
   case ASM_MEDIA_FMT_MP3:
      me->capi_v1_ptr->SetParam(CMp3EncoderLib::eMp3EndOfStream, 1);
      break;
   default:
      return;
   }
   return;
}

static void check_set_end_of_stream(audio_enc_svc_capi_v1_wrapper_t *me, bool_t endOfStreamFlag)
{
   //if EoS flag has not changed, then nothing to do
   if (endOfStreamFlag == me->prev_eos_flag)
   {
      return;
   }

   me->prev_eos_flag = endOfStreamFlag;

   return set_end_of_stream(me, endOfStreamFlag);
}

/**
 * reinits if already initialized
 */
static int init_capi(audio_enc_svc_capi_v1_wrapper_t* me, CAPI_Buf_t *params)
{
   int ret;
   if (me->capi_init_done)
   {
      ret = me->capi_v1_ptr->ReInit(params);
   }
   else
   {
      ret = me->capi_v1_ptr->Init(params);
   }

   me->prev_eos_flag = false;
   //initialize the EoF/EoS flags in the beginning, as some CAPIs do not initialize themselves.
   set_end_of_stream(me, false);

   if (ENC_SUCCESS == ret)
   {
      me->capi_init_done = true;
   }
   return conv_capi_v1_err_to_capi_v2_err(ret);
}

capi_v2_err_t set_bit_rate(audio_enc_svc_capi_v1_wrapper_t *me, capi_v2_buf_t *params_ptr)
{
   int res = ENC_SUCCESS;

   if (params_ptr->actual_data_len < sizeof(asm_bitrate_param_t))
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "SET_ENC_BITRATE param size not big enough!");
      return CAPI_V2_EBADPARAM;
   }
   asm_bitrate_param_t *spbitrate = (asm_bitrate_param_t*) params_ptr->data_ptr;
   uint32_t new_bitrate = spbitrate->bitrate;
   uint32_t current_bitrate = 0;

   switch (me->enc_cfg_id)
   {
   case ASM_MEDIA_FMT_SBC:
   {
      //place holder, need to add SBC support, now return unsupported
      res = ENC_BADPARAM_FAILURE;
   }
   break;
   case ASM_MEDIA_FMT_WMA_V8:
   {
      (void) me->capi_v1_ptr->GetParam(CWmaEncoderLib::eWmaBitRate, (int*) (&current_bitrate));

      if (new_bitrate != current_bitrate)
      {
         //WMA encoder setParam always return success, so ignore its return value
         (void) me->capi_v1_ptr->SetParam(CWmaEncoderLib::eWmaBitRate, new_bitrate);
         res = init_capi(me, NULL);
      }
   }
   break;
   default:
   {
      res = me->capi_v1_ptr->SetParam(eIcapiBitRate, new_bitrate);
      break;
   }
   }
   if (ENC_SUCCESS != res)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to set encoder bit rate param!");
      return CAPI_V2_EBADPARAM;
   }
   else
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Success setting encoder bit rate param!");
   }

   return conv_capi_v1_err_to_capi_v2_err(res);
}

capi_v2_err_t set_pcm_enc_cfg(audio_enc_svc_capi_v1_wrapper_t *me,
      asm_multi_channel_pcm_enc_cfg_v3_t* pPcmConfig)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   CAPI_Buf_t Params;                 // for send parameters to Init() function
   VoiceInitParamsType VInitParams;

   VInitParams.VocTypeID = me->enc_cfg_id;
   VInitParams.VocConfigBlk = (void *) pPcmConfig;

   Params.Data = (char*) &VInitParams;
   Params.nActualDataLen = sizeof(VoiceInitParamsType);
   Params.nMaxDataLen = sizeof(VoiceInitParamsType);

   VInitParams.VocConfigBlkSize = sizeof(asm_multi_channel_pcm_enc_cfg_v3_t);

   //set num channels
   (void) me->capi_v1_ptr->SetParam(eIcapiNumChannels, pPcmConfig->num_channels);
   //set sample rate
   (void) me->capi_v1_ptr->SetParam(eIcapiSamplingRate, pPcmConfig->sample_rate);
   //set bps
   (void) me->capi_v1_ptr->SetParam(eIcapiBitsPerSample, pPcmConfig->bits_per_sample);

   //set channel mapping
   CAPI_ChannelMap_t channel_map;
   memset(&channel_map, 0, sizeof(CAPI_ChannelMap_t));
   channel_map.nChannels = pPcmConfig->num_channels;
   if (0 != pPcmConfig->num_channels)
   {
      memscpy(&channel_map.nChannelMap, sizeof(pPcmConfig->channel_mapping), pPcmConfig->channel_mapping, sizeof(pPcmConfig->channel_mapping));
   }
   (void) me->capi_v1_ptr->SetParam(eIcapiOutputChanMap, (int) &channel_map);

   //0 sample rate or # channels means native mode, so hold off init
   //till we receive media format cmd from PP
   result = CAPI_V2_EOK;
   if (pPcmConfig->num_channels && pPcmConfig->sample_rate)
   {
      result = init_capi(me, &Params);
      if (CAPI_V2_EOK == result)
      {
          /* Raising PCM encoder KPPS vote based on encoder config media format */
   		  uint32_t pcm_enc_kpps = 0;
		  pcm_enc_kpps = (PCM_ENC_KPPS_FACTOR * pPcmConfig->num_channels * pPcmConfig->sample_rate * (pPcmConfig->bits_per_sample>>3))/1000;
		  pcm_enc_kpps = (pcm_enc_kpps > PCM_ENC_MIN_KPPS) ? pcm_enc_kpps : PCM_ENC_MIN_KPPS;
		  check_raise_kpps_event(me, pcm_enc_kpps);
	  }
   }

   return result;
}

capi_v2_err_t set_enc_cfg_blk(audio_enc_svc_capi_v1_wrapper_t *me, capi_v2_buf_t *params_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   switch (me->enc_cfg_id)
   {
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
   {
      asm_multi_channel_pcm_enc_cfg_v3_t* pPcmConfig = (asm_multi_channel_pcm_enc_cfg_v3_t*) (params_ptr->data_ptr);
      result = set_pcm_enc_cfg(me, pPcmConfig);
      if (CAPI_V2_FAILED(result)) return result;

      break;
   }
   case ASM_MEDIA_FMT_G711_ALAW_FS:
   case ASM_MEDIA_FMT_G711_MLAW_FS:
   {
      //assuming Alaw Mlaw enc cfg structs are the same
      asm_g711_alaw_enc_cfg_t *pCfgBlk = (asm_g711_alaw_enc_cfg_t *) params_ptr->data_ptr;

      (void) me->capi_v1_ptr->SetParam(eIcapiSamplingRate, pCfgBlk->sample_rate);

      CAPI_Buf_t Params;                 // for send parameters to Init() function
      VoiceInitParamsType VInitParams;

      VInitParams.VocTypeID = me->enc_cfg_id;
      VInitParams.VocConfigBlk = (void *) params_ptr->data_ptr;

      Params.Data = (char*) &VInitParams;
      Params.nActualDataLen = sizeof(VoiceInitParamsType);
      Params.nMaxDataLen = sizeof(VoiceInitParamsType);

      //0 sample rate means native mode...so hold off intialization until
      //we get media format cmd from PP.
      result = CAPI_V2_EOK;
      if (pCfgBlk->sample_rate)
      {
         VInitParams.VocConfigBlkSize = sizeof(asm_g711_alaw_enc_cfg_t);
         result = init_capi(me, &Params);
      }
      break;
   }
   case ASM_MEDIA_FMT_MP3:
   {
      asm_mp3_enc_cfg_t* pMp3Config = (asm_mp3_enc_cfg_t*) (params_ptr->data_ptr);

      result = me->capi_v1_ptr->SetParam(CMp3EncoderLib::eMp3BitRate, pMp3Config->bit_rate);
      if (ENC_SUCCESS != result)
      {
         return CAPI_V2_EBADPARAM;
      }

      //set num channels
      if (0 != pMp3Config->channel_cfg)
      {
         result = me->capi_v1_ptr->SetParam(eIcapiNumChannels, pMp3Config->channel_cfg);
         if (ENC_SUCCESS != result)
         {
            return CAPI_V2_EBADPARAM;
         }
      }

      //set sample rate
      if (0 != pMp3Config->sample_rate)
      {
         result = me->capi_v1_ptr->SetParam(eIcapiSamplingRate, pMp3Config->sample_rate);
         if (ENC_SUCCESS != result)
         {
            return CAPI_V2_EBADPARAM;
         }
      }

      //0 sample rate or 0 channels means native mode...so hold off intialization until
      //we get media format cmd from PP.
      result = CAPI_V2_EOK;
      if (pMp3Config->channel_cfg && pMp3Config->sample_rate)
      {
         result = init_capi(me,NULL);
         if (CAPI_V2_EOK == result)
         {
            check_raise_kpps_event(me, KCPS_WMA_V8); /* use the same KCPS as WMA until the lib is optimized */
         }
      }
      break;
   }
   case ASM_MEDIA_FMT_DTS:
   {
      asm_dts_enc_cfg_t* pDtsConfig = (asm_dts_enc_cfg_t*) (params_ptr->data_ptr);

      //set sample rate
      (void) me->capi_v1_ptr->SetParam(eIcapiSamplingRate, pDtsConfig->sample_rate);

      //set dts channel map
      CAPI_ChannelMap_t dts_channel_map;
      memset(&dts_channel_map, 0, sizeof(CAPI_ChannelMap_t));
      dts_channel_map.nChannels = pDtsConfig->num_channels;

      if (0 != pDtsConfig->num_channels)
      {
         /*Ensure to copy channel map only if num_channels is valid to avoid accessing uninitialized memory */
         memscpy(&dts_channel_map.nChannelMap, sizeof(pDtsConfig->channel_mapping), pDtsConfig->channel_mapping, sizeof(pDtsConfig->channel_mapping));
      }
      (void) me->capi_v1_ptr->SetParam(eIcapiOutputChanMap, (int) &dts_channel_map);

      result = CAPI_V2_EOK;
      if (pDtsConfig->num_channels && pDtsConfig->sample_rate)
      {
         init_capi(me, NULL);
      }
      break;
   }
   case ASM_MEDIA_FMT_EAC3:
   case ASM_MEDIA_FMT_AC3:
   {
      asm_ddp_enc_cfg_t  * pDdpConfig = (asm_ddp_enc_cfg_t*) (params_ptr->data_ptr);
      CAPI_ChannelMap_t ddpenc_channel_map;                            /* set Ddpencoder channel map */
      memset(&ddpenc_channel_map, 0, sizeof(CAPI_ChannelMap_t));
      ddpenc_channel_map.nChannels = pDdpConfig->num_channels;

      if(0 != pDdpConfig->num_channels)
      {
         memscpy(&ddpenc_channel_map.nChannelMap,sizeof(pDdpConfig->channel_mapping), pDdpConfig->channel_mapping, sizeof(pDdpConfig->channel_mapping));
      }
      (void) me->capi_v1_ptr->SetParam(eIcapiOutputChanMap, (int)&ddpenc_channel_map);
      (void) me->capi_v1_ptr->SetParam(eIcapiSamplingRate, pDdpConfig->sample_rate);
      (void) me->capi_v1_ptr->SetParam(eIcapiNumChannels, pDdpConfig->num_channels);

      result=  me->capi_v1_ptr->SetParam(CDolbyplusEncoderLib::ddpEncAcmod, pDdpConfig->num_channels);
      if(result != ENC_SUCCESS)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "DolbyPulseEncoder :setparam Error While sending setparam for Audio acmod ");
         return result ;
      }
      if(pDdpConfig->num_channels && pDdpConfig->sample_rate)
      {
         result = init_capi(me, NULL);
         if(CAPI_V2_EOK == result)
         {
            check_raise_kpps_event(me, KCPS_DDP_ENC);
         }
      }
      break;
   }
   case ASM_MEDIA_FMT_SBC:
   {
      asm_sbc_enc_cfg_t *pSbcCfg = (asm_sbc_enc_cfg_t*) (params_ptr->data_ptr);
      (void) me->capi_v1_ptr->SetParam(CSbcEncoderLib::eSbcBands, pSbcCfg->num_subbands);
      (void) me->capi_v1_ptr->SetParam(CSbcEncoderLib::eSbcBlocks, pSbcCfg->blk_len);
      (void) me->capi_v1_ptr->SetParam(CSbcEncoderLib::eSbcMode, pSbcCfg->channel_mode);
      (void) me->capi_v1_ptr->SetParam(CSbcEncoderLib::eSbcAlloc, pSbcCfg->alloc_method);
      (void) me->capi_v1_ptr->SetParam(CSbcEncoderLib::eSbcBitRate, pSbcCfg->bit_rate);
      (void) me->capi_v1_ptr->SetParam(eIcapiSamplingRate, pSbcCfg->sample_rate);

      //0 sample rate or 0 channels means native mode...so hold off intialization until
      //we get media format cmd from PP.
      result = CAPI_V2_EOK;
      if (pSbcCfg->channel_mode && pSbcCfg->sample_rate)
      {
         result = init_capi(me, NULL);
      }
      break;
   }
   case ASM_MEDIA_FMT_ADPCM:
   {
      asm_adpcm_enc_cfg_t *pAdpcmConfig = (asm_adpcm_enc_cfg_t*) (params_ptr->data_ptr);
      (void) me->capi_v1_ptr->SetParam(eIcapiNumChannels, pAdpcmConfig->num_channels);
      (void) me->capi_v1_ptr->SetParam(CADPCMEncoderLib::eADPCMBitsPerSample, pAdpcmConfig->bits_per_sample);
      (void) me->capi_v1_ptr->SetParam(eIcapiSamplingRate, pAdpcmConfig->sample_rate);
      (void) me->capi_v1_ptr->SetParam(CADPCMEncoderLib::eADPCMNumBlockSize, pAdpcmConfig->blk_size);

      //0 sample rate or 0 channels means native mode...so hold off intialization until
      //we get media format cmd from PP.
      result = CAPI_V2_EOK;
      if (pAdpcmConfig->sample_rate && pAdpcmConfig->num_channels)
      {
         result = init_capi(me, NULL);
      }
      break;
   }

   case ASM_MEDIA_FMT_AMRNB_FS:
   case ASM_MEDIA_FMT_AMRWB_FS:
   case ASM_MEDIA_FMT_EVRC_FS:
   case ASM_MEDIA_FMT_EVRCB_FS:
   case ASM_MEDIA_FMT_EVRCWB_FS:
   case ASM_MEDIA_FMT_V13K_FS:
   {
      CAPI_Buf_t Params;                 // for send parameters to Init() function
      VoiceInitParamsType VInitParams;

      VInitParams.VocTypeID = me->enc_cfg_id;
      VInitParams.VocConfigBlk = (void *) params_ptr->data_ptr;

      Params.Data = (char*) &VInitParams;
      Params.nActualDataLen = sizeof(VoiceInitParamsType);
      Params.nMaxDataLen = sizeof(VoiceInitParamsType);

      VInitParams.VocConfigBlkSize = params_ptr->actual_data_len;
      result = init_capi(me, &Params);
      break;
   }
   case ASM_MEDIA_FMT_FR_FS:
   {
      //no config block for GSM-FR
      result = ENC_SUCCESS;
      break;
   }
   case ASM_MEDIA_FMT_WMA_V8:
   {
      asm_wmastdv8_enc_cfg_t* pWmaConfig = (asm_wmastdv8_enc_cfg_t*) (params_ptr->data_ptr);

      //set bit rate
      (void) me->capi_v1_ptr->SetParam(CWmaEncoderLib::eWmaBitRate, pWmaConfig->bit_rate);

      uint32_t kpps = 0;

      //set number of channels
      (void) me->capi_v1_ptr->SetParam(eIcapiNumChannels, pWmaConfig->channel_cfg);

      //set sample rate
      (void) me->capi_v1_ptr->SetParam(eIcapiSamplingRate, pWmaConfig->sample_rate);

      //0 sample rate or 0 channels means native mode...so hold off intialization until
      //we get media format cmd from PP.
      result = CAPI_V2_EOK;
      if (pWmaConfig->channel_cfg && pWmaConfig->sample_rate)
      {
         result = init_capi(me, NULL);
         if (CAPI_V2_EOK == result)
         {
            check_raise_kpps_event(me, kpps);
         }
      }
      break;
   }

   default:
   {
      result = (me->capi_v1_ptr->SetParam(eIcapiMediaInfoPtr, (int) params_ptr->data_ptr));
   }
   } /* switch(me->enc_cfg_id) */

   if (ENC_SUCCESS != result)
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to set encoder cfg params!");
      result = CAPI_V2_EBADPARAM;
   }
   else
   {
      MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Success setting encoder cfg params!");
   }
   return conv_capi_v1_err_to_capi_v2_err(result);
}

capi_v2_err_t set_param(audio_enc_svc_capi_v1_wrapper_t *me, uint32_t param_id, capi_v2_buf_t *params_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   int res = ENC_SUCCESS;
   switch(param_id)
   {
   case CAPI_PARAM_ENC_CFG_BLK:
   {
      result = set_enc_cfg_blk(me, params_ptr);
      break;
   }
   case ASM_PARAM_ID_ENCDEC_BITRATE:
   {
      result = set_bit_rate(me, params_ptr);
      break;
   }
   case ASM_PARAM_ID_DDP_ENC_DATA_RATE:
   {
      asm_ddpencoder_generic_param_t *pddpencparams = (asm_ddpencoder_generic_param_t*) params_ptr->data_ptr;
      if (params_ptr->actual_data_len < sizeof(asm_ddpencoder_generic_param_t))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "ASM_PARAM_ID_DDP_ENCODER_PARAMS not big enough!" );
         result = CAPI_V2_ENEEDMORE;
      }
      res = me->capi_v1_ptr->SetParam(CDolbyplusEncoderLib::ddpEncDataRate, pddpencparams->param_val);
      result = conv_capi_v1_err_to_capi_v2_err(res);
      break;
   }
   case ASM_PARAM_ID_DDP_ENC_LFE:
   {
      asm_ddpencoder_generic_param_t *pddpencparams = (asm_ddpencoder_generic_param_t*) params_ptr->data_ptr;
      if (params_ptr->actual_data_len < sizeof(asm_ddpencoder_generic_param_t))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "ASM_PARAM_ID_DDP_ENCODER_PARAMS not big enough!" );
         result = CAPI_V2_ENEEDMORE;
      }
      res =  me->capi_v1_ptr->SetParam(CDolbyplusEncoderLib::ddpEncLfeEnable, pddpencparams->param_val);
      result = conv_capi_v1_err_to_capi_v2_err(res);
      break;
   }
   case ASM_PARAM_ID_DDP_ENC_PH90_FILT:
   {
      asm_ddpencoder_generic_param_t *pddpencparams = (asm_ddpencoder_generic_param_t*) params_ptr->data_ptr;
      if (params_ptr->actual_data_len < sizeof(asm_ddpencoder_generic_param_t))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "ASM_PARAM_ID_DDP_ENCODER_PARAMS not big enough!" );
         result = CAPI_V2_ENEEDMORE;
      }
      res =  me->capi_v1_ptr->SetParam(CDolbyplusEncoderLib::ddpEncPh90Filt, pddpencparams->param_val);
      result = conv_capi_v1_err_to_capi_v2_err(res);
      break;
   }
   case ASM_PARAM_ID_DDP_ENC_DIAL_NORM:
   {
      asm_ddpencoder_generic_param_t *pddpencparams = (asm_ddpencoder_generic_param_t*) params_ptr->data_ptr;
      if (params_ptr->actual_data_len < sizeof(asm_ddpencoder_generic_param_t))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "ASM_PARAM_ID_DDP_ENCODER_PARAMS not big enough!" );
         result = CAPI_V2_ENEEDMORE;
      }
      res =  me->capi_v1_ptr->SetParam(CDolbyplusEncoderLib::ddpEncDailNorm, pddpencparams->param_val);
      result = conv_capi_v1_err_to_capi_v2_err(res);
      break;
   }
   case ASM_PARAM_ID_DDP_ENC_GBL_DRC_PROF:
   {
      asm_ddpencoder_generic_param_t *pddpencparams = (asm_ddpencoder_generic_param_t*) params_ptr->data_ptr;
      if (params_ptr->actual_data_len < sizeof(asm_ddpencoder_generic_param_t))
      {
         MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "ASM_PARAM_ID_DDP_ENCODER_PARAMS not big enough!" );
         result = CAPI_V2_ENEEDMORE;
      }
      res = me->capi_v1_ptr->SetParam(CDolbyplusEncoderLib::ddpEncGblDrcProf, pddpencparams->param_val);
      result = conv_capi_v1_err_to_capi_v2_err(res);
      break;
   }
   default:
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set param for 0x%lx not supported", param_id);
      result = CAPI_V2_EUNSUPPORTED;
   }
   return result;
}
/** @ingroup
  Processes an input data and provides output for all input and output ports.

  @datatypes
  capi_v2_t \n
  capi_v2_stream_data_t

  @param[in,out] _pif       Pointer to the module object.
  @param[in,out] input      Array of pointers to the input data for each
                            input port. The length of the array is the number
                            of input ports. The function
                            must modify the actual_data_len field to
                            indicate how many bytes were consumed.
  @param[out]    output     Array of pointers to the output data for each
                            output port. The function sets the actual_data_len
                            field to indicate how many bytes were generated.

  @detdesc
  This is a generic processing function.
  @par
  On each call to the %process() function, the behavior of the module depends
  on the value it returned for the requires_data_buffering property. Please
  refer to the comments for the requires_data_buffering property for a
  description of the behavior.
  @par
  No debug messages are allowed in this function.
  @par

  @return
  Indication of success or failure.

  @dependencies
  The change_media_type() function must have been executed.
 */
//#define DEBUG_MEDIUM
//#define DEBUG_VERBOSE
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_process(capi_v2_t* _pif,
      capi_v2_stream_data_t* input[],
      capi_v2_stream_data_t* output[])
{
   capi_v2_err_t result = CAPI_V2_EOK;

   if (!_pif) return CAPI_V2_EBADPARAM;

   audio_enc_svc_capi_v1_wrapper_t *me = (audio_enc_svc_capi_v1_wrapper_t*)_pif;

   //handling one input and one output only.
   uint32_t port=0 ;

#ifdef DEBUG_MEDIUM
   MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper process enter: input [port %lu] TS: %lu,%lu, bufs_num:%lu. ",port,
         (uint32_t)(input[port]->timestamp>>32), (uint32_t)input[port]->timestamp, input[port]->bufs_num);

   MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper process enter: input [port %lu] flag:[%lu,%lu,%lu,%lu,%lu,%lu]",port,
         (uint32_t)input[port]->flags.is_timestamp_valid, (uint32_t)input[port]->flags.end_of_frame,
         (uint32_t)input[port]->flags.marker_eos,(uint32_t)input[port]->flags.marker_1,
         (uint32_t)input[port]->flags.marker_2,(uint32_t)input[port]->flags.marker_3);
   uint32_t buf_num;
   for (buf_num = 0; buf_num < input[port]->bufs_num; buf_num++)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper process enter: input [port %lu]. buf_num:%lu. in_size=%lu",port,
            buf_num, input[port]->buf_ptr[0].actual_data_len);
   }
   for (buf_num = 0; buf_num < input[port]->bufs_num; buf_num++)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper process enter: output [port %lu]. buf_num:%lu. out_size=%lu",port,
            buf_num, output[port]->buf_ptr[0].actual_data_len);
   }
#endif
   //error check

   //do not drop data if waiting for enc cfg (even though it breaks backward compatible behavior)
   if (!me->capi_init_done)
   {
      (void)init_capi(me, NULL);
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper process automatically called init although enc cfg was probably not received.");
   }

   //set end of frame/stream
   check_set_end_of_stream(me, input[0]->flags.marker_eos>0);

   CAPI_Buf_t                          input_buf;
   CAPI_Buf_t                          output_buf;
   CAPI_BufList_t                      input_buf_list;
   CAPI_BufList_t                      output_buf_list;

   int in_size_before = input[port]->buf_ptr->actual_data_len;

   input_buf.Data = (char*)input[port]->buf_ptr->data_ptr;
   input_buf.nActualDataLen = input[port]->buf_ptr->actual_data_len;
   input_buf.nMaxDataLen = input[port]->buf_ptr->max_data_len;
   input_buf_list.nBufs = 1;
   input_buf_list.pBuf = &input_buf;

   output_buf.Data = (char*)output[port]->buf_ptr->data_ptr;
   output_buf.nActualDataLen = output[port]->buf_ptr->actual_data_len = 0;
   //for CAPI v2 data_ptr is where, data starts & actual_len can be uninitialized but for capi v1 data_ptr+actual_len is where data starts. make actual_len to zero to achieve this.
   //if actual len is not zero, then CAPIs which add it will face issues.
   output_buf.nMaxDataLen = output[port]->buf_ptr->max_data_len;
   output_buf_list.nBufs = 1;
   output_buf_list.pBuf = &output_buf;

   int enc_res = me->capi_v1_ptr->Process(&input_buf_list, &output_buf_list, NULL);

   bool_t need_events = true;

   if( (input_buf.nActualDataLen<0) || (input_buf.nActualDataLen > in_size_before))
   {
      //The number of remaining bytes are negative or greater than valid number of bytes
      //at the beginning of encode. This indicates that encoding did not happen correctly
      //and encoder might be in a bad state
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,
            "Invalid # of available input bytes after CAPI process call at input port 0 : Available bytes before = %d, Available bytes after = %d",
            in_size_before, input_buf.nActualDataLen);

      //set the result to ENC_RECOVERY_FAILURE which will discard the contents of the
      //current buffer
      enc_res = ENC_FAILURE;

      //reset the encoder in order to avoid undefined behavior with future buffers
      init_capi(me, NULL);
   }

   //check the status of the encoder and take action as per the return type
   //handle enc error codes because depacketizer can return it.
   switch ( enc_res )
   {
   case ENC_SUCCESS:
   {
#ifdef DEBUG_VERBOSE
      {
         char buf[50];
         uint8_t *ptr = (uint8_t*)output_buf.Data;
         char *buf_ptr = buf;
         for (uint8_t i=0;i<8;i++)
         {
            snprintf(buf_ptr,50,"%02x ",*ptr);
            buf_ptr+=3; ptr++;
            if ((i+1)%4)
            {
               snprintf(buf_ptr,50," "); buf_ptr+=1;
            }
         }
         printf("Output data %s \n",buf);
      }
      {
         if (output_buf.nActualDataLen > (int)me->bytes_logged)
         {
            int8_t * buf_addr;
            uint32_t buf_size;

            printf("me->bytes_logged = %lu, output_buf.nActualDataLen %d\n", me->bytes_logged, output_buf.nActualDataLen);
            buf_addr = (int8_t *)(output_buf.Data) + me->bytes_logged;
            buf_size = (output_buf.nActualDataLen -  me->bytes_logged);

            if(buf_size)
            {
               FILE *fp;
               fp = fopen("enc_out.raw","a"); // in append mode
               fwrite(buf_addr,sizeof(int8_t),buf_size,fp);
               fclose(fp);
            }
         }
      }
#endif

      result = CAPI_V2_EOK;
      break;
   }
   case ENC_NEED_MORE:
   {
      result = CAPI_V2_EOK; //as encoder knows how to find need-more
      need_events = false;
      break;
   }
   case ENC_BUFFERTOOSMALL_FAILURE:
   {
      result = CAPI_V2_ENOMEMORY;
      need_events = false;
      break;
   }
   default:
   {
      MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Encoder failure; %d",enc_res);

      result = CAPI_V2_EFAILED;
      need_events = false;
      break;
   }
   }

   me->bytes_logged = input_buf.nActualDataLen;

   input[port]->buf_ptr->data_ptr = (int8_t*)input_buf.Data;
   input[port]->buf_ptr->actual_data_len = in_size_before - input_buf.nActualDataLen; //in capi v2 input buf actual size on output means, the num bytes read.
   input[port]->buf_ptr->max_data_len = input_buf.nMaxDataLen;

   output[port]->buf_ptr->data_ptr = (int8_t*)output_buf.Data;
   output[port]->buf_ptr->actual_data_len = output_buf.nActualDataLen;
   output[port]->buf_ptr->max_data_len = output_buf.nMaxDataLen;

#ifdef DEBUG_MEDIUM
   MSG_4(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper process exit: output [port %lu] TS: %lu,%lu, bufs_num:%lu.",port,
         (uint32_t)(output[port]->timestamp>>32), (uint32_t)output[port]->timestamp, output[port]->bufs_num);

   MSG_7(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper process exit: output [port %lu] flag:[%lu,%lu,%lu,%lu,%lu,%lu]",port,
         (uint32_t)output[port]->flags.is_timestamp_valid, (uint32_t)output[port]->flags.end_of_frame,
         (uint32_t)output[port]->flags.marker_eos,(uint32_t)output[port]->flags.marker_1,
         (uint32_t)output[port]->flags.marker_2,(uint32_t)output[port]->flags.marker_3);

   for (buf_num = 0; buf_num < output[port]->bufs_num; buf_num++)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper process exit: output [port %lu]. buf_num:%lu. out_size=%lu",port,
            buf_num, output[port]->buf_ptr[0].actual_data_len);
   }
   for (buf_num = 0; buf_num < input[port]->bufs_num; buf_num++)
   {
      MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper process exit: input [port %lu]. buf_num:%lu. in_size read=%lu\n",port,
            buf_num, input[port]->buf_ptr[0].actual_data_len);
   }
#endif

   //Do not raise events in case of failure or in case of NEED_MORE, RECOVERY_SUCCESS
   if (!need_events) return result;

   output[port]->flags = input[port]->flags;
   output[port]->timestamp = input[port]->timestamp;

   int val=0;
   int res =  me->capi_v1_ptr->GetParam(eIcapiKCPSRequired, &val);
   if (res != DEC_SUCCESS)
   {
      val = AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_KPPS;
   }
   result = check_raise_kpps_event(me, val);

   result |= check_raise_bw_event(me, AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_CODE_BW, AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_DATA_BW);

   audio_enc_svc_capi_v2_media_fmt_t new_fmt;

   copy_media_fmt(&new_fmt, &(me->out_media_fmt[0])); //initialize with old values.

   if ( (new_fmt.main.format_header.data_format == CAPI_V2_FIXED_POINT) ||
         (new_fmt.main.format_header.data_format == CAPI_V2_IEC61937_PACKETIZED))
   {
      res = me->capi_v1_ptr->GetParam(eIcapiBitsPerSample, &val);

      if ((DEC_BADPARAM_FAILURE == res) || (ENC_BADPARAM_FAILURE == res))
      {
         val = 16; //ignore bad param failure (for voice capis)
      }
      else if (res != ENC_SUCCESS)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Get param failed for bits per sample");
         return CAPI_V2_EFAILED;
      }

      new_fmt.std.bits_per_sample = (uint32_t)val;
      new_fmt.std.q_factor = (val == 16)? PCM_16BIT_Q_FORMAT : ELITE_32BIT_PCM_Q_FORMAT;

      res = me->capi_v1_ptr->GetParam(eIcapiSamplingRate, &val);
      if (res != ENC_SUCCESS)
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Get param failed for sampling rate");
         return CAPI_V2_EFAILED;
      }
      new_fmt.std.sampling_rate = (uint32_t)val;

      CAPI_ChannelMap_t *out_chan_map = NULL;
      res = me->capi_v1_ptr->GetParam(eIcapiOutputChanMap, (int*)&out_chan_map); //ignore error for backward compatibility
      if ((NULL == out_chan_map) || (res != ENC_SUCCESS) )
      {
         new_fmt.std.num_channels = 2;
         new_fmt.std.channel_type[0] = PCM_CHANNEL_L;
         new_fmt.std.channel_type[1] = PCM_CHANNEL_R;
      }
      else
      {
         new_fmt.std.num_channels = out_chan_map->nChannels;
         for (unsigned int ch=0;(ch<new_fmt.std.num_channels) && (ch<CAPI_V2_MAX_CHANNELS); ch++)
         {
            new_fmt.std.channel_type[ch] = (uint16_t)out_chan_map->nChannelMap[ch];
         }
      }
   }
   else //CAPI_V2_RAW_COMPRESSED
   {
      int format = me->enc_cfg_id;
      me->capi_v1_ptr->GetParam(CDepacketizerLib::eDepacketizerFormatType, &format);
      new_fmt.raw_fmt.bitstream_format = format;
   }

   {
      capi_v2_err_t err_code = check_raise_output_media_fmt_updated_event(me, &new_fmt);
      if (CAPI_V2_FAILED(err_code))
      {
         result = err_code;
      }
   }

   return result;
}

/** @ingroup
  Returns the module to the uninitialized state and frees any memory that
  was allocated by it.

  @datatypes
  capi_v2_t

  @param[in,out] _pif    Pointer to the module object.

  @detdesc
  Before returning SUCCESS and once the function is complete, a debug
  message is to be printed:
  @par
  @code
  MSG(MSG_SSID_QDSP6,DBG_HIGH_PRIO,"CAPI_V2 Libname End done");
  @endcode
  @par
  @note1hang This function undoes all the work that capi_v2_init_f() has done.
             Any call using the module object is not allowed after this call.

  @dependencies
  None.
 */
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_end(capi_v2_t* _pif)
{
   if (!_pif) return CAPI_V2_EOK;

   audio_enc_svc_capi_v1_wrapper_t *me = (audio_enc_svc_capi_v1_wrapper_t*)_pif;

   int res = me->capi_v1_ptr->End();

   me->capi_init_done = false;

   me->vtbl.vtbl_ptr = NULL;

   MSG_1(MSG_SSID_QDSP6,DBG_HIGH_PRIO,"Capi_V1_enc_wrapper End done. res = %d", res);

   return CAPI_V2_EOK;
}

/** @ingroup
  Sets a parameter value or a parameter structure containing multiple
  parameters.

  @datatypes
  capi_v2_t \n
  capi_v2_buf_t

  @param[in,out] _pif        Pointer to the module object.
  @param[in]     param_id    Parameter ID of the parameter whose value is
                             being passed in this function. For example: \n
                             - CAPI_V2_LIBNAME_ENABLE
                             - CAPI_V2_LIBNAME_FILTER_COEFF @tablebulletend
  @param[in]     params_ptr  Buffer containing the value of the parameter.
                             The format depends on the implementation.

  @detdesc
  In the event of a failure, the appropriate error code is to be
  returned.
  The actual_data_len field of the parameter pointer is to be at least the size
  of the parameter structure. Therefore, the following check must be exercised
  for each tuning parameter ID.
  @par
  @code
  if (params_ptr->actual_data_len >= sizeof(asm_gain_struct_t))
  {
     :
     :
  }
  else
  {
     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"CAPI_V2 Libname Set, Bad param size
     %lu",params_ptr->actual_data_len);
     return CAPI_V2_ENEEDMORE;
  }
  @endcode
  @par
  Optionally, some parameter values can be printed for tuning verification.
  @par
  Before returning, the actual_data_len field must be filled with the number
  of bytes read from the buffer.
  @par
  Before returning SUCCESS and once the function is complete, a debug message
  is to be printed:
  @par
  @code
  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"CAPI_V2 Libname Set param for 0x%lx done",param_id);
  @endcode

  @return
  None.

  @dependencies
  None.
 */
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_set_param(capi_v2_t* _pif,
      uint32_t param_id,
      const capi_v2_port_info_t *port_info_ptr,
      capi_v2_buf_t *params_ptr)
{
   audio_enc_svc_capi_v1_wrapper_t *me = (audio_enc_svc_capi_v1_wrapper_t*)_pif;
   capi_v2_err_t result = set_param(me, param_id, params_ptr);

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Set param for 0x%lx done", param_id);

   return result;
}

/** @ingroup capi_v2_virt_func_get_params
  Gets either a parameter value or a parameter structure containing multiple
  parameters.

  @datatypes
  capi_v2_t \n
  capi_v2_buf_t

  @param[in,out] _pif        Pointer to the module object.
  @param[in]     param_id    Parameter ID of the parameter whose value is
                             being passed in this function. For example:\n
                             - CAPI_V2_LIBNAME_ENABLE
                             - CAPI_V2_LIBNAME_FILTER_COEFF @tablebulletend
  @param[out]    params_ptr  Buffer containing the value of the parameter.
                             The format depends on the implementation.

  @detdesc
  In the event of a failure, the appropriate error code is to be
  returned.
  @par
  The max_data_len field of the parameter pointer must be at least the size
  of the parameter structure. Therefore, the following check must be
  exercised for each tuning parameter ID.
  @par
  @code
  if (params_ptr->max_data_len >= sizeof(asm_gain_struct_t))
  {
     :
     :
  }
  @endcode
  @newpage
  @code
  else
  {
     MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"CAPI_V2 Libname Get, Bad param size
     %lu",params_ptr->max_data_len);
     return CAPI_V2_ENEEDMORE;
  }
  @endcode
  @par
  Before returning, the actual_data_len field must be filled with the number
  of bytes written into the buffer.
  @par
  Optionally, some parameter values can be printed for tuning verification.
  @par
  Before returning SUCCESS and once the function is complete, a debug message
  is to be printed:
  @par
  @code
  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"CAPI_V2 Libname Get param for 0x%lx done",param_id);
  @endcode

  @return
  Indication of success or failure.

  @dependencies
  None.
 */
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_get_param(capi_v2_t* _pif,
      uint32_t param_id,
      const capi_v2_port_info_t *port_info_ptr,
      capi_v2_buf_t *params_ptr)
{
   if (params_ptr->actual_data_len < sizeof(int))
   {
      MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Bad param size %lu", param_id, params_ptr->actual_data_len);
      return CAPI_V2_ENEEDMORE;
   }

   audio_enc_svc_capi_v1_wrapper_t *me = (audio_enc_svc_capi_v1_wrapper_t*)_pif;
   int res = 0;

   switch(param_id)
   {
   case CAPI_PARAM_SAMPLES_PER_FRAME:
   {
      res = me->capi_v1_ptr->GetParam(eIcapiSamplesPerFrame, (int*)(params_ptr->data_ptr));
      params_ptr->actual_data_len = sizeof(int);
      break;
   }
   default:
   {
      res = me->capi_v1_ptr->GetParam((int) param_id, (int*)(params_ptr->data_ptr));
      params_ptr->actual_data_len = sizeof(int);
      break;
   }
   }

   if (ENC_SUCCESS != res)
   {
      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Get param for 0x%lx failed", param_id);
      return CAPI_V2_EFAILED;
   }

   MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Get param for 0x%lx done", param_id);

   return CAPI_V2_EOK;
}

/** @ingroup
  Sets a list of property values.

  @datatypes
  capi_v2_t \n
  capi_v2_proplist_t

  @param[in,out] _pif        Pointer to the module object.
  @param[in]     props_ptr   Contains the property values to be set.

  @detdesc
  In the event of a failure, the appropriate error code is to be
  returned.
  @par
  Optionally, some property values can be printed for debugging.
  @par
  Before returning SUCCESS and once the function is complete, a debug message
  is to be printed:
  @par
  @code
  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"CAPI_V2 Libname Set property for 0x%lx done", props_ptr->prop_ptr[i].id);
  @endcode

  @return
  None.

  @dependencies
  None.
 */
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_set_properties(capi_v2_t* _pif,
      capi_v2_proplist_t *props_ptr)
{
   if (!_pif)
   {
      return CAPI_V2_EBADPARAM;
   }

   audio_enc_svc_capi_v1_wrapper_t *me = (audio_enc_svc_capi_v1_wrapper_t*)_pif;

   return audio_enc_svc_capi_v1_wrapper_process_set_properties(me, props_ptr);
}

/** @ingroup
  Gets a list of property values.

  @datatypes
  capi_v2_t \n
  capi_v2_proplist_t

  @param[in,out] _pif        Pointer to the module object.
  @param[out]    props_ptr   Contains the empty structures that must be
                             filled with the appropriate property values
                             based on the property ids provided.

  @detdesc
  In the event of a failure, the appropriate error code is to be
  returned.
  @par
  Before returning SUCCESS and once the function is complete, a debug message
  is to be printed:
  @par
  @code
  MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"CAPI_V2 Libname Get property for 0x%lx done",props_ptr->prop_ptr[i].id);
  @endcode

  @return
  Indication of success or failure.

  @dependencies
  None.
 */
static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_get_properties(capi_v2_t* _pif,
      capi_v2_proplist_t *props_ptr)
{
   if (!_pif)
   {
      return CAPI_V2_EBADPARAM;
   }

   audio_enc_svc_capi_v1_wrapper_t *me = (audio_enc_svc_capi_v1_wrapper_t*)_pif;

   return audio_enc_svc_capi_v1_wrapper_process_get_properties(me, props_ptr);
}

static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_algorithmic_reset(audio_enc_svc_capi_v1_wrapper_t *me)
{
   return (capi_v2_err_t)init_capi(me, NULL);
}

static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_process_set_properties(audio_enc_svc_capi_v1_wrapper_t* me, capi_v2_proplist_t *props_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   capi_v2_prop_t *prop_ptr= props_ptr->prop_ptr;
   uint32_t i;

   for (i=0; i < props_ptr->props_num; i++)
   {
      capi_v2_buf_t *payload = &prop_ptr[i].payload;

      //Ignore prop_ptr[i].port_info.is_valid

      switch(prop_ptr[i].id)
      {
      case CAPI_V2_EVENT_CALLBACK_INFO:
      {
         if (payload->actual_data_len >= sizeof(capi_v2_event_callback_info_t))
         {
            capi_v2_event_callback_info_t *data_ptr = (capi_v2_event_callback_info_t*)payload->data_ptr;
            if (NULL == data_ptr)
            {
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EBADPARAM);
            }

            me->cb_info = *data_ptr;
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->actual_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_PORT_NUM_INFO:
      {
         if (payload->actual_data_len >= sizeof(capi_v2_port_num_info_t))
         {
            capi_v2_port_num_info_t *data_ptr = (capi_v2_port_num_info_t*)payload->data_ptr;

            if ((data_ptr->num_input_ports > AUDIO_ENC_SVC_CAPI_V1_WRAPPER_MAX_IN_PORTS) ||
                  (data_ptr->num_output_ports > AUDIO_ENC_SVC_CAPI_V1_WRAPPER_MAX_OUT_PORTS) )
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, Param id 0x%lx num of in and out ports cannot be port than 1", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EBADPARAM);
            }
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->actual_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_HEAP_ID:
      {
         if (payload->actual_data_len >= sizeof(capi_v2_heap_id_t))
         {
            //capi_v2_heap_id_t *data_ptr = (capi_v2_heap_id_t*)payload->data_ptr;
            MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, Param id 0x%lx unsupported", (uint32_t)prop_ptr[i].id);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_EUNSUPPORTED);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->actual_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_ALGORITHMIC_RESET:
      {
         result |= audio_enc_svc_capi_v1_wrapper_algorithmic_reset(me);
         break;
      }
      case CAPI_V2_INPUT_MEDIA_FORMAT:
      {
         if (payload->actual_data_len >= sizeof(audio_enc_svc_capi_v2_media_fmt_t))
         {
            bool_t any_set_done=false;
            audio_enc_svc_capi_v2_media_fmt_t *data_ptr = (audio_enc_svc_capi_v2_media_fmt_t*)payload->data_ptr;
            if (NULL == me || NULL == me->capi_v1_ptr || NULL == data_ptr ||
                  (prop_ptr[i].port_info.is_valid && prop_ptr[i].port_info.port_index !=0 ) ||
                  ((data_ptr->main.format_header.data_format != CAPI_V2_FIXED_POINT) &&
                        (data_ptr->main.format_header.data_format != CAPI_V2_IEC61937_PACKETIZED)))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, failed to set Param id 0x%lx due to invalid/unexpected values", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EFAILED);
            }

            capi_v2_err_t local_result = CAPI_V2_EOK;
            //bits per sample is set.
            if (data_ptr->std.bits_per_sample != CAPI_V2_DATA_FORMAT_INVALID_VAL)
            {
               any_set_done = true;
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, bits per sample %lu", data_ptr->std.bits_per_sample);

               int enc_result = me->capi_v1_ptr->SetParam(eIcapiBitsPerSample, data_ptr->std.bits_per_sample);
               if ((ENC_BADPARAM_FAILURE == enc_result) || (ENC_BADPARAM_FAILURE == enc_result))
               {
                  //ignoring to be backward compatible (Eg. Mp2 enc returns fail to all set param, but we shouldn't fail enc create)
                  //enc svc anyway query bps and adjust if needed.
                  local_result |= CAPI_V2_EOK;
               }
               else if (ENC_SUCCESS != enc_result)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, failed to set Param id 0x%lx (bits per sample). res = %d. Ignoring.",
                        (uint32_t)prop_ptr[i].id, enc_result);
                  any_set_done = false;
                  local_result |= CAPI_V2_EFAILED;
               }
            }
            if (data_ptr->std.num_channels != CAPI_V2_DATA_FORMAT_INVALID_VAL)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, channels  %lu", data_ptr->std.num_channels);

               int enc_result = ENC_SUCCESS;

               enc_result |= me->capi_v1_ptr->SetParam(eIcapiNumChannels, data_ptr->std.num_channels);

               CAPI_ChannelMap_t chan_map;
               chan_map.nChannels = data_ptr->std.num_channels;
               bool_t channelmap_valid = true;
               for (unsigned int i=0; i<chan_map.nChannels;i++)
               {
                  if (data_ptr->std.channel_type[i] != (uint16_t)CAPI_V2_DATA_FORMAT_INVALID_VAL)
                  {
                     chan_map.nChannelMap[i] = data_ptr->std.channel_type[i];
                  }
                  else
                  {
                     channelmap_valid = false;
                  }
               }

               if (channelmap_valid)
               {
                  enc_result |= me->capi_v1_ptr->SetParam(eIcapiOutputChanMap, (int)&chan_map);
               }
               any_set_done = true;
               if (ENC_SUCCESS != enc_result)
               {
                  any_set_done = false;
                  local_result |= CAPI_V2_EFAILED;
               }
            }
            if (data_ptr->std.sampling_rate != CAPI_V2_DATA_FORMAT_INVALID_VAL)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, sample rate  %lu", data_ptr->std.sampling_rate);
               (void) me->capi_v1_ptr->SetParam(eIcapiSamplingRate, data_ptr->std.sampling_rate);
               //ignore error.
               any_set_done = true;
            }

            //reinit CAPI after media format.
            if (local_result == CAPI_V2_EOK && any_set_done == true)
            {
               init_capi(me, NULL);
				/* Raising PCM encoder KPPS vote based on media format from upstream svc */
				if((ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2 == me->enc_cfg_id) ||
         			(ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3 == me->enc_cfg_id))
				{
					uint32_t pcm_enc_kpps = 0;
					pcm_enc_kpps = (PCM_ENC_KPPS_FACTOR * data_ptr->std.num_channels * data_ptr->std.sampling_rate * (data_ptr->std.bits_per_sample>>3))/1000;
					pcm_enc_kpps = (pcm_enc_kpps > PCM_ENC_MIN_KPPS) ? pcm_enc_kpps : PCM_ENC_MIN_KPPS;
					check_raise_kpps_event(me, pcm_enc_kpps);
				}
            }
            result |= local_result;
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Set, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->actual_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Set property for 0x%x. Not supported.",prop_ptr[i].id);
         payload->actual_data_len = 0;
         result |= CAPI_V2_EUNSUPPORTED;
      }
      }

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Set property for 0x%x done", prop_ptr[i].id);
   }

   return result;
}

static capi_v2_err_t audio_enc_svc_capi_v1_wrapper_process_get_properties(audio_enc_svc_capi_v1_wrapper_t* me, capi_v2_proplist_t *props_ptr)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   capi_v2_prop_t *prop_ptr= props_ptr->prop_ptr;
   uint32_t i;
   for (i=0; i < props_ptr->props_num; i++)
   {
      capi_v2_buf_t *payload = &prop_ptr[i].payload;
      //Ignore prop_ptr[i].port_info.is_valid

      switch(prop_ptr[i].id)
      {
      case CAPI_V2_INIT_MEMORY_REQUIREMENT:
      {
         if (payload->max_data_len >= sizeof(capi_v2_init_memory_requirement_t))
         {
            capi_v2_init_memory_requirement_t *data_ptr = (capi_v2_init_memory_requirement_t*)payload->data_ptr;
            data_ptr->size_in_bytes = sizeof(audio_enc_svc_capi_v1_wrapper_t);
            payload->actual_data_len = sizeof(capi_v2_init_memory_requirement_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_STACK_SIZE:
      {
         if (payload->max_data_len >= sizeof(capi_v2_stack_size_t))
         {
            capi_v2_stack_size_t *data_ptr = (capi_v2_stack_size_t*)payload->data_ptr;

            int res, stack_size;

            if (NULL == me || NULL == me->capi_v1_ptr)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return CAPI_V2_EFAILED;
            }

            res = me->capi_v1_ptr->GetParam(eIcapiThreadStackSize, &stack_size);
            if (ENC_SUCCESS != res)
            {
               MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx ICAPI returned failure %d", (uint32_t)prop_ptr[i].id, res);
               stack_size = AUDIO_ENC_SVC_CAPI_V1_WRAPPER_STACK_SIZE;
            }
            data_ptr->size_in_bytes = (uint32_t)stack_size;
            payload->actual_data_len = sizeof(capi_v2_stack_size_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_MAX_METADATA_SIZE:
      {
         if (payload->max_data_len >= sizeof(capi_v2_max_metadata_size_t))
         {
            capi_v2_max_metadata_size_t *data_ptr = (capi_v2_max_metadata_size_t*)payload->data_ptr;

            int metadata_size;

            if (NULL == me || NULL == me->capi_v1_ptr)
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return CAPI_V2_EFAILED;
            }

            metadata_size= 0;
            result = CAPI_V2_EOK; //ignore res to be backward compatible.

            data_ptr->size_in_bytes = (uint32_t)metadata_size;
            payload->actual_data_len = sizeof(capi_v2_max_metadata_size_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_IS_INPLACE:
      {
         if (payload->max_data_len >= sizeof(capi_v2_is_inplace_t))
         {
            capi_v2_is_inplace_t *data_ptr = (capi_v2_is_inplace_t*)payload->data_ptr;
            data_ptr->is_inplace = FALSE;
            payload->actual_data_len = sizeof(capi_v2_is_inplace_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_REQUIRES_DATA_BUFFERING:
      {
         if (payload->max_data_len >= sizeof(capi_v2_requires_data_buffering_t))
         {
            capi_v2_requires_data_buffering_t *data_ptr = (capi_v2_requires_data_buffering_t*)payload->data_ptr;
            data_ptr->requires_data_buffering = TRUE;
            payload->actual_data_len = sizeof(capi_v2_requires_data_buffering_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_OUTPUT_MEDIA_FORMAT_SIZE:
      {
         if (payload->max_data_len >= sizeof(capi_v2_output_media_format_size_t))
         {
            capi_v2_output_media_format_size_t *data_ptr = (capi_v2_output_media_format_size_t*)payload->data_ptr;

            if (NULL == me || NULL == me->capi_v1_ptr )
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EFAILED);
            }
            if ( (me->out_data_fmt == CAPI_V2_FIXED_POINT) || (me->out_data_fmt == CAPI_V2_IEC61937_PACKETIZED))
            {
               data_ptr->size_in_bytes = sizeof(capi_v2_standard_data_format_t);
            }
            else if (me->out_data_fmt == CAPI_V2_RAW_COMPRESSED)
            {
               data_ptr->size_in_bytes = sizeof(capi_v2_raw_compressed_data_format_t);
            }
            else
            {
               data_ptr->size_in_bytes = 0;
            }
            payload->actual_data_len = sizeof(capi_v2_output_media_format_size_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_OUTPUT_MEDIA_FORMAT:
      {
         if (payload->max_data_len >= sizeof(audio_enc_svc_capi_v2_media_fmt_t))
         {
            audio_enc_svc_capi_v2_media_fmt_t *data_ptr = (audio_enc_svc_capi_v2_media_fmt_t*)payload->data_ptr;

            if (NULL == me || NULL == me->capi_v1_ptr || NULL == data_ptr ||
                  (prop_ptr[i].port_info.is_valid && prop_ptr[i].port_info.port_index !=0 ))
            {
               MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, failed to get Param id 0x%lx due to invalid/unexpected values", (uint32_t)prop_ptr[i].id);
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EFAILED);
            }

            *data_ptr = me->out_media_fmt[0];
            uint32_t size_in_bytes = 0;
            if ( (me->out_data_fmt == CAPI_V2_FIXED_POINT) || (me->out_data_fmt == CAPI_V2_IEC61937_PACKETIZED))
            {
               size_in_bytes = sizeof(capi_v2_standard_data_format_t);
            }
            else if (me->out_data_fmt == CAPI_V2_RAW_COMPRESSED)
            {
               size_in_bytes = sizeof(capi_v2_raw_compressed_data_format_t);
            }
            else
            {
               size_in_bytes = 0;
            }

            payload->actual_data_len = size_in_bytes;
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      case CAPI_V2_PORT_DATA_THRESHOLD:
      {
         if (payload->max_data_len >= sizeof(capi_v2_port_data_threshold_t))
         {
            capi_v2_port_data_threshold_t *data_ptr = (capi_v2_port_data_threshold_t*)payload->data_ptr;
            if (!prop_ptr[i].port_info.is_valid)
            {
               MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, port id not valid");
               payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
               return (result|CAPI_V2_EBADPARAM);
            }
            if (prop_ptr[i].port_info.is_input_port)
            {
               if (0 != prop_ptr[i].port_info.port_index)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx max input port is 1. asking for %lu", (uint32_t)prop_ptr[i].id, prop_ptr[i].port_info.port_index);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EBADPARAM);
               }

               int res, input_buf_size;

               if (NULL == me || NULL == me->capi_v1_ptr)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return CAPI_V2_EFAILED;
               }

               res = me->capi_v1_ptr->GetParam(eIcapiInputBufferSize, &input_buf_size);
               if (ENC_SUCCESS != res)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx ICAPI returned failure %d", (uint32_t)prop_ptr[i].id, res);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return CAPI_V2_EUNSUPPORTED;
               }

               data_ptr->threshold_in_bytes = input_buf_size;
            }
            else
            {
               if (0 != prop_ptr[i].port_info.port_index)
               {
                  MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx max output port is 1. asking for %lu", (uint32_t)prop_ptr[i].id, prop_ptr[i].port_info.port_index);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EBADPARAM);
               }
               int res, output_buf_size = AUDIO_ENC_SVC_CAPI_V1_WRAPPER_OUTPUT_SIZE;

               if (NULL == me || NULL == me->capi_v1_ptr)
               {
                  MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Wrapper or the ICAPI must be created", (uint32_t)prop_ptr[i].id);
                  payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
                  return (result|CAPI_V2_EFAILED);
               }

               res = me->capi_v1_ptr->GetParam(eIcapiOutputBufferSize, &output_buf_size);
               if (ENC_SUCCESS != res)
               {
                  output_buf_size = AUDIO_ENC_SVC_CAPI_V1_WRAPPER_OUTPUT_SIZE;
                  MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx ICAPI returned failure %d. Assuming %lu bytes", (uint32_t)prop_ptr[i].id, res, output_buf_size);
               }

               data_ptr->threshold_in_bytes = output_buf_size;
            }
            payload->actual_data_len = sizeof(capi_v2_port_data_threshold_t);
         }
         else
         {
            MSG_2(MSG_SSID_QDSP6, DBG_ERROR_PRIO,"Capi_V1_enc_wrapper Get, Param id 0x%lx Bad param size %lu", (uint32_t)prop_ptr[i].id, payload->max_data_len);
            payload->actual_data_len = 0; //according to CAPI V2, actual len should be num of bytes read(set)/written(get)
            return (result|CAPI_V2_ENEEDMORE);
         }
         break;
      }
      default:
      {
         MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Get property for 0x%x. Not supported.",prop_ptr[i].id);
         payload->actual_data_len = 0;
         result |= CAPI_V2_EUNSUPPORTED;
      }
      }

      MSG_1(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Get property for 0x%x done",prop_ptr[i].id);
   }

   return result;
}

/** @ingroup
Used to query for static properties of the module that are independent of the
instance. This function is used to query the memory requirements of the module
in order to create an instance. The same properties that are sent to the module
in the call to init() are also sent to this function to enable the module to
calculate the memory requirement.

@param[in]  init_set_properties  The same properties that will be sent in the
                         call to the init() function.
@param[out] static_properties   Pointer to the structure that the module must
                         fill with the appropriate values based on the property
                         id.

@return
Indication of success or failure.

@dependencies
None.
 */
capi_v2_err_t audio_enc_svc_capi_v1_wrapper_get_static_properties (
      capi_v2_proplist_t *init_set_properties,
      capi_v2_proplist_t *static_properties)
{
   capi_v2_err_t result = CAPI_V2_EOK;

   if (NULL != static_properties)
   {
      result = audio_enc_svc_capi_v1_wrapper_process_get_properties((audio_enc_svc_capi_v1_wrapper_t*)NULL, static_properties);
      if (result != CAPI_V2_EOK)
      {
         return result;
      }
   }

   if (NULL != init_set_properties)
   {
      //ignore currently.
   }

   return result;
}

/** @ingroup
Instantiates the module to set up the virtual function table, and also
allocates any memory required by the module. States within the module must
be initialized at the same time.

@datatypes
capi_v2_t \n
capi_v2_proplist_t \n
capi_v2_event_cb \n
void *

@param[in,out] _pif            Pointer to the module object.
@param[in]    init_set_properties Properties set by the service to be used
                              while init().

@return
Indication of success or failure.

@dependencies
 */
capi_v2_err_t audio_enc_svc_capi_v1_wrapper_init (capi_v2_t*  _pif,
      capi_v2_proplist_t      *init_set_properties)
{
   capi_v2_err_t result = CAPI_V2_EOK;
   if (!_pif)
   {
      return CAPI_V2_EBADPARAM;
   }

   audio_enc_svc_capi_v1_wrapper_t *me = (audio_enc_svc_capi_v1_wrapper_t*)_pif;

   if ((NULL == me->capi_v1_ptr))
   {
      MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper Capi creation failed");
      return CAPI_V2_ENOMEMORY;
   }

   init_media_fmt(me);

   if (NULL != init_set_properties) //should contain IN_BITS_PER_SAMPLE, EVENT_CALLBACK_INFO, PORT_INFO
   {
      result = audio_enc_svc_capi_v1_wrapper_process_set_properties(me, init_set_properties);

      if (CAPI_V2_IS_ERROR_CODE_SET(result, CAPI_V2_EUNSUPPORTED))
      {
         MSG(MSG_SSID_QDSP6, DBG_HIGH_PRIO,"Capi_V1_enc_wrapper ignoring unsupported prop error during init");
         result &= ~CAPI_V2_EUNSUPPORTED; //clear UNSUPPORTED prop errors as these are ok.
      }

      if (CAPI_V2_EOK != result)
      {
         return result;
      }
   }

   switch( me->enc_cfg_id )
   {
   case ASM_MEDIA_FMT_G711_ALAW_FS:
   case ASM_MEDIA_FMT_G711_MLAW_FS:
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V2:
   case ASM_MEDIA_FMT_MULTI_CHANNEL_PCM_V3:
   case ASM_MEDIA_FMT_AMRNB_FS:
   case ASM_MEDIA_FMT_AMRWB_FS:
   case ASM_MEDIA_FMT_EVRC_FS:
   case ASM_MEDIA_FMT_EVRCB_FS:
   case ASM_MEDIA_FMT_EVRCWB_FS:
   case ASM_MEDIA_FMT_V13K_FS:
   case ASM_MEDIA_FMT_FR_FS:
   {
      VoiceInitParamsType VInitParams;
      VInitParams.VocTypeID = me->enc_cfg_id;
      VInitParams.VocConfigBlkSize = 0;
      VInitParams.VocConfigBlk = NULL;

      CAPI_Buf_t Params;
      Params.Data = (char*) &VInitParams;
      Params.nActualDataLen = sizeof(VoiceInitParamsType);
      Params.nMaxDataLen = sizeof(VoiceInitParamsType);

      //Initialize CAPI
      int enc_result = init_capi(me, &Params);
      if (ENC_SUCCESS != enc_result)
      {
         result = CAPI_V2_EFAILED;
      }

      break;
   }
   default:
   {
      //other capis init at enc cfg blk
      break;
   }
   }

#if 0 //for encoder we never queried before.
   int val=0;
   int res =  me->capi_v1_ptr->GetParam(eIcapiKCPSRequired, &val);
   if (res != CAPI_V2_EOK)
   {
      val = AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_KPPS;
   }
#endif
   {
      int enc_result = check_raise_kpps_event(me, determine_kpps(me));
      if (ENC_SUCCESS != enc_result)
      {
         result = CAPI_V2_EFAILED;
      }
   }



   {
      int enc_result = check_raise_bw_event(me, AUDIO_ENC_SVC_CAPI_V1_WRAPPER_DEFAULT_CODE_BW, determine_data_bw(me));
      if (ENC_SUCCESS != enc_result)
      {
         result = CAPI_V2_EFAILED;
      }
   }


   if (CAPI_V2_FAILED(result))   return CAPI_V2_EFAILED;

   me->prev_eos_flag = false;

   me->vtbl.vtbl_ptr = &vtbl;

   return CAPI_V2_EOK;
}
