/**
@file MixerSvc_LatencyModuleUtils.cpp
@brief This file defines latency module functions that 
       the audio matrix mixer uses.
 */

/*========================================================================
Copyright (c) 2014 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
 *//*====================================================================== */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/aud/services/static_svcs/matrix_mixer_svc/core/lib/src/MixerSvc_LatencyModuleUtils.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
06/10/2014 aprasad  Created file.
========================================================================== */

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "qurt_elite.h"
#include "Elite.h"
#include "MixerSvc.h"
#include "MixerSvc_MsgHandlers.h"
#include "MixerSvc_OutPortHandler.h"
#include "MixerSvc_Util.h"
#include "adsp_media_fmt.h"
#include "AudioStreamMgr_GetSetBits.h"
#include "MixerSvc_InPortHandler.h"
#include "MixerSvc_LatencyModuleUtils.h"
#include "AudDevMgr_PrivateDefs.h"
#include "MixerSvc_Pspd.h"

static ADSPResult MtMx_CreatePspdLatencyModule(This_t *me, uint32_t unInPortID, uint32_t unOutPortID);
static ADSPResult MtMx_SetLatencyModuleCfg(This_t *me, uint32_t unInPortID, uint32_t unOutPortID);

//TODO: add return parameter to this function.
void MtMx_InOutPortsToCheckReInitLatencyModuleLibrary(This_t *me, uint32_t unInPortID, uint32_t unOutPortID)
{
	MatrixInPortInfoType *pCurrentInPort  = me->inPortParams[unInPortID];
	mt_mx_struct_latency_module_t *pCurrentInputOutputLatencyModule = &(pCurrentInPort->structLatencyModule[unOutPortID]);
	ADSPResult result = ADSP_EOK;   

	// No need to create latency module for zero latency.
	// But when latency module is already created, latency value of zero can be set.
	if ((0 == pCurrentInputOutputLatencyModule->unLatency) 
			&& (FALSE == pCurrentInputOutputLatencyModule->bIsLatencyModuleCreated))
	{
		return;
	}

	//If the o/p port is a native mode port, the ChMixer lib will not be init/used.
	if(FALSE == pCurrentInputOutputLatencyModule->bIsLatencyModuleCreated)
	{
		//Create new PSPD latency module
		result = MtMx_CreatePspdLatencyModule(me, unInPortID, unOutPortID);
		if (ADSP_EOK != result)
		{
			MSG_4(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, latency module creation failed, result=%d. Cont w/o lib.",
					me->mtMxID, unInPortID, unOutPortID, result);
			goto __bailoutMtMxInOutPortsToCheckReInitLatencyModuleLibrary;
		}
		else
		{
			MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, created latency module library", me->mtMxID, unInPortID, unOutPortID);
		}
	}

	//Set the channel configuration through set param
	result = MtMx_SetLatencyModuleCfg(me, unInPortID, unOutPortID);
	if (ADSP_EOK != result)
	{
		MSG_3(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, MtMx_SetLatencyModuleCfg returned failure. Cont w/o lib.",
				me->mtMxID, unInPortID, unOutPortID);
		goto __bailoutMtMxInOutPortsToCheckReInitLatencyModuleLibrary;
	}
	//Successful init
	MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, MtMx_SetLatencyModuleCfg returned success.",
			me->mtMxID, unInPortID, unOutPortID);

	//Set Latency value
	(void)MtMx_SetLatency(me, unInPortID, unOutPortID);

	MSG_3(MSG_SSID_QDSP6, DBG_HIGH_PRIO, "MtMx #%lu: i/p port ID %lu, o/p port ID %lu, MtMx_SetLatency returned success.",
			me->mtMxID, unInPortID, unOutPortID);

	return;

	__bailoutMtMxInOutPortsToCheckReInitLatencyModuleLibrary:
	//Destroy current PSPD latency module (if it exists)
	MtMx_DestroyPspdLatencyModule(me, unInPortID, unOutPortID);
	return;
}

static ADSPResult MtMx_CreatePspdLatencyModule(This_t *me, uint32_t unInPortID, uint32_t unOutPortID)
{
	//This function creates latency module. It assumes that the PSPD thread is already created.
	MatrixInPortInfoType  *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_struct_latency_module_t *pCurrentInputOutputLatencyModule = &(pCurrentInPort->structLatencyModule[unOutPortID]);
	elite_msg_any_t param_msg;	
	ADSPResult result = ADSP_EOK;

	uint32_t unPayloadSize = sizeof(EliteMsg_CustomPspdCfgLatencyModuleInout);
	mt_mx_struct_pspd_t *pCurrentPspd = &(pCurrentInPort->structPspd[unOutPortID]);

	//Create new message and setup payload and secondary OpCode.
	if(ADSP_EOK != (result = elite_msg_create_msg( &param_msg, &unPayloadSize, ELITE_CUSTOM_MSG, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q, 0, 0)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create elite msg with error = %d", result);
		return result;
	}

	EliteMsg_CustomPspdCfgLatencyModuleInout *payload = (EliteMsg_CustomPspdCfgLatencyModuleInout *)param_msg.pPayload;
	payload->unSecOpCode = ELITEMSG_CUSTOM_PSPD_CREATE_LATENCY_MODULE;

	//Fill up params.
	payload->param.bit_width =16;
	payload->param.num_ch = 2;
	payload->param.ch_map[0] = PCM_CHANNEL_L;
	payload->param.ch_map[1] = PCM_CHANNEL_R;
	payload->param.unSampleRate = MT_MX_SAMPLING_RATE_48000;	

	//Send command to PSPD thread and wait for response.
	result = MtMx_PspdSendAndWaitForResp(&param_msg, pCurrentPspd->thread_param.cmdQ, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q);

	if(ADSP_EOK != result)
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed MtMx_CreatePspdLatencyModule; result = %d", result);
		goto __bailoutCreatePspdLatencyModule;
	}

	pCurrentInputOutputLatencyModule->bIsLatencyModuleCreated = TRUE;
	return result;

	__bailoutCreatePspdLatencyModule:
	pCurrentInputOutputLatencyModule->bIsLatencyModuleCreated = FALSE;
	return result;
}

void MtMx_DestroyPspdLatencyModule(This_t *me, uint32_t unInPortID, uint32_t unOutPortID)
{
	//This function destroys the latency module. It assumes that the PSPD thread is already created and not joined yet.
	MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unInPortID];
	elite_msg_any_t param_msg;	
	mt_mx_struct_latency_module_t *pCurrentInputOutputLatencyModule = &(pCurrentInPort->structLatencyModule[unOutPortID]);
	ADSPResult result = ADSP_EOK;

	//Free up the APPI pointer
	if(pCurrentInputOutputLatencyModule->bIsLatencyModuleCreated)
	{
		uint32_t unPayloadSize = sizeof(EliteMsg_CustomPspdCfgLatencyModuleInout);
		mt_mx_struct_pspd_t *pCurrentPspd = &(pCurrentInPort->structPspd[unOutPortID]);

		//Create new message and setup payload and secondary OpCode.
        if(ADSP_EOK != (result = elite_msg_create_msg( &param_msg, &unPayloadSize, ELITE_CUSTOM_MSG, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q, 0, 0)))
		{
			MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create elite msg with error = %d", result);
			return;
		}
		EliteMsg_CustomPspdCfgLatencyModuleInout *payload = (EliteMsg_CustomPspdCfgLatencyModuleInout *)param_msg.pPayload;
		payload->unSecOpCode = ELITEMSG_CUSTOM_PSPD_DESTROY_LATENCY_MODULE;

		//No params to send.

		//Send command to PSPD thread and wait for response.
        (void)MtMx_PspdSendAndWaitForResp(&param_msg, pCurrentPspd->thread_param.cmdQ, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q);
	}
	pCurrentInputOutputLatencyModule->bIsLatencyModuleCreated = FALSE;
}

static ADSPResult MtMx_SetLatencyModuleCfg(This_t *me, uint32_t unInPortID, uint32_t unOutPortID)
{
	ADSPResult result = ADSP_EOK;
	elite_msg_any_t param_msg;
	uint32_t unPayloadSize = sizeof(EliteMsg_CustomPspdCfgLatencyModuleInout);

	MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unInPortID];
	MatrixOutPortInfoType *pCurrentOutPort = me->outPortParams[unOutPortID];
	mt_mx_struct_pspd_t *pCurrentPspd = &(pCurrentInPort->structPspd[unOutPortID]);

	//Create new message and setup payload and secondary OpCode.
	if(ADSP_EOK != (result = elite_msg_create_msg( &param_msg, &unPayloadSize, ELITE_CUSTOM_MSG, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q, 0, 0)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create elite msg with error = %d", result);
		return result;
	}
	EliteMsg_CustomPspdCfgLatencyModuleInout *payload = (EliteMsg_CustomPspdCfgLatencyModuleInout *)param_msg.pPayload;
	payload->unSecOpCode = ELITEMSG_CUSTOM_PSPD_CFG_LATENCY_MODULE_INOUT;

	//Fill up params.
	mt_mx_struct_channel_mixer_t *pCurrentInputOutputChMixer;
	pCurrentInputOutputChMixer = &(pCurrentInPort->structChanMixer[unOutPortID]);
	payload->param.bit_width = (pCurrentInPort->unBitwidth)?(pCurrentInPort->unBitwidth):(16);
	payload->param.unSampleRate = pCurrentOutPort->unSampleRate;		
	if(TRUE == pCurrentInputOutputChMixer->bIsQcomChannelMixerLibCreated)
	{
		payload->param.num_ch = pCurrentOutPort->unNumChannels;
		MtMx_CopyChannelMap(pCurrentOutPort->unChannelMapping, payload->param.ch_map, pCurrentOutPort->unNumChannels);
	}
	else
	{
		payload->param.num_ch = pCurrentInPort->unNumChannels;
		MtMx_CopyChannelMap(pCurrentInPort->unChannelMapping, payload->param.ch_map, pCurrentInPort->unNumChannels);
	}

	//Send command to PSPD thread and wait for response.
	if(ADSP_EOK != (result = MtMx_PspdSendAndWaitForResp(&param_msg, pCurrentPspd->thread_param.cmdQ, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "PSPD channel mixer config command failed with error = %d", result);
		return result;	
	}

	//Update matrix KPPS and/or BW and raise ADSPPM event if needed.
	return(MtMx_SetReqKppsAndBW(me));
}

ADSPResult MtMx_SetLatency(This_t *me, uint32_t unInPortID, uint32_t unOutPortID)
{
	ADSPResult result = ADSP_EOK;
	elite_msg_any_t param_msg;
	uint32_t unPayloadSize = sizeof(EliteMsg_CustomPspdSetLatency);
	MatrixInPortInfoType *pCurrentInPort = me->inPortParams[unInPortID];
	mt_mx_struct_pspd_t *pCurrentPspd = &(pCurrentInPort->structPspd[unOutPortID]);
	mt_mx_struct_latency_module_t *pCurrentInputOutputLatencyModule = &(pCurrentInPort->structLatencyModule[unOutPortID]);

	if ((FALSE == pCurrentInputOutputLatencyModule->bIsLatencyModuleCreated) && (0 == pCurrentInputOutputLatencyModule->unLatency)) 	
	{
		// it is OK to return here. Latency module will be created only when latency value is zero.
		return ADSP_EOK;
	}	

	if ((FALSE == pCurrentInputOutputLatencyModule->bIsLatencyModuleCreated) && (pCurrentInputOutputLatencyModule->unLatency)) 	
	{
		MSG(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Trying to set latency module before latency module is created !!");			
		return ADSP_EFAILED;
	}

	//Create new message and setup payload and secondary OpCode.
	if(ADSP_EOK != (result = elite_msg_create_msg( &param_msg, &unPayloadSize, ELITE_CUSTOM_MSG, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q, 0, 0)))
	{
		MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Failed to create elite msg with error = %d", result);
		return result;
	}
	EliteMsg_CustomPspdSetLatency *payload = (EliteMsg_CustomPspdSetLatency *)param_msg.pPayload;
	payload->unSecOpCode = ELITEMSG_CUSTOM_PSPD_SET_LATENCY;

	//Fill up params.
	payload->unLatency = pCurrentInputOutputLatencyModule->unLatency;

	//Send command to PSPD thread and wait for response.
	return MtMx_PspdSendAndWaitForResp(&param_msg, pCurrentPspd->thread_param.cmdQ, (qurt_elite_queue_t *)pCurrentPspd->cmd_resp_q);
}

