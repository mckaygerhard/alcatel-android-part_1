#ifndef ELITE_INTF_EXTNS_VOCODER_H
#define ELITE_INTF_EXTNS_VOCODER_H

/* ======================================================================== */
/**
@file Elite_intf_extns_vocoder.h

@brief Interface extension header for vocoders

  This file defines the parameters, events and other behaviors associated
  with vocoders
*/

/* =========================================================================
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.(QTI)
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   09/28/15   ka      Initial Version.
   ========================================================================= */
#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/


#include "Elite_CAPI_V2_types.h"

#define INTF_EXTN_VOCODER           (0x00010E68)
/**< Unique identifier to represent custom interface extension for vocoders */

/*------------------------------------------------------------------------------
 * Parameter IDs
 *----------------------------------------------------------------------------*/

#define VOC_PARAM_ENC_DTX           (0x00010e69)
/**< Parameter that identifies if discontinuous transmission is enabled or disabled
     DTX mode provided via command VSM_CMD_ENC_SET_DTX_MODE will be passed to the encoder via this param id.
     Associated data structure: voc_param_enc_dtx_t */

typedef struct voc_param_enc_dtx_t voc_param_enc_dtx_t;

struct voc_param_enc_dtx_t
{
   uint32_t dtx_mode;
   /**< DTX mode */
};

#define VOC_PARAM_ENC_RATE          (0x00010e6a)
/**< Parameter that identifies the rate the encoder should encode at.
     The exact interpretation of the value and valid range depends on the encoder in question.
     Rate provided via command VSM_CMD_ENC_SET_RATE will be passed to the encoder via this param id.
     Associated data structure: voc_param_enc_rate_t */

typedef struct voc_param_enc_rate_t voc_param_enc_rate_t;

struct voc_param_enc_rate_t
{
   uint32_t rate;
   /**< Encoder rate */
};

#define VOC_PARAM_ENC_MINMAX_RATE   (0x00010e6b)
/**< Parameter that identifies the minimum and maximum rate of encoding to be used
     The exact interpretation of the values and valid ranges depend on the encoder in question.
     Rates provided via command VSM_CMD_ENC_SET_MINMAX_RATE will be passed to the encoder via this param id.
     This command may not be supported by all encoders.
     Associated data structure: voc_param_enc_minmax_rate_t */

typedef struct voc_param_enc_minmax_rate_t voc_param_enc_minmax_rate_t;

struct voc_param_enc_minmax_rate_t
{
   uint32_t min_rate;
   /**< Minimum rate */
   uint32_t max_rate;
   /**< Maximum rate */
};

#define VOC_PARAM_ENC_RATE_MOD          (0x00010e6c)
/**< Parameter that identifies the rate the encoder should encode at.
     The exact interpretation of the value and valid range depends on the encoder in question.
     Rate provided via command VSM_CMD_ENC_SET_RATE_MOD will be passed to the encoder via this param id.
     This command may not be supported by all encoders.
     Associated data structure: voc_param_enc_rate_mod_t */

typedef struct voc_param_enc_rate_mod_t voc_param_enc_rate_mod_t;

struct voc_param_enc_rate_mod_t
{
   uint32_t rate_modulation_param;
   /**< Encoder rate modulation command */
};

#define VOC_PARAM_ENC_MODE          (0x00010e6d)
/**< Parameter that identifies the rate and bandwidth the encoder should encode at.
     The exact interpretation of the value and valid range depends on the encoder in question.
     Mode provided via command VSM_CMD_SET_EVS_ENC_OPERATING_MODE will be passed to the encoder via this param id.
     Associated data structure: voc_param_enc_mode_t */

typedef struct voc_param_enc_mode_t voc_param_enc_mode_t;

struct voc_param_enc_mode_t
{
   uint32_t rate;
   /**< Encoder rate */
   uint32_t bandwidth;
   /**< Encoder bandwidth */
};

#define VOC_PARAM_ENC_CHANNEL_AWARE_MODE          (0x00010e6e)
/**< Parameter that identifies setting of channel aware mode
     The exact interpretation of the value and valid range depends on the encoder in question.
     Mode provided via commands VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_ENABLE and
     VSM_CMD_SET_EVS_ENC_CHANNEL_AWARE_MODE_DISABLE will be passed to the encoder via this param id.
     This command may not be supported by all encoders.
     Associated data structure: voc_param_enc_channel_aware_mode_t */

typedef struct voc_param_enc_channel_aware_mode_t voc_param_enc_channel_aware_mode_t;

struct voc_param_enc_channel_aware_mode_t
{
   uint32_t mode;
   /**< Channel aware mode */
   uint8_t fec_offset;
   /**< Forward error correction offset */
   uint8_t fer_rate;
   /**< FER threshold */

};


#define VOC_PARAM_DEC_BEAMR             (0x00010e6f)
/**< Parameter that identifies if BeAMR is to be enabled
     This param is only associated with VSM_MEDIA_TYPE_AMR_NB_MODEM, VSM_MEDIA_TYPE_AMR_NB_IF2 and VSM_MEDIA_TYPE_EAMR
     Associated data structure: voc_param_beamr_t */

typedef struct voc_param_beamr_t voc_param_beamr_t;

struct voc_param_beamr_t
{
   uint32_t beamr_enable;
   /**< BeAMR enable/disable */
};


/*------------------------------------------------------------------------------
 * Events
 *----------------------------------------------------------------------------*/

#define VOC_EVT_OPERATING_MODE_UPDATE  (0x00010e70)
/**< The vocoder raises this event upon initialization as well as when its internal
     operating mode changes. Operating mode represents the actual bandwidth of the signal
     being encoded by the encoder or being outputted by the decoder
     The payload for this event is: voc_evt_operating_mode_update_t
*/

typedef struct voc_evt_operating_mode_update_t voc_evt_operating_mode_update_t;

struct voc_evt_operating_mode_update_t
{
   uint32_t direction;
   /**< Direction of operating mode. See voc_operating_mode_direction_t for valid values */
   uint32_t mode;
   /**< Mode of the vocoder. See voc_operating_mode_t for valid values */
};

typedef enum voc_operating_mode_direction_t
{
   VOC_OPERATING_MODE_DIRECTION_TX = 0,
   /**< Mode change detection from Tx, i.e. encoder */
   VOC_OPERATING_MODE_DIRECTION_RX
   /**< Mode change detection from Rx, i.e. decoder */
}voc_operating_mode_direction_t;

typedef enum voc_operating_mode_t
{
   VOC_OPERATING_MODE_NB = 0x00013086,
   /**< indicates 8kHz vocoder bandwidth mode. */
   VOC_OPERATING_MODE_WB =0x00013087,
   /**< indicates 16kHz vocoder bandwidth mode. */
   VOC_OPERATING_MODE_SWB = 0x00013088,
   /**< indicates 32kHz vocoder bandwidth mode. */
   VOC_OPERATING_MODE_FB = 0x00013089
   /**< indicates 48kHz vocoder bandwidth mode. */
} voc_operating_mode_t;


#define VOC_EVT_DEC_PKT_OVERWRITTEN (0x00010e71)
/**< This event is raised by the decoder when a packet of zero size is provided
     or the packet is invalid. In such cases, the input packet is overwritten with
     an erasure packet by the decoder and this event is raised to indicate the size
     of the overwritten packet
     The payload for this event is: voc_evt_dec_erasure_t
*/

typedef struct voc_evt_dec_pkt_owt_t voc_evt_dec_pkt_owt_t;

struct voc_evt_dec_pkt_owt_t
{
   uint32_t packet_size;
   /**< Size of overwritten packet */
};

#ifdef __cplusplus
}
#endif //__cplusplus
#endif
