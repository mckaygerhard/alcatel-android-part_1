#ifndef ELITE_FWK_EXTNS_CNG_H
#define ELITE_FWK_EXTNS_CNG_H

/* ======================================================================== */
/**
@file Elite_fwk_extns_cng.h

@brief Framework extension header for comfort noise generator

  This file defines the parameters, events and other behaviors associated
  with vocoders
*/

/* =========================================================================
  Copyright (c) 2010-2014 Qualcomm Technologies, Inc.(QTI)
  All rights reserved. Qualcomm Proprietary and Confidential.
  ========================================================================== */

/* =========================================================================
                             Edit History

   when       who     what, where, why
   --------   ---     ------------------------------------------------------
   09/28/15   ka      Initial Version.
   ========================================================================= */
#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/


#include "Elite_CAPI_V2_types.h"

#define FWK_EXTN_CNG                              (0x00010E94)
/**< Unique identifier to represent custom framework extension for CNG */

/*------------------------------------------------------------------------------
 * Parameter IDs
 *----------------------------------------------------------------------------*/

#define VOC_PARAM_CNG_TX_MUTE                     (0x00010E95)
/**< Parameter that identifies if Tx path is muted or not.
     Associated data structure: voc_param_cng_tx_mute_t */

typedef struct voc_param_cng_tx_mute_t voc_param_cng_tx_mute_t;

struct voc_param_cng_tx_mute_t
{
   uint16_t is_mute;
   /**< 0: Not Mute
    *   1: Mute */
};

#define VOC_PARAM_CNG_RX_TTY_DETECTED             (0x00010E96)
/**< Parameter that identifies if Rx TTY is detected or not.
     Associated data structure: voc_param_cng_rx_tty_detected_t */

typedef struct voc_param_cng_rx_tty_detected_t voc_param_cng_rx_tty_detected_t;

struct voc_param_cng_rx_tty_detected_t
{
   uint16_t is_detected;
   /**< 0: Not detected
    *   1: Detected */
};

#define VOC_PARAM_CNG_IS_TTY_MODE_FULL           (0x00010E97)
/**< Parameter that identifies if TTY mode is FULL or not The modes are defined in adsp_vsm_api.h under
 *   vsm_tty_mode_t.
     Associated data structure: voc_param_cng_is_tty_mode_full_t */

typedef struct voc_param_cng_is_tty_mode_full_t voc_param_cng_is_tty_mode_full_t;

struct voc_param_cng_is_tty_mode_full_t
{
   uint16_t is_tty_mode_full;
   /**< 0: TTY mode is not Full.
    *   1: TTY mode is Full. */
};

#ifdef __cplusplus
}
#endif //__cplusplus
#endif //ELITE_FWK_EXTNS_CNG_H
