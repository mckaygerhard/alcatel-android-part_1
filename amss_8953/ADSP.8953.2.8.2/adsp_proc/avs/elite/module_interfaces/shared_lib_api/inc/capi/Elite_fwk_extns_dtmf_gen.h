#ifndef ELITE_FWK_EXTNS_DTMF_GEN_H
#define ELITE_FWK_EXTNS_DTMF_GEN_H

/**
  @file Elite_fwk_extns_dtmf_gen.h

  @brief Parameters required to be implemented by DTMF Gen module

  This file defines a framework extension for DTMF Gen modules.
*/

/*==============================================================================
  Copyright (c) 2016 Qualcomm Technologies, Inc.(QTI)
  All rights reserved.
  Qualcomm Technologies Proprietary and Confidential.
==============================================================================*/

/*==============================================================================
  Edit History

  $Header: //components/rel/avs.adsp/2.7/elite/module_interfaces/shared_lib_api/inc/capi/Elite_fwk_extns_dtmf_gen.h#2 $

  when        who       what, where, why
  --------    ---       -------------------------------------------------------
  01/08/16    shridhar  Creation.
==============================================================================*/

#ifdef __cplusplus
extern "C" {
#endif /*__cplusplus*/

/*------------------------------------------------------------------------------
 * Include Files
 *----------------------------------------------------------------------------*/
#include "qurt_elite_types.h"

#define FWK_EXTN_DTMF_GEN                                 0x00010E9D
/**< Unique identifier to represent that module supports DTMF Detection
     Framework extension */

#define FWK_EXTN_PARAM_ID_DTMF_GEN_SET_DTMF_GENERATION    0x00010E9E
/**  Parameter used to send the DTMF tone generation payload to the module
     Associated data structure: vsm_set_dtmf_generation_t */

/*------------------------------------------------------------------------------
 * Events
 *----------------------------------------------------------------------------*/

#define FWK_EXTN_EVENT_ID_DTMF_GEN_END_DETECTED           0x00010E9F
/**< Parameter for raising detection status event. End point of this
     event is DSP service.
     @see CAPI_V2_EVENT_DATA_TO_DSP_SERVICE in Elite_CAPI_V2_events.h
     Even has no payload
*/

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /* #ifndef ELITE_FWK_EXTNS_DTMF_GEN_H */
