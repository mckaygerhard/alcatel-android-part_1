/*
  Copyright (C) 2013-2015 QUALCOMM Technologies Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary

  $Header: //components/rel/avs.adsp/2.7/vsd/common/cvd/cvd_utils/src/cvd_devcfg_parser.c#11 $
  $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#ifndef WINSIM
  #include "DALSys.h"
  #include "DALSysTypes.h"
#endif /* !WINSIM */

#include "msg.h"
#include "apr_errcodes.h"

#include "vss_private_if.h"

/****************************************************************************
 * GLOBALS                                                                  *
 ****************************************************************************/

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

/****************************************************************************
 * DEFINITIONS                                                              *
 ****************************************************************************/

/****************************************************************************
 * VARIABLE DECLARATIONS                                                    *
 ****************************************************************************/

static cvd_devcfg_clock_table_t* cvd_devcfg_parser_clock_table = NULL;
static cvd_devcfg_voice_use_case_na_values_t* cvd_devcfg_parser_voice_use_case_na_values = NULL;
static uint32_t cvd_devcfg_parser_cycles_per_thousand_instr_packet;
static uint32_t cvd_devcfg_parser_mmpm_sleep_latency_us;
static uint32_t cvd_devcfg_parser_max_q6_core_clock_mhz = 729; /* Set default in case parser failed. */

/* Set default Mininum core clock to 300 Mhz in case parser failed. */
static uint32_t cvd_devcfg_parser_min_q6_core_clock = 300;

#ifndef WINSIM
static cvd_devcfg_mmpm_core_info_t* cvd_devcfg_parser_mmpm_core_info = NULL;
static cvd_devcfg_mmpm_bw_table_t* cvd_devcfg_parser_bw_table = NULL;
static uint32_t cvd_devcfg_parser_mmpm_cpp = 1; /* Set default to 1 in case parser failed. */
/* Set the default minimal BW incase parser failed. */
static uint64_t cvd_devcfg_parser_max_bw = ( 90 << 20 );/* 90 MBps. */

#endif /* !WINSIM */

/****************************************************************************
 * INTERNAL ROUTINES                                                        *
 ****************************************************************************/
#ifndef WINSIM

static uint32_t cvd_devcfg_extract_min_q6_core_clock( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  uint32_t clock_table_size = 0;
  uint32_t clock_level;

  if ( NULL == cvd_devcfg_parser_clock_table )
  {
    return APR_EFAILED;
  }

  clock_table_size = cvd_devcfg_parser_clock_table->num_clock_levels;

  /* Init with clock value at 0th index for comparison. */
  cvd_devcfg_parser_min_q6_core_clock = 
    cvd_devcfg_parser_clock_table->clock_levels[0].core_floor_clock_hz;

  /* Loop around all the clock values in the cvd_devcfg clock table and cache 
     the minimal q6 core clock. */
  for( index = 0; index < clock_table_size; index++ )
  {
    clock_level =
      cvd_devcfg_parser_clock_table->clock_levels[index].core_floor_clock_hz;
    if( clock_level < cvd_devcfg_parser_min_q6_core_clock )
    {
      cvd_devcfg_parser_min_q6_core_clock = clock_level;
    }
  }

  /* Convert to MHz. */
  cvd_devcfg_parser_min_q6_core_clock /= 1000000;

  return rc;
}

static uint32_t cvd_devcfg_extract_max_bw( void )
{
  uint32_t rc = APR_EOK;
  uint32_t index = 0;
  uint32_t bw_table_size = 0;
  uint64_t bw;

  if ( NULL == cvd_devcfg_parser_bw_table )
  {
    return APR_EFAILED;
  }

  bw_table_size = cvd_devcfg_parser_bw_table->num_bw_values;

  /* Init with bw value at 0th index for comparison. */
  cvd_devcfg_parser_max_bw = 
    cvd_devcfg_parser_bw_table->bw_values[0].bw_val.bwValue.busBwValue.bwBytePerSec;

  /* Loop around all the bw values in the cvd_devcfg bw table and cache 
     the maximum bw. */
  for( index = 0; index < bw_table_size; index++ )
  {
    bw = cvd_devcfg_parser_bw_table->bw_values[index].bw_val.bwValue.busBwValue.bwBytePerSec;
    if( bw > cvd_devcfg_parser_max_bw )
    {
      cvd_devcfg_parser_max_bw = bw;
    }
  }

  return rc;
}

#endif

/****************************************************************************
 * ROUTINES                                                                 *
 ****************************************************************************/

APR_INTERNAL int32_t cvd_devcfg_parser_init ( void )
{
#if defined( WINSIM )

  /* Not needed on the simulator. */
  return APR_EOK;

#else

  int32_t rc = APR_EOK;
  DALResult dal_rc;
  DALSYS_PROPERTY_HANDLE_DECLARE( dev_prop_handle );
  DALSYSPropertyVar clk_tbl_prop;
  DALSYSPropertyVar use_case_na_val_prop;
  DALSYSPropertyVar mmpm_core_info_prop;
  DALSYSPropertyVar mmpm_bw_requirement_prop;
  DALSYSPropertyVar mmpm_sleep_latency_prop;
  DALSYSPropertyVar cycles_per_thousand_instr_packet_prop;
  DALSYSPropertyVar mmpm_cpp_prop;
  DALSYSPropertyVar max_q6_core_clock_mhz;

  MSG( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "====== cvd_devcfg_parser_init()======" );

  for ( ;; )
  { /* Read all the CVD device configurations from the devcfg xml file. */
    dal_rc = DALSYS_GetDALPropertyHandleStr(
               CVD_DEVCFG_DEV_ID_NAME, dev_prop_handle );
    if ( dal_rc != DAL_SUCCESS )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_init(): Failed to get property handle for " \
                                              "device ID %s.",
                                              CVD_DEVCFG_DEV_ID_NAME );
      rc = APR_EFAILED;
      break;
    }

    dal_rc = DALSYS_GetPropertyValue(
               dev_prop_handle, CVD_DEVCFG_CLOCK_TABLE_PROP_NAME, 0,
               &clk_tbl_prop );
    if ( dal_rc != DAL_SUCCESS )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_init(): Failed to get property value for " \
                                              "property %s.",
                                              CVD_DEVCFG_CLOCK_TABLE_PROP_NAME );
      rc = APR_EFAILED;
      break;
    }

    dal_rc = DALSYS_GetPropertyValue(
               dev_prop_handle, CVD_DEVCFG_VOICE_USE_CASE_NA_VALUES_PROP_NAME, 0,
               &use_case_na_val_prop );
    if ( dal_rc != DAL_SUCCESS )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_init(): Failed to get property value for " \
                                              "property %s.",
                                              CVD_DEVCFG_VOICE_USE_CASE_NA_VALUES_PROP_NAME );
      rc = APR_EFAILED;
      break;
    }

    dal_rc = DALSYS_GetPropertyValue(
               dev_prop_handle, CVD_DEVCFG_MMPM_CORE_INFO_PROP_NAME, 0,
               &mmpm_core_info_prop );
    if ( dal_rc != DAL_SUCCESS )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_init(): Failed to get property value for " \
                                              "property %s.",
                                              CVD_DEVCFG_MMPM_CORE_INFO_PROP_NAME );
      rc = APR_EFAILED;
      break;
    }

    dal_rc = DALSYS_GetPropertyValue(
               dev_prop_handle, CVD_DEVCFG_MMPM_BW_REQUIREMENT_PROP_NAME, 0,
               &mmpm_bw_requirement_prop );
    if ( dal_rc != DAL_SUCCESS )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_init(): Failed to get property value for " \
                                              "property %s.",
                                              CVD_DEVCFG_MMPM_BW_REQUIREMENT_PROP_NAME );
      rc = APR_EFAILED;
      break;
    }

    dal_rc = DALSYS_GetPropertyValue(
               dev_prop_handle, CVD_DEVCFG_MMPM_SLEEP_LATENCY_PROP_NAME, 0,
               &mmpm_sleep_latency_prop );
    if ( dal_rc != DAL_SUCCESS )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_init(): Failed to get property value for " \
                                              "property %s.",
                                              CVD_DEVCFG_MMPM_SLEEP_LATENCY_PROP_NAME );
      rc = APR_EFAILED;
      break;
    }

    dal_rc = DALSYS_GetPropertyValue(
               dev_prop_handle,
               CVD_DEVCFG_CYCLES_PER_THOUSAND_INSTR_PACKET_PROP_NAME, 0,
               &cycles_per_thousand_instr_packet_prop );
    if ( dal_rc != DAL_SUCCESS )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_init(): Failed to get property value for " \
                                              "property %s.",
                                              CVD_DEVCFG_CYCLES_PER_THOUSAND_INSTR_PACKET_PROP_NAME );
      rc = APR_EFAILED;
      break;
    }

    dal_rc = DALSYS_GetPropertyValue(
               dev_prop_handle,
               CVD_DEVCFG_MMPM_CYCLES_PER_INSTR_PACKET_PROP_NAME, 0,
               &mmpm_cpp_prop );
    if ( dal_rc != DAL_SUCCESS )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_init(): Failed to get property value for " \
                                              "property %s.",
                                              CVD_DEVCFG_MMPM_CYCLES_PER_INSTR_PACKET_PROP_NAME );
      rc = APR_EFAILED;
      break;
    }

    dal_rc = DALSYS_GetPropertyValue(
               dev_prop_handle,
               CVD_DEVCFG_MAX_Q6_CORE_CLOCK_MHZ, 0,
               &max_q6_core_clock_mhz );
    if ( dal_rc != DAL_SUCCESS )
    {
      MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_init(): Failed to get property value for " \
                                              "property %s.",
                                              CVD_DEVCFG_MAX_Q6_CORE_CLOCK_MHZ );
      rc = APR_EFAILED;
      break;
    }

    cvd_devcfg_parser_clock_table =
      ( ( cvd_devcfg_clock_table_t* ) clk_tbl_prop.Val.pStruct );

    cvd_devcfg_parser_voice_use_case_na_values =
      ( ( cvd_devcfg_voice_use_case_na_values_t* ) use_case_na_val_prop.Val.pStruct );

    cvd_devcfg_parser_mmpm_core_info =
      ( ( cvd_devcfg_mmpm_core_info_t* ) mmpm_core_info_prop.Val.pStruct );

    cvd_devcfg_parser_bw_table =
      ( ( cvd_devcfg_mmpm_bw_table_t* ) mmpm_bw_requirement_prop.Val.pStruct );

    cvd_devcfg_parser_mmpm_sleep_latency_us = mmpm_sleep_latency_prop.Val.dwVal;

    cvd_devcfg_parser_cycles_per_thousand_instr_packet =
      cycles_per_thousand_instr_packet_prop.Val.dwVal;

    cvd_devcfg_parser_mmpm_cpp =
      mmpm_cpp_prop.Val.dwVal;

    cvd_devcfg_parser_max_q6_core_clock_mhz =
      max_q6_core_clock_mhz.Val.dwVal;

    ( void ) cvd_devcfg_extract_min_q6_core_clock();
    ( void ) cvd_devcfg_extract_max_bw();

    break;
  }

  return rc;

#endif /* WINSIM */

}

APR_INTERNAL int32_t cvd_devcfg_parser_deinit ( void )
{
  return APR_EOK;
}

#ifndef WINSIM

APR_INTERNAL int32_t cvd_devcfg_parser_get_clock_and_bus_config (
  cvd_devcfg_parser_voice_use_case_t* use_case,
  cvd_devcfg_parser_clock_and_bus_config_t* ret_clock_and_bus_config
)
{
  uint32_t clk_level_idx;
  uint32_t bw_level_idx;
  uint32_t use_case_idx;
  uint32_t num_clk_levels;
  uint32_t num_bw_levels;
  uint32_t num_supported_use_cases;
  bool_t clock_level_use_case_found = FALSE;
  bool_t bw_level_use_case_found = FALSE;
  cvd_devcfg_clock_level_t* clk_level;
  cvd_devcfg_mmpm_bw_value_t* bw_level;
  cvd_devcfg_supported_voice_use_case_t* supported_use_case;
  cvd_devcfg_voice_use_case_na_values_t* na_val;

  if ( ( use_case == NULL ) || ( ret_clock_and_bus_config == NULL ) )
  {
    return APR_EBADPARAM;
  }

  if ( ( cvd_devcfg_parser_clock_table == NULL ) || ( cvd_devcfg_parser_voice_use_case_na_values == NULL ) )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_get_clock_and_bus_config(): " \
                                           "Invalid input table" );
    return APR_EBADPARAM;
  }

#ifndef WINSIM
  if ( ( cvd_devcfg_parser_mmpm_core_info == NULL ) || ( cvd_devcfg_parser_bw_table == NULL ) )
  {
    MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_get_clock_and_bus_config(): " \
                                           "Invalid mmpm info" );
    return APR_EBADPARAM;
  }
#endif /* !WINSIM */

  num_clk_levels = cvd_devcfg_parser_clock_table->num_clock_levels;
  na_val = cvd_devcfg_parser_voice_use_case_na_values;

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvd_devcfg_parser_get_clock_and_bus_config(): " \
                                         "voice_sessions %d, nb_streams %d, wb_streams %d",
                                         use_case->num_voice_sessions,
                                         use_case->num_nb_streams,
                                         use_case->num_wb_streams );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvd_devcfg_parser_get_clock_and_bus_config(): " \
                                         "swb_streams %d, fb_plus_streams %d, nb_vocprocs %d",
                                         use_case->num_swb_streams,
                                         use_case->num_fb_plus_streams,
                                         use_case->num_nb_vocprocs );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvd_devcfg_parser_get_clock_and_bus_config(): " \
                                         "wb_vocprocs %d, swb_vocprocs %d, fb_plus_vocprocs %d",
                                         use_case->num_wb_vocprocs,
                                         use_case->num_swb_vocprocs,
                                         use_case->num_fb_plus_vocprocs );

  MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvd_devcfg_parser_get_clock_and_bus_config(): " \
                                         "tx_topology_id 0x%08X, rx_topology_id 0x%08X, media_id 0x%04X",
                                         use_case->tx_topology_id,
                                         use_case->rx_topology_id,
                                         use_case->media_id );

  MSG_1( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvd_devcfg_parser_get_clock_and_bus_config(): " \
                                         "vfr_mode 0x%08X",
                                         use_case->vfr_mode );

  /* Find the first clock level in supported use-case table in cvd_devcfg.c that
   * can support the client specified voice use case.
   */
  for ( clk_level_idx = 0; clk_level_idx < num_clk_levels; ++clk_level_idx )
  {
    clk_level = &cvd_devcfg_parser_clock_table->clock_levels[ clk_level_idx ];
    num_supported_use_cases = clk_level->num_supported_use_cases;

    for ( use_case_idx = 0; use_case_idx < num_supported_use_cases; ++use_case_idx )
    {
      supported_use_case = &clk_level->supported_use_cases[ use_case_idx ];

      if ( ( ( supported_use_case->max_num_voice_sessions ==
               na_val->max_num_voice_sessions ) ||
             ( use_case->num_voice_sessions <=
               supported_use_case->max_num_voice_sessions ) ) &&

           ( ( supported_use_case->max_num_nb_streams ==
               na_val->max_num_nb_streams ) ||
             ( use_case->num_nb_streams <=
               supported_use_case->max_num_nb_streams ) ) &&

           ( ( supported_use_case->max_num_wb_streams ==
               na_val->max_num_wb_streams ) ||
             ( use_case->num_wb_streams <=
               supported_use_case->max_num_wb_streams ) ) &&

          ( ( supported_use_case->max_num_swb_streams ==
              na_val->max_num_swb_streams ) ||
            ( use_case->num_swb_streams <=
              supported_use_case->max_num_swb_streams ) ) &&

          ( ( supported_use_case->max_num_fb_plus_streams ==
              na_val->max_num_fb_plus_streams ) ||
            ( use_case->num_fb_plus_streams <=
              supported_use_case->max_num_fb_plus_streams ) ) &&

           ( ( supported_use_case->max_num_nb_vocprocs ==
               na_val->max_num_nb_vocprocs ) ||
             ( use_case->num_nb_vocprocs <=
               supported_use_case->max_num_nb_vocprocs ) ) &&

           ( ( supported_use_case->max_num_wb_vocprocs ==
               na_val->max_num_wb_vocprocs ) ||
             ( use_case->num_wb_vocprocs <=
               supported_use_case->max_num_wb_vocprocs ) ) &&

          ( ( supported_use_case->max_num_swb_vocprocs ==
              na_val->max_num_swb_vocprocs ) ||
            ( use_case->num_swb_vocprocs <=
              supported_use_case->max_num_swb_vocprocs ) ) &&

          ( ( supported_use_case->max_num_fb_plus_vocprocs ==
              na_val->max_num_fb_plus_vocprocs ) ||
            ( use_case->num_fb_plus_vocprocs <=
              supported_use_case->max_num_fb_plus_vocprocs ) ) &&

           ( ( supported_use_case->tx_topology_id ==
               na_val->tx_topology_id ) ||
             ( use_case->tx_topology_id ==
               supported_use_case->tx_topology_id ) ) &&

           ( ( supported_use_case->rx_topology_id ==
               na_val->rx_topology_id ) ||
             ( use_case->rx_topology_id ==
               supported_use_case->rx_topology_id ) ) &&

          ( ( supported_use_case->media_id ==
              na_val->media_id ) ||
            ( use_case->media_id ==
              supported_use_case->media_id ) ) &&

           ( ( supported_use_case->vfr_mode == na_val->vfr_mode ) ||
             ( use_case->vfr_mode == supported_use_case->vfr_mode ) )
	    )
      {
        ret_clock_and_bus_config->core_floor_clock_hz =
          clk_level->core_floor_clock_hz;

        ret_clock_and_bus_config->cycles_per_thousand_instr_packets =
          cvd_devcfg_parser_cycles_per_thousand_instr_packet;

        ret_clock_and_bus_config->sleep_latency_us = cvd_devcfg_parser_mmpm_sleep_latency_us;

        MSG_3( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvd_devcfg_parser_get_clock_and_bus_config(): " \
                                               "core clock_hz %d, cycles_per_thousand_instr_packets %d, sleep_latency_us %d",
                                               ret_clock_and_bus_config->core_floor_clock_hz,
                                               ret_clock_and_bus_config->cycles_per_thousand_instr_packets,
                                               ret_clock_and_bus_config->sleep_latency_us );
        clock_level_use_case_found = TRUE;
        break;
      }
    }

    if( clock_level_use_case_found == TRUE )
    {
      break;
    }
  }

  /* Find the first bus bandwidth in supported use-case table in cvd_devcfg.c that
   * can support the client specified voice use case.
   */
  num_bw_levels = cvd_devcfg_parser_bw_table->num_bw_values;

  for ( bw_level_idx = 0; bw_level_idx < num_bw_levels; ++bw_level_idx )
  {
    bw_level = &cvd_devcfg_parser_bw_table->bw_values[ bw_level_idx ];
    num_supported_use_cases = bw_level->num_supported_use_cases;

    for ( use_case_idx = 0; use_case_idx < num_supported_use_cases; ++use_case_idx )
    {
      supported_use_case = &bw_level->supported_use_cases[ use_case_idx ];

      if ( ( ( supported_use_case->max_num_voice_sessions ==
               na_val->max_num_voice_sessions ) ||
             ( use_case->num_voice_sessions <=
               supported_use_case->max_num_voice_sessions ) ) &&

             ( ( supported_use_case->max_num_nb_streams ==
                 na_val->max_num_nb_streams ) ||
               ( use_case->num_nb_streams <=
                 supported_use_case->max_num_nb_streams ) ) &&

             ( ( supported_use_case->max_num_wb_streams ==
                 na_val->max_num_wb_streams ) ||
               ( use_case->num_wb_streams <=
                 supported_use_case->max_num_wb_streams ) ) &&

             ( ( supported_use_case->max_num_swb_streams ==
                 na_val->max_num_swb_streams ) ||
               ( use_case->num_swb_streams <=
                 supported_use_case->max_num_swb_streams ) ) &&

             ( ( supported_use_case->max_num_fb_plus_streams ==
                 na_val->max_num_fb_plus_streams ) ||
               ( use_case->num_fb_plus_streams <=
                 supported_use_case->max_num_fb_plus_streams ) ) &&

             ( ( supported_use_case->max_num_nb_vocprocs ==
                 na_val->max_num_nb_vocprocs ) ||
               ( use_case->num_nb_vocprocs <=
                 supported_use_case->max_num_nb_vocprocs ) ) &&

             ( ( supported_use_case->max_num_wb_vocprocs ==
                 na_val->max_num_wb_vocprocs ) ||
               ( use_case->num_wb_vocprocs <=
                 supported_use_case->max_num_wb_vocprocs ) ) &&

             ( ( supported_use_case->max_num_swb_vocprocs ==
                 na_val->max_num_swb_vocprocs ) ||
               ( use_case->num_swb_vocprocs <=
                 supported_use_case->max_num_swb_vocprocs ) ) &&

             ( ( supported_use_case->max_num_fb_plus_vocprocs ==
                 na_val->max_num_fb_plus_vocprocs ) ||
               ( use_case->num_fb_plus_vocprocs <=
                 supported_use_case->max_num_fb_plus_vocprocs ) ) &&

             ( ( supported_use_case->tx_topology_id ==
                 na_val->tx_topology_id ) ||
               ( use_case->tx_topology_id ==
                 supported_use_case->tx_topology_id ) ) &&

             ( ( supported_use_case->rx_topology_id ==
                 na_val->rx_topology_id ) ||
               ( use_case->rx_topology_id ==
                 supported_use_case->rx_topology_id ) ) &&

             ( ( supported_use_case->media_id ==
                 na_val->media_id ) ||
               ( use_case->media_id ==
                 supported_use_case->media_id ) ) &&

             ( ( supported_use_case->vfr_mode == na_val->vfr_mode ) ||
               ( use_case->vfr_mode == supported_use_case->vfr_mode ) ) )
      {

        ret_clock_and_bus_config->bw_requirement.numOfBw = 1;
        ret_clock_and_bus_config->bw_requirement.pBandWidthArray = &bw_level->bw_val;

        MSG_2( MSG_SSID_DFLT, MSG_LEGACY_HIGH, "cvd_devcfg_parser_get_clock_and_bus_config(): " \
                                               "Bandwidth values: Bytes per sec (%d), Usage percentage (%d)",
                                               ( ret_clock_and_bus_config->bw_requirement.pBandWidthArray->bwValue.busBwValue.bwBytePerSec >> 20 ),
                                               ret_clock_and_bus_config->bw_requirement.pBandWidthArray->bwValue.busBwValue.usagePercentage );

        bw_level_use_case_found = TRUE;
        break;
      }
    }

    if( bw_level_use_case_found == TRUE )
    {
      break;
    }
  }

  if( ( clock_level_use_case_found == FALSE ) || ( bw_level_use_case_found == FALSE ) )
  {
    if ( clock_level_use_case_found == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_get_clock_and_bus_config(): Get clock config failed" );
    }

    if ( bw_level_use_case_found == FALSE )
    {
      MSG( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvd_devcfg_parser_get_clock_and_bus_config(): Get bus config failed" );
    }

    return APR_EFAILED;
  }

  return APR_EOK;
}

APR_INTERNAL int32_t cvd_devcfg_parser_get_mmpm_core_info (
  cvd_devcfg_parser_mmpm_core_info_t* ret_mmpm_core_info
)
{
  if ( ret_mmpm_core_info == NULL )
  {
    return APR_EBADPARAM;
  }

  ret_mmpm_core_info->core_id = cvd_devcfg_parser_mmpm_core_info->core_id;
  ret_mmpm_core_info->instance_id = cvd_devcfg_parser_mmpm_core_info->instance_id;

  return APR_EOK;
}

APR_INTERNAL int32_t cvd_devcfg_parser_get_mmpm_cpp (
  uint32_t* ret_mmpm_cpp
)
{
  if ( ret_mmpm_cpp == NULL )
  {
    return APR_EBADPARAM;
  }

  *ret_mmpm_cpp = cvd_devcfg_parser_mmpm_cpp;

  return APR_EOK;
}

APR_INTERNAL int32_t cvd_devcfg_parser_get_max_q6_core_clock(
  uint32_t* ret_max_q6_core_clock
)
{
  if ( ret_max_q6_core_clock == NULL )
  {
    return APR_EBADPARAM;
  }

  *ret_max_q6_core_clock = cvd_devcfg_parser_max_q6_core_clock_mhz;

  return APR_EOK;
}

APR_INTERNAL int32_t cvd_devcfg_parser_get_min_q6_core_clock(
  uint32_t* ret_min_q6_core_clock
)
{
  if ( ret_min_q6_core_clock == NULL )
  {
    return APR_EBADPARAM;
  }

  *ret_min_q6_core_clock = cvd_devcfg_parser_min_q6_core_clock;

  return APR_EOK;
}

APR_INTERNAL int32_t cvd_devcfg_parser_get_max_bw(
  uint64_t* ret_max_bw
)
{

  if ( ret_max_bw == NULL )
  {
    return APR_EBADPARAM;
  }

  *ret_max_bw = cvd_devcfg_parser_max_bw;

  return APR_EOK;
}

#endif /* !WINSIM */

