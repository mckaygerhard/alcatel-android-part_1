/*
  Copyright (C) 2014-2015 QUALCOMM Technologies Incorporated.
  All rights reserved.
  Qualcomm Technologies, Inc. Confidential and Proprietary

  $Header: //components/rel/avs.adsp/2.7/vsd/common/cvd/cvs/src/cvs_mailbox_timer.c#5 $
  $Author: pwbldsvc $
*/

/****************************************************************************
 * INCLUDE HEADER FILES                                                     *
 ****************************************************************************/

#include "msg.h"
#include "err.h"
#include "mmstd.h"

#ifndef WINSIM
#include "avtimer_drv_api.h"
#endif /* WINSIM */

#include "apr_comdef.h"
#include "apr_errcodes.h"
#include "apr_memmgr.h"
#include "apr_objmgr.h"
#include "apr_lock.h"
#include "apr_event.h"
#include "apr_thread.h"
#include "apr_misc.h"

#include "vocsvc_avtimer_api.h"
#include "cvd_task.h"
#include "cvs_mailbox_timer_api_i.h"

/* This file contains the implementation for the timer used for mailbox vocoder
 * packet exchange. The actual timer used is product requirement specific. The
 * current timer used is AVTimer.
 *
 * Current limitation when using AVTimer. The following timer APIs in this file
 * can only be used after a voice call is started (i.e. modem or APPS client
 * issued start voice to CVD).
 * - cvs_mailbox_timer_get_time
 * - cvs_mailbox_timer_start_absolute
 * - cvs_mailbox_timer_create
 * - cvs_mailbox_timer_destroy
 * This is because CVD only votes for the AVTimer HW resources after a voice
 * call is started and release the AVTimer HW resources after a voice call is
 * ended in order to allow ADSP power collapse when there is no voice call.
 */

/****************************************************************************
 * DEFINES                                                                  *
 ****************************************************************************/

#define CVS_MAILBOX_TIMER_HEAP_SIZE_V ( 512 )

#define CVS_MAILBOX_TIMER_HANDLE_TOTAL_BITS_V ( 16 )

#define CVS_MAILBOX_TIMER_HANDLE_INDEX_BITS_V ( 3 ) /* 3 bits = 8 handles. */

#define CVS_MAILBOX_TIMER_MAX_TIMER_OBJECTS_V ( 1 << CVS_MAILBOX_TIMER_HANDLE_INDEX_BITS_V )

#define CVS_MAILBOX_TIMER_PANIC_ON_ERROR( rc ) \
  { if ( rc ) { ERR_FATAL( "Error[%d]", rc, 0, 0 ); } }

#define CVS_MAILBOX_TIMER_EXPIRY_SAFETY_MARGIN_US ( 20 )
  /**<
    * A timer with an expiry time that is within 20 microseconds from the
    * current time will be considered as expired. There is no practical need
    * to configure a timer to expire within then next 20 microseconds. This
    * safety margin is to tolerate AVTimer jitters (e.g. AVTimer fires slightly
    * early than configured).
    */

#define CVS_MAILBOX_TIMER_EXPIRY_SIGNAL_MASK ( 1 )

#define CVS_MAILBOX_TIMER_EXIT_SIGNAL_MASK ( 2 )


/****************************************************************************
 * DEFINITIONS                                                              *
 ****************************************************************************/

typedef struct cvs_mailbox_timer_object_t
{
  uint32_t handle;
  cvs_mailbox_timer_cb_fn_t client_cb_fn;
  void* client_data;
  uint64_t expiry_time_us;
}
  cvs_mailbox_timer_object_t;

typedef struct cvs_mailbox_timer_obj_list_item_t
{
  apr_list_node_t link;

  uint32_t timer_handle;
}
  cvs_mailbox_timer_obj_list_item_t;

typedef enum cvs_mailbox_timer_thread_state_enum_t
{
  CVS_MAILBOX_TIMER_THREAD_STATE_ENUM_INIT,
  CVS_MAILBOX_TIMER_THREAD_STATE_ENUM_READY,
  CVS_MAILBOX_TIMER_THREAD_STATE_ENUM_EXIT
}
  cvs_mailbox_timer_thread_state_enum_t;


/****************************************************************************
 * VARIABLE DECLARATIONS                                                    *
 ****************************************************************************/

/* Lock Management */
static apr_lock_t cvs_mailbox_timer_objmgr_lock;
static apr_lock_t cvs_mailbox_timer_thread_lock;

/* Heap Management */
static uint8_t cvs_mailbox_timer_heap_pool[ CVS_MAILBOX_TIMER_HEAP_SIZE_V ];
static apr_memmgr_type cvs_mailbox_timer_heapmgr;

/* Object Management */
static apr_objmgr_object_t cvs_mailbox_timer_object_table[ CVS_MAILBOX_TIMER_MAX_TIMER_OBJECTS_V ];
static apr_objmgr_t cvs_mailbox_timer_objmgr;

/* Timer tracking */
static cvs_mailbox_timer_obj_list_item_t cvs_mailbox_timer_obj_list_pool[ CVS_MAILBOX_TIMER_MAX_TIMER_OBJECTS_V ];
static apr_list_t cvs_mailbox_timer_obj_free_q;
static apr_list_t cvs_mailbox_timer_obj_active_q;

/* Worker Thread Management */
static apr_event_t cvs_mailbox_timer_thread_create_event;
static apr_thread_t cvs_mailbox_timer_thread_handle;
static cvs_mailbox_timer_thread_state_enum_t cvs_mailbox_timer_thread_state = CVS_MAILBOX_TIMER_THREAD_STATE_ENUM_INIT;
static uint8_t cvs_mailbox_timer_task_stack[ CVS_MAILBOX_TIMER_TASK_STACK_SIZE ];

static uint32_t cvs_mailbox_timer_num_timer_created = 0;
static bool_t cvs_mailbox_timer_is_cur_expiry_time_set = FALSE;
static uint64_t cvs_mailbox_timer_cur_expiry_time_us;

#ifndef WINSIM
static qurt_elite_timer_t cvs_mailbox_timer_qurt_elite_timer;
static qurt_elite_channel_t cvs_mailbox_timer_channel;
static qurt_elite_signal_t cvs_mailbox_timer_expiry_signal;
static qurt_elite_signal_t cvs_mailbox_timer_exit_signal;
#endif /* WINSIM */


/****************************************************************************
 * API ROUTINES                                                             *
 ****************************************************************************/

static void cvs_mailbox_timer_objmgr_lock_fn ( void )
{
  ( void ) apr_lock_enter( cvs_mailbox_timer_objmgr_lock );
}

static void cvs_mailbox_timer_objmgr_unlock_fn ( void )
{
  ( void ) apr_lock_leave( cvs_mailbox_timer_objmgr_lock );
}

static int32_t cvs_mailbox_timer_create_timer_object (
  cvs_mailbox_timer_object_t** ret_timer_obj
)
{
  int32_t rc;
  uint32_t checkpoint = 0;
  cvs_mailbox_timer_object_t* timer_obj;
  apr_objmgr_object_t* objmgr_obj;

  for ( ;; )
  {
    if ( ret_timer_obj == NULL )
    {
      rc = APR_EBADPARAM;
      break;
    }

    { /* Allocate memory for the new timer object. */
      timer_obj = apr_memmgr_malloc( &cvs_mailbox_timer_heapmgr,
                                      sizeof( cvs_mailbox_timer_object_t ) );

      if ( timer_obj == NULL )
      {
        rc = APR_ENORESOURCE;
        break;
      }
      checkpoint = 1;
    }

    { /* Allocate a new handle for the timer object. */
      rc = apr_objmgr_alloc_object( &cvs_mailbox_timer_objmgr, &objmgr_obj );
      if ( rc ) break;
      checkpoint = 2;
    }

    /* Link the timer object to the handle */
    objmgr_obj->any.ptr = timer_obj;

    { /* Initialize the timer object. */
      mmstd_memset( timer_obj, 0xFD, sizeof( cvs_mailbox_timer_object_t ) );
      timer_obj->handle = objmgr_obj->handle;
    }

    *ret_timer_obj = timer_obj;

    return APR_EOK;
  }

  switch ( checkpoint )
  {
  case 2:
    ( void ) apr_objmgr_free_object( &cvs_mailbox_timer_objmgr, objmgr_obj->handle );
    /*-fallthru */

  case 1:
    {
      apr_memmgr_free( &cvs_mailbox_timer_heapmgr, timer_obj );
    }
    /*-fallthru */

  default:
    break;
  }

  return rc;
}

static int32_t cvs_mailbox_timer_free_timer_object (
  cvs_mailbox_timer_object_t* timer_obj
)
{
  if ( timer_obj == NULL )
  {
    return APR_EBADPARAM;
  }

  /* Free the object memory and object handle. */
  ( void ) apr_objmgr_free_object( &cvs_mailbox_timer_objmgr, timer_obj->handle );

  apr_memmgr_free( &cvs_mailbox_timer_heapmgr, timer_obj );

  return APR_EOK;
}

static int32_t cvs_mailbox_timer_get_timer_object (
  uint32_t handle,
  cvs_mailbox_timer_object_t** ret_timer_obj
)
{
  int32_t rc;
  apr_objmgr_object_t* objmgr_obj;
  cvs_mailbox_timer_object_t* timer_obj;

  for ( ;; )
  {
    if ( ret_timer_obj == NULL )
    {
      rc = APR_EBADPARAM;
      break;
    }

    rc = apr_objmgr_find_object( &cvs_mailbox_timer_objmgr, handle, &objmgr_obj );
    if ( rc ) break;

    timer_obj = ( ( cvs_mailbox_timer_object_t* ) objmgr_obj->any.ptr );
    if ( timer_obj == NULL )
    {
      rc = APR_EFAILED;
      break;
    }

    *ret_timer_obj = timer_obj;

    return APR_EOK;
  }

  return rc;
}

static int32_t cvs_mailbox_timer_handle_expiry( void )
{
  int32_t rc;
  uint64_t cur_time_us = 0;
  cvs_mailbox_timer_obj_list_item_t* pivot_timer_obj_list_item;
  cvs_mailbox_timer_obj_list_item_t* cur_timer_obj_list_item;
  cvs_mailbox_timer_object_t* timer_obj;
  uint64_t next_expiry_time_us = 0;
  bool_t is_next_expiry_time_found = FALSE;

  ( void ) cvs_mailbox_timer_get_time( &cur_time_us );

  ( void ) apr_lock_enter( cvs_mailbox_timer_thread_lock );

  pivot_timer_obj_list_item =
    ( ( cvs_mailbox_timer_obj_list_item_t* ) &cvs_mailbox_timer_obj_active_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &cvs_mailbox_timer_obj_active_q,
                            ( ( apr_list_node_t* ) pivot_timer_obj_list_item ),
                            ( ( apr_list_node_t** ) &cur_timer_obj_list_item ) );
    if ( rc ) break;

    rc = cvs_mailbox_timer_get_timer_object(
           cur_timer_obj_list_item->timer_handle, &timer_obj );
    if ( rc )
    {
      CVS_MAILBOX_TIMER_PANIC_ON_ERROR( rc );
      break;
    }

    if ( timer_obj->expiry_time_us <=
         ( cur_time_us + CVS_MAILBOX_TIMER_EXPIRY_SAFETY_MARGIN_US ) )
    {
      timer_obj->client_cb_fn( timer_obj->client_data );

      ( void ) apr_list_delete(
                 &cvs_mailbox_timer_obj_active_q, &cur_timer_obj_list_item->link );

      ( void ) apr_list_add_tail(
                 &cvs_mailbox_timer_obj_free_q, &cur_timer_obj_list_item->link );
    }
    else
    {
      pivot_timer_obj_list_item = cur_timer_obj_list_item;

      if ( is_next_expiry_time_found == FALSE )
      {
        is_next_expiry_time_found = TRUE;
        next_expiry_time_us = timer_obj->expiry_time_us;
      }
      else
      if ( timer_obj->expiry_time_us < next_expiry_time_us )
      {
        next_expiry_time_us = timer_obj->expiry_time_us;
      }
    }
  }

  if ( is_next_expiry_time_found == TRUE )
  {
    cvs_mailbox_timer_is_cur_expiry_time_set = TRUE;
    cvs_mailbox_timer_cur_expiry_time_us = next_expiry_time_us;

#ifndef WINSIM
    ( void ) avtimer_drv_start_absolute(
               &cvs_mailbox_timer_qurt_elite_timer,
               cvs_mailbox_timer_cur_expiry_time_us );
#endif /* WINSIM */
  }
  else
  {
    cvs_mailbox_timer_is_cur_expiry_time_set = FALSE;
  }

  ( void ) apr_lock_leave( cvs_mailbox_timer_thread_lock );

  return APR_EOK;
}

#ifndef WINSIM
static int32_t cvs_mailbox_timer_worker_thread_fn ( void* param )
{
  uint32_t signal_mask;

  qurt_elite_channel_init( &cvs_mailbox_timer_channel );
  ( void ) qurt_elite_signal_init( &cvs_mailbox_timer_expiry_signal );
  ( void ) qurt_elite_signal_init( &cvs_mailbox_timer_exit_signal );

  ( void ) qurt_elite_channel_add_signal(
             &cvs_mailbox_timer_channel,
             &cvs_mailbox_timer_expiry_signal,
             CVS_MAILBOX_TIMER_EXPIRY_SIGNAL_MASK );

  ( void ) qurt_elite_channel_add_signal(
             &cvs_mailbox_timer_channel,
             &cvs_mailbox_timer_exit_signal,
             CVS_MAILBOX_TIMER_EXIT_SIGNAL_MASK );

  cvs_mailbox_timer_thread_state = CVS_MAILBOX_TIMER_THREAD_STATE_ENUM_READY;

  ( void ) apr_event_signal( cvs_mailbox_timer_thread_create_event );

  while ( 1 )
  {
    signal_mask = qurt_elite_channel_wait(
                    &cvs_mailbox_timer_channel,
                    ( CVS_MAILBOX_TIMER_EXPIRY_SIGNAL_MASK | CVS_MAILBOX_TIMER_EXIT_SIGNAL_MASK ) );

    if ( signal_mask & CVS_MAILBOX_TIMER_EXPIRY_SIGNAL_MASK )
    {
      qurt_elite_signal_clear( &cvs_mailbox_timer_expiry_signal );

      ( void ) cvs_mailbox_timer_handle_expiry( );
    }

    if ( signal_mask & CVS_MAILBOX_TIMER_EXIT_SIGNAL_MASK )
    {
      qurt_elite_signal_clear( &cvs_mailbox_timer_exit_signal );

      /* Exit the thread. */
      break;
    }
  }

  qurt_elite_signal_deinit( &cvs_mailbox_timer_exit_signal );
  qurt_elite_signal_deinit( &cvs_mailbox_timer_expiry_signal );
  qurt_elite_channel_destroy( &cvs_mailbox_timer_channel );

  cvs_mailbox_timer_thread_state = CVS_MAILBOX_TIMER_THREAD_STATE_ENUM_EXIT;

  return APR_EOK;
}
#endif /* WINSIM */

static int32_t cvs_mailbox_timer_add_new_timer_into_active_list (
  uint32_t timer_handle
)
{
  int32_t rc;
  cvs_mailbox_timer_obj_list_item_t* timer_obj_list_item;

  ( void ) apr_lock_enter( cvs_mailbox_timer_thread_lock );

  /* Get a free timer obj list item. */
  rc = apr_list_remove_head( &cvs_mailbox_timer_obj_free_q,
                             ( ( apr_list_node_t** ) &timer_obj_list_item ) );
  if ( rc )
  {
    ( void ) apr_lock_leave( cvs_mailbox_timer_thread_lock );
    return APR_ENORESOURCE;
  }

  timer_obj_list_item->timer_handle = timer_handle;

  ( void ) apr_list_add_tail(
             &cvs_mailbox_timer_obj_active_q, &timer_obj_list_item->link );

  ( void ) apr_lock_leave( cvs_mailbox_timer_thread_lock );

  return APR_EOK;
}

static int32_t cvs_mailbox_timer_remove_timer_from_active_list (
  uint32_t timer_handle
)
{
  int32_t rc;
  bool_t is_found = FALSE;
  cvs_mailbox_timer_obj_list_item_t* timer_obj_list_item;

  ( void ) apr_lock_enter( cvs_mailbox_timer_thread_lock );

  timer_obj_list_item =
    ( ( cvs_mailbox_timer_obj_list_item_t* ) &cvs_mailbox_timer_obj_active_q.dummy );

  for ( ;; )
  {
    rc = apr_list_get_next( &cvs_mailbox_timer_obj_active_q,
                            ( ( apr_list_node_t* ) timer_obj_list_item ),
                            ( ( apr_list_node_t** ) &timer_obj_list_item ) );
    if ( rc ) break;

    if ( timer_obj_list_item->timer_handle == timer_handle )
    {
      is_found = TRUE;

      ( void ) apr_list_delete(
                 &cvs_mailbox_timer_obj_active_q, &timer_obj_list_item->link );

      ( void ) apr_list_add_tail(
                 &cvs_mailbox_timer_obj_free_q, &timer_obj_list_item->link );

      break;
    }
  }

  ( void ) apr_lock_leave( cvs_mailbox_timer_thread_lock );

  if ( is_found == FALSE )
  {
    rc = APR_EALREADY;
  }
  else
  {
    rc = APR_EOK;
  }

  return rc;
}

APR_INTERNAL int32_t cvs_mailbox_timer_create (
  cvs_mailbox_timer_cb_fn_t timer_cb,
  void* client_data,
  uint32_t* ret_timer_handle
)
{
  int32_t rc;
  cvs_mailbox_timer_object_t* timer_obj;
#ifndef WINSIM
  ADSPResult avtimer_rc;
#endif

  for ( ;; )
  {
    if ( ( timer_cb == NULL ) || ( ret_timer_handle == NULL ) )
    {
      rc = APR_EBADPARAM;
      break;
    }

    rc = cvs_mailbox_timer_create_timer_object( &timer_obj );
    if ( rc ) break;

    timer_obj->client_data = client_data;
    timer_obj->client_cb_fn = timer_cb;
    timer_obj->expiry_time_us = 0;

    *ret_timer_handle = timer_obj->handle;

#ifndef WINSIM
    {
      /* Create AVTimer when there is a need (i.e. upon the first timer
       * creation).
       */
      if ( cvs_mailbox_timer_num_timer_created == 0 )
      {
        avtimer_rc = avtimer_drv_create(
                       &cvs_mailbox_timer_qurt_elite_timer, QURT_ELITE_TIMER_ONESHOT_ABSOLUTE,
                       &cvs_mailbox_timer_expiry_signal );
        if ( ADSP_FAILED( avtimer_rc ) )
        {
#ifndef SIM_DEFINED
          MSG_1( MSG_SSID_DFLT, MSG_LEGACY_ERROR, "cvs_mailbox_timer_create(): Failed to create AVTimer rc = 0x%08X",
                 avtimer_rc );
#endif
          rc = APR_EFAILED;

          break;
        }
      }

      cvs_mailbox_timer_num_timer_created++;
    }
#endif /* WINSIM */

    break;
  }

  return rc;
}

APR_INTERNAL int32_t cvs_mailbox_timer_destroy (
  uint32_t timer_handle
)
{
  int32_t rc;
  cvs_mailbox_timer_object_t* timer_obj;

  ( void ) cvs_mailbox_timer_remove_timer_from_active_list( timer_handle );

  rc = cvs_mailbox_timer_get_timer_object( timer_handle, &timer_obj );
  if ( rc ) return rc;

  ( void ) cvs_mailbox_timer_free_timer_object( timer_obj );

#ifndef WINSIM
  cvs_mailbox_timer_num_timer_created--;

  if ( cvs_mailbox_timer_num_timer_created == 0 )
  { /* Destroy the avtimer when there are no more timers. */
    ( void ) avtimer_drv_cancel_oneshot( &cvs_mailbox_timer_qurt_elite_timer );
    ( void ) avtimer_drv_delete( &cvs_mailbox_timer_qurt_elite_timer );
  }
#endif

  return APR_EOK;
}

APR_INTERNAL int32_t cvs_mailbox_timer_start_absolute (
  uint32_t timer_handle,
  uint64_t expiry_time_us
)
{
  int32_t rc;
  cvs_mailbox_timer_object_t* timer_obj;
#ifndef WINSIM
  uint64_t cur_time_us = 0;
#endif

  rc = cvs_mailbox_timer_get_timer_object( timer_handle, &timer_obj );
  if ( rc ) return rc;

#if defined( WINSIM )
  timer_obj->client_cb_fn( timer_obj->client_data );
#else
  for ( ;; )
  {
    /* Remove the timer from active list if it is in the active list. */
    ( void ) cvs_mailbox_timer_remove_timer_from_active_list( timer_handle );

    ( void ) cvs_mailbox_timer_get_time( &cur_time_us );

    if ( expiry_time_us <=
         ( cur_time_us + CVS_MAILBOX_TIMER_EXPIRY_SAFETY_MARGIN_US ) )
    {
      /* Issue callback to client immediately if the expiry time has passed. */
      timer_obj->client_cb_fn( timer_obj->client_data );
      break;
    }

    timer_obj->expiry_time_us = expiry_time_us;

    ( void ) cvs_mailbox_timer_add_new_timer_into_active_list( timer_handle );

    ( void ) apr_lock_enter( cvs_mailbox_timer_thread_lock );

    if ( cvs_mailbox_timer_is_cur_expiry_time_set == TRUE )
    {
      if ( expiry_time_us < cvs_mailbox_timer_cur_expiry_time_us )
      {
        cvs_mailbox_timer_cur_expiry_time_us = expiry_time_us;

        ( void ) avtimer_drv_start_absolute(
                   &cvs_mailbox_timer_qurt_elite_timer,
                   cvs_mailbox_timer_cur_expiry_time_us );
      }
    }
    else
    {
      cvs_mailbox_timer_is_cur_expiry_time_set = TRUE;
      cvs_mailbox_timer_cur_expiry_time_us = expiry_time_us;

      ( void ) avtimer_drv_start_absolute(
                 &cvs_mailbox_timer_qurt_elite_timer,
                 cvs_mailbox_timer_cur_expiry_time_us );
    }

    ( void ) apr_lock_leave( cvs_mailbox_timer_thread_lock );

    break;
  }
#endif /*WINSIM */

  return APR_EOK;
}

APR_INTERNAL int32_t cvs_mailbox_timer_cancel_absolute (
  uint32_t timer_handle
)
{
  int32_t rc;

  rc = cvs_mailbox_timer_remove_timer_from_active_list( timer_handle );

  return rc;
}

APR_INTERNAL int32_t cvs_mailbox_timer_get_time (
  uint64_t* ret_time_us
)
{
  int32_t rc = APR_EOK;

#if defined( WINSIM )
  *ret_time_us = 0;
#else
  rc = vocsvc_avtimer_get_time(
         ( vocsvc_avtimer_timestamp_t* ) ret_time_us );
#endif

  return rc;
}

/* Assume this function is called only by a single thread - CVS thread. */
APR_INTERNAL int32_t cvs_mailbox_timer_init ( void )
{
  uint32_t timer_obj_count;
#ifndef WINSIM
  int32_t rc;
#endif
  { /* Initialize the locks. */
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &cvs_mailbox_timer_objmgr_lock );
    ( void ) apr_lock_create( APR_LOCK_TYPE_MUTEX, &cvs_mailbox_timer_thread_lock );
  }

  { /* Initialize the custom heap. */
    apr_memmgr_init_heap( &cvs_mailbox_timer_heapmgr,
                          ( ( void* ) &cvs_mailbox_timer_heap_pool ),
                          sizeof( cvs_mailbox_timer_heap_pool ), NULL, NULL );
  }

  { /* Initialize the object manager. */
    apr_objmgr_setup_params_t params;

    params.table = cvs_mailbox_timer_object_table;
    params.total_bits = CVS_MAILBOX_TIMER_HANDLE_TOTAL_BITS_V;
    params.index_bits = CVS_MAILBOX_TIMER_HANDLE_INDEX_BITS_V;
    params.lock_fn = cvs_mailbox_timer_objmgr_lock_fn;
    params.unlock_fn = cvs_mailbox_timer_objmgr_unlock_fn;
    ( void ) apr_objmgr_construct( &cvs_mailbox_timer_objmgr, &params );
  }

  { /* Initialize the timer tracking list. */
    ( void ) apr_list_init_v2(
               &cvs_mailbox_timer_obj_free_q, NULL, NULL );

    for ( timer_obj_count = 0; timer_obj_count < CVS_MAILBOX_TIMER_MAX_TIMER_OBJECTS_V; ++timer_obj_count )
    {
      ( void ) apr_list_init_node( ( apr_list_node_t* ) &cvs_mailbox_timer_obj_list_pool[ timer_obj_count ] );
      ( void ) apr_list_add_tail(
                 &cvs_mailbox_timer_obj_free_q,
                 ( ( apr_list_node_t* ) &cvs_mailbox_timer_obj_list_pool[ timer_obj_count ] ) );
    }

    ( void ) apr_list_init_v2(
               &cvs_mailbox_timer_obj_active_q, NULL, NULL );
  }
#ifndef WINSIM
  { /* Create the worker thread. */
    cvs_mailbox_timer_thread_state = CVS_MAILBOX_TIMER_THREAD_STATE_ENUM_INIT;

    rc = apr_event_create( &cvs_mailbox_timer_thread_create_event );
    CVS_MAILBOX_TIMER_PANIC_ON_ERROR( rc );

    ( void ) apr_thread_create(
               &cvs_mailbox_timer_thread_handle,
               CVS_MAILBOX_TIMER_TASK_NAME, CVS_MAILBOX_TIMER_TASK_PRIORITY, 
               cvs_mailbox_timer_task_stack, CVS_MAILBOX_TIMER_TASK_STACK_SIZE,
               cvs_mailbox_timer_worker_thread_fn, NULL );

    /* Wait for the worker thread to be created. */
    ( void ) apr_event_wait( cvs_mailbox_timer_thread_create_event );

    rc = apr_event_destroy( cvs_mailbox_timer_thread_create_event );
    CVS_MAILBOX_TIMER_PANIC_ON_ERROR( rc );
  }
#endif

  return APR_EOK;
}

/* Assume this function is called only by a single thread - CVS thread. */
APR_INTERNAL int32_t cvs_mailbox_timer_deinit ( void )
{
#ifndef WINSIM
  { /* Destroy the worker thread. */
    qurt_elite_signal_send( &cvs_mailbox_timer_exit_signal );

    do
    {
      ( void ) apr_misc_sleep( 1000000 ); /* Sleep for 1ms. */
    }
    while( cvs_mailbox_timer_thread_state != CVS_MAILBOX_TIMER_THREAD_STATE_ENUM_EXIT );

    ( void ) apr_thread_destroy( cvs_mailbox_timer_thread_handle );
  }
#endif

  { /* Release the timer obj tracking list. */
    ( void ) apr_list_destroy( &cvs_mailbox_timer_obj_active_q );
    ( void ) apr_list_destroy( &cvs_mailbox_timer_obj_free_q );
  }

  { /* Release the object management. */
    ( void ) apr_objmgr_destruct( &cvs_mailbox_timer_objmgr );
  }

  { /* Release the lock. */
    ( void ) apr_lock_destroy( cvs_mailbox_timer_thread_lock );
    ( void ) apr_lock_destroy( cvs_mailbox_timer_objmgr_lock );
  }

  return APR_EOK;
}

