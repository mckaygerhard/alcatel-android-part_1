/**
@file capi_v2_aanc.h

@brief CAPI V2 API wrapper for AANC lib

*/

/*========================================================================
$Header: //components/rel/avs.adsp/2.7/afe/algos/aanc/capiv2/inc/capi_v2_aanc.h#4 $

Edit History

when       who     what, where, why
--------   ---     -------------------------------------------------------
4/4/2014   rv       Created
==========================================================================*/

/*-----------------------------------------------------------------------
   Copyright (c) 2012-2015 Qualcomm  Technologies, Inc.  All Rights Reserved.
   Qualcomm Technologies Proprietary and Confidential.
-----------------------------------------------------------------------*/

#ifndef CAPI_V2_AANC_H_
#define CAPI_V2_AANC_H_


#if defined(__cplusplus)
extern "C" {
#endif // __cplusplus

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "Elite_CAPI_V2.h"
#include "adsp_error_codes.h"
#include "mmdefs.h"

/*----------------------------------------------------------------------------
 * Function Declarations
 * -------------------------------------------------------------------------*/

capi_v2_err_t capi_v2_aanc_get_static_properties (
   capi_v2_proplist_t *init_set_properties,
   capi_v2_proplist_t *static_properties);


capi_v2_err_t capi_v2_aanc_init (
   capi_v2_t*              _pif,
   capi_v2_proplist_t      *init_set_properties);


#if defined(__cplusplus)
}
#endif // __cplusplus

#endif /* CAPI_V2_AANC_H_ */
