/*========================================================================
Copyright (c) 2013 QUALCOMM Technologies, Inc. (QTI).  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government, Diversion contrary to U.S. law prohibited.
=====================================---================================= */

/*========================================================================
Edit History

$Header: //components/rel/avs.adsp/2.7/afe/drivers/hw/avtimer/inc/avtimer_hal.h#3 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
12/31/10   kk      Created file.
========================================================================== */
/**
@file avtimer_hal.h

@brief This file contains avtimer hardware abstraction layer.
*/

#ifndef AVTIMER_HAL_H
#define AVTIMER_HAL_H

/* =======================================================================
INCLUDE FILES FOR MODULE
========================================================================== */
#include "adsp_error_codes.h"
#include "common.h"
#include <stdint.h>    //for uint32_t etc. define

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

#define AVTIMER_HW_VERSION_2        0x30040000    //8x62 (avtimer only), 8084 (avtimer + atime), 9x35 (avtimer only), 
#define AVTIMER_HW_VERSION_2_1      0x30040001    //8902 (avtimer + atime1 + atime2)

#define AVTIMER_HW_TICK_FREQ_27MHZ          27000000
#define AVTIMER_HW_TICK_FREQ_19_2MHZ        19200000

/**
 * For avtimer hw instance define
 * Starting from avtimer hw version 0x30050000, there are two hw instances:
 * I.  U_AVTIMER [generate STC]
 * II. U_A_TIME  [source from pull-able PLL for avsync]
 */
typedef enum 
{
   AVTIMER_HW_INSTANCE_INDEX_INVALID = -1,
   AVTIMER_HW_INSTANCE_INDEX_0 = 0,     //U_AVTIMER [generate STC]
   AVTIMER_HW_INSTANCE_INDEX_1,         //U_A_TIME1  [source from pull-able PLL for avsync]
   AVTIMER_HW_INSTANCE_INDEX_2,         //U_A_TIME2  [source from pull-able PLL for avsync, 8092 only]
   AVTIMER_HW_INSTANCE_INDEX_MAX,
} avtimer_hw_index;

/**
 * For avtimer hw instance define
 * Starting from avtimer hw version 0x30050000, there are two hw instances:
 * I.  U_AVTIMER [generate STC]
 * II. U_A_TIME  [source from pull-able PLL for avsync]
 */
typedef enum 
{
   AVTIMER_HW_MODE_INVALID = -1,
   AVTIMER_HW_MODE_N_ALPAH = 0,    //Legacy mode
   AVTIMER_HW_MODE_L_MN,
   AVTIMER_HW_MODE_QCOUNTER,
   AVTIMER_HW_MODE_MAX,
} avtimer_hw_mode;


typedef struct
{
   uint32 avtimer_base_phys_addr;
   uint32 avtimer_base_virt_addr;
   uint32 root_clk;
   uint32 avt_offset;       // avtimer reg offset LPASS_AVTIMER_HW_VERSION
   uint32 atime1_offset;
   uint32 atime2_offset;
   uint32 audsync_offset;   // aud sync reg offset LPASS_AUD_SYNC_CTL

#ifdef DEBUG_AVTIMER_HAL
   uint32 l2vic_base_virt_addr;
   uint32 lcc_base_virt_addr;
#endif
   uint32 avtimer_l2_int_0_num;
#ifdef DEBUG_AVTIMER_HAL
   uint32 avtimer_l2_int_0_num_in_slice;
#endif
   uint32 hw_revision;

   void* avtimer_hal_func_def_ptr;
   void* avsync_hal_func_def_ptr;
} avtimer_hw_cfg_t;   


#define AVTIMER_HW_INDEX_0_MODE     AVTIMER_HW_MODE_L_MN       //AVTIMER_HW_INSTANCE_INDEX_0
#define AVTIMER_HW_INDEX_1_MODE     AVTIMER_HW_MODE_L_MN       //AVTIMER_HW_INSTANCE_INDEX_1
#define AVTIMER_HW_INDEX_2_MODE     AVTIMER_HW_MODE_L_MN       //AVTIMER_HW_INSTANCE_INDEX_2

#include "avtimer_hal_func_defs.h"

/**
Initialize avtimer hw. Register q6ss_sirc0 interrupt(int 23).
 * @param[out] version Version of the implementation of Audio HAL.
 * @param[in]  avtimer_base_addr Base virtual address of the avtimer registes.
 * @param[in]  l2vic_base_addr Base virtual address of the L2VIC registes.
 * @param[in]  lcc_base_addr Base virtual address of the LCC registes.


@return
 ADSPResult error code.
*/
ADSPResult hal_avtimer_hw_init(avtimer_hw_cfg_t *avt_hw_cfg);

static inline ADSPResult hal_avtimer_hw_enable(avtimer_hw_index idx, avtimer_hw_mode hw_mode)
{
   return avtimer_hal_func_def_ptr_g->hal_avtimer_hw_enable_f(idx, hw_mode);
}

/**
Deinitialize avtimer hw. 

@return
 ADSPResult error code.
*/
static inline ADSPResult hal_avtimer_hw_disable(avtimer_hw_index idx, avtimer_hw_mode hw_mode)
{
   return avtimer_hal_func_def_ptr_g->hal_avtimer_hw_disable_f(idx, hw_mode);
}

/**
return the tick frequency 

@return
 uint32_t 
*/
static inline uint32_t hal_avtimer_get_hw_tick_frequency(void)
{
   uint32_t tick_freq = 0;
   
   if(AVTIMER_HW_INDEX_0_MODE == AVTIMER_HW_MODE_L_MN)
   {
      tick_freq = AVTIMER_HW_TICK_FREQ_27MHZ;
   }
   else if( AVTIMER_HW_INDEX_0_MODE == AVTIMER_HW_MODE_QCOUNTER)
   {

      tick_freq = AVTIMER_HW_TICK_FREQ_19_2MHZ;
   }
   
   return tick_freq;
}

/**
convert the tick count to time in us

@return
 uint64_t 
*/
static inline uint64_t hal_avtimer_convert_hw_tick_to_time(uint64_t  tick_count)
{
   uint64_t  timestamp;

   
   if(AVTIMER_HW_INDEX_0_MODE == AVTIMER_HW_MODE_L_MN)
   {
      timestamp = tick_count/27;  //27 MHz tick

   }
   else if( AVTIMER_HW_INDEX_0_MODE == AVTIMER_HW_MODE_QCOUNTER)
   {
      timestamp = (tick_count * 10)/192; //Qcounter mode (19.2 MHz tick)

   }
  
   return timestamp;
}


/**
program int0(avtimer_int0) with the match value. Interrupt(Int0)
will be generated when the timer count matches the 
int0_match_value 

@param[in] int0_match_value match value @ which int0 will be generated
           It is always based on 1 MHz ticks

@return
 none.
*/
static inline void hal_avtimer_prog_hw_timer_int0(avtimer_hw_index idx, avtimer_hw_mode hw_mode, uint64_t int0_match_value)
{
   return avtimer_hal_func_def_ptr_g->hal_avtimer_prog_hw_timer_int0_f(idx, hw_mode, int0_match_value);
}

/**
program int1(avtimer_int1) with the match value. Interrupt(Int1)
will be generated when the timer count matches the 
int1_match_value 

@param[in] int1_match_value match value @ which int1 will be generated
           It is always based on 1 MHz ticks

@return
 none.
*/
static inline void hal_avtimer_prog_hw_timer_int1(avtimer_hw_index idx, avtimer_hw_mode hw_mode, uint64_t int1_match_value)
{
   return avtimer_hal_func_def_ptr_g->hal_avtimer_prog_hw_timer_int1_f(idx, hw_mode, int1_match_value);
}

/**
program resmod(avtimer_resmod) with the resmod_value. Whenever 
the resolution counter go past the resmod_value, timer count is 
incremented by 1. 
 
@param[in] resmod_value value for which timer count is 
      incremented by 1.

@return
 none.
*/
static inline void hal_avtimer_prog_hw_timer_resmod(avtimer_hw_index idx, uint32_t resmod_value)
{
   return avtimer_hal_func_def_ptr_g->hal_avtimer_prog_hw_timer_resmod_f(idx, resmod_value);
}


/**
get hw timer tick current value 
 
 
@return
 returns current timer count value.
*/
static inline uint64_t hal_avtimer_get_current_hw_timer_tick(avtimer_hw_index idx, avtimer_hw_mode hw_mode)
{
   return avtimer_hal_func_def_ptr_g->hal_avtimer_get_current_hw_timer_tick_f(idx, hw_mode);
}


/** @} */ /* end_addtogroup avtimer_hal */

#ifdef __cplusplus
}
#endif //__cplusplus

#endif // #ifndef AVTIMER_HAL_H

