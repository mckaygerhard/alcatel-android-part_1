/*==============================================================================
$Header: //components/rel/avs.adsp/2.7/afe/drivers/hw/devcfg/config/lpasshwio_devcfg_8992.c#10 $
$DateTime: 2016/01/22 01:18:44 $
$Author: pwbldsvc $
$Change: 9769601 $
$Revision: #10 $

FILE: lpasshwio_devcfg_8996.c 

DESCRIPTION: 8996 Device Config

Copyright 2014 QUALCOMM Technologies, Inc. (QTI).
All Rights Reserved.
QUALCOMM Proprietary/GTDR
==============================================================================*/

/*=====================================================================
 Includes
 ======================================================================*/
#include "lpasshwio_devcfg.h"

HwdLpassPropertyType lpass_prop ={0xFE04F000}; // LPASS hw version reg base addr


lpasshwio_prop_lpaif_struct_t lpaif_prop = {
   0xFE0C0000,  // lpaif reg base addr LPASS_LPAIF_VERSION
   (128*1024),  // lpaif reg page size
   {0xFE0C4000, //LPASS_LPAIF_I2S_CTL0
    5,           // inf_type_cnt ( The total number below, how many interface, max is 5 )
   {PRIMARY_MI2S,SECONDARY_MI2S,TERTIARY_MI2S,QUATERNARY_MI2S,SPEAKER_I2S}, 
   {4,4,4,8,2}, // maximum # of channel for audio if.
   {DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,DATA_LINE_SINK},
   SUPPORTED},  // i2s hw revision}
   {0xFE0C0010, //LPASS_LPAIF_PCM_CTL0
    2,           // inf_type_cnt ( The total number below, how many interface, max is 5 )
   {PRIMARY_PCM,SECONDARY_PCM, 0, 0}, 
   {4,4,0,0}, // maximum # of channel for audio if.
   {DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT, 0, 0}, 
    SUPPORTED},  // pcm hw revision}

   {0,           //LPASS_LPAIF_PCM_CTL0
    0,           // inf_type_cnt ( The total number below, how many interface, max is 5 )
   {0, 0, 0, 0}, 
   {0, 0, 0, 0}, // maximum # of channel for audio if.
   {0, 0, 0, 0}, 
    NOT_SUPPORTED},  // tdm hw revision}
    
   {0xFD9A0000, //hdmiAudHwRegBase
    (4*1024),  // hdmiAudHwRegSize
    SUPPORTED},  
   {0,            // mux reg base LPASS_LPAIF_PCM_I2S_SEL0
    0,           // # of interfaces that  is shared the output with mux
   {0,0,0,0},    // 1:1 mapping for i2s and pcm  interface  
    NOT_SUPPORTED},     
    0x30080000,  // hw revision
};


lpasshwio_prop_afe_dma_struct_t audioif_dma_prop = {
   0xFE0C0000, //  lpaif reg base addr
   (64*1024),  //  lpaif reg page size
   0xFE0CF000,  // dma offset LPASS_LPAIF_IRQ_EN0 : interrupt
   0xFE0D2000,  // dma offset LPASS_LPAIF_RDDMA_CTLa
   0xFE0D8000,  // dma offset LPASS_LPAIF_WRDMA_CTLa
   0xFE0D2028,  // stc rddma offset   LPASS_LPAIF_RDDMA_STC_LSB0
   0xFE0D8028,  // stc wrdma offset LPASS_LPAIF_WRDMA_STC_LSB0
   5,   // sink DMA channel count
   4,   // source DMA channel count
   19,  // lpaif intr irq #
   2,   // interrupt line #
   0x30080000   //  i2s hw revision
};

lpasshwio_prop_afe_dma_struct_t hdmiout_dma_prop = {
   0,      // hdmi output reg base addr
   0,     // hdmi output reg page size
   0,      // not requried 
   0,      // not required 
   0,     // not requried 
   0,     // not required 
   0,      // not required
   0,   // sink DMA channel count
   0,   // source DMA channel count
   0,  // l2vic id for hdmi interrupt
   0,   // there is no interrupt host
   0   //  hdmiout hw revision
};

HwdAvtimerPropertyType avtimer_prop = {
         0xFE09C000,  // baseRegAddr: base address of av timers LPASS_LPASS_SYNC_WRAPPER
         0x1000,      // regSize: register page size of av timers
         19200000,    // rootClk:
         0xFE09C000,  // LPASS_AVTIMER_HW_VERSION
         0,            // atime1
         0,            // atime2
         0,            // LPASS_AUD_SYNC_CTL
         50,           // isrHookPinNum: avtimer intr irq#
         0x30040000    // av timer hardware version
         };

HwdResamplerPropertyType resampler_prop = {
           0xFE0B0000,   // baseRegAddr: base address of resampler
           0x4000,       // regSize: hw resampler reg page size
           53,           // isrHookPinNum: hw resampler intr irq #
           0x20000000    // hw_revision: resampler hw version
           };    

lpasshwio_vfr_prop_t vfr_prop = {
        0xFE03C000,     // VFR reg base address
        0x9000,         // VFR register page size
        0xFE040000,      // LPASS_VFR_IRQ_MUX_CTL_1
        0xFE042000,      // LPASS_GP_IRQ_TS_MUX_CTL_0
        0xFE043000,      // LPASS_GP_IRQ_TS_MUX_CTL_1
        0x10000000,     // VFR hw revision
        0x1,            // VFR hw latching version
        0x2,            // Number of VFR timestamp mux
         2,             // Number of General Purpose timestamp mux
        {51, 124}, // l2vic_num[]: Available L2VIC # for VFR interrupts
        {{0x0, 0x1},   // vfr_src_prop[0]: Mux ctrl for VFR src sel, VFR src is hw-supported or not
        {0x7, 0x1}}       // vfr_src_prop[1]
    };

lpasshwio_prop_riva_struct_t afe_riva_prop = {0, 0, 0, 0, 0, 0, 0, 0};

lpasshwio_prop_slimbus_struct_t afe_slimbus_prop = {
  1,   // hw revision, 0-not supported
  1    // Indicates if hw latching support for AV-timer present
}; 

HwdMidiPropertyType midi_prop = {0,0, 0, 0, 0,0 };
lpasshwio_prop_spdif_struct_t spdiftx_prop = {0, 0, 0, 0, 0, 0, 0};

lpasshwio_prop_hdmi_output_struct_t hdmi_output_prop = {
                                                  0x0,  // hdmi output reg base addr
                                                  0x0,  // hdmi output reg page size
                                                  0x0,  // hdmi output reset reg base addr
                                                  0x0,  // hdmi output reset reg page size
                                                  0x0,   // hdmi core output reg base addr
                                                  0x0,   // hdmi core output reg page size
                                                  0x0    // hdmi output hw version
                                                  }; 

/* LPM configuration for MSM8994 */
lpm_prop_struct_t lpm_prop = {
                              0xFE0E0000, /**< Starting physical address, 64-bit */
                              64*1024,    /**< Size of LPM in bytes */
                              64*1024,    /**< Size allocated for AFE DMA buffers, in bytes */
                              0,          /**< Size allocated for AFE working buffer, in bytes */
                              QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE,  /**< Cache attribute: L1 writeback L2 uncached */
                              2 /**< AHB bus vote scaling factor to reach LPM. 1 - if uncached or cached with writethrough (Unidirectional access)
                                                                               2 - if cached and writeback (Duplex access) */
                              };

HwdGenericClkPropertyType genericclk_prop = {0x30080000, 0, 0, 0, {0}, 0, {0}, {0}, {0}};   
