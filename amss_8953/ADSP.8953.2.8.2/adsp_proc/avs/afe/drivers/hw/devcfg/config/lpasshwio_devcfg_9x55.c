/*==============================================================================
$Header: //components/rel/avs.adsp/2.7/afe/drivers/hw/devcfg/config/lpasshwio_devcfg_9x55.c#6 $
$DateTime: 2016/01/22 01:18:44 $
$Author: pwbldsvc $
$Change: 9769601 $
$Revision: #6 $

FILE: lpasshwio_devcfg_9x55.c 

DESCRIPTION: 9x55 Device Config

Copyright 2015 QUALCOMM Technologies, Inc. (QTI).
All Rights Reserved.
QUALCOMM Proprietary/GTDR
==============================================================================*/

/*=====================================================================
 Includes
 ======================================================================*/
#include "lpasshwio_devcfg.h"

HwdLpassPropertyType lpass_prop ={0x0770000}; // ULTAUDIO_HW_VERSION LPASS hw version reg base addr


lpasshwio_prop_lpaif_struct_t lpaif_prop = {
    0x7708000,  // lpaif reg base addr LPASS_LPAIF_VERSION
   (0x1000*16),  // lpaif reg page size
   {0x07709000, //LPASS_LPAIF_I2S_CTL0
    2,           // inf_type_cnt ( The total number below, how many interface, max is 5 )
   {PRIMARY_MI2S,SECONDARY_MI2S,0,0,0}, 
   {4,4,0,0,0}, // maximum # of channel for audio if.
   {DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT,0,0,0},
   SUPPORTED},  // i2s hw revision}
   {0x07709040, //LPASS_LPAIF_PCM_CTL0
    2,           // inf_type_cnt ( The total number below, how many interface, max is 5 )
   {PRIMARY_PCM,SECONDARY_PCM, 0, 0}, 
   {4,4,0,0}, // maximum # of channel for audio if.
   {DATA_LINE_BIDIRECT,DATA_LINE_BIDIRECT, 0, 0}, 
    SUPPORTED},  // pcm hw revision}

   {0,           //LPASS_LPAIF_PCM_CTL0
    0,             // inf_type_cnt ( The total number below, how many interface, max is 5 )
   {0, 0, 0, 0}, 
   {0, 0, 0, 0},   // maximum # of channel for audio if.
   {0, 0, 0, 0}, 
    NOT_SUPPORTED},// tdm hw revision}
    
    
   {0,   //hdmiAudHwRegBase
    0,  // hdmiAudHwRegSize
    NOT_SUPPORTED},  

    {0,                  //LPASS_LPAIF_PCM_I2S_SEL0
    0,           // # of interfaces that  is shared the output with mux
   {0,0,0,0},    // 1:1 mapping for i2s and pcm  interface  
    NOT_SUPPORTED},     
    0x30030001,  // hw revision
};


lpasshwio_prop_afe_dma_struct_t audioif_dma_prop = {
   0x7708000,   // dma LPASS_LPAIF_IRQ_EN0
   (0x1000*16),
   0x0770E000,  // dma offset LPASS_LPAIF_IRQ_EN0 : interrupt
   0x07710400,  // dma offset LPASS_LPAIF_RDDMA_CTLa
   0x07713000,  // dma offset LPASS_LPAIF_WRDMA_CTLa
   0x07710428,  // stc rddma offset   LPASS_LPAIF_RDDMA_STC_LSB0
   0x07713028,  // stc wrdma offset LPASS_LPAIF_WRDMA_STC_LSB0
   2,   // sink DMA channel count
   2,   // source DMA channel count
   68,  // lpaif intr irq #
   1,   // interrupt line #
   0x30030001   //  i2s hw revision
};

lpasshwio_prop_afe_dma_struct_t hdmiout_dma_prop = {
   0,   // hdmi output reg base addr
   0,    // hdmi output reg page size
   0,    // not requried 
   0,    // not required 
   0,    // not requried 
   0,    // not required 
   0,    // not required
   0,   // sink DMA channel count
   0,   // source DMA channel count
   0,  // l2vic id for hdmi interrupt
   0,   // there is no interrupt host
   0   //  hdmiout hw revision
};

HwdAvtimerPropertyType avtimer_prop = {
         0x7706000,  // baseRegAddr: base address of av timers LPASS_LPASS_SYNC_WRAPPER
         0x1000*2,      // regSize: register page size of av timers
         19200000,    // rootClk:
         0x07706000,  // LPASS_AVTIMER_HW_VERSION
         0,            // atime1
         0,            // atime2
         0,            // LPASS_AUD_SYNC_CTL
         76,           // isrHookPinNum: avtimer intr irq#
         0x30040000    // av timer hardware version
         };

HwdResamplerPropertyType resampler_prop = {
           0,   // baseRegAddr: base address of resampler
           0,       // regSize: hw resampler reg page size
           0,           // isrHookPinNum: hw resampler intr irq #
           0    // hw_revision: resampler hw version
           };    

lpasshwio_vfr_prop_t vfr_prop = {
        0x7701000,      // VFR reg base address
        0x1000,         // VFR register page size
        0x07701010,     // LPASS_VFR_IRQ_MUX_CTL_1
        0x07701020,     // LPASS_GP_IRQ_TS_MUX_CTL_0
        0x07701030,     // LPASS_GP_IRQ_TS_MUX_CTL_1
        0x10000000,     // VFR hw revision
        0x1,            // VFR hw latching version
        2,              // Number of VFR timestamp mux      
        2,              // Number of General Purpose timestamp mux
        {73, 74}, // l2vic_num[]: Available L2VIC # for VFR interrupts
        {{0x0, 0x1},    // vfr_src_prop[0]: Mux ctrl for VFR src sel, VFR src is hw-supported or not
        {0x7, 0x1}}     // vfr_src_prop[1]
    };

lpasshwio_prop_riva_struct_t afe_riva_prop = {0, 0, 0, 0, 0, 0, 0, 0};

lpasshwio_prop_slimbus_struct_t afe_slimbus_prop = {
  1,   // hw revision, 0-not supported
  1    // Indicates if hw latching support for AV-timer present
}; 

HwdMidiPropertyType midi_prop = {0,0, 0, 0, 0,0 };
lpasshwio_prop_spdif_struct_t spdiftx_prop = {0, 0, 0, 0, 0, 0, 0};

lpasshwio_prop_hdmi_output_struct_t hdmi_output_prop = {
                                                  0x0,  // hdmi output reg base addr
                                                  0x0,  // hdmi output reg page size
                                                  0x0,  // hdmi output reset reg base addr
                                                  0x0,  // hdmi output reset reg page size
                                                  0x0,   // hdmi core output reg base addr
                                                  0x0,   // hdmi core output reg page size
                                                  0x0    // hdmi output hw version
                                                  }; 

/* LPM configuration for MSM8994 */
lpm_prop_struct_t lpm_prop = {
                              0x7718000, /**< Starting physical address, 64-bit */
                              8*1024,    /**< Size of LPM in bytes */
                              8*1024,    /**< Size allocated for AFE DMA buffers, in bytes */
                              0,          /**< Size allocated for AFE working buffer, in bytes */
                              QURT_MEM_CACHE_WRITEBACK_NONL2CACHEABLE,  /**< Cache attribute: L1 writeback L2 uncached */
                              2 /**< AHB bus vote scaling factor to reach LPM. 1 - if uncached or cached with writethrough (Unidirectional access)
                                                                               2 - if cached and writeback (Duplex access) */
                              };

HwdGenericClkPropertyType genericclk_prop = {0x30030001, 0, 0, 0, {0}, 0, {0}, {0}, {0}};   

