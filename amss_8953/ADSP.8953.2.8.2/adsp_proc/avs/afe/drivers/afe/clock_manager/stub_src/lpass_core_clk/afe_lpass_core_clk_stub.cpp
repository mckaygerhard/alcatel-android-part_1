/*==============================================================================
$Header: //components/rel/avs.adsp/2.7/afe/drivers/afe/clock_manager/stub_src/lpass_core_clk/afe_lpass_core_clk_stub.cpp#1 $
$DateTime: 2015/12/11 19:30:28 $
$Author: pwbldsvc $
$Change: 9575091 $
$Revision: #1 $

FILE:     clock_manager.cpp

DESCRIPTION: Main driver interface to the clock control

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

Copyright (c) 2013 QUALCOMM Technologies, Inc. (QTI)
All Rights Reserved.
QUALCOMM Proprietary/GTDR
==============================================================================*/
/*============================================================================
EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order. Please
use ISO format for dates.

$Header: //components/rel/avs.adsp/2.7/afe/drivers/afe/clock_manager/stub_src/lpass_core_clk/afe_lpass_core_clk_stub.cpp#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------

============================================================================*/


/*=====================================================================
 Includes
 ======================================================================*/
#include "afe_lpass_core_clk.h"

//this is for the vote related to AHB bus shared clocks
//This is supposed to be okay for all chip family because it will be always on if it is not required.
ADSPResult afe_lpass_core_clk_init(uint32_t hw_version)
{
  return ADSP_EUNSUPPORTED;
}

//this is for the vote related to AHB bus shared clocks
ADSPResult afe_lpass_core_clk_deinit(void)
{
  return ADSP_EUNSUPPORTED;
}

/**
  @brief Set clock

  @param[in] dev_port_ptr pointer for AFE port structure
  @param[in] cfg_ptr   pointer to the configuration structure
  @param[in] payload_size   size of the configuration payload

  @return  ADSP_EOK on success, an error code on error

*/
ADSPResult afe_lpass_core_clk_set(int8_t *cfg_ptr, uint32_t payload_size)
{
   return ADSP_EUNSUPPORTED;   
}

/**
  @brief registering MMPM_CORE_ID_LPASS_CORE with MMPM 

*/

uint32_t afe_mmpm_register_lpass_core()
{
   return 0;   
}
