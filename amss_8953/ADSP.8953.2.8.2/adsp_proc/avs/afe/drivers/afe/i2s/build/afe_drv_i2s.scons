#===============================================================================
#
# AVS AU
#
# GENERAL DESCRIPTION
#    Build script
#
# Copyright (c) 2009-2009 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/avs.adsp/2.7/afe/drivers/afe/i2s/build/afe_drv_i2s.scons#5 $
#  $DateTime: 2016/04/18 07:46:13 $
#  $Author: pwbldsvc $
#  $Change: 10285265 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
Import('env')

if 'USES_ENABLE_FUNCTION_SECTIONS' in env:
   env.Append(CFLAGS = '-ffunction-sections')


CBSP_API = [
   'BOOT',
   'DAL',
   'DEBUGTOOLS',
   'HAL',
   'MPROC',
   'POWER',
   'SERVICES',
   'SYSTEMDRIVERS',

   # needs to be last also contains wrong comdef.h
   'KERNEL',   
]

env.RequirePublicApi(CBSP_API, area='core')
env.RequireRestrictedApi(['AVS','SHARED_LIBRARY_INC_PATHS'])
env.RequireProtectedApi('AVS')

env.PublishPrivateApi('AVS',[
   '${AVS_ROOT}/afe/drivers/afe/i2s/common/inc',
   '${AVS_ROOT}/afe/drivers/afe/i2s/common/src',
   ])

#-------------------------------------------------------------------------------
# Setup source PATH
#-------------------------------------------------------------------------------
SRCPATH = ".."

LIBNAME = 'AfeI2SDriver'

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)
 
BUILD_TARGET = env.get('BUILD_ASIC')

if BUILD_TARGET in ["8992", "8994", "9x55", "8952", "8953", "8937", "8976"]:
    avs_reg_sources = env.GlobSourceFiles(['/common/src/*.cpp', '/common/src/*.c', '/hal_v1/src/*.c', '/hal_v2/stub_src/*.c'], SRCPATH,posix=True)
elif BUILD_TARGET in ["8996"]:
    avs_reg_sources = env.GlobSourceFiles(['/common/src/*.cpp', '/common/src/*.c', '/hal_v2/src/*.c', '/hal_v1/stub_src/*.c'], SRCPATH,posix=True)
else:
    avs_reg_sources = env.GlobSourceFiles(['/common/src/*.cpp', '/common/src/*.c', '/hal_v2/src/*.c', '/hal_v1/src/*.c'], SRCPATH,posix=True)


# Generate stub source file list
avs_stub_sources = env.GlobSourceFiles(['/common/stub_src/*.cpp', '/common/stub_src/*.c'], SRCPATH,posix=True)

# Generate proprietary source file list
avs_prop_sources = []

#import pdb; pdb.set_trace()
if 'USES_AVS_LIBRARY_BUILDER' in env:
   env.AddAvsLibrary(['AVS_ADSP','AVS_ADSP_USER'], '${BUILDPATH}/'+LIBNAME,
      [avs_reg_sources, avs_stub_sources, avs_prop_sources])	  
      
#env.Append(CFLAGS = '-O0')
