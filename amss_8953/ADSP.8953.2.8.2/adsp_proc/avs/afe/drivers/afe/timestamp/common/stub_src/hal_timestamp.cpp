/*==============================================================================
$Header: //components/rel/avs.adsp/2.7/afe/drivers/afe/timestamp/common/stub_src/hal_timestamp.cpp#3 $
$DateTime: 2014/08/25 05:50:46 $
$Author: svutukur $
$Change: 6478346 $
$Revision: #3 $

FILE:     hal_vfr_driver.cpp

DESCRIPTION: VFR register programming

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

Copyright 2013 Qualcomm Technologies Incorporated.
All Rights Reserved.
QUALCOMM Proprietary/GTDR
==============================================================================*/
/*============================================================================
EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order. Please
use ISO format for dates.

$Header: //components/rel/avs.adsp/2.7/afe/drivers/afe/timestamp/common/stub_src/hal_timestamp.cpp#3 $

when       who     what, where, why
--------   ---     -------------------------------------------------------
7/19/2012  RP      created

============================================================================*/

#include "hal_timestamp.h"

ts_hw_drv_t  ts_hw_drv_global;

/*==========================================================================
  Macro\Constant definitions
  ========================================================================== */

ADSPResult ts_hw_drv_init()
{
   return ADSP_EUNSUPPORTED;
}

void hal_ts_release_ts_mux(uint32_t mux_id)
{
   return;
}
