/*========================================================================
   This file contains AFE Error handler implementation

  Copyright (c) 2015 Qualcomm Technologies, Inc.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is regulated
  by the U.S. Government, Diversion contrary to U.S. law prohibited.
 
  $Header: 
 ====================================================================== */

#ifndef _AFE_ERR_HNDLR_I_H_
#define _AFE_ERR_HNDLR_I_H_

/*==========================================================================
  Include files
  ========================================================================== */

#include "qurt_elite.h"

/*----------------------------------------------------------------------------
 * Macro definition
 * -------------------------------------------------------------------------*/

/* TODO : As of now, making window duration and number of allowed recoveries as MACROS,
 * actually need to get these values during device config*/

#ifdef AFE_ERR_HANDLER_ENABLE
/**<For internal build, CRASH for the first signal miss. */

/**< For the first signal miss, time stamp difference (ts_diff) will always zero,
 * hence window interval of 1ms would be always greater than ts_diff,
 * here error recovery count matches with MAX_NUM_ALLOWED_RECOVERIES (=0),
 * therefore allows crash.*/

#define MAX_NUM_ALLOWED_RECOVERIES                0
/**< Indicates maximum number of recoveries allowed in a certain window duration.*/
#define ERR_HNDLR_REC_WINDOW_DURATION          1000    //ex: 1000 micro seconds
/**< Indicates the window duration in micro seconds.
 *  Used as reference to crash/allow recovery depends on number of recoveries taken place.*/

#else
/**<For production build, perform always device recovery.*/

/**<Within 1ms window interval, never three recoveries can take place
 * Hence, will always allows device recoveries.*/

#define MAX_NUM_ALLOWED_RECOVERIES                3
/**< Indicates maximum number of recoveries allowed in a certain window duration.*/
#define ERR_HNDLR_REC_WINDOW_DURATION          1000     //ex:1000 micro seconds
/**< Indicates the window duration in micro seconds.
 *  Used as reference to crash/allow recovery depends on number of recoveries taken place.*/

#endif //AFE_ERR_HANDLER_ENABLE

typedef struct afe_err_handler_info
{
  uint64_t ts_queue[MAX_NUM_ALLOWED_RECOVERIES+1];
           /**< Queue that holds time stamp of past device recoveries
            * In order to avoid compilation error, when  MAX_NUM_ALLOWED_RECOVERIES=0,
            * making ts_queue length to be MAX_NUM_ALLOWED_RECOVERIES+1.
            * Note that, this extra +1 is not used for queue operations
            * */
  uint16_t front_idx;
           /**< Indicates front index of the queue.
            * Front index will always points to first(oldest) device recovery time stamp in current window.*/
  uint16_t back_idx;
           /**< Indicates back index of the queue
            * Back index will always points to recent(latest) device recovery time stamp in current window.*/
  uint16_t err_recovery_count;
           /**< error recovery count indicates number of recoveries taken place in current window.*/
} afe_err_handler_info_t;

/** Error handling types */
typedef enum afe_err_hndlr_type_t
{
   AFE_ERR_HNDL_NONE = 0,  /**< No Error Handling */
   AFE_DEBUG_CRASH,        /**< Force crash for worker thread signal miss */
   AFE_ERR_RECOVERY        /**< Recover device from permanent mute or stall due to signal miss */
} afe_err_hndlr_type_t;


/*==========================================================================
  Function declarations
  ========================================================================== */

afe_err_hndlr_type_t afe_get_err_handler_type(uint32_t port_id);


#endif /* _AFE_ERR_HNDLR_I_H_ */
