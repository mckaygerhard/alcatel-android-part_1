#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define FARF_HIGH 1 
#include "HAP_farf.h"

#include "rpcmem.h"
#include "adsp.h"

#include "qurt_memory_adsp.h"

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

int cache[524288];  // 512K

/**
cacheSizeToLock unit is KB
*/
int adsp_mem_l2cache_line_lock (
const unsigned int cacheSizeToLock
)
{
	int result = 1;
	
	// Lock 256KB in L2 cache
	//result = qurt_mem_l2cache_line_lock((qurt_addr_t)cache, (qurt_size_t)262144);
	result = qurt_mem_l2cache_line_lock((unsigned int)cache, cacheSizeToLock * 1024);
	
	if (result == 0) 
	{
		FARF(ALWAYS, "qurt_mem_l2cache_line_lock result == 0");
	}
	else
	{
		FARF(ALWAYS, "qurt_mem_l2cache_line_lock result != 0");
	}

	return result;
}


/**
cacheSizeToUnlock unit is KB
*/
int adsp_mem_l2cache_line_unlock (
const unsigned int cacheSizeToUnlock
)
{
	int result = 1;
	
	result = qurt_mem_l2cache_line_unlock((unsigned int)cache, cacheSizeToUnlock * 1024);
	
	if (result == 0) 
	{
		FARF(ALWAYS, "qurt_mem_l2cache_line_unlock result == 0");
	}
	else
	{
		FARF(ALWAYS, "qurt_mem_l2cache_line_unlock result != 0");
	}
	
	return result;
}

/**

1st argument
typedef enum {
	HEXAGON_L1_I_CACHE = 0,     < Hexagon L1 instruction cache.
	HEXAGON_L1_D_CACHE = 1,     < Hexagon L1 data cache.
	HEXAGON_L2_CACHE = 2        < Hexagon L2 cache.
} qurt_cache_type_t;
		

2nd argument
typedef enum {
	FULL_SIZE = 0,                < Fully shared cache, without partitioning.
	HALF_SIZE = 1,                < 1/2 for main, 1/2 for auxiliary.
	THREE_QUARTER_SIZE = 2,       < 3/4 for main, 1/4 for auxiliary.
	SEVEN_EIGHTHS_SIZE = 3        < 7/8 for main, 1/8 for auxiliary. For L2 cache only.
} qurt_cache_partition_size_t;

*/

int adsp_cache_partition (
const unsigned int cachePartitionSize
)
{
	int result = 1;
	result = qurt_mem_configure_cache_partition(2, cachePartitionSize);
	
	if (result == 0) 
	{
		FARF(ALWAYS, "adsp_cache_partition result == 0, success");
	}
	else
	{
		FARF(ALWAYS, "adsp_cache_partition result != 0, failure");
	}
	
	return result;
}

