/**@ingroup func_qurt_mem_l2cache_line_lock 
  Performs an L2 cache line locking operation. This function locks selective lines in the L2 cache memory.

  @note1hang The line lock operation can be performed only on the 32-byte aligned size and address.

  @datatypes
  #qurt_addr_t \n
  #qurt_size_t 
 
  @param[in] addr   Address of the L2 cache memory line to be locked; the address must be 32-byte aligned.
  @param[in] size   Size (in bytes) of L2 cache memory to be line locked. Size must be a multiple of 32 bytes.
 
  @return
  QURT_EOK -- Success.\n
  QURT_EALIGN -- Aligning data or address failure.\n

  @dependencies
  None.
*/
int qurt_mem_l2cache_line_lock (unsigned int addr, unsigned int size);

/**@ingroup func_qurt_mem_l2cache_line_unlock 
  Performs an L2 cache line unlocking operation. This function unlocks selective lines in the L2 cache memory.

  @note1hang The line unlock operation can be performed only on a 32-byte aligned size and address.

  @datatypes
  #qurt_addr_t \n
  #qurt_size_t 
 
  @param[in] addr   Address of the L2 cache memory line to be locked; the address must be 32-byte aligned.
  @param[in] size   Size (in bytes) of the L2 cache memory to be line ulocked; size must be a multiple of 32 bytes.
 
  @return
  QURT_EOK -- Success. \n
  QURT_EALIGN -- Aligning data or address failure. \n
  QURT_EFAILED -- Operation failed, cannot find the matching tag.

  @dependencies
  None.
*/
int qurt_mem_l2cache_line_unlock(unsigned int addr, unsigned int size);


int qurt_mem_configure_cache_partition(unsigned int  cache_type, unsigned int partition_size);