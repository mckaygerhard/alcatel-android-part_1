#include <string.h>
#include <stdio.h>
#include "err.h"
#include "qurt_elite.h"
#include "adsp_err_fatal.h"


void AdspfatalerrApi(char* err_str,int strlength)
{
	static char adsp_err_fatal[80];
	strlcpy(adsp_err_fatal,err_str,80);

#if (ENABLE_FATAL_ERROR==1)

  	MSG_1(MSG_SSID_QDSP6, DBG_ERROR_PRIO, "Force crash Q6 due to AFE signal miss",0);
#if defined(__qdsp6__) && !defined(SIM)
   	ERR_FATAL("FATAL_ERR: Force crash Q6 due to AFE signal miss", 0, 0, 0);
#endif

#endif //ENABLE_FATAL_ERROR

   	return; /* After entering ERR_FATAL(), execution never comes back here */
}

void Adspfatalerr_Init_Client(void)
{

	return;

}

void Adspfatalerr_Deinit_Client(void)
{

	return;

}

