#===============================================================================
#
# I2C Libs
#
# GENERAL DESCRIPTION
#    Public build script for I2C BUS driver.
#
# Copyright (c) 2009-2015 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //components/rel/core.adsp/2.6.6/buses/i2c/build/i2c.scons#1 $
#  $DateTime: 2016/05/04 01:42:14 $
#  $Author: pwbldsvc $
#  $Change: 10389076 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 05/06/15   np      Added QDSS Logging
# 04/21/15   vg      Enabling I2C Driver for 8976
# 09/09/14   sk      Fixed DAL API support to sensors image
# 08/29/14   np      Added support for 8992
# 05/12/14   lk      Moved init to sensors image.
# 02/28/14   lk      Added support for 8994.
# 05/24/13   lk      Added support for 8084, 8962.
# 02/09/12   lk      Added device inc path.
# 02/09/12   lk      Added configurable properties file.
# 02/09/12   ag      Fixed the location where the object files are built.
# 01/21/12   ag		 Initial release
#
#===============================================================================
Import('env')
#-------------------------------------------------------------------------------
# Load sub scripts
#-------------------------------------------------------------------------------
env.LoadSoftwareUnits()


#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
env = env.Clone()
#print env
# Additional defines
env.Append(CPPDEFINES = ["FEATURE_LIBRARY_ONLY"])   
#env.Append(CCFLAGS = " -O3 ")

SRCPATH = ".."

IMAGES = []
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 
CBSP_APIS = []
I2C_CONFIG_XML = []

#-------------------------------------------------------------------------------
# Publish Private APIs
#-------------------------------------------------------------------------------
if env.has_key('USES_I2C_QDSS_TRACER'):
   env.Append(LOGPATH = ["${INC_ROOT}/core/buses/i2c/src/logs/qdss/trace"])
else :
   env.Append(LOGPATH = ["${INC_ROOT}/core/buses/i2c/src/logs/qdss/notrace"])

env.PublishPrivateApi('BUSES_I2C_DEVICE', [
   '${INC_ROOT}/core/buses/i2c/src/dev',
   '${INC_ROOT}/core/buses/i2c/src/drv',
   '${LOGPATH}',
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_APIS += [
   'BUSES',
   'DAL',
   'HAL',
   'SYSTEMDRIVERS',
   'HWENGINES',
   'KERNEL',   
   'SERVICES',
   'POWER',
   'DEBUGTRACE'
]

env.RequirePublicApi(CBSP_APIS)
env.RequireRestrictedApi(CBSP_APIS)

     


#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
#I2C_DEVICE_GLOB_FILES = env.GlobFiles('../src/*/*.c', posix=True)

I2C_SRC = [
   '${BUILDPATH}/src/dev/I2cDeviceInit.c'    ,
   '${BUILDPATH}/src/drv/I2cBsp.c'           ,
   '${BUILDPATH}/src/drv/I2cDriverInit.c'    ,
   '${BUILDPATH}/src/drv/I2cPlatSvcInit.c'   ,
   '${BUILDPATH}/src/drv/I2cDriverDalFwk.c'  ,
   '${BUILDPATH}/src/drv/I2cDriverDalInfo.c'  ,
]


I2C_ISLAND_SRC = [
   '${BUILDPATH}/src/dev/I2cDevice.c'        ,
   '${BUILDPATH}/src/drv/I2cDriver.c'        ,
   '${BUILDPATH}/src/drv/I2cPlatSvc.c'       ,
   '${BUILDPATH}/src/drv/I2cSys.c'           ,
]
 
#GLOB returns the relative path name, it needs to replaced with correct build location
#I2C_DEVICE_SOURCES = [path.replace(SRCPATH, '${BUILDPATH}') for path in I2C_DEVICE_GLOB_FILES]


if env.has_key('ADSP_PROC') and env['MSM_ID'] in ['8974','9x25','8x26','8084','8962','9x35','8994','8992','8952','8976', '8953', '8937']:
   IMAGES = ['QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SW','CORE_QDSP6_SENSOR_SW']

   if env['MSM_ID'] in ['8992']:
      I2C_ISLAND_SRC.append('${BUILDPATH}/config/i2c_adsp_8992.c')
   elif env['MSM_ID'] in ['8952']:
      I2C_ISLAND_SRC.append('${BUILDPATH}/config/i2c_adsp_8952.c')    
   elif env['MSM_ID'] in ['8953']:
      I2C_ISLAND_SRC.append('${BUILDPATH}/config/i2c_adsp_8953.c')
   elif env['MSM_ID'] in ['8937']:
      I2C_ISLAND_SRC.append('${BUILDPATH}/config/i2c_adsp_8937.c')	  
   elif env['MSM_ID'] in ['8994']:
      I2C_ISLAND_SRC.append('${BUILDPATH}/config/i2c_adsp_8994.c')
   elif env['MSM_ID'] in ['8976']:
      I2C_ISLAND_SRC.append('${BUILDPATH}/config/i2c_adsp_8976.c')
 
elif env.has_key('APPS_PROC') and env['MSM_ID'] in ['9x25','9x35']:
   IMAGES = ['APPS_IMAGE',      'CBSP_APPS_IMAGE']
else:
   Return(); 



#-------------------------------------------------------------------------------
# Add Libraries to image
# env.AddLibrary is a new API, only if the IMAGES is valid in the build env
# the objects will built and added to the image.
#-------------------------------------------------------------------------------

if 'USES_ISLAND' in env:
    U_IMG_LIB = env.AddLibrary(
      ['CBSP_QDSP6_SW_IMAGE', 'CORE_QDSP6_SENSOR_SW'],
      '${BUILDPATH}/I2cDriver_UImg', I2C_ISLAND_SRC)
    env.AddIslandLibrary(['CORE_QDSP6_SENSOR_SW'], U_IMG_LIB)
else:
    I2C_SRC.append(I2C_ISLAND_SRC)

BIG_IMG_LIB = env.AddLibrary(IMAGES, '${BUILDPATH}/I2cDriver', I2C_SRC)


if 'USES_I2C_QDSS_TRACER' in env:
   if 'USES_QDSS_SWE' in env:
      env.Append(CPPDEFINES = ["I2C_TRACER_SWEVT"])
      QDSS_IMG = ['QDSS_EN_IMG']
      events = [
         ['I2C_EVENT_DRVAPI_READ',    'I2C DRV READ: Bus Freq = %d'],
         ['I2C_EVENT_DRVAPI_WRITE',  'I2C DRV WRITE: Bus Freq = %d'],
         ['I2C_EVENT_DRVAPI_BATCHTRANSFER', 'I2C_EVENT_DRVAPI_BATCHTRANSFER: Bus Freq = %d, In Island mode? (success=0): %d'],
         ['I2C_SIZE_DATABYTES', 'I2C_SIZE_DATABYTES: IN data bytes size = %d, OUT data bytes size = %d'],
         ['I2C_AFTER_SEQUENCE_SUBMISSION',  'I2C SEQUENCE SUBMITTTED'],
         ['I2C_QUPSERVICE_QUP_VALID_INTERRUPT_OR_VALID_POLLSTATUS', 'I2C_QUPSERVICE_QUP_VALID_INTERRUPT: QUP_OPERATIONAL = %d'],
         ['I2C_QUPSERVICE_BEGIN', 'I2C_QUPSERVICE_BEGIN'],
         ['I2C_QUPSERVICE_END', 'I2C_QUPSERVICE_END'],
         ['I2C_STATE_CHANGE_ATTEMPT_RUN',  'I2C STATE CHANGE TO RUN'],
         ['I2C_STATE_CHANGE_ATTEMPT_RESET',  'I2C STATE CHANGE TO RESET'],
         ['I2C_STATE_CHANGE_ATTEMPT_PAUSE',  'I2C STATE CHANGE TO PAUSE'],
         ['I2C_STATE_CHANGE_ATTEMPT_SUCCESSFULL', 'I2C_STATE_CHANGE_ATTEMPT_SUCCESSFULL'],
         ['I2C_STATE_CHANGE_ATTEMPT_FAILED', 'I2C_STATE_CHANGE_ATTEMPT_FAILED']]
      env.AddSWEInfo(QDSS_IMG, events)

if 'QDSS_TRACER_SWE' in env:
   env.SWEBuilder(['${BUILD_ROOT}/core/buses/i2c/build/${BUILDPATH}/src/I2CSWEventId.h'], None)
   env.Append(CPPPATH = ['${BUILD_ROOT}/core/buses/i2c/build/${BUILDPATH}/src'])


#---------------------------------------------------------------------------
# RCINIT
#---------------------------------------------------------------------------
if 'USES_RCINIT' in env:
   RCINIT_IMG = ['CORE_QDSP6_SENSOR_SW']
   env.AddRCInitFunc(           # Code Fragment in TMC: NO
    RCINIT_IMG,                 # define TMC_RCINIT_INIT_DRIVER_INIT
    {
     'sequence_group'             : 'RCINIT_GROUP_7',                # required
     'init_name'                  : 'I2cBsp_Init',                   # required
     'init_function'              : 'I2cBsp_Init',                   # required
    })

#---------------------------------------------------------------------------
# DAL Device Config to Sensors PD
#---------------------------------------------------------------------------
if 'USES_DEVCFG' in env:
   if 'USES_SENSOR_IMG' in env:
      DEVCFG_IMG_SENSORS = ['DEVCFG_CORE_QDSP6_SENSOR_SW']
#   DEVCFG_IMG = ['DAL_DEVCFG_IMG']
      env.AddDevCfgInfo(DEVCFG_IMG_SENSORS, 
      {
         '8994_xml'    : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8994.c',
                          '${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8994.xml'],
         '8952_xml'    : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8952.c',
                          '${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8952.xml'],  
         '8953_xml'    : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8953.c',
                          '${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8953.xml'],
         '8937_xml'    : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8937.c',
                          '${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8937.xml'],
		 '8976_xml'    : ['${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8976.c',
                          '${BUILD_ROOT}/core/buses/i2c/config/i2c_adsp_8976.xml'] 
      })


