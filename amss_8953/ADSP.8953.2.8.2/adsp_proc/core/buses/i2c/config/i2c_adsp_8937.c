/*=============================================================================

  FILE:     i2c_adsp_8937.c

  OVERVIEW: This file has the devices for 8937 platform for adsp. 
 
            Copyright (c) 2015 Qualcomm Technologies Incorporated.
            All Rights Reserved.
            Qualcomm Confidential and Proprietary 

  ===========================================================================*/

/*=========================================================================
  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.adsp/2.6.6/buses/i2c/config/i2c_adsp_8937.c#1 $
  $DateTime: 2016/05/04 01:42:14 $$Author: pwbldsvc $

  When     Who    What, where, why
  -------- ---    -----------------------------------------------------------
  09/25/15  vg     Added support for 8937
  01/09/15  SG     Created.

  ===========================================================================*/

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/

#include "I2cDriverTypes.h"
#include "I2cPlatSvc.h"
#include "I2cDevice.h"

#define I2C_NUM_PLATFORM_DEVICES         (1)


/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/

const uint32              i2cDeviceNum = I2C_NUM_PLATFORM_DEVICES;

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/


#ifdef BUILD_FOR_ISLAND
#define ATTRIBUTE_ISLAND_CODE __attribute__((section("RX.island")))
#define ATTRIBUTE_ISLAND_CONST __attribute__((section("RO.island")))
#define ATTRIBUTE_ISLAND_DATA __attribute__((section("RW.island")))
#else
#define ATTRIBUTE_ISLAND_CODE /* empty */
#define ATTRIBUTE_ISLAND_CONST /* empty */
#define ATTRIBUTE_ISLAND_DATA /* empty */
#endif


/*-------------------------------------------------------------------------
 * Global Data Definitions
 * ----------------------------------------------------------------------*/
I2cPlat_PropertyType i2cPlatPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
//  I2CPLAT_PROPERTY_INIT(0x2001C023, 0x2001C033, 2, "BLSP1_BLSP", 0x35000, CLOCK_GCC_BLSP1_AHB_CLK, CLOCK_GCC_BLSP1_QUP1_APPS_CLK, "/clk/pcnoc"), //GPIO: 02 & 03 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001C063, 0x2001C073, 2, "BLSP1_BLSP", 0x36000, CLOCK_GCC_BLSP1_AHB_CLK, CLOCK_GCC_BLSP1_QUP2_APPS_CLK, "/clk/pcnoc"), //GPIO: 06 & 07 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001C0A3, 0x2001C0B3, 2, "BLSP1_BLSP", 0x37000, CLOCK_GCC_BLSP1_AHB_CLK, CLOCK_GCC_BLSP1_QUP3_APPS_CLK, "/clk/pcnoc"), //GPIO: 10 & 11 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
	I2CPLAT_PROPERTY_INIT(0x2001C0E3, 0x2001C0F3, 2, "BLSP1_BLSP", 0x38000, CLOCK_GCC_BLSP1_AHB_CLK, CLOCK_GCC_BLSP1_QUP4_APPS_CLK, "/clk/pcnoc"), //GPIO: 14 & 15 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma

//  I2CPLAT_PROPERTY_INIT(0x2001C123, 0x2001C133, 2, "BLSP2_BLSP", 0x35000, CLOCK_GCC_BLSP2_AHB_CLK, CLOCK_GCC_BLSP2_QUP1_APPS_CLK, "/clk/pcnoc"), //GPIO: 18 & 19 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001C163, 0x2001C173, 2, "BLSP2_BLSP", 0x36000, CLOCK_GCC_BLSP2_AHB_CLK, CLOCK_GCC_BLSP2_QUP2_APPS_CLK, "/clk/pcnoc"), //GPIO: 22 & 23 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001c574, 0x2001c584, 2, "BLSP2_BLSP", 0x37000, CLOCK_GCC_BLSP2_AHB_CLK, CLOCK_GCC_BLSP2_QUP3_APPS_CLK, "/clk/pcnoc"), //GPIO: 87 & 88 -> Func: 4, Dir: O/p, PullType: Up, Drive: 2ma
//  I2CPLAT_PROPERTY_INIT(0x2001c623, 0x2001c633, 2, "BLSP2_BLSP", 0x38000, CLOCK_GCC_BLSP2_AHB_CLK, CLOCK_GCC_BLSP2_QUP4_APPS_CLK, "/clk/pcnoc"), //GPIO: 98 & 99 -> Func: 3, Dir: O/p, PullType: Up, Drive: 2ma
};

I2cDev_PropertyType i2cDevPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{

//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
	I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
 
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
//  I2CDEV_PROPERTY_INIT(I2CDEV_PROPVALUE_INPUT_CLK_19200_KHZ),
  
};

I2cDrv_DriverProperty i2cDrvPropertyArray[] ATTRIBUTE_ISLAND_DATA =
{
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_1, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_2, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_3, FALSE, 0),
   I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_4, FALSE, 0),
   
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_5, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_6, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_7, FALSE, 0),
// I2CDRV_DRIVERPROPERTY_INIT(I2CDRV_I2C_8, FALSE, 0),

};

I2cDrv_DescType i2cDrvDescArray[I2C_NUM_PLATFORM_DEVICES] ATTRIBUTE_ISLAND_DATA ;