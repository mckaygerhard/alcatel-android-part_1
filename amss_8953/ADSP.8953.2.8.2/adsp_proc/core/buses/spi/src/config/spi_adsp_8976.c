/*=============================================================================

  FILE:     spi_adsp_8952.c

  OVERVIEW: Contains target specific SPI configuration for 8994 ADSP 
 
            Copyright (c) 2009 - 2015 Qualcomm Technologies Incorporated.
            All Rights Reserved.
            Qualcomm Confidential and Proprietary 

  ===========================================================================*/

/*=========================================================================
  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.adsp/2.6.6/buses/spi/src/config/spi_adsp_8976.c#1 $
  $DateTime: 2016/05/04 01:42:14 $$Author: pwbldsvc $

  When        Who    What, where, why
  --------    ---    -----------------------------------------------------------
  2015/03/19  dpk     Initial version

  ===========================================================================*/
#include "SpiDevicePlatSvc.h"
#include "SpiDriverTypes.h"
#include "SpiDriver.h"
#include "DALDeviceId.h"
#include "DDIHWIO.h"
#include "DDITlmm.h"

spiDevice spiDevices[SPIPD_DEVICE_COUNT] ATTRIBUTE_ISLAND_DATA = {
   { NULL, 0, 0 }, //device handle, core number, current status
   { NULL, 7, 0 }
   //Add entry here to initialize an SPI core, format {NULL, QUPCORENUMBER, 0}
   //Valid range for QUPCORENUMBER is 0 to 7.
};


SpiDevicePlat_DevCfg Spi_DeviceCfg[SPIPD_DEVICE_COUNT] ATTRIBUTE_ISLAND_DATA;

#define SPI_PERIPH_SS_BASE_PHYS_ADDR 0x7880000
//#define SPI_OPERATING_FREQ_HZ  1000000 // 1 MHz
#define SPI_OPERATING_FREQ_HZ  5000000 // 5 MHz
//#define SPI_OPERATING_FREQ_HZ  8000000 // 8 MHz
//#define SPI_OPERATING_FREQ_HZ 10000000 // 10 MHz

const uint32 SpiQupPhysAddrOffset[] ATTRIBUTE_ISLAND_DATA = {0x35000, 0x36000, 0x37000, 0x38000, 0x275000, 0x276000, 0x277000, 0x278000};
const uint32 SpiQupVirAddrOffset[] ATTRIBUTE_ISLAND_DATA = {0x35000, 0x36000, 0x37000, 0x38000, 0x35000, 0x36000, 0x37000, 0x38000};

char *SpiClks[] ATTRIBUTE_ISLAND_DATA = { "gcc_blsp1_qup1_spi_apps_clk", "gcc_blsp1_qup2_spi_apps_clk", "gcc_blsp1_qup3_spi_apps_clk", "gcc_blsp1_qup4_spi_apps_clk",
                                          "gcc_blsp2_qup1_spi_apps_clk", "gcc_blsp2_qup2_spi_apps_clk", "gcc_blsp2_qup3_spi_apps_clk", "gcc_blsp2_qup4_spi_apps_clk" };

const uint32 SpiGpioClk[] ATTRIBUTE_ISLAND_CONST = {0x2006C031, 0x2006C071, 0x2006C0B1, 0x2006C0F1, 0x2006c891, 0x2006C171, 0x2006c6f1, 0x2006c131};
const uint32 SpiGpioCS[] ATTRIBUTE_ISLAND_CONST = {0x2006C021, 0x2006C061, 0x2006C0A1, 0x2006C0E1, 0x2006c881, 0x2006C161, 0x2006c6e1, 0x2006c121};
const uint32 SpiClkGpioMISO[] ATTRIBUTE_ISLAND_CONST = {0x2006C011, 0x2006C051, 0x2006C091, 0x2006C0D1, 0x2006c871, 0x2006C151, 0x2006c6d2, 0x2006c111};
const uint32 SpiClkGpioMOSI[] ATTRIBUTE_ISLAND_CONST = {0x2006C001, 0x2006C041, 0x2006C081, 0x2006C0C1, 0x2006c861, 0x2006C141, 0x2006c6c2, 0x2006c101};


static DalDeviceHandle  ATTRIBUTE_ISLAND_DATA *pClkHandle;
static DalDeviceHandle  ATTRIBUTE_ISLAND_DATA *pTlmmHandle;

void SpiInit(void)
{
   int32 i;
   SpiDevicePlat_DevCfg *tgtCfg;
   DALResult dalRes;
   uint8 *blsp1VirBase, *blsp2VirBase;
   DalDeviceHandle *phDalHWIO = NULL;
   ClockIdType QupAppClkId;

   blsp1VirBase = blsp2VirBase = NULL;
   memset(Spi_DeviceCfg, 0, sizeof(Spi_DeviceCfg));

   dalRes = DAL_DeviceAttach(DALDEVICEID_HWIO, &phDalHWIO);
   if ((DAL_SUCCESS != dalRes) || (NULL == phDalHWIO))
   {
      return;
   }
   if (DAL_SUCCESS != DalHWIO_MapRegion(phDalHWIO, "BLSP1_BLSP", &blsp1VirBase))
   {
      return;
   }
   
   if (DAL_SUCCESS != DalHWIO_MapRegion(phDalHWIO, "BLSP2_BLSP", &blsp2VirBase))
   {
      return;
   }
   
   if (NULL == pClkHandle)
   {
      dalRes = DAL_ClockDeviceAttach(DALDEVICEID_CLOCK, &pClkHandle);
      if ((DAL_SUCCESS != dalRes) || (NULL == pClkHandle))
      {
         return;
      }
   }
   
   if (NULL == pTlmmHandle)
   {
      if (DAL_SUCCESS != DAL_DeviceAttachEx(NULL, DALDEVICEID_TLMM, DALTLMM_INTERFACE_VERSION, &pTlmmHandle))
      {
         return;
      }

      if (DAL_SUCCESS != DalDevice_Open(pTlmmHandle, DAL_OPEN_SHARED))
      {
         DAL_DeviceDetach(pTlmmHandle);
         pTlmmHandle = NULL;
         return;
      }
   }

   for (i = 0; i < SPIPD_DEVICE_COUNT; i++)
   {
      tgtCfg = &(Spi_DeviceCfg[i]);
      tgtCfg->uQupCoreNum = spiDevices[i].qup_core_num;
      tgtCfg->bInterruptBased = 0;
      tgtCfg->pQupAppClkName = SpiClks[tgtCfg->uQupCoreNum];
      tgtCfg->pQupHClkName = ((tgtCfg->uQupCoreNum <= 3) ? "gcc_blsp1_ahb_clk" : "gcc_blsp2_ahb_clk");
      tgtCfg->qupPhysBlockAddr = SPI_PERIPH_SS_BASE_PHYS_ADDR + SpiQupPhysAddrOffset[tgtCfg->uQupCoreNum];
      tgtCfg->qupVirtBlockAddr = (tgtCfg->uQupCoreNum < 4) ? (uint32)blsp1VirBase :(uint32)blsp2VirBase + SpiQupVirAddrOffset[tgtCfg->uQupCoreNum];
      tgtCfg->pClkHandle = pClkHandle;
	  
	  //set up the initial CLK frequency. This does not turn on the clocks, just sets the frequency, 
	  //the clocks are turned on during SPI transfers.
	  dalRes = DalClock_GetClockId(pClkHandle, tgtCfg->pQupAppClkName, &QupAppClkId);
      if ((DAL_SUCCESS != dalRes) || (NULL == QupAppClkId))
      {
         return;
      }
	  dalRes = DalClock_SetClockFrequency (pClkHandle, QupAppClkId, SPI_OPERATING_FREQ_HZ, CLOCK_FREQUENCY_HZ_AT_MOST, NULL);
      if (DAL_SUCCESS != dalRes)
      {
         return;
      }
   }
}

void SpiInit_DeviceInstance(spi_device_id_t device_id)
{
   static uint32 SpiInitDone = FALSE;
   uint32 clkSig, mosiSig, misoSig, csSig;
   
   if (SpiInitDone == FALSE)
   {
      SpiInit();
      SpiInitDone = TRUE;
   }
   
   clkSig = SpiGpioClk[device_id];
   csSig = SpiGpioCS[device_id];
   misoSig = SpiClkGpioMISO[device_id];
   mosiSig = SpiClkGpioMOSI[device_id];
   do
   {
      if(DAL_SUCCESS != DalTlmm_ConfigGpio(pTlmmHandle, (DALGpioSignalType) clkSig, DAL_TLMM_GPIO_ENABLE))
         break;
      if(DAL_SUCCESS != DalTlmm_ConfigGpio(pTlmmHandle, (DALGpioSignalType) mosiSig, DAL_TLMM_GPIO_ENABLE))
         break;
      if(DAL_SUCCESS != DalTlmm_ConfigGpio(pTlmmHandle, (DALGpioSignalType) misoSig, DAL_TLMM_GPIO_ENABLE))
         break;
      if(csSig)
      {
         if(DAL_SUCCESS != DalTlmm_ConfigGpio(pTlmmHandle, (DALGpioSignalType) csSig, DAL_TLMM_GPIO_ENABLE))
            break;
      }
   }
   while (0); 
}

void SpiDeInit_DeviceInstance(spi_device_id_t device_id)
{
   uint32 clkSig, mosiSig, misoSig, csSig;
   
   clkSig = SpiGpioClk[device_id];
   csSig = SpiGpioCS[device_id];
   misoSig = SpiClkGpioMISO[device_id];
   mosiSig = SpiClkGpioMOSI[device_id];

   do
   {
      if(DAL_SUCCESS != DalTlmm_ConfigGpio(pTlmmHandle, (DALGpioSignalType) clkSig, DAL_TLMM_GPIO_DISABLE))
         break;
      if(DAL_SUCCESS != DalTlmm_ConfigGpio(pTlmmHandle, (DALGpioSignalType) mosiSig, DAL_TLMM_GPIO_DISABLE))
         break;
      if(DAL_SUCCESS != DalTlmm_ConfigGpio(pTlmmHandle, (DALGpioSignalType) misoSig, DAL_TLMM_GPIO_DISABLE))
         break;
      if(csSig)
      {
         if(DAL_SUCCESS != DalTlmm_ConfigGpio(pTlmmHandle, (DALGpioSignalType) csSig, DAL_TLMM_GPIO_DISABLE))
            break;
      }
   }
   while (0); 
}

