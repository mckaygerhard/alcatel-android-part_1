/*=============================================================================

  FILE:     spi_adsp_8952.c

  OVERVIEW: Contains target specific SPI configuration for 8994 ADSP 
 
            Copyright (c) 2009 - 2015 Qualcomm Technologies Incorporated.
            All Rights Reserved.
            Qualcomm Confidential and Proprietary 

  ===========================================================================*/

/*=========================================================================
  EDIT HISTORY FOR MODULE

  $Header: //components/rel/core.adsp/2.6.6/buses/spi/src/config/spi_adsp_8952.c#1 $
  $DateTime: 2016/05/04 01:42:14 $$Author: pwbldsvc $

  When        Who    What, where, why
  --------    ---    -----------------------------------------------------------
  2015/12/21  dpk    Added Alternate CS lines GPIO configuration.
  2015/10/14  dpk    Updated the driver for sensors image
  2015/03/20  dpk    Updated the Base name for HWIO Map region
  2014/04/01  vk     Initial version

  ===========================================================================*/
#include "SpiDevicePlatSvc.h"
#include "SpiDriverTypes.h"
#include "SpiDriver.h"
#include "DALDeviceId.h"
#include "DDIHWIO.h"
#include "uTlmm.h"
#include "qurt_island.h"
#include "SpiConfig.h"

spiDevice spiDevices[] ATTRIBUTE_ISLAND_DATA = {
   { NULL, 0, 0 }, //device handle, core number, current status
   { NULL, 1, 0 },
   { NULL, 2, 0 }, 
   { NULL, 3, 0 },
   { NULL, 4, 0 },
   { NULL, 5, 0 },
   { NULL, 6, 0 },
   { NULL, 7, 0 }  //8956/76 has 8 QUP cores
};


SpiDevicePlat_DevCfg Spi_DeviceCfg[8] ATTRIBUTE_ISLAND_DATA;

#define SPI_PERIPH_SS_BASE_PHYS_ADDR 0x7880000
const uint32 SpiQupPhysAddrOffset[] ATTRIBUTE_ISLAND_DATA = {0x35000, 0x36000, 0x37000, 0x38000, 0x275000, 0x276000, 0x277000, 0x278000};
const uint32 SpiQupVirAddrOffset[] ATTRIBUTE_ISLAND_DATA = {0x35000, 0x36000, 0x37000, 0x38000, 0x35000, 0x36000, 0x37000, 0x38000};

static uClockIdType SpiClks[]  ATTRIBUTE_ISLAND_DATA =   {CLOCK_GCC_BLSP1_QUP1_SPI_APPS_CLK, CLOCK_GCC_BLSP1_QUP2_SPI_APPS_CLK, CLOCK_GCC_BLSP1_QUP3_SPI_APPS_CLK, CLOCK_GCC_BLSP1_QUP4_SPI_APPS_CLK,
 CLOCK_GCC_BLSP2_QUP1_SPI_APPS_CLK, CLOCK_GCC_BLSP2_QUP2_SPI_APPS_CLK, CLOCK_GCC_BLSP2_QUP3_SPI_APPS_CLK,
   CLOCK_GCC_BLSP2_QUP4_SPI_APPS_CLK};

const uint32 SpiGpioClk[] ATTRIBUTE_ISLAND_CONST = {0x2006C031, 0x2006C071, 0x2006C0B1, 0x2006C0F1, 0x2006C131, 0x2006C171, 0x2006C5F2, 0x2006C632};
const uint32 SpiGpioCS[] ATTRIBUTE_ISLAND_CONST = {0x2006C021, 0x2006C061, 0x2006C0A1, 0x2006C0E1, 0x2006C121, 0x2006C161, 0x2006C5E2, 0x2006C622};
const uint32 SpiClkGpioMISO[] ATTRIBUTE_ISLAND_CONST = {0x2006C011, 0x2006C051, 0x2006C091, 0x2006C0D1, 0x2006C111, 0x2006C151, 0x2006C5D1, 0x2006C611};
const uint32 SpiClkGpioMOSI[] ATTRIBUTE_ISLAND_CONST = {0x2006C001, 0x2006C041, 0x2006C081, 0x2006C0C1, 0x2006C101, 0x2006C141, 0x2006C5C1, 0x2006C601};
const uint32 SpiGpioCS_1[] ATTRIBUTE_ISLAND_CONST = {0, 0, 0, 0, 0, 0, 0, 0};
const uint32 SpiGpioCS_2[] ATTRIBUTE_ISLAND_CONST = {0, 0, 0, 0, 0, 0x2006c2f1, 0, 0};


void SpiInit(void)
{
   int32 i;
   SpiDevicePlat_DevCfg *tgtCfg;
   DALResult dalRes;
   uint8 *blsp1VirBase, *blsp2VirBase;
   DalDeviceHandle *phDalHWIO = NULL;
   
   blsp1VirBase = blsp2VirBase = NULL;
   memset(Spi_DeviceCfg, 0, sizeof(Spi_DeviceCfg));

   dalRes = DAL_DeviceAttach(DALDEVICEID_HWIO, &phDalHWIO);
   if ((DAL_SUCCESS != dalRes) || (NULL == phDalHWIO))
   {
      return;
   }
   if (DAL_SUCCESS != DalHWIO_MapRegion(phDalHWIO, "BLSP1_BLSP", &blsp1VirBase))
   {
      return;
   }

   if (DAL_SUCCESS != DalHWIO_MapRegion(phDalHWIO, "BLSP2_BLSP", &blsp2VirBase))
   {
      return;
   }
   
   for (i = 0; i < SPIPD_DEVICE_COUNT; i++)
   {
      tgtCfg = &(Spi_DeviceCfg[i]);
      tgtCfg->uQupCoreNum = spiDevices[i].qup_core_num;
      tgtCfg->bInterruptBased = 0;
      tgtCfg->qupPhysBlockAddr = SPI_PERIPH_SS_BASE_PHYS_ADDR + SpiQupPhysAddrOffset[tgtCfg->uQupCoreNum];
	  tgtCfg->qupVirtBlockAddr = (tgtCfg->uQupCoreNum < 4) ? (uint32)blsp1VirBase :(uint32)blsp2VirBase + SpiQupVirAddrOffset[tgtCfg->uQupCoreNum];
	  tgtCfg->QupAppClkId = SpiClks[tgtCfg->uQupCoreNum];
	  tgtCfg->QupHClkId = (tgtCfg->uQupCoreNum < 4) ? CLOCK_GCC_BLSP1_AHB_CLK : CLOCK_GCC_BLSP2_AHB_CLK;
   }
}

void SpiInit_DeviceInstance(spi_device_id_t device_id)
{
   static uint32 SpiInitDone = FALSE;
   uint32 clkSig, mosiSig, misoSig, csSig, cs1Sig, cs2Sig;
   
   if (SpiInitDone == FALSE &&  qurt_island_get_status() == 0)
   {
      SpiInit();
      SpiInitDone = TRUE;
   }
   
   clkSig = SpiGpioClk[device_id];
   csSig = SpiGpioCS[device_id];
   misoSig = SpiClkGpioMISO[device_id];
   mosiSig = SpiClkGpioMOSI[device_id];
   cs1Sig = SpiGpioCS_1[device_id];
   cs2Sig = SpiGpioCS_2[device_id];

   do
   {
      if (TRUE != uTlmm_ConfigGpio(clkSig, UTLMM_GPIO_ENABLE))
         break;
      if (TRUE != uTlmm_ConfigGpio(mosiSig, UTLMM_GPIO_ENABLE))
         break;
      if (TRUE != uTlmm_ConfigGpio(misoSig, UTLMM_GPIO_ENABLE))
         break;
      if (csSig)
      {
         if (TRUE != uTlmm_ConfigGpio(csSig, UTLMM_GPIO_ENABLE))
            break;
      }
      if (cs1Sig)
      {
         if (TRUE != uTlmm_ConfigGpio(cs1Sig, UTLMM_GPIO_ENABLE))
            break;
      }
      if (cs2Sig)
      {
         if (TRUE != uTlmm_ConfigGpio(cs2Sig, UTLMM_GPIO_ENABLE))
            break;
      }
   }
   while (0); 
}

void SpiDeInit_DeviceInstance(spi_device_id_t device_id)
{
   uint32 clkSig, mosiSig, misoSig, csSig, cs1Sig, cs2Sig;
   
   clkSig = SpiGpioClk[device_id];
   csSig = SpiGpioCS[device_id];
   misoSig = SpiClkGpioMISO[device_id];
   mosiSig = SpiClkGpioMOSI[device_id];
   cs1Sig = SpiGpioCS_1[device_id];
   cs2Sig = SpiGpioCS_2[device_id];

   do
   {
      if (TRUE != uTlmm_ConfigGpio(clkSig, UTLMM_GPIO_DISABLE))
         break;
      if (TRUE != uTlmm_ConfigGpio(mosiSig, UTLMM_GPIO_DISABLE))
         break;
      if (TRUE != uTlmm_ConfigGpio(misoSig, UTLMM_GPIO_DISABLE))
         break;
      if (csSig)
      {
         if (TRUE != uTlmm_ConfigGpio(csSig, UTLMM_GPIO_DISABLE))
            break;
      }
      if (cs1Sig)
      {
         if (TRUE != uTlmm_ConfigGpio(cs1Sig, UTLMM_GPIO_DISABLE))
            break;
      }
      if (cs2Sig)
      {
         if (TRUE != uTlmm_ConfigGpio(cs2Sig, UTLMM_GPIO_DISABLE))
            break;
      }
   }
   while (0); 
}

uint32 SpiConfig_GetMaxSourceClkFreqHz(void)
{
   return  19200000;
}

uint32 SpiConfig_GetMaxClkDividers(void)
{
   return 16;
}
