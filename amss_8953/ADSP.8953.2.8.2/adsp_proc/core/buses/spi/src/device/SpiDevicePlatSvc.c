/*
===========================================================================

FILE:   SpiDevicePlatSvc.c

DESCRIPTION:

===========================================================================

        Edit History

$Header: //components/rel/core.adsp/2.6.6/buses/spi/src/device/SpiDevicePlatSvc.c#1 $

When     Who    What, where, why
-------- ---    -----------------------------------------------------------
07/19/13 lk	Added xpu protection call. 
05/15/13 ag	Created 

===========================================================================
        Copyright c 2013 Qualcomm Technologies Incorporated.
            All Rights Reserved.
            Qualcomm Proprietary/GTDR

===========================================================================
*/

#include "SpiDevice.h"
#include "SpiDevicePlatSvc.h"
#include "SpiDeviceOsSvc.h"
#include "SpiDriver.h"
#include "SpiConfig.h"

#include "stddef.h"
#include "stdio.h"

#include "DALStdDef.h"
#include "DALSys.h"
#include "uClock.h"
#include "DDIHWIO.h"
#include "DDIClock.h"
#include "qurt_island.h"

typedef enum SpiDevicePlat_Error
{
   SPIDEVICE_PLAT_ERROR_DAL_GET_PROPERTY_HANDLE = SPI_ERROR_CLS_PLATFORM,
   SPIDEVICE_PLAT_ERROR_ATTACH_TO_CLOCKS,
   SPIDEVICE_PLAT_ERROR_GETTING_CLK_ID,
   SPIDEVICE_PLAT_ERROR_INVALID_POWER_STATE,
   SPIDEVICE_PLAT_ERROR_FAILED_TO_SET_APPCLK_FREQ,
   SPIDEVICE_PLAT_ERROR_FAILED_TO_ENABLE_APPCLK,
   SPIDEVICE_PLAT_ERROR_FAILED_TO_DISABLE_APPCLK,
   SPIDEVICE_PLAT_ERROR_FAILED_TO_ENABLE_HCLK,
   SPIDEVICE_PLAT_ERROR_FAILED_TO_DISABLE_HCLK,
   SPIDEVICE_PLAT_ERROR_NULL_PTR,
} SpiDevicePlat_Error;

extern SpiDevicePlat_DevCfg Spi_DeviceCfg[];
static int32 SpiDevicePlat_ClkCtrl(SpiDevicePlat_DevCfg *pDev, boolean enable);

static SpiDevicePlat_DevCfg* SpiDevicePlat_GetTargetConfig(uint32 qup_core_num)
{
   SpiDevicePlat_DevCfg *tgtCfg = NULL;
   int32 index = -1, i;

   for (i = 0; i < SPIPD_DEVICE_COUNT; i++)
   {
      if (Spi_DeviceCfg[i].uQupCoreNum == qup_core_num)
      {
         index = i;
      }
   }
   if (index == -1)
   {
      return NULL;
   }
   tgtCfg = &(Spi_DeviceCfg[index]);

   return tgtCfg;
}

static uint32 SpiDevicePlat_DetermineDivider(uint32 req_freq, uint32 *real_freq)
{
   uint32 i, divider = 0;
   *real_freq = 0;
   uint32 source_freq = SpiConfig_GetMaxSourceClkFreqHz();
   uint32 max_divider = SpiConfig_GetMaxClkDividers();

   if (req_freq >= source_freq)
   {
      divider = 1;
      *real_freq = source_freq;
   }
   else
   {
      for (i = 2; i <= max_divider * 2; i++)
      {
         if (((source_freq*2)/i) <= req_freq)
         {
            divider = i;
            break;
         }
      }
      if (divider != 0)
         *real_freq = (source_freq*2) / divider;
   }
   return divider;
}

int32 SpiDevicePlat_InitTarget(uint32 qup_core_num, SPIDEVICE_PLAT_HANDLE *phPlat)
{
   int32 res = -1;
   SpiDevicePlat_DevCfg *tgtCfg = NULL;

   tgtCfg = SpiDevicePlat_GetTargetConfig(qup_core_num);

   if (NULL != tgtCfg)
   {
      *phPlat = (SPIDEVICE_PLAT_HANDLE)tgtCfg;

      do
      {
         if (SPI_SUCCESS != (res = SpiDevicePlat_ClkCtrl(tgtCfg, TRUE)))
         {
            break;
         }
         res = SPI_SUCCESS;
      }
      while (0);
   }
   return res;
}

int32 SpiDevicePlat_DeInitTarget(SPIDEVICE_PLAT_HANDLE hPlat)
{
   /* SpiDevicePlat_InitTarget, just does data structure allocation.
   * The allocated data structures will be available for the life-time 
   * of the driver. There is no point in allocating and de-allocating 
   * them.
   */
   SpiDevicePlat_DevCfg *tgtCfg = (SpiDevicePlat_DevCfg *)hPlat;

   (void)SpiDevicePlat_ClkCtrl(tgtCfg, FALSE);
   
   return SPI_SUCCESS;
}

static int32 SpiDevicePlat_ClkCtrl(SpiDevicePlat_DevCfg *pDev, boolean enable)
{
   if(enable)
   {

      if(!uClock_EnableClock(pDev->QupHClkId))
     {
        return SPIDEVICE_PLAT_ERROR_FAILED_TO_ENABLE_HCLK;
     }

     if(!uClock_EnableClock(pDev->QupAppClkId)) 
     {
        return SPIDEVICE_PLAT_ERROR_FAILED_TO_ENABLE_APPCLK;
     }
   }
   else
   {

      if(!uClock_DisableClock(pDev->QupHClkId)) 
     {
       return SPIDEVICE_PLAT_ERROR_FAILED_TO_DISABLE_HCLK;
     }


     if(!uClock_DisableClock(pDev->QupAppClkId)) 
     {
       return SPIDEVICE_PLAT_ERROR_FAILED_TO_DISABLE_APPCLK;
     }

   }
   return SPI_SUCCESS;
}

uint32 SpiDevicePlat_SetAppClkHz (SPIDEVICE_PLAT_HANDLE hPlat,
                                  uint32 reqFrequencyHz,
                                  uint32 *pFinalFrequencyHz)
{
   uint32 divider;
   SpiDevicePlat_DevCfg *pDev = (SpiDevicePlat_DevCfg *)hPlat;
   divider = SpiDevicePlat_DetermineDivider(reqFrequencyHz, pFinalFrequencyHz);

   if (divider != 0 && uClock_SetClockDivider (pDev->QupAppClkId, divider))
      return SPI_SUCCESS;
   else 
      return SPIDEVICE_PLAT_ERROR_FAILED_TO_SET_APPCLK_FREQ;

}
