#ifndef _SPICONFIG_H_
#define _SPICONFIG_H_
/*
===========================================================================

FILE:   SpiConfig.h

DESCRIPTION:

===========================================================================

        Edit History



When     Who    What, where, why
-------- ---    -----------------------------------------------------------
01/16/15 vk     Created

===========================================================================
        Copyright (c) 2015 Qualcomm Technologies Incorporated.
            All Rights Reserved.
         Qualcomm Confidentail & Proprietary

===========================================================================
*/
uint32 SpiConfig_GetMaxSourceClkFreqHz(void);
uint32 SpiConfig_GetMaxClkDividers(void);

#endif
