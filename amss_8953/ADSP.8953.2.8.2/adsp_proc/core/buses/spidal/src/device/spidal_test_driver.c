#include "DALSys.h"
#include "DALStdErr.h"
#include "DALDeviceId.h"
#include "comdef.h"
#include "target.h"
#include "queue.h"
#include "memory.h"
#include "DALSys.h"
#include "ddispi.h"
#include "DALStdErr.h"
#include "DALDeviceId.h"
#include "err.h"
#include "rcinit.h"

#define  SPI_TEST_SUCCESS                 0  
#define  SPI_TEST_ERROR                  -1  // General failure
#define  SPI_TEST_FAIL                    1  
#define  SPI_TEST_ATTACH_FAIL             2  
#define  SPI_TEST_DEATTACH_FAIL           3
#define  SPI_TEST_CONFIG_FAIL             4  
#define  SPI_TEST_DEVICE_OPEN_FAIL        5  
#define  SPI_TEST_DEVICE_CLOSE_FAIL        6
#define  SPI_TEST_SPI_TRANSFER_FAIL       7  
#define  SPI_TEST_READ_WRITE_MATCHFAIL                    0xFFFF


#define  SPI_TEST_RPT_TIMER_SIG  0x00000002
//static uint32 spi_test_sleep_dur = 0x300000; //uSec

/*  Dog variables - now we MAY be dynamic       */
uint32  dog_spi_test_rpt_var       = 0;       /* dog report ID */
uint32  dog_spi_test_rpt_time = 1000;            /* Report Period */
uint8 spi_test_id = 0;
static volatile uint32 spi_test_ready = 0;
//rex_timer_type spi_test_rpt_timer;
static int32  test_spi_status = SPI_TEST_ERROR;
static int32  spi_trans_status = SPI_TEST_SUCCESS;

static uint32 test_spi_data = 0;
static uint32 write_mem_alloc_failed = 0;
static uint32 read_mem_alloc_failed = 0;
static uint32 currloopCnt = 0;

volatile uint32 IsTransferEnable = 0;

int spi_test(SpiTransferModeType mode)
{
	static DalDeviceHandle *hDalDevice = NULL;
	DALDEVICEID SpiDalDeviceId;
	static SpiDeviceInfoType spiDeviceInfo;
	static SpiTransferType spiTransfer;
	static SpiDataAddrType readBuf;
	static SpiDataAddrType writeBuf;

	// Allocate DAL memory region so that DAL ISRs can access it
	DALSYSMemHandle hWriteMem = NULL;
	DALSYSMemHandle hReadMem = NULL;
	static DALSYSMemInfo writeMemInfo;
	static DALSYSMemInfo readMemInfo;
	//DWORD writeBufLen = 512;
	//DWORD readBufLen = 512;
	uint32 writeBufLen = 64;
	uint32 readBufLen = 64;
	uint32 writeBufLenBytes = writeBufLen*4;
	uint32 readBufLenBytes = readBufLen*4;

	uint32 * pWriteBuf;
	uint32 * pReadBuf;
	uint32 i;
	uint32 transferId;
	int inputDataLen;
	DALResult dalResult;

   SpiDalDeviceId = DALDEVICEID_SPI_DEVICE_3;

	DALSYS_InitMod(NULL);

    if(DAL_SUCCESS != DAL_DeviceAttach(SpiDalDeviceId, //Device Id
      				&hDalDevice)  //Handle
      	  )
      	{
      		test_spi_status = SPI_TEST_ATTACH_FAIL;
			return -1;
      	}

	do
	{
		if( DAL_SUCCESS != DalDevice_Open(hDalDevice,
      				DAL_OPEN_SHARED) )
      	{
			test_spi_status = SPI_TEST_DEVICE_OPEN_FAIL;
      		return -1;
      	}
      	/* set up configuration */
      	spiDeviceInfo.deviceParameters.eClockMode = SPI_CLK_NORMAL;
      	spiDeviceInfo.deviceParameters.eClockPolarity = SPI_CLK_IDLE_HIGH;
      	spiDeviceInfo.deviceParameters.eShiftMode = SPI_OUTPUT_FIRST_MODE;
      	//spiDeviceInfo.deviceParameters.u32DeassertionTime =  1 * (1000000000UL / (10*1000*1000));
      	/*Aardvark need high de-assertion time close to 4 us*/
      	spiDeviceInfo.deviceParameters.u32DeassertionTime = 5000* 1 * (1000000000UL / (10*1000*1000));
      	spiDeviceInfo.deviceParameters.u32MinSlaveFrequencyHz = 0;
      	spiDeviceInfo.deviceParameters.u32MaxSlaveFrequencyHz = 10*1000*1000;
      	/*Typical frequency at which Aardvark can operate*/
      	//spiDeviceInfo.deviceParameters.u32MaxSlaveFrequencyHz = 1000*1000;
      	spiDeviceInfo.deviceParameters.eCSPolarity = SPI_CS_ACTIVE_LOW;
      	//spiDeviceInfo.deviceParameters.eCSMode = SPI_CS_DEASSERT;
      	/*Aardvark needs the CS ASSERTED througout the transfer*/
      	spiDeviceInfo.deviceParameters.eCSMode = SPI_CS_KEEP_ASSERTED;
      	spiDeviceInfo.deviceBoardInfo.nSlaveNumber = 0;
      	spiDeviceInfo.deviceBoardInfo.eCoreMode = SPI_CORE_MODE_MASTER;
      	//spiDeviceInfo.transferParameters.nNumBits = 32;
      	/*For Aardvark high clock rate N needs to be 8 to make sure we have 4us de-assertion time*/
      	spiDeviceInfo.transferParameters.nNumBits = 8;
      	spiDeviceInfo.transferParameters.eTransferMode = mode;
      	spiDeviceInfo.transferParameters.eLoopbackMode = SPI_LOOPBACK_ENABLED;
      	//spiDeviceInfo.transferParameters.eLoopbackMode = SPI_LOOPBACK_DISABLED;
      	spiDeviceInfo.transferParameters.eInputPacking = SPI_INPUT_PACKING_DISABLED;
      	spiDeviceInfo.transferParameters.eOutputUnpacking = SPI_OUTPUT_UNPACKING_DISABLED;
      	spiDeviceInfo.transferParameters.slaveTimeoutUs = 0;

      	if(DAL_SUCCESS != DalSpi_ConfigureDevice(hDalDevice, &spiDeviceInfo))
      	{
      		 test_spi_status = SPI_TEST_CONFIG_FAIL;
      		 return -1;
      	}
		
   if (IsTransferEnable)  /* c_pdonth only for testing Open/Close */
   {
      	/* allocate write buffer */
      	if( DALSYS_MemRegionAlloc(DALSYS_MEM_PROPS_UNCACHED|DALSYS_MEM_PROPS_PHYS_CONT,
      				DALSYS_MEM_ADDR_NOT_SPECIFIED, // dont specify virt
      				DALSYS_MEM_ADDR_NOT_SPECIFIED, // dont specify phys
      				writeBufLen*sizeof(uint32),
      				&hWriteMem,                 // returned handle
      				NULL                        // no alloc for this object
      				) != DAL_SUCCESS )
      	{
	        write_mem_alloc_failed = 1;
      		break;
      	}

      	DALSYS_MemInfo(hWriteMem, &writeMemInfo);
      	pWriteBuf = (uint32 *) writeMemInfo.VirtualAddr;

      	if( DALSYS_MemRegionAlloc(DALSYS_MEM_PROPS_UNCACHED|DALSYS_MEM_PROPS_PHYS_CONT,
      				DALSYS_MEM_ADDR_NOT_SPECIFIED,
      				DALSYS_MEM_ADDR_NOT_SPECIFIED, // dont specify phys
      				readBufLen*sizeof(uint32),
      				&hReadMem,                 // returned handle
      				NULL                        // no alloc for this object
      				) != DAL_SUCCESS )
      	{
	        read_mem_alloc_failed = 1;
      		break;
      	}

      	DALSYS_MemInfo(hReadMem, &readMemInfo);
      	pReadBuf = (uint32 *) readMemInfo.VirtualAddr;

      	for (i=0;i<writeBufLen;i++)
      	{
      		pWriteBuf[i] = i;
      	}

      	for (i=0;i<readBufLen;i++)
      	{
      		pReadBuf[i] = 0;
      	}

      	spiTransfer.u32NumOutputTransfers = writeBufLen;
      	spiTransfer.u32NumInputTransfers = readBufLen;
      	spiTransfer.dalSysEvent = NULL;
      	spiTransfer.queueIfBusy = TRUE;
      	spiTransfer.transferMode = mode;

      	writeBuf.virtualAddr = (void *) writeMemInfo.VirtualAddr;
      	writeBuf.physicalAddr = (void *) writeMemInfo.PhysicalAddr;

      	readBuf.virtualAddr = (void *) readMemInfo.VirtualAddr;
      	readBuf.physicalAddr = (void *) readMemInfo.PhysicalAddr;

      	dalResult = DalSpi_StartTransfer(hDalDevice, &spiTransfer, &writeBuf, writeBufLenBytes,
      					&readBuf, readBufLenBytes, &inputDataLen, &transferId);
      					
        spi_trans_status = dalResult;

      	if(SPI_COMPLETE != dalResult)
      	{
      		break;
      	}
      	
      	for (i=0;i<readBufLen;i++)
      	{
			if (pWriteBuf[i] != pReadBuf[i])
      		{
				test_spi_data = SPI_TEST_READ_WRITE_MATCHFAIL;
      		}
      	}
   	}
   
	}while(0);

if (IsTransferEnable)   /* c_pdonth only for testing Open/Close */
{

	if (hReadMem != NULL)
	{
		DALSYS_Free(hReadMem);
	}
	if (hWriteMem != NULL)
	{
		DALSYS_Free(hWriteMem);
	}
}

	if(DAL_SUCCESS != DalDevice_Close(hDalDevice))
	{
		test_spi_status = SPI_TEST_DEVICE_CLOSE_FAIL;
		return -1;
	}
	
	if(DAL_SUCCESS != DAL_DeviceDetach(hDalDevice))
	{
		test_spi_status = SPI_TEST_DEATTACH_FAIL;
		return -1;
	}

	return 0;
}

void test_spi_var_init(void)
{
	spi_trans_status = 0;
	test_spi_status = SPI_TEST_FAIL;
	test_spi_data = 0;
	write_mem_alloc_failed = 0;
	read_mem_alloc_failed = 0;
	currloopCnt = 0;
}


void tstspi_handler(void)
{
 	  if(!spi_test(SPI_TRANSFER_MODE_DEFAULT))
    {
	  		  test_spi_status = SPI_TEST_SUCCESS;
	  }

}
