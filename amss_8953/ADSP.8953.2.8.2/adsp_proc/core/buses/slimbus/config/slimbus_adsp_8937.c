/*
===========================================================================

FILE:         slimbus_bsp_data.c

DESCRIPTION:  This file implements the SLIMbus board support data.

===========================================================================

                             Edit History

$Header: //components/rel/core.adsp/2.6.6/buses/slimbus/config/slimbus_adsp_8937.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
09/11/15   SG     Initial revision.

===========================================================================
             Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

#include "DALSys.h"
#include "HALhwio.h"
#include "SlimBus.h"
#include "SlimBusDal.h"
#include "DDITlmm.h"
#include "mmpm.h"


/* Slimbus BSP data */
SlimBusBSPType SlimBusBSP[] =
{
  {
    2,
    "SLIMBUS",
    {0x00, 0x00, 0x30, 0x02, 0x17, 0x02},
    "LPASS",
    0x00140000,
    0x0C140000,
    0x0C104000,
    11,
    12,
    0,
    { DAL_GPIO_CFG(70, 1, DAL_GPIO_INPUT, DAL_GPIO_KEEPER, DAL_GPIO_8MA),
      DAL_GPIO_CFG(71, 1, DAL_GPIO_INPUT, DAL_GPIO_KEEPER, DAL_GPIO_8MA),
      DAL_GPIO_CFG(72, 1, DAL_GPIO_INPUT, DAL_GPIO_KEEPER, DAL_GPIO_8MA) },
    71,
    { 1, 1, 1 }
  }
};

const SlimBusDeviceDalProps sbDeviceProps[] = 
{
      {0xc0, {0x00, 0x00, 0x30, 0x02, 0x17, 0x02}, 0x01}, //SB Framer
      {0xc1, {0x00, 0x01, 0x30, 0x02, 0x17, 0x02}, 0x01}, //SB Interface
      {0xc2, {0x00, 0x03, 0x30, 0x02, 0x17, 0x02}, 0x01}, //SB NGD1
      {0xc3, {0x00, 0x04, 0x30, 0x02, 0x17, 0x02}, 0x01}, //SB NGD2
      {0xc4, {0x00, 0x05, 0x30, 0x02, 0x17, 0x02}, 0x03}, //SB PGD
      {0xc5, {0x00, 0x00, 0xe0, 0x00, 0x17, 0x02}, 0x01}, //Codec Tapan PGD
      {0xc6, {0x00, 0x01, 0xe0, 0x00, 0x17, 0x02}, 0x01}, //Codec Tapan Interface
      {0xc7, {0x00, 0x00, 0xa0, 0x01, 0x17, 0x02}, 0x01}, //Codec Tasha PGD 
      {0xc8, {0x00, 0x01, 0xa0, 0x01, 0x17, 0x02}, 0x03}, //Codec Tasha Interface
      {0xce, {0x00, 0x00, 0x30, 0x01, 0x17, 0x02}, 0x01}, //Codec TOMTOM PGD 
      {0xcf, {0x00, 0x01, 0x30, 0x01, 0x17, 0x02}, 0x01}  //Codec TOMTOM Interface
	  
};

const uint32 sbNumDeviceProps = sizeof(sbDeviceProps) / sizeof(SlimBusDeviceDalProps);

const MmpmRegParamType sbMmpmRegParam = 
{
  MMPM_REVISION,
  MMPM_CORE_ID_LPASS_SLIMBUS,
  MMPM_CORE_INSTANCE_0,
  "slimbus",
  PWR_CTRL_NONE,
  CALLBACK_NONE,
  NULL,
  0
};
