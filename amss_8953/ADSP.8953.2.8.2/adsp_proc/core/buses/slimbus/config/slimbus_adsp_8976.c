/*
===========================================================================

FILE:         slimbus_bsp_data.c

DESCRIPTION:  This file implements the SLIMbus board support data.

===========================================================================

                             Edit History

$Header: //components/rel/core.adsp/2.6.6/buses/slimbus/config/slimbus_adsp_8976.c#1 $

when       who     what, where, why
--------   ---     -------------------------------------------------------- 
05/11/15   NSD     Add dual data line support.
04/14/15   NSD     Initial revision for 8976 ADSP.

===========================================================================
             Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
===========================================================================
*/

#include "DALSys.h"
#include "HALhwio.h"
#include "SlimBus.h"
#include "SlimBusDal.h"
#include "DDITlmm.h"
#include "mmpm.h"

/* Slimbus BSP data */
SlimBusBSPType SlimBusBSP[] =
{
  {
    2,
    "SLIMBUS",
    { 0x00, 0x00, 0x70, 0x01, 0x17, 0x02 },  
    "LPASS",
    0x0140000,   
    0xC140000,   
    0x0C104000,  
    11,
    12, 
    0,
    { DAL_GPIO_CFG(117, 1, DAL_GPIO_INPUT, DAL_GPIO_KEEPER, DAL_GPIO_8MA),         
      DAL_GPIO_CFG(118, 1, DAL_GPIO_INPUT, DAL_GPIO_KEEPER, DAL_GPIO_8MA),         
      DAL_GPIO_CFG(119, 1, DAL_GPIO_INPUT, DAL_GPIO_KEEPER, DAL_GPIO_8MA)},		   
    118,
    { 1, 1, 1 }
  }
};

const SlimBusDeviceDalProps sbDeviceProps[] = 
{
      {0xc0, {0x00, 0x00, 0x70, 0x01, 0x17, 0x02}, 0x01},
      {0xc1, {0x00, 0x01, 0x70, 0x01, 0x17, 0x02}, 0x01},
      {0xc2, {0x00, 0x03, 0x70, 0x01, 0x17, 0x02}, 0x01},
      {0xc3, {0x00, 0x04, 0x70, 0x01, 0x17, 0x02}, 0x01},
      {0xc4, {0x00, 0x05, 0x70, 0x01, 0x17, 0x02}, 0x03},
      {0xca, {0x00, 0x00, 0xa0, 0x01, 0x17, 0x02}, 0x01},
      {0xcb, {0x00, 0x01, 0xa0, 0x01, 0x17, 0x02}, 0x03},
      {0xcc, {0x00, 0x00, 0x30, 0x01, 0x17, 0x02}, 0x01},
      {0xcd, {0x00, 0x01, 0x30, 0x01, 0x17, 0x02}, 0x01}
};

const uint32 sbNumDeviceProps = sizeof(sbDeviceProps) / sizeof(SlimBusDeviceDalProps);

const MmpmRegParamType sbMmpmRegParam = 
{
  MMPM_REVISION,
  MMPM_CORE_ID_LPASS_SLIMBUS,
  MMPM_CORE_INSTANCE_0,
  "slimbus",
  PWR_CTRL_NONE,
  CALLBACK_NONE,
  NULL,
  0
};
