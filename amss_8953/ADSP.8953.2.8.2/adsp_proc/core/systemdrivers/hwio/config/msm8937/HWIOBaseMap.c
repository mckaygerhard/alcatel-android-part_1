
/*
===========================================================================
*/
/**
  @file HWIOBaseMap.c
  @brief Auto-generated HWIO Device Configuration base file.

  DESCRIPTION:
    This file contains Device Configuration data structures for mapping
    physical and virtual memory for HWIO blocks.
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/core.adsp/2.6.6/systemdrivers/hwio/config/msm8937/HWIOBaseMap.c#1 $
  $DateTime: 2016/05/04 01:42:14 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "DalHWIO.h"


/*=========================================================================
      Data Definitions
==========================================================================*/

static HWIOModuleType HWIOModules_SPDM_WRAPPER_TOP[] =
{
  { "SPDM_SPDM_CREG",                              0x00000000, 0x00000120 },
  { "SPDM_SPDM_OLEM",                              0x00001000, 0x0000015c },
  { "SPDM_SPDM_RTEM",                              0x00002000, 0x00000318 },
  { "SPDM_SPDM_SREG",                              0x00004000, 0x00000120 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM_SS_MSG_RAM_START_ADDRESS[] =
{
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PDM_PERPH_WEB[] =
{
  { "PDM_WEB_TCXO4",                               0x00000000, 0x00004000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SECURITY_CONTROL[] =
{
  { "SECURITY_CONTROL_CORE",                       0x00000000, 0x00007000 },
  { "SECURE_CHANNEL",                              0x00008000, 0x00004200 },
  { "KEY_CTRL",                                    0x00008000, 0x00004000 },
  { "CRI_CM",                                      0x0000c000, 0x00000100 },
  { "CRI_CM_EXT",                                  0x0000c100, 0x00000100 },
  { "SEC_CTRL_XPU",                                0x0000e000, 0x00000b80 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PRNG_PRNG_TOP[] =
{
  { "PRNG_CM_CM_PRNG_CM",                          0x00000000, 0x00001000 },
  { "PRNG_TZ_TZ_PRNG_TZ",                          0x00001000, 0x00001000 },
  { "PRNG_MSA_MSA_PRNG_SUB",                       0x00002000, 0x00001000 },
  { "PRNG_EE2_EE2_PRNG_SUB",                       0x00003000, 0x00001000 },
  { "PRNG_EE3_EE3_PRNG_SUB",                       0x00004000, 0x00001000 },
  { "PRNG_EE4_EE4_PRNG_SUB",                       0x00005000, 0x00001000 },
  { "PRNG_EE5_EE5_PRNG_SUB",                       0x00006000, 0x00001000 },
  { "PRNG_EE6_EE6_PRNG_SUB",                       0x00007000, 0x00001000 },
  { "PRNG_EE7_EE7_PRNG_SUB",                       0x00008000, 0x00001000 },
  { "PRNG_EE8_EE8_PRNG_SUB",                       0x00009000, 0x00001000 },
  { "PRNG_EE9_EE9_PRNG_SUB",                       0x0000a000, 0x00001000 },
  { "PRNG_EE10_EE10_PRNG_SUB",                     0x0000b000, 0x00001000 },
  { "PRNG_EE11_EE11_PRNG_SUB",                     0x0000c000, 0x00001000 },
  { "PRNG_EE12_EE12_PRNG_SUB",                     0x0000d000, 0x00001000 },
  { "PRNG_EE13_EE13_PRNG_SUB",                     0x0000e000, 0x00001000 },
  { "PRNG_EE14_EE14_PRNG_SUB",                     0x0000f000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_RPM[] =
{
  { "RPM_DEC",                                     0x00000000, 0x00002000 },
  { "RPM_QTMR_AC",                                 0x00002000, 0x00001000 },
  { "RPM_F0_QTMR_V1_F0",                           0x00003000, 0x00001000 },
  { "RPM_F1_QTMR_V1_F1",                           0x00004000, 0x00001000 },
  { "RPM_APU",                                     0x00007000, 0x00000300 },
  { "RPM_VMIDMT",                                  0x00008000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_MPM2_MPM[] =
{
  { "MPM2_MPM",                                    0x00000000, 0x00001000 },
  { "MPM2_G_CTRL_CNTR",                            0x00001000, 0x00001000 },
  { "MPM2_G_RD_CNTR",                              0x00002000, 0x00001000 },
  { "MPM2_SLP_CNTR",                               0x00003000, 0x00001000 },
  { "MPM2_QTIMR_AC",                               0x00004000, 0x00001000 },
  { "MPM2_QTIMR_V1",                               0x00005000, 0x00001000 },
  { "MPM2_TSYNC",                                  0x00006000, 0x00001000 },
  { "MPM2_APU",                                    0x00007000, 0x00000780 },
  { "MPM2_TSENS",                                  0x00008000, 0x00001000 },
  { "MPM2_TSENS_TM",                               0x00009000, 0x00001000 },
  { "MPM2_WDOG",                                   0x0000a000, 0x00000020 },
  { "MPM2_PSHOLD",                                 0x0000b000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PC_NOC[] =
{
  { "PC_NOC",                                      0x00000000, 0x00014080 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_SYSTEM_NOC[] =
{
  { "SYSTEM_NOC",                                  0x00000000, 0x00016080 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_TLMM[] =
{
  { "TLMM_MPU1132_16_M22L12_AHB",                  0x00300000, 0x00000a00 },
  { "TLMM_CSR",                                    0x00000000, 0x00300000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CLK_CTL[] =
{
  { "GCC_CLK_CTL_REG",                             0x00000000, 0x00080000 },
  { "GCC_RPU_RPU1132_32_L12",                      0x00080000, 0x00001200 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_CORE_TOP_CSR[] =
{
  { "TCSR_TCSR_MUTEX",                             0x00005000, 0x00020000 },
  { "TCSR_REGS_APU1132_16",                        0x00036000, 0x00000a00 },
  { "TCSR_TCSR_REGS",                              0x00037000, 0x00021000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_PMIC_ARB[] =
{
  { "SPMI_CFG_TOP",                                0x00000000, 0x0000d000 },
  { "SPMI_GENI_CFG",                               0x0000a000, 0x00000700 },
  { "SPMI_CFG",                                    0x0000a700, 0x00001a00 },
  { "SPMI_PIC",                                    0x01800000, 0x00200000 },
  { "PMIC_ARB_MPU1132_25_M25L12_AHB",              0x0000e000, 0x00000e6d },
  { "PMIC_ARB_CORE",                               0x0000f000, 0x00001000 },
  { "PMIC_ARB_CORE_REGISTERS",                     0x00400000, 0x00800000 },
  { "PMIC_ARB_CORE_REGISTERS_OBS",                 0x00c00000, 0x00800000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_QDSS_APB_DEC_QDSS_APB[] =
{
  { "QDSS_APB_DEC_DAPROM",                         0x00000000, 0x00001000 },
  { "QDSS_APB_DEC_QDSSCSR",                        0x00001000, 0x00001000 },
  { "QDSS_APB_DEC_CXSTM_8_32_32_TRUE",             0x00002000, 0x00001000 },
  { "QDSS_APB_DEC_TPDA_TPDA_TPDA",                 0x00003000, 0x00001000 },
  { "QDSS_APB_DEC_CTI0_CTI0_CSCTI",                0x00010000, 0x00001000 },
  { "QDSS_APB_DEC_CTI1_CTI1_CSCTI",                0x00011000, 0x00001000 },
  { "QDSS_APB_DEC_CTI2_CTI2_CSCTI",                0x00012000, 0x00001000 },
  { "QDSS_APB_DEC_CTI3_CTI3_CSCTI",                0x00013000, 0x00001000 },
  { "QDSS_APB_DEC_CTI4_CTI4_CSCTI",                0x00014000, 0x00001000 },
  { "QDSS_APB_DEC_CTI5_CTI5_CSCTI",                0x00015000, 0x00001000 },
  { "QDSS_APB_DEC_CTI6_CTI6_CSCTI",                0x00016000, 0x00001000 },
  { "QDSS_APB_DEC_CTI7_CTI7_CSCTI",                0x00017000, 0x00001000 },
  { "QDSS_APB_DEC_CTI8_CTI8_CSCTI",                0x00018000, 0x00001000 },
  { "QDSS_APB_DEC_CTI9_CTI9_CSCTI",                0x00019000, 0x00001000 },
  { "QDSS_APB_DEC_CTI10_CTI10_CSCTI",              0x0001a000, 0x00001000 },
  { "QDSS_APB_DEC_CTI11_CTI11_CSCTI",              0x0001b000, 0x00001000 },
  { "QDSS_APB_DEC_CTI12_CTI12_CSCTI",              0x0001c000, 0x00001000 },
  { "QDSS_APB_DEC_CTI13_CTI13_CSCTI",              0x0001d000, 0x00001000 },
  { "QDSS_APB_DEC_CTI14_CTI14_CSCTI",              0x0001e000, 0x00001000 },
  { "QDSS_APB_DEC_CTI15_CTI15_CSCTI",              0x0001f000, 0x00001000 },
  { "QDSS_APB_DEC_CSTPIU_CSTPIU_CSTPIU",           0x00020000, 0x00001000 },
  { "QDSS_APB_DEC_IN_FUN0_IN_FUN0_CXATBFUNNEL_128W8SP", 0x00021000, 0x00001000 },
  { "QDSS_APB_DEC_REPL64_REPL64_CXATBREPLICATOR_64WP", 0x00026000, 0x00001000 },
  { "QDSS_APB_DEC_ETR_ETR_CXTMC_R64W32D",          0x00028000, 0x00001000 },
  { "QDSS_APB_DEC_ETFETB_ETFETB_CXTMC_F128W16K",   0x00027000, 0x00001000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_QDSS_AHB_DEC_QDSS_AHB[] =
{
  { "QDSS_AHB_DEC_NDPBAM_NDPBAM_BAM_NDP_TOP_AUTO_SCALE_V2_0", 0x00000000, 0x00019000 },
  { "QDSS_AHB_DEC_NDPBAM_BAM",                     0x00004000, 0x00015000 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_QDSS_WRAPPER_TOP[] =
{
  { "QDSS_WRAPPER_QDA_CENTER_FUN_QDA_CENTER_CXATBFUNNEL_32W8SP", 0x00000000, 0x00001000 },
  { "QDSS_WRAPPER_CENTER_HWE_MUX_WRAPPER",         0x00001000, 0x00001000 },
  { "QDSS_WRAPPER_QDA_RIGHT_FUN_QDA_RIGHT_CXATBFUNNEL_128W8SP", 0x00020000, 0x00001000 },
  { "QDSS_WRAPPER_RIGHT_HWE_MUX_WRAPPER",          0x00021000, 0x00001000 },
  { "QDSS_WRAPPER_QDA_MM_FUN_QDA_MM_CXATBFUNNEL_128W8SP", 0x00030000, 0x00001000 },
  { "QDSS_WRAPPER_QDA_MM_HWE_MUX_WRAPPER",         0x00031000, 0x00001000 },
  { "QDSS_WRAPPER_QDA_CAMERA_FUN_QDA_CAMERA_CXATBFUNNEL_32W8SP", 0x00032000, 0x00001000 },
  { "QDSS_WRAPPER_DEBUG_UI",                       0x00008000, 0x00001000 },
  { "APB2JTAG",                                    0x00004000, 0x00004000 },
  { "DCC_TPDM_TPDM_ATB8_ATCLK_CMB32_CSDED7E5A1",   0x00010000, 0x00001000 },
  { "DCC_TPDM_TPDM_ATB8_ATCLK_CMB32_CSDED7E5A1_SUB", 0x00010280, 0x00000d80 },
  { "DCC_TPDM_TPDM_ATB8_ATCLK_CMB32_CSDED7E5A1_GPR", 0x00010000, 0x0000027d },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_BLSP1_BLSP[] =
{
  { "BLSP1_BLSP_BAM",                              0x00004000, 0x0001f000 },
  { "BLSP1_BLSP_BAM_XPU2",                         0x00002000, 0x00002000 },
  { "BLSP1_BLSP_BAM_VMIDMT",                       0x00000000, 0x00001000 },
  { "BLSP1_BLSP_UART0_UART0_DM",                   0x0002f000, 0x00000200 },
  { "BLSP1_BLSP_UART1_UART1_DM",                   0x00030000, 0x00000200 },
  { "BLSP1_BLSP_QUP0",                             0x00035000, 0x00000600 },
  { "BLSP1_BLSP_QUP1",                             0x00036000, 0x00000600 },
  { "BLSP1_BLSP_QUP2",                             0x00037000, 0x00000600 },
  { "BLSP1_BLSP_QUP3",                             0x00038000, 0x00000600 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_BLSP2_BLSP[] =
{
  { "BLSP2_BLSP_BAM",                              0x00004000, 0x0001f000 },
  { "BLSP2_BLSP_BAM_XPU2",                         0x00002000, 0x00002000 },
  { "BLSP2_BLSP_BAM_VMIDMT",                       0x00000000, 0x00001000 },
  { "BLSP2_BLSP_UART0_UART0_DM",                   0x0002f000, 0x00000200 },
  { "BLSP2_BLSP_UART1_UART1_DM",                   0x00030000, 0x00000200 },
  { "BLSP2_BLSP_QUP0",                             0x00035000, 0x00000600 },
  { "BLSP2_BLSP_QUP1",                             0x00036000, 0x00000600 },
  { "BLSP2_BLSP_QUP2",                             0x00037000, 0x00000600 },
  { "BLSP2_BLSP_QUP3",                             0x00038000, 0x00000600 },
  { NULL, 0, 0 }
};

static HWIOModuleType HWIOModules_LPASS[] =
{
  { "LPASS_LPASS_CC_REG",                          0x00000000, 0x0004e000 },
  { "LPASS_LPASS_TCSR",                            0x00050000, 0x00010000 },
  { "LPASS_LPASS_QOS",                             0x00060000, 0x00004000 },
  { "LPASS_QOS_QOS_GENERIC",                       0x00060000, 0x00000100 },
  { "LPASS_QOS_QOS_DEBUG",                         0x00060100, 0x00000200 },
  { "LPASS_QOS_QOS_DANGER",                        0x00060300, 0x00000200 },
  { "LPASS_QOS_QOS_INTERRUPTS",                    0x00061000, 0x00002000 },
  { "LPASS_TOP_LPASS_TCSR_XPU",                    0x00064000, 0x00001000 },
  { "LPASS_AHBE_TIME",                             0x00071000, 0x00001000 },
  { "LPASS_SENSOR",                                0x00080000, 0x00008000 },
  { "LPASS_LPASS_CSR",                             0x00090000, 0x00010000 },
  { "LPASS_AHBI_TIME",                             0x000a2000, 0x00001000 },
  { "LPASS_LPASS_SYNC_WRAPPER",                    0x000a3000, 0x00000ffd },
  { "LPASS_AVTIMER",                               0x000a3000, 0x00000100 },
  { "LPASS_LPASS_AUDSYNC_WRAPPER",                 0x000a4000, 0x000001fd },
  { "LPASS_CORE_LPASS_CORE_QOS",                   0x000bc000, 0x00003000 },
  { "LPASS_CORE_QOS_QOS_GENERIC",                  0x000bc000, 0x00000100 },
  { "LPASS_CORE_QOS_QOS_CORE_DEBUG",               0x000bc100, 0x00000200 },
  { "LPASS_CORE_QOS_QOS_INTERRUPTS",               0x000bd000, 0x00002000 },
  { "LPASS_LPA_IF",                                0x000c0000, 0x0001fffd },
  { "LPASS_LPASS_LPM",                             0x000e0000, 0x00008000 },
  { "LPASS_CODEC_CORE",                            0x000f0000, 0x00000314 },
  { "LPASS_SBMASTER0_BASE",                        0x00100000, 0x00080000 },
  { "LPASS_SB_SLIMBUS_BAM_LITE",                   0x00100000, 0x0002e000 },
  { "LPASS_SB_BAM",                                0x00104000, 0x0002a000 },
  { "LPASS_SLIMBUS",                               0x00140000, 0x0002c000 },
  { "LPASS_QDSP6SS_PUBLIC",                        0x00200000, 0x00080000 },
  { "LPASS_QDSP6SS_PUB",                           0x00200000, 0x00004040 },
  { "LPASS_QDSP6SS_PRIVATE",                       0x00280000, 0x00080000 },
  { "LPASS_QDSP6SS_CSR",                           0x00280000, 0x00008028 },
  { "LPASS_QDSP6SS_L2VIC",                         0x00290000, 0x00001000 },
  { "LPASS_QDSP6SS_QDSP6SS_QTMR_AC",               0x002a0000, 0x00001000 },
  { "LPASS_QDSP6SS_QTMR_F0_0",                     0x002a1000, 0x00001000 },
  { "LPASS_QDSP6SS_QTMR_F1_1",                     0x002a2000, 0x00001000 },
  { "LPASS_QDSP6SS_QTMR_F2_2",                     0x002a3000, 0x00001000 },
  { "LPASS_QDSP6SS_QDSP6SS_SAW2",                  0x002b0000, 0x00000ff0 },
  { NULL, 0, 0 }
};

HWIOPhysRegionType HWIOBaseMap[] =
{
  {
    "SPDM_WRAPPER_TOP",
    (DALSYSMemAddr)0x00040000,
    0x00005000,
    (DALSYSMemAddr)0xe0040000,
    HWIOModules_SPDM_WRAPPER_TOP
  },
  {
    "RPM_SS_MSG_RAM_START_ADDRESS",
    (DALSYSMemAddr)0x00060000,
    0x00006000,
    (DALSYSMemAddr)0xe0160000,
    HWIOModules_RPM_SS_MSG_RAM_START_ADDRESS
  },
  {
    "PDM_PERPH_WEB",
    (DALSYSMemAddr)0x00068000,
    0x00004000,
    (DALSYSMemAddr)0xe0268000,
    HWIOModules_PDM_PERPH_WEB
  },
  {
    "SECURITY_CONTROL",
    (DALSYSMemAddr)0x000a0000,
    0x0000f000,
    (DALSYSMemAddr)0xe03a0000,
    HWIOModules_SECURITY_CONTROL
  },
  {
    "PRNG_PRNG_TOP",
    (DALSYSMemAddr)0x000e0000,
    0x00010000,
    (DALSYSMemAddr)0xe04e0000,
    HWIOModules_PRNG_PRNG_TOP
  },
  {
    "RPM",
    (DALSYSMemAddr)0x00280000,
    0x00009000,
    (DALSYSMemAddr)0xe0580000,
    HWIOModules_RPM
  },
  {
    "MPM2_MPM",
    (DALSYSMemAddr)0x004a0000,
    0x0000c000,
    (DALSYSMemAddr)0xe06a0000,
    HWIOModules_MPM2_MPM
  },
  {
    "PC_NOC",
    (DALSYSMemAddr)0x00500000,
    0x00015000,
    (DALSYSMemAddr)0xe0700000,
    HWIOModules_PC_NOC
  },
  {
    "SYSTEM_NOC",
    (DALSYSMemAddr)0x00580000,
    0x00017000,
    (DALSYSMemAddr)0xe0880000,
    HWIOModules_SYSTEM_NOC
  },
  {
    "TLMM",
    (DALSYSMemAddr)0x01000000,
    0x00301000,
    (DALSYSMemAddr)0xe0c00000,
    HWIOModules_TLMM
  },
  {
    "CLK_CTL",
    (DALSYSMemAddr)0x01800000,
    0x00082000,
    (DALSYSMemAddr)0xe1000000,
    HWIOModules_CLK_CTL
  },
  {
    "CORE_TOP_CSR",
    (DALSYSMemAddr)0x01900000,
    0x00058000,
    (DALSYSMemAddr)0xe1100000,
    HWIOModules_CORE_TOP_CSR
  },
  {
    "PMIC_ARB",
    (DALSYSMemAddr)0x02000000,
    0x01908000,
    (DALSYSMemAddr)0xe2000000,
    HWIOModules_PMIC_ARB
  },
  {
    "QDSS_APB_DEC_QDSS_APB",
    (DALSYSMemAddr)0x06000000,
    0x00029000,
    (DALSYSMemAddr)0xe4000000,
    HWIOModules_QDSS_APB_DEC_QDSS_APB
  },
  {
    "QDSS_AHB_DEC_QDSS_AHB",
    (DALSYSMemAddr)0x06040000,
    0x00019000,
    (DALSYSMemAddr)0xe4140000,
    HWIOModules_QDSS_AHB_DEC_QDSS_AHB
  },
  {
    "QDSS_WRAPPER_TOP",
    (DALSYSMemAddr)0x06100000,
    0x00033000,
    (DALSYSMemAddr)0xe4200000,
    HWIOModules_QDSS_WRAPPER_TOP
  },
  {
    "BLSP1_BLSP",
    (DALSYSMemAddr)0x07880000,
    0x00039000,
    (DALSYSMemAddr)0xe4380000,
    HWIOModules_BLSP1_BLSP
  },
  {
    "BLSP2_BLSP",
    (DALSYSMemAddr)0x07ac0000,
    0x00039000,
    (DALSYSMemAddr)0xe44c0000,
    HWIOModules_BLSP2_BLSP
  },
  {
    "LPASS",
    (DALSYSMemAddr)0x0c000000,
    0x002b1000,
    (DALSYSMemAddr)0xee000000,
    HWIOModules_LPASS
  },
  { NULL, 0, 0, 0, NULL }
};

