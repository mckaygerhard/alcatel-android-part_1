/*
==============================================================================

FILE:         HALclkDIGCODEC.c

DESCRIPTION:
   This auto-generated file contains the clock HAL code for the 
   DIGCODEC clocks.

   List of clock domains:
   -HAL_clk_mLPASSCDCClkDomain


==============================================================================

                             Edit History

$Header: //components/rel/core.adsp/2.6.6/systemdrivers/hal/clk/hw/msm8937/src/lcc/HALclkDIGCODEC.c#1 $

when          who     what, where, why
----------    ---     --------------------------------------------------------
11/28/2014            Auto-generated.

==============================================================================
            Copyright (c) 2015 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
                  QUALCOMM Proprietary/GTDR
==============================================================================
*/

/*============================================================================

                     INCLUDE FILES FOR MODULE

============================================================================*/


#include <HALhwio.h>

#include "HALclkInternal.h"
#include "HALclkTest.h"
#include "HALclkGeneric.h"
#include "HALclkHWIO.h"


/*============================================================================

             DEFINITIONS AND DECLARATIONS FOR MODULE

=============================================================================*/


/* ============================================================================
**    Prototypes
** ==========================================================================*/


/* ============================================================================
**    Externs
** ==========================================================================*/

extern HAL_clk_ClockDomainControlType  HAL_clk_mLPASSClockDomainControl;
extern HAL_clk_ClockDomainControlType  HAL_clk_mLPASSClockDomainControlRO;


/* ============================================================================
**    Data
** ==========================================================================*/

                                    
/*                           
 *  HAL_clk_mCDCClkDomainClks
 *                  
 *  List of clocks supported by this domain.
 */
static HAL_clk_ClockDescType HAL_clk_mCDCClkDomainClks[] =
{
  {
    /* .szClockName      = */ "digcodec_clk",
    /* .mRegisters       = */ { HWIO_OFFS(LPASS_DIGCODEC_CBCR), 0, {0, 0} },
    /* .pmControl        = */ &HAL_clk_mGenericClockControl,
    /* .nTestClock       = */ HAL_CLK_LPASS_TEST_DIGCODEC_CLK
  },
};


/*
 * HAL_clk_mLPASSCDCClkDomain
 *
 * CDC clock domain.
 */
HAL_clk_ClockDomainDescType HAL_clk_mLPASSCDCClkDomain =
{
  /* .nCGRAddr             = */ HWIO_OFFS(LPASS_DIGCODEC_CMD_RCGR),
  /* .pmClocks             = */ HAL_clk_mCDCClkDomainClks,
  /* .nClockCount          = */ sizeof(HAL_clk_mCDCClkDomainClks)/sizeof(HAL_clk_mCDCClkDomainClks[0]),
  /* .pmControl            = */ &HAL_clk_mLPASSClockDomainControl,
  /* .pmNextClockDomain    = */ NULL
};

