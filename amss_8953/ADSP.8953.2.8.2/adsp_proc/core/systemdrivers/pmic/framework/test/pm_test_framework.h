#ifndef PM_TEST_FRAMEWORK_H
#define PM_TEST_FRAMEWORK_H

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                PM NPA FRAMEWORK TEST HEADER

GENERAL DESCRIPTION
  This file contains initialization functions for NPA

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2010           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/03/14   mr      Added PMIC Framework/Driver Test support. (CR-713705)
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "string.h"
#include "com_dtypes.h"

#include "pm_lib_err.h"


/*===========================================================================

                 LOCAL CONSTANT AND MACRO DEFINITIONS

===========================================================================*/
typedef enum
{
    PM_TEST_LVL_NONE,

    /* Driver Test Level */
    PM_TEST_DRIVER_LVL_0,
    PM_TEST_DRIVER_LVL_1,
    PM_TEST_DRIVER_LVL_2,

    /* NPA Framework Test Level */
    PM_TEST_NPA_LVL_0,
    PM_TEST_NPA_LVL_1,
} pm_test_level;

typedef enum
{
    PM_NO_TEST,

    /* CX/MX NPA Voting - Not a API */
    PM_NPA_CX_VOTE,
    PM_NPA_MX_VOTE,
} pm_test_apis;

#define PM_TEST_ARRAY_SIZE    4

typedef struct
{
    pm_test_level lvl : 8;
    pm_test_apis api  : 8;
    uint32 rcrs_id    : 8;
    pm_err_flag_type err_code   : 8;
} pm_test_driver_state;


/*===========================================================================

                EXPORTED GLOBAL VARIABLES DEFINITIONS

===========================================================================*/
extern pm_test_driver_state g_pm_test_status[PM_TEST_ARRAY_SIZE];
extern uint8 g_f_cnt;


/*===========================================================================

                LOCAL FUNCTION PROTOTYPES

===========================================================================*/

void pm_test_handle_error(pm_test_apis api, uint8 rcrs, pm_err_flag_type error);

pm_err_flag_type pm_test_npa_framework ( void );


#endif  /* PM_TEST_FRAMEWORK_H */

