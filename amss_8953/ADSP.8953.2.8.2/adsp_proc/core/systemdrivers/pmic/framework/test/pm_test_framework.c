/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             PM FRAMEWORK TEST

GENERAL DESCRIPTION
  This file contains Driver Framework test codes.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2014-15        by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header:
$DateTime:

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/05/15   mr      Added NPA stub for Pre-Silicon (RUMI/VIRTIO) s/w validation (CR-803648)
===========================================================================*/

/*===========================================================================

                INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pm_test_framework.h"


/*===========================================================================

                VARIABLES DEFINITIONS

===========================================================================*/
pm_test_driver_state g_pm_test_status[PM_TEST_ARRAY_SIZE];
uint8 g_f_cnt = 0;


/*===========================================================================

                TEST FRAMEWORK ERROR LOGGING API

===========================================================================*/
void pm_test_handle_error(pm_test_apis api, uint8 rcrs, pm_err_flag_type error)
{
    if (g_f_cnt <= PM_TEST_ARRAY_SIZE-1)
    {
        g_pm_test_status[g_f_cnt].api = api;
        g_pm_test_status[g_f_cnt].rcrs_id = rcrs;
        g_pm_test_status[g_f_cnt].err_code = error;
    }
    ++g_f_cnt;

    if (g_f_cnt < PM_TEST_ARRAY_SIZE)
    {
        g_pm_test_status[g_f_cnt].lvl = g_pm_test_status[g_f_cnt-1].lvl;
    }
}


/*===========================================================================

                TEST FRAMEWORK INIT DEFINITIONS

===========================================================================*/
pm_err_flag_type pm_test_framework (void)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    memset(g_pm_test_status, 0, sizeof(g_pm_test_status));

    pm_test_npa_framework ();

    return err_flag;
}

