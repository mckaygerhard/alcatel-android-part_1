/*==============================================================================
@file DevCfgDyn.c

        Copyright � 2015 Qualcomm Technologies Incorporated.
        All Rights Reserved.
        Qualcomm Confidential and Proprietary
==============================================================================*/
#include "dlfcn.h"
#include "platform_libs.h"
#include "DALSysTypes.h"
#include "DALSysCmn.h"
#include "DALSysInt.h"


void* gpDevCfgDLOpenhandle = NULL;
extern DALProps* gpDALPROP_PropsShObjInfo;

// initialization dependency
PL_DEP(rtld)

static int DEVCFG_LOADER_INIT(void)
{  
   return PL_INIT(rtld);
}

static void DEVCFG_LOADER_DEINIT(void)
{
   dlclose(gpDevCfgDLOpenhandle);
}

DALResult DEVCFG_LOADER(void)
{  
   if(!gpDevCfgDLOpenhandle)
   {  

      if(!DEVCFG_LOADER_INIT())
      { 
        return DAL_ERROR;
      }

      gpDevCfgDLOpenhandle = dlopen("DevCfg_guestos.so", RTLD_NOW);
      if(!gpDevCfgDLOpenhandle)
      {
         DALSYS_LOG_FATAL_EVENT("pDevCfgDLhandle error: %s", dlerror());
         return DAL_ERROR;
      }
      gpDALPROP_PropsShObjInfo = dlsym(gpDevCfgDLOpenhandle, "DALPROP_PropsInfo");
      if(!gpDALPROP_PropsShObjInfo)
      {
         DALSYS_LOG_FATAL_EVENT("DALPROP_PropsShObjInfo error: %s", dlerror());
         return DAL_ERROR;
      }
   }

   return DAL_SUCCESS;
}

DALResult DEVCFG_DELOADER(void)
{  
   DEVCFG_LOADER_DEINIT();
   return DAL_SUCCESS;
}   

