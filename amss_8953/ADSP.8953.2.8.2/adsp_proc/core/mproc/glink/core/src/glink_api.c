/*===========================================================================

                    GLINK API Source File


 Copyright (c) 2014 by QUALCOMM Technologies, Incorporated.  All Rights
 Reserved.
===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/core.adsp/2.6.6/mproc/glink/core/src/glink_api.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
3/22/14    bm     Initial version. 
===========================================================================*/


/*===========================================================================
                        INCLUDE FILES
===========================================================================*/
#include "glink.h"
#include "glink_os.h"
#include "glink_internal.h"
#include "smem_list.h"

#define GLINK_NOT_INITIALIZED 0
#define GLINK_INITIALIZED     1
#define GLINK_NUM_HOSTS       6

/*===========================================================================
                              GLOBAL DATA DECLARATIONS
===========================================================================*/
int glink_core_status = GLINK_NOT_INITIALIZED;

void *glink_transport_q_cs;
void *glink_mem_log_cs;

glink_mem_log_entry_type glink_mem_log_arr[GLINK_MEM_LOG_SIZE];
uint32 glink_mem_log_idx = 0;

/* Keep a list of registered transport for each edge allowed for this host */
smem_list_type glink_registered_transports[GLINK_NUM_HOSTS];

/* List of supported hosts */
const char* glink_hosts_supported[]   = { "apps",
                                          "modem",
                                          "adsp",
                                          "dsps",
                                          "wcnss",
                                          "rpm",
                                        };
/*===========================================================================
                    LOCAL FUNCTION DEFINITIONS
===========================================================================*/
static glink_err_type glinki_add_ch_to_xport
(
  glink_transport_if_type  *if_ptr,
  glink_channel_ctx_type   *ch_ctx 
)
{
  glink_err_type             status;
  glink_channel_ctx_type     *open_ch_ctx;
  glink_core_xport_ctx_type  *xport_ctx = if_ptr->glink_core_priv;

  /* See if channel already exists in open_list */
  open_ch_ctx = smem_list_first(&if_ptr->glink_core_priv->open_list);
  while(open_ch_ctx != NULL)
  {
    if(strncmp(open_ch_ctx->name, ch_ctx->name, sizeof(ch_ctx->name)) == 0
        && strlen(open_ch_ctx->name) == strlen(ch_ctx->name) ) 
    {
      /* We've found a channel name is already in the list of open channel */

      /* Case A: Channel was opened before on the same host */
      if(open_ch_ctx->state == GLINK_CH_STATE_REMOTE_OPEN) {
        /* Set the channel state to OPEN and send ACK to transport */
        open_ch_ctx->state = GLINK_CH_STATE_OPEN;
        smem_list_init(&open_ch_ctx->remote_intent_q);
        smem_list_init(&open_ch_ctx->remote_intent_pending_tx_q);
        smem_list_init(&open_ch_ctx->local_intent_q);
        smem_list_init(&open_ch_ctx->local_intent_client_q);

        if_ptr->tx_cmd_ch_remote_open_ack(if_ptr, open_ch_ctx->rcid);

        /* Copy local open ctx params */
        open_ch_ctx->notify_rx = ch_ctx->notify_rx;
        open_ch_ctx->notify_tx_done = ch_ctx->notify_tx_done;
        open_ch_ctx->notify_state = ch_ctx->notify_state;

        glink_cs_lock(&if_ptr->glink_core_priv->liid_cs, TRUE);
        open_ch_ctx->lcid = if_ptr->glink_core_priv->free_lcid;
        if_ptr->glink_core_priv->free_lcid++;
        glink_cs_lock(&if_ptr->glink_core_priv->liid_cs, FALSE);

        glink_free(ch_ctx);

        /* Inform the client */
        open_ch_ctx->notify_state(open_ch_ctx, open_ch_ctx->priv, GLINK_CONNECTED);

        /* Send the OPEN command to transport */
        return if_ptr->tx_cmd_ch_open(if_ptr, 
                    open_ch_ctx->lcid, open_ch_ctx->name);
      } else { 
        /* Case B: Channel was already opened, return error */
        glink_free(ch_ctx);
        return GLINK_STATUS_FAILURE;
      }
    } /* end if match found */
    open_ch_ctx = smem_list_next(open_ch_ctx);
  }/* end while */        

  /* Channel not in the list - it was not previously opened */
  /* Set channel state */
  ch_ctx->state = GLINK_CH_STATE_OPENING;

  /* Append the channel to the transport interface's open_list */
  glink_cs_lock(&xport_ctx->channel_q_cs, TRUE);
  ch_ctx->lcid = xport_ctx->free_lcid;
  xport_ctx->free_lcid++;
  smem_list_append(&if_ptr->glink_core_priv->open_list, ch_ctx);
  glink_cs_lock(&xport_ctx->channel_q_cs, FALSE);

  /* Send the OPEN command to transport */
  status = if_ptr->tx_cmd_ch_open(if_ptr, ch_ctx->lcid, ch_ctx->name);
  if( status != GLINK_STATUS_SUCCESS) 
  {
    /* Remove the channel from the transport interface's open_list */
    glink_cs_lock(&xport_ctx->channel_q_cs, TRUE);
    xport_ctx->free_lcid--;
    smem_list_delete(&if_ptr->glink_core_priv->open_list, ch_ctx);
    glink_cs_lock(&xport_ctx->channel_q_cs, FALSE);

    /* free the ch_ctx structure and return */
    glink_free(ch_ctx);
    
    return status;
  }

  return GLINK_STATUS_SUCCESS;
} 

/*===========================================================================
                    EXTERNAL FUNCTION DEFINITIONS
===========================================================================*/
/*===========================================================================
FUNCTION      glink_init

DESCRIPTION   Initializes the GLink core library.

ARGUMENTS     None

RETURN VALUE  None

SIDE EFFECTS  None
===========================================================================*/
void glink_init(void)
{
  int i;

  glink_mem_log_cs = glink_cs_create();

  glink_core_status = GLINK_INITIALIZED;

  /* Create/Initalize crtitical sections */
  glink_transport_q_cs = glink_cs_create();
  if(glink_transport_q_cs == NULL) {
    return;
  }

  glink_cs_lock(glink_transport_q_cs, TRUE);
  for(i= 0; i < sizeof(glink_registered_transports)/sizeof(smem_list_type);
      i++)
  {
    smem_list_init(&glink_registered_transports[i]);
  }
  glink_cs_lock(glink_transport_q_cs, FALSE);

}

/*===========================================================================
FUNCTION      glink_core_register_transport

DESCRIPTION   Transport calls this API to register its interface with GLINK 
              Core

ARGUMENTS   *if_ptr   Pointer to interface instance; must be unique
                      for each edge
                         
            *cfg      Pointer to transport configuration structure.

RETURN VALUE  Standard GLINK error codes.

SIDE EFFECTS  None
===========================================================================*/
glink_err_type glink_core_register_transport
(
  glink_transport_if_type       *if_ptr,
  glink_core_transport_cfg_type *cfg
)
{
  unsigned int remote_host = 0;
  glink_core_xport_ctx_type *xport_ctx;
  /* Param validation */
  if(if_ptr == NULL || cfg == NULL)
  {
    GLINK_LOG_EVENT(GLINK_EVENT_REGISTER_XPORT, NULL, "", "", 
        GLINK_STATUS_INVALID_PARAM);    
    return GLINK_STATUS_INVALID_PARAM;
  }

  if(cfg->name == NULL       ||
     cfg->remote_ss == NULL  ||
     cfg->version == NULL    ||
     cfg->version_count == 0 ||
     cfg->max_cid == 0       ||
     cfg->max_iid == 0 )
  {
    GLINK_LOG_EVENT(GLINK_EVENT_REGISTER_XPORT, NULL, "", "", 
        GLINK_STATUS_INVALID_PARAM);    
    return GLINK_STATUS_INVALID_PARAM;
  }


  if(if_ptr->tx_cmd_version == NULL             ||
     if_ptr->tx_cmd_version_ack == NULL         ||
     if_ptr->set_version == NULL                ||
     if_ptr->tx_cmd_ch_open == NULL             ||
     if_ptr->tx_cmd_ch_close == NULL            ||
     if_ptr->tx_cmd_ch_remote_open_ack == NULL  ||
     if_ptr->tx_cmd_ch_remote_close_ack == NULL ||
     if_ptr->tx_cmd_local_rx_intent == NULL     ||
     if_ptr->tx_cmd_local_rx_done == NULL)
  {
    GLINK_LOG_EVENT(GLINK_EVENT_REGISTER_XPORT, NULL, cfg->name, cfg->remote_ss, 
        GLINK_STATUS_INVALID_PARAM);    
    return GLINK_STATUS_INVALID_PARAM;;
  }


  /* Allocate/fill out the GLink Core interface structure */
  {
    glink_core_if_type *core_if = glink_calloc(sizeof(glink_core_if_type));
    if(core_if == NULL) {
      GLINK_LOG_EVENT(GLINK_EVENT_REGISTER_XPORT, NULL, cfg->name, cfg->remote_ss, 
          GLINK_STATUS_OUT_OF_RESOURCES);      
      return GLINK_STATUS_OUT_OF_RESOURCES;
    }
    core_if->link_up = glink_link_up;
    core_if->rx_cmd_version = glink_rx_cmd_version;
    core_if->rx_cmd_version_ack = glink_rx_cmd_version_ack;
    core_if->rx_cmd_ch_remote_open = glink_rx_cmd_ch_remote_open;
    core_if->rx_cmd_ch_open_ack = glink_rx_cmd_ch_open_ack;
    core_if->rx_cmd_ch_close_ack = glink_rx_cmd_ch_close_ack;
    core_if->rx_cmd_ch_remote_close = glink_rx_cmd_ch_remote_close;
    core_if->ch_state_local_trans = glink_ch_state_local_trans;
    core_if->rx_cmd_remote_rx_intent_put = glink_rx_cmd_remote_rx_intent_put;
    core_if->rx_cmd_rx_data = glink_rx_cmd_rx_data;
    core_if->rx_cmd_rx_data_fragment = glink_rx_cmd_rx_data_fragment;
    core_if->rx_cmd_tx_done = glink_rx_cmd_tx_done;
    core_if->tx_resume = glink_tx_resume;

    /* Set the glink_core_if_ptr to point to the allocated structure */
    if_ptr->glink_core_if_ptr = core_if;
  }

  /* Allocate/fill out the GLink private context data */
  {
     xport_ctx = glink_calloc(sizeof(glink_core_xport_ctx_type));
    if(xport_ctx == NULL) {
      /* Free previously allocated memory */
      glink_free(if_ptr->glink_core_if_ptr);

      GLINK_LOG_EVENT(GLINK_EVENT_REGISTER_XPORT, NULL, cfg->name, cfg->remote_ss, 
          GLINK_STATUS_OUT_OF_RESOURCES);
    
      return GLINK_STATUS_OUT_OF_RESOURCES;
    }

    glink_string_copy(xport_ctx->xport, cfg->name, sizeof(xport_ctx->xport));
    glink_string_copy(xport_ctx->remote_ss, cfg->remote_ss, 
        sizeof(xport_ctx->xport));
    xport_ctx->free_lcid = 0;
    xport_ctx->version_array = cfg->version;
    xport_ctx->version_indx = cfg->version_count - 1;

    glink_cs_init(&xport_ctx->channel_q_cs);
    glink_cs_init(&xport_ctx->liid_cs);

    glink_cs_lock(&xport_ctx->channel_q_cs, TRUE);
    smem_list_init(&xport_ctx->open_list);
    glink_cs_lock(&xport_ctx->channel_q_cs, FALSE);

    /* Set the glink_core_if_ptr to point to the allocated structure */
    if_ptr->glink_core_priv = xport_ctx;
    xport_ctx->status = GLINK_XPORT_REGISTERED;  
  }

  /* Push the transport interface into appropriate queue */
  for(remote_host = 0; 
      remote_host < sizeof(glink_hosts_supported)/sizeof(char *);
      remote_host++) {
    if( 0 == strncmp(glink_hosts_supported[remote_host], cfg->remote_ss, 
          strlen(glink_hosts_supported[remote_host])) ) {
      /* Match found, break out of loop */
      break;
    }
  }

  if(remote_host == GLINK_NUM_HOSTS ) {
    /* Unknown transport name trying to register with GLink */
    GLINK_LOG_EVENT(GLINK_EVENT_REGISTER_XPORT, NULL, xport_ctx->xport, 
           xport_ctx->remote_ss, GLINK_STATUS_INVALID_PARAM);
    
    return GLINK_STATUS_INVALID_PARAM;
  }
  glink_cs_lock(glink_transport_q_cs, TRUE);
  smem_list_append(&glink_registered_transports[remote_host], if_ptr);
  glink_cs_lock(glink_transport_q_cs, FALSE);

  GLINK_LOG_EVENT(GLINK_EVENT_REGISTER_XPORT, NULL, xport_ctx->xport, 
      xport_ctx->remote_ss, GLINK_STATUS_SUCCESS);

  return GLINK_STATUS_SUCCESS;
}
  
/** 
 * Opens a logical GLink based on the specified config params
 *
 * @param[in]    cfg_ptr  Pointer to the configuration structure for the
 *                        GLink. See glink.h
 * @param[out]   handle   GLink handle associated with the logical channel
 *
 * @return       Standard GLink error codes
 *
 * @sideeffects  Allocates channel resources and informs remote host about
 *               channel open.
 */
glink_err_type glink_open
(
  glink_open_config_type *cfg_ptr,
  glink_handle_type      *handle
)
{
  glink_err_type          status;
  glink_transport_if_type *if_ptr;
  glink_channel_ctx_type  *ch_ctx;
  unsigned int            remote_host;
  glink_core_xport_ctx_type *xport_ctx;

  /* Param validation */
  if(cfg_ptr == NULL)
  {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_OPEN, NULL, "", "", 
        GLINK_STATUS_INVALID_PARAM);     
    return GLINK_STATUS_INVALID_PARAM;
  }

  if(cfg_ptr->remote_ss == NULL      ||
     cfg_ptr->name == NULL           ||
     cfg_ptr->notify_rx == NULL      ||
     cfg_ptr->notify_tx_done == NULL ||
     cfg_ptr->notify_state == NULL )
  {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_OPEN, NULL, "", "", 
        GLINK_STATUS_INVALID_PARAM);    
    return GLINK_STATUS_INVALID_PARAM;
  }

  /* Evaluate the equivalent edge name->enum for future use */
  for(remote_host = 0; 
      remote_host < sizeof(glink_hosts_supported)/sizeof(char *);
      remote_host++) {
    if( 0 == strncmp(glink_hosts_supported[remote_host], cfg_ptr->remote_ss, 
          strlen(glink_hosts_supported[remote_host])) ) {
      /* Match found, break out of loop */
      break;
    }
  }

  if(remote_host == GLINK_NUM_HOSTS ) {
    /* Unknown transport name trying to register with GLink */
    GLINK_LOG_EVENT(GLINK_EVENT_REGISTER_XPORT, cfg_ptr->name, "", cfg_ptr->remote_ss,
        GLINK_STATUS_INVALID_PARAM);    

    return GLINK_STATUS_INVALID_PARAM;
  }

  /* Allocate and initialize channel info structure */  
  ch_ctx = glink_calloc(sizeof(glink_channel_ctx_type));
  if(ch_ctx == NULL) {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_OPEN, cfg_ptr->name, "",
        "", GLINK_STATUS_OUT_OF_RESOURCES);
    return GLINK_STATUS_OUT_OF_RESOURCES;    
  }
 
  /* Fill in the channel info structure */
  glink_string_copy(ch_ctx->name, cfg_ptr->name, sizeof(ch_ctx->name));
  ch_ctx->priv = cfg_ptr->priv;
  ch_ctx->notify_rx = cfg_ptr->notify_rx;
  ch_ctx->notify_tx_done = cfg_ptr->notify_tx_done;
  ch_ctx->notify_state = cfg_ptr->notify_state;
  glink_cs_init(&ch_ctx->intent_q_cs);

  /* Check to see if requested transport is available */
  if(cfg_ptr->transport == NULL) {
    /* No transport requested, see if default available */
    if_ptr = smem_list_first(&glink_registered_transports[remote_host]);
    if(if_ptr == NULL) {
      glink_free(ch_ctx);

      GLINK_LOG_EVENT(GLINK_EVENT_CH_OPEN, cfg_ptr->name, "",
          "", GLINK_STATUS_NO_TRANSPORT);

      return GLINK_STATUS_NO_TRANSPORT;
    } else {
      ch_ctx->if_ptr = if_ptr;
      xport_ctx = if_ptr->glink_core_priv;

      status = glinki_add_ch_to_xport(if_ptr, ch_ctx);
      if(status == GLINK_STATUS_SUCCESS) {
       /* Set the handle and return */
       *handle = ch_ctx;
      } else {
        *handle = NULL;
      }
      GLINK_LOG_EVENT(GLINK_EVENT_CH_OPEN, ch_ctx->name, "",
          xport_ctx->remote_ss, status);      
      return status;
    }
  } /* end if xport == NULL */

  /* Check to see if the transport has registered */
  if_ptr = smem_list_first(&glink_registered_transports[remote_host]);
  while(if_ptr != NULL) {
    if(0 == strncmp(cfg_ptr->transport, if_ptr->glink_core_priv->xport, 
        sizeof(if_ptr->glink_core_priv->xport))) {
      /* Xport match found */
      status = glinki_add_ch_to_xport(if_ptr, ch_ctx);
      if(status == GLINK_STATUS_SUCCESS) {
        /* Set the handle and return */
        *handle = ch_ctx;
      } else {
        *handle = NULL;
      }
      GLINK_LOG_EVENT(GLINK_EVENT_CH_OPEN, ch_ctx->name, "",
          if_ptr->glink_core_priv->remote_ss, status);
      return status;
    }
    if_ptr = smem_list_next(if_ptr);
  } /* end while() */    

  /* Code gets here if we are not able to find reqeusted transport */
  GLINK_LOG_EVENT(GLINK_EVENT_CH_OPEN, ch_ctx->name, xport_ctx->xport,
      xport_ctx->remote_ss, GLINK_STATUS_NO_TRANSPORT);
  glink_free(ch_ctx);
  return GLINK_STATUS_NO_TRANSPORT;
}
  
/** 
 * Closes the GLink logical channel specified by the handle.
 *
 * @param[in]    handle   GLink handle associated with the logical channel
 *
 * @return       Standard GLink error codes
 *
 * @sideeffects  Closes local end of the channel and informs remote host
 */
glink_err_type glink_close
(
  glink_handle_type handle
)
{
  glink_err_type status;
  glink_core_xport_ctx_type *xport_ctx = handle->if_ptr->glink_core_priv;
  if(handle == NULL) {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_CLOSE, NULL, "",
        "", GLINK_STATUS_INVALID_PARAM);
    return GLINK_STATUS_INVALID_PARAM;
  }

  /* Check to see if channel is in open/opening state */
  if( handle->state != GLINK_CH_STATE_OPEN && 
      handle->state != GLINK_CH_STATE_OPENING )
  {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_CLOSE, handle->name, xport_ctx->xport,
        xport_ctx->remote_ss, GLINK_STATUS_FAILURE);
    return GLINK_STATUS_FAILURE;
  }

  /* Transition to closing */
  handle->state = GLINK_CH_STATE_CLOSING;

  /* Send CLOSE cmd to the transport interface */
  status = handle->if_ptr->tx_cmd_ch_close(handle->if_ptr, handle->lcid);
  if(status != GLINK_STATUS_SUCCESS) 
  {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_CLOSE, handle->name, xport_ctx->xport,
        xport_ctx->remote_ss, status);
    return status;
  }

  return GLINK_STATUS_SUCCESS;
}

/** 
 * Transmit the provided buffer over GLink.
 *
 * @param[in]    handle   GLink handle associated with the logical channel
 *
 * @param[in]   *pkt_priv Per packet private data
 *
 * @param[in]   *data     Pointer to the data buffer to be transmitted
 *
 * @param[in]   size      Size of buffer
 *
 * @return       Standard GLink error codes
 *
 * @sideeffects  Causes remote host to wake-up and process rx pkt
 */
glink_err_type glink_tx
(
  glink_handle_type handle,
  const void        *pkt_priv,
  const void        *data,
  size_t            size
)
{
  glink_err_type         status;
  glink_core_tx_pkt_type pctx;
  glink_rx_intent_type   *rm_intent;
  glink_core_xport_ctx_type *xport_ctx = handle->if_ptr->glink_core_priv;

  /* Input validation */
  if(handle == NULL || data == NULL || size == 0) {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_TX, NULL, "",
        "", GLINK_STATUS_INVALID_PARAM);
    return GLINK_STATUS_INVALID_PARAM;
  }

  /* Make sure channel is in OPEN state */
  if(handle->state != GLINK_CH_STATE_OPEN )
  {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_CLOSE, handle->name, xport_ctx->xport,
      xport_ctx->remote_ss, GLINK_STATUS_FAILURE);
    return GLINK_STATUS_FAILURE;
  }

  /* Check to see if remote side has queued intent to receive data */
  rm_intent = smem_list_first(&handle->remote_intent_q);
  if(rm_intent == NULL || rm_intent->size < size) {
    /* No buffer to receive data at the remote end */
    GLINK_LOG_EVENT(GLINK_EVENT_CH_TX, handle->name, xport_ctx->xport,
        xport_ctx->remote_ss, GLINK_STATUS_OUT_OF_RESOURCES);
    return GLINK_STATUS_OUT_OF_RESOURCES;
  }

  pctx.data = (void*)data;
  pctx.pkt_priv = pkt_priv;
  pctx.size = size;
  pctx.size_remaining = size;
  pctx.iid = rm_intent->iid;

  /* Store the intent in pending_tx_q */
  rm_intent->data = (void*)data;
  rm_intent->pkt_priv = pkt_priv;
  rm_intent->pkt_sz = size;
  rm_intent->used = size;

  glink_cs_lock(&handle->intent_q_cs, TRUE);
  smem_list_delete(&handle->remote_intent_q, rm_intent);
  smem_list_append(&handle->remote_intent_pending_tx_q, rm_intent);
  glink_cs_lock(&handle->intent_q_cs, FALSE);

  /* Call transport API to transmit data */
  status = handle->if_ptr->tx(handle->if_ptr, handle->lcid, &pctx);

  GLINK_LOG_EVENT(GLINK_EVENT_CH_TX, handle->name, xport_ctx->xport,
      xport_ctx->remote_ss, status);
  return status;
}

/** 
 * Queue one or more Rx intent for the logical GPIC Link channel.
 *
 * @param[in]    handle   GLink handle associated with the logical channel
 *
 * @param[in]   *pkt_priv Per packet private data
 *
 * @param[in]   size      Size of buffer
 *
 * @return       Standard GLink error codes
 *
 * @sideeffects  GLink XAL allocates rx buffers for receiving packets
 */
glink_err_type glink_queue_rx_intent
(
  glink_handle_type handle,
  const void        *pkt_priv,
  size_t            size
)
{
  glink_err_type         status;
  glink_rx_intent_type   *lc_intent;
  glink_core_xport_ctx_type *xport_ctx = handle->if_ptr->glink_core_priv;

  /* Input validation */
  if(handle == NULL || size == 0) {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_Q_RX_INTENT, NULL, "",
        "", GLINK_STATUS_INVALID_PARAM);
    return GLINK_STATUS_INVALID_PARAM;
  }

  /* Make sure channel is in OPEN state */
  if(handle->state != GLINK_CH_STATE_OPEN)
  {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_Q_RX_INTENT, handle->name, xport_ctx->xport,
      xport_ctx->remote_ss, GLINK_STATUS_FAILURE);
    return GLINK_STATUS_FAILURE;
  }

  /* Allocate an intent structure */
  lc_intent = glink_calloc(sizeof(glink_rx_intent_type));
  if(lc_intent == NULL) {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_Q_RX_INTENT, handle->name, xport_ctx->xport,
        xport_ctx->remote_ss, GLINK_STATUS_OUT_OF_RESOURCES);
    return GLINK_STATUS_OUT_OF_RESOURCES;
  }
  glink_cs_lock(&handle->if_ptr->glink_core_priv->liid_cs, TRUE);
  /* Call transport API to queue rx intent. Transport allocates intent buffer */
  status = handle->if_ptr->tx_cmd_local_rx_intent(handle->if_ptr, 
              handle->lcid, size, handle->if_ptr->glink_core_priv->liid);
  if(status == GLINK_STATUS_SUCCESS) {

    /* push the intent on local queue */
    lc_intent->iid = handle->if_ptr->glink_core_priv->liid;
    lc_intent->size = size;
    lc_intent->pkt_priv = pkt_priv;
    glink_cs_lock(&handle->intent_q_cs, TRUE);
    smem_list_append(&handle->local_intent_q, lc_intent);
    glink_cs_lock(&handle->intent_q_cs, FALSE);

    /* Increment the local intent ID counter associated with this channel */
    handle->if_ptr->glink_core_priv->liid++;
  } else {
    /* Failure */
    glink_free(lc_intent);
  }
  glink_cs_lock(&handle->if_ptr->glink_core_priv->liid_cs, FALSE);

  GLINK_LOG_EVENT(GLINK_EVENT_CH_Q_RX_INTENT, handle->name, xport_ctx->xport,
      xport_ctx->remote_ss, status);
  return status;  
}

/** 
 * Client uses this to signal to GLink layer that it is done with the received 
 * data buffer. This API should be called to free up the receive buffer, which,
 * in zero-copy mode is actually remote-side's transmit buffer.
 *
 * @param[in]    handle   GLink handle associated with the logical channel
 *
 * @param[in]   *ptr      Pointer to the received buffer
 *
 * @return       Standard GLink error codes
 *
 * @sideeffects  GLink XAL frees the Rx buffer
 */
glink_err_type glink_rx_done
(
  glink_handle_type handle,
  const void        *ptr
)
{
  glink_rx_intent_type      *lc_intent;
  glink_core_xport_ctx_type *xport_ctx = handle->if_ptr->glink_core_priv;

  /* Input validation */
  if(handle == NULL || ptr == NULL) {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_RX_DONE, NULL, "",
        "", GLINK_STATUS_INVALID_PARAM);
    return GLINK_STATUS_INVALID_PARAM;
  }

  /* Make sure channel is in OPEN state */
  if(handle->state != GLINK_CH_STATE_OPEN)
  {
    GLINK_LOG_EVENT(GLINK_EVENT_CH_RX_DONE, handle->name, xport_ctx->xport,
      xport_ctx->remote_ss, GLINK_STATUS_FAILURE);
    return GLINK_STATUS_FAILURE;
  }    

  /* Free the intent */
  lc_intent = smem_list_first(&handle->local_intent_client_q);
  while(lc_intent != NULL) {
    if(lc_intent->data == ptr) {
      /* Found intent, delete it */
      glink_cs_lock(&handle->intent_q_cs, TRUE);
      smem_list_delete(&handle->local_intent_client_q, lc_intent);
      glink_cs_lock(&handle->intent_q_cs, FALSE);

      /* Note that the actual buffer, lc_intent->data, was allocated by the 
      * transport and should be freed by the xport. We should not touch it */
      /* Let the xport know we are done with the buffer */
      handle->if_ptr->tx_cmd_local_rx_done(handle->if_ptr, handle->lcid, 
                            lc_intent->iid);
      /* Free the intent */
      glink_free(lc_intent);

      GLINK_LOG_EVENT(GLINK_EVENT_CH_RX_DONE, handle->name, xport_ctx->xport,
          xport_ctx->remote_ss, GLINK_STATUS_SUCCESS);
      return GLINK_STATUS_SUCCESS;     
    }
    lc_intent = smem_list_next(lc_intent);
  }

  GLINK_LOG_EVENT(GLINK_EVENT_CH_RX_DONE, handle->name, xport_ctx->xport,
      xport_ctx->remote_ss, GLINK_STATUS_INVALID_PARAM);
  return GLINK_STATUS_INVALID_PARAM; 
}  


void glink_mem_log
(
  const char *func, 
  uint32 line,
  glink_log_event_type type, 
  const char *msg, 
  const char *xport, 
  const char *remote_ss, 
  uint32 param
)
{
  glink_cs_lock(glink_mem_log_cs, TRUE);

  glink_mem_log_arr[glink_mem_log_idx].func = func; 
  glink_mem_log_arr[glink_mem_log_idx].line = line;
  glink_mem_log_arr[glink_mem_log_idx].type = type;
  glink_mem_log_arr[glink_mem_log_idx].msg = msg;
  glink_mem_log_arr[glink_mem_log_idx].xport = xport;
  glink_mem_log_arr[glink_mem_log_idx].remote_ss = remote_ss;
  glink_mem_log_arr[glink_mem_log_idx].param = param;

  if (++glink_mem_log_idx >= GLINK_MEM_LOG_SIZE)
  {
    glink_mem_log_idx = 0;
  }

  glink_cs_lock(glink_mem_log_cs, FALSE);
}
