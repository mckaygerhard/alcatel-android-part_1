/*==============================================================================
  FILE:         lpr_init.c

  OVERVIEW:     This file provides the uSleep LPR init functions which are
                called while in normal operational mode

  DEPENDENCIES: None
  
                Copyright (c) 2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: //components/rel/core.adsp/2.6.6/power/sleep2.0/src/uSleep/ddr/lpr/lpr_init.c#2 $
$DateTime: 2016/07/28 05:07:45 $
==============================================================================*/
#include "DALStdDef.h"
#include "CoreVerify.h"
#include "uSleep_lpr.h"
#include "spm.h"
#include "rpm.h"
#include "rpmclient.h"
#include "kvp.h"
#include "uSleep_util.h"
#include "q6_lpm_config.h"
#include "sleep_target.h"

extern boolean g_uSleepApcrEnable;

/*==============================================================================
                       INTERNAL FUNCTION DEFINITIONS
 =============================================================================*/
/*
 * uSleepLPR_apcrInit
 */
void uSleepLPR_apcrInit(boolean islandEntry)
{
  /* All modes in uSleep do APCR + PLL off. Setup to perform APCR standalone
   * mode initially on entry and clock gating on exit. */
  if(g_uSleepApcrEnable)
  {
    if(TRUE == islandEntry)
    {
      q6LPMConfig_setupModeConfig(SPM_MODE_APCR_PLL_LPM, TRUE);
      q6LPMConfig_setupL2RetConfig();
    }
    else
    {
      q6LPMConfig_setupModeConfig(SPM_MODE_CLK_GATE, FALSE);
    }
  }
  else
  {
    q6LPMConfig_setupModeConfig(SPM_MODE_CLK_GATE, FALSE);
  }

  return;
}

/* 
 * uSleepLPR_cxoInit
 */
void uSleepLPR_cxoInit(boolean islandEntry)
{ 
  static uint32             vddReq;
  static uint32             vddResID;
  DalChipInfoFamilyType     cpuFam;
  DalChipInfoVersionType    cpuVer;
  static kvp_t              *vddKVP     = NULL;
  static rpm_resource_type  vddResType  = RPM_SMPS_A_REQ;

  sleepTarget_getCPUArchitecture(&cpuFam, &cpuVer);

  /* Normal operational mode has already added the XO vote to the sleep set.
   * This vote is never removed, and all we need to do is add the vdd min KVP values. */
  if(NULL == vddKVP)
  {
    vddKVP = kvp_create(1*4*3);
    
    /* For Bear family, moving to Level based voting */
    if((cpuFam == DALCHIPINFO_FAMILY_MSM8952) ||
		(cpuFam == DALCHIPINFO_FAMILY_MSM8956)||
        (cpuFam == DALCHIPINFO_FAMILY_MSM8937)||
        (cpuFam == DALCHIPINFO_FAMILY_MSM8917)||
        (cpuFam == DALCHIPINFO_FAMILY_MSM8940)||
		(cpuFam == DALCHIPINFO_FAMILY_MSM8920)||
        (cpuFam == DALCHIPINFO_FAMILY_MSM8953))
    {
      vddResID = 2;
      vddReq   = 16;    
      kvp_put(vddKVP, /* PM_NPA_KEY_CORNER_VOLT_LEVEL */ 0x6C766C76,
          sizeof(vddReq), (void *)&vddReq);
    }
    else
    {    
      vddResID = 1;
      vddReq   = 1;
      kvp_put(vddKVP, /* PM_NPA_KEY_CORNER_LEVEL_KEY */ 0x6E726F63,
          sizeof(vddReq), (void *)&vddReq);
    }
  }

  if(TRUE == islandEntry)
  {
    /* Add vdd min vote to sleep set */
    kvp_reset(vddKVP);
    rpm_post_request(RPM_SLEEP_SET, vddResType, vddResID, vddKVP);
  }
  else
  {
    /* Cancelling vdd min request */
    rpm_post_request(RPM_SLEEP_SET, vddResType, vddResID, NULL);
  }

  return;
}

