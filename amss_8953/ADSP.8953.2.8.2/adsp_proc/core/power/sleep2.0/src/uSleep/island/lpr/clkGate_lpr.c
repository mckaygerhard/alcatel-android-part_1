/*==============================================================================
  FILE:         clkGate_lpr.c

  OVERVIEW:     This file provides uSleep clock gating LPR functions

  DEPENDENCIES: Object file generated from source is marked as island section
  
                Copyright (c) 2015 Qualcomm Technologies, Inc. (QTI).
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary
================================================================================
$Header: 
$DateTime: 
==============================================================================*/

#include "qurt.h"
#include "DALStdDef.h"

/*==============================================================================
                       EXTERNAL FUNCTION DEFINITIONS
 =============================================================================*/

void uSleepLPR_clkGateEnter( uint64 wakeupTime )
{
  qurt_power_wait_for_active();
  return;
}

void uSleepLPR_clkGateExit( void )
{
  return;
}