/*
* Copyright (c) 2015 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies, Inc. Confidential and Proprietary.
*/

/*
 * hal_sleep.c
 *
 *  Created on: Feb 13, 2012
 *      Author: yrusakov
 */

#include "npa.h"
#include "adsppm_utils.h"
#include "adsppm.h"
#include "asic.h"
#include "core.h"
//#include "stdio.h"
#include "stdlib.h"
#include "hal_sleep.h"
#include "sleep_lpr.h"

#define USLEEP_NODE_NAME "/core/uSleep"
#define USLEEP_ALLOW 1
#define USLEEP_DISALLOW 0
const char SLEEP_LATENCY_NODE_NAME[] = "/core/cpu/latency/usec";
const char ADSPPM_SLEEP_CLIENT_NAME[] = "/core/power/adsppm";

typedef struct
{
    npa_client_handle handle;
    char clientName[MAX_ADSPPM_CLIENT_NAME];
} clientHandleType;

typedef struct
{
    boolean          slpInitSuccess;
    DALSYSSyncHandle slpdrvCtxLock;
    clientHandleType slpRequestHandle;
    clientHandleType uSlpRequestHandle;
    boolean          uSlpNodeAvailable;
} gSlpInfoType;

static gSlpInfoType gSlpInfo;

static void SlpHandleInitCb(void *context, unsigned int event_type, void *data, unsigned int data_size);
static void USlpHandleInitCb(void *context, unsigned int event_type, void *data, unsigned int data_size);


// Enter and exit function currently unused
AdsppmStatusType Slp_Init(void (*sleep_enter_func)(uint32), void (*sleep_exit_func)(void))
{
    AdsppmStatusType sts = Adsppm_Status_Success;
    gSlpInfo.slpInitSuccess = FALSE;
    gSlpInfo.uSlpNodeAvailable = FALSE;
    ADSPPM_LOG_FUNC_PROFILESTART;
    if(DAL_SUCCESS != DALSYS_SyncCreate(
        DALSYS_SYNC_ATTR_RESOURCE,
        (DALSYSSyncHandle *)&gSlpInfo.slpdrvCtxLock,
        NULL))
    {
        sts = Adsppm_Status_Failed;
    }
    else
    {
        strlcpy(gSlpInfo.slpRequestHandle.clientName, "adsppmsleep", MAX_ADSPPM_CLIENT_NAME);
        strlcpy(gSlpInfo.uSlpRequestHandle.clientName, "adsppm_uSleep", MAX_ADSPPM_CLIENT_NAME);
        npa_resource_available_cb(SLEEP_LATENCY_NODE_NAME, SlpHandleInitCb, NULL);
        npa_resource_available_cb(USLEEP_NODE_NAME, USlpHandleInitCb, NULL);

    }
    ADSPPM_LOG_FUNC_PROFILEEND;
    return sts;
}


static void SlpHandleInitCb(void *context, unsigned int event_type, void *data, unsigned int data_size)
{
    ADSPPM_LOG_FUNC_PROFILESTART;
    gSlpInfo.slpRequestHandle.handle = npa_create_sync_client(
        SLEEP_LATENCY_NODE_NAME,
        gSlpInfo.slpRequestHandle.clientName,
        NPA_CLIENT_REQUIRED);
    if(NULL != gSlpInfo.slpRequestHandle.handle)
    {
        gSlpInfo.slpInitSuccess = TRUE;
    }
    ADSPPM_LOG_FUNC_PROFILEEND;
}


AdsppmStatusType Slp_RequestLatency(uint32 latency)
{
    AdsppmStatusType sts = Adsppm_Status_Success;
    ADSPPM_LOG_FUNC_PROFILESTART;
    if (FALSE == gSlpInfo.slpInitSuccess)
    {
        sts = Adsppm_Status_NotInitialized;
    }
    else
    {
        adsppm_lock(gSlpInfo.slpdrvCtxLock);
        if(HAL_SLEEP_LATENCY_DONT_CARE == latency)
        {
            ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO,
                "Complete NPA request for %s",
                SLEEP_LATENCY_NODE_NAME);
            npa_complete_request(gSlpInfo.slpRequestHandle.handle);
        }
        else
        {
            ADSPPM_LOG_PRINTF_2(ADSPPM_LOG_LEVEL_INFO,
                "NPA request for %s, latency: %d",
                SLEEP_LATENCY_NODE_NAME, latency);
            npa_issue_required_request(gSlpInfo.slpRequestHandle.handle, latency);
        }
        adsppm_unlock(gSlpInfo.slpdrvCtxLock);
    }
    ADSPPM_LOG_FUNC_PROFILEEND;
    return sts;
}


AdsppmStatusType Slp_Release()
{
    AdsppmStatusType sts = Adsppm_Status_Success;
    ADSPPM_LOG_FUNC_PROFILESTART;
    if(FALSE == gSlpInfo.slpInitSuccess)
    {
        sts = Adsppm_Status_NotInitialized;
    }
    else
    {
        adsppm_lock(gSlpInfo.slpdrvCtxLock);
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_INFO,
            "Complete NPA request for %s",
            SLEEP_LATENCY_NODE_NAME);
        npa_complete_request(gSlpInfo.slpRequestHandle.handle);
        adsppm_unlock(gSlpInfo.slpdrvCtxLock);
    }
    ADSPPM_LOG_FUNC_PROFILEEND;
    return sts;
}


static void USlpHandleInitCb(void *context, unsigned int event_type, void *data, unsigned int data_size)
{
    ADSPPM_LOG_FUNC_PROFILESTART;
    gSlpInfo.uSlpRequestHandle.handle = npa_create_sync_client(
        USLEEP_NODE_NAME,
        gSlpInfo.uSlpRequestHandle.clientName,
        NPA_CLIENT_REQUIRED);
    if(NULL == gSlpInfo.uSlpRequestHandle.handle)
    {
        ADSPPM_LOG_PRINTF_1(ADSPPM_LOG_LEVEL_ERROR,
            "Failed to create NPA client for %s", USLEEP_NODE_NAME);
    }
    else
    {
        gSlpInfo.uSlpNodeAvailable = TRUE;
    }
    ADSPPM_LOG_FUNC_PROFILEEND;
}

boolean Slp_IsUSleepNpaAvailable(void)
{
    boolean sts = FALSE;
    ADSPPM_LOG_FUNC_PROFILESTART;
    if(gSlpInfo.slpInitSuccess == TRUE)
    {
        adsppm_lock(gSlpInfo.slpdrvCtxLock);
        if(gSlpInfo.uSlpNodeAvailable)
        {
            sts = TRUE;
        }
        adsppm_unlock(gSlpInfo.slpdrvCtxLock);
    }
    ADSPPM_LOG_FUNC_PROFILEEND;
    return sts;
}

AdsppmStatusType Slp_AllowUSleep(void)
{
    AdsppmStatusType sts = Adsppm_Status_Success;
    ADSPPM_LOG_FUNC_PROFILESTART;
    if(gSlpInfo.slpInitSuccess == FALSE)
    {
        sts = Adsppm_Status_NotInitialized;
    }
    else
    {
        adsppm_lock(gSlpInfo.slpdrvCtxLock);
        if(gSlpInfo.uSlpNodeAvailable)
        {
            ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_INFO,
                "NPA request to allow uSleep");
            npa_issue_required_request(
                gSlpInfo.uSlpRequestHandle.handle, USLEEP_ALLOW);
        }
        adsppm_unlock(gSlpInfo.slpdrvCtxLock);
    }
    ADSPPM_LOG_FUNC_PROFILEEND;
    return sts;
}

AdsppmStatusType Slp_DisallowUSleep(void)
{
    AdsppmStatusType sts = Adsppm_Status_Success;
    ADSPPM_LOG_FUNC_PROFILESTART;
    if(gSlpInfo.slpInitSuccess == FALSE)
    {
        sts = Adsppm_Status_NotInitialized;
    }
    else
    {
        adsppm_lock(gSlpInfo.slpdrvCtxLock);
        if(gSlpInfo.uSlpNodeAvailable)
        {
            ADSPPM_LOG_PRINTF_0(ADSPPM_LOG_LEVEL_INFO,
                "NPA request to disallow uSleep");
            npa_issue_required_request(
                gSlpInfo.uSlpRequestHandle.handle, USLEEP_DISALLOW);
        }
        adsppm_unlock(gSlpInfo.slpdrvCtxLock);
    }
    ADSPPM_LOG_FUNC_PROFILEEND;
    return sts;
}
