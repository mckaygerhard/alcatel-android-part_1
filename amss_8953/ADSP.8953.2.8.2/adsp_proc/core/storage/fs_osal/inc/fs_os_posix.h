/***********************************************************************
 * fs_os_posix.h
 *
 * FS Posix abstraction
 * Copyright (C) 2015 QUALCOMM Technologies, Inc.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.adsp/2.6.6/storage/fs_osal/inc/fs_os_posix.h#1 $ $DateTime: 2016/05/04 01:42:14 $ $Author: pwbldsvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2015-02-06   dks   Create

===========================================================================*/

#ifndef __FS_OS_POSIX_H__
#define __FS_OS_POSIX_H__

#ifdef FEATURE_FS_OS_FOR_POSIX

#include "pthread.h"
#include "posix_errno.h"
#include "task.h"

#define FS_OS_THREAD_DEFAULT_STACK_SIZE 4096

typedef pthread_t fs_os_thread_t;
typedef pthread_mutex_t fs_os_mutex_t;
typedef void* fs_os_thread_return_type;

#define fs_os_thread_exit( )               \
          do {                             \
            pthread_exit((void *)NULL);    \
            return NULL;                   \
          } while (0)

#endif /* not FEATURE_FS_OS_FOR_POSIX */

#endif /* not __FS_OS_POSIX_H__ */
