/***********************************************************************
 * fs_os_errno.h
 *
 * FS OS Absraction Error list.
 * Copyright (C) 2015 QUALCOMM Technologies, Inc.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.adsp/2.6.6/storage/fs_osal/inc/fs_os_errno.h#1 $ $DateTime: 2016/05/04 01:42:14 $ $Author: pwbldsvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2015-03-02   dks   Create

===========================================================================*/

#ifndef __FS_OS_ERRNO_H__
#define __FS_OS_ERRNO_H__

#define FS_OS_EBIG  1
#define FS_OS_EACCES  2
#define FS_OS_EADDRINUSE  3
#define FS_OS_EADDRNOTAVAIL  4
#define FS_OS_EAFNOSUPPORT  5
#define FS_OS_EAGAIN  6
#define FS_OS_EALREADY  7
#define FS_OS_EBADF  8
#define FS_OS_EBADMSG  9
#define FS_OS_EBUSY  10
#define FS_OS_ECANCELED  11
#define FS_OS_ECHILD  12
#define FS_OS_ECONNABORTED  13
#define FS_OS_ECONNREFUSED  14
#define FS_OS_ECONNRESET  15
#define FS_OS_EDEADLK  16
#define FS_OS_EDESTADDRREQ  17
#define FS_OS_EDOM  18
#define FS_OS_EDQUOT  19
#define FS_OS_EEXIST  20
#define FS_OS_EFAULT  21
#define FS_OS_EFBIG  22
#define FS_OS_EHOSTUNREACH  23
#define FS_OS_EIDRM  24
#define FS_OS_EILSEQ  25
#define FS_OS_EINPROGRESS  26
#define FS_OS_EINTR  27
#define FS_OS_EINVAL  28
#define FS_OS_EIO  29
#define FS_OS_EISCONN  30
#define FS_OS_EISDIR  31
#define FS_OS_ELOOP  32
#define FS_OS_EMFILE  33
#define FS_OS_EMLINK  34
#define FS_OS_EMSGSIZE  35
#define FS_OS_EMULTIHOP  36
#define FS_OS_ENAMETOOLONG  37
#define FS_OS_ENETDOWN  38
#define FS_OS_ENETRESET  39
#define FS_OS_ENETUNREACH  40
#define FS_OS_ENFILE  41
#define FS_OS_ENOBUFS  42
#define FS_OS_ENODATA  43
#define FS_OS_ENODEV  44
#define FS_OS_ENOENT  45
#define FS_OS_ENOEXEC  46
#define FS_OS_ENOLCK  47
#define FS_OS_ENOLINK  48
#define FS_OS_ENOMEM  49
#define FS_OS_ENOMSG  50
#define FS_OS_ENOPROTOOPT  51
#define FS_OS_ENOSPC  52
#define FS_OS_ENOSR  53
#define FS_OS_ENOSTR  54
#define FS_OS_ENOSYS  55
#define FS_OS_ENOTCONN  56
#define FS_OS_ENOTDIR  57
#define FS_OS_ENOTEMPTY  58
#define FS_OS_ENOTRECOVERABLE  59
#define FS_OS_ENOTSOCK  60
#define FS_OS_ENOTSUP  61
#define FS_OS_ENOTTY  62
#define FS_OS_ENXIO  63
#define FS_OS_EOPNOTSUPP  64
#define FS_OS_EOVERFLOW  65
#define FS_OS_EOWNERDEAD  66
#define FS_OS_EPERM  67
#define FS_OS_EPIPE  68
#define FS_OS_EPROTO  69
#define FS_OS_EPROTONOSUPPORT  70
#define FS_OS_EPROTOTYPE  71
#define FS_OS_ERANGE  72
#define FS_OS_EROFS  73
#define FS_OS_ESPIPE  74
#define FS_OS_ESRCH  75
#define FS_OS_ESTALE  76
#define FS_OS_ETIME  77
#define FS_OS_ETIMEDOUT  78
#define FS_OS_ETXTBSY  79
#define FS_OS_EWOULDBLOCK  80
#define FS_OS_EXDEV  81

uint32 fs_os_get_errno (uint32 local_os_errno);

#endif /* __FS_OS_ERRNO_H__ */

