/***********************************************************************
 * fs_os_qurt.c
 *
 * FS Qurt Abstraction
 * Copyright (C) 2015 QUALCOMM Technologies, Inc.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.adsp/2.6.6/storage/fs_osal/src/fs_os_qurt.c#1 $ $DateTime: 2016/05/04 01:42:14 $ $Author: pwbldsvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2015-02-06   dks   Create

===========================================================================*/

#include "fs_os.h"

#ifdef FEATURE_FS_OS_FOR_QURT

#include "fs_os_qurt.h"
#include "fs_os_err.h"
#include "fs_os_errno.h"
#include "fs_os_string.h"
#include "fs_public.h"
#include "qurt_error.h"
#include <stdio.h>
#include <string.h>

struct fs_os_tls_key_type
{
  uint8 is_created;
  int key;
};

int
fs_os_mutex_init (fs_os_mutex_t *hdl_ptr)
{
  FS_OS_ASSERT (hdl_ptr != NULL);

  qurt_rmutex_init ((qurt_mutex_t *)hdl_ptr);

  return 0;
}

int
fs_os_mutex_lock (fs_os_mutex_t *hdl_ptr)
{
  FS_OS_ASSERT (hdl_ptr != NULL);

  qurt_rmutex_lock ((qurt_mutex_t *)hdl_ptr);

  return 0;
}

int
fs_os_mutex_unlock (fs_os_mutex_t *hdl_ptr)
{
  FS_OS_ASSERT (hdl_ptr != NULL);

  qurt_rmutex_unlock ((qurt_mutex_t *)hdl_ptr);

  return 0;
}

void
fs_os_thread_attr_init (fs_os_thread_attr_t *thread_attr)
{
  FS_OS_ASSERT (thread_attr);

  memset (thread_attr, 0x0, sizeof (fs_os_thread_attr_t));
}

int
fs_os_thread_create (fs_os_thread_t *thread, fs_os_thread_attr_t *attr,
                     fs_os_thread_return_type (*thread_start) (void *),
                     void *args)
{
  int result;
  qurt_thread_attr_t qurt_attr;

  FS_OS_ASSERT (thread != NULL);
  FS_OS_ASSERT (thread_start != NULL);
  FS_OS_ASSERT (attr != NULL);

  qurt_thread_attr_init (&qurt_attr);

  if (attr->thread_name != NULL)
  {
    qurt_thread_attr_set_name (&qurt_attr, attr->thread_name);
  }

  /* Qurt needs stack to be specified by caller. */
  FS_OS_ASSERT (attr->stack_size != 0);
  FS_OS_ASSERT (attr->stack_addr != NULL);

  qurt_thread_attr_set_stack_size (&qurt_attr, attr->stack_size);
  qurt_thread_attr_set_stack_addr (&qurt_attr, attr->stack_addr);
  qurt_thread_attr_set_priority (&qurt_attr, 85);

/*  result = qurt_thread_create (&thread->handle, &qurt_attr,
                                 thread_start, args);*/
  result = qurt_thread_create (thread, &qurt_attr, thread_start, args);
  if (result == QURT_EOK)
  {
    result = 0;
  }
  else
  {
    result = fs_os_get_errno (result);
  }

  return result;
}

fs_os_thread_t
fs_os_thread_self (void)
{
  fs_os_thread_t thread_handle = (fs_os_thread_t) NULL;

  thread_handle = qurt_thread_get_id ();

  return thread_handle;
}

void
fs_os_init_qmi_client_os_params (qmi_client_os_params *os_params,
                                 unsigned int sig, unsigned int timeout_sig)
{
  FS_OS_ASSERT (os_params != NULL);

  memset (os_params, 0x0, sizeof (qmi_client_os_params));

  os_params->sig = sig;
  os_params->timer_sig = timeout_sig;
}

int
fs_os_thread_join (fs_os_thread_t *thread_handle, void **value_ptr)
{
  int result;
  int status;

  FS_OS_ASSERT (thread_handle != NULL);

  /* qurt doesnt provide exit status address. No way to return an address */
  (void) value_ptr;

  result = qurt_thread_join (*thread_handle, &status);
  if (result != QURT_ENOTHREAD || result != QURT_EOK)
  {
    return fs_os_get_errno (result);
  }

  return 0;
}

static void *
fs_os_get_thread_local_storage (struct fs_os_tls_key_type *tls)
{
  void *data = NULL;
  int result;

  FS_OS_ASSERT (tls != NULL);

  if (!tls->is_created)
  {
    result = qurt_tls_create_key (&tls->key, NULL);
    if (result != QURT_EOK)
    {
      FS_OS_ERR_FATAL ("TLS Create Key failed for tls %d%d%d", result, 0, 0);
    }
    data = fs_os_malloc (sizeof (uint32));
    if (data == NULL)
    {
      FS_OS_ERR_FATAL ("malloc failed for efs_errno %d%d%d", 0, 0, 0);
    }

    result = qurt_tls_set_specific (tls->key, data);
    if (result != QURT_EOK)
    {
      FS_OS_ERR_FATAL ("TLS set key failed for tls %d%d%d", result, 0, 0);
    }

    tls->is_created = 1;
  }

  data = NULL;
  data = qurt_tls_get_specific (tls->key);
  if (data == NULL)
  {
    FS_OS_ERR_FATAL ("TLS Get Specific Key failed for tls %d%d%d",0,0,0);
  }

  return data;
}

static struct fs_os_tls_key_type fs_os_tls_key_os_errno;
int*
fs_os_get_errno_address (void)
{
  int *data = NULL;

  data = fs_os_get_thread_local_storage(&fs_os_tls_key_os_errno);

  FS_OS_ASSERT (data != NULL);

  return data;
}

#ifndef FS_OS_SIMULATOR_BUILD

static struct fs_os_tls_key_type fs_os_tls_key_efs_errno;

int*
efs_get_errno_address (void)
{
  int *data = NULL;

  data = fs_os_get_thread_local_storage(&fs_os_tls_key_efs_errno);

  FS_OS_ASSERT (data != NULL);

  return data;
}

#endif

#endif
