/***********************************************************************
 * fs_qmi_log.h
 *
 * Diag MSG warpper for fs_qmi module.
 *
 * Copyright (C) 2015 QUALCOMM Technologies, Inc.
 *
 ***********************************************************************/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/core.adsp/2.6.6/storage/efs_qmi_put_get/common/inc/fs_qmi_msg.h#1 $ $DateTime: 2016/05/04 01:42:14 $ $Author: pwbldsvc $

when         who   what, where, why
----------   ---   ---------------------------------------------------------
2015-03-20   dks   Create

===========================================================================*/

#ifndef __FS_QMI_LOG_H__
#define __FS_QMI_LOG_H__

#include "fs_os_msg.h"

#define FS_QMI_MSG_ERROR_0(a) \
        FS_OS_MSG_ERROR_0(MSG_SSID_RFS_ACCESS,a)
#define FS_QMI_MSG_ERROR_1(a,b) \
       FS_OS_MSG_ERROR_1(MSG_SSID_RFS_ACCESS,a,b)
#define FS_QMI_MSG_ERROR_2(a,b,c) \
        FS_OS_MSG_ERROR_2(MSG_SSID_RFS_ACCESS,a,b,c)
#define FS_QMI_MSG_ERROR_3(a,b,c,d) \
       FS_OS_MSG_ERROR_3(MSG_SSID_RFS_ACCESS,a,b,c,d)

#define FS_QMI_MSG_HIGH_0(a) \
        FS_OS_MSG_HIGH_0(MSG_SSID_RFS_ACCESS,a)
#define FS_QMI_MSG_HIGH_1(a,b) \
        FS_OS_MSG_HIGH_1(MSG_SSID_RFS_ACCESS,a,b)
#define FS_QMI_MSG_HIGH_2(a,b,c) \
        FS_OS_MSG_HIGH_2(MSG_SSID_RFS_ACCESS,a,b,c)
#define FS_QMI_MSG_HIGH_3(a,b,c,d) \
        FS_OS_MSG_HIGH_3(MSG_SSID_RFS_ACCESS,a,b,c,d)

#define FS_QMI_MSG_MED_0(a) \
        FS_OS_MSG_MED_0(MSG_SSID_RFS_ACCESS,a)
#define FS_QMI_MSG_MED_1(a,b) \
        FS_OS_MSG_MED_1(MSG_SSID_RFS_ACCESS,a,b)
#define FS_QMI_MSG_MED_2(a,b,c) \
        FS_OS_MSG_MED_2(MSG_SSID_RFS_ACCESS,a,b,c)
#define FS_QMI_MSG_MED_3(a,b,c,d) \
        FS_OS_MSG_MED_3(MSG_SSID_RFS_ACCESS,a,b,c,d)

#define FS_QMI_MSG_LOW_0(a) \
        FS_OS_MSG_LOW_0(MSG_SSID_RFS_ACCESS,a)
#define FS_QMI_MSG_LOW_1(a,b) \
        FS_OS_MSG_LOW_1(MSG_SSID_RFS_ACCESS,a,b)
#define FS_QMI_MSG_LOW_2(a,b,c) \
        FS_OS_MSG_LOW_2(MSG_SSID_RFS_ACCESS,a,b,c)
#define FS_QMI_MSG_LOW_3(a,b,c,d) \
        FS_OS_MSG_LOW_3(MSG_SSID_RFS_ACCESS,a,b,c,d)

#define FS_QMI_MSG_STRING_1(a,b) \
        FS_OS_MSG_STRING_1(MSG_SSID_RFS_ACCESS,a,b)



#endif /* not __FS_QMI_LOG_H__ */
