#
#  File of general utility classes and functions
#   for QuRT Python scripting
#
#  Use one of the following to import:
#     import lib.qurt                   {Imports everything but with lib.qurt needed as a prefix}
#     from lib.qurt import *            {Imports everything, no prefix needed}
#     from lib.qurt import x,y,z        {Imports just the listed identifiers; replace x,y,z with actual identifiers}
#

if __name__ == '__main__':
    raise SystemExit('qurt.py is not an executable script')

class _QurtInf(object):
    #
    # Integer Infinities, usable as QurtInfinity and -QurtInfinity.
    # The intent is that these can be used where an integer is normally
    #  used, and that some basic arithmetic operations work between
    #  infinite values and themselves and with integers -- in particular
    #  comparison, subtraction, and sorting.
    # Note that this implementation differs from the IEEE concept of
    #  floating point infinities in one major way:  infinities of the
    #  same sign compare equal to each other and have a difference of
    #  integer zero.  QurtInfinity - QurtInfinity is equal to zero.
    #  (IEEE floating point defines Inf - Inf as being NaN.)
    #
    # Note that the class name is prepended with an underscore to
    #  indicate that it's not expected to be used directly by
    #  scripts.  There should generally only be two instances of
    #  this class in existence, and they are accessible as QurtInfinity
    #  (or +QurtInfinity) and -QurtInfinity.
    #
    def __init__(self, sign):
        self.sign = sign                            # sign = 1 or -1
    def __repr__(self):
        return self.repr_string
    def __cmp__(self,other):
        if isinstance(other,self.__class__):
            return (self.sign-other.sign)           # Comparison between infinities
        else:
            return self.sign                        # Comparison against non-infinity
    def __sub__(self,other):
        if self == other:
            return 0                                # Two infinities of the same sign have 0 difference
        else:
            return self                             # Subtracting anything else is a no-op
    def __rsub__(self,other):
        return -(self-other)                        # other-self; return -(self-other)
    def __pos__(self):
        return self                                 # +X returns X
    def __neg__(self):
        return self.negated                         # -X returns X.negated

QurtInfinity = _QurtInf(1)
QurtInfinity.repr_string = '+QurtInfinity'
QurtInfinity.negated = QurtInfinity.__class__(-1)   # Initialize the negative of +infinity
QurtInfinity.negated.repr_string = '-QurtInfinity'
QurtInfinity.negated.negated = QurtInfinity         # Initialize the negative of -infinity

class QurtAddressRange(object):
    #
    # Define range description.  This is a left boundary, a right boundary,
    #  and a set of attributes.  Attributes which are not present
    #  are returned as None.  Zero-length ranges evaluate as boolean False and
    #  have no attributes.  Non-empty ranges evaluate as boolean True and
    #  may have attributes.
    #
    __slots__ = ['left','right',
                 'addr','size',
                 'container',
                 'full','__dict__']                     # Keep these attributes out of __dict__

    def __getattr__(self, a):
        return None                                     # If an attribute is missing, it's None

    def __nonzero__(self):
        return (self.size > 0)                          # Allows testing non-zero range with "if range:"

    def __repr__(self):
        tmp = '%s(' % self.__class__.__name__
        if self.size > 0:
            leftrepr = '%r' % self.left
            rightrepr = '%r' % self.right
            try:
                leftrepr = '0x%X' % self.left
            except TypeError:
                pass
            try:
                rightrepr = '0x%X' % self.right
            except TypeError:
                pass
            tmp += 'left=%s,right=%s' % (leftrepr,rightrepr)
            for k in self.__dict__:
                tmp += ',%s=%r' % (k,self.__dict__[k])
        return tmp + ')'

    def __init__(self, addr=0, size=0, left=0, right=0, full=False, templates=None, container=None, **kw):
        if full:
            left,right = (-QurtInfinity,QurtInfinity)   # Range is full (infinite)
        elif right > left:
            pass                                        # Range already passed as left,right
        elif size > 0:
            left,right = (addr,addr+size)               # Range was (addr,size) and non-empty
        else:
            left,right = (0,0)                          # Range is empty

        self.left = left
        self.right = right
        self.addr = left
        self.size = right-left
        self.full = (left == -QurtInfinity and right == QurtInfinity)
        self.container = container

        if self.size > 0:
            if templates:
                for t in templates:
                    self.__dict__.update(t.__dict__)    # Add each template's attributes in order
            self.__dict__.update(kw)                    # Add new attributes passed with the creation request
            for k,v in self.__dict__.items():
                if v == None:
                    del self.__dict__[k]                # Delete attributes which are set to None

    def accepts(self, other):
        #
        # Called to check if self and other can be merged
        #
        # If they can be merged, should return the template
        #  array for merging the two.  Generally the template
        #  array will be either [self,other] to merge the
        #  attributes of other on top of self, or it will be
        #  [other] to replace the attributes.
        #
        # If they cannot be merged, throw an appropriate
        #  exception.
        #
        return [other]                                  # Base class behavior if not overridden is
                                                        #  to accept all merges, and to replace the
                                                        #  attributes rather than merging.

    def merge(self, other):
        #
        # Merge other over top of self
        #
        if self.left >= other.right:
            return                                      # No overlap
        if other.left >= self.right:
            return                                      # No overlap
        templates = self.accepts(other)
        newobjs = [self.__class__(left=self.left,
                                  right=other.left,
                                  templates=[self],
                                  container=self.container),
                   templates[0].__class__(left=max(self.left, other.left),
                                          right=min(self.right, other.right),
                                          templates=templates,
                                          container=self.container),
                   self.__class__(left=other.right,
                                  right=self.right,
                                  templates=[self],
                                  container=self.container)]
        ix = self.container.ranges.index(self)
        self.container.ranges[ix:ix+1] = [obj for obj in newobjs if obj]

class AllocatedRange(QurtAddressRange):
    def accepts(self, other):
        if isinstance(other,AllocatedRange):
            if self.reserve_only:
                return [other]
            if other.reserve_only:
                return [self]
            raise Exception('Two entities attempting to allocate same address:\n%r\n%r' % (self,other))
        return [self,other]

class QurtAddressSpace(object):
    #
    # Describe an entire address space.  An address space is basically
    #  represented by an array of QurtAddressRange objects (or objects
    #  derived from QurtAddressRange).  The individual range objects
    #  cover the entire range from address -QurtInfinity to +QurtInfinity,
    #  thus making no assumptions about the size of the address space.
    #  There are no gaps; undescribed or unfilled areas in an address
    #  space are filled with ranges with no attributes.
    #
    def __init__(self):
        self.ranges = [QurtAddressRange(full=True, container=self)]
    def __iadd__(self, other):
        for x in self.ranges[:]:
            x.merge(other)
        return self
    def alloc(self, **kw):
        tmp = AllocatedRange(**kw)
        self += tmp
        return tmp
    def report(self):
        for x in self.ranges:
            print x
    def find(self, size, align, **kw):
        # Same as alloc but find the "best fit" range with size and align
        alist = [r for r in self.ranges if not isinstance(r,AllocatedRange)]
        alist.sort(key=lambda r: (r.size,r.addr))
        for r in alist:
            padleft = alignup(r.addr,align) - r.addr
            if r.size >= size + padleft:
                return self.alloc(addr = r.addr+padleft,
                                  size = size,
                                  **kw)
        return None
    def find_optimal(self, size, tlblimit=0x100000, **kw):
        # Same as alloc but find the "most optimal fit" range for the given
        #  size.  Here, "most optimal" means the one that uses the fewest
        #  TLB entries.
        total_size = alignup(size, 0x1000)
        largest_tlb = 0x1000
        while 4*largest_tlb <= min(tlblimit,total_size):
            largest_tlb *= 4
        alist = [r for r in self.ranges if not isinstance(r,AllocatedRange)]
        alist.sort(key=lambda r: (r.size,r.addr))
        for r in alist:
            base_addr = alignup(r.addr,0x1000)
            esize = 0x1000
            while esize < largest_tlb:
                # Compute the number of TLB entries we want to see of size (esize)
                needed = (total_size / esize) & 3
                # Compute the number of TLB entries needed from base_addr to reach
                #  the next size
                delta = (-(base_addr / esize)) & 3
                if delta > needed:
                    base_addr += esize * (delta-needed)
                esize *= 4
            if base_addr + total_size <= r.right:
                return self.alloc(addr = base_addr,
                                  size = total_size,
                                  **kw)
        return None

def convert_size(s):
    #
    # Convert the string to an integer.
    # Permit suffixes of 'K', 'KB', 'M', 'MB', 'G', 'GB'
    #  which correspond to the appropriate multipliers.
    # The suffix matching is non-case-sensitive.
    #
    import re
    p = re.split('(?i)([KMG]B?)', s, 1)      # Split with regex that matches K, M, G with an optional B, case-insensitive
    shift_count = 0
    if len(p) > 1:
        if p[2]:                             # The size suffix didn't come at the end of the string
            raise ValueError('Improper size format: %s' % s)
        shift_count = {'K':10,'M':20,'G':30}[p[1][0]]
    return int(p[0],0) << shift_count

def aligndown(n, a):
    return n & ~(a-1)

def alignup(n, a):
    return (n+(a-1)) & ~(a-1)

def run_script(main):
    from traceback import format_exc
    from sys import argv, stderr
    try:
        raise SystemExit(main(argv))
    except (SystemExit, KeyboardInterrupt):
        raise
    except Exception, err:
        print >> stderr, "%s\n%s\n%s: FAILED\n%s" % ('*'*72, format_exc(), argv[0], '*'*72)
        raise SystemExit(1)

_BookmarkDict = dict()

def BookmarkWrite(bookmark, f, s):
    #
    # BookmarkWrite() executes f.write(s).  The argument "bookmark"
    #  is expected to be either a string not beginning with an @ sign,
    #  which sets a bookmark before writing, or a string beginning
    #  with an @ sign, which rewinds to a bookmark before writing.
    #
    # Example:
    #   BookmarkWrite("HEADER1", f, 'foo')        # Writes the string 'foo'
    #   ... followed later by ...
    #   BookmarkWrite("@HEADER1", f, 'bar')       # Overwrites 'foo' with 'bar'
    #
    if bookmark.startswith('@'):
        f.flush()
        loc = _BookmarkDict[bookmark[1:]]
        f.seek(loc)
    else:
        loc = f.tell()
        _BookmarkDict[bookmark] = loc
    f.write(s)
    f.flush()
    return (loc,len(s))

class SubFile(object):
    #
    # Returns a read-only file-like object which returns only the data beginning
    #  at offset start, and extending for len bytes.
    # Raises an exception during reading if the data cannot be properly read.
    #
    def __init__(self, f, start, len):
        self._fbase = f
        self._start = start
        self._end = start+len
        self.seek(0)
    def close(self):
        self._fbase = None
    def seek(self, offset, whence=0):
        if whence == 0:
            self._loc = self._start + offset
        elif whence == 1:
            self._loc = self._loc + offset
        elif whence == 2:
            self._loc = self._end + offset
        else:
            self._loc = self._end + 1   # Force an error
        if self._loc < self._start or self._loc > self._end:
            raise IOError()
    def tell(self):
        return self._loc - self._start
    def read(self, count = -1):
        if count < 0:
            count = self._end
        count = min(count,self._end - self._loc)
        self._fbase.seek(self._loc)
        ret = self._fbase.read(count)
        N = len(ret)
        if N != count:
            raise IOError('SubFile of %s: premature EOF' % self._fbase.name)
        self._loc += N
        return ret

QurtHeaderDict = dict()

class QurtHeaderCodec(object):
    #
    # A base class from which specific header coder/decoders can be built.
    # Developed to easily build ELF header coder/decoders.
    #
    # The easiest way to do this is to define the variable _format
    #  in the sub-class.
    #
    # _format can be a string which is separated on whitespace, and
    #  which gives in order any number of repeats of: attribute name,
    #  struct.unpack format, and printable format.  The string can
    #  span multiple lines for readability.
    #
    # Example which parses the C struct:
    #   struct foo {
    #     int16_t a;
    #     int16_t b;
    #     uint32_t c;
    #   };
    #
    # _format = 'a h %d b h %d c L %u'
    #
    # Another important option in the sub-class is the ability to
    #  add the _legal() function, which should self-inspect the
    #  object and throw an appropriate exception if the fields
    #  are inconsistent or malformed.
    #

    def __init__(self,data,**kw):
        if isinstance(data,dict):
            self.__dict__.update(data)
        else:
            import struct
            val = QurtHeaderDict.get(self.__class__,None)
            if val == None:
                tmp = self._format.split()
                attrnames = tmp[0::3]
                structfmt = tmp[1::3]
                printfmt = tmp[2::3]
                fmt = '<'+''.join(structfmt)
                val = dict(_decode = fmt,
                           _size = struct.calcsize(fmt),
                           _attrnames = attrnames,
                           _printfmt = printfmt)
                QurtHeaderDict[self.__class__] = val
            self.__dict__.update(val)
            if hasattr(data,'read'):
                # File like object; read from it
                data = data.read(self._size)
            if data != None:
                tup = struct.unpack(self._decode,data)
                self.__dict__.update(zip(self._attrnames,tup))
            self.__dict__.update(kw)
            self._legal()               # Check for legality after decoding
    def _legal(self):
        pass
    def copy(self):
        return self.__class__(self.__dict__)
    def output(self):
        v = ''
        if not getattr(self,'temp',False):
            import struct
            self._legal()               # Check for legality before encoding
            v = struct.pack(self._decode,*[getattr(self,a) for a in self._attrnames])
        return v

class Elf32Header(QurtHeaderCodec):
    _format = '''
        e_ident     16s     %r
        e_type        H     %u
        e_machine     H     %u
        e_version     L     %u
        e_entry       L   0x%X
        e_phoff       L     %u
        e_shoff       L     %u
        e_flags       L   0x%X
        e_ehsize      H     %u
        e_phentsize   H     %u
        e_phnum       H     %u
        e_shentsize   H     %u
        e_shnum       H     %u
        e_shstrndx    H     %u
    '''
    def _legal(self):
        if self.e_ehsize != 52:
            raise Exception('Bad ELF format')
        if self.e_phentsize != 32:
            raise Exception('Bad ELF format')
        if self.e_shentsize != 40:
            raise Exception('Bad ELF format')

class Elf32ProgramHeader(QurtHeaderCodec):
    _format = '''
        p_type        L   0x%X
        p_offset      L   0x%X
        p_vaddr       L   0x%X
        p_paddr       L   0x%X
        p_filesz      L   0x%X
        p_memsz       L   0x%X
        p_flags       L   0x%X
        p_align       L   0x%X
    '''
    PT_LOAD = 1
    PT_PHDR = 6
    PT_GNU_STACK = 0x6474E551
    def contents(self):
        #
        # Return a file-like object which returns the file contents associated with the program header.
        # In order for this to work, the containing file object must have been installed as an
        #  attribute under self.f.
        #
        return SubFile(self.f, self.p_offset, self.p_filesz)

class Elf32SectionHeader(QurtHeaderCodec):
    _format = '''
        sh_name       L   0x%X
        sh_type       L   0x%X
        sh_flags      L   0x%X
        sh_addr       L   0x%X
        sh_offset     L   0x%X
        sh_size       L   0x%X
        sh_link       L   0x%X
        sh_info       L   0x%X
        sh_addralign  L   0x%X
        sh_entsize    L   0x%X
    '''

    SHF_WRITE = 1
    SHF_ALLOC = 2
    SHF_EXECINSTR = 4
    SHN_UNDEF = 0
    SHT_NULL = 0
    SHT_PROGBITS = 1
    SHT_STRTAB = 3
    SHT_NOBITS = 8

    def contents(self):
        #
        # Return a file-like object which returns the file contents associated with the section header.
        # In order for this to work, the containing file object must have been installed as an
        #  attribute under self.f.
        #
        # Note that SHT_NOBITS sections have no file contents and return an empty string.  If it
        #  becomes desirable in the future, we might consider having SHT_NOBITS return a string
        #  of zero bytes of the appropriate section length.
        #
        if self.sh_type == self.SHT_NOBITS:
            return SubFile(self.f, 0, 0)
        else:
            return SubFile(self.f, self.sh_offset, self.sh_size)

def strtabify(shdr):
    ret = '\0'
    for s in sorted(shdr, key=lambda s: -len(s.name)):
        if s.output():
            key = s.name+'\0'
            N = ret.find(key)
            if N < 0:
                ret += key
                N = ret.find(key)
            s.sh_name = N
    return ret

