#ifndef DECODE_STRUCT_H
#define DECODE_STRUCT_H
/*==============================================================================
  Copyright (c) 2012-2013 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

/*==============================================================================
  Sample decode library wrapper header file
 
  This file contains structure definitions that are internal to Sample
  decode CAPI.
==============================================================================*/

static const uint32_t DECODER_INP_BUF_SIZE = 4096 * 2;
//Maximum buffer size for input. This corresponds to enum variable
//eDec_input_bytes_per_sample used by Elite Framework.

static const uint32_t DECODER_OUT_BUF_SIZE = 8192 * 2;
//Maximum buffer size for input.
//This corresponds to enum variable eDec_output_bytes_per_sample used by Elite
//Framework.

static const uint32_t DECODER_THREAD_STACK_SIZE = 2048;
//Corresponds to enum variable eIcapiThreadStackSize used by Elite Framework
// to get thread stack size value from CAPI.
// Based on Decoder Requirement the stack size can be changed.

static const uint32_t DECODER_FORMAT_BLOCK_REQ = 1;
//Corresponds to enum variable eIcapiWaitForFormatBlock used by Elite Framework
//to get format block value from CAPI(Useful in Update Media Format Function).
//If the decode service is ready to be initialized without any SetParam,
// then this value can be set to 0.

static const uint32_t DECODER_KIPS = 20000;
//Corresponds to enum variable eIcapiKCPSRequired used by Elite Framework
//to get KCPS value from CAPI.

static const uint32_t DEC_FRAME_SIZE = 1000;
//Corresponds to the minimum bytes required by functional lib for processing

#ifdef __cplusplus
extern "C"
{
#endif
/* Enums used by SetParam/GetParam Subroutines helpful in setting		*
 * parameters from Elite Framework  									*/
enum eDecoder {
  eNumBlockSize = eIcapiMaxParamIndex,      // Decoder specific Enum variables starting at eIcapiMaxParamIndex
  eFormatTag,
  eIsSigned,
  eIsInterleaved,
  eDec_input_bytes_per_sample,
  eDec_output_bytes_per_sample,
  eDec_EndOfFrame,
  eDec_samples_to_copy
};

typedef struct asm_decode_fmt_blk_t asm_decode_fmt_blk_t;

/**
 * Define the new media format block
 */
struct asm_decode_fmt_blk_t {
  uint16_t  num_channels;
  /**< Number of channels.

       @values 1 to 8 */

  uint16_t  bits_per_sample;
  /**< Number of bits per sample per channel.

       @values 16, 24

       When used for playback, the client must send 24-bit samples packed in
       32-bit words. The 24-bit samples must be placed in the most
       significant 24 bits of the 32-bit word.

       When used for recording, the aDSP sends 24-bit samples packed in
       32-bit words. The 24-bit samples are placed in the most significant
       24 bits of the 32-bit word. */

  uint32_t  sample_rate;
  /**< Number of samples per second.

       @values 2000 to 48000, 96000, 192000 Hz */

  uint16_t  is_signed;
  /**< Flag that indicates the PCM samples are signed (1). */

  uint16_t  reserved;
  /**< This field must be set to zero. */

  uint8_t   channel_mapping[8];
  /**< Channel array of size 8. Channel[i] mapping describes channel I. Each
       element i of the array describes channel I inside the buffer where 0
       @le I < num_channels. An unused channel is set to zero.*/

};

typedef struct capi_pcm_decoder_t capi_pcm_decoder_t;
struct capi_pcm_decoder_t {
  const ICAPIVtbl* vtbl;

  decode_t* m_pDecoder;
  int32_t m_lFormatTag; // Media Format Tag
  int16_t m_sNumberOfChannels; // Number of granules
  int16_t m_sBitsPerSample; // Bits Per Sample
  int32_t m_lSamplingRate; // Sampling Rate
  int32_t m_lIsSigned; // Signed or Unsigned
  int32_t m_lIsInterleaved; // Interleaved or de-interleaved
  int32_t m_lNumBlockSize; // Number of SAMPLE Block Size
  int32_t m_lrSampleUsed; // Number of bytes of input used
  int32_t m_lrSamplesWritten; // Number of samples outputted
  int32_t m_lDec_output_bytes_per_sample; // Output Bytes per Sample
  int32_t m_lDec_input_bytes_per_sample; // Input Bytes per Sample
  int32_t m_lDec_EndOfFrame; // End of one frame
  int32_t m_lDec_SamplesToCopy; // Samples to copy from input to output
  asm_decode_fmt_blk_t* m_lDec_MediaFmtPtr; // Media format update pointer
  CAPI_ChannelMap_t   				out_chan_map; // output channel map
};

#ifdef __cplusplus
}
#endif /*__cplusplus*/

#endif /*#ifndef DECODE_STRUCT_H */
