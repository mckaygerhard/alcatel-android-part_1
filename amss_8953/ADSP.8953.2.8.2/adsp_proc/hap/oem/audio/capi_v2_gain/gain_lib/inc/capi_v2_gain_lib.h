#ifndef PCM_GAIN_H
#define PCM_GAIN_H
/* ======================================================================== */
/**
   @file capi_v2_gain_lib.h

   C source file to implement decimation.
 */

/*==============================================================================
  Copyright (c) 2015 Qualcomm Technologies, Inc.
  All rights reserved. Qualcomm Proprietary and Confidential.
==============================================================================*/

#include "mmdefs.h"

#ifdef __cplusplus
extern "C" {
#endif

void pcm_gain(int16_t* primary_mic_ptr,
              int16_t* out_ptr,
              uint32_t gain_val,
              uint32_t num_samples);

#ifdef __cplusplus
}
#endif

#endif // PCM_GAIN_H