#ifndef __CORE_HANG_HWIO_H__
#define __CORE_HANG_HWIO_H__
/*
===========================================================================
*/
/**
  @file core_hang_hwio.h
  @brief Auto-generated HWIO interface include file.

  Reference chip release:
    MSM8953 (Jacala) [jacala_v1.0_p3q3r17.3_MTO]
 
  This file contains HWIO register definitions for the following modules:
    APCS_ALIAS0_APSS_ACS
    APCS_ALIAS1_APSS_ACS
    APCS_ALIAS2_APSS_ACS
    APCS_ALIAS3_APSS_ACS
    APCS_ALIAS4_APSS_ACS
    APCS_ALIAS5_APSS_ACS
    APCS_ALIAS6_APSS_ACS
    APCS_ALIAS7_APSS_ACS

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/rpm.bf/2.4/core/bsp/rpm/inc/core_hang_hwio.h#1 $
  $DateTime: 2015/12/15 01:31:49 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"

#define A53SS_BASE                                                                                          0x6B000000

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS0_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS0_APSS_ACS_REG_BASE                                                                      (A53SS_BASE      + 0x00188000)

#define HWIO_APCS_ALIAS0_APC_SECURE_ADDR                                                                   (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS0_APC_SECURE_RMSK                                                                          0xf
#define HWIO_APCS_ALIAS0_APC_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_SECURE_ADDR, HWIO_APCS_ALIAS0_APC_SECURE_RMSK)
#define HWIO_APCS_ALIAS0_APC_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS0_APC_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_APC_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS0_APC_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_APC_SECURE_ADDR,m,v,HWIO_APCS_ALIAS0_APC_SECURE_IN)
#define HWIO_APCS_ALIAS0_APC_SECURE_VOTE_BMSK                                                                     0x8
#define HWIO_APCS_ALIAS0_APC_SECURE_VOTE_SHFT                                                                     0x3
#define HWIO_APCS_ALIAS0_APC_SECURE_TST_BMSK                                                                      0x4
#define HWIO_APCS_ALIAS0_APC_SECURE_TST_SHFT                                                                      0x2
#define HWIO_APCS_ALIAS0_APC_SECURE_CLK_CTL_BMSK                                                                  0x2
#define HWIO_APCS_ALIAS0_APC_SECURE_CLK_CTL_SHFT                                                                  0x1
#define HWIO_APCS_ALIAS0_APC_SECURE_SLP_CTL_BMSK                                                                  0x1
#define HWIO_APCS_ALIAS0_APC_SECURE_SLP_CTL_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR                                                                  (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_RMSK                                                                  0x200006ff
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS0_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS0_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                                  0x20000000
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                                        0x1d
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                                      0x400
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                                        0xa
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                                        0x200
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                                          0x9
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                                           0x80
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                                            0x7
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_GATE_CLK_BMSK                                                               0x40
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_GATE_CLK_SHFT                                                                0x6
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_COREPOR_RST_BMSK                                                            0x20
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_COREPOR_RST_SHFT                                                             0x5
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_RST_BMSK                                                               0x10
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_RST_SHFT                                                                0x4
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                                             0x8
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                                             0x3
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_L1_RST_DIS_BMSK                                                              0x4
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_L1_RST_DIS_SHFT                                                              0x2
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                                          0x2
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                                          0x1
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CLAMP_BMSK                                                                   0x1
#define HWIO_APCS_ALIAS0_CPU_PWR_CTL_CLAMP_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS0_APC_A53_CFG_STS_ADDR                                                              (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000040)
#define HWIO_APCS_ALIAS0_APC_A53_CFG_STS_RMSK                                                              0x10000000
#define HWIO_APCS_ALIAS0_APC_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS0_APC_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS0_APC_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS0_APC_A53_CFG_STS_SMPNAMP_BMSK                                                      0x10000000
#define HWIO_APCS_ALIAS0_APC_A53_CFG_STS_SMPNAMP_SHFT                                                            0x1c

#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_ADDR                                                               (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_RMSK                                                                0xb1f37ff
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_PWR_STATUS_ADDR, HWIO_APCS_ALIAS0_APC_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_SAW_SLP_ACK_BMSK                                                    0x8000000
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_SAW_SLP_ACK_SHFT                                                         0x1b
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_NO_PWR_DWN_BMSK                                                0x2000000
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_NO_PWR_DWN_SHFT                                                     0x19
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_PWRUP_REQ_BMSK                                                 0x1000000
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_PWRUP_REQ_SHFT                                                      0x18
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_HW_EVENT_BMSK                                                        0x1f0000
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_HW_EVENT_SHFT                                                            0x10
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_RET_SLP_ACK_BMSK                                                       0x2000
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_RET_SLP_ACK_SHFT                                                          0xd
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_RET_SLP_REQ_BMSK                                                       0x1000
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_RET_SLP_REQ_SHFT                                                          0xc
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_TRGTD_DBG_RST_BMSK                                                      0x400
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_TRGTD_DBG_RST_SHFT                                                        0xa
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_RST_BMSK                                                           0x200
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_RST_SHFT                                                             0x9
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_COREPOR_RST_BMSK                                                        0x100
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_COREPOR_RST_SHFT                                                          0x8
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_HS_STS_BMSK                                                              0x80
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_HS_STS_SHFT                                                               0x7
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_MEM_HS_STS_BMSK                                                     0x40
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_MEM_HS_STS_SHFT                                                      0x6
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CLAMP_BMSK                                                               0x20
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CLAMP_SHFT                                                                0x5
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_FRC_CLK_OFF_BMSK                                                         0x10
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_FRC_CLK_OFF_SHFT                                                          0x4
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_AHB_CLK_BMSK                                                              0x8
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_AHB_CLK_SHFT                                                              0x3
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_REF_CLK_BMSK                                                              0x4
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_REF_CLK_SHFT                                                              0x2
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_AUX_CLK_BMSK                                                         0x2
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_AUX_CLK_SHFT                                                         0x1
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_PLL_CLK_BMSK                                                         0x1
#define HWIO_APCS_ALIAS0_APC_PWR_STATUS_CORE_PLL_CLK_SHFT                                                         0x0

#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_ADDR                                                             (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_EN_BMSK                                                                 0x4
#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_EN_SHFT                                                                 0x2
#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_SEL_BMSK                                                                0x3
#define HWIO_APCS_ALIAS0_APC_TEST_BUS_SEL_SEL_SHFT                                                                0x0

#define HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_ADDR                                                            (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_RMSK                                                                   0x1
#define HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_ADDR, HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_RMSK)
#define HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_ADDR, m)
#define HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_ADDR,v)
#define HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_ADDR,m,v,HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_IN)
#define HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_RST_BMSK                                                               0x1
#define HWIO_APCS_ALIAS0_CPU_TRGTD_DBG_RST_RST_SHFT                                                               0x0

#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR                                                             (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_RMSK                                                             0xff000001
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_HS_CNT_BMSK                                                      0xff000000
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_HS_CNT_SHFT                                                            0x18
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_HS_EN_BMSK                                                              0x1
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_CTL_HS_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_ADDR                                                          (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_RMSK                                                             0x10003
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_IDLE_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_IDLE_SHFT                                                           0x10
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_EN_REST_BMSK                                                         0x2
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_EN_REST_SHFT                                                         0x1
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                                          0x1
#define HWIO_APCS_ALIAS0_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                                          0x0

#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_ADDR                                                            (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000044)
#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_RMSK                                                               0x101ff
#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_ADDR, HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_RMSK)
#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_IN)
#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_BMSK                                             0x10000
#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_SHFT                                                0x10
#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_AUX_STARTADDR_BMSK                                                   0x1ff
#define HWIO_APCS_ALIAS0_SPM_AUX_STARTADDR_AUX_STARTADDR_SHFT                                                     0x0

#define HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_ADDR                                                               (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_RMSK                                                                    0x3ff
#define HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_ADDR, HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_RMSK)
#define HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_ADDR, m)
#define HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_ADDR,v)
#define HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_ADDR,m,v,HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_IN)
#define HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_SLP_CNT_BMSK                                                            0x3ff
#define HWIO_APCS_ALIAS0_APC_L1_SLP_CNT_SLP_CNT_SHFT                                                              0x0

#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_ADDR                                                                  (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000050)
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_RMSK                                                                  0xc0007fff
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS0_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                                                0x80000000
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                                      0x1f
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                                                 0x40000000
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                                       0x1e
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                                           0x7fff
#define HWIO_APCS_ALIAS0_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_ADDR                                                                (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_RMSK                                                                0xc0007fff
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                                              0x80000000
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                                    0x1f
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                                               0x40000000
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                                     0x1e
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                                         0x7fff
#define HWIO_APCS_ALIAS0_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                                            0x0

#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_ADDR                                                          (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_RMSK                                                          0xc0007fff
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK                                0x80000000
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                                      0x1f
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK                                 0x40000000
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                                       0x1e
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                                           0x7fff
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                                              0x0

#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_ADDR                                                           (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x0000005c)
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_RMSK                                                           0xc0007fff
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK                                  0x80000000
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                                        0x1f
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK                                   0x40000000
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                                         0x1e
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                                             0x7fff
#define HWIO_APCS_ALIAS0_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                                                0x0

#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_ADDR                                                           (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_RMSK                                                           0x80000001
#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_ADDR, HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_RMSK)
#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_ADDR, m)
#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_ADDR,v)
#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_ADDR,m,v,HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_IN)
#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_BMSK                                       0x80000000
#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_SHFT                                             0x1f
#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_BMSK                                              0x1
#define HWIO_APCS_ALIAS0_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_SHFT                                              0x0

#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_ADDR                                                             (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                                    0x4
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                                    0x2
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_IGNORE_BMSK                                                             0x2
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_IGNORE_SHFT                                                             0x1
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                                         0x1
#define HWIO_APCS_ALIAS0_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_ADDR                                                           (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000070)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_RMSK                                                                0x1ff
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_EN_BMSK                                                             0x1ff
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EN_EN_SHFT                                                               0x0

#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_ADDR                                                              (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000074)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_RMSK                                                                   0x1ff
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                                         0x1ff
#define HWIO_APCS_ALIAS0_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                                           0x0

#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_ADDR                                                         (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_RMSK                                                            0x1ffff
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                                         0x1ffff
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                                             0x0

#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_ADDR                                                            (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x0000007c)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_RMSK                                                               0x1ffff
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                                                   0x1ffff
#define HWIO_APCS_ALIAS0_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                                       0x0

#define HWIO_APCS_ALIAS0_SPM_EVENT_STS_ADDR                                                                (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000080)
#define HWIO_APCS_ALIAS0_SPM_EVENT_STS_RMSK                                                                     0x1ff
#define HWIO_APCS_ALIAS0_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS0_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS0_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_EVENT_STS_EVENT_VAL_BMSK                                                           0x1ff
#define HWIO_APCS_ALIAS0_SPM_EVENT_STS_EVENT_VAL_SHFT                                                             0x0

#define HWIO_APCS_ALIAS0_SPM_PWR_CTL_STS_ADDR                                                              (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000084)
#define HWIO_APCS_ALIAS0_SPM_PWR_CTL_STS_RMSK                                                                 0x1ffff
#define HWIO_APCS_ALIAS0_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS0_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS0_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                                     0x1ffff
#define HWIO_APCS_ALIAS0_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_ADDR                                                    (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_RMSK                                                           0xf
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                       0x8
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                       0x3
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_BMSK                               0x4
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_SHFT                               0x2
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_BMSK                              0x2
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_SHFT                              0x1
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_BMSK                              0x1
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_SHFT                              0x0

#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_ADDR                                                        (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_RMSK                                                        0xffffffff
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_BMSK                            0xffffffff
#define HWIO_APCS_ALIAS0_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_SHFT                                   0x0

#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR                                                 (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000a0)
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK                                                        0xf
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                 0x8
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                 0x3
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_BMSK                         0x4
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_SHFT                         0x2
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_BMSK                        0x2
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_SHFT                        0x1
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_BMSK                        0x1
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_SHFT                        0x0

#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_ADDR                                                     (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x0000009c)
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_BMSK                      0xffffffff
#define HWIO_APCS_ALIAS0_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_SHFT                             0x0

#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR                                                   (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000a8)
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK                                                          0xf
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                     0x8
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                     0x3
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_BMSK                             0x4
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_SHFT                             0x2
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_BMSK                            0x2
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_SHFT                            0x1
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_BMSK                            0x1
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_SHFT                            0x0

#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_ADDR                                                       (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000a4)
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_RMSK                                                       0xffffffff
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_BMSK                          0xffffffff
#define HWIO_APCS_ALIAS0_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_SHFT                                 0x0

#define HWIO_APCS_ALIAS0_SPARE_ADDR                                                                        (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000ff8)
#define HWIO_APCS_ALIAS0_SPARE_RMSK                                                                              0xff
#define HWIO_APCS_ALIAS0_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPARE_ADDR, HWIO_APCS_ALIAS0_SPARE_RMSK)
#define HWIO_APCS_ALIAS0_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS0_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS0_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPARE_ADDR,m,v,HWIO_APCS_ALIAS0_SPARE_IN)
#define HWIO_APCS_ALIAS0_SPARE_SPARE_BMSK                                                                        0xff
#define HWIO_APCS_ALIAS0_SPARE_SPARE_SHFT                                                                         0x0

#define HWIO_APCS_ALIAS0_SPARE_TZ_ADDR                                                                     (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_ALIAS0_SPARE_TZ_RMSK                                                                           0xff
#define HWIO_APCS_ALIAS0_SPARE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_SPARE_TZ_ADDR, HWIO_APCS_ALIAS0_SPARE_TZ_RMSK)
#define HWIO_APCS_ALIAS0_SPARE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_SPARE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS0_SPARE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_SPARE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS0_SPARE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_SPARE_TZ_ADDR,m,v,HWIO_APCS_ALIAS0_SPARE_TZ_IN)
#define HWIO_APCS_ALIAS0_SPARE_TZ_SPARE_BMSK                                                                     0xff
#define HWIO_APCS_ALIAS0_SPARE_TZ_SPARE_SHFT                                                                      0x0

#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_ADDR                                                     (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_RMSK                                                          0x7ff
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_ADDR, HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_RMSK)
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_ADDR, m)
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_ADDR,v)
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_ADDR,m,v,HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_IN)
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_BMSK                                  0x7f8
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_SHFT                                    0x3
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_BMSK                                      0x4
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_SHFT                                      0x2
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_BMSK                                0x2
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_SHFT                                0x1
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_BMSK                                0x1
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_SHFT                                0x0

#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_STATUS_ADDR                                                  (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x00000094)
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_STATUS_RMSK                                                  0xffffffff
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_STATUS_ADDR, HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_BMSK                           0xffffffff
#define HWIO_APCS_ALIAS0_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_SHFT                                  0x0

#define HWIO_APCS_ALIAS0_MAS_CFG_ADDR                                                                      (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000c0)
#define HWIO_APCS_ALIAS0_MAS_CFG_RMSK                                                                         0x10707
#define HWIO_APCS_ALIAS0_MAS_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_MAS_CFG_ADDR, HWIO_APCS_ALIAS0_MAS_CFG_RMSK)
#define HWIO_APCS_ALIAS0_MAS_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_MAS_CFG_ADDR, m)
#define HWIO_APCS_ALIAS0_MAS_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_MAS_CFG_ADDR,v)
#define HWIO_APCS_ALIAS0_MAS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_MAS_CFG_ADDR,m,v,HWIO_APCS_ALIAS0_MAS_CFG_IN)
#define HWIO_APCS_ALIAS0_MAS_CFG_MAS_MEM_CLAMP_EN_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS0_MAS_CFG_MAS_MEM_CLAMP_EN_SHFT                                                           0x10
#define HWIO_APCS_ALIAS0_MAS_CFG_MAS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS0_MAS_CFG_MAS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS0_MAS_CFG_MAS_APC_EN_REST_BMSK                                                             0x7
#define HWIO_APCS_ALIAS0_MAS_CFG_MAS_APC_EN_REST_SHFT                                                             0x0

#define HWIO_APCS_ALIAS0_APM_TILE_CFG_ADDR                                                                 (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000c4)
#define HWIO_APCS_ALIAS0_APM_TILE_CFG_RMSK                                                                      0x1ff
#define HWIO_APCS_ALIAS0_APM_TILE_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_APM_TILE_CFG_ADDR, HWIO_APCS_ALIAS0_APM_TILE_CFG_RMSK)
#define HWIO_APCS_ALIAS0_APM_TILE_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_APM_TILE_CFG_ADDR, m)
#define HWIO_APCS_ALIAS0_APM_TILE_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_APM_TILE_CFG_ADDR,v)
#define HWIO_APCS_ALIAS0_APM_TILE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_APM_TILE_CFG_ADDR,m,v,HWIO_APCS_ALIAS0_APM_TILE_CFG_IN)
#define HWIO_APCS_ALIAS0_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_BMSK                                               0x100
#define HWIO_APCS_ALIAS0_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_SHFT                                                 0x8
#define HWIO_APCS_ALIAS0_APM_TILE_CFG_APM_TILE_APMCONFIG_BMSK                                                    0xff
#define HWIO_APCS_ALIAS0_APM_TILE_CFG_APM_TILE_APMCONFIG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS0_MAS_STS_ADDR                                                                      (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000c8)
#define HWIO_APCS_ALIAS0_MAS_STS_RMSK                                                                        0x3ff373
#define HWIO_APCS_ALIAS0_MAS_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_MAS_STS_ADDR, HWIO_APCS_ALIAS0_MAS_STS_RMSK)
#define HWIO_APCS_ALIAS0_MAS_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_MAS_STS_ADDR, m)
#define HWIO_APCS_ALIAS0_MAS_STS_TMR_CNT_BMSK                                                                0x3f0000
#define HWIO_APCS_ALIAS0_MAS_STS_TMR_CNT_SHFT                                                                    0x10
#define HWIO_APCS_ALIAS0_MAS_STS_STEADY_SLEEP_BMSK                                                             0x8000
#define HWIO_APCS_ALIAS0_MAS_STS_STEADY_SLEEP_SHFT                                                                0xf
#define HWIO_APCS_ALIAS0_MAS_STS_STEADY_ACTIVE_BMSK                                                            0x4000
#define HWIO_APCS_ALIAS0_MAS_STS_STEADY_ACTIVE_SHFT                                                               0xe
#define HWIO_APCS_ALIAS0_MAS_STS_SPM_RET_BMSK                                                                  0x2000
#define HWIO_APCS_ALIAS0_MAS_STS_SPM_RET_SHFT                                                                     0xd
#define HWIO_APCS_ALIAS0_MAS_STS_SPM_PC_BMSK                                                                   0x1000
#define HWIO_APCS_ALIAS0_MAS_STS_SPM_PC_SHFT                                                                      0xc
#define HWIO_APCS_ALIAS0_MAS_STS_NEW_MODE_BMSK                                                                  0x300
#define HWIO_APCS_ALIAS0_MAS_STS_NEW_MODE_SHFT                                                                    0x8
#define HWIO_APCS_ALIAS0_MAS_STS_FSM_STATE_BMSK                                                                  0x70
#define HWIO_APCS_ALIAS0_MAS_STS_FSM_STATE_SHFT                                                                   0x4
#define HWIO_APCS_ALIAS0_MAS_STS_CURR_MODE_BMSK                                                                   0x3
#define HWIO_APCS_ALIAS0_MAS_STS_CURR_MODE_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS0_PWR_MUX_STS_ADDR                                                                  (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000cc)
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_RMSK                                                                     0x70773
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_PWR_MUX_STS_ADDR, HWIO_APCS_ALIAS0_PWR_MUX_STS_RMSK)
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_PWR_MUX_STS_ADDR, m)
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_CLAMP_BMSK                                                               0x40000
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_CLAMP_SHFT                                                                  0x12
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_TGL_SEL_BMSK                                                             0x20000
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_TGL_SEL_SHFT                                                                0x11
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_MEM_RET_BMSK                                                             0x10000
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_MEM_RET_SHFT                                                                0x10
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_APCC_EN_REST_BMSK                                                           0x70
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_APCC_EN_REST_SHFT                                                            0x4
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_EN_FEW_BMSK                                                                  0x3
#define HWIO_APCS_ALIAS0_PWR_MUX_STS_EN_FEW_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_ADDR                                                                 (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000d0)
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_RMSK                                                                   0x811377
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_MAS_OVERRIDE_ADDR, HWIO_APCS_ALIAS0_MAS_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_MAS_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_MAS_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_MAS_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS0_MAS_OVERRIDE_IN)
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_BMSK                                          0x800000
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_SHFT                                              0x17
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_BMSK                                 0x10000
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_SHFT                                    0x10
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_BMSK                             0x1000
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_SHFT                                0xc
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_BMSK                                       0x300
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_SHFT                                         0x8
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_BMSK                                     0x70
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_SHFT                                      0x4
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_BMSK                                     0x7
#define HWIO_APCS_ALIAS0_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_SHFT                                     0x0

#define HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_ADDR                                                          (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000b0)
#define HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_RMSK                                                          0xffffffff
#define HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_ADDR, HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_RMSK)
#define HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_ADDR, m)
#define HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_ADDR,v)
#define HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_ADDR,m,v,HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_IN)
#define HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_BMSK                                0xffffffff
#define HWIO_APCS_ALIAS0_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_SHFT                                       0x0

#define HWIO_APCS_ALIAS0_CORE_HANG_VALUE_ADDR                                                              (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000b4)
#define HWIO_APCS_ALIAS0_CORE_HANG_VALUE_RMSK                                                              0xffffffff
#define HWIO_APCS_ALIAS0_CORE_HANG_VALUE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_CORE_HANG_VALUE_ADDR, HWIO_APCS_ALIAS0_CORE_HANG_VALUE_RMSK)
#define HWIO_APCS_ALIAS0_CORE_HANG_VALUE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_CORE_HANG_VALUE_ADDR, m)
#define HWIO_APCS_ALIAS0_CORE_HANG_VALUE_VALUE_WHEN_HUNG_BMSK                                              0xffffffff
#define HWIO_APCS_ALIAS0_CORE_HANG_VALUE_VALUE_WHEN_HUNG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_ADDR                                                             (APCS_ALIAS0_APSS_ACS_REG_BASE      + 0x000000b8)
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_RMSK                                                                  0x1f7
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_ADDR, HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_RMSK)
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_ADDR, m)
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_ADDR,v)
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_ADDR,m,v,HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_IN)
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_PMUEVENT_SEL_BMSK                                                     0x1f0
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_PMUEVENT_SEL_SHFT                                                       0x4
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_BMSK                                           0x4
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_SHFT                                           0x2
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_BMSK                                               0x2
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_SHFT                                               0x1
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_CORE_HANG_STATUS_BMSK                                                   0x1
#define HWIO_APCS_ALIAS0_CORE_HANG_CONFIG_CORE_HANG_STATUS_SHFT                                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS1_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS1_APSS_ACS_REG_BASE                                                                      (A53SS_BASE      + 0x00198000)

#define HWIO_APCS_ALIAS1_APC_SECURE_ADDR                                                                   (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS1_APC_SECURE_RMSK                                                                          0xf
#define HWIO_APCS_ALIAS1_APC_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_SECURE_ADDR, HWIO_APCS_ALIAS1_APC_SECURE_RMSK)
#define HWIO_APCS_ALIAS1_APC_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS1_APC_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_APC_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS1_APC_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_APC_SECURE_ADDR,m,v,HWIO_APCS_ALIAS1_APC_SECURE_IN)
#define HWIO_APCS_ALIAS1_APC_SECURE_VOTE_BMSK                                                                     0x8
#define HWIO_APCS_ALIAS1_APC_SECURE_VOTE_SHFT                                                                     0x3
#define HWIO_APCS_ALIAS1_APC_SECURE_TST_BMSK                                                                      0x4
#define HWIO_APCS_ALIAS1_APC_SECURE_TST_SHFT                                                                      0x2
#define HWIO_APCS_ALIAS1_APC_SECURE_CLK_CTL_BMSK                                                                  0x2
#define HWIO_APCS_ALIAS1_APC_SECURE_CLK_CTL_SHFT                                                                  0x1
#define HWIO_APCS_ALIAS1_APC_SECURE_SLP_CTL_BMSK                                                                  0x1
#define HWIO_APCS_ALIAS1_APC_SECURE_SLP_CTL_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_ADDR                                                                  (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_RMSK                                                                  0x200046ff
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                                  0x20000000
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                                        0x1d
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                                         0x4000
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                                            0xe
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                                      0x400
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                                        0xa
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                                        0x200
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                                          0x9
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                                           0x80
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                                            0x7
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_GATE_CLK_BMSK                                                               0x40
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_GATE_CLK_SHFT                                                                0x6
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_COREPOR_RST_BMSK                                                            0x20
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_COREPOR_RST_SHFT                                                             0x5
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_RST_BMSK                                                               0x10
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_RST_SHFT                                                                0x4
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                                             0x8
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                                             0x3
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_L1_RST_DIS_BMSK                                                              0x4
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_L1_RST_DIS_SHFT                                                              0x2
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                                          0x2
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                                          0x1
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CLAMP_BMSK                                                                   0x1
#define HWIO_APCS_ALIAS1_CPU_PWR_CTL_CLAMP_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS1_APC_A53_CFG_STS_ADDR                                                              (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000040)
#define HWIO_APCS_ALIAS1_APC_A53_CFG_STS_RMSK                                                              0x10000000
#define HWIO_APCS_ALIAS1_APC_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS1_APC_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS1_APC_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_APC_A53_CFG_STS_SMPNAMP_BMSK                                                      0x10000000
#define HWIO_APCS_ALIAS1_APC_A53_CFG_STS_SMPNAMP_SHFT                                                            0x1c

#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_ADDR                                                               (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_RMSK                                                                0xb1f37ff
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_PWR_STATUS_ADDR, HWIO_APCS_ALIAS1_APC_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_SAW_SLP_ACK_BMSK                                                    0x8000000
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_SAW_SLP_ACK_SHFT                                                         0x1b
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_NO_PWR_DWN_BMSK                                                0x2000000
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_NO_PWR_DWN_SHFT                                                     0x19
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_PWRUP_REQ_BMSK                                                 0x1000000
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_PWRUP_REQ_SHFT                                                      0x18
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_HW_EVENT_BMSK                                                        0x1f0000
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_HW_EVENT_SHFT                                                            0x10
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_RET_SLP_ACK_BMSK                                                       0x2000
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_RET_SLP_ACK_SHFT                                                          0xd
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_RET_SLP_REQ_BMSK                                                       0x1000
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_RET_SLP_REQ_SHFT                                                          0xc
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_TRGTD_DBG_RST_BMSK                                                      0x400
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_TRGTD_DBG_RST_SHFT                                                        0xa
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_RST_BMSK                                                           0x200
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_RST_SHFT                                                             0x9
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_COREPOR_RST_BMSK                                                        0x100
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_COREPOR_RST_SHFT                                                          0x8
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_HS_STS_BMSK                                                              0x80
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_HS_STS_SHFT                                                               0x7
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_MEM_HS_STS_BMSK                                                     0x40
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_MEM_HS_STS_SHFT                                                      0x6
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CLAMP_BMSK                                                               0x20
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CLAMP_SHFT                                                                0x5
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_FRC_CLK_OFF_BMSK                                                         0x10
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_FRC_CLK_OFF_SHFT                                                          0x4
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_AHB_CLK_BMSK                                                              0x8
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_AHB_CLK_SHFT                                                              0x3
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_REF_CLK_BMSK                                                              0x4
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_REF_CLK_SHFT                                                              0x2
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_AUX_CLK_BMSK                                                         0x2
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_AUX_CLK_SHFT                                                         0x1
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_PLL_CLK_BMSK                                                         0x1
#define HWIO_APCS_ALIAS1_APC_PWR_STATUS_CORE_PLL_CLK_SHFT                                                         0x0

#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_ADDR                                                             (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_EN_BMSK                                                                 0x4
#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_EN_SHFT                                                                 0x2
#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_SEL_BMSK                                                                0x3
#define HWIO_APCS_ALIAS1_APC_TEST_BUS_SEL_SEL_SHFT                                                                0x0

#define HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_ADDR                                                            (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_RMSK                                                                   0x1
#define HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_ADDR, HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_RMSK)
#define HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_ADDR, m)
#define HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_ADDR,v)
#define HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_ADDR,m,v,HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_IN)
#define HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_RST_BMSK                                                               0x1
#define HWIO_APCS_ALIAS1_CPU_TRGTD_DBG_RST_RST_SHFT                                                               0x0

#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_ADDR                                                             (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_RMSK                                                             0xff000001
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_HS_CNT_BMSK                                                      0xff000000
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_HS_CNT_SHFT                                                            0x18
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_HS_EN_BMSK                                                              0x1
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_CTL_HS_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_ADDR                                                          (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_RMSK                                                             0x10003
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_IDLE_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_IDLE_SHFT                                                           0x10
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_EN_REST_BMSK                                                         0x2
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_EN_REST_SHFT                                                         0x1
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                                          0x1
#define HWIO_APCS_ALIAS1_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                                          0x0

#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_ADDR                                                            (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000044)
#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_RMSK                                                               0x101ff
#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_ADDR, HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_RMSK)
#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_IN)
#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_BMSK                                             0x10000
#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_SHFT                                                0x10
#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_AUX_STARTADDR_BMSK                                                   0x1ff
#define HWIO_APCS_ALIAS1_SPM_AUX_STARTADDR_AUX_STARTADDR_SHFT                                                     0x0

#define HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_ADDR                                                               (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_RMSK                                                                    0x3ff
#define HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_ADDR, HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_RMSK)
#define HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_ADDR, m)
#define HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_ADDR,v)
#define HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_ADDR,m,v,HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_IN)
#define HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_SLP_CNT_BMSK                                                            0x3ff
#define HWIO_APCS_ALIAS1_APC_L1_SLP_CNT_SLP_CNT_SHFT                                                              0x0

#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_ADDR                                                                  (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000050)
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_RMSK                                                                  0xc0007fff
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS1_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                                                0x80000000
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                                      0x1f
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                                                 0x40000000
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                                       0x1e
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                                           0x7fff
#define HWIO_APCS_ALIAS1_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_ADDR                                                                (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_RMSK                                                                0xc0007fff
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                                              0x80000000
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                                    0x1f
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                                               0x40000000
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                                     0x1e
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                                         0x7fff
#define HWIO_APCS_ALIAS1_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                                            0x0

#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_ADDR                                                          (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_RMSK                                                          0xc0007fff
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK                                0x80000000
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                                      0x1f
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK                                 0x40000000
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                                       0x1e
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                                           0x7fff
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                                              0x0

#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_ADDR                                                           (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x0000005c)
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_RMSK                                                           0xc0007fff
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK                                  0x80000000
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                                        0x1f
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK                                   0x40000000
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                                         0x1e
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                                             0x7fff
#define HWIO_APCS_ALIAS1_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                                                0x0

#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_ADDR                                                           (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_RMSK                                                           0x80000001
#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_ADDR, HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_RMSK)
#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_ADDR, m)
#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_ADDR,v)
#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_ADDR,m,v,HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_IN)
#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_BMSK                                       0x80000000
#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_SHFT                                             0x1f
#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_BMSK                                              0x1
#define HWIO_APCS_ALIAS1_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_SHFT                                              0x0

#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_ADDR                                                             (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                                    0x4
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                                    0x2
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_IGNORE_BMSK                                                             0x2
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_IGNORE_SHFT                                                             0x1
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                                         0x1
#define HWIO_APCS_ALIAS1_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_ADDR                                                           (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000070)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_RMSK                                                                0x1ff
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_EN_BMSK                                                             0x1ff
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EN_EN_SHFT                                                               0x0

#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_ADDR                                                              (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000074)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_RMSK                                                                   0x1ff
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                                         0x1ff
#define HWIO_APCS_ALIAS1_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                                           0x0

#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_ADDR                                                         (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_RMSK                                                            0x1ffff
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                                         0x1ffff
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                                             0x0

#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_ADDR                                                            (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x0000007c)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_RMSK                                                               0x1ffff
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                                                   0x1ffff
#define HWIO_APCS_ALIAS1_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                                       0x0

#define HWIO_APCS_ALIAS1_SPM_EVENT_STS_ADDR                                                                (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000080)
#define HWIO_APCS_ALIAS1_SPM_EVENT_STS_RMSK                                                                     0x1ff
#define HWIO_APCS_ALIAS1_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS1_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS1_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_EVENT_STS_EVENT_VAL_BMSK                                                           0x1ff
#define HWIO_APCS_ALIAS1_SPM_EVENT_STS_EVENT_VAL_SHFT                                                             0x0

#define HWIO_APCS_ALIAS1_SPM_PWR_CTL_STS_ADDR                                                              (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000084)
#define HWIO_APCS_ALIAS1_SPM_PWR_CTL_STS_RMSK                                                                 0x1ffff
#define HWIO_APCS_ALIAS1_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS1_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS1_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                                     0x1ffff
#define HWIO_APCS_ALIAS1_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_ADDR                                                    (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_RMSK                                                           0xf
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                       0x8
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                       0x3
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_BMSK                               0x4
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_SHFT                               0x2
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_BMSK                              0x2
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_SHFT                              0x1
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_BMSK                              0x1
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_SHFT                              0x0

#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_ADDR                                                        (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_RMSK                                                        0xffffffff
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_BMSK                            0xffffffff
#define HWIO_APCS_ALIAS1_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_SHFT                                   0x0

#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR                                                 (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000a0)
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK                                                        0xf
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                 0x8
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                 0x3
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_BMSK                         0x4
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_SHFT                         0x2
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_BMSK                        0x2
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_SHFT                        0x1
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_BMSK                        0x1
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_SHFT                        0x0

#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_ADDR                                                     (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x0000009c)
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_BMSK                      0xffffffff
#define HWIO_APCS_ALIAS1_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_SHFT                             0x0

#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR                                                   (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000a8)
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK                                                          0xf
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                     0x8
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                     0x3
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_BMSK                             0x4
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_SHFT                             0x2
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_BMSK                            0x2
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_SHFT                            0x1
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_BMSK                            0x1
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_SHFT                            0x0

#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_ADDR                                                       (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000a4)
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_RMSK                                                       0xffffffff
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_BMSK                          0xffffffff
#define HWIO_APCS_ALIAS1_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_SHFT                                 0x0

#define HWIO_APCS_ALIAS1_SPARE_ADDR                                                                        (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000ff8)
#define HWIO_APCS_ALIAS1_SPARE_RMSK                                                                              0xff
#define HWIO_APCS_ALIAS1_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPARE_ADDR, HWIO_APCS_ALIAS1_SPARE_RMSK)
#define HWIO_APCS_ALIAS1_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS1_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS1_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPARE_ADDR,m,v,HWIO_APCS_ALIAS1_SPARE_IN)
#define HWIO_APCS_ALIAS1_SPARE_SPARE_BMSK                                                                        0xff
#define HWIO_APCS_ALIAS1_SPARE_SPARE_SHFT                                                                         0x0

#define HWIO_APCS_ALIAS1_SPARE_TZ_ADDR                                                                     (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_ALIAS1_SPARE_TZ_RMSK                                                                           0xff
#define HWIO_APCS_ALIAS1_SPARE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_SPARE_TZ_ADDR, HWIO_APCS_ALIAS1_SPARE_TZ_RMSK)
#define HWIO_APCS_ALIAS1_SPARE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_SPARE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS1_SPARE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_SPARE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS1_SPARE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_SPARE_TZ_ADDR,m,v,HWIO_APCS_ALIAS1_SPARE_TZ_IN)
#define HWIO_APCS_ALIAS1_SPARE_TZ_SPARE_BMSK                                                                     0xff
#define HWIO_APCS_ALIAS1_SPARE_TZ_SPARE_SHFT                                                                      0x0

#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_ADDR                                                     (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_RMSK                                                          0x7ff
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_ADDR, HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_RMSK)
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_ADDR, m)
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_ADDR,v)
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_ADDR,m,v,HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_IN)
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_BMSK                                  0x7f8
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_SHFT                                    0x3
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_BMSK                                      0x4
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_SHFT                                      0x2
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_BMSK                                0x2
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_SHFT                                0x1
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_BMSK                                0x1
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_SHFT                                0x0

#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_STATUS_ADDR                                                  (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x00000094)
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_STATUS_RMSK                                                  0xffffffff
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_STATUS_ADDR, HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_BMSK                           0xffffffff
#define HWIO_APCS_ALIAS1_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_SHFT                                  0x0

#define HWIO_APCS_ALIAS1_MAS_CFG_ADDR                                                                      (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000c0)
#define HWIO_APCS_ALIAS1_MAS_CFG_RMSK                                                                         0x10707
#define HWIO_APCS_ALIAS1_MAS_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MAS_CFG_ADDR, HWIO_APCS_ALIAS1_MAS_CFG_RMSK)
#define HWIO_APCS_ALIAS1_MAS_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MAS_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_MAS_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MAS_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_MAS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MAS_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_MAS_CFG_IN)
#define HWIO_APCS_ALIAS1_MAS_CFG_MAS_MEM_CLAMP_EN_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS1_MAS_CFG_MAS_MEM_CLAMP_EN_SHFT                                                           0x10
#define HWIO_APCS_ALIAS1_MAS_CFG_MAS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS1_MAS_CFG_MAS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS1_MAS_CFG_MAS_APC_EN_REST_BMSK                                                             0x7
#define HWIO_APCS_ALIAS1_MAS_CFG_MAS_APC_EN_REST_SHFT                                                             0x0

#define HWIO_APCS_ALIAS1_APM_TILE_CFG_ADDR                                                                 (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000c4)
#define HWIO_APCS_ALIAS1_APM_TILE_CFG_RMSK                                                                      0x1ff
#define HWIO_APCS_ALIAS1_APM_TILE_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_APM_TILE_CFG_ADDR, HWIO_APCS_ALIAS1_APM_TILE_CFG_RMSK)
#define HWIO_APCS_ALIAS1_APM_TILE_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_APM_TILE_CFG_ADDR, m)
#define HWIO_APCS_ALIAS1_APM_TILE_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_APM_TILE_CFG_ADDR,v)
#define HWIO_APCS_ALIAS1_APM_TILE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_APM_TILE_CFG_ADDR,m,v,HWIO_APCS_ALIAS1_APM_TILE_CFG_IN)
#define HWIO_APCS_ALIAS1_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_BMSK                                               0x100
#define HWIO_APCS_ALIAS1_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_SHFT                                                 0x8
#define HWIO_APCS_ALIAS1_APM_TILE_CFG_APM_TILE_APMCONFIG_BMSK                                                    0xff
#define HWIO_APCS_ALIAS1_APM_TILE_CFG_APM_TILE_APMCONFIG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS1_MAS_STS_ADDR                                                                      (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000c8)
#define HWIO_APCS_ALIAS1_MAS_STS_RMSK                                                                        0x3ff373
#define HWIO_APCS_ALIAS1_MAS_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MAS_STS_ADDR, HWIO_APCS_ALIAS1_MAS_STS_RMSK)
#define HWIO_APCS_ALIAS1_MAS_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MAS_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_MAS_STS_TMR_CNT_BMSK                                                                0x3f0000
#define HWIO_APCS_ALIAS1_MAS_STS_TMR_CNT_SHFT                                                                    0x10
#define HWIO_APCS_ALIAS1_MAS_STS_STEADY_SLEEP_BMSK                                                             0x8000
#define HWIO_APCS_ALIAS1_MAS_STS_STEADY_SLEEP_SHFT                                                                0xf
#define HWIO_APCS_ALIAS1_MAS_STS_STEADY_ACTIVE_BMSK                                                            0x4000
#define HWIO_APCS_ALIAS1_MAS_STS_STEADY_ACTIVE_SHFT                                                               0xe
#define HWIO_APCS_ALIAS1_MAS_STS_SPM_RET_BMSK                                                                  0x2000
#define HWIO_APCS_ALIAS1_MAS_STS_SPM_RET_SHFT                                                                     0xd
#define HWIO_APCS_ALIAS1_MAS_STS_SPM_PC_BMSK                                                                   0x1000
#define HWIO_APCS_ALIAS1_MAS_STS_SPM_PC_SHFT                                                                      0xc
#define HWIO_APCS_ALIAS1_MAS_STS_NEW_MODE_BMSK                                                                  0x300
#define HWIO_APCS_ALIAS1_MAS_STS_NEW_MODE_SHFT                                                                    0x8
#define HWIO_APCS_ALIAS1_MAS_STS_FSM_STATE_BMSK                                                                  0x70
#define HWIO_APCS_ALIAS1_MAS_STS_FSM_STATE_SHFT                                                                   0x4
#define HWIO_APCS_ALIAS1_MAS_STS_CURR_MODE_BMSK                                                                   0x3
#define HWIO_APCS_ALIAS1_MAS_STS_CURR_MODE_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS1_PWR_MUX_STS_ADDR                                                                  (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000cc)
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_RMSK                                                                     0x70773
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_PWR_MUX_STS_ADDR, HWIO_APCS_ALIAS1_PWR_MUX_STS_RMSK)
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_PWR_MUX_STS_ADDR, m)
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_CLAMP_BMSK                                                               0x40000
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_CLAMP_SHFT                                                                  0x12
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_TGL_SEL_BMSK                                                             0x20000
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_TGL_SEL_SHFT                                                                0x11
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_MEM_RET_BMSK                                                             0x10000
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_MEM_RET_SHFT                                                                0x10
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_APCC_EN_REST_BMSK                                                           0x70
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_APCC_EN_REST_SHFT                                                            0x4
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_EN_FEW_BMSK                                                                  0x3
#define HWIO_APCS_ALIAS1_PWR_MUX_STS_EN_FEW_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_ADDR                                                                 (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000d0)
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_RMSK                                                                   0x811377
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_MAS_OVERRIDE_ADDR, HWIO_APCS_ALIAS1_MAS_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_MAS_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_MAS_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_MAS_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS1_MAS_OVERRIDE_IN)
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_BMSK                                          0x800000
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_SHFT                                              0x17
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_BMSK                                 0x10000
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_SHFT                                    0x10
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_BMSK                             0x1000
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_SHFT                                0xc
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_BMSK                                       0x300
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_SHFT                                         0x8
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_BMSK                                     0x70
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_SHFT                                      0x4
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_BMSK                                     0x7
#define HWIO_APCS_ALIAS1_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_SHFT                                     0x0

#define HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_ADDR                                                          (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000b0)
#define HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_RMSK                                                          0xffffffff
#define HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_ADDR, HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_RMSK)
#define HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_ADDR, m)
#define HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_ADDR,v)
#define HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_ADDR,m,v,HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_IN)
#define HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_BMSK                                0xffffffff
#define HWIO_APCS_ALIAS1_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_SHFT                                       0x0

#define HWIO_APCS_ALIAS1_CORE_HANG_VALUE_ADDR                                                              (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000b4)
#define HWIO_APCS_ALIAS1_CORE_HANG_VALUE_RMSK                                                              0xffffffff
#define HWIO_APCS_ALIAS1_CORE_HANG_VALUE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HANG_VALUE_ADDR, HWIO_APCS_ALIAS1_CORE_HANG_VALUE_RMSK)
#define HWIO_APCS_ALIAS1_CORE_HANG_VALUE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HANG_VALUE_ADDR, m)
#define HWIO_APCS_ALIAS1_CORE_HANG_VALUE_VALUE_WHEN_HUNG_BMSK                                              0xffffffff
#define HWIO_APCS_ALIAS1_CORE_HANG_VALUE_VALUE_WHEN_HUNG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_ADDR                                                             (APCS_ALIAS1_APSS_ACS_REG_BASE      + 0x000000b8)
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_RMSK                                                                  0x1f7
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_ADDR, HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_RMSK)
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_ADDR, m)
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_ADDR,v)
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_ADDR,m,v,HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_IN)
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_PMUEVENT_SEL_BMSK                                                     0x1f0
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_PMUEVENT_SEL_SHFT                                                       0x4
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_BMSK                                           0x4
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_SHFT                                           0x2
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_BMSK                                               0x2
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_SHFT                                               0x1
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_CORE_HANG_STATUS_BMSK                                                   0x1
#define HWIO_APCS_ALIAS1_CORE_HANG_CONFIG_CORE_HANG_STATUS_SHFT                                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS2_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS2_APSS_ACS_REG_BASE                                                                      (A53SS_BASE      + 0x001a8000)

#define HWIO_APCS_ALIAS2_APC_SECURE_ADDR                                                                   (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS2_APC_SECURE_RMSK                                                                          0xf
#define HWIO_APCS_ALIAS2_APC_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_SECURE_ADDR, HWIO_APCS_ALIAS2_APC_SECURE_RMSK)
#define HWIO_APCS_ALIAS2_APC_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS2_APC_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_APC_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS2_APC_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_APC_SECURE_ADDR,m,v,HWIO_APCS_ALIAS2_APC_SECURE_IN)
#define HWIO_APCS_ALIAS2_APC_SECURE_VOTE_BMSK                                                                     0x8
#define HWIO_APCS_ALIAS2_APC_SECURE_VOTE_SHFT                                                                     0x3
#define HWIO_APCS_ALIAS2_APC_SECURE_TST_BMSK                                                                      0x4
#define HWIO_APCS_ALIAS2_APC_SECURE_TST_SHFT                                                                      0x2
#define HWIO_APCS_ALIAS2_APC_SECURE_CLK_CTL_BMSK                                                                  0x2
#define HWIO_APCS_ALIAS2_APC_SECURE_CLK_CTL_SHFT                                                                  0x1
#define HWIO_APCS_ALIAS2_APC_SECURE_SLP_CTL_BMSK                                                                  0x1
#define HWIO_APCS_ALIAS2_APC_SECURE_SLP_CTL_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_ADDR                                                                  (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_RMSK                                                                  0x200046ff
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS2_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS2_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                                  0x20000000
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                                        0x1d
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                                         0x4000
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                                            0xe
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                                      0x400
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                                        0xa
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                                        0x200
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                                          0x9
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                                           0x80
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                                            0x7
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_GATE_CLK_BMSK                                                               0x40
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_GATE_CLK_SHFT                                                                0x6
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_COREPOR_RST_BMSK                                                            0x20
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_COREPOR_RST_SHFT                                                             0x5
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_RST_BMSK                                                               0x10
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_RST_SHFT                                                                0x4
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                                             0x8
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                                             0x3
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_L1_RST_DIS_BMSK                                                              0x4
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_L1_RST_DIS_SHFT                                                              0x2
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                                          0x2
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                                          0x1
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CLAMP_BMSK                                                                   0x1
#define HWIO_APCS_ALIAS2_CPU_PWR_CTL_CLAMP_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS2_APC_A53_CFG_STS_ADDR                                                              (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000040)
#define HWIO_APCS_ALIAS2_APC_A53_CFG_STS_RMSK                                                              0x10000000
#define HWIO_APCS_ALIAS2_APC_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS2_APC_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS2_APC_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS2_APC_A53_CFG_STS_SMPNAMP_BMSK                                                      0x10000000
#define HWIO_APCS_ALIAS2_APC_A53_CFG_STS_SMPNAMP_SHFT                                                            0x1c

#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_ADDR                                                               (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_RMSK                                                                0xb1f37ff
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_PWR_STATUS_ADDR, HWIO_APCS_ALIAS2_APC_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_SAW_SLP_ACK_BMSK                                                    0x8000000
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_SAW_SLP_ACK_SHFT                                                         0x1b
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_NO_PWR_DWN_BMSK                                                0x2000000
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_NO_PWR_DWN_SHFT                                                     0x19
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_PWRUP_REQ_BMSK                                                 0x1000000
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_PWRUP_REQ_SHFT                                                      0x18
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_HW_EVENT_BMSK                                                        0x1f0000
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_HW_EVENT_SHFT                                                            0x10
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_RET_SLP_ACK_BMSK                                                       0x2000
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_RET_SLP_ACK_SHFT                                                          0xd
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_RET_SLP_REQ_BMSK                                                       0x1000
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_RET_SLP_REQ_SHFT                                                          0xc
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_TRGTD_DBG_RST_BMSK                                                      0x400
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_TRGTD_DBG_RST_SHFT                                                        0xa
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_RST_BMSK                                                           0x200
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_RST_SHFT                                                             0x9
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_COREPOR_RST_BMSK                                                        0x100
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_COREPOR_RST_SHFT                                                          0x8
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_HS_STS_BMSK                                                              0x80
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_HS_STS_SHFT                                                               0x7
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_MEM_HS_STS_BMSK                                                     0x40
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_MEM_HS_STS_SHFT                                                      0x6
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CLAMP_BMSK                                                               0x20
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CLAMP_SHFT                                                                0x5
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_FRC_CLK_OFF_BMSK                                                         0x10
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_FRC_CLK_OFF_SHFT                                                          0x4
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_AHB_CLK_BMSK                                                              0x8
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_AHB_CLK_SHFT                                                              0x3
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_REF_CLK_BMSK                                                              0x4
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_REF_CLK_SHFT                                                              0x2
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_AUX_CLK_BMSK                                                         0x2
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_AUX_CLK_SHFT                                                         0x1
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_PLL_CLK_BMSK                                                         0x1
#define HWIO_APCS_ALIAS2_APC_PWR_STATUS_CORE_PLL_CLK_SHFT                                                         0x0

#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_ADDR                                                             (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_EN_BMSK                                                                 0x4
#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_EN_SHFT                                                                 0x2
#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_SEL_BMSK                                                                0x3
#define HWIO_APCS_ALIAS2_APC_TEST_BUS_SEL_SEL_SHFT                                                                0x0

#define HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_ADDR                                                            (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_RMSK                                                                   0x1
#define HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_ADDR, HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_RMSK)
#define HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_ADDR, m)
#define HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_ADDR,v)
#define HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_ADDR,m,v,HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_IN)
#define HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_RST_BMSK                                                               0x1
#define HWIO_APCS_ALIAS2_CPU_TRGTD_DBG_RST_RST_SHFT                                                               0x0

#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_ADDR                                                             (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_RMSK                                                             0xff000001
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_HS_CNT_BMSK                                                      0xff000000
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_HS_CNT_SHFT                                                            0x18
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_HS_EN_BMSK                                                              0x1
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_CTL_HS_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_ADDR                                                          (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_RMSK                                                             0x10003
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_IDLE_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_IDLE_SHFT                                                           0x10
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_EN_REST_BMSK                                                         0x2
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_EN_REST_SHFT                                                         0x1
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                                          0x1
#define HWIO_APCS_ALIAS2_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                                          0x0

#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_ADDR                                                            (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000044)
#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_RMSK                                                               0x101ff
#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_ADDR, HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_RMSK)
#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_IN)
#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_BMSK                                             0x10000
#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_SHFT                                                0x10
#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_AUX_STARTADDR_BMSK                                                   0x1ff
#define HWIO_APCS_ALIAS2_SPM_AUX_STARTADDR_AUX_STARTADDR_SHFT                                                     0x0

#define HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_ADDR                                                               (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_RMSK                                                                    0x3ff
#define HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_ADDR, HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_RMSK)
#define HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_ADDR, m)
#define HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_ADDR,v)
#define HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_ADDR,m,v,HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_IN)
#define HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_SLP_CNT_BMSK                                                            0x3ff
#define HWIO_APCS_ALIAS2_APC_L1_SLP_CNT_SLP_CNT_SHFT                                                              0x0

#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_ADDR                                                                  (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000050)
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_RMSK                                                                  0xc0007fff
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS2_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                                                0x80000000
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                                      0x1f
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                                                 0x40000000
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                                       0x1e
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                                           0x7fff
#define HWIO_APCS_ALIAS2_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_ADDR                                                                (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_RMSK                                                                0xc0007fff
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                                              0x80000000
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                                    0x1f
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                                               0x40000000
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                                     0x1e
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                                         0x7fff
#define HWIO_APCS_ALIAS2_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                                            0x0

#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_ADDR                                                          (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_RMSK                                                          0xc0007fff
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK                                0x80000000
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                                      0x1f
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK                                 0x40000000
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                                       0x1e
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                                           0x7fff
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                                              0x0

#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_ADDR                                                           (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x0000005c)
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_RMSK                                                           0xc0007fff
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK                                  0x80000000
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                                        0x1f
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK                                   0x40000000
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                                         0x1e
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                                             0x7fff
#define HWIO_APCS_ALIAS2_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                                                0x0

#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_ADDR                                                           (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_RMSK                                                           0x80000001
#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_ADDR, HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_RMSK)
#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_ADDR, m)
#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_ADDR,v)
#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_ADDR,m,v,HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_IN)
#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_BMSK                                       0x80000000
#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_SHFT                                             0x1f
#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_BMSK                                              0x1
#define HWIO_APCS_ALIAS2_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_SHFT                                              0x0

#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_ADDR                                                             (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                                    0x4
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                                    0x2
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_IGNORE_BMSK                                                             0x2
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_IGNORE_SHFT                                                             0x1
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                                         0x1
#define HWIO_APCS_ALIAS2_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_ADDR                                                           (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000070)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_RMSK                                                                0x1ff
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_EN_BMSK                                                             0x1ff
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EN_EN_SHFT                                                               0x0

#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_ADDR                                                              (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000074)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_RMSK                                                                   0x1ff
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                                         0x1ff
#define HWIO_APCS_ALIAS2_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                                           0x0

#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_ADDR                                                         (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_RMSK                                                            0x1ffff
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                                         0x1ffff
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                                             0x0

#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_ADDR                                                            (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x0000007c)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_RMSK                                                               0x1ffff
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                                                   0x1ffff
#define HWIO_APCS_ALIAS2_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                                       0x0

#define HWIO_APCS_ALIAS2_SPM_EVENT_STS_ADDR                                                                (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000080)
#define HWIO_APCS_ALIAS2_SPM_EVENT_STS_RMSK                                                                     0x1ff
#define HWIO_APCS_ALIAS2_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS2_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS2_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_EVENT_STS_EVENT_VAL_BMSK                                                           0x1ff
#define HWIO_APCS_ALIAS2_SPM_EVENT_STS_EVENT_VAL_SHFT                                                             0x0

#define HWIO_APCS_ALIAS2_SPM_PWR_CTL_STS_ADDR                                                              (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000084)
#define HWIO_APCS_ALIAS2_SPM_PWR_CTL_STS_RMSK                                                                 0x1ffff
#define HWIO_APCS_ALIAS2_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS2_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS2_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                                     0x1ffff
#define HWIO_APCS_ALIAS2_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_ADDR                                                    (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_RMSK                                                           0xf
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                       0x8
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                       0x3
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_BMSK                               0x4
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_SHFT                               0x2
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_BMSK                              0x2
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_SHFT                              0x1
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_BMSK                              0x1
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_SHFT                              0x0

#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_ADDR                                                        (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_RMSK                                                        0xffffffff
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_BMSK                            0xffffffff
#define HWIO_APCS_ALIAS2_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_SHFT                                   0x0

#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR                                                 (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000a0)
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK                                                        0xf
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                 0x8
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                 0x3
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_BMSK                         0x4
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_SHFT                         0x2
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_BMSK                        0x2
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_SHFT                        0x1
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_BMSK                        0x1
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_SHFT                        0x0

#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_ADDR                                                     (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x0000009c)
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_BMSK                      0xffffffff
#define HWIO_APCS_ALIAS2_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_SHFT                             0x0

#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR                                                   (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000a8)
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK                                                          0xf
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                     0x8
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                     0x3
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_BMSK                             0x4
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_SHFT                             0x2
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_BMSK                            0x2
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_SHFT                            0x1
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_BMSK                            0x1
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_SHFT                            0x0

#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_ADDR                                                       (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000a4)
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_RMSK                                                       0xffffffff
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_BMSK                          0xffffffff
#define HWIO_APCS_ALIAS2_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_SHFT                                 0x0

#define HWIO_APCS_ALIAS2_SPARE_ADDR                                                                        (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000ff8)
#define HWIO_APCS_ALIAS2_SPARE_RMSK                                                                              0xff
#define HWIO_APCS_ALIAS2_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPARE_ADDR, HWIO_APCS_ALIAS2_SPARE_RMSK)
#define HWIO_APCS_ALIAS2_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS2_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS2_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPARE_ADDR,m,v,HWIO_APCS_ALIAS2_SPARE_IN)
#define HWIO_APCS_ALIAS2_SPARE_SPARE_BMSK                                                                        0xff
#define HWIO_APCS_ALIAS2_SPARE_SPARE_SHFT                                                                         0x0

#define HWIO_APCS_ALIAS2_SPARE_TZ_ADDR                                                                     (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_ALIAS2_SPARE_TZ_RMSK                                                                           0xff
#define HWIO_APCS_ALIAS2_SPARE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_SPARE_TZ_ADDR, HWIO_APCS_ALIAS2_SPARE_TZ_RMSK)
#define HWIO_APCS_ALIAS2_SPARE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_SPARE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS2_SPARE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_SPARE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS2_SPARE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_SPARE_TZ_ADDR,m,v,HWIO_APCS_ALIAS2_SPARE_TZ_IN)
#define HWIO_APCS_ALIAS2_SPARE_TZ_SPARE_BMSK                                                                     0xff
#define HWIO_APCS_ALIAS2_SPARE_TZ_SPARE_SHFT                                                                      0x0

#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_ADDR                                                     (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_RMSK                                                          0x7ff
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_ADDR, HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_RMSK)
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_ADDR, m)
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_ADDR,v)
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_ADDR,m,v,HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_IN)
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_BMSK                                  0x7f8
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_SHFT                                    0x3
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_BMSK                                      0x4
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_SHFT                                      0x2
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_BMSK                                0x2
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_SHFT                                0x1
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_BMSK                                0x1
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_SHFT                                0x0

#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_STATUS_ADDR                                                  (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x00000094)
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_STATUS_RMSK                                                  0xffffffff
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_STATUS_ADDR, HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_BMSK                           0xffffffff
#define HWIO_APCS_ALIAS2_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_SHFT                                  0x0

#define HWIO_APCS_ALIAS2_MAS_CFG_ADDR                                                                      (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000c0)
#define HWIO_APCS_ALIAS2_MAS_CFG_RMSK                                                                         0x10707
#define HWIO_APCS_ALIAS2_MAS_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_MAS_CFG_ADDR, HWIO_APCS_ALIAS2_MAS_CFG_RMSK)
#define HWIO_APCS_ALIAS2_MAS_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_MAS_CFG_ADDR, m)
#define HWIO_APCS_ALIAS2_MAS_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_MAS_CFG_ADDR,v)
#define HWIO_APCS_ALIAS2_MAS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_MAS_CFG_ADDR,m,v,HWIO_APCS_ALIAS2_MAS_CFG_IN)
#define HWIO_APCS_ALIAS2_MAS_CFG_MAS_MEM_CLAMP_EN_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS2_MAS_CFG_MAS_MEM_CLAMP_EN_SHFT                                                           0x10
#define HWIO_APCS_ALIAS2_MAS_CFG_MAS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS2_MAS_CFG_MAS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS2_MAS_CFG_MAS_APC_EN_REST_BMSK                                                             0x7
#define HWIO_APCS_ALIAS2_MAS_CFG_MAS_APC_EN_REST_SHFT                                                             0x0

#define HWIO_APCS_ALIAS2_APM_TILE_CFG_ADDR                                                                 (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000c4)
#define HWIO_APCS_ALIAS2_APM_TILE_CFG_RMSK                                                                      0x1ff
#define HWIO_APCS_ALIAS2_APM_TILE_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_APM_TILE_CFG_ADDR, HWIO_APCS_ALIAS2_APM_TILE_CFG_RMSK)
#define HWIO_APCS_ALIAS2_APM_TILE_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_APM_TILE_CFG_ADDR, m)
#define HWIO_APCS_ALIAS2_APM_TILE_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_APM_TILE_CFG_ADDR,v)
#define HWIO_APCS_ALIAS2_APM_TILE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_APM_TILE_CFG_ADDR,m,v,HWIO_APCS_ALIAS2_APM_TILE_CFG_IN)
#define HWIO_APCS_ALIAS2_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_BMSK                                               0x100
#define HWIO_APCS_ALIAS2_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_SHFT                                                 0x8
#define HWIO_APCS_ALIAS2_APM_TILE_CFG_APM_TILE_APMCONFIG_BMSK                                                    0xff
#define HWIO_APCS_ALIAS2_APM_TILE_CFG_APM_TILE_APMCONFIG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS2_MAS_STS_ADDR                                                                      (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000c8)
#define HWIO_APCS_ALIAS2_MAS_STS_RMSK                                                                        0x3ff373
#define HWIO_APCS_ALIAS2_MAS_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_MAS_STS_ADDR, HWIO_APCS_ALIAS2_MAS_STS_RMSK)
#define HWIO_APCS_ALIAS2_MAS_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_MAS_STS_ADDR, m)
#define HWIO_APCS_ALIAS2_MAS_STS_TMR_CNT_BMSK                                                                0x3f0000
#define HWIO_APCS_ALIAS2_MAS_STS_TMR_CNT_SHFT                                                                    0x10
#define HWIO_APCS_ALIAS2_MAS_STS_STEADY_SLEEP_BMSK                                                             0x8000
#define HWIO_APCS_ALIAS2_MAS_STS_STEADY_SLEEP_SHFT                                                                0xf
#define HWIO_APCS_ALIAS2_MAS_STS_STEADY_ACTIVE_BMSK                                                            0x4000
#define HWIO_APCS_ALIAS2_MAS_STS_STEADY_ACTIVE_SHFT                                                               0xe
#define HWIO_APCS_ALIAS2_MAS_STS_SPM_RET_BMSK                                                                  0x2000
#define HWIO_APCS_ALIAS2_MAS_STS_SPM_RET_SHFT                                                                     0xd
#define HWIO_APCS_ALIAS2_MAS_STS_SPM_PC_BMSK                                                                   0x1000
#define HWIO_APCS_ALIAS2_MAS_STS_SPM_PC_SHFT                                                                      0xc
#define HWIO_APCS_ALIAS2_MAS_STS_NEW_MODE_BMSK                                                                  0x300
#define HWIO_APCS_ALIAS2_MAS_STS_NEW_MODE_SHFT                                                                    0x8
#define HWIO_APCS_ALIAS2_MAS_STS_FSM_STATE_BMSK                                                                  0x70
#define HWIO_APCS_ALIAS2_MAS_STS_FSM_STATE_SHFT                                                                   0x4
#define HWIO_APCS_ALIAS2_MAS_STS_CURR_MODE_BMSK                                                                   0x3
#define HWIO_APCS_ALIAS2_MAS_STS_CURR_MODE_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS2_PWR_MUX_STS_ADDR                                                                  (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000cc)
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_RMSK                                                                     0x70773
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_PWR_MUX_STS_ADDR, HWIO_APCS_ALIAS2_PWR_MUX_STS_RMSK)
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_PWR_MUX_STS_ADDR, m)
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_CLAMP_BMSK                                                               0x40000
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_CLAMP_SHFT                                                                  0x12
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_TGL_SEL_BMSK                                                             0x20000
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_TGL_SEL_SHFT                                                                0x11
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_MEM_RET_BMSK                                                             0x10000
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_MEM_RET_SHFT                                                                0x10
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_APCC_EN_REST_BMSK                                                           0x70
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_APCC_EN_REST_SHFT                                                            0x4
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_EN_FEW_BMSK                                                                  0x3
#define HWIO_APCS_ALIAS2_PWR_MUX_STS_EN_FEW_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_ADDR                                                                 (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000d0)
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_RMSK                                                                   0x811377
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_MAS_OVERRIDE_ADDR, HWIO_APCS_ALIAS2_MAS_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_MAS_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_MAS_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_MAS_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS2_MAS_OVERRIDE_IN)
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_BMSK                                          0x800000
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_SHFT                                              0x17
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_BMSK                                 0x10000
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_SHFT                                    0x10
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_BMSK                             0x1000
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_SHFT                                0xc
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_BMSK                                       0x300
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_SHFT                                         0x8
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_BMSK                                     0x70
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_SHFT                                      0x4
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_BMSK                                     0x7
#define HWIO_APCS_ALIAS2_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_SHFT                                     0x0

#define HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_ADDR                                                          (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000b0)
#define HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_RMSK                                                          0xffffffff
#define HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_ADDR, HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_RMSK)
#define HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_ADDR, m)
#define HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_ADDR,v)
#define HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_ADDR,m,v,HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_IN)
#define HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_BMSK                                0xffffffff
#define HWIO_APCS_ALIAS2_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_SHFT                                       0x0

#define HWIO_APCS_ALIAS2_CORE_HANG_VALUE_ADDR                                                              (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000b4)
#define HWIO_APCS_ALIAS2_CORE_HANG_VALUE_RMSK                                                              0xffffffff
#define HWIO_APCS_ALIAS2_CORE_HANG_VALUE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_CORE_HANG_VALUE_ADDR, HWIO_APCS_ALIAS2_CORE_HANG_VALUE_RMSK)
#define HWIO_APCS_ALIAS2_CORE_HANG_VALUE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_CORE_HANG_VALUE_ADDR, m)
#define HWIO_APCS_ALIAS2_CORE_HANG_VALUE_VALUE_WHEN_HUNG_BMSK                                              0xffffffff
#define HWIO_APCS_ALIAS2_CORE_HANG_VALUE_VALUE_WHEN_HUNG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_ADDR                                                             (APCS_ALIAS2_APSS_ACS_REG_BASE      + 0x000000b8)
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_RMSK                                                                  0x1f7
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_ADDR, HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_RMSK)
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_ADDR, m)
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_ADDR,v)
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_ADDR,m,v,HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_IN)
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_PMUEVENT_SEL_BMSK                                                     0x1f0
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_PMUEVENT_SEL_SHFT                                                       0x4
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_BMSK                                           0x4
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_SHFT                                           0x2
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_BMSK                                               0x2
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_SHFT                                               0x1
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_CORE_HANG_STATUS_BMSK                                                   0x1
#define HWIO_APCS_ALIAS2_CORE_HANG_CONFIG_CORE_HANG_STATUS_SHFT                                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS3_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS3_APSS_ACS_REG_BASE                                                                      (A53SS_BASE      + 0x001b8000)

#define HWIO_APCS_ALIAS3_APC_SECURE_ADDR                                                                   (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS3_APC_SECURE_RMSK                                                                          0xf
#define HWIO_APCS_ALIAS3_APC_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_SECURE_ADDR, HWIO_APCS_ALIAS3_APC_SECURE_RMSK)
#define HWIO_APCS_ALIAS3_APC_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS3_APC_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_APC_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS3_APC_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_APC_SECURE_ADDR,m,v,HWIO_APCS_ALIAS3_APC_SECURE_IN)
#define HWIO_APCS_ALIAS3_APC_SECURE_VOTE_BMSK                                                                     0x8
#define HWIO_APCS_ALIAS3_APC_SECURE_VOTE_SHFT                                                                     0x3
#define HWIO_APCS_ALIAS3_APC_SECURE_TST_BMSK                                                                      0x4
#define HWIO_APCS_ALIAS3_APC_SECURE_TST_SHFT                                                                      0x2
#define HWIO_APCS_ALIAS3_APC_SECURE_CLK_CTL_BMSK                                                                  0x2
#define HWIO_APCS_ALIAS3_APC_SECURE_CLK_CTL_SHFT                                                                  0x1
#define HWIO_APCS_ALIAS3_APC_SECURE_SLP_CTL_BMSK                                                                  0x1
#define HWIO_APCS_ALIAS3_APC_SECURE_SLP_CTL_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_ADDR                                                                  (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_RMSK                                                                  0x200046ff
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS3_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS3_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                                  0x20000000
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                                        0x1d
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                                         0x4000
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                                            0xe
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                                      0x400
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                                        0xa
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                                        0x200
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                                          0x9
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                                           0x80
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                                            0x7
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_GATE_CLK_BMSK                                                               0x40
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_GATE_CLK_SHFT                                                                0x6
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_COREPOR_RST_BMSK                                                            0x20
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_COREPOR_RST_SHFT                                                             0x5
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_RST_BMSK                                                               0x10
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_RST_SHFT                                                                0x4
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                                             0x8
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                                             0x3
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_L1_RST_DIS_BMSK                                                              0x4
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_L1_RST_DIS_SHFT                                                              0x2
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                                          0x2
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                                          0x1
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CLAMP_BMSK                                                                   0x1
#define HWIO_APCS_ALIAS3_CPU_PWR_CTL_CLAMP_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS3_APC_A53_CFG_STS_ADDR                                                              (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000040)
#define HWIO_APCS_ALIAS3_APC_A53_CFG_STS_RMSK                                                              0x10000000
#define HWIO_APCS_ALIAS3_APC_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS3_APC_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS3_APC_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS3_APC_A53_CFG_STS_SMPNAMP_BMSK                                                      0x10000000
#define HWIO_APCS_ALIAS3_APC_A53_CFG_STS_SMPNAMP_SHFT                                                            0x1c

#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_ADDR                                                               (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_RMSK                                                                0xb1f37ff
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_PWR_STATUS_ADDR, HWIO_APCS_ALIAS3_APC_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_SAW_SLP_ACK_BMSK                                                    0x8000000
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_SAW_SLP_ACK_SHFT                                                         0x1b
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_NO_PWR_DWN_BMSK                                                0x2000000
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_NO_PWR_DWN_SHFT                                                     0x19
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_PWRUP_REQ_BMSK                                                 0x1000000
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_PWRUP_REQ_SHFT                                                      0x18
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_HW_EVENT_BMSK                                                        0x1f0000
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_HW_EVENT_SHFT                                                            0x10
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_RET_SLP_ACK_BMSK                                                       0x2000
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_RET_SLP_ACK_SHFT                                                          0xd
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_RET_SLP_REQ_BMSK                                                       0x1000
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_RET_SLP_REQ_SHFT                                                          0xc
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_TRGTD_DBG_RST_BMSK                                                      0x400
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_TRGTD_DBG_RST_SHFT                                                        0xa
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_RST_BMSK                                                           0x200
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_RST_SHFT                                                             0x9
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_COREPOR_RST_BMSK                                                        0x100
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_COREPOR_RST_SHFT                                                          0x8
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_HS_STS_BMSK                                                              0x80
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_HS_STS_SHFT                                                               0x7
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_MEM_HS_STS_BMSK                                                     0x40
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_MEM_HS_STS_SHFT                                                      0x6
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CLAMP_BMSK                                                               0x20
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CLAMP_SHFT                                                                0x5
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_FRC_CLK_OFF_BMSK                                                         0x10
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_FRC_CLK_OFF_SHFT                                                          0x4
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_AHB_CLK_BMSK                                                              0x8
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_AHB_CLK_SHFT                                                              0x3
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_REF_CLK_BMSK                                                              0x4
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_REF_CLK_SHFT                                                              0x2
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_AUX_CLK_BMSK                                                         0x2
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_AUX_CLK_SHFT                                                         0x1
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_PLL_CLK_BMSK                                                         0x1
#define HWIO_APCS_ALIAS3_APC_PWR_STATUS_CORE_PLL_CLK_SHFT                                                         0x0

#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_ADDR                                                             (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_EN_BMSK                                                                 0x4
#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_EN_SHFT                                                                 0x2
#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_SEL_BMSK                                                                0x3
#define HWIO_APCS_ALIAS3_APC_TEST_BUS_SEL_SEL_SHFT                                                                0x0

#define HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_ADDR                                                            (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_RMSK                                                                   0x1
#define HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_ADDR, HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_RMSK)
#define HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_ADDR, m)
#define HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_ADDR,v)
#define HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_ADDR,m,v,HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_IN)
#define HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_RST_BMSK                                                               0x1
#define HWIO_APCS_ALIAS3_CPU_TRGTD_DBG_RST_RST_SHFT                                                               0x0

#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_ADDR                                                             (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_RMSK                                                             0xff000001
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_HS_CNT_BMSK                                                      0xff000000
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_HS_CNT_SHFT                                                            0x18
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_HS_EN_BMSK                                                              0x1
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_CTL_HS_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_ADDR                                                          (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_RMSK                                                             0x10003
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_IDLE_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_IDLE_SHFT                                                           0x10
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_EN_REST_BMSK                                                         0x2
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_EN_REST_SHFT                                                         0x1
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                                          0x1
#define HWIO_APCS_ALIAS3_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                                          0x0

#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_ADDR                                                            (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000044)
#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_RMSK                                                               0x101ff
#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_ADDR, HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_RMSK)
#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_IN)
#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_BMSK                                             0x10000
#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_SHFT                                                0x10
#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_AUX_STARTADDR_BMSK                                                   0x1ff
#define HWIO_APCS_ALIAS3_SPM_AUX_STARTADDR_AUX_STARTADDR_SHFT                                                     0x0

#define HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_ADDR                                                               (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_RMSK                                                                    0x3ff
#define HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_ADDR, HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_RMSK)
#define HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_ADDR, m)
#define HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_ADDR,v)
#define HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_ADDR,m,v,HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_IN)
#define HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_SLP_CNT_BMSK                                                            0x3ff
#define HWIO_APCS_ALIAS3_APC_L1_SLP_CNT_SLP_CNT_SHFT                                                              0x0

#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_ADDR                                                                  (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000050)
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_RMSK                                                                  0xc0007fff
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS3_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                                                0x80000000
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                                      0x1f
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                                                 0x40000000
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                                       0x1e
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                                           0x7fff
#define HWIO_APCS_ALIAS3_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_ADDR                                                                (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_RMSK                                                                0xc0007fff
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                                              0x80000000
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                                    0x1f
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                                               0x40000000
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                                     0x1e
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                                         0x7fff
#define HWIO_APCS_ALIAS3_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                                            0x0

#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_ADDR                                                          (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_RMSK                                                          0xc0007fff
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK                                0x80000000
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                                      0x1f
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK                                 0x40000000
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                                       0x1e
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                                           0x7fff
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                                              0x0

#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_ADDR                                                           (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x0000005c)
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_RMSK                                                           0xc0007fff
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK                                  0x80000000
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                                        0x1f
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK                                   0x40000000
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                                         0x1e
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                                             0x7fff
#define HWIO_APCS_ALIAS3_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                                                0x0

#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_ADDR                                                           (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_RMSK                                                           0x80000001
#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_ADDR, HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_RMSK)
#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_ADDR, m)
#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_ADDR,v)
#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_ADDR,m,v,HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_IN)
#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_BMSK                                       0x80000000
#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_SHFT                                             0x1f
#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_BMSK                                              0x1
#define HWIO_APCS_ALIAS3_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_SHFT                                              0x0

#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_ADDR                                                             (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                                    0x4
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                                    0x2
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_IGNORE_BMSK                                                             0x2
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_IGNORE_SHFT                                                             0x1
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                                         0x1
#define HWIO_APCS_ALIAS3_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_ADDR                                                           (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000070)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_RMSK                                                                0x1ff
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_EN_BMSK                                                             0x1ff
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EN_EN_SHFT                                                               0x0

#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_ADDR                                                              (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000074)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_RMSK                                                                   0x1ff
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                                         0x1ff
#define HWIO_APCS_ALIAS3_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                                           0x0

#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_ADDR                                                         (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_RMSK                                                            0x1ffff
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                                         0x1ffff
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                                             0x0

#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_ADDR                                                            (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x0000007c)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_RMSK                                                               0x1ffff
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                                                   0x1ffff
#define HWIO_APCS_ALIAS3_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                                       0x0

#define HWIO_APCS_ALIAS3_SPM_EVENT_STS_ADDR                                                                (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000080)
#define HWIO_APCS_ALIAS3_SPM_EVENT_STS_RMSK                                                                     0x1ff
#define HWIO_APCS_ALIAS3_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS3_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS3_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_EVENT_STS_EVENT_VAL_BMSK                                                           0x1ff
#define HWIO_APCS_ALIAS3_SPM_EVENT_STS_EVENT_VAL_SHFT                                                             0x0

#define HWIO_APCS_ALIAS3_SPM_PWR_CTL_STS_ADDR                                                              (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000084)
#define HWIO_APCS_ALIAS3_SPM_PWR_CTL_STS_RMSK                                                                 0x1ffff
#define HWIO_APCS_ALIAS3_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS3_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS3_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                                     0x1ffff
#define HWIO_APCS_ALIAS3_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_ADDR                                                    (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_RMSK                                                           0xf
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                       0x8
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                       0x3
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_BMSK                               0x4
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_SHFT                               0x2
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_BMSK                              0x2
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_SHFT                              0x1
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_BMSK                              0x1
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_SHFT                              0x0

#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_ADDR                                                        (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_RMSK                                                        0xffffffff
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_BMSK                            0xffffffff
#define HWIO_APCS_ALIAS3_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_SHFT                                   0x0

#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR                                                 (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000a0)
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK                                                        0xf
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                 0x8
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                 0x3
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_BMSK                         0x4
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_SHFT                         0x2
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_BMSK                        0x2
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_SHFT                        0x1
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_BMSK                        0x1
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_SHFT                        0x0

#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_ADDR                                                     (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x0000009c)
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_BMSK                      0xffffffff
#define HWIO_APCS_ALIAS3_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_SHFT                             0x0

#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR                                                   (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000a8)
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK                                                          0xf
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                     0x8
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                     0x3
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_BMSK                             0x4
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_SHFT                             0x2
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_BMSK                            0x2
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_SHFT                            0x1
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_BMSK                            0x1
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_SHFT                            0x0

#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_ADDR                                                       (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000a4)
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_RMSK                                                       0xffffffff
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_BMSK                          0xffffffff
#define HWIO_APCS_ALIAS3_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_SHFT                                 0x0

#define HWIO_APCS_ALIAS3_SPARE_ADDR                                                                        (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000ff8)
#define HWIO_APCS_ALIAS3_SPARE_RMSK                                                                              0xff
#define HWIO_APCS_ALIAS3_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPARE_ADDR, HWIO_APCS_ALIAS3_SPARE_RMSK)
#define HWIO_APCS_ALIAS3_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS3_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS3_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPARE_ADDR,m,v,HWIO_APCS_ALIAS3_SPARE_IN)
#define HWIO_APCS_ALIAS3_SPARE_SPARE_BMSK                                                                        0xff
#define HWIO_APCS_ALIAS3_SPARE_SPARE_SHFT                                                                         0x0

#define HWIO_APCS_ALIAS3_SPARE_TZ_ADDR                                                                     (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_ALIAS3_SPARE_TZ_RMSK                                                                           0xff
#define HWIO_APCS_ALIAS3_SPARE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_SPARE_TZ_ADDR, HWIO_APCS_ALIAS3_SPARE_TZ_RMSK)
#define HWIO_APCS_ALIAS3_SPARE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_SPARE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS3_SPARE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_SPARE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS3_SPARE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_SPARE_TZ_ADDR,m,v,HWIO_APCS_ALIAS3_SPARE_TZ_IN)
#define HWIO_APCS_ALIAS3_SPARE_TZ_SPARE_BMSK                                                                     0xff
#define HWIO_APCS_ALIAS3_SPARE_TZ_SPARE_SHFT                                                                      0x0

#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_ADDR                                                     (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_RMSK                                                          0x7ff
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_ADDR, HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_RMSK)
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_ADDR, m)
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_ADDR,v)
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_ADDR,m,v,HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_IN)
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_BMSK                                  0x7f8
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_SHFT                                    0x3
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_BMSK                                      0x4
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_SHFT                                      0x2
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_BMSK                                0x2
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_SHFT                                0x1
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_BMSK                                0x1
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_SHFT                                0x0

#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_STATUS_ADDR                                                  (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x00000094)
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_STATUS_RMSK                                                  0xffffffff
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_STATUS_ADDR, HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_BMSK                           0xffffffff
#define HWIO_APCS_ALIAS3_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_SHFT                                  0x0

#define HWIO_APCS_ALIAS3_MAS_CFG_ADDR                                                                      (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000c0)
#define HWIO_APCS_ALIAS3_MAS_CFG_RMSK                                                                         0x10707
#define HWIO_APCS_ALIAS3_MAS_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_MAS_CFG_ADDR, HWIO_APCS_ALIAS3_MAS_CFG_RMSK)
#define HWIO_APCS_ALIAS3_MAS_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_MAS_CFG_ADDR, m)
#define HWIO_APCS_ALIAS3_MAS_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_MAS_CFG_ADDR,v)
#define HWIO_APCS_ALIAS3_MAS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_MAS_CFG_ADDR,m,v,HWIO_APCS_ALIAS3_MAS_CFG_IN)
#define HWIO_APCS_ALIAS3_MAS_CFG_MAS_MEM_CLAMP_EN_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS3_MAS_CFG_MAS_MEM_CLAMP_EN_SHFT                                                           0x10
#define HWIO_APCS_ALIAS3_MAS_CFG_MAS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS3_MAS_CFG_MAS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS3_MAS_CFG_MAS_APC_EN_REST_BMSK                                                             0x7
#define HWIO_APCS_ALIAS3_MAS_CFG_MAS_APC_EN_REST_SHFT                                                             0x0

#define HWIO_APCS_ALIAS3_APM_TILE_CFG_ADDR                                                                 (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000c4)
#define HWIO_APCS_ALIAS3_APM_TILE_CFG_RMSK                                                                      0x1ff
#define HWIO_APCS_ALIAS3_APM_TILE_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_APM_TILE_CFG_ADDR, HWIO_APCS_ALIAS3_APM_TILE_CFG_RMSK)
#define HWIO_APCS_ALIAS3_APM_TILE_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_APM_TILE_CFG_ADDR, m)
#define HWIO_APCS_ALIAS3_APM_TILE_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_APM_TILE_CFG_ADDR,v)
#define HWIO_APCS_ALIAS3_APM_TILE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_APM_TILE_CFG_ADDR,m,v,HWIO_APCS_ALIAS3_APM_TILE_CFG_IN)
#define HWIO_APCS_ALIAS3_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_BMSK                                               0x100
#define HWIO_APCS_ALIAS3_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_SHFT                                                 0x8
#define HWIO_APCS_ALIAS3_APM_TILE_CFG_APM_TILE_APMCONFIG_BMSK                                                    0xff
#define HWIO_APCS_ALIAS3_APM_TILE_CFG_APM_TILE_APMCONFIG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS3_MAS_STS_ADDR                                                                      (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000c8)
#define HWIO_APCS_ALIAS3_MAS_STS_RMSK                                                                        0x3ff373
#define HWIO_APCS_ALIAS3_MAS_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_MAS_STS_ADDR, HWIO_APCS_ALIAS3_MAS_STS_RMSK)
#define HWIO_APCS_ALIAS3_MAS_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_MAS_STS_ADDR, m)
#define HWIO_APCS_ALIAS3_MAS_STS_TMR_CNT_BMSK                                                                0x3f0000
#define HWIO_APCS_ALIAS3_MAS_STS_TMR_CNT_SHFT                                                                    0x10
#define HWIO_APCS_ALIAS3_MAS_STS_STEADY_SLEEP_BMSK                                                             0x8000
#define HWIO_APCS_ALIAS3_MAS_STS_STEADY_SLEEP_SHFT                                                                0xf
#define HWIO_APCS_ALIAS3_MAS_STS_STEADY_ACTIVE_BMSK                                                            0x4000
#define HWIO_APCS_ALIAS3_MAS_STS_STEADY_ACTIVE_SHFT                                                               0xe
#define HWIO_APCS_ALIAS3_MAS_STS_SPM_RET_BMSK                                                                  0x2000
#define HWIO_APCS_ALIAS3_MAS_STS_SPM_RET_SHFT                                                                     0xd
#define HWIO_APCS_ALIAS3_MAS_STS_SPM_PC_BMSK                                                                   0x1000
#define HWIO_APCS_ALIAS3_MAS_STS_SPM_PC_SHFT                                                                      0xc
#define HWIO_APCS_ALIAS3_MAS_STS_NEW_MODE_BMSK                                                                  0x300
#define HWIO_APCS_ALIAS3_MAS_STS_NEW_MODE_SHFT                                                                    0x8
#define HWIO_APCS_ALIAS3_MAS_STS_FSM_STATE_BMSK                                                                  0x70
#define HWIO_APCS_ALIAS3_MAS_STS_FSM_STATE_SHFT                                                                   0x4
#define HWIO_APCS_ALIAS3_MAS_STS_CURR_MODE_BMSK                                                                   0x3
#define HWIO_APCS_ALIAS3_MAS_STS_CURR_MODE_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS3_PWR_MUX_STS_ADDR                                                                  (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000cc)
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_RMSK                                                                     0x70773
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_PWR_MUX_STS_ADDR, HWIO_APCS_ALIAS3_PWR_MUX_STS_RMSK)
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_PWR_MUX_STS_ADDR, m)
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_CLAMP_BMSK                                                               0x40000
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_CLAMP_SHFT                                                                  0x12
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_TGL_SEL_BMSK                                                             0x20000
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_TGL_SEL_SHFT                                                                0x11
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_MEM_RET_BMSK                                                             0x10000
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_MEM_RET_SHFT                                                                0x10
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_APCC_EN_REST_BMSK                                                           0x70
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_APCC_EN_REST_SHFT                                                            0x4
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_EN_FEW_BMSK                                                                  0x3
#define HWIO_APCS_ALIAS3_PWR_MUX_STS_EN_FEW_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_ADDR                                                                 (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000d0)
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_RMSK                                                                   0x811377
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_MAS_OVERRIDE_ADDR, HWIO_APCS_ALIAS3_MAS_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_MAS_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_MAS_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_MAS_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS3_MAS_OVERRIDE_IN)
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_BMSK                                          0x800000
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_SHFT                                              0x17
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_BMSK                                 0x10000
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_SHFT                                    0x10
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_BMSK                             0x1000
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_SHFT                                0xc
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_BMSK                                       0x300
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_SHFT                                         0x8
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_BMSK                                     0x70
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_SHFT                                      0x4
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_BMSK                                     0x7
#define HWIO_APCS_ALIAS3_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_SHFT                                     0x0

#define HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_ADDR                                                          (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000b0)
#define HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_RMSK                                                          0xffffffff
#define HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_ADDR, HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_RMSK)
#define HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_ADDR, m)
#define HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_ADDR,v)
#define HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_ADDR,m,v,HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_IN)
#define HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_BMSK                                0xffffffff
#define HWIO_APCS_ALIAS3_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_SHFT                                       0x0

#define HWIO_APCS_ALIAS3_CORE_HANG_VALUE_ADDR                                                              (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000b4)
#define HWIO_APCS_ALIAS3_CORE_HANG_VALUE_RMSK                                                              0xffffffff
#define HWIO_APCS_ALIAS3_CORE_HANG_VALUE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_CORE_HANG_VALUE_ADDR, HWIO_APCS_ALIAS3_CORE_HANG_VALUE_RMSK)
#define HWIO_APCS_ALIAS3_CORE_HANG_VALUE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_CORE_HANG_VALUE_ADDR, m)
#define HWIO_APCS_ALIAS3_CORE_HANG_VALUE_VALUE_WHEN_HUNG_BMSK                                              0xffffffff
#define HWIO_APCS_ALIAS3_CORE_HANG_VALUE_VALUE_WHEN_HUNG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_ADDR                                                             (APCS_ALIAS3_APSS_ACS_REG_BASE      + 0x000000b8)
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_RMSK                                                                  0x1f7
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_ADDR, HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_RMSK)
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_ADDR, m)
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_ADDR,v)
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_ADDR,m,v,HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_IN)
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_PMUEVENT_SEL_BMSK                                                     0x1f0
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_PMUEVENT_SEL_SHFT                                                       0x4
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_BMSK                                           0x4
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_SHFT                                           0x2
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_BMSK                                               0x2
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_SHFT                                               0x1
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_CORE_HANG_STATUS_BMSK                                                   0x1
#define HWIO_APCS_ALIAS3_CORE_HANG_CONFIG_CORE_HANG_STATUS_SHFT                                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS4_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS4_APSS_ACS_REG_BASE                                                                      (A53SS_BASE      + 0x00088000)

#define HWIO_APCS_ALIAS4_APC_SECURE_ADDR                                                                   (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS4_APC_SECURE_RMSK                                                                          0xf
#define HWIO_APCS_ALIAS4_APC_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_SECURE_ADDR, HWIO_APCS_ALIAS4_APC_SECURE_RMSK)
#define HWIO_APCS_ALIAS4_APC_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS4_APC_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_APC_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS4_APC_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_APC_SECURE_ADDR,m,v,HWIO_APCS_ALIAS4_APC_SECURE_IN)
#define HWIO_APCS_ALIAS4_APC_SECURE_VOTE_BMSK                                                                     0x8
#define HWIO_APCS_ALIAS4_APC_SECURE_VOTE_SHFT                                                                     0x3
#define HWIO_APCS_ALIAS4_APC_SECURE_TST_BMSK                                                                      0x4
#define HWIO_APCS_ALIAS4_APC_SECURE_TST_SHFT                                                                      0x2
#define HWIO_APCS_ALIAS4_APC_SECURE_CLK_CTL_BMSK                                                                  0x2
#define HWIO_APCS_ALIAS4_APC_SECURE_CLK_CTL_SHFT                                                                  0x1
#define HWIO_APCS_ALIAS4_APC_SECURE_SLP_CTL_BMSK                                                                  0x1
#define HWIO_APCS_ALIAS4_APC_SECURE_SLP_CTL_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR                                                                  (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_RMSK                                                                  0x200046ff
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS4_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS4_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                                  0x20000000
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                                        0x1d
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                                         0x4000
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                                            0xe
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                                      0x400
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                                        0xa
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                                        0x200
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                                          0x9
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                                           0x80
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                                            0x7
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_GATE_CLK_BMSK                                                               0x40
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_GATE_CLK_SHFT                                                                0x6
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_COREPOR_RST_BMSK                                                            0x20
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_COREPOR_RST_SHFT                                                             0x5
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_RST_BMSK                                                               0x10
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_RST_SHFT                                                                0x4
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                                             0x8
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                                             0x3
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_RST_DIS_BMSK                                                              0x4
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_L1_RST_DIS_SHFT                                                              0x2
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                                          0x2
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                                          0x1
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CLAMP_BMSK                                                                   0x1
#define HWIO_APCS_ALIAS4_CPU_PWR_CTL_CLAMP_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS4_APC_A53_CFG_STS_ADDR                                                              (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000040)
#define HWIO_APCS_ALIAS4_APC_A53_CFG_STS_RMSK                                                              0x10000000
#define HWIO_APCS_ALIAS4_APC_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS4_APC_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS4_APC_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS4_APC_A53_CFG_STS_SMPNAMP_BMSK                                                      0x10000000
#define HWIO_APCS_ALIAS4_APC_A53_CFG_STS_SMPNAMP_SHFT                                                            0x1c

#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_ADDR                                                               (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_RMSK                                                                0xb1f37ff
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_STATUS_ADDR, HWIO_APCS_ALIAS4_APC_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_SAW_SLP_ACK_BMSK                                                    0x8000000
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_SAW_SLP_ACK_SHFT                                                         0x1b
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_NO_PWR_DWN_BMSK                                                0x2000000
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_NO_PWR_DWN_SHFT                                                     0x19
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_PWRUP_REQ_BMSK                                                 0x1000000
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_PWRUP_REQ_SHFT                                                      0x18
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_HW_EVENT_BMSK                                                        0x1f0000
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_HW_EVENT_SHFT                                                            0x10
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_RET_SLP_ACK_BMSK                                                       0x2000
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_RET_SLP_ACK_SHFT                                                          0xd
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_RET_SLP_REQ_BMSK                                                       0x1000
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_RET_SLP_REQ_SHFT                                                          0xc
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_TRGTD_DBG_RST_BMSK                                                      0x400
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_TRGTD_DBG_RST_SHFT                                                        0xa
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_RST_BMSK                                                           0x200
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_RST_SHFT                                                             0x9
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_COREPOR_RST_BMSK                                                        0x100
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_COREPOR_RST_SHFT                                                          0x8
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_HS_STS_BMSK                                                              0x80
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_HS_STS_SHFT                                                               0x7
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_MEM_HS_STS_BMSK                                                     0x40
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_MEM_HS_STS_SHFT                                                      0x6
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CLAMP_BMSK                                                               0x20
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CLAMP_SHFT                                                                0x5
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_FRC_CLK_OFF_BMSK                                                         0x10
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_FRC_CLK_OFF_SHFT                                                          0x4
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_AHB_CLK_BMSK                                                              0x8
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_AHB_CLK_SHFT                                                              0x3
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_REF_CLK_BMSK                                                              0x4
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_REF_CLK_SHFT                                                              0x2
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_AUX_CLK_BMSK                                                         0x2
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_AUX_CLK_SHFT                                                         0x1
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_PLL_CLK_BMSK                                                         0x1
#define HWIO_APCS_ALIAS4_APC_PWR_STATUS_CORE_PLL_CLK_SHFT                                                         0x0

#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_ADDR                                                             (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_EN_BMSK                                                                 0x4
#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_EN_SHFT                                                                 0x2
#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_SEL_BMSK                                                                0x3
#define HWIO_APCS_ALIAS4_APC_TEST_BUS_SEL_SEL_SHFT                                                                0x0

#define HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_ADDR                                                            (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_RMSK                                                                   0x1
#define HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_ADDR, HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_RMSK)
#define HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_ADDR, m)
#define HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_ADDR,v)
#define HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_ADDR,m,v,HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_IN)
#define HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_RST_BMSK                                                               0x1
#define HWIO_APCS_ALIAS4_CPU_TRGTD_DBG_RST_RST_SHFT                                                               0x0

#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR                                                             (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_RMSK                                                             0xff000001
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_HS_CNT_BMSK                                                      0xff000000
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_HS_CNT_SHFT                                                            0x18
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_HS_EN_BMSK                                                              0x1
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_CTL_HS_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_ADDR                                                          (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_RMSK                                                             0x10003
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_IDLE_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_IDLE_SHFT                                                           0x10
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_EN_REST_BMSK                                                         0x2
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_EN_REST_SHFT                                                         0x1
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                                          0x1
#define HWIO_APCS_ALIAS4_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                                          0x0

#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_ADDR                                                            (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000044)
#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_RMSK                                                               0x101ff
#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_ADDR, HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_RMSK)
#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_IN)
#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_BMSK                                             0x10000
#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_SHFT                                                0x10
#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_AUX_STARTADDR_BMSK                                                   0x1ff
#define HWIO_APCS_ALIAS4_SPM_AUX_STARTADDR_AUX_STARTADDR_SHFT                                                     0x0

#define HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_ADDR                                                               (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_RMSK                                                                    0x3ff
#define HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_ADDR, HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_RMSK)
#define HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_ADDR, m)
#define HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_ADDR,v)
#define HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_ADDR,m,v,HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_IN)
#define HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_SLP_CNT_BMSK                                                            0x3ff
#define HWIO_APCS_ALIAS4_APC_L1_SLP_CNT_SLP_CNT_SHFT                                                              0x0

#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_ADDR                                                                  (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000050)
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_RMSK                                                                  0xc0007fff
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS4_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                                                0x80000000
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                                      0x1f
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                                                 0x40000000
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                                       0x1e
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                                           0x7fff
#define HWIO_APCS_ALIAS4_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_ADDR                                                                (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_RMSK                                                                0xc0007fff
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                                              0x80000000
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                                    0x1f
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                                               0x40000000
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                                     0x1e
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                                         0x7fff
#define HWIO_APCS_ALIAS4_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                                            0x0

#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_ADDR                                                          (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_RMSK                                                          0xc0007fff
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK                                0x80000000
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                                      0x1f
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK                                 0x40000000
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                                       0x1e
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                                           0x7fff
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                                              0x0

#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_ADDR                                                           (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x0000005c)
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_RMSK                                                           0xc0007fff
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK                                  0x80000000
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                                        0x1f
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK                                   0x40000000
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                                         0x1e
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                                             0x7fff
#define HWIO_APCS_ALIAS4_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                                                0x0

#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_ADDR                                                           (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_RMSK                                                           0x80000001
#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_ADDR, HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_RMSK)
#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_ADDR, m)
#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_ADDR,v)
#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_ADDR,m,v,HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_IN)
#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_BMSK                                       0x80000000
#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_SHFT                                             0x1f
#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_BMSK                                              0x1
#define HWIO_APCS_ALIAS4_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_SHFT                                              0x0

#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_ADDR                                                             (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                                    0x4
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                                    0x2
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_IGNORE_BMSK                                                             0x2
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_IGNORE_SHFT                                                             0x1
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                                         0x1
#define HWIO_APCS_ALIAS4_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_ADDR                                                           (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000070)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_RMSK                                                                0x1ff
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_EN_BMSK                                                             0x1ff
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EN_EN_SHFT                                                               0x0

#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_ADDR                                                              (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000074)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_RMSK                                                                   0x1ff
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                                         0x1ff
#define HWIO_APCS_ALIAS4_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                                           0x0

#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_ADDR                                                         (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_RMSK                                                            0x1ffff
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                                         0x1ffff
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                                             0x0

#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_ADDR                                                            (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x0000007c)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_RMSK                                                               0x1ffff
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                                                   0x1ffff
#define HWIO_APCS_ALIAS4_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                                       0x0

#define HWIO_APCS_ALIAS4_SPM_EVENT_STS_ADDR                                                                (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000080)
#define HWIO_APCS_ALIAS4_SPM_EVENT_STS_RMSK                                                                     0x1ff
#define HWIO_APCS_ALIAS4_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS4_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS4_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_EVENT_STS_EVENT_VAL_BMSK                                                           0x1ff
#define HWIO_APCS_ALIAS4_SPM_EVENT_STS_EVENT_VAL_SHFT                                                             0x0

#define HWIO_APCS_ALIAS4_SPM_PWR_CTL_STS_ADDR                                                              (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000084)
#define HWIO_APCS_ALIAS4_SPM_PWR_CTL_STS_RMSK                                                                 0x1ffff
#define HWIO_APCS_ALIAS4_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS4_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS4_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                                     0x1ffff
#define HWIO_APCS_ALIAS4_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_ADDR                                                    (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_RMSK                                                           0xf
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                       0x8
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                       0x3
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_BMSK                               0x4
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_SHFT                               0x2
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_BMSK                              0x2
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_SHFT                              0x1
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_BMSK                              0x1
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_SHFT                              0x0

#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_ADDR                                                        (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_RMSK                                                        0xffffffff
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_BMSK                            0xffffffff
#define HWIO_APCS_ALIAS4_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_SHFT                                   0x0

#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR                                                 (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000a0)
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK                                                        0xf
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                 0x8
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                 0x3
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_BMSK                         0x4
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_SHFT                         0x2
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_BMSK                        0x2
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_SHFT                        0x1
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_BMSK                        0x1
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_SHFT                        0x0

#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_ADDR                                                     (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x0000009c)
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_BMSK                      0xffffffff
#define HWIO_APCS_ALIAS4_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_SHFT                             0x0

#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR                                                   (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000a8)
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK                                                          0xf
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                     0x8
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                     0x3
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_BMSK                             0x4
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_SHFT                             0x2
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_BMSK                            0x2
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_SHFT                            0x1
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_BMSK                            0x1
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_SHFT                            0x0

#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_ADDR                                                       (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000a4)
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_RMSK                                                       0xffffffff
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_BMSK                          0xffffffff
#define HWIO_APCS_ALIAS4_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_SHFT                                 0x0

#define HWIO_APCS_ALIAS4_SPARE_ADDR                                                                        (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000ff8)
#define HWIO_APCS_ALIAS4_SPARE_RMSK                                                                              0xff
#define HWIO_APCS_ALIAS4_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPARE_ADDR, HWIO_APCS_ALIAS4_SPARE_RMSK)
#define HWIO_APCS_ALIAS4_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS4_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS4_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPARE_ADDR,m,v,HWIO_APCS_ALIAS4_SPARE_IN)
#define HWIO_APCS_ALIAS4_SPARE_SPARE_BMSK                                                                        0xff
#define HWIO_APCS_ALIAS4_SPARE_SPARE_SHFT                                                                         0x0

#define HWIO_APCS_ALIAS4_SPARE_TZ_ADDR                                                                     (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_ALIAS4_SPARE_TZ_RMSK                                                                           0xff
#define HWIO_APCS_ALIAS4_SPARE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_SPARE_TZ_ADDR, HWIO_APCS_ALIAS4_SPARE_TZ_RMSK)
#define HWIO_APCS_ALIAS4_SPARE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_SPARE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS4_SPARE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_SPARE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS4_SPARE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_SPARE_TZ_ADDR,m,v,HWIO_APCS_ALIAS4_SPARE_TZ_IN)
#define HWIO_APCS_ALIAS4_SPARE_TZ_SPARE_BMSK                                                                     0xff
#define HWIO_APCS_ALIAS4_SPARE_TZ_SPARE_SHFT                                                                      0x0

#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_ADDR                                                     (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_RMSK                                                          0x7ff
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_ADDR, HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_RMSK)
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_ADDR, m)
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_ADDR,v)
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_ADDR,m,v,HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_IN)
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_BMSK                                  0x7f8
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_SHFT                                    0x3
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_BMSK                                      0x4
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_SHFT                                      0x2
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_BMSK                                0x2
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_SHFT                                0x1
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_BMSK                                0x1
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_SHFT                                0x0

#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_STATUS_ADDR                                                  (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x00000094)
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_STATUS_RMSK                                                  0xffffffff
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_STATUS_ADDR, HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_BMSK                           0xffffffff
#define HWIO_APCS_ALIAS4_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_SHFT                                  0x0

#define HWIO_APCS_ALIAS4_MAS_CFG_ADDR                                                                      (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000c0)
#define HWIO_APCS_ALIAS4_MAS_CFG_RMSK                                                                         0x10707
#define HWIO_APCS_ALIAS4_MAS_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_MAS_CFG_ADDR, HWIO_APCS_ALIAS4_MAS_CFG_RMSK)
#define HWIO_APCS_ALIAS4_MAS_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_MAS_CFG_ADDR, m)
#define HWIO_APCS_ALIAS4_MAS_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_MAS_CFG_ADDR,v)
#define HWIO_APCS_ALIAS4_MAS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_MAS_CFG_ADDR,m,v,HWIO_APCS_ALIAS4_MAS_CFG_IN)
#define HWIO_APCS_ALIAS4_MAS_CFG_MAS_MEM_CLAMP_EN_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS4_MAS_CFG_MAS_MEM_CLAMP_EN_SHFT                                                           0x10
#define HWIO_APCS_ALIAS4_MAS_CFG_MAS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS4_MAS_CFG_MAS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS4_MAS_CFG_MAS_APC_EN_REST_BMSK                                                             0x7
#define HWIO_APCS_ALIAS4_MAS_CFG_MAS_APC_EN_REST_SHFT                                                             0x0

#define HWIO_APCS_ALIAS4_APM_TILE_CFG_ADDR                                                                 (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000c4)
#define HWIO_APCS_ALIAS4_APM_TILE_CFG_RMSK                                                                      0x1ff
#define HWIO_APCS_ALIAS4_APM_TILE_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_APM_TILE_CFG_ADDR, HWIO_APCS_ALIAS4_APM_TILE_CFG_RMSK)
#define HWIO_APCS_ALIAS4_APM_TILE_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_APM_TILE_CFG_ADDR, m)
#define HWIO_APCS_ALIAS4_APM_TILE_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_APM_TILE_CFG_ADDR,v)
#define HWIO_APCS_ALIAS4_APM_TILE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_APM_TILE_CFG_ADDR,m,v,HWIO_APCS_ALIAS4_APM_TILE_CFG_IN)
#define HWIO_APCS_ALIAS4_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_BMSK                                               0x100
#define HWIO_APCS_ALIAS4_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_SHFT                                                 0x8
#define HWIO_APCS_ALIAS4_APM_TILE_CFG_APM_TILE_APMCONFIG_BMSK                                                    0xff
#define HWIO_APCS_ALIAS4_APM_TILE_CFG_APM_TILE_APMCONFIG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS4_MAS_STS_ADDR                                                                      (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000c8)
#define HWIO_APCS_ALIAS4_MAS_STS_RMSK                                                                        0x3ff373
#define HWIO_APCS_ALIAS4_MAS_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_MAS_STS_ADDR, HWIO_APCS_ALIAS4_MAS_STS_RMSK)
#define HWIO_APCS_ALIAS4_MAS_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_MAS_STS_ADDR, m)
#define HWIO_APCS_ALIAS4_MAS_STS_TMR_CNT_BMSK                                                                0x3f0000
#define HWIO_APCS_ALIAS4_MAS_STS_TMR_CNT_SHFT                                                                    0x10
#define HWIO_APCS_ALIAS4_MAS_STS_STEADY_SLEEP_BMSK                                                             0x8000
#define HWIO_APCS_ALIAS4_MAS_STS_STEADY_SLEEP_SHFT                                                                0xf
#define HWIO_APCS_ALIAS4_MAS_STS_STEADY_ACTIVE_BMSK                                                            0x4000
#define HWIO_APCS_ALIAS4_MAS_STS_STEADY_ACTIVE_SHFT                                                               0xe
#define HWIO_APCS_ALIAS4_MAS_STS_SPM_RET_BMSK                                                                  0x2000
#define HWIO_APCS_ALIAS4_MAS_STS_SPM_RET_SHFT                                                                     0xd
#define HWIO_APCS_ALIAS4_MAS_STS_SPM_PC_BMSK                                                                   0x1000
#define HWIO_APCS_ALIAS4_MAS_STS_SPM_PC_SHFT                                                                      0xc
#define HWIO_APCS_ALIAS4_MAS_STS_NEW_MODE_BMSK                                                                  0x300
#define HWIO_APCS_ALIAS4_MAS_STS_NEW_MODE_SHFT                                                                    0x8
#define HWIO_APCS_ALIAS4_MAS_STS_FSM_STATE_BMSK                                                                  0x70
#define HWIO_APCS_ALIAS4_MAS_STS_FSM_STATE_SHFT                                                                   0x4
#define HWIO_APCS_ALIAS4_MAS_STS_CURR_MODE_BMSK                                                                   0x3
#define HWIO_APCS_ALIAS4_MAS_STS_CURR_MODE_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS4_PWR_MUX_STS_ADDR                                                                  (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000cc)
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_RMSK                                                                     0x70773
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_PWR_MUX_STS_ADDR, HWIO_APCS_ALIAS4_PWR_MUX_STS_RMSK)
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_PWR_MUX_STS_ADDR, m)
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_CLAMP_BMSK                                                               0x40000
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_CLAMP_SHFT                                                                  0x12
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_TGL_SEL_BMSK                                                             0x20000
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_TGL_SEL_SHFT                                                                0x11
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_MEM_RET_BMSK                                                             0x10000
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_MEM_RET_SHFT                                                                0x10
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_APCC_EN_REST_BMSK                                                           0x70
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_APCC_EN_REST_SHFT                                                            0x4
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_EN_FEW_BMSK                                                                  0x3
#define HWIO_APCS_ALIAS4_PWR_MUX_STS_EN_FEW_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_ADDR                                                                 (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000d0)
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_RMSK                                                                   0x811377
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_MAS_OVERRIDE_ADDR, HWIO_APCS_ALIAS4_MAS_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_MAS_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_MAS_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_MAS_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS4_MAS_OVERRIDE_IN)
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_BMSK                                          0x800000
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_SHFT                                              0x17
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_BMSK                                 0x10000
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_SHFT                                    0x10
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_BMSK                             0x1000
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_SHFT                                0xc
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_BMSK                                       0x300
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_SHFT                                         0x8
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_BMSK                                     0x70
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_SHFT                                      0x4
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_BMSK                                     0x7
#define HWIO_APCS_ALIAS4_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_SHFT                                     0x0

#define HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_ADDR                                                          (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000b0)
#define HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_RMSK                                                          0xffffffff
#define HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_ADDR, HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_RMSK)
#define HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_ADDR, m)
#define HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_ADDR,v)
#define HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_ADDR,m,v,HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_IN)
#define HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_BMSK                                0xffffffff
#define HWIO_APCS_ALIAS4_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_SHFT                                       0x0

#define HWIO_APCS_ALIAS4_CORE_HANG_VALUE_ADDR                                                              (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000b4)
#define HWIO_APCS_ALIAS4_CORE_HANG_VALUE_RMSK                                                              0xffffffff
#define HWIO_APCS_ALIAS4_CORE_HANG_VALUE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_CORE_HANG_VALUE_ADDR, HWIO_APCS_ALIAS4_CORE_HANG_VALUE_RMSK)
#define HWIO_APCS_ALIAS4_CORE_HANG_VALUE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_CORE_HANG_VALUE_ADDR, m)
#define HWIO_APCS_ALIAS4_CORE_HANG_VALUE_VALUE_WHEN_HUNG_BMSK                                              0xffffffff
#define HWIO_APCS_ALIAS4_CORE_HANG_VALUE_VALUE_WHEN_HUNG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_ADDR                                                             (APCS_ALIAS4_APSS_ACS_REG_BASE      + 0x000000b8)
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_RMSK                                                                  0x1f7
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_ADDR, HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_RMSK)
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_ADDR, m)
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_ADDR,v)
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_ADDR,m,v,HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_IN)
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_PMUEVENT_SEL_BMSK                                                     0x1f0
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_PMUEVENT_SEL_SHFT                                                       0x4
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_BMSK                                           0x4
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_SHFT                                           0x2
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_BMSK                                               0x2
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_SHFT                                               0x1
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_CORE_HANG_STATUS_BMSK                                                   0x1
#define HWIO_APCS_ALIAS4_CORE_HANG_CONFIG_CORE_HANG_STATUS_SHFT                                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS5_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS5_APSS_ACS_REG_BASE                                                                      (A53SS_BASE      + 0x00098000)

#define HWIO_APCS_ALIAS5_APC_SECURE_ADDR                                                                   (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS5_APC_SECURE_RMSK                                                                          0xf
#define HWIO_APCS_ALIAS5_APC_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_SECURE_ADDR, HWIO_APCS_ALIAS5_APC_SECURE_RMSK)
#define HWIO_APCS_ALIAS5_APC_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS5_APC_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_APC_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS5_APC_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_APC_SECURE_ADDR,m,v,HWIO_APCS_ALIAS5_APC_SECURE_IN)
#define HWIO_APCS_ALIAS5_APC_SECURE_VOTE_BMSK                                                                     0x8
#define HWIO_APCS_ALIAS5_APC_SECURE_VOTE_SHFT                                                                     0x3
#define HWIO_APCS_ALIAS5_APC_SECURE_TST_BMSK                                                                      0x4
#define HWIO_APCS_ALIAS5_APC_SECURE_TST_SHFT                                                                      0x2
#define HWIO_APCS_ALIAS5_APC_SECURE_CLK_CTL_BMSK                                                                  0x2
#define HWIO_APCS_ALIAS5_APC_SECURE_CLK_CTL_SHFT                                                                  0x1
#define HWIO_APCS_ALIAS5_APC_SECURE_SLP_CTL_BMSK                                                                  0x1
#define HWIO_APCS_ALIAS5_APC_SECURE_SLP_CTL_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR                                                                  (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_RMSK                                                                  0x200046ff
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS5_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS5_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                                  0x20000000
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                                        0x1d
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                                         0x4000
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                                            0xe
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                                      0x400
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                                        0xa
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                                        0x200
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                                          0x9
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                                           0x80
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                                            0x7
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_GATE_CLK_BMSK                                                               0x40
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_GATE_CLK_SHFT                                                                0x6
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_COREPOR_RST_BMSK                                                            0x20
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_COREPOR_RST_SHFT                                                             0x5
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_RST_BMSK                                                               0x10
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_RST_SHFT                                                                0x4
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                                             0x8
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                                             0x3
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_RST_DIS_BMSK                                                              0x4
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_L1_RST_DIS_SHFT                                                              0x2
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                                          0x2
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                                          0x1
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CLAMP_BMSK                                                                   0x1
#define HWIO_APCS_ALIAS5_CPU_PWR_CTL_CLAMP_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS5_APC_A53_CFG_STS_ADDR                                                              (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000040)
#define HWIO_APCS_ALIAS5_APC_A53_CFG_STS_RMSK                                                              0x10000000
#define HWIO_APCS_ALIAS5_APC_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS5_APC_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS5_APC_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS5_APC_A53_CFG_STS_SMPNAMP_BMSK                                                      0x10000000
#define HWIO_APCS_ALIAS5_APC_A53_CFG_STS_SMPNAMP_SHFT                                                            0x1c

#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_ADDR                                                               (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_RMSK                                                                0xb1f37ff
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_STATUS_ADDR, HWIO_APCS_ALIAS5_APC_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_SAW_SLP_ACK_BMSK                                                    0x8000000
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_SAW_SLP_ACK_SHFT                                                         0x1b
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_NO_PWR_DWN_BMSK                                                0x2000000
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_NO_PWR_DWN_SHFT                                                     0x19
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_PWRUP_REQ_BMSK                                                 0x1000000
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_PWRUP_REQ_SHFT                                                      0x18
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_HW_EVENT_BMSK                                                        0x1f0000
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_HW_EVENT_SHFT                                                            0x10
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_RET_SLP_ACK_BMSK                                                       0x2000
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_RET_SLP_ACK_SHFT                                                          0xd
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_RET_SLP_REQ_BMSK                                                       0x1000
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_RET_SLP_REQ_SHFT                                                          0xc
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_TRGTD_DBG_RST_BMSK                                                      0x400
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_TRGTD_DBG_RST_SHFT                                                        0xa
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_RST_BMSK                                                           0x200
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_RST_SHFT                                                             0x9
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_COREPOR_RST_BMSK                                                        0x100
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_COREPOR_RST_SHFT                                                          0x8
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_HS_STS_BMSK                                                              0x80
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_HS_STS_SHFT                                                               0x7
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_MEM_HS_STS_BMSK                                                     0x40
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_MEM_HS_STS_SHFT                                                      0x6
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CLAMP_BMSK                                                               0x20
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CLAMP_SHFT                                                                0x5
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_FRC_CLK_OFF_BMSK                                                         0x10
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_FRC_CLK_OFF_SHFT                                                          0x4
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_AHB_CLK_BMSK                                                              0x8
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_AHB_CLK_SHFT                                                              0x3
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_REF_CLK_BMSK                                                              0x4
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_REF_CLK_SHFT                                                              0x2
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_AUX_CLK_BMSK                                                         0x2
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_AUX_CLK_SHFT                                                         0x1
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_PLL_CLK_BMSK                                                         0x1
#define HWIO_APCS_ALIAS5_APC_PWR_STATUS_CORE_PLL_CLK_SHFT                                                         0x0

#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_ADDR                                                             (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_EN_BMSK                                                                 0x4
#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_EN_SHFT                                                                 0x2
#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_SEL_BMSK                                                                0x3
#define HWIO_APCS_ALIAS5_APC_TEST_BUS_SEL_SEL_SHFT                                                                0x0

#define HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_ADDR                                                            (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_RMSK                                                                   0x1
#define HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_ADDR, HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_RMSK)
#define HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_ADDR, m)
#define HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_ADDR,v)
#define HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_ADDR,m,v,HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_IN)
#define HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_RST_BMSK                                                               0x1
#define HWIO_APCS_ALIAS5_CPU_TRGTD_DBG_RST_RST_SHFT                                                               0x0

#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR                                                             (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_RMSK                                                             0xff000001
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_HS_CNT_BMSK                                                      0xff000000
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_HS_CNT_SHFT                                                            0x18
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_HS_EN_BMSK                                                              0x1
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_CTL_HS_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_ADDR                                                          (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_RMSK                                                             0x10003
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_IDLE_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_IDLE_SHFT                                                           0x10
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_EN_REST_BMSK                                                         0x2
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_EN_REST_SHFT                                                         0x1
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                                          0x1
#define HWIO_APCS_ALIAS5_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                                          0x0

#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_ADDR                                                            (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000044)
#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_RMSK                                                               0x101ff
#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_ADDR, HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_RMSK)
#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_IN)
#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_BMSK                                             0x10000
#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_SHFT                                                0x10
#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_AUX_STARTADDR_BMSK                                                   0x1ff
#define HWIO_APCS_ALIAS5_SPM_AUX_STARTADDR_AUX_STARTADDR_SHFT                                                     0x0

#define HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_ADDR                                                               (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_RMSK                                                                    0x3ff
#define HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_ADDR, HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_RMSK)
#define HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_ADDR, m)
#define HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_ADDR,v)
#define HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_ADDR,m,v,HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_IN)
#define HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_SLP_CNT_BMSK                                                            0x3ff
#define HWIO_APCS_ALIAS5_APC_L1_SLP_CNT_SLP_CNT_SHFT                                                              0x0

#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_ADDR                                                                  (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000050)
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_RMSK                                                                  0xc0007fff
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS5_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                                                0x80000000
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                                      0x1f
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                                                 0x40000000
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                                       0x1e
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                                           0x7fff
#define HWIO_APCS_ALIAS5_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_ADDR                                                                (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_RMSK                                                                0xc0007fff
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                                              0x80000000
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                                    0x1f
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                                               0x40000000
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                                     0x1e
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                                         0x7fff
#define HWIO_APCS_ALIAS5_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                                            0x0

#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_ADDR                                                          (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_RMSK                                                          0xc0007fff
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK                                0x80000000
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                                      0x1f
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK                                 0x40000000
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                                       0x1e
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                                           0x7fff
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                                              0x0

#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_ADDR                                                           (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x0000005c)
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_RMSK                                                           0xc0007fff
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK                                  0x80000000
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                                        0x1f
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK                                   0x40000000
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                                         0x1e
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                                             0x7fff
#define HWIO_APCS_ALIAS5_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                                                0x0

#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_ADDR                                                           (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_RMSK                                                           0x80000001
#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_ADDR, HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_RMSK)
#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_ADDR, m)
#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_ADDR,v)
#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_ADDR,m,v,HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_IN)
#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_BMSK                                       0x80000000
#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_SHFT                                             0x1f
#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_BMSK                                              0x1
#define HWIO_APCS_ALIAS5_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_SHFT                                              0x0

#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_ADDR                                                             (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                                    0x4
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                                    0x2
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_IGNORE_BMSK                                                             0x2
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_IGNORE_SHFT                                                             0x1
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                                         0x1
#define HWIO_APCS_ALIAS5_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_ADDR                                                           (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000070)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_RMSK                                                                0x1ff
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_EN_BMSK                                                             0x1ff
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EN_EN_SHFT                                                               0x0

#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_ADDR                                                              (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000074)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_RMSK                                                                   0x1ff
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                                         0x1ff
#define HWIO_APCS_ALIAS5_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                                           0x0

#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_ADDR                                                         (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_RMSK                                                            0x1ffff
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                                         0x1ffff
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                                             0x0

#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_ADDR                                                            (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x0000007c)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_RMSK                                                               0x1ffff
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                                                   0x1ffff
#define HWIO_APCS_ALIAS5_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                                       0x0

#define HWIO_APCS_ALIAS5_SPM_EVENT_STS_ADDR                                                                (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000080)
#define HWIO_APCS_ALIAS5_SPM_EVENT_STS_RMSK                                                                     0x1ff
#define HWIO_APCS_ALIAS5_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS5_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS5_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_EVENT_STS_EVENT_VAL_BMSK                                                           0x1ff
#define HWIO_APCS_ALIAS5_SPM_EVENT_STS_EVENT_VAL_SHFT                                                             0x0

#define HWIO_APCS_ALIAS5_SPM_PWR_CTL_STS_ADDR                                                              (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000084)
#define HWIO_APCS_ALIAS5_SPM_PWR_CTL_STS_RMSK                                                                 0x1ffff
#define HWIO_APCS_ALIAS5_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS5_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS5_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                                     0x1ffff
#define HWIO_APCS_ALIAS5_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_ADDR                                                    (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_RMSK                                                           0xf
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                       0x8
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                       0x3
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_BMSK                               0x4
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_SHFT                               0x2
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_BMSK                              0x2
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_SHFT                              0x1
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_BMSK                              0x1
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_SHFT                              0x0

#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_ADDR                                                        (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_RMSK                                                        0xffffffff
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_BMSK                            0xffffffff
#define HWIO_APCS_ALIAS5_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_SHFT                                   0x0

#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR                                                 (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000a0)
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK                                                        0xf
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                 0x8
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                 0x3
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_BMSK                         0x4
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_SHFT                         0x2
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_BMSK                        0x2
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_SHFT                        0x1
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_BMSK                        0x1
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_SHFT                        0x0

#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_ADDR                                                     (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x0000009c)
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_BMSK                      0xffffffff
#define HWIO_APCS_ALIAS5_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_SHFT                             0x0

#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR                                                   (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000a8)
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK                                                          0xf
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                     0x8
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                     0x3
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_BMSK                             0x4
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_SHFT                             0x2
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_BMSK                            0x2
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_SHFT                            0x1
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_BMSK                            0x1
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_SHFT                            0x0

#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_ADDR                                                       (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000a4)
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_RMSK                                                       0xffffffff
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_BMSK                          0xffffffff
#define HWIO_APCS_ALIAS5_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_SHFT                                 0x0

#define HWIO_APCS_ALIAS5_SPARE_ADDR                                                                        (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000ff8)
#define HWIO_APCS_ALIAS5_SPARE_RMSK                                                                              0xff
#define HWIO_APCS_ALIAS5_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPARE_ADDR, HWIO_APCS_ALIAS5_SPARE_RMSK)
#define HWIO_APCS_ALIAS5_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS5_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS5_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPARE_ADDR,m,v,HWIO_APCS_ALIAS5_SPARE_IN)
#define HWIO_APCS_ALIAS5_SPARE_SPARE_BMSK                                                                        0xff
#define HWIO_APCS_ALIAS5_SPARE_SPARE_SHFT                                                                         0x0

#define HWIO_APCS_ALIAS5_SPARE_TZ_ADDR                                                                     (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_ALIAS5_SPARE_TZ_RMSK                                                                           0xff
#define HWIO_APCS_ALIAS5_SPARE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_SPARE_TZ_ADDR, HWIO_APCS_ALIAS5_SPARE_TZ_RMSK)
#define HWIO_APCS_ALIAS5_SPARE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_SPARE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS5_SPARE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_SPARE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS5_SPARE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_SPARE_TZ_ADDR,m,v,HWIO_APCS_ALIAS5_SPARE_TZ_IN)
#define HWIO_APCS_ALIAS5_SPARE_TZ_SPARE_BMSK                                                                     0xff
#define HWIO_APCS_ALIAS5_SPARE_TZ_SPARE_SHFT                                                                      0x0

#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_ADDR                                                     (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_RMSK                                                          0x7ff
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_ADDR, HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_RMSK)
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_ADDR, m)
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_ADDR,v)
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_ADDR,m,v,HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_IN)
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_BMSK                                  0x7f8
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_SHFT                                    0x3
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_BMSK                                      0x4
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_SHFT                                      0x2
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_BMSK                                0x2
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_SHFT                                0x1
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_BMSK                                0x1
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_SHFT                                0x0

#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_STATUS_ADDR                                                  (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x00000094)
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_STATUS_RMSK                                                  0xffffffff
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_STATUS_ADDR, HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_BMSK                           0xffffffff
#define HWIO_APCS_ALIAS5_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_SHFT                                  0x0

#define HWIO_APCS_ALIAS5_MAS_CFG_ADDR                                                                      (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000c0)
#define HWIO_APCS_ALIAS5_MAS_CFG_RMSK                                                                         0x10707
#define HWIO_APCS_ALIAS5_MAS_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_MAS_CFG_ADDR, HWIO_APCS_ALIAS5_MAS_CFG_RMSK)
#define HWIO_APCS_ALIAS5_MAS_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_MAS_CFG_ADDR, m)
#define HWIO_APCS_ALIAS5_MAS_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_MAS_CFG_ADDR,v)
#define HWIO_APCS_ALIAS5_MAS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_MAS_CFG_ADDR,m,v,HWIO_APCS_ALIAS5_MAS_CFG_IN)
#define HWIO_APCS_ALIAS5_MAS_CFG_MAS_MEM_CLAMP_EN_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS5_MAS_CFG_MAS_MEM_CLAMP_EN_SHFT                                                           0x10
#define HWIO_APCS_ALIAS5_MAS_CFG_MAS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS5_MAS_CFG_MAS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS5_MAS_CFG_MAS_APC_EN_REST_BMSK                                                             0x7
#define HWIO_APCS_ALIAS5_MAS_CFG_MAS_APC_EN_REST_SHFT                                                             0x0

#define HWIO_APCS_ALIAS5_APM_TILE_CFG_ADDR                                                                 (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000c4)
#define HWIO_APCS_ALIAS5_APM_TILE_CFG_RMSK                                                                      0x1ff
#define HWIO_APCS_ALIAS5_APM_TILE_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_APM_TILE_CFG_ADDR, HWIO_APCS_ALIAS5_APM_TILE_CFG_RMSK)
#define HWIO_APCS_ALIAS5_APM_TILE_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_APM_TILE_CFG_ADDR, m)
#define HWIO_APCS_ALIAS5_APM_TILE_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_APM_TILE_CFG_ADDR,v)
#define HWIO_APCS_ALIAS5_APM_TILE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_APM_TILE_CFG_ADDR,m,v,HWIO_APCS_ALIAS5_APM_TILE_CFG_IN)
#define HWIO_APCS_ALIAS5_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_BMSK                                               0x100
#define HWIO_APCS_ALIAS5_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_SHFT                                                 0x8
#define HWIO_APCS_ALIAS5_APM_TILE_CFG_APM_TILE_APMCONFIG_BMSK                                                    0xff
#define HWIO_APCS_ALIAS5_APM_TILE_CFG_APM_TILE_APMCONFIG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS5_MAS_STS_ADDR                                                                      (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000c8)
#define HWIO_APCS_ALIAS5_MAS_STS_RMSK                                                                        0x3ff373
#define HWIO_APCS_ALIAS5_MAS_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_MAS_STS_ADDR, HWIO_APCS_ALIAS5_MAS_STS_RMSK)
#define HWIO_APCS_ALIAS5_MAS_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_MAS_STS_ADDR, m)
#define HWIO_APCS_ALIAS5_MAS_STS_TMR_CNT_BMSK                                                                0x3f0000
#define HWIO_APCS_ALIAS5_MAS_STS_TMR_CNT_SHFT                                                                    0x10
#define HWIO_APCS_ALIAS5_MAS_STS_STEADY_SLEEP_BMSK                                                             0x8000
#define HWIO_APCS_ALIAS5_MAS_STS_STEADY_SLEEP_SHFT                                                                0xf
#define HWIO_APCS_ALIAS5_MAS_STS_STEADY_ACTIVE_BMSK                                                            0x4000
#define HWIO_APCS_ALIAS5_MAS_STS_STEADY_ACTIVE_SHFT                                                               0xe
#define HWIO_APCS_ALIAS5_MAS_STS_SPM_RET_BMSK                                                                  0x2000
#define HWIO_APCS_ALIAS5_MAS_STS_SPM_RET_SHFT                                                                     0xd
#define HWIO_APCS_ALIAS5_MAS_STS_SPM_PC_BMSK                                                                   0x1000
#define HWIO_APCS_ALIAS5_MAS_STS_SPM_PC_SHFT                                                                      0xc
#define HWIO_APCS_ALIAS5_MAS_STS_NEW_MODE_BMSK                                                                  0x300
#define HWIO_APCS_ALIAS5_MAS_STS_NEW_MODE_SHFT                                                                    0x8
#define HWIO_APCS_ALIAS5_MAS_STS_FSM_STATE_BMSK                                                                  0x70
#define HWIO_APCS_ALIAS5_MAS_STS_FSM_STATE_SHFT                                                                   0x4
#define HWIO_APCS_ALIAS5_MAS_STS_CURR_MODE_BMSK                                                                   0x3
#define HWIO_APCS_ALIAS5_MAS_STS_CURR_MODE_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS5_PWR_MUX_STS_ADDR                                                                  (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000cc)
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_RMSK                                                                     0x70773
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_PWR_MUX_STS_ADDR, HWIO_APCS_ALIAS5_PWR_MUX_STS_RMSK)
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_PWR_MUX_STS_ADDR, m)
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_CLAMP_BMSK                                                               0x40000
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_CLAMP_SHFT                                                                  0x12
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_TGL_SEL_BMSK                                                             0x20000
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_TGL_SEL_SHFT                                                                0x11
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_MEM_RET_BMSK                                                             0x10000
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_MEM_RET_SHFT                                                                0x10
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_APCC_EN_REST_BMSK                                                           0x70
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_APCC_EN_REST_SHFT                                                            0x4
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_EN_FEW_BMSK                                                                  0x3
#define HWIO_APCS_ALIAS5_PWR_MUX_STS_EN_FEW_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_ADDR                                                                 (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000d0)
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_RMSK                                                                   0x811377
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_MAS_OVERRIDE_ADDR, HWIO_APCS_ALIAS5_MAS_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_MAS_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_MAS_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_MAS_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS5_MAS_OVERRIDE_IN)
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_BMSK                                          0x800000
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_SHFT                                              0x17
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_BMSK                                 0x10000
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_SHFT                                    0x10
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_BMSK                             0x1000
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_SHFT                                0xc
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_BMSK                                       0x300
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_SHFT                                         0x8
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_BMSK                                     0x70
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_SHFT                                      0x4
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_BMSK                                     0x7
#define HWIO_APCS_ALIAS5_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_SHFT                                     0x0

#define HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_ADDR                                                          (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000b0)
#define HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_RMSK                                                          0xffffffff
#define HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_ADDR, HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_RMSK)
#define HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_ADDR, m)
#define HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_ADDR,v)
#define HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_ADDR,m,v,HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_IN)
#define HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_BMSK                                0xffffffff
#define HWIO_APCS_ALIAS5_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_SHFT                                       0x0

#define HWIO_APCS_ALIAS5_CORE_HANG_VALUE_ADDR                                                              (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000b4)
#define HWIO_APCS_ALIAS5_CORE_HANG_VALUE_RMSK                                                              0xffffffff
#define HWIO_APCS_ALIAS5_CORE_HANG_VALUE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_CORE_HANG_VALUE_ADDR, HWIO_APCS_ALIAS5_CORE_HANG_VALUE_RMSK)
#define HWIO_APCS_ALIAS5_CORE_HANG_VALUE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_CORE_HANG_VALUE_ADDR, m)
#define HWIO_APCS_ALIAS5_CORE_HANG_VALUE_VALUE_WHEN_HUNG_BMSK                                              0xffffffff
#define HWIO_APCS_ALIAS5_CORE_HANG_VALUE_VALUE_WHEN_HUNG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_ADDR                                                             (APCS_ALIAS5_APSS_ACS_REG_BASE      + 0x000000b8)
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_RMSK                                                                  0x1f7
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_ADDR, HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_RMSK)
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_ADDR, m)
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_ADDR,v)
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_ADDR,m,v,HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_IN)
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_PMUEVENT_SEL_BMSK                                                     0x1f0
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_PMUEVENT_SEL_SHFT                                                       0x4
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_BMSK                                           0x4
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_SHFT                                           0x2
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_BMSK                                               0x2
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_SHFT                                               0x1
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_CORE_HANG_STATUS_BMSK                                                   0x1
#define HWIO_APCS_ALIAS5_CORE_HANG_CONFIG_CORE_HANG_STATUS_SHFT                                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS6_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS6_APSS_ACS_REG_BASE                                                                      (A53SS_BASE      + 0x000a8000)

#define HWIO_APCS_ALIAS6_APC_SECURE_ADDR                                                                   (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS6_APC_SECURE_RMSK                                                                          0xf
#define HWIO_APCS_ALIAS6_APC_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_SECURE_ADDR, HWIO_APCS_ALIAS6_APC_SECURE_RMSK)
#define HWIO_APCS_ALIAS6_APC_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS6_APC_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_APC_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS6_APC_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_APC_SECURE_ADDR,m,v,HWIO_APCS_ALIAS6_APC_SECURE_IN)
#define HWIO_APCS_ALIAS6_APC_SECURE_VOTE_BMSK                                                                     0x8
#define HWIO_APCS_ALIAS6_APC_SECURE_VOTE_SHFT                                                                     0x3
#define HWIO_APCS_ALIAS6_APC_SECURE_TST_BMSK                                                                      0x4
#define HWIO_APCS_ALIAS6_APC_SECURE_TST_SHFT                                                                      0x2
#define HWIO_APCS_ALIAS6_APC_SECURE_CLK_CTL_BMSK                                                                  0x2
#define HWIO_APCS_ALIAS6_APC_SECURE_CLK_CTL_SHFT                                                                  0x1
#define HWIO_APCS_ALIAS6_APC_SECURE_SLP_CTL_BMSK                                                                  0x1
#define HWIO_APCS_ALIAS6_APC_SECURE_SLP_CTL_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR                                                                  (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_RMSK                                                                  0x200046ff
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS6_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS6_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                                  0x20000000
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                                        0x1d
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                                         0x4000
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                                            0xe
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                                      0x400
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                                        0xa
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                                        0x200
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                                          0x9
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                                           0x80
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                                            0x7
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_GATE_CLK_BMSK                                                               0x40
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_GATE_CLK_SHFT                                                                0x6
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_COREPOR_RST_BMSK                                                            0x20
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_COREPOR_RST_SHFT                                                             0x5
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_RST_BMSK                                                               0x10
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_RST_SHFT                                                                0x4
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                                             0x8
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                                             0x3
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_RST_DIS_BMSK                                                              0x4
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_L1_RST_DIS_SHFT                                                              0x2
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                                          0x2
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                                          0x1
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CLAMP_BMSK                                                                   0x1
#define HWIO_APCS_ALIAS6_CPU_PWR_CTL_CLAMP_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS6_APC_A53_CFG_STS_ADDR                                                              (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000040)
#define HWIO_APCS_ALIAS6_APC_A53_CFG_STS_RMSK                                                              0x10000000
#define HWIO_APCS_ALIAS6_APC_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS6_APC_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS6_APC_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS6_APC_A53_CFG_STS_SMPNAMP_BMSK                                                      0x10000000
#define HWIO_APCS_ALIAS6_APC_A53_CFG_STS_SMPNAMP_SHFT                                                            0x1c

#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_ADDR                                                               (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_RMSK                                                                0xb1f37ff
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_STATUS_ADDR, HWIO_APCS_ALIAS6_APC_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_SAW_SLP_ACK_BMSK                                                    0x8000000
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_SAW_SLP_ACK_SHFT                                                         0x1b
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_NO_PWR_DWN_BMSK                                                0x2000000
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_NO_PWR_DWN_SHFT                                                     0x19
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_PWRUP_REQ_BMSK                                                 0x1000000
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_PWRUP_REQ_SHFT                                                      0x18
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_HW_EVENT_BMSK                                                        0x1f0000
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_HW_EVENT_SHFT                                                            0x10
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_RET_SLP_ACK_BMSK                                                       0x2000
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_RET_SLP_ACK_SHFT                                                          0xd
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_RET_SLP_REQ_BMSK                                                       0x1000
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_RET_SLP_REQ_SHFT                                                          0xc
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_TRGTD_DBG_RST_BMSK                                                      0x400
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_TRGTD_DBG_RST_SHFT                                                        0xa
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_RST_BMSK                                                           0x200
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_RST_SHFT                                                             0x9
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_COREPOR_RST_BMSK                                                        0x100
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_COREPOR_RST_SHFT                                                          0x8
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_HS_STS_BMSK                                                              0x80
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_HS_STS_SHFT                                                               0x7
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_MEM_HS_STS_BMSK                                                     0x40
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_MEM_HS_STS_SHFT                                                      0x6
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CLAMP_BMSK                                                               0x20
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CLAMP_SHFT                                                                0x5
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_FRC_CLK_OFF_BMSK                                                         0x10
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_FRC_CLK_OFF_SHFT                                                          0x4
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_AHB_CLK_BMSK                                                              0x8
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_AHB_CLK_SHFT                                                              0x3
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_REF_CLK_BMSK                                                              0x4
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_REF_CLK_SHFT                                                              0x2
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_AUX_CLK_BMSK                                                         0x2
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_AUX_CLK_SHFT                                                         0x1
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_PLL_CLK_BMSK                                                         0x1
#define HWIO_APCS_ALIAS6_APC_PWR_STATUS_CORE_PLL_CLK_SHFT                                                         0x0

#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_ADDR                                                             (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_EN_BMSK                                                                 0x4
#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_EN_SHFT                                                                 0x2
#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_SEL_BMSK                                                                0x3
#define HWIO_APCS_ALIAS6_APC_TEST_BUS_SEL_SEL_SHFT                                                                0x0

#define HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_ADDR                                                            (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_RMSK                                                                   0x1
#define HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_ADDR, HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_RMSK)
#define HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_ADDR, m)
#define HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_ADDR,v)
#define HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_ADDR,m,v,HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_IN)
#define HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_RST_BMSK                                                               0x1
#define HWIO_APCS_ALIAS6_CPU_TRGTD_DBG_RST_RST_SHFT                                                               0x0

#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR                                                             (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_RMSK                                                             0xff000001
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_HS_CNT_BMSK                                                      0xff000000
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_HS_CNT_SHFT                                                            0x18
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_HS_EN_BMSK                                                              0x1
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_CTL_HS_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_ADDR                                                          (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_RMSK                                                             0x10003
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_IDLE_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_IDLE_SHFT                                                           0x10
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_EN_REST_BMSK                                                         0x2
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_EN_REST_SHFT                                                         0x1
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                                          0x1
#define HWIO_APCS_ALIAS6_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                                          0x0

#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_ADDR                                                            (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000044)
#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_RMSK                                                               0x101ff
#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_ADDR, HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_RMSK)
#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_IN)
#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_BMSK                                             0x10000
#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_SHFT                                                0x10
#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_AUX_STARTADDR_BMSK                                                   0x1ff
#define HWIO_APCS_ALIAS6_SPM_AUX_STARTADDR_AUX_STARTADDR_SHFT                                                     0x0

#define HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_ADDR                                                               (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_RMSK                                                                    0x3ff
#define HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_ADDR, HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_RMSK)
#define HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_ADDR, m)
#define HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_ADDR,v)
#define HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_ADDR,m,v,HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_IN)
#define HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_SLP_CNT_BMSK                                                            0x3ff
#define HWIO_APCS_ALIAS6_APC_L1_SLP_CNT_SLP_CNT_SHFT                                                              0x0

#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_ADDR                                                                  (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000050)
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_RMSK                                                                  0xc0007fff
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS6_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                                                0x80000000
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                                      0x1f
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                                                 0x40000000
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                                       0x1e
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                                           0x7fff
#define HWIO_APCS_ALIAS6_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_ADDR                                                                (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_RMSK                                                                0xc0007fff
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                                              0x80000000
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                                    0x1f
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                                               0x40000000
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                                     0x1e
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                                         0x7fff
#define HWIO_APCS_ALIAS6_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                                            0x0

#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_ADDR                                                          (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_RMSK                                                          0xc0007fff
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK                                0x80000000
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                                      0x1f
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK                                 0x40000000
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                                       0x1e
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                                           0x7fff
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                                              0x0

#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_ADDR                                                           (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x0000005c)
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_RMSK                                                           0xc0007fff
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK                                  0x80000000
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                                        0x1f
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK                                   0x40000000
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                                         0x1e
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                                             0x7fff
#define HWIO_APCS_ALIAS6_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                                                0x0

#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_ADDR                                                           (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_RMSK                                                           0x80000001
#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_ADDR, HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_RMSK)
#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_ADDR, m)
#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_ADDR,v)
#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_ADDR,m,v,HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_IN)
#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_BMSK                                       0x80000000
#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_SHFT                                             0x1f
#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_BMSK                                              0x1
#define HWIO_APCS_ALIAS6_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_SHFT                                              0x0

#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_ADDR                                                             (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                                    0x4
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                                    0x2
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_IGNORE_BMSK                                                             0x2
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_IGNORE_SHFT                                                             0x1
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                                         0x1
#define HWIO_APCS_ALIAS6_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_ADDR                                                           (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000070)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_RMSK                                                                0x1ff
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_EN_BMSK                                                             0x1ff
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EN_EN_SHFT                                                               0x0

#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_ADDR                                                              (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000074)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_RMSK                                                                   0x1ff
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                                         0x1ff
#define HWIO_APCS_ALIAS6_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                                           0x0

#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_ADDR                                                         (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_RMSK                                                            0x1ffff
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                                         0x1ffff
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                                             0x0

#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_ADDR                                                            (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x0000007c)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_RMSK                                                               0x1ffff
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                                                   0x1ffff
#define HWIO_APCS_ALIAS6_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                                       0x0

#define HWIO_APCS_ALIAS6_SPM_EVENT_STS_ADDR                                                                (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000080)
#define HWIO_APCS_ALIAS6_SPM_EVENT_STS_RMSK                                                                     0x1ff
#define HWIO_APCS_ALIAS6_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS6_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS6_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_EVENT_STS_EVENT_VAL_BMSK                                                           0x1ff
#define HWIO_APCS_ALIAS6_SPM_EVENT_STS_EVENT_VAL_SHFT                                                             0x0

#define HWIO_APCS_ALIAS6_SPM_PWR_CTL_STS_ADDR                                                              (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000084)
#define HWIO_APCS_ALIAS6_SPM_PWR_CTL_STS_RMSK                                                                 0x1ffff
#define HWIO_APCS_ALIAS6_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS6_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS6_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                                     0x1ffff
#define HWIO_APCS_ALIAS6_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_ADDR                                                    (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_RMSK                                                           0xf
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                       0x8
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                       0x3
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_BMSK                               0x4
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_SHFT                               0x2
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_BMSK                              0x2
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_SHFT                              0x1
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_BMSK                              0x1
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_SHFT                              0x0

#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_ADDR                                                        (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_RMSK                                                        0xffffffff
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_BMSK                            0xffffffff
#define HWIO_APCS_ALIAS6_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_SHFT                                   0x0

#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR                                                 (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000a0)
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK                                                        0xf
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                 0x8
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                 0x3
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_BMSK                         0x4
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_SHFT                         0x2
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_BMSK                        0x2
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_SHFT                        0x1
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_BMSK                        0x1
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_SHFT                        0x0

#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_ADDR                                                     (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x0000009c)
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_BMSK                      0xffffffff
#define HWIO_APCS_ALIAS6_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_SHFT                             0x0

#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR                                                   (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000a8)
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK                                                          0xf
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                     0x8
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                     0x3
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_BMSK                             0x4
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_SHFT                             0x2
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_BMSK                            0x2
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_SHFT                            0x1
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_BMSK                            0x1
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_SHFT                            0x0

#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_ADDR                                                       (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000a4)
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_RMSK                                                       0xffffffff
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_BMSK                          0xffffffff
#define HWIO_APCS_ALIAS6_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_SHFT                                 0x0

#define HWIO_APCS_ALIAS6_SPARE_ADDR                                                                        (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000ff8)
#define HWIO_APCS_ALIAS6_SPARE_RMSK                                                                              0xff
#define HWIO_APCS_ALIAS6_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPARE_ADDR, HWIO_APCS_ALIAS6_SPARE_RMSK)
#define HWIO_APCS_ALIAS6_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS6_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS6_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPARE_ADDR,m,v,HWIO_APCS_ALIAS6_SPARE_IN)
#define HWIO_APCS_ALIAS6_SPARE_SPARE_BMSK                                                                        0xff
#define HWIO_APCS_ALIAS6_SPARE_SPARE_SHFT                                                                         0x0

#define HWIO_APCS_ALIAS6_SPARE_TZ_ADDR                                                                     (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_ALIAS6_SPARE_TZ_RMSK                                                                           0xff
#define HWIO_APCS_ALIAS6_SPARE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_SPARE_TZ_ADDR, HWIO_APCS_ALIAS6_SPARE_TZ_RMSK)
#define HWIO_APCS_ALIAS6_SPARE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_SPARE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS6_SPARE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_SPARE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS6_SPARE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_SPARE_TZ_ADDR,m,v,HWIO_APCS_ALIAS6_SPARE_TZ_IN)
#define HWIO_APCS_ALIAS6_SPARE_TZ_SPARE_BMSK                                                                     0xff
#define HWIO_APCS_ALIAS6_SPARE_TZ_SPARE_SHFT                                                                      0x0

#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_ADDR                                                     (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_RMSK                                                          0x7ff
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_ADDR, HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_RMSK)
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_ADDR, m)
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_ADDR,v)
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_ADDR,m,v,HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_IN)
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_BMSK                                  0x7f8
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_SHFT                                    0x3
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_BMSK                                      0x4
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_SHFT                                      0x2
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_BMSK                                0x2
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_SHFT                                0x1
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_BMSK                                0x1
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_SHFT                                0x0

#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_STATUS_ADDR                                                  (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x00000094)
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_STATUS_RMSK                                                  0xffffffff
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_STATUS_ADDR, HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_BMSK                           0xffffffff
#define HWIO_APCS_ALIAS6_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_SHFT                                  0x0

#define HWIO_APCS_ALIAS6_MAS_CFG_ADDR                                                                      (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000c0)
#define HWIO_APCS_ALIAS6_MAS_CFG_RMSK                                                                         0x10707
#define HWIO_APCS_ALIAS6_MAS_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_MAS_CFG_ADDR, HWIO_APCS_ALIAS6_MAS_CFG_RMSK)
#define HWIO_APCS_ALIAS6_MAS_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_MAS_CFG_ADDR, m)
#define HWIO_APCS_ALIAS6_MAS_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_MAS_CFG_ADDR,v)
#define HWIO_APCS_ALIAS6_MAS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_MAS_CFG_ADDR,m,v,HWIO_APCS_ALIAS6_MAS_CFG_IN)
#define HWIO_APCS_ALIAS6_MAS_CFG_MAS_MEM_CLAMP_EN_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS6_MAS_CFG_MAS_MEM_CLAMP_EN_SHFT                                                           0x10
#define HWIO_APCS_ALIAS6_MAS_CFG_MAS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS6_MAS_CFG_MAS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS6_MAS_CFG_MAS_APC_EN_REST_BMSK                                                             0x7
#define HWIO_APCS_ALIAS6_MAS_CFG_MAS_APC_EN_REST_SHFT                                                             0x0

#define HWIO_APCS_ALIAS6_APM_TILE_CFG_ADDR                                                                 (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000c4)
#define HWIO_APCS_ALIAS6_APM_TILE_CFG_RMSK                                                                      0x1ff
#define HWIO_APCS_ALIAS6_APM_TILE_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_APM_TILE_CFG_ADDR, HWIO_APCS_ALIAS6_APM_TILE_CFG_RMSK)
#define HWIO_APCS_ALIAS6_APM_TILE_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_APM_TILE_CFG_ADDR, m)
#define HWIO_APCS_ALIAS6_APM_TILE_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_APM_TILE_CFG_ADDR,v)
#define HWIO_APCS_ALIAS6_APM_TILE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_APM_TILE_CFG_ADDR,m,v,HWIO_APCS_ALIAS6_APM_TILE_CFG_IN)
#define HWIO_APCS_ALIAS6_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_BMSK                                               0x100
#define HWIO_APCS_ALIAS6_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_SHFT                                                 0x8
#define HWIO_APCS_ALIAS6_APM_TILE_CFG_APM_TILE_APMCONFIG_BMSK                                                    0xff
#define HWIO_APCS_ALIAS6_APM_TILE_CFG_APM_TILE_APMCONFIG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS6_MAS_STS_ADDR                                                                      (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000c8)
#define HWIO_APCS_ALIAS6_MAS_STS_RMSK                                                                        0x3ff373
#define HWIO_APCS_ALIAS6_MAS_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_MAS_STS_ADDR, HWIO_APCS_ALIAS6_MAS_STS_RMSK)
#define HWIO_APCS_ALIAS6_MAS_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_MAS_STS_ADDR, m)
#define HWIO_APCS_ALIAS6_MAS_STS_TMR_CNT_BMSK                                                                0x3f0000
#define HWIO_APCS_ALIAS6_MAS_STS_TMR_CNT_SHFT                                                                    0x10
#define HWIO_APCS_ALIAS6_MAS_STS_STEADY_SLEEP_BMSK                                                             0x8000
#define HWIO_APCS_ALIAS6_MAS_STS_STEADY_SLEEP_SHFT                                                                0xf
#define HWIO_APCS_ALIAS6_MAS_STS_STEADY_ACTIVE_BMSK                                                            0x4000
#define HWIO_APCS_ALIAS6_MAS_STS_STEADY_ACTIVE_SHFT                                                               0xe
#define HWIO_APCS_ALIAS6_MAS_STS_SPM_RET_BMSK                                                                  0x2000
#define HWIO_APCS_ALIAS6_MAS_STS_SPM_RET_SHFT                                                                     0xd
#define HWIO_APCS_ALIAS6_MAS_STS_SPM_PC_BMSK                                                                   0x1000
#define HWIO_APCS_ALIAS6_MAS_STS_SPM_PC_SHFT                                                                      0xc
#define HWIO_APCS_ALIAS6_MAS_STS_NEW_MODE_BMSK                                                                  0x300
#define HWIO_APCS_ALIAS6_MAS_STS_NEW_MODE_SHFT                                                                    0x8
#define HWIO_APCS_ALIAS6_MAS_STS_FSM_STATE_BMSK                                                                  0x70
#define HWIO_APCS_ALIAS6_MAS_STS_FSM_STATE_SHFT                                                                   0x4
#define HWIO_APCS_ALIAS6_MAS_STS_CURR_MODE_BMSK                                                                   0x3
#define HWIO_APCS_ALIAS6_MAS_STS_CURR_MODE_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS6_PWR_MUX_STS_ADDR                                                                  (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000cc)
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_RMSK                                                                     0x70773
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_PWR_MUX_STS_ADDR, HWIO_APCS_ALIAS6_PWR_MUX_STS_RMSK)
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_PWR_MUX_STS_ADDR, m)
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_CLAMP_BMSK                                                               0x40000
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_CLAMP_SHFT                                                                  0x12
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_TGL_SEL_BMSK                                                             0x20000
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_TGL_SEL_SHFT                                                                0x11
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_MEM_RET_BMSK                                                             0x10000
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_MEM_RET_SHFT                                                                0x10
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_APCC_EN_REST_BMSK                                                           0x70
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_APCC_EN_REST_SHFT                                                            0x4
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_EN_FEW_BMSK                                                                  0x3
#define HWIO_APCS_ALIAS6_PWR_MUX_STS_EN_FEW_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_ADDR                                                                 (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000d0)
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_RMSK                                                                   0x811377
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_MAS_OVERRIDE_ADDR, HWIO_APCS_ALIAS6_MAS_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_MAS_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_MAS_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_MAS_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS6_MAS_OVERRIDE_IN)
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_BMSK                                          0x800000
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_SHFT                                              0x17
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_BMSK                                 0x10000
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_SHFT                                    0x10
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_BMSK                             0x1000
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_SHFT                                0xc
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_BMSK                                       0x300
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_SHFT                                         0x8
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_BMSK                                     0x70
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_SHFT                                      0x4
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_BMSK                                     0x7
#define HWIO_APCS_ALIAS6_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_SHFT                                     0x0

#define HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_ADDR                                                          (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000b0)
#define HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_RMSK                                                          0xffffffff
#define HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_ADDR, HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_RMSK)
#define HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_ADDR, m)
#define HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_ADDR,v)
#define HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_ADDR,m,v,HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_IN)
#define HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_BMSK                                0xffffffff
#define HWIO_APCS_ALIAS6_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_SHFT                                       0x0

#define HWIO_APCS_ALIAS6_CORE_HANG_VALUE_ADDR                                                              (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000b4)
#define HWIO_APCS_ALIAS6_CORE_HANG_VALUE_RMSK                                                              0xffffffff
#define HWIO_APCS_ALIAS6_CORE_HANG_VALUE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_CORE_HANG_VALUE_ADDR, HWIO_APCS_ALIAS6_CORE_HANG_VALUE_RMSK)
#define HWIO_APCS_ALIAS6_CORE_HANG_VALUE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_CORE_HANG_VALUE_ADDR, m)
#define HWIO_APCS_ALIAS6_CORE_HANG_VALUE_VALUE_WHEN_HUNG_BMSK                                              0xffffffff
#define HWIO_APCS_ALIAS6_CORE_HANG_VALUE_VALUE_WHEN_HUNG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_ADDR                                                             (APCS_ALIAS6_APSS_ACS_REG_BASE      + 0x000000b8)
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_RMSK                                                                  0x1f7
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_ADDR, HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_RMSK)
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_ADDR, m)
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_ADDR,v)
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_ADDR,m,v,HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_IN)
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_PMUEVENT_SEL_BMSK                                                     0x1f0
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_PMUEVENT_SEL_SHFT                                                       0x4
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_BMSK                                           0x4
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_SHFT                                           0x2
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_BMSK                                               0x2
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_SHFT                                               0x1
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_CORE_HANG_STATUS_BMSK                                                   0x1
#define HWIO_APCS_ALIAS6_CORE_HANG_CONFIG_CORE_HANG_STATUS_SHFT                                                   0x0

/*----------------------------------------------------------------------------
 * MODULE: APCS_ALIAS7_APSS_ACS
 *--------------------------------------------------------------------------*/

#define APCS_ALIAS7_APSS_ACS_REG_BASE                                                                      (A53SS_BASE      + 0x000b8000)

#define HWIO_APCS_ALIAS7_APC_SECURE_ADDR                                                                   (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000000)
#define HWIO_APCS_ALIAS7_APC_SECURE_RMSK                                                                          0xf
#define HWIO_APCS_ALIAS7_APC_SECURE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_SECURE_ADDR, HWIO_APCS_ALIAS7_APC_SECURE_RMSK)
#define HWIO_APCS_ALIAS7_APC_SECURE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_SECURE_ADDR, m)
#define HWIO_APCS_ALIAS7_APC_SECURE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_APC_SECURE_ADDR,v)
#define HWIO_APCS_ALIAS7_APC_SECURE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_APC_SECURE_ADDR,m,v,HWIO_APCS_ALIAS7_APC_SECURE_IN)
#define HWIO_APCS_ALIAS7_APC_SECURE_VOTE_BMSK                                                                     0x8
#define HWIO_APCS_ALIAS7_APC_SECURE_VOTE_SHFT                                                                     0x3
#define HWIO_APCS_ALIAS7_APC_SECURE_TST_BMSK                                                                      0x4
#define HWIO_APCS_ALIAS7_APC_SECURE_TST_SHFT                                                                      0x2
#define HWIO_APCS_ALIAS7_APC_SECURE_CLK_CTL_BMSK                                                                  0x2
#define HWIO_APCS_ALIAS7_APC_SECURE_CLK_CTL_SHFT                                                                  0x1
#define HWIO_APCS_ALIAS7_APC_SECURE_SLP_CTL_BMSK                                                                  0x1
#define HWIO_APCS_ALIAS7_APC_SECURE_SLP_CTL_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR                                                                  (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000004)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_RMSK                                                                  0x200046ff
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR, HWIO_APCS_ALIAS7_CPU_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_CPU_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS7_CPU_PWR_CTL_IN)
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_SPM_WAKEUP_MASK_BMSK                                                  0x20000000
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_SPM_WAKEUP_MASK_SHFT                                                        0x1d
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_WL_EN_CLK_BMSK                                                         0x4000
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_WL_EN_CLK_SHFT                                                            0xe
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_SLEEP_STATE_BMSK                                                      0x400
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_SLEEP_STATE_SHFT                                                        0xa
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_RET_N_BMSK                                                        0x200
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_RET_N_SHFT                                                          0x9
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_PWRD_UP_BMSK                                                           0x80
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_PWRD_UP_SHFT                                                            0x7
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_GATE_CLK_BMSK                                                               0x40
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_GATE_CLK_SHFT                                                                0x6
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_COREPOR_RST_BMSK                                                            0x20
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_COREPOR_RST_SHFT                                                             0x5
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_RST_BMSK                                                               0x10
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_RST_SHFT                                                                0x4
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_HS_BMSK                                                             0x8
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_HS_SHFT                                                             0x3
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_RST_DIS_BMSK                                                              0x4
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_L1_RST_DIS_SHFT                                                              0x2
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_CLAMP_BMSK                                                          0x2
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CORE_MEM_CLAMP_SHFT                                                          0x1
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CLAMP_BMSK                                                                   0x1
#define HWIO_APCS_ALIAS7_CPU_PWR_CTL_CLAMP_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS7_APC_A53_CFG_STS_ADDR                                                              (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000040)
#define HWIO_APCS_ALIAS7_APC_A53_CFG_STS_RMSK                                                              0x10000000
#define HWIO_APCS_ALIAS7_APC_A53_CFG_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_A53_CFG_STS_ADDR, HWIO_APCS_ALIAS7_APC_A53_CFG_STS_RMSK)
#define HWIO_APCS_ALIAS7_APC_A53_CFG_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_A53_CFG_STS_ADDR, m)
#define HWIO_APCS_ALIAS7_APC_A53_CFG_STS_SMPNAMP_BMSK                                                      0x10000000
#define HWIO_APCS_ALIAS7_APC_A53_CFG_STS_SMPNAMP_SHFT                                                            0x1c

#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_ADDR                                                               (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000008)
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_RMSK                                                                0xb1f37ff
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_STATUS_ADDR, HWIO_APCS_ALIAS7_APC_PWR_STATUS_RMSK)
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_SAW_SLP_ACK_BMSK                                                    0x8000000
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_SAW_SLP_ACK_SHFT                                                         0x1b
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_NO_PWR_DWN_BMSK                                                0x2000000
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_NO_PWR_DWN_SHFT                                                     0x19
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_PWRUP_REQ_BMSK                                                 0x1000000
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_PWRUP_REQ_SHFT                                                      0x18
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_HW_EVENT_BMSK                                                        0x1f0000
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_HW_EVENT_SHFT                                                            0x10
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_RET_SLP_ACK_BMSK                                                       0x2000
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_RET_SLP_ACK_SHFT                                                          0xd
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_RET_SLP_REQ_BMSK                                                       0x1000
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_RET_SLP_REQ_SHFT                                                          0xc
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_TRGTD_DBG_RST_BMSK                                                      0x400
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_TRGTD_DBG_RST_SHFT                                                        0xa
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_RST_BMSK                                                           0x200
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_RST_SHFT                                                             0x9
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_COREPOR_RST_BMSK                                                        0x100
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_COREPOR_RST_SHFT                                                          0x8
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_HS_STS_BMSK                                                              0x80
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_HS_STS_SHFT                                                               0x7
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_MEM_HS_STS_BMSK                                                     0x40
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_MEM_HS_STS_SHFT                                                      0x6
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CLAMP_BMSK                                                               0x20
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CLAMP_SHFT                                                                0x5
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_FRC_CLK_OFF_BMSK                                                         0x10
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_FRC_CLK_OFF_SHFT                                                          0x4
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_AHB_CLK_BMSK                                                              0x8
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_AHB_CLK_SHFT                                                              0x3
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_REF_CLK_BMSK                                                              0x4
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_REF_CLK_SHFT                                                              0x2
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_AUX_CLK_BMSK                                                         0x2
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_AUX_CLK_SHFT                                                         0x1
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_PLL_CLK_BMSK                                                         0x1
#define HWIO_APCS_ALIAS7_APC_PWR_STATUS_CORE_PLL_CLK_SHFT                                                         0x0

#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_ADDR                                                             (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x0000000c)
#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_ADDR, HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_RMSK)
#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_ADDR, m)
#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_ADDR,v)
#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_ADDR,m,v,HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_IN)
#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_EN_BMSK                                                                 0x4
#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_EN_SHFT                                                                 0x2
#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_SEL_BMSK                                                                0x3
#define HWIO_APCS_ALIAS7_APC_TEST_BUS_SEL_SEL_SHFT                                                                0x0

#define HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_ADDR                                                            (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000010)
#define HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_RMSK                                                                   0x1
#define HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_ADDR, HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_RMSK)
#define HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_ADDR, m)
#define HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_ADDR,v)
#define HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_ADDR,m,v,HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_IN)
#define HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_RST_BMSK                                                               0x1
#define HWIO_APCS_ALIAS7_CPU_TRGTD_DBG_RST_RST_SHFT                                                               0x0

#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR                                                             (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000014)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_RMSK                                                             0xff000001
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR, HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_RMSK)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR, m)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR,v)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_ADDR,m,v,HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_IN)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_HS_CNT_BMSK                                                      0xff000000
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_HS_CNT_SHFT                                                            0x18
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_HS_EN_BMSK                                                              0x1
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_CTL_HS_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_ADDR                                                          (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000030)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_RMSK                                                             0x10003
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_ADDR, HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_RMSK)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_IDLE_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_IDLE_SHFT                                                           0x10
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_EN_REST_BMSK                                                         0x2
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_EN_REST_SHFT                                                         0x1
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_EN_FEW_BMSK                                                          0x1
#define HWIO_APCS_ALIAS7_APC_PWR_GATE_STATUS_EN_FEW_SHFT                                                          0x0

#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_ADDR                                                            (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000044)
#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_RMSK                                                               0x101ff
#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_ADDR, HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_RMSK)
#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_IN)
#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_BMSK                                             0x10000
#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_AUX_STARTADDR_SEL_SHFT                                                0x10
#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_AUX_STARTADDR_BMSK                                                   0x1ff
#define HWIO_APCS_ALIAS7_SPM_AUX_STARTADDR_AUX_STARTADDR_SHFT                                                     0x0

#define HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_ADDR                                                               (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000034)
#define HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_RMSK                                                                    0x3ff
#define HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_ADDR, HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_RMSK)
#define HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_ADDR, m)
#define HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_ADDR,v)
#define HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_ADDR,m,v,HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_IN)
#define HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_SLP_CNT_BMSK                                                            0x3ff
#define HWIO_APCS_ALIAS7_APC_L1_SLP_CNT_SLP_CNT_SHFT                                                              0x0

#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_ADDR                                                                  (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000050)
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_RMSK                                                                  0xc0007fff
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_VOTE_TZ_ADDR, HWIO_APCS_ALIAS7_SPM_VOTE_TZ_RMSK)
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_VOTE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_VOTE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_VOTE_TZ_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_VOTE_TZ_IN)
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_BMSK                                                0x80000000
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_PMIC_HANDSHAKE_EN_SHFT                                                      0x1f
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_BMSK                                                 0x40000000
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_RPM_HANDSHAKE_EN_SHFT                                                       0x1e
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_PWR_CTL_EN_BMSK                                                           0x7fff
#define HWIO_APCS_ALIAS7_SPM_VOTE_TZ_PWR_CTL_EN_SHFT                                                              0x0

#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_ADDR                                                                (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000054)
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_RMSK                                                                0xc0007fff
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_ADDR, HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_RMSK)
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_IN)
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_BMSK                                              0x80000000
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_PMIC_HANDSHAKE_EN_SHFT                                                    0x1f
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_BMSK                                               0x40000000
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_RPM_HANDSHAKE_EN_SHFT                                                     0x1e
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_PWR_CTL_EN_BMSK                                                         0x7fff
#define HWIO_APCS_ALIAS7_SPM_VOTE_HLOS_PWR_CTL_EN_SHFT                                                            0x0

#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_ADDR                                                          (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000058)
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_RMSK                                                          0xc0007fff
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_ADDR, HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_RMSK)
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_BMSK                                0x80000000
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_PMIC_HANDSHAKE_INT_STATUS_SHFT                                      0x1f
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_BMSK                                 0x40000000
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_RPM_HANDSHAKE_INT_STATUS_SHFT                                       0x1e
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_BMSK                                           0x7fff
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_STATUS_PWR_CTL_INT_STATUS_SHFT                                              0x0

#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_ADDR                                                           (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x0000005c)
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_RMSK                                                           0xc0007fff
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_BMSK                                  0x80000000
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_PMIC_HANDSHAKE_INT_CLEAR_SHFT                                        0x1f
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_BMSK                                   0x40000000
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_RPM_HANDSHAKE_INT_CLEAR_SHFT                                         0x1e
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_BMSK                                             0x7fff
#define HWIO_APCS_ALIAS7_SPM_VOTE_INT_CLEAR_PWR_CTL_INT_CLEAR_SHFT                                                0x0

#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_ADDR                                                           (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000060)
#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_RMSK                                                           0x80000001
#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_ADDR, HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_RMSK)
#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_ADDR, m)
#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_ADDR,v)
#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_ADDR,m,v,HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_IN)
#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_BMSK                                       0x80000000
#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_GCC_APCSDBGPWRUPACK_SHFT                                             0x1f
#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_BMSK                                              0x1
#define HWIO_APCS_ALIAS7_GCC_DBG_CLK_ON_REQ_APCS_GCCDBGPWRUPREQ_SHFT                                              0x0

#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_ADDR                                                             (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000064)
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_RMSK                                                                    0x7
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_ADDR, HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_RMSK)
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_IN)
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_BMSK                                                    0x4
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_SPM_LEGACY_MODE_SHFT                                                    0x2
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_IGNORE_BMSK                                                             0x2
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_IGNORE_SHFT                                                             0x1
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_SW_CONTROL_BMSK                                                         0x1
#define HWIO_APCS_ALIAS7_SPM_QCHANNEL_CFG_SW_CONTROL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_ADDR                                                           (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000070)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_RMSK                                                                0x1ff
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_ADDR, HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_RMSK)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_IN)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_EN_BMSK                                                             0x1ff
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EN_EN_SHFT                                                               0x0

#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_ADDR                                                              (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000074)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_RMSK                                                                   0x1ff
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_ADDR, HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_RMSK)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_IN)
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EVENT_VAL_BMSK                                                         0x1ff
#define HWIO_APCS_ALIAS7_SPM_FORCE_EVENT_EVENT_VAL_SHFT                                                           0x0

#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_ADDR                                                         (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000078)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_RMSK                                                            0x1ffff
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_ADDR, HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_RMSK)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_IN)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_EN_BMSK                                                         0x1ffff
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_EN_EN_SHFT                                                             0x0

#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_ADDR                                                            (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x0000007c)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_RMSK                                                               0x1ffff
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_ADDR, HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_RMSK)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_IN)
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_BMSK                                                   0x1ffff
#define HWIO_APCS_ALIAS7_SPM_FORCE_PWR_CTL_PWR_CTL_VAL_SHFT                                                       0x0

#define HWIO_APCS_ALIAS7_SPM_EVENT_STS_ADDR                                                                (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000080)
#define HWIO_APCS_ALIAS7_SPM_EVENT_STS_RMSK                                                                     0x1ff
#define HWIO_APCS_ALIAS7_SPM_EVENT_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_EVENT_STS_ADDR, HWIO_APCS_ALIAS7_SPM_EVENT_STS_RMSK)
#define HWIO_APCS_ALIAS7_SPM_EVENT_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_EVENT_STS_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_EVENT_STS_EVENT_VAL_BMSK                                                           0x1ff
#define HWIO_APCS_ALIAS7_SPM_EVENT_STS_EVENT_VAL_SHFT                                                             0x0

#define HWIO_APCS_ALIAS7_SPM_PWR_CTL_STS_ADDR                                                              (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000084)
#define HWIO_APCS_ALIAS7_SPM_PWR_CTL_STS_RMSK                                                                 0x1ffff
#define HWIO_APCS_ALIAS7_SPM_PWR_CTL_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_PWR_CTL_STS_ADDR, HWIO_APCS_ALIAS7_SPM_PWR_CTL_STS_RMSK)
#define HWIO_APCS_ALIAS7_SPM_PWR_CTL_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_PWR_CTL_STS_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_PWR_CTL_STS_PWR_CTL_VAL_BMSK                                                     0x1ffff
#define HWIO_APCS_ALIAS7_SPM_PWR_CTL_STS_PWR_CTL_VAL_SHFT                                                         0x0

#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_ADDR                                                    (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x0000008c)
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_RMSK                                                           0xf
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                       0x8
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                       0x3
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_BMSK                               0x4
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_RESET_SHFT                               0x2
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_BMSK                              0x2
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_FREEZE_SHFT                              0x1
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_BMSK                              0x1
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_CFG_SPM_PC_WAKEUP_COUNTER_CLK_EN_SHFT                              0x0

#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_ADDR                                                        (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000088)
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_RMSK                                                        0xffffffff
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_BMSK                            0xffffffff
#define HWIO_APCS_ALIAS7_SPM_PC_WAKEUP_COUNTER_SPM_PC_WAKEUP_COUNTER_VALUE_SHFT                                   0x0

#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR                                                 (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000a0)
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK                                                        0xf
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                 0x8
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                 0x3
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_BMSK                         0x4
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_RESET_SHFT                         0x2
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_BMSK                        0x2
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_FREEZE_SHFT                        0x1
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_BMSK                        0x1
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_CFG_SPM_STDBY_WAKEUP_COUNTER_CLK_EN_SHFT                        0x0

#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_ADDR                                                     (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x0000009c)
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_RMSK                                                     0xffffffff
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_BMSK                      0xffffffff
#define HWIO_APCS_ALIAS7_SPM_STDBY_WAKEUP_COUNTER_SPM_STDBY_WAKEUP_COUNTER_VALUE_SHFT                             0x0

#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR                                                   (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000a8)
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK                                                          0xf
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_RMSK)
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,v)
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_ADDR,m,v,HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_IN)
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_BMSK                     0x8
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_OVERFLOW_FLAG_SHFT                     0x3
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_BMSK                             0x4
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_RESET_SHFT                             0x2
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_BMSK                            0x2
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_FREEZE_SHFT                            0x1
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_BMSK                            0x1
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_CFG_SPM_C2D_WAKEUP_COUNTER_CLK_EN_SHFT                            0x0

#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_ADDR                                                       (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000a4)
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_RMSK                                                       0xffffffff
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_ADDR, HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_RMSK)
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_ADDR, m)
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_BMSK                          0xffffffff
#define HWIO_APCS_ALIAS7_SPM_C2D_WAKEUP_COUNTER_SPM_C2D_WAKEUP_COUNTER_VALUE_SHFT                                 0x0

#define HWIO_APCS_ALIAS7_SPARE_ADDR                                                                        (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000ff8)
#define HWIO_APCS_ALIAS7_SPARE_RMSK                                                                              0xff
#define HWIO_APCS_ALIAS7_SPARE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPARE_ADDR, HWIO_APCS_ALIAS7_SPARE_RMSK)
#define HWIO_APCS_ALIAS7_SPARE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPARE_ADDR, m)
#define HWIO_APCS_ALIAS7_SPARE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPARE_ADDR,v)
#define HWIO_APCS_ALIAS7_SPARE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPARE_ADDR,m,v,HWIO_APCS_ALIAS7_SPARE_IN)
#define HWIO_APCS_ALIAS7_SPARE_SPARE_BMSK                                                                        0xff
#define HWIO_APCS_ALIAS7_SPARE_SPARE_SHFT                                                                         0x0

#define HWIO_APCS_ALIAS7_SPARE_TZ_ADDR                                                                     (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000ffc)
#define HWIO_APCS_ALIAS7_SPARE_TZ_RMSK                                                                           0xff
#define HWIO_APCS_ALIAS7_SPARE_TZ_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_SPARE_TZ_ADDR, HWIO_APCS_ALIAS7_SPARE_TZ_RMSK)
#define HWIO_APCS_ALIAS7_SPARE_TZ_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_SPARE_TZ_ADDR, m)
#define HWIO_APCS_ALIAS7_SPARE_TZ_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_SPARE_TZ_ADDR,v)
#define HWIO_APCS_ALIAS7_SPARE_TZ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_SPARE_TZ_ADDR,m,v,HWIO_APCS_ALIAS7_SPARE_TZ_IN)
#define HWIO_APCS_ALIAS7_SPARE_TZ_SPARE_BMSK                                                                     0xff
#define HWIO_APCS_ALIAS7_SPARE_TZ_SPARE_SHFT                                                                      0x0

#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_ADDR                                                     (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000090)
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_RMSK                                                          0x7ff
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_ADDR, HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_RMSK)
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_ADDR, m)
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_ADDR,v)
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_ADDR,m,v,HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_IN)
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_BMSK                                  0x7f8
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_DELAY_COUNT_SHFT                                    0x3
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_BMSK                                      0x4
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_SHFT                                      0x2
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_BMSK                                0x2
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_QREQN_CSR_CNTRL_SHFT                                0x1
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_BMSK                                0x1
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_CTL_QCHANNEL_C1_STAGGER_DISBALE_SHFT                                0x0

#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_STATUS_ADDR                                                  (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x00000094)
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_STATUS_RMSK                                                  0xffffffff
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_STATUS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_STATUS_ADDR, HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_STATUS_RMSK)
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_STATUS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_STATUS_ADDR, m)
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_BMSK                           0xffffffff
#define HWIO_APCS_ALIAS7_QCHANNEL_STANDBY_FSM_STATUS_QCHANNEL_C1_FSM_STATUS_SHFT                                  0x0

#define HWIO_APCS_ALIAS7_MAS_CFG_ADDR                                                                      (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000c0)
#define HWIO_APCS_ALIAS7_MAS_CFG_RMSK                                                                         0x10707
#define HWIO_APCS_ALIAS7_MAS_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_MAS_CFG_ADDR, HWIO_APCS_ALIAS7_MAS_CFG_RMSK)
#define HWIO_APCS_ALIAS7_MAS_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_MAS_CFG_ADDR, m)
#define HWIO_APCS_ALIAS7_MAS_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_MAS_CFG_ADDR,v)
#define HWIO_APCS_ALIAS7_MAS_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_MAS_CFG_ADDR,m,v,HWIO_APCS_ALIAS7_MAS_CFG_IN)
#define HWIO_APCS_ALIAS7_MAS_CFG_MAS_MEM_CLAMP_EN_BMSK                                                        0x10000
#define HWIO_APCS_ALIAS7_MAS_CFG_MAS_MEM_CLAMP_EN_SHFT                                                           0x10
#define HWIO_APCS_ALIAS7_MAS_CFG_MAS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS7_MAS_CFG_MAS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS7_MAS_CFG_MAS_APC_EN_REST_BMSK                                                             0x7
#define HWIO_APCS_ALIAS7_MAS_CFG_MAS_APC_EN_REST_SHFT                                                             0x0

#define HWIO_APCS_ALIAS7_APM_TILE_CFG_ADDR                                                                 (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000c4)
#define HWIO_APCS_ALIAS7_APM_TILE_CFG_RMSK                                                                      0x1ff
#define HWIO_APCS_ALIAS7_APM_TILE_CFG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_APM_TILE_CFG_ADDR, HWIO_APCS_ALIAS7_APM_TILE_CFG_RMSK)
#define HWIO_APCS_ALIAS7_APM_TILE_CFG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_APM_TILE_CFG_ADDR, m)
#define HWIO_APCS_ALIAS7_APM_TILE_CFG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_APM_TILE_CFG_ADDR,v)
#define HWIO_APCS_ALIAS7_APM_TILE_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_APM_TILE_CFG_ADDR,m,v,HWIO_APCS_ALIAS7_APM_TILE_CFG_IN)
#define HWIO_APCS_ALIAS7_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_BMSK                                               0x100
#define HWIO_APCS_ALIAS7_APM_TILE_CFG_APM_TILE_APMAONOVRRIDE_SHFT                                                 0x8
#define HWIO_APCS_ALIAS7_APM_TILE_CFG_APM_TILE_APMCONFIG_BMSK                                                    0xff
#define HWIO_APCS_ALIAS7_APM_TILE_CFG_APM_TILE_APMCONFIG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS7_MAS_STS_ADDR                                                                      (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000c8)
#define HWIO_APCS_ALIAS7_MAS_STS_RMSK                                                                        0x3ff373
#define HWIO_APCS_ALIAS7_MAS_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_MAS_STS_ADDR, HWIO_APCS_ALIAS7_MAS_STS_RMSK)
#define HWIO_APCS_ALIAS7_MAS_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_MAS_STS_ADDR, m)
#define HWIO_APCS_ALIAS7_MAS_STS_TMR_CNT_BMSK                                                                0x3f0000
#define HWIO_APCS_ALIAS7_MAS_STS_TMR_CNT_SHFT                                                                    0x10
#define HWIO_APCS_ALIAS7_MAS_STS_STEADY_SLEEP_BMSK                                                             0x8000
#define HWIO_APCS_ALIAS7_MAS_STS_STEADY_SLEEP_SHFT                                                                0xf
#define HWIO_APCS_ALIAS7_MAS_STS_STEADY_ACTIVE_BMSK                                                            0x4000
#define HWIO_APCS_ALIAS7_MAS_STS_STEADY_ACTIVE_SHFT                                                               0xe
#define HWIO_APCS_ALIAS7_MAS_STS_SPM_RET_BMSK                                                                  0x2000
#define HWIO_APCS_ALIAS7_MAS_STS_SPM_RET_SHFT                                                                     0xd
#define HWIO_APCS_ALIAS7_MAS_STS_SPM_PC_BMSK                                                                   0x1000
#define HWIO_APCS_ALIAS7_MAS_STS_SPM_PC_SHFT                                                                      0xc
#define HWIO_APCS_ALIAS7_MAS_STS_NEW_MODE_BMSK                                                                  0x300
#define HWIO_APCS_ALIAS7_MAS_STS_NEW_MODE_SHFT                                                                    0x8
#define HWIO_APCS_ALIAS7_MAS_STS_FSM_STATE_BMSK                                                                  0x70
#define HWIO_APCS_ALIAS7_MAS_STS_FSM_STATE_SHFT                                                                   0x4
#define HWIO_APCS_ALIAS7_MAS_STS_CURR_MODE_BMSK                                                                   0x3
#define HWIO_APCS_ALIAS7_MAS_STS_CURR_MODE_SHFT                                                                   0x0

#define HWIO_APCS_ALIAS7_PWR_MUX_STS_ADDR                                                                  (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000cc)
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_RMSK                                                                     0x70773
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_PWR_MUX_STS_ADDR, HWIO_APCS_ALIAS7_PWR_MUX_STS_RMSK)
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_PWR_MUX_STS_ADDR, m)
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_CLAMP_BMSK                                                               0x40000
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_CLAMP_SHFT                                                                  0x12
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_TGL_SEL_BMSK                                                             0x20000
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_TGL_SEL_SHFT                                                                0x11
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_MEM_RET_BMSK                                                             0x10000
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_MEM_RET_SHFT                                                                0x10
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_MX_EN_REST_BMSK                                                            0x700
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_MX_EN_REST_SHFT                                                              0x8
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_APCC_EN_REST_BMSK                                                           0x70
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_APCC_EN_REST_SHFT                                                            0x4
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_EN_FEW_BMSK                                                                  0x3
#define HWIO_APCS_ALIAS7_PWR_MUX_STS_EN_FEW_SHFT                                                                  0x0

#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_ADDR                                                                 (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000d0)
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_RMSK                                                                   0x811377
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_MAS_OVERRIDE_ADDR, HWIO_APCS_ALIAS7_MAS_OVERRIDE_RMSK)
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_MAS_OVERRIDE_ADDR, m)
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_MAS_OVERRIDE_ADDR,v)
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_MAS_OVERRIDE_ADDR,m,v,HWIO_APCS_ALIAS7_MAS_OVERRIDE_IN)
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_BMSK                                          0x800000
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_CSR_OVERRIDE_EN_SHFT                                              0x17
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_BMSK                                 0x10000
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_APMTGLSELAPC_CSR_OVERRIDE_SHFT                                    0x10
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_BMSK                             0x1000
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_APMMEMCELLRETONMX_CSR_OVERRIDE_SHFT                                0xc
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_BMSK                                       0x300
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_APMENFEW_CSR_OVERRIDE_SHFT                                         0x8
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_BMSK                                     0x70
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_MXAPMENREST_CSR_OVERRIDE_SHFT                                      0x4
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_BMSK                                     0x7
#define HWIO_APCS_ALIAS7_MAS_OVERRIDE_MAS_PMUX_APCAPMENREST_CSR_OVERRIDE_SHFT                                     0x0

#define HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_ADDR                                                          (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000b0)
#define HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_RMSK                                                          0xffffffff
#define HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_ADDR, HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_RMSK)
#define HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_ADDR, m)
#define HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_ADDR,v)
#define HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_ADDR,m,v,HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_IN)
#define HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_BMSK                                0xffffffff
#define HWIO_APCS_ALIAS7_CORE_HANG_THRESHOLD_CORE_HANG_THRESHOLD_VALUE_SHFT                                       0x0

#define HWIO_APCS_ALIAS7_CORE_HANG_VALUE_ADDR                                                              (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000b4)
#define HWIO_APCS_ALIAS7_CORE_HANG_VALUE_RMSK                                                              0xffffffff
#define HWIO_APCS_ALIAS7_CORE_HANG_VALUE_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_CORE_HANG_VALUE_ADDR, HWIO_APCS_ALIAS7_CORE_HANG_VALUE_RMSK)
#define HWIO_APCS_ALIAS7_CORE_HANG_VALUE_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_CORE_HANG_VALUE_ADDR, m)
#define HWIO_APCS_ALIAS7_CORE_HANG_VALUE_VALUE_WHEN_HUNG_BMSK                                              0xffffffff
#define HWIO_APCS_ALIAS7_CORE_HANG_VALUE_VALUE_WHEN_HUNG_SHFT                                                     0x0

#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_ADDR                                                             (APCS_ALIAS7_APSS_ACS_REG_BASE      + 0x000000b8)
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_RMSK                                                                  0x1f7
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_IN          \
        in_dword_masked(HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_ADDR, HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_RMSK)
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_ADDR, m)
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_OUT(v)      \
        out_dword(HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_ADDR,v)
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_ADDR,m,v,HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_IN)
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_PMUEVENT_SEL_BMSK                                                     0x1f0
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_PMUEVENT_SEL_SHFT                                                       0x4
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_BMSK                                           0x4
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_CORE_HANG_COUNTER_SPM_EN_SHFT                                           0x2
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_BMSK                                               0x2
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_CORE_HANG_COUNTER_EN_SHFT                                               0x1
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_CORE_HANG_STATUS_BMSK                                                   0x1
#define HWIO_APCS_ALIAS7_CORE_HANG_CONFIG_CORE_HANG_STATUS_SHFT                                                   0x0


#endif /* __CORE_HANG_HWIO_H__ */
