/*! \file  pm_config_rpm_npa_pam.c
 *  
 *  \brief  File Contains the PMIC NPA CMI Code
 *  \details  This file contains the needed definition and enum for PMIC NPA layer.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Locked Version: MSM8953_PM8953_NPA_v1p04_2016_02_23 - LOCKED

 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2016 Qualcomm Technologies, All Rights Reserved
 */

/*===========================================================================

                EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module over time.

$Header: //components/rel/rpm.bf/2.4/core/systemdrivers/pmic/config/msm8953/pm_config_rpm_npa_pam.c#3 $ 
$DateTime: 2016/03/08 02:45:14 $  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 

===========================================================================*/

/*===========================================================================

                        INCLUDE HEADER FILES

===========================================================================*/

#include "pm_npa_device_smps.h"
#include "pm_npa.h"
#include "pm_rpm_npa.h"
#include "pm_rpm_npa_device.h"
/*===========================================================================

                        VARIABLES DEFINITION

===========================================================================*/

/* LPDDR Client */
static pm_npa_smps_int_rep
pm_rpm_pam_lpddr_a_smps2 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_SLEEP_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_LOW_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_MID_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_HIGH_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};
static pm_npa_smps_int_rep
pm_rpm_pam_lpddr_a_smps3 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_SLEEP_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_LOW_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_MID_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_HIGH_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};
static pm_npa_smps_int_rep
pm_rpm_pam_lpddr_a_smps7 [] =
{
   /**< Mode: PMIC_NPA_MODE_ID_DDR_SLEEP_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_LOW_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_MID_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__AUTO, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
   /**< Mode: PMIC_NPA_MODE_ID_DDR_HIGH_SPEED */
   {
      PM_NPA_GENERIC_DISABLE, /**< [Disable (default), Enable] -> max aggregation (left to right). */
      PM_NPA_SW_MODE_SMPS__NPM, /**< [AUTO (default), IPEAK, NPM] -> max aggregation (left to right). */
      PM_NPA_PIN_CONTROL_ENABLE__NONE, /**< [NONE, EN1, EN2, EN3, EN4] -> ORed value of list. */
      PM_NPA_PIN_CONTROL_POWER_MODE__NONE, /**< [NONE, EN1, EN2, EN3, EN4, SLEEPB] -> ORed value of list. */
      0, /**< global_byp_en - Keeps whether the child LDOs are allowed to go into bypass */
      0, /**< [X uV] -> voltage headroom needed. */
      0, /**< [X uV] -> max aggregation. */
      0, /**< [X mA] -> summed aggregation. */
      PM_SWITCHING_FREQ_FREQ_NONE, /**< [xx MHz] -> max within a priority group. */
      PM_NPA_FREQ_REASON_NONE, /**< Freq4 BT -> Freq4 GPS -> Freq4 WLAN -> Freq 4 WAN (lowest to highest priority). */
      PM_NPA_QUIET_MODE__DISABLE, /**< [None, Quiet, Super Quiet] -> max aggregation (left to right). */
      PM_NPA_BYPASS_ALLOWED, /**< [Allowed (default), Disallowed] */
      0, /**< reserve 1 - for 32 bit boundary */
   },
};

pm_npa_pam_client_cfg_type
pm_rpm_pam_lpddr_rails_info [] =
{
   {
      (void*)pm_rpm_pam_lpddr_a_smps2,
      PM_NPA_VREG_SMPS
   },
   {
      (void*)pm_rpm_pam_lpddr_a_smps3,
      PM_NPA_VREG_SMPS
   },
   {
      (void*)pm_rpm_pam_lpddr_a_smps7,
      PM_NPA_VREG_SMPS
   },
};
