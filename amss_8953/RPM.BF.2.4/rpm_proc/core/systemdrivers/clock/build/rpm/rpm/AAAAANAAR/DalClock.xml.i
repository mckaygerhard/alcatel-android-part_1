<!-- NOTE: DAL config has trouble parsing multi-line comment, so please put -->
<!--       proper comment tags on each line                                 -->
<!--                                                                        -->
<!-- Clock Driver Properties file                                           -->
<!-- Note we depend on the include path for the following directories to be -->
<!-- available to the DAL config compiler:                                  -->
<!-- "${INC_ROOT}/core/systemdrivers/hal/clk/inc",                          -->
<!-- "${INC_ROOT}/core/systemdrivers/clock/src",                            -->
<!-- "${INC_ROOT}/core/systemdrivers/clock/config"                          -->
<!-- "${INC_ROOT}/core/systemdrivers/clock/config/${CHIPSET}"               -->
<driver name="Clock">
<global_def>
<!-- Min and MAx frequency (in Hz)for bimc clock  -->
  <var_seq name="bimc_gb" type=DALPROP_DATA_TYPE_UINT32_SEQ>
    0xFFFFFFFF, 0xFFFFFFFF,end
   </var_seq>
  <var_seq name="snoc_gb" type=DALPROP_DATA_TYPE_UINT32_SEQ>
    0xFFFFFFFF, 0xFFFFFFFF,end
  </var_seq>
<!-- Min and MAx frequency (in Hz)for SYSMMNOC clock  -->
  <var_seq name="sysmmnoc_gb" type=DALPROP_DATA_TYPE_UINT32_SEQ>
    0xFFFFFFFF, 0xFFFFFFFF,end
  </var_seq>
<!-- Min and MAx frequency (in Hz)for PCNOC clock  -->
  <var_seq name="pcnoc_gb" type=DALPROP_DATA_TYPE_UINT32_SEQ>
    0xFFFFFFFF, 0xFFFFFFFF,end
   </var_seq>
<!-- Min and MAx frequency (in Hz)for IPA clock  -->
  <var_seq name="ipa_gb" type=DALPROP_DATA_TYPE_UINT32_SEQ>
    0xFFFFFFFF, 0xFFFFFFFF,end
  </var_seq>
<!-- Min and MAx frequency (in Hz)for RPM clock  -->
  <var_seq name="cpu_gb" type=DALPROP_DATA_TYPE_UINT32_SEQ>
    0xFFFFFFFF, 0xFFFFFFFF,end
  </var_seq>
<!-- To set all the RPM controlled clocks to voltage corner  -->
  <var_seq name="volt_gb" type=DALPROP_DATA_TYPE_UINT32_SEQ>
    0xFFFFFFFF,end
  </var_seq>
</global_def>
  <device id=DALDEVICEID_CLOCK>
<!-- Below properties are for getting the min/max limit as specified in ClockchipsetGlobal.xml -->
<!-- Property for querying bimc clock limit -->
<props name="lim_bimc" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR>
  bimc_gb
</props>
<!-- Property for querying SYSNOC clock limit -->
<props name="lim_snoc" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR>
  snoc_gb
</props>
<!-- Property for querying SYSMMNOC clock limit -->
<props name="lim_sysmmnoc" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR>
  sysmmnoc_gb
</props>
<!-- Property for querying PCNOC clock limit -->
<props name="lim_pcnoc" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR>
  pcnoc_gb
</props>
<!-- Property for querying RPM clock limit -->
<props name="lim_cpu" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR>
  cpu_gb
</props>
<!-- Property for querying voltage corner -->
<props name="clk_volt_corn" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR>
  volt_gb
</props>
<!-- Clock sources -->
<props name="ClockSources" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  SourceConfig
</props>
<!-- RPM Clocks -->
<props name="gcc_rpm_proc_fclk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  RPMClockConfig
</props>
<!-- System NOC Clocks -->
<props name="gcc_sys_noc_axi_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  SystemNOCClockConfig
</props>
<!-- System MMNOC Clocks -->
<props name="gcc_sys_mm_noc_axi_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  SystemMMNOCClockConfig
</props>
<!-- Peripheral Config NOC Clocks -->
<props name="gcc_pcnoc_ahb_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  PCNOClockConfig
</props>
<!-- BIMC Clocks -->
<props name="gcc_bimc_ddr_ch0_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  BIMCClockConfig
  </props>
<!-- BIMC GPU Clock -->
<props name="gcc_bimc_gpu_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  BIMCGPUClockConfig
  </props>
  <!-- BIMC Clock Plans -->
   <props name="BIMCClockPlans" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
     BIMCClockPlans
   </props>
<!-- APSS_TCU_ASYNC Clock -->
<props name="gcc_bimc_apss_tcu_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  APSSTCUASYNCClockConfig
</props>
  <!-- APSS_AXI Clock -->
  <props name="gcc_apss_axi_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
    APSSAXIClockConfig
  </props>
  <!-- Q6 TBU Clock -->
  <props name="gcc_mss_q6_bimc_axi_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
    Q6TBUClockConfig
  </props>
<!-- QDSS AT Clocks -->
<props name="gcc_qdss_at_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  QDSSATClockConfig
</props>
<!-- QDSS Trace Clocks -->
<props name="gcc_qdss_traceclkin_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  QDSSTraceClockConfig
</props>
<!-- QDSS STM Clocks -->
<props name="gcc_qdss_stm_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  QDSSSTMClockConfig
</props>
<!-- QDSS TSCTR Div2 Clocks -->
<props name="gcc_qdss_tsctr_div2_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  QDSSTSCTRDiv2ClockConfig
</props>
<!-- RBCPR Clocks -->
<props name="gcc_rbcpr_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  RBCPRClockConfig
</props>
<!-- SPMI AHB Clocks -->
<props name="gcc_spmi_ahb_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  SPMIAHBClockConfig
</props>
<!-- SPMI SER Clocks -->
<props name="gcc_spmi_ser_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  SPMISERClockConfig
</props>
<!-- IPA Clocks -->
<props name="gcc_ipa_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  IPAClockConfig
</props>
<!-- VS Clocks -->
<props name="gcc_vdda_voltage_droop_detector_gpll0_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  VSClockConfig
</props>
<props name="gcc_mx_voltage_droop_detector_gpll0_clk" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  VSClockConfig
</props>
<!-- Clock Log Defaults -->
<props name="ClockLogDefaults" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  ClockLogDefaultConfig
</props>
<!-- Clock Vreg level railway level mapping -->
<props name="ClockVregRailMap" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  ClockVregRailMapConfig
</props>
<!-- Clock BIMC MMNOC level mapping -->
<props name="ClockBIMCMMNOCMap" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
  ClockBIMCMMNOCMapConfig
</props>
  </device>
</driver>
