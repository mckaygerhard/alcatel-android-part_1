#ifndef VSENSE_INTERNAL_H
#define VSENSE_INTERNAL_H
/*
===========================================================================


  @file vsense_internal.h

=======================================================================
 *! \file vsense.h
 *  \n
 *  \brief This file contains vsense related function prototypes,
 *         enums and driver data structure type.   
 *  \n  
 *  \n &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
 *
* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/25/14   aks     created

========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "railway.h"
#include "pmapp_npa.h"
#include "com_dtypes.h"
#include "ClockDefs.h"
 
 /*===========================================================================
 
                      STRUCTURE TYPE AND ENUM
 
 ===========================================================================*/
#define VSENSE_FIFO_ARRAY_SIZE  64

typedef enum
{
  VSENSE_TYPE_RAIL_MX      ,
  VSENSE_TYPE_RAIL_MSS     , 
  VSENSE_TYPE_RAIL_CX      ,
  VSENSE_TYPE_RAIL_APC0    , 
  VSENSE_TYPE_RAIL_APC1    , 
  VSENSE_TYPE_RAIL_MAX     , 
}vsense_type_rail;

typedef enum
{
  VSENSE_TYPE_GCC_VS_MX_CLK,
  VSENSE_TYPE_GCC_VS_CX_CLK,  
  VSENSE_TYPE_CLK_MAX,
}vsense_type_clk;

typedef struct
{
  vsense_type_rail     rail;
  ClockIdType          clk_id; 
  ClockCallbackHandle  clk_handle;
  vsense_type_clk      vsense_clk_id; 
}vsense_type_clk_data;

typedef struct
{
  vsense_type_rail rail;
  railway_corner   previous_corner;
  uint32           previous_uv;
  int32            railway_id;
  boolean          is_supported;
  boolean          is_vsense_pwr_en;
  boolean          is_vsense_en;
  uint32           config_0_address;
  uint32           config_1_address;
  uint32           status_address;
  uint32           status;
  uint32           config_0;
  uint32           config_1;
  uint8            fifo_data[VSENSE_FIFO_ARRAY_SIZE];
  vsense_type_clk  vsense_clk_id; 
}vsense_type_rail_info;
 
 typedef struct
 {
   vsense_type_rail          rail;
   uint32                    not_calibrated; 
   uint32                    fuse1_voltage_uv;
   uint32                    fuse2_voltage_uv;
   uint32                    fuse1_voltage_code;
   uint32                    fuse2_voltage_code;
 }vsense_type_fuse_info;
 
 typedef struct
 {
   uint32                    version;
   uint32                    num_of_vsense_rails; //no. of rails vsense is monitoring
   vsense_type_fuse_info     rails_fuse_info[VSENSE_TYPE_RAIL_MAX]; //array size is num_of_rails_supported value 
 }vsense_type_smem_info;
 
typedef enum
{
  VSENSE_TYPE_INTERRUPT_MODE,
  VSENSE_TYPE_WATERMARK_MODE,
}vsense_type_mode;

typedef struct 
{ 
  vsense_type_smem_info      smem_info;
  //allocate only rpm managed rails 
  vsense_type_rail_info      rail_info[VSENSE_TYPE_RAIL_APC0];
}vsense_type_rail_and_fuse_info;

typedef struct 
{
  uint8 min_threshold_offset; //offset in vsense code - 1 code =>5mv roughly
  uint8 max_threshold_offset; //offset in vsense code 
  uint8 mode ; //watermark or interrupt
  uint32 duration_us ; //duration in us for min max capture 
}vsense_type_dal_cfg;

typedef enum
{
  VSENSE_TRIG_PRE,
  VSENSE_TRIG_MID,
  VSENSE_TRIG_POST,  
}vsense_type_trig_pos;

typedef enum
{
  VSENSE_TRIG_SEL_0,
  VSENSE_TRIG_SEL_1,
  VSENSE_TRIG_SEL_2,
  VSENSE_TRIG_SEL_3,
}vsense_type_trig_sel;


/*----------------------------------------------------------------------------
 * Function : vsense_set_mode_alarm
 * -------------------------------------------------------------------------*/
/*!
    Description: set the vsense into alaram mode for the requested rail
    @param
      rail - MX, CX, GFX, EBI 
      voltage_uv - voltage of the rail to determine thresholds
    @return
    void
    
-------------------------------------------------------------------------*/

void vsense_set_mode_alarm(vsense_type_rail rail, uint32 voltage_uv );

/*----------------------------------------------------------------------------
 * Function : vsense_set_mode_slope_detection
 * -------------------------------------------------------------------------*/
/*!
    Description: set the vsense into slope detection mode for the requested rail
    @param
      rail - MX, CX, GFX, EBI 
      
    @return
    void
    
-------------------------------------------------------------------------*/

void vsense_set_mode_slope_detection(vsense_type_rail rail);

/*----------------------------------------------------------------------------
 * Function : vsense_set_mode_min_max
 * -------------------------------------------------------------------------*/
/*!
    Description: set the vsense into min max mode for the requested rail
    @param
      rail - MX, CX, GFX, EBI 
      
    @return
    void
    
-------------------------------------------------------------------------*/

void vsense_set_mode_min_max(vsense_type_rail rail);

/*----------------------------------------------------------------------------
 * Function : vsense_get_config_info
 * -------------------------------------------------------------------------*/
/*!
    Description: Get a reference to the config info for the vsense driver 
    @param
      void
    @return
    void*
    
-------------------------------------------------------------------------*/

void* vsense_get_config_info(uint32 prop_id);


#endif 
