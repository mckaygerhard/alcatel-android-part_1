/*
===========================================================================
*/
/**
  @file  vsense.c
  @brief This file contains vsense driver functions, and implementaions.   

  Reference chip release:
    MSM8956 (Eldarion) 
 
*/

/*=======================================================================

  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/rpm.bf/2.4/core/systemdrivers/vsense/src/msm8953/vsense.c#2 $
  $DateTime: 2016/04/07 01:17:39 $
  $Author: pwbldsvc $

  ===========================================================================
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/20/15   akt     Updated for Eldarion
09/25/14   aks     created

========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "vsense_internal.h"
#include "vsense.h"
#include "DALSys.h"
#include "vsense_dal_prop_ids.h"
#include "HALhwio.h"
#include "tcsr_hwio_rpm.h"
#include "smem.h"
#include "smem_type.h"
#include "CoreVerify.h"
#include "swevent.h"
#include "Clock.h"
#include "hwio_qdss_cti.h"
#include "time_service.h"
#include "vsense_utils.h"

//#ifndef VSENSE_DRIVER_ENABLED
#ifdef VSENSE_FEATURE_ENABLED

void vsense_init(void)
{
    return;
}
void vsense_enter_sleep(bool enter)
{
    return;
}

#else



static DALSYS_PROPERTY_HANDLE_DECLARE(hProp_vsense);
static DALSYSPropertyVar prop_vsense;               


#define VSENSE_FIFO_READ_DELAY  1 // 10 XO cycles
#define VSENSE_STATUS_ALARM_MASK 0x3C00 //mask for alarm bits

static vsense_type_dal_cfg*  vsense_dal_data;
static vsense_type_clk_data  vsense_clk_array[VSENSE_TYPE_CLK_MAX]; 

#define RPM_IRQ_VSENSE            46


static uint64 vsense_watermark_mode_start_timestamp = 0; //TODO uncomment it later

static vsense_type_rail_and_fuse_info vsense_rail_and_fuse_info =
{
  .rail_info = 
  {
    {
       .rail  = VSENSE_TYPE_RAIL_MX, //MX L2 APC0
      .is_supported = TRUE,
      .config_0_address =  HWIO_ADDR(TCSR_VSENS1_CONFIG_0) ,
      .config_1_address =  HWIO_ADDR(TCSR_VSENS1_CONFIG_1),
      .status_address   =  HWIO_ADDR(TCSR_VSENS1_STATUS)	
   },                
   {               
    .rail = VSENSE_TYPE_RAIL_MSS, //MX L2 APC1
    .is_supported = TRUE, 
    .config_0_address = HWIO_ADDR(TCSR_VSENS2_CONFIG_0) ,
    .config_1_address =  HWIO_ADDR(TCSR_VSENS2_CONFIG_1),
    .status_address   =  HWIO_ADDR(TCSR_VSENS2_STATUS)
   },
   {               
    .rail = VSENSE_TYPE_RAIL_CX,
    .is_supported = TRUE, 
    .config_0_address = HWIO_ADDR(TCSR_VSENS0_CONFIG_0) ,
    .config_1_address =  HWIO_ADDR(TCSR_VSENS0_CONFIG_1),
    .status_address   =  HWIO_ADDR(TCSR_VSENS0_STATUS)
   },

  },
//SMEM values are initialised but must be read from SMEM calibrated vaules written by BOOT
  .smem_info = 
  {  
    .version = 1,
    .num_of_vsense_rails = VSENSE_TYPE_RAIL_MAX,
    .rails_fuse_info = 
    {
      {
        .rail  = VSENSE_TYPE_RAIL_MX,
        .fuse1_voltage_uv = 900000,  
        .fuse2_voltage_uv =  1000000 ,
        .fuse1_voltage_code = 88,
        .fuse2_voltage_code = 108,     
      },                
      {               
        .rail = VSENSE_TYPE_RAIL_MSS,
        .fuse1_voltage_uv = 900000,  
        .fuse2_voltage_uv =  1000000 ,
        .fuse1_voltage_code = 88,
        .fuse2_voltage_code = 108,
     },
      {               
        .rail = VSENSE_TYPE_RAIL_CX,
        .fuse1_voltage_uv = 900000, 
        .fuse2_voltage_uv =  1000000 ,
        .fuse1_voltage_code = 84,
        .fuse2_voltage_code = 105,
      },
     {               
        .rail = VSENSE_TYPE_RAIL_APC0,
        .fuse1_voltage_uv = 1000000,
        .fuse2_voltage_uv = 1115000,
        .fuse1_voltage_code = 105,
        .fuse2_voltage_code = 121,
     },
     {               
        .rail = VSENSE_TYPE_RAIL_APC1,
        .fuse1_voltage_uv =  1000000, 
        .fuse2_voltage_uv =  1115000,
        .fuse1_voltage_code = 105,
        .fuse2_voltage_code = 121,
     },
   }
  }
};

static void vsense_pre_vdd_switch_cb(const railway_settings *proposal, void * callback_cookie);
static void vsense_post_vdd_switch_cb(const railway_settings *proposal, void * callback_cookie);
uint32 vsense_cal_voltage_code(vsense_type_rail rail,uint32 input_uv);
void vsense_config_common(vsense_type_rail rail);
void vsense_power_en(vsense_type_rail rail, boolean en_vsense);
void vsense_log_rail_status(void);
void vsense_read_fifo(vsense_type_rail rail, uint8* fifo_data);
void vsense_disable_and_reset(vsense_type_rail rail);
void vsense_clock_init(void);
void vsense_en(vsense_type_rail rail, boolean en_vsense);
void vsense_clear_bit_enable(vsense_type_rail rail, boolean enable);
void vsense_unreset_and_cgc_clk_enable(vsense_type_rail rail);
void vsense_reset_and_cgc_clk_disable(vsense_type_rail rail);
void vsense_isr_config(void);
void vsense_config_watermark_mode(vsense_type_rail rail);
void vsense_check_watermark_duration(void);

void vsense_power_down(vsense_type_rail rail);

static void vsense_log_status_and_fifo_isr(void) __irq ;


//To enable SMEM access from RPM these MACROs are MUST for Eldarion MSM8976
//As per RPM team recommnendation
#define RPM_CSR_CONFIGURE_MSGRAM_ACCESS HWIO_RPM_PAGE_SELECT_OUT(0x0)
#define RPM_CSR_RECONFIGURE_MSGRAM_ACCESS HWIO_RPM_PAGE_SELECT_OUT(0x2)


//Debug variables
volatile int g_init_vs = 0;
int g_num_of_isr = 0;
int g_num_of_pre_switch = 0;
int g_num_of_post_switch = 0;

//Configure the initial state of the vsense, 
//It's the entry function to driver,
//Should be called from RPM main function

void vsense_init(void)
{
  int32 rail_count;
  uint32 smem_size;
  int32  railway_id;
  //railway_settings cur_rail_settings = {RAILWAY_NO_REQUEST, 0} ; ==> TODO Gives compilation error " Error:  #188-D: enumerated type mixed with another type"
  railway_settings cur_rail_settings ;
  DALSYS_GetDALPropertyHandleStr("/sysdrivers/vsense",hProp_vsense);

  if (0 == g_init_vs)
  {
	  return ; //This is to make sure that it does not go through and crash; but can only be debugged
  }

  vsense_dal_data       = (vsense_type_dal_cfg*)
                         vsense_get_config_info(VSENSE_PROP_CONFIG);


  vsense_type_smem_info* vsense_data = 
       (vsense_type_smem_info*) smem_get_addr(SMEM_VSENSE_DATA, &smem_size);
  

  // TODO - ENABLE it after SMEM is activated
  if (vsense_data == NULL || vsense_dal_data == NULL)
  {
    SWEVENT(VSENSE_EVENT_INIT); 
    return; 
  }
  else
  {
    RPM_CSR_RECONFIGURE_MSGRAM_ACCESS;
    memset((void*)&vsense_rail_and_fuse_info.smem_info, 0 , sizeof(vsense_rail_and_fuse_info.smem_info));
                                              
    memcpy((void*)&vsense_rail_and_fuse_info.smem_info,vsense_data , 
                   sizeof(vsense_rail_and_fuse_info.smem_info));
    RPM_CSR_CONFIGURE_MSGRAM_ACCESS;
  }

  vsense_clock_init();

  ///TODO - Temporarily don't support the MX rails
  //vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_CX].is_supported = FALSE;
  //vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MX].is_supported = FALSE;
  //vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS].is_supported = FALSE;
 
  for( rail_count = 0; rail_count < VSENSE_TYPE_RAIL_APC0; rail_count++ )
  {
    //mark the vsense as not supported if the calibration is fails in BOOT 
    if(vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail_count].not_calibrated == TRUE)
    {
      vsense_rail_and_fuse_info.rail_info[rail_count].is_supported = FALSE;
      SWEVENT(VSENSE_EVENT_RAIL_NOT_CALIBRATED, rail_count);
    }
     
    if(vsense_rail_and_fuse_info.rail_info[rail_count].is_supported )
    {
      switch(vsense_rail_and_fuse_info.rail_info[rail_count].rail)
      {
        case VSENSE_TYPE_RAIL_MX:
        case VSENSE_TYPE_RAIL_MSS:
         railway_id = rail_id("vddmx");
         break;
        case VSENSE_TYPE_RAIL_CX:
         railway_id = rail_id("vddcx");
         break;

        default: 
         railway_id = RAIL_NOT_SUPPORTED_BY_RAILWAY;
      }
      vsense_rail_and_fuse_info.rail_info[rail_count].railway_id = railway_id; 

      railway_get_current_settings(railway_id, &cur_rail_settings);
      vsense_rail_and_fuse_info.rail_info[rail_count].previous_corner = cur_rail_settings.mode;
      vsense_rail_and_fuse_info.rail_info[rail_count].previous_uv = cur_rail_settings.microvolts;
      
      railway_set_callback(railway_id, RAILWAY_PRECHANGE_CB, 
                          vsense_pre_vdd_switch_cb,
                          &vsense_rail_and_fuse_info.rail_info[rail_count]);
        
      if(vsense_dal_data->mode == VSENSE_TYPE_INTERRUPT_MODE)
      {
        railway_set_callback(railway_id, RAILWAY_POSTCHANGE_CB, 
                          vsense_post_vdd_switch_cb,
                          &vsense_rail_and_fuse_info.rail_info[rail_count]);

        vsense_set_mode_alarm(vsense_rail_and_fuse_info.rail_info[rail_count].rail,
                           cur_rail_settings.microvolts );
        vsense_set_mode_slope_detection(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
          //register isr
        vsense_isr_config();
      }
      else if(vsense_dal_data->mode == VSENSE_TYPE_WATERMARK_MODE)
      {
        vsense_config_watermark_mode(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
      }
    }
    else
    {
      SWEVENT(VSENSE_EVENT_RAIL_NOT_SUPPORTED, rail_count);
    }
  }
}


/// Internal function, and it registers the ISR callbacks
void vsense_isr_config(void)
{
  //TODO - define three handlers so that ONLY the corresponding rails can be logged
  //this will help faster processing without checking for all rails/status
  interrupt_set_isr(RPM_IRQ_VSENSE, vsense_log_status_and_fifo_isr);
  interrupt_configure(RPM_IRQ_VSENSE, RISING_EDGE);
  interrupt_enable(RPM_IRQ_VSENSE);


}

static void vsense_log_status_and_fifo_isr(void) __irq 
{
  g_num_of_isr++; //TODO for debug purpose to see how many times Alarms triggered
  lock_ints();
  SWEVENT(VSENSE_EVENT_ISR,  g_num_of_isr);
  vsense_log_rail_status();
  unlock_ints();
}

void vsense_clock_init(void)
{
  DALResult dal_result;
  int32     clk_count;
  ClockIdType clock_id = 0;

  dal_result = Clock_GetClockId( "gcc_vdda_voltage_droop_detector_gpll0_clk",  &clock_id);
  if(dal_result != DAL_SUCCESS)
  {
    return ; //
  }
  vsense_clk_array[VSENSE_TYPE_GCC_VS_CX_CLK].clk_id = clock_id;
  vsense_clk_array[VSENSE_TYPE_GCC_VS_CX_CLK].rail = VSENSE_TYPE_RAIL_CX; 
  vsense_clk_array[VSENSE_TYPE_GCC_VS_CX_CLK].vsense_clk_id = VSENSE_TYPE_GCC_VS_CX_CLK;

  //keep the clock ID in rail info for future reference
  vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_CX].vsense_clk_id = VSENSE_TYPE_GCC_VS_CX_CLK;

  dal_result = Clock_GetClockId( "gcc_mx_voltage_droop_detector_gpll0_clk",  &clock_id);
  if(dal_result != DAL_SUCCESS)
  {
    return ; //
  }
  vsense_clk_array[VSENSE_TYPE_GCC_VS_MX_CLK].clk_id = clock_id;
  vsense_clk_array[VSENSE_TYPE_GCC_VS_MX_CLK].rail = VSENSE_TYPE_RAIL_MX; 
  vsense_clk_array[VSENSE_TYPE_GCC_VS_MX_CLK].vsense_clk_id = VSENSE_TYPE_GCC_VS_MX_CLK;

  //keep the clock ID in rail info for future reference
  vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MX].vsense_clk_id = VSENSE_TYPE_GCC_VS_MX_CLK;


  for(clk_count= 0; clk_count < VSENSE_TYPE_CLK_MAX ; clk_count++)
  {
    uint32 outclk = 0;
    Clock_EnableClock( vsense_clk_array[clk_count].clk_id);
    Clock_SetClockFrequency(vsense_clk_array[clk_count].clk_id, 600, CLOCK_FREQUENCY_MHZ_AT_LEAST, &outclk);
	//TODO - handle the error code
	//if (540 > outclk)
	//{
	//	return;
	//}
  }
  
}

//power on the vsense if it was previously off
void vsense_power_en(vsense_type_rail rail, boolean pwr_en_vsense)
{
  uint32 addr_config_0 = 0;
  uint32 val = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;
  val = inpdw(addr_config_0);
  
  if((vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en == TRUE) && (pwr_en_vsense == FALSE))
  {
    val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, POWER_EN); //power disable sensor
    outpdw(addr_config_0, val);
    vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en = FALSE;
    vsense_power_down(rail); //Write POR value for shutdown
  }
  else if((vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en == FALSE) && (pwr_en_vsense == TRUE))
  {
    vsense_config_common(rail);
    vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en = TRUE;
  }
  
}

void vsense_alarm_en(vsense_type_rail rail, boolean en_alarm)
{
  uint32 addr_config_1 = 0;
  uint32 val = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  val = inpdw(addr_config_1);

  if(en_alarm)
  {
    val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, ALARM_MIN_EN, 1);
	outpdw(addr_config_1, val);
	val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, ALARM_MAX_EN, 1);
	outpdw(addr_config_1, val);
  }
  else
  {
    val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_MIN_EN);
	outpdw(addr_config_1, val);
	val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_MAX_EN);
	outpdw(addr_config_1, val);
  }
  

}

//enable/disable vsense 
void vsense_en(vsense_type_rail rail, boolean en_vsense)
{
  uint32 addr_config_1 = 0;
  uint32 val = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  val = inpdw(addr_config_1);
  
  if(/*(vsense_rail_and_fuse_info.rail_info[rail].is_vsense_en == TRUE) &&*/ (en_vsense == FALSE))
  {
    val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, FUNC_EN); //disable sensor
    //vsense_rail_and_fuse_info.rail_info[rail].is_vsense_en = FALSE;
  }
  else if(/*(vsense_rail_and_fuse_info.rail_info[rail].is_vsense_en == FALSE) &&*/ (en_vsense == TRUE))
  {
    val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, FUNC_EN, 1); //enable sensor
    //vsense_rail_and_fuse_info.rail_info[rail].is_vsense_en = TRUE;
  }

   outpdw(addr_config_1, val);
}


//change the rail min or max thresholds based on the mode 
void vsense_pre_vdd_switch_cb(const railway_settings *proposal, void * callback_cookie)
{
  uint32 voltage_code = 0;
  uint32 addr_config_0 = 0;
  uint32 val = 0;
  //uint32 status_addr ;
  vsense_type_rail_info* rail_info = (vsense_type_rail_info*) callback_cookie;
  static int is_event_handled = 0; //should be 0 or 1;

  SWEVENT(VSENSE_EVENT_PRE_VDD_CB, rail_info->previous_corner, proposal->mode ); 

  g_num_of_pre_switch++;

  if (VSENSE_TYPE_RAIL_MSS == rail_info->rail)
  {
    if (is_event_handled == 0)
    {
        is_event_handled = 1;
        vsense_pre_vdd_switch_cb(proposal, &vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MX]) ;
    }
    else
    {
        is_event_handled = 0;
    }
  }
  
  if (VSENSE_TYPE_RAIL_MX == rail_info->rail)
  {
    if (is_event_handled == 0)
    {
        is_event_handled = 1;
        vsense_pre_vdd_switch_cb(proposal, &vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS]) ;
    }
    else
    {
        is_event_handled = 0;
    }
  }



  if(vsense_rail_and_fuse_info.rail_info[rail_info->rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
 
  if(vsense_dal_data->mode == VSENSE_TYPE_WATERMARK_MODE)
  {
    vsense_check_watermark_duration();
    return;
  }
 
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail_info->rail].config_0_address;
  //status_addr   = vsense_rail_and_fuse_info.rail_info[rail_info->rail].status_address;
  
  //vsense_rail_and_fuse_info.rail_info[rail_info->rail].status = inpdw(status_addr);

  //log the rail status in case the interrupt did not woke us up
  vsense_log_rail_status();
  
  switch(proposal->mode)
  {
    case RAILWAY_NO_REQUEST:
    case RAILWAY_RETENTION :      
    case RAILWAY_SVS_SOC   :   
    case RAILWAY_SVS_HIGH  :
      vsense_power_en( rail_info->rail, FALSE);
      //Clock_DisableClock( vsense_clk_array[rail_info->rail].clk_id); 
      break;

    case RAILWAY_NOMINAL   :
    case RAILWAY_NOMINAL_HIGH:
    case RAILWAY_TURBO     :
    case RAILWAY_SUPER_TURBO :
    case RAILWAY_SUPER_TURBO_NO_CPR :
      //if we are coming out of SVS , do not do anything in pre
      if(rail_info->previous_corner > RAILWAY_SVS_HIGH)
      {
              
        
        /// TODO - DON"T do anything in PRE just disable it; Enable after MIN/MAX update in post
        //voltage_code = vsense_cal_voltage_code(rail_info->rail , proposal->microvolts);
        vsense_alarm_en(rail_info->rail, FALSE);
        vsense_en(rail_info->rail, FALSE);
        vsense_clear_bit_enable(rail_info->rail, FALSE);
        vsense_clear_bit_enable(rail_info->rail, TRUE);
        vsense_reset_and_cgc_clk_disable(rail_info->rail);
        //vsense_unreset_and_cgc_clk_enable(rail_info->rail);
        vsense_power_en( rail_info->rail, FALSE); //Write POR values and power OFF the sensor
        
        val = inpdw(addr_config_0);
        if(rail_info->previous_corner < proposal->mode) //going high
        {
          val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, THRESHOLD_MAX);
          val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, THRESHOLD_MAX, 
                        voltage_code + vsense_dal_data->max_threshold_offset);
          
        }
        else if(rail_info->previous_corner > proposal->mode)//going low
        {
          val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, THRESHOLD_MIN);
          val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, THRESHOLD_MIN, 
                        voltage_code - vsense_dal_data->min_threshold_offset);
          
        }
        outpdw(addr_config_0, val);
        //vsense_power_en(rail_info->rail, TRUE);
        /*vsense_config_common(rail_info->rail);
        vsense_en(rail_info->rail, TRUE);
        vsense_alarm_en(rail_info->rail, TRUE);
        vsense_set_mode_slope_detection(rail_info->rail);
        SWEVENT(VSENSE_EVENT_PRE_VDD_CB_THRESHOLD, rail_info->rail, proposal->microvolts,
               voltage_code, voltage_code - vsense_dal_data->min_threshold_offset);*/
               /*voltage_code + vsense_dal_data->max_threshold_offset*/ 
      }
      break;
    
  }

}

//change the rail min or max thresholds based on the mode 
void vsense_post_vdd_switch_cb(const railway_settings *proposal, void * callback_cookie)
{
  
  uint32 voltage_code = 0;
  uint32 addr_config_0 ;
  uint32 val = 0;
  vsense_type_rail_info* rail_info = (vsense_type_rail_info*) callback_cookie;
  static int is_event_handled = 0; //should be 0 or 1;

  g_num_of_post_switch++;

  
  /**
  * As we have two sensors on MX, so for any of the sensors also call CB for the other sensor 
  * we will have the same callback registered for both the MX sensors. While registering as 
  * APC1 L2 sensor will overwrite APC0 L2, need to handle both when get EBI (APC1)
  */
  if (VSENSE_TYPE_RAIL_MSS == rail_info->rail)
  {
    if (is_event_handled == 0)
    {
        is_event_handled = 1;
        vsense_post_vdd_switch_cb(proposal, &vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MX]);
    }
    else
    {
        is_event_handled = 0;
    }
  }
  
  if (VSENSE_TYPE_RAIL_MX == rail_info->rail)
  {
    if (is_event_handled == 0)
    {
        is_event_handled = 1;
        vsense_post_vdd_switch_cb(proposal, &vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MSS]);
    }
    else
    {
        is_event_handled = 0;
    }
  }


  if(vsense_rail_and_fuse_info.rail_info[rail_info->rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }

  
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail_info->rail].config_0_address;
  
  switch(proposal->mode)
  {
    case RAILWAY_NOMINAL   :
    case RAILWAY_TURBO     :
    case RAILWAY_NOMINAL_HIGH:
    case RAILWAY_SUPER_TURBO :
    case RAILWAY_SUPER_TURBO_NO_CPR : 
    
      if(rail_info->previous_corner <= RAILWAY_SVS_HIGH)
      {
        //Clock_EnableClock( vsense_clk_array[VSENSE_TYPE_GCC_VS_CX_CLK].clk_id);
        //vsense_power_en( rail_info->rail, TRUE);
      }
      /*
	  vsense_power_en( rail_info->rail, FALSE);
      voltage_code = vsense_cal_voltage_code(rail_info->rail, proposal->microvolts); 
      val = inpdw(addr_config_0);
      vsense_alarm_en(rail_info->rail, FALSE);
      vsense_en(rail_info->rail, FALSE);
      vsense_clear_bit_enable(rail_info->rail, FALSE);
      vsense_clear_bit_enable(rail_info->rail, TRUE);
      vsense_reset_and_cgc_clk_disable(rail_info->rail);
      vsense_unreset_and_cgc_clk_enable(rail_info->rail);
	  */
      //we can set both min and max here since in the pre we would have 
      //already set the required min or max , so one of the thresholds(low/high) would 
      //have already been set by the time we reach here.
      // if(rail_info->previous_corner < proposal->mode) //going high
      // {
        voltage_code = vsense_cal_voltage_code(rail_info->rail, proposal->microvolts); 
		val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, THRESHOLD_MIN);
        val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, THRESHOLD_MIN, 
                        voltage_code - vsense_dal_data->min_threshold_offset);
      // }
      // else if(rail_info->previous_corner > proposal->mode)//going low
      // {
        val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, THRESHOLD_MAX);
        val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, THRESHOLD_MAX, 
                      voltage_code + vsense_dal_data->max_threshold_offset);
      // }
      outpdw(addr_config_0, val);
      vsense_config_common(rail_info->rail);
      vsense_en(rail_info->rail, TRUE);
      vsense_alarm_en(rail_info->rail, TRUE);
      vsense_set_mode_slope_detection(rail_info->rail);
      SWEVENT(VSENSE_EVENT_POST_VDD_CB_THRESHOLD,rail_info->rail, proposal->microvolts,
               voltage_code, voltage_code - vsense_dal_data->min_threshold_offset
               /*voltage_code + vsense_dal_data->max_threshold_offset */);
      break;
  
  }
  rail_info->previous_corner = proposal->mode;
  rail_info->previous_uv     = proposal->microvolts;
  
}

//common settings for all the voltage sensors 
void vsense_config_common(vsense_type_rail rail)
{
  uint32 val_config_0 ;
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  val_config_0 = inpdw(addr_config_0);
  val_config_1 = inpdw(addr_config_1);
  //need to write multiple times to the same register since sw
  //needs to maintain the state machine for Elessar
  //DO NOT OPTMIZE THIS CODE TO A SINGLE REGISTER WRITE
  val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, VS_CSR_QSS_MUX_SEL, 1);
  outpdw(addr_config_1, val_config_1);
  val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, POWER_EN, 1);
  outpdw(addr_config_0, val_config_0);

 // val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, CLAMP_DIS, 1) ;
 // outpdw(addr_config_1, val_config_1);

  //val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, RESET_FUNC, 1);
  //outpdw(addr_config_0, val_config_0);
  
  //val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, CGC_CLOCK_ENABLE, 1);
  //outpdw(addr_config_1, val_config_1);

}

//configure the vsense to set alarm
void vsense_set_mode_alarm(vsense_type_rail rail, uint32 voltage_uv )
{
  uint8 voltage_code = 0;
  uint32 val ;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  voltage_code = vsense_cal_voltage_code(rail, voltage_uv);
  vsense_config_common(rail);
  
  val = inpdw(addr_config_0);
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, TRIG_POS);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, TRIG_POS, VSENSE_TRIG_MID);
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, CAPTURE_DELAY);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, CAPTURE_DELAY, 0);
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, THRESHOLD_MIN);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, THRESHOLD_MIN, 
                        voltage_code - vsense_dal_data->min_threshold_offset);
                        
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, THRESHOLD_MAX);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, THRESHOLD_MAX, 
                        voltage_code + vsense_dal_data->max_threshold_offset);
                        
  outpdw(addr_config_0, val);

    
  val = inpdw(addr_config_1);
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, FUNC_EN);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, FUNC_EN, 1);
  outpdw(addr_config_1, val);
  //need to write twice to the same register since sw
  //needs to maintain the state machine for Elessar
  //DO NOT OPTMIZE THIS CODE TO A SINGLE REGISTER WRITE
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_MIN_EN);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, ALARM_MIN_EN, 1);
  outpdw(addr_config_1, val);


  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_MAX_EN);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, ALARM_MAX_EN, 1);
  outpdw(addr_config_1, val);

  SWEVENT(VSENSE_EVENT_CONFIG_ALARM, rail, voltage_uv, voltage_code,
               voltage_code - vsense_dal_data->min_threshold_offset
               /*voltage_code + vsense_dal_data->max_threshold_offset*/ );

  vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en = TRUE;
 
}
//configure the vsense to detect droops (slope) 
void vsense_set_mode_slope_detection(vsense_type_rail rail)
{
/* TODO remove it for memory constraint
  uint32 val ;
  uint32 addr_config_1 ;
  uint32 addr_config_0 ;
  uint8  slope_threshold;
  uint8  delta_cycle = 1; //diff between 1st and 3rd sample;
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
 

 
  uint32 fuse1_code = 
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail].fuse1_voltage_code;
  uint32 fuse2_code = 
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail].fuse2_voltage_code;
  uint32 fuse1_voltage = 
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail].fuse1_voltage_uv /1000; //convert to mV 
  uint32 fuse2_voltage = 
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail].fuse2_voltage_uv / 1000; //convert to mV

  slope_threshold = 3 * ((fuse2_voltage - fuse1_voltage) / (fuse2_code - fuse1_code));
        
  SWEVENT(VSENSE_EVENT_SLOPE_THRESHOLD, rail, slope_threshold, delta_cycle);
 
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  vsense_config_common(rail);
   
  val = inpdw(addr_config_0);
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, TRIG_POS);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, TRIG_POS, VSENSE_TRIG_MID);
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, CAPTURE_DELAY);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, CAPTURE_DELAY, 0);
  outpdw(addr_config_0, val);

  val  = inpdw(addr_config_1);
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, SLOPE_THRESHOLD);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, SLOPE_THRESHOLD, slope_threshold); //may not cross min-max but still a transient to catch 
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, DELTA_CYCLE);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, DELTA_CYCLE, delta_cycle); //diff between 1st and 3rd sample
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, FUNC_EN);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, FUNC_EN, 1);
  outpdw(addr_config_1, val);
  //need to write twice to the same register since sw
  //needs to maintain the state machine for Elessar
  //DO NOT OPTMIZE THIS CODE TO A SINGLE REGISTER WRITE
  val &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, ALARM_SLOPE_ENABLE);
  val |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, ALARM_SLOPE_ENABLE, 1);
  outpdw(addr_config_1, val);

  
  vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en = TRUE;
   */
}
//configure the vsense to capture min and max for a given duration
void vsense_set_mode_min_max(vsense_type_rail rail)
{
  uint32 val_config_0;
  uint32 val_config_1;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
    //logsomething and return
    return;
  }
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  vsense_config_common(rail);
  
  val_config_0  = inpdw(addr_config_0);
  val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, MODE_SEL, 1);
  outpdw(addr_config_0, val_config_0);

  val_config_1  = inpdw(addr_config_1);
  val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, FUNC_EN, 1);
  outpdw(addr_config_1, val_config_1);


  val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, SW_CAPTURE, 1);
  outpdw(addr_config_0, val_config_0);

  DALSYS_BusyWait(1000000); 

  val_config_0 &= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, SW_CAPTURE, 0);
  outpdw(addr_config_0, val_config_0);
  
  vsense_rail_and_fuse_info.rail_info[rail].is_vsense_pwr_en = TRUE;

}

//get vsense config data 
void* vsense_get_config_info(uint32 prop_id)
{
  void* cfg_ptr = NULL;

  if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp_vsense, NULL, prop_id, &prop_vsense))
  {
      cfg_ptr = (void*) prop_vsense.Val.pStruct;
  }

  return cfg_ptr;
}

//calculate the code point based on input voltage
uint32 vsense_cal_voltage_code(vsense_type_rail rail, uint32 input_uv)
{
  int32 voltage_code = 0;
  int32 input_volt = input_uv;
  int32 fuse1_code = 
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail].fuse1_voltage_code;
  int32 fuse2_code = 
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail].fuse2_voltage_code;
  int32 fuse1_voltage = 
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail].fuse1_voltage_uv /1000; //convert to mV 
  int32 fuse2_voltage = 
      vsense_rail_and_fuse_info.smem_info.rails_fuse_info[rail].fuse2_voltage_uv / 1000; //convert to mV

  //voltage_code = (fuse1_code)+ (fuse2_code - fuse1_code )*((input_uv/1000) - 
  //               fuse1_voltage)/(fuse2_voltage - fuse1_voltage) ;
  SWEVENT(VSENSE_EVENT_CAL_VOLT_CODE, fuse1_code, fuse1_voltage, fuse2_code, fuse2_voltage );
  /*voltage_code =  (fuse1_code)+ (float)(((input_volt/1000) - fuse1_voltage))/
                   ((float)(fuse2_voltage - fuse1_voltage) / (float)(fuse2_code - fuse1_code));*/
  voltage_code =  (fuse1_code)+ (((input_volt/1000) - fuse1_voltage) * (fuse2_code - fuse1_code))/
                   (fuse2_voltage - fuse1_voltage); //Update the formula for INT calculation

  return voltage_code;
}

//disable and reset the vsense
void vsense_disable_and_reset(vsense_type_rail rail)
{
  uint32 val_config_0 ;
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  val_config_0 = 0x00FF0000; //POR value from the HPG
  val_config_1 = 0x00001FE0; //POR value from the HPG
  /*val_config_0 = inpdw(addr_config_0);
  val_config_1 = inpdw(addr_config_1);
  
  val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, FUNC_EN);
  outpdw(addr_config_1, val_config_1);

  val_config_0 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, SW_CAPTURE); 
  outpdw(addr_config_0, val_config_0);

  val_config_0 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, RESET_FUNC);
  outpdw(addr_config_0, val_config_0);


  val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, RESET_FUNC, 1);
  outpdw(addr_config_0, val_config_0);
*/
   outpdw(addr_config_0, val_config_0);
   outpdw(addr_config_1, val_config_1); 


}

void vsense_clear_bit_enable(vsense_type_rail rail, boolean enable)
{
  uint32 val_config_1 =0;
  uint32 addr_config_1 ;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;

  if(enable == TRUE)
  {
    val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, CLEAR);
  }
  else
  {
    val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, CLEAR, 1);
  }

   outpdw(addr_config_1, val_config_1); 

}
void vsense_log_rail_status()
{
  int32 rail_count;
  uint32 addr_status;
  uint32 val_status_reg;
  static uint32 val_config_0;
  static uint32 val_config_1;
  uint32 addr_config_1;
  uint32 addr_config_0;
  static uint8* fifo_array = NULL;
  uint32 fifo_log_loop;
  uint32* fifo_ptr = NULL;
  
  for( rail_count = 0; rail_count < VSENSE_TYPE_RAIL_APC0; rail_count++ )
  {
    if(vsense_rail_and_fuse_info.rail_info[rail_count].is_supported)
    {
      addr_status = vsense_rail_and_fuse_info.rail_info[rail_count].status_address;
      val_status_reg = inpdw(addr_status);
      
      if(val_status_reg &  VSENSE_STATUS_ALARM_MASK)
      {
        addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail_count].config_1_address;
        addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail_count].config_0_address;

        val_config_0 = inpdw(addr_config_0);
        val_config_1 = inpdw(addr_config_1);

        SWEVENT(VSENSE_EVENT_STATUS, rail_count, 
                  (val_config_0 & HWIO_FMSK(TCSR_VSENS0_CONFIG_0, THRESHOLD_MIN)) >> HWIO_SHFT(TCSR_VSENS0_CONFIG_0, THRESHOLD_MIN) ,
                  (val_config_0 & HWIO_FMSK(TCSR_VSENS0_CONFIG_0, THRESHOLD_MAX)) >> HWIO_SHFT(TCSR_VSENS0_CONFIG_0, THRESHOLD_MAX) ,
                  (val_config_1 & HWIO_FMSK(TCSR_VSENS0_CONFIG_0, THRESHOLD_SLOPE)) >> HWIO_SHFT(TCSR_VSENS0_CONFIG_0, THRESHOLD_SLOPE));
        
        SWEVENT(VSENSE_EVENT_ERROR_STATUS,
                  (val_status_reg & HWIO_FMSK(TCSR_VSENS0_STATUS, ALARM_MIN)) >> HWIO_SHFT(TCSR_VSENS0_STATUS, ALARM_MIN), 
                  (val_status_reg & HWIO_FMSK(TCSR_VSENS0_STATUS, ALARM_MAX)) >> HWIO_SHFT(TCSR_VSENS0_STATUS, ALARM_MAX),
                  (val_status_reg & HWIO_FMSK(TCSR_VSENS0_STATUS, ALARM_SLOPE_POS)) >> HWIO_SHFT(TCSR_VSENS0_STATUS, ALARM_SLOPE_POS),
                  (val_status_reg & HWIO_FMSK(TCSR_VSENS0_STATUS, ALARM_SLOPE_NEG)) >> HWIO_SHFT(TCSR_VSENS0_STATUS, ALARM_SLOPE_NEG));
        
        vsense_rail_and_fuse_info.rail_info[rail_count].status = val_status_reg;
        vsense_rail_and_fuse_info.rail_info[rail_count].config_0 = val_config_0;
        vsense_rail_and_fuse_info.rail_info[rail_count].config_1 = val_config_1;
        fifo_array = vsense_rail_and_fuse_info.rail_info[rail_count].fifo_data;  
        vsense_read_fifo(vsense_rail_and_fuse_info.rail_info[rail_count].rail,
                         fifo_array );
        
        fifo_ptr = (uint32*)fifo_array;       
        fifo_ptr+= 16; 
        fifo_ptr--;
        for(fifo_log_loop =0; fifo_log_loop < 4 ; fifo_log_loop++)
        {
          SWEVENT(VSENSE_EVENT_FIFO, *(fifo_ptr - 0), *(fifo_ptr - 1), *(fifo_ptr - 2), *(fifo_ptr - 3));
          fifo_ptr = fifo_ptr - 4;
        }
        //we detected alarm, reset the sensor and re-arm
        vsense_disable_and_reset(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
        //previous voltage should be valid since alarm would not be set if we are coming out of SVS 
        //going into SVS should have a valid voltage
        vsense_set_mode_alarm(vsense_rail_and_fuse_info.rail_info[rail_count].rail,
                             vsense_rail_and_fuse_info.rail_info[rail_count].previous_uv);
        vsense_set_mode_slope_detection(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
       // abort();
      }
    }
  } 
}

void vsense_unreset_and_cgc_clk_enable(vsense_type_rail rail)
{
  //uint32 val_config_0 ;
//  uint32 val_config_1 ;
 // uint32 addr_config_1 = 0;
  //uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  //addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  //addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  //val_config_0 = inpdw(addr_config_0);
  //val_config_1 = inpdw(addr_config_1);

  //val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_0, RESET_FUNC, 1);
  //outpdw(addr_config_0, val_config_0);

  //val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, CGC_CLOCK_ENABLE, 1);
  //outpdw(addr_config_1, val_config_1);

}


void vsense_reset_and_cgc_clk_disable(vsense_type_rail rail)
{
  //uint32 val_config_0 ;
  //uint32 val_config_1 ;
  //uint32 addr_config_1 = 0;
  //uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  //addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  //addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  //val_config_0 = inpdw(addr_config_0);
  //val_config_1 = inpdw(addr_config_1);


  //val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, CGC_CLOCK_ENABLE);
  //outpdw(addr_config_1, val_config_1);


  //val_config_0 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_0, RESET_FUNC);
  //outpdw(addr_config_0, val_config_0);

}


//read the 64 bytes fifo from the status register
void vsense_read_fifo(vsense_type_rail rail, uint8* fifo_data)
{
  uint32 addr_config_1 ;
  uint32 addr_status ;
  uint32 val_config_1 = 0;
  uint32 val_status_reg = 0; 
  int8  fifo_reg_offset  = 0x3F;
  uint8  read_byte_count = 0;
     
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   return;
  }
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_status = vsense_rail_and_fuse_info.rail_info[rail].status_address;
  val_status_reg = inpdw(addr_status);
 
  if(val_status_reg & HWIO_FVAL(TCSR_VSENS0_STATUS, FIFO_COMPLETE, 1))
  {
    do
    {
      val_config_1 = inpdw(addr_config_1);
      val_config_1 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, FIFO_RD_ADDRESS);
      val_config_1 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, FIFO_RD_ADDRESS, fifo_reg_offset);
      outpdw(addr_config_1, val_config_1);
      DALSYS_BusyWait(VSENSE_FIFO_READ_DELAY); //10 XO clk  cycles
      val_status_reg = inpdw(addr_status);
      fifo_data[read_byte_count++] = 
      (val_status_reg & HWIO_FMSK(TCSR_VSENS0_STATUS, FIFO_DATA)) >>
      HWIO_SHFT(TCSR_VSENS0_STATUS, FIFO_DATA);
      fifo_reg_offset--;
    }while (fifo_reg_offset >= 0);
  }
}

//start capture enable disable
void vsense_start_capture(vsense_type_rail rail, boolean en_capture)
{
// TODO remove it for memory constraint
  uint32 val_config_0 ;
  uint32 addr_config_0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;
  val_config_0 = inpdw(addr_config_0);

  
  if(en_capture == TRUE)
  {
  val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, SW_CAPTURE, 1);
  }
  else
  {
    val_config_0 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, SW_CAPTURE);
  }

  outpdw(addr_config_0, val_config_0);
 //*/
  
}


void vsense_config_watermark_mode(vsense_type_rail rail)
{
// TODO remove it for memory constraint
  uint32 addr_config_0 ;
  uint32 val_config_0 = 0;
     
  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   return;
  }
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  vsense_disable_and_reset(rail);
  vsense_config_common(rail);
  
  val_config_0 = inpdw(addr_config_0);
  val_config_0 &= ~HWIO_FMSK(TCSR_VSENS0_CONFIG_1, MODE_SEL);
  val_config_0 |= HWIO_FVAL(TCSR_VSENS0_CONFIG_1, MODE_SEL, VSENSE_TYPE_WATERMARK_MODE);

  outpdw(addr_config_0, val_config_0);
  
  vsense_en(rail, TRUE);
  vsense_start_capture(rail, TRUE); 
  vsense_watermark_mode_start_timestamp = time_service_now();
   //*/
}

void vsense_check_watermark_duration(void)
{
// TODO remove it for memory constraint
  uint8 rail_count;
  static uint8* fifo_array = NULL;
  uint64 current_time = time_service_now();

  //we have expired the duration
  if ( vsense_dal_data->duration_us < vsense_convert_timetick_to_time(current_time - 
  	                               vsense_watermark_mode_start_timestamp))
  {
    //stop the capture and log the data
    for( rail_count = 0; rail_count < VSENSE_TYPE_RAIL_APC0; rail_count++ )
    {
      if(vsense_rail_and_fuse_info.rail_info[rail_count].is_supported)
      {
        vsense_start_capture(vsense_rail_and_fuse_info.rail_info[rail_count].rail, FALSE);
        fifo_array = vsense_rail_and_fuse_info.rail_info[rail_count].fifo_data;  
        vsense_read_fifo(vsense_rail_and_fuse_info.rail_info[rail_count].rail,
                         fifo_array );
        
        SWEVENT(VSENSE_EVENT_FIFO, fifo_array[63], fifo_array[62], fifo_array[61], fifo_array[60]);
        vsense_config_watermark_mode(vsense_rail_and_fuse_info.rail_info[rail_count].rail);
      }
    }

  }
  //*/
}

int g_sleep_enter = 0;
int g_sleep_exit = 0;

void vsense_enter_sleep(bool enter)
{
  vsense_type_rail rail_count = VSENSE_TYPE_RAIL_MX;
  for( rail_count = VSENSE_TYPE_RAIL_MX; rail_count < VSENSE_TYPE_RAIL_APC0; rail_count++ )
  {
    //mark the vsense as not supported if the calibration is fails in BOOT 
    if(vsense_rail_and_fuse_info.rail_info[rail_count].is_supported == TRUE)
    {      
      if (TRUE == enter)
      {
          vsense_alarm_en(rail_count, FALSE);
          vsense_en(rail_count, FALSE);
          vsense_clear_bit_enable(rail_count, FALSE);
          vsense_clear_bit_enable(rail_count, TRUE);
          vsense_reset_and_cgc_clk_disable(rail_count);
	      vsense_power_en(rail_count, FALSE); //enter will be true for sleep enter so negation of the flag
          Clock_DisableClock( vsense_clk_array[vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MX].vsense_clk_id].clk_id); 
          g_sleep_enter++;
      }
      else
      {
          Clock_EnableClock( vsense_clk_array[vsense_rail_and_fuse_info.rail_info[VSENSE_TYPE_RAIL_MX].vsense_clk_id].clk_id);
		  //vsense_set_mode_alarm(vsense_rail_and_fuse_info.rail_info[rail_count].rail,
            //                 vsense_rail_and_fuse_info.rail_info[rail_count].previous_uv);
          g_sleep_exit++;
      }
    }
  }
}


//disable and reset the vsense
void vsense_power_down(vsense_type_rail rail)
{
  uint32 val_config_0 ;
  uint32 val_config_1 ;
  uint32 addr_config_1 = 0;
  uint32 addr_config_0 = 0;

  if(vsense_rail_and_fuse_info.rail_info[rail].is_supported == FALSE)
  {
   //logsomething and return
   return;
  }
  
  addr_config_1 = vsense_rail_and_fuse_info.rail_info[rail].config_1_address;
  addr_config_0 = vsense_rail_and_fuse_info.rail_info[rail].config_0_address;

  val_config_0 = 0x00FF0000;
  val_config_1 = 0x00001FE0;
  
  outpdw(addr_config_0, val_config_0);
  outpdw(addr_config_1, val_config_1); 
  
}


#endif
