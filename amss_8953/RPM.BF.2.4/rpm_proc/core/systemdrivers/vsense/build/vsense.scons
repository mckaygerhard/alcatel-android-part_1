#===============================================================================
#
#
# GENERAL DESCRIPTION
#
# Copyright (c) 2011-2015 Qualcomm Technologies Incorporated. All Rights Reserved
#
# Qualcomm Proprietary
#
# Export of this technology or software is regulated by the U.S. Government.
# Diversion contrary to U.S. law prohibited.
#
# All ideas, data and information contained in or disclosed by
# this document are confidential and proprietary information of
# Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
# By accepting this material the recipient agrees that this material
# and the information contained therein are held in confidence and in
# trust and will not be used, copied, reproduced in whole or in part,
# nor its contents revealed in any manner to others without the express
# written permission of Qualcomm Technologies Incorporated.
#
#-------------------------------------------------------------------------------
#
# $Header: //components/rel/rpm.bf/2.4/core/systemdrivers/vsense/build/vsense.scons#2 $
# $DateTime: 2016/01/13 21:29:37 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 06/17/15   akt     First revision 
#===============================================================================

Import('env')
env = env.Clone()



supported_targets = ['8994', '8996', '8976', '8956', '8953']


if env['MSM_ID'] not in supported_targets:
    env.PrintWarning('Bailing from VSENSE scripts; Voltage sensor is NOT supported for %s;Supported targets %s' % (env['MSM_ID'], supported_targets))
    Return()

#### NOT using the USES as have one control to enable the CPPDEFINES from RPM.scons
#if 'USES_VSENSE' in env:
#    env.Append(CPPDEFINES = 'VSENSE_DRIVER_ENABLED')
#    env.PrintWarning('****************************** VSENSE driver is compiled completely *************************')
#else:
#    env.PrintWarning('****************************** VSENSE driver is NOT compiled completely *************************')
#    #Return()

	
#-----------------------------------------------------------------------------
# Define paths
#-----------------------------------------------------------------------------

SRCPATH = "${BUILD_ROOT}/core/systemdrivers/vsense/src/"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------

CBSP_API = [
   'BOOT',
   'DAL',
   'BUSES',
   'MPROC',
   'HAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'POWER',
   'KERNEL',
   'DEBUGTRACE'
]

env.Append(CCFLAGS = ' --c99') #needed for designated initializers
env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

env.PublishPrivateApi('SYSTEMDRIVERS_VSENSE', [
   '${INC_ROOT}/core/systemdrivers/vsense/inc/${CHIPSET}',
   '${INC_ROOT}/core/systemdrivers/vsense/inc',
])

env.RequirePrivateApi('SYSTEMDRIVERS_VSENSE')

VSENSE_SOURCES =  [
  '${BUILDPATH}/${CHIPSET}/vsense.c',
  '${BUILDPATH}/vsense_utils.c'
]

env.AddLibrary(['RPM_IMAGE' ], '${BUILDPATH}/vsense', VSENSE_SOURCES)

#-------------------------------------------------------------------------------
# SWEvent processing
#-------------------------------------------------------------------------------
if 'USES_QDSS_SWE' in env:
   QDSS_IMG = ['QDSS_EN_IMG']
   events = [['VSENSE_EVENT_FIRST=600',             'vsense first event placeholder'],
             ['VSENSE_EVENT_INIT',                  'vsense not initialized, smem NULL'],
             ['VSENSE_EVENT_POST_VDD_CB_THRESHOLD', 'vsense rail id :%d cur uv: %d, voltage_code=0x%x min_thresh :0x%x'],
             ['VSENSE_EVENT_PRE_VDD_CB_THRESHOLD',  'vsense rail id :%d cur uv: %d, voltage_code=0x%x min_thresh :0x%x'],
             ['VSENSE_EVENT_CONFIG_ALARM',          'vsense rail id :%d cur uv: %d, voltage_code=0x%x min_thresh :0x%x'],
             ['VSENSE_EVENT_SLOPE_THRESHOLD',       'vsense slope rail id :%d threshold:0x%x, delta_cycle:%d'],
             ['VSENSE_EVENT_STATUS',                'vsense_status: rail_id: %d min thresh:0x%x , max thresh:0x%x, slope threshold:0x%x '],
             ['VSENSE_EVENT_ERROR_STATUS',          'vsense_status: min_alarm :%d , max_alarm :%d, slope pos:%d, slope neg:%d '],
             ['VSENSE_EVENT_FIFO',                  'vsense_fifo:[0x] 0x%x | 0x%x | 0x%x | 0x%x'],
             ['VSENSE_EVENT_PRE_VDD_CB',            'vsense PRE VDD CB : prev corner:%d new corner:%d'],
             ['VSENSE_EVENT_POST_VDD_CB',           'vsense POST VDD CB'],
             ['VSENSE_EVENT_CAL_VOLT_CODE',         'vsense calc_volt_code fuse1_code:0x%x, fuse1_voltage:%d, fuse2_code:0x%x, fuse2_voltage:%d'],
             ['VSENSE_EVENT_ISR',                   'vsense ISR TRIGGERED'],
             ['VSENSE_EVENT_RAIL_NOT_SUPPORTED',    'vsense rail %d NOT SUPPORTED '],
             ['VSENSE_EVENT_RAIL_NOT_CALIBRATED',   'vsense rail %d NOT CALIBRATED '],    
             ['VSENSE_EVENT_LAST=639',              'vsense last event placeholder']
            ]
   env.AddSWEInfo(QDSS_IMG, events)

if 'USES_DEVCFG' in env:
    DEVCFG_IMG = ['DAL_DEVCFG_IMG']
    LIB_NAME        = 'vsense_config'
    env.AddDevCfgInfo(DEVCFG_IMG,{
        'devcfg_xml' : '${BUILD_ROOT}/core/systemdrivers/vsense/config/${CHIPSET}/vsense_config.xml'
    })
    env.AddLibrary(DEVCFG_IMG, '${BUILDPATH}/'+LIB_NAME+'_'+'${CHIPSET}', '${BUILD_ROOT}/core/systemdrivers/vsense/config/${CHIPSET}/vsense_config.c')

  

