/**
 * @file:  cpr_isr.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/01/22 01:57:42 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/common/src/cpr_isr.c#2 $
 * $Change: 9769842 $
 */
#include "cpr_cfg.h"
#include "cpr_enablement.h"
#include "cpr_rail.h"
#include "cpr_defs.h"

boolean cpr_isr_get_interrupt(cpr_domain_id railId, uint32 *interrupt)
{
    cpr_rail* rail = cpr_get_rail (railId);
    
        if(rail->id == railId) {
            *interrupt = rail->interruptId;
            return true;
        }
    return false;
}

void cpr_isr_process(cpr_domain_id railId)
{
    cpr_rail_isr( cpr_get_rail( railId ) );
}
