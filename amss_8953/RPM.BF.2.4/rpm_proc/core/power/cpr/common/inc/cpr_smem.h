/**
 * @file:  cpr_smem.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/10 04:32:44 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/common/inc/cpr_smem.h#2 $
 * $Change: 9561832 $
 */
#ifndef CPR_SMEM_H
#define CPR_SMEM_H

#include "cpr_cfg.h"
#include "cpr_data.h"

void cpr_smem_deserialize_config(cpr_rail* rail, cpr_rail_state* state);
void cpr_smem_serialize_config(cpr_rail_state* state, boolean append);

#endif
