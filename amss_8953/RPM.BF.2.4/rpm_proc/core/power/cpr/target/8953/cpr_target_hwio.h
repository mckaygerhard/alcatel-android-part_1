/**
 * @file:  cpr_target_hwio.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/10 04:32:44 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/target/8953/cpr_target_hwio.h#1 $
 * $Change: 9561832 $
 *
 */
#ifndef CPR_TARGET_HWIO_H
#define CPR_TARGET_HWIO_H

#include "msmhwiobase.h"
#include "cpr_hwio.h"
#include "cpr_fuses_hwio.h"

#endif /* CPR_TARGET_HWIO_H */
