/**
 * @file:  cpr_image_defs.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/10 04:32:44 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/image/rpm/inc/cpr_image_defs.h#1 $
 * $Change: 9561832 $
 */
#ifndef CPR_IMAGE_DEFS_H
#define	CPR_IMAGE_DEFS_H

#include "HALhwio.h"
#include "Chipinfo.h"
#include "CoreVerify.h"
#include "msmhwiobase.h"

#define CPR_CHIPINFO_VERSION CHIPINFO_VERSION
#define CPR_ASSERT(x) CORE_VERIFY(x)

#endif

