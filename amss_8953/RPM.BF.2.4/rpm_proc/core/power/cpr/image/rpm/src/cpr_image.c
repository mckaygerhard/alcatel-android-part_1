/**
 * @file:  cpr_image.c
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/03/30 05:04:26 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/image/rpm/src/cpr_image.c#6 $
 * $Change: 10169006 $
 */
#include <stdlib.h>
#include "cpr_image.h"
#include "cpr_image_target.h"
#include "cpr_resource.h"
#include "cpr_cfg.h"
#include "cpr_data.h"
#include "cpr.h"
#include "cpr_utils.h"

#include "rpmserver.h"
#include "railway.h"
#include "CoreVerify.h"
#include "ClockDefs.h"
#include "Clock.h"
#include "cortex-m3.h"
#include "swevent.h"
#include "Chipinfo.h"

#include <stringl/stringl.h>

void RBCPRTaskISR(void* task);
void* RBCPRTaskInit(cpr_image_isr func, void* ctx, uint32_t client_interrupt_id);

//******************************************************************************
// Global Data
//******************************************************************************

typedef struct
{
    cpr_domain_id   cpr_rail_id;
    const char*     railway_name;
} rail_name_map;

const static rail_name_map railway_name_map[] =
{
    { CPR_RAIL_MX,       "vddmx"      },
    { CPR_RAIL_CX,       "vddcx"      },
    { CPR_RAIL_GFX,      "vddgfx"     },
    { CPR_RAIL_VDDA,     "vdda_ebi"   },
    { CPR_RAIL_SSC_MX,   "vdd_ssc_mx" },
    { CPR_RAIL_SSC_CX,   "vdd_ssc_cx" },
};

typedef struct
{
    boolean         init;
    int32           railway_rail_id;
    railway_voter_t voter;
} railway_handle;

typedef struct
{
    uint32 interrupt;
    void* task;
} cpr_isr_data;

static cpr_isr_data isr_data[CPR_NUM_RAILS] = {0};
static railway_handle handles[CPR_NUM_RAILS] = {0};

//******************************************************************************
// Local Helper Functions
//******************************************************************************

static const char* cpr_rpm_railway_rail_name(cpr_domain_id rail_id)
{
    for(int i = 0; i < (sizeof(railway_name_map) / sizeof(rail_name_map)); i++) {
        if(rail_id == railway_name_map[i].cpr_rail_id) {
            return railway_name_map[i].railway_name;
        }
    }

    CPR_ASSERT( 0 );
}

static railway_handle* get_railway_handle(cpr_domain_id railId)
{
    uint32 railIdx = cpr_get_rail_idx( railId );

    if(!handles[railIdx].init)
    {
        handles[railIdx].railway_rail_id = rail_id( cpr_rpm_railway_rail_name( railId ) );
        CPR_ASSERT( handles[railIdx].railway_rail_id != RAIL_NOT_SUPPORTED_BY_RAILWAY );

        handles[railIdx].init = true;
    }

    return &handles[railIdx];
}

/*
 * function: cpr_image_do_set_corner_voltage
 * 
 * Make sure voltages for modes are in increasing order
 */
 
static void cpr_image_do_set_corner_voltage(int railIdx, int modeIdx, uint32 recommendation)
{

	cpr_mode_settings *modeSettings = &cpr_info.railStates[railIdx].modeSettings[modeIdx];
	
    if(modeIdx>0)
    {
		cpr_mode_settings *prev_modeSettings = &cpr_info.railStates[railIdx].modeSettings[modeIdx-1];
		uint32 prev_corner_voltage = prev_modeSettings->subModes->current;
		
		/* Recurse until previous mode voltage is less than microvolts
		 * to satisfy voltages are in increasing order.
		 */
		 
		if(prev_corner_voltage > recommendation)
			cpr_image_do_set_corner_voltage(railIdx, (modeIdx-1), recommendation); //Should recurse such that voltages for modes can't overlap
    }

    if(modeIdx < (cpr_info.railStates[railIdx].modeSettingsCount-1) )
    {
		cpr_mode_settings *next_modeSettings = &cpr_info.railStates[railIdx].modeSettings[modeIdx+1];
		uint32 next_corner_voltage = next_modeSettings->subModes->current;
		
		/* Recurse until next mode voltage is greater than microvolts
		 * to satisfy voltages are in increasing order.
		 */
		 
		if(next_corner_voltage < recommendation)
			cpr_image_do_set_corner_voltage(railIdx, (modeIdx+1), recommendation); //Should recurse such that voltages for modes can't overlap
    }
	
	modeSettings->subModes->current = recommendation;
}

void cpr_image_set_rail_mode_voltage(cpr_rail* rail, cpr_voltage_mode mode, uint32 recommendation)
{	
	uint32            railIdx        =  rail->railIdx;
	uint32 			  modeIdx 		 =  rail->vp->idxLookupFunc(mode);
	
	cpr_image_do_set_corner_voltage(railIdx, modeIdx, recommendation);
    
    //Now verify that the voltages are increasing with the corner
    for(int i=0; i< (cpr_info.railStates[railIdx].modeSettingsCount-1) ; i++)
    {
		cpr_mode_settings *curr_modeSettings = &cpr_info.railStates[railIdx].modeSettings[i];
		cpr_mode_settings *next_modeSettings = &cpr_info.railStates[railIdx].modeSettings[i+1];
        CORE_VERIFY(curr_modeSettings->subModes->current <= next_modeSettings->subModes->current);
    }
	
}

static void cpr_image_pre_switch(const railway_settings *settings, void* ctx)
{
    cpr_domain_id railId = (cpr_domain_id)(uintptr_t)ctx;
    cpr_pre_state_switch( railId );
}

static void cpr_image_post_switch(const railway_settings *settings, void* ctx)
{
    cpr_domain_id railId = (cpr_domain_id)(uintptr_t)ctx;
    cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {(cpr_voltage_mode)settings->mode}};
    cpr_post_state_switch( railId, &info, settings->microvolts );
}

static void cpr_isr() __irq
{
    uint32 interrupt = interrupt_current_isr();
    interrupt_clear( interrupt );

    for(int i = 0; i < CPR_NUM_RAILS; i++)
    {
        if(isr_data[i].interrupt == interrupt)
        {
            RBCPRTaskISR( isr_data[i].task );
            return;
        }
    }

    CPR_ASSERT( 0 );
}

//******************************************************************************
// Public API Functions
//******************************************************************************

void* cpr_image_malloc(uint32 size)
{
    void* buf = calloc( 1, size );
    CPR_ASSERT( buf );
    return buf;
}

void cpr_image_free(void* buf)
{
    free( buf );
}

void cpr_image_memscpy(void* dst, void* src, uint32 size)
{
    memscpy(dst, size, src, size);
}

void cpr_image_register_thermal_cb(cpr_therm_cb cb)
{
    rpm_register_resource( RPM_RBCPR_REQ, 1, sizeof(cpr_therm_notification), cpr_resource_xlate, cpr_resource_apply, cb );
}

void cpr_image_register_isr(cpr_domain_id railId, uint32 interrupt, cpr_image_isr isr, void* ctx)
{
    railway_handle* hdl = get_railway_handle( railId );
    uint32 railIdx = cpr_get_rail_idx( railId );

    isr_data[railIdx].interrupt = interrupt;
    isr_data[railIdx].task = RBCPRTaskInit(isr, ctx, interrupt);

    interrupt_set_isr( interrupt, cpr_isr );
    interrupt_configure( interrupt, RISING_EDGE );
    interrupt_clear( interrupt );
    interrupt_enable( interrupt );

    railway_set_callback( hdl->railway_rail_id, RAILWAY_PRECHANGE_CB, cpr_image_pre_switch, (void*) railId );
    railway_set_callback( hdl->railway_rail_id, RAILWAY_POSTCHANGE_CB, cpr_image_post_switch, (void*) railId );
}

void cpr_image_enable_clock(const char* clkId)
{
    //Clock_EnableClock( clkId );
}

uint32 cpr_image_get_chip_version()
{
    return Chipinfo_GetVersion();
}

cpr_foundry_id cpr_image_get_foundry()
{
    return CPR_FOUNDRY_SS;
}

boolean cpr_image_set_rail_mode(cpr_domain_id railId, cpr_domain_info* info)
{
    railway_handle* hdl = get_railway_handle( railId );

    if(!hdl->voter) {
        hdl->voter = railway_create_voter( hdl->railway_rail_id, true, RAILWAY_CPR_SETTLING_VOTER );
    }

    railway_corner_vote( hdl->voter, (railway_corner) info->u.mode );
    railway_transition_rail( hdl->railway_rail_id );

    return true;
}

boolean cpr_image_rail_transition_voltage(cpr_domain_id railId)
{
    railway_handle* hdl = get_railway_handle( railId );
    railway_transition_rail( hdl->railway_rail_id );

    return true;
}

void cpr_image_enter_sleep(void)
{
    /* TODO: do we want to use cpr_relinquish_control()? It sets the voltage to safe voltage. cpr_relinquish_control() was implemented for Modem MVC use case. */
    for(int i = 0; i < CPR_NUM_RAILS; i++)
    {
        if(!cpr_utils_is_closed_loop_mode((cpr_domain_id)i))
        {
            continue;
        }

        cpr_relinquish_control( (cpr_domain_id)i );
    }
}

void cpr_image_exit_sleep(void)
{
    /* TODO: may need to fix this. When exiting from sleep, activeMode might be NULL.*/
    for(int i = 0; i < CPR_NUM_RAILS; i++)
    {
        if(!cpr_utils_is_closed_loop_mode((cpr_domain_id)i))
        {
            continue;
        }

        cpr_domain_id railId = (cpr_domain_id)i;
        uint32 railIdx = cpr_get_rail_idx( railId );
        cpr_voltage_mode mode = cpr_info.railStates[railIdx].activeMode->mode;
        cpr_domain_info info = {CPR_DOMAIN_TYPE_MODE_BASED, {mode}};

        cpr_resume_control( railId, &info, cpr_get_voltage( railId, &info ) );
    }
}

void cpr_image_open_remote_cfg(void** cfg, uint32* size)
{
    cpr_image_target_open_remote_cfg(cfg, size);
}

void cpr_image_close_remote_cfg()
{
    cpr_image_target_close_remote_cfg();
}

void cpr_image_wait(uint32 us)
{
    DALSYS_BusyWait( us );
}

void rbcpr_core_dump()
{

}

uint32 cpr_image_get_eldo_voltage(cpr_domain_id railId, cpr_domain_info* info)
{
    return 0;
}

boolean cpr_image_can_resume_control(cpr_domain_id railId)
{
    return true;
}

void cpr_image_enable_measurements()
{
}

void cpr_image_disable_measurements()
{
}

