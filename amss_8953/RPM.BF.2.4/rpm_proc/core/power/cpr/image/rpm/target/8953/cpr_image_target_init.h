/**
 * @file:  cpr_image_target_init.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2016/02/24 05:14:41 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/image/rpm/target/8953/cpr_image_target_init.h#3 $
 * $Change: 9950595 $
 */
#ifndef CPR_IMAGE_TARGET_INIT_H
#define CPR_IMAGE_TARGET_INIT_H

#include "cpr_cfg.h"

//******************************************************************************
// Default Enablement Structures
//******************************************************************************

extern cpr_cfg_funcs CPR_INIT_NONE; /* used to disable CPR */
extern cpr_cfg_funcs CPR_INIT_SMEM;
extern cpr_cfg_funcs CPR_INIT_OPEN_LOOP;
extern cpr_cfg_funcs CPR_INIT_SW_CLOSED_LOOP;

#endif

