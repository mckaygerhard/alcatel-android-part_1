/**
 * @file:  cpr_image_logs.h
 * @brief:
 *
 * Copyright (c) 2015 by Qualcomm Technologies Incorporated. All Rights Reserved.
 *
 * $DateTime: 2015/12/10 04:32:44 $
 * $Header: //components/rel/rpm.bf/2.4/core/power/cpr/image/rpm/inc/cpr_image_logs.h#1 $
 * $Change: 9561832 $
 */
#ifndef CPR_IMAGE_LOGS_H
#define CPR_IMAGE_LOGS_H

#include <stdbool.h>
#include "ULogFront.h"
#include "CoreVerify.h"

#define CPR_LOG_NAME "CPR Log"
#define CPR_LOG_SIZE 128

extern ULogHandle cprLogHandle;

#define CPR_IMAGE_LOG_INIT() ULogFront_RealTimeInit(&cprLogHandle, \
                                                    CPR_LOG_NAME, \
                                                    CPR_LOG_SIZE, \
                                                    ULOG_MEMORY_LOCAL, \
                                                    ULOG_LOCK_OS)

#define GET_COUNT_MACRO(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,COUNT,...) COUNT
#define DO_ULOG(...) ULogFront_RealTimePrintf(cprLogHandle, GET_COUNT_MACRO(__VA_ARGS__,10,9,8,7,6,5,4,3,2,1,0), __VA_ARGS__)

#define CPR_IMAGE_LOG_FATAL(msg, args...)     do { DO_ULOG("Fatal: "msg, ##args); CPR_ASSERT(false); } while(false)
#define CPR_IMAGE_LOG_ERROR(msg, args...)     DO_ULOG("Error: "msg, ##args)
#define CPR_IMAGE_LOG_WARNING(msg, args...)   DO_ULOG("Warn: "msg, ##args)
#define CPR_IMAGE_LOG_INFO(msg, args...)      DO_ULOG("Info: "msg, ##args)
#define CPR_IMAGE_LOG_VERBOSE(msg, args...)   DO_ULOG("Verbose: "msg, ##args)
#define CPR_IMAGE_LOG_TRACE(msg, args...)     DO_ULOG("Trace: "msg, ##args)
#define CPR_IMAGE_LOG_TRACE_RAW(msg, args...) CPR_IMAGE_LOG_TRACE(msg, ##args)

#endif

