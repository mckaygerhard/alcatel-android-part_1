#ifndef __DDR_STATUSREG_H__
#define __DDR_STATUSREG_H__

/*=============================================================================

                              DDR HAL
                            Header File
GENERAL DESCRIPTION
This is the header file that describe the DDR status registers to be logged during clock/voltage switch.
this can be extended to log any other BIMC/PHY registers that aid in debugging .

Copyright 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.

===========================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/2.4/core/boot/ddr/hw/msm8953/ddr_status_reg.h#3 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
05/16/16   sc      Added DDR_CC registers for status register dump feature
09/02/15   sc      Initial version.
=============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"

/*==============================================================================
                                  MACROS
==============================================================================*/

#define MAX_STATUS_REG 40

/*==============================================================================
                                 DATA TYPES
==============================================================================*/

typedef enum
{
  PRE_CLK_VOL_SWITCH_LOGGING =0,
  POST_CLK_VOL_SWITCH_LOGGING,
  ERROR_ABORT,
} reg_status_log;

/* lets have structure element names same as IPCAT/SWI defined names for ease of debugging,
 * ensure that an entry here should have its HWIO address in the corresponding location 
 * of the array(ddr_status_reg_addr[]) which is defined in ddr_status_reg.c file */
typedef struct 
{
  uint32 DDR_CC_DDRCC_PLLCTRL_CLK_SWITCH_STATUS;
  uint32 DDR_CC_DDRCC_PLLCTRL_STATUS;
  uint32 DDR_CC_DDRCC_DLLCTRL_STATUS;
  uint32 DDR_CC_DDRCC_TXPHYCTRL_STATUS;
  uint32 DDR_CC_DDRCC_TXMCCTRL_STATUS;
  uint32 DDR_CC_DDRCC_PLL0_STATUS;
  uint32 DDR_CC_DDRCC_PLL1_STATUS;
  uint32 DDR_CC_DDRCC_TX0_STATUS;
  uint32 DDR_CC_DDRCC_TX1_STATUS;
  uint32 DDR_CC_DDRCC_TXPHY_STATUS;
  uint32 DDR_CC_DDRCC_DLL0_STATUS;
  uint32 DDR_CC_DDRCC_DLL1_STATUS;
  uint32 DDR_CC_DDRCC_SDPLL0_RESET_SM_READY_STATUS;
  uint32 DDR_CC_DDRCC_SDPLL0_CORE_VCO_TUNE_STATUS;
  uint32 DDR_CC_DDRCC_SDPLL0_CORE_KVCO_CODE_STATUS;
  uint32 DDR_CC_DDRCC_SDPLL1_RESET_SM_READY_STATUS;
  uint32 DDR_CC_DDRCC_SDPLL1_CORE_VCO_TUNE_STATUS;
  uint32 DDR_CC_DDRCC_SDPLL1_CORE_KVCO_CODE_STATUS;
}ddr_status_reg;

typedef union
{
  ddr_status_reg status_reg[2];
  uint32 status_reg_arr[MAX_STATUS_REG * 2];
}ddr_status_struct;



/* -----------------------------------------------------------------------
**                           FUNCTION DECLARATIONS
** ----------------------------------------------------------------------- */

void ddr_store_status_regs(reg_status_log pre_post);

#endif /* __DDR_STATUSREG_H__ */
