
#ifndef __DDR_FUNC_TBL_H__
#define __DDR_FUNC_TBL_H__


/**
 * @file ddr_func_tbl.h
 * @brief
 * Header file that contains shared function table structure for SBL and RPM to
 * use common ddr library
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/rpm.bf/2.4/core/boot/ddr/hw/msm8953/ddr_func_tbl.h#2 $
$DateTime: 2015/11/23 02:53:09 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
09/02/15   sc      Initial version.
================================================================================
                      Copyright 2014 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"


/*==============================================================================
                                  STRUCTURE
==============================================================================*/
/* function table for sbl and RPM to share */
typedef struct 
{
  
  /* clock switch api */
  boolean (*pre_ddr_clock_switch) (void *ddr, DDR_CHANNEL channel, uint32 curr_clk_khz,
                                  uint32 new_clk_khz);
  boolean (*post_ddr_clock_switch) (void *ddr, DDR_CHANNEL channel, uint32 curr_clk_khz,
                                   uint32 new_clk_khz);

  /* power collapse api */
  boolean (*enter_ddr_power_collapse) (void *ddr, DDR_CHANNEL channel);
  boolean (*exit_ddr_power_collapse) (void *ddr, DDR_CHANNEL channel);

  /* Self refresh api */
  boolean (*enter_self_refresh) (void *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);
  boolean (*exit_self_refresh) (void *ddr, DDR_CHANNEL channel, DDR_CHIPSELECT chip_select);

  /* functions to access DDR struct members*/
  void*   (*Get_DDR_Struct_Addr)();
  void*   (*Get_CDT_Ptr)();

  /* function to get BIMC/PHY register addresses to save status during pre/post clock swithc and abort*/
  void*   (*Get_DDR_Status_Reg_Addr)();
  
}ddr_func;

#endif
