#ifndef VSENSE_H
#define VSENSE_H


/*=======================================================================
 *! \file vsense.h
 *  \n
 *  \brief This file contains vsense related function prototypes,
 *         enums and driver data structure type.   
 *  \n  
 *  \n &copy;  Copyright (c) 2015 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
 */
/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/25/14   aks     created

========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/


/*----------------------------------------------------------------------------
 * Function : vsense_init
 * -------------------------------------------------------------------------*/
/*!
    Description: Initialize the vsense driver 
    @param
      void
    @return
    void
    
-------------------------------------------------------------------------*/
void vsense_init(void);

/*----------------------------------------------------------------------------
 * Function : vsense_enter_sleep
 * -------------------------------------------------------------------------*/
/*!
    Description: Called while entering into VDD MIN, it should handle VSENSE
                 conigurations during enter/exit of VDD MIN
    @param
      enter - it should be TRUE while entering sleep, FALSE while exit sleep
    @return
    void
    
-------------------------------------------------------------------------*/
void vsense_enter_sleep(bool enter);


#endif 
