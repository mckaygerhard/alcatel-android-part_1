typedef unsigned long uintptr_t;
typedef long intptr_t;
typedef signed char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long int64_t;
typedef long intmax_t;
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long uint64_t;
typedef unsigned long uintmax_t;
typedef int8_t int_fast8_t;
typedef int64_t int_fast64_t;
typedef int8_t int_least8_t;
typedef int16_t int_least16_t;
typedef int32_t int_least32_t;
typedef int64_t int_least64_t;
typedef uint8_t uint_fast8_t;
typedef uint64_t uint_fast64_t;
typedef uint8_t uint_least8_t;
typedef uint16_t uint_least16_t;
typedef uint32_t uint_least32_t;
typedef uint64_t uint_least64_t;
typedef int32_t int_fast16_t;
typedef int32_t int_fast32_t;
typedef uint32_t uint_fast16_t;
typedef uint32_t uint_fast32_t;
typedef unsigned char boolean;
typedef uint32_t bool32;
typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uint8_t uint8;
typedef int32_t int32;
typedef int16_t int16;
typedef int8_t int8;
typedef unsigned long uintnt;
typedef uint8_t byte;
typedef unsigned short word;
typedef unsigned int dword;
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned int uint4;
typedef signed char int1;
typedef signed short int2;
typedef long int int4;
typedef signed int sint31;
typedef signed short sint15;
typedef signed char sint7;
typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32 Word32 ;
typedef int16 Word16 ;
typedef uint8 UWord8 ;
typedef int8 Word8 ;
typedef int32 Vect32 ;
      typedef long long int64;
      typedef unsigned long long uint64;
        typedef struct __attribute__((packed))
        { uint16 x; }
        unaligned_uint16;
        typedef struct __attribute__((packed))
        { uint32 x; }
        unaligned_uint32;
        typedef struct __attribute__((packed))
        { uint64 x; }
        unaligned_uint64;
        typedef struct __attribute__((packed))
        { int16 x; }
        unaligned_int16;
        typedef struct __attribute__((packed))
        { int32 x; }
        unaligned_int32;
        typedef struct __attribute__((packed))
        { int64 x; }
        unaligned_int64;
  extern dword rex_int_lock(void);
  extern dword rex_int_free(void);
    extern dword rex_fiq_lock(void);
    extern void rex_fiq_free(void);
   extern void rex_task_lock( void);
   extern void rex_task_free( void);
typedef enum TzBspBlspPeripheralId TzBspBlspPeripheralId;
enum TzBspBlspPeripheralId
{
   BLSP_QUP_1,
   BLSP_QUP_2,
   BLSP_QUP_3,
   BLSP_QUP_4,
   BLSP_QUP_5,
   BLSP_QUP_6,
   BLSP_QUP_7,
   BLSP_QUP_8,
   BLSP_QUP_9,
   BLSP_QUP_10,
   BLSP_QUP_11,
   BLSP_QUP_12,
   BLSP_QUP_END,
   BLSP_UART_START=15,
   BLSP_UART_1 = BLSP_UART_START,
   BLSP_UART_2,
   BLSP_UART_3,
   BLSP_UART_4,
   BLSP_UART_5,
   BLSP_UART_6,
   BLSP_UART_7,
   BLSP_UART_8,
   BLSP_UART_9,
   BLSP_UART_10,
   BLSP_UART_11,
   BLSP_UART_12,
   BLSP_INVALID_ID = 0xFFFFFFFF,
};
typedef enum TzBspBlspProtocol TzBspBlspProtocol;
enum TzBspBlspProtocol
{
   PROTOCOL_SPI,
   PROTOCOL_I2C,
   PROTOCOL_UART_2_LINE,
   PROTOCOL_UART_4_LINE
};
 int tzbsp_blsp_initaccess(void);
int tzbsp_blsp_modify_ownership(uint32 peripheralId, uint32 owner_ss);
<driver name="NULL">
   <global_def>
      <var_seq name="blsp1_hclk_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp1_ahb_clk </var_seq>
      <var_seq name="blsp2_hclk_name" type=DALPROP_DATA_TYPE_STRING> gcc_blsp2_ahb_clk </var_seq>
   </global_def>
   <device id="/dev/buses/qup/blsp_qup_1">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_QUP_1 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_SPI </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 0, 1, 2, 3, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 15, end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 1 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 9 </props>
   </device>
   <device id="/dev/buses/qup/blsp_qup_2">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_QUP_2 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_I2C </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 6, 7, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 3 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 10 </props>
   </device>
   <device id="/dev/buses/qup/blsp_qup_3">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_QUP_3 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_SPI </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 8, 9, 10, 11, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 11 </props>
   </device>
   <device id="/dev/buses/qup/blsp_qup_4">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_QUP_4 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_I2C </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 14, 15, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 6 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 12 </props>
   </device>
   <device id="/dev/buses/uart/blsp_uart_1">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_UART_1 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_UART_4_LINE </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 0, 1, 2, 3, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 7 </props>
   </device>
   <device id="/dev/buses/uart/blsp_uart_2">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_UART_2 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_UART_2_LINE </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 4, 5, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 3 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 8 </props>
   </device>
   <device id="/dev/buses/qup/blsp_qup_5">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_QUP_5 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_I2C </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 18, 19, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 3 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 9 </props>
   </device>
   <device id="/dev/buses/qup/blsp_qup_6">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_QUP_6 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_SPI </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 20, 21, 22, 23, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 6 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 10 </props>
   </device>
   <device id="/dev/buses/qup/blsp_qup_7">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_QUP_7 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_SPI </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 85, 86, 87, 88, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 11 </props>
   </device>
   <device id="/dev/buses/qup/blsp_qup_8">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_QUP_8 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_SPI </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 96, 97, 98, 99, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 12 </props>
   </device>
   <device id="/dev/buses/uart/blsp_uart_3">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_UART_3 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_UART_4_LINE </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 16, 17, 18, 19, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 7 </props>
   </device>
   <device id="/dev/buses/uart/blsp_uart_4">
      <props name="CHIP_BUS_INDEX" type=DALPROP_ATTR_TYPE_UINT32> BLSP_UART_4 </props>
      <props name="BUS_PROTOCOL" type=DALPROP_ATTR_TYPE_UINT32> PROTOCOL_UART_4_LINE </props>
      <props name="IS_GPIO_PROTECTED" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="GPIO_NUMBERS" type=DALPROP_ATTR_TYPE_BYTE_SEQ> 20, 21, 22, 23, end </props>
      <props name="GPIO_RG_INDEX" type=DALPROP_ATTR_TYPE_BYTE_SEQ> end </props>
      <props name="SUBSYSTEM_ID" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="IS_PERSISTENT" type=DALPROP_ATTR_TYPE_UINT32> 0 </props>
      <props name="CORE_RG_INDEX" type=DALPROP_ATTR_TYPE_UINT32> 8 </props>
   </device>
   <device id="BLSP_GLOBAL_PROP">
      <props name="NUM_PERIPH_BLSP_CORES" type=DALPROP_ATTR_TYPE_UINT32> 2 </props>
      <props name="NUM_BLSP_QUP_CORES" type=DALPROP_ATTR_TYPE_UINT32> 4 </props>
      <props name="NUM_BLSP_UART_CORES" type=DALPROP_ATTR_TYPE_UINT32> 2 </props>
      <props name="BLSP1_BLSP_BAM_BASE" type=DALPROP_ATTR_TYPE_UINT32> 0x07884000 </props>
      <props name="BLSP2_BLSP_BAM_BASE" type=DALPROP_ATTR_TYPE_UINT32> 0x07AC4000 </props>
      <props name="BLSP1_HCLK_NAME" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp1_hclk_name </props>
      <props name="BLSP2_HCLK_NAME" type=DALPROP_ATTR_TYPE_STRING_PTR> blsp2_hclk_name </props>
   </device>
</driver>
