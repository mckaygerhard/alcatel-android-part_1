#ifndef TZBLSPAC_H
#define TZBLSPAC_H

/*===========================================================================
         Copyright (c) 2015 by QUALCOMM Technologies, Incorporated.  
              All Rights Reserved.
            QUALCOMM Confidential & Proprietary
===========================================================================*/

/*===========================================================================

  EDIT HISTORY FOR FILE


  when       who     what, where, why
  --------   ---     ------------------------------------------------------------
  12/18/15   dpk     Removed subsystem ID enums
  09/15/15   dpk     Created
  =============================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>


/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/

typedef enum TzBspBlspProtocol TzBspBlspProtocol;
enum TzBspBlspProtocol
{
   PROTOCOL_SPI,
   PROTOCOL_I2C,
   PROTOCOL_UART_2_LINE,   /* Without HW Flow control*/
   PROTOCOL_UART_4_LINE    /* With HW Flow control */
};
 
/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/
/**
 * Assigns BLSP QUP/UARTs to a particular subsystem at cold boot stage
 *
 * @return E_SUCCESS on success, an error code otherwise.
 */
 int tzbsp_blsp_initaccess(void);

/**
 * System call to change BLSP ownership.
 *
 * @param[in] peripheralId      Blsp Peripheral Id.
 * @param[in] owner_ss          Subsystem Owner ID.
 *
 * @return E_SUCCESS on success, an error code otherwise.
 */
int tzbsp_blsp_modify_ownership(uint32 peripheralId, uint32 owner_ss);

#endif /* tzblspac_h */

