#===============================================================================
#
#                             Edit History
# $Header: //components/rel/core.tz/1.0.5/kernel/smmu/build/v2.sconscript#1 $
#
# when         who     what, where, why
# ----------   ---     ---------------------------------------------------------
# 2011/04/26   spa     Create initial version
#
#===============================================================================
# Copyright (c) 2011 - 2015
# Qualcomm Technologies Incorporated.
# All Rights Reserved.
# Qualcomm Confidential and Proprietary
#===============================================================================
import os 

#-------------------------------------------------------------------------------
# SMMU Lib
#-------------------------------------------------------------------------------
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "../v2"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0) 
#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_APIS = [
   'BUSES',
   'DAL',
   'HAL',
   'SERVICES',
   'SYSTEMDRIVERS',
   'KERNEL',
   'SECUREMSM',
   'TZCHIPSET',
   'MINK'
]

if env['TARGET_FAMILY'] in ['badger']:
   CBSP_APIS.extend(['TZLIBARMV7'])
else:
   CBSP_APIS.extend(['TZLIBARMV8'])

env.RequirePublicApi(CBSP_APIS)
env.RequireRestrictedApi(CBSP_APIS)

#-------------------------------------------------------------------------------
# Internal depends within subunit
#-------------------------------------------------------------------------------
env.PublishPrivateApi('SMMU_V2', [
   '${INC_ROOT}/core/kernel/smmu/v2/src',
   '${INC_ROOT}/core/kernel/smmu/v2/${SMMU_MSM_ID}',
   '${INC_ROOT}/core/kernel/smmu/v2/${SMMU_MSM_ID}/tz',
   '${INC_ROOT}/core/kernel/smmu/v2/${SMMU_MSM_ID}/hyp',
])

#-------------------------------------------------------------------------------
# Compiler options
#-------------------------------------------------------------------------------
#env.Append(CCFLAGS = "--gnu")

#env.Replace(SMMU_UNITTEST = "yes")
if 'SMMU_UNITTEST' in env:
   if env.has_key('TZOS_IMAGE'):
      env.Append(CPPDEFINES = 'SMMU_TZ_UNITTEST')

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

if 'USES_DEVCFG' in env:
   DEVCFG_IMG = ['DAL_DEVCFG_HYP_IMG']
   env.AddDevCfgInfo(DEVCFG_IMG,
   {
      'devcfg_xml' : '${BUILD_ROOT}/core/kernel/smmu/v2/${SMMU_MSM_ID}/SMMUImplDef1Settings.xml'
   })

SMMU_COMMON_SOURCES = [
   '${BUILDPATH}/${SMMU_MSM_ID}/HALSMMU.c',
]

#target specific
SMMU_TARGET_SOURCES = [
   '${BUILDPATH}/${SMMU_MSM_ID}/SMMUConfig.c',
   '${BUILDPATH}/${SMMU_MSM_ID}/SMMUConfigInfo.c',
]

#TZ side
SMMU_TZ_SOURCES = [
   '${BUILDPATH}/${SMMU_MSM_ID}/tz/SMMUInit.c',
   '${BUILDPATH}/${SMMU_MSM_ID}/tz/SMMUTrustZone.c',
   '${BUILDPATH}/${SMMU_MSM_ID}/tz/SMMUFaults.c',
]

#HYP side
SMMU_HYP_SOURCES = [
   '${BUILDPATH}/src/SMMUTranslation.c',
   '${BUILDPATH}/src/SMMU_vm.c',
   '${BUILDPATH}/src/SMMU_vm_secdisp.c',
   '${BUILDPATH}/src/SMMUVirtualization.c',
   '${BUILDPATH}/${SMMU_MSM_ID}/hyp/SMMUInit.c',
   '${BUILDPATH}/${SMMU_MSM_ID}/hyp/SMMUHypervisor.c',
   '${BUILDPATH}/${SMMU_MSM_ID}/hyp/SMMUVirtBuffer.c',
]

#syscall function on HYP side
SMMU_HYP_SYSCALL_SOURCES = [
   '${BUILDPATH}/src/SMMU_ADSP_syscall.c',
   '${BUILDPATH}/${SMMU_MSM_ID}/hyp/SMMU_Dynamic_SID2VMID.c'
]

SMMU_TZ_SYSCALL_SOURCES = [
   '${BUILDPATH}/${SMMU_MSM_ID}/tz/SMMU_TZ_Dynamic_SID2VMID.c'
]

#Unit test
if 'SMMU_UNITTEST' in env:
    SMMU_COMMON_SOURCES += [
        '${BUILDPATH}/${SMMU_MSM_ID}/SMMU_UnitTest_ISR.c',
    ]

HAL_SMMU_PACKOUT_HEADER_FILES = [
   '${BUILDPATH}/src/HALSMMU.h',
   '${BUILDPATH}/src/SMMUInternal.h',
   '${BUILDPATH}/${SMMU_MSM_ID}/hyp/SMMUVirtBuffer.h',
]

HAL_SMMU_PACKOUT_SOURCE_FILES = env.FindFiles(
   '*.c',
   '${INC_ROOT}/core/kernel/smmu/v2/')

env.CleanPack(['TZOS_IMAGE','HYPERVISOR_IMAGE'],
              HAL_SMMU_PACKOUT_HEADER_FILES)

env.CleanPack(['TZOS_IMAGE','HYPERVISOR_IMAGE'],
              HAL_SMMU_PACKOUT_SOURCE_FILES)

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------

env.AddBinaryLibrary(['TZOS_IMAGE'], '${BUILDPATH}/SMMUTZOS',
                     [SMMU_COMMON_SOURCES,SMMU_TARGET_SOURCES,SMMU_TZ_SOURCES])

env.AddBinaryLibrary(['HYPERVISOR_IMAGE','TZBSPTEST_IMAGE'], '${BUILDPATH}/SMMUHYPOS',
                     [SMMU_COMMON_SOURCES,SMMU_TARGET_SOURCES,SMMU_HYP_SOURCES])

#add syscall function to map                     
env.AddBinaryObject(['TZOS_IMAGE'], SMMU_TZ_SYSCALL_SOURCES)
env.AddBinaryObject(['HYPERVISOR_IMAGE'], SMMU_HYP_SYSCALL_SOURCES)
