# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/core/securemsm/trustzone/qsee/mink/oem/config/msm8937/oem_config.xml"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 170 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/core/securemsm/trustzone/qsee/mink/oem/config/msm8937/oem_config.xml" 2
<driver name="NULL">
  <global_def>

  </global_def>
  <device id="/tz/oem">
    <props name="OEM_keystore_enable_rpmb" type=DALPROP_ATTR_TYPE_UINT32>
      1
    </props>
    <props name="OEM_keystore_wrong_passwd_penalty" type=DALPROP_ATTR_TYPE_UINT32>
      2000
    </props>
    <props name="OEM_keystore_retain_wrong_passwd_attempt" type=DALPROP_ATTR_TYPE_UINT32>
      1
    </props>
    <props name="OEM_counter_enable_rpmb" type=DALPROP_ATTR_TYPE_UINT32>
      1
    </props>
    <props name="OEM_allow_rpmb_key_provision" type=DALPROP_ATTR_TYPE_UINT32>
      0
    </props>
    <props name="OEM_sec_wdog_bark_time" type="0x00000002">
      6000
    </props>
    <props name="OEM_sec_wdog_bite_time" type=DALPROP_ATTR_TYPE_UINT32>
      22000
    </props>
    <props name="OEM_reset_reason_list" type=DALPROP_ATTR_TYPE_STRUCT_PTR>
      oem_rst_reason_list
    </props>
    <props name="OEM_l2_wa_enable" type="0x00000002">
    0
    </props>

    <!-- PIL load region information -->
    <props name="OEM_pil_secure_app_load_region_start" type=DALPROP_ATTR_TYPE_UINT32>
      0x85B00000
    </props>
    <props name="OEM_pil_secure_app_load_region_size" type=DALPROP_ATTR_TYPE_UINT32>
      0x800000
    </props>
    <props name="OEM_pil_subsys_load_region_start" type=DALPROP_ATTR_TYPE_UINT32>
      0x80000000
    </props>
    <props name="OEM_pil_subsys_load_region_size" type=DALPROP_ATTR_TYPE_UINT32>
      0x20000000
    </props>

 <!-- OEM MRC FUSE INDEX -->
 <!--
    <props name="apps_root_cert_index" type=DALPROP_ATTR_TYPE_UINT32>
      0x0
    </props>
    <props name="msa_root_cert_index" type=DALPROP_ATTR_TYPE_UINT32>
      0x0
    </props>
 -->
 <!-- END OF OEM MRC FUSE INDEX -->

 <!-- APDP/DEVCFG Roll back fuse data will be updated at this
         offset starting from OEM IMEM base address.
         If given offset is outside OEM IMEM range, fuse version update ignored. -->
    <props name="oem_imem_apdp_devcfg_ver_offset" type=DALPROP_ATTR_TYPE_UINT32>
      0x40
    </props>

    <!-- END OF OEM IMEM OFFSET -->

  </device>
</driver>
