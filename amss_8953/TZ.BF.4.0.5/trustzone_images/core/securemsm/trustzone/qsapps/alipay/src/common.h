/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include "qsee_log.h"

#undef ALOGD
#undef ALOGI
#undef ALOGW
#undef ALOGE
#define MODNAME "SecSvr"

void hexdump2(const char *hdr, const void *addr, uint32_t len);

#ifdef CONSOLE_DEBUG
#define LOGD(fmt, ...) printf(fmt"\n", ##__VA_ARGS__)
#define LOGI(fmt, ...) printf(fmt"\n", ##__VA_ARGS__)
#define LOGW(fmt, ...) printf(fmt"\n", ##__VA_ARGS__)
#define LOGE(fmt, ...) printf(fmt"\n", ##__VA_ARGS__)
#else
#define LOGD(fmt, ...) QSEE_LOG(QSEE_LOG_MSG_DEBUG, fmt, ##__VA_ARGS__)
#define LOGI(fmt, ...) QSEE_LOG(QSEE_LOG_MSG_LOW, fmt, ##__VA_ARGS__)
#define LOGW(fmt, ...) QSEE_LOG(QSEE_LOG_MSG_ERROR, fmt, ##__VA_ARGS__)
#define LOGE(fmt, ...) QSEE_LOG(QSEE_LOG_MSG_FATAL, fmt, ##__VA_ARGS__)
#endif
#define ALOGD(fmt, ...) LOGD(fmt, ##__VA_ARGS__)
#define ALOGI(fmt, ...) LOGI(fmt, ##__VA_ARGS__)
#define ALOGW(fmt, ...) LOGW(fmt, ##__VA_ARGS__)
#define ALOGE(fmt, ...) LOGE(fmt, ##__VA_ARGS__)




#endif
