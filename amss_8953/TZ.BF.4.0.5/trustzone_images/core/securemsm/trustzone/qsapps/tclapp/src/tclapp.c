/* Copyright (C) 2016 Tcl Corporation Limited */
/*
@file app_main.c
@brief App main entry point.

*/
/*===========================================================================
   Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE
  $Header: //source/qcom/qct/core/pkg/trustzone/rel/2.0/trustzone_images/core/securemsm/trustzone/qsapps/sampleapp/src/app_main.c#19 $
  $DateTime: 2014/09/11 16:15:52 $
  $Author: pwbldsvc $
cat /d/tzdbg/qsee_log

# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
  08/4/14   pdosi      Added missing fs api tests
===========================================================================*/
#include <stdarg.h>
#include <stdio.h>
#include "qsee_sfs.h"
#include "qsee_spi.h"
#include "qsee_log.h"
/* MODIFIED-BEGIN by Xiaoyun.Wei, 2016-11-14,BUG-3335803*/
#include "qsee_core.h"
#include "common.h"


typedef struct
{
  int cmd;
  int len;
}__attribute__((packed)) qseecom_cmd;

#define QBLEN 256
static uint8_t qbuf[QBLEN];
/* MODIFIED-END by Xiaoyun.Wei,BUG-3335803*/

void tz_app_init(void)
{

}

void tz_app_cmd_handler(void* cmd, uint32_t cmdlen,
                        void* rsp, uint32_t rsplen)
{
  /* MODIFIED-BEGIN by Xiaoyun.Wei, 2016-11-14,BUG-3335803*/
  qseecom_cmd *qsci = (qseecom_cmd *)cmd;
  qseecom_cmd *qsco = (qseecom_cmd *)rsp;
  int ret;

  QSEE_LOG(QSEE_LOG_MSG_DEBUG, "cmd=%d, ilen=%d, olen=%d, in=0x%x, out=0x%x",qsci->cmd, cmdlen, rsplen, cmd, rsp);
  if((NULL == cmd) || (NULL == rsp) || !cmdlen || !rsplen)
  {
    QSEE_LOG(QSEE_LOG_MSG_ERROR, "in=%x, in_len=%d, out=%x, out_len=%d", cmd, cmdlen, rsp, rsplen);
    return;
  }

  switch(qsci->cmd)
  {
  case 6:
  {
    ret = qsee_get_secure_state((qsee_secctrl_secure_status_t *)qbuf);
    LOGD("qsee_get_secure_state ret=%d", ret);
    LOGD("dat1=0x%x, dat2=0x%x", *((uint32_t *) qbuf), *((uint32_t *) (qbuf + 4)));
    qsco->len=12;
    *((uint32_t *)(qsco+1))=*((uint32_t *) qbuf);
    *(((uint32_t *)(qsco+1))+1)=*((uint32_t *) (qbuf + 4));
    *(((int *)(qsco+1))+2)=qsee_read_serial_num();
    break;
  }
  default:
    LOGE("Not support cmd=%d", qsci->cmd);
    break;
  }
  /* MODIFIED-END by Xiaoyun.Wei,BUG-3335803*/
}


void tz_app_shutdown(void)
{

}

