/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef __GX_PRODUCT_HEAD_DEF____
#define __GX_PRODUCT_HEAD_DEF____
typedef enum {
    GFX16M = 0,
    GF318M,
    MILANF,
    MILANE,
    MILANG,
    MILANL,
    MILANFN,
    MILANK,
    GF_PRODUCT_COUNTS,
}GF_Product_t;

typedef enum {
    FS_PLAIN = 0,
    FS_ENC_SW,
    FS_FTS,
    FS_SFS,
    FS_OEM,
}GF_FS_Type;

#endif
