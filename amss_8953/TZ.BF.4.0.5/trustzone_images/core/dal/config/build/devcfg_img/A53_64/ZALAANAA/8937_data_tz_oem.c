#include "DALSysTypes.h" 

extern const DALSYSPropStructTblType DALPROP_StructPtrs_8937_xml_tz[];

extern const uint32 DALPROP_PropBin_8937_xml_tz[];

extern const StringDevice driver_list_8937_xml_tz[];


const DALProps DALPROP_PropsInfo_8937_xml_tz = {(const byte*)DALPROP_PropBin_8937_xml_tz, DALPROP_StructPtrs_8937_xml_tz, 31, driver_list_8937_xml_tz};
