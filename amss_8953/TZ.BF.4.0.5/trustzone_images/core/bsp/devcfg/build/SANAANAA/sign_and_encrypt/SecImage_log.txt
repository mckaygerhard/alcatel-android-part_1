Logging to /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/core/bsp/devcfg/build/SANAANAA/sign_and_encrypt/SecImage_log.txt
Config path is set to: /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/tools/build/scons/sectools/config/integration/secimage.xml
WARNING: OEM ID is set to 0 for sign_id "devcfg"
Output dir is set to: /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/core/bsp/devcfg/build/SANAANAA/sign_and_encrypt
Openssl v1.0.1 or greater is available at: "/usr/bin/openssl"
------------------------------------------------------
Processing 1/1: /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/build/ms/bin/SANAANAA/unsigned/devcfg.mbn

l1_file_name = /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/tools/build/scons/sectools/resources/data_prov_assets/Encryption/Unified/default/l1_key.bin
l2_file_name = /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/tools/build/scons/sectools/resources/data_prov_assets/Encryption/Unified/default/l2_key.bin
l3_file_name = /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/tools/build/scons/sectools/resources/data_prov_assets/Encryption/Unified/default/l3_key.bin
Signing image: /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/build/ms/bin/SANAANAA/unsigned/devcfg.mbn
attestation_certificate_extensions = /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/tools/build/scons/sectools/resources/data_prov_assets/General_Assets/Signing/openssl/v3_attest.ext
ca_certificate_extensions = /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/tools/build/scons/sectools/resources/data_prov_assets/General_Assets/Signing/openssl/v3.ext
openssl_configfile = /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/tools/build/scons/sectools/resources/data_prov_assets/General_Assets/Signing/openssl/opensslroot.cfg
Generating new Root certificate and a random key
Generating new Attestation CA certificate and a random key
Generating new Attestation certificate and a random key

Attestation Certificate Properties:
| SW_ID     | 0x0000000000000005  |
| HW_ID     | 0x0000000000000000  |
| DEBUG     | 0x0000000000000002  |
| OEM_ID    | 0x0000              |
| SW_SIZE   | 168                 |
| MODEL_ID  | 0x0000              |
| SHA256    | True                |
| APP_ID    | None                |
| CRASH_DUMP| None                |
| ROT_EN    | None                |
| Exponent  | 3                   |
| TCG_MIN   | None                |
| TCG_MAX   | None                |
| FID_MIN   | None                |
| FID_MAX   | None                |

Signed & Encrypted image is stored at /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/core/bsp/devcfg/build/SANAANAA/sign_and_encrypt/default/devcfg/devcfg.mbn
Image /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/core/bsp/devcfg/build/SANAANAA/sign_and_encrypt/default/devcfg/devcfg.mbn signature is valid
Image /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/core/bsp/devcfg/build/SANAANAA/sign_and_encrypt/default/devcfg/devcfg.mbn is encrypted

Base Properties: 
| Integrity Check                 | True  |
| Signed                          | True  |
| Encrypted                       | True  |
| Size of signature               | 256   |
| Size of one cert                | 2048  |
| Num of certs in cert chain      | 3     |
| Number of root certs            | 1     |
| Hash Page Segments as segments  | False |
| Cert chain size                 | 6144  |

ELF Properties: 
Elf Header: 
| Magic                      | ELF                           |
| Class                      | ELF64                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | 183                            |
| Version                    | 0x1                            |
| Entry address              | 0x866e6000                     |
| Program headers offset     | 0x00000040                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x00000000                     |
| ELF header size            | 64                             |
| Program headers size       | 56                             |
| Number of program headers  | 2                              |
| Section headers size       | 64                             |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Elf Program Headers: 
| S.No | Type | Offset | VirtAddr | PhysAddr | FileSize | MemSize | Flags | Align |
|------|------|--------|----------|----------|----------|---------|-------|-------|
|  1   | LOAD | 0x3000 |0x866e6000|0x866e6000|  0x5250  |  0x5250 |  0x6  | 0x8   |
|  2   | LOAD | 0x8250 |0x866f5000|0x866f5000|  0x00c4  |  0x00c4 |  0x6  | 0x8   |

Hash Segment Properties: 
| Header Size  | 40B  |

Header: 
| cert_chain_ptr  | 0xffffffff  |
| cert_chain_size | 0x00001800  |
| code_size       | 0x00000080  |
| data_is_none    | 0x00000000  |
| flash_parti_ver | 0x00000003  |
| image_dest_ptr  | 0xffffffff  |
| image_id        | 0x00000004  |
| image_size      | 0x00001980  |
| image_src       | 0x00000000  |
| sig_ptr         | 0xffffffff  |
| sig_size        | 0x00000100  |

SecElf Properties: 
| image_type        | 0     |
| max_elf_segments  | 100   |
| testsig_serialnum | None  |

------------------------------------------------------

SUMMARY:
Following actions were performed: "sign, encrypt, validate"
Output is saved at: /local/mnt/workspace/CRMBuilds/TZ.BF.4.0.5-00026-M8937AAAAANAZT-1_20160902_131205/b/HY11_1/trustzone_images/core/bsp/devcfg/build/SANAANAA/sign_and_encrypt

| Idx | SignId | Parse | Integrity | Sign | Encrypt |              Validate              |
|     |        |       |           |      |         | Parse | Integrity | Sign | Encrypt |
|-----|--------|-------|-----------|------|---------|-------|-----------|------|---------|
|  1. | devcfg |   T   |     NA    |  T   |    T    |   T   |     T     |  T   |    T    |

