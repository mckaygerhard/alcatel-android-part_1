typedef unsigned int uint32;
typedef unsigned short uint16;
typedef unsigned char uint8;
typedef signed int int32;
typedef signed short int16;
typedef signed char int8;
typedef unsigned long long uint64;
typedef long long int64;
typedef unsigned char byte;
typedef unsigned long UINTN;
typedef uint32 DALBOOL;
typedef uint32 DALDEVICEID;
typedef uint32 DalPowerCmd;
typedef uint32 DalPowerDomain;
typedef uint32 DalSysReq;
typedef UINTN DALHandle;
typedef int DALResult;
typedef void * DALEnvHandle;
typedef void * DALSYSEventHandle;
typedef uint32 DALMemAddr;
typedef UINTN DALSYSMemAddr;
typedef uint32 DALInterfaceVersion;
typedef unsigned char * DALDDIParamPtr;
typedef struct DALEventObject DALEventObject;
struct DALEventObject
{
    uint32 obj[8];
};
typedef DALEventObject * DALEventHandle;
typedef struct _DALMemObject
{
   uint32 memAttributes;
   uint32 sysObjInfo[2];
   uint32 dwLen;
   uint32 ownerVirtAddr;
   uint32 virtAddr;
   uint32 physAddr;
}
DALMemObject;
typedef struct _DALDDIMemBufDesc
{
   uint32 dwOffset;
   uint32 dwLen;
   uint32 dwUser;
}
DALDDIMemBufDesc;
typedef struct _DALDDIMemDescList
{
   uint32 dwFlags;
   uint32 dwNumBufs;
   DALDDIMemBufDesc bufList[1];
}
DALDDIMemDescList;
typedef struct DALSysMemDescBuf DALSysMemDescBuf;
struct DALSysMemDescBuf
{
   DALSYSMemAddr VirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   uint32 size;
   uint32 user;
};
typedef struct DALSysMemDescList DALSysMemDescList;
struct DALSysMemDescList
{
   uint32 dwObjInfo; uint32 hOwnerProc; DALSYSMemAddr thisVirtualAddr;
   DALSYSMemAddr PhysicalAddr;
   DALSYSMemAddr VirtualAddr;
   uint32 dwCurBufIdx;
   uint32 dwNumDescBufs;
   DALSysMemDescBuf BufInfo[1];
};
typedef struct {
   uint32 nGpioNumber;
   uint32 nFunctionSelect;
}TLMMGpioIdType;
<driver name="NULL">
  <device id="/tlmm/configs">
    <props name="blsp_spi_mosi[1]" type="TLMMGpioIdType">{0, 1}</props>
    <props name="blsp_spi_miso[1]" type="TLMMGpioIdType">{1, 1}</props>
    <props name="blsp_spi_cs_n[1]" type="TLMMGpioIdType">{2, 1}</props>
    <props name="blsp_spi_clk[1]" type="TLMMGpioIdType">{3, 1}</props>
    <props name="blsp_i2c_sda[1]" type="TLMMGpioIdType">{2, 3}</props>
    <props name="blsp_i2c_scl[1]" type="TLMMGpioIdType">{3, 3}</props>
    <props name="blsp_spi_mosi[2]" type="TLMMGpioIdType">{4, 1}</props>
    <props name="blsp_spi_miso[2]" type="TLMMGpioIdType">{5, 1}</props>
    <props name="blsp_spi_cs_n[2]" type="TLMMGpioIdType">{6, 1}</props>
    <props name="blsp_spi_clk[2]" type="TLMMGpioIdType">{7, 1}</props>
    <props name="blsp_i2c_sda[2]" type="TLMMGpioIdType">{6, 3}</props>
    <props name="blsp_i2c_scl[2]" type="TLMMGpioIdType">{7, 3}</props>
    <props name="blsp_spi_mosi[3]" type="TLMMGpioIdType">{8, 1}</props>
    <props name="blsp_spi_miso[3]" type="TLMMGpioIdType">{9, 1}</props>
    <props name="blsp_spi_cs_n[3]" type="TLMMGpioIdType">{10, 1}</props>
    <props name="blsp_spi_clk[3]" type="TLMMGpioIdType">{11, 1}</props>
    <props name="blsp_i2c_sda[3]" type="TLMMGpioIdType">{10, 3}</props>
    <props name="blsp_i2c_scl[3]" type="TLMMGpioIdType">{11, 3}</props>
    <props name="blsp_spi_mosi[4]" type="TLMMGpioIdType">{12, 1}</props>
    <props name="blsp_spi_miso[4]" type="TLMMGpioIdType">{13, 1}</props>
    <props name="blsp_spi_cs_n[4]" type="TLMMGpioIdType">{14, 1}</props>
    <props name="blsp_spi_clk[4]" type="TLMMGpioIdType">{15, 1}</props>
    <props name="blsp_i2c_sda[4]" type="TLMMGpioIdType">{14, 3}</props>
    <props name="blsp_i2c_scl[4]" type="TLMMGpioIdType">{15, 3}</props>
    <props name="blsp_spi_mosi[5]" type="TLMMGpioIdType">{16, 1}</props>
    <props name="blsp_spi_miso[5]" type="TLMMGpioIdType">{17, 1}</props>
    <props name="blsp_spi_cs_n[5]" type="TLMMGpioIdType">{18, 1}</props>
    <props name="blsp_spi_clk[5]" type="TLMMGpioIdType">{19, 1}</props>
    <props name="blsp_i2c_sda[5]" type="TLMMGpioIdType">{18, 3}</props>
    <props name="blsp_i2c_scl[5]" type="TLMMGpioIdType">{19, 3}</props>
    <props name="blsp_spi_mosi[6]" type="TLMMGpioIdType">{20, 1}</props>
    <props name="blsp_spi_miso[6]" type="TLMMGpioIdType">{21, 1}</props>
    <props name="blsp_spi_cs_n[6]" type="TLMMGpioIdType">{22, 1}</props>
    <props name="blsp_spi_clk[6]" type="TLMMGpioIdType">{23, 1}</props>
    <props name="blsp_i2c_sda[6]" type="TLMMGpioIdType">{22, 3}</props>
    <props name="blsp_i2c_scl[6]" type="TLMMGpioIdType">{23, 3}</props>
    <props name="blsp6_spi_cs1_n" type="TLMMGpioIdType">{47, 1}</props>
    <props name="blsp_spi_mosi[7]" type="TLMMGpioIdType">{85, 2}</props>
    <props name="blsp_spi_miso[7]" type="TLMMGpioIdType">{86, 2}</props>
    <props name="blsp_spi_cs_n[7]" type="TLMMGpioIdType">{87, 2}</props>
    <props name="blsp_spi_clk[7]" type="TLMMGpioIdType">{88, 2}</props>
    <props name="blsp_i2c_sda[7]" type="TLMMGpioIdType">{87, 4}</props>
    <props name="blsp_i2c_scl[7]" type="TLMMGpioIdType">{88, 4}</props>
    <props name="blsp_spi_mosi[8]" type="TLMMGpioIdType">{96, 1}</props>
    <props name="blsp_spi_miso[8]" type="TLMMGpioIdType">{97, 1}</props>
    <props name="blsp_spi_cs_n[8]" type="TLMMGpioIdType">{98, 1}</props>
    <props name="blsp_spi_clk[8]" type="TLMMGpioIdType">{99, 1}</props>
    <props name="blsp_i2c_sda[8]" type="TLMMGpioIdType">{98, 3}</props>
    <props name="blsp_i2c_scl[8]" type="TLMMGpioIdType">{99, 3}</props>
    <props name="blsp8_spi_cs1_n" type="TLMMGpioIdType">{130, 1}</props>
    <props name="tlmm_base" type=DALPROP_ATTR_TYPE_UINT32>
      0x01000000
    </props>
    <props name="tlmm_offset" type=DALPROP_ATTR_TYPE_UINT32>
      0x00000000
    </props>
    <props name="tlmm_total_gpio" type=DALPROP_ATTR_TYPE_UINT32>
      134
    </props>
  </device>
</driver>
