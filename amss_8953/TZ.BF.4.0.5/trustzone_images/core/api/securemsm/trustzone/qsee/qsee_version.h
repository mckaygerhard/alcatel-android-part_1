#ifndef QSEE_VERSION_H
#define QSEE_VERSION_H

/*============================================================================
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
=========================================================================== */

/*=========================================================================
                              Edit History

  $Header: //components/rel/core.tz/1.0.5/api/securemsm/trustzone/qsee/qsee_version.h#1 $
  $DateTime: 2016/03/24 12:08:26 $
  $Author: pwbldsvc $


when       who     what, where, why
--------   ---     --------------------------------------------------------
08/04/11    rv     Initial Revision

=========================================================================== */

#define QSEE_VERSION_MAJOR 02
#define QSEE_VERSION_MINOR 01

#endif /*QSEE_VERSION_H*/

