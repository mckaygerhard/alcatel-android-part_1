LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := Monster_TmsService
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

LOCAL_MULTILIB := 64

LOCAL_PREBUILT_JNI_LIBS := libs/arm64-v8a/libams-1.1.9-mfr.so \
                       libs/arm64-v8a/libams-1.1.9-m-mfr.so \
                       libs/arm64-v8a/libbuffalo-1.0.0-mfr.so \
                       libs/arm64-v8a/libbumblebee-1.0.4-mfr.so \
                       libs/arm64-v8a/libdce-1.1.14-mfr.so \
                       libs/arm64-v8a/liboptimus-1.0.0-mfr.so \
                       libs/arm64-v8a/libQQImageCompare-1.2-mfr.so \
                       libs/arm64-v8a/libTmsdk-2.0.9-mfr.so


include $(BUILD_PREBUILT)

