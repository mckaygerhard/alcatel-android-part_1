LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := MingPian_CC
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS := lib/armeabi/libbcr6.so \
        lib/armeabi/libContactMerger.so \
        lib/armeabi/libcoorTrans.so \
        lib/armeabi/libencryptfile.so \
        lib/armeabi/libFocusArea.so \
        lib/armeabi/libissocket_android.so \
        lib/armeabi/liblocSDK6a.so \
        lib/armeabi/libQREngine.so \
        lib/armeabi/libscanner6.so

include $(BUILD_PREBUILT)
