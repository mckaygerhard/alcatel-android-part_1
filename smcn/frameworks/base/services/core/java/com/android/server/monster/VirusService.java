/* Copyright (C) 2016 Tcl Corporation Limited */
package com.android.server.monster;

import java.util.HashMap;

import android.app.monster.IVirusManager;
import android.app.monster.MulwareProviderHelp.MulwareTable;
import android.os.Handler;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;

import android.app.monster.MulwareProviderHelp;

/**
 *TCL monster add 2016-8-20
 * @author luolaigang
 *
 */
public class VirusService extends IVirusManager.Stub {
	private static final String TAG = "VirusManagerService";
	private Context mContext;
	private HashMap<String, String> allDisabledIpsAndUrlsWithPackageName = new HashMap<>();
	boolean isInit = false;
	private StringBuffer outBuffer = new StringBuffer();

	public VirusService(Context context) {
		mContext = context;
		VirusChangeContent content = new VirusChangeContent(new Handler());
		// 注册病毒库变化监听
		mContext.getContentResolver().registerContentObserver(
				MulwareTable.CONTENT_URI, true, content);
	}

	public void systemReady() {
		mContext.sendBroadcast(new Intent(
				MulwareProviderHelp.ACTION_SERVICE_START));
		new Thread() {
			public void run() {
				initList();
			};
		}.start();
	}

	private void initList() {
		// 查询病毒库
		Cursor cursor = mContext.getContentResolver()
				.query(MulwareTable.CONTENT_URI,
						new String[] {MulwareTable.AD_BANIPS, MulwareTable.AD_BANURLS, MulwareTable.AD_PACKAGENAME, },
						MulwareTable.AD_PROHIBIT + "=?", new String[] { "1" },
						null);
		allDisabledIpsAndUrlsWithPackageName.clear();
		outBuffer.delete(0, outBuffer.length());
		if (cursor != null && cursor.getCount() > 0) {
			for (int i = 0; i < cursor.getCount(); i++) {
				if (cursor.moveToPosition(i)) {
					String ips = cursor.getString(0);
					String urls = cursor.getString(1);
					allDisabledIpsAndUrlsWithPackageName.put(cursor.getString(2), ips+"_"+urls);
				}
			}
		}

		if (cursor != null) {
			cursor.close();
		}
	}

	public String getValue(String packageName) {
		return allDisabledIpsAndUrlsWithPackageName.get(packageName);
	}

	class VirusChangeContent extends ContentObserver {
		public VirusChangeContent(Handler handler) {
			super(handler);
		}

		@Override
		public void onChange(boolean selfChange) {
			super.onChange(selfChange);
			initList();
		}
	}
}
