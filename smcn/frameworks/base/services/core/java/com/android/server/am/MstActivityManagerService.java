/* Copyright (C) 2016 Tcl Corporation Limited */
//TCL monster add by liuqin for permission 2016-11-11 start
package com.android.server.am;

import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.os.SystemProperties;
import android.util.Log;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.UserHandle;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.RemoteException;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ProviderInfo;
import android.app.AppGlobals;
import android.view.View;
import android.view.WindowManager;
import android.content.pm.PermissionInfo;
import android.app.KeyguardManager;
import android.R;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.text.TextUtils;
import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.FrameLayout;
import android.view.View.OnClickListener;
import com.android.server.pm.MstPermission;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import android.util.Slog;
import android.util.MstCheckPermission;
import com.android.server.ServiceThread;
import com.android.server.Watchdog;
import android.app.IMstActivityManager;


/**
 *  {@hide}
 */
public class MstActivityManagerService extends IMstActivityManager.Stub{

    private static final boolean DEBUG_PERM = true;

    private static final int GN_SHOW_PERM_ERROR = 200;
    private static final int GN_PERM_COUNT_DOWN = 201;
    private static final int GN_PERM_UPDATEDB = 202;
    public static final String GN_PERM_TAG = "perm_ct.l";
    private static final int GN_MAX_WAIT_TIME = 10;
	private static final long WATCHDOG_TIMEOUT = 1000*60*12;

    private Map<String, Integer> mPermCtrlResultMap = new HashMap<String, Integer>();
    private MstPermDialogEntry mPermDialogEntry;
    private List<Message> mPermMsgList = new ArrayList<Message>();
    private boolean mIsPermDialogShowing = false;
    private Handler mPermCtrlHandler;
    private KeyguardManager mKeyguardManager;
    static final int PEMISSION_CONTROL_DENIDE = -1 ;
    static final int PEMISSION_CONTROL_ASK = 0 ;
    static final int PEMISSION_CONTROL_GRANTED = 1 ;
    static final int PEMISSION_CONTROL_SLEEP_TIME = 250 ;
    static final int PEMISSION_CONTROL_SLEEP_COUNTS = 41 ;
    private boolean mRememberMe = true ;
    private int mPermissionsStatus = 0 ;
    private boolean mStartSetting = false ;
    private Context mContext;
    private ActivityManagerService mService = null;
	final ServiceThread mHandlerThread;
	private List<String> mPackageSetting = new ArrayList<String>();

    public MstActivityManagerService(Context context, ActivityManagerService service){
		if (DEBUG_PERM) Slog.w(GN_PERM_TAG, "New MstActivityManagerService");

		mHandlerThread = new ServiceThread(GN_PERM_TAG, Process.THREAD_PRIORITY_FOREGROUND, true);
		mHandlerThread.start();
        mPermCtrlHandler = new MstPermCtrlHandler(mHandlerThread.getLooper());

        mContext = context;
        mService = service;
		MstPermission.getInstance().initPermissionProp();
		//Watchdog.getInstance().addThread(mPermCtrlHandler, WATCHDOG_TIMEOUT);
    }

    void mstReadSystemAppInfo(Context context) {
        MstPermission.getInstance().readSystemAppInfo(context);
    }

    void mstRegistCloseSysDlg() {
        IntentFilter closeSysDlg = new IntentFilter();
        closeSysDlg.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
               mstCleanAllPermMessages();
            }
        }, closeSysDlg);
    }

    public int mstCheckPermmison(String permissionName, int callingUid){
       return mstCheckComponentPermission(permissionName, callingUid, -1, true, mService.mBooted);
    }

    int mstCheckComponentPermission(String permission, int uid,
            int owningUid, boolean exported, boolean booted) {
        // Root, system server get to do everything.
        if (uid == 0 || uid == Process.SYSTEM_UID) {
            return PackageManager.PERMISSION_GRANTED;
        }

        // Isolated processes don't get any permissions.
        if (UserHandle.isIsolated(uid)) {
            return PackageManager.PERMISSION_DENIED;
        }

        // If there is a uid that owns whatever is being accessed, it has
        // blanket access to it regardless of the permissions it requires.
        if (owningUid >= 0 && UserHandle.isSameApp(uid, owningUid)) {
            return PackageManager.PERMISSION_GRANTED;
        }

        // If the target is not exported, then nobody else can get to it.
        if (!exported) {
            if (DEBUG_PERM) Slog.w(GN_PERM_TAG, "Permission denied: checkComponentPermission() owningUid=" + owningUid);
            return PackageManager.PERMISSION_DENIED;
        }
        if (permission == null) {
            return PackageManager.PERMISSION_GRANTED;
        }
        permission = mstTransPermission(permission);

        if (!MstCheckPermission.containGrpPerm(mContext, permission)
                || MstCheckPermission.mSepcialPackage.contains(mstGetNameForUid(uid)) ) {
        try {
                //if (DEBUG_PERM) Slog.w(GN_PERM_TAG, "Check perm in pms " + permission);
                return AppGlobals.getPackageManager().checkUidPermission(mstRealPermission(permission), uid);
            } catch (RemoteException e) {
                Slog.e(GN_PERM_TAG, "PackageManager is dead?!?", e);
            }
        }

        int checkPermission = PackageManager.PERMISSION_DENIED;
        try {
            checkPermission = AppGlobals.getPackageManager().checkUidPermission(mstRealPermission(permission), uid);
        } catch (RemoteException e) {
            // Should never happen, but if it does... deny!
            Slog.e(GN_PERM_TAG, "PackageManager is dead?!?", e);
            return PackageManager.PERMISSION_DENIED;
        }

        return mstCheckComponentPermission(permission, uid, booted, checkPermission);
    }

    public int mstCheckComponentPermission(String permission, int uid) {
    	return mstCheckComponentPermission(permission, uid, mService.mBooted, PackageManager.PERMISSION_GRANTED);
    }

    public int mstCheckComponentPermissionWithoutAlert(String permission, int uid) {
    	if (uid == 0 || uid == Process.SYSTEM_UID) {
            return PackageManager.PERMISSION_GRANTED;
        }

    	String packageName = mstGetNameForUid(uid);
    	if (MstPermission.getInstance().isSystemApp(packageName)) {
    		return PackageManager.PERMISSION_GRANTED;
    	}
    	int status = PackageManager.PERMISSION_GRANTED;
    	if(mService.mBooted && !mstIsScreenLocked()) {
    		status = mstGetPermStatus(packageName, permission);
    		if(status == 0 || status == 1) {
    			status = PackageManager.PERMISSION_GRANTED;
    		} else {
    			status = PackageManager.PERMISSION_DENIED;
    		}
    	}

    	return status;
    }

    private int mstCheckComponentPermission(String permission, int uid, boolean booted, int checkPermission) {
        if (uid == 0 || uid == Process.SYSTEM_UID) {
            return PackageManager.PERMISSION_GRANTED;
        }

//    	if (uid >= Process.FIRST_APPLICATION_UID) {
			String packageName = mstGetNameForUid(uid);
			if (MstPermission.getInstance().isSystemApp(packageName)) {
			    return checkPermission;
			}
//		}

        if (PackageManager.PERMISSION_DENIED == checkPermission) {
            return checkPermission;
        }

        if (bPackagePermDisableInSetting(uid) && mstPermCtrlDenied(permission, uid)) {
            return PackageManager.PERMISSION_DENIED;
        }

        mstInterceptPermission(permission, uid, PackageManager.PERMISSION_GRANTED, booted);

        String pkgName = mstGetNameForUid(uid);
        if (booted && !mstIsScreenLocked() && mstPermCtrlDenied(permission, uid)) {
            if (DEBUG_PERM) Log.i(GN_PERM_TAG, "pkgName:" + pkgName + ", permission:" + permission);
            if (mstGetPermStatus(pkgName, permission) == PEMISSION_CONTROL_ASK) {
                boolean foundFlag = false;
                int checkResult = PEMISSION_CONTROL_ASK;
                for (int i = 0; i < PEMISSION_CONTROL_SLEEP_COUNTS; i++) {
                    if(mStartSetting){
                        mStartSetting = false ;
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "mStartSetting true");
                        break ;
                    }

                    if(mPermissionsStatus == PEMISSION_CONTROL_DENIDE ){
                        mPermissionsStatus = PEMISSION_CONTROL_ASK ;
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "PEMISSION_CONTROL_DENIDE");
                        break ;
                    }
                    if(mPermissionsStatus == PEMISSION_CONTROL_GRANTED ){
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "PEMISSION_CONTROL_GRANTED");
                        mPermissionsStatus = PEMISSION_CONTROL_ASK ;
                        foundFlag = true;
                        break;
                    }
                    if (mstIsScreenLocked()){
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "ScreenLocked");
                        break;
                    }

                    if (mstGetPermStatus(pkgName, permission) == PEMISSION_CONTROL_DENIDE) {
                        mPermissionsStatus = PEMISSION_CONTROL_ASK ;
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "mstGetPermStatus denied");
                        break ;
                    }

                    SystemClock.sleep(PEMISSION_CONTROL_SLEEP_TIME);
                }

                if (DEBUG_PERM)  Log.i(GN_PERM_TAG, "---->checkResult " + checkResult + ", foundFlag=" + foundFlag);

                if (!foundFlag){
                    handleDialogTimeout(pkgName, permission);
                    if (DEBUG_PERM) Log.i(GN_PERM_TAG, "return PERMISSION_DENIED");
                    return PackageManager.PERMISSION_DENIED;
                }
           } else if (mstGetPermStatus(pkgName, permission) == PEMISSION_CONTROL_DENIDE) {
                if (DEBUG_PERM) Log.i(GN_PERM_TAG, "---->getPermStatus(pkgName, permission) == PEMISSION_CONTROL_DENIDE ");
                try {
                    int times = MstPermission.getInstance().mstGetPermDeniedTime(permission, pkgName);
                    if (times < 1) {
                        mstSendNotificationBroacast(pkgName, permission,false);
                    }
                    MstPermission.getInstance().mstSetPermDeniedTime(permission, pkgName, times + 1);

                } catch (Exception e) {
                    Log.i(GN_PERM_TAG, "exception:", e);
                }
                handleDialogTimeout(pkgName, permission);
                return PackageManager.PERMISSION_DENIED;
            }
        }

        if (MstPermission.getInstance().mstIsContainPermDenied(permission, pkgName)){
            MstPermission.getInstance().mstSetPermDeniedTime(permission, pkgName, 0);
        }
        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "Crant pkgName:" + pkgName + ", permission:" + permission);
        return PackageManager.PERMISSION_GRANTED;
    }

    private void handleDialogTimeout(String pkgName, String permission) {
    	String key = MstPermission.getInstance().mstGetPermKey(permission, pkgName, null);
        int count = 0;
        if(null != mPermCtrlResultMap && mPermCtrlResultMap.containsKey(key)){
            count  = mPermCtrlResultMap.get(key);
        }
        Slog.i(GN_PERM_TAG, "isDialogTimeout key = " + key + ", count = " + count);
        if (count >  0 && count < GN_MAX_WAIT_TIME) {
            if (mPermDialogEntry != null && mPermDialogEntry.dialog != null && mPermDialogEntry.dialog.isShowing()) {
                mstClearPermMessages(pkgName);
                mPermCtrlHandler.removeMessages(GN_PERM_COUNT_DOWN);
                mPermDialogEntry.dialog.dismiss();
                mIsPermDialogShowing = false;
            }
        }

        mPermissionsStatus = PEMISSION_CONTROL_ASK;
    }

    int mstInterceptPermission(String permission, int uid, int returnValueWhenDenied, boolean booted) {
        if(mstPermCtrlDenied(permission, uid)) {
            // If the system is not booted or the screen is locked,
            // we do not show the permisison dialog.
            if (!booted || mstIsScreenLocked()) {
            } else {
                String pkgName = mstGetNameForUid(uid);
                String permGrp = mstGetPkgPermGrp(pkgName, permission);
                String key = MstPermission.getInstance().mstGetPermKey(permission, pkgName, permGrp);

                if (mstGetPermStatus(pkgName, permission) == PEMISSION_CONTROL_DENIDE) {
                    return returnValueWhenDenied;
                }
                synchronized (mPermMsgList) {
                    if (!TextUtils.isEmpty(pkgName) && !mstIsPermMsgExisted(key)) {
                        // While there is a permission group check of a valid app, which is neither existed
                        // in the array nor dealing with, we new a check message and countdown.
                        Log.d(GN_PERM_TAG, "Waiting new control result of permission " + key);
                        mPermCtrlResultMap.put(key, GN_MAX_WAIT_TIME);

                        Message msg = mPermCtrlHandler.obtainMessage(GN_SHOW_PERM_ERROR);
                        msg.arg1 = uid;
                        String[] arg = new String[] {key, pkgName, permGrp, permission};
                        msg.obj =  arg;
                        mPermMsgList.add(msg);

                        if (!mIsPermDialogShowing) {
                            if (DEBUG_PERM) Log.d(GN_PERM_TAG, "No permission dialog is showing!");
                            try {
                                mstPopPermMessage();
                            } catch (IllegalStateException e) {
                                if (DEBUG_PERM) Log.e(GN_PERM_TAG,"mstPopPermMessage error ", e);
                            }
                        }
                    }
                }
            }
            return returnValueWhenDenied;
        }
        return PERMISSION_GRANTED;
    }

   private boolean mstPermCtrlDenied(String permName, int uid) {
        if (uid <= 1000 || TextUtils.isEmpty(permName)) {
            return false;
        }
        boolean isDenied = false;
        if (uid >= Process.FIRST_APPLICATION_UID) {
            String packageName = mstGetNameForUid(uid);
            isDenied = MstPermission.getInstance().mstCheckPermDenied(permName,packageName);
        }

        return isDenied;
    }

    public void mstUpdatePermStatus(Context context, String pkgName, String permission, int status) {
        Message updateDb = mPermCtrlHandler.obtainMessage(GN_PERM_UPDATEDB);
        updateDb.arg1 = status;
        String[] arg = new String[] {pkgName, permission};
        updateDb.obj =  arg;
        mPermCtrlHandler.sendMessage(updateDb);
    }

    private void mstUpdatePermDB(final String pkgName, final String permission, final int status) {

        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "update and start Activity!");
        MstPermission.getInstance().mstUpdatePermStatus(permission, pkgName, status);

        mPermCtrlHandler.post(new Runnable() {
            @Override
            public void run() {
            	MstPermission.getInstance().mstUpdatePermissionStatusToDb(permission, pkgName, status);
            }
        });
    }

    private void mstStartSettingActivity(Context context,String packageName) {

        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "start Setting Activity!");

        Intent intent = new Intent();

        intent.setAction("com.gionee.permission");
        intent.addCategory("com.gionee.permission.category");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try{
            PackageInfo pi = context.getPackageManager().getPackageInfo(packageName, 0);
            String appName = pi.applicationInfo.loadLabel(context.getPackageManager()).toString();
            intent.putExtra("packagename", packageName);
            intent.putExtra("title", appName);
            context.startActivity(intent);
			mPackageSetting.add(packageName);
        }catch (Exception e) {
            Log.d(GN_PERM_TAG, "getPackageInfo Error");
        }
    }

    private String mstGetAppNameLabel(Context context,String pkgName) {
        String label = "";
        PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo  appInfo = pm.getApplicationInfo(pkgName, 0);
            String temp = (String)pm.getApplicationLabel(appInfo);
            label = ( TextUtils.isEmpty(temp) ) ? appInfo.processName : temp;
        } catch (NameNotFoundException e) {
            Log.d(GN_PERM_TAG, "Invalid group name:" + pkgName);
        }
        return label;
    }

    public String mstGetPkgPermGrp(String pkgName, String permName) {
        return MstPermission.getInstance().mstGetPkgPermGrp(pkgName, permName);
    }

    private class MstPermDialogEntry {
        String key;
        int uid;
        String pkgName;
        AlertDialog dialog;
        String perm;

        public MstPermDialogEntry(String key, int uid, String pkgName, String perm, AlertDialog dialog) {
            this.key = key;
            this.uid = uid;
            this.pkgName = pkgName;
            this.dialog = dialog;
            this.perm = perm;
        }
    }

    private boolean mstIsScreenLocked() {
        if (mKeyguardManager == null) {
            mKeyguardManager = (KeyguardManager)
            mContext.getSystemService(Context.KEYGUARD_SERVICE);
        }
        boolean isScreenLocked = mKeyguardManager == null || mKeyguardManager.isKeyguardLocked();
        return isScreenLocked;
    }

    private boolean mstIsPermMsgExisted(String key) {
        boolean existed = false;
        if (mPermDialogEntry != null &&
                mIsPermDialogShowing && mPermDialogEntry.key.equals(key)) {
            if (DEBUG_PERM) Log.d(GN_PERM_TAG, "The permission dialog of " + key + " is already showing");
            existed = true;
        }
        synchronized (mPermMsgList) {
            for (Message msg : mPermMsgList) {
                if (((String[]) msg.obj)[0].equals(key)) {
                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "The permission message of " + key + " is already existed");
                    existed = true;
                }
            }
        }
        return existed;
    }

    private void mstPopPermMessage() {
        mPermCtrlHandler.removeMessages(GN_PERM_COUNT_DOWN);
        if (mPermMsgList.size() == 0) {
            if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Permission messages array is empty!");
            mIsPermDialogShowing = false;
            return;
        }
        Message msg = mPermMsgList.remove(0);
        if (!mPermCtrlResultMap.containsKey(((String[]) msg.obj)[0])) {
            if (DEBUG_PERM) Log.d(GN_PERM_TAG,  ((String[]) msg.obj)[0] + "'s permission message is already overdue!");
            mIsPermDialogShowing = false;
            mstPopPermMessage();
            return;
        }
        mIsPermDialogShowing = true;
        mPermissionsStatus = PEMISSION_CONTROL_ASK ;
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Pop message " + ((String[]) msg.obj)[0]);
        mPermCtrlHandler.sendMessage(msg);
    }

    private void mstClearPermMessages(String pkgName) {
        Set<String> trashSet = new HashSet<String>();
        for (String key : mPermCtrlResultMap.keySet()) {
            if (key.contains(pkgName)) {
                if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Find trash " + key);
                trashSet.add(key);
            }
        }
        synchronized (mPermMsgList) {
            for (int i = mPermMsgList.size() - 1; i >= 0; i--) {
                String key = ((String[])mPermMsgList.get(i).obj)[0];
                if (trashSet.contains(key)) {
                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Remove trash " + key + " from mPermMsgList!");
                    mPermMsgList.remove(i);
                }
            }

            for (String key : trashSet) {
                if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Remove trash " + key + " from maps!");
                mPermCtrlResultMap.remove(key);
            }
        }
        trashSet.clear();
    }

    private void mstCleanAllPermMessages() {
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Clean all permission messages!");

        if (mPermDialogEntry != null && mPermDialogEntry.dialog.isShowing()) {
            mPermCtrlHandler.removeMessages(GN_PERM_COUNT_DOWN);
            if (mPermCtrlResultMap.get(mPermDialogEntry.key) != null
                    && mPermCtrlResultMap.get(mPermDialogEntry.key) >= 0
                    && mPermDialogEntry.pkgName != null
                    && mPermDialogEntry.perm != null) {
                if(!mRememberMe){
            	    mstUpdatePermStatus(mContext, mPermDialogEntry.pkgName, mPermDialogEntry.perm, PEMISSION_CONTROL_ASK);
                } else {
            	    mstUpdatePermStatus(mContext, mPermDialogEntry.pkgName, mPermDialogEntry.perm, PEMISSION_CONTROL_DENIDE);
                }//if(!mRememberMe
            }//if (mPermCtrlResultMap.get(mPermDialogEntry.key) != null

            mPermCtrlResultMap.put(mPermDialogEntry.key, -1);
            mPermDialogEntry.dialog.dismiss();
        }//if (mPermDialogEntry != null

        mIsPermDialogShowing = false;
        mPermMsgList.clear();
        mPermCtrlResultMap.clear();

        mPackageSetting.clear();
    }

    private class MstPermCtrlHandler extends Handler {

        MstPermCtrlHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {

            switch (msg.what) {

                case GN_SHOW_PERM_ERROR: {
                    final String key = ((String[]) msg.obj)[0];
                    final String pkgName = ((String[]) msg.obj)[1];
                    final String permGrp = ((String[]) msg.obj)[2];
                    final String permission = ((String[]) msg.obj)[3];
                    final int uid = msg.arg1;

                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "permGrp = "+permGrp + " uid = "+uid + " pkgName = "+pkgName);

                    AlertDialog permDialog = new AlertDialog.Builder(mContext)
//                            .setIcon(com.mst.R.drawable.ic_alarm)
                            .create();
                    permDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                    permDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                    Resources res= mContext.getResources();

                    String message = mstGetAppNameLabel(mContext, pkgName);
                    permDialog.setTitle(message);
//                    String message = res.getString(gionee.R.string.zzzzz_gn_perm_meg, mstGetAppNameLabel(mContext, pkgName));
//                    message = "正在尝试访问 " + mstGetPkgPermLabel(permission) + " 权限";
                    message = res.getString(com.android.internal.R.string.grant_permissions_header_text) +":"+ mstGetPkgPermLabel(permission);
                    String messageMore = "";
//                    String messageMore = res.getString(gionee.R.string.zzzzz_gn_perm_more);
//					message += messageMore;
                    View layout = mstBuildPermissionView(message, messageMore, permission);
	    			permDialog.setCanceledOnTouchOutside(false);
                    permDialog.setView(layout);

                    DialogInterface.OnClickListener dialogClickLsn = new DialogInterface.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (DEBUG_PERM)Log.d(GN_PERM_TAG, "which clicked = " + which);
                            switch (which) {
                                case AlertDialog.BUTTON_POSITIVE:
                                    mPermCtrlResultMap.remove(key);
                                    mPermissionsStatus = PEMISSION_CONTROL_GRANTED ;
                        	        if(!mRememberMe){
                        	            mstUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
                        	        } else {
                	        	        mstUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_GRANTED);
                	                }
                                    mstPopPermMessage();
                                    break;
                                case AlertDialog.BUTTON_NEGATIVE:
                                    mStartSetting = true ;
                                    mstClearPermMessages(pkgName);
                                    mstStartSettingActivity(mContext,pkgName);
                                    //mService.forceStopPackage(pkgName, -1);
                                    mstPopPermMessage();
                                    break;
                                case AlertDialog.BUTTON_NEUTRAL:
                               	    mPermCtrlResultMap.remove(key);
                            	    mPermissionsStatus = PEMISSION_CONTROL_DENIDE ;
                                    if(!mRememberMe){
                	                    mRememberMe = true ;
                	                    mstUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
                                        } else {
                	                    mstUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_DENIDE);
                	                }
                                    mstPopPermMessage();
                                    break;
                                default:
                                    break;
                            }//switch
                        }//onClick
                    };//OnClickListener

                    permDialog.setButton(AlertDialog.BUTTON_POSITIVE,
                            res.getString(com.android.internal.R.string.sms_control_yes),
                            dialogClickLsn);

                    permDialog.setButton(AlertDialog.BUTTON_NEUTRAL,
//                            String.format(res.getString(gionee.R.string.zzzzz_gn_perm_cancel), GN_MAX_WAIT_TIME),
                            res.getString(com.android.internal.R.string.sms_control_no) + "("+GN_MAX_WAIT_TIME+")",
                            dialogClickLsn);

//                    permDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
//                            res.getString(R.string.cancel),
//                            dialogClickLsn);

                    permDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (DEBUG_PERM) Log.d(GN_PERM_TAG, "The showing dialog dimiss!");

                            if (mPermCtrlResultMap != null && key != null && mPermCtrlResultMap.get(key) != null && mPermCtrlResultMap.get(key) >= 0) {
                                if (DEBUG_PERM) Log.d(GN_PERM_TAG, "We remove the dialog from the map!");

                                if(!mRememberMe){
            	                    mRememberMe = true ;
            	                    mstUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
            	                } else {
            	                    mstUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_DENIDE);
            	                }
                                mPermissionsStatus = PEMISSION_CONTROL_DENIDE ;
                                mstClearPermMessages(pkgName);
                                mstPopPermMessage();
                            } // (mPermCtrlResultMap != null
                        }// onDismiss
                    });//OnDismissListener

                    mPermDialogEntry = new MstPermDialogEntry(key, uid, pkgName, permission, permDialog);
                    mPermDialogEntry.dialog.show();
                    if (mPermDialogEntry.dialog.getButton(AlertDialog.BUTTON_NEGATIVE) != null) {
                        mPermDialogEntry.dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(0xffff9000);
                    }
                    Message countDownMsg = mPermCtrlHandler.obtainMessage(GN_PERM_COUNT_DOWN);
                    countDownMsg.obj = key;
                    mPermCtrlHandler.sendMessageDelayed(countDownMsg, 1000);
                    break;
                } // case GN_SHOW_PERM_ERROR

                case GN_PERM_COUNT_DOWN:{
                    String key = (String) msg.obj;
                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Count down " + key);

                    if (mstIsScreenLocked()) {
                        mstCleanAllPermMessages();
                    } else {
                        // we need to check if the perm_map has the key ,if not, get key will return null,conn't convert to int type.
                        if(null != mPermCtrlResultMap && mPermCtrlResultMap.containsKey(key)){
                            int count = mPermCtrlResultMap.get(key);
                            count --;
                            mPermCtrlResultMap.put(key, count);
                            mPermDialogEntry.dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setText(
//                                    String.format(mContext.getResources().getString(gionee.R.string.zzzzz_gn_perm_cancel), count)
		                            mContext.getResources().getString(com.android.internal.R.string.sms_control_no) + "("+count+")"
                            		);

                            if (count > 0) {
                                if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Count down!");
                                msg = mPermCtrlHandler.obtainMessage(GN_PERM_COUNT_DOWN);
                                msg.obj = key;
                                mPermCtrlHandler.sendMessageDelayed(msg, 1000);
                            } else if (count == 0) {
                                if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Count down end!");
                                mPermDialogEntry.dialog.getButton(AlertDialog.BUTTON_NEUTRAL).performClick();
                            }
                        }//if(null != mPermCtrlResultMap
                    }
                    break;
                } // case GN_PERM_COUNT_DOWN

                case GN_PERM_UPDATEDB:{
                    final String pkgName = ((String[]) msg.obj)[0];
                    final String permission = ((String[]) msg.obj)[1];
                    final int status = msg.arg1;
                    mstUpdatePermDB(pkgName, permission, status);
	            break;
                } //  case GN_PERM_UPDATEDB
            }//switch
        }//handleMessage
    }; //MstPermCtrlHandler

    private int mstGetPermStatus(String pkgName, String permission) {
        return MstPermission.getInstance().mstGetPermStatus(permission, pkgName);
    }

    private void mstSendNotificationBroacast(String pkgName, String permission,boolean important) {
        Intent intent = new Intent();
        intent.setAction("Permissio.Action.Denied");
        intent.putExtra("pkgName", pkgName);
        intent.putExtra("permission", permission);
        intent.putExtra("important", important);
        mContext.sendBroadcast(intent);
    }

    private String mstGetPkgPermLabel(final String permission) {
    	if (permission.contains("INSTALL_SHORTCUT")) {
//    		return mContext.getResources().getString();
//    		return "新建快捷方式";
    		return "install shortcut";
    	}
    	if (permission.contains("SYSTEM_ALERT_WINDOW")) {
//    		return "悬浮窗";
    		return "overlay window";
    	}
    	if (permission.contains("PACKAGE_USAGE_STATS")) {
//    		return "使用记录访问";
    		return "history access";
    	}
    	if (permission.contains("POST_NOTIFICATION")) {
//    		return "通知使用权";
    		return "notifcation";
    	}
    	if (permission.contains("PREMIUM_SMS")) {
//    		return "付费短信";
    		return "sms";
    	}
    	if (permission.contains("ACCESS_VR_MANAGER")) {
//    		return "VR助手服务";
    		return "VR service";
    	}
    	if (permission.contains("BIND_DEVICE_ADMIN")) {
//    		return "设备管理器";
    		return "device manager";
    	}

        String tempGrpName = permission;
    	PermissionGroupInfo groupInfo = null;
        PackageManager mPm = mContext.getPackageManager();
        if (null != mPm && null != tempGrpName) {
        	try {
        		String groupName = MstPermission.getInstance().mstGetPermGroupName(permission);
        		groupInfo = mPm.getPermissionGroupInfo(groupName, 0);
        	} catch (NameNotFoundException e) {
        		Log.d(GN_PERM_TAG, "Invalid group name:" + tempGrpName);
        	}
        	if (null != groupInfo) {
        		tempGrpName = groupInfo.loadLabel(mPm).toString();
        	}
        }
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "getPkgPermGrupLabel return :" + tempGrpName);
        return tempGrpName;
    }

    private void mstResetNoRememberPermission(String pkgName,String permission,int status) {
    	switch(status) {
            case PEMISSION_CONTROL_GRANTED :
	        if(!mRememberMe){
                    mRememberMe = true ;
                    mstUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
                }
                break;
            case PEMISSION_CONTROL_DENIDE :
               if(!mRememberMe){
                   mRememberMe = true ;
                   mstUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
               }
               break;
            default :
                break ;
    	}
    }

    private String mstTransPermission(String permission) {
        if(permission == null){
            return null;
        }
        String result = permission;
        if (result.contains("CONTACTS_CALLS")) {
            result = result.replace("_CONTACTS_CALLS", "_CALL_LOG");
        }
        return result;
    }

    private String mstRealPermission(String permission) {
        if(permission == null){
            return null;
        }
        String result = permission;
        if (result.contains("_SMS_MMS")) {
            result = result.replace("_SMS_MMS", "_SMS");
        }
        return result;
    }

    private View mstBuildPermissionView(String message, String messageMore, String permission) {
    	LinearLayout layout = new LinearLayout(mContext);
        layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT));
        layout.setOrientation(LinearLayout.VERTICAL);
        TextView textView = new TextView(mContext);
        textView.setText(message);
        textView.setTextColor(0xcc000000);
        textView.setTextSize(18);
        int margin = mstDip2px(mContext, 20);
        int dividerMargin = mstDip2px(mContext, 16);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(margin, dividerMargin, margin, 0);
        layout.addView(textView,layoutParams);

        final CheckBox checkBox = new CheckBox(mContext);
        checkBox.setTextSize(12);
        if (MstCheckPermission.mSpecialPerHs.contains(permission)) {
            mRememberMe = false;
            checkBox.setChecked(false);
        } else {
            mRememberMe = true;
            checkBox.setChecked(true);
        }
        mRememberMe = false;
        checkBox.setChecked(false);
//        checkBox.setText("记住我的选择");
        checkBox.setText(com.android.internal.R.string.save_password_remember);
        checkBox.setTextColor(0x66000000);
        checkBox.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    mRememberMe = true ;
                } else {
                    mRememberMe = false ;
                }
            }
        });

        View dividerView = new View(mContext);

        LinearLayout.LayoutParams dividerParams = new LinearLayout.LayoutParams(
        FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        dividerParams.setMargins(dividerMargin, dividerMargin, dividerMargin, dividerMargin);
        dividerView.setBackgroundColor(0XFFBBBBBB);
        //layout.addView(dividerView,dividerParams);
        layout.addView(checkBox,dividerParams);
        return layout;
    }

    private int mstDip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    boolean mstCheckWritePermission(ProviderInfo cpi) {
        return cpi.writePermission != null
                        && (cpi.writePermission.contains("WRITE_SMS")
                                    || cpi.writePermission.contains("WRITE_CONTACTS")
                                    || cpi.writePermission.contains("WRITE_CALL_LOG"));
    }

    boolean mstCheckReadPermission(ProviderInfo cpi) {
        return cpi.readPermission != null
                      && (cpi.readPermission.contains("READ_SMS")
                                  || cpi.readPermission.contains("READ_CONTACTS")
                                  || cpi.readPermission.contains("READ_CALL_LOG"));
    }

    public String mstGetNameForUid(int uid) {
        String pkgList[] = mContext.getPackageManager().getPackagesForUid(uid);
        if(null == pkgList || 0 == pkgList.length) {
            return null;
        }
        return pkgList[0];
    }

    public void setPackagePermEnable(String pk){
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "setPackagePermEnable pk: " + pk);
        mPackageSetting.remove(pk);
    }

    private boolean bPackagePermDisableInSetting(int uid){
        String pk = mstGetNameForUid(uid);
        boolean bFound = false;
        for (int i = 0; i < mPackageSetting.size(); i++) {
            if (pk.equals(mPackageSetting.get(i))) {
                bFound = true;
                break;
            }
        }
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "bPackagePermEnable pk: " + pk + " bFound " + bFound);
        return bFound;
    }
}
//TCL monster add by liuqin for permission 2016-11-11 end
