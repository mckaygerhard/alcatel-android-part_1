/* Copyright (C) 2016 Tcl Corporation Limited */
//TCL monster add by liuqin for permission 2016-11-11 start
package com.android.server.pm;

import java.io.BufferedReader;
import android.text.TextUtils;
import android.database.ContentObserver;
import android.database.Cursor;
import android.content.ContentResolver;
import java.util.HashMap;
import android.net.Uri;
import android.os.SystemProperties;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import android.content.Context;
import android.content.pm.PackageParser;
import android.provider.Settings.Secure;
import android.os.Environment;
import android.util.Log;
import android.content.ContentValues;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import java.util.Set;
import android.content.pm.PermissionInfo;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;
import android.util.AtomicFile;

import com.android.internal.telephony.ISms;
import com.android.internal.telephony.SmsUsageMonitor;
import com.android.internal.util.FastXmlSerializer;
import libcore.io.IoUtils;
import android.content.pm.PackageManager;
import static org.xmlpull.v1.XmlPullParser.END_DOCUMENT;
import static org.xmlpull.v1.XmlPullParser.START_TAG;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import android.util.Xml;
import android.util.MstCheckPermission;
import android.database.SQLException;

public class MstPermission {
    private static final boolean DEBUG_PERM = true;
    static final String TAG = "perm_ct.Ap";
    static final Uri GN_TRUST_URI = Uri.parse("content://com.monster.settings.PermissionProvider/whitelist");
    private HashMap<String, Integer> mPkgNmaeMap = new HashMap<String, Integer>();
    private static String PKG_TAG = "pkg";
    private static String VERSION_TAG = "version";
    private static int IS_SYSTEMAPP = 1;
    private AtomicFile mPakageNameFile;
    private String mRomVersion;
    private static MstPermission mInstance = null;

    private Context mContext;
    private Handler mHandler;
    private PermObserver mPermObserver;
    private PackageManager mPm;

    static final Uri GN_PERM_URI = Uri.parse("content://com.monster.settings.PermissionProvider/permissions");
    private static final String ACTION_UPDATE_PERM_MAP = "com.gionee.action.UPDATE_PERM_DB";
    private HashMap<String, Integer> mPermsMap = new HashMap<String, Integer>();

    private HashMap<String, Integer> mPermsDeniedTimeMap = new HashMap<String, Integer>();

    static final int UPDATE_PERM_MAP_AFTER_PKG_ADDED = 17;
	static final int POST_INSTALL = 9;
	static final int UPDATE_GNPERMISSIN_DB = 20;
	static final int DELETE_GNPERMISSIN_DB = 21;
    private BroadcastReceiver mIntentReceiver = null;

    private static final String DATA_APP_PATH="data/app";
    private static final String SYSTEM_APP_PATH="system/app";
	private static final String PRIV_APP_PATH="system/priv-app";
    private static final String OPERATOR_APP_PATH="system/vendor/operator/app";
    private List<String> mPakagenameList = new ArrayList<String>();
    public static final String ALL_TRUST = "1";
    public static final String PARTIAL_TRUST = "0";
    public static final String NONE_TRUST = "-1";
	public static final String DEFULT_LEVEL = NONE_TRUST;

    private ISms mSmsManager;

    public static MstPermission getInstance() {
        if (mInstance != null) {
            return mInstance;
        }

        mInstance = new MstPermission();

        return mInstance;
    }

    public void init(Context context, Handler handler) {
        mContext = context;
        mHandler = handler;
        initSystemManager(context);
    }

    public static void destory() {
        mInstance.mPermObserver.unObserve();
        mInstance.mPermsMap.clear();
        mInstance.mContext.unregisterReceiver(mInstance.mIntentReceiver);
    }

    public boolean mstCheckPermDenied(String permName, String pkgName) {
        boolean denied = false;
        boolean isFound = false;
        initInteral();
        if(permName.equals(MstCheckPermission.PREMIUM_SMS)) {
        	denied = getSmsState(pkgName) != SmsUsageMonitor.PREMIUM_SMS_PERMISSION_ALWAYS_ALLOW ;
        } else if(!mPermsMap.isEmpty()){
        	String temp = mstGetPermKey(permName, pkgName);
        	synchronized (mPermsMap) {
        		if(mPermsMap.containsKey(temp)){
        			denied = (mPermsMap.get(temp) != 1);
        			isFound = true;
        		}
        	}
        }

        return denied;
    }

    public int mstGetPermStatus(String permName, String pkgName) {
        int status = 1;
        boolean isFound = false;
        initInteral();
        if(permName.equals(MstCheckPermission.PREMIUM_SMS)) {
        	status = getSmsState(pkgName);
        	if(status == SmsUsageMonitor.PREMIUM_SMS_PERMISSION_ALWAYS_ALLOW) {
        		status = 1;
        	} else if(status == SmsUsageMonitor.PREMIUM_SMS_PERMISSION_ASK_USER) {
        		status = 0;
        	} else {
        		status = -1;
        	}
        } else if(!mPermsMap.isEmpty()){
        	String temp = mstGetPermKey(permName, pkgName);
        	synchronized (mPermsMap) {
        		if(mPermsMap.containsKey(temp)){
        			status = mPermsMap.get(temp);
        			isFound = true;
        		}
        	}
        }

        return status;
    }

    public void mstUpdatePermStatus(String permName, String pkgName, int status) {
        if(mPermsMap.isEmpty() || null == permName || null == pkgName){
            return;
        }
        String temp = mstGetPermKey(permName, pkgName);
        synchronized (mPermsMap) {
            if(mPermsMap.containsKey(temp)){
                mPermsMap.put(temp, status);
            }
        }
    }

    public void updatePermStatusEx(String permName, String pkgName, int status) {
        if(null == permName || null == pkgName){
            return;
        }
        String temp = mstGetPermKey(permName, pkgName);
        synchronized (mPermsMap) {
        	mPermsMap.put(temp, status);
        }
    }

    public int mstGetPermDeniedTime(String permName, String pkgName) {
        int times = 0;
        if(mPermsDeniedTimeMap.isEmpty() || null == permName || null == pkgName){
            return times;
        }
        String temp = mstGetPermKey(permName, pkgName);
        synchronized (mPermsDeniedTimeMap) {
            if(mPermsDeniedTimeMap.containsKey(temp)){
                times = mPermsDeniedTimeMap.get(temp);
            }
        }
        return times;
    }

    public void mstSetPermDeniedTime(String permName, String pkgName, int times) {
        if (null == permName || null == pkgName) {
            return;
        }
        String temp = mstGetPermKey(permName, pkgName);
        synchronized (mPermsDeniedTimeMap) {
            mPermsDeniedTimeMap.put(temp, times);
        }
    }

    public boolean mstIsContainPermDenied(String permName, String pkgName) {
        if (null == permName || null == pkgName) {
            return false;
        }
        String temp = mstGetPermKey(permName, pkgName);
        return mPermsDeniedTimeMap.containsKey(temp);
    }

	public String mstGetPkgPermGrp(String pkgName, String permName){
		return mstGetPermGroupName(permName);
    }

    public void doHandleMessageCheckPermission(Message msg) {
        switch(msg.what) {
            case  UPDATE_PERM_MAP_AFTER_PKG_ADDED: {
                mPermsMap.clear();
                gnInitPermissionsMap();
                break;
            }

		    case  UPDATE_GNPERMISSIN_DB: {
		        if(mPkgNmaeMap.containsKey((String) msg.obj)){
		        	return;
		        }

		        initPermissionProp();
				updateGnPermissinDB((String) msg.obj, true);
				insertTrustDB((String) msg.obj);
				break;
		    }

            case DELETE_GNPERMISSIN_DB: {
                try {
			 	    String packageName = (String) msg.obj;
                    Log.i(TAG, "remove PermDb for " + packageName);
                    mContext.getContentResolver().delete(MstPermission.GN_PERM_URI,
                        " packagename = ?", new String[] { packageName });
                    mContext.getContentResolver().delete(MstPermission.GN_TRUST_URI,
                    " packagename = ?", new String[]{packageName});
                } catch (Exception unused) {
                    Log.e(TAG, "remove PermDb failed ");
                }
			 	break;
            }
        }
    }

    private MstPermission() {
    }

    private void initInteral() {
        if (mIntentReceiver == null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(ACTION_UPDATE_PERM_MAP);

            mIntentReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if(action.equals(ACTION_UPDATE_PERM_MAP)){
                        Log.d("perm_ctrl", "pms mIntentReceiver");
                        mHandler.sendEmptyMessage(UPDATE_PERM_MAP_AFTER_PKG_ADDED);
                    }
                }
            };

            mContext.registerReceiver(mIntentReceiver, filter);
            mPermObserver = new PermObserver(mHandler);
            mPermObserver.observe();
            gnInitPermissionsMap();
        }
    }

    private void gnInitPermissionsMap() {
        String[] projection = new String[]{"permission","packagename","status","permissiongroup"};
		try {
            Cursor cursor = mContext.getContentResolver().query(GN_PERM_URI, projection, null, null, null);
            Log.d("perm_ctrl", "pms gnInitPermissionsMap!");
            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                        String keys = cursor.getString(3)+"-"+ cursor.getString(1);
                        int value = cursor.getInt(2);
						String permGrup = cursor.getString(3);
						synchronized (mPermsMap) {
                          mPermsMap.put(keys, value);
						}
                        cursor.moveToNext();
                }
                cursor.close();
            }
        } catch (Exception e) {
        }
    }

    class PermObserver extends ContentObserver{

        PermObserver(Handler handler) {
            super(handler);
        }

        public void observe() {
            ContentResolver resolver = mContext.getContentResolver();
            resolver.registerContentObserver(GN_PERM_URI,false,this);
            Log.d("perm_ctrl", "pms observe DB");
        }

        @Override
        public void onChange(boolean selfChange) {
            synchronized (mPermsMap) {
                mPermsMap.clear();
            }

            gnInitPermissionsMap();
        }

        public void unObserve() {
            ContentResolver resolver = mContext.getContentResolver();
            resolver.unregisterContentObserver(this);
        }
    }

    private void updateGnPermissinDB(String packageName, boolean isInsert) {
        List<PermissionInfo> mPermsList = new ArrayList<PermissionInfo>();
        Set<PermissionInfo> permSet = new HashSet<PermissionInfo>();

        PackageInfo pkgInfo;
        try {
            pkgInfo = mContext.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
        } catch (NameNotFoundException e) {
            return;
        }
        // Extract all user permissions
        if ((pkgInfo.applicationInfo != null) && (pkgInfo.applicationInfo.uid != -1)) {
            getAllUsedPermissions(pkgInfo.applicationInfo.uid, permSet);
        }
        for (PermissionInfo tmpInfo : permSet) {
            mPermsList.add(tmpInfo);
        }

       PermissionInfo pmInfo = new PermissionInfo();
       pmInfo.name = MstCheckPermission.POST_NOTIFICATION;
       mPermsList.add(pmInfo);

       if(getSmsState(packageName) != SmsUsageMonitor.PREMIUM_SMS_PERMISSION_UNKNOWN) {
    	   pmInfo = new PermissionInfo();
    	   pmInfo.name = MstCheckPermission.PREMIUM_SMS;
            mPermsList.add(pmInfo);
       }

        int status = ALL_TRUST.equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level"))?1:0 ;
        ContentResolver cr = mContext.getContentResolver();
        for (PermissionInfo pInfo : mPermsList) {
            if (!MstCheckPermission.containGrpPerm(mContext, pInfo.name)) {
                continue;
            }

            int level = status;
            if (MstCheckPermission.containImportantGrpPerm(mContext, pInfo.name)
            		|| MstCheckPermission.containSeniorGrpPerm(mContext, pInfo.name) ) {
                level = -1;
                if (MstCheckPermission.PREMIUM_SMS.equals(pInfo.name)) {
                	setSmsState(packageName, SmsUsageMonitor.PREMIUM_SMS_PERMISSION_NEVER_ALLOW);
                }
            }

            try {
                String grpName = mstGetPermGroupName(pInfo.name);
                if (!isInsertedPermStatus(packageName, grpName)) {
                    ContentValues cv = new ContentValues();
                    cv.put("packagename", packageName);
//                    cv.put("permission", pInfo.name);
                    cv.put("permissiongroup", grpName);
                    cv.put("status", level);
                    cr.insert(GN_PERM_URI, cv);
                    Log.i(TAG, "---->add permission " + pInfo.name + " to GnPermissinDB for " + packageName);
                    if(isInsert) {
                    	updatePermStatusEx(pInfo.name, packageName, level);
                    }
                }
            } catch (SQLException unused) {
        	    Log.e(TAG, "Insert PermDb failed ");
            } catch (IllegalArgumentException argExp) {
                Log.e(TAG, "Insert PermDb failed argExp ");
            } catch (Exception e){
            	e.printStackTrace();
            }
        }
//        Intent intent = new Intent();
//        intent.setAction("com.gionee.action.UPDATE_PERM_DB");
//        mContext.sendBroadcast(intent);
    }

    private void insertTrustDB(String packageName) {
    	try {
    		if(getInstalledAppTrustCount(packageName) > 0) {
    			//deleteTrustByPkgName(packageName);
    		} else {
    			int status = ALL_TRUST.equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level"))?1:0 ;
    			ContentResolver cr = mContext.getContentResolver();
    			ContentValues cv = new ContentValues();
    			cv.put("packagename", packageName);
    			cv.put("status", status);
    			cr.insert(GN_TRUST_URI, cv);
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }

    private int getInstalledAppTrustCount(String packageName){
        int count = 0;
        Cursor cursor = mContext.getContentResolver().query(GN_TRUST_URI, new String[]{"status"},
                " packagename = ?", new String[]{packageName}, null);
        if(cursor != null){
            count = cursor.getCount();
            cursor.close();
        }
        return count;
    }
    private void deleteTrustByPkgName(String packageName){
    	mContext.getContentResolver().delete(GN_TRUST_URI, " packagename = ?", new String[]{packageName});
    }

    private void getAllUsedPermissions(int sharedUid, Set<PermissionInfo> permSet) {
        String[] sharedPkgList = mContext.getPackageManager().getPackagesForUid(sharedUid);
        if (sharedPkgList == null || (sharedPkgList.length == 0)) {
            return;
        }
        for (String sharedPkg : sharedPkgList) {
            getPermissionsForPackage(sharedPkg, permSet);
        }
    }

    private void getPermissionsForPackage(String packageName, Set<PermissionInfo> permSet) {
        PackageInfo pkgInfo;
        try {
            pkgInfo = mContext.getPackageManager()
                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
        } catch (NameNotFoundException e) {
            Log.v(TAG, "Could'nt retrieve permissions for package:" + packageName);
            return;
        }
        if ((pkgInfo != null) && (pkgInfo.requestedPermissions != null)) {
            extractPerms(pkgInfo.requestedPermissions, permSet);
        }
    }

    private void extractPerms(String[] strList, Set<PermissionInfo> permSet) {
        if ((strList == null) || (strList.length == 0)) {
            return;
        }
        for (String permName : strList) {
            try {
                PermissionInfo tmpPermInfo = mContext.getPackageManager().getPermissionInfo(permName, 0);
                if (tmpPermInfo != null) {
                    permSet.add(tmpPermInfo);
                }
            } catch (NameNotFoundException e) {
            }
        }
    }

    private boolean isInsertedPermStatus(String pkgName, String permission) {
        boolean status = false;
        Cursor c = null;
        try {
            c = mContext.getContentResolver().query(GN_PERM_URI, new String[] {"status"},
                    "  packagename = ? and permissiongroup =? ", new String[] {pkgName, permission}, null);
            if (c != null && c.getCount() > 0) {
                status = true;
            }
        } finally {
            if (c != null && !c.isClosed()) {
                c.close();
                c = null;
            }
        }
        return status;
    }
    public void readSystemAppInfo(Context context) {
    	initSystemManager(context);
    	mPakageNameFile = new AtomicFile(new File("/data/system/", "packagename.xml"));
    	Handler mHandler = new Handler();
    	mHandler.post(new Runnable() {
			@Override
			public void run() {
				boolean read_sucucess = mstReadSystemAppInfoPkgname();
				if (getRomVersion().equals(mRomVersion) && read_sucucess){
					return;
				}
				getPreInstallAppInfo();
				mstWriteSystemAppInfoPkgname(mPakagenameList);
			}
		});
    }

    private void getPreInstallAppInfo() {
    	File systemAppDir = new File(SYSTEM_APP_PATH);
    	listPreInstallAppFile(systemAppDir,true);
    	File dataAppDir = new File(DATA_APP_PATH);
    	listPreInstallAppFile(dataAppDir,false);
    	File privAppDir = new File(PRIV_APP_PATH);
    	listPreInstallAppFile(privAppDir,true);
		File operatorAppDir = new File(OPERATOR_APP_PATH);
   	    listPreInstallAppFile(operatorAppDir,true);
    }

	private void listPreInstallAppFile(File f, boolean isSystemApp) {
	    if(f == null) {
            return;
	    }
        if(f.isDirectory()){
            File[] fileArray=f.listFiles();
            if(fileArray!=null){
                for (int i = 0; i < fileArray.length; i++) {
                    listPreInstallAppFile(fileArray[i], isSystemApp);
                }
            }
        } else{
            //fillPkgNmaeMapAndList(f);
			getPkgNameOfPreInstallApp(f, isSystemApp);

        }

    }

    private void getPkgNameOfPreInstallApp(File f, boolean isSystemApp){
	    if(f == null) {
            return;
	    }
        String filename = f.getName();
        if(isSystemApp){
            if(filename.endsWith(".apk")){
                fillPkgNmaeMapAndList(f);
            }
        } else {
            if (filename.equals("base.apk") && f.getPath().split("\\.").length > 2){
                try {
                    PackageManager pm = mContext.getPackageManager();
                    PackageInfo appinfo = pm.getPackageArchiveInfo(f.getPath(), PackageManager.GET_ACTIVITIES);
                    if(appinfo != null && appinfo.applicationInfo!= null && appinfo.applicationInfo.packageName != null){
                        updateGnPermissinDB(appinfo.applicationInfo.packageName, false);
                        insertTrustDB(appinfo.applicationInfo.packageName);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "InertDB Failed!!!");
                }
            } else
            if(filename.endsWith(".apk") && filename.split("\\.").length == 2){

            	if(isSystemApp) {
            		fillPkgNmaeMapAndList(f);
            	} else {
            		try {
            			PackageManager pm = mContext.getPackageManager();
            			PackageInfo appinfo = pm.getPackageArchiveInfo(f.getPath(), PackageManager.GET_ACTIVITIES);
            			if(appinfo != null && appinfo.applicationInfo!= null && appinfo.applicationInfo.packageName != null
            					&& (appinfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0){
            				updateGnPermissinDB(appinfo.applicationInfo.packageName, false);
            				insertTrustDB(appinfo.applicationInfo.packageName);
            			} else {
            				fillPkgNmaeMapAndList(f, appinfo);
            			}
            		} catch (Exception e) {
            			Log.e(TAG, "InertDB Failed!!!");
            		}
            	}
            }
        }
    }

    public static HashSet<String> mTrustPackage = new HashSet<String>();
    static {
        mTrustPackage.add("com.cttl.testService");
        mTrustPackage.add("com.cttl.usbtest");
    }

    private void fillPkgNmaeMapAndList(File f){
    	fillPkgNmaeMapAndList(f, null);
    }

    private void fillPkgNmaeMapAndList(File f, PackageInfo appinfo){
    	if(appinfo == null) {
    		PackageManager pm = mContext.getPackageManager();
    		appinfo = pm.getPackageArchiveInfo(f.getPath(), PackageManager.GET_ACTIVITIES);
    	}
        if(appinfo != null
            && appinfo.applicationInfo!= null
            && !mTrustPackage.contains(appinfo.applicationInfo.packageName)){
			mPkgNmaeMap.put(appinfo.applicationInfo.packageName, 0);
			mPakagenameList.add(appinfo.applicationInfo.packageName);
        }
    }

    public boolean isSystemApp(String pkgName){
    	if(pkgName != null){
    		if(mPkgNmaeMap.containsKey(pkgName)){
    			return true ;
    		}
    	}
    	return false ;
    }

    private boolean mstReadSystemAppInfoPkgname() {
    	boolean state = false ;
    	mPkgNmaeMap.clear();
        FileInputStream fis = null;
        try {
            fis = mPakageNameFile.openRead();
            final XmlPullParser in = Xml.newPullParser();
            in.setInput(fis, null);
            int type;
            while ((type = in.next()) != END_DOCUMENT) {
                final String tag = in.getName();
                if (type == START_TAG) {
                    if (PKG_TAG.equals(tag)) {
                        String pkgName = in.nextText();
                        mPkgNmaeMap.put(pkgName, IS_SYSTEMAPP);
                    }
                    if(VERSION_TAG.equals(tag)){
                    	mRomVersion = in.nextText();
                    }
                }
            }
            if(mPkgNmaeMap.size() > 0) {
            	state = true ;
            }
        } catch (FileNotFoundException e) {
        	state = false ;
            Log.e(TAG, "mstReadSystemAppInfoPkgname", e);
        } catch (IOException e) {
        	state = false ;
            Log.e(TAG, "mstReadSystemAppInfoPkgname", e);
        } catch (XmlPullParserException e) {
        	state = false ;
            Log.e(TAG, "mstReadSystemAppInfoPkgname", e);
        } finally {
            IoUtils.closeQuietly(fis);
        }
        return state ;
    }
    public static String getRomVersion() {
        String version = "";
        try {
            Class<?> clazz = Class.forName("android.os.SystemProperties");
            Method method = clazz.getMethod("get", String.class, String.class);
            version = (String) method.invoke(null, "ro.tct.sys.ver", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }
    private void mstWriteSystemAppInfoPkgname(List pkgNameList) {
        FileOutputStream fos = null;
        try {
            fos = mPakageNameFile.startWrite();
            XmlSerializer out = new FastXmlSerializer();
            out.setOutput(fos, "utf-8");
            out.startDocument(null, true);
            for (int i = 0; i < pkgNameList.size(); i++) {
                out.startTag(null, PKG_TAG);
                out.text("" + pkgNameList.get(i));
                out.endTag(null, PKG_TAG);
            }
            out.startTag(null, VERSION_TAG);
            out.text("" + getRomVersion());
            out.endTag(null, VERSION_TAG);
            out.endDocument();
            mPakageNameFile.finishWrite(fos);
        } catch (IOException e) {
            if (fos != null) {
            	mPakageNameFile.failWrite(fos);
            }
            Log.e(TAG, "mstWriteSystemAppInfoPkgname", e);
        }
    }

    public void initPermissionProp() {
        if("".equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level"))){
            SystemProperties.set("persist.sys.permission.level", SystemProperties.get("ro.gn.permission.trust.level", DEFULT_LEVEL));
        }
    }

    public String mstGetPermKey(String permission, String pkgName, String groupName) {
        String permGrp = groupName;
        if(TextUtils.isEmpty(permGrp)) {
        	permGrp = mstGetPermGroupName(permission);
        }
        return permGrp + "-" + pkgName;
    }

    public String mstGetPermKey(String permission, String pkgName) {
    	return mstGetPermKey(permission, pkgName, null);
    }

    public String mstGetPermGroupName(String permission) {
    	String groupName = permission;
    	try {
    		PermissionInfo permissionInfo = mPm.getPermissionInfo(permission, 0);
    		groupName = permissionInfo.group;
    		if(TextUtils.isEmpty(groupName)) {
    			groupName = permission;
    		}
		} catch (Exception e) {
		}
        return groupName;
    }

    public void mstUpdatePermissionStatusToDb(String permission, String pkgName, int status) {
    	try {
    		String groupName = mstGetPermGroupName(permission);
    		ContentValues cv = new ContentValues();
    		cv.put("status", status);
    		if(isInsertedPermStatus(pkgName, groupName)) {
    			mContext.getContentResolver().update(GN_PERM_URI, cv, " packagename = ? and permissiongroup =?",
    					new String[] {pkgName, groupName});
    			if (DEBUG_PERM) Log.d(TAG, "updateData : pkgName = " + pkgName + " permission = " + permission + " status = " + status);
//    		} else {
////    			cv.put("permission", permission);
//    			cv.put("packagename", pkgName);
//    			cv.put("permissiongroup", groupName);
//    			mContext.getContentResolver().insert(GN_PERM_URI, cv);
    		}
    	} catch (Exception e){
    		e.printStackTrace();
    	}
    }

    private void initSystemManager(Context context) {
        if(mPm == null) {
        	mPm = context.getPackageManager();
        }
        if(mSmsManager == null) {
        	mSmsManager = ISms.Stub.asInterface(ServiceManager.getService("isms"));
        }
    }

    private int getSmsState(String pkg) {
        try {
        	if(mSmsManager == null) {
        		mSmsManager = ISms.Stub.asInterface(ServiceManager.getService("isms"));
        	}
            return mSmsManager.getPremiumSmsPermission(pkg);
        } catch (Exception e) {
            return SmsUsageMonitor.PREMIUM_SMS_PERMISSION_UNKNOWN;
        }
    }

    private void setSmsState(String pkg, int state) {
        try {
        	if(mSmsManager == null) {
        		mSmsManager = ISms.Stub.asInterface(ServiceManager.getService("isms"));
        	}
            mSmsManager.setPremiumSmsPermission(pkg, state);
        } catch (Exception e) {
        }
    }
}
//TCL monster add by liuqin for permission 2016-11-11 end
