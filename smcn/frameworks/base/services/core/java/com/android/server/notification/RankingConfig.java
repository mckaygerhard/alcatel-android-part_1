/* Copyright (C) 2016 Tcl Corporation Limited */
/**
 * Copyright (c) 2014, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.server.notification;

public interface RankingConfig {

    int getPriority(String packageName, int uid);

    void setPriority(String packageName, int uid, int priority);

    /* MODIFIED-BEGIN by zhicang.liu, 2016-10-24,BUG-2905505*/
    /* MODIFIED-BEGIN by rongjun.zheng, 2016-10-14,BUG-3121354*/
    int getShowInStatusBar(String packageName, int uid);

    void setShowInStatusBar(String packageName, int uid, int showInStatusBar);
    /* MODIFIED-END by rongjun.zheng,BUG-3121354*/
    /* MODIFIED-END by zhicang.liu,BUG-2905505*/

    int getVisibilityOverride(String packageName, int uid);

    void setVisibilityOverride(String packageName, int uid, int visibility);

    void setImportance(String packageName, int uid, int importance);

    int getImportance(String packageName, int uid);
	/* MODIFIED-BEGIN by zhicang.liu, 2016-09-29,BUG-2905505*/
	void setColor(String packageName, int uid, int color);

    int getColor(String packageName, int uid);
    /* MODIFIED-END by zhicang.liu,BUG-2905505*/
    //MST: begin add by zhicang.liu
    boolean getPackageSuperScriptOverride(String packageName, int uid);

    void setPackageSuperScriptOverride(String packageName, int uid, boolean peekable);

    boolean getPackageLockScreenNotifyOverride(String packageName, int uid);

    void setPackageLockScreenNotifyOverride(String packageName, int uid, boolean peekable);
    //MST: end add by zhicang.liu
}
