/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app.monster;

import android.net.Uri;
import android.provider.BaseColumns;
/**
 * Demonstration of bottom to top implementation of a content provider holding
 * structured data through displaying it in the UI, using throttling to reduce
 * the number of queries done when its data changes.
 * @hide
 */
public class MulwareProviderHelp{
    static final String TAG = "MulwareProviderHelp";
    public static final String SPLIT = "_";
    /**
     * The authority we use to get to our sample provider.
     */
    public static final String AUTHORITY = "com.monster.appmanager.db.MulwareProvider";
    /**notify virus service started*/
    public static final String ACTION_SERVICE_START = "android.intent.action.ACTION_SERVICE_START";
    public static final String ACTION_APP_LIST = "android.intent.action.ACTION_APP_LIST";
    public static final String REQUEST_PERMISSIONS = "android.content.appmanager.action.REQUEST_PERMISSIONS";

    public static final class MulwareTable implements BaseColumns {

        // This class cannot be instantiated
        private MulwareTable() {}

        /**
         * The table name offered by this provider
         */
        public static final String TABLE_NAME = "mulwares";

        /**
         * The content:// style URL for this table
         */
        public static final Uri CONTENT_URI =  Uri.parse("content://" + AUTHORITY + "/mulwares");

        /**
         * The content URI base for a single row of data. Callers must
         * append a numeric row id to this Uri to retrieve a row
         */
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse("content://" + AUTHORITY + "/mulwares/");

        /**
         * The MIME type of {@link #CONTENT_URI}.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/mulwares";

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single row.
         */
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/mulwares";
        /**
         * The default sort order for this table
         */
        public static final String DEFAULT_SORT_ORDER = MulwareTable._ID + " COLLATE LOCALIZED ASC";

        public static final String AD_PACKAGENAME = "ad_packagename";
        public static final String AD_COUNT = "ad_count";
        public static final String AD_PROHIBIT = "ad_prohibit";
        public static final String AD_NAME = "ad_name";
        public static final String AD_BANIPS = "ad_banIps";
        public static final String AD_BANURLS = "ad_banUrls";
    }
}

