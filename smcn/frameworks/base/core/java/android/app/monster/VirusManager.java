/* Copyright (C) 2016 Tcl Corporation Limited */
package android.app.monster;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Log;

/**
 *
 * @author luolaigang
 *@hide
 */
public class VirusManager
{
    	public static final String VIRUS_SERVICE = "virus_scan"; // MODIFIED by luolaigang, 2016-11-10,BUG-2905529
        public static final String TAG = "VirusManager";
        IVirusManager mService;

        public VirusManager(IVirusManager service) {
                mService = service;
        }

        public String getValue(String name){
            try {
                return mService.getValue(name);
            } catch (RemoteException e) {
                Log.e(TAG, "[getValue] RemoteException");
            }
            return null;
        }
}
