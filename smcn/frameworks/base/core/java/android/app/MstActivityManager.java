/* Copyright (C) 2016 Tcl Corporation Limited */
//TCL monster add by liuqin for permission 2016-11-11 start
/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app;
import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.content.pm.PackageManager;
/**
 * Class that operates consumer infrared on the device.
 *
 * <p>
 * To obtain an instance of the system infrared transmitter, call
 * {link android.content.Context#getSystemService(java.lang.String)
 * Context.getSystemService()} with
 * {link android.content.Context#MST_ACTIVITY_MANAGER} as the argument.
 * </p>
 * {@hide}
 */
public final class MstActivityManager  {
    private static final String TAG = "MstActivtyManager";
    private final IMstActivityManager mService;

    public MstActivityManager(Context context) {
        mService = IMstActivityManager.Stub.asInterface(
                ServiceManager.getService(Context.MST_ACTIVITY_MANAGER));
    }

    public void setPackagePermEnable(String pk) {
        // TODO Auto-generated method stub
        try{
             mService.setPackagePermEnable(pk);
        }catch (RemoteException e){
        }
    }

    public MstActivityManager() {
        mService = IMstActivityManager.Stub.asInterface(
                ServiceManager.getService(Context.MST_ACTIVITY_MANAGER));
    }

    public int mstCheckPermmison(String permissionName, int callingUid) {
		int checkValue = PackageManager.PERMISSION_GRANTED;
        try{
             checkValue = mService.mstCheckPermmison(permissionName, callingUid);
        }catch (RemoteException e){
        }
		return checkValue;
    }
}
//TCL monster add by liuqin for permission 2016-11-11 end
