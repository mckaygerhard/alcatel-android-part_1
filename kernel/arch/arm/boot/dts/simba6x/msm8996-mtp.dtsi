/* Copyright (c) 2015-2016, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include "msm8996-pinctrl.dtsi"
#include "msm8996-camera-sensor-mtp.dtsi"
#include "msm8996-wsa881x.dtsi"

/ {
	bluetooth: bt_qca6174 {
		compatible = "qca,qca6174";
		qca,bt-reset-gpio = <&pm8994_gpios 19 0>; /* BT_EN */
		qca,bt-vdd-core-supply = <&pm8994_s3>;
		qca,bt-vdd-pa-supply = <&rome_vreg>;
		qca,bt-vdd-io-supply = <&pm8994_s4>;
		qca,bt-vdd-xtal-supply = <&pm8994_l30>;
		qca,bt-chip-pwd-voltage-level = <1300000 1300000>;
		qca,bt-vdd-io-voltage-level = <1800000 1800000>;
		qca,bt-vdd-xtal-voltage-level = <1800000 1800000>;
	};
};

&ufs_ice {
        status = "ok";
};

&sdcc1_ice {
        status = "ok";
};

&ufsphy1 {
	status = "ok";
};

&ufs1 {
	status = "ok";
};

&uartblsp2dm1 {
	status = "ok";
	pinctrl-names = "default";
	pinctrl-0 = <&uart_console_active>;
};

&sdhc_1 {
	vdd-supply = <&pm8994_l20>;
	qcom,vdd-voltage-level = <2950000 2950000>;
	qcom,vdd-current-level = <200 570000>;

	vdd-io-supply = <&pm8994_s4>;
	qcom,vdd-io-always-on;
	qcom,vdd-io-voltage-level = <1800000 1800000>;
	qcom,vdd-io-current-level = <110 325000>;

	pinctrl-names = "active", "sleep";
	pinctrl-0 = <&sdc1_clk_on &sdc1_cmd_on &sdc1_data_on &sdc1_rclk_on>;
	pinctrl-1 = <&sdc1_clk_off &sdc1_cmd_off &sdc1_data_off &sdc1_rclk_off>;

	qcom,clk-rates = <400000 20000000 25000000 50000000 96000000 192000000 384000000>;
	qcom,ice-clk-rates = <300000000 150000000>;
	qcom,nonremovable;
	qcom,bus-speed-mode = "HS400_1p8v", "HS200_1p8v", "DDR_1p8v";

	status = "ok";
};

&sdhc_2 {
	vdd-supply = <&pm8994_l21>;
	qcom,vdd-voltage-level = <2950000 2950000>;
	qcom,vdd-current-level = <200 800000>;

	vdd-io-supply = <&pm8994_l13>;
	qcom,vdd-io-voltage-level = <1800000 2950000>;
	qcom,vdd-io-current-level = <200 22000>;

	/* MODIFIED-BEGIN by qiuwei, 2016-04-20,BUG-1965011*/
	pinctrl-names = "active", "sleep" , "onetime";
	pinctrl-0 = <&sdc2_clk_on  &sdc2_cmd_on &sdc2_data_on &sdc2_cd_on>;// MODIFIED by qiuwei, 2016-03-23,BUG-1660471
	pinctrl-1 = <&sdc2_clk_off &sdc2_cmd_off &sdc2_data_off &sdc2_cd_off >;
	pinctrl-2 = <&uim1_present &uim2_present>;
	/* MODIFIED-END by qiuwei,BUG-1965011*/
	qcom,clk-rates = <400000 20000000 25000000 50000000 100000000 200000000>;
	qcom,bus-speed-mode = "SDR12", "SDR25", "SDR50", "DDR50", "SDR104";

	cd-gpios = <&tlmm 95 0x0>;    //Add by TCTNB.PANYI, Task-1177060, 2015/12/17, SD card development in msm8996 platform

	status = "ok";
};

&pm8994_vadc {
	chan@5 {
		label = "vcoin";
		reg = <5>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <1>;
		qcom,calibration-type = "absolute";
		qcom,scale-function = <0>;
		qcom,hw-settle-time = <0>;
		qcom,fast-avg-setup = <0>;
	};

	chan@7 {
		label = "vph_pwr";
		reg = <7>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <1>;
		qcom,calibration-type = "absolute";
		qcom,scale-function = <0>;
		qcom,hw-settle-time = <0>;
		qcom,fast-avg-setup = <0>;
	};

	chan@73 {
		label = "msm_therm";
		reg = <0x73>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
	};

	chan@74 {
		label = "emmc_therm";
		reg = <0x74>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
	};

	chan@75 {
		label = "pa_therm0";
		reg = <0x75>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
	};

	chan@77 {
		label = "pa_therm1";
		reg = <0x77>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
	};

	chan@78 {
		label = "quiet_therm";
		reg = <0x78>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
	};

	chan@7c {
		label = "xo_therm_buf";
		reg = <0x7c>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <4>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
	};

	chan@7c {
		label = "xo_therm_buf";
		reg = <0x7c>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <4>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
	};
};

&pm8994_adc_tm {
	chan@73 {
		label = "msm_therm";
		reg = <0x73>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
		qcom,btm-channel-number = <0x48>;
		qcom,thermal-node;
	};

	chan@74 {
		label = "emmc_therm";
		reg = <0x74>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
		qcom,btm-channel-number = <0x68>;
		qcom,thermal-node;
	};

	chan@75 {
		label = "pa_therm0";
		reg = <0x75>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
		qcom,btm-channel-number = <0x70>;
		qcom,thermal-node;
	};

	chan@77 {
		label = "pa_therm1";
		reg = <0x77>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
		qcom,btm-channel-number = <0x78>;
		qcom,thermal-node;
	};

	chan@78 {
		label = "quiet_therm";
		reg = <0x78>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <2>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
		qcom,btm-channel-number = <0x80>;
		qcom,thermal-node;
	};

	chan@7c {
		label = "xo_therm_buf";
		reg = <0x7c>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <0>;
		qcom,calibration-type = "ratiometric";
		qcom,scale-function = <4>;
		qcom,hw-settle-time = <2>;
		qcom,fast-avg-setup = <0>;
		qcom,btm-channel-number = <0x88>;
		qcom,thermal-node;
	};
};

&mdss_hdmi_tx {
	pinctrl-names = "hdmi_hpd_active", "hdmi_ddc_active", "hdmi_cec_active",
				"hdmi_active", "hdmi_sleep";
	pinctrl-0 = <&mdss_hdmi_hpd_active &mdss_hdmi_ddc_suspend
						&mdss_hdmi_cec_suspend>;
	pinctrl-1 = <&mdss_hdmi_hpd_active &mdss_hdmi_ddc_active
						&mdss_hdmi_cec_suspend>;
	pinctrl-2 = <&mdss_hdmi_hpd_active &mdss_hdmi_cec_active
						&mdss_hdmi_ddc_suspend>;
	pinctrl-3 = <&mdss_hdmi_hpd_active &mdss_hdmi_ddc_active
						&mdss_hdmi_cec_active>;
	pinctrl-4 = <&mdss_hdmi_hpd_suspend &mdss_hdmi_ddc_suspend
						&mdss_hdmi_cec_suspend>;
};

&pmi8994_vadc {
	chan@0 {
		label = "usbin";
		reg = <0>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <4>;
		qcom,calibration-type = "absolute";
		qcom,scale-function = <0>;
		qcom,hw-settle-time = <0>;
		qcom,fast-avg-setup = <0>;
	};

	chan@1 {
		label = "dcin";
		reg = <1>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <4>;
		qcom,calibration-type = "absolute";
		qcom,scale-function = <0>;
		qcom,hw-settle-time = <0>;
		qcom,fast-avg-setup = <0>;
	};

	chan@43 {
		label = "usb_dp";
		reg = <0x43>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <1>;
		qcom,calibration-type = "absolute";
		qcom,scale-function = <0>;
		qcom,hw-settle-time = <0>;
		qcom,fast-avg-setup = <0>;
	};

	chan@44 {
		label = "usb_dm";
		reg = <0x44>;
		qcom,decimation = <0>;
		qcom,pre-div-channel-scaling = <1>;
		qcom,calibration-type = "absolute";
		qcom,scale-function = <0>;
		qcom,hw-settle-time = <0>;
		qcom,fast-avg-setup = <0>;
	};
};

#include "msm8996-mdss-panels.dtsi"

&mdss_mdp {
	qcom,mdss-pref-prim-intf = "dsi";
};

&mdss_dsi {
	hw-config = "split_dsi";
};

/* [PLATFORM]-Mod-BEGIN by TCTNB.WPL, 2015/11/27, develop LCD driver*/
&dsi_s6e3fa3_amoled_pmic_cmd {
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply_pmi8994>;
};


&mdss_dsi0 {
	qcom,dsi-pref-prim-pan = <&dsi_s6e3fa3_amoled_pmic_cmd>;
	pinctrl-names = "mdss_default", "mdss_sleep";
/* [PLATFORM]-Mod-BEGIN by TCTNB.WPL, 2016/01/08, defect-1275452, add gpio24 to control v1.8 */
	pinctrl-0 = <&mdss_dsi_active &lcd_power_active &lcd_1v8_active &mdss_te_active>;
	pinctrl-1 = <&mdss_dsi_suspend &lcd_power_suspend &lcd_1v8_suspend &mdss_te_suspend>;
/* [PLATFORM]-Mod-END by TCTNB.WPL, 2016/01/08 */

	qcom,platform-te-gpio = <&tlmm 10 0>;
	qcom,platform-reset-gpio = <&tlmm 8 0>;
/*	qcom,platform-bklight-en-gpio = <&pm8994_gpios 14 0>; */
	qcom,pmi8994-mpp4-gpio = <&pmi8994_mpps 4 0>;

};

&mdss_dsi1 {
	qcom,dsi-pref-prim-pan = <&dsi_s6e3fa3_amoled_pmic_cmd>;
	pinctrl-names = "mdss_default", "mdss_sleep";
/* [PLATFORM]-Mod-BEGIN by TCTNB.WPL, 2016/01/08, defect-1275452, add gpio24 to control v1.8 */
	pinctrl-0 = <&mdss_dsi_active &lcd_power_active &lcd_1v8_active &mdss_te_active>;
	pinctrl-1 = <&mdss_dsi_suspend &lcd_power_suspend &lcd_1v8_suspend &mdss_te_suspend>;
/* [PLATFORM]-Mod-END by TCTNB.WPL, 2016/01/08 */
	qcom,platform-te-gpio = <&tlmm 10 0>;
	qcom,platform-reset-gpio = <&tlmm 8 0>;
/*	qcom,platform-bklight-en-gpio = <&pm8994_gpios 14 0>; */
	qcom,pmi8994-mpp4-gpio = <&pmi8994_mpps 4 0>;
};
/* [PLATFORM]-Mod-END by TCTNB.WPL, 2015/11/27, develop LCD driver*/


&labibb {
	status = "ok";
	qpnp,qpnp-labibb-mode = "amoled";
	qpnp,swire-control; /* [PLATFORM]-Mod-BEGIN by TCTNB.WPL, porting from task-1158877 to defect-1657510, 2016/02/25, modify for configure lab register for display */
};

&dsi_dual_sharp_video {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
};

&dsi_sharp_1080_cmd {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
};

&dsi_dual_nt35597_video {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
};

&dsi_dual_nt35597_cmd {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
	qcom,partial-update-enabled;
	qcom,panel-roi-alignment = <720 128 720 128 1440 128>;
	qcom,panel-allow-phy-poweroff;
};

&dsi_nt35950_4k_dsc_cmd {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
};

&dsi_sharp_4k_dsc_video {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
};

&dsi_sharp_4k_dsc_cmd {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
	qcom,partial-update-enabled;
	/* panel supports slice height of 8/16/32/48/3840 */
	qcom,panel-roi-alignment = <1080 8 1080 8 1080 8>;
};

&dsi_nt35597_dsc_video {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
};

&dsi_nt35597_dsc_cmd {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
	qcom,panel-allow-phy-poweroff;
};

&dsi_dual_sharp_1080_120hz_cmd {
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_wled";
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
};

&dsi_dual_jdi_video {
	pwms = <&pmi8994_pwm_4 0 0>;
	pwm-names = "backlight";
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_pwm";
	qcom,mdss-dsi-bl-pwm-pmi;
	qcom,mdss-dsi-bl-pmic-pwm-frequency = <100>;
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,5v-boost-gpio = <&pmi8994_gpios 8 0>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
};

&dsi_dual_jdi_cmd {
	pwms = <&pmi8994_pwm_4 0 0>;
	pwm-names = "backlight";
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_pwm";
	qcom,mdss-dsi-bl-pwm-pmi;
	qcom,mdss-dsi-bl-pmic-pwm-frequency = <100>;
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,5v-boost-gpio = <&pmi8994_gpios 8 0>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
	qcom,partial-update-enabled;
	qcom,panel-roi-alignment = <4 4 2 2 20 20>;
};

&dsi_dual_jdi_4k_nofbc_video {
	pwms = <&pmi8994_pwm_4 0 0>;
	pwm-names = "backlight";
	qcom,mdss-dsi-bl-pmic-control-type = "bl_ctrl_pwm";
	qcom,mdss-dsi-bl-pwm-pmi;
	qcom,mdss-dsi-bl-pmic-pwm-frequency = <100>;
	qcom,mdss-dsi-bl-min-level = <1>;
	qcom,mdss-dsi-bl-max-level = <4095>;
	qcom,panel-supply-entries = <&dsi_panel_pwr_supply>;
};

/{
	mtp_batterydata: qcom,battery-data {
		qcom,batt-id-range-pct = <15>;
/* [FEATURE]-MOD-BEGIN TCTNB.WJ,09/29/2015,536521 */
		#include "batterydata-byd-3000mah-4400mv.dtsi"
		#include "batterydata-scud-3000mah-4400mv-skt.dtsi"
/* [FEATURE]-MOD-END TCTNB.WJ,09/29/2015,536521 */
	};
};


/*  [FEATURE]-DEL-BEGIN TCTNB.WJ,12/17/2015, No support DC path at idol4s-CN project */
/*
&pmi8994_charger {
	qcom,dc-psy-type = "Wipower";
	qcom,dcin-vadc = <&pmi8994_vadc>;
	qcom,wipower-default-ilim-map = <4000000 20000000 550 700 300>;
	qcom,wipower-pt-ilim-map = <4000000 7140000 550 700 300>,
					<7140000 8140000 550 700 300>,
					<8140000 9140000 500 700 300>,
					<9140000 9950000 500 700 300>;
	qcom,wipower-div2-ilim-map = <4000000 4820000 550 700 300>,
					<4820000 5820000 550 700 300>,
					<5820000 6820000 550 650 650>,
					<6820000 7820000 550 700 600>,
					<7820000 8500000 550 700 550>;
};
*/
/* [FEATURE]-DEL-END TCTNB.WJ,12/17/2015 */


&i2c_7 {
	smb1351-charger@1d {
		compatible = "qcom,smb1351-charger";
		reg = <0x1d>;
		qcom,parallel-charger;
		qcom,float-voltage-mv = <4400>;
		qcom,recharge-mv = <100>;
	};
};

&pmi8994_fg {
	qcom,battery-data = <&mtp_batterydata>;
	qcom,ext-sense-type;
};

/*  [FEATURE]-ADD-BEGIN TCTNB.WJ,1/15/2016, FR979657 */
&pmi8994_charger {
	qcom,battery-data = <&mtp_batterydata>;
};
/* [FEATURE]-ADD-END TCTNB.WJ,1/15/2016 */

&usb_otg_switch {
	status = "okay";
};

&pm8994_mpps {
	mpp@a100 { /* MPP 2 */
		qcom,mode = <1>;		/* Digital output */
		qcom,output-type = <0>;		/* CMOS logic */
		qcom,vin-sel = <2>;		/* S4 1.8V */
		qcom,src-sel = <0>;		/* Constant */
		qcom,master-en = <1>;		/* Enable GPIO */
		status = "okay";
	};

	mpp@a300 { /* MPP 4 */
		/* HDMI_5v_vreg regulator enable */
		qcom,mode = <1>;		/* Digital output */
		qcom,output-type = <0>;		/* CMOS logic */
		qcom,vin-sel = <2>;		/* S4 1.8V */
		qcom,src-sel = <0>;		/* Constant */
		qcom,master-en = <1>;		/* Enable GPIO */
		qcom,invert = <1>;
		status = "okay";
	};
};

&pmi8994_gpios {
	gpio@c400 { /* GPIO 5 - USB3 OTG SWITCH EN */
		qcom,mode = <1>;	/* Digital output */
		qcom,vin-sel = <2>;	/* 1.8 */
		qcom,src-sel = <0>;	/* GPIO */
		qcom,master-en = <1>;	/* Enable GPIO */
		qcom,invert = <0>;	/* Output low initially */
		status = "okay";
	};
};

&pmi8994_gpios {
	gpio@c700 {	/* GPIO 8, lcd_reg_en, 5V boost  */
		qcom,mode = <1>;
		qcom,vin-sel = <2>;
		qcom,src-sel = <0>;
		qcom,invert = <1>;   /* need invert = 0 */
		qcom,master-en = <1>;
		status = "okay";
	};

	gpio@c100 {	/* GPIO 2  SPKR_SD_N */
		qcom,mode = <1>;	/* DIGITAL OUT */
		qcom,pull = <5>;	/* No Pull */
		qcom,vin-sel = <2>;	/* 1.8 */
		qcom,src-sel = <0>;	/* CONSTANT */
		qcom,master-en = <1>;	/* ENABLE GPIO */
		status = "okay";
	};

	gpio@c200 {	/* GPIO 3 SPKR_SD_N */
		qcom,mode = <1>;	/* DIGITAL OUT */
		qcom,pull = <5>;	/* No Pull */
		qcom,vin-sel = <2>;	/* 1.8 */
		qcom,src-sel = <0>;	/* CONSTANT */
		qcom,master-en = <1>;	/* ENABLE GPIO */
		status = "okay";
	};

	gpio@c900 { /* GPIO 10 - HPH_EN1 */
		qcom,mode = <1>;
		qcom,pull = <5>;
		qcom,vin-sel = <2>;
		qcom,src-sel = <2>;
		qcom,master-en = <1>;
		status = "okay";
	};
};

&pmi8994_pwm_4 {
	qcom,channel-owner = "lcd_bl";
	qcom,dtest-line = <4>;
	qcom,dtest-output = <1>;
	status = "okay";
};

&pmi8994_mpps {
	mpp@a000 { /* MPP 1 */
		qcom,mode = <1>;                /* Digital output */
		qcom,output-type = <0>;         /* CMOS logic */
		qcom,vin-sel = <2>;             /* S4 1.8V */
		qcom,src-sel = <7>;             /* DTEST4 */
		qcom,master-en = <1>;           /* Enable MPP */
		status = "okay";
	};

	mpp@a300 { /* MPP 4 */
		/* WLED FET */
		qcom,mode = <1>;        /* DIGITAL OUT */
		qcom,vin-sel = <0>;     /* VIN0 */
		qcom,master-en = <1>;
		status = "okay";
	};
};

&soc {
	i2c@75ba000 {
		synaptics@20 {
			compatible = "synaptics,dsx";
			reg = <0x20>;
			interrupt-parent = <&tlmm>;
			interrupts = <125 0x2008>;
			vdd-supply = <&pm8994_l14>;
			avdd-supply = <&pm8994_l22>;
			pinctrl-names = "pmx_ts_active", "pmx_ts_suspend";
			pinctrl-0 = <&ts_active>;
			pinctrl-1 = <&ts_suspend>;
			synaptics,display-coords = <0 0 1599 2559>;
			synaptics,panel-coords = <0 0 1599 2703>;
			synaptics,reset-gpio = <&tlmm 89 0x00>;
			synaptics,irq-gpio = <&tlmm 125 0x2008>;
			synaptics,disable-gpios;
			synaptics,fw-name = "PR1702898-s3528t_00350002.img";
			/* Underlying clocks used by secure touch */
			clock-names = "iface_clk", "core_clk";
			clocks = <&clock_gcc clk_gcc_blsp2_ahb_clk>,
				 <&clock_gcc clk_gcc_blsp2_qup6_i2c_apps_clk>;
		};
/*[BUGFIX]-Mod-BEGIN by TCTNB.YQJ,525105 , 2015/09/09, add st dtsi for Idol4s*/
		st@49 {
			compatible = "st,fts";
			reg = <0x49>;
			interrupt-parent = <&tlmm>;
			interrupts = <125 0x2008>;
			vdd_ana-supply = <&pm8994_l22>;
			vcc_i2c-supply = <&pm8994_l14>;
			touch,regulator_vdd = "vdd_ana";
			touch,regulator_vddio = "vcc_i2c";
			touch,irq-gpio = <&tlmm 125 0x2008>;
			touch,reset-gpio = <&tlmm 89 0x00>;
			pinctrl-names = "pmx_ts_active", "pmx_ts_suspend";
			pinctrl-0 = <&ts_active>;
			pinctrl-1 = <&ts_suspend>;
			st,irq-on-state = <0>;
			st,irq-flags = <0x2008>; /* IRQF_ONESHOT | IRQF_TRIGGER_LOW */
			//synaptics,power-delay-ms = <200>;
			//synaptics,reset-delay-ms = <200>;
			//synaptics,max-y-for-2d = <800>; /* remove if no virtual buttons */
			//synaptics,vir-button-codes = <102 100 900 100 60 158 300 900 100 60>;
		};
/*[BUGFIX]-Mod-END  by TCTNB.YQJ*/
	};

	gen-vkeys {
		compatible = "qcom,gen-vkeys";
		label = "synaptics_dsx";
		qcom,disp-maxx = <1599>;
		qcom,disp-maxy = <2559>;
		qcom,panel-maxx = <1599>;
		qcom,panel-maxy = <2703>;
		qcom,key-codes = <158 139 102 217>;
	};

	/*[FEATURE]-Add-BEGIN by TCTNB.QW, 526575, 2015/09/02, add HALL for Idol4s*/
	hall:qcom,hall {
        cell-index = <0>;
        compatible = "qcom,hall";
        interrupt-parent = <&tlmm>;
        interrupts = <124 0x3> , <123 0x3>;
        interrupt-gpios = <&tlmm 124 0x00> , <&tlmm 123 0x00>;
        vdd-supply = <&pm8994_s2_corner>;
        pinctrl-names = "default";
	pinctrl-0 = <&hall_default>;
	};
	/*[FEATURE]-Add-END by TCTNB.QW, 526575, 2015/09/02, add HALL for Idol4s*/

	gpio_keys {
		compatible = "gpio-keys";
		input-name = "gpio-keys";

		vol_up {
			label = "volume_up";
			gpios = <&pm8994_gpios 2 0x1>;
			linux,input-type = <1>;
			linux,code = <115>;
			gpio-key,wakeup;
			debounce-interval = <15>;
		};
/*
		cam_snapshot {
			label = "cam_snapshot";
			gpios = <&pm8994_gpios 4 0x1>;
			linux,input-type = <1>;
			linux,code = <766>;
			gpio-key,wakeup;
			debounce-interval = <15>;
		};
*/
/* TCT_NB-qw modify for add boomkey begin,2015.8.26*/
               boom_key {
                       label = "boom_key";
                       gpios = <&pm8994_gpios 4 0x1>;
                       linux,input-type = <1>;
                       linux,code = <550>;
                       gpio-key,wakeup;
                       debounce-interval = <15>;
               };
/* TCT_NB-qw modify for add boomkey end,2015.8.26*/
		cam_focus {
			label = "cam_focus";
			gpios = <&pm8994_gpios 5 0x1>;
			linux,input-type = <1>;
			linux,code = <528>;
			gpio-key,wakeup;
			debounce-interval = <15>;
		};
	};
/*TCT-NB Tianhongwei modify for idol4s cn audio*/
	sound-9335 {
		qcom,model = "msm8996-tasha-mtp-snd-card";

		qcom,audio-routing =
			"AIF4 VI", "MCLK",
			"RX_BIAS", "MCLK",
			"MADINPUT", "MCLK",
			"hifi amp", "LINEOUT1",
			"hifi amp", "LINEOUT2",
			"AMIC2", "MIC BIAS2",
			"AMIC3", "MIC BIAS1",
			"AMIC4", "MIC BIAS1",
			"MIC BIAS1", "Handset Mic",
			"MIC BIAS2", "Headset Mic";
/*			"AMIC2", "MIC BIAS2",
			"MIC BIAS2", "Headset Mic",
			"AMIC3", "MIC BIAS2",
			"MIC BIAS2", "ANCRight Headset Mic",
			"AMIC4", "MIC BIAS2",
			"MIC BIAS2", "ANCLeft Headset Mic",
			"AMIC5", "MIC BIAS3",
			"MIC BIAS3", "Handset Mic",
			"AMIC6", "MIC BIAS4",
			"MIC BIAS4", "Analog Mic6",
			"DMIC0", "MIC BIAS1",
			"MIC BIAS1", "Digital Mic0",
			"DMIC1", "MIC BIAS1",
			"MIC BIAS1", "Digital Mic1",
			"DMIC2", "MIC BIAS3",
			"MIC BIAS3", "Digital Mic2",
			"DMIC3", "MIC BIAS3",
			"MIC BIAS3", "Digital Mic3",
			"DMIC4", "MIC BIAS4",
			"MIC BIAS4", "Digital Mic4",
			"DMIC5", "MIC BIAS4",
			"MIC BIAS4", "Digital Mic5";
			"SpkrLeft IN", "SPK1 OUT",
			"SpkrRight IN", "SPK2 OUT";*//*TCT-NB Tianhongwei remove for idol4s cn*/

		qcom,hdmi-audio-rx;
		asoc-codec = <&stub_codec>, <&hdmi_audio>;
		asoc-codec-names = "msm-stub-codec.1", "msm-hdmi-audio-codec-rx";
//		qcom,hph-en1-gpio = <&pmi8994_gpios 10 0>;
//		qcom,hph-en0-gpio = <&pm8994_gpios 13 0>;
		qcom,msm-mbhc-hphl-swh = <1>;/*TCT-NB Tianhongwei add for idol4s cn*/
		qcom,wsa-max-devs = <0>; // TCTNB.CY - remove wsa device

/*		qcom,us-euro-gpios = <&pm8994_mpps 2 0>;
		qcom,wsa-max-devs = <2>;
		qcom,wsa-devs = <&wsa881x_211>, <&wsa881x_212>,
				<&wsa881x_213>, <&wsa881x_214>;
		qcom,wsa-aux-dev-prefix = "SpkrLeft", "SpkrRight",
					  "SpkrLeft", "SpkrRight";*//*TCT-NB Tianhongwei remove for idol4s cn*/
	};
/*[PLATFORM]-add Begin by TCTNB.ZXZ,2016/02/22,task-1644006,for GPIO NPI DOWN */
	npi_down{
			compatible = "qcom,npi-down-status";
			qcom,npi-down-gpio = <&tlmm 38 0>;
		};
/*[PLATFORM]-add End by TCTNB.ZXZ,2015/12/02 */

};
/*TCT-NB Tianhongwei end idol4s cn*/

&pm8994_gpios {
	gpio@c600 { /* GPIO 7 - NFC DWL REQ */
		qcom,mode = <1>;
		qcom,output-type = <0>;
		qcom,pull = <5>;
		qcom,vin-sel = <2>;
		qcom,out-strength = <3>;
		qcom,src-sel = <0>;
		qcom,master-en = <1>;
		status = "okay";
	};

	gpio@c700 { /* GPIO 8 - WLAN_EN */
		qcom,mode = <1>;		/* Digital output*/
		qcom,pull = <4>;		/* Pulldown 10uA */
		qcom,vin-sel = <2>;		/* VIN2 */
		qcom,src-sel = <0>;		/* GPIO */
		qcom,invert = <0>;		/* Invert */
		qcom,master-en = <1>;		/* Enable GPIO */
		status = "okay";
	};

	gpio@c800 { /* GPIO 9 - Rome 3.3V control */
		qcom,mode = <1>;		/* Digital output */
		qcom,output-type = <0>;		/* MOS logic */
		qcom,invert = <1>;		/* Output high */
		qcom,vin-sel = <0>;		/* VPH_PWR */
		qcom,src-sel = <0>;		/* Constant */
		qcom,out-strength = <1>;	/* High drive strength */
		qcom,master-en = <1>;		/* Enable GPIO */
		status = "okay";
	};

	gpio@c900 { /* GPIO 10 - NFC CLK _REQ*/
		qcom,mode = <0>;
		qcom,vin-sel = <2>;
		qcom,src-sel = <0>;
		qcom,master-en = <1>;
		status = "okay";
	};

	gpio@cd00 { /* GPIO 14 - lcd_bklt_reg_en */
		qcom,mode = <1>;	/* DIGITAL OUT */
		qcom,output-type = <0>;		/* CMOS logic */
		qcom,invert = <1>; 	/* output hight initially */
		qcom,vin-sel = <2>;	/* 1.8 */
		qcom,src-sel = <0>;	/* CONSTANT */
		qcom,out-strength = <1>;	/* Low drive strength */
		qcom,master-en = <1>;	/* ENABLE GPIO */
		status = "okay";
	};
	gpio@c100 { /* GPIO 2 */
		qcom,mode = <0>;
		qcom,pull = <0>;
		qcom,vin-sel = <2>;
		qcom,src-sel = <0>;
		status = "okay";
	};

	gpio@c300 { /* GPIO 4 */
		qcom,mode = <0>;
		qcom,pull = <0>;
		qcom,vin-sel = <2>;
		qcom,src-sel = <0>;
		status = "okay";
	};

	gpio@c400 { /* GPIO 5 */
		qcom,mode = <0>;
		qcom,pull = <0>;
		qcom,vin-sel = <2>;
		qcom,src-sel = <0>;
		status = "okay";
	};

	gpio@cc00 { /* GPIO 13 - HPH_EN0 */
		qcom,mode = <1>;
		qcom,output-type = <0>;
		qcom,pull = <5>;
		qcom,vin-sel = <2>;
		qcom,out-strength = <1>;
		qcom,src-sel = <2>;
		qcom,master-en = <1>;
		status = "okay";
	};

	gpio@ce00 { /* GPIO 15 */
		qcom,mode = <1>;
		qcom,output-type = <0>;
		qcom,pull = <5>;
		qcom,vin-sel = <2>;
		qcom,out-strength = <1>;
		qcom,src-sel = <2>;
		qcom,master-en = <1>;
		status = "okay";
	};

	gpio@d100 { /* GPIO 18 - Rome Sleep Clock */
		qcom,mode = <1>;		/* Digital output */
		qcom,output-type = <0>;		/* CMOS logic */
		qcom,invert = <0>;		/* Output low initially */
		qcom,vin-sel = <2>;		/* VIN 2 */
		qcom,src-sel = <3>;		/* Function 2 */
		qcom,out-strength = <2>;	/* Medium */
		qcom,master-en = <1>;		/* Enable GPIO */
		status = "okay";
	};

	gpio@d200 { /* GPIO 19 - Rome BT Reset */
		qcom,mode = <1>;		/* Digital output*/
		qcom,pull = <4>;		/* Pulldown 10uA */
		qcom,vin-sel = <2>;		/* VIN2 */
		qcom,src-sel = <0>;		/* GPIO */
		qcom,invert = <0>;		/* Invert */
		qcom,master-en = <1>;		/* Enable GPIO */
		status = "okay";
	};
};

&pmi8994_haptics {
	status = "okay";
};

&flash_led {
	qcom,follow-otst2-rb-disabled;
};

&blsp1_uart2 {
	status = "ok";
};

&i2c_6 {
	at24@51 {
		compatible = "atmel,24c32";
		reg = <0x51>;
	};
};

&i2c_7 {
	silabs4705@11 { /* SiLabs FM chip, slave id 0x11*/
		status = "ok";
		compatible = "silabs,si4705";
		reg = <0x11>;
		vdd-supply = <&pm8994_s4>;
		silabs,vdd-supply-voltage = <1800000 1800000>;
		va-supply = <&rome_vreg>;
		silabs,va-supply-voltage = <3300000 3300000>;
		pinctrl-names = "pmx_fm_active","pmx_fm_suspend";
		pinctrl-0 = <&fm_int_active &fm_status_int_active &fm_rst_active>;
		pinctrl-1 = <&fm_int_suspend &fm_status_int_suspend &fm_rst_suspend>;
		silabs,reset-gpio = <&tlmm 39 0>;
		silabs,int-gpio = <&tlmm 38 0>;
		silabs,status-gpio = <&tlmm 78 0>;
		#address-cells = <0>;
		interrupts = <0 1>;
		#interrupt-cells = <1>;
		interrupt-map-mask = <0xffffffff>;
		interrupt-map = <
				  0 &tlmm 38 2
				  1 &tlmm 78 1
				>;
		interrupt-names = "silabs_fm_int", "silabs_fm_status_int";
	};
};

&i2c_8 { /* BLSP2 QUP2 */
	nq@28 {
		compatible = "qcom,nq-nci";
		reg = <0x28>;
		qcom,nq-irq = <&tlmm 9 0x00>;
		qcom,nq-ven = <&tlmm 12 0x00>;
		qcom,nq-firm = <&pm8994_gpios 7 0x00>;
		qcom,nq-clkreq = <&pm8994_gpios 10 0x00>;
		interrupt-parent = <&tlmm>;
		qcom,clk-src = "BBCLK2";
		interrupts = <9 0>;
		interrupt-names = "nfc_irq";
		pinctrl-names = "nfc_active", "nfc_suspend";
		pinctrl-0 = <&nfc_int_active &nfc_disable_active>;
		pinctrl-1 = <&nfc_int_suspend &nfc_disable_suspend>;
		clocks = <&clock_gcc clk_bb_clk2_pin>;
		clock-names = "ref_clk";
	};
};
/* [PLATFORM]-Add-BEGIN by TCTNB.Alvin, 1660471, 2016/04/21, Remove unused driver */
/*
&wil6210 {
	status = "ok";
};*/
/* [PLATFORM]-Add-END by TCTNB.Alvin */

/*TCTNB.CY - add smart PA 2016/7/28*/
&i2c_11 { /* BLSP2 QUP4 */
	tfa98xx@36 {
		compatible = "nxp,tfa98xx";
		reg = <0x36>;
		pinctrl-names ="default", "nxp98xx_spk_rcv_switch_suspend", "nxp98xx_spk_rcv_switch_active";
		pinctrl-0 = <&nxp98xx_reset_gpio_default>;
		pinctrl-1 = <&nxp98xx_spk_rcv_suspend>;
		pinctrl-2 = <&nxp98xx_spk_rcv_active>;
		reset-gpio = <&tlmm 115 0x00>;
		switch-gpio = <&tlmm 85 0x00>;
		vdd-supply = <&pm8994_s4>;
	};
	tfa98xx@34 {
		compatible = "nxp,tfa98xx";
		reg = <0x34>;
		reset-gpio = <&tlmm 133 0x00>;
		switch-gpio = <&tlmm 84 0x00>;
		vdd-supply = <&pm8994_s4>;
	};
};
/*TCTNB.CY end 2016/7/28*/


/* [PLATFORM]-Add-BEGIN by TCTNB.WPL, 2016/01/19, task-1463332, add ANX7418 type C OTG */
&i2c_1 { /* BLSP1 QUP1 */
		analogix_i2c@50{
			compatible ="analogix,ohio";
			status = "ok";
			reg = <0x50>;
			interrupt-parent = <&tlmm>;
			interrupts = <13 0>; //cable_det gpio is interrupt pin

			analogix,p-on-gpio = <&tlmm 96 0x0>;//EDU_PM_GPIO1
			analogix,reset-gpio = <&tlmm 74 0x00>;//EDU_PM_GPIO2
			analogix,intr-comm-gpio = <&tlmm 121 0x0>;//APQ_GPIO_14
			analogix,cbl-det-gpio = <&tlmm 40 0x0>;//APQ_GPIO_80
			analogix.external-ldo-control = <1>;
			analogix,i2c-pull-up = <1>;
		};
};
/* [PLATFORM]-Add-End by TCTNB.WPL, 2016/01/19 */
/* [PLATFORM]-Add-BEGIN by TCTNB.Alvin, 1660471, 2016/04/21, Remove unused driver */
&pcie1 {
	status = "disabled";
};
/* [PLATFORM]-Add-END by TCTNB.Alvin */
/*TCT-NB Tianhongwei add for idol4s audio 2016/3/16*/
&pcie2 {
	status = "disabled";
};
/*TCT-NB Tianhongwei end 2016/3/16*/

