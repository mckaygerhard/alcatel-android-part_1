/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef BRINGUP_WIP
//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedMediaRecorder"
#include <common/AVLog.h>

#include "ExtendedMediaRecorder.h"
#include <media/IMediaRecorder.h>

namespace android {

ExtendedMediaRecorder::ExtendedMediaRecorder(const String16& opPackageName)
  : MediaRecorder(opPackageName),
    mIsPaused(false) {
}

status_t ExtendedMediaRecorder::start() {
    if (!(mCurrentState & MEDIA_RECORDER_PREPARED) && !mIsPaused) {
        AVLOGE("start called in an invalid state: %d mIsPaused = %d", mCurrentState, mIsPaused);
        return INVALID_OPERATION;
    }
    if (mIsPaused) {
        mIsPaused = false;
        return mMediaRecorder->start();
    }
    return MediaRecorder::start();
}

status_t ExtendedMediaRecorder::setParameters(const String8& params) {
    if (!strncmp("pause", params.string(), 5)) {
        AVLOGV("pause");
        if (!(mCurrentState & MEDIA_RECORDER_RECORDING)) {
            AVLOGE("pause called in an invalid state: %d", mCurrentState);
            return INVALID_OPERATION;
        }

        status_t ret = mMediaRecorder->setParameters(params);
        if (OK != ret) {
            AVLOGE("pause failed: %d", ret);
            mCurrentState = MEDIA_RECORDER_ERROR;
            return ret;
        }

        mIsPaused = true;
        return ret;
    }
    return MediaRecorder::setParameters(params);
}

status_t ExtendedMediaRecorder::stop() {
    if (!(mCurrentState & MEDIA_RECORDER_RECORDING)) {
        AVLOGE("stop called in an invalid state: %d", mCurrentState);
        return INVALID_OPERATION;
    }
    mIsPaused = false;
    return MediaRecorder::stop();
}

}
#endif
