/*
 * Copyright (c) 2015, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _ENCODER_MEDIA_BUFFER_H_
#define _ENCODER_MEDIA_BUFFER_H_

#include <common/AVLog.h>
#include <media/stagefright/foundation/ABase.h>
#include <utils/StrongPointer.h>
#include <utils/RefBase.h>
#include <HardwareAPI.h>

namespace android {

class EncoderMediaBuffer : public RefBase {
public:
    struct SubBuffer {
        static const int NumFields = 6;
        int32_t *fd;
        int32_t *offset;
        int32_t *length;
        int32_t *csc;
        int32_t *timestamp;
        int32_t *usage;

        SubBuffer(int32_t *, int32_t *, int32_t *, int32_t *, int32_t *, int32_t *);
        DISALLOW_EVIL_CONSTRUCTORS(SubBuffer);
    };

    /** Sniffs the VideoNativeHandleMetadata and creates appropriate
     * EncoderMediaBuffer
     */
    static sp<EncoderMediaBuffer> Create(VideoNativeHandleMetadata *);

    virtual ~EncoderMediaBuffer() = default;

    /** Determines if the buffer is constructed well */
    virtual bool valid();

    /** Returns number of SubBuffers inside the EncoderMediaBuffer */
    virtual int count();

    /** Iterates through all SubBuffers and calls f(&SubBuffer).
     * Noop if buffer is not valid */
    template<typename Function>
    void each(Function f) {
        if (!valid())
            return;

        for (int c = 0; c < count(); ++c) {
            SubBuffer sb = at(c);
            f(&sb);
        }
    }

protected:
    VideoNativeHandleMetadata *mBuffer;

    EncoderMediaBuffer(VideoNativeHandleMetadata *);

    /** Returns the SubBuffer at index */
    virtual SubBuffer at(int);

private:
    DISALLOW_EVIL_CONSTRUCTORS(EncoderMediaBuffer);
};

}

#endif
