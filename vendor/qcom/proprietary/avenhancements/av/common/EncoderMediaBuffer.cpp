/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "common/EncoderMediaBuffer.h"
#include <OMX_QCOMExtns.h>

namespace android {

sp<EncoderMediaBuffer> EncoderMediaBuffer::Create(
        VideoNativeHandleMetadata *buffer) {
    if (EncoderMediaBuffer(buffer).valid())
        return new EncoderMediaBuffer(buffer);

    return NULL;
}

EncoderMediaBuffer::EncoderMediaBuffer(VideoNativeHandleMetadata *buffer) :
    mBuffer(buffer) {
}

int EncoderMediaBuffer::count() {
    native_handle_t *nh = (native_handle_t *)mBuffer->pHandle;

    return valid() ? nh->numFds : 0;
}

bool EncoderMediaBuffer::valid() {
    if (!mBuffer)
        return false;

    native_handle_t *nh = (native_handle_t *)mBuffer->pHandle;

    return mBuffer->eType == kMetadataBufferTypeNativeHandleSource &&
        nh->numInts == (nh->numFds * (SubBuffer::NumFields - 1 /* exclude FDs */)) + VIDEO_METADATA_NUM_COMMON_INTS;
}

EncoderMediaBuffer::SubBuffer EncoderMediaBuffer::at(int index) {
    native_handle_t *nh = (native_handle_t *)mBuffer->pHandle;

    int32_t *fd = &nh->data[index + 0 * nh->numFds],
            *offset = &nh->data[index + 1 * nh->numFds],
            *length = &nh->data[index + 2 * nh->numFds],
            *csc = &nh->data[index + 3 * nh->numFds],
            *timestamp = &nh->data[index + 4 * nh->numFds],
            *usage = &nh->data[index + 5 * nh->numFds];

    return {fd, offset, length, csc, timestamp, usage};
}

EncoderMediaBuffer::SubBuffer::SubBuffer(int32_t *fd, int32_t *offset,
        int32_t *length, int32_t *csc, int32_t *timestamp, int32_t *usage) :
    fd(fd),
    offset(offset),
    length(length),
    csc(csc),
    timestamp(timestamp),
    usage(usage) {
}

}
