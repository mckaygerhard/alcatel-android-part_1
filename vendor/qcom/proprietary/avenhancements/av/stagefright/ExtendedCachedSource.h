/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef BRINGUP_WIP
#ifndef _EXTENDED_NUCACHEDSOURCE2_H_
#define _EXTENDED_NUCACHEDSOURCE2_H_

#include "include/NuCachedSource2.h"

namespace android {

struct ExtendedCachedSource : public NuCachedSource2 {
    static sp<ExtendedCachedSource> Create(
        const sp<DataSource> &source,
        const char *cacheConfig,
        bool disconnectAtHighwatermark);

    virtual void suspendCaching();
    virtual void resumeCaching(const sp<DataSource> &source);

private:
    ExtendedCachedSource(
        const sp<DataSource> &source,
        const char *cacheConfig,
        bool disconnectAtHighwatermark);

    DISALLOW_EVIL_CONSTRUCTORS(ExtendedCachedSource);
};

} // namespace android

#endif // _EXTENDED_NUCACHEDSOURCE2_H_
#endif
