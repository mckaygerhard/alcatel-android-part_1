/*
 * Copyright (c) 2015, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _EXT_AUDIO_SOURCE_H
#define _EXT_AUDIO_SOURCE_H

#include <stdint.h>
#include <media/stagefright/AudioSource.h>
#include <media/stagefright/MediaBuffer.h>

namespace android {

struct AudioSource;

struct ExtendedAudioSource : public AudioSource {
    ExtendedAudioSource(
            audio_source_t inputSource,
            const String16 &opPackageName,
            uint32_t sampleRate,
            uint32_t channels,
            uint32_t outSampleRate = 0,
	    uid_t clientUid = -1,
	    pid_t clientPid = -1);
    virtual status_t reset();
    void onEvent(int event, void* info);

protected:
    virtual ~ExtendedAudioSource();

private:
    uint32_t mPrevPosition;
    uint32_t mAllocBytes;
    audio_session_t mAudioSessionId;
    AudioRecord::transfer_type mTransferMode;
    AudioRecord::Buffer mTempBuf;
    void audioRecordInit(audio_source_t inputSource, const String16 &opPackageName, uint32_t channelCount);
};

}

#endif //_EXT_AUDIO_SOURCE_H
