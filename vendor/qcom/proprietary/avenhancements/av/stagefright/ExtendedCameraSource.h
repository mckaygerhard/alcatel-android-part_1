/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef BRINGUP_WIP
#ifndef _EXT_CAMERA_SOURCE_H
#define _EXT_CAMERA_SOURCE_H

#include <media/stagefright/CameraSource.h>

namespace android {

class CameraSource;
class ICamera;
class ICameraRecordingProxy;
class String16;
class IGraphicBufferProducer;

struct ExtendedCameraSource : public CameraSource {
    ExtendedCameraSource(const sp<ICamera>& camera, const sp<ICameraRecordingProxy>& proxy,
                 int32_t cameraId, const String16& clientName, uid_t clientUid,
                 Size videoSize, int32_t frameRate,
                 const sp<IGraphicBufferProducer>& surface,
                 bool storeMetaDataInVideoBuffers);
    virtual status_t start(MetaData *params = NULL);
    virtual status_t pause();
    virtual void dataCallbackTimestamp(int64_t timestampUs, int32_t msgType,
            const sp<IMemory> &data);

private:
    bool mRecPause;
    int64_t mPauseAdjTimeUs;
    int64_t mPauseStartTimeUs;
    int64_t mPauseEndTimeUs;
    int64_t mLastFrameOriginalTimestampUs;
};

}
#endif
#endif //_EXT_CAMERA_SOURCE_H
