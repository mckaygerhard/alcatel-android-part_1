/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef BRINGUP_WIP
//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedCameraSource"
#include <common/AVLog.h>

#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MediaErrors.h>
#include <media/stagefright/MetaData.h>
#include <camera/Camera.h>
#include <camera/CameraParameters.h>
#include <gui/Surface.h>
#include <utils/String8.h>
#include <cutils/properties.h>
#include "ExtendedCameraSource.h"

namespace android {

ExtendedCameraSource::ExtendedCameraSource(
             const sp<ICamera>& camera,
             const sp<ICameraRecordingProxy>& proxy,
             int32_t cameraId,
             const String16& clientName,
             uid_t clientUid,
             Size videoSize,
             int32_t frameRate,
             const sp<IGraphicBufferProducer>& surface,
             bool storeMetaDataInVideoBuffers)
  : CameraSource(camera, proxy, cameraId, clientName, clientUid, videoSize,
                 frameRate, surface, storeMetaDataInVideoBuffers),
    mRecPause(false),
    mPauseAdjTimeUs(0),
    mPauseStartTimeUs(0),
    mPauseEndTimeUs(0),
    mLastFrameOriginalTimestampUs(0) {
}

status_t ExtendedCameraSource::start(MetaData *params) {
    if(mRecPause) {
        mRecPause = false;
        mPauseAdjTimeUs += mPauseEndTimeUs - mPauseStartTimeUs;
        AVLOGI("resume : mPause Adj / End / Start : %" PRId64 " / %" PRId64 " / %" PRId64 "us",
                mPauseAdjTimeUs, mPauseEndTimeUs, mPauseStartTimeUs);
        return OK;
    }
    mRecPause = false;
    mPauseAdjTimeUs = 0;
    mPauseStartTimeUs = 0;
    mPauseEndTimeUs = 0;
    return CameraSource::start(params);
}

status_t ExtendedCameraSource::pause() {
    mRecPause = true;
    mPauseStartTimeUs = mLastFrameOriginalTimestampUs;
    AVLOGI("pause : mPauseStart %" PRId64 " us, #Queued Frames : %zu",
        mPauseStartTimeUs, mFramesReceived.size());
    return OK;
}

void ExtendedCameraSource::dataCallbackTimestamp(int64_t timestampUs, int32_t msgType,
    const sp<IMemory> &data) {
    {
        Mutex::Autolock autoLock(mLock);
        if (!mStarted || (mNumFramesReceived == 0 && timestampUs < mStartTimeUs)) {
            AVLOGV("Drop frame at %lld/%lld us", (long long)timestampUs, (long long)mStartTimeUs);
            releaseOneRecordingFrame(data);
            return;
        }

        if (mRecPause == true) {
            if(!mFramesReceived.empty()) {
                AVLOGV("releaseQueuedFrames - #Queued Frames : %zu",
                        mFramesReceived.size());
                releaseQueuedFrames();
            }
            AVLOGV("Release one video frame for Pause : %" PRId64 " us",
                    timestampUs);
            releaseOneRecordingFrame(data);
            mPauseEndTimeUs = timestampUs;
            return;
        }

        mLastFrameOriginalTimestampUs = timestampUs;
        timestampUs -= mPauseAdjTimeUs;
    }
    CameraSource::dataCallbackTimestamp(timestampUs, msgType, data);
}

}
#endif
