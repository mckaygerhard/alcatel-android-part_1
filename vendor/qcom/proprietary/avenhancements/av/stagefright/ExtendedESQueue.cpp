/*
 * Copyright (c) 2015, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedESQueue"

#include "stagefright/ExtendedESQueue.h"
#include "stagefright/ExtendedUtils.h"
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MetaData.h>
#include <QCMetaData.h>
#include <common/AVLog.h>

namespace android {

ExtendedESQueue::ExtendedESQueue(
        ElementaryStreamQueue::Mode mode,
        uint32_t flags):
        ElementaryStreamQueue(mode, flags) {
}

ExtendedESQueue::~ExtendedESQueue() {
}

sp<MetaData> ExtendedESQueue::MakeHEVCCodecSpecificData(const sp<ABuffer> &accessUnit) {
    const uint8_t *data = accessUnit->data();
    size_t size = accessUnit->size();

    if (data == NULL || size == 0) {
        ALOGE("Invalid HEVC CSD");
        return NULL;
    }

    sp<MetaData> meta = new MetaData;
    meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_VIDEO_HEVC);
    meta->setData(kKeyHVCC, kTypeHVCC, data, size);

    // Set width & height to minimum (QCIF). This will trigger a port reconfig &
    // the decoder will find the correct dimensions.
    meta->setInt32(kKeyWidth, (int32_t)177);
    meta->setInt32(kKeyHeight, (int32_t)144);

    // Let the decoder do the frame parsing for HEVC in case access unit data is
    // not aligned to frame boundaries.
    meta->setInt32(kKeyUseArbitraryMode, 1);

    // Set the container format as TS, so that timestamp reordering can be
    // enabled for HEVC TS clips.
    meta->setCString(kKeyFileFormat, MEDIA_MIMETYPE_CONTAINER_MPEG2TS);

    return meta;
}

sp<ABuffer> ExtendedESQueue::dequeueAccessUnitH265() {
    if (mRangeInfos.empty()) {
            return NULL;
    }

    RangeInfo info = *mRangeInfos.begin();
    mRangeInfos.erase(mRangeInfos.begin());

    sp<ABuffer> accessUnit = new ABuffer(info.mLength);
    memcpy(accessUnit->data(), mBuffer->data(), info.mLength);
    accessUnit->meta()->setInt64("timeUs", info.mTimestampUs);

    memmove(mBuffer->data(),
                mBuffer->data() + info.mLength,
                mBuffer->size() - info.mLength);

    mBuffer->setRange(0, mBuffer->size() - info.mLength);

    if (mFormat == NULL) {
        mFormat = MakeHEVCCodecSpecificData(accessUnit);
    }

    return accessUnit;
}

} // namespace android
