/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
/*
 * Copyright (c) 2013 - 2015, The Linux Foundation. All rights reserved.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedExtractor"
#include <common/AVLog.h>
#include <dlfcn.h>
#include "ExtendedExtractor.h"
#include <media/stagefright/MetaData.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MediaExtractor.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/DataSource.h>
#include <MPEG4Extractor.h>

namespace android {

static const char* kExtendedExtractorLibName = "libExtendedExtractor.so";
static const char* kExtendedExtractorCreator = "CreateExtractor";
static const char* kExtendedExtractorSniffer = "SniffExtendedExtractor";

//static
void *ExtendedExtractor::mLibHandle = NULL;

//static
ExtendedExtractor::CreateFunc ExtendedExtractor::sCreator =
        ExtendedExtractor::loadCreator();

//static
DataSource::SnifferFunc ExtendedExtractor::sSniffer =
        ExtendedExtractor::loadSniffer();

MediaExtractor* ExtendedExtractor::create (
        const sp<DataSource> &source,
        const char *mime,
        const sp<AMessage> &) {

    //TBD: Need to add all conditional checks to decide if default extractor or
    //QTI extractor to be used.

    if (sCreator == NULL) {
        AVLOGW("ExtendedExtractor::Create: Failed to load mm-parser");
        return NULL;
    }

    if (mime == NULL) {
        AVLOGE("ExtendedExtractor::Create: NULL mime");
        return NULL;
    }

    MediaExtractor* extractor = NULL;
    AVLOGI("QTIParser is prefered");
    extractor = (*sCreator)(source, mime);
    AVLOGI("ExtendedExtractor::create %p", extractor);
    return extractor;
}

static bool yetAnotherDumbSniffer(
        const sp<DataSource> &, String8 *,
        float *, sp<AMessage> *) {
    return false;
}

DataSource::SnifferFunc ExtendedExtractor::getExtendedSniffer() {
    DataSource::SnifferFunc sniffer = sSniffer != NULL ? sSniffer :
            yetAnotherDumbSniffer;
    return sniffer;
}

//static
ExtendedExtractor::CreateFunc ExtendedExtractor::loadCreator() {
    if (mLibHandle == NULL) {
        mLibHandle = ::dlopen(kExtendedExtractorLibName, RTLD_LAZY);
        if (mLibHandle == NULL) {
            AVLOGW("Failed to load %s : %s", kExtendedExtractorLibName, dlerror());
        }
    }

    ExtendedExtractor::CreateFunc creator = NULL;
    if (mLibHandle != NULL) {
        creator = (ExtendedExtractor::CreateFunc) dlsym (mLibHandle,
                kExtendedExtractorCreator);
        if (creator == NULL) {
            AVLOGW("Failed to load symbol %s : %s", kExtendedExtractorCreator,
                    dlerror());
        }
    }
    return creator;
}

//statc
DataSource::SnifferFunc ExtendedExtractor::loadSniffer() {
    if (mLibHandle == NULL) {
        mLibHandle = ::dlopen(kExtendedExtractorLibName, RTLD_LAZY);
        if (mLibHandle == NULL) {
            AVLOGW("Failed to load %s : %s", kExtendedExtractorLibName, dlerror());
        }
    }

    DataSource::SnifferFunc sniffer = NULL;
    if (mLibHandle != NULL) {
        sniffer = (DataSource::SnifferFunc) dlsym (mLibHandle,
                kExtendedExtractorSniffer);
        if (sniffer == NULL) {
            AVLOGW("Failed to load symbol %s : %s", kExtendedExtractorSniffer,
                    dlerror());
        }
    }
    return sniffer;
}

}  // namespace android
