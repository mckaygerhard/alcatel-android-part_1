/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _EXTENDED_NUPLAYER_H_
#define _EXTENDED_NUPLAYER_H_

namespace android {

struct ExtendedNuPlayer : public NuPlayer {
    ExtendedNuPlayer(pid_t pid);

    virtual void setDataSourceAsync(
            const sp<IMediaHTTPService> &httpService,
            const char *url,
            const KeyedVector<String8, String8> *headers);

    virtual status_t instantiateDecoder(
        bool audio, sp<DecoderBase> *decoder, bool checkAudioModeChange = true);
    virtual void onResume();
#ifdef PCM_OFFLOAD_ENABLED
    virtual void tryOpenAudioSinkForOffload(const sp<AMessage> &format, bool hasVideo);
    virtual void setDecodedPcmOffload(bool decodedPcmOffload);
    virtual bool ifDecodedPCMOffload();
    virtual bool canOffloadDecodedPCMStream(const sp<MetaData> meta,
            bool hasVideo, bool isStreaming, audio_stream_type_t streamType);
#endif

protected:
    virtual ~ExtendedNuPlayer();
#ifndef BRINGUP_WIP
    virtual void notifyListener(int msg, int ext1, int ext2, const Parcel *in = NULL);
#endif
    virtual void onSourceNotify(const sp<AMessage> &msg);

private:
    ExtendedNuPlayer(const ExtendedNuPlayer &);
    ExtendedNuPlayer &operator=(ExtendedNuPlayer &);

private:
    bool mIsSourcePersistent;
    bool mOffloadDecodedPCM;
    void showImageInNativeWindow(const sp<AMessage> &format);
};

} // namespace android

#endif // _EXTENDED_NUPLAYER_H_

