/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef BRINGUP_WIP
//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedGenericSource"
#include <common/AVLog.h>

#include "ExtendedGenericSource.h"
#include "../stagefright/ExtendedCachedSource.h"
#include "include/HTTPBase.h"
#include <media/IMediaHTTPService.h>
#include <cutils/properties.h>

namespace android {

Mutex ExtendedGenericSource::mSourceLock;
sp<ExtendedGenericSource> ExtendedGenericSource::mPersistentSource;

static bool isLocalHost(const char* url) {
   static const size_t kLocalHostStrLen = 16;
   if (url != NULL && strlen(url) >= kLocalHostStrLen + 1) {
       if (!strncmp(url, "http://localhost", kLocalHostStrLen) ||
           !strncmp(url, "http://127.0.0.1", kLocalHostStrLen) ||
           !strncmp(url, "https://localhost", kLocalHostStrLen + 1) ||
           !strncmp(url, "https://127.0.0.1", kLocalHostStrLen + 1)) {
          AVLOGI("Local host URL, dont create persistent source");
          return true;
       }
   }
   return false;
}

sp<ExtendedGenericSource> ExtendedGenericSource::CreatePersistentSource(
        const sp<AMessage> &notify,
        bool uidValid,
        uid_t uid,
        const char *url) {
    AVLOGI("CreatePersistentSource uid %d url %s", uid, url);

    char value[PROPERTY_VALUE_MAX];
    if (property_get("av.debug.disable.pers.cache", value, NULL) &&
        (!strcmp("1", value) || !strcasecmp("true", value))) {
        AVLOGI("Persistent source creation is disabled by property");
        return new ExtendedGenericSource(notify, uidValid, uid);
    }

    if (isLocalHost(url)) {
        return new ExtendedGenericSource(notify, uidValid, uid);
    }
    Mutex::Autolock autoLock(mSourceLock);

    // PersistentSource exists and it is not in use
    if (mPersistentSource != NULL &&
        mPersistentSource->getStrongCount() == 1 &&
        uid == mPersistentSource->mUID &&
        !strcmp(url, mPersistentSource->mUri.c_str()) &&
        mPersistentSource->mPrepared)  {
        // Reuse if UID and URL match, and source is prepared
        mPersistentSource->mNotify = notify;
        AVLOGI("Found matching inactive source, resume playback");
        return mPersistentSource;
    } else if (mPersistentSource == NULL ||
               (mPersistentSource != NULL &&
                mPersistentSource->getStrongCount() == 1)){
        // Persistent source is NULL, create new one
        // overwrite old source here if
        //    PersistentSource is not in use
        //    PersistentSource is not prepared
        AVLOGI("Create persistent source object");
        mPersistentSource = new ExtendedGenericSource(
                                 notify, uidValid, uid, true);
        return mPersistentSource;
    }
    AVLOGI("Persistent source is in use, create new source");
    return new ExtendedGenericSource(notify, uidValid, uid);
}

void ExtendedGenericSource::clearPersistentSource(int err) {
    Mutex::Autolock autoLock(mSourceLock);
    if (mPersistentSource != NULL && mIsPersistent) {
        AVLOGI("Clear persistent source %d", err);
        mPersistentSource.clear();
    }
}

ExtendedGenericSource::ExtendedGenericSource(
        const sp<AMessage> &notify,
        bool uidValid,
        uid_t uid,
        bool isPeristent)
      : GenericSource(notify, uidValid, uid),
        mIsPersistent(isPeristent),
        mPrepared(false) {
    AVLOGV("Create ExtendedGenericSource for uid = %d, is static = %d",
        uid, mIsPersistent);
}

ExtendedGenericSource::~ExtendedGenericSource() {
    AVLOGV("~ExtendedGenericSource");
}

bool ExtendedGenericSource::isPersistent() {
    return mIsPersistent;
}

status_t ExtendedGenericSource::reconnectAndResumeCaching() {
    AVLOGI("reconnectAndResumeCaching");

    status_t err = UNKNOWN_ERROR;
    mHttpSource = DataSource::CreateMediaHTTP(mHTTPService);
    if (mHttpSource == NULL) {
        AVLOGE("Failed to create http source!");
        notifyPreparedAndCleanup(err);
        return err;
    }

    String8 cacheConfig;
    bool disconnectAtHighwatermark;
    KeyedVector<String8, String8> nonCacheSpecificHeaders;
    nonCacheSpecificHeaders = mUriHeaders;
    NuCachedSource2::RemoveCacheSpecificHeaders(
        &nonCacheSpecificHeaders,
        &cacheConfig,
        &disconnectAtHighwatermark);

    if (static_cast<HTTPBase *>(mHttpSource.get())->connect(
                      mUri.c_str(), &nonCacheSpecificHeaders) != OK) {
        AVLOGE("Failed to connect http source!");
        notifyPreparedAndCleanup(err);
        return err;
    }
    if (mDataSource != NULL) {
        static_cast<ExtendedCachedSource *>(mDataSource.get())->resumeCaching(mHttpSource);
    }

    return OK;
}

void ExtendedGenericSource::notifyPrepared(status_t err) {
    AVLOGV("notifyPrepared %d, use pers %d", err, mIsPersistent );
    setPrepared(err);
    GenericSource::notifyPrepared(err);
}

void ExtendedGenericSource::notifyPreparedAndCleanup(status_t err) {
    AVLOGV("notifyPreparedAndCleanup %d, %d", err, mIsPersistent);
    setPrepared(err);
    GenericSource::notifyPreparedAndCleanup(err);
}

void ExtendedGenericSource::setPrepared(status_t err) {
    mPrepared = (err == OK ? true: false);
    AVLOGV("setPrepared %d", mPrepared);
}

void ExtendedGenericSource::onPrepareAsync() {
    AVLOGV("onPrepareAsync");
    if (mIsPersistent && mPrepared) {
        return onPrepareAsyncInternal();
    }
    GenericSource::onPrepareAsync();
}

void ExtendedGenericSource::onPrepareAsyncInternal() {
    AVLOGV("prepareAsyncInternal");

    if (reconnectAndResumeCaching() != OK) {
        return;
    }

    if (mIsSecure) {
        // secure decoders must be instantiated before starting widevine source
        sp<AMessage> reply = new AMessage(kWhatSecureDecodersInstantiated, this);
        notifyInstantiateSecureDecoders(reply);
    }

    notifyPrepared(OK);
    notifyFlagsChanged(
            (mIsSecure ? FLAG_SECURE : 0)
            | (mDecryptHandle != NULL ? FLAG_PROTECTED : 0)
            | FLAG_CAN_PAUSE
            | FLAG_CAN_SEEK_BACKWARD
            | FLAG_CAN_SEEK_FORWARD
            | FLAG_CAN_SEEK);
}

void ExtendedGenericSource::stop() {
    AVLOGV("stop");
    if (mIsPersistent) {
        AVLOGI("cancel poll buffering");
        cancelPollBuffering();
    }
    if (mIsPersistent && mDataSource != NULL &&
        mDataSource->flags() &
                 DataSource::kIsCachingDataSource) {
        static_cast<ExtendedCachedSource *>
                 (mDataSource.get())->suspendCaching();
    }

    GenericSource::stop();
}

}  // namespace android
#endif
