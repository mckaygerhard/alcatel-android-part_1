/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _EXTENDED_GENERIC_SOURCE_H_
#define _EXTENDED_GENERIC_SOURCE_H_

#include "nuplayer/NuPlayer.h"
#include "nuplayer/GenericSource.h"

#ifndef BRINGUP_WIP
namespace android {

class DataSource;
struct IMediaHTTPService;

struct ExtendedGenericSource : public NuPlayer::GenericSource {
    ExtendedGenericSource(
        const sp<AMessage> &notify,
        bool uidValid,
        uid_t uid,
        bool isPersistent = false);

    static sp<ExtendedGenericSource> CreatePersistentSource(
        const sp<AMessage> &notify,
        bool uidValid,
        uid_t uid,
        const char *url);

    virtual void stop();

    void clearPersistentSource(int err);
    bool isPersistent();

protected:
    virtual ~ExtendedGenericSource();
    virtual void onPrepareAsync();
    virtual void notifyPrepared(status_t err = OK);
    virtual void notifyPreparedAndCleanup(status_t err);

private:
    void onPrepareAsyncInternal();
    void setPrepared(status_t err);
    status_t reconnectAndResumeCaching();

private:
    static Mutex                      mSourceLock;
    static sp<ExtendedGenericSource>  mPersistentSource;
    bool                              mIsPersistent;
    bool                              mPrepared;

    DISALLOW_EVIL_CONSTRUCTORS(ExtendedGenericSource);
};

} //namespace android
#endif //BRINGUP_WIP

#endif // _EXTENDED_PERSISTENT_SOURCE_H_

