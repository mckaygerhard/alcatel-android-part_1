/*
 * Copyright (c) 2015-2016, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "ExtendedNuPlayer"
#include <common/AVLog.h>
#include <cutils/properties.h>

#include <nuplayer/NuPlayer.h>
#include <nuplayer/NuPlayerDecoderBase.h>
#include <nuplayer/NuPlayerDecoderPassThrough.h>
#include <nuplayer/NuPlayerSource.h>
#include <nuplayer/NuPlayerRenderer.h>
#include <nuplayer/RTSPSource.h>
#include <nuplayer/HTTPLiveSource.h>

#include "stagefright/AVExtensions.h"
#include <media/stagefright/Utils.h>
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/foundation/ABuffer.h>

#ifdef HLS_AUDIO_ONLY_IMG_DISPLAY
#include <gui/IGraphicBufferProducer.h>
#include <gui/Surface.h>
#include <ui/GraphicBufferMapper.h>
#include <system/window.h>
#endif

#include "mediaplayerservice/AVNuExtensions.h"
#include "mediaplayerservice/ExtendedNuPlayer.h"
#include "mediaplayerservice/ExtendedGenericSource.h"
#include "mediaplayerservice/ExtendedHTTPLiveSource.h"
#include "stagefright/ExtendedUtils.h"

namespace android {

ExtendedNuPlayer::ExtendedNuPlayer(pid_t pid)
        : NuPlayer(pid),
          mIsSourcePersistent(false),
          mOffloadDecodedPCM(false) {
    updateLogLevel();
    AVLOGV("ExtendedNuPlayer()");
}

ExtendedNuPlayer::~ExtendedNuPlayer() {
    AVLOGV("~ExtendedNuPlayer()");
}

void ExtendedNuPlayer::setDataSourceAsync(
            const sp<IMediaHTTPService> &httpService,
            const char *url,
            const KeyedVector<String8, String8> *headers) {
    AVLOGV("setDataSource url %s", url);

    sp<AMessage> msg = new AMessage(kWhatSetDataSource, this);
    size_t len = strlen(url);

    sp<AMessage> notify = new AMessage(kWhatSourceNotify, this);

    sp<Source> source;
    if (NuPlayer::IsHTTPLiveURL(url)) {
        char value[PROPERTY_VALUE_MAX];
        property_get("persist.media.hls.customization", value, "0");
        if(atoi(value)) {
            AVLOGI("new ExtendedHTTPLiveSource");
            source = new ExtendedHTTPLiveSource(notify, httpService, url, headers);
        } else {
            source = new HTTPLiveSource(notify, httpService, url, headers);
        }
    } else if (!strncasecmp(url, "rtsp://", 7)) {
        source = new RTSPSource(
                notify, httpService, url, headers, mUIDValid, mUID);
    } else if ((!strncasecmp(url, "http://", 7)
                || !strncasecmp(url, "https://", 8))
                    && ((len >= 4 && !strcasecmp(".sdp", &url[len - 4]))
                    || strstr(url, ".sdp?"))) {
        source = new RTSPSource(
                notify, httpService, url, headers, mUIDValid, mUID, true);
    } else {
        sp<GenericSource> genericSource;
        bool isStreaming = false;
        if (!strncasecmp(url, "http://", 7)
            || !strncasecmp(url, "https://", 8)) {
            isStreaming = true;
        }
#ifndef BRINGUP_WIP
        if (isStreaming) {
            genericSource = ExtendedGenericSource::CreatePersistentSource(
                                               notify, mUIDValid, mUID, url);
        } else {
#endif
        genericSource = new GenericSource(notify, mUIDValid, mUID);
#ifndef BRINGUP_WIP
        }
#endif

        // Don't set FLAG_SECURE on mSourceFlags here for widevine.
        // The correct flags will be updated in Source::kWhatFlagsChanged
        // handler when  GenericSource is prepared.

        status_t err = genericSource->setDataSource(httpService, url, headers);

        if (err == OK) {
            source = genericSource;
#ifndef BRINGUP_WIP
            if (isStreaming) {
                mIsSourcePersistent =
                    ((ExtendedGenericSource *)genericSource.get())->isPersistent();
            }
#endif
        } else {
            AVLOGE("Failed to set data source!");
        }
    }
    msg->setObject("source", source);
    msg->post();
}

status_t ExtendedNuPlayer::instantiateDecoder(
    bool audio, sp<DecoderBase> *decoder, bool checkAudioModeChange) {

    bool bIgnore = false;
    if (audio) {
        char disableAudio[PROPERTY_VALUE_MAX];
        property_get("persist.debug.sf.noaudio", disableAudio, "0");
        bIgnore = (atoi(disableAudio) & 0x01) != 0;
        AVLOGV("Audio disabled %d", bIgnore);
    }

    if (!bIgnore) {
        return NuPlayer::instantiateDecoder(audio, decoder, checkAudioModeChange);
    }
    return OK;
}

void ExtendedNuPlayer::onResume() {

    if (!mPaused) {
        return;
    }

    if (mSource != NULL) {
        sp<AMessage> audioformat = mSource->getFormat(true /*audio*/);
        const bool hasaudio = (audioformat != NULL);

        //check whether decoder can be allowed from utils
        if (hasaudio) {
            if (!ExtendedUtils::isHwAudioDecoderSessionAllowed(audioformat)) {
                AVLOGD("voice_conc: Failed to resume audio decoder");
                notifyListener(MEDIA_ERROR, MEDIA_ERROR_UNKNOWN, 0);
                return;
            }
        }
    }

    NuPlayer::onResume();
}

#ifndef BRINGUP_WIP
void ExtendedNuPlayer::notifyListener(int msg, int ext1, int ext2, const Parcel *in) {
   if (msg == MEDIA_PLAYBACK_COMPLETE || msg == MEDIA_ERROR) {
       AVLOGI("notifyListener %d %d %d", msg, ext1, ext2);
       if (mIsSourcePersistent) {
           static_cast<ExtendedGenericSource*>(mSource.get())->clearPersistentSource(msg);
       }
   }
   NuPlayer::notifyListener(msg, ext1, ext2, in);
}
#endif

void ExtendedNuPlayer::onSourceNotify(const sp<AMessage> &msg) {
    int32_t what;
    if (msg->findInt32("what", &what)
            && what == ExtendedHTTPLiveSource::kWhatShowEmbeddedImage
            && mSurface != NULL) {
        AVLOGV("show the embedded image");
        sp<AMessage> imageMessage;
        if (msg->findMessage("image-msg", &imageMessage)
                && imageMessage != NULL) {
            showImageInNativeWindow(imageMessage);
        }
        return;
    }
    NuPlayer::onSourceNotify(msg);
}

void ExtendedNuPlayer::showImageInNativeWindow(const sp<AMessage> &format) {
    int32_t width;
    int32_t height;
    sp<ABuffer> outBuffer;
    if (!format->findInt32("image-width", &width)
            || !format->findInt32("image-height", &height)
            || !format->findBuffer("image-buffer", &outBuffer)
            || outBuffer == NULL) {
        AVLOGE("no meta was found");
        return;
    }
#ifdef HLS_AUDIO_ONLY_IMG_DISPLAY
    AVLOGV("showImageInNativeWindow");

    int32_t err = native_window_set_usage(mSurface.get(),
            GRALLOC_USAGE_SW_READ_NEVER | GRALLOC_USAGE_SW_WRITE_OFTEN
            | GRALLOC_USAGE_HW_TEXTURE | GRALLOC_USAGE_EXTERNAL_DISP);
    if (err != 0) {
        AVLOGE("native_window_set_usage failed: %d", err);
        return;
    }
    err = native_window_set_scaling_mode(mSurface.get(),
            NATIVE_WINDOW_SCALING_MODE_SCALE_TO_WINDOW);
    if (err != 0) {
        AVLOGE("native_window_set_scaling_mode failed: %d", err);
        return;
    }
    err = native_window_set_buffers_dimensions(mSurface.get(), width,
            height);
    if (err != 0) {
        AVLOGE("native_window_set_buffers_dimensions failed: %d", err);
        return;
    }
    err = native_window_set_buffers_format(mSurface.get(),
            HAL_PIXEL_FORMAT_RGB_565);
    if (err != 0) {
        AVLOGE("native_window_set_buffers_format failed: %d", err);
        return;
    }
    android_native_rect_t crop;
    crop.left = 0;
    crop.top = 0;
    crop.right = width - 1;
    crop.bottom = height - 1;
    err = native_window_set_crop(mSurface.get(), &crop);
    if (err != 0) {
        AVLOGE("native_window_set_crop failed: %d", err);
        return;
    }
    err = native_window_set_buffers_transform(
            mSurface.get(), 0);
    if (err != 0) {
        AVLOGE("native_window_set_buffers_transform failed: %d", err);
        return;
    }
    ANativeWindowBuffer *buf;
    mSurface->getIGraphicBufferProducer()->allowAllocation(true);
    if ((err = native_window_dequeue_buffer_and_wait(mSurface.get(),
            &buf)) != 0) {
        AVLOGE("native_window_dequeue_buffer_and_wait returned error %d", err);
        buf = NULL;
        mSurface->getIGraphicBufferProducer()->allowAllocation(false);
        return;
    }
    mSurface->getIGraphicBufferProducer()->allowAllocation(false);
    AVLOGV("native window buffer: width = %d, height = %d, stride = %d",
            buf->width, buf->height, buf->stride);
    if ((size_t)(buf->width * buf->height * 2) > outBuffer->size()) {
        AVLOGE("out of image buffer");
        buf = NULL;
        return;
    }
    GraphicBufferMapper &mapper = GraphicBufferMapper::get();
    Rect bounds(width, height);
    void *dst;
    if ((err = mapper.lock(buf->handle, GRALLOC_USAGE_SW_WRITE_OFTEN,
            bounds, &dst)) != 0) {
        AVLOGE("mapper.lock failed %d", err);
        buf = NULL;
        return;
    }
    uint8_t *dst_y = (uint8_t *)dst;
    const uint8_t *src_y = (const uint8_t *)(outBuffer->data());
    for (int i = 0; i < height; i++) {
        memcpy(dst_y, src_y, width * 2);
        src_y += width * 2;
        dst_y += buf->stride * 2;
    }
    if ((err = mapper.unlock(buf->handle)) != 0) {
        AVLOGE("mapper.unlock failed %d", err);
        buf = NULL;
        return;
    }
    sp<ANativeWindow> nativeWindow = static_cast<ANativeWindow *>(mSurface.get());
    if ((err = nativeWindow->queueBuffer(mSurface.get(), buf,
            -1)) != 0) {
        AVLOGE("native window queueBuffer returned error %d", err);
        buf = NULL;
        return;
    }
    buf = NULL;
    AVLOGV("show the image in native window");
    notifyListener(MEDIA_SET_VIDEO_SIZE, width, height);
#endif // HLS_AUDIO_ONLY_IMG_DISPLAY
}

}
