<?xml version="1.0" encoding="utf-8"?>
<!--
/*
 * Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
-->
<!--
/*
**
** Copyright 2015, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
    <!-- Spoken description to let the user know that when typing in a password, they can plug in a headset in to hear spoken descriptions of the keys they type. [CHAR LIMIT=NONE] -->
    <string name="spoken_use_headphones">"Conectați un set căști-microfon pentru a auzi tastele apăsate când introduceți parola."</string>
    <!-- Spoken description for the currently entered text -->
    <string name="spoken_current_text_is">"Textul curent este %s"</string>
    <!-- Spoken description when there is no text entered -->
    <string name="spoken_no_text_entered">"Nu a fost introdus text"</string>
    <!-- Spoken description to let the user know what auto-correction will be performed when a key is pressed. An auto-correction replaces a single word with one or more words. -->
    <string name="spoken_auto_correct">"<xliff:g id="KEY_NAME">%1$s</xliff:g> corectează <xliff:g id="ORIGINAL_WORD">%2$s</xliff:g> cu <xliff:g id="CORRECTED_WORD">%3$s</xliff:g>"</string>
    <!-- Spoken description used during obscured (e.g. password) entry to let the user know that auto-correction will be performed when a key is pressed. -->
    <string name="spoken_auto_correct_obscured">"<xliff:g id="KEY_NAME">%1$s</xliff:g> efectuează corectare automată"</string>
    <!-- Spoken description of a suggestion when nothing is specified and the field is blank. -->
    <string name="spoken_empty_suggestion">"Nicio sugestie"</string>
    <!-- Spoken description for unknown keyboard keys. -->
    <string name="spoken_description_unknown">"Caracter necunoscut"</string>
    <!-- Spoken description for the "Shift" keyboard key when "Shift" is off. -->
    <string name="spoken_description_shift">"Shift"</string>
    <!-- Spoken description for the "Shift" keyboard key in symbols mode. -->
    <string name="spoken_description_symbols_shift">"Mai multe simboluri"</string>
    <!-- Spoken description for the "Shift" keyboard key when "Shift" is on. -->
    <string name="spoken_description_shift_shifted">"Shift"</string>
    <!-- Spoken description for the "Shift" keyboard key in 2nd symbols (a.k.a. symbols shift) mode. -->
    <string name="spoken_description_symbols_shift_shifted">"Simboluri"</string>
    <!-- Spoken description for the "Shift" keyboard key when "Caps lock" is on. -->
    <string name="spoken_description_caps_lock">"Shift"</string>
    <!-- Spoken description for the "Delete" keyboard key. -->
    <string name="spoken_description_delete">"Ștergeți"</string>
    <!-- Spoken description for the "To Symbol" keyboard key. -->
    <string name="spoken_description_to_symbol">"Simboluri"</string>
    <!-- Spoken description for the "To Alpha" keyboard key. -->
    <string name="spoken_description_to_alpha">"Litere"</string>
    <!-- Spoken description for the "To Numbers" keyboard key. -->
    <string name="spoken_description_to_numeric">"Cifre"</string>
    <!-- Spoken description for the "Settings" keyboard key. -->
    <string name="spoken_description_settings">"Setări"</string>
    <!-- Spoken description for the "Tab" keyboard key. -->
    <string name="spoken_description_tab">"Tab"</string>
    <!-- Spoken description for the "Space" keyboard key. -->
    <string name="spoken_description_space">"Tasta Space"</string>
    <!-- Spoken description for the "Mic" keyboard key. -->
    <string name="spoken_description_mic">"Intrare vocală"</string>
    <!-- Spoken description for the "Emoji" keyboard key. -->
    <string name="spoken_description_emoji">"Emoticonuri"</string>
    <!-- Spoken description for the "Return" keyboard key. -->
    <string name="spoken_description_return">"Return"</string>
    <!-- Spoken description for the "Search" keyboard key. -->
    <string name="spoken_description_search">"Căutați"</string>
    <!-- Spoken description for the "U+2022" (BULLET) keyboard key. -->
    <string name="spoken_description_dot">"Bulină"</string>
    <!-- Spoken description for the "Switch language" keyboard key. -->
    <string name="spoken_description_language_switch">"Schimbați limba"</string>
    <!-- Spoken description for the "Next" action keyboard key. -->
    <string name="spoken_description_action_next">"Înai."</string>
    <!-- Spoken description for the "Previous" action keyboard key. -->
    <string name="spoken_description_action_previous">"Înapoi"</string>
    <!-- Spoken feedback after turning "Shift" mode on. -->
    <string name="spoken_description_shiftmode_on">"Tasta Shift a fost activată"</string>
    <!-- Spoken feedback after turning "Caps lock" mode on. -->
    <string name="spoken_description_shiftmode_locked">"Tasta Caps Lock este activată"</string>
    <!-- Spoken feedback after changing to the symbols keyboard. -->
    <string name="spoken_description_mode_symbol">"Modul Simboluri"</string>
    <!-- Spoken feedback after changing to the 2nd symbols (a.k.a. symbols shift) keyboard. -->
    <string name="spoken_description_mode_symbol_shift">"Modul Mai multe simboluri"</string>
    <!-- Spoken feedback after changing to the alphanumeric keyboard. -->
    <string name="spoken_description_mode_alpha">"Modul Alfanumeric"</string>
    <!-- Spoken feedback after changing to the phone dialer keyboard. -->
    <string name="spoken_description_mode_phone">"Modul Telefon"</string>
    <!-- Spoken feedback after changing to the shifted phone dialer (symbols) keyboard. -->
    <string name="spoken_description_mode_phone_shift">"Modul Telefon cu simboluri"</string>
    <!-- Spoken feedback when the keyboard is hidden. -->
    <string name="announce_keyboard_hidden">"Tastatura este ascunsă"</string>
    <!-- Spoken feedback when the keyboard mode changes. -->
    <string name="announce_keyboard_mode">"Se afișează tastatura pentru <xliff:g id="KEYBOARD_MODE">%s</xliff:g>"</string>
    <!-- Description of the keyboard mode for entering dates. -->
    <string name="keyboard_mode_date">"data"</string>
    <!-- Description of the keyboard mode for entering dates and times. -->
    <string name="keyboard_mode_date_time">"date și ore"</string>
    <!-- Description of the keyboard mode for entering email addresses. -->
    <string name="keyboard_mode_email">"adrese de e-mail"</string>
    <!-- Description of the keyboard mode for entering text messages. -->
    <string name="keyboard_mode_im">"mesaje"</string>
    <!-- Description of the keyboard mode for entering numbers. -->
    <string name="keyboard_mode_number">"numere"</string>
    <!-- Description of the keyboard mode for entering phone numbers. -->
    <string name="keyboard_mode_phone">"telefoane"</string>
    <!-- Description of the keyboard mode for entering generic text. -->
    <string name="keyboard_mode_text">"text"</string>
    <!-- Description of the keyboard mode for entering times. -->
    <string name="keyboard_mode_time">"ore"</string>
    <!-- Description of the keyboard mode for entering URLs. -->
    <string name="keyboard_mode_url">"adrese URL"</string>
    <!-- Description of the emoji category icon of Recents. -->
    <string name="spoken_descrption_emoji_category_recents">"Recente"</string>
    <!-- Description of the emoji category icon of People. -->
    <string name="spoken_descrption_emoji_category_people">"Persoane"</string>
    <!-- Description of the emoji category icon of Objects. -->
    <string name="spoken_descrption_emoji_category_objects">"Obiecte"</string>
    <!-- Description of the emoji category icon of Nature. -->
    <string name="spoken_descrption_emoji_category_nature">"Natură"</string>
    <!-- Description of the emoji category icon of Places. -->
    <string name="spoken_descrption_emoji_category_places">"Locații"</string>
    <!-- Description of the emoji category icon of Symbols. -->
    <string name="spoken_descrption_emoji_category_symbols">"Simboluri"</string>
    <!-- Description of the emoji category icon of Flags. -->
    <string name="spoken_descrption_emoji_category_flags">"Steaguri"</string>
    <!-- Description of the emoji category icon of Smiley & People. -->
    <string name="spoken_descrption_emoji_category_eight_smiley_people">"Smiley-uri şi persoane"</string>
    <!-- Description of the emoji category icon of Animals & Nature. -->
    <string name="spoken_descrption_emoji_category_eight_animals_nature">"Animale şi natură"</string>
    <!-- Description of the emoji category icon of Food & Drink. -->
    <string name="spoken_descrption_emoji_category_eight_food_drink">"Alimente şi băuturi"</string>
    <!-- Description of the emoji category icon of Travel & Places. -->
    <string name="spoken_descrption_emoji_category_eight_travel_places">"Transport şi locuri"</string>
    <!-- Description of the emoji category icon of Activity. -->
    <string name="spoken_descrption_emoji_category_eight_activity">"Activitate"</string>
    <!-- Description of the emoji category icon of Emoticons. -->
    <string name="spoken_descrption_emoji_category_emoticons">"Emoticonuri"</string>
    <!-- Description of an upper case letter of LOWER_LETTER. -->
    <string name="spoken_description_upper_case">"<xliff:g id="LOWER_LETTER">%s</xliff:g> mare"</string>
    <!-- Spoken description for Unicode code point U+0049: "I" LATIN CAPITAL LETTER I
         Note that depending on locale, the lower-case of this letter is U+0069 or U+0131. -->
    <string name="spoken_letter_0049">"I mare"</string>
    <!-- Spoken description for Unicode code point U+0130: "İ" LATIN CAPITAL LETTER I WITH DOT ABOVE
         Note that depending on locale, the lower-case of this letter is U+0069 or U+0131. -->
    <string name="spoken_letter_0130">"I mare, punct deasupra"</string>
    <!-- Spoken description for unknown symbol code point. -->
    <string name="spoken_symbol_unknown">"Simbol necunoscut"</string>
    <!-- Spoken description for unknown emoji code point. -->
    <string name="spoken_emoji_unknown">"Emoji necunoscut"</string>
    <!-- Spoken description for emoticons ":-!". -->
    <string name="spoken_emoticon_3A_2D_21_20">"Față plictisită"</string>
    <!-- Spoken description for emoticons ":-$". -->
    <string name="spoken_emoticon_3A_2D_24_20">"Față jenată"</string>
    <!-- Spoken description for emoticons "B-)". -->
    <string name="spoken_emoticon_42_2D_29_20">"Față cu ochelari de soare"</string>
    <!-- Spoken description for emoticons ":O". -->
    <string name="spoken_emoticon_3A_4F_20">"Față surprinsă"</string>
    <!-- Spoken description for emoticons ":-*". -->
    <string name="spoken_emoticon_3A_2D_2A_20">"Față care sărută"</string>
    <!-- Spoken description for emoticons ":-[". -->
    <string name="spoken_emoticon_3A_2D_5B_20">"Față încruntată"</string>
    <!-- Spoken descriptions when opening a more keys keyboard that has alternative characters. -->
    <string name="spoken_open_more_keys_keyboard">"Sunt disponibile caractere alternative"</string>
    <!-- Spoken descriptions when closing a more keys keyboard that has alternative characters. -->
    <string name="spoken_close_more_keys_keyboard">"S-au închis caracterele alternative"</string>
    <!-- Spoken descriptions when opening a more suggestions panel that has alternative suggested words. -->
    <string name="spoken_open_more_suggestions">"Sunt disponibile sugestii alternative"</string>
    <!-- Spoken descriptions when closing a more suggestions panel that has alternative suggested words. -->
    <string name="spoken_close_more_suggestions">"S-au închis sugestiile alternative"</string></resources>