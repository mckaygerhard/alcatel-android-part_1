<?xml version="1.0" encoding="utf-8"?>
<!--
/*
 * Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
-->
<!--
/**
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
-->
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
  <!-- General provisioning strings used for both device owner and work profile provisioning -->
  <!-- Title of the provisioning error dialog. [CHAR LIMIT=45] -->
  <string name="provisioning_error_title">"¡Vaya!"</string>
  <!-- Work profile provisioning flow UI. -->
  <!-- User consent -->
  <!-- Title of all work profile setup screens. [CHAR LIMIT=NONE] -->
  <string name="setup_work_profile">"Configurar el perfil"</string>
  <!-- The text that asks for user consent to create a work profile [CHAR LIMIT=NONE] -->
  <string name="company_controls_workspace">"Tu organización controla este perfil y mantiene su seguridad. Tú controlas el resto del contenido del dispositivo."</string>
  <!-- The text that asks for user consent to set up a work device [CHAR LIMIT=NONE] -->
  <string name="company_controls_device">"Tu organización controlará este dispositivo y mantendrá su seguridad."</string>
  <!-- String shown above the icon of the app that will control the work profile after it is set up. [CHAR LIMIT=NONE]-->
  <string name="the_following_is_your_mdm">"Esta aplicación necesitará acceder a tu perfil:"</string>
  <!-- String shown above the icon of the app that will control the work device after it is set up. [CHAR LIMIT=NONE]-->
  <string name="the_following_is_your_mdm_for_device">"Esta aplicación administrará tu dispositivo:"</string>
  <!-- String for positive button on user consent dialog for creating a profile [CHAR LIMIT=NONE] -->
  <string name="set_up">"Configurar"</string>
  <!-- Progress text shown when setting up work profile [CHAR LIMIT=NONE] -->
  <string name="setting_up_workspace">"Configurando tu perfil de trabajo\u2026"</string>
  <!-- Text in a pop-up that asks the user to confirm that they allow the mobile device management app to take control of the profile they are creating. [CHAR LIMIT=NONE]-->
  <string name="admin_has_ability_to_monitor_profile">"Tu administrador puede controlar y administrar los ajustes, el acceso corporativo, las aplicaciones, los permisos y los datos asociados al perfil, como la información sobre su ubicación y la actividad de red."</string>
  <!-- Text in a pop-up that asks the user to confirm that they allow the mobile device management app to take control of the device. [CHAR LIMIT=NONE]-->
  <string name="admin_has_ability_to_monitor_device">"Tu administrador puede controlar y administrar los ajustes, el acceso corporativo, las aplicaciones, los permisos y los datos asociados a este dispositivo, como la información sobre su ubicación y la actividad de red."</string>
  <!-- Text in a pop-up that asks the user to confirm that theft protection will be disabled. [CHAR LIMIT=NONE]-->
  <string name="theft_protection_disabled_warning">"Si continúas, las funciones de protección contra robo no se habilitarán. Para activarlas, cancela esta acción y configura este dispositivo como personal."</string>
  <!-- String telling the user how to get more information about their work profile-->
  <string name="contact_your_admin_for_more_info">"Para obtener más información, incluidas las políticas de privacidad de tu organización, ponte en contacto con el administrador."</string>
  <!-- Text on the link that will forward the user to a website with more information about work profiles [CHAR LIMIT=30]-->
  <string name="learn_more_link">"Más información"</string>
  <!-- Text on negative button in a popup shown to the user to confirm that they want to start setting up a work profile [CHAR LIMIT=15]-->
  <string name="cancel_setup">"CANCELAR"</string>
  <!-- Text on positive button in a popup shown to the user to confirm that they want to start setting up a work profile [CHAR LIMIT=15]-->
  <string name="ok_setup">"Aceptar"</string>
  <!-- Default profile name used for the creation of work profiles. [CHAR LIMIT=NONE] -->
  <string name="default_managed_profile_name">"Perfil de trabajo"</string>
  <!-- Title on the dialog that prompts the user to confirm that they really want to delete their existing work profile-->
  <string name="delete_profile_title">"¿Eliminar perfil de trabajo?"</string>
  <!-- Opening string on the dialog that prompts the user to confirm that they really want to delete
       their existing work profile. The administration app icon and name appear after the final
       colon. [CHAR LIMIT=NONE] -->
  <string name="opening_paragraph_delete_profile_unknown_company">"Ya existe un perfil de trabajo, que está administrado por:"</string>
    <!-- Opening string on the dialog that prompts the user to confirm that they really want to delete
         their existing work profile. The administration app icon and name appear after the final
         colon, the %s is replaced by the domain name of the organization that owns the work
         profile. [CHAR LIMIT=NONE] -->
  <string name="opening_paragraph_delete_profile_known_company">"Este perfil de trabajo está administrado para %s con:"</string>
  <!-- String on the dialog that links through to more information about the profile management application. -->
  <string name="read_more_delete_profile">"Para continuar, "<a href="#read_this_link">"consulta esta información"</a>"."</string>
  <!-- String on the dialog that prompts the user to confirm that they really want to delete their existing work profile-->
  <string name="sure_you_want_to_delete_profile">"Si continúas, se eliminarán las aplicaciones y los datos de este perfil."</string>
  <!-- String on the button that triggers the deletion of a work profile.-->
  <string name="delete_profile">"ELIMINAR"</string>
  <!-- String on the button that cancels out of the deletion of a work profile.-->
  <string name="cancel_delete_profile">"CANCELAR"</string>
  <!-- String shown on prompt for user to encrypt and reboot the device, in case of profile setup [CHAR LIMIT=NONE]-->
  <string name="encrypt_device_text_for_profile_owner_setup">"Para seguir configurando tu perfil de trabajo, tendrás que cifrar el dispositivo. Este proceso puede tardar unos minutos."</string>
  <!-- String shown on prompt for user to encrypt and reboot the device, in case of device setup [CHAR LIMIT=NONE]-->
  <string name="encrypt_device_text_for_device_owner_setup">"Para seguir configurando el dispositivo, tendrás que cifrarlo. Este proceso puede tardar unos minutos."</string>
  <!-- String shown on button for user to cancel setup when asked to encrypt device. [CHAR LIMIT=20]-->
  <string name="encrypt_device_cancel">"CANCELAR"</string>
  <!-- String shown on button for user to continue to settings to encrypt the device. [CHAR LIMIT=20]-->
  <string name="encrypt_device_launch_settings">"CIFRAR"</string>
  <!-- Title for reminder notification to resume provisioning after encryption [CHAR LIMIT=30] -->
  <string name="continue_provisioning_notify_title">"Cifrado completado"</string>
  <!-- Body for reminder notification to resume provisioning after encryption [CHAR LIMIT=NONE] -->
  <string name="continue_provisioning_notify_text">"Toca para continuar la configuración del perfil de trabajo"</string>
  <!-- Explains the failure and what to do to next. [CHAR LIMIT=NONE] -->
  <string name="managed_provisioning_error_text">"No se ha podido configurar el perfil de trabajo. Ponte en contacto con el departamento de TI o vuelve a intentarlo más tarde."</string>
  <!-- Error string displayed if this device doesn't support work profiles. -->
  <string name="managed_provisioning_not_supported">"Tu dispositivo no admite perfiles de trabajo"</string>
  <!-- Error string displayed if the user that initiated the provisioning is not the user owner. -->
  <string name="user_is_not_owner">"El usuario principal del dispositivo debe configurar los perfiles de trabajo"</string>
  <!-- Error string displayed if provisioning was initiated on a device with a Device Owner -->
  <string name="device_owner_exists">"Los perfiles de trabajo no se pueden configurar en un dispositivo administrado"</string>
  <!-- Error string displayed if maximum user limit is reached -->
  <string name="maximum_user_limit_reached">"No se puede crear el perfil de trabajo porque se ha alcanzado el número máximo de usuarios del dispositivo. Elimina uno o varios usuarios y vuelve a intentarlo."</string>
  <!-- Error string displayed if the selected launcher doesn't support work profiles. -->
  <string name="managed_provisioning_not_supported_by_launcher">"Esta aplicación de launcher no admite tu perfil de trabajo. Debes utilizar un launcher que sea compatible."</string>
  <!-- Button text for the button that cancels provisioning  -->
  <string name="cancel_provisioning">"CANCELAR"</string>
  <!-- Button text for the button that opens the launcher picker  -->
  <string name="pick_launcher">"ACEPTAR"</string>
  <!-- Device owner provisioning flow UI. -->
  <!-- Default username for the user of the owned device. [CHAR LIMIT=45] -->
  <string name="default_owned_device_username">"Usuario del dispositivo de trabajo"</string>
  <!-- Title of the work device setup screen. [CHAR LIMIT=NONE] -->
  <string name="setup_work_device">"Configurar el dispositivo"</string>
  <!-- Progress text indicating that the setup data is processed. [CHAR LIMIT=45] -->
  <string name="progress_data_process">"Procesando datos de configuración\u2026"</string>
  <!-- Progress text indicating that wifi is set up. [CHAR LIMIT=45] -->
  <string name="progress_connect_to_wifi">"Conectando a red Wi-Fi\u2026"</string>
  <!-- Progress text indicating that the device admin package is being downloaded. [CHAR LIMIT=45] -->
  <string name="progress_download">"Descargando aplicación de administración\u2026"</string>
  <!-- Progress text indicating that the device admin package is being installed. [CHAR LIMIT=45] -->
  <string name="progress_install">"Instalando aplicación de administración\u2026"</string>
  <!-- Progress text indicating that the device admin package is set as ownder. [CHAR LIMIT=45] -->
  <string name="progress_set_owner">"Estableciendo propietario del dispositivo\u2026"</string>
  <!-- Message of the cancel dialog. [CHAR LIMIT=NONE] -->
  <string name="device_owner_cancel_message">"¿Quieres detener la configuración y borrar los datos del dispositivo?"</string>
  <!-- Cancel button text of the cancel dialog. [CHAR LIMIT=45] -->
  <string name="device_owner_cancel_cancel">"CANCELAR"</string>
  <!-- OK button text of the error dialog. [CHAR LIMIT=45] -->
  <string name="device_owner_error_ok">"Aceptar"</string>
  <!-- Reset button text of the error dialog. [CHAR LIMIT=45] -->
  <string name="device_owner_error_reset">"RESTABLECER"</string>
  <!-- Message of the error dialog in case of an unspecified error. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_general">"No se ha podido configurar el dispositivo. Ponte en contacto con el departamento de TI."</string>
  <!-- Message of the error dialog when already provisioned. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_already_provisioned">"Este dispositivo ya está configurado."</string>
  <!-- Message of the error dialog when a secondary user has already been provisioned. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_already_provisioned_user">"Este dispositivo ya está configurado para este usuario."</string>
  <!-- Message of the error dialog when setting up wifi failed. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_wifi">"No se ha podido establecer conexión con la red Wi-Fi"</string>
  <!-- Message of the error dialog when passing factory reset protection fails. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_frp">"Este dispositivo está bloqueado con la protección para restablecer datos de fábrica. Ponte en contacto con el departamento de TI."</string>
  <!-- Message informing the user that the factory reset protection partition is being erased [CHAR LIMIT=30] -->
  <string name="frp_clear_progress_title">"Borrando"</string>
  <!-- Progress screen text shown while erasing the factory reset protection partition [CHAR LIMIT=75] -->
  <string name="frp_clear_progress_text">"Espera..."</string>
  <!-- Message of the error dialog when data integrity check fails. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_hash_mismatch">"No se ha podido utilizar la aplicación de administración debido a un error de la suma de comprobación. Ponte en contacto con el departamento de TI."</string>
  <!-- Message of the error dialog when device owner apk could not be downloaded. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_download_failed">"No se ha podido descargar la aplicación de administración"</string>
  <!-- Message of the error dialog when package to install is invalid. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_package_invalid">"No se puede utilizar la aplicación de administración porque faltan componentes o está dañada. Ponte en contacto con el administrador de TI."</string>
  <!-- Message of the error dialog when the name of the admin package to be installed is invalid. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_package_name_invalid">"No se puede instalar la aplicación de administración porque el nombre del paquete no es válido. Ponte en contacto con el departamento de TI."</string>
  <!-- Message of the error dialog when package could not be installed. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_installation_failed">"No se ha podido instalar la aplicación de administración"</string>
  <!-- Message of the error dialog when device admin package is not installed. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_package_not_installed">"La aplicación de administración no está instalada en tu dispositivo"</string>
  <!-- Message of the cancel dialog. [CHAR LIMIT=NONE] -->
  <string name="profile_owner_cancel_message">"¿Detener configuración?"</string>
  <!-- Cancel button text of the cancel dialog. [CHAR LIMIT=45] -->
  <string name="profile_owner_cancel_cancel">"NO"</string>
  <!-- OK button text of the error dialog. [CHAR LIMIT=45] -->
  <string name="profile_owner_cancel_ok">"SÍ"</string>
  <!-- Message of the cancel progress dialog. [CHAR LIMIT=45] -->
  <string name="profile_owner_cancelling">"Cancelando..."</string>
  <!-- Title of the work profile setup cancel dialog. [CHAR LIMIT=45] -->
  <string name="work_profile_setup_later_title">"¿Detener la configuración del perfil?"</string>
  <!-- Message of the work profile setup cancel dialog. [CHAR LIMIT=NONE] -->
  <string name="work_profile_setup_later_message">"Puedes configurar tu perfil de trabajo más tarde con la aplicación de administración de dispositivos que utilice tu organización."</string>
  <!-- Cancel button text of the work profile setup cancel dialog. [CHAR LIMIT=45] -->
  <string name="work_profile_setup_continue">"CONTINUAR"</string>
  <!-- OK button text of the work profile setup cancel dialog. [CHAR LIMIT=45] -->
  <string name="work_profile_setup_stop">"DETENER"</string>
  <!-- CertService -->
  <!-- Title of a status bar notification for a service which installs CA certificates to secondary users. [CHAR LIMIT=45] -->
  <string name="provisioning">"Administrando"</string>
  <!-- Content of a status bar notification for a service which installs CA certificates to secondary users. [CHAR LIMIT=45] -->
  <string name="copying_certs">"Configurando certificados de CA"</string>
  <!-- Accessibility Descriptions -->
  <!-- Accessibility description for the profile setup screen showing organization info. [CHAR LIMIT=NONE] -->
  <string name="setup_profile_start_setup">"Configura tu perfil. Iniciar configuración"</string>
  <!-- Accessibility description for the profile setup screen showing encryption info. [CHAR LIMIT=NONE] -->
  <string name="setup_profile_encryption">"Configura tu perfil. Cifrado"</string>
  <!-- Accessibility description for the profile setup screen showing progress. [CHAR LIMIT=NONE] -->
  <string name="setup_profile_progress">"Configura tu perfil. Mostrando progreso"</string>
  <!-- Accessibility description for the device setup screen showing company info. [CHAR LIMIT=NONE] -->
  <string name="setup_device_start_setup">"Configura tu dispositivo. Iniciar configuración"</string>
  <!-- Accessibility description for the device setup screen showing encryption info. [CHAR LIMIT=NONE] -->
  <string name="setup_device_encryption">"Configura tu dispositivo. Cifrado"</string>
  <!-- Accessibility description for the device setup screen showing progress. [CHAR LIMIT=NONE] -->
  <string name="setup_device_progress">"Configura tu dispositivo. Mostrando progreso"</string>
  <!-- Accessibility description for the learn more link in user consent dialog. [CHAR LIMIT=NONE] -->
  <string name="learn_more_label">"Botón para obtener más información"</string>
  <!-- Accessibility description for the company icon. [CHAR LIMIT=NONE] -->
  <string name="mdm_icon_label">"Icono de <xliff:g id="ICON_LABEL">%1$s</xliff:g>"</string></resources>