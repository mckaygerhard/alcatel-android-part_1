/**
 * Copyright (c) 2012-2013, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */

package com.qualcomm.wallpaper;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

public class WallpaperActivity extends Activity {
    private final static String TREBUCHET_PACKAGE = "com.android.launcher3";
    private final static String TREBUCHET_WALLPAPER_ACTIVITY =
            "com.android.launcher3.LauncherWallpaperPickerActivity";
    private final static String LAUNCHER2_PACKAGE = "com.android.launcher";
    private final static String LAUNCHER2_WALLPAPER_ACTIVITY =
            "com.android.launcher2.WallpaperChooser";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Intent intent = new Intent();
            intent.setClassName(TREBUCHET_PACKAGE, TREBUCHET_WALLPAPER_ACTIVITY);
            intent.putExtra("packagename", getPackageName());
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException e) {
            try {
                Intent intent = new Intent();
                intent.setClassName(LAUNCHER2_PACKAGE, LAUNCHER2_WALLPAPER_ACTIVITY);
                intent.putExtra("packagename", getPackageName());
                startActivityForResult(intent, 0);
            } catch (ActivityNotFoundException ne) {
                Toast.makeText(this, R.string.cant_launch, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK || data == null) {
            finish();
            return;
        }

        try {
            int resId = data.getIntExtra("resid", R.drawable.wallpaper_1);
            WallpaperManager wpm = (WallpaperManager) getSystemService(Context.WALLPAPER_SERVICE);
            wpm.setResource(resId);
        } catch (IOException e) {
            Log.e("WallpaperActivity", "Failed to set wallpaper: " + e);
        } finally {
            finish();
        }
    }

}
