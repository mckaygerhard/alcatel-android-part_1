LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_PACKAGE_NAME := ZeroBalanceHelper
LOCAL_MODULE_PATH := $(TARGET_OUT)/priv-app
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
LOCAL_DEX_PREOPT := nostripping
include $(BUILD_PACKAGE)
