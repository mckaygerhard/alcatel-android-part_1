/**
 * Copyright (c) 2015 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

package com.qualcomm.qti.loadcarrier;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Settings;
import android.util.Log;

import com.android.internal.telephony.TelephonyIntents;

public class TriggerApplication extends Application {
    private static final String TAG = "TriggerApplication";

    private static final String ACTION_PHONE_READY = "com.android.phone.ACTION_PHONE_READY";
    private static final String INTENTFILTER_SCHEME = "android_secret_code";

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            unregisterReceiver(mReceiver);
            if (Utils.DEBUG) Log.i(TAG, "TriggerApplication onReceive. intent.getAction()="
                    + intent.getAction());
            // If the trigger function has been enabled, we will start the service.
            boolean defaultValue = context.getResources().getBoolean(R.bool.trigger_enabled);
            String currentCarrierName = Utils.getCurrentCarriersName(Utils.getCurrentCarriers());
            if (Utils.getValue(Utils.PROP_KEY_TRIGGER, defaultValue)
                    && (Utils.getValue(Utils.PROP_KEY_TRIGGER_FIRST_INSERT, false) == false
                            || (Utils.getValue(Utils.PROP_KEY_TRIGGER_FIRST_INSERT, false)
                                    && currentCarrierName.equals("Default")))
                    && Settings.Global.getInt(context.getContentResolver(),
                            Settings.Global.AIRPLANE_MODE_ON, 0) == 0) {
                if (Utils.DEBUG) Log.i(TAG, "Start the trigger service.");
                context.startService(new Intent(context, TriggerService.class));
            }
        }
    };

    BroadcastReceiver mTriggerActionReceiver = new TriggerActionReceiver();

    @Override
    public void onCreate() {
        super.onCreate();

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_PHONE_READY);
        filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        filter.setPriority(1000);
        registerReceiver(mReceiver, filter);
        Log.d(TAG, "register receiver to start TriggerService");

        filter = new IntentFilter();
        filter.addAction(TelephonyIntents.SECRET_CODE_ACTION);
        filter.addDataScheme(INTENTFILTER_SCHEME);
        filter.addDataAuthority(TriggerActionReceiver.HOST_TRIGGER_Y, null);
        filter.addDataAuthority(TriggerActionReceiver.HOST_TRIGGER_N, null);
        filter.addDataAuthority(TriggerActionReceiver.HOST_TRIGGER_VALUE, null);
        filter.addDataAuthority(TriggerActionReceiver.HOST_TRIGGER_START, null);
        filter.addDataAuthority(TriggerActionReceiver.HOST_FILE_BASED_Y, null);
        filter.addDataAuthority(TriggerActionReceiver.HOST_FILE_BASED_N, null);
        filter.addDataAuthority(TriggerActionReceiver.HOST_BLOCK_BASED_Y, null);
        filter.addDataAuthority(TriggerActionReceiver.HOST_BLOCK_BASED_N, null);
        filter.setPriority(1000);
        registerReceiver(mTriggerActionReceiver, filter);
        filter = new IntentFilter();
        filter.addAction(TriggerActionReceiver.ACTION_CARRIER_PREFERRED_SIM_SWAPPED);
        filter.setPriority(1000);
        registerReceiver(mTriggerActionReceiver, filter);
        Log.d(TAG, "register TriggerActionReceiver");
    }
}
