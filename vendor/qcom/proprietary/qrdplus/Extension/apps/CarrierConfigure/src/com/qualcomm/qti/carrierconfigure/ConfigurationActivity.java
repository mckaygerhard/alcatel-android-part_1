/**
 * Copyright (c) 2014, 2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

package com.qualcomm.qti.carrierconfigure;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class ConfigurationActivity extends Activity {
    private static final String KEY_NO_TITILE = "no_title";
    private static final String KEY_VISIBLE_CONTAINER_ID = "visible_container_id";

    private boolean mNoTitle = false;
    private int mVisibleContainerId = -1;

    private RadioPreferenceFragment mFragment = null;
    public  static ArrayList<Activity> activities = new ArrayList<Activity>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initFragment();
        if (savedInstanceState != null) {
            mNoTitle = savedInstanceState.getBoolean(KEY_NO_TITILE, false);
            mVisibleContainerId = savedInstanceState.getInt(KEY_VISIBLE_CONTAINER_ID, -1);
        }

        if (mVisibleContainerId < 0) {
            finish();
            return;
        }

        if (mNoTitle) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        }

        try {
            ViewConfiguration mconfig = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(mconfig, false);
            }
        } catch (Exception ex) {
        }
        setContentView(R.layout.configuration_activity);
        addActivity((Activity)this);
        findViewById(mVisibleContainerId).setVisibility(View.VISIBLE);
    }

    private void addActivity(Activity activity) {
        activities.add(activity);
    }

    public static void onDestroyfinish() {
        for (Activity activity : activities) {
            activity.finish();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(KEY_NO_TITILE, mNoTitle);
        outState.putInt(KEY_VISIBLE_CONTAINER_ID, mVisibleContainerId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.carrierconfig_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mFragment != null) {
            switch (item.getItemId()) {
                case R.id.standard_view:
                    mFragment.onDisplayModeChanged(Utils.STANDARD_VIEW);
                    return true;
                case R.id.hierarchy_view:
                    mFragment.onDisplayModeChanged(Utils.HIERARCHY_VIEW);
                    return true;
                default:
                    break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void initFragment() {
        String action = getIntent().getAction();
        if (Intent.ACTION_MAIN.equals(action)) {
            if (getIntent().hasCategory(StartActivityReceiver.CODE_SPEC_SWITCH_L)) {
                getFragmentManager().beginTransaction()
                        .add(R.id.first, new MultiSimConfigFragmentL())
                        .commit();
                getFragmentManager().beginTransaction()
                        .add(R.id.second, new CarrierConfigFragmentL())
                        .commit();
            } else {
                getFragmentManager().beginTransaction()
                        .add(R.id.first, new MultiSimConfigFragment())
                        .commit();
                mFragment = new CarrierConfigFragment();
                getFragmentManager().beginTransaction()
                        .add(R.id.second, mFragment)
                        .commit();
            }

            mNoTitle = false;
            mVisibleContainerId = R.id.two_fragment;
        } else if (Utils.ACTION_CARRIER_RESET.equals(action)){
            getFragmentManager().beginTransaction()
                    .add(R.id.one_fragment, CarrierResetFragment.newInstance())
                    .commit();

            mNoTitle = false;
            mVisibleContainerId = R.id.one_fragment;
        } else {
            ArrayList<Carrier> list =
                    getIntent().getParcelableArrayListExtra(Utils.EXTRA_CARRIER_LIST);
            if (list == null || list.size() < 1) return;

            if (Utils.ACTION_TRIGGER_WELCOME.equals(action)) {
                getFragmentManager().beginTransaction()
                        .add(R.id.one_fragment, TriggerWelcomeFragment.newInstance(list))
                        .commit();
            } else if (Utils.ACTION_TRIGGER_START.equals(action)) {
                getFragmentManager().beginTransaction()
                           .add(R.id.one_fragment,TriggerStartFragment.newInstance(list))
                           .commit();
            } else if (Utils.ACTION_TRIGGER_REBOOT.equals(action)) {
                getFragmentManager().beginTransaction()
                           .add(R.id.one_fragment,TriggerRebootFragment.newInstance(list))
                           .commit();
            } else if (Utils.ACTION_TRIGGER_CONFIGUAGE_LATER.equals(action)) {
                getFragmentManager().beginTransaction()
                           .add(R.id.one_fragment,TriggerConfigureLaterFragment.newInstance(list))
                           .commit();
            }
            mNoTitle = true;
            mVisibleContainerId = R.id.one_fragment;
        }
    }

}
