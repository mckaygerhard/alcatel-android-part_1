
PRODUCT_PACKAGES += \
    DeviceInfo \
    init.carrier.rc \
    SimContacts \
    qcril.db \
    libapkscanner \
    PhoneFeatures \
    com.qrd.wappush \
    com.qrd.wappush.xml \
    wrapper-updater \
    CarrierConfigure \
    CarrierLoadService \
    CarrierCacheService \
    NotificationService \
    TouchPal_Global \
    ArabicPack \
    BengaliPack \
    CangjiePack \
    ChtPack \
    HindiPack \
    IndonesianPack \
    MarathiPack \
    PortuguesebrPack \
    RussianPack \
    SpanishLatinPack \
    TagalogPack \
    TamilPack \
    TeluguPack \
    ThaiPack \
    VietnamPack \
    ProfileMgr \
    ExtSettings \
    TimerSwitch \
    QSService \
    PowerOffHandler \
    PowerOffHandlerApp \
    QTITaskManager \
    smartsearch \
    com.qualcomm.qti.smartsearch.xml \
    Firewall \
    LauncherUnreadService \
    NetworkControl \
    NotePad2 \
    PowerOnAlert \
    StorageCleaner \
    UsbSecurity \
    ProfileMgr \
    BatterySaver \
    CalendarWidget \
    LunarInfoProvider \
    ZeroBalanceHelper \
    libdatactrl \
    Setup_Wizard

PRODUCT_PACKAGE_OVERLAYS += vendor/qcom/proprietary/qrdplus/Extension/apps/BatterySaver/overlay
