# Add LatamClaro Apps
PRODUCT_PACKAGES += \
    LatamClaroPeruFrameworksRes \
    LatamClaroPeruEmailRes \
    LatamClaroPeruBrowserRes \
    LatamClaroPeruStkRes \
    LatamClaroPeruTelephonyCarrierPackRes \
    LatamClaroPeruMmsRes \
    LatamClaroPeruLauncherRes \
    LatamClaroPeruSimContactsRes \
    LatamClaroPeruContactsRes \
    LatamClaroPeruTeleServiceRes \
    LatamClaroPeruConfigurationClientRes \
    LatamClaroPeruOmaDownloadRes \
    LatamClaroPeruSettingsRes \
    LatamClaroPeruCellBroadcastReceiverRes


#Product properties
PRODUCT_BRAND := Claro
PRODUCT_LOCALES := en_US es_US
