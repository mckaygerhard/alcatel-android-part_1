PRODUCT_PACKAGES += \
    VodafoneUKBrowserRes \
    VodafoneUKContactsRes \
    VodafoneUKDialerRes \
    VodafoneUKMmsRes \
    VodafoneUKNetworkSettingRes \
    VodafoneUKFrameworksRes \
    VodafoneUKPhoneCommonRes \
    VodafoneUKSettingsProviderRes \
    VodafoneUKSystemUIRes \
    VodafoneUKTeleServiceRes \
    VodafoneUKLatinIMERes \
    VodafoneUKEmailRes \
    VodafoneUKSimContactsRes \
    VodafoneUKOmaDownloadRes \
    VodafoneUKSettingsRes
