PRODUCT_PACKAGES += \
      mcfg_sw.mbn \
     .preloadspec \
     vendor.prop \
    VodafonePTBrowserRes \
    VodafonePTContactsRes \
    VodafonePTDialerRes \
    VodafonePTMmsRes \
    VodafonePTNetworkSettingRes \
    VodafonePTFrameworksRes \
    VodafonePTPhoneCommonRes \
    VodafonePTSettingsRes \
    VodafonePTSettingsProviderRes \
    VodafonePTSystemUIRes \
    VodafonePTTeleServiceRes \
    VodafonePTLatinIMERes \
    VodafonePTEmailRes \
    VodafonePTSimContactsRes \
    VodafonePTOmaDownloadRes \
    VodafonePTStkRes \
    VodafonePTWallPaperRes \
    VodafonePTCellBroadcastReceiverRes
