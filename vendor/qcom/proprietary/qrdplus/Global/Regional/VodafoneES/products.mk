PRODUCT_PACKAGES += \
     mcfg_sw.mbn \
     .preloadspec \
     vendor.prop \
    VodafoneESBrowserRes \
    VodafoneESContactsRes \
    VodafoneESDialerRes \
    VodafoneESMmsRes \
    VodafoneESFrameworksRes \
    VodafoneESPhoneCommonRes \
    VodafoneESSettingsRes \
    VodafoneESSettingsProviderRes \
    VodafoneESSystemUIRes \
    VodafoneESTeleServiceRes \
    VodafoneESLatinIMERes \
    VodafoneESEmailRes \
    VodafoneESSimContactsRes \
    VodafoneESOmaDownloadRes \
    VodafoneESWallPaperRes \
    VodafoneESCellBroadcastReceiverRes \
    VodafoneESStkRes \
    VodafoneESNetworkSettingRes
