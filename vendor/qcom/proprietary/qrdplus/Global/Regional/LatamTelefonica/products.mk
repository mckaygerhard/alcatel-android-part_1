PRODUCT_PACKAGES += \
    mcfg_sw.mbn \
    LatamTelefonicaContactsRes \
    LatamTelefonicaMmsRes \
    LatamTelefonicaFrameworksRes \
    LatamTelefonicaSettingsProviderRes \
    LatamTelefonicaTeleServiceRes \
    LatamTelefonicaStkRes \
    LatamTelefonicaPhoneFeaturesRes \
    LatamTelefonicaSimContactsRes \
    LatamTelefonicaSettingsRes \
    LatamTelefonicaSystemUIRes \
    MobileUpdateClient \
    libdme_main \
    LatamTelefonicaCellBroadcastReceiverRes
