# Add PhilippinesOpenMarket Apps
PRODUCT_PACKAGES += \
    PhilippinesOpenMarketFrameworksRes \
    PhilippinesOpenMarketContactsRes \
    PhilippinesOpenMarketMmsRes \
    PhilippinesOpenMarketSettingsProviderRes \
    PhilippinesOpenMarketFMRecordRes \
    PhilippinesOpenMarketLatinIMERes \
    PhilippinesOpenMarketSoundRecorderRes \
    PhilippinesOpenMarketSettingsRes \
    PhilippinesOpenMarketTeleServiceRes \
    PhilippinesOpenMarketPhoneFeaturesRes \
    PhilippinesOpenMarketFM2Res \
    PhilippinesOpenMarketSystemUIRes
