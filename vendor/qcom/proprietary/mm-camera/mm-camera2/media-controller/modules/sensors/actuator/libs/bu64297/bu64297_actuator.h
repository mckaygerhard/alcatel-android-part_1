/**
 * rohm_bu64297gwz_actuator.h
 *
 * DESCRIPTION
 *  Auto-Generated by Actuator tool.
 *
 * Copyright (c) 2015 Qualcomm Technologies, Inc. All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */
  {
    .actuator_params =
    {
      .module_name = "sunny_vcm",
      .actuator_name = "bu64297",
      .i2c_addr = 0x18,
      .i2c_data_type = CAMERA_I2C_BYTE_DATA,
      .i2c_addr_type = CAMERA_I2C_BYTE_ADDR,
      .act_type = ACTUATOR_TYPE_VCM,
      .data_size = 10,
      .reg_tbl =
      {
        .reg_tbl_size = 1,
        .reg_params =
        {
          {
            .reg_write_type = ACTUATOR_WRITE_DAC,
            .hw_mask = 0x0000C400,
            .reg_addr = 0xC000,
            .hw_shift = 0,
            .data_shift = 0,
          },
        },
      },
      .init_setting_size = 24,
      .init_settings =
      {
        {0xc2, CAMERA_I2C_BYTE_ADDR, 0x00, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        /* Q-fact=7-9 */
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0xB1, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE8, CAMERA_I2C_BYTE_ADDR, 0xC3, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE8, CAMERA_I2C_BYTE_ADDR, 0x8F, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE8, CAMERA_I2C_BYTE_ADDR, 0xC7, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0x16, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0x7A, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE8, CAMERA_I2C_BYTE_ADDR, 0x00, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0x19, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0x3F, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE8, CAMERA_I2C_BYTE_ADDR, 0xBD, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xEA, CAMERA_I2C_BYTE_ADDR, 0x63, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0x63, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE8, CAMERA_I2C_BYTE_ADDR, 0xE6, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE8, CAMERA_I2C_BYTE_ADDR, 0xF3, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0x2D, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0x69, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0x9B, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0xBF, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0xD7, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xE9, CAMERA_I2C_BYTE_ADDR, 0xE7, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        {0xEA, CAMERA_I2C_BYTE_ADDR, 0x00, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        /* 80Hz, Fo=125 X 0.4+30 */ //80 hz for a21n01j
        {0xd0, CAMERA_I2C_BYTE_ADDR, 0x7D, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
        /* step mode, isrc 0.8x mode(0x00 0.5x mode, 0x01 0.8x mode, 0x02 1.0x mode */
        {0xc8, CAMERA_I2C_BYTE_ADDR, 0x00, CAMERA_I2C_BYTE_DATA, ACTUATOR_I2C_OP_WRITE, 0},
      },
    }, /* actuator_params */

    .actuator_tuned_params =
    {
      .scenario_size =
      {
        2, /* MOVE_NEAR */
        2, /* MOVE_FAR */
      },
      .ringing_scenario =
      {
        /* MOVE_NEAR */
        {
          36,
          519,
        },
        /* MOVE_FAR */
        {
          36,
          519,
        },
      },
      .initial_code = 370,
      .region_size = 1,
      .region_params =
      {
        {
          .step_bound =
          {
            519, /* Macro step boundary*/
            0, /* Infinity step boundary*/
          },
          .code_per_step = 1,
          .qvalue = 519,
        },
      },
      .damping =
      {
        /* damping[MOVE_NEAR] */
        {
          /* Scenario 0 */
          {
            .ringing_params =
            {
              /* Region 0 */
              {
                .damping_step = 0x3FF,
                .damping_delay = 9000,
                .hw_params = 0x0000C400,
              },
            },
          },
          /* Scenario 1 */
          {
            .ringing_params =
            {
              /* Region 0 */
              {
                .damping_step = 0x3FF,
                .damping_delay = 9000,
                .hw_params = 0x0000C400,
              },
            },
          },
        },
        /* damping[MOVE_NEAR] */
        {
          /* Scenario 0 */
          {
            .ringing_params =
            {
              /* Region 0 */
              {
                .damping_step = 0x3FF,
                .damping_delay = 9000,
                .hw_params = 0x0000C400,
              },
            },
          },
          /* Scenario 1 */
          {
            .ringing_params =
            {
              /* Region 0 */
              {
                .damping_step = 0x3FF,
                .damping_delay = 9000,
                .hw_params = 0x0000C400,
              },
            },
          },
        },
      },
    }, /* actuator_tuned_params */
  },
