/*
 * Copyright (c) 2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef __IMX386_LIB_H__
#define __IMX386_LIB_H__

#include "sensor_lib.h"
#define SENSOR_MODEL "imx386"

/* IMX386 REGISTERS */
#define IMX386_DIG_GAIN_GR_ADDR           0x020E
#define IMX386_DIG_GAIN_R_ADDR            0x0210
#define IMX386_DIG_GAIN_B_ADDR            0x0212
#define IMX386_DIG_GAIN_GB_ADDR           0x0214

#define PD_CAL_ENABLE                     0x3047
#define AREA_MODE_ADDR                    0x315d
#define PD_OUT_EN_ADDR                    0x3049
#define PD_AREA_X_OFFSET_ADDR             0x3100
#define PD_AREA_Y_OFFSET_ADDR             0x3102
#define PD_AREA_WIDTH_ADDR                0x3104
#define PD_AREA_HEIGHT_ADDR               0x3106
#define FLEX_AREA_EN_ADDR                 0x315e
#define FLEX_AREA_XSTA0_ADDR              0x3108



/* IMX386 CONSTANTS */
#define IMX386_MAX_INTEGRATION_MARGIN   10

/* STATS DATA TYPE */
#define IMX386_CSI_PD_ISTATS            0x36

#define IMX386_DATA_PEDESTAL            0x40    /* 10bit value */

#define IMX386_MIN_AGAIN_REG_VAL        0       /* 1.0x */
#define IMX386_MAX_AGAIN_REG_VAL        480     /* 16.0x */

#define IMX386_MIN_DGAIN_REG_VAL        256     /* 1.0x */
#define IMX386_MAX_DGAIN_REG_VAL        256     /* 1.0x */
#define IMX386_MAX_DGAIN_DECIMATOR      256

/* IMX386 FORMULAS */
#define IMX386_MIN_AGAIN    (512 / (512 - IMX386_MIN_AGAIN_REG_VAL))
#define IMX386_MAX_AGAIN    (512 / (512 - IMX386_MAX_AGAIN_REG_VAL))

#define IMX386_MIN_DGAIN    (IMX386_MIN_DGAIN_REG_VAL / 256)
#define IMX386_MAX_DGAIN    (IMX386_MAX_DGAIN_REG_VAL / 256)

#define IMX386_MIN_GAIN     IMX386_MIN_AGAIN * IMX386_MIN_DGAIN
#define IMX386_MAX_GAIN     IMX386_MAX_AGAIN * IMX386_MAX_DGAIN
#define FULL_SIZE_WIDTH     4032
#define FULL_SIZE_HEIGHT    3016

#define PD_FLEX_WIN_NUM 6

#define PD_AREA_MODE  1 /* AREA_MODE: 0 - 16x12, 1 - 8x6, 2 - Flexible */
#define PD_WIN_X (PD_AREA_MODE==2 ? 1 : (16/(PD_AREA_MODE+1)))
#define PD_WIN_Y (PD_AREA_MODE==2 ? PD_FLEX_WIN_NUM : (12/(PD_AREA_MODE+1)))


/* PD area settings for full-size */
#define PD_AREA_WIDTH     (244*(PD_AREA_MODE+1))
#define PD_AREA_HEIGHT    (248*(PD_AREA_MODE+1))
#define PD_AREA_X_OFFSET  64
#define PD_AREA_Y_OFFSET  16

#define PD_WIN_X_S_0 1420
#define PD_WIN_Y_S_0 1064
#define PD_WIN_X_E_0 2339
#define PD_WIN_Y_E_0 1748
#define PD_WIN_X_S_1 1716
#define PD_WIN_Y_S_1 1064
#define PD_WIN_X_E_1 2635
#define PD_WIN_Y_E_1 1748
#define PD_WIN_X_S_2 1420
#define PD_WIN_Y_S_2 1291
#define PD_WIN_X_E_2 2339
#define PD_WIN_Y_E_2 1975
#define PD_WIN_X_S_3 1716
#define PD_WIN_Y_S_3 1291
#define PD_WIN_X_E_3 2635
#define PD_WIN_Y_E_3 1975
#define PD_WIN_X_S_4 1420
#define PD_WIN_Y_S_4 1064
#define PD_WIN_X_E_4 2635
#define PD_WIN_Y_E_4 1975
#define PD_WIN_X_S_5 1217
#define PD_WIN_Y_S_5 912
#define PD_WIN_X_E_5 2838
#define PD_WIN_Y_E_5 2127


#define START_REG_ARRAY \
{ \
  {0x0100, 0x01, 0x00}, \
}

#define STOP_REG_ARRAY \
{ \
  {0x0100, 0x00, 0x00}, \
}

#define GROUPON_REG_ARRAY \
{ \
  {0x0104, 0x01, 0x00}, \
}

#define GROUPOFF_REG_ARRAY \
{ \
  {0x0104, 0x00, 0x00}, \
}

#define INIT0_REG_ARRAY \
{ \
  {0x0101, 0x03, 0x00}, \
  {0x0136, 0x18, 0x00}, \
  {0x0137, 0x00, 0x00}, \
  {0x3A7D, 0x00, 0x00}, \
  {0x3A7E, 0x01, 0x00}, \
  {0x3A7F, 0x05, 0x00}, \
  {0x3100, PD_AREA_X_OFFSET>>8, 0x00}, \
  {0x3101, PD_AREA_X_OFFSET&0xFF, 0x00}, \
  {0x3102, PD_AREA_Y_OFFSET>>8, 0x00}, \
  {0x3103, PD_AREA_Y_OFFSET&0xFF, 0x00}, \
  {0x3104, PD_AREA_WIDTH>>8, 0x00}, \
  {0x3105, PD_AREA_WIDTH&0xFF, 0x00}, \
  {0x3106, PD_AREA_HEIGHT>>8, 0x00}, \
  {0x3107, PD_AREA_HEIGHT&0xFF, 0x00}, \
  {0x3150, 0x04, 0x00}, \
  {0x3151, 0x03, 0x00}, \
  {0x3152, 0x02, 0x00}, \
  {0x3153, 0x01, 0x00}, \
  {0x5A86, 0x00, 0x00}, \
  {0x5A87, 0x82, 0x00}, \
  {0x5D1A, 0x00, 0x00}, \
  {0x5D95, 0x02, 0x00}, \
  {0x5E1B, 0x00, 0x00}, \
  {0x5F5A, 0x00, 0x00}, \
  {0x5F5B, 0x04, 0x00}, \
  {0x682C, 0x31, 0x00}, \
  {0x6831, 0x31, 0x00}, \
  {0x6835, 0x0E, 0x00}, \
  {0x6836, 0x31, 0x00}, \
  {0x6838, 0x30, 0x00}, \
  {0x683A, 0x06, 0x00}, \
  {0x683B, 0x33, 0x00}, \
  {0x683D, 0x30, 0x00}, \
  {0x6842, 0x31, 0x00}, \
  {0x6844, 0x31, 0x00}, \
  {0x6847, 0x31, 0x00}, \
  {0x6849, 0x31, 0x00}, \
  {0x684D, 0x0E, 0x00}, \
  {0x684E, 0x32, 0x00}, \
  {0x6850, 0x31, 0x00}, \
  {0x6852, 0x06, 0x00}, \
  {0x6853, 0x33, 0x00}, \
  {0x6855, 0x31, 0x00}, \
  {0x685A, 0x32, 0x00}, \
  {0x685C, 0x33, 0x00}, \
  {0x685F, 0x31, 0x00}, \
  {0x6861, 0x33, 0x00}, \
  {0x6865, 0x0D, 0x00}, \
  {0x6866, 0x33, 0x00}, \
  {0x6868, 0x31, 0x00}, \
  {0x686B, 0x34, 0x00}, \
  {0x686D, 0x31, 0x00}, \
  {0x6872, 0x32, 0x00}, \
  {0x6877, 0x33, 0x00}, \
  {0x7FF0, 0x01, 0x00}, \
  {0x7FF4, 0x08, 0x00}, \
  {0x7FF5, 0x3C, 0x00}, \
  {0x7FFA, 0x01, 0x00}, \
  {0x7FFD, 0x00, 0x00}, \
  {0x831E, 0x00, 0x00}, \
  {0x831F, 0x00, 0x00}, \
  {0x9301, 0xBD, 0x00}, \
  {0x9B94, 0x03, 0x00}, \
  {0x9B95, 0x00, 0x00}, \
  {0x9B96, 0x08, 0x00}, \
  {0x9B97, 0x00, 0x00}, \
  {0x9B98, 0x0A, 0x00}, \
  {0x9B99, 0x00, 0x00}, \
  {0x9BA7, 0x18, 0x00}, \
  {0x9BA8, 0x18, 0x00}, \
  {0x9D04, 0x08, 0x00}, \
  {0x9D50, 0x8C, 0x00}, \
  {0x9D51, 0x64, 0x00}, \
  {0x9D52, 0x50, 0x00}, \
  {0x9E31, 0x04, 0x00}, \
  {0x9E32, 0x04, 0x00}, \
  {0x9E33, 0x04, 0x00}, \
  {0x9E34, 0x04, 0x00}, \
  {0xA200, 0x00, 0x00}, \
  {0xA201, 0x0A, 0x00}, \
  {0xA202, 0x00, 0x00}, \
  {0xA203, 0x0A, 0x00}, \
  {0xA204, 0x00, 0x00}, \
  {0xA205, 0x0A, 0x00}, \
  {0xA206, 0x01, 0x00}, \
  {0xA207, 0xC0, 0x00}, \
  {0xA208, 0x00, 0x00}, \
  {0xA209, 0xC0, 0x00}, \
  {0xA20C, 0x00, 0x00}, \
  {0xA20D, 0x0A, 0x00}, \
  {0xA20E, 0x00, 0x00}, \
  {0xA20F, 0x0A, 0x00}, \
  {0xA210, 0x00, 0x00}, \
  {0xA211, 0x0A, 0x00}, \
  {0xA212, 0x01, 0x00}, \
  {0xA213, 0xC0, 0x00}, \
  {0xA214, 0x00, 0x00}, \
  {0xA215, 0xC0, 0x00}, \
  {0xA300, 0x00, 0x00}, \
  {0xA301, 0x0A, 0x00}, \
  {0xA302, 0x00, 0x00}, \
  {0xA303, 0x0A, 0x00}, \
  {0xA304, 0x00, 0x00}, \
  {0xA305, 0x0A, 0x00}, \
  {0xA306, 0x01, 0x00}, \
  {0xA307, 0xC0, 0x00}, \
  {0xA308, 0x00, 0x00}, \
  {0xA309, 0xC0, 0x00}, \
  {0xA30C, 0x00, 0x00}, \
  {0xA30D, 0x0A, 0x00}, \
  {0xA30E, 0x00, 0x00}, \
  {0xA30F, 0x0A, 0x00}, \
  {0xA310, 0x00, 0x00}, \
  {0xA311, 0x0A, 0x00}, \
  {0xA312, 0x01, 0x00}, \
  {0xA313, 0xC0, 0x00}, \
  {0xA314, 0x00, 0x00}, \
  {0xA315, 0xC0, 0x00}, \
  {0xBC19, 0x01, 0x00}, \
  {0xBC1C, 0x0A, 0x00}, \
  {AREA_MODE_ADDR, PD_AREA_MODE, 0x00}, /* 0-16x12, 1-8x6, 2-Flexible */ \
  /* Flexiable PDAF windows */ \
  {0x315e, 0x01, 0x00}, \
  {0x315f, 0x01, 0x00}, \
  {0x3160, 0x01, 0x00}, \
  {0x3161, 0x01, 0x00}, \
  {0x3162, 0x01, 0x00}, \
  {0x3163, 0x01, 0x00}, \
  {0x3164, 0x01, 0x00}, \
  {0x3165, 0x01, 0x00}, \
}

#define RES0_REG_ARRAY \
{ \
  {0x0112,0x0A,0x00}, \
  {0x0113,0x0A,0x00}, \
  {0x0301,0x06,0x00}, \
  {0x0303,0x02,0x00}, \
  {0x0305,0x04,0x00}, \
  {0x0306,0x00,0x00}, \
  {0x0307,0xC6,0x00}, \
  {0x0309,0x0A,0x00}, \
  {0x030B,0x01,0x00}, \
  {0x030D,0x0C,0x00}, \
  {0x030E,0x02,0x00}, \
  {0x030F,0x3F,0x00}, \
  {0x0310,0x01,0x00}, \
  {0x0342,0x10,0x00}, \
  {0x0343,0xC8,0x00}, \
  {0x0340,0x0C,0x00}, \
  {0x0341,0x00,0x00}, \
  {0x0344,0x00,0x00}, \
  {0x0345,0x00,0x00}, \
  {0x0346,0x00,0x00}, \
  {0x0347,0x00,0x00}, \
  {0x0348,0x0F,0x00}, \
  {0x0349,0xBF,0x00}, \
  {0x034A,0x0B,0x00}, \
  {0x034B,0xC7,0x00}, \
  {0x0385,0x01,0x00}, \
  {0x0387,0x01,0x00}, \
  {0x0900,0x00,0x00}, \
  {0x0901,0x11,0x00}, \
  {0x300D,0x00,0x00}, \
  {0x302E,0x00,0x00}, \
  {0x0401,0x00,0x00}, \
  {0x0404,0x00,0x00}, \
  {0x0405,0x10,0x00}, \
  {0x040C,0x0F,0x00}, \
  {0x040D,0xC0,0x00}, \
  {0x040E,0x0B,0x00}, \
  {0x040F,0xC8,0x00}, \
  {0x034C,0x0F,0x00}, \
  {0x034D,0xC0,0x00}, \
  {0x034E,0x0B,0x00}, \
  {0x034F,0xC8,0x00}, \
  {0x0114,0x03,0x00}, \
  {0x0408,0x00,0x00}, \
  {0x0409,0x00,0x00}, \
  {0x040A,0x00,0x00}, \
  {0x040B,0x00,0x00}, \
  {0x0902,0x00,0x00}, \
  {0x3030,0x00,0x00}, \
  {0x3031,0x01,0x00}, \
  {0x3032,0x00,0x00}, \
  {0x3047,0x01,0x00}, \
  {0x3049,0x01,0x00}, \
  {0x30E6,0x02,0x00}, \
  {0x30E7,0x59,0x00}, \
  {0x4E25,0x80,0x00}, \
  {0x663A,0x02,0x00}, \
  {0x9311,0x00,0x00}, \
  {0xA0CD,0x19,0x00}, \
  {0xA0CE,0x19,0x00}, \
  {0xA0CF,0x19,0x00}, \
  {0x0202,0x0B,0x00}, \
  {0x0203,0xF6,0x00}, \
  {0x0204,0x00,0x00}, \
  {0x0205,0x00,0x00}, \
  {0x020E,0x01,0x00}, \
  {0x020F,0x00,0x00}, \
  {0x0210,0x01,0x00}, \
  {0x0211,0x00,0x00}, \
  {0x0212,0x01,0x00}, \
  {0x0213,0x00,0x00}, \
  {0x0214,0x01,0x00}, \
  {0x0215,0x00,0x00}, \
  {0x3100, PD_AREA_X_OFFSET>>8, 0x00}, \
  {0x3101, PD_AREA_X_OFFSET&0xFF, 0x00}, \
  {0x3102, PD_AREA_Y_OFFSET>>8, 0x00}, \
  {0x3103, PD_AREA_Y_OFFSET&0xFF, 0x00}, \
  {0x3104, PD_AREA_WIDTH>>8, 0x00}, \
  {0x3105, PD_AREA_WIDTH&0xFF, 0x00}, \
  {0x3106, PD_AREA_HEIGHT>>8, 0x00}, \
  {0x3107, PD_AREA_HEIGHT&0xFF, 0x00}, \
  {0x3108, PD_WIN_X_S_0>>8, 0x00}, \
  {0x3109, PD_WIN_X_S_0&0xFF, 0x00}, \
  {0x310a, PD_WIN_Y_S_0>>8, 0x00}, \
  {0x310b, PD_WIN_Y_S_0&0xFF, 0x00}, \
  {0x310c, PD_WIN_X_E_0>>8, 0x00}, \
  {0x310d, PD_WIN_X_E_0&0xFF, 0x00}, \
  {0x310e, PD_WIN_Y_E_0>>8, 0x00}, \
  {0x310f, PD_WIN_Y_E_0&0xFF, 0x00}, \
  {0x3110, PD_WIN_X_S_1>>8, 0x00}, \
  {0x3111, PD_WIN_X_S_1&0xFF, 0x00}, \
  {0x3112, PD_WIN_Y_S_1>>8, 0x00}, \
  {0x3113, PD_WIN_Y_S_1&0xFF, 0x00}, \
  {0x3114, PD_WIN_X_E_1>>8, 0x00}, \
  {0x3115, PD_WIN_X_E_1&0xFF, 0x00}, \
  {0x3116, PD_WIN_Y_E_1>>8, 0x00}, \
  {0x3117, PD_WIN_Y_E_1&0xFF, 0x00}, \
  {0x3118, PD_WIN_X_S_2>>8, 0x00}, \
  {0x3119, PD_WIN_X_S_2&0xFF, 0x00}, \
  {0x311a, PD_WIN_Y_S_2>>8, 0x00}, \
  {0x311b, PD_WIN_Y_S_2&0xFF, 0x00}, \
  {0x311c, PD_WIN_X_E_2>>8, 0x00}, \
  {0x311d, PD_WIN_X_E_2&0xFF, 0x00}, \
  {0x311e, PD_WIN_Y_E_2>>8, 0x00}, \
  {0x311f, PD_WIN_Y_E_2&0xFF, 0x00}, \
  {0x3120, PD_WIN_X_S_3>>8, 0x00}, \
  {0x3121, PD_WIN_X_S_3&0xFF, 0x00}, \
  {0x3122, PD_WIN_Y_S_3>>8, 0x00}, \
  {0x3123, PD_WIN_Y_S_3&0xFF, 0x00}, \
  {0x3124, PD_WIN_X_E_3>>8, 0x00}, \
  {0x3125, PD_WIN_X_E_3&0xFF, 0x00}, \
  {0x3126, PD_WIN_Y_E_3>>8, 0x00}, \
  {0x3127, PD_WIN_Y_E_3&0xFF, 0x00}, \
  {0x3128, PD_WIN_X_S_4>>8, 0x00}, \
  {0x3129, PD_WIN_X_S_4&0xFF, 0x00}, \
  {0x312a, PD_WIN_Y_S_4>>8, 0x00}, \
  {0x312b, PD_WIN_Y_S_4&0xFF, 0x00}, \
  {0x312c, PD_WIN_X_E_4>>8, 0x00}, \
  {0x312d, PD_WIN_X_E_4&0xFF, 0x00}, \
  {0x312e, PD_WIN_Y_E_4>>8, 0x00}, \
  {0x312f, PD_WIN_Y_E_4&0xFF, 0x00}, \
  {0x3130, PD_WIN_X_S_5>>8, 0x00}, \
  {0x3131, PD_WIN_X_S_5&0xFF, 0x00}, \
  {0x3132, PD_WIN_Y_S_5>>8, 0x00}, \
  {0x3133, PD_WIN_Y_S_5&0xFF, 0x00}, \
  {0x3134, PD_WIN_X_E_5>>8, 0x00}, \
  {0x3135, PD_WIN_X_E_5&0xFF, 0x00}, \
  {0x3136, PD_WIN_Y_E_5>>8, 0x00}, \
  {0x3137, PD_WIN_Y_E_5&0xFF, 0x00}, \
}

#define RES1_REG_ARRAY \
{ \
  {0x0112,0x0A,0x00}, \
  {0x0113,0x0A,0x00}, \
  {0x0301,0x06,0x00}, \
  {0x0303,0x02,0x00}, \
  {0x0305,0x0C,0x00}, \
  {0x0306,0x01,0x00}, \
  {0x0307,0x2D,0x00}, \
  {0x0309,0x0A,0x00}, \
  {0x030B,0x02,0x00}, \
  {0x030D,0x0C,0x00}, \
  {0x030E,0x01,0x00}, \
  {0x030F,0x2D,0x00}, \
  {0x0310,0x00,0x00}, \
  {0x0342,0x10,0x00}, \
  {0x0343,0xC8,0x00}, \
  {0x0340,0x06,0x00}, \
  {0x0341,0x15,0x00}, \
  {0x0344,0x00,0x00}, \
  {0x0345,0x00,0x00}, \
  {0x0346,0x00,0x00}, \
  {0x0347,0x00,0x00}, \
  {0x0348,0x0F,0x00}, \
  {0x0349,0xBF,0x00}, \
  {0x034A,0x0B,0x00}, \
  {0x034B,0xC7,0x00}, \
  {0x0385,0x01,0x00}, \
  {0x0387,0x01,0x00}, \
  {0x0900,0x01,0x00}, \
  {0x0901,0x22,0x00}, \
  {0x300D,0x00,0x00}, \
  {0x302E,0x00,0x00}, \
  {0x0401,0x00,0x00}, \
  {0x0404,0x00,0x00}, \
  {0x0405,0x10,0x00}, \
  {0x040C,0x07,0x00}, \
  {0x040D,0xE0,0x00}, \
  {0x040E,0x05,0x00}, \
  {0x040F,0xE4,0x00}, \
  {0x034C,0x07,0x00}, \
  {0x034D,0xE0,0x00}, \
  {0x034E,0x05,0x00}, \
  {0x034F,0xE4,0x00}, \
  {0x0114,0x03,0x00}, \
  {0x0408,0x00,0x00}, \
  {0x0409,0x00,0x00}, \
  {0x040A,0x00,0x00}, \
  {0x040B,0x00,0x00}, \
  {0x0902,0x02,0x00}, \
  {0x3030,0x00,0x00}, \
  {0x3031,0x01,0x00}, \
  {0x3032,0x00,0x00}, \
  {0x3047,0x01,0x00}, \
  {0x3049,0x01,0x00}, \
  {0x30E6,0x00,0x00}, \
  {0x30E7,0x00,0x00}, \
  {0x4E25,0x80,0x00}, \
  {0x663A,0x01,0x00}, \
  {0x9311,0x3F,0x00}, \
  {0xA0CD,0x0A,0x00}, \
  {0xA0CE,0x0A,0x00}, \
  {0xA0CF,0x0A,0x00}, \
  {0x0202,0x06,0x00}, \
  {0x0203,0x0B,0x00}, \
  {0x0204,0x00,0x00}, \
  {0x0205,0x00,0x00}, \
  {0x020E,0x01,0x00}, \
  {0x020F,0x00,0x00}, \
  {0x0210,0x01,0x00}, \
  {0x0211,0x00,0x00}, \
  {0x0212,0x01,0x00}, \
  {0x0213,0x00,0x00}, \
  {0x0214,0x01,0x00}, \
  {0x0215,0x00,0x00}, \
  {0x3100, PD_AREA_X_OFFSET/2>>8, 0x00},/* PD_AREA_X_OFFSET */ \
  {0x3101, PD_AREA_X_OFFSET/2 & 0xFF, 0x00}, \
  {0x3102, PD_AREA_Y_OFFSET/2>>8, 0x00}, /* PD_AREA_Y_OFFSET */ \
  {0x3103, PD_AREA_Y_OFFSET/2 & 0xFF, 0x00}, \
  {0x3104, PD_AREA_WIDTH/2>>8, 0x00}, /* PD_AREA_WIDTH */ \
  {0x3105, PD_AREA_WIDTH/2 & 0xFF, 0x00}, \
  {0x3106, PD_AREA_HEIGHT/2>>8, 0x00}, /* PD_AREA_HEIGHT */ \
  {0x3107, PD_AREA_HEIGHT/2 & 0xFF, 0x00}, \
  {0x3108, PD_WIN_X_S_0/2>>8, 0x00}, \
  {0x3109, PD_WIN_X_S_0/2&0xFF, 0x00}, \
  {0x310a, PD_WIN_Y_S_0/2>>8, 0x00}, \
  {0x310b, PD_WIN_Y_S_0/2&0xFF, 0x00}, \
  {0x310c, PD_WIN_X_E_0/2>>8, 0x00}, \
  {0x310d, PD_WIN_X_E_0/2&0xFF, 0x00}, \
  {0x310e, PD_WIN_Y_E_0/2>>8, 0x00}, \
  {0x310f, PD_WIN_Y_E_0/2&0xFF, 0x00}, \
  {0x3110, PD_WIN_X_S_1/2>>8, 0x00}, \
  {0x3111, PD_WIN_X_S_1/2&0xFF, 0x00}, \
  {0x3112, PD_WIN_Y_S_1/2>>8, 0x00}, \
  {0x3113, PD_WIN_Y_S_1/2&0xFF, 0x00}, \
  {0x3114, PD_WIN_X_E_1/2>>8, 0x00}, \
  {0x3115, PD_WIN_X_E_1/2&0xFF, 0x00}, \
  {0x3116, PD_WIN_Y_E_1/2>>8, 0x00}, \
  {0x3117, PD_WIN_Y_E_1/2&0xFF, 0x00}, \
  {0x3118, PD_WIN_X_S_2/2>>8, 0x00}, \
  {0x3119, PD_WIN_X_S_2/2&0xFF, 0x00}, \
  {0x311a, PD_WIN_Y_S_2/2>>8, 0x00}, \
  {0x311b, PD_WIN_Y_S_2/2&0xFF, 0x00}, \
  {0x311c, PD_WIN_X_E_2/2>>8, 0x00}, \
  {0x311d, PD_WIN_X_E_2/2&0xFF, 0x00}, \
  {0x311e, PD_WIN_Y_E_2/2>>8, 0x00}, \
  {0x311f, PD_WIN_Y_E_2/2&0xFF, 0x00}, \
  {0x3120, PD_WIN_X_S_3/2>>8, 0x00}, \
  {0x3121, PD_WIN_X_S_3/2&0xFF, 0x00}, \
  {0x3122, PD_WIN_Y_S_3/2>>8, 0x00}, \
  {0x3123, PD_WIN_Y_S_3/2&0xFF, 0x00}, \
  {0x3124, PD_WIN_X_E_3/2>>8, 0x00}, \
  {0x3125, PD_WIN_X_E_3/2&0xFF, 0x00}, \
  {0x3126, PD_WIN_Y_E_3/2>>8, 0x00}, \
  {0x3127, PD_WIN_Y_E_3/2&0xFF, 0x00}, \
  {0x3128, PD_WIN_X_S_4/2>>8, 0x00}, \
  {0x3129, PD_WIN_X_S_4/2&0xFF, 0x00}, \
  {0x312a, PD_WIN_Y_S_4/2>>8, 0x00}, \
  {0x312b, PD_WIN_Y_S_4/2&0xFF, 0x00}, \
  {0x312c, PD_WIN_X_E_4/2>>8, 0x00}, \
  {0x312d, PD_WIN_X_E_4/2&0xFF, 0x00}, \
  {0x312e, PD_WIN_Y_E_4/2>>8, 0x00}, \
  {0x312f, PD_WIN_Y_E_4/2&0xFF, 0x00}, \
  {0x3130, PD_WIN_X_S_5/2>>8, 0x00}, \
  {0x3131, PD_WIN_X_S_5/2&0xFF, 0x00}, \
  {0x3132, PD_WIN_Y_S_5/2>>8, 0x00}, \
  {0x3133, PD_WIN_Y_S_5/2&0xFF, 0x00}, \
  {0x3134, PD_WIN_X_E_5/2>>8, 0x00}, \
  {0x3135, PD_WIN_X_E_5/2&0xFF, 0x00}, \
  {0x3136, PD_WIN_Y_E_5/2>>8, 0x00}, \
  {0x3137, PD_WIN_Y_E_5/2&0xFF, 0x00}, \
}

#define RES2_REG_ARRAY \
{ \
  {0x0112,0x0A,0x00}, \
  {0x0113,0x0A,0x00}, \
  {0x0301,0x06,0x00}, \
  {0x0303,0x02,0x00}, \
  {0x0305,0x0C,0x00}, \
  {0x0306,0x01,0x00}, \
  {0x0307,0x3C,0x00}, \
  {0x0309,0x0A,0x00}, \
  {0x030B,0x01,0x00}, \
  {0x030D,0x0C,0x00}, \
  {0x030E,0x01,0x00}, \
  {0x030F,0x3C,0x00}, \
  {0x0310,0x00,0x00}, \
  {0x0342,0x08,0x00}, \
  {0x0343,0xD0,0x00}, \
  {0x0340,0x06,0x00}, \
  {0x0341,0x14,0x00}, \
  {0x0344,0x00,0x00}, \
  {0x0345,0x00,0x00}, \
  {0x0346,0x00,0x00}, \
  {0x0347,0x00,0x00}, \
  {0x0348,0x0F,0x00}, \
  {0x0349,0xBF,0x00}, \
  {0x034A,0x0B,0x00}, \
  {0x034B,0xC7,0x00}, \
  {0x0385,0x01,0x00}, \
  {0x0387,0x01,0x00}, \
  {0x0900,0x01,0x00}, \
  {0x0901,0x22,0x00}, \
  {0x300D,0x00,0x00}, \
  {0x302E,0x00,0x00}, \
  {0x0401,0x00,0x00}, \
  {0x0404,0x00,0x00}, \
  {0x0405,0x10,0x00}, \
  {0x040C,0x07,0x00}, \
  {0x040D,0xE0,0x00}, \
  {0x040E,0x05,0x00}, \
  {0x040F,0xE4,0x00}, \
  {0x034C,0x07,0x00}, \
  {0x034D,0xE0,0x00}, \
  {0x034E,0x05,0x00}, \
  {0x034F,0xE4,0x00}, \
  {0x0114,0x03,0x00}, \
  {0x0408,0x00,0x00}, \
  {0x0409,0x00,0x00}, \
  {0x040A,0x00,0x00}, \
  {0x040B,0x00,0x00}, \
  {0x0902,0x02,0x00}, \
  {0x3030,0x00,0x00}, \
  {0x3031,0x01,0x00}, \
  {0x3032,0x00,0x00}, \
  {0x3047,0x01,0x00}, \
  {0x3049,0x00,0x00}, \
  {0x30E6,0x00,0x00}, \
  {0x30E7,0x00,0x00}, \
  {0x4E25,0x80,0x00}, \
  {0x663A,0x01,0x00}, \
  {0x9311,0x3F,0x00}, \
  {0xA0CD,0x0A,0x00}, \
  {0xA0CE,0x0A,0x00}, \
  {0xA0CF,0x0A,0x00}, \
  {0x0202,0x06,0x00}, \
  {0x0203,0x0A,0x00}, \
  {0x0204,0x00,0x00}, \
  {0x0205,0x00,0x00}, \
  {0x020E,0x01,0x00}, \
  {0x020F,0x00,0x00}, \
  {0x0210,0x01,0x00}, \
  {0x0211,0x00,0x00}, \
  {0x0212,0x01,0x00}, \
  {0x0213,0x00,0x00}, \
  {0x0214,0x01,0x00}, \
  {0x0215,0x00,0x00}, \
}

#define RES3_REG_ARRAY \
{ \
  {0x0112,0x0A,0x00}, \
  {0x0113,0x0A,0x00}, \
  {0x0301,0x06,0x00}, \
  {0x0303,0x02,0x00}, \
  {0x0305,0x0C,0x00}, \
  {0x0306,0x01,0x00}, \
  {0x0307,0xDA,0x00}, \
  {0x0309,0x0A,0x00}, \
  {0x030B,0x01,0x00}, \
  {0x030D,0x0C,0x00}, \
  {0x030E,0x01,0x00}, \
  {0x030F,0xDA,0x00}, \
  {0x0310,0x00,0x00}, \
  {0x0342,0x08,0x00}, \
  {0x0343,0xD0,0x00}, \
  {0x0340,0x06,0x00}, \
  {0x0341,0x14,0x00}, \
  {0x0344,0x00,0x00}, \
  {0x0345,0x00,0x00}, \
  {0x0346,0x00,0x00}, \
  {0x0347,0x00,0x00}, \
  {0x0348,0x0F,0x00}, \
  {0x0349,0xBF,0x00}, \
  {0x034A,0x0B,0x00}, \
  {0x034B,0xC7,0x00}, \
  {0x0385,0x01,0x00}, \
  {0x0387,0x01,0x00}, \
  {0x0900,0x01,0x00}, \
  {0x0901,0x22,0x00}, \
  {0x300D,0x00,0x00}, \
  {0x302E,0x00,0x00}, \
  {0x0401,0x00,0x00}, \
  {0x0404,0x00,0x00}, \
  {0x0405,0x10,0x00}, \
  {0x040C,0x07,0x00}, \
  {0x040D,0xE0,0x00}, \
  {0x040E,0x05,0x00}, \
  {0x040F,0xE4,0x00}, \
  {0x034C,0x07,0x00}, \
  {0x034D,0xE0,0x00}, \
  {0x034E,0x05,0x00}, \
  {0x034F,0xE4,0x00}, \
  {0x0114,0x03,0x00}, \
  {0x0408,0x00,0x00}, \
  {0x0409,0x00,0x00}, \
  {0x040A,0x00,0x00}, \
  {0x040B,0x00,0x00}, \
  {0x0902,0x02,0x00}, \
  {0x3030,0x00,0x00}, \
  {0x3031,0x01,0x00}, \
  {0x3032,0x00,0x00}, \
  {0x3047,0x01,0x00}, \
  {0x3049,0x00,0x00}, \
  {0x30E6,0x00,0x00}, \
  {0x30E7,0x00,0x00}, \
  {0x4E25,0x80,0x00}, \
  {0x663A,0x01,0x00}, \
  {0x9311,0x3F,0x00}, \
  {0xA0CD,0x0A,0x00}, \
  {0xA0CE,0x0A,0x00}, \
  {0xA0CF,0x0A,0x00}, \
  {0x0202,0x06,0x00}, \
  {0x0203,0x0A,0x00}, \
  {0x0204,0x00,0x00}, \
  {0x0205,0x00,0x00}, \
  {0x020E,0x01,0x00}, \
  {0x020F,0x00,0x00}, \
  {0x0210,0x01,0x00}, \
  {0x0211,0x00,0x00}, \
  {0x0212,0x01,0x00}, \
  {0x0213,0x00,0x00}, \
  {0x0214,0x01,0x00}, \
  {0x0215,0x00,0x00}, \
}


#define RES4_REG_ARRAY \
{ \
  {0x0112,0x0A,0x00}, \
  {0x0113,0x0A,0x00}, \
  {0x0301,0x06,0x00}, \
  {0x0303,0x02,0x00}, \
  {0x0305,0x0C,0x00}, \
  {0x0306,0x01,0x00}, \
  {0x0307,0xE0,0x00}, \
  {0x0309,0x0A,0x00}, \
  {0x030B,0x01,0x00}, \
  {0x030D,0x0C,0x00}, \
  {0x030E,0x01,0x00}, \
  {0x030F,0xE0,0x00}, \
  {0x0310,0x00,0x00}, \
  {0x0342,0x08,0x00}, \
  {0x0343,0xD0,0x00}, \
  {0x0340,0x04,0x00}, \
  {0x0341,0x9E,0x00}, \
  {0x0344,0x00,0x00}, \
  {0x0345,0x00,0x00}, \
  {0x0346,0x01,0x00}, \
  {0x0347,0x7C,0x00}, \
  {0x0348,0x0F,0x00}, \
  {0x0349,0xBF,0x00}, \
  {0x034A,0x0A,0x00}, \
  {0x034B,0x4B,0x00}, \
  {0x0385,0x01,0x00}, \
  {0x0387,0x01,0x00}, \
  {0x0900,0x01,0x00}, \
  {0x0901,0x22,0x00}, \
  {0x300D,0x00,0x00}, \
  {0x302E,0x00,0x00}, \
  {0x0401,0x00,0x00}, \
  {0x0404,0x00,0x00}, \
  {0x0405,0x10,0x00}, \
  {0x040C,0x07,0x00}, \
  {0x040D,0x80,0x00}, \
  {0x040E,0x04,0x00}, \
  {0x040F,0x38,0x00}, \
  {0x034C,0x07,0x00}, \
  {0x034D,0x80,0x00}, \
  {0x034E,0x04,0x00}, \
  {0x034F,0x38,0x00}, \
  {0x0114,0x03,0x00}, \
  {0x0408,0x00,0x00}, \
  {0x0409,0x30,0x00}, \
  {0x040A,0x00,0x00}, \
  {0x040B,0x18,0x00}, \
  {0x0902,0x02,0x00}, \
  {0x3030,0x00,0x00}, \
  {0x3031,0x01,0x00}, \
  {0x3032,0x00,0x00}, \
  {0x3047,0x01,0x00}, \
  {0x3049,0x00,0x00}, \
  {0x30E6,0x00,0x00}, \
  {0x30E7,0x00,0x00}, \
  {0x4E25,0x80,0x00}, \
  {0x663A,0x01,0x00}, \
  {0x9311,0x3F,0x00}, \
  {0xA0CD,0x0A,0x00}, \
  {0xA0CE,0x0A,0x00}, \
  {0xA0CF,0x0A,0x00}, \
  {0x0202,0x04,0x00}, \
  {0x0203,0x94,0x00}, \
  {0x0204,0x00,0x00}, \
  {0x0205,0x00,0x00}, \
  {0x020E,0x01,0x00}, \
  {0x020F,0x00,0x00}, \
  {0x0210,0x01,0x00}, \
  {0x0211,0x00,0x00}, \
  {0x0212,0x01,0x00}, \
  {0x0213,0x00,0x00}, \
  {0x0214,0x01,0x00}, \
  {0x0215,0x00,0x00}, \
}


/* Sensor Handler */
static sensor_lib_t sensor_lib_ptr =
{
  .sensor_slave_info =
  {
    .sensor_name = SENSOR_MODEL,
    .slave_addr = 0x34,
    .i2c_freq_mode = SENSOR_I2C_MODE_FAST,
    .addr_type = CAMERA_I2C_WORD_ADDR,
    .sensor_id_info =
    {
      .sensor_id_reg_addr = 0x0016,
      .sensor_id = 0x0386,
    },
    .power_setting_array =
    {
      .power_setting_a =
      {
        {
          .seq_type = CAMERA_POW_SEQ_GPIO,
          .seq_val = CAMERA_GPIO_STANDBY,
          .config_val = GPIO_OUT_LOW,
          .delay = 1,
        },
        {
          .seq_type = CAMERA_POW_SEQ_GPIO,
          .seq_val = CAMERA_GPIO_VANA,
          .config_val = GPIO_OUT_HIGH,
          .delay = 0,
        },
        {
          .seq_type = CAMERA_POW_SEQ_VREG,
          .seq_val = CAMERA_VDIG,
          .config_val = 0,
          .delay = 0,
        },
        {
          .seq_type = CAMERA_POW_SEQ_VREG,
          .seq_val = CAMERA_VIO,
          .config_val = 0,
          .delay = 0,
        },
        {
          .seq_type = CAMERA_POW_SEQ_CLK,
          .seq_val = CAMERA_MCLK,
          .config_val = 24000000,
          .delay = 1,
        },
        {
          .seq_type = CAMERA_POW_SEQ_GPIO,
          .seq_val = CAMERA_GPIO_STANDBY,
          .config_val = GPIO_OUT_HIGH,
          .delay = 10,
        },
      },
      .size = 6,
      .power_down_setting_a =
      {
        {
          .seq_type = CAMERA_POW_SEQ_CLK,
          .seq_val = CAMERA_MCLK,
          .config_val = 0,
          .delay = 1,
        },
        {
          .seq_type = CAMERA_POW_SEQ_GPIO,
          .seq_val = CAMERA_GPIO_STANDBY,
          .config_val = GPIO_OUT_LOW,
          .delay = 1,
        },
        {
          .seq_type = CAMERA_POW_SEQ_VREG,
          .seq_val = CAMERA_VIO,
          .config_val = 0,
          .delay = 0,
        },
        {
          .seq_type = CAMERA_POW_SEQ_VREG,
          .seq_val = CAMERA_VDIG,
          .config_val = 0,
          .delay = 0,
        },
        {
          .seq_type = CAMERA_POW_SEQ_GPIO,
          .seq_val = CAMERA_GPIO_VANA,
          .config_val = GPIO_OUT_LOW,
          .delay = 0,
        },
      },
      .size_down = 5,
    },
  },
  .sensor_output =
  {
    .output_format = SENSOR_BAYER,
    .connection_mode = SENSOR_MIPI_CSI,
    .raw_output = SENSOR_10_BIT_DIRECT,
    .filter_arrangement = SENSOR_BGGR,
  },
  .output_reg_addr =
  {
    .x_output = 0x034C,
    .y_output = 0x034E,
    .line_length_pclk = 0x0342,
    .frame_length_lines = 0x0340,
  },
  .exp_gain_info =
  {
    .coarse_int_time_addr = 0x0202,
    .global_gain_addr = 0x0204,
    .vert_offset = IMX386_MAX_INTEGRATION_MARGIN,
  },
  .aec_info =
  {
    .min_gain = IMX386_MIN_GAIN,
    .max_gain = IMX386_MAX_GAIN,
    .max_analog_gain = IMX386_MAX_AGAIN,
    .max_linecount = 65535 - IMX386_MAX_INTEGRATION_MARGIN,
  },
  .sensor_num_frame_skip = 2,
  .sensor_num_HDR_frame_skip = 2,
  .sensor_max_pipeline_frame_delay = 2,
  .sensor_property =
  {
    .pix_size = 1.0,
    .sensing_method = SENSOR_SMETHOD_ONE_CHIP_COLOR_AREA_SENSOR,
    .crop_factor = 1.0,
  },
  .pixel_array_size_info =
  {
    .active_array_size =
    {
      .width = 4032,
      .height = 3016,
    },
    .left_dummy = 12,
    .right_dummy = 12,
    .top_dummy = 16,
    .bottom_dummy = 16,
  },
  .color_level_info =
  {
    .white_level = 1023,
    .r_pedestal = IMX386_DATA_PEDESTAL,
    .gr_pedestal = IMX386_DATA_PEDESTAL,
    .gb_pedestal = IMX386_DATA_PEDESTAL,
    .b_pedestal = IMX386_DATA_PEDESTAL,
  },
  .sensor_stream_info_array =
  {
    .sensor_stream_info =
    {
      {
        .vc_cfg_size = 2,
        .vc_cfg =
        {
          {
            .cid = 0,
            .dt = CSI_RAW10,
            .decode_format = CSI_DECODE_10BIT,
          },
          {
            .cid = 1,
            .dt = IMX386_CSI_PD_ISTATS,
            .decode_format = CSI_DECODE_10BIT,
          },

        },
        .pix_data_fmt =
        {
          SENSOR_BAYER,
          SENSOR_META,
        },
      },
    },
    .size = 1,
  },
  .start_settings =
  {
    .reg_setting_a = START_REG_ARRAY,
    .addr_type = CAMERA_I2C_WORD_ADDR,
    .data_type = CAMERA_I2C_BYTE_DATA,
    .delay = 0,
  },
  .stop_settings =
  {
    .reg_setting_a = STOP_REG_ARRAY,
    .addr_type = CAMERA_I2C_WORD_ADDR,
    .data_type = CAMERA_I2C_BYTE_DATA,
    .delay = 0,
  },
  .groupon_settings =
  {
    .reg_setting_a = GROUPON_REG_ARRAY,
    .addr_type = CAMERA_I2C_WORD_ADDR,
    .data_type = CAMERA_I2C_BYTE_DATA,
    .delay = 0,
  },
  .groupoff_settings =
  {
    .reg_setting_a = GROUPOFF_REG_ARRAY,
    .addr_type = CAMERA_I2C_WORD_ADDR,
    .data_type = CAMERA_I2C_BYTE_DATA,
    .delay = 0,
  },
  .test_pattern_info =
  {
    .test_pattern_settings =
    {
      {
        .mode = SENSOR_TEST_PATTERN_OFF,
        .settings =
        {
          .reg_setting_a =
          {
            {0x0600, 0x0000, 0x00},
          },
          .size = 1,
          .addr_type = CAMERA_I2C_WORD_ADDR,
          .data_type = CAMERA_I2C_WORD_DATA,
          .delay = 0,
        }
      },
      {
        .mode = SENSOR_TEST_PATTERN_SOLID_COLOR,
        .settings =
        {
          .reg_setting_a =
          {
            {0x0600, 0x0001, 0x00},
          },
          .size = 1,
          .addr_type = CAMERA_I2C_WORD_ADDR,
          .data_type = CAMERA_I2C_WORD_DATA,
          .delay = 0,
        },
      },
      {
        .mode = SENSOR_TEST_PATTERN_COLOR_BARS,
        .settings =
        {
          .reg_setting_a =
          {
            {0x0600, 0x0002, 0x00},
          },
          .size = 1,
          .addr_type = CAMERA_I2C_WORD_ADDR,
          .data_type = CAMERA_I2C_WORD_DATA,
          .delay = 0,
        },
      },
      {
        .mode = SENSOR_TEST_PATTERN_COLOR_BARS_FADE_TO_GRAY,
        .settings =
        {
          .reg_setting_a =
          {
            {0x0600, 0x0003, 0x00},
          },
          .size = 1,
          .addr_type = CAMERA_I2C_WORD_ADDR,
          .data_type = CAMERA_I2C_WORD_DATA,
          .delay = 0,
        },
      },
      {
        .mode = SENSOR_TEST_PATTERN_PN9,
        .settings =
        {
          .reg_setting_a =
          {
            {0x0600, 0x0004, 0x00},
          },
          .size = 1,
          .addr_type = CAMERA_I2C_WORD_ADDR,
          .data_type = CAMERA_I2C_WORD_DATA,
          .delay = 0,
        },
      },
    },
    .size = 5,
    .solid_mode_addr =
    {
      .r_addr = 0x0602,
      .gr_addr = 0x0604,
      .b_addr = 0x0606,
      .gb_addr = 0x0608,
    },
  },
  .init_settings_array =
  {
    .reg_settings =
    {
      {
        .reg_setting_a = INIT0_REG_ARRAY,
        .addr_type = CAMERA_I2C_WORD_ADDR,
        .data_type = CAMERA_I2C_BYTE_DATA,
        .delay = 0,
      },
    },
    .size = 1,
  },
  .res_settings_array =
  {
    .reg_settings =
    {
      /* Res 0 */
      {
        .reg_setting_a = RES0_REG_ARRAY,
        .addr_type = CAMERA_I2C_WORD_ADDR,
        .data_type = CAMERA_I2C_BYTE_DATA,
        .delay = 0,
      },
      /* Res 1 */
      {
        .reg_setting_a = RES1_REG_ARRAY,
        .addr_type = CAMERA_I2C_WORD_ADDR,
        .data_type = CAMERA_I2C_BYTE_DATA,
        .delay = 0,
      },
      /* Res 2 */
      {
        .reg_setting_a = RES2_REG_ARRAY,
        .addr_type = CAMERA_I2C_WORD_ADDR,
        .data_type = CAMERA_I2C_BYTE_DATA,
        .delay = 0,
      },
      /* Res 3 */
      {
        .reg_setting_a = RES3_REG_ARRAY,
        .addr_type = CAMERA_I2C_WORD_ADDR,
        .data_type = CAMERA_I2C_BYTE_DATA,
        .delay = 0,
      },
      /* Res 4 */
      {
        .reg_setting_a = RES4_REG_ARRAY,
        .addr_type = CAMERA_I2C_WORD_ADDR,
        .data_type = CAMERA_I2C_BYTE_DATA,
        .delay = 0,
      },
    },
    .size = 5,
  },
  .out_info_array =
  {
    .out_info =
    {
      /* Res 0 */
      {
        .x_output = 4032,
        .y_output = 3016,
        .line_length_pclk = 4296,
        .frame_length_lines = 3072,
        .vt_pixel_clk = 396000000,
        .op_pixel_clk = 460000000,
        .binning_factor = 1,
        .min_fps = 7.50,
        .max_fps = 30.00,
        .mode = SENSOR_DEFAULT_MODE,
        .offset_x = 0,
        .offset_y = 0,
        .scale_factor = 0.000,
        .is_pdaf_supported = 1,
      },
      /* Res 1 */
      {
        .x_output = 2016,
        .y_output = 1508,
        .line_length_pclk = 4296,
        .frame_length_lines = 1557,
        .vt_pixel_clk = 200670000,
        .op_pixel_clk = 240800000,
        .binning_factor = 2,
        .min_fps = 7.50,
        .max_fps = 30.00,
        .mode = SENSOR_DEFAULT_MODE,
        .offset_x = 0,
        .offset_y = 0,
        .scale_factor = 0.000,
        .is_pdaf_supported = 1,
      },
      /* Res 2 */
      {
        .x_output = 2016,
        .y_output = 1508,
        .line_length_pclk = 2256,
        .frame_length_lines = 1556,
        .vt_pixel_clk = 210600000,
        .op_pixel_clk = 252800000,
        .binning_factor = 1,
        .min_fps = 7.50,
        .max_fps = 60.01,
        .mode = SENSOR_HFR_MODE,
        .offset_x = 0,
        .offset_y = 0,
        .scale_factor = 0.000,
        .is_pdaf_supported = 0,
      },
      /* Res 3 */
      {
        .x_output = 2016,
        .y_output = 1508,
        .line_length_pclk = 2256,
        .frame_length_lines = 1556,
        .vt_pixel_clk = 316000000,
        .op_pixel_clk = 379200000,
        .binning_factor = 2,
        .min_fps = 7.50,
        .max_fps = 90.01,
        .mode = SENSOR_HFR_MODE,
        .offset_x = 0,
        .offset_y = 0,
        .scale_factor = 0.000,
        .is_pdaf_supported = 0,
      },
      /* Res 4 */
      {
        .x_output = 1920,
        .y_output = 1080,
        .line_length_pclk = 2256,
        .frame_length_lines = 1182,
        .vt_pixel_clk = 320000000,
        .op_pixel_clk = 384000000,
        .binning_factor = 2,
        .min_fps = 7.50,
        .max_fps = 120.0,
        .mode = SENSOR_HFR_MODE,
        .offset_x = 0,
        .offset_y = 380,
        .scale_factor = 1.000,
        .is_pdaf_supported = 0,
      },
    },
    .size = 5,
  },
  .csi_params =
  {
    .lane_cnt = 4,
    .settle_cnt = 0xb,
    .is_csi_3phase = 0,
  },
  .csid_lut_params_array =
  {
    .lut_params =
    {
      /* Res 0 */
      {
        .num_cid = 2,
        .vc_cfg_a =
        {
          {
            .cid = 0,
            .dt = CSI_RAW10,
            .decode_format = CSI_DECODE_10BIT
          },
          {
            .cid = 1,
            .dt = IMX386_CSI_PD_ISTATS,
            .decode_format = CSI_DECODE_10BIT
          },
        },
      },
      /* Res 1 */
      {
        .num_cid = 2,
        .vc_cfg_a =
        {
          {
            .cid = 0,
            .dt = CSI_RAW10,
            .decode_format = CSI_DECODE_10BIT
          },
          {
            .cid = 1,
            .dt = IMX386_CSI_PD_ISTATS,
            .decode_format = CSI_DECODE_10BIT
          },
        },
      },
      /* Res 2 */
      {
        .num_cid = 1,
        .vc_cfg_a =
        {
          {
            .cid = 0,
            .dt = CSI_RAW10,
            .decode_format = CSI_DECODE_10BIT
          },
        },
      },
      /* Res 3 */
      {
        .num_cid = 1,
        .vc_cfg_a =
        {
          {
            .cid = 0,
            .dt = CSI_RAW10,
            .decode_format = CSI_DECODE_10BIT
          },
        },
      },
      /* Res 4 */
      {
        .num_cid = 1,
        .vc_cfg_a =
        {
          {
            .cid = 0,
            .dt = CSI_RAW10,
            .decode_format = CSI_DECODE_10BIT
          },
        },
      },
    },
    .size = 5,
  },
  .crop_params_array =
  {
    .crop_params =
    {
      /* Res 0 */
      {
        .top_crop = 0,
        .bottom_crop = 0,
        .left_crop = 0,
        .right_crop = 0,
      },
      /* Res 1 */
      {
        .top_crop = 0,
        .bottom_crop = 0,
        .left_crop = 0,
        .right_crop = 0,
      },
      /* Res 2 */
      {
        .top_crop = 0,
        .bottom_crop = 0,
        .left_crop = 0,
        .right_crop = 0,
      },
      /* Res 3 */
      {
        .top_crop = 0,
        .bottom_crop = 0,
        .left_crop = 0,
        .right_crop = 0,
      },
      /* Res 4 */
      {
        .top_crop = 0,
        .bottom_crop = 0,
        .left_crop = 0,
        .right_crop = 0,
      },
   },
    .size = 5,
  },
  .exposure_func_table =
  {
    .sensor_calculate_exposure = sensor_calculate_exposure,
    .sensor_fill_exposure_array = sensor_fill_exposure_array,
  },
  .meta_data_out_info_array =
  {
    .meta_data_out_info =
    {
      {
        .width = 4032,
        .height = 2,
        .stats_type = PD_STATS,
        .dt = IMX386_CSI_PD_ISTATS,
      },
    },
    .size = 1,
  },
  .sensor_capability = 0,
  .parse_RDI_stats =
  {
    .pd_data_format = SENSOR_STATS_RAW10_11B_CONF_11B_PD,
  },
  .rolloff_config =
  {
    .enable = FALSE,
    .full_size_info =
    {
      .full_size_width = 0,
      .full_size_height = 0,
      .full_size_left_crop = 0,
      .full_size_top_crop = 0,
    },
  },
  .adc_readout_time = 0,
  .sensor_num_fast_aec_frame_skip = 0,
  .sensorlib_pdaf_api =
  {
    .calcdefocus = "imx386_pdaf_calculate_defocus",
    .libname = "\0",
    .pdaf_get_defocus_API = "\0",
    .pdaf_get_version_API = "\0",
    .pdaf_init_API = "\0",
    .pdaf_deinit_API = "\0",
  },
  .noise_coeff = {
    .gradient_S = 3.738032e-06,
    .offset_S = 3.651935e-04,
    .gradient_O = 4.499952e-07,
    .offset_O = -2.968624e-04,
  },
};

#endif /* __IMX386_LIB_H__ */
