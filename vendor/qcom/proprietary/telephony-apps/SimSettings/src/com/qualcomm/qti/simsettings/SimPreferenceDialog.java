/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qualcomm.qti.simsettings;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.uicc.SpnOverride;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


public class SimPreferenceDialog extends Activity {

    private Context mContext;
    private SubscriptionInfo mSubInfoRecord;
    private int mSlotId;
    private int[] mTintArr;
    private String[] mColorStrings;
    private int mTintSelectorPos;
    private SubscriptionManager mSubscriptionManager;
    AlertDialog.Builder mBuilder;
    Button mOkButton;
    View mDialogLayout;
    private final String SIM_NAME = "sim_name";
    private final String TINT_POS = "tint_pos";
    /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
    private int[] mTypeArr;
    private int[] mTypeArrAll;
    private String[] mTypeStrings;
    private int mTypeSelectorPos;
    private TypedArray mTypedArray;
    private final String TYPE_POS = "type_pos";
    private int[] mNetModeArr;
    private String[] mNetModeStrings;
    private String[] mNetModeArray;
    private int mNetModeSelectorPos;
    private final String NETMODE_POS = "netmode_pos";
    private final String SIM_NUMBER = "sim_number";
    private Phone mPhone;
    private int preferredNetworkMode = Phone.PREFERRED_NT_MODE;
    private boolean needSaveNumber=false;
    private final int NUM = 8;
    /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        mContext = this;
        final Bundle extras = getIntent().getExtras();
        mSlotId = extras.getInt(SimSettings.EXTRA_SLOT_ID, -1);
        mSubscriptionManager = SubscriptionManager.from(mContext);
        mSubInfoRecord = mSubscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(mSlotId);
        mTintArr = mContext.getResources().getIntArray(com.android.internal.R.array.sim_colors);
        mColorStrings = mContext.getResources().getStringArray(R.array.color_picker);
        mTintSelectorPos = 0;
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        mTypeArrAll = mContext.getResources().getIntArray(com.android.internal.R.array.sim_icontype_index);
        mTypeArr = new int[NUM];
        int j = 0;
        if (mSlotId == 0) {
            for (int i = 0; i < mTypeArrAll.length; i++) {
                if (i == 1) {
                    continue;
                }
                mTypeArr[j++] = mTypeArrAll[i];
            }
        } else {
            for (int i = 0; i < mTypeArrAll.length; i++) {
                if (i == 0) {
                    continue;
                }
                mTypeArr[j++] = mTypeArrAll[i];
            }
        }

        mTypeStrings = mContext.getResources().getStringArray(R.array.icontype_picker);
        mTypeSelectorPos = 0;
        mTypedArray = mContext.getResources().obtainTypedArray(com.android.internal.R.array.sim_icontype_drawable);

        mNetModeStrings = mContext.getResources().getStringArray(R.array.preferred_network_mode_choices_custom_default);
        mNetModeSelectorPos = 0;
        mNetModeArray = mContext.getResources().getStringArray(R.array.preferred_network_mode_values_custom_default);
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

        mBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = (LayoutInflater)mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDialogLayout = inflater.inflate(R.layout.multi_sim_dialog, null);
        mBuilder.setView(mDialogLayout);

        createEditDialog(bundle);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(TINT_POS, mTintSelectorPos);

        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        savedInstanceState.putInt(TYPE_POS, mTypeSelectorPos);
        savedInstanceState.putInt(NETMODE_POS, mNetModeSelectorPos);
        final EditText numberEditText = (EditText)mDialogLayout.findViewById(R.id.edit_number);
        savedInstanceState.putString(SIM_NUMBER, numberEditText.getText().toString());
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

        final EditText nameText = (EditText)mDialogLayout.findViewById(R.id.sim_name);
        savedInstanceState.putString(SIM_NAME, nameText.getText().toString());

        super.onSaveInstanceState(savedInstanceState);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int pos = savedInstanceState.getInt(TINT_POS);
        final Spinner tintSpinner = (Spinner) mDialogLayout.findViewById(R.id.spinner);
        tintSpinner.setSelection(pos);
        mTintSelectorPos = pos;

        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        int typepos = savedInstanceState.getInt(TYPE_POS);
        final Spinner typeSpinner = (Spinner) mDialogLayout.findViewById(R.id.icon_spinner);
        typeSpinner.setSelection(typepos);
        mTypeSelectorPos = typepos;
        int netmodepos = savedInstanceState.getInt(NETMODE_POS);
        final Spinner netModeSpinner = (Spinner) mDialogLayout.findViewById(R.id.network_mode_spinner);
        netModeSpinner.setSelection(netmodepos);
        mNetModeSelectorPos = netmodepos;
        final EditText numberEditText = (EditText)mDialogLayout.findViewById(R.id.edit_number);
        numberEditText.setText(savedInstanceState.getString(SIM_NUMBER));
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

        EditText nameText = (EditText)mDialogLayout.findViewById(R.id.sim_name);
        nameText.setText(savedInstanceState.getString(SIM_NAME));
    }

    /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
    //Get preferred network mode based on subId
    private int getPreferredNetworkModeForSubId() {
        int[] sId = SubscriptionManager.getSubId(mSlotId);
        final int subId = sId[0];
        int nwMode;

        nwMode = android.provider.Settings.Global.getInt(
                mContext.getContentResolver(),
                android.provider.Settings.Global.PREFERRED_NETWORK_MODE + subId,
                preferredNetworkMode);
        Log.e("SimPreferenceDialog","getPreferredNetworkModeForSubId: phoneNwMode = " + nwMode +
                " subId = "+ subId);
        return nwMode;
    }

    private String getOperatorName(){
        final TelephonyManager tm = (TelephonyManager) mContext.getSystemService(
                Context.TELEPHONY_SERVICE);
        String simCarrierName = tm.getSimOperatorName(mSubInfoRecord
                .getSubscriptionId());

        //get carrier name from sim
        if(TextUtils.isEmpty(simCarrierName)) {
            simCarrierName = TelephonyManager.getDefault().getNetworkOperatorName(
                    mSubInfoRecord.getSubscriptionId());
            if (TextUtils.isEmpty(simCarrierName)) {
                String operator = TelephonyManager.getDefault().getSimOperator(
                        mSubInfoRecord.getSubscriptionId());
                SpnOverride mSpnOverride = new SpnOverride();
                String CarrierName = operator;
                if (mSpnOverride.containsCarrier(CarrierName)) {
                    simCarrierName = mSpnOverride.getSpn(CarrierName);
                } else {
                    simCarrierName = mContext.getResources().getString(
                            R.string.sim_card_number_title, mSlotId + 1);
                }
            }
        }
        return simCarrierName;
    }
    /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

    private void createEditDialog(Bundle bundle) {
        final Resources res = mContext.getResources();
        EditText nameText = (EditText)mDialogLayout.findViewById(R.id.sim_name);
        nameText.setText(mSubInfoRecord.getDisplayName());
        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        String simCarrierName = getOperatorName();
        nameText.setHint(simCarrierName);
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

        final Spinner tintSpinner = (Spinner) mDialogLayout.findViewById(R.id.spinner);
        SelectColorAdapter adapter = new SelectColorAdapter(mContext,
                R.layout.settings_color_picker_item, mColorStrings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tintSpinner.setAdapter(adapter);

        for (int i = 0; i < mTintArr.length; i++) {
            if (mTintArr[i] == mSubInfoRecord.getIconTint()) {
                tintSpinner.setSelection(i);
                mTintSelectorPos = i;
                break;
            }
        }

        tintSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id){
                tintSpinner.setSelection(pos);
                mTintSelectorPos = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
        //icon type
        final Spinner typeSpinner = (Spinner) mDialogLayout.findViewById(R.id.icon_spinner);
        SelectTypeAdapter typeadapter = new SelectTypeAdapter(mContext,
                R.layout.settings_icon_type_picker_item, mTypeStrings);
        typeadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(typeadapter);

        for (int i = 0; i < mTypeArr.length; i++) {
            if (mTypeArr[i] == mSubInfoRecord.getSimIconType()) {
                typeSpinner.setSelection(i);
                mTypeSelectorPos = i;
                break;
            }
        }

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id){
                typeSpinner.setSelection(pos);
                mTypeSelectorPos = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //network mode
        final Spinner netModeSpinner = (Spinner) mDialogLayout.findViewById(R.id.network_mode_spinner);
        SelectNetWorkModeAdapter netmodeadapter = new SelectNetWorkModeAdapter(mContext,
                R.layout.settings_network_mode_picker_item, mNetModeStrings);
        netmodeadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        netModeSpinner.setAdapter(netmodeadapter);

        int currentmode = getPreferredNetworkModeForSubId();
        for (int i = 0; i < mNetModeArray.length; i++) {
            if (Integer.valueOf(mNetModeArray[i]).intValue()==currentmode) {
                netModeSpinner.setSelection(i);
                mNetModeSelectorPos = i;
                break;
            }
        }

        netModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id){
                netModeSpinner.setSelection(pos);
                mNetModeSelectorPos = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        //number
//        final TelephonyManager tm = (TelephonyManager) mContext.getSystemService(
//                Context.TELEPHONY_SERVICE);
//        TextView numberView = (TextView)mDialogLayout.findViewById(R.id.number);
//        final String rawNumber =  tm.getLine1Number(mSubInfoRecord.getSubscriptionId());
//        if (TextUtils.isEmpty(rawNumber)) {
//            numberView.setText(res.getString(com.android.internal.R.string.unknownName));
//        } else {
//            numberView.setText(PhoneNumberUtils.formatNumber(rawNumber));
//        }
        final TelephonyManager tm = (TelephonyManager) mContext.getSystemService(
                Context.TELEPHONY_SERVICE);
        TextView numberView = (TextView)mDialogLayout.findViewById(R.id.number);
        EditText numberEditText = (EditText)mDialogLayout.findViewById(R.id.edit_number);
        final String rawNumber =  tm.getLine1Number(mSubInfoRecord.getSubscriptionId());
        if (TextUtils.isEmpty(rawNumber)) {
            String subNumber=mSubInfoRecord.getNumber();
            if(TextUtils.isEmpty(subNumber)){
                numberView.setVisibility(View.GONE);
                numberEditText.setVisibility(View.VISIBLE);
                numberEditText.setText(subNumber);
                numberEditText.setHint(res.getString(com.android.internal.R.string.unknownName));
                needSaveNumber=true;
            }else{
                numberView.setVisibility(View.VISIBLE);
                numberEditText.setVisibility(View.GONE);
                numberView.setText(PhoneNumberUtils.formatNumber(subNumber));
                needSaveNumber=false;
            }
        } else {
            needSaveNumber=false;
            numberView.setVisibility(View.VISIBLE);
            numberEditText.setVisibility(View.GONE);
            numberView.setText(PhoneNumberUtils.formatNumber(rawNumber));
        }

        //Carrier
//        String simCarrierName = tm.getSimOperatorName(mSubInfoRecord.getSubscriptionId());
//        TextView carrierView = (TextView)mDialogLayout.findViewById(R.id.carrier);
//        carrierView.setText(!TextUtils.isEmpty(simCarrierName) ? simCarrierName :
//                mContext.getString(com.android.internal.R.string.unknownName));
        TextView carrierView = (TextView)mDialogLayout.findViewById(R.id.carrier);
        carrierView.setText(simCarrierName);
        /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/

        mBuilder.setTitle(String.format(res.getString(R.string.sim_editor_title),
                (mSubInfoRecord.getSimSlotIndex() + 1)));

        mBuilder.setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                final EditText nameText = (EditText)mDialogLayout.findViewById(R.id.sim_name);

                String displayName = nameText.getText().toString();
                int subId = mSubInfoRecord.getSubscriptionId();
                mSubInfoRecord.setDisplayName(displayName);
                mSubscriptionManager.setDisplayName(displayName, subId,
                        SubscriptionManager.NAME_SOURCE_USER_INPUT);

                final int tintSelected = tintSpinner.getSelectedItemPosition();
                int subscriptionId = mSubInfoRecord.getSubscriptionId();
                int tint = mTintArr[tintSelected];
                mSubInfoRecord.setIconTint(tint);
                mSubscriptionManager.setIconTint(tint, subscriptionId);

                /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
                //icon type
                final int typeSelected = typeSpinner.getSelectedItemPosition();
                int mSubscriptionId = mSubInfoRecord.getSubscriptionId();
                int type = mTypeArr[typeSelected];
                mSubInfoRecord.setSimIconType(type);
                mSubscriptionManager.setSimIconType(type, mSubscriptionId);

                //network mode
                int buttonNetworkMode;
                buttonNetworkMode = Integer.valueOf(mNetModeArray[mNetModeSelectorPos]).intValue();
                int settingsNetworkMode = getPreferredNetworkModeForSubId();
                boolean isNetWorkModevalid=true;
                int msubId = mSubInfoRecord.getSubscriptionId();
                if (buttonNetworkMode != settingsNetworkMode) {
//                    int modemNetworkMode;
                    // if new mode is invalid ignore it
                    switch (buttonNetworkMode) {
                        case Phone.NT_MODE_WCDMA_PREF:
                        case Phone.NT_MODE_GSM_ONLY:
                        case Phone.NT_MODE_WCDMA_ONLY:
                        case Phone.NT_MODE_GSM_UMTS:
                        case Phone.NT_MODE_CDMA:
                        case Phone.NT_MODE_CDMA_NO_EVDO:
                        case Phone.NT_MODE_EVDO_NO_CDMA:
                        case Phone.NT_MODE_GLOBAL:
                        case Phone.NT_MODE_LTE_CDMA_AND_EVDO:
                        case Phone.NT_MODE_LTE_GSM_WCDMA:
                        case Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA:
                        case Phone.NT_MODE_LTE_ONLY:
                        case Phone.NT_MODE_LTE_WCDMA:
                        case Phone.NT_MODE_TDSCDMA_ONLY:
                        case Phone.NT_MODE_TDSCDMA_WCDMA:
                        case Phone.NT_MODE_LTE_TDSCDMA:
                        case Phone.NT_MODE_TDSCDMA_GSM:
                        case Phone.NT_MODE_LTE_TDSCDMA_GSM:
                        case Phone.NT_MODE_TDSCDMA_GSM_WCDMA:
                        case Phone.NT_MODE_LTE_TDSCDMA_WCDMA:
                        case Phone.NT_MODE_LTE_TDSCDMA_GSM_WCDMA:
                        case Phone.NT_MODE_TDSCDMA_CDMA_EVDO_GSM_WCDMA:
                        case Phone.NT_MODE_LTE_TDSCDMA_CDMA_EVDO_GSM_WCDMA:
                            // This is one of the modes we recognize
                            isNetWorkModevalid = true;
                            break;
                        default:
                            isNetWorkModevalid = false;
                    }
                    // Set the modem network mode
                    if (isNetWorkModevalid) {
                        tm.setPreferredNetworkMode(msubId,buttonNetworkMode);
                    }
                }
                if (needSaveNumber) {
                    final EditText numberEditText = (EditText) mDialogLayout.findViewById(R.id.edit_number);
                    String displayNumber = numberEditText.getText().toString();
                    int subsId = mSubInfoRecord.getSubscriptionId();
                    if (!TextUtils.isEmpty(displayNumber)) {
                        mSubscriptionManager.setDisplayNumber(displayNumber, subsId);
                    }
                }
                /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
                dialog.dismiss();
                finish();
            }
        });

        mBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                finish();
            }
        });

        mBuilder.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
        AlertDialog dialog = mBuilder.create();
        TextWatcher watcher = new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                String displayName = nameText.getText().toString();
                if (displayName.trim().isEmpty()) {
                    mOkButton.setEnabled(false);
                } else {
                    mOkButton.setEnabled(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        };
        nameText.addTextChangedListener(watcher);
        dialog.show();
        mOkButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
    }

    private class SelectColorAdapter extends ArrayAdapter<CharSequence> {
        private Context mContext;
        private int mResId;

        public SelectColorAdapter(
                Context context, int resource, String[] arr) {
            super(context, resource, arr);
            mContext = context;
            mResId = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView;
            final ViewHolder holder;
            Resources res = mContext.getResources();
            int iconSize = res.getDimensionPixelSize(R.dimen.color_swatch_size);
            int strokeWidth = res.getDimensionPixelSize(R.dimen.color_swatch_stroke_width);

            if (convertView == null) {
                // Cache views for faster scrolling
                rowView = inflater.inflate(mResId, null);
                holder = new ViewHolder();
                ShapeDrawable drawable = new ShapeDrawable(new OvalShape());
                drawable.setIntrinsicHeight(iconSize);
                drawable.setIntrinsicWidth(iconSize);
                drawable.getPaint().setStrokeWidth(strokeWidth);
                holder.label = (TextView) rowView.findViewById(R.id.color_text);
                holder.icon = (ImageView) rowView.findViewById(R.id.color_icon);
                holder.swatch = drawable;
                rowView.setTag(holder);
            } else {
                rowView = convertView;
                holder = (ViewHolder) rowView.getTag();
            }

            holder.label.setText(getItem(position));
            holder.swatch.getPaint().setColor(mTintArr[position]);
            holder.swatch.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
            holder.icon.setVisibility(View.VISIBLE);
            holder.icon.setImageDrawable(holder.swatch);
            return rowView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View rowView = getView(position, convertView, parent);
            final ViewHolder holder = (ViewHolder) rowView.getTag();

            if (mTintSelectorPos == position) {
                holder.swatch.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
            } else {
                holder.swatch.getPaint().setStyle(Paint.Style.STROKE);
            }
            holder.icon.setVisibility(View.VISIBLE);
            return rowView;
        }

        private class ViewHolder {
            TextView label;
            ImageView icon;
            ShapeDrawable swatch;
        }
    }

    /* ADD-BEGIN by Dingyi for dualsim 2016/08/03 FR 2655692*/
    private class SelectTypeAdapter extends ArrayAdapter<CharSequence> {
        private Context mContext;
        private int mResId;

        public SelectTypeAdapter(
                Context context, int resource, String[] arr) {
            super(context, resource, arr);
            mContext = context;
            mResId = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView;
            final ViewHolder holder;
            Resources res = mContext.getResources();

            if (convertView == null) {
                // Cache views for faster scrolling
                rowView = inflater.inflate(mResId, null);
                holder = new ViewHolder();
                holder.label = (TextView) rowView.findViewById(R.id.icontype_text);
                holder.icon = (ImageView) rowView.findViewById(R.id.icontype_icon);
                rowView.setTag(holder);
            } else {
                rowView = convertView;
                holder = (ViewHolder) rowView.getTag();
            }
            int resID=mTypedArray.getResourceId(mTypeArr[position], 0);
            Drawable drawable = res.getDrawable(resID);
            holder.swatch = drawable;
            holder.swatch.setTintList(null);
            holder.label.setTextColor(Color.BLACK);
            holder.label.setText(getItem(position));
            holder.icon.setVisibility(View.VISIBLE);
            holder.icon.setImageDrawable(holder.swatch);
            return rowView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View rowView = getView(position, convertView, parent);
            final ViewHolder holder = (ViewHolder) rowView.getTag();
            holder.icon.setVisibility(View.VISIBLE);
            return rowView;
        }

        private class ViewHolder {
            TextView label;
            ImageView icon;
            Drawable swatch;
        }
    }

    //network mode Adapter
    private class SelectNetWorkModeAdapter extends ArrayAdapter<CharSequence> {
        private Context mContext;
        private int mResId;

        public SelectNetWorkModeAdapter(
                Context context, int resource, String[] arr) {
            super(context, resource, arr);
            mContext = context;
            mResId = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater)
                    mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View rowView;
            final ViewHolder holder;
            Resources res = mContext.getResources();

            if (convertView == null) {
                // Cache views for faster scrolling
                rowView = inflater.inflate(mResId, null);
                holder = new ViewHolder();
                holder.label = (TextView) rowView.findViewById(R.id.netmode_text);
                rowView.setTag(holder);
            } else {
                rowView = convertView;
                holder = (ViewHolder) rowView.getTag();
            }

            holder.label.setTextColor(Color.BLACK);
            holder.label.setText(getItem(position));

            return rowView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View rowView = getView(position, convertView, parent);
            final ViewHolder holder = (ViewHolder) rowView.getTag();
            return rowView;
        }

        private class ViewHolder {
            TextView label;
        }
    }
    /* ADD-END by Dingyi for dualsim 2016/08/03 FR 2655692*/
}
