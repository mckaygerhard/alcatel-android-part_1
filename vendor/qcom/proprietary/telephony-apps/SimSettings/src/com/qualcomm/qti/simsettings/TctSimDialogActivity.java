/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.qualcomm.qti.simsettings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.qualcomm.qti.simsettings.TctSimDialog;
import com.qualcomm.qti.simsettings.TctSimDialog.TctSimDialogCloseListener;

public class TctSimDialogActivity extends Activity {
    private static String TAG = "TctSimDialogActivity";
    private Context mContext;
    private TctSimDialogCloseClass mTctSimDialogCloseListener = new TctSimDialogCloseClass();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getApplicationContext();
        final Bundle extras = getIntent().getExtras();
        final int dialogType = extras.getInt(TctSimDialog.DIALOG_TYPE_KEY, TctSimDialog.INVALID_PICK);
        final int prefersim = extras.getInt(TctSimDialog.PREFERRED_SIM,0);
        final boolean restore = extras.getBoolean("RestoreVoiceSmsSubId", false);

        TctSimDialog mTctSimDialog = new TctSimDialog();
        mTctSimDialog.setPreferredSimChangeListener(null);
        mTctSimDialog.setSimDialogCloseListener(mTctSimDialogCloseListener);
        mTctSimDialog.showdialog(this,dialogType,prefersim,restore);
    }
    class TctSimDialogCloseClass implements TctSimDialogCloseListener {
        @Override
        public void TctSimDialogClose() {
            finish();
        }
    }
}
