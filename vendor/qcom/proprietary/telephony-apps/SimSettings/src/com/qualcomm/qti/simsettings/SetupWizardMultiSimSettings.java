package com.qualcomm.qti.simsettings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import android.content.ComponentName;
import android.net.wifi.WifiManager;
import android.content.Context;
import android.content.res.Resources;

import com.android.setupwizard.navigationbar.SetupWizardNavBar;
import com.android.setupwizard.navigationbar.SetupWizardNavBar.NavigationBarListener;
import com.android.setupwizardlib.SetupWizardLayout;
import com.android.setupwizardlib.util.SystemBarHelper;

public class SetupWizardMultiSimSettings extends Activity{
    SimSettings fragment;
    private SetupWizardNavBar mNavigationBar;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        Log.i("com.tct.setupwizard", "SetupWizardMultiSimSettings->onCreate");
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.setup_wizard_qct_new);
        SetupWizardLayout layout = (SetupWizardLayout) findViewById(R.id.setupwizard_layout);
        layout.getNavigationBar().getBackButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        layout.getNavigationBar().getNextButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Util.returnToGoogleSetupWizard(ExperienceImprovementActivity.this);
                Log.i("SetupWizardMultiSimSettings","getNextButton down");
                finishOrNext(RESULT_OK);
            }
        });

        fragment=(SimSettings)getFragmentManager().findFragmentById(
                R.id.MultiSimSettingsFragment);
         SystemBarHelper.hideSystemBars(getWindow());

    }

    @Override
    protected void onApplyThemeResource(Resources.Theme theme, int resid, boolean first) {
        resid = SetupWizardUtils.getTheme(getIntent());
        super.onApplyThemeResource(theme, resid, first);
    }

    public void finishOrNext(int resultCode) {
        if (SetupWizardUtils.isUsingWizardManager(this)) {
            SetupWizardUtils.sendResultsToSetupWizard(this, resultCode);
        } else {
            setResult(resultCode);
            finish();
        }
    }
}
