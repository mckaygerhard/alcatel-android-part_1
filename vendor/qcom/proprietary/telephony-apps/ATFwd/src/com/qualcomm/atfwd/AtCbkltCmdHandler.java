/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2013 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  sheng .chu                                                      */
/*  Email  :  sheng.chu@tcl.com                                               */
/*  Role   :  Protocol                                                        */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :vendor/qcom/proprietary/telephony-apps/ATFwd/src/com/qualcomm/  */
/*             atfwd/AtCbkltCmdHandler.java                                   */
/*                                                                            */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/01/2016|      Danbin.Xu       |     Task-2828809     |Error occurs when */
/*           |                      |                      | input command:A- */
/*           |                      |                      |T+CBKLT=2, porti- */
/*           |                      |                      |ng 528819         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.qualcomm.atfwd;

import android.content.Context;
import android.content.ContentResolver;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.HandlerThread;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.util.Log;
import static android.provider.Settings.System.SCREEN_OFF_TIMEOUT;

public class AtCbkltCmdHandler extends AtCmdBaseHandler implements AtCmdHandler {

    private static final String LOG_TAG = "AtCbkltCmdHandler";

    private static final long SCREEN_TIMEOUT_NEVER = -1;

    //<state> 0 disable
    private static final int SCREEN_DISABLE = 0;
    //<state> 1 enable for the duration specified
    private static final int SCREEN_ENABLE_DURATION = 1;
    //<state> 2 enable indefinitely
    private static final int SCREEN_ENABLE_INDEFINITE = 2;
    //<state> 3 enable for a short duration specified by the UE manufacturer
    private static final int SCREEN_ENABLE_SHORT_DURATION = 3;
    //Screen Time Out enable value
    private  int mScreenTimeout[] = {15,30,60,120,300,600,1800};//15s,30s,1m

    private PowerManager mPowerManager;

    public AtCbkltCmdHandler(Context c) throws AtCmdHandlerInstantiationException
    {
        super(c);
    }

    @Override
    public String getCommandName() {
        return "+CBKLT";
    }

    @Override
    public AtCmdResponse handleCommand(AtCmd cmd) {
        AtCmdResponse ret = null;
        String result = null;
        String tokens[] = cmd.getTokens();
        boolean isAtCmdRespOK = false;
        boolean isSetCmd = false;
        ContentResolver resolver = mContext.getContentResolver();
        mPowerManager = (PowerManager)mContext.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = mPowerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_DIM_WAKE_LOCK,"wakelock");
        Log.d(LOG_TAG, "OpCode == " + cmd.getOpcode());

        switch(cmd.getOpcode()){
            case AtCmd.AT_OPCODE_NA:
            //+CBKLT
            break;
            case AtCmd.ATCMD_OPCODE_NA_EQ_AR://write command
                //+CBKLT=[<state>[,<duration>]]
                try{
                    if (tokens == null || tokens.length == 0) {
                        result = cmd.getAtCmdErrStr(AtCmd.AT_ERR_INCORRECT_PARAMS);
                        break;
                    }
                    // mode Range check
                    Integer screenMode = changeStringToInt(tokens[0]);
                    Integer screenDuration = 0;
                    if (screenMode < SCREEN_DISABLE || screenMode > SCREEN_ENABLE_SHORT_DURATION) {
                        result = cmd.getAtCmdErrStr(AtCmd.AT_ERR_INCORRECT_PARAMS);
                        break;
                    }
                    //the duration specified check
                    if (tokens.length > 1 && tokens[1] != null) {
                        screenDuration = changeStringToInt(tokens[1]);
                        if(!checkDuration(screenDuration)){
                            result = cmd.getAtCmdErrStr(AtCmd.AT_ERR_INCORRECT_PARAMS);
                            break;
                        }
                    }
                    //excute command
                    if (screenMode == SCREEN_DISABLE && screenDuration == 0) {
                        mPowerManager.goToSleep(SystemClock.uptimeMillis());
                    } else if ((screenMode == SCREEN_ENABLE_DURATION || screenMode == SCREEN_ENABLE_SHORT_DURATION)) {
                        if(screenDuration != 0){
                            Settings.System.putLong(resolver, SCREEN_OFF_TIMEOUT, screenDuration * 1000);
                        }
                        if(wl != null){
                            wl.acquire();
                        }
                    } else if (screenMode == SCREEN_ENABLE_INDEFINITE && screenDuration == 0) {
                        Settings.System.putLong(resolver, SCREEN_OFF_TIMEOUT, SCREEN_TIMEOUT_NEVER);
                        if(wl != null){
                            wl.acquire();
                        }
                    } else{
                        result = cmd.getAtCmdErrStr(AtCmd.AT_ERR_INCORRECT_PARAMS);
                        break;
                    }

                    isSetCmd = true;
                    isAtCmdRespOK = true;
                }catch(Exception ex){
                    Log.e(LOG_TAG, "Unable to perfom AT+CBKLT "+ tokens + "Exception : " + ex);
                    result = cmd.getAtCmdErrStr(AtCmd.AT_ERR_OP_NOT_ALLOW);
                }
            break;
            case AtCmd.ATCMD_OPCODE_NA_QU://read command
                //+CBKLT?
                try{
                    boolean screenOn = mPowerManager.isScreenOn();
                    if(screenOn){
                        long currentTimeout = Settings.System.getLong(resolver, SCREEN_OFF_TIMEOUT,15000);
                        if(currentTimeout == SCREEN_TIMEOUT_NEVER){
                            result = SCREEN_ENABLE_INDEFINITE + "";
                        }else{
                            result = SCREEN_ENABLE_DURATION + "," + currentTimeout/1000;
                        }
                    }else{
                        result = SCREEN_DISABLE + "";
                    }
                    isAtCmdRespOK = true;
                }catch(Exception ex){
                    Log.e(LOG_TAG, "Unable to perfom AT+CBKLT "+ tokens + "Exception : " + ex);
                    result = cmd.getAtCmdErrStr(AtCmd.AT_ERR_OP_NOT_ALLOW);
                }
            break;
            case AtCmd.ATCMD_OPCODE_NA_EQ_QU://test command
                //+CBKLT=?
                //0 disable
                //1 enable for the duration specified
                //2 enable indefinitely
                //3 enable for a short duration specified by the UE manufacturer
                result = "(" + SCREEN_DISABLE + "-" + SCREEN_ENABLE_SHORT_DURATION + ")";
                isAtCmdRespOK = true;
                break;
        }

        if (!isSetCmd && isAtCmdRespOK) {
            result = getCommandName() + ": " + result;
        }

        return isAtCmdRespOK ? new AtCmdResponse(AtCmdResponse.RESULT_OK, result) : new AtCmdResponse(AtCmdResponse.RESULT_ERROR, result);
    }

    private int changeStringToInt(String input) {
        Integer output = -1;
        try {
            output = Integer.parseInt(input);
        } catch (NumberFormatException ex) {
            Log.e(LOG_TAG, "Not an Integer: " + ex);
        }
        return output;
    }

    private boolean checkDuration(int input){
        boolean isVaild = false;
        for(int i = 0; i < mScreenTimeout.length; i++){
            if(input == mScreenTimeout[i]){
                isVaild = true;
            }
        }
        return isVaild;
    }

}
