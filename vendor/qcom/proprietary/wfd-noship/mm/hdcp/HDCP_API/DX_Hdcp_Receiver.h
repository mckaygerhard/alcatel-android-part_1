/***************************************************************************
*   Copyright 2015 (C) Discretix Technologies Ltd.
*
*   This software is protected by copyright, international
*   treaties and various patents. Any copy, reproduction or otherwise use of
*   this software must be authorized by Discretix in a license agreement and
*   include this Copyright Notice and any other notices specified
*   in the license agreement. Any redistribution in binary form must be
*   authorized in the license agreement and include this Copyright Notice
*   and any other notices specified in the license agreement and/or in
*   materials provided with the binary distribution.
*
*   Some software modules which may be used or linked, may be subject to
*   respective "open source" license agreement(s), and may be copyrighted by
*   their respective author(s), as detailed in such respective license
*   agreement(s); please refer to those license agreement(s), and to any
*   Copyright file(s) or Copyright notices included in them, for further
*   information with regard to copyright of third parties and other license
*   provisions.
****************************************************************************/

#ifndef _DX_HDCP_RECEIVER_H
#define _DX_HDCP_RECEIVER_H

/*! \file DX_Hdcp_Receiver.h
This module provides HDCP receiver services.
*/

#include "DX_Hdcp_Types.h"

#ifdef __cplusplus
extern "C"
{
#endif
/*! Inits receiver & default parameters values
\parameters:
    notifyCallbackFunction -	Callback function reference for events notification
\return init status
*/
EXTERNAL_API uint32_t DX_HDCP_Rcv_Init(EventCbFunction notifyCallbackFunction);

/*! Opens receiver's connection (listens to transmitter's authentication request). 
\parameters:
    localIpAddr -		Local IP address (4 bytes)
    ctrlPort -			Control port (for HDCP authentication messages)
\return listen status
*/
EXTERNAL_API uint32_t DX_HDCP_Rcv_Listen(uint8_t *localIpAddr, uint32_t ctrlPort);

/*! Decrypts transmitted data. 
\parameters:
    pesPrivateData -	PES private data. If pesPrivateData = NULL input encrypted data will be copied to the output decrypted data as-is
    msgIn -				Input encrypted data
    msgOut -			Output decrypted data (value returned by function)
    msgLen -			Input/Output data length
\return decryption status   
*/
EXTERNAL_API uint32_t DX_HDCP_Rcv_Decrypt(const DxPesData_t pesPrivateData, uint64_t msgIn, uint64_t msgOut, uint32_t msgLen, EDxHdcpStreamType streamType);

/*! Decrypts transmitted data.
\parameters:
    pesPrivateData -	PES private data. If pesPrivateData = NULL input encrypted data will be copied to the output decrypted data as-is
    msgIn -				Input encrypted data
    msgOut -			Output decrypted data (value returned by function)
    msgLen -			Input/Output data length
    msgOutOffset -	    Offset related to the beginning of output data, in bytes
    EDxHdcpStreamType - Stream type (video/audio)
    inputBufferType -	Input buffer type (virtual memory address or shared memory handle)
    outputBufferType -	Output buffer type (virtual memory address or shared memory handle)
\return decryption status
*/
EXTERNAL_API uint32_t DX_HDCP_Rcv_Decrypt2(const DxPesData_t    pesPrivateData, 
                                           uint64_t             msgIn,
                                           uint64_t             msgOut,
                                           uint32_t             msgLen,
                                           uint32_t             msgOutOffset,
                                           EDxHdcpStreamType    streamType,
                                           EDxHdcpBufferType    inputBufferType,
                                           EDxHdcpBufferType    outputBufferType);

/*! Closes receiver's session.
\return close session status
*/
EXTERNAL_API uint32_t DX_HDCP_Rcv_Close_Session();

/*! Closes receiver.
\return close status
*/
EXTERNAL_API uint32_t DX_HDCP_Rcv_Close();

/*! Set system parameter. This API is deprecated and should not be used.
\parameters:
paramID -			Parameter key
paramData -			Parameter value (returned by function)
\return set-parameter status
*/
EXTERNAL_API uint32_t DX_HDCP_Rcv_Set_Parameter(EDxHdcpConfigParam paramID, void *paramData);

/*! Get version
\return receiver's version
*/
EXTERNAL_API char *DX_HDCP_Rcv_Get_Version();

#ifdef  __cplusplus
}
#endif

#endif
