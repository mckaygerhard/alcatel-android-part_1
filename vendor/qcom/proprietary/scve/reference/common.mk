## ------------------------------------------------------------------------------
## Common make configurations for SCVE Reference Apps
## ------------------------------------------------------------------------------

#To compile only in 32bit mode.
LOCAL_MULTILIB := 32

LOCAL_CFLAGS += -Wno-unused-parameter

# Make the hash-style compatible with more devices
LOCAL_LDFLAGS += -Wl,--hash-style=both

ifeq ($(SCVE_MAKE_ANDROID_L), true)
   LOCAL_C_INCLUDES_32 += prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/4.7/include \
           prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/4.7/libs/armeabi-v7a/include
else
   LOCAL_C_INCLUDES    += prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/4.7/include \
                          prebuilts/ndk/current/sources/cxx-stl/gnu-libstdc++/4.7/libs/armeabi-v7a/include
endif

