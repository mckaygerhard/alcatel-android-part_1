extern void assure_dir(char* path);
extern int do_search_path(char **arg, char reply[REPLY_MAX] __unused);
extern int do_prepare_path(char **arg, char reply[REPLY_MAX] __unused);
extern int do_share_link(char **arg, char reply[REPLY_MAX] __unused);
extern int do_disable_share(char **arg, char reply[REPLY_MAX] __unused);


