# All Rights Reserved
#
# PowerMo Confidential
# Copyright 2012-2014 PowerMo information tech ltd all rights reserved.
# The source code contained or described herein and all documents related to
# the source code ("material") are owned by PowerMo information tech ltd or its
# suppliers or licensors.
#
# Title to the material remains with PowerMo information tech ltd or its
# suppliers and licensors. the material contains trade secrets and proprietary
# and confidential information of PowerMo or its suppliers and licensors.
# The material is protected by worldwide copyright and trade secret
# laws and treaty provisions. no part of the material may be used, copied,
# reproduced, modified, published, uploaded, posted, transmitted, distributed,
# or disclosed in any way without PowerMo's prior express written permission.
#
# No license under any patent, copyright, trade secret or other intellectual
# property right is granted to or conferred upon you by disclosure or delivery
# of the materials, either expressly, by implication, inducement, estoppel or
# otherwise. Any license under such intellectual property rights must be
# express and approved by PowerMo in writing.

LOCAL_PATH:= $(call my-dir)

define dump-pm-value
$(patsubst ./%,%, \
  $(shell cd $(PM_LOCAL_PATH) ; echo $(1) > values.txt) \
 )
endef

PM_PREBUILT_SYSAPP := $(patsubst $(LOCAL_PATH)/%,%,$(wildcard $(LOCAL_PATH)/system/app/*.apk))
PM_PREBUILT_SYSPRIVAPP := $(patsubst $(LOCAL_PATH)/%,%,$(wildcard $(LOCAL_PATH)/system/priv-app/*.apk))
PM_PREBUILT_USRAPP := $(patsubst $(LOCAL_PATH)/%,%,$(wildcard $(LOCAL_PATH)/data/app/*.apk))
PM_PREBUILT_LIBS := $(patsubst $(LOCAL_PATH)/%,%,$(wildcard $(LOCAL_PATH)/system/lib/$(TARGET_CPU_ABI)/*.so))
PM_PREBUILT_EXECUTABLES := $(patsubst $(LOCAL_PATH)/%,%,$(wildcard $(LOCAL_PATH)/system/bin/$(TARGET_CPU_ABI)/*))
PM_PREBUILT_JAVA_LIBRARIES := $(patsubst $(LOCAL_PATH)/%,%,$(wildcard $(LOCAL_PATH)/system/framework/*.jar))
PM_PREBUILT_STATIC_JAVA_LIBRARIES := $(patsubst $(LOCAL_PATH)/%,%,$(wildcard $(LOCAL_PATH)/sdk/*.jar))

# automatically imported .sos/binaries/static-jars/framework-jars
##################################################
include $(CLEAR_VARS)

$(foreach t,$(PM_PREBUILT_LIBS), \
$(eval LOCAL_PREBUILT_LIBS += $(subst .so,,$(notdir $(t))):$(t)) \
)

$(foreach t,$(PM_PREBUILT_EXECUTABLES), \
$(eval LOCAL_PREBUILT_EXECUTABLES += $(notdir $(t)):$(t)) \
)

$(foreach t,$(PM_PREBUILT_STATIC_JAVA_LIBRARIES), \
$(eval LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES += $(subst .jar,,$(notdir $(t))):$(t)) \
)

$(foreach t,$(PM_PREBUILT_JAVA_LIBRARIES), \
$(eval LOCAL_PREBUILT_JAVA_LIBRARIES += $(subst .jar,,$(notdir $(t))):$(t)) \
)

include $(BUILD_MULTI_PREBUILT)

# automatically imported system/priv-app
#################################################
$(foreach t,$(PM_PREBUILT_SYSPRIVAPP), \
$(eval include $(CLEAR_VARS)) \
$(eval LOCAL_MODULE := $(subst .apk,,$(notdir $(t)))) \
$(eval LOCAL_SRC_FILES := $(t)) \
$(eval LOCAL_MODULE_TAGS := optional) \
$(eval LOCAL_MODULE_CLASS := APPS) \
$(eval LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)) \
$(eval LOCAL_CERTIFICATE := platform) \
$(eval LOCAL_PRIVILEGED_MODULE := true) \
$(eval include $(BUILD_PREBUILT)) \
)

# automatically imported system/app
#################################################
$(foreach t,$(PM_PREBUILT_SYSAPP), \
$(eval include $(CLEAR_VARS)) \
$(eval LOCAL_MODULE := $(subst .apk,,$(notdir $(t)))) \
$(eval LOCAL_SRC_FILES := $(t)) \
$(eval LOCAL_MODULE_TAGS := optional) \
$(eval LOCAL_MODULE_CLASS := APPS) \
$(eval LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)) \
$(eval LOCAL_CERTIFICATE := platform) \
$(eval include $(BUILD_PREBUILT)) \
)

# automatically imported data/app
#################################################
$(foreach t,$(PM_PREBUILT_USRAPP), \
$(eval include $(CLEAR_VARS)) \
$(eval LOCAL_MODULE := $(subst .apk,,$(notdir $(t)))) \
$(eval LOCAL_SRC_FILES := $(t)) \
$(eval LOCAL_MODULE_TAGS := optional) \
$(eval LOCAL_MODULE_CLASS := APPS) \
$(eval LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)) \
$(eval LOCAL_CERTIFICATE := PRESIGNED) \
$(eval LOCAL_MODULE_PATH := $(TARGET_OUT_DATA_APPS)) \
$(eval include $(BUILD_PREBUILT)) \
)

# automatically imported system/framework
#################################################
$(foreach t,$(LOCAL_PREBUILT_JAVA_LIBRARIES), \
$(eval include $(CLEAR_VARS)) \
$(eval LOCAL_MODULE := $(subst .jar,,$(notdir $(t)))) \
$(eval LOCAL_SRC_FILES := $(t)) \
$(eval LOCAL_MODULE_TAGS := optional) \
$(eval include $(BUILD_JAVA_LIBRARY)) \
)

