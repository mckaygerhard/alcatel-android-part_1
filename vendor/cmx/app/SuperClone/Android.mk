LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, java)
LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res

LOCAL_PRIVILEGED_MODULE := true

#LOCAL_PROGUARD_FLAG_FILES := proguard.flags

#include $(LOCAL_PATH)/version.mk
#LOCAL_AAPT_FLAGS := \
#        --auto-add-overlay \
#        --version-name "$(version_name_package)" \
#        --version-code $(version_code_package) \

LOCAL_PACKAGE_NAME := SuperClone

LOCAL_CERTIFICATE := platform

include $(BUILD_PACKAGE)

