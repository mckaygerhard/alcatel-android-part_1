package com.cmx.cmplus.superclone;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.UserHandle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.cmx.cmplus.SmartContainerConfig;
import com.cmx.cmplus.sdk.CloneHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Settings for clone
 */
public class CloneSettingsActivity extends Activity {

    private List<ApplicationCheckedState> mTempAppsState = new ArrayList<ApplicationCheckedState>();
    private GridView mAppGridView;
    private CheckedAppListAdapter mAppListAdapter = new CheckedAppListAdapter(mTempAppsState);
    private Context mContext;
    private CloneHelper mCloneHelper;
    private Switch mSwitchView;

    private final String TAG = "CloneSettingsActivity";

    private class ApplicationCheckedState{
        public ApplicationCheckedState(ApplicationInfo ai, Intent intent){
            applicationInfo = ai;
            this.checked = false;
            this.intent = intent;
        }
        public ApplicationInfo applicationInfo;
        public boolean checked;
        public Intent intent;
    }

    private class CheckedAppListAdapter extends BaseAdapter {

        private List<ApplicationCheckedState> mAppCheckedStates;

        public CheckedAppListAdapter(List<ApplicationCheckedState> stateList) {
            mAppCheckedStates = stateList;
        }

        @Override
        public int getCount() {
            return mAppCheckedStates.size();
        }

        @Override
        public Object getItem(int i) {
            return mAppCheckedStates.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if( view == null ){
                LayoutInflater inflator = LayoutInflater.from(mContext);
                view = inflator.inflate(R.layout.app_checked_item, null);
                view.setTag(mAppCheckedStates.get(i));
            }

            ImageView iv = (ImageView) view.findViewById(R.id.imageView);
            TextView tv = (TextView) view.findViewById(R.id.textView);
            CheckBox cb = (CheckBox) view.findViewById(R.id.checkBox);
            cb.setTag(mAppCheckedStates.get(i));
            iv.setTag(mAppCheckedStates.get(i));

            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    ApplicationCheckedState state = (ApplicationCheckedState) compoundButton.getTag();
                    state.checked = b;
                }
            });

            cb.setChecked(mAppCheckedStates.get(i).checked);
            int uid = mAppCheckedStates.get(i).applicationInfo.uid;
            String pkg = mAppCheckedStates.get(i).applicationInfo.packageName;
            if (mCloneHelper.isMasterInstance(uid) && mCloneHelper.isPackageCloned(pkg)) {
                cb.setVisibility(View.INVISIBLE);
            } else {
                cb.setVisibility(View.VISIBLE);
            }

            if (mCloneHelper.isCloneInstance(uid)){
                Drawable defaultIcon = mContext.getPackageManager()
                            .getApplicationIcon(mAppCheckedStates.get(i).applicationInfo);
                //iv.setImageDrawable(mCloneHelper.getCloneBadgedIcon(defaultIcon));
                iv.setImageDrawable(defaultIcon);
            } else {
                iv.setImageDrawable(
                        mContext.getPackageManager().getApplicationIcon(mAppCheckedStates.get(i).applicationInfo));
            }
            tv.setText(mContext.getPackageManager().getApplicationLabel(mAppCheckedStates.get(i).applicationInfo));
            iv.setOnClickListener(clickListener);
            return view;
        }

        private View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationCheckedState state = (ApplicationCheckedState) view.getTag();
                if (state == null) {
                    Log.e(TAG,"Error image view on click");
                }else{
                    if (mCloneHelper.isCloneInstance(state.applicationInfo.uid)) {
                        mCloneHelper.startActivityInClone(mContext, state.intent);
                    } else {
                        mContext.startActivity(state.intent);
                    }
                }
            }
        };
    }

    private void refreshView(){
        Log.d(TAG, "onResetSelection ");
        mTempAppsState.clear();
        if (! SmartContainerConfig.WITH_OUT_APP_CLONE ) {
            Intent launcherIntent = new Intent(Intent.ACTION_MAIN);
            launcherIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> resolveInfos = getPackageManager().queryIntentActivities(launcherIntent, 0);
            for (ResolveInfo ri : resolveInfos) {
                if (!mCloneHelper.canPackageBeCloned(ri.activityInfo.packageName)) {
                    continue;
                }

                if (!mCloneHelper.isMasterInstance(ri.activityInfo.applicationInfo.uid)) {
                    continue;
                }
                Intent intent = new Intent(launcherIntent);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setComponent(new ComponentName(ri.activityInfo.packageName, ri.activityInfo.name));
                mTempAppsState.add(new ApplicationCheckedState(ri.activityInfo.applicationInfo, intent));

                if (mCloneHelper.isPackageCloned(ri.activityInfo.packageName)) {
                    ApplicationInfo clonedAi = mCloneHelper.getCloneApplicationInfo(ri.activityInfo.packageName, 0);
                    if (clonedAi != null ) {
                        mTempAppsState.add(new ApplicationCheckedState( clonedAi, intent));
                    }
                }
            }
        }
        mAppListAdapter.notifyDataSetChanged();

        if (SmartContainerConfig.WITH_OUT_APP_CLONE) {
            mSwitchView.setChecked(false);
            mSwitchView.setText("Disabled");
        } else{
            mSwitchView.setChecked(true);
            mSwitchView.setText("Enabled");
        }

    }

    private void onDeleteClone() {
        Log.d(TAG, "onDeleteClone");
        for(ApplicationCheckedState state: mTempAppsState){
            if(state.checked){
                mCloneHelper.deleteCloneForPackage(state.applicationInfo.packageName);
            }
        }
        refreshView();
    }

    private void onCreateClone(){
        Log.d(TAG, "onCreateClone");
        for(ApplicationCheckedState state: mTempAppsState){
            if(state.checked){
                mCloneHelper.createCloneForPackage(state.applicationInfo.packageName);
            }
        }
        refreshView();
    }
    @Override
    protected void onResume() {
        super.onResume();
        SmartContainerConfig.init();
        refreshView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mCloneHelper = CloneHelper.get(mContext);
        setTitle(R.string.clone_box_name);
        setContentView(R.layout.clone_box_settings);
        mAppGridView = (GridView) findViewById(R.id.packages_grid_view);
        mAppGridView.setAdapter(mAppListAdapter);
        Button cloneButton = (Button) findViewById(R.id.clone_button);
        cloneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateClone();
            }
        });

        Button deleteButton = (Button) findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteClone();
            }
        });

        Button resetButton = (Button) findViewById(R.id.reset_button);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshView();
            }
        });

        mSwitchView = (Switch) findViewById(R.id.enable_clone);
        mSwitchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.d(TAG, "onCheckedChanged " + b);
                if (b && SmartContainerConfig.WITH_OUT_APP_CLONE) {
                    mCloneHelper.enableCloneFeature();
                    Toast.makeText(mContext, getString(R.string.reboot_suggestion), Toast.LENGTH_SHORT).show();
                }else if( !b && !SmartContainerConfig.WITH_OUT_APP_CLONE){
                    mCloneHelper.disableCloneFeature();
                    Toast.makeText(mContext, getString(R.string.reboot_suggestion), Toast.LENGTH_SHORT).show();
                }
                refreshView();
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
