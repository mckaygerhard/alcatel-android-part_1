package com.tcl.secretmessage.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesHelper {
	private static final String SHARED_PREFERENCES_KEY = "com.tcl.secretmessage";
	private static final String SECRET_MESSAGE_SWITCH_STATUS = "secret_message_switch_status";
	private static final String SECRET_MESSAGE_CONFIGED_STATUS = "secret_message_configed_status";

	public static boolean getSecretMessageSwitchStatus(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
		        SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
		return sharedPreferences.getInt(SECRET_MESSAGE_SWITCH_STATUS, 0) == 0 ? false
		        : true;
	}

	public static void setSecretMessageSwitchStatus(Context context,
	        boolean value) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
		        SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(SECRET_MESSAGE_SWITCH_STATUS, value ? 1 : 0);
		editor.commit();
	}

	public static boolean getSecretMessageConfigedStatus(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
		        SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
		return sharedPreferences.getBoolean(SECRET_MESSAGE_CONFIGED_STATUS,
		        false);
	}

	public static void setSecretMessageConfigedStatus(Context context,
	        boolean flag) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
		        SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(SECRET_MESSAGE_CONFIGED_STATUS, flag);
		editor.commit();
	}

}
