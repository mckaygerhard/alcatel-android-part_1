package com.tcl.secretmessage.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.tcl.secretmessage.notification.SecretMessageNotification;
import com.tcl.secretmessage.utils.SharedPreferencesHelper;
import com.tcl.secretmessage2.R;

public class MainActivity extends Activity implements
        CompoundButton.OnCheckedChangeListener, OnClickListener {
	private Switch mSwitch;
	private SecretMessageNotification mNotification;
	public static final String TEXTVIEW_UPDATE_ACTION = "android.intent.action.TCL_ENC_TEXTVIEW_UPDATE";
	// private static final String TAG = "MainActivity";
	RelativeLayout help_next;
	RelativeLayout statement;
	ImageButton actionbar_back;
	// public static boolean isFingerprintAuthention = false;
	FingerprintManager mFingerprintManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mSwitch = (Switch) findViewById(R.id.secret_message_swtich);
		mFingerprintManager = getSystemService(FingerprintManager.class);
		mNotification = new SecretMessageNotification(this);
		if (SharedPreferencesHelper.getSecretMessageSwitchStatus(this)) {
			if (mNotification.queryNotification()) {

				mSwitch.setChecked(true);

			} else {
				mSwitch.setChecked(false);
			}

		} else {
			mSwitch.setChecked(false);
		}
		mSwitch.setOnCheckedChangeListener(this);
		statement = (RelativeLayout) findViewById(R.id.statement);
		statement.setOnClickListener(this);
		help_next = (RelativeLayout) findViewById(R.id.help_next);
		help_next.setOnClickListener(this);
		actionbar_back = (ImageButton) findViewById(R.id.actionbar_back);
		actionbar_back.setOnClickListener(this);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			Intent intent = new Intent();
			intent.setAction("com.tct.securitycenter.FingerprintVerify");
			MainActivity.this.startActivityForResult(intent, 100);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		} else {
			mNotification.sendCustomerNotification(SecretMessageNotification.POWER_OFF);
			mNotification.cancelCustomerNotification();
			SharedPreferencesHelper.setSecretMessageSwitchStatus(
			        MainActivity.this, isChecked);
			
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.help_next:
			Intent helperIntent = new Intent(MainActivity.this,
			        HelperActivity.class);
			startActivity(helperIntent);
			break;
		case R.id.statement:
			// Intent statementIntent = new Intent(MainActivity.this,
			// StatementActivity.class);
			// startActivity(statementIntent);
			break;
		case R.id.actionbar_back:
			finish();
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 100 && resultCode == RESULT_OK) {
			SharedPreferencesHelper.setSecretMessageSwitchStatus(
			        MainActivity.this, true);
			mSwitch.setChecked(true);
			mNotification
			        .sendCustomerNotification(SecretMessageNotification.POWER_ON);
		} else {
			SharedPreferencesHelper.setSecretMessageSwitchStatus(
			        MainActivity.this, false);
			mSwitch.setChecked(false);
			Toast.makeText(MainActivity.this, "onAuthentication failed",
			        Toast.LENGTH_SHORT).show();
		}
	}

/*	@Override
	protected void onPause() {
		super.onPause();
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		boolean isScreenOn = pm.isScreenOn();
		if (isScreenOn) {
			finish();
		}
	}*/
}
