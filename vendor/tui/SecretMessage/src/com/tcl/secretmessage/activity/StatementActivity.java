package com.tcl.secretmessage.activity;

import com.tcl.secretmessage2.R;
import com.tcl.secretmessage2.R.id;
import com.tcl.secretmessage2.R.layout;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class StatementActivity extends Activity {
	private ImageButton actionbar_back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statement);
		actionbar_back = (ImageButton) findViewById(R.id.actionbar_back);
		actionbar_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				finish();
			}
		});
	}
}
