package com.tcl.secretmessage.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.service.notification.StatusBarNotification;
import android.support.v7.app.NotificationCompat;

import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.tcl.secretmessage.activity.MainActivity;
import com.tcl.secretmessage.utils.RefClassUtil;
import com.tcl.secretmessage2.R;

public class SecretMessageNotification {
	private Context mContext;
	public static final int POWER_ON = 0;
	public static final int POWER_OFF = 1;
	public static final int WAIT_AUTHENTION = 2;
	public static final int AUTHENTION_OK = 3;
	public static final String KEY = "command";
	private final int NotificationID = 1001;
	public final static String BROADCAST_ACTION = "com.tcl.secretmessage";
	private NotificationManager mManager;

	public SecretMessageNotification(Context context) {
		mContext = context;
		mManager = (NotificationManager) mContext
		        .getSystemService(Context.NOTIFICATION_SERVICE);

	}

	public void sendCustomerNotification(int command) {

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
		        mContext);
		builder.setTicker(mContext.getResources().getString(
		        R.string.notification_hint_power_on));
		builder.setSmallIcon(R.drawable.statusbar);
		
		builder.setLargeIcon(BitmapFactory.decodeResource(
		        mContext.getResources(), R.drawable.ic_launcher));
		builder.setAutoCancel(false);
		builder.setOngoing(true);
		builder.setShowWhen(false);
		RemoteViews remoteViews = new RemoteViews(mContext.getPackageName(),
		        R.layout.notification_customer);
		remoteViews.setImageViewResource(R.id.notification_icon,
		        R.drawable.notification);

		remoteViews.setTextViewText(R.id.notification_title, mContext
		        .getResources().getString(R.string.app_name));

		Intent intent = new Intent();
		intent.setAction(BROADCAST_ACTION);

		switch (command) {
		case POWER_ON:
			remoteViews.setTextViewText(
			        R.id.notification_text,
			        mContext.getResources().getString(
			                R.string.notification_hint_power_on));
			remoteViews.setTextViewText(R.id.notification_button, mContext
			        .getResources().getString(R.string.power_off));
			intent.putExtra("command", POWER_OFF);
			encSwitch(POWER_ON);
			builder.setPriority(Notification.PRIORITY_HIGH);
			Toast.makeText(mContext, RefClassUtil.getInstance().sysGetValue(),
			        Toast.LENGTH_SHORT).show();
			break;
		case POWER_OFF:

			remoteViews.setTextViewText(
			        R.id.notification_text,
			        mContext.getResources().getString(
			                R.string.notification_hint_power_off));
			remoteViews.setTextViewText(R.id.notification_button, mContext
			        .getResources().getString(R.string.power_on));
			intent.putExtra("command", POWER_ON);
			encSwitch(POWER_OFF);
			builder.setPriority(Notification.PRIORITY_MIN);
			Toast.makeText(mContext, RefClassUtil.getInstance().sysGetValue(),
			        Toast.LENGTH_SHORT).show();

			break;
		case WAIT_AUTHENTION:
			remoteViews.setTextViewText(
			        R.id.notification_text,
			        mContext.getResources().getString(
			                R.string.notification_hint_power_off));
			remoteViews.setTextViewText(R.id.notification_button,mContext
			        .getResources().getString(R.string.power_on));
			intent.putExtra("command", WAIT_AUTHENTION);
			builder.setPriority(Notification.PRIORITY_HIGH);
			break;
		}
		PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0,
		        intent, PendingIntent.FLAG_UPDATE_CURRENT);
	
		if (command != WAIT_AUTHENTION) {
			remoteViews.setOnClickPendingIntent(R.id.notification_button,
			        pendingIntent);
		}

		builder.setContent(remoteViews);
		Notification notification = builder.build();
		mManager.notify(NotificationID, notification);

	}

	private void encSwitch(int status) {
		RefClassUtil.getInstance().sysSetValue(status == POWER_ON ? "1" : "0");
		Intent textViewUpdateOnAction = new Intent(
		        MainActivity.TEXTVIEW_UPDATE_ACTION);
		textViewUpdateOnAction.putExtra("tcl_enc_setting",
		        status == POWER_ON ? 1 : 0);
		mContext.sendBroadcast(textViewUpdateOnAction);
	}

	public void cancelCustomerNotification() {
		mManager.cancel(NotificationID);
	}

	public boolean queryNotification() {
		StatusBarNotification[] temp = mManager.getActiveNotifications();
		for (int i = 0; i < temp.length; i++) {
			if (temp[i].getId() == NotificationID) {
				return true;
			} else {
				continue;
			}
		}
		return false;
	}

}
