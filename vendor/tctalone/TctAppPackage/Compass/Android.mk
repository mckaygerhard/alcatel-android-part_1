LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := Compass
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
#[BUGFIX]-MOD-BEGIN BY TCTNB.kang.yu,1/21/2016,PR 1488178
#[BUGFIX]-MOD-BEGIN BY TCTNB.yusong.xuan,02/02/2016,PR 1533523
LOCAL_CERTIFICATE := PRESIGNED
#[BUGFIX]-MOD-END BY TCTNB.yusong.xuan
LOCAL_PRIVILEGED_MODULE := true
#[BUGFIX]-MOD-END BY TCTNB.kang.yu
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_PACKAGE_NAME := CompassRes
LOCAL_MODULE_STEM := Compass-overlay
LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/overlay
IS_INDEPENDENT_APP := true
include $(BUILD_PLF)
include $(BUILD_PACKAGE)
