#####################################################################
# Gallery.apk
LOCAL_PATH := $(call my-dir)

my_archs := arm
my_src_arch := $(call get-prebuilt-src-arch, $(my_archs))
ifeq ($(my_src_arch),arm)
my_src_abi := armeabi-v7a
else ifeq ($(my_src_arch),arm64)
my_src_abi := arm64-v8a
endif

include $(CLEAR_VARS)
LOCAL_MODULE := Gallery
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_BUILT_MODULE_STEM := package.apk
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRIVILEGED_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED

LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))

#LOCAL_REQUIRED_MODULES :=
LOCAL_PREBUILT_JNI_LIBS := \
    @lib/$(my_src_abi)/libjni_eglfence.so \
    @lib/$(my_src_abi)/libjni_filtershow_filters.so \
    @lib/$(my_src_abi)/libjni_jpegstream.so \
    @lib/$(my_src_abi)/librs.convolve3x3.so \
    @lib/$(my_src_abi)/librs.grad.so \
    @lib/$(my_src_abi)/librs.grey.so \
    @lib/$(my_src_abi)/librs.saturation.so \
    @lib/$(my_src_abi)/librs.vignette.so \
    @lib/$(my_src_abi)/librsjni.so \
    @lib/$(my_src_abi)/libRSSupport.so
LOCAL_MODULE_TARGET_ARCH := $(my_src_arch)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_PACKAGE_NAME := GalleryRes
LOCAL_MODULE_STEM := Gallery-overlay
LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/overlay
IS_INDEPENDENT_APP := true
include $(BUILD_PLF)
include $(BUILD_PACKAGE)
