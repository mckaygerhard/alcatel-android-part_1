LOCAL_PATH := $(call my-dir)
ifneq ($(SMCN_ROM_CONTROL_FLAG),true)
include $(CLEAR_VARS)

LOCAL_MODULE := Tethering
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
#MODIFIED-BEGIN by huan_liu, 2016-04-18,BUG-1938770
LOCAL_PRIVILEGED_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_PACKAGE_NAME := TetheringRes
LOCAL_MODULE_STEM := Tethering-overlay
LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/overlay
IS_INDEPENDENT_APP := true
include $(BUILD_PACKAGE)
#MODIFIED-END by huan_liu,BUG-1938770
endif
