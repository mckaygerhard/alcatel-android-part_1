#This file is generated
LOCAL_PATH := $(call my-dir)
ifneq ($(SMCN_ROM_CONTROL_FLAG),true)
include $(CLEAR_VARS)

LOCAL_MODULE := JrdFota
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(patsubst $(LOCAL_PATH)/%,%,$(shell find $(LOCAL_PATH) -name "*.apk"))
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_PRIVILEGED_MODULE := true
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_PACKAGE_NAME := FotaRes
LOCAL_MODULE_STEM := JrdFota-overlay
LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/overlay
IS_INDEPENDENT_APP := true
include $(BUILD_PLF)
include $(BUILD_PACKAGE)
endif
