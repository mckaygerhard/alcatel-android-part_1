#!/bin/sh

final_result=0
failed_tests=()

# Below, we will append filters to the exclusion portion of GTEST_FILTER, so we
# need to guarantee it has one.
if [ -z "$GTEST_FILTER" ]; then
  # If it wasn't set, make it add all tests, and remove none.
  GTEST_FILTER="*-"
# if GTEST_FILTER already has a negative sign, we leave it alone.
elif [ 0 -eq `expr index "$GTEST_FILTER" "-"` ]; then
  # If GTEST_FILTER was set, but does not have a negative sign, add one.  This
  # gives gtest an empty list of tests to skip.
  GTEST_FILTER="$GTEST_FILTER-"
fi

# Execute a command in "adb shell" and capture the result.
adb_shell_run() {
  local test_file=$1
  shift
  if adb shell ls /data/widevine_tests/$test_file; then
    test_file=/data/widevine_tests/$test_file
  else
    test_file=/system/bin/$test_file
    echo $test_file
  fi
  local tmp_log="$OUT/mediadrmtest.log"
  local adb_error="[ADB SHELL] $@ $test_file failed"
  adb shell "GTEST_FILTER=$GTEST_FILTER $@ $test_file" \|\| echo "$adb_error" | tee "$tmp_log"
  ! grep -Fq "$adb_error" "$tmp_log"
  local result=$?
  if [ $result -ne 0 ]; then
    final_result=$result
    failed_tests+=("$adb_error")
  fi
}

# Use Package Manager (via adb shell) to enable or disable DroidGuard.
# Disabling DroidGuard during the test run prevents concurrency issues
# with provisioning status.
set_droidguard() {
  adb shell pm $1 com.google.android.gms/com.google.android.gms.droidguard.DroidGuardService
}

if [ -z "$ANDROID_BUILD_TOP" ]; then
    echo "Android build environment not set"
    exit -1
fi

echo "waiting for device"
ADB_OUTPUT=`adb root && adb wait-for-device remount`
echo $ADB_OUTPUT
if echo $ADB_OUTPUT | grep -qi "verity"; then
  echo
  echo "ERROR: This device has Verity enabled. run_all_unit_tests.sh does not "
  echo "work if Verity is enabled. Please disable Verity with"
  echo "\"adb disable-verity\" and try again."
  exit -1
fi

# Disable DroidGuard to prevent provisioning collisions
set_droidguard disable

adb_shell_run oemcrypto_test \
  GTEST_FILTER="$GTEST_FILTER:*Level1Required" FORCE_LEVEL3_OEMCRYPTO=yes
adb_shell_run oemcrypto_test
adb_shell_run request_license_test
# cdm_extended_duration_test takes >30 minutes to run.
# adb_shell_run cdm_extended_duration_test
adb_shell_run max_res_engine_unittest
adb_shell_run policy_engine_unittest
adb_shell_run libwvdrmmediacrypto_test
adb_shell_run libwvdrmdrmplugin_test
adb_shell_run cdm_engine_test
adb_shell_run cdm_session_unittest
adb_shell_run file_store_unittest
adb_shell_run license_unittest
adb_shell_run initialization_data_unittest
adb_shell_run device_files_unittest
adb_shell_run timer_unittest
adb_shell_run buffer_reader_test
adb_shell_run circular_buffer_test
adb_shell_run entry_writer_test

library_path="/system/vendor/lib/mediadrm/ "
adb_shell_run libwvdrmengine_test LD_LIBRARY_PATH=$library_path

# Re-enable DroidGuard
set_droidguard enable

adb shell am start com.widevine.test/com.widevine.test.MediaDrmAPITest
if [ $final_result -eq 0 ]; then
  echo "MediaDrm unittests completed successfully!"
else
  printf '\n%s\n' "${failed_tests[@]}"
  exit $final_result
fi

