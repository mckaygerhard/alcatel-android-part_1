# ----------------------------------------------------------------
# Builds libcdm.a
#
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_C_INCLUDES := \
    vendor/widevine/libwvdrmengine/cdm/profiler/include \
    vendor/widevine/libwvdrmengine/cdm/core/include \
    vendor/widevine/libwvdrmengine/cdm/include \
    vendor/widevine/libwvdrmengine/oemcrypto/include \
    vendor/widevine/libwvdrmengine/third_party/stringencoders/src

LOCAL_C_INCLUDES += \
    external/jsmn \
    external/protobuf/src \

LOCAL_STATIC_LIBRARIES := libcdm_protos libcrypto_static

SRC_DIR := src
CORE_SRC_DIR := core/src
PROFILER_SRC_DIR := profiler/src

LOCAL_SRC_FILES := \
    $(CORE_SRC_DIR)/buffer_reader.cpp \
    $(CORE_SRC_DIR)/cdm_engine.cpp \
    $(CORE_SRC_DIR)/cdm_session.cpp \
    $(CORE_SRC_DIR)/certificate_provisioning.cpp \
    $(CORE_SRC_DIR)/crypto_session.cpp \
    $(CORE_SRC_DIR)/device_files.cpp \
    $(CORE_SRC_DIR)/initialization_data.cpp \
    $(CORE_SRC_DIR)/license.cpp \
    $(CORE_SRC_DIR)/max_res_engine.cpp \
    $(CORE_SRC_DIR)/oemcrypto_adapter_dynamic.cpp \
    $(CORE_SRC_DIR)/policy_engine.cpp \
    $(CORE_SRC_DIR)/privacy_crypto_openssl.cpp \
    $(SRC_DIR)/wv_content_decryption_module.cpp \
    $(PROFILER_SRC_DIR)/circular_buffer.cpp \
    $(PROFILER_SRC_DIR)/entry_writer.cpp \
    $(PROFILER_SRC_DIR)/profiled_scope.cpp \
    $(PROFILER_SRC_DIR)/profiler.cpp \
    $(PROFILER_SRC_DIR)/call_table.cpp \
    $(PROFILER_SRC_DIR)/call_history.cpp

LOCAL_MODULE := libcdm
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_TARGET_ARCH := arm x86 mips

include $(BUILD_STATIC_LIBRARY)
