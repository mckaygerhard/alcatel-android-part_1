# -------------------------------------------------------------------
# Makes a unit or end to end test.
# test_name must be passed in as the base filename(without the .cpp).
#
$(call assert-not-null,test_name)

include $(CLEAR_VARS)

LOCAL_MODULE := $(test_name)
LOCAL_MODULE_TAGS := tests

LOCAL_SRC_FILES := \
    $(test_src_dir)/$(test_name).cpp \
    ../core/test/config_test_env.cpp \
    ../core/test/http_socket.cpp \
    ../core/test/license_request.cpp \
    ../core/test/test_printers.cpp \
    ../core/test/url_request.cpp

LOCAL_C_INCLUDES := \
    external/gmock/include \
    external/gtest/include \
    external/openssl/include \
    vendor/widevine/libwvdrmengine/android/cdm/test \
    vendor/widevine/libwvdrmengine/cdm/core/include \
    vendor/widevine/libwvdrmengine/cdm/core/test \
    vendor/widevine/libwvdrmengine/cdm/profiler/include \
    vendor/widevine/libwvdrmengine/cdm/profiler/test \
    vendor/widevine/libwvdrmengine/cdm/include \
    vendor/widevine/libwvdrmengine/oemcrypto/include \

LOCAL_C_INCLUDES += external/protobuf/src

LOCAL_STATIC_LIBRARIES := \
    libcdm \
    libcdm_protos \
    libcdm_utils \
    libcrypto_static \
    libjsmn \
    libgmock \
    libgtest \
    libgtest_main \
    libwvlevel3 \

LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libdl \
    liblog \
    libprotobuf-cpp-lite \
    libssl \
    libutils \

LOCAL_CFLAGS += -DUNIT_TEST

LOCAL_MODULE_TARGET_ARCH := arm x86 mips

include $(BUILD_EXECUTABLE)
