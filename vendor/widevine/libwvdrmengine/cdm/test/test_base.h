// Copyright 2015 Google Inc. All Rights Reserved.

#include <gtest/gtest.h>

namespace wvcdm {
class WvCdmTestBase : public ::testing::Test {
 public:
  WvCdmTestBase() {}
  virtual ~WvCdmTestBase() {}

 protected:
  virtual void SetUp() {
    const ::testing::TestInfo* const test_info =
        ::testing::UnitTest::GetInstance()->current_test_info();
    LOGD("Running test %s.%s", test_info->name(),
         test_info->test_case_name());
  }
};
}  // namespace wvcdm
