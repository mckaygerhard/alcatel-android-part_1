// Copyright 2015 Google Inc. All Rights Reserved.

#include <gtest/gtest.h>
#include <stdint.h>

#include "entry_writer.h"

namespace wvcdm {

TEST(EntryWriterTest, ConstructorTest) {
  oemprofiler::EntryWriter writer;

  ASSERT_TRUE(writer.GetData() != nullptr);
  ASSERT_EQ(0u, writer.GetSize());
}

TEST(EntryWriterTest, WriteU8Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(1, writer.WriteU8(0x01));
  ASSERT_EQ(0x01u, writer.GetData()[0]);
}

TEST(EntryWriterTest, WriteU16Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(2, writer.WriteU16(0x0102));
  ASSERT_EQ(0x01u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
}

TEST(EntryWriterTest, WriteU32Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(4, writer.WriteU32(0x01020304));
  ASSERT_EQ(0x01u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
  ASSERT_EQ(0x03u, writer.GetData()[2]);
  ASSERT_EQ(0x04u, writer.GetData()[3]);
}

TEST(EntryWriterTest, WriteU64Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(8, writer.WriteU64(0x0102030405060708));
  ASSERT_EQ(0x01u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
  ASSERT_EQ(0x03u, writer.GetData()[2]);
  ASSERT_EQ(0x04u, writer.GetData()[3]);
  ASSERT_EQ(0x05u, writer.GetData()[4]);
  ASSERT_EQ(0x06u, writer.GetData()[5]);
  ASSERT_EQ(0x07u, writer.GetData()[6]);
  ASSERT_EQ(0x08u, writer.GetData()[7]);
}

TEST(EntryWriterTest, WriteVLVAs8Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(1, writer.WriteVLV(0x01));
  ASSERT_EQ(0x01u, writer.GetData()[0]);
}

TEST(EntryWriterTest, WriteVLVAs16Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(2, writer.WriteVLV(0x0102));
  ASSERT_EQ(0x01u | 0x20u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
}

TEST(EntryWriterTest, WriteVLVAs24Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(3, writer.WriteVLV(0x010203));
  ASSERT_EQ(0x01u | 0x40u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
  ASSERT_EQ(0x03u, writer.GetData()[2]);
}

TEST(EntryWriterTest, WriteVLVAs32Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(4, writer.WriteVLV(0x01020304));
  ASSERT_EQ(0x01u | 0x60u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
  ASSERT_EQ(0x03u, writer.GetData()[2]);
  ASSERT_EQ(0x04u, writer.GetData()[3]);
}

TEST(EntryWriterTest, WriteVLVAs40Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(5, writer.WriteVLV(0x0102030405));
  ASSERT_EQ(0x01u | 0x80u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
  ASSERT_EQ(0x03u, writer.GetData()[2]);
  ASSERT_EQ(0x04u, writer.GetData()[3]);
  ASSERT_EQ(0x05u, writer.GetData()[4]);
}

TEST(EntryWriterTest, WriteVLVAs48Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(6, writer.WriteVLV(0x010203040506));
  ASSERT_EQ(0x01u | 0xA0u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
  ASSERT_EQ(0x03u, writer.GetData()[2]);
  ASSERT_EQ(0x04u, writer.GetData()[3]);
  ASSERT_EQ(0x05u, writer.GetData()[4]);
  ASSERT_EQ(0x06u, writer.GetData()[5]);
}

TEST(EntryWriterTest, WriteVLVAs56Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(7, writer.WriteVLV(0x01020304050607));
  ASSERT_EQ(0x01u | 0xC0u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
  ASSERT_EQ(0x03u, writer.GetData()[2]);
  ASSERT_EQ(0x04u, writer.GetData()[3]);
  ASSERT_EQ(0x05u, writer.GetData()[4]);
  ASSERT_EQ(0x06u, writer.GetData()[5]);
  ASSERT_EQ(0x07u, writer.GetData()[6]);
}

TEST(EntryWriterTest, WriteVLVAs64Test) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(8, writer.WriteVLV(0x0102030405060708));
  ASSERT_EQ(0x01u | 0xE0u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
  ASSERT_EQ(0x03u, writer.GetData()[2]);
  ASSERT_EQ(0x04u, writer.GetData()[3]);
  ASSERT_EQ(0x05u, writer.GetData()[4]);
  ASSERT_EQ(0x06u, writer.GetData()[5]);
  ASSERT_EQ(0x07u, writer.GetData()[6]);
  ASSERT_EQ(0x08u, writer.GetData()[7]);
}

TEST(EntryWriterTest, ClearTest) {
  oemprofiler::EntryWriter writer;

  ASSERT_EQ(0u, writer.GetSize());

  ASSERT_EQ(1, writer.WriteU8(0x01));
  ASSERT_EQ(1u, writer.GetSize());

  ASSERT_EQ(1, writer.WriteU8(0x02));
  ASSERT_EQ(2u, writer.GetSize());

  ASSERT_EQ(1, writer.WriteU8(0x03));
  ASSERT_EQ(3u, writer.GetSize());

  ASSERT_EQ(1, writer.WriteU8(0x04));
  ASSERT_EQ(4u, writer.GetSize());

  writer.Clear();

  ASSERT_EQ(0u, writer.GetSize());

  //Clear should not clear the data form the buffer
  ASSERT_EQ(0x01u, writer.GetData()[0]);
  ASSERT_EQ(0x02u, writer.GetData()[1]);
  ASSERT_EQ(0x03u, writer.GetData()[2]);
  ASSERT_EQ(0x04u, writer.GetData()[3]);
}

}  // namespace
