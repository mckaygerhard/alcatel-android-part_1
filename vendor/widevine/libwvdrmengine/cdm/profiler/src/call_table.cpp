#include "call_table.h"

#include <limits>

#include "entry_writer.h"

namespace wvcdm {
namespace oemprofiler {

namespace {
  const uint8_t kOutputVersionNumber = 0x00;
}

CallTable::Row::Row() :
    min_(std::numeric_limits<uint64_t>::max()),
    max_(0),
    sample_size_(0),
    mean_(0),
    variance_m_(0),
    variance_s_(0) {

}

void CallTable::Row::Add(uint64_t value) {
  min_ = std::min(min_, value);
  max_ = std::max(max_, value);

  mean_ = ((mean_ * sample_size_) + value) / (sample_size_ + 1.0);
  sample_size_ += 1;

  // Welford's method for standard deviation and variance
  const double old_m = variance_m_;
  const double old_s = variance_s_;
  variance_m_ = old_m + (value - old_m) / sample_size_;
  variance_s_ = old_s + (value - variance_m_) * (value -old_m);
}

uint64_t CallTable::Row::GetSampleSize() const {
  return sample_size_;
}

uint64_t CallTable::Row::GetMin() const {
  return min_;
}

uint64_t CallTable::Row::GetMax() const {
  return max_;
}

double CallTable::Row::GetMean() const {
  return mean_;
}

double CallTable::Row::GetVariance() const {
  return sample_size_ > 1 ? variance_s_ / (sample_size_ - 1) : 0;
}

const CallTable::Row* CallTable::LookUp(uint64_t row_id) const {
  return map_.count(row_id) == 0 ? NULL : &map_.find(row_id)->second;
}

void CallTable::Write(uint64_t row_id, uint64_t sample) {
  if (map_.count(row_id) == 0) {
     map_.insert(std::pair<uint64_t, Row>(row_id, Row()));
  }

  map_[row_id].Add(sample);
}

void CallTable::Read(std::vector<uint8_t>& output) const {
  output.push_back(kOutputVersionNumber);

  for (std::map<uint64_t, Row>::const_iterator it = map_.begin();
      it != map_.end(); ++it) {

    const uint64_t values_to_write[] = {
      it->first,
      it->second.GetSampleSize(),
      it->second.GetMin(),
      it->second.GetMax(),
      static_cast<uint64_t>(it->second.GetMean()),
      // get the decimal places to the one-hundredths
      static_cast<uint64_t>(it->second.GetMean() * 100) % 100,
      static_cast<uint64_t>(it->second.GetVariance()),
      // get the decimal places to the one-hundredths
      static_cast<uint64_t>(it->second.GetVariance() * 100) % 100
    };

    const size_t kValuesToWriteSize = 8;

    for (size_t i = 0; i < kValuesToWriteSize; i++) {
      EntryWriter writer;
      writer.WriteVLV(values_to_write[i]);

      for (size_t w_i = 0; w_i < writer.GetSize(); w_i++) {
        output.push_back(writer.GetData()[w_i]);
      }
    }
  }
}

}  // namespace oemprofiler
}  // namespace wvcdm
