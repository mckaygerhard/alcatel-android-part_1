// Copyright 2016 Google Inc. All Rights Reserved.

#include "profiler.h"

namespace wvcdm {
namespace oemprofiler {

CallTable Profiler::global_table_;
CallHistory Profiler::global_history_;

CallTable& Profiler::GetTable() {
  return global_table_;
}

CallHistory& Profiler::GetHistory() {
  return global_history_;
}

}  // namespace wvcdm
}  // namespace oemprofiler

