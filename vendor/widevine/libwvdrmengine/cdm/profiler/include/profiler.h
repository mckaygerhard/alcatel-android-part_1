// Copyright 2016 Google Inc. All Rights Reserved.

#ifndef WVCDM_PROFILER_H_
#define WVCDM_PROFILER_H_

#include "call_table.h"
#include "call_history.h"

namespace wvcdm {
namespace oemprofiler {

class Profiler {
 public:
  static CallTable& GetTable();
  static CallHistory& GetHistory();

 private:
  static CallTable global_table_;
  static CallHistory global_history_;
};

}  // namespace oemprofiler
}  // namespace wvcdm

#endif
