#ifndef WVCDM_PROFILER_CALL_TABLE_H_
#define WVCDM_PROFILER_CALL_TABLE_H_

#include <map>
#include <stdint.h>
#include <vector>

namespace wvcdm {
namespace oemprofiler {

class CallTable {
 public:
  class Row {
   public:
    Row();
    void Add(uint64_t sample);

    uint64_t GetSampleSize() const;

    uint64_t GetMin() const;
    uint64_t GetMax() const;

    double GetMean() const;
    double GetVariance() const;

   private:
    uint64_t min_;
    uint64_t max_;
    uint64_t sample_size_;
    double mean_;
    double variance_m_;
    double variance_s_;

  };

  const Row* LookUp(uint64_t row_id) const;

  void Write(uint64_t row_id, uint64_t sample);

  void Read(std::vector<uint8_t>& output) const;

 private:
  std::map<uint64_t, Row> map_;
};

}  // namespace oemprofiler
}  // namespace wvcdm

#endif
