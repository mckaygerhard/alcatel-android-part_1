// Copyright 2016 Google Inc. All Rights Reserved.

#ifndef WVCDM_PROFILER_PROFILED_SCOPE_H_
#define WVCDM_PROFILER_PROFILED_SCOPE_H_

#include <stdint.h>

#include "entry_writer.h"
#include "oem_functions.h"

namespace wvcdm {
namespace oemprofiler {

class ProfiledScope {
 public:
  explicit ProfiledScope(OEM_FUNCTION fid);
 ~ProfiledScope();

  EntryWriter meta_data_;

 private:
  OEM_FUNCTION fid_;

  uint64_t start_time_;

  void Submit(uint64_t end_time) const;
  uint64_t GetNowUS() const;

  // disallow copy and assign
  ProfiledScope(const ProfiledScope&);
  void operator=(const ProfiledScope&);
};

} // namespace oemprofiler
} // namespace wvcdm
#endif
