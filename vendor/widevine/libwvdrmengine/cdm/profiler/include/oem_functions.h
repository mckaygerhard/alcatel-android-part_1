// Copyright 2016 Google Inc. All Rights Reserved.

#ifndef WVCDM_PROFILER_OEM_FUNCTIONS_H_
#define WVCDM_PROFILER_OEM_FUNCTIONS_H_

namespace wvcdm {
namespace oemprofiler {

enum OEM_FUNCTION {
  OEM_FUNCTION_OPEN_SESSION = 0,
  OEM_FUNCTION_COPY_BUFFER,
  OEM_FUNCTION_INSTALL_KEYBOX,
  OEM_FUNCTION_IS_KEYBOX_VALID,
  OEM_FUNCTION_GET_DEVICE_ID,
  OEM_FUNCTION_GET_KEY_DATA,
  OEM_FUNCTION_API_VERSION,
  OEM_FUNCTION_SECURITY_PATCH_LEVEL,
  OEM_FUNCTION_SECURITY_LEVEL,
  OEM_FUNCTION_GET_HDCP_CAPABILITIY,
  OEM_FUNCTION_SUPPORTS_USAGE_TABLE,
  OEM_FUNCTION_IS_ANTI_ROLLBACK_HW_PRESENT,
  OEM_FUNCTION_GET_NUMBER_OF_OPEN_SESSIONS,
  OEM_FUNCTION_GET_MAX_NUMBER_OF_SESSIONS,
  OEM_FUNCTION_CLOSE_SESSION,
  OEM_FUNCTION_GENERATE_DERIVED_KEYS,
  OEM_FUNCTION_GENERATE_NONCE,
  OEM_FUNCTION_GENERATE_SIGNATURE,
  OEM_FUNCTION_LOAD_KEYS,
  OEM_FUNCTION_REFRESH_KEYS,
  OEM_FUNCTION_QUERY_KEY_CONTROL,
  OEM_FUNCTION_SELECT_KEY,
  OEM_FUNCTION_DECRYPT_CENC,
  OEM_FUNCTION_WRAP_KEYBOX,
  OEM_FUNCTION_LOAD_TEST_KEYBOX,
  OEM_FUNCTION_GET_RANDOM,
  OEM_FUNCTION_REWRAP_DEVICE_RSA_KEY,
  OEM_FUNCTION_LOAD_DEVICE_RSA_KEY,
  OEM_FUNCTION_LOAD_TEST_RSA_KEY,
  OEM_FUNCTION_GENERATE_RSA_SIGNATURE,
  OEM_FUNCTION_DERIVE_KEYS_FROM_SESSION_KEY,
  OEM_FUNCTION_GENERIC_ENCRYPT,
  OEM_FUNCTION_GENERIC_DECRYPT,
  OEM_FUNCTION_GENERIC_SIGN,
  OEM_FUNCTION_GENERIC_VERIFY,
  OEM_FUNCTION_UPDATE_USAGE_TABLE,
  OEM_FUNCTION_DEACTIVATE_USAGE_ENTRY,
  OEM_FUNCTION_REPORT_USAGE,
  OEM_FUNCTION_DELETE_USAGE_ENTRY,
  OEM_FUNCTION_FORCE_DELETE_USAGE_ENTRY,
  OEM_FUNCTION_DELETE_USAGE_TABLE,

  OEM_FUNCTION_TESTING, // dummy value for testing purposes
  OEM_FUNCTION_COUNT
};

} // namespace oemprofiler
} // namespace wvcdm

#endif
