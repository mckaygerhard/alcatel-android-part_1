LOCAL_PATH:= $(call my-dir)

ifeq ($(filter mips mips64, $(TARGET_ARCH)),)
# Tests need to be compatible with devices that do not support gnu hash-style
LOCAL_LDFLAGS+=-Wl,--hash-style=both
endif

LOCAL_SRC_FILES:= \
  oemcrypto_test.cpp \
  oemcrypto_test_android.cpp \
  oemcrypto_test_main.cpp \

LOCAL_C_INCLUDES += \
  external/gtest/include \
  $(LOCAL_PATH)/../include \
  $(LOCAL_PATH)/../mock/src \
  vendor/widevine/libwvdrmengine/cdm/core/include \
  vendor/widevine/libwvdrmengine/third_party/stringencoders/src \

LOCAL_STATIC_LIBRARIES := \
  libcdm \
  libcdm_utils \
  libcrypto_static \
  libgtest \
  libgtest_main \
  libwvlevel3 \
  libcdm_utils \

LOCAL_SHARED_LIBRARIES := \
  libcutils \
  libdl \
  liblog \
  libutils \
  libz \

