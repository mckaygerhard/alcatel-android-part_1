// Copyright 2015 Google Inc. All Rights Reserved.
//
//  Mock implementation of OEMCrypto APIs
//
//  This file contains oemcrypto engine properties that would be for a
//  level 2 device that does not have persistant storage or a keybox.
//  Note: this is for illustration only.  Production devices are rarely level 2.
#include "oemcrypto_engine_mock.h"

namespace wvoec_mock {

// If local_display() returns true, we pretend we are using a built-in display,
// instead of HDMI or WiFi output.
bool CryptoEngine::local_display() {
  return true;
}

// A closed platform is permitted to use clear buffers.
bool CryptoEngine::closed_platform() {
  return false;
}

// Returns the HDCP version currently in use.
OEMCrypto_HDCP_Capability CryptoEngine::current_hdcp_capability() {
  return local_display() ? HDCP_NO_DIGITAL_OUTPUT : HDCP_V1;
}

// Returns the max HDCP version supported.
OEMCrypto_HDCP_Capability CryptoEngine::maximum_hdcp_capability() {
  return HDCP_NO_DIGITAL_OUTPUT;
}

// Returns true if the client supports persistent storage of
// offline usage table information.
bool CryptoEngine::supports_storage() {
  return false;
}

// Returns true if the client uses a keybox as the root of trust.
bool CryptoEngine::supports_keybox() {
  return false;
}

// Returns true to indicate the client does support anti-rollback hardware.
bool CryptoEngine::is_anti_rollback_hw_present() {
  return false;
}

// Returns "L3" for a software only library.  L1 is for hardware protected keys
// and data paths.  L2 is for hardware protected keys but no data path
// protection.
const char* CryptoEngine::security_level() {
  return "L2";
}

// This should start at 0, and be incremented only when a security patch has
// been applied to the device that fixes a security bug.
uint8_t CryptoEngine::security_patch_level() {
  return 0;
}

}  // namespace wvoec_mock
