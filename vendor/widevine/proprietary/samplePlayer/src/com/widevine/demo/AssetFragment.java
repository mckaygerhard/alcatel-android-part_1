/*
 * (c)Copyright 2011 Google, Inc
 */

package com.widevine.demo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public abstract class AssetFragment extends Fragment {

    public static final String TAG = "WVM Sample Player";

    private int currentPage;
    protected ArrayList<AssetsPage> pages;
    private View mView;

    /** Called when the fragment is first created. */
    @Override
    public View onCreateView(LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mView = inflater.inflate(R.layout.empty, container, false);

        currentPage = 0;
        pages = new ArrayList<AssetsPage>();
        return mView;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (setUpAssetPages()) {
            createView();
        }
    }

    protected abstract boolean setUpAssetPages();

    private void createView() {

        ViewGroup viewGroup = (ViewGroup)mView;
        viewGroup.removeAllViews();

        LinearLayout mainLayout = new LinearLayout(getActivity());

        ImageView empty = new ImageView(getActivity());
        empty.setBackground(getResources().getDrawable(R.drawable.empty,
                        getActivity().getTheme()));

        View[] clips = new View[6];
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDither = true;

        AssetsPage page = pages.get(currentPage);

        for (int i = 0; i < page.getPageCount(); i++) {

            AssetItem assetItem = page.getPage(i);
            clips[i] = createViewItem(getBitmapFromAssetItem(assetItem),
                    assetItem.getAssetPath(), assetItem.getTitle());

        }

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, 1);
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;

        LinearLayout.LayoutParams paramsMain = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, 1);
        paramsMain.gravity = Gravity.CENTER;

        LinearLayout left = new LinearLayout(getActivity());
        left.setOrientation(LinearLayout.VERTICAL);
        if (clips[0] != null) {
            left.addView(clips[0], params);
        }
        if (clips[3] != null) {
            left.addView(clips[3], params);
        } else {
            left.addView(createEmptyView(), params);
        }

        LinearLayout middle = new LinearLayout(getActivity());
        middle.setOrientation(LinearLayout.VERTICAL);
        if (clips[1] != null) {
            middle.addView(clips[1], params);
        }
        if (clips[4] != null) {
            middle.addView(clips[4], params);
        } else {
            middle.addView(createEmptyView(), params);
        }

        LinearLayout right = new LinearLayout(getActivity());
        right.setOrientation(LinearLayout.VERTICAL);
        if (clips[2] != null) {
            right.addView(clips[2], params);
        }
        params.gravity = Gravity.BOTTOM;
        if (clips[5] != null) {
            right.addView(clips[5], params);
        } else {
            right.addView(createEmptyView(), params);
        }
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;

        LinearLayout body = new LinearLayout(getActivity());

        body.addView(left, paramsMain);
        body.addView(middle, paramsMain);
        body.addView(right, paramsMain);

        // Next button listener
        View.OnClickListener nextButtonListener = new View.OnClickListener() {

            public void onClick(View v) {
                Log.d(TAG, "Click next page: " + currentPage);
                if (currentPage < pages.size() - 1) {
                    currentPage++;
                    createView();
                }
            }
        };

        Button next = new Button(getActivity());
        next.setText(R.string.next_page);
        next.setTextSize(14);
        next.setOnClickListener(nextButtonListener);

        // Previous button listener
        View.OnClickListener prevButtonListener = new View.OnClickListener() {

            public void onClick(View v) {

                Log.d(TAG, "Click prev page: " + currentPage);
                if (currentPage > 0) {
                    currentPage--;
                    createView();
                }
            }
        };
        Button prev = new Button(getActivity());
        prev.setText(R.string.previous_page);
        prev.setTextSize(14);
        prev.setOnClickListener(prevButtonListener);

        LinearLayout buttons = new LinearLayout(getActivity());
        buttons.addView(prev, params);
        buttons.addView(next, params);

        body.setBackground(getActivity().getResources().getDrawable(R.drawable.background3,
                getActivity().getTheme()));

        mainLayout.setOrientation(LinearLayout.VERTICAL);
        mainLayout.addView(body, params);

        LinearLayout.LayoutParams paramButtons = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        paramButtons.gravity = Gravity.CENTER;

        mainLayout.addView(buttons, paramButtons);
        viewGroup.addView(mainLayout, paramsMain);
    }

    private View createEmptyView() {
        ImageView empty = new ImageView(getActivity());
        empty.setBackground(getResources().getDrawable(R.drawable.empty, getActivity().getTheme()));

        TextView emptyText = new TextView(getActivity());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
        LinearLayout body = new LinearLayout(getActivity());

        body.setOrientation(LinearLayout.VERTICAL);
        body.addView(empty, params);

        body.addView(emptyText, params);

        return body;
    }

    private View createViewItem(Bitmap image, String path, String title) {

        final String assetPath = path;

        ClipImageView clip = new ClipImageView(getActivity());

        clip.setImageBitmap(image);
        clip.setBackgroundColor(0);

        // Set the onClick listener for each image
        clip.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Log.d(TAG, "Click Asset path: " + assetPath);
                Intent intent = new Intent(getActivity(), VideoPlayerView.class);
                intent.putExtra("com.widevine.demo.Path", assetPath);
                getActivity().startActivity(intent);

            }
        });

        TextView text = new TextView(getActivity());
        text.setText((title == null) ? path : title);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        params.gravity = Gravity.CENTER;
        LinearLayout body = new LinearLayout(getActivity());

        body.setOrientation(LinearLayout.VERTICAL);
        body.addView(clip, params);

        body.addView(text, params);

        return body;

    }

    private Bitmap getBitmapFromAssetItem(AssetItem assetItem) {
        Bitmap clipImage = null;
        String imageUrl = null;

        if (assetItem.getImagePath() == null || assetItem.getImagePath().equals("")) {
            if (!assetItem.getAssetPath().contains("http") && !assetItem.getAssetPath().contains("wvplay"))
                clipImage = BitmapFactory.decodeResource(getResources(), R.drawable.download_clip);
            else
                clipImage = BitmapFactory.decodeResource(getResources(), R.drawable.streaming_clip);
        } else {
            InputStream bitmapStream = null;
            if (assetItem.getImagePath().contains("http")) {

                imageUrl = assetItem.getImagePath();
                if (imageUrl != null) {
                    ImageHandler imageHandler = new ImageHandler(imageUrl);
                    imageHandler.start();
                    try {
                        imageHandler.join();
                    } catch (InterruptedException e) {
                    }

                    clipImage = imageHandler.getBitmap();
                }

            } else {
                try {
                    bitmapStream = new FileInputStream(assetItem.getImagePath());
                } catch (FileNotFoundException e) {
                    bitmapStream = null;
                }

                clipImage = BitmapFactory.decodeStream(bitmapStream);
            }

            if (clipImage == null) {
                clipImage = BitmapFactory.decodeResource(getResources(), R.drawable.streaming_clip);
            }

        }

        return clipImage;
    }

}
