/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2007-2008 Esmertec AG.
 * Copyright (C) 2007-2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/12/2013|       ji.li          |        FR-551269     |auto register for cucc */
/* ----------|----------------------|----------------------|----------------- */
/* 12/18/2013|       ji.li          |        FR-557607     |auto register for ctcc */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.tct.autoregister;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.provider.Telephony.Sms.Intents;
import android.util.Log;
import android.util.TctLog;

//[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
//[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
import android.telephony.SubscriptionManager;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;
import android.telephony.ServiceState;
import android.os.Handler;
import android.net.Uri;
//[BUGFIX]-Add-END by TCTNB.Dandan.Fang

//import com.android.internal.telephony.TelephonyIntents;
/**
 * Handle incoming SMSes. Just dispatches the work off to a Service.
 */
public class SmsRegisterReceiver extends BroadcastReceiver {
    static final Object mStartingServiceSync = new Object();
    static PowerManager.WakeLock mStartingService = null;
    final String Tag = "SmsRegisterReceiver";
    private static SmsRegisterReceiver sInstance;

    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
    //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
    private static int mServiceState_0 = -1;
    private static int mServiceState_1 = -1;
    public static boolean allowedRegistered = false;
    public static boolean getCdmaService = false;
    private static final String TAG = "SmsRegisterReceiver";
    public static final String MESSAGE_SENT_ACTION = "com.android.mms.transaction.MESSAGE_SENT";
    static final String OPERATOR_id = "com.android.tct.autoregister.uri";
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    public static SmsRegisterReceiver getInstance() {
        if (sInstance == null) {
            sInstance = new SmsRegisterReceiver();
        }
        return sInstance;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(Tag, "onReceive " + intent.getAction());
        if (context.getResources().getInteger(com.android.internal.R.integer.def_sms_autoregister_operator) == 0)
            return;
        onReceiveWithPrivilege(context, intent, false);
    }

    protected void onReceiveWithPrivilege(Context context, Intent intent, boolean privileged) {
        // If 'privileged' is false, it means that the intent was delivered to the base
        // no-permissions receiver class. If we get an SMS_RECEIVED message that way, it
        // means someone has tried to spoof the message by delivering it outside the normal
        // permission-checked route, so we just ignore it.
        if (!privileged && intent.getAction().equals("android.provider.Telephony.SMS_REGISTER_RECEIVED")) {//[BUGFIX]-Mod by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
            return;
        }

        intent.setClass(context, SmsRegisterService.class);
        intent.putExtra("result", getResultCode());

        if (intent.getAction().equals("com.android.tct.autoregister.AUTO_REGISTER_RETRY_ALARM")) {
            int reqCode = intent.getIntExtra("messageID", 0);
            intent.putExtra("messageID", reqCode);
            intent.putExtra("autoRegisterRetry", true);
            //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
            //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
            beginStartingService(context, intent);
            //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
        }

        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
      //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
        if(TelephonyIntents.ACTION_SERVICE_STATE_CHANGED.equals(intent.getAction()) && allowedRegistered == true ){
            ServiceState ss = ServiceState.newFromBundle(intent.getExtras());
            int newState = ss.getState();
            int subId = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY, SubscriptionManager.INVALID_SUBSCRIPTION_ID);
            int slotId = SubscriptionManager.getSlotId(subId);

            if(slotId == PhoneConstants.SUB1){
                if(newState == ServiceState.STATE_IN_SERVICE && mServiceState_0 != ServiceState.STATE_IN_SERVICE){
                    if(registeredOnCdmaNetwork(ss.getRilVoiceRadioTechnology()) == true){
                        getCdmaService = true;
                        intent.putExtra("subId_cdma_in_service", subId);
                        beginStartingService(context, intent);
                    }else{
                        Log.i(TAG,"in service, but no on cdma network in slot1, abandon.");
                    }
                }else{
                    Log.i(TAG,"Still no in service in slot1");
                }
                mServiceState_0 = newState;
            }else if(slotId == PhoneConstants.SUB2){
                if(newState == ServiceState.STATE_IN_SERVICE && mServiceState_1 != ServiceState.STATE_IN_SERVICE){
                    if(registeredOnCdmaNetwork(ss.getRilVoiceRadioTechnology()) == true){
                        getCdmaService = true;
                        intent.putExtra("subId_cdma_in_service", subId);
                        beginStartingService(context, intent);
                    }else{
                        Log.i(TAG,"in service, but no on cdma network in slot2, abandon.");
                    }
                }else{
                    Log.i(TAG,"Still no in service in slot2");
                }
                mServiceState_1 = newState;
            }
        }else if(Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())){
            allowedRegistered = true;
            Log.i(TAG,"receive boot completed, set allowedRegistered as true");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if( getCdmaService == false ){
                        Log.i(TAG,"After power up 3 minutes, no get cdma service.");
                        allowedRegistered = false;
                    }
                }
            }, 7 * 60 * 1000);// moren then 5 minutes
        }else if(MESSAGE_SENT_ACTION.equals(intent.getAction())){
            Uri uri = intent.getData();
            if (uri != null && uri.toString().equals(OPERATOR_id)) {
                Log.i(TAG,"uri is got from SmsRegisterService,start service.");
                beginStartingService(context, intent);
            }
        }else if("android.provider.Telephony.SMS_REGISTER_RECEIVED".equals(intent.getAction())){
            beginStartingService(context, intent);
        }
       //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
    }

    // N.B.: <code>beginStartingService</code> and
    // <code>finishStartingService</code> were copied from
    // <code>com.android.calendar.AlertReceiver</code>. We should
    // factor them out or, even better, improve the API for starting
    // services under wake locks.

    /**
     * Start the service to process the current event notifications, acquiring
     * the wake lock before returning to ensure that the service will run.
     */
    public static void beginStartingService(Context context, Intent intent) {
        synchronized (mStartingServiceSync) {
            context.startService(intent);
        }
    }

    /**
     * Called back by the service when it has finished processing notifications,
     * releasing the wake lock if the service is now stopping.
     */
    public static void finishStartingService(Service service, int startId) {
        service.stopSelfResult(startId);
    }

    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
    //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
    private boolean registeredOnCdmaNetwork(int voiceRat){
        boolean isCdmaEvdo = false;
        switch(voiceRat) {
        case ServiceState.RIL_RADIO_TECHNOLOGY_IS95A:
        case ServiceState.RIL_RADIO_TECHNOLOGY_IS95B:
        case ServiceState.RIL_RADIO_TECHNOLOGY_1xRTT:
        case ServiceState.RIL_RADIO_TECHNOLOGY_EVDO_0:
        case ServiceState.RIL_RADIO_TECHNOLOGY_EVDO_A:
        case ServiceState.RIL_RADIO_TECHNOLOGY_EVDO_B:
        case ServiceState.RIL_RADIO_TECHNOLOGY_EHRPD:
            Log.i(TAG,"voice rat as cdma or evdo");
            isCdmaEvdo = true;
            break;

        }
        return isCdmaEvdo;
    }

    public static void noAllowRegistered(){
        Log.i(TAG,"registeredSuccessful ,set allowedRegistered as false .");
        allowedRegistered = false;
    }
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
}
