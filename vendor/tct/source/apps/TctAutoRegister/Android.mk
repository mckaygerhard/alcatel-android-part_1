# Copyright (C) 2016 Tcl Corporation Limited
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

#LOCAL_SRC_FILES := $(call all-subdir-java-files)
LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := TctAutoRegister
#LOCAL_MODULE_PATH := $(TARGET_OUT_APP_PATH)
#LOCAL_SDK_VERSION := current
#LOCAL_CERTIFICATE := shared
LOCAL_CERTIFICATE := platform

LOCAL_JAVA_LIBRARIES += telephony-common tct.feature_query
#mms-common  tct.framework

#LOCAL_JAVA_LIBRARIES = tct.feature_query tct.framework
#[BUGFIX]-Add-BEGIN by TCTNB.(Ji.Li),12/19/2013, FR-557607,auto
LOCAL_JAVA_LIBRARIES += qcrilhook
#[BUGFIX]-Add-END by TCTNB.(Ji.Li),12/19/2013, FR-557607,auto
include $(BUILD_PLF)
include $(BUILD_PACKAGE)

# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))
