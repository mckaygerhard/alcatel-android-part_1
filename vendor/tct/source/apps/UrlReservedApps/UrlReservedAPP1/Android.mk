LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_PACKAGE_NAME := UrlReservedAPP1
LOCAL_MODULE_PATH := $(TARGET_OUT_APP_PATH)
LOCAL_DEX_PREOPT := false

LOCAL_SDK_VERSION := current

include $(BUILD_PLF)
include $(BUILD_PACKAGE)
include $(call all-makefiles-under,$(LOCAL_PATH))
