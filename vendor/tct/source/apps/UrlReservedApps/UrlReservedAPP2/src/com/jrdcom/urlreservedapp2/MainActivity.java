/******************************************************************************/
/*                                                               Date:10/2012 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* ----------|----------------------|----------------------|----------------- */
/*    date   |        Author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 03/12/2013|qi.zhu                |399977                |Add an URL in mainmenu*/
/* ----------|----------------------|----------------------|----------------- */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.jrdcom.urlreservedapp2;

import java.net.URI;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class MainActivity extends Activity {
    private final String TAG = "urlreservedapp2";
    private String operator_name;
    private String url;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent mIntent = new Intent();
        String mUriString = this.getResources().getString(R.string.def_urlreservedapp2_url);
        if (mUriString == null || mUriString.length() < 2) {
            finish();
        }
        Uri mUri = Uri.parse(mUriString);
        mIntent.setAction(Intent.ACTION_VIEW);
        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mIntent.setData(mUri);
        this.startActivity(mIntent);
        finish();
    }

}
