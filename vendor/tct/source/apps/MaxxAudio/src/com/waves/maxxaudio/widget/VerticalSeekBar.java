/* Copyright (C) 2016 Tcl Corporation Limited */
package com.waves.maxxaudio.widget;

import com.waves.maxxaudio.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;


/**
 * Created by 60537 on 2015/10/24.
 */
public class VerticalSeekBar extends FrameLayout {
//    private static final String TAG = "VerticalSeekBar";

    public static final int ROTATION_ANGLE_CW_90 = 90;
    public static final int ROTATION_ANGLE_CW_270 = 270;

    private int mRotationAngle = ROTATION_ANGLE_CW_90;

    private int mProgressHeight = 0;
    private SeekBarLayout mSeekBar = null;
    private TextView mTitle = null;

    public VerticalSeekBar(Context context) {
        super(context);
        initialize(context, null, 0, 0);
    }

    public VerticalSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs, 0, 0);
    }

    public VerticalSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize(context, attrs, defStyle, 0);
    }

    public VerticalSeekBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(context, attrs, defStyleAttr, defStyleRes);
    }

    private void initialize(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {

        int max = 12;
        int progress = 6;
        Drawable progressDraw = null;
        Drawable thumb = null;

        String title = null;
        int progressPaddingBottom = 2;
        int titleAppearanceResId = R.style.VirticalSeekBarTitleStyle;
        int paddingStart = 45;
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.VerticalSeekBar, defStyleAttr, defStyleRes);
            final int rotationAngle = a.getInteger(R.styleable.VerticalSeekBar_seekBarRotation, 0);
            if (isValidRotationAngle(rotationAngle)) {
                mRotationAngle = rotationAngle;
            }
            progress = a.getInteger(R.styleable.VerticalSeekBar_progress, progress);
            max = a.getInteger(R.styleable.VerticalSeekBar_maxProgress, max);
            progressDraw = a.getDrawable(R.styleable.VerticalSeekBar_progressdraw);
            thumb = a.getDrawable(R.styleable.VerticalSeekBar_thumb);
            title = a.getString(R.styleable.VerticalSeekBar_titleString);
            mProgressHeight = (int)a.getDimension(R.styleable.VerticalSeekBar_progressHeight, mProgressHeight);
            titleAppearanceResId = a.getResourceId(R.styleable.VerticalSeekBar_titleAppearance, titleAppearanceResId);
            progressPaddingBottom = (int)a.getDimension(R.styleable.VerticalSeekBar_progressPaddingBottom, progressPaddingBottom);
            paddingStart = (int)a.getDimension(R.styleable.VerticalSeekBar_progressPaddingStart, paddingStart);
            a.recycle();
        }

        mSeekBar = new SeekBarLayout(context, progressPaddingBottom, paddingStart);
        setProgress(progress);
        setMax(max);
        if (null != thumb) setThumb(thumb);
        if (null != progressDraw) {
            mSeekBar.getSeekBar().setProgressDrawable(progressDraw);
            mSeekBar.getSeekBar().setSplitTrack(false);
        }
        int matchParent = ViewGroup.LayoutParams.WRAP_CONTENT;

        LayoutParams params = new LayoutParams(matchParent, mProgressHeight);
        params.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
        addView(mSeekBar, params);

        initTitle(context, title, titleAppearanceResId);
        this.setEnabled(false);
        this.setClickable(false);
        setPressed(false);
    }

    private void initTitle(Context context, String title, int appearanceResId) {
        mTitle = new TextView(context);
        if (null != title) {
            mTitle.setText(title);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
           // mTitle.setTextAppearance(appearanceResId);
        } else {
            mTitle.setTextAppearance(getContext(), appearanceResId);
        }
        int wrapContent = ViewGroup.LayoutParams.WRAP_CONTENT;
        LayoutParams titleParams = new LayoutParams(wrapContent, wrapContent);
        titleParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        addView(mTitle, titleParams);
    }

    public SeekBar getSeekBar() {
        return mSeekBar.getSeekBar();
    }

    public void setMax(int max) {
        mSeekBar.getSeekBar().setMax(max);
    }

    public int getMax() {
        return mSeekBar.getSeekBar().getMax();
    }
    public void setProgress(int progress) {
        mSeekBar.getSeekBar().setProgress(progress);
    }

    public int getProgress() {
        return mSeekBar.getSeekBar().getProgress();
    }

    public void setThumb(Drawable d) {
        mSeekBar.getSeekBar().setThumb(d);
    }

    public void setOnSeekBarChangeListener(SeekBar.OnSeekBarChangeListener l) {
        mSeekBar.getSeekBar().setOnSeekBarChangeListener(l);
    }

    private static boolean isValidRotationAngle(int angle) {
        return (angle == ROTATION_ANGLE_CW_90 || angle == ROTATION_ANGLE_CW_270);
    }

    private class SeekBarLayout extends FrameLayout {
        private SeekBar mSeekBar = null;
        /**
         * Use to make sure the seekBar will be translated to correct position.
         * <p>
         *     Notes:We get value from {@link com.waves.maxxaudio.R.attr#progressPaddingStart}
         * </p>
         */
        private int mPaddingStart = 45;
        public SeekBarLayout(Context context, int paddingBottom, int paddingStart) {
            super(context);
            mPaddingStart = paddingStart;
            mSeekBar = new SeekBar(context);
            int matchParent = ViewGroup.LayoutParams.MATCH_PARENT;
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(matchParent, matchParent);
            mSeekBar.setPadding(paddingStart, 0, paddingBottom, 0);
            addView(mSeekBar, params);
            applyViewRotation();
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            applyViewRotation(w, h);
        }

		@Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

            final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
            if ((mSeekBar != null) && (widthMode != MeasureSpec.EXACTLY)) {

                mSeekBar.measure(heightMeasureSpec, widthMeasureSpec);
                final int seekBarWidth = mSeekBar.getMeasuredHeight();
                final int seekBarHeight = mSeekBar.getMeasuredWidth();

                final int measuredWidth = ViewCompat.resolveSizeAndState(seekBarWidth, widthMeasureSpec, 0);
                final int measuredHeight = ViewCompat.resolveSizeAndState(seekBarHeight, heightMeasureSpec, 0);

                setMeasuredDimension(measuredWidth, measuredHeight);
            } else {
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            }
        }

        /*package*/
        void applyViewRotation() {
            applyViewRotation(getWidth(), getHeight());
        }

        private void applyViewRotation(int w, int h) {
            SeekBar seekBar = mSeekBar;
            if (seekBar != null) {
                // reset mSeekBar's layout params.
//                final ViewGroup.LayoutParams lp = seekBar.getLayoutParams();
//                lp.width = h;
//                lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
//                seekBar.setLayoutParams(lp);

                if (mRotationAngle == ROTATION_ANGLE_CW_90) {
                    final int paddingEnd = ViewCompat.getPaddingEnd(seekBar);
                    ViewCompat.setRotation(seekBar, ROTATION_ANGLE_CW_90);
                    ViewCompat.setTranslationX(seekBar, -(h - w) / 2);
                    ViewCompat.setTranslationY(seekBar, h / 2 - paddingEnd);
                } else if (mRotationAngle == ROTATION_ANGLE_CW_270) {
                    ViewCompat.setRotation(seekBar, ROTATION_ANGLE_CW_270);
                    ViewCompat.setTranslationX(seekBar, -(h - w) / 2);
                    int height = h / 2 - mPaddingStart;
                    ViewCompat.setTranslationY(seekBar, height);
                }
            }
        }

        public SeekBar getSeekBar() {
            return mSeekBar;
        }
    }
}
