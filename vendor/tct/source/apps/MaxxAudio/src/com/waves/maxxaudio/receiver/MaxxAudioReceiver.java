/* Copyright (C) 2016 Tcl Corporation Limited */
package com.waves.maxxaudio.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

public class MaxxAudioReceiver extends BroadcastReceiver{

	private String HEADSET_NOTIFICATION_SWITCH = "headset_notification_switch";
	private String TAG = "MaxxAudioReceiver";
	private String ACTION = "com.waves.maxxaudio.action.headset_notification_not_show";
	private Context mContext;
	static MaxxAudioReceiverListener mListener = null;

	public interface MaxxAudioReceiverListener {
		void MaxxAudioHeadsetClick(Intent intent);
	}

	public static void registerMaxxAudioHeadsetListener(MaxxAudioReceiverListener callback) {
		mListener = callback;
	}
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		String action = intent.getAction();
		Log.d(TAG, "onReceive action: " + action);
		mContext = context;
		if (action != null) {
			if (ACTION.equals(action)) {
				setHeadsetNotification(false);
                if(mListener!=null) {
                    mListener.MaxxAudioHeadsetClick(intent);
                }
				Log.d(TAG, "set headset notification not show");
			}
		}
	}

	/*
	 * control insert headset notification
	 */
	private void setHeadsetNotification(boolean isChecked) {
		// TODO Auto-generated method stub
		Settings.Global.putInt(mContext.getContentResolver(), HEADSET_NOTIFICATION_SWITCH, isChecked ? 1 : 0);
        Intent headsetIntent = new Intent();
        if (isChecked){
            headsetIntent.setAction("android.intent.action.HEADSET_NOTIFICATION_POPUP");
        } else {
            headsetIntent.setAction("android.intent.action.HEADSET_NOTIFICATION_MISS");
        }
        headsetIntent.putExtra("isHeadsetNotificationON", String.valueOf(isChecked ? 1 : 0));
        mContext.sendBroadcast(headsetIntent);
	}

}
