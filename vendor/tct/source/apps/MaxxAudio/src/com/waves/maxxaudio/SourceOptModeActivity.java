/* Copyright (C) 2016 Tcl Corporation Limited */
package com.waves.maxxaudio;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

import com.waves.maxxclientbase.MaxxHandle;
import com.waves.maxxclientbase.MaxxHandle.CurrentAppChangeListener;
import com.waves.maxxclientbase.MaxxHandle.EffectCreatedListener;
import com.waves.maxxclientbase.MaxxHandle.EffectReleasedListener;
import com.waves.maxxclientbase.MaxxHandle.EnableStateChangeListener;
import com.waves.maxxclientbase.MaxxHandle.MaxxSenseEnableChangeListener;
import com.waves.maxxclientbase.MaxxHandle.MessageListener;
import com.waves.maxxclientbase.MaxxHandle.NotificationBarEnableChangeListener;
import com.waves.maxxclientbase.MaxxHandle.OutputModeChangeListener;
import com.waves.maxxclientbase.MaxxHandle.ParametersChangeListener;
import com.waves.maxxclientbase.MaxxHandle.ServiceConnectListener;
import com.waves.maxxclientbase.MaxxHandle.SoundModeChangeListener;
import com.waves.maxxclientbase.MaxxHandle.TagChangeListener;
import com.waves.maxxutil.MaxxDefines;
import com.waves.maxxutil.MaxxLogger;
import com.waves.maxxutil.MaxxSenseAppInfo;
import com.waves.maxxaudio.widget.VerticalSeekBar;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

public class SourceOptModeActivity extends Activity implements OutputModeChangeListener, SoundModeChangeListener,
ParametersChangeListener, EnableStateChangeListener, MaxxSenseEnableChangeListener, ServiceConnectListener, MessageListener,CurrentAppChangeListener,
NotificationBarEnableChangeListener, TagChangeListener, EffectCreatedListener, EffectReleasedListener, OnClickListener{

	private String TAG = "SourceOptModeActivity_mode_change";
	private String mCurrentAppLabel = "";

	private Vector<SeekBar> mSeekBars;
	private ActionBar mActionBar;

	private VerticalSeekBar mBassPin = null;
    private VerticalSeekBar mTreblePin = null;
    private VerticalSeekBar mStereoPin = null;
    private VerticalSeekBar mRevivePin = null;
    private ImageView mResetBtn;
    protected MaxxHandle mMaxxHandle;
    private SharedPreferences m_SharedPreferences;
    private MenuItem mSaveBtn;

    private HashMap<Integer, Double> mLastSavedValues = new HashMap<>();
    /**
     * Record the current modification.When user click {@link #mSaveBtn}, we will save these modification
     * into {@link #mLastSavedValues} and clear this map.
     */
    private HashMap<Integer, Double> mUnsavedValues = new HashMap<>();

    private int m_nOutputMode = -2;
    private int m_nSoundMode = -2;
    private int m_AudioGenre = 0;
    private int originalSoundMode = -2;

    private final int MAXX_BASS = 0;
    private final int MAXX_TREBLE = 2;
    private final int MAXX_STEREO = 1;
    private final int MAXX_REVIVE = 3;

    private int mSoundMode;
    private boolean mIsNTFOn = false;
    /**
     * True means intercepting the for loop of {@link #updateGUI(int[], double[])}
     * <p>
     *     Notes: we will set value to true at {@link #onDestroy()}
     * </p>
     */
    private boolean isInterception = false;

    private static final String mServiceName = "com.waves.maxxservice.MaxxService";
    private static final String KEY_PREFERENCE_SOUNDMODE = MaxxDefines.KEY_PREFERENCE_SOUNDMODE;
    private static final String KEY_PREFERENCE_IS_SENSE_ENABLED = MaxxDefines.KEY_PREFERENCE_IS_MAXXSENSE_ENABLED;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.mode_knob_page_activity);
		mActionBar = getActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeAsUpIndicator(R.drawable.ic_back_up);
		Intent intent = getIntent();
		mSoundMode = intent.getIntExtra("source_mode", m_nSoundMode);
		Log.d(TAG, "onCreate soundmode is " + mSoundMode);
		m_SharedPreferences = getSharedPreferences("MaxxAudioPreference",MODE_PRIVATE);
		initMaxxHandle();
		initKnobUI();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.source_opt_menu, menu);
		mSaveBtn = menu.findItem(R.id.btn_save);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
            return true;
		case R.id.btn_save:
			Log.d(TAG, "onClick save button" + mUnsavedValues.size());
			if (mUnsavedValues.size() > 0){
                //mSaveBtn.setIcon(R.drawable.ic_save_grey);
                //mResetBtn.setBackgroundResource(R.drawable.ic_reset);
                Toast.makeText(this, R.string.preset_settings_saved_toast, Toast.LENGTH_SHORT).show();
                Set<Integer> keys = mUnsavedValues.keySet();
                for (Integer key : keys) {
                    mLastSavedValues.put(key, mUnsavedValues.get(key));
                }
                saveResetFlag(true);
                Log.d(TAG, "[onClick] save button , clear mUnsavedValues.");
                mUnsavedValues.clear();
            }
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void changeSoundMode(int soundMode) {
        originalSoundMode = GetSoundMode();
        Log.d(TAG, "changeSoundMode soundMode = " + soundMode + "orignal sound mode is " + originalSoundMode);
        SetSoundMode(soundMode);
        if(originalSoundMode != soundMode) {
            mMaxxHandle.SetSoundMode(MaxxDefines.MSG_ASYNC, soundMode);
            mMaxxHandle.PresetSetParameter(MaxxDefines.MSG_ASYNC, MaxxDefines.ALG_MAAP_IPEQ_2_ACTIVE, MaxxDefines.ALG_PARAMETER_IS_ACTIVE, GetOutputMode(), GetSoundMode());
            mMaxxHandle.requestUpdateClientPresetParameters(false);
        }

        if (originalSoundMode != -1) {
        	RestoreParameters(GetOutputMode(), originalSoundMode, m_AudioGenre);
        }
    }
	private void initMaxxHandle() {
		mMaxxHandle = new MaxxHandle(this);
		mMaxxHandle.SetOutputModeChangeListener(this);
        mMaxxHandle.SetSoundModeChangeListener(this);
        mMaxxHandle.SetParametersChangeListener(this);
        mMaxxHandle.SetEnableStateChangeListener(this);
        mMaxxHandle.SetMaxxSenseEnableChangeListener(this);
        mMaxxHandle.SetAlgType(MaxxDefines.ALG_TYPE_MAXXMOBILE2);
        mMaxxHandle.setServiceName(mServiceName);
        mMaxxHandle.SetServiceConnectListener(this);
        mMaxxHandle.SetNotificationBarEnableChangeListener(this);
        mMaxxHandle.SetTagChangeListener(this);
        mMaxxHandle.setEffectCreatedListener(this);
        mMaxxHandle.setEffectReleasedListener(this);
        if (!this.mMaxxHandle.connectService()) {
        	mMaxxHandle.connectService();
        }
	}

	private void initKnobUI() {
		// TODO Auto-generated method stub
		mBassPin = (VerticalSeekBar) findViewById(R.id.btn_maxx_bass);
        mTreblePin = (VerticalSeekBar) findViewById(R.id.btn_maxx_treble);
        mStereoPin = (VerticalSeekBar) findViewById(R.id.btn_maxx_stereo);
        mRevivePin = (VerticalSeekBar) findViewById(R.id.btn_maxx_revive);
        mResetBtn = (ImageView) findViewById(R.id.reset);
        mSeekBars = new Vector<SeekBar>(4);
        mSeekBars.add(mBassPin.getSeekBar());
        mSeekBars.add(mStereoPin.getSeekBar());
        mSeekBars.add(mTreblePin.getSeekBar());
        mSeekBars.add(mRevivePin.getSeekBar());

        for (SeekBar seekBar : mSeekBars) {
            seekBar.setOnSeekBarChangeListener(mSeekBarChangeListener);
        }
        mResetBtn.setOnClickListener(this);


	}

	private SeekBar.OnSeekBarChangeListener mSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			// TODO Auto-generated method stub
			//Log.d(TAG, "onProgressChanged" + "seekBar id = " + id);

		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			int id = mSeekBars.indexOf(seekBar);
			 double value = seekBar.getProgress();
			switch(id) {
			case MAXX_BASS:
				Log.d(TAG, "[onStopTracking]maxx bass value = " + value);
				SetParameter(MaxxDefines.ALG_MAAP_MAXX_BASS_EFFECT, value);
				break;
			case MAXX_TREBLE:
				Log.d(TAG, "[onStopTracking]maxx treble value = " + value);
				SetParameter(MaxxDefines.ALG_MAAP_MAXX_HF_EFFECT, value);
				break;
			case MAXX_REVIVE: // maxxdialog_seekbar:
				Log.d(TAG, "[onStopTracking]maxx revive value = " + value);
				SetParameter(MaxxDefines.ALG_MAAP_CENTER_GAIN_CENTER, value);
				break;
			case MAXX_STEREO: // maxxstereo_seekbar:
				Log.d(TAG, "[onStopTracking]maxx stereo value = " + value);
				SetParameter(MaxxDefines.ALG_MAAP_MAXX_3D_EFFECT, value);
				break;
			}
		}

	};

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		isInterception = true;
		RestoreParameters(GetOutputMode(), GetSoundMode(), m_AudioGenre);
		this.mMaxxHandle.disconnectService();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Log.d(TAG, "onResume");
		super.onResume();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		Log.d(TAG, "[onStart]");
		super.onStart();
		if (null == this.mMaxxHandle.GetAlgType()) {
			RuntimeException var1 = new RuntimeException("derived class must call SetAlgType() in onCreate()");
			var1.printStackTrace();
			throw var1;
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	public int GetSoundMode() {
        return this.m_nSoundMode;
    }

    public void SetSoundMode(int soundMode) {
    	Log.d(TAG, "[SetSoundMode = ] "+ soundMode);
        this.m_nSoundMode = soundMode;
    }

    public int GetOutputMode() {
        return this.m_nOutputMode;
    }

    public void SetOutputMode(int outputMode) {
        this.m_nOutputMode = outputMode;
    }

    public MaxxHandle getMaxxHandle() {
        return mMaxxHandle;
    }

    /**
     * <p>
     *     Notes: Called by {@link #saveResetFlag(boolean)} and {@link #isShowResetButton()}
     * </p>
     * @return a String like ResetFlag_2_3(ResetFlag_ + {@link #GetSoundMode()} + _ + {@link #m_AudioGenre}.
     *          If the sound mode (like MOVIE) has not {@link #m_AudioGenre}, used 0 instead.
     */
    private String getResetFlagKey() {
        StringBuffer buffer = new StringBuffer("ResetFlag");
        Log.d(TAG, "[getResetFlagKey] GetOutputMode()=" + GetOutputMode());
        if (GetOutputMode() != MaxxDefines.OUTPUTMODE_SPEAKER) {
            // We should Add out put mode into the key to distinguish different out put mode
            // which belong to the same sound mode.
            buffer.append(GetOutputMode());
        }
        if (GetSoundMode() == MaxxDefines.SOUNDMODE_MUSIC) {
            //int genre = (int) Math.pow(2, mGenreSpinner.getSelectedItemPosition());
            Log.d(TAG, "[getResetFlagKey] m_AudioGenre=" + m_AudioGenre);
            return buffer.append(GetSoundMode() + "_" + m_AudioGenre).toString();
        }
        return buffer.append(GetSoundMode() + "_0").toString();
    }

    /**
    *
    * @param key a String like {@link #GetSoundMode()} + "_" + {@link #m_AudioGenre}.
    *            If the sound mode (like MOVIE) has not {@link #m_AudioGenre}, used 0 instead.
    * @param value a boolean, true means need to show reset button and false means show hide reset button.
    */
   public void saveBooleanToPref(String key, boolean value) {
       try {
           Log.d(TAG, "[saveBooleanToPref] key=" + key + " | value=" + value);
           m_SharedPreferences.edit().putBoolean(key, value).commit();
       } catch (Exception e) {
           Log.d(TAG, "[saveBooleanToPref] save key(" + key + ") value(" + value + ") Exception.", e);
       }
   }

    /**
    * Call {@link #saveBooleanToPref(String, boolean)} to save value to the key position.
    * We used {@link #getResetFlagKey()} to calculate the key.
    * @param value
    */
   private void saveResetFlag(boolean value) {
       // PR 1275809 Add by chunlin.tang@tcl.com 2016-01-06 Begin
       // Fix: The value of Knob and EQ will be changed to default after reboot device.
       mMaxxHandle.SavePreset(MaxxDefines.MSG_ASYNC);
       // PR 1275809 Add by chunlin.tang@tcl.com 2016-01-06 End
       saveBooleanToPref(getResetFlagKey(), value);
   }
   /**
    * true means need to show reset button. and false means do not show reset button.
    * @return a boolean. if can't find the value or occur exception, return false.
    */
   private boolean isShowResetButton() {
       String key = null;
       try {
           key = getResetFlagKey();
           Log.d(TAG, "[isShowResetButton] key=" + key + " | m_AudioGenre=" + m_AudioGenre + " | GetSoundMode()=" + GetSoundMode());
           return m_SharedPreferences.getBoolean(key, false);
       } catch (Exception e) {
           Log.d(TAG, "[isShowResetButton]  read reset value key(" + key + ") Exception.", e);
       }
       return false;
   }

	private void SetParameter(int nParameter, double dValue) {
        mMaxxHandle.PresetSetParameter(MaxxDefines.MSG_ASYNC, nParameter, dValue, GetOutputMode(), GetSoundMode(), GetSoundMode() == MaxxDefines.SOUNDMODE_MUSIC ? m_AudioGenre : 0);
        // PR 1425600 Modified by chunlin.tang@tcl.com 2016-01-15 Begin
        // Fix: MaxxAudio FC caused by NullPointerException
        // Notes: If Double is null, equal it with double will throw NullPointerException.
        Double last = null;
        if (null != mLastSavedValues) {
            last = mLastSavedValues.get(nParameter);
        }
        Log.d(TAG, "[SetParameter] nParameter=" + nParameter + " | dValue=" + dValue
                + " | mLastSavedValues.get(" + nParameter + ")=" + last);
        if (null == last || dValue != last) {
        // PR 1425600 Modified by chunlin.tang@tcl.com 2016-01-15 End
            //mSaveBtn.setIcon(R.drawable.ic_save);
           //mResetBtn.setBackgroundResource(R.drawable.ic_reset);
            if (!isShowResetButton()){
                saveResetFlag(false);
            }
            mUnsavedValues.put(nParameter, dValue);
        }
    }

	private void RestoreParameters(int outputMode, int soundMode, int tag) {

        Log.d(TAG, "RestoreParameters, outputMode = " + outputMode + ", soundMode = " + soundMode + ", tag = " + tag);

        int num = mUnsavedValues.size();
        int[] restoreParameters = new int[num];
        double[] restoreValues = new double[num];
        Set<Integer> keys = mUnsavedValues.keySet();
        int index = 0;
        for (Integer key : keys) {
            restoreParameters[index] = key;
            if (null != mLastSavedValues.get(key)) {
                restoreValues[index] = mLastSavedValues.get(key);
            }
            index++;
        }

        Log.d(TAG, "restoreParameters, " + Arrays.toString(restoreParameters));
        Log.d(TAG, "restoreValues, " + Arrays.toString(restoreValues));

        if (mUnsavedValues.size() > 0) {
            ///mSaveBtn.setIcon(R.drawable.ic_save_grey);
            Toast.makeText(this, R.string.unsaved_changes_toast, Toast.LENGTH_SHORT).show();
        }
        mUnsavedValues.clear();
        mMaxxHandle.PresetSetParameterArray(MaxxDefines.MSG_ASYNC, restoreParameters.length, restoreParameters, restoreValues, outputMode, soundMode, soundMode == MaxxDefines.SOUNDMODE_MUSIC ? tag : 0);
        Log.d(TAG, "[restoreParameter] setParameterArray soundMode = " + soundMode);
        saveResetFlag(false);
    }

	@Override
	public void OnOutputModeChanged(int nOutputMode) {
		// TODO Auto-generated method stub
		Log.d(TAG, "outModeChange  getOutputMode = " + GetOutputMode() +
				"whether equal = " + (GetOutputMode() != nOutputMode) +  "nOutputMode = " + nOutputMode);
		if (GetOutputMode() != nOutputMode) {
			Log.d(TAG, "onOutPutModeChanged soundMode = " + GetSoundMode() + " to restoreParameters");
            RestoreParameters(nOutputMode, GetSoundMode(), m_AudioGenre);
            SetOutputMode(nOutputMode);

            Log.d(TAG, "onOutputMOdeChanged to presetParameter soundMode = " + GetSoundMode());
            mMaxxHandle.PresetSetParameter(MaxxDefines.MSG_ASYNC, MaxxDefines.ALG_MAAP_IPEQ_2_ACTIVE, MaxxDefines.ALG_PARAMETER_IS_ACTIVE, GetOutputMode(), GetSoundMode());
        }
	}

	@Override
	public void OnSoundModeChanged(int nSoundMode) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onSoundModeChanged" + " soundMode = " + nSoundMode + " getSoundMode = " + GetSoundMode());
		//soundModeChanged(nSoundMode);//chunzhi.sun
		/*
		 * why  callback nSoundMode is wrong , not equal GetSoundMode()/
		 */
		soundModeChanged(nSoundMode);
	}

	private void soundModeChanged(int nSoundMode) {
		Log.d(TAG, "soundModeChanged soundMode = " + nSoundMode);
		SetSoundMode(nSoundMode);
        mMaxxHandle.PresetSetParameter(MaxxDefines.MSG_ASYNC, MaxxDefines.ALG_MAAP_IPEQ_2_ACTIVE, MaxxDefines.ALG_PARAMETER_IS_ACTIVE, GetOutputMode(), GetSoundMode());

        switch (nSoundMode) {
            case MaxxDefines.SOUNDMODE_MUSIC:
                //set different Label
            	mActionBar.setTitle(getResources().getString(R.string.music_mode_title));
                break;
            case MaxxDefines.SOUNDMODE_MOVIE:
            	mActionBar.setTitle(getResources().getString(R.string.video_mode_title));
                break;
            case MaxxDefines.SOUNDMODE_GAMING:
            	mActionBar.setTitle(getResources().getString(R.string.general_mode_titile));
                break;

        }
        m_SharedPreferences.edit().putInt(KEY_PREFERENCE_SOUNDMODE, nSoundMode).apply();
	}

	@Override
	public void OnParametersChanged(int[] anParameter, double[] adValue, int outputMode,
			int soundMode, int tag) {
		// TODO Auto-generated method stub
		Log.d(TAG, "onParametersChanged  soundMode = " + soundMode);
		runUpdateGUIRunnable(anParameter, adValue, outputMode, soundMode, tag);

	}

	private void runUpdateGUIRunnable(final int[] anParameter,
			final double[] adValue, final int outputMode, final int soundMode,
			final int tag) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// The Undo err after reversible device

				if (GetSoundMode() != soundMode
						|| GetOutputMode() != outputMode) {
					Log.d(TAG,
							"[runUpdateGUIRunnable] clear mUnsavedValues firstly!");
					mLastSavedValues.clear();
					mUnsavedValues.clear();

				} else if (mUnsavedValues.size() > 0 && !isShowResetButton()) {
					//mSaveBtn.setIcon(R.drawable.ic_save);
					//mResetBtn.setBackgroundResource(R.drawable.ic_reset);
				}

				updateGUI(anParameter, adValue);
			}
		});
	}

	/**
	 * if {@link #isInterception} is true, we will intercept the for loop.
	 *
	 * @param anParameter
	 * @param adValue
	 */
	private void updateGUI(int[] anParameter, double[] adValue) {
		for (int index = 0; index < anParameter.length; index++) {
			if (isInterception) {
				Log.d(TAG,
						"[updateGUI] isInterception is true, break the for loop.");
				break;
			}
			int nParameter = anParameter[index];
			double dValue = adValue[index];
			//Log.d(TAG, "nParameter = " + nParameter + " dValue = " + dValue);
			mLastSavedValues.put(nParameter, dValue);

			// update corresponding GUI component when parameter is changed
			switch (nParameter) {
			case MaxxDefines.ALG_MAAP_MAXX_BASS_EFFECT:
				//mBassPin.setValue((float) dValue);
				mBassPin.getSeekBar().setProgress((int) dValue);
				break;
			case MaxxDefines.ALG_MAAP_MAXX_HF_EFFECT:
				mTreblePin.getSeekBar().setProgress((int) dValue);
				break;
			case MaxxDefines.ALG_MAAP_CENTER_GAIN_CENTER:
				mRevivePin.getSeekBar().setProgress((int) dValue);
				break;
			case MaxxDefines.ALG_MAAP_MAXX_3D_EFFECT:
				mStereoPin.getSeekBar().setProgress((int) dValue);
				break;
			}
		}
		//doLoading(true, View.GONE);
	}



	@Override
	public void OnEnableChanged(boolean arg0) {
		// TODO Auto-generated method stub
        // no need
	}

	@Override
	public void OnMaxxSenseEnabledChanged(boolean arg0) {
		// TODO Auto-generated method stub
		//no need to listener
	}

	@Override
	public void OnServiceConnected() {
		// TODO Auto-generated method stub
		Log.d(TAG, "[onServiceConnected]");
		mMaxxHandle.GetMaxxSenseEnabled(MaxxDefines.MSG_ASYNC);
        mMaxxHandle.GetNotificationBarEnabled(MaxxDefines.MSG_ASYNC);
        //showNTF();
        changeSoundMode(mSoundMode);
	}

	@Override
	public void OnNotificationBarEnableChanged(boolean arg0) {
		// TODO Auto-generated method stub
		Log.d(TAG, "[onNotificationBar]");
	}

	@Override
	public void OnTagChanged(int tag) {
		// TODO Auto-generated method stub
		//no need
	}

	@Override
	public void onEffectCreated() {
		// TODO Auto-generated method stub
		Log.d(TAG, "[onEffectCreated]");
		mMaxxHandle.requestUpdateClientPresetParameters(MaxxDefines.MSG_ASYNC);
		Log.d(TAG, "onEffectCreated after request update client preset parameters");

	}

	@Override
	public void onEffectReleased() {
		// TODO Auto-generated method stub
		Log.d(TAG, "onEffectReleased");
	}



    /**
     * Show notification to user
     * Should send msg to service
     */
    private void showNTF() {
        //Log.d(TAG, "[showNTF] mIsMasterSwitchOn=" + mIsMasterSwitchOn + "| mIsSenseOn:" + mIsSenseOn + "| mIsNTFOn=" + mIsNTFOn);
        mMaxxHandle.SetNotificationBarEnabled(MaxxDefines.MSG_ASYNC, mIsNTFOn);
    }

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		int id = view.getId();
		switch (id) {
		case R.id.reset:
			onClickResetButton();
			break;

		default:
			Log.d(TAG, "unknow id");
			break;
		}
	}

	private void onClickResetButton() {
		// TODO Auto-generated method stub
		if (mUnsavedValues.size() > 0 || isShowResetButton()) {
            Toast.makeText(this, R.string.undo_summary, Toast.LENGTH_SHORT).show();
        }
       // mSaveBtn.setIcon(R.drawable.ic_save_grey);
        //mResetBtn.setBackgroundResource(R.drawable.ic_reset_grey);//set button disable
        mUnsavedValues.clear();

        // We must backup mLastSavedValues's data into values before execute reset option.
        // Because the mLastSavedValues will be cleared when resetting option.
        //HashMap<Integer, Double> values = (HashMap<Integer, Double>) mLastSavedValues.clone();

        if (GetSoundMode() == MaxxDefines.SOUNDMODE_MUSIC) {
            mMaxxHandle.PresetClearGuiParameters(MaxxDefines.MSG_ASYNC, GetOutputMode(), GetSoundMode(), m_AudioGenre);
        } else {
            mMaxxHandle.PresetClearGuiParameters(MaxxDefines.MSG_ASYNC, GetOutputMode(), GetSoundMode(), 0);
        }
        saveResetFlag(false);
	}

	@Override
	public boolean HandleMessage(Message message) {
		// TODO Auto-generated method stub
		Log.d(TAG, "[HandleMessage]:  " + message.what);
		return false;
	}

	@Override
	public void OnCurrentAppChanged(MaxxSenseAppInfo maxxSenseAppInfo) {
		// TODO Auto-generated method stub
		mCurrentAppLabel = maxxSenseAppInfo.getLabel();
        Log.d(TAG, "mCurrentAppLabel = " + mCurrentAppLabel);
	}


}
