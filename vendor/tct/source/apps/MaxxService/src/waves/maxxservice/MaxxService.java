/********************************************************************************
 **
 **    Copyright 2015 Waves Audio Ltd. All Rights Reserved.
 **
 ********************************************************************************
 **
 **  The source code, information and material ("Material") contained herein
 **  is owned by Waves Audio Ltd.(“Waves”), and title
 **  to such Material remains with Waves.
 **  The Material contains proprietary information of Waves. The Material is protected by worldwide copyright laws and treaty
 **  provisions. Recipient shall use the Materials solely for the purpose of its implementation of MaxxAudio into the Material.
 **  No part of the Material may be copied, reproduced, modified,
 **  published, uploaded, posted, transmitted, distributed or disclosed in any way
 **  without Waves's prior express written permission. No license under any patent,
 **  copyright or other intellectual property rights in the Material is granted to
 **  or conferred upon you, either expressly, by implication, inducement, estoppel
 **  or otherwise. Any license under such intellectual property rights must
 **  be express and approved by Waves in writing.
 **
 **  Unless otherwise agreed by Waves in writing, you may not remove or alter this
 **  notice or any other notice embedded in the Materials”
 **
 ********************************************************************************
 **  *Third-party brands and names are the property of their respective owners.

 ** The provisions of this notice shall not derogate from any of Waves' rights under the Non Disclosure Agreement between Waves and Recipient or under any Service Level Agreement ("SLA"), regardless of when such SLA is signed between Waves and Recipient.
 */

package com.waves.maxxservice;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;

import com.waves.maxxservicebase.MaxxFXIPC;
import com.waves.maxxservicebase.MaxxMessages;
import com.waves.maxxservicebase.MaxxSense;
import com.waves.maxxservicetemplates.MaxxMAM2Service;
import com.waves.maxxutil.MaxxDefines;
import com.waves.maxxutil.MaxxLogger;

import java.util.Arrays;
import java.util.HashSet;

public class MaxxService extends MaxxMAM2Service {
    private static final boolean SUPPORT_IPC = true;
    private static final boolean SUPPORT_MAXXSENSE = true;
    private static final boolean SUPPORT_ORIENTATION = true;

    public static final String EXTRA_FROM_PROVIDER = "from_provider";
    private WavesFXProvider m_WavesFXProvider;

    private MaxxServiceBinder m_MaxxServiceBinder = new MaxxServiceBinder();
    private MaxxServiceSharedPreferences m_MaxxServiceSharedPreferences;

    private final Integer[] mParameterNeedToBeSmooth = new Integer[] {
            MaxxDefines.ALG_MAAP_MAXX_BASS_ACTIVE,
            MaxxDefines.ALG_MAAP_MAXX_HF_ACTIVE,
            MaxxDefines.ALG_MAAP_MAXX_3D_ACTIVE,
            MaxxDefines.ALG_MAAP_IVOLUME_ACTIVE,
            MaxxDefines.ALG_MAAP_CENTER_ACTIVE,
            MaxxDefines.ALG_MAAP_EXCITER_ACTIVE,
            MaxxDefines.ALG_MAAP_MAXX_NR_ACTIVE
    };

    @Override
    public void onCreate() {
        super.onCreate();

        SetSmoothSet(new HashSet<>(Arrays.asList(mParameterNeedToBeSmooth)));
    }

    @Override
    protected void postEffectInitialize() {
        super.postEffectInitialize();

        // for MaxxSense
        if (SUPPORT_IPC) {
            m_IPC = new MaxxFXIPC(this);
            m_IPC.AddSupportedMessages(m_ActivePidsMessage);
            m_IPC.AddSupportedMessages(mMaxxSenseDataMessage);
            m_IPC.Start();
        }

        if (SUPPORT_MAXXSENSE) {
            m_MaxxSense = new MaxxSense(this, MaxxDefines.ALG_TYPE_MAXXMOBILE2);
        	m_MaxxSense.LoadFromDB("/system/etc/maxxsense.db", true);
            m_MaxxSense.setMaxxSenseListener(this);
            m_MaxxSense.start();
            boolean bMaxxSenseEnabled = m_SharedPreferences.get(MaxxDefines.ALG_TYPE_MAXXMOBILE2).getBoolean(MaxxDefines.KEY_PREFERENCE_IS_MAXXSENSE_ENABLED, DEFAULT_MAXXSENSE_ENABLED);
            EffectSetMaxxSenseEnabled(MaxxDefines.ALG_TYPE_MAXXMOBILE2, bMaxxSenseEnabled);
        }

        if (SUPPORT_ORIENTATION) {
            EffectSetOrientationEnable(MaxxDefines.ALG_TYPE_MAXXMOBILE2, true);
        }
    }

    @Override
    protected void preEffectUnInitialize() {
        if (SUPPORT_MAXXSENSE) {
            EffectSetMaxxSenseEnabled(MaxxDefines.ALG_TYPE_MAXXMOBILE2, false);
            if (null != m_MaxxSense){
                m_MaxxSense.stop();
            }
        }

        if (null != m_IPC){
            m_IPC.Stop();
        }

        super.preEffectUnInitialize();
    }

    @Override
    protected void HandleMessage(Message msg) {
        switch (msg.what)
        {
            default:
            {
                super.HandleMessage(msg);
            }
        }
    }

    // for MaxxSense
    private MaxxMessages.MaxxMessage m_ActivePidsMessage = new MaxxMessages.MaxxActivePidsMessage() {
        @Override
        protected void Execute() {
            int [] pids = GetActivePids();
            if (pids == null)
            {
                m_MaxxSense.UpdateActivePids(0, null);
            }
            else
            {
                m_MaxxSense.UpdateActivePids(pids.length, pids);
            }
        }
    };

    private MaxxMessages.MaxxMessage mMaxxSenseDataMessage = new MaxxMessages.MaxxSetMaxxSenseDataMessage() {
        @Override
        protected void Execute() {
            m_MaxxSense.onMaxxSenseDataUpdate(getMaxxSenseDatas());
        }
    };

    @Override
    protected void RegisterReceivers() {
        super.RegisterReceivers();

        registerReceiver(m_MaxxServiceReceiver, new IntentFilter(Intent.ACTION_LOCALE_CHANGED));
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        boolean isFromProvider = intent.getBooleanExtra(EXTRA_FROM_PROVIDER, false);
        if (isFromProvider) {
            return m_MaxxServiceBinder;
        }
        else {
            return super.onBind(intent);
        }
    }

    @Override
    protected void EnableNotifications(boolean bNotificationBarEnabled) {
        MaxxLogger.Debug("EnableNotifications, bNotificationBarEnabled = " + bNotificationBarEnabled);
    }

    @Override
    protected void InitMaxxSharedPreferences() {
        m_SharedPreferences.put(MaxxDefines.ALG_TYPE_MAXXMOBILE2, new MaxxServiceSharedPreferences(PreferenceManager.getDefaultSharedPreferences(getApplicationContext())));

        m_MaxxServiceSharedPreferences = (MaxxServiceSharedPreferences)m_SharedPreferences.get(MaxxDefines.ALG_TYPE_MAXXMOBILE2);
    }

    @Override
    protected void HandleIntentReceived(Intent intent, int flags, int startId) {
        if (intent == null || intent.getAction() == null) {
            return;
        }

        MaxxLogger.Debug("Intent action = " + intent.getAction());
    }

    public int GetSoundMode()
    {
        return m_EffectMode.getSoundMode();
    }

    public void SetSoundMode(int soundMode)
    {
        EffectSetSoundMode(MaxxDefines.ALG_TYPE_MAXXMOBILE2, soundMode);
    }

    public boolean IsEnabled()
    {
        return m_MaxxServiceSharedPreferences.GetIsEnabled();
    }

    public void SetEnabled(boolean enabled)
    {
        EffectSetEnabled(MaxxDefines.ALG_TYPE_MAXXMOBILE2, enabled);
    }

    @Override
    protected boolean EffectSetEnabled(String strAlgType, boolean bEnabled)
    {
        boolean bStateChanged = (m_MaxxServiceSharedPreferences.GetIsEnabled() != bEnabled);

        boolean status = super.EffectSetEnabled(strAlgType, bEnabled);

        if (bStateChanged)
        {
            NotifyConfigChanged();
        }

        return status;
    }

    public class MaxxServiceBinder extends Binder
    {
        public MaxxService getService()
        {
            return MaxxService.this;
        }
    }

    public void SetProvider(WavesFXProvider provider)
    {
        m_WavesFXProvider = provider;
    }

    private void NotifyConfigChanged()
    {
        if (null != m_WavesFXProvider) {
            m_WavesFXProvider.NotifyConfigObservers();
        }
    }
}
