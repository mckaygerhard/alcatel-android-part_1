/********************************************************************************
 **
 **    Copyright 2015 Waves Audio Ltd. All Rights Reserved.
 **
 ********************************************************************************
 **
 **  The source code, information and material ("Material") contained herein
 **  is owned by Waves Audio Ltd.(“Waves”), and title
 **  to such Material remains with Waves.
 **  The Material contains proprietary information of Waves. The Material is protected by worldwide copyright laws and treaty
 **  provisions. Recipient shall use the Materials solely for the purpose of its implementation of MaxxAudio into the Material.
 **  No part of the Material may be copied, reproduced, modified,
 **  published, uploaded, posted, transmitted, distributed or disclosed in any way
 **  without Waves's prior express written permission. No license under any patent,
 **  copyright or other intellectual property rights in the Material is granted to
 **  or conferred upon you, either expressly, by implication, inducement, estoppel
 **  or otherwise. Any license under such intellectual property rights must
 **  be express and approved by Waves in writing.
 **
 **  Unless otherwise agreed by Waves in writing, you may not remove or alter this
 **  notice or any other notice embedded in the Materials”
 **
 ********************************************************************************
 **  *Third-party brands and names are the property of their respective owners.

 ** The provisions of this notice shall not derogate from any of Waves' rights under the Non Disclosure Agreement between Waves and Recipient or under any Service Level Agreement ("SLA"), regardless of when such SLA is signed between Waves and Recipient.
 */

package com.waves.maxxservice;

import android.content.SharedPreferences;

import com.waves.maxxservicebase.MaxxSharedPreferences;

public class MaxxServiceSharedPreferences extends MaxxSharedPreferences {

	public MaxxServiceSharedPreferences(SharedPreferences in_SharedPreferences) {
		super(in_SharedPreferences);
	}

	@Override
	public boolean DefaultIsEnabled() {
		return true;
	}

	@Override
	public boolean DefaultIsShowNotification() {
		return MaxxService.DEFAULT_NOTIFICATION_BAR_ENABLED;
	}
}
