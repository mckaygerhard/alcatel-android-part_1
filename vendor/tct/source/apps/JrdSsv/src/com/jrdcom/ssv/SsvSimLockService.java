package com.jrdcom.ssv;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.SsvManager;
import android.os.Process;
import android.os.*;
import android.os.SystemProperties;
import android.util.Log;

public class SsvSimLockService extends Service {
    private static final String TAG = "SsvSimLockService";
    private Context mContext = null;
    private SsvUtil mUtil;
    private SsvManager ssv;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mUtil = SsvUtil.getInstance();
        Log.v(TAG, "SsvSimLockService start step onCreate");
        if (mUtil == null) {
            mUtil = SsvUtil.init(mContext);
        }
        if (ssv == null) {
            ssv = SsvManager.getInstance();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            new Thread(new Runnable(){
                public void run(){
                    {
                        Log.d(TAG, "thread...run()");

                        // 1. TODO hide apk
                        Log.d(TAG, "thread...TODO hide apk");

                        // 2. close SsvSimLockActivity (if it was shown on booting)
                        mContext.sendBroadcast(new Intent(
                                SsvSimLockActivity.ACTION_SIM_UNLOCKED));

                        // 3. if user dismissed PIN code dialog, and unlock SIM card via
                        // Settings, start SsvActivity again, to handle SSV configuration
                        // here
                        Log.d(TAG, "dismissed = " + mUtil.isSimLockDismissed());
                        /* MODIFIED-BEGIN by kaibang.liu-nb, 2016-05-30,BUG-2222103*/
                        boolean isMccmncChanged = mUtil.isMccMncChanged();//SsvManager.getInstance().isMccMncChanged();
                        Log.d(TAG, "istmoStartSsvActivity == " + isTMOStartSsvActivity() + "    isMccmncChanged=" + isMccmncChanged);
                        // if (mUtil.isSimLockDismissed()) {
                        //add by peng.kuang for pin_lock dismiss
                        if (isMccmncChanged) {
                        /* MODIFIED-END by kaibang.liu-nb,BUG-2222103*/
                            String operatorSim = SystemProperties.get("ro.ssv.operator.choose", "TMO");
                            if ("TMO".equals(operatorSim)) {
                                String tmossvstate = SystemProperties.get("persist.sys.tmossv.off", "false");
                                Log.d(TAG, "tmossvstate == " + tmossvstate);
                                if ("false".equals(tmossvstate) && isTMOStartSsvActivity()) {
                                    // enable & start SsvActivity to handle SSV
                                    // configurations
                                    PackageManager pm = mContext.getPackageManager();
                                    ComponentName ssvActivity = new ComponentName(
                                            "com.jrdcom.ssv", "com.jrdcom.ssv.SsvActivity");
                                    Log.v(TAG, "set enabled settings....ENABLED SsvActivity");
                                    try {
                                        pm.setComponentEnabledSetting(
                                                ssvActivity,
                                                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                                                PackageManager.DONT_KILL_APP);
                                        Log.v(TAG,
                                                "SSV,,,,AMS set enabled settings....DONE");
                                        Intent i = new Intent(mContext, SsvActivity.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        mContext.startActivity(i);
                                    } catch (Exception e) {
                                        Log.v(TAG,
                                                "SSV,,,,AMS set enabled settings....Exception:"
                                                        + e.getMessage());
                                    }
                                }
                            } else {
                                // enable & start SsvActivity to handle SSV configurations
                                PackageManager pm = mContext.getPackageManager();
                                ComponentName ssvActivity = new ComponentName(
                                        "com.jrdcom.ssv", "com.jrdcom.ssv.SsvActivity");
                                Log.v(TAG, "set enabled settings....ENABLED SsvActivity");
                                try {
                                    pm.setComponentEnabledSetting(
                                            ssvActivity,
                                            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                                            PackageManager.DONT_KILL_APP);
                                    Log.v(TAG, "SSV,,,,AMS set enabled settings....DONE");
                                    Intent i = new Intent(mContext, SsvActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    mContext.startActivity(i);
                                } catch (Exception e) {
                                    Log.v(TAG,
                                            "SSV,,,,AMS set enabled settings....Exception:"
                                                    + e.getMessage());
                                }
                            }
                        }
                        // send broadcast to update app (TODO need app handle it)
                        //sendBroadcast(new Intent("com.jrd.ssv.service_requests_update"));

                        // done
                        SsvSimLockService.this.stopSelf();
                    }
                }
            }).start();
        } catch (IllegalThreadStateException ex) {
            Log.e(TAG, "thread has been started before.");
        }
        return START_STICKY;
    }



    private boolean isTMOStartSsvActivity() {//whether the TMO need start activity, simcard is main branch/sub branch
        // and status switch (no card/main branch/sub branch/ORIG(the first)), main --> sub ,return false
        boolean ret = false;
        String nowmccmnc = ssv.getSimMccMnc();
        String everypremccmnc = ssv.getEveryPrevMccMnc();
        int transition = mUtil.getTransition();
        Log.v(TAG, "transition=" + transition);
        switch (transition) {

            case SsvManager.TRANSITION_ORIG_TO_DEF:
                mUtil.setCurrentState(SsvManager.STATE_DEF);
                ret = false;
                break;

            case SsvManager.TRANSITION_DEF_TO_OP:
            case SsvManager.TRANSITION_DEF_TO_SUB:
            case SsvManager.TRANSITION_ORIG_TO_OP:
            case SsvManager.TRANSITION_ORIG_TO_SUB:
            case SsvManager.TRANSITION_OP_TO_OP:
                // modify by yanqi.liu for pop-up dialog again when change
                // Simcard
                if (mUtil.getNotifySimNextTime()
                        || !nowmccmnc.equalsIgnoreCase(everypremccmnc)) {
                    // continue to show activity to notify user
                    ret = true;
                } else {
                    ret = false;
                }
                break;
            case SsvManager.TRANSITION_NONE:
            default:
                ret = false;
                break;
        }
        return ret;
    }
}
