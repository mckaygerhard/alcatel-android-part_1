package com.jrdcom.ssv;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.SsvManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.app.StatusBarManager;

public class SsvSimLockActivity extends Activity {
    public static final String ACTION_SIM_UNLOCKED = "com.jrdcom.ssv.sim_card_unlocked";
    public static final String ACTION_SIM_LOCK_DISMISSED = "com.jrdcom.ssv.sim_lock_dismissed";
    private static final String HIDELOCK_FROM_SSVACTIVITY = "jrdssv.ssvactivity.hidelock";
    private static final String TAG = "SsvSimLockActivity";
    //[BUGFIX]-Add-BEGIN by TSCD.peng.kuang,12/31/2014,888959
    public static boolean DISMISS_TAG = false;
    //[BUGFIX]-Add-End by TSCD.peng.kuang,12/31/2014,888959
    private boolean mIsReceiverRegistered = false;
    private SsvUtil mUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean ssvEnabled = SystemProperties.getBoolean("ro.ssv.enabled",
                false);
        if (!ssvEnabled) {
            Log.d(TAG, "onCreate, return, because SSV is not enabled");
            ssvActivityDone();
            return;
        }

        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        StatusBarManager statusBarManager = (StatusBarManager) getApplication().getSystemService(Context.STATUS_BAR_SERVICE);
        statusBarManager.disable(StatusBarManager.DISABLE_EXPAND);
        mUtil = SsvUtil.getInstance();
        if (mUtil == null) {
            mUtil = SsvUtil.init(this);
        }
        mUtil.setSsvKeyGuardState(true);
        mUtil.setSimLockDismissed(false); // reset
        // modify by yanqi.liu for PR-515904
        int airmode = Settings.System.getInt(getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0);
        Log.d(TAG, "airmode==========" + airmode);
        SsvManager ssvMgr = SsvManager.getInstance();
        String preMcc = SystemProperties.get("persist.sys.pre.mcccmncspn", "");
        //if (ssvMgr.isSimLock() && (airmode == 0) && preMcc.equals("")) {
        if (ssvMgr.isSimLock() && (airmode == 0)) {
            // wait for unlock or dismiss
            Log.d(TAG, "SIM card is locked, waiting...");

            // show layout
            setContentView(R.layout.sim_lock);
            TextView body = (TextView) findViewById(R.id.body);
          //[BUGFIX]-Add-BEGIN by TSCD.peng.kuang,11/26/2014,851269 848300 827763
            String operatorSim = SystemProperties.get("ro.ssv.operator.choose", "TMO");
            if(operatorSim.equals("TMO")){
                body.setText(R.string.tmo_notify_sim_locked);
            } else {
                body.setText(R.string.notify_sim_locked);
            }
            //[BUGFIX]-Add-END by TSCD.peng.kuang,11/26/2014,851269 848300 827763
            // register receiver to handle unlock or dismiss
            IntentFilter filter = new IntentFilter(ACTION_SIM_UNLOCKED);
            filter.addAction(ACTION_SIM_LOCK_DISMISSED); // from SimUnlockScreen
            filter.addAction("action_melock_dismiss");
            registerReceiver(mReceiver, filter);
            mIsReceiverRegistered = true;
        } else {
            // nothing to do
            Log.d(TAG, "SIM card is not locked, pass");
            ssvActivityDone();
        }
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayUseLogoEnabled(false);
    }

    @Override
    protected void onDestroy() {
        if (mIsReceiverRegistered) {
            unregisterReceiver(mReceiver);
            mIsReceiverRegistered = false;
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // do not response BACK
        return;
    }

    private void ssvActivityDone() {
        // disable ourself (will be enabled again before AMS starting HOME)
        PackageManager pm = getPackageManager();
        ComponentName name = new ComponentName(this, SsvSimLockActivity.class);
        try {
            pm.setComponentEnabledSetting(name,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
        } catch (Exception e) {
            Log.e(TAG,
                    "SSV,,,,set enabled settings....Exception:"
                            + e.getMessage());
        }
        if (mUtil != null) {
        mUtil.setSsvKeyGuardState(false);
        }
        //[BUGFIX]-Add-BEGIN by TSCD.peng.kuang,09/19/2014,792365
        boolean ssvEnabled = SystemProperties.getBoolean("ro.ssv.enabled",
                false);
        if (ssvEnabled) {
            StatusBarManager statusBarManager = (StatusBarManager) getApplication().getSystemService(Context.STATUS_BAR_SERVICE);
            statusBarManager.disable(StatusBarManager.DISABLE_NONE);
        }
        //[BUGFIX]-Add-END by TSCD.peng.kuang,09/19/2014,792365
        finish();
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive, action = " + action);

            if (ACTION_SIM_UNLOCKED.equals(action)) {
                // SIM card is unlocked, close activity
                Log.d(TAG, "onReceive, close activity");
                SsvSimLockActivity.this.ssvActivityDone();
                // modify while "enter SIM me lock",click dissmiss button,the
                // ssvsimlock can't dissmiss.
            } else if (ACTION_SIM_LOCK_DISMISSED.equals(action)
                    || "action_melock_dismiss".equals(action)) {
                Log.d(TAG, "onReceive, sim lock is dismissed");
                if (!SsvManager.getInstance().isSimLock()) {
                    // this would happen in quick boot mode
                    // when quick boot, PIN code dialog appear, but SsvService
                    // was not restarted
                    // need not to update ssv
                    Log.d(TAG, "onReceive, return, quick boot mode");
                    return;
                }

                mUtil.setSimLockDismissed(true);
                Log.d(TAG, "dismissed = " + mUtil.isSimLockDismissed());
                // go to DEF state//modify by peng.kuang 20140813 may use it in tmo_ssv
                if(SystemProperties.get("ro.ssv.operator.choose", "TMO").equals("TMO")){
                    defaultConfiguration();
                }
                // TODO hide apk ???
                SsvSimLockActivity.this.ssvActivityDone();
                //[BUGFIX]-Add-BEGIN by TSCD.peng.kuang,12/31/2014,888959
                DISMISS_TAG = true;
                //[BUGFIX]-Add-End by TSCD.peng.kuang,12/31/2014,888959
            }
        }
    };

    private void defaultConfiguration() {
        // clear mccmnc
        mUtil.setPrevMccMnc("");
        // clear mccmnc for language customization
        mUtil.setLangMccMnc("");
        // set current state
        mUtil.setCurrentState(SsvManager.STATE_DEF);
        // need not to update mccmnc configuration(0 by default)

        // update ringtone
        updateRintone();
        // need not to update language
    }

    private void updateRintone() {
        // ringtone new solution
        Log.d(TAG, "start ringtone service");
        Intent i = new Intent(this, SsvRingtoneService.class);
        i.putExtra("userConfirmed", true);
        startService(i);
    }
}
