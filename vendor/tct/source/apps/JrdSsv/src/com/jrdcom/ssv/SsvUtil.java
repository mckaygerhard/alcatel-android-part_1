package com.jrdcom.ssv;

import java.io.IOException;

import android.app.ActivityManagerNative;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.RemoteException;
import android.os.SsvManager;
import android.os.SystemProperties;
import android.util.Log;

/**
 * a util class. wrap SsvManager, handle xml files...
 */
public class SsvUtil {
    private static final String TAG = "SsvUtil";

    public static final String TRANSITION = "transition";
    public static final String XML_DEFAULT = "ssv_default.xml";
    private static final String KEY_NOTIFY_SIM_NEXT_TIME = "notify_sim_next_time";
    private static final String KEY_NOTIFY_LANG_NEXT_TIME = "notify_lang_next_time";
    private static final String KEY_SIM_LOCK_DISMISSED = "sim_lock_dismissed";

    private static SsvUtil mInstance = null;
    private Context mContext = null;
    private SsvManager mSsvManager = null;
    private SharedPreferences mSp;

    public static SsvUtil init(Context context) {
        if (context == null) {
            return null;
        }

        mInstance = new SsvUtil(context);
        return mInstance;
    }

    /**
     * init(Context) should be called before calling this method
     */
    public static SsvUtil getInstance() {
        return mInstance;
    }

    private SsvUtil(Context context) {
        mContext = context;
        if (mContext == null) {
            Log.d(TAG, "mContext == null");
        }
        mSsvManager = (SsvManager) mContext
                .getSystemService(Context.SSV_SERVICE);
        if (mSsvManager == null) {
            Log.d(TAG, "SsvUtil mSsvManager == null");
        } else {
            Log.d(TAG, "SsvUtil mSsvManager is current");
        }
        mSp = mContext.getSharedPreferences("jrd_ssv", 0);
    }

    public int getTransition() {
        return mSsvManager.getTransition();
    }

    public String getSimMccMnc() {
        return mSsvManager.getSimMccMnc();
    }

    public String getSimSpn() {
        return mSsvManager.getSimSpn();
    }

    public String getPrevMccMnc() {
        return mSsvManager.getPrevMccMnc();
    }

    public boolean getNotifySimNextTime() {
        return mSp.getBoolean(KEY_NOTIFY_SIM_NEXT_TIME, true);
    }

    public boolean getNotifyLangNextTime() {
        return mSp.getBoolean(KEY_NOTIFY_LANG_NEXT_TIME, true);
    }

    //[BUGFIX]-Add-BEGIN by TSCD.kun.jiang,1030417
    public boolean getIsFirstRun() {
        return mSsvManager.getIsFirstRun();
    }

    public void setIsFirstRun(boolean b) {
        mSsvManager.setIsFirstRun(b);
    }
    //[BUGFIX]-Add-END by TSCD.kun.jiang,1030417

    public void setSimLockDismissed(boolean dismissed) {
        // use shared preference to store value because process may be killed
        // before unlock
        mSp.edit().putBoolean(KEY_SIM_LOCK_DISMISSED, dismissed).commit();
    }

    public boolean isSimLockDismissed() {
        return mSp.getBoolean(KEY_SIM_LOCK_DISMISSED, false);
    }

    public void setPrevMccMnc(String mccmnc) {
        mSsvManager.setPrevMccMnc(mccmnc);
    }

    public void setPreSimSpn(String spn) {
        mSsvManager.setPreSimSpn(spn);
    }

    public void setSwitchEnable(boolean enable) {
        mSsvManager.setSwitchEnable(enable);
    }

    public void setLangMccMnc(String mccmnc) {
        mSsvManager.setLangMccMnc(mccmnc);
    }

    public void setNotifySimNextTime(boolean notify) {
        mSp.edit().putBoolean(KEY_NOTIFY_SIM_NEXT_TIME, notify).commit();
    }

    public String getButtonNoNum() {
        return mSsvManager.getButtonNoNum();
    }

    public void setButtonNoNum(String buttonstate) {
        mSsvManager.setButtonNoNum(buttonstate);
    }

    public void setNotifyLangNextTime(boolean notify) {
        mSp.edit().putBoolean(KEY_NOTIFY_LANG_NEXT_TIME, notify).commit();
    }

    public void setCurrentState(int state) {
        mSsvManager.setCurrentState(state);
    }

    public void updateMccMncConfiguration(String mccmnc) {
        if (mccmnc.length() < 5) {
            return;
        }

        int mcc, mnc;

        try {
            mcc = Integer.parseInt(mccmnc.substring(0, 3));
            mnc = Integer.parseInt(mccmnc.substring(3));
        } catch (NumberFormatException e) {
            Log.e(TAG, "Error parsing IMSI");
            return;
        }

        Log.v(TAG, "updateMccMncConfiguration: mcc=" + mcc + ", mnc=" + mnc);
        try {
            Configuration config = ActivityManagerNative.getDefault()
                    .getConfiguration();
            if (mcc != 0) {
                config.mcc = mcc;
            }
            if (mnc != 0) {
                config.mnc = mnc;
            }
            ActivityManagerNative.getDefault().updateConfiguration(config);
        } catch (RemoteException e) {
            Log.e(TAG, "Can't update configuration", e);
        }
    }

    /**
     * Get the default xml
     */
    public String getDefaultXml() {
        return XML_DEFAULT;
    }

    /**
     * Get the current xml.
     *
     * @return return the suitable xml, "ssv_defaule.xml" for default.
     */
    public String getCurrentXml() {
        String mccmnc = mSsvManager.getSimMccMnc();
        if (mccmnc == null || mccmnc.equals("")) {
            return XML_DEFAULT;
        }

        String xmlFile = "ssv_plmn_" + mccmnc + ".xml";
        try {
            mContext.getAssets().open(xmlFile);
        } catch (IOException e) {
            Log.e(TAG, "no xml file for mccmnc: " + mccmnc);
            return XML_DEFAULT;
        }
        return xmlFile;
    }


    public void initSsvSettings() {
            mSsvManager.initSsvSettings();
    }

    public void setSsvKeyGuardState(boolean state) {
        mSsvManager.setSsvKeyGuardState(state);
    }

    /* MODIFIED-BEGIN by kaibang.liu-nb, 2016-05-30,BUG-2222103*/
    public boolean isMccMncChanged() {
        boolean result = false;
        try {
            result = mSsvManager.isMccMncChanged();
        } catch (Exception e) {
            Log.e(TAG, "SsvUtil isMccMncChanged, Exception==>:" + e);
        }

        return result;
    }
    /* MODIFIED-END by kaibang.liu-nb,BUG-2222103*/
}
