package com.jrdcom.ssv;

import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences; // MODIFIED by kaibang.liu-nb, 2016-05-25,BUG-2152988
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.SsvManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.TctLog;
import android.app.PendingIntent;
import android.widget.RemoteViews;
import android.provider.Settings;
import android.content.res.Resources;
import android.app.AlarmManager;
import android.net.wifi.WifiConfiguration;
import android.text.TextUtils;
import android.telephony.PhoneNumberUtils;

//import com.android.internal.telephony.gsm.SpnOverride;


public class SsvReceiver extends BroadcastReceiver {
    private static final String TAG = "SsvReceiver";
    private String mSpnList = "/custpack/jrd_spn_table.lis";
    public static final String ACTION_SIM_LOCK_DISMISSED = "com.jrdcom.ssv.sim_lock_dismissed";
    private static String mLastSimStateExtra ="";
    private static final int DPM_CONFIG_MASK_FASTDORMANCY = 0x00000001;
    private static final int DPM_CONFIG_MASK_CT = 0x00000002;
    private static final String ACTION_LOAD_RESOURCES="com.jrdcom.ssv.action.LOAD_RESOURCES";
    private static final String PHONEBOOK_MIN_MATCH="persist.sys.phone.minmatch.len";
    private final String RESET_LOCATION_MODE="com.jrdcom.ssv.action.RESET_LOCATION_MODE";
    private final String ACTION_LOAD_SSV_CONFIG = "com.jrdcom.ssv.LOAD_SSV_CONFIG";

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean ssvEnabled = SystemProperties.getBoolean("ro.ssv.enabled", false);
        TctLog.d(TAG, "onReceive, return, because SSV status=" + ssvEnabled );
        if (!ssvEnabled) {
            Log.d(TAG, "onReceive, return, because SSV is not enabled");
            return;
        }

        String action = intent.getAction();
        Log.d(TAG, "onReceive, action = " + action);
        //[BUG-FIX]-ADD-BEGIN by xiaowei.yang-nb ,13/11/2015,PR-899557
        // We don't use sim status change broadcast any more, because this broadcast will be send when system reboot.
        if (ACTION_LOAD_SSV_CONFIG.equals(action) || "android.intent.action.BOOT_COMPLETED".equals(action)) {
            String ssvNeedChangedConfigs = Settings.System.getString(context.getContentResolver(),"ssvNeedChangedConfigs");//update in jrdssv.apk
            TctLog.d(TAG,"boot completed ssvNeedChangedConfigs = "+ssvNeedChangedConfigs);
            if("true".equals(ssvNeedChangedConfigs)) {
                Settings.System.putString(context.getContentResolver(),"ssvNeedChangedConfigs","false");

                setCountryName(context);
                setRomaing(context);
                setDateFormat(context);
                setTimeOrTimeZoneStatus(context);
                setWifiHostName(context);
                setTimeZone(context);
                setTimeFormat(context);
                setDPMFeature(context);
                setWifiDefaultStatus(context);
                setScreenOffTime(context);
                setPhoneNumberMatchLen(context);
                setLocationMode(context);
                setScreenRotationStatus(context);
                setDataConnectionStatus(context);
                setUserName(context);
            }
        }
        //[BUG-FIX]-ADD-END by xiaowei.yang-nb ,13/11/2015,PR-899557
        //[BUGFIX]-Add-BEGIN by TSCD.kun.jiang,1030417

        Log.d(TAG, "main:  onReceive, Do not send Notification  getIsFirstValidate()=" + SsvManager.getInstance().getIsFirstRun());
        if ("android.intent.action.BOOT_COMPLETED".equals(action) && SsvManager.getInstance().getIsFirstRun()){
            if (!isApkInstall(context, "de.telekom.appslauncher.om"))
                return;
            SsvManager.getInstance().setIsFirstRun(false);
            sendNofitificationInfo(context);
        }
        //[BUGFIX]-Add-END by TSCD.kun.jiang,1030417
        if ("action_pin_dismiss".equals(action)) {
            if (!SsvManager.getInstance().isSimLock()) {
                // if SIM card has been unlocked, we don't care about this
                // action anymore
                Log.d(TAG, "onReceive, return, because SIM card is not locked");
                return;
            }
            // release the SsvActivity to idle
            context.startService(new Intent(context, SsvSimLockService.class));

        } else if ("android.intent.action.SIM_STATE_CHANGED".equals(action)) {
            String stateExtra = intent.getStringExtra("ss");
            Log.d(TAG, "onReceive, stateExtra = " + stateExtra);
            if(stateExtra != null && !mLastSimStateExtra.equals(stateExtra)){
              if (!mLastSimStateExtra.equals("") && stateExtra.equals("ABSENT")) {
                  Log.d(TAG, "onReceive, because sim_state ABSENT");
                  Intent closessvactivity = new Intent(ACTION_SIM_LOCK_DISMISSED);
                  context.sendBroadcast(closessvactivity);
              }
              mLastSimStateExtra = stateExtra;
            }
             //sim state changed, check if it's unlocked ?
            //if (!SsvManager.getInstance().isSimLock()) {
                 //if SIM card has been unlocked, we don't care about this
                 //action anymore
               // Log.d(TAG, "onReceive, return, because SIM card is not locked");
                //return;
            //}

            /* MODIFIED-BEGIN by kaibang.liu-nb, 2016-05-21,BUG-2160204*/
            // We ignoring the status of simcard if we can get mccmnc,
            String preSetMccmnc = SystemProperties.get("persist.sys.mcc.mnc", " ");
            Log.d(TAG, "onReceive, setMccmnc:" + preSetMccmnc);
            //if (stateExtra == null || !(stateExtra.equals("LOADED") || stateExtra.equals("READY"))) {
            if (TextUtils.isEmpty(preSetMccmnc)) {
                Log.d(TAG, "onReceive, return, because sim not loaded");
                return;
                /* MODIFIED-END by kaibang.liu-nb,BUG-2160204*/
            }

            TelephonyManager teleponyservice = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            String mccmnc = teleponyservice.getSimOperator();
            String spn = SystemProperties.get("gsm.sim.operator.alpha");
            if (spn.equals("")) {
                spn = SystemProperties.get("gsm.sim.operator.default-name");
            }

            // read the /custpack/jrd_spn_table.lis for SIM_LOCK
//            try {
//                InputStreamReader reader = new InputStreamReader(
//                        new FileInputStream(mSpnList));
//                BufferedReader br = new BufferedReader(reader);
//                String line = "";
//                line = br.readLine();
//                while ((line != null)) {
//                    String[] tempArray = line.split(",");
//                    String tempSpn = tempArray[tempArray.length - 1]
//                            .substring(2);
//                    if (spn.equals(tempSpn)) {
//                        spn = tempArray[2].substring(1,
//                                tempArray[2].length() - 2);
//                        Log.d(TAG, "get the matched spn: " + spn);
//                        break;
//                    } else {
//                        line = br.readLine();
//                    }
//                }
//                br.close();
//                reader.close();
//            } catch (Exception e) {
//                Log.d(TAG, "Open file error");
//            }
            mccmnc = teleponyservice.getSimOperator();
            String gid = teleponyservice.getGroupIdLevel1();
//            String gid = TelephonyManager.getDefault().getGroupIdLevel1();
            Log.d(TAG, "details:  onReceive, getSimOperator=" + mccmnc + "    getSimMccmnc=" + mccmnc + "    getSimSPN=" + spn + "    getGid=" + gid);

            /* MODIFIED-BEGIN by kaibang.liu-nb, 2016-05-21,BUG-2160204*/
            if (TextUtils.isEmpty(mccmnc)) {
                mccmnc = preSetMccmnc;
                Log.d(TAG, "onReceive, mccmnc is null, using preSetMccmnc replace it");
            }
            /* MODIFIED-END by kaibang.liu-nb,BUG-2160204*/

            if (mccmnc != null && mccmnc.length() != 0) {
//                SsvManager.getInstance().unlock(mccmnc, spn);
                SsvManager.getInstance().unLock(mccmnc, spn, gid == null ? "" : gid);
                // start service to handle SIM card unlocked
                context.startService(new Intent(context,
                        SsvSimLockService.class));
            }
        }
      //[BUG]-Add-BEGIN by TSCD.peng.kuang 08/01/2014 , 750468
//        else if (Intent.ACTION_MEDIA_SCANNER_FINISHED.equals(action)) {
//            Uri uri = intent.getData();
//            Log.d(TAG, "onReceive, media scanner finished, uri=" + uri);
//            if (!uri.toString().startsWith("file:///system/media")) {
//                // wait for internal storage is ready
//                return;
//            }
//
//            // mark internal media ready
//            Log.d(TAG, "onReceive, internal media ready!");
//            try {
//                SystemProperties.set(
//                        SsvRingtoneService.PROP_INTERNAL_MEDIA_READY, "true");
//                // tell SsvRingtoneService that media is ready
//                Log.d(TAG, "onReceive, start ringtone service");
//                Intent i = new Intent(context, SsvRingtoneService.class);
//                i.putExtra("mediaReady", true);
//                context.startService(i);
//            } catch (IllegalArgumentException ex) {
//                Log.e(TAG, "onReceive, can not set property: "
//                        + SsvRingtoneService.PROP_INTERNAL_MEDIA_READY);
//            }
//        }
        //[BUG]-Add-END by TSCD.peng.kuang 08/01/2014 , 750468
    }

    //[BUGFIX]-Add-BEGIN by TSCD.kun.jiang,1030417
    boolean isApkInstall(Context context, String packagename) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(
                packagename, 0);
        } catch (NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        Log.d(TAG + "gg", "is app installed:" + packageInfo);
        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    void sendNofitificationInfo(Context context) {
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notification_layout);
        contentView.setTextViewText(R.id.title, context.getResources().getString(R.string.stater_title));
        contentView.setTextViewText(R.id.contace, context.getResources().getString(R.string.stater_context));
        Intent intent = new Intent();
        intent.setClassName("de.telekom.appslauncher.om", "com.communology.dte.assistant.collection.ui.CollectionsActivity");
        PendingIntent pendingIntent3 = PendingIntent.getActivity(context, 0, intent, 0);
        NotificationManager manager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notify = new Notification.Builder(context)
                .setSmallIcon(R.drawable.app_stater_icon)
                .setContent(contentView)
                .setContentIntent(pendingIntent3)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setTicker("App Starter")
                            .setContentIntent(pendingIntent3)
                .build();
        manager.notify(1, notify);
        Log.d(TAG + "gg", " notification sent");
    }
    //[BUGFIX]-Add-END by TSCD.kun.jiang,1030417
    //[BUG-FIX]-ADD-BEGIN by xiaowei.yang-nb ,13/11/2015,PR-899557
    private void setCountryName(Context context){
        Resources res = context.getResources();
        String countryName= res.getString(R.string.def_ssv_countryName);
        SystemProperties.set("persist.sys.lang.country", countryName);
        TctLog.d(TAG, "persist.sys.lang.country = " + countryName);
    }

    private void setRomaing(Context context) {
        Intent intent= new Intent(ACTION_LOAD_RESOURCES);
        String mccMnc = SystemProperties.get("persist.sys.lang.mccmnc", "");
        intent.putExtra("current_mccmnc",mccMnc);
        TctLog.d(TAG,"ssv to set default data roaming by broadcast");
        context.sendBroadcast(intent);
    }

    //[BUGFIX]-Mod-BEGIN by TSNJ,kaibang.liu,04/26/2016,Defect-1986168,
    private void setDataConnectionStatus(Context context) {
        boolean dataConnectionEnable = Resources.getSystem().getBoolean(com.android.internal.R.bool.def_tctfw_ssv_dataConnectionState);
        Log.d(TAG, "setDataConnectionStatus dataConnectionEnable:" + dataConnectionEnable);
        Settings.Global.putInt(context.getContentResolver(), "mobile_data", dataConnectionEnable ? 1 : 0);
    }
    //[BUGFIX]-Mod-END by TSNJ,kaibang.liu,

    private void setDateFormat(Context context){
        String mccMncString = SystemProperties.get("persist.sys.lang.mccmnc","");
        Resources res = context.getResources();

        //[BUGFIX]-Mod-BEGIN by TSNJ,kaibang.liu,12/29/2015, Defect: 1217869
        String configDateFormat = res.getString(com.android.internal.R.string.def_tctfw_ssv_dateFormat);
        TctLog.d(TAG , "setDateFormat configDateFormat = " + configDateFormat);
        if (!TextUtils.isEmpty(configDateFormat)) {
            String dateFomat = formatConfigDateStr(configDateFormat);
            TctLog.d(TAG , "setDateFormat dateFomat = " + dateFomat);
            Settings.System.putString(context.getContentResolver(),
                    Settings.System.DATE_FORMAT, dateFomat);
        }
        //[BUGFIX]-Mod-END by TSNJ,kaibang.liu

        String vibrateRingStr = res.getString(com.android.internal.R.string.def_tctfw_ssv_vibrateWhenRinging);
        if(null != vibrateRingStr && (vibrateRingStr.equals("0")||vibrateRingStr.equals("1"))){
            Settings.System.putInt(context.getContentResolver(),
                    Settings.System.VIBRATE_WHEN_RINGING, Integer.parseInt(vibrateRingStr));
        }
        if(mccMncString.equals("334020")){
            Settings.Secure.putString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED, "gps,network");
            TctLog.d(TAG,"LOCATION_PROVIDERS_ALLOWED=gps,network ,mccMncString=" +mccMncString );
        }else{
            Settings.Secure.putString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED, "");
            TctLog.d(TAG,"LOCATION_PROVIDERS_ALLOWED=null ,mccMncString=" +mccMncString );
        }
    }

    /**
     * Make sure the date format string a standart Format as follow:
     * M: mounth;
     * d: day;
     * y: year.
     * Add for Defect: 1217869
     * @param persoData
     * @return a standard data format string
     */
    private String formatConfigDateStr(String configData) {
        if (configData.contains("D"))
            configData = configData.replace('D', 'd');

        if (configData.contains("m"))
            configData = configData.replace('m', 'M');

        if (configData.contains("Y"))
            configData = configData.replace('Y', 'y');

        return configData;
    }

    //[BUGFIX]-Mod-BEGIN by TSNJ,kaibang.liu,05/17/2016,PR-2152988,
    private void setWifiHostName(Context context) {
        String ssvNetHostName = context.getString(R.string.def_ssv_wifiHostName);
        TctLog.d(TAG, "setWifiHostName ssvNetHostName = " + ssvNetHostName);
        if (!TextUtils.isEmpty(ssvNetHostName)) {
            SystemProperties.set("net.hostname", ssvNetHostName);
        }
    }
    //[BUGFIX]-Mod-END by TSNJ,kaibang.liu,

    private void setTimeZone(Context context){
        String timeZone = context.getResources().getString(com.android.internal.R.string.def_tctfw_ssv_timeZone);
        TctLog.d(TAG, "setTimeZone timeZone = " + timeZone);
        AlarmManager mAlarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        mAlarmManager.setTimeZone(timeZone);

    }
    private void setTimeOrTimeZoneStatus(Context context){
        Boolean autoTimeEnabled = context.getResources().getBoolean(com.android.internal.R.bool.def_tctfw_ssv_autoTimeEnabled);
        Boolean autoTimeZoneEnabled = context.getResources().getBoolean(com.android.internal.R.bool.def_tctfw_ssv_autoTimeZoneEnabled);
        TctLog.d(TAG,"setTimeOrTimeZone autoTimeEnabled = "+autoTimeEnabled+" autoTimeZoneEnabled = "+autoTimeZoneEnabled);
        Settings.Global.putInt(context.getContentResolver(), Settings.Global.AUTO_TIME,
                autoTimeEnabled ? 1 : 0);
        Settings.Global.putInt(context.getContentResolver(), Settings.Global.AUTO_TIME_ZONE,
                autoTimeZoneEnabled ? 1 : 0);
    }
    private void setTimeFormat(Context context) {
        Resources res = context.getResources();
        String timeFormatStr = res.getString(com.android.internal.R.string.def_tctfw_ssv_timeFormat);
        TctLog.d(TAG, "setTimeFormat dateFormatStr = " + timeFormatStr);
        if ("12".equals(timeFormatStr) || "24".equals(timeFormatStr)) {
            Settings.System.putString(context.getContentResolver(),
                    Settings.System.TIME_12_24,
                    timeFormatStr);
        }
    }
    //Add SSV control for Fastdormancy feature
    private void setDPMFeature(Context context) {
        String property =  SystemProperties.get("ro.ssv.enabled", "false");
        String  operatorChoose =  SystemProperties.get("ro.ssv.operator.choose", "");
        int dpmFeature = SystemProperties.getInt("persist.dpm.feature", 0);
        boolean isEnableFastDormancy = false;
        int dpmFeatureChanged = dpmFeature;

        if (property.equals("true") &&  "AMV".equals(operatorChoose)) {
            isEnableFastDormancy = context.getResources().getBoolean(com.android.internal.R.bool.def_tctfw_ssv_fastDormancy);
        }
        TctLog.d(TAG, "setDPMFeature isEnableFastDormancy = "+isEnableFastDormancy);
        if(isEnableFastDormancy ) {
            if ((dpmFeature & DPM_CONFIG_MASK_FASTDORMANCY) == 0) {
                dpmFeatureChanged |=  DPM_CONFIG_MASK_FASTDORMANCY;
            }
            if ((dpmFeature & DPM_CONFIG_MASK_CT) == 0) {
                dpmFeatureChanged |=  DPM_CONFIG_MASK_CT;
            }
        }
        else {
            if ( (dpmFeature & DPM_CONFIG_MASK_FASTDORMANCY) == DPM_CONFIG_MASK_FASTDORMANCY ) {
                dpmFeatureChanged &= ~DPM_CONFIG_MASK_FASTDORMANCY;
            }
            if ( (dpmFeature & DPM_CONFIG_MASK_CT ) == DPM_CONFIG_MASK_CT) {
                dpmFeatureChanged &= ~DPM_CONFIG_MASK_CT;
            }
        }

        Log.v(TAG,"setDPMFeature dpmFeatureChanged = " + dpmFeatureChanged);
        if ( dpmFeatureChanged != dpmFeature) {
            Log.v(TAG,"modify perist.dpm.feature to " + dpmFeatureChanged);
            dpmFeature = dpmFeatureChanged;
            SystemProperties.set("persist.dpm.feature", String.valueOf(dpmFeature));
        }
    }

    private void setWifiDefaultStatus(Context context){
        //[BUGFIX]-Add begin by TSNJ,kaibang.liu,12/14/2015, Defect:1118465
        boolean wifiEnabled = context.getResources().getBoolean(com.android.internal.R.bool.def_tctfw_ssv_wifiEnabled);
        WifiManager wifiMagager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiMagager != null) {
            wifiMagager.setWifiEnabled(wifiEnabled);
            TctLog.d(TAG,"setWifiDefaultStatus wifiEnabled = "+wifiEnabled);
        }

        /* MODIFIED-BEGIN by kaibang.liu-nb, 2016-05-25,BUG-2152988*/
        try {
            String wifiNetworks = context.getResources().getString(R.string.def_ssv_predefindWifi);
            TctLog.d(TAG,"setWifiDefaultStatus wifiNetworks = "+wifiNetworks);

            final List<WifiConfiguration> savedConfigs = wifiMagager.getConfiguredNetworks();

            removeWifiRecord(context, wifiMagager);

            if (!TextUtils.isEmpty(wifiNetworks)) {
                String[] wifiArray = wifiNetworks.split(";");
                StringBuilder ids = new StringBuilder();
                for (String wifi : wifiArray) {
                    String[] configs = wifi.split(",");
                    if (TextUtils.isEmpty(configs[0]) && ssidExist(savedConfigs, configs[0])) {
                        continue;
                    }

                    WifiConfiguration config = new WifiConfiguration();
                    config.SSID = configs[0];

                    int keyMgt = 0;
                    try {
                        keyMgt = Integer.parseInt(configs[1]);
                    } catch (Exception e) {
                        Log.d(TAG, "KeyMgmt is invalid");
                    }
                    config.allowedKeyManagement.set(keyMgt);

                    int id = wifiMagager.addNetwork(config);
                    if (id != -1) {
                        ids.append(id + ",");
                        wifiMagager.enableNetwork(id, false);
                        wifiMagager.saveConfiguration();
                    }
                }

                if (!TextUtils.isEmpty(ids)) {
                    SharedPreferences.Editor editor = context.getSharedPreferences(
                            "wifiNetworkIds", Context.MODE_PRIVATE).edit();
                    editor.putString("id", ids.toString());
                    editor.commit();
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "Something wrong with adding wifi network for ssv");
        }
        //[BUGFIX]-Add end by TSNJ,kaibang.liu
    }

    private void removeWifiRecord(Context context, WifiManager wifiMagager) {
        SharedPreferences sp = context.getSharedPreferences("wifiNetworkIds", Context.MODE_PRIVATE);
        String idRecords = sp.getString("id", null);

        if (!TextUtils.isEmpty(idRecords)) {
            String[] ids = idRecords.split(",");
            for (String id : ids) {
                if (!TextUtils.isEmpty(id)) {
                    wifiMagager.removeNetwork(Integer.parseInt(id));
                }
            }
        }
    }

    private boolean ssidExist(List<WifiConfiguration> configs, String ssid) {
        for (WifiConfiguration config : configs) {
            if (config != null && ssid.equals(config.SSID)) {
                return true;
            }
        }

        return false;
    }
    /* MODIFIED-END by kaibang.liu-nb,BUG-2152988*/

    private void setScreenOffTime(Context context) {
        String screenOffTime = context.getResources().getString(com.android.internal.R.string.def_tctfw_ssv_screenOffTime);
        TctLog.d(TAG , "setScreenOffTime screenOffTime = " + screenOffTime);
        if (!TextUtils.isEmpty(screenOffTime)) {
            Settings.System.putString(context.getContentResolver(),
                    Settings.System.SCREEN_OFF_TIMEOUT,screenOffTime);
        }
    }
    private void setPhoneNumberMatchLen(Context context) {
        String minMatchLen = "";//String.valueOf(PhoneNumberUtils.getMinmatchNumber("AMV"));
        SystemProperties.set(PHONEBOOK_MIN_MATCH,minMatchLen);
        TctLog.d(TAG,"setPhoneNumberMatchLen minMatchLen = "+SystemProperties.get(PHONEBOOK_MIN_MATCH, "6"));
    }
    private void setLocationMode(Context context) {
        TctLog.d(TAG,"setLocationMode ...");
        Settings.System.putString(context.getContentResolver(), "reset_location_mode", "true");
        Intent intent = new Intent(RESET_LOCATION_MODE);
        context.sendBroadcast(intent);
    }

    //[BUGFIX]-Add by TSNJ,kaibang.liu,12/14/2015, Defect:1118465
    private void setScreenRotationStatus(Context context) {
        // This REQ is just for America Movil now.
        if (!"AMV".equals(SystemProperties.get("ro.ssv.operator.choose", ""))) {
            return;
        }

        boolean autoRotation = Resources.getSystem().getBoolean(com.android.internal.R.bool.def_tctfw_ssv_autoScreenRotationEnabled);

        int rotation = autoRotation ? 1 : 0;
        int current = Settings.System.getInt(context.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, -1);
        if (rotation != current) {
            Settings.System.putInt(context.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, rotation);
        }
        TctLog.d(TAG, "setScreenRotationStatus, rotation:" + rotation);
    }
    //[BUG-FIX]-ADD-END by xiaowei.yang-nb ,13/11/2015,PR-899557
    //[BUG-FIX]-ADD-BEGIN by yong.zhou1,07/25/2016,PR-2466308
    private void setUserName(Context context) {
        UserManager userManager = (UserManager)context.getSystemService(Context.USER_SERVICE);
        String name = "Owner";//context.getResources().getString(com.android.internal.R.string.customize_owner_name);
        TctLog.d(TAG,"setUserName name ="+name);
        userManager.setUserName(UserHandle.USER_OWNER, name);
    }
    //[BUG-FIX]-ADD-END by yong.zhou1,07/25/2016,PR-2466308
}
