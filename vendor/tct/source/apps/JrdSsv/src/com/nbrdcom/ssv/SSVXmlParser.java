/**************************************************************************************************/
/*                                                                                 Date : 05/2011 */
/*                            PRESENTATION                                                        */
/*              Copyright (c) 2011 TCL Communications(Ningbo) R&D Center.                         */
/**************************************************************************************************/
/*                                                                                                */
/*    This material is company confidential, cannot be reproduced in any                          */
/*    form without the written permission of TCL Communications(Ningbo) R&D Center.               */
/*                                                                                                */
/*================================================================================================*/
/*   Author :  SHI Haojun                                                                         */
/*   Role   :  SSV                                                                                */
/*   Reference documents :                                                                        */
/*================================================================================================*/
/* Comments :                                                                                     */
/*     file    : SSVXmlParser.java                                                                */
/*     Labels  :                                                                                  */
/*================================================================================================*/
/* Modifications   (month/day/year)                                                               */
/*================================================================================================*/
/* date    |  author       |FeatureID                    |modification                            */
/*=========|===============|=============================|========================================*/
/*05/06/11 | SHI Haojun    |bug[211283]                  |Parse the xml                           */
/*================================================================================================*/
/* Problems Report(PR/CR)                                                                         */
/*================================================================================================*/
/* date    | author        | PR #                        |                                        */
/*=========|===============|=============================|========================================*/
/*=========|===============|=============================|========================================*/
/*         |               |                             |                                        */
/*================================================================================================*/
package com.nbrdcom.ssv;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.content.Context;

import com.nbrdcom.ssv.module.SSVModule;

/**
 * SSVXmlParser is a lightweight xml file parser that has a clearly module and
 * simple data structure.
 */
public class SSVXmlParser {
    private static SSVXmlParser mInstance = null;
    private boolean isParserd = false;
    private HashMap<String, SSVModule.Data> data = null;
    private ArrayList<String> moduleList = null;
    // to notify when one module parse finished.
    private Listener mListener = null;
    // current module name
    private String curModuleName = null;
    // current module data
    private SSVModuleData curModuleData = null;
    // current collection name
    private String curSetName = null;
    // is in base element
    private boolean isBase = false;
    // current base data tag. e.g bool, int ...
    private String curBaseTag = null;
    // current base data name
    private String curBaseName = null;

    private SSVXmlParser() {
    }

    /**
     * parse the hole xml file. nofity the listener when one module is finished.
     */
    public static void parse(Context context, String xmlFile, Listener listener)
            throws Exception {
        if (context == null || xmlFile == null || xmlFile.length() == 0) {
            throw new Exception("SSVXmlParser parameter error!");
        }
        if (mInstance == null) {
            mInstance = new SSVXmlParser();
            mInstance.mListener = listener;
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            InputStream is = context.getAssets().open(xmlFile);
            parser.setInput(is, "utf-8");
            mInstance.parse(parser);
        }
    }

    /**
     * retrieve the SSVXmlParser instance.
     */
    public static SSVXmlParser retrieveParser() {
        if (mInstance != null) {
            return mInstance;
        }
        return null;
    }

    /**
     * get the module data by name. if the module name not exist, it will return
     * null.
     */
    public SSVModule.Data getModuleData(String name) {
        SSVModule.Data ret = null;
        if (data.containsKey(name)) {
            ret = data.get(name);
        }
        return ret;
    }

    /**
     * get all module names. This method is deprecated.
     */
    String[] getModulesName() {
        String[] names = null;
        Object[] keys = moduleList.toArray();
        if (keys != null && keys.length > 0) {
            names = new String[keys.length];
            for (int i = 0; i < keys.length; i++) {
                names[i] = keys[i].toString();
            }
        }
        return names;
    }

    /**
     * parse the hole xml file.
     */
    private synchronized void parse(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        if (!isParserd) {
            data = new HashMap<String, SSVModule.Data>();
            moduleList = new ArrayList<String>();
            int eventType = parser.getEventType();
            while (true) {
                switch (eventType) {
                case XmlPullParser.END_DOCUMENT:
                    isParserd = true;
                    return;
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    processStartTag(parser);
                    break;
                case XmlPullParser.TEXT:
                    processText(parser);
                    break;
                case XmlPullParser.END_TAG:
                    processEndTag(parser);
                    break;
                default:
                    break;
                }
                eventType = parser.next();
            }
        }
    }

    /**
     * process the end tag.
     */
    private void processEndTag(XmlPullParser parser) {
        String name = parser.getName();
        if (name.equalsIgnoreCase("module")) {
            mListener.singleModuleParsed(curModuleName);
            curModuleData = null;
            curModuleName = null;
        } else if (name.equalsIgnoreCase("set")) {
            curSetName = null;
        } else if (name.equalsIgnoreCase("record")) {
        } else if (isBaseTag(name)) {
            isBase = false;
            curBaseTag = null;
            curBaseName = null;
        }
    }

    /**
     * process the text in element.
     */
    private void processText(XmlPullParser parser) {
        if (isBase) {
            String tmpText = parser.getText().trim();
            if (curBaseTag.equalsIgnoreCase("int")) {
                int value = Integer.parseInt(tmpText);
                curModuleData.put(curBaseName, value);
            } else if (curBaseTag.equalsIgnoreCase("bool")) {
                if (tmpText.equalsIgnoreCase("true")) {
                    curModuleData.put(curBaseName, true);
                } else {
                    curModuleData.put(curBaseName, false);
                }
            } else if (curBaseTag.equalsIgnoreCase("string")) {
                curModuleData.put(curBaseName, tmpText);
            } else if (curBaseTag.equalsIgnoreCase("float")) {
                float value = Float.parseFloat(tmpText);
                curModuleData.put(curBaseName, value);
            }
        }

    }

    /**
     * process the start tag.
     */
    private void processStartTag(XmlPullParser parser) {
        String name = parser.getName();
        if (name.equalsIgnoreCase("module")) {
            curModuleName = parser.getAttributeValue(0).trim();
            curModuleData = new SSVModuleData();
            data.put(curModuleName, curModuleData);
            moduleList.add(curModuleName);
        } else if (name.equalsIgnoreCase("set")) {
            curSetName = parser.getAttributeValue(0).trim();
        } else if (name.equalsIgnoreCase("record")) {
            cRecord tmpRecord = new cRecord();
            int count = parser.getAttributeCount();
            for (int i = 0; i < count; i++) {
                tmpRecord.put(parser.getAttributeName(i), parser
                        .getAttributeValue(i).trim());
            }
            curModuleData.put(curSetName, tmpRecord);
        } else if (isBaseTag(name)) {
            isBase = true;
            curBaseTag = name;
            curBaseName = parser.getAttributeValue(0).trim();
        }
    }

    /**
     * find out the current tag is base tag. only support string, bool, int,
     * float.
     */
    private boolean isBaseTag(String name) {
        boolean ret = false;
        if (name.equalsIgnoreCase("string")) {
            ret = true;
        } else if (name.equalsIgnoreCase("bool")) {
            ret = true;
        } else if (name.equalsIgnoreCase("int")) {
            ret = true;
        } else if (name.equalsIgnoreCase("float")) {
            ret = true;
        }
        return ret;
    }

    public interface Listener {
        void singleModuleParsed(String moduleName);
    }

    /**
     * SSV Module data, it restore the all data parsed from xml file.
     */
    private class SSVModuleData implements SSVModule.Data {
        private HashMap<String, Variant> map = new HashMap<String, Variant>();

        public Variant get(String key) {
            Variant v = null;
            if (map.containsKey(key)) {
                v = map.get(key);
            }
            return v;
        }

        public int getInt(String key, int defValue) {
            int value = defValue;
            Variant v = get(key);
            if (v != null && v.getType() == Variant.TYPE_INT) {
                value = v.toInt();
            }
            return value;
        }

        public float getFloat(String key, float defValue) {
            float value = defValue;
            Variant v = get(key);
            if (v != null && v.getType() == Variant.TYPE_FLOAT) {
                value = v.toFloat();
            }
            return value;
        }

        public String getString(String key, String defValue) {
            String value = defValue;
            Variant v = get(key);
            if (v != null && v.getType() == Variant.TYPE_STRING) {
                value = v.toString();
            }
            return value;
        }

        public boolean getBoolean(String key, boolean defValue) {
            boolean value = defValue;
            Variant v = get(key);
            if (v != null && v.getType() == Variant.TYPE_BOOL) {
                value = v.toBool();
            }
            return value;
        }

        public Record[] getRecords(String key) {
            Record[] record = null;
            Variant v = get(key);
            if (v != null && v.getType() == Variant.TYPE_RECORDS) {
                record = v.toRecords();
            }
            return record;
        }

        public boolean containsKey(String key) {
            return map.containsKey(key);
        }

        public Iterator<Entry<String, Variant>> getAll() {
            return map.entrySet().iterator();
        }

        void put(String key, int value) {
            if (key != null && key.length() != 0) {
                map.put(key, new cVariant(new Integer(value), Variant.TYPE_INT));
            }
        }

        void put(String key, float value) {
            if (key != null && key.length() != 0) {
                map.put(key, new cVariant(new Float(value), Variant.TYPE_FLOAT));
            }
        }

        void put(String key, String value) {
            if (key != null && key.length() != 0) {
                map.put(key, new cVariant(new String(value),
                        Variant.TYPE_STRING));
            }
        }

        void put(String key, boolean value) {
            if (key != null && key.length() != 0) {
                map.put(key,
                        new cVariant(new Boolean(value), Variant.TYPE_BOOL));
            }
        }

        void put(String key, Record value) {
            if (key != null && key.length() != 0) {
                if (map.containsKey(key)) {
                    cVariant v = (cVariant) map.get(key);
                    if (v.getType() == Variant.TYPE_RECORDS) {
                        v.addRecord(value);
                    }
                } else {
                    ArrayList<Record> al = new ArrayList<Record>();
                    cVariant v = new cVariant(al, Variant.TYPE_RECORDS);
                    v.addRecord(value);
                    map.put(key, v);
                }
            }
        }
    }

    /**
     * all data parsed from xml file will packaged to Variant.
     */
    private class cVariant implements SSVModule.Data.Variant {
        Object obj = null;
        int type = TYPE_KNOWN;

        public cVariant(Object value, int type) {
            if (value != null) {
                this.obj = value;
                this.type = type;
            }
        }

        public int getType() {
            return type;
        }

        public int toInt() {
            if (type == cVariant.TYPE_INT) {
                return (Integer) obj;
            }
            return 0;
        }

        public float toFloat() {
            if (type == cVariant.TYPE_FLOAT) {
                return (Float) obj;
            }
            return 0;
        }

        public boolean toBool() {
            if (type == cVariant.TYPE_BOOL) {
                return (Boolean) obj;
            }
            return false;
        }

        public String toString() {
            if (type == cVariant.TYPE_STRING) {
                return obj.toString();
            }
            return null;
        }

        public void addRecord(SSVModule.Data.Record r) {
            if (type == cVariant.TYPE_RECORDS) {
                @SuppressWarnings("unchecked")
                ArrayList<SSVModule.Data.Record> al = (ArrayList<SSVModule.Data.Record>) obj;
                al.add(r);
            }
        }

        public SSVModule.Data.Record[] toRecords() {
            SSVModule.Data.Record[] record = null;
            if (type == cVariant.TYPE_RECORDS) {
                ArrayList<?> al = (ArrayList<?>) obj;
                if (al != null && al.size() > 0) {
                    record = new cRecord[al.size()];
                    for (int i = 0; i < al.size(); i++) {
                        record[i] = (SSVModule.Data.Record) al.get(i);
                    }
                }
            }
            return record;
        }
    }

    /**
     * Record data type .
     */
    private class cRecord implements SSVModule.Data.Record {
        private HashMap<String, String> map = new HashMap<String, String>();
        private Object[] values = null;
        private Object[] keys = null;

        void put(String key, String value) {
            map.put(key, value);
        }

        public int count() {
            return map.size();
        }

        public String getKey(int index, String defValue) {
            String key = defValue;
            if (keys == null) {
                keys = map.keySet().toArray();
            }
            if (keys != null && index < keys.length) {
                key = keys[index].toString();
            }
            return key;
        }

        public String getValue(int index, String defValue) {
            String value = defValue;
            if (values == null) {
                values = map.values().toArray();
            }
            if (values != null && index < values.length) {
                value = values[index].toString();
            }
            return value;
        }

        public String getValue(String key, String defValue) {
            String value = defValue;
            if (map.containsKey(key)) {
                value = map.get(key);
            }
            return value;
        }
    }
}
