/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 02/11/2014|gong .Zhang           |CR-578385             |Update APN by CP  */
/* ----------|----------------------|----------------------|----------------- */
/* 05/08/2014|     Song Fuqiang     |      FR-645242       |[HOMO][SFR][OMAC- */
/*           |                      |                      |P] Email configu- */
/*           |                      |                      |ration failed     */
/* ----------|----------------------|----------------------|----------------- */
/* 05/15/2014|      Changjiu.Guo    |       FR641588       |[HOMO][HOMO][TMO] */
/*           |                      |                      |The MMS new profile*/
/*           |                      |                      |should be created */
/*           |                      |                      |and activated by default*/
/* ----------|----------------------|----------------------|----------------- */
/* 08/29/2014|     dagang.yang      |        777809        |[Wap push&CP]     */
/*           |                      |                      |       [Dual]There*/
/*           |                      |                      |is APN but not    */
/*           |                      |                      |activate after    */
/*           |                      |                      |receive a CP and  */
/*           |                      |                      |install it        */
/* ----------|----------------------|----------------------|----------------- */
/* 01/16/2015|     zhichuan.wei     |        879273        |[Clone][OMA Provi */
/*           |                      |                      |sioning]The insta */
/*           |                      |                      |lled provision apn*/
/*           |                      |                      |is not the default*/
/*           |                      |                      |APN               */
/* ----------|----------------------|----------------------|----------------- */
/* 03/05/2015|     lilin            |        936139        |[Clone][OMA Provi */
/*           |                      |                      |sioning]mms provison */
/*           |                      |                      |install abnormal when */
/*           |                      |                      |use double SIM cards*/
/*           |                      |                      |                  */
/* ----------|----------------------|----------------------|----------------- */
/* 04/29/2015|     dagang.yang      |        984231        |[HOMO][EUSKALTEL] */
/*           |                      |                      |[OMA Provisioning]*/
/*           |                      |                      |OTA configuration */
/*           |                      |                      |messages are not  */
/*           |                      |                      |visible and not   */
/*           |                      |                      |selected as       */
/*           |                      |                      |default after     */
/*           |                      |                      |installing        */
/* ----------|----------------------|----------------------|----------------- */
/* 12/18/2015|     Fuqiang.Song     |       1177364        |[Provision]The s- */
/*           |                      |                      |elected APN is c- */
/*           |                      |                      |hanged after ins- */
/*           |                      |                      |tall the CP with  */
/*           |                      |                      |MMS APN + Homepa- */
/*           |                      |                      |ge                */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.otaprovisioningclient.persister;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.telephony.TelephonyManager;
import android.util.TctLog;
import android.util.Log; // MODIFIED by zhaoyan, 2016-05-23,BUG-2024523
import android.telephony.SubscriptionManager;

import com.android.otaprovisioningclient.oma.ClientProvAccessPoint;
import com.android.otaprovisioningclient.oma.WapProvisioningDoc;
//[FEATURE]-Add-BEGIN by TCTNB.gong.zhang, 2014/02/11, CR-578385 requirement for OMA profile install
import com.android.otaprovisioningclient.R;


//[FEATURE]-Add-END by TCTNB.gong.zhang,
import java.util.ArrayList;

import android.text.TextUtils;
import android.util.Log;
/*
 * active the config and store it in the table.
 */

public class ApnPersister {

    // fixbug, active the apn config after saving it
    private static Uri mActiveUri = null;
    private static final String PREFERRED_APN_URI = "content://telephony/carriers/preferapn";
    private static final String APN_ID = "apn_id";
    private static WapProvisioningDoc mProvDoc;

    public static boolean saveAP(Context context, WapProvisioningDoc provDoc) {
        //[BUGFIX]-Mod-BEGIN by TCTNB.Fuqiang.Song,12/18/2015,1177364,
        //[Provision]The selected APN is changed after install the CP with MMS APN + Ho-
        //mepage,Clone from defect 1035114
        String newPrefAPN = null;
        String orgPrefAPN = getSelectedApnKey(context);
        //[BUGFIX]-Mod-END by TCTNB.Fuqiang.Song
        mProvDoc = provDoc;
        mActiveUri = null;
        boolean saveSuccess = false;
        Uri uri = Uri.parse("content://telephony/carriers");
        ContentResolver contentResolver = context.getContentResolver();

        AccessPointFactory accessPointFactory = new AccessPointFactory(provDoc);
        ArrayList<ClientProvAccessPoint> accessPointList = accessPointFactory
                .createAccessPoints();

        if (null == accessPointList || accessPointList.isEmpty()) {
            return false;
            /*
             * if CP only create Email accounts or set browser home page,No APN,
             * the file is also valid.
             */

        }

        //[FEATURE]-Add-BEGIN by TCTNB.Fuqiang.song, 2014/05/08, PR-645242,
        //SFR does not need to configure apn by OMACP
        if (context.getResources().getBoolean(R.bool.feature_otaprovisioning_SFRProvision_on)){
            TctLog.i("ApnPersister", "SFR does not need configure apn by OMACP");
            return false;
        }
        //[FEATURE]-Add-END by TCTNB.Fuqiang.Song

        for (ClientProvAccessPoint accessPoint : accessPointList) {

            if (null != accessPoint.proxy || null != accessPoint.napDef) {
                ContentValues contentValues = new ContentValues();

                // value the elements of apn
                populateApn(contentValues, accessPoint);
                /* MODIFIED-BEGIN by zhaoyan, 2016-05-23,BUG-2048219*/
                //add by yan.zhao@jrdcom.com for Defect2024523 at 2016/05/23 begin
                Log.i("ApnPersister", "contentValues--tostring()="+contentValues.toString());
                //add by yan.zhao@jrdcom.com for Defect2024523 at 2016/05/23 end
                /* MODIFIED-END by zhaoyan,BUG-2048219*/
                //PR936139-li-lin-001 begin
                if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                    long sub = sp.getLong("getSubID",SubscriptionManager.getDefaultDataSubscriptionId());
                    contentValues.put("sub_id", sub);
                  }
               //PR936139-li-lin-001 end
                contentValues.put("apnnew", 1); // MODIFIED by fliu2, 2016-05-23,BUG-2197942
                updateCurrentFlag(context, contentValues);
                if (contentValues.getAsString("name") == null
                        || contentValues.getAsString("name").equals("")
                        || contentValues.getAsString("apn") == null
                        || contentValues.getAsString("apn").equals("")
                        || contentValues.getAsString("type") == null
                        || contentValues.getAsString("type").equals("")) {

                    continue;

                }

                // Update APN by CP
                /* MODIFIED-BEGIN by zhaoyan, 2016-05-23,BUG-2048219*/
                //mod by yan.zhao for Defect2024523 at 2016/05/23 begin
                String[] CONTENT_PROJECTION = new String[] { "_id", "name","numeric", "apn", "user", "server", "password",
                        "proxy", "port", "mmsproxy", "mmsport", "mmsc","authtype", "type", "protocol","new_apn", "roaming_protocol" };
                //mod by yan.zhao for Defect2024523 at 2016/05/23 end
                String numeric = contentValues.getAsString("numeric");
                String selection = "numeric = '" + numeric + "' AND name = '"+ contentValues.getAsString("name") + "'";
                Cursor cursor = null;
                try {
                   cursor = contentResolver.query(uri, CONTENT_PROJECTION, selection, null, null);
                   if (cursor != null) {
                      //mod by yan.zhao for Defect2024523 at 2016/05/23 begin
                      //Keep editable of apns in the mobile after received CP apn ,NOTICE: Depend on FR467218
                      if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()
                            && (cursor.getInt(cursor.getColumnIndexOrThrow("new_apn")) == -1)) {
                         contentValues.put("new_apn", -1);
                      } else {
                          contentValues.put("new_apn", 0);
                      }
                   //mod by yan.zhao for Defect2024523 at 2016/05/23 end
                   /* MODIFIED-END by zhaoyan,BUG-2048219*/
                   //Considering "default and mms " types are combined in one apn of the mobile,
                   //although CP send a "default" typ's apn,we should legacy the mms apn.
                   //[FEATURE]-Add-BEGIN by TCTNB.gong.zhang, 2014/02/11, CR-578385 requirement for OMA profile install
                   if (context.getResources().getBoolean(R.bool.feature_otaprovisioning_UpdateApn_on)){
                   if(cursor.getCount()>0 && cursor.moveToFirst()){
                   String apnType = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.TYPE));
                   if (contentValues.getAsString("type").equalsIgnoreCase("default")
                         && ((!apnType.isEmpty()) && ((apnType.contains("mms")) || (apnType.contains("default"))))) {
                      String mmsProxy = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.MMSPROXY));
                      String mmsPort = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.MMSPORT));
                      String mmsc = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.MMSC));

                      contentValues.put(Telephony.Carriers.MMSPROXY, mmsProxy);
                      contentValues.put(Telephony.Carriers.MMSPORT, mmsPort);
                      contentValues.put(Telephony.Carriers.MMSC, mmsc);
                      contentValues.put(Telephony.Carriers.TYPE, "default,mms");
                      TctLog.i("ApnPersister", "CP apn is default apn, merge mms configure from old apn to new one");
                      TctLog.i("ApnPersister", "old apn: mmsProxy = " + mmsProxy + "; mmsPort = " + mmsPort + "; mmsc = " + mmsc);
                   } else if (contentValues.getAsString("type").equalsIgnoreCase("mms")
                                && ((!apnType.isEmpty()) && ((apnType.contains("mms")) || (apnType.contains("default"))))) {
                             String user = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.USER));
                             String server = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.SERVER));
                             String password = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.PASSWORD));
                             String proxy = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.PROXY));
                             String port = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.PORT));

                             if (contentValues.getAsString(Telephony.Carriers.USER).isEmpty()) {
                                contentValues.put(Telephony.Carriers.USER, user);
                             }
                             if (contentValues.getAsString(Telephony.Carriers.SERVER).isEmpty()) {
                                contentValues.put(Telephony.Carriers.SERVER, server);
                             }
                             if (contentValues.getAsString(Telephony.Carriers.PASSWORD).isEmpty()) {
                                contentValues.put(Telephony.Carriers.PASSWORD, password);
                             }
                             if (contentValues.getAsString(Telephony.Carriers.PROXY).isEmpty()) {
                                contentValues.put(Telephony.Carriers.PROXY, proxy);
                             }
                             if (contentValues.getAsString(Telephony.Carriers.PORT).isEmpty()) {
                                contentValues.put(Telephony.Carriers.PORT, port);
                             }
                             contentValues.put(Telephony.Carriers.TYPE, "default,mms");
                             TctLog.i("ApnPersister", "CP apn is mms apn, merge default configure from old apn to new one");
                             TctLog.i("ApnPersister", "old apn: user = " + user + "; server = " + server + "; password = " + password
                                    + "; proxy = " + proxy + "; port = " + port);
                          }
                       }
                   }
                   //[FEATURE]-Add-END by TCTNB.gong.zhang,
                   }
                } catch (Exception e) {
                } finally {
                   if (cursor != null)
                      cursor.close();
                }

                //PR1001158-xiaobin-yang-001 begin
                long[] mmsIds = null;
                boolean activeMms = context.getResources().getBoolean(R.bool.feature_otaprovisioning_ActiveMms_on);
                if(activeMms){
                   mmsIds = queryApnByType(context,numeric,"mms","");
                }
                //PR1001158-xiaobin-yang-001 end

                String apnName = contentValues.getAsString("name");
                TctLog.i("ApnPersister", "saveAP()  apnName = " + apnName);

                // fuwenliang, PR1073531, begin
                //contentResolver.delete(uri, "name=\"" + apnName + "\"", null);
                contentResolver.delete(uri, "name=\'" + apnName + "\'", null);
                // fuwenliang, PR1073531, end

                //Changed by fei.liu for defect-2116443,05/13/2016,begin
                if (activeMms && mmsIds!=null && mmsIds.length>0 && contentValues.getAsString("type") != null
                        && contentValues.getAsString("type").equals("mms")) {
                    for (long i : mmsIds) {
                        deleteApnById(context, i);
                    }
                }
                //Changed by fei.liu for defect-2116443,end

                //[BUGFIX]-ADD-BEGIN by TSCD.dagang.yang,04/29/2015, PR-984231
                if(context.getResources().getBoolean(R.bool.feature_otaprovisioning_overwriteviaapn_on)){
                     String apn = contentValues.getAsString("apn");
                     setContentValuesByApn(context,contentValues);
                     if(!apn.isEmpty()){
                         // fuwenliang, PR1073531, begin
                         //contentResolver.delete(uri, "apn=\"" + apn + "\"", null);
                         contentResolver.delete(uri, "apn=\'" + apn + "\'", null);
                         // fuwenliang, PR1073531, end
                     }
                }
                //[BUGFIX]-ADD-END by TSCD.dagang.yang,04/29/2015
                Uri insertedUri = contentResolver.insert(uri, contentValues);
                if (insertedUri != null) {
                    saveSuccess = true;
                    //PR1001158-xiaobin-yang-001 begin
                    //Delete behavior should be done before insert,changed by fei.liu for defect-2116443,05/13/2016,begin
                    /*if (activeMms && mmsIds!=null && mmsIds.length>0 && contentValues.getAsString("type") != null
                            && contentValues.getAsString("type").equals("mms")) {
                        for (long i : mmsIds) {
                            deleteApnById(context, i);
                            }
                    }*/
                    //Changed by fei.liu for defect-2116443,end
                    //PR1001158-xiaobin-yang-001 end
                }
                TctLog.i("ApnPersister", "saveAP: " + "uri = " + insertedUri);
                String type = contentValues.getAsString("type");
                String appType = accessPoint.mAppType;//Add by xiaobin.yang for fix the BUG1001223

                //[FEATURE]-Add-BEGIN by TSCD.changjiu.guo,05/15/2014,FR641588
                //if ((null != type) && (type.equals("default"))
                //        && (null == mActiveUri)) {
                if ((null != type) && (insertedUri != null)
                          && (null == mActiveUri)) {
                //[FEATURE]-Add-END by TSCD.changjiu.guo,05/15/2014,FR641588
                    mActiveUri = insertedUri;

                     //[BUGFIX]-Mod-BEGIN by TCTNB.Fuqiang.Song,12/18/2015,1177364,
                    //[Provision]The selected APN is changed after install the CP with MMS APN + Ho-
                    //mepage,Clone from defect 1035114
                    if(mActiveUri!=null){
                        newPrefAPN = mActiveUri.getLastPathSegment();
                    }
                    //[BUGFIX]-Mod-END by TCTNB.Fuqiang.Song

                    //[BUGFIX]-ADD-BEGIN by TSCD.dagang.yang,04/29/2015, PR-984231
                    if(!type.equalsIgnoreCase("mms")){
                        //Add by xiaobin.yang for fix the BUG1001223 begin
                        /*if ((appType.equalsIgnoreCase("w2") && accessPointList.size() == 1) || appType.equalsIgnoreCase("default")) {
                        }*/
                        activeApn(context);
                        //Add by xiaobin.yang for fix the BUG1001223 end
                    }
                    //[BUGFIX]-ADD-END by TSCD.dagang.yang,04/29/2015
                }
            }
        }
        //[BUGFIX]-Mod-BEGIN by TCTNB.Fuqiang.Song,12/18/2015,1177364,
        //[Provision]The selected APN is changed after install the CP with MMS APN + Ho-
        //mepage,Clone from defect 1035114
        String activeApnType = queryActiveApnType(context,newPrefAPN);
        if(!activeApnType.contains("default")){
            activeOrgApn(context,orgPrefAPN);
        }
        //[BUGFIX]-Mod-END by TCTNB.Fuqiang.Song
        return saveSuccess;
    }

    private static void populateApn(ContentValues contentValues,
            ClientProvAccessPoint accessPoint) {
        if (null != accessPoint.proxy) {
            contentValues.put("proxy", accessPoint.proxy.proxy);
            contentValues.put("port", accessPoint.proxy.port);
            contentValues.put("mmsproxy", accessPoint.proxy.mmsproxy);
            contentValues.put("mmsport", accessPoint.proxy.mmsport);
            contentValues.put("mmsc", accessPoint.proxy.mmsc);
        } else {
            contentValues.put("proxy", (String) null);
            contentValues.put("port", (String) null);
            contentValues.put("mmsproxy", (String) null);
            contentValues.put("mmsport", (String) null);
            contentValues.put("mmsc", (String) null);
        }

        if (null != accessPoint.napDef) {
            contentValues.put("name", accessPoint.napDef.name);
            contentValues.put("apn", accessPoint.napDef.apn);
            contentValues.put("type", accessPoint.napDef.type);
            if (null != accessPoint.napDef.accessPointAuth) {
                String authType = accessPoint.napDef.accessPointAuth.authType;
                int authValue = 0;
                if (null != authType) {
                    if ("PAP".equals(authType)) {
                        authValue = 1;
                    } else if ("CHAP".equals(authType)) {
                        authValue = 2;
                    }
                }
                contentValues.put("authtype", new Integer(authValue));
                contentValues.put("user",
                        accessPoint.napDef.accessPointAuth.userName);
                contentValues.put("password",
                        accessPoint.napDef.accessPointAuth.userSecret);
            }

            if (null != accessPoint.napDef.accessPointValidity) {
                contentValues.put("mcc",
                        accessPoint.napDef.accessPointValidity.country);
                contentValues.put("mnc",
                        accessPoint.napDef.accessPointValidity.network);
                contentValues
                        .put("numeric",
                                accessPoint.napDef.accessPointValidity.country
                                        + accessPoint.napDef.accessPointValidity.network);
            } else {
                contentValues.put("mcc", mProvDoc.mcc);
                contentValues.put("mnc", mProvDoc.mnc);
                String numeric = "";
                if (mProvDoc.mcc != null && !"".equals(mProvDoc.mcc)
                        && mProvDoc.mnc != null && !"".equals(mProvDoc.mnc)) {
                    numeric = mProvDoc.mcc + mProvDoc.mnc;
                }
                contentValues.put("numeric", numeric);
            }

        } else {
            contentValues.put("name", (String) null);
            contentValues.put("apn", (String) null);
            contentValues.put("type", (String) null);
            contentValues.put("user", (String) null);
            contentValues.put("password", (String) null);
            contentValues.put("mcc", mProvDoc.mcc);
            contentValues.put("mnc", mProvDoc.mnc);
            String numeric = "";
            if (mProvDoc.mcc != null && !"".equals(mProvDoc.mcc)
                    && mProvDoc.mnc != null && !"".equals(mProvDoc.mnc)) {
                numeric = mProvDoc.mcc + mProvDoc.mnc;
            }
            contentValues.put("numeric", numeric);
        }
    }

    private static void updateCurrentFlag(Context context,
            ContentValues contentValues) {
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        String numeric = telephonyManager.getSimOperator();
        String currentMcc = null;
        String currentMnc = null;

        if (numeric != null && numeric.length() > 4) {
            currentMcc = numeric.substring(0, 3);
            currentMnc = numeric.substring(3);
        }

        if (currentMnc != null && currentMnc.equals(contentValues.get("mnc"))
                && currentMcc != null
                && currentMcc.equals(contentValues.get("mcc"))) {
            contentValues.put("current", 1);
        }
    }

    // active the apn config after saving it
    private static void activeApn(Context context) {
        if (mActiveUri != null) {
            ContentResolver resolver = context.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(APN_ID, mActiveUri.getLastPathSegment());
            //[BUGFIX]-MOD-BEGIN by TSCD.dagang.yang,08/29/2014, PR-777809
            Uri mtempUri = null;
            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                //[BUGFIX]-Add-BEGIN by TCTNJ.(zhichuan.wei),01/16/2015 for PR879273
                //change activeApn for idol3
//                int sub = sp.getInt("getSubID", 1);
//                if(sub < 1 || sub > 2){
//                   sub = 1;
//                }
                long sub = sp.getLong("getSubID",SubscriptionManager.getDefaultDataSubscriptionId());//PR918525-Quanshui-Ye-001
                mtempUri = Uri.parse(PREFERRED_APN_URI + "/subId/" + sub);
                //[BUGFIX]-Add-END by TCTNJ.(zhichuan.wei)
            } else {
                mtempUri = Uri.parse(PREFERRED_APN_URI);
            }
            resolver.update(mtempUri, values, null, null);
            //[BUGFIX]-MOD-END by TSCD.dagang.yang,08/29/2014
        }
    }

    //[BUGFIX]-ADD-BEGIN by TSCD.dagang.yang,04/29/2015, PR-984231
    private static void setContentValuesByApn(Context context,ContentValues contentValues){
        // Update APN by CP
        String[] CONTENT_PROJECTION = new String[] { "mmsproxy", "mmsport", "mmsc","mvno_type","mvno_match_data"};
        String numeric = contentValues.getAsString("numeric");
        /* MODIFIED-BEGIN by zhaoyan, 2016-05-23,BUG-2048219*/
        //add by yan.zhao@tcl.com for Defect2024523 at 2016/05/23 begin
        //String selection = "numeric = '" + numeric + "' AND apn = '"+ contentValues.getAsString("apn") + "'";
        String selection = "numeric = \'" +numeric + "\' and ( apn = \'" + contentValues.getAsString("apn")
                + "\' or name = \'" + contentValues.getAsString("name") +"\')";
        //add by yan.zhao@tcl.com for Defect2024523 at 2016/05/23 end
        /* MODIFIED-END by zhaoyan,BUG-2048219*/
        Cursor cursor = null;
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = Uri.parse("content://telephony/carriers");
        try {
              cursor = contentResolver.query(uri, CONTENT_PROJECTION, selection, null, null);
              if (cursor != null) {
              if(cursor.getCount()>0 && cursor.moveToFirst()){
                   String moveType = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.MVNO_TYPE));
                   String moveMatchData = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.MVNO_MATCH_DATA));
                   String mmsProxy = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.MMSPROXY));
                   String mmsPort = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.MMSPORT));
                   String mmsc = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Carriers.MMSC));
                if(!moveType.isEmpty()){
                  contentValues.put(Telephony.Carriers.MVNO_TYPE, moveType);
                }
               if(!moveMatchData.isEmpty()){
                  contentValues.put(Telephony.Carriers.MVNO_MATCH_DATA, moveMatchData);
               }
               if(contentValues.getAsString("type").equalsIgnoreCase("mms")){
                   if( null == contentValues.getAsString("mmsproxy") && !mmsProxy.isEmpty()){
                        contentValues.put(Telephony.Carriers.MMSPROXY, mmsProxy);
                    }
                   if(null == contentValues.getAsString("mmsPort")&& !mmsProxy.isEmpty()){
                        contentValues.put(Telephony.Carriers.MMSPORT, mmsPort);
                   }
                   if(null == contentValues.getAsString("mmsc") && !mmsProxy.isEmpty()){
                        contentValues.put(Telephony.Carriers.MMSC, mmsc);
                    }
               }
            }
          }
              /* MODIFIED-BEGIN by zhaoyan, 2016-05-23,BUG-2048219*/
              //mod by yan.zhao for Defect2024523 at 2016/05/23 begin
              String mvnoType = contentValues.getAsString(Telephony.Carriers.MVNO_TYPE);
              String mvnoData = contentValues.getAsString(Telephony.Carriers.MVNO_MATCH_DATA);
              Log.d("ApnPersister", "new apn: mvnoType = " + mvnoType + ", mvnoData = " + mvnoData);
              //            if(TextUtils.isEmpty(mvnoType) || TextUtils.isEmpty(mvnoData)){
              boolean insertMVNO = context.getResources().getBoolean(R.bool.feature_otaprovisioning_insertMVNOforapn_on);
              if(insertMVNO) {
                  if ("21403".equals(numeric)) {
                      SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
                      long subId = sp.getLong("getSubID", SubscriptionManager.getDefaultDataSubscriptionId());
                      String spn = TelephonyManager.getDefault().getSimOperatorName((int)subId);
                      if (null!=spn && spn.length()>0 && !spn.equals(mvnoData)) {
                          contentValues.put(Telephony.Carriers.MVNO_TYPE, "spn");
                          contentValues.put(Telephony.Carriers.MVNO_MATCH_DATA, spn);
                          Log.d("ApnPersister", "new apn: spn = " + spn);
                          }
                      }
                  }//mod by yan.zhao for Defect2024523 at 2016/05/23 end
                  /* MODIFIED-END by zhaoyan,BUG-2048219*/
        } catch (Exception e) {
        } finally {
           if (cursor != null)
           cursor.close();
        }
    }
    //[BUGFIX]-ADD-END by TSCD.dagang.yang,04/29/2015

    //PR1001158-xiaobin-yang-001 begin
    private static long[] queryApnByType(Context context, String numeric,
            String ApnType, String ApnId) {
        long[] id = null;
        int i = 0;
        Cursor cursor = null;
        Uri mUri = Uri.parse("content://telephony/carriers");
        ContentResolver mContentResolver = context.getContentResolver();
        try {
            String where = "type = '" + String.valueOf(ApnType)
                    + "' AND numeric = '" + String.valueOf(numeric) + "'";

            cursor = mContentResolver.query(mUri,
                    new String[] { Telephony.Carriers._ID }, where, null,
                    Telephony.Carriers.DEFAULT_SORT_ORDER);
            while (cursor.moveToNext()) {
                if (id == null) {
                    id = new long[cursor.getCount()];
                }
                id[i++] = Long.parseLong(cursor.getString(0));

            }
        } catch (SQLException e) {
            TctLog.i("ApnPersister", "QUERY SQLException happened!");
        } catch (UnsupportedOperationException e) {
            TctLog.i("ApnPersister", "QUERY UnsupportedOperationException happened!");
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return id;
    }
    private static boolean deleteApnById(Context context, long id) {
        int rows = -1;
        Uri mUri = Uri.parse("content://telephony/carriers");
        ContentResolver mContentResolver = context.getContentResolver();
        try {
            String where = "_id = '" + String.valueOf(id) + "'";
            rows = mContentResolver.delete(mUri, where, null);
        } catch (SQLException e) {
            TctLog.i("ApnPersister", "deleteApn SQLException happened!");
        } catch (UnsupportedOperationException e) {
            TctLog.i("ApnPersister", "deleteApn UnsupportedOperationException happened!");
        }
        return (rows > 0) ? true : false;
    }
    //PR1001158-xiaobin-yang-001 end

       //[BUGFIX]-Mod-BEGIN by TCTNB.Fuqiang.Song,12/18/2015,1177364,
       //[Provision]The selected APN is changed after install the CP with MMS APN + Ho-
       //mepage,Clone from defect 1035114
        private static String queryActiveApnType(Context context, String newPrefAPN) {
        String apnType = "";
        Uri mUri = Uri.parse("content://telephony/carriers");
        Cursor cursor = null;
        ContentResolver mContentResolver = context.getContentResolver();
        try {
            String where = "_id = '" + newPrefAPN + "'";
            Log.i("ApnPersister", "queryActiveApnType where = "+ where);
            cursor = mContentResolver.query(mUri,
                    new String[] { Telephony.Carriers.TYPE }, where, null,
                    Telephony.Carriers.DEFAULT_SORT_ORDER);
            while (cursor.moveToNext()) {
                apnType = cursor.getString(0);
            }

        } catch (SQLException e) {
            Log.i("ApnPersister",
                    "QUERY SQLException happened! " + e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return apnType;
    }
    /**
     * update the preferred apn
     */
    private static void activeOrgApn(Context context,String apnId) {
            ContentResolver resolver = context.getContentResolver();
            ContentValues values = new ContentValues();
            if(TextUtils.isEmpty(apnId)){
                return;
            }
            values.put(APN_ID, apnId);
            resolver.update(Uri.parse(PREFERRED_APN_URI), values, null, null);
    }

    /**
     * get the preferred apn's id
     */
    private static String getSelectedApnKey(Context context) {
        String key = null;

        Cursor cursor = context.getContentResolver().query(Uri.parse(PREFERRED_APN_URI), new String[] {"_id"},
                null, null, Telephony.Carriers.DEFAULT_SORT_ORDER);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            key = cursor.getString(0);
        }
        cursor.close();
        return key;
    }
    //[BUGFIX]-Mod-END by TCTNB.Fuqiang.Song
}
