/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 01/24/2014|      Xu Danbin       |      PR-594556       |fix force close   */
/*           |                      |                      |caused by illegal */
/*           |                      |                      | param            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.otaprovisioningclient.oma;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Characteristic {
    public String type; //type attribute of the Characteristic

    //the legal parm sub-elements of the Characteristic
    private ArrayList<Parm> mParms = new ArrayList<Parm>();

    //the legal inner sub-Characteristic of the Characteristic
    private ArrayList<Characteristic> mInnerCharacteristics = new ArrayList<Characteristic>();

//---PR 230218: NMR: store namespace, start =====================
    public String mNameSpace;
//---PR 230218: NMR: store namespace, over =====================

//---PR 230218: NMR: store namespace, start =====================
    public Characteristic(String mType, String nameSpace){
        type = mType;
    mNameSpace = nameSpace;
    }
//---PR 230218: NMR: store namespace, over =====================

    //[BUGFIX]-Add&Mod-BEGIN by TCTNB.Xu Danbin,01/24/2014,PR-594556,
    //fix force close caused by illegal param
    /**
     * add a parm element to the Characteristic
     * @param mParm current Parm being parsed
     * @return true if the Parm is added or false if not added
     */
    public boolean addParm(Parm mParm){
        if(isLegalParm(mParm.name)){
            return mParms.add(mParm);
        }
        return false;
        //[BUGFIX]-Add&Mod-END by TCTNB.Xu Danbin
    }

    /**
     * add a inner Characteristic element to the Characteristic
     * @param mCharacteristic current Characteristic being parsed
     * @return void
     */
    public void addInnerCharacteristic(Characteristic mCharacteristic){
        if(isLegalCharacteristic(mCharacteristic)){
            mInnerCharacteristics.add(mCharacteristic);
        }
    }

    /**
     * get all the parm element of the Characteristic
     * @return an arraylist of all the parm
     */
    public ArrayList<Parm> getAllParms(){
        if(null==mParms || 0==mParms.size()){
            return null;
        }
        return mParms;
    }

    /**
     * get an parm element of the Characteristic
     * @param parmName name of a parm
     * @return an arraylist of the parm enclosed in the characteristic
     */
    public ArrayList<Parm> getParm(String parmName){
        if( (null==parmName) || (parmName.equals(""))){
            return null;
        }
        ArrayList<Parm> specParm = new ArrayList<Parm>();
        ArrayList<Parm> allParms = getAllParms();
        if(null==allParms || allParms.isEmpty()){
            return null;
        }
        for(Parm curParm:allParms){
            if (parmName.equals(curParm.name)){
                specParm.add(curParm);
            }
        }
        return specParm;

    }

    public void removeParm(Parm parm){
        if(null!=parm && null!=mParms && !mParms.isEmpty()){
             for(int i=0;i<mParms.size();i++){
                 if(mParms.get(i).equals(parm)){
                     mParms.remove(i);
                     break;
                 }
             }
        }

    }

    /**
     * get all the inner-Characteristic element of the Characteristic
     * @return an Iterator of the inner-Characteristic
     */
    public Iterator<Characteristic> getInnerCharacteristic(){
        if (null!=mInnerCharacteristics && mInnerCharacteristics.size()>0){
            return mInnerCharacteristics.iterator();
        }else{
            return null;
        }
    }

    public ArrayList<Characteristic> getInnerCharacteristic(String type){
        ArrayList<Characteristic> charaList = new ArrayList<Characteristic>();
        if(null!=type && null!=mInnerCharacteristics && !mInnerCharacteristics.isEmpty()){
            for(Characteristic inner:mInnerCharacteristics){
                if(inner.type.equals(type)){
                    charaList.add(inner);
                }
            }
        }
        return charaList;
    }



    /**
     * check whether the current parm is a legal parm
     * @param mParmName name attribute of the Parm
     * @return true if the Parm is legal,
     *         false if parm name is null or not a sub-element of the characteristic
     *         or parm is redundant
     */
    private boolean isLegalParm(String mParmName){
        boolean isLegal = true;
        if ((null==mParmName) ||(mParmName.equals(""))){
            isLegal=false;
            return isLegal;
        }
        ArrayList<String> parms = Element.getSingleInstance().getCharacteristicParms(type);
        if(null == parms){
            isLegal=false;
            return isLegal;
        }
        if(parms.contains(mParmName)){
            int maxRedef = 1;
            HashMap<String,Integer> parmsSize = Element.getSingleInstance().getCharacteristicParmsSize(type);
            if (null != parmsSize){
                if(parmsSize.containsKey(mParmName)){
                    maxRedef = parmsSize.get(mParmName).intValue();
                }
                int firstIndex = mParms.indexOf(mParmName);
                int lastIndex = mParms.lastIndexOf(mParmName);
                if((firstIndex>0) && (lastIndex>0)){
                    if ((maxRedef>=1) &&(lastIndex-firstIndex+1 > maxRedef)){
                        isLegal = false;
                        return isLegal;
                    }
                }
            }else{
                int firstIndex = mParms.indexOf(mParmName);
                int lastIndex = mParms.lastIndexOf(mParmName);
                if((firstIndex>0) && (lastIndex>0) && (lastIndex-firstIndex+1 > 1)){
                        isLegal = false;
                        return isLegal;
                }

            }
        }else{
            isLegal = false;
        }
        return isLegal;
    }

    /**
     * check whether the inner-Characteristic is a legal Characteristic
     * @param mCharacteristicType type attribute of the Characteristic
     * @return true if the Characteristic is legal,
     *         false if Characteristic type is null or not a sub-element of the characteristic
     */
    private boolean isLegalCharacteristic(String mCharacteristicType){
        boolean isLegal = true;
        if ((null==mCharacteristicType) ||(mCharacteristicType.equals(""))){
            isLegal=false;
            return isLegal;
        }

        ArrayList<String> allCharacteristics = Element.getSingleInstance().getAllCharacteristics();
        if((null == allCharacteristics) || (!allCharacteristics.contains(mCharacteristicType))){
            isLegal = false;
            return isLegal;
        }

        /*whether the inner characteristic is duplicated*/
        ArrayList<Characteristic> innerCharacteristic =  getInnerCharacteristic(mCharacteristicType);
        if(null!=innerCharacteristic){
            String id = "";
            ArrayList<String> parms = Element.getSingleInstance().getCharacteristicParms(mCharacteristicType);
            if(null!=parms){
                id = parms.get(0);
            }

            for(Characteristic chara:innerCharacteristic){
                if(mCharacteristicType.equals(chara.type)){
                    ArrayList<Parm> parmList = chara.getAllParms();
                    if(null!=parmList){
                        for(Parm mParm:parmList){
                            if(id.equals(mParm.name)){
                                isLegal = false;
                                break;
                            }
                        }
                    }

                }
                if(!isLegal){
                    break;
                }
            }
        }



        /*
         * if a particular NAP-ID value is used multiple times (to define a NAP) within a document then the latter definition is
         * regarded as illegal.
         */

        return isLegal;
    }


    /**
     * check whether the inner-Characteristic is a legal Characteristic
     * @param mCharacteristicType type attribute of the Characteristic
     * @return true if the Characteristic is legal,
     *         false if Characteristic type is null or not a sub-element of the characteristic
     */
    private boolean isLegalCharacteristic(Characteristic mCharacteristic){
        boolean isLegal = true;
        if(null==mCharacteristic){
            isLegal = false;
            return isLegal;
        }
        String mCharacteristicType = mCharacteristic.type;

        if ((null==mCharacteristicType) ||(mCharacteristicType.equals(""))){
            isLegal=false;
            return isLegal;
        }

        ArrayList<String> allCharacteristics = Element.getSingleInstance().getAllCharacteristics();
        if((null == allCharacteristics) || (!allCharacteristics.contains(mCharacteristicType))){
            isLegal = false;
            return isLegal;
        }

        /*whether the inner characteristic is duplicated*/
        //the existing inner characteristics
        ArrayList<Characteristic> innerCharacteristic = getInnerCharacteristic(mCharacteristicType);

        if(null!=innerCharacteristic){
            String id = "";
            String value = "";
            ArrayList<String> parms = Element.getSingleInstance().getCharacteristicParms(mCharacteristicType);
            if(null!=parms){
                id = parms.get(0); //identifier of the characteristic
                ArrayList<Parm> charaParmList =  mCharacteristic.getParm(id);
                if(null!=charaParmList && !charaParmList.isEmpty()){
                    value = charaParmList.get(0).value; //value the characteristic identifier
                }
            }

            for(Characteristic chara:innerCharacteristic){
                if(mCharacteristicType.equals(chara.type)){
                    ArrayList<Parm> parmList = chara.getParm(id);
                    if(null!=parmList && !parmList.isEmpty()){
                        if(value.equals(parmList.get(0).value)){
                            isLegal = false;
                            break;
                        }
                    }
                }
            }
        }
        return isLegal;
    }



   public void resetCharaForType(ArrayList<Characteristic> filtered,String type){

        if(type!=null&& null!=filtered && !filtered.isEmpty()){
            String filterType = filtered.get(0).type;
            if(filterType.equals(type)){
                removeCharaForType(type);
                for(Characteristic chara:filtered){
                    mInnerCharacteristics.add(chara);
                }
            }

        }else{
            removeCharaForType(type);
        }
    }

    private void removeCharaForType(String type){
        if(null!=type){
             Iterator<Characteristic> innerCharas = getInnerCharacteristic();
             if(null!=innerCharas){
                 while(innerCharas.hasNext()){
                     Characteristic chara = innerCharas.next();
                     if(chara.type.equals(type)){
                         innerCharas.remove();
                     }
                 }
             }
        }
    }
}
