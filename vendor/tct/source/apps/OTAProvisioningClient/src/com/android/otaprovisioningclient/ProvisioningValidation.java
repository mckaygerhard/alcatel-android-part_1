/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 01/02/2014|ji.li                 |FR-570124             |The OTA profile still display */
/* ----------|----------------------|----------------------|----------------- */
/* 01/09/2014|gong.zhang            |CR-578398             |OMA-CP message    */
/*           |                      |                      |requirment        */
/* ----------|----------------------|----------------------|----------------- */
/* 01/09/2014|gong.zhang            |FR-579073             |OMACP client still*/
/*           |                      |                      |active after the  */
/*           |                      |                      |installation of an*/
/*           |                      |                      |OMA-CP message    */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.otaprovisioningclient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.android.otaprovisioningclient.security.OmaSigner;
import com.android.otaprovisioningclient.security.UnknownSecurityMechanismException;
import com.android.otaprovisioningclient.utils.HexConvertor;
//[FEATURE]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, CR-578398 [TMO][OMA Provisioning] OMA CP message USER-PIN's
//min length is 4 digits
import android.text.TextWatcher;
import android.text.Editable;
//[FEATURE]-Add-END by TCTNB.gong.zhang,

public class ProvisioningValidation extends Activity {

    private Button mCancel;

    private Button mOk;

    private EditText mUserSuppliedPin;

    private OmaSigner mSigner;

    private byte[] mMac;

    private byte[] mDocument;

    private Dialog mBadUserPinDialog;

    private int mInvalidPinCount = 0;

    private int provisioning_id;
    //[BUGFIX]-Mod-BEGIN by TCTNB.ji.li,01/02/2014,PR-570124
    @Override
    protected void onPause() {
        super.onPause();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
    //[BUGFIX]-Mod-END by TCTNB.ji.li,01/02/2014,PR-570124
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.enter_pin);

        // To inform user that he/she has inputed USER-PIN incorrectly more than
        // three times
        mBadUserPinDialog = new AlertDialog.Builder(ProvisioningValidation.this)
                .setIcon(R.drawable.icon).setTitle(R.string.bad_pin_title)
                .setPositiveButton(android.R.string.ok, null)
                .setMessage(R.string.bad_pin_message).create();

        mUserSuppliedPin = (EditText) findViewById(R.id.user_supplied_pin);
        mUserSuppliedPin.setOnKeyListener(new View.OnKeyListener() {

            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER
                        && KeyEvent.ACTION_DOWN == event.getAction()) {
                    pinEntered();
                    return true;
                } else {
                    return false;
                }
            }
        });

        mCancel = (Button) findViewById(R.id.pin_cancel);
        mCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //[BUGFIX]-Mod-BEGIN by TCTNB.gong.zhang, 2014/01/09, FR-579073, [TMO] click "cancel"shall keep CP message notification
                if(!getResources().getBoolean(R.bool.feature_otaprovisioning_TmoProvision_on)) {
                    ProvisioningNotification.clearNotification(
                        ProvisioningValidation.this, provisioning_id);
                }
                //[BUGFIX]-Mod-END by TCTNB.gong.zhang,
                finish();
            }
        });

        mOk = (Button) findViewById(R.id.pin_ok);
        //[FEATURE]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/28, CR-578398 [TMO][OMA Provisioning] OMA CP message
        //OK button should disable when there is nothing to input
        String str = mUserSuppliedPin.getText().toString().trim();
        if((getResources().getBoolean(R.bool.feature_otaprovisioning_TmoProvision_on)) && (str.equals(""))) {
            mOk.setEnabled(false);
        }
        //[FEATURE]-Add-END by TCTNB.gong.zhang,
        mOk.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                pinEntered();
            }

        });
        //[FEATURE]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, CR-578398 [TMO][OMA Provisioning] OMA CP message USER-PIN's
        //min length is 4 digits
        if(getResources().getBoolean(R.bool.feature_otaprovisioning_TmoProvision_on)) {
           mUserSuppliedPin.addTextChangedListener(new TextWatcher(){
                @Override
                public void beforeTextChanged(CharSequence c,int start,int end,int count){
                }

                @Override
                public void onTextChanged(CharSequence c,int start,int end,int count){
                }

                @Override
                public void afterTextChanged(Editable s){
                    if(s.length()>=4){
                        mOk.setEnabled(true);
                    }else{
                        mOk.setEnabled(false);
                    }
                }
            });
         }
         //[FEATURE]-Add-END by TCTNB.gong.zhang,
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent myIntent = getIntent();
        String secString = myIntent
                .getStringExtra("com.android.otaprovisioningclient.SEC");
        mDocument = myIntent
                .getByteArrayExtra("com.android.otaprovisioningclient.provisioning-data");
        String macString = myIntent
                .getStringExtra("com.android.otaprovisioningclient.MAC");
        provisioning_id = myIntent.getIntExtra(
                "com.android.otaprovisioningclient.provisioning-id", 0);
        if (secString == null || secString.length() == 0) {
            // fixbug 59723, it should not accept the Ota commnad without secure
            // info
            ProvisioningNotification.clearNotification(
                    ProvisioningValidation.this, provisioning_id);
            StoreProvisioning.provision(ProvisioningValidation.this, mDocument);
            // ProvisioningFailed.fail(this, R.string.no_security_specified);
            finish();
            return;
        }
        int sec = Integer.valueOf(secString);

        if (macString != null) {
            // HMAC is encoded as a string of hexadecimal digits where each pair
            // of consecutive digits represent a byte
            mMac = HexConvertor.convert(macString); // generic security
        } else {
            mMac = null; // out-of-band delivery of the MAC security
        }

        TelephonyManager tm = (TelephonyManager) ProvisioningValidation.this
                .getSystemService(Context.TELEPHONY_SERVICE);

        try {
            mSigner = OmaSigner.signerFor(sec, mDocument, tm.getSubscriberId());
        } catch (UnknownSecurityMechanismException e) {
            throw new RuntimeException(e);
        }

        if (!mSigner.isUserPinRequired()) {
            boolean valid = mSigner.isDocumentValid(mUserSuppliedPin.getText()
                    .toString(), mMac);
            if (valid) {
                ProvisioningNotification.clearNotification(
                        ProvisioningValidation.this, provisioning_id);
                StoreProvisioning.provision(ProvisioningValidation.this,
                        mDocument);
                finish();
                return;
            } else {
                ProvisioningNotification.clearNotification(
                        ProvisioningValidation.this, provisioning_id);
                ProvisioningFailed.fail(this, R.string.bad_network_pin_message);
                finish();
                return;
            }
        }

    }

    private void pinEntered() {
        if (mSigner
                .isDocumentValid(mUserSuppliedPin.getText().toString(), mMac)) {
            ProvisioningNotification.clearNotification(
                    ProvisioningValidation.this, provisioning_id);
            StoreProvisioning.provision(ProvisioningValidation.this, mDocument);
            finish();
            return;
        } else {
            mInvalidPinCount++;
            if (mInvalidPinCount >= 3) {
                mInvalidPinCount = 0;
                ProvisioningNotification.clearNotification(
                        ProvisioningValidation.this, provisioning_id);
                ProvisioningFailed.fail(this, R.string.bad_network_pin_message);
                finish();
            } else {
                mBadUserPinDialog.show();
            }
        }
    }

    public static PendingIntent createPendingValidationActivity(
            Context context, String sec, String mac, byte[] document,
            int notificationID) {

        Intent validationIntent = new Intent(context,
                ProvisioningValidation.class);
        //[BUGFIX]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, FR-579073, click"OK", this activity still exist in history task list
        validationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        //[BUGFIX]-Add-BEGIN by TCTNB.gong.zhang,
        if (sec != null) {
            validationIntent.putExtra("com.android.otaprovisioningclient.SEC",
                    sec);
        }
        if (mac != null) {
            validationIntent.putExtra("com.android.otaprovisioningclient.MAC",
                    mac);
        }
        validationIntent
                .putExtra(
                        "com.android.otaprovisioningclient.provisioning-data",
                        document);

        validationIntent.putExtra(
                "com.android.otaprovisioningclient.provisioning-id",
                notificationID);

        return PendingIntent.getActivity(context, notificationID,
                validationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

    }

}
