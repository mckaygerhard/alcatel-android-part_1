/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 01/24/2014|     Dandan.Fang      |       PR595961       | [TMO][MTR-2989-  */
/*           |                      |                      |/2994]No configu- */
/*           |                      |                      |ration menu to d- */
/*           |                      |                      |isable or enable  */
/*           |                      |                      |USERPIN           */
/* ----------|----------------------|----------------------|----------------- */
/* 04/18/2014|     Gong.Zhang       |        FR645209      |[SFR][Email Settin*/
/*           |                      |                      |gs][Provision]When*/
/*           |                      |                      |receive  Email OTA*/
/*           |                      |                      |provision, can't  */
/*           |                      |                      |configure         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.oma;

import java.util.ArrayList;
import java.util.HashMap;

public class Element {

    private static Element _singleElement = null;

    public static Element getSingleInstance() {
        if (null == _singleElement) {
            _singleElement = new Element();
        }
        return _singleElement;
    }

    private Element() {

    }

    public static final String PXLOGICAL = "PXLOGICAL";
    public static final String NAPDEF = "NAPDEF";
    public static final String APPLICATION = "APPLICATION";

    // legal Characteristics defined in Client Provisioning
    // Content-V1_1-20090728-A
    private final static String[] characteristics = {
            "PXLOGICAL",
            "PXPHYSICAL",
            "PXAUTHINFO",
            "PORT",
            "NAPDEF",
            "NAPAUTHINFO",
            "VALIDITY",
            "BOOTSTRAP",
            "CLIENTIDENTITY",
            "VENDORCONFIG",
            "APPLICATION",
            "APPADDR",
            "APPAUTH",
            "RESOURCE",
            "ACCESS"
    };
    private final static ArrayList<String> characteristicList = arrayToList(characteristics);

    // parms of PXLOGICAL characteristic
    private final static String[] mPXLOGICALParms = { "PROXY-ID", "PROXY-PW",
            "PPGAUTH-TYPE", "PROXY-PROVIDER-ID", "NAME", "DOMAIN", "TRUST",
            "MASTER", "STARTPAGE", "BASAUTH-ID", "BASAUTH-PW", "WSP-VERSION",
            "PUSHENABLED", "PULLENABLED" };
    private final static ArrayList<String> mPXLOGICALParmsList = arrayToList(mPXLOGICALParms);
    private final static HashMap<String, Integer> mPXLOGICALParmsSize = new HashMap<String, Integer>();
    /*
     * max size of the parm in its enclosing characteristic -1 : 0 or more 4:
     * max size is 4 default:1
     */
    static {
        mPXLOGICALParmsSize.put("DOMAIN", new Integer(4));
    }

    // parms of PXPHYSICAL characteristic
    private final static String[] mPXPHYSICALParms = { "PHYSICAL-PROXY-ID",
            "DOMAIN", "PXADDR", "PXADDRTYPE", "PXADDR-FQDN", "WSP-VERSION",
            "PUSHENABLED", "PULLENABLED", "TO-NAPID" };
    private final static ArrayList<String> mPXPHYSICALParmsList = arrayToList(mPXPHYSICALParms);
    private final static HashMap<String, Integer> mPXPHYSICALParmsSize = new HashMap<String, Integer>();
    static {
        mPXPHYSICALParmsSize.put("DOMAIN", new Integer(4));
        mPXPHYSICALParmsSize.put("TO-NAPID", new Integer(-1));
    }

    // parms of PXAUTHINFO characteristic
    private final static String[] mPXAUTHINFOParms = { "PXAUTH-TYPE",
            "PXAUTH-ID", "PXAUTH-PW" };
    private final static ArrayList<String> mPXAUTHINFOParmsList = arrayToList(mPXAUTHINFOParms);

    // parms of PORT characteristic
    private final static String[] mPORTParms = { "PORTNBR", "SERVICE" };
    private final static ArrayList<String> mPORTParmsList = arrayToList(mPORTParms);
    private final static HashMap<String, Integer> mPORTParmsSize = new HashMap<String, Integer>();
    static {
        mPORTParmsSize.put("SERVICE", new Integer(-1));
    }

    // parms of NAPDEF characteristic
    private final static String[] mNAPDEFParms = { "NAPID", "BEARER", "NAME",
            //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/24/2014,595961,
            "INTERNET", "NAP-ADDRESS", "NAP-ADDRTYPE", "CALLTYPE", "LOCAL-ADDR",
            //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
            "LOCAL-ADDRTYPE", "DNS-ADDR", "LINKSPEED", "DNLINKSPEED", "LINGER",
            "DELIVERY-ERR-SDU", "DELIVERY-ORDER", "TRAFFIC-CLASS",
            "MAX-SDU-SIZE", "MAX-BITRATE-UPLINK", "MAX-BITRATE-DNLINK",
            "RESIDUAL-BER", "SDU-ERROR-RATIO", "TRAFFIC-HANDL-PRIO",
            "TRANSFER-DELAY", "GUARANTEED-BITRATE-UPLINK",
            "GUARANTEED-BITRATE-DNLINK", "MAX-NUM-RETRY",
            "FIRST-RETRY-TIMEOUT", "REREG-THRESHOLD", "T-BIT" };
    private final static ArrayList<String> mNAPDEFParmsList = arrayToList(mNAPDEFParms);
    private final static HashMap<String, Integer> mNAPDEFParmsSize = new HashMap<String, Integer>();
    static {
        mNAPDEFParmsSize.put("BEARER", new Integer(-1));
        mNAPDEFParmsSize.put("DNS-ADDR", new Integer(-1));
    }

    // parms of NAPAUTHINFO characteristic
    private final static String[] mNAPAUTHINFOParms = { "AUTHTYPE", "AUTHNAME",
            "AUTHSECRET", "AUTH-ENTITY", "SPI" };
    private final static ArrayList<String> mNAPAUTHINFOParmsList = arrayToList(mNAPAUTHINFOParms);
    private final static HashMap<String, Integer> mNAPAUTHINFOParmsSize = new HashMap<String, Integer>();
    static {
        mNAPAUTHINFOParmsSize.put("AUTH-ENTITY", new Integer(-1));
    }

    // parms of VALIDITY characteristic
    private final static String[] mVALIDITYParms = { "COUNTRY", "NETWORK",
            "SID", "SOC", "VALIDUNTIL" };
    private final static ArrayList<String> mVALIDITYParmsList = arrayToList(mVALIDITYParms);

    // parms of BOOTSTRAP characteristic
    private final static String[] mBOOTSTRAPParms = { "NAME", "NETWORK",
            "COUNTRY", "PROXY-ID", "PROVURL", "CONTEXT-ALLOW" };
    private final static ArrayList<String> mBOOTSTRAPParmsList = arrayToList(mBOOTSTRAPParms);
    private final static HashMap<String, Integer> mBOOTSTRAPParmsSize = new HashMap<String, Integer>();
    static {
        mBOOTSTRAPParmsSize.put("NETWORK", new Integer(-1));
        mBOOTSTRAPParmsSize.put("PROXY-ID", new Integer(-1));
    }

    // parms of CLIENTIDENTITY characteristic
    private final static String[] mCLIENTIDENTITYParms = { "CLIENT-ID" };
    private final static ArrayList<String> mCLIENTIDENTITYParmsList = arrayToList(mCLIENTIDENTITYParms);

    // parms of VENDORCONFIG characteristic
    private final static String[] mVENDORCONFIGParms = { "NAME", "MCC", "MNC" };
    private final static ArrayList<String> mVENDORCONFIGParmsList = arrayToList(mVENDORCONFIGParms);

    // parms of APPLICATION characteristic
    //[BUGFIX]-Add-BEGIN by TCTNB.gong.zhang,04/18/2014,645209, port 605885
    private final static String[] mAPPLICATIONParms = { "APPID", "PROVIDER-ID","DISPLAY-NAME",
            "NAME", "AACCEPT", "APROTOCOL", "TO-PROXY", "TO-NAPID", "ADDR",
            "FROM" };
    //[BUGFIX]-Add-END by TCTNB.gong.zhang
    private final static ArrayList<String> mAPPLICATIONParmsList = arrayToList(mAPPLICATIONParms);
    private final static HashMap<String, Integer> mAPPLICATIONParmsSize = new HashMap<String, Integer>();
    static {
        mAPPLICATIONParmsSize.put("TO-PROXY", new Integer(-1));
        mAPPLICATIONParmsSize.put("TO-NAPID", new Integer(-1));
        mAPPLICATIONParmsSize.put("ADDR", new Integer(-1));
        mAPPLICATIONParmsSize.put("FROM", new Integer(-1));

    }

    // parms of APPADDR characteristic
    private final static String[] mAPPADDRParms = { "ADDR", "ADDRTYPE" };
    private final static ArrayList<String> mAPPADDRParmsList = arrayToList(mAPPADDRParms);

    // parms of APPAUTH characteristic
    private final static String[] mAPPAUTHParms = { "AAUTHLEVEL", "AAUTHTYPE",
            "AAUTHNAME", "AAUTHSECRET", "AAUTHDATA" };
    private final static ArrayList<String> mAPPAUTHParmsList = arrayToList(mAPPAUTHParms);

    // parms of RESOURCE characteristic
    private final static String[] mRESOURCEParms = { "URI", "NAME", "AACCEPT",
            "AAUTHTYPE", "AAUTHNAME", "AAUTHSECRET", "AAUTHDATA", "STARTPAGE" };
    private final static ArrayList<String> mRESOURCEParmsList = arrayToList(mRESOURCEParms);
    private final static HashMap<String, Integer> mRESOURCEParmsSize = new HashMap<String, Integer>();
    static {
        mRESOURCEParmsSize.put("AACCEPT", new Integer(-1));
    }

    // parms of ACCESS characteristic
    private final static String[] mACCESSParms = { "RULE", "APPID", "PORTNBR",
            "DOMAIN", "TO-NAPID", "TO-PROXY" };
    private final static ArrayList<String> mACCESSParmsList = arrayToList(mACCESSParms);
    private final static HashMap<String, Integer> mACCESSParmsSize = new HashMap<String, Integer>();
    static {
        mACCESSParmsSize.put("RULE", new Integer(-1));
        mACCESSParmsSize.put("APPID", new Integer(-1));
        mACCESSParmsSize.put("PORTNBR", new Integer(-1));
        mACCESSParmsSize.put("DOMAIN", new Integer(-1));
        mACCESSParmsSize.put("TO-NAPID", new Integer(-1));
        mACCESSParmsSize.put("TO-PROXY", new Integer(-1));
    }

    // parm set of every characteristic
    private final static HashMap<String, ArrayList<String>> mCharacteristicParms = new HashMap<String, ArrayList<String>>();
    static {
        mCharacteristicParms.put("PXLOGICAL", mPXLOGICALParmsList);
        mCharacteristicParms.put("PXPHYSICAL", mPXPHYSICALParmsList);
        mCharacteristicParms.put("PXAUTHINFO", mPXAUTHINFOParmsList);
        mCharacteristicParms.put("PORT", mPORTParmsList);
        mCharacteristicParms.put("NAPDEF", mNAPDEFParmsList);
        mCharacteristicParms.put("NAPAUTHINFO", mNAPAUTHINFOParmsList);
        mCharacteristicParms.put("VALIDITY", mVALIDITYParmsList);
        mCharacteristicParms.put("BOOTSTRAP", mBOOTSTRAPParmsList);
        mCharacteristicParms.put("CLIENTIDENTITY", mCLIENTIDENTITYParmsList);
        mCharacteristicParms.put("VENDORCONFIG", mVENDORCONFIGParmsList);
        mCharacteristicParms.put("APPLICATION", mAPPLICATIONParmsList);
        mCharacteristicParms.put("APPADDR", mAPPADDRParmsList);
        mCharacteristicParms.put("APPAUTH", mAPPAUTHParmsList);
        mCharacteristicParms.put("RESOURCE", mRESOURCEParmsList);
        mCharacteristicParms.put("ACCESS", mACCESSParmsList);
    }

    // parm size of every Characteristic
    private final static HashMap<String, HashMap<String, Integer>> mCharacteristicParmsSize = new HashMap<String, HashMap<String, Integer>>();
    static {
        mCharacteristicParmsSize.put("PXLOGICAL", mPXLOGICALParmsSize);
        mCharacteristicParmsSize.put("PXPHYSICAL", mPXPHYSICALParmsSize);
        mCharacteristicParmsSize.put("PORT", mPORTParmsSize);
        mCharacteristicParmsSize.put("NAPDEF", mNAPDEFParmsSize);
        mCharacteristicParmsSize.put("NAPAUTHINFO", mNAPAUTHINFOParmsSize);
        mCharacteristicParmsSize.put("BOOTSTRAP", mBOOTSTRAPParmsSize);
        mCharacteristicParmsSize.put("APPLICATION", mAPPLICATIONParmsSize);
        mCharacteristicParmsSize.put("RESOURCE", mRESOURCEParmsSize);
        mCharacteristicParmsSize.put("ACCESS", mACCESSParmsSize);
    }

    /**
     * get all the Characteristic type
     *
     * @return an list of the Characteristic type or null
     */
    public ArrayList<String> getAllCharacteristics() {
        return characteristicList;
    }

    /**
     * get parms size of a characteristic
     *
     * @param mCharacteristicType
     *            type attribute of a characteristic
     * @return a HashMap of the parms size
     */
    public HashMap<String, Integer> getCharacteristicParmsSize(
            String characteristicType) {
        HashMap<String, Integer> parmsSize = null;
        try {
            if (mCharacteristicParmsSize.keySet().contains(characteristicType)) {
                parmsSize = mCharacteristicParmsSize.get(characteristicType);
            }
        } catch (NullPointerException nullPointExc) {
            return null;
        }
        return parmsSize;
    }

    /**
     * get parms of a characteristic
     *
     * @param mCharacteristicType
     *            type attribute of a characteristic
     * @return an ArrayList of the parms of a characteristic
     */
    public ArrayList<String> getCharacteristicParms(String characteristicType) {
        ArrayList<String> parms = null;
        try {
            if (mCharacteristicParms.keySet().contains(characteristicType)) {
                parms = mCharacteristicParms.get(characteristicType);
            }
        } catch (NullPointerException nullPointExc) {
            return null;
        }

        return parms;
    }

    private static ArrayList<String> arrayToList(String[] items) {
        ArrayList<String> list = new ArrayList<String>();
        if (0 == items.length) {
            return null;
        }
        for (int i = 0; i < items.length; i++) {
            list.add(items[i]);
        }
        return list;
    }
}
