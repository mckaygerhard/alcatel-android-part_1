/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.otaprovisioningclient.oma;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.util.TctLog;

public class WapProvisioningDoc {

    private String TAG = "OTACP";
    public HashMap<String, Application> applications = new HashMap<String, Application>();

    public String name;

    public String mnc;

    public String mcc;

    public int docType = 0; // docType=1: APN;
                            // docType=2: MMS
                            // docType=3: APN and MMS

    public boolean isDocumentValid() {
        return !applications.isEmpty();
    }

    public boolean containsValidBrowserApplication() {
        Application application = applications.get(Application.BROWSER);
        if (application == null) {
            return false;
        }
        AccessPoint accessPoint = application.accessPoint;
        Proxy proxy = application.proxy;

        return hasNameAndHni() && accessPoint != null && accessPoint.isValid()
                && (proxy == null || proxy.isValid());
    }

    public boolean containsValidMmsApplication() {
        Application application = applications.get(Application.MMS);
        if (application == null) {
            return false;
        }
        AccessPoint accessPoint = application.accessPoint;
        Proxy proxy = application.proxy;

        return hasNameAndHni() && accessPoint != null && accessPoint.isValid()
                && proxy != null && proxy.isValid();
    }

    public boolean containsValidSYMLApplication() {
        Application application = applications.get(Application.SYNCML);
        if (application == null) {
            return false;
        }
        AccessPoint accessPoint = application.accessPoint;
        Proxy proxy = application.proxy;

        AppAuth appAuth = application.appAuth;

        return hasNameAndHni() && appAuth != null && appAuth.isValid();
    }

    private boolean hasNameAndHni() {
        return name != null && !name.equals("") && !mcc.equals("")
                && mnc != null && !mnc.equals("");
    }

    // update by 93042 starts
    /*
     * root characteristics defined in Client Provisioning Content
     */
    private static final String[] rootCharacteristics = { "PXLOGICAL",
            "NAPDEF", "BOOTSTRAP", "CLIENTIDENTITY", "VENDORCONFIG",
            "APPLICATION", "ACCESS" };
    private static final ArrayList<String> mRootCharacteristic = new ArrayList<String>();
    static {
        for (int i = 0; i < rootCharacteristics.length; i++) {
            mRootCharacteristic.add(rootCharacteristics[i]);
        }
    }

    /*
     * root characteristics of a provisioning document
     */
    private ArrayList<Characteristic> mDocCharacteristic = new ArrayList<Characteristic>();

    /**
     * add a root characteristic to the root characteristic list of the
     * provisioning document if the Characteristic is a legal root
     * Characteristic
     *
     * @param mCharacteristic
     *            a root Characteristic
     * @return void
     */
    public void addCharacteristic(Characteristic mCharacteristic) {
        if (isLegalRootCharacteristic(mCharacteristic)
                && !isDuplicatedCharacteristic(mCharacteristic)) {
            mDocCharacteristic.add(mCharacteristic);
        }
    }

    /**
     * get all the root Characteristics of the provisioning document
     *
     * @return an Iterator of all the root Characteristics
     */
    public Iterator<Characteristic> getAllCharacteristic() {
        if (mDocCharacteristic.size() > 0) {
            return mDocCharacteristic.iterator();
        } else {
            return null;
        }
    }

    public ArrayList<Characteristic> getCharacteristic(String CharacteristicType) {
        ArrayList<Characteristic> charaList = new ArrayList<Characteristic>();
        Iterator<Characteristic> allChara = getAllCharacteristic();
        if (null != allChara) {
            while (allChara.hasNext()) {
                Characteristic curChara = allChara.next();
                if (CharacteristicType.equals(curChara.type)) {
                    charaList.add(curChara);
                }
            }
        }
        return charaList;
    }

    public void removeApplication(Characteristic appChara) {
        if (null != appChara && appChara.type.equals("APPLICATION")) {
            ArrayList<Characteristic> appList = getCharacteristic("APPLICATION");
            if (null != mDocCharacteristic && mDocCharacteristic.size() > 0) {
                for (int i = 0; i < mDocCharacteristic.size(); i++) {
                    if (mDocCharacteristic.get(i).type.equals("APPLICATION")) {
                        String appID = mDocCharacteristic.get(i)
                                .getParm("APPID").get(0).value;
                        if (appChara.getParm("APPID").get(0).value
                                .equals(appID)) {
                            mDocCharacteristic.remove(i);
                            TctLog.i(TAG, "remove an Application,ID:"
                                    + appChara.getParm("APPID").get(0).value);
                            break;
                        }
                    }
                }
            }

        }
    }

    // remove an illegal NAPDEF characteristic form prov doc
    public void removeNAPDef(Characteristic napChara) {

        if (null != napChara && napChara.type.equals("NAPDEF")) {

            if (null != mDocCharacteristic && !mDocCharacteristic.isEmpty()) {
                for (int i = 0; i < mDocCharacteristic.size(); i++) {
                    if (mDocCharacteristic.get(i).type.equals("NAPDEF")) {
                        String napID = mDocCharacteristic.get(i)
                                .getParm("NAPID").get(0).value;
                        if (napChara.getParm("NAPID").get(0).value
                                .equals(napID)) {
                            mDocCharacteristic.remove(i);
                            TctLog.i(TAG, "remove an napChara,ID:"
                                    + napChara.getParm("NAPID").get(0).value);
                            break;
                        }
                    }
                }
            }

        }

    }

    /*
     * Remove an illegal PXLOGICAL Characteristic
     */
    public void removePXLogical(Characteristic pxLogicalChara) {

        if (null != pxLogicalChara && pxLogicalChara.type.equals("PXLOGICAL")) {

            if (null != mDocCharacteristic && !mDocCharacteristic.isEmpty()) {
                for (int i = 0; i < mDocCharacteristic.size(); i++) {
                    if (mDocCharacteristic.get(i).type.equals("PXLOGICAL")) {
                        String proxyID = mDocCharacteristic.get(i)
                                .getParm("PROXY-ID").get(0).value;
                        if (pxLogicalChara.getParm("PROXY-ID").get(0).value
                                .equals(proxyID)) {
                            mDocCharacteristic.remove(i);
                            TctLog.i(TAG,
                                    "remove an PXLOGICAL Chara,ID:"
                                            + pxLogicalChara
                                                    .getParm("PROXY-ID").get(0).value);
                            break;
                        }
                    }
                }
            }

        }

    }

    /**
     * check whether the current characteristic is a root characteristic
     *
     * @param mCharacteristicType
     *            type attribute of the characteristic
     * @return true if the current characteristic is a root characteristic,
     *         false if type is null or not a root characteristic
     */
    private boolean isLegalRootCharacteristic(Characteristic mChara) {
        boolean isLegal = true;
        String mCharacteristicType = mChara.type;
        if ((null == mCharacteristicType)
                || (0 == mCharacteristicType.length())) {
            isLegal = false;
            return isLegal;
        }
        if ((null == mRootCharacteristic)
                || (!mRootCharacteristic.contains(mCharacteristicType))) {
            isLegal = false;
            return isLegal;
        }

        String id = null;
        if (mCharacteristicType.equals("NAPDEF")) {
            id = "NAPID";
        } else if (mCharacteristicType.equals("PXLOGICAL")) {
            id = "PROXY-ID";
        } else if (mCharacteristicType.equals("APPID")) {
            id = "APPID";
        }
        if (null != id) {
            ArrayList<Parm> charaId = mChara.getParm(id);
            if (null == charaId || charaId.isEmpty()) {
                isLegal = false;
            }
        }
        return isLegal;
    }

    private boolean isDuplicatedCharacteristic(Characteristic mCharacteristic) {
        boolean duplicate = false;
        String type = mCharacteristic.type;
        String id = null;
        if (null != type && !"".equals(type)) {
            if (type.equals("NAPDEF")) {
                id = mCharacteristic.getParm("NAPID").get(0).value;
            } else if (type.equals("PXLOGICAL")) {
                id = mCharacteristic.getParm("PROXY-ID").get(0).value;
            } else if (type.equals("APPLICATION")) {
                id = mCharacteristic.getParm("APPID").get(0).value;
            } else if (type.equals("CLIENTIDENTITY")) {
                id = mCharacteristic.getParm("CLIENT-ID").get(0).value;
            }

            duplicate = isExist(type, id);
        }
        return duplicate;

    }

    private boolean isExist(String type, String id) {
        boolean exist = false;
        if (null != mDocCharacteristic && null != type && !"".equals(type)
                && null != id && !"".equals(id)) {
            for (Characteristic chara : mDocCharacteristic) {
                if (type.equals(chara.type)) {

                    if ("NAPDEF".equals(type)) {
                        if (id.equals(chara.getParm("NAPID").get(0).value)) {
                            exist = true;
                            break;
                        }
                    } else if ("PXLOGICAL".equals(type)) {
                        if (id.equals(chara.getParm("PROXY-ID").get(0).value)) {
                            exist = true;
                            break;
                        }
                    } else if ("APPLICATION".equals(type)) {
                        if (id.equals(chara.getParm("APPID").get(0).value)) {
                            exist = true;
                            break;
                        }
                    } else if ("CLIENTIDENTITY".equals(type)) {
                        if (id.equals(chara.getParm("CLIENT-ID").get(0).value)) {
                            exist = true;
                            break;
                        }
                    }
                }
            }
        }
        return exist;
    }
}
