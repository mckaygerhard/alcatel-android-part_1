/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.oma;

public class OMASettingInfo {

    //Specifies the server identifier for data synchronization server
    //used in the server alerted sync.
    public static final String PROVIDER_ID= "provider_id";
    public static final String TO_NAPID = "to_napid";//APN to use for SyncML DS
    //usage : nonce used at the next session if the authentication is MD5
    public static final String NEXT_NONCE = "next_nonce";
    public static final String OMA_DS_SETTING_ACTION = "syncml.intent.action.OMA_DS_SET";

    public static final String SERVER_URL = "server_url"; //server address : type string non-null
    public static final String USER_ID = "user_id"; //user name : type string
    public static final String PASSWD = "passwd"; //password : type string

    public static final String PROXY_SERVER_URL = "proxy_server_url"; // proxy server address : type string
    public static final String PROXY_SERVER_PORT = "proxy_server_port"; // proxy server port : type int
    public static final String PROXY_USER_ID = "proxy_user_id"; //proxy server user name : type string
    public static final String PROXY_PASSWD = "proxy_passwd"; //proxy server user name : type string

    public static final String CON_DB_URL = "con_db_url"; //contact remote databases url : type string non-null
    public static final String CON_DB_NAME = "con_db_name"; //contact databases name : type string
    public static final String CON_USER_ID = "con_user_id"; //contact remote user name : type string
    public static final String CON_PASSWD = "con_passwd"; //contact remote password : type string

    public static final String CAL_DB_URL = "cal_db_url"; //calendar remote databases url : type string non-null
    public static final String CAL_DB_NAME = "cal_db_name"; //calendar databases name : type string
    public static final String CAL_USER_ID = "cal_user_id"; //calendar remote user name : type string
    public static final String CAL_PASSWD = "cal_passwd"; //calendar remote password : type string
}
