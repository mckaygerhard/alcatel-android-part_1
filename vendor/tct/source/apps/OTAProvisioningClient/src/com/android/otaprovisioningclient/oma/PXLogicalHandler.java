/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.oma;

import java.util.ArrayList;
import java.util.Iterator;

class PXLogicalHandler {

    public static boolean checkValidity(WapProvisioningDoc mProvDoc) {

        if (null != mProvDoc) {
            Iterator<Characteristic> remainder = mProvDoc
                    .getAllCharacteristic();
            if (null != remainder && remainder.hasNext()) {
                ArrayList<Characteristic> pxlogicalList = mProvDoc
                        .getCharacteristic("PXLOGICAL");

                if (null != pxlogicalList && !pxlogicalList.isEmpty()) {

                    for (Characteristic pxlogical : pxlogicalList) {

                        ArrayList<Characteristic> authInfoList = pxlogical
                                .getInnerCharacteristic("PXAUTHINFO");

                        if (null != authInfoList && !authInfoList.isEmpty()) {
                            Iterator<Characteristic> authInfoIterator = authInfoList
                                    .iterator();
                            while (authInfoIterator.hasNext()) {
                                Characteristic inner = authInfoIterator.next();
                                if (!checkAuthInfo(inner)) {
                                    authInfoIterator.remove();
                                }
                            }
                            pxlogical.resetCharaForType(authInfoList,
                                    "PXAUTHINFO");
                        }

                        ArrayList<Characteristic> portList = pxlogical
                                .getInnerCharacteristic("PORT");
                        if (null != portList && !portList.isEmpty()) {
                            Iterator<Characteristic> portIterator = portList
                                    .iterator();
                            while (portIterator.hasNext()) {
                                Characteristic inner = portIterator.next();
                                if (!checkPort(inner)) {
                                    portIterator.remove();
                                }
                            }
                            pxlogical.resetCharaForType(portList, "PORT");
                        }

                        ArrayList<Characteristic> pxphyList = pxlogical
                                .getInnerCharacteristic("PXPHYSICAL");
                        if (null != pxphyList && !pxphyList.isEmpty()) {
                            Iterator<Characteristic> pxphyIterator = pxphyList
                                    .iterator();
                            while (pxphyIterator.hasNext()) {
                                Characteristic inner = pxphyIterator.next();
                                if (!checkPYLogical(inner, mProvDoc)) {
                                    pxphyIterator.remove();
                                }
                            }
                            if (pxphyList.isEmpty()) {
                                /*
                                 * the PXLOGICAL does not have one PXPHYSICAL
                                 * characteristic
                                 */
                                mProvDoc.removePXLogical(pxlogical);
                            } else {
                                pxlogical.resetCharaForType(pxphyList,
                                        "PXPHYSICAL");
                            }

                        } else {
                            /*
                             * a PXLOGICAL characteristic must have at least one
                             * PXPHYSICAL characteristic
                             */
                            mProvDoc.removePXLogical(pxlogical);

                        }

                    }// end for
                }// end if

                ApplicationHandler.checkValidity(mProvDoc);

            }
        }

        return false;
    }

    private static boolean checkAuthInfo(Characteristic authChara) {
        boolean isValid = true;
        if (null != authChara && authChara.type.equals("PXAUTHINFO")) {
            ArrayList<Parm> authType = authChara.getParm("PXAUTH-TYPE");
            if (null == authType || authType.isEmpty()) {
                isValid = false;
            }
        }
        return isValid;
    }

    private static boolean checkPort(Characteristic portChara) {
        boolean isValid = true;
        if (null != portChara && portChara.type.equals("PORT")) {
            ArrayList<Parm> portNB = portChara.getParm("PORTNBR");
            if (null == portNB || portNB.isEmpty()) {
                isValid = false;
            }
        }
        return isValid;
    }

    private static boolean checkPYLogical(Characteristic pylogical,
            WapProvisioningDoc mProvDoc) {
        boolean isValid = true;

        // check PXADDR parm
        ArrayList<Parm> pxAddr = pylogical.getParm("PXADDR");
        if (null == pxAddr || pxAddr.isEmpty()) {
            isValid = false;
            return isValid;
        }

        // check TO-NAPID parm
        ArrayList<Parm> napRef = pylogical.getParm("TO-NAPID");
        if (null == napRef || napRef.isEmpty()) {
            isValid = false;
            return isValid;
        } else { // check whether the nap are valid
            ArrayList<Characteristic> napList = mProvDoc
                    .getCharacteristic("NAPDEF");
            ArrayList<String> napIdList = new ArrayList<String>();
            if (null != napList && !napList.isEmpty()) {
                for (Characteristic chara : napList) {
                    napIdList.add(chara.getParm("NAPID").get(0).value);
                }
            }

            for (Parm tonap : napRef) {
                String id = tonap.value;
                if (!id.equals("INTERNET") && !napIdList.contains(id)) {
                    pylogical.removeParm(tonap);

                }
            }

            napRef = pylogical.getParm("TO-NAPID");
            if (null == napRef || napRef.isEmpty()) {
                isValid = false;
                return isValid;
            }

        }

        // check PORT characteristic

        ArrayList<Characteristic> innerPortList = pylogical
                .getInnerCharacteristic("PORT");
        if (null != innerPortList) {
            Iterator<Characteristic> innerPorts = innerPortList.iterator();
            while (innerPorts.hasNext()) {
                Characteristic port = innerPorts.next();
                if (!checkPort(port)) {
                    innerPorts.remove();
                }
            }
            // reset the InnerCharacteristic
            pylogical.resetCharaForType(innerPortList, "PORT");

        }

        return isValid;
    }

}
