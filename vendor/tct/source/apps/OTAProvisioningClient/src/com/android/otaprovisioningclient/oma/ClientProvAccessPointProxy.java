/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.oma;

import java.util.ArrayList;
import java.util.Iterator;

import android.util.TctLog;

import com.android.otaprovisioningclient.oma.ClientProvAccessPointDef.ClientProvAccessPointAuth;
import com.android.otaprovisioningclient.oma.ClientProvAccessPointDef.ClientProvAccessPointValidity;

public class ClientProvAccessPointProxy {

    public String proxy;
    public String port;
    public String mmsproxy;
    public String mmsport;
    public String mmsc;

    private String TAG = "OTACP";

    private ArrayList<String> mPortList = new ArrayList<String>(); // inner
                                                                   // parm:PORT
    public ArrayList<ProvPxPhysical> physicalProxy = new ArrayList<ProvPxPhysical>(); // inner
                                                                                      // characteristic:PXPHYSICAL

    public ClientProvAccessPointProxy() {
    };

    public ClientProvAccessPointProxy(Characteristic pxLogicalChara,
            WapProvisioningDoc provDoc) {

        if (null != pxLogicalChara && "PXLOGICAL".equals(pxLogicalChara.type)) {

            Iterator<Characteristic> pxLogicalInnerChara = pxLogicalChara
                    .getInnerCharacteristic();

            if (null != pxLogicalInnerChara) {

                while (pxLogicalInnerChara.hasNext()) {
                    Characteristic innerChara = pxLogicalInnerChara.next();
                    if ("PORT".equals(innerChara.type)) {
                        //Changed by fei.liu for defect-2116443,05/13/2016,begin
                        ArrayList<Parm> tempList = innerChara.getParm("PORTNBR");
                        if (tempList != null && tempList.size() > 0)
                            mPortList.add(tempList.get(0).value);
                        //mPortList.add(port);
                        //Changed by fei.liu for defect-2116443,end
                    } else if ("PXPHYSICAL".equals(innerChara.type)) {
                        ProvPxPhysical pxPhysical = new ProvPxPhysical(
                                innerChara, provDoc);
                        if (null != pxPhysical.provApDefList
                                && !pxPhysical.provApDefList.isEmpty()) {
                            physicalProxy.add(pxPhysical);
                        }// physical proxy, defined without a valid NAP,is
                         // ignored

                    }

                }
                appendPortToPxPhysical();
            }

        }
    }

    private void appendPortToPxPhysical() {
        for (ProvPxPhysical pxPhysical : physicalProxy) {
            if (null == pxPhysical.portList) {
                pxPhysical.portList = mPortList;
            } else {
                pxPhysical.portList.addAll(mPortList);
            }
        }
    }

    public class ProvPxPhysical {

        public ArrayList<ClientProvAccessPointDef> provApDefList;
        public ArrayList<String> portList;
        public String address;

        private ArrayList<Parm> NAPParms;
        private WapProvisioningDoc mProvDoc;
        private Iterator<Characteristic> innerChara;

        ProvPxPhysical(Characteristic pxPhysical, WapProvisioningDoc provDoc) {
            ArrayList<Parm> tempList;
            if ("PXPHYSICAL".equals(pxPhysical.type)) {

                mProvDoc = provDoc;
                tempList = pxPhysical.getParm("PXADDR");
                if (tempList != null && tempList.size() > 0)
                    address = pxPhysical.getParm("PXADDR").get(0).value;
                else
                    address = null;
                NAPParms = pxPhysical.getParm("TO-NAPID");

                provApDefList = new ArrayList<ClientProvAccessPointDef>();
                innerChara = pxPhysical.getInnerCharacteristic();

                portList = new ArrayList<String>();

                getAccessPoints();
                getPorts();
            }

        }

        /**
         * get all the napdefs based on TO-NAPID
         *
         * @return a list of ClientProvAccessPointDef
         */
        private ArrayList<ClientProvAccessPointDef> getAccessPoints() {

            TctLog.i(TAG, "getAccessPoints");
            ArrayList<Parm> tempList;

            ArrayList<Characteristic> napList = mProvDoc
                    .getCharacteristic("NAPDEF");
            if ((null != napList && !napList.isEmpty())
                    && (null != NAPParms && !NAPParms.isEmpty())) {
                for (Parm napParm : NAPParms) {
                    String napID = napParm.value;
                    for (int i = 0; i < napList.size(); i++) {
                        Characteristic npChara = napList.get(i);
                        tempList = npChara.getParm("NAPID");
                        if ((tempList != null && tempList.size() > 0)
                                && napID.equals(tempList.get(0).value)) {
                            ArrayList<ClientProvAccessPointDef> _apList = new ClientProvAccessPointDef(
                                    npChara).getClientProvAccessPointDef();
                            for (ClientProvAccessPointDef ap : _apList) {
                                provApDefList.add(ap);
                            }
                            break;
                        }
                    }
                }
                return provApDefList;
            }
            return null;
        }

        /**
         * get all the ports enclosed in the PXPHYSICAL
         *
         * @return a list of port
         */
        private ArrayList<String> getPorts() {
            ArrayList<Parm> tempList;
            if (null != innerChara) {
                while (innerChara.hasNext()) {
                    Characteristic portChara = innerChara.next();
                    if ("PORT".equals(portChara.type)) {
                        tempList = portChara.getParm("PORTNBR");
                        if (tempList != null && tempList.size() > 0)
                            portList.add(tempList.get(0).value);
                    }
                }
                return portList;
            }
            return null;
        }

    }

}
