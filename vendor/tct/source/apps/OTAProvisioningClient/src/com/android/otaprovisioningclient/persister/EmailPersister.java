/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 01/06/2014|gong.zhang            |FR-579117             |DUT is not correct*/
/*           |                      |                      |handling OMA CP   */
/*           |                      |                      |provision message */
/*           |                      |                      |for email         */
/* ----------|----------------------|----------------------|----------------- */
/* 05/08/2014|     Song Fuqiang     |      FR-645242       |[HOMO][SFR][OMAC- */
/*           |                      |                      |P] Email configu- */
/*           |                      |                      |ration failed     */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.persister;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.util.TctLog;

import com.android.otaprovisioningclient.oma.Application;
import com.android.otaprovisioningclient.oma.EmailAccount;
import com.android.otaprovisioningclient.oma.WapProvisioningDoc;

public class EmailPersister {
    private static String TAG = "EmailPersister";
    private static String email_account;
    private static String email_password;
    private static String email_account_for_authentication;
    private static String email_password_for_authentication;
    private static String email_address;
    private static String inboundAddress;
    private static String inboundPort;
    private static String outboundAddress;
    private static String outboundPort;

    private static String security_type_forinbound;
    private static String security_type_foroutbound;
    private static String email_account_type_forinbound;
    private static String email_account_type_foroutbound;

    private static ArrayList<EmailAccount> inBound = new ArrayList<EmailAccount>();
    private static ArrayList<EmailAccount> outBound = new ArrayList<EmailAccount>();

    private static boolean checkEmail(ArrayList<EmailAccount> emailAccounts) {
        inBound.clear();
        outBound.clear();
        boolean ok = true;
        if (null != emailAccounts && emailAccounts.size() > 0) {
            for (EmailAccount acc : emailAccounts) {

                if (acc.email_address != null) {
                    int index_suffix = acc.email_address.indexOf('@');
                    if (index_suffix == acc.email_address.length() - 1)
                        continue;
                    else if (index_suffix != -1) {
                        String email_address_suffix = acc.email_address
                                .substring(index_suffix + 1);
                        //[BUGFIX]-Del-BEGIN by TCTNB.gong.zhang, 2014/01/06, FR-579117, DUT is not correctly handling OMA CP provisioning message for email
                        //Ignore doman checking, CP server should make sure the CP's accuracy
                        //For case like username is xxx@hotmail.com, but the server is xxx.live.com
                        //if (!acc.ioboundAddress.endsWith(email_address_suffix))
                        //    continue;
                        //[BUGFIX]-Del-END by TCTNB.gong.zhang,
                    }

                }

                if (acc.email_account_type_foriobound
                        .equals(Application.INBOUNDEMAIL_IMAP4)
                        || acc.email_account_type_foriobound
                                .equals(Application.INBOUNDEMAIL_POP3)) {
                    if (acc.email_account_for_authentication != null
                            && acc.email_password_for_authentication != null) {
                        if (acc.ioboundPort == null) {
                            if (acc.email_account_type_foriobound
                                    .equals(Application.INBOUNDEMAIL_IMAP4)) {
                                acc.ioboundPort = Application.INBOUNDEMAIL_IMAP4;
                            } else {
                                acc.ioboundPort = Application.INBOUNDEMAIL_POP3;
                            }
                        }
                        inBound.add(acc);
                    }
                } else if (acc.email_account_type_foriobound
                        .equals(Application.OUTBOUNDEMAIL)) {
                    if (acc.email_address != null && acc.ioboundAddress != null
                            && acc.email_address != null) {
                        if (acc.ioboundPort == null) {
                            acc.ioboundPort = Application.OUTBOUNDEMAIL;
                        }
                        outBound.add(acc);
                    }

                }
            }
            if (inBound.isEmpty() || outBound.isEmpty()) {
                ok = false;
            }
        } else {
            ok = false;
        }
        return ok;
    }

    public static boolean saveEmail(Context context,
            WapProvisioningDoc provisioningDocument) {
        boolean ok = false;
        AccessPointFactory accessPointFactory = new AccessPointFactory(
                provisioningDocument);
        ArrayList<EmailAccount> emailAccounts = accessPointFactory
                .createEmailAccount();

        if (null != emailAccounts && emailAccounts.size() > 0) {

            ok = checkEmail(emailAccounts);

            if (ok) {
                for (EmailAccount inAcc : inBound) {
                    email_account_type_forinbound = inAcc.email_account_type_foriobound;
                    if (email_account_type_forinbound.equals("110")) {
                        email_account_type_forinbound = "pop3";
                    } else if (email_account_type_forinbound.equals("143")) {
                        email_account_type_forinbound = "imap";
                    }
                    email_account = inAcc.email_account_for_authentication;
                    email_password = inAcc.email_password_for_authentication;
                    inboundAddress = inAcc.ioboundAddress;
                    inboundPort = inAcc.ioboundPort;
                    security_type_forinbound = inAcc.security_type_foriobound;

                    //[BUGFIX]-Add-BEGIN by TCTNB.fuqiang.song,05/08/2014,645242,
                    //[HOMO][SFR][OMACP] Email configuration failed
                    if ("995".equals(security_type_forinbound)||"993".equals(security_type_forinbound)) {
                        security_type_forinbound = "ssl";
                       }
                    //[BUGFIX]-Add-END by TCTNB.fuqiang.song

                    for (EmailAccount outAcc : outBound) {
                        email_account_type_foroutbound = outAcc.email_account_type_foriobound;
                        if (email_account_type_foroutbound.equals("25")) {
                            email_account_type_foroutbound = "smtp";
                        }
                        email_account_for_authentication = outAcc.email_account_for_authentication;
                        email_password_for_authentication = outAcc.email_password_for_authentication;
                        email_address = outAcc.email_address;
                        outboundAddress = outAcc.ioboundAddress;
                        outboundPort = outAcc.ioboundPort;
                        security_type_foroutbound = outAcc.security_type_foriobound;

                        //[BUGFIX]-Add-BEGIN by TCTNB.fuqiang.song,05/08/2014,645242,
                        //[HOMO][SFR][OMACP] Email configuration failed
                        if ("465".equals(security_type_foroutbound)) {
                            security_type_foroutbound = "ssl";
                         }
                        //[BUGFIX]-Add-END by TCTNB.fuqiang.song

                        TctLog.i(TAG, "" + "EMAIL_COUNT:" + email_account
                                + "\n**EMAIL_PASSWORD:" + email_password
                                + "\n**INBOUNDADDRESS:" + inboundAddress
                                + "\n**INBOUNDPORT:" + inboundPort
                                + "\n**EMAIL_ACCOUNT_FOR_AUTHENTICATION:"
                                + email_account_for_authentication
                                + "\n**EMAIL_PASSWORD_FOR_AUTHENTICATION:"
                                + email_password_for_authentication
                                + "\n**EMAIL_ADDRESS:" + email_address
                                + "\n**OUTBOUNDADDRESS:" + outboundAddress
                                + "\n**OUTBOUNDPORT:" + outboundPort
                                + "\n**SECURITY_TYPE_FORIN:"
                                + security_type_forinbound
                                + "\n**SECURITY_TYPE_FOROUT:"
                                + security_type_foroutbound
                                + "\n**EMAIL_ACCOUNT_TYPE:"
                                + email_account_type_forinbound);
                        Intent intent = new Intent(
                                "email.intent.android.emailsetting");
                        intent.putExtra("EMAIL_COUNT_NAME", email_account);
                        intent.putExtra("EMAIL_PASSWORD", email_password);
                        intent.putExtra("INBOUNDADDRESS", inboundAddress);
                        intent.putExtra("INBOUNDPORT", inboundPort);
                        intent.putExtra("EMAIL_ACCOUNT_FOR_AUTHENTICATION",
                                email_account_for_authentication);
                        intent.putExtra("EMAIL_PASSWORD_FOR_AUTHENTICATION",
                                email_password_for_authentication);
                        intent.putExtra("EMAIL_ADDRESS", email_address);
                        intent.putExtra("OUTBOUNDADDRESS", outboundAddress);
                        intent.putExtra("OUTBOUNDPORT", outboundPort);
                        intent.putExtra("SECURITY_TYPE_FORIN",
                                security_type_forinbound);
                        intent.putExtra("SECURITY_TYPE_FOROUT",
                                security_type_foroutbound);
                        intent.putExtra("EMAIL_ACCOUNT_TYPE_INBOUND",
                                email_account_type_forinbound);
                        intent.putExtra("EMAIL_ACCOUNT_TYPE_OUTBOUND",
                                email_account_type_foroutbound);
                        context.sendBroadcast(intent);

                    }
                }
            }

        }
        return ok;
    }

}
