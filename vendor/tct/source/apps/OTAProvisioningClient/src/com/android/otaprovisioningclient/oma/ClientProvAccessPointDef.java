/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.oma;

import java.util.ArrayList;
import java.util.Iterator;

public class ClientProvAccessPointDef {

    public String name; // NAPDEF.NAME
    public String apn; // NAPDEF.NAP-ADDRESS
    public String type; // corresponding to APPLICATION.APPID

    public String napID; // NAPDEF.NAPID

    public ClientProvAccessPointAuth accessPointAuth;
    public ClientProvAccessPointValidity accessPointValidity;

    private ArrayList<ClientProvAccessPointAuth> accessPointAuthList;
    private ArrayList<ClientProvAccessPointValidity> accessPointValidityList;

    private ArrayList<ClientProvAccessPointDef> accessPointDefList;

    public ClientProvAccessPointDef() {
    }

    public ClientProvAccessPointDef(Characteristic napChara) {
        ArrayList<Parm> tempList;

        if ("NAPDEF".equals(napChara.type)) {

            accessPointAuthList = new ArrayList<ClientProvAccessPointAuth>();
            accessPointValidityList = new ArrayList<ClientProvAccessPointValidity>();
            accessPointDefList = new ArrayList<ClientProvAccessPointDef>();

            tempList = napChara.getParm("NAPID");
            if (tempList != null && tempList.size() > 0)
                napID = napChara.getParm("NAPID").get(0).value;
            tempList = napChara.getParm("NAME");
            if (tempList != null && tempList.size() > 0)
                name = napChara.getParm("NAME").get(0).value;
            tempList = napChara.getParm("NAP-ADDRESS");
            if (tempList != null && tempList.size() > 0)
                apn = napChara.getParm("NAP-ADDRESS").get(0).value;

            Iterator<Characteristic> napInner = napChara
                    .getInnerCharacteristic();
            if (null != napInner) {
                while (napInner.hasNext()) {
                    Characteristic innerChara = napInner.next();
                    if ("NAPAUTHINFO".equals(innerChara.type)) {
                        accessPointAuthList.add(new ClientProvAccessPointAuth(
                                innerChara));
                    } else if ("VALIDITY".equals(innerChara.type)) {
                        ClientProvAccessPointValidity validity = new ClientProvAccessPointValidity(
                                innerChara);
                        while (validity.hasNext()) {
                            accessPointValidityList.add(validity.next());
                        }
                    }
                }
                // compose(accessPointAuthList,accessPointValidityList);
            }
            compose(accessPointAuthList, accessPointValidityList);
        }

    }

    public ArrayList<ClientProvAccessPointDef> getClientProvAccessPointDef() {
        return accessPointDefList;
    }

    private void compose(ArrayList<ClientProvAccessPointAuth> authInfoList,
            ArrayList<ClientProvAccessPointValidity> validityList) {
        if (!authInfoList.isEmpty() && !validityList.isEmpty()) {
            for (int i = 0; i < authInfoList.size(); i++) {
                for (int j = 0; j < validityList.size(); j++) {
                    ClientProvAccessPointDef apDef = new ClientProvAccessPointDef();
                    apDef.name = this.name;
                    apDef.apn = this.apn;
                    apDef.napID = this.napID;
                    apDef.accessPointAuth = authInfoList.get(i);
                    apDef.accessPointValidity = validityList.get(j);
                    accessPointDefList.add(apDef);
                }
            }
        } else if (authInfoList.isEmpty() && !validityList.isEmpty()) {
            for (int j = 0; j < validityList.size(); j++) {
                ClientProvAccessPointDef apDef = new ClientProvAccessPointDef();
                apDef.name = this.name;
                apDef.apn = this.apn;
                apDef.napID = this.napID;
                apDef.accessPointAuth = null;
                apDef.accessPointValidity = validityList.get(j);
                accessPointDefList.add(apDef);
            }
        } else if (!authInfoList.isEmpty() && validityList.isEmpty()) {
            for (int i = 0; i < authInfoList.size(); i++) {
                ClientProvAccessPointDef apDef = new ClientProvAccessPointDef();
                apDef.name = this.name;
                apDef.apn = this.apn;
                apDef.napID = this.napID;
                apDef.accessPointAuth = authInfoList.get(i);
                apDef.accessPointValidity = null;
                accessPointDefList.add(apDef);
            }
        } else {
            ClientProvAccessPointDef apDef = new ClientProvAccessPointDef();
            apDef.name = this.name;
            apDef.apn = this.apn;
            apDef.napID = this.napID;
            apDef.accessPointAuth = null;
            apDef.accessPointValidity = null;
            accessPointDefList.add(apDef);
        }
    }

    public class ClientProvAccessPointAuth {

        public String authType;
        public String userName;
        public String userSecret;

        ClientProvAccessPointAuth(Characteristic authInfoChara) {
            ArrayList<Parm> parms;
            parms = authInfoChara.getParm("AUTHTYPE");
            if (parms != null && parms.size() > 0)
                authType = authInfoChara.getParm("AUTHTYPE").get(0).value;

            parms = authInfoChara.getParm("AUTHNAME");
            if (null != parms && parms.size() > 0) {
                userName = parms.get(0).value;
            }

            parms = authInfoChara.getParm("AUTHSECRET");
            if (null != parms && parms.size() > 0) {
                userSecret = parms.get(0).value;
            }

        }

    }

    public class ClientProvAccessPointValidity {
        public String country;
        public String network;

        private String mccs;
        private ArrayList<String> networkList;
        private int index = 0;

        ClientProvAccessPointValidity(Characteristic validityChara) {
            ArrayList<Parm> mccList = validityChara.getParm("NETWORK");
            mccs = (mccList != null && mccList.size() > 0) ? mccList.get(0).value
                    : (String) null;

            ArrayList<Parm> mncList = validityChara.getParm("COUNTRY");

            if (null != mccs) {
                country = (mncList != null && mncList.size() > 0) ? mncList
                        .get(0).value : (String) null;
                networkList = new ArrayList<String>();
                departNetwork();
            }
        }

        private ClientProvAccessPointValidity() {
        }

        private void departNetwork() {
            if (null != mccs) {
                String[] _mccs = mccs.split(","); // ([0-9]{2,3})
                for (String mcc : _mccs) {
                    networkList.add(mcc);
                }
            }
        }

        public boolean hasNext() {
            if (null != networkList && !networkList.isEmpty()) {
                return null != country && index < networkList.size() - 1;
            } else {
                return null != country && index < 1;
            }
        }

        public ClientProvAccessPointValidity next() {
            ClientProvAccessPointValidity validity = new ClientProvAccessPointValidity();
            validity.country = country;
            validity.network = networkList.get(index);
            index++;
            return validity;
        }

    }

}
