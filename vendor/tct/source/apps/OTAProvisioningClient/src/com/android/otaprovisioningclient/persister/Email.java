/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.persister;

import java.util.ArrayList;
import java.util.Iterator;

import com.android.otaprovisioningclient.oma.Application;
import com.android.otaprovisioningclient.oma.Characteristic;
import com.android.otaprovisioningclient.oma.EmailAccount;
import com.android.otaprovisioningclient.oma.Parm;
import com.android.otaprovisioningclient.oma.WapProvisioningDoc;

public class Email {
    // public String TAG = "EmailPersister";

    private Characteristic mEmailApp;
    private WapProvisioningDoc mProvDoc;
    private ArrayList<EmailAccount> mEmailAccountList = null;

    public Email(Characteristic emailApp, WapProvisioningDoc provDoc) {
        mEmailApp = emailApp;
        mProvDoc = provDoc;
        mEmailAccountList = new ArrayList<EmailAccount>();
    }

    public ArrayList<EmailAccount> createEmailAccount() {
        ArrayList<Parm> tempList;
        if (isEmailApp()) {
            EmailAccount emailAcc = new EmailAccount();
            tempList = mEmailApp.getParm("APPID");
            if (tempList != null && tempList.size() > 0)
                emailAcc.email_account_type_foriobound = tempList.get(0).value;
            mEmailAccountList.add(emailAcc);

            ArrayList<Parm> addrParms = mEmailApp.getParm("ADDR");
            if (null != addrParms && addrParms.size() > 0) {
                expandAddress(addrParms);
            } else {
                addrParms = mEmailApp.getParm("FROM");
                if (null != addrParms && addrParms.size() > 0) {
                    expandAddress(addrParms);
                }
            }

            Iterator<Characteristic> innerCharaList = mEmailApp
                    .getInnerCharacteristic();
            if (null != innerCharaList) {
                ArrayList<Characteristic> appAuthList = new ArrayList<Characteristic>();
                ArrayList<Characteristic> appAddrList = new ArrayList<Characteristic>();
                while (innerCharaList.hasNext()) {
                    Characteristic curChara = innerCharaList.next();
                    if (curChara.type.equals("APPAUTH")) {
                        appAuthList.add(curChara);
                    } else if (curChara.type.equals("APPADDR")) {
                        appAddrList.add(curChara);
                    }
                }
                if (appAuthList.size() > 0) {
                    expandAppAuth(appAuthList);
                }
                if (appAddrList.size() > 0) {
                    expandAppAddr(appAddrList);
                }
            }
        }
        return mEmailAccountList;
    }

    private boolean isEmailApp() {
        boolean isEmail = false;
        ArrayList<Parm> tempList;
        if (null != mEmailApp) {
            tempList = mEmailApp.getParm("APPID");
            if (tempList != null && tempList.size() > 0) {
                String mAppID = tempList.get(0).value;
                isEmail = mAppID.equals(Application.INBOUNDEMAIL_IMAP4)
                        || mAppID.equals(Application.INBOUNDEMAIL_POP3)
                        || mAppID.equals(Application.OUTBOUNDEMAIL);
            }
        }
        return isEmail;

    }

    private void expandAddress(ArrayList<Parm> addrList) {

        if (null != addrList && addrList.size() > 0) {
            ArrayList<EmailAccount> cloneList = (ArrayList<EmailAccount>) mEmailAccountList
                    .clone();
            mEmailAccountList.clear();

            for (EmailAccount curAcc : cloneList) {
                for (Parm curAddr : addrList) {
                    curAcc.email_address = curAddr.value;
                    mEmailAccountList.add(curAcc);
                }
            }
        }

    }

    private void expandAppAuth(ArrayList<Characteristic> appAuthList) {

        if (null != appAuthList && appAuthList.size() > 0) {
            ArrayList<EmailAccount> cloneList = (ArrayList<EmailAccount>) mEmailAccountList
                    .clone();
            mEmailAccountList.clear();

            for (EmailAccount curAcc : cloneList) {
                for (Characteristic curAppAuth : appAuthList) {
                    ArrayList<Parm> curParmList = curAppAuth
                            .getParm("AAUTHNAME");
                    curAcc.email_account_for_authentication = (null != curParmList && curParmList
                            .size() > 0) ? curParmList.get(0).value : null;
                    curParmList = curAppAuth.getParm("AAUTHSECRET");
                    curAcc.email_password_for_authentication = (null != curParmList && curParmList
                            .size() > 0) ? curParmList.get(0).value : null;
                    mEmailAccountList.add(curAcc);
                }
            }
        }
    }

    private void expandAppAddr(ArrayList<Characteristic> appAddrList) {

        if (null != appAddrList && appAddrList.size() > 0) {
            ArrayList<EmailAccount> cloneList = (ArrayList<EmailAccount>) mEmailAccountList
                    .clone();
            mEmailAccountList.clear();
            for (EmailAccount curAcc : cloneList) {
                for (Characteristic appAddrChara : appAddrList) {
                    ArrayList<EmailAccount> emailAppAddr = composeAppAddr(appAddrChara);
                    if (null != emailAppAddr && emailAppAddr.size() > 0) {
                        for (EmailAccount acc : emailAppAddr) {
                            curAcc.ioboundAddress = acc.ioboundAddress;
                            curAcc.ioboundPort = acc.ioboundPort;
                            curAcc.security_type_foriobound = acc.security_type_foriobound;
                            mEmailAccountList.add(curAcc);
                        }
                    } else {
                        mEmailAccountList.add(curAcc);
                    }
                }
            }
        }
    }

    private ArrayList<EmailAccount> composeAppAddr(Characteristic appAddrChara) {
        ArrayList<Parm> tempList;
        ArrayList<EmailAccount> accounts = new ArrayList<EmailAccount>();
        if (null != appAddrChara && appAddrChara.type.equals("APPADDR")) {
            EmailAccount newAcc = new EmailAccount();
            tempList = appAddrChara.getParm("ADDR");
            if (tempList != null && tempList.size() > 0)
                newAcc.ioboundAddress = tempList.get(0).value;
            Iterator<Characteristic> innerCharas = appAddrChara
                    .getInnerCharacteristic();
            if (null != innerCharas) {
                while (innerCharas.hasNext()) {
                    Characteristic curChara = innerCharas.next();
                    if (curChara.type.equals("PORT")) {
                        ArrayList<EmailAccount> emailPorts = composePort(curChara);
                        if (null != emailPorts && emailPorts.size() > 0) {
                            for (EmailAccount curAcc : emailPorts) {
                                newAcc.ioboundPort = curAcc.ioboundPort;
                                newAcc.security_type_foriobound = curAcc.security_type_foriobound;
                                accounts.add(newAcc);
                            }
                        }
                    }
                }
            } else {
                accounts.add(newAcc);
            }

        }

        return accounts;
    }

    private ArrayList<EmailAccount> composePort(Characteristic portChara) {
        ArrayList<EmailAccount> accounts = new ArrayList<EmailAccount>();
        ArrayList<Parm> tempList;
        if (null != portChara && portChara.type.equals("PORT")) {
            EmailAccount newAcc = new EmailAccount();
            tempList = portChara.getParm("PORTNBR");
            if (tempList != null && tempList.size() > 0)
                newAcc.ioboundPort = tempList.get(0).value;
            ArrayList<Parm> srvParmList = portChara.getParm("SERVICE");
            if (null != srvParmList && srvParmList.size() > 0) {
                ArrayList<EmailAccount> servers = expandServer(srvParmList,
                        newAcc);
                if (null != servers && servers.size() > 0) {
                    for (EmailAccount acc : servers) {
                        accounts.add(acc);
                    }
                }
            } else {
                accounts.add(newAcc);
            }
        }

        return accounts;
    }

    private ArrayList<EmailAccount> expandPort(
            ArrayList<Characteristic> portList, EmailAccount curAcc) {
        ArrayList<EmailAccount> allAccounts = new ArrayList<EmailAccount>();
        if (null != curAcc && null != portList && portList.size() > 0) {
            for (Characteristic portChara : portList) {
                EmailAccount newAccount = new EmailAccount(curAcc);
                ArrayList<Parm> curParmList = portChara.getParm("PORTNBR");

                curAcc.ioboundPort = (null != curParmList && curParmList.size() > 0) ? curParmList
                        .get(0).value : null;

                ArrayList<Parm> srvList = portChara.getParm("SERVICE");
                if (null != srvList && srvList.size() > 0) {

                }
            }
        }
        return allAccounts;
    }

    // set security_type
    private ArrayList<EmailAccount> expandServer(ArrayList<Parm> srvList,
            EmailAccount curAcc) {
        ArrayList<EmailAccount> allAccounts = new ArrayList<EmailAccount>();
        if (null != curAcc && null != srvList && srvList.size() > 0) {
            for (Parm srv : srvList) {
                EmailAccount newAcc = new EmailAccount(curAcc);
                newAcc.security_type_foriobound = srv.value;
                allAccounts.add(newAcc);
            }
        }
        return allAccounts;
    }

}
