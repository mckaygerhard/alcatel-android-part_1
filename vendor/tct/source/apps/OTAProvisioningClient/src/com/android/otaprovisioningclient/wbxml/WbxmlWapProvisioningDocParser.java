/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 01/24/2014|      Xu Danbin       |      PR-594556       |fix force close   */
/*           |                      |                      |caused by illegal */
/*           |                      |                      | param            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.otaprovisioningclient.wbxml;

import android.util.TctLog;

import com.android.otaprovisioningclient.wap.WbxmlParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.android.otaprovisioningclient.oma.Characteristic;
import com.android.otaprovisioningclient.oma.NAPHandler;
import com.android.otaprovisioningclient.oma.Parm;
import com.android.otaprovisioningclient.oma.WapProvisioningDoc;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class WbxmlWapProvisioningDocParser {

    private WapProvisioningDoc mDocument;

    private WbxmlParser mPullParser;

    private String TAG ="OTACP";


    public WbxmlWapProvisioningDocParser() {
        mDocument = new WapProvisioningDoc();
        mPullParser = new WbxmlParser();
    }

    public WapProvisioningDoc parse(InputStream bytesIn) throws XmlPullParserException, IOException {
        mPullParser.setTagTable(0, WbxmlTokenTable.TAG_TABLE_CODEPAGE_0);
        mPullParser.setTagTable(1, WbxmlTokenTable.TAG_TABLE_CODEPAGE_1);
        mPullParser.setAttrStartTable(0, WbxmlTokenTable.ATTRIBUTE_START_TABLE_CODEPAGE_0);
        mPullParser.setAttrStartTable(1, WbxmlTokenTable.ATTRIBUTE_START_TABLE_CODEPAGE_1);
        mPullParser.setAttrValueTable(0, WbxmlTokenTable.ATTRIBUTE_VALUE_TABLE_CODEPAGE_0);
        mPullParser.setAttrValueTable(1, WbxmlTokenTable.ATTRIBUTE_VALUE_TABLE_CODEPAGE_1);

        mPullParser.setInput(bytesIn, null);

        parseWapProvisioningDoc();

        NAPHandler.checkValidity(mDocument);

        return mDocument;
    }

    private boolean match(int nextState, int expectedState, String expectedName)
            throws XmlPullParserException, IOException {
        if (expectedState == nextState) {
            if ((expectedName == null && mPullParser.getName() == null)
                    || expectedName.equals(mPullParser.getName())) {
                return true;
            }
        }

        String error = "Parse error, expected " + expectedName + ":" + expectedState + " got "
                + mPullParser.getName() + ":" + nextState;

        throw new XmlPullParserException(error);
    }


    private HashMap<String, String> getAttributes() {
        HashMap<String, String> attributes = new HashMap<String, String>();

        int attributeCount = 0;
        if ((attributeCount = mPullParser.getAttributeCount()) > 0) {
            for (int i = 0; i < attributeCount; i++) {
                attributes.put(mPullParser.getAttributeName(i), mPullParser.getAttributeValue(i));
            }
        }

        return attributes;
    }


    /**
     * parse a client provisionging document
     */
    private void parseWapProvisioningDoc() throws XmlPullParserException, IOException {
   //---PR 230218: NMR: store namespace, start =====================
        String nameSpace = null;
   //---PR 230218: NMR: store namespace, over =====================
        match(mPullParser.next(), XmlPullParser.START_TAG, "wap-provisioningdoc");

        int nextState = mPullParser.next();
        while(nextState==XmlPullParser.TEXT){
            nextState = mPullParser.next();
        }

        String nextName = mPullParser.getName();

        TctLog.i(TAG,"<wap-provisioningdoc>");

        while (nextState == XmlPullParser.START_TAG && "characteristic".equals(nextName)) {  //root characteristics

            HashMap<String, String> characteristicAttributes = getAttributes();
            String characteristicType = characteristicAttributes.get("type");

            if(null==characteristicType || "".equals(characteristicType)){
                continue;
            }

   //---PR 230218: NMR: store namespace, start =====================
            if(nameSpace == null)
               TctLog.i(TAG,"<characteristic type="+ "\""+characteristicType+"\">");
            else
               TctLog.i(TAG,"<" + nameSpace + "characteristic type="+ "\""+characteristicType+"\">");

            Characteristic enclosingCharacteristic = new Characteristic(characteristicType, nameSpace);
   //---PR 230218: NMR: store namespace, over =====================
            parseCharacteristic(enclosingCharacteristic);

   //---PR 230218: NMR: store namespace, start =====================
            if(nameSpace == null)
                TctLog.i(TAG,"</characteristic>");
            else
                TctLog.i(TAG,"</" + nameSpace + "characteristic>");
   //---PR 230218: NMR: store namespace, over =====================

            mDocument.addCharacteristic(enclosingCharacteristic);


            nextState = mPullParser.next();

            while(nextState==XmlPullParser.TEXT){
                nextState = mPullParser.next();
            }

            nextName = mPullParser.getName();

   //---PR 230218: NMR: store namespace, start =====================

                if(null!=nextName){
                    nameSpace = null;
                    String [] temp1 = nextName.split(":");
                    if(temp1.length == 2){
                        nextName = temp1[1];
                        nameSpace = temp1[0] + ":";
                    }
                }

   //---PR 230218: NMR: store namespace, over =====================
        }

        match(nextState, XmlPullParser.END_TAG, "wap-provisioningdoc");

        TctLog.i(TAG,"</wap-provisioningdoc>");

        match(mPullParser.next(), XmlPullParser.END_DOCUMENT, null);
    }

    /**
     * parse a characteristic
     * @param enclosingCharacteristic  the enclosing characteristic of parm or inner characteristic elements
     * @return void
     */
    private void parseCharacteristic(Characteristic enclosingCharacteristic) throws XmlPullParserException, IOException{
   //---PR 230218: NMR: store namespace, start =====================
        String nameSpace = null;
   //---PR 230218: NMR: store namespace, over =====================
        int nextState = mPullParser.next();

        while(nextState==XmlPullParser.TEXT){
            nextState = mPullParser.next();
        }

        String nextName = mPullParser.getName(); //tag name


   //---PR 230218: NMR: store namespace, start =====================
        if(null!=nextName){
            String [] temp1 = nextName.split(":");
            if(temp1.length == 2){
                nextName = temp1[1];
                nameSpace = temp1[0] + ":";
            }
        }
   //---PR 230218: NMR: store namespace, over =====================

        while (nextState == XmlPullParser.START_TAG
                && ("parm".equals(nextName) || "characteristic".equals(nextName))){
            if("parm".equals(nextName)){
                parseParm(enclosingCharacteristic, nameSpace);
            }else if("characteristic".equals(nextName)){
                HashMap<String, String> characteristicAttributes = getAttributes();
                String characteristicType = characteristicAttributes.get("type");

                if(null==characteristicType || "".equals(characteristicType)){
                    continue;
                }

   //---PR 230218: NMR: store namespace, start =====================
                if(nameSpace == null)
                    TctLog.i(TAG,"<characteristic type="+ "\""+characteristicType+"\">");
             else
                    TctLog.i(TAG,"<" + nameSpace + "characteristic type="+ "\""+characteristicType+"\">");

                Characteristic innerCharacteristic = new Characteristic(characteristicType, nameSpace);
   //---PR 230218: NMR: store namespace, over =====================

                parseCharacteristic(innerCharacteristic);

   //---PR 230218: NMR: store namespace, start =====================
            if(nameSpace == null)
                TctLog.i(TAG,"</characteristic>");
         else
                TctLog.i(TAG,"</" + nameSpace + "characteristic>");
   //---PR 230218: NMR: store namespace, over =====================

                enclosingCharacteristic.addInnerCharacteristic(innerCharacteristic);
            }
            nextState = mPullParser.next();

            while(nextState==XmlPullParser.TEXT){
                nextState = mPullParser.next();
            }

            nextName = mPullParser.getName();


   //---PR 230218: NMR: store namespace, start =====================
            if(null!=nextName){
                nameSpace = null;
                String[] temp1 = nextName.split(":");
                if(temp1.length == 2){
                    nextName = temp1[1];
                    nameSpace = temp1[0] + ":";
                }
            }
        }

     if(enclosingCharacteristic.mNameSpace == null)
            match(nextState, XmlPullParser.END_TAG, "characteristic");
     else
            match(nextState, XmlPullParser.END_TAG, enclosingCharacteristic.mNameSpace + "characteristic");
   //---PR 230218: NMR: store namespace, over =====================

    }

    /**
     * parse a parm element
     * @param enclosingCharacteristic  the enclosing characteristic of the parm element
     * @return void
     */
    private void parseParm(Characteristic enclosingCharacteristic, String nameSpace) throws XmlPullParserException, IOException{
        HashMap<String, String> attributes = getAttributes();
        String parmName = attributes.get("name");
        String parmValue = "";
        if(attributes.containsKey("value")){
            parmValue=attributes.get("value");
        }
     String parmXmlnsValue = null;
     String xmlnsName = null;
     if(nameSpace != null){
         xmlnsName = "xmlns:" + nameSpace.substring(0,nameSpace.length()-1);
            if(attributes.containsKey(xmlnsName)){
                parmXmlnsValue=attributes.get(xmlnsName);
            }
     }

        if("BOOTSTRAP".equals(enclosingCharacteristic.type)){
            if("NAME".equals(parmName)){
                mDocument.name=parmValue;
            }
        }

        String log="<";;
     if(nameSpace != null)
            log += nameSpace;// "<parm name=\""+parmName+"\"   value=\""+parmValue+"\"/>";
     log += "parm name=\""+parmName+"\"   ";//value=\""+parmValue+"\"/>";
     if(parmXmlnsValue != null && xmlnsName != null)
        log += xmlnsName + "=\""+parmXmlnsValue+"\"   ";
     log += "value=\""+parmValue+"\"/>";

        TctLog.i(TAG,log);
        Parm parm= new Parm();
        parm.name = parmName;
        parm.value = parmValue;
        //[BUGFIX]-Add&Mod-BEGIN by TCTNB.Xu Danbin,01/24/2014,PR-594556,
        //fix force close caused by illegal param
        if (!enclosingCharacteristic.addParm(parm)) {
            String errStr = "Illegal param " + parmName + " not be added.";
            throw new IOException(errStr);
        }
        //[BUGFIX]-Add&Mod-END by TCTNB.Xu Danbin

        if("APPID".equals(parmName)){
            if("w4".equals(parmValue)){
                mDocument.docType = mDocument.docType | 0x02 ;
            }
            if("w2".equals(parmValue)){
                mDocument.docType = mDocument.docType | 0x01 ;
            }
            if("25".equals(parmValue) || "110".equals(parmValue)){
                mDocument.docType = mDocument.docType | 0x04 ;
            }

        }

     if(nameSpace == null)
            match(mPullParser.next(), XmlPullParser.END_TAG, "parm");
     else
            match(mPullParser.next(), XmlPullParser.END_TAG, nameSpace + "parm");

    }


}
