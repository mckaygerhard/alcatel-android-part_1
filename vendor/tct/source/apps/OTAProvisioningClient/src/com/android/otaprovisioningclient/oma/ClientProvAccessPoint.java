/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.oma;

public class ClientProvAccessPoint {

    /*
     * elements stored in table carriers are divided into proxy or access point
     * type
     */
    public ClientProvAccessPointProxy proxy;
    /*
     * proxy,port,mmsproxy,mmsport,mmsc
     */

    public ClientProvAccessPointDef napDef;

    /*
     * name,apn,type,authtype,user,password,mcc,mnc
     */

    /*
     * Added by xiaobin.yang for BUG1001223, mark the app type
     */
    public String mAppType;

    public ClientProvAccessPoint() {
    };

    public ClientProvAccessPoint(ClientProvAccessPoint AP) {
        proxy = new ClientProvAccessPointProxy();
        proxy.mmsc = AP.proxy.mmsc;
        proxy.mmsport = AP.proxy.mmsport;
        proxy.mmsproxy = AP.proxy.mmsproxy;
        proxy.proxy = AP.proxy.proxy;
        proxy.port = AP.proxy.port;

        napDef = new ClientProvAccessPointDef();
        napDef.accessPointValidity = AP.napDef.accessPointValidity;
        napDef.accessPointAuth = AP.napDef.accessPointAuth;
        napDef.apn = AP.napDef.apn;
        napDef.name = AP.napDef.name;
        napDef.type = AP.napDef.type;
        mAppType = AP.mAppType; // Add by xiaobin.yang for fix the BUG1001223

    }

}
