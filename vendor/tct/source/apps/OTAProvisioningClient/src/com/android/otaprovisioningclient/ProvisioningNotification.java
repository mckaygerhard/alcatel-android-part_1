/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 01/09/2014|gong.zhang            |FR-579073             |OMACP client still*/
/*           |                      |                      |active after the  */
/*           |                      |                      |installation of an*/
/*           |                      |                      |OMA-CP message    */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.otaprovisioningclient;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;

public class ProvisioningNotification {

    // private static final int NOTIFICATION_ID = 1;
    private static final String PREFERENCE_NAME = "provisioning_id";

    public static void createNotification(Context context, String sec,
            String mac, byte[] document) {

        String tickerText = context
                .getString(R.string.provisioning_received_ticker);
        String title = context.getString(R.string.provisioning_received_title);
        String message = context
                .getString(R.string.provisioning_received_message);

        NotificationManager manager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = new Notification();
        notification.when = System.currentTimeMillis();
        notification.tickerText = tickerText;
        notification.icon = R.drawable.stat_notify_configuration;

        SharedPreferences preferences = context.getSharedPreferences(
                PREFERENCE_NAME, Context.MODE_WORLD_READABLE
                        | Context.MODE_WORLD_WRITEABLE);
        int notification_id = preferences.getInt("next_provisioning_id", 0);

        notification.setLatestEventInfo(context, title, message,
                ProvisioningValidation.createPendingValidationActivity(context,
                        sec, mac, document, notification_id));
        notification.defaults |= Notification.DEFAULT_ALL;

        manager.notify(notification_id, notification);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("next_provisioning_id", (++notification_id) % 256);
        editor.commit();
    }

    public static void clearNotification(Context context, int notificationID) {
        NotificationManager manager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(notificationID);
    }

    //[BUGFIX]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, FR-579073 , [TMO] click "cancel" shall keep CP message notification, silent notification
    public static void createNotificationTmo(Context context, String sec,String mac, byte[] document) {

        String title = context.getString(R.string.provisioning_received_title);
        String message = context.getString(R.string.provisioning_received_message);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = new Notification();
        notification.when = System.currentTimeMillis();
        notification.tickerText = null;
        notification.icon = R.drawable.stat_notify_configuration;

        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_WORLD_READABLE | Context.MODE_WORLD_WRITEABLE);
        int notification_id = preferences.getInt("next_provisioning_id", 0);

        notification.setLatestEventInfo(context, title, message,ProvisioningValidation.createPendingValidationActivity(context,sec, mac, document, notification_id));
        notification.defaults= Notification.DEFAULT_LIGHTS;

        manager.notify(notification_id, notification);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("next_provisioning_id", (++notification_id) % 256);
        editor.commit();
    }
    //[BUGFIX]-Add-BEGIN by TCTNB.gong.zhang,

}
