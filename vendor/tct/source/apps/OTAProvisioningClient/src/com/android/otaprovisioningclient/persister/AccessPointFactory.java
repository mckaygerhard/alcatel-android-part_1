/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.persister;

import java.util.ArrayList;
import java.util.Iterator;

import android.util.TctLog;

import com.android.otaprovisioningclient.oma.Application;
import com.android.otaprovisioningclient.oma.Characteristic;
import com.android.otaprovisioningclient.oma.ClientProvAccessPoint;
import com.android.otaprovisioningclient.oma.ClientProvAccessPointDef;
import com.android.otaprovisioningclient.oma.ClientProvAccessPointProxy;
import com.android.otaprovisioningclient.oma.ClientProvAccessPointProxy.ProvPxPhysical;
import com.android.otaprovisioningclient.oma.Element;
import com.android.otaprovisioningclient.oma.EmailAccount;
import com.android.otaprovisioningclient.oma.Parm;
import com.android.otaprovisioningclient.oma.WapProvisioningDoc;

public class AccessPointFactory {

    private String TAG = "OTACP";

    private WapProvisioningDoc mProvDoc;

    // a list of access point from provisioning doc
    private ArrayList<ClientProvAccessPoint> provAccessPointList = new ArrayList<ClientProvAccessPoint>();

    private ArrayList<EmailAccount> emailAccountList = new ArrayList<EmailAccount>();

    private ArrayList<Characteristic> remainingNapdef;
    private ArrayList<Characteristic> remainingProxy;

    // APPID of current APPLICATION
    private String mAppID;

    // ADDR list of current APPLICATION
    private ArrayList<String> mAddrList = new ArrayList<String>();

    public AccessPointFactory(WapProvisioningDoc provDoc) {
        mProvDoc = provDoc;
    }

    /**
     * compose all the access points based on a provisioning doc all the access
     * points are store in a list
     *
     * @return a list of all the access points
     */
    public ArrayList<ClientProvAccessPoint> createAccessPoints() {

        // all the APPLICATION Characteristic in the provisioning doc
        ArrayList<Characteristic> application = mProvDoc
                .getCharacteristic(Element.APPLICATION);
        Iterator<Characteristic> appIterator = (null != application) ? application
                .iterator() : null;

        // all the PXLOGICAL Characteristic in the provisioning doc
        ArrayList<Characteristic> allProxy = mProvDoc
                .getCharacteristic(Element.PXLOGICAL);

        // all the NAPDEF Characteristic in the provisioning doc
        ArrayList<Characteristic> allNapdef = mProvDoc
                .getCharacteristic(Element.NAPDEF);

        // PROXY Characteristic unused by APPLICATIONs
        remainingProxy = mProvDoc.getCharacteristic(Element.PXLOGICAL);

        // NAPDEF Characteristic unused by APPLICATIONs
        remainingNapdef = mProvDoc.getCharacteristic(Element.NAPDEF);

        TctLog.i(TAG, "createAccessPoints");

        if (null != appIterator) {
            while (appIterator.hasNext()) {

                Characteristic curApp = appIterator.next();

                ArrayList<Parm> appIDs = curApp.getParm("APPID");
                if (null == appIDs || appIDs.isEmpty()) {
                    continue;
                }
                mAppID = appIDs.get(0).value;

                if (mAppID.equals(Application.INBOUNDEMAIL_IMAP4)
                        || mAppID.equals(Application.INBOUNDEMAIL_POP3)
                        || mAppID.equals(Application.OUTBOUNDEMAIL)) {
                    continue;
                }

                TctLog.i(TAG, "createAccessPoints,APPID:" + mAppID);

                mAddrList.clear(); // clear the last APPLICATION's ADDR

                ArrayList<Parm> addrList = curApp.getParm("ADDR");
                if (null != addrList && !addrList.isEmpty()) {
                    for (Parm appr : addrList) {
                        mAddrList.add(appr.value);
                    }
                }

                // the TO-PROXY parm used by current APPLICATION
                ArrayList<Parm> pxLogicalParmList = curApp.getParm("TO-PROXY");
                // the TO-NAPID parm used by current APPLICATION
                ArrayList<Parm> napDefParmList = curApp.getParm("TO-NAPID");

                // PROXY Characteristic corresponding to APPLICATION.TO-PROXY
                ArrayList<Characteristic> appProxyList = new ArrayList<Characteristic>();

                if (null != pxLogicalParmList && !pxLogicalParmList.isEmpty()) {

                    if (null != allProxy && !allProxy.isEmpty()) {

                        for (int i = 0; i < pxLogicalParmList.size(); i++) {

                            String proxyID = pxLogicalParmList.get(i).value;

                            for (int j = 0; j < allProxy.size(); j++) {
                                Characteristic proxyChara = allProxy.get(j);
                                String id = proxyChara.getParm("PROXY-ID").get(
                                        0).value;
                                if (proxyID.equals(id)) {
                                    appProxyList.add(proxyChara);
                                    removeProxy(remainingProxy, proxyChara);
                                    break;
                                }
                            }
                        }
                    }

                }

                if (!appProxyList.isEmpty()) {
                    compose(appProxyList, null);
                }

                // NAPDEF characteristic corresponding to APPLICATION.TO-NAPID
                ArrayList<Characteristic> appNapList = new ArrayList<Characteristic>();

                if (null != napDefParmList && !napDefParmList.isEmpty()) {

                    if (null != allNapdef && !allNapdef.isEmpty()) {

                        for (int i = 0; i < napDefParmList.size(); i++) {
                            String napID = napDefParmList.get(i).value;
                            for (int j = 0; j < allNapdef.size(); j++) {
                                Characteristic napChara = allNapdef.get(j);
                                String id = napChara.getParm("NAPID").get(0).value;
                                if (napID.equals(id)) {
                                    appNapList.add(napChara);
                                    removeNap(remainingNapdef, napChara);
                                    break;
                                }
                            }
                        }

                    }
                }

                if (!appNapList.isEmpty()) {
                    compose(null, appNapList);
                }

            }// end while

        }// end if

        mAppID = "";
        if ((null != remainingProxy) && (remainingProxy.size() > 0)) {
            compose(remainingProxy, null);
        }

        if (null != remainingNapdef && remainingNapdef.size() > 0) {
            compose(null, remainingNapdef);
        }

        return provAccessPointList;
    }

    /**
     * compose all the access points from proxy and nap
     *
     * @param proxyList
     *            a list of proxy characteristic
     * @param napdefList
     *            a list of napdef characteristic
     * @return void all the access points are store in a list
     */
    private void compose(ArrayList<Characteristic> proxyList,
            ArrayList<Characteristic> napdefList) {

        ClientProvAccessPoint accessPoint = new ClientProvAccessPoint();

        if (null != proxyList && !proxyList.isEmpty()) {

            for (Characteristic curProxy : proxyList) {

                TctLog.i(TAG,
                        "compose,proxy:"
                                + curProxy.getParm("PROXY-ID").get(0).value);

                ClientProvAccessPointProxy provProxy = new ClientProvAccessPointProxy(
                        curProxy, mProvDoc);

                for (int i = 0; i < provProxy.physicalProxy.size(); i++) {

                    ProvPxPhysical physicalPx = provProxy.physicalProxy.get(i);

                    provProxy.proxy = physicalPx.address;

                    // a PXPHSICAL has at least one TO-NAPID
                    for (ClientProvAccessPointDef napdef : physicalPx.provApDefList) {

                        ClientProvAccessPoint newAP = new ClientProvAccessPoint();

                        newAP.napDef = napdef;

                        removeNap(remainingNapdef, napdef.napID);

                        newAP.napDef.type = "default";
                        newAP.mAppType = "default";//Add by xiaobin.yang for fix the BUG1001223


                        if ("w4".equals(mAppID)) {
                            newAP.napDef.type = "mms";
                        } else if ("w2".equals(mAppID)) {
                            newAP.napDef.type = "default";
                            newAP.mAppType = "w2";//Add by xiaobin.yang for fix the BUG1001223
                        }

                        if (null != physicalPx.portList
                                && !physicalPx.portList.isEmpty()) {
                            for (String _port : physicalPx.portList) {
                                provProxy.port = _port;

                                if ("w4".equals(mAppID)) {
                                    provProxy.mmsproxy = provProxy.proxy;
                                    provProxy.mmsport = provProxy.port;
                                    TctLog.d(TAG, "Mms  set proxy and port empty");
                                    provProxy.proxy = "";
                                    provProxy.port = "";
                                    if (!mAddrList.isEmpty()) {
                                        for (String appr : mAddrList) {
                                            provProxy.mmsc = appr;
                                            newAP.proxy = provProxy;
                                            ClientProvAccessPoint curAP = new ClientProvAccessPoint(
                                                    newAP);
                                            provAccessPointList.add(curAP);
                                        }

                                    } else {
                                        newAP.proxy = provProxy;
                                        ClientProvAccessPoint curAP = new ClientProvAccessPoint(
                                                newAP);
                                        provAccessPointList.add(curAP);
                                    }
                                } else {
                                    newAP.proxy = provProxy;
                                    ClientProvAccessPoint curAP = new ClientProvAccessPoint(
                                            newAP);
                                    provAccessPointList.add(curAP);
                                }

                            }

                        } else {
                            if ("w4".equals(mAppID)) {
                                provProxy.mmsproxy = provProxy.proxy;
                                provProxy.mmsport = provProxy.port;
                                if (!mAddrList.isEmpty()) {
                                    for (String appr : mAddrList) {
                                        provProxy.mmsc = appr;
                                        newAP.proxy = provProxy;
                                        ClientProvAccessPoint curAP = new ClientProvAccessPoint(
                                                newAP);
                                        provAccessPointList.add(curAP);
                                    }
                                } else {
                                    newAP.proxy = provProxy;
                                    provAccessPointList.add(newAP);
                                }
                            } else {
                                newAP.proxy = provProxy;
                                provAccessPointList.add(newAP);
                            }
                        }

                    }
                }

            }// end for
        }// else
        if (null != napdefList && !napdefList.isEmpty()) {

            for (Characteristic curNapdef : napdefList) {

                ClientProvAccessPoint newAP = new ClientProvAccessPoint();
                ClientProvAccessPointDef accessPointDef = new ClientProvAccessPointDef(
                        curNapdef);
                ArrayList<ClientProvAccessPointDef> apDefList = accessPointDef
                        .getClientProvAccessPointDef();

                if (null != apDefList && !apDefList.isEmpty()) {
                    for (int i = 0; i < apDefList.size(); i++) {
                        newAP.napDef = apDefList.get(i);
                        newAP.napDef.type = "default";
                        newAP.mAppType = "default";//Add by xiaobin.yang for fix the BUG1001223


                        if ("w4".equals(mAppID)) {
                            newAP.napDef.type = "mms";
                        } else if ("w2".equals(mAppID)) {
                            newAP.napDef.type = "default";
                            newAP.mAppType = "w2";//Add by xiaobin.yang for fix the BUG1001223
                        }

                        if (null != mAddrList && !mAddrList.isEmpty()) {
                            for (String appr : mAddrList) {
                                // provProxy.mmsc=appr;
                                ClientProvAccessPointProxy provProxy = new ClientProvAccessPointProxy(
                                        null, mProvDoc);
                                provProxy.mmsc = appr;
                                newAP.proxy = provProxy;
                                ClientProvAccessPoint curAP = new ClientProvAccessPoint(
                                        newAP);
                                provAccessPointList.add(curAP);
                            }
                        } else {
                            newAP.proxy = null;
                            provAccessPointList.add(newAP);
                        }

                    }
                }

            }// end for
        }

    }

    /**
     * compose a proxy with a set of nap characteristic
     *
     * @param provProxy
     *            a proxy
     * @param napdefList
     *            a list of napdef characteristic
     * @return void all the access points are store in a list
     */
    private void composeToNap(ClientProvAccessPointProxy provProxy,
            ArrayList<Characteristic> napdefList) {
        ArrayList<Parm> tempList;
        ClientProvAccessPoint accessPoint = new ClientProvAccessPoint();
        accessPoint.proxy = provProxy;
        for (Characteristic curNapdef : napdefList) {
            ClientProvAccessPointDef accessPointDef = new ClientProvAccessPointDef(
                    curNapdef);
            ArrayList<ClientProvAccessPointDef> apDefList = accessPointDef
                    .getClientProvAccessPointDef();
            if (null != apDefList && !apDefList.isEmpty()) {
                for (int i = 0; i < apDefList.size(); i++) {
                    accessPoint.napDef = apDefList.get(i);
                    tempList = curNapdef.getParm("NAME");
                    if (tempList != null && tempList.size() > 0)
                        accessPoint.napDef.name = tempList.get(0).value;
                    accessPoint.napDef.type = "default";
                    if ("w4".equals(mAppID)) {
                        accessPoint.napDef.type = "mms";
                    }
                    provAccessPointList.add(accessPoint);
                }
            }

        }// end for
    }

    /**
     * remove a proxy characteristic from a set of proxy characteristic
     *
     * @param proxyList
     *            set of proxy characteristic
     * @param proxy
     *            a proxy characteristic
     * @return void
     */
    private void removeProxy(ArrayList<Characteristic> proxyList,
            Characteristic proxy) {
        if (null != proxyList && !proxyList.isEmpty() && null != proxy) {
            String proxyID = proxy.getParm("PROXY-ID").get(0).value;
            for (int i = 0; i < proxyList.size(); i++) {
                if (proxyID
                        .equals(proxyList.get(i).getParm("PROXY-ID").get(0).value)) {
                    proxyList.remove(i);
                    break;
                }
            }
        }
    }

    /**
     * remove a napdef characteristic from a set of napdef characteristic
     *
     * @param napList
     *            set of napdef characteristic
     * @param nap
     *            a napdef characteristic
     * @return void
     */
    private void removeNap(ArrayList<Characteristic> napList, Characteristic nap) {
        if (null != napList && !napList.isEmpty() && null != nap) {
            String napID = nap.getParm("NAPID").get(0).value;
            for (int i = 0; i < napList.size(); i++) {
                if (napID.equals(napList.get(i).getParm("NAPID").get(0).value)) {
                    napList.remove(i);
                    break;
                }
            }
        }

    }

    private void removeNap(ArrayList<Characteristic> napList, String napID) {
        if (null != napList && !napList.isEmpty() && null != napID
                && !("".equals(napID))) {
            for (int i = 0; i < napList.size(); i++) {
                if (napID.equals(napList.get(i).getParm("NAPID").get(0).value)) {
                    napList.remove(i);
                    break;
                }
            }
        }

    }

    public ArrayList<EmailAccount> createEmailAccount() {

        // all the APPLICATION Characteristic in the provisioning doc
        ArrayList<Characteristic> application = mProvDoc
                .getCharacteristic(Element.APPLICATION);
        Iterator<Characteristic> appIterator = (null != application) ? application
                .iterator() : null;

        if (null != appIterator) {
            while (appIterator.hasNext()) {
                Characteristic curApp = appIterator.next();

                ArrayList<Parm> appIDs = curApp.getParm("APPID");
                if (null == appIDs || 0 == appIDs.size()) {
                    continue;
                }
                mAppID = appIDs.get(0).value;
                if (mAppID.equals(Application.INBOUNDEMAIL_IMAP4)
                        || mAppID.equals(Application.INBOUNDEMAIL_POP3)
                        || mAppID.equals(Application.OUTBOUNDEMAIL)) {

                    Email mEmail = new Email(curApp, mProvDoc);

                    mProvDoc.removeApplication(curApp);

                    ArrayList<EmailAccount> accList = mEmail
                            .createEmailAccount();
                    if (null != accList && accList.size() > 0) {
                        for (EmailAccount acc : accList) {
                            emailAccountList.add(acc);
                        }
                    }
                }
            }

        }

        return emailAccountList;

    }

    //PR934470-Quanshui-Ye-001 begin
    public ArrayList<EmailAccount> getEmailAccount() {

        // all the APPLICATION Characteristic in the provisioning doc
        ArrayList<Characteristic> application = mProvDoc
                .getCharacteristic(Element.APPLICATION);
        Iterator<Characteristic> appIterator = (null != application) ? application
                .iterator() : null;
        ArrayList<EmailAccount> accountList = new ArrayList<EmailAccount>();
        if (null != appIterator) {
            while (appIterator.hasNext()) {
                Characteristic curApp = appIterator.next();

                ArrayList<Parm> appIDs = curApp.getParm("APPID");
                if (null == appIDs || 0 == appIDs.size()) {
                    continue;
                }
                mAppID = appIDs.get(0).value;
                if (mAppID.equals(Application.INBOUNDEMAIL_IMAP4)
                        || mAppID.equals(Application.INBOUNDEMAIL_POP3)
                        || mAppID.equals(Application.OUTBOUNDEMAIL)) {

                    Email mEmail = new Email(curApp, mProvDoc);

                    ArrayList<EmailAccount> accList = mEmail
                            .createEmailAccount();
                    if (null != accList && accList.size() > 0) {
                        for (EmailAccount acc : accList) {
                        	accountList.add(acc);
                        }
                    }
                }
            }

        }

        return accountList;

    }
    //PR934470-Quanshui-Ye-001 end

}
