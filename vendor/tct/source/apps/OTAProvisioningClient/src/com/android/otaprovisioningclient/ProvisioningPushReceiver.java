/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 01/09/2014|gong.zhang            |CR-578398             |[HOMO]:OMA-CP     */
/*           |                      |                      |message requirment*/
/* ----------|----------------------|----------------------|----------------- */
/* 01/24/2014|     Dandan.Fang      |       PR595961       | [TMO][MTR-2989-  */
/*           |                      |                      |/2994]No configu- */
/*           |                      |                      |ration menu to d- */
/*           |                      |                      |isable or enable  */
/*           |                      |                      |USERPIN           */
/* ----------|----------------------|----------------------|----------------- */
/* 08/05/2014|     xueyong.zhang    |       PR748431       |Message with wrong*/
/*           |                      |                      |IMSI/NetwPIN not  */
/*           |                      |                      |discarded silently*/
/* ----------|----------------------|----------------------|----------------- */
/* 08/21/2014|     dagang.yang      |        767043        |[Wap push&CP]     */
/*           |                      |                      |[Dual]There       */
/*           |                      |                      |is no APN after   */
/*           |                      |                      |receive a CP and  */
/*           |                      |                      |install it        */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.otaprovisioningclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SubscriptionManager;
import android.util.TctLog;

import java.util.HashMap;

import com.android.internal.telephony.PhoneConstants;
import com.android.otaprovisioningclient.utils.HexConvertor;
//[FEATURE]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/06, CR-578398  [HOMO][HOMO][T-Mobile HOMO]: OMA-CP message requirment
import com.android.otaprovisioningclient.R;
//[FEATURE]-Add-END by TCTNB.gong.zhang,



import android.preference.PreferenceManager;
//[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/24/2014,595961,
//[TMO][MTR-2989/2994]No configuration menu to disable or enable USERPIN
import android.provider.Settings;


//[BUGFIX]-Add-END by TCTNB.Dandan.Fang
//[BUGFIX]-MOD-BEGIN by TSCD.xueyong.zhang,08/05/2014,PR-748431
import com.android.otaprovisioningclient.security.OmaSigner;
import com.android.otaprovisioningclient.security.UnknownSecurityMechanismException;

import android.telephony.TelephonyManager;
//[BUGFIX]-MOD-END by TSCD.xueyong.zhang

public class ProvisioningPushReceiver extends BroadcastReceiver {

    // workaround: need use Settings.System.OMA_CP_MESSAGE_USERPIN_ENABLE
    public static final String OMA_CP_MESSAGE_USERPIN_ENABLE = "oma_cp_message_userpin_enable";

    @SuppressWarnings("unchecked")
    @Override
    public void onReceive(Context context, Intent intent) {
        byte[] document = intent.getByteArrayExtra("data");
        String sec = null;
        String mac = null;

        //[BUGFIX]-ADD-BEGIN by TSCD.dagang.yang,08/21/2014, PR-767043-->
        //PR918525-Quanshui-Ye-001 begin
        //int sub = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY, 0);
        // fuwenliang, defect1131749, begin
        //long sub = intent.getLongExtra(PhoneConstants.SUBSCRIPTION_KEY,SubscriptionManager.getDefaultDataSubId());
        int subInt = intent.getIntExtra(PhoneConstants.SUBSCRIPTION_KEY,SubscriptionManager.getDefaultDataSubscriptionId());
        android.util.Log.v("===ydg===", "ProvisioningPushReceiver subInt=" + subInt);
        long sub = subInt;
        android.util.Log.v("===ydg===", "ProvisioningPushReceiver long sub=" + sub);
        // fuwenliang, defect1131749, end
        if(sub <= SubscriptionManager.INVALID_SUBSCRIPTION_ID){
            sub = SubscriptionManager.getDefaultDataSubscriptionId();
         }
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        //editor.putInt("getSubID", sub);
        editor.putLong("getSubID", sub);
        //PR918525-Quanshui-Ye-001 end
        editor.commit();
        android.util.Log.v("===ydg===", "ProvisioningPushReceiver sub="+sub);
        //[BUGFIX]-ADD-END by TSCD.dagang.yang,08/21/2014

        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/24/2014,595961,
        //[TMO][MTR-2989/2994]No configuration menu to disable or enable USERPIN
        int isUserPinAllowed = Settings.System.getInt(context.getContentResolver(),
                OMA_CP_MESSAGE_USERPIN_ENABLE, 0);
        TctLog.i("OTACP", "isUserPinAllowed " + isUserPinAllowed );
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

        if (intent.getExtras() != null) {
            HashMap<String, String> provisioningParameters = (HashMap<String, String>) intent
                    .getExtras().get("contentTypeParameters");
            if (provisioningParameters != null) {
                sec = provisioningParameters.get("SEC"); // security method
                mac = provisioningParameters.get("MAC"); // message authentication code
            }
        }
        //[FEATURE]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, CR-578398  [HOMO][HOMO][T-Mobile HOMO]: OMA-CP message requirment
        //OMA CP message without any authentication or invalid information likeinvalid IMSI  ,device need to discard sliently directly
        /* MODIFIED-BEGIN by wen.ye, 2016-08-23,BUG-2786614*/
        boolean ifIgnore = context.getResources().getBoolean(R.bool.feature_otaprovisioning_ignore_empty_on);
        TctLog.i("OTACP", "ifIgnore:" + ifIgnore + ";    SEC:" + sec + ";    MAC:" + mac);
        if ((ifIgnore
              || context.getResources().getBoolean(R.bool.feature_otaprovisioning_TmoProvision_on))
              /* MODIFIED-END by wen.ye,BUG-2786614*/
            && (sec==null || sec.length()==0)
            && (mac==null || mac.length()==0))
        {
            TctLog.i("OTACP", "SEC or MAC is null, discard sliently directly");
            return;
        }
        //[FEATURE]-Add-END by TCTNB.gong.zhang,

        //[FEATURE]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, CR-578398  [HOMO][HOMO][T-Mobile HOMO]: OMA-CP message requirment
        //CP message include only user pin but without network PIN, device need to discard slently directly
        if (context.getResources().getBoolean(R.bool.feature_otaprovisioning_TmoProvision_on)
            && ("1".equals(sec) && (isUserPinAllowed != 1)))
        {
            String debugString=null;
            //1:USERPIN,0:NETWPIN
            if("1".equals(sec))
                debugString="USERPIN Only";
          /*  else if("0".equals(sec))
                debugString="NETWPIN Only";*/
            TctLog.i("OTACP", "SEC is "+debugString);
            return;
         }
        //[FEATURE]-Add-END by TCTNB.gong.zhang,
        //[BUGFIX]-MOD-BEGIN by TSCD.xueyong.zhang,08/05/2014,PR-748431
        TctLog.i("OTACP", "SEC is NETWPIN Only = "+"0".equals(sec));
        if(context.getResources().getBoolean(R.bool.feature_otaprovisioning_TmoProvision_on) && "0".equals(sec) ) {
            byte[] macbyte;
            String userPin = "notuse";
            OmaSigner networkSigner;
            if (mac != null) {
                macbyte = HexConvertor.convert(mac);
            }else {
                return;
            }

            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            try {
                networkSigner = OmaSigner.signerFor(0, document, tm.getSubscriberId());
            } catch (UnknownSecurityMechanismException e) {
                throw new RuntimeException(e);
            }
            boolean valid = networkSigner.isDocumentValid(userPin, macbyte);
            if (!valid) {
                TctLog.i("OTACP", "[TMO] Networkpin invalid");
                return;
            }
        }
      //[BUGFIX]-MOD-END by TSCD.xueyong.zhang

        String wbxml = HexConvertor.convert(document);
        TctLog.i("OTACP", "data:" + wbxml + ";SEC:" + sec + ";MAC:" + mac);

        ProvisioningNotification
                .createNotification(context, sec, mac, document);
    }

}
