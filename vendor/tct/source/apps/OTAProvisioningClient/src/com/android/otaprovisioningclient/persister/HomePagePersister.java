/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.persister;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.android.otaprovisioningclient.oma.Application;
import com.android.otaprovisioningclient.oma.Characteristic;
import com.android.otaprovisioningclient.oma.Parm;
import com.android.otaprovisioningclient.oma.WapProvisioningDoc;

public class HomePagePersister {
    public static final String AUTHORITY = "com.browser.homepage";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/homePage");

    public static boolean save(Context context, WapProvisioningDoc provisioningDocument) {
        Application browserApplication = provisioningDocument.applications
                .get(Application.BROWSER);

        ArrayList<Characteristic> charaList = provisioningDocument
                .getCharacteristic("APPLICATION");
        Characteristic browserApp = null;
        String homePage = null;
        ArrayList<Parm> tempList;
        if (null != charaList && !charaList.isEmpty()) {
            for (Characteristic curChara : charaList) {
                tempList = curChara.getParm("APPID");
                if ((tempList != null && tempList.size() > 0)
                        && tempList.get(0).value.equals(Application.BROWSER)) {
                    browserApp = curChara;
                    ArrayList<Characteristic> resources = curChara
                            .getInnerCharacteristic("RESOURCE");
                    if (null != resources && !resources.isEmpty()) {
                        Characteristic resChara = resources.get(0);
                        ArrayList<Parm> uriParm = resChara.getParm("URI");
                        if (null != uriParm && !uriParm.isEmpty()) {
                            homePage = uriParm.get(0).value;
                        }
                    }
                    if (null != homePage) {
                        break;
                    }

                    ArrayList<Parm> addrList = curChara.getParm("ADDR");
                    if (null != addrList && !addrList.isEmpty()) {
                        homePage = addrList.get(0).value;
                        break;
                    }

                }
            }
        }

        if (null == homePage) {
            ArrayList<Characteristic> pxLogicalList = provisioningDocument
                    .getCharacteristic("PXLOGICAL");
            if (null != pxLogicalList && !pxLogicalList.isEmpty()) {
                for (Characteristic pxLogical : pxLogicalList) {
                    ArrayList<Parm> startPage = pxLogical.getParm("STARTPAGE");
                    if (null != startPage && !startPage.isEmpty()) {
                        homePage = startPage.get(0).value;
                        break;
                    }
                }
            }
        }
        if (null != browserApp && null != homePage) {
            String url = homePage;
            Intent intent = new Intent(
                    "homepage.intent.android.homepagesetting");
            intent.putExtra("HOMEPAGEURL", url);
            context.sendBroadcast(intent);
            return true;
        } else {
            return false;
        }

    }
}
