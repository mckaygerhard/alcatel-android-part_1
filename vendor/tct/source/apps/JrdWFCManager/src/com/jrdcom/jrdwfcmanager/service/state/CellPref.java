package com.jrdcom.jrdwfcmanager.service.state;

import com.jrdcom.jrdwfcmanager.service.IComponentHolder;

public class CellPref extends _State {
    public CellPref(IComponentHolder holder) {
        super(holder);
    }

    @Override
    public void onStateActivated(EventData eData) {
        clearNotification();
        updateWFCSettingsStateInfo(CELL_PREF, null);
    }

    @Override
    public _State onAirplaneOn(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegistering(EventData eData) {
        _State state = getHighPriorityState(eData);
        if (state == null && isIgnoreCellPref()) {
            state = mHolder.getWFCStateProxy().obtainState(Registering.class);
        }
        return state;
    }

    @Override
    public _State onRegistered(EventData eData) {
        _State state = getHighPriorityState(eData);
        if (state == null && isIgnoreCellPref()) {
            state = mHolder.getWFCStateProxy().obtainState(Registered.class);
        }
        return state;
    }

    @Override
    public _State onRegerror(EventData eData) {
        _State state = getHighPriorityState(eData);
        if (state == null && isIgnoreCellPref()) {
            state = mHolder.getWFCStateProxy().obtainState(RegError.class);
        }
        return state;
    }

    @Override
    public _State onWifiPref(EventData eData) {
        return onWifiOnly(eData);
    }

    @Override
    public _State onWifiOnly(EventData eData) {
        return getNextState(eData, Registering.class);
    }
}
