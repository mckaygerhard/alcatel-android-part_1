package com.jrdcom.jrdwfcmanager.service.state;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.android.ims.ImsReasonInfo;
import com.jrdcom.jrdwfcmanager.R;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.service.FeatureUiHandler;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;
import com.jrdcom.jrdwfcmanager.service.WFCStateProxy;
import com.jrdcom.jrdwfcmanager.ui.DialogManagerActivity;
import com.jrdcom.jrdwfcmanager.ui.NotificationManager;

public abstract class _State implements WFCUtils, _IStateImpl {
    protected static final int DISABLED = 1;
    protected static final int ENABLING = 2;
    protected static final int ERROR_CODE = 3;
    protected static final int POOR_WIFI_SIGNAL = 4;
    protected static final int CELL_PREF = 5;
    protected static final int READY_FOR_CALLS = 6;
    protected static final int WIFI_OFF = 7;
    protected static final int AIRPLANE_ON = 8;
    protected static final int DISABLING = 9;
    protected static final int UNREGISTER_SERVER_REQUEST = 10;
    protected static final int INVALID_SIM = 11;
    protected static final int NOT_CONNECTED_TO_WIFI = 12;

    private static String[] mSettingsStatusStr;

    protected IComponentHolder mHolder;


    // _IStateImpl {
    @Override
    public _State onSystemLocaleChanged(_State.EventData eData) {
        mSettingsStatusStr = mHolder.getContext().getResources().getStringArray(R.array.wifi_calling_status);
        return this;
    }

    @Override
    public _State onCallDropAlert(EventData eData) {
        showPopup(WFCUtils.DIALOG_ROVEOUT, true);
        return null;
    }

    @Override
    public _State onOnCall(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onOffCall(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onWifiOn(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onWifiOff(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onWifiConnected(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onWifiDisconnected(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onPoorWifiSignal(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onAirplaneOn(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onAirplaneOff(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onRegistering(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onRegistered(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onRegerror(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onInvalidSim(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onEnabled(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onDisabled(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onWifiPref(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onWifiOnly(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onCellPref(EventData eData) {
        return getHighPriorityState(eData);
    }

    @Override
    public _State onNoCellEvents(EventData eData) {
        int what = 0;
        FeatureUiHandler.UiData data = new FeatureUiHandler.UiData();
        switch (eData.event) {
            case NO_CELL_DIAL_TIP:
                // what = UI_HANDLER_BIT_POPUP;
                // data.dialog = DialogManagerActivity.getDialogPack(WFCUtils.DIALOG_WFC_NO_CELL_DIAL);
                break;
            case NO_CELL_MSG_TIP:
                what = UI_HANDLER_BIT_POPUP;
                data.dialog = DialogManagerActivity.getDialogPack(WFCUtils.DIALOG_WFC_NO_CELL_SEND_MSG);
                break;
            case NO_CELL_DIALER_ENTERED_NOTIFY:
                what = UI_HANDLER_BIT_NOTIFICATION;
                data.notification = NotificationManager.getNotificationPack(WFCUtils.NOTIFICATION_NO_CELL_DIAL);
                break;
            case NO_CELL_MMS_ENTERED_NOTIFY:
                what = UI_HANDLER_BIT_NOTIFICATION;
                data.notification = NotificationManager.getNotificationPack(WFCUtils.NOTIFICATION_NO_CELL_SEND_MSG);
                break;
        }
        updateUI(what, data);
        return null;
    }
    // } _IStateImpl


    public _State(IComponentHolder holder) {
        mHolder = holder;
        if (mSettingsStatusStr == null) {
            mSettingsStatusStr = mHolder.getContext().getResources().getStringArray(R.array.wifi_calling_status);
        }
    }

    public void onStateActivated(EventData eData) {
    }

    public void onStateDeactivated(EventData eData) {
    }

    /**
     * For Notification
     */
    protected final void clearNotification() {
        FeatureUiHandler.UiData data = new FeatureUiHandler.UiData();
        data.notification = NotificationManager.getNotificationPack(WFCUtils.NOTIFICATION_DISSMISS);
        data.showWFCRightIcon = false;
        updateUI(UI_HANDLER_BIT_WFC_RIGHT_SIDE_ICON | UI_HANDLER_BIT_NOTIFICATION, data);
    }

    protected final void showNotification(Integer type) {
        this.showNotification(type, null, null);
    }

    protected final void showNotification(Integer type, Boolean rightIcon) {
        this.showNotification(type, rightIcon, null);
    }

    protected final void showNotification(Integer type, Boolean rightIcon, ImsReasonInfo info) {
        FeatureUiHandler.UiData data = new FeatureUiHandler.UiData();
        int what = 0;

        if (type != null) {
            data.notification = NotificationManager.getNotificationPack(type, info);
            what |= UI_HANDLER_BIT_NOTIFICATION;
        }

        if (rightIcon != null) {
            data.showWFCRightIcon = rightIcon;
            what |= UI_HANDLER_BIT_WFC_RIGHT_SIDE_ICON;
        }

        updateUI(what, data);
    }


    /**
     * For Popup
     */
    protected final void showPopup(Integer type) {
        showPopup(type, false);
    }

    protected final void showPopup(Integer type, boolean beep) {
        FeatureUiHandler.UiData data = new FeatureUiHandler.UiData();
        int what = 0;

        if (type != null) {
            data.dialog = DialogManagerActivity.getDialogPack(type);
            what |= UI_HANDLER_BIT_POPUP;
        }

        if (beep) {
            what |= UI_HANDLER_BIT_BEEP;
        }

        updateUI(what, data);
    }


    protected final void updateUI(int what, FeatureUiHandler.UiData data) {
        if (data == null) {
            Logger.loge("_State#updateUI return: UiData is null!");
            return;
        }

        if (data.notification == null) {
            what &= ~UI_HANDLER_BIT_NOTIFICATION;
        }

        if (data.dialog == null || data.dialog.type == DIALOG_SKIP) {
            what &= ~UI_HANDLER_BIT_POPUP;
        }

        if (what == 0) {
            Logger.logd("_State#updateUI return: Nothing to do.");
            return;
        }

        Handler uiHanlder = mHolder.getFeatureUiHandler();
        Message msg = uiHanlder.obtainMessage(what, data);
        uiHanlder.sendMessage(msg);
        Logger.logd("update UI on state: " + this + "  what: " + what);
    }

    /**
     * For Settings Status
     */
    protected final void updateWFCSettingsStateInfo(int type, String error) {
        String settingsStatus = null;
        // update settings status
        try {
            if (TextUtils.isEmpty(error)) {
                settingsStatus = mSettingsStatusStr[type];
            } else {
                settingsStatus = mSettingsStatusStr[type] + " " + error;
            }
        } catch (Exception e) {
            e.printStackTrace();
            settingsStatus = mSettingsStatusStr[ENABLING];
        }

        if (settingsStatus != null) {
            SystemRFStatus.setWFCSettingsState(settingsStatus);
        }
    }

    protected final _State getHighPriorityState(EventData eData) {
        return this.getNextState(eData, null);
    }

    /**
     * When change state form one to another, we have to check the system environment first.
     * In some cases, the objective state can't be reached.
     *
     * The priority of the states which represents special cases are:
     * 1. Disabled.class
     * 2. CellPref.class(not in airplane mode and not in low cell signal state)
     * 3. WifiOff.class
     * 4. WifiDisconnected.class
     * 5. InvalidSim.class
     *
     * @param eData fired event data
     * @param clazz key of the next state
     * @return
     */
    protected final _State getNextState(EventData eData, Class<? extends _State> clazz) {
        WFCStateProxy sHolder = mHolder.getWFCStateProxy();
        _State state = null;
        // check if wfc disabled
        if (!SystemRFStatus.isWFCEnabled()) {
            state = sHolder.obtainState(Disabled.class);
            SystemRFStatus.setWFCRegState(false);
        }

        // check if cell pref when not in airplane mode
        else if (!isIgnoreCellPref()) {
            state = sHolder.obtainState(CellPref.class);
            SystemRFStatus.setWFCRegState(false);
        }

        // check if wifi enabled
        else if (!SystemRFStatus.isWifiEnabled()) {
            state = sHolder.obtainState(WifiOff.class);
            SystemRFStatus.setWFCRegState(false);
        }

        // check if wifi connected
        else if (!SystemRFStatus.isWifiConnected()) {
            state = sHolder.obtainState(WifiDisconnected.class);
            SystemRFStatus.setWFCRegState(false);
        }

        // check if sim absent
        else if (!mHolder.getSystemRFStatus().isSimCardOK()) {
            eData.imsReasonInfo = ImsReasonInfoFactory.getER05Info();
            state = sHolder.obtainState(InvalidSim.class);
            SystemRFStatus.setWFCRegState(false);
        }

        // check if missing 911 address sim card
        else if (mHolder.getSystemRFStatus().isMissing911Address()) {
            eData.imsReasonInfo = ImsReasonInfoFactory.getMissing911AddressInfo();
            state = sHolder.obtainState(InvalidSim.class);
        }

        // get next state
        else if (clazz != null) {
            state = mHolder.getWFCStateProxy().obtainState(clazz);
        }

        return state;
    }

    protected final boolean isIgnoreCellPref() {
        boolean isCellPref = SystemRFStatus.getWFCPref() == WFCUtils.IMS_CELL_PREF;
        boolean isAirplaneModeOn = SystemRFStatus.isAirplaneModeOn();
        boolean isLowSignal = mHolder.getSystemRFStatus().isLowSignal();

        return !isCellPref || isAirplaneModeOn || isLowSignal;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode());
    }

    /**
     * inner class
     */
    public static class EventData {
        public WFCUtils.STATE_EVENT event;
        // The state which is handling this event.
        public _State eventHandleState;
        public ImsReasonInfo imsReasonInfo;
    }
}
