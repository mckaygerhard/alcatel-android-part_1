/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.ui;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.SimpleAdapter;
import com.jrdcom.jrdwfcmanager.R;
import com.jrdcom.jrdwfcmanager.common.ImplementorFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Questions extends ListActivity {
    private static final String TAG = "Questions";

    private static final String TAG_QA = "IMSQandA";
    private static final String TAG_QUESTION = "Question";
    private static final String TAG_ANSWER = "Answer";

    private String[] question;
    private String[] answer;

    private Questions.IImplementor mImplementor;

    public Questions() {
        mImplementor = (Questions.IImplementor) ImplementorFactory.getImplementor(this.getClass(), this);
        if (mImplementor == null) {
            throw new RuntimeException("IImplementor is null.");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mImplementor.setActionBar();
        question = mImplementor.getQuestions();
        answer = mImplementor.getAnswers();

        setListAdapter(new SimpleAdapter(this,
                getInformation(),
                R.layout.simple_list_item_2,
                new String[]{TAG_QUESTION, TAG_ANSWER},
                new int[]{android.R.id.text1, android.R.id.text2}));
    }

    private List<Map<String, Object>> getInformation() {
        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < question.length; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put(TAG_QUESTION, question[i]);
            map.put(TAG_ANSWER, answer[i]);
            listItems.add(map);
        }
        return listItems;
    }


    /**
     * inner classes
     */
    public interface IImplementor extends ImplementorFactory.IImplementor {
        String[] getQuestions();

        String[] getAnswers();

        void setActionBar();
    }
}