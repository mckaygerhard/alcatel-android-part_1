/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
/* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
import android.content.res.Configuration;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;

import com.android.ims.JrdIMSSettings;
/* MODIFIED-END by Yuan Yili,BUG-2419568*/
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.receiver.SystemReceiver;

public class WFCManagerService extends Service implements IComponentHolder, WFCUtils {
    private static final String THREAD_NAME_FEATURE_HANDLER = "process_handler";

    private FeatureUiHandler mFeatureUiHandler;

    private SystemRFStatus mSystemRFStatus;
    private FeatureProcessHandler mProcessHandler;
    private WFCStateProxy mWFCStateProxy;
    private IImsOperator mImsOperator;

    private CellPhoneStateListener mCellPhoneStateListener;
    private SystemReceiver mSystemReceiver;

    private WFCContentObserver mWFCContentobserver;

    @Override
    public void onCreate() {
        Logger.logd("onCreate");
        mFeatureUiHandler = new FeatureUiHandler(this);
        mSystemRFStatus = new SystemRFStatus();

        HandlerThread handlerThread = new HandlerThread(THREAD_NAME_FEATURE_HANDLER);
        handlerThread.start();
        mProcessHandler = new FeatureProcessHandler(handlerThread.getLooper(), this);
        mWFCStateProxy = new WFCStateProxy(this);

        mImsOperator = new ImsOperator(this);
        mSystemReceiver = new SystemReceiver(this);
        mCellPhoneStateListener = new CellPhoneStateListener(this);
        mWFCContentobserver = new WFCContentObserver(this);

        mProcessHandler.sendMessage(IMPL_MSG.FIRST_RUN_CHECK);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.logd("onStartCommand");
        mProcessHandler.sendMessageDelayed(IMPL_MSG.IMPL_CHECK_RADIO_POWER, 10000);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onApplicationDisabled(); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
    }

    // IComponentHolder {
    @Override
    public void onApplicationReady() {
        Logger.logd("onApplicationReady");
        mImsOperator.init();
        mSystemReceiver.registerReceiver();
        mCellPhoneStateListener.listen();
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        mCellPhoneStateListener.onIMSRegisterStateChanged();
        mWFCContentobserver.init();
    }

    public void onApplicationDisabled() {
        Logger.logd("onApplicationDisabled");
        mCellPhoneStateListener.unListen();
        mSystemReceiver.unregisterReceiver();
        mWFCContentobserver.onStop();
    }
    /* MODIFIED-END by Yuan Yili,BUG-2419568*/

    @Override
    public Context getContext() {
        return getBaseContext();
    }

    @Override
    public IImsOperator getImsOperator() {
        return mImsOperator;
    }

    @Override
    public SystemRFStatus getSystemRFStatus() {
        return mSystemRFStatus;
    }

    @Override
    public Handler getFeatureUiHandler() {
        return mFeatureUiHandler;
    }

    @Override
    public FeatureProcessHandler getProcessHandler() {
        return mProcessHandler;
    }

    @Override
    public WFCStateProxy getWFCStateProxy() {
        return mWFCStateProxy;
    }
    // } IComponentHolder
}
