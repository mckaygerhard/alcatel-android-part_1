/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.ui.tracfone;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import com.android.ims.JrdIMSSettings; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
import com.android.internal.app.AlertController;
import com.jrdcom.jrdwfcmanager.JrdWFCManager;
import com.jrdcom.jrdwfcmanager.R;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;

public class DialogManagerActivityImpl extends com.jrdcom.jrdwfcmanager.ui.tmo.DialogManagerActivityImpl {
    public DialogManagerActivityImpl(Object activity) {
        super(activity);
    }

    @Override
    public String getMessageBody(int type) {
        return mActivity.getResources().getStringArray(R.array.wfc_dialog_msg_tracfone)[type];
    }

    @Override
    public void onCreateDialog(AlertController.AlertParams p, int type) {
        switch (type) {
            case WFCUtils.DIALOG_E911:
                TextView e911_textview = (TextView) p.mView.findViewById(R.id.topmsg);
                e911_textview.setText(R.string.missing_e911_msg_tracfone);
                break;
            case WFCUtils.DIALOG_FIRST_WFC:
                CheckBox check = (CheckBox) p.mView.findViewById(R.id.checkbox);
                check.setVisibility(View.GONE);
                JrdIMSSettings.put(JrdWFCManager.getContext(), WFCUtils.CONTENT_FIRST_CALL_POPUP_SKIP, true); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
                break;
            default:
                super.onCreateDialog(p, type);
        }
    }
}
