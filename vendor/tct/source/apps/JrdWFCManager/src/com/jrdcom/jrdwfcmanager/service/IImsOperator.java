package com.jrdcom.jrdwfcmanager.service;

import android.os.Message;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;

public interface IImsOperator {
    void init();

    boolean isBusyFor(Message msg);

    void setWifiCallingPreference(WFCUtils.IMPL_MSG msg, int wifiCallingStatus, int wifiCallingPreference);
}
