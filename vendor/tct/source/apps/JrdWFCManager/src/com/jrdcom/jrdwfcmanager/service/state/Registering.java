package com.jrdcom.jrdwfcmanager.service.state;

import com.jrdcom.jrdwfcmanager.service.IComponentHolder;

public class Registering extends _State {
    public Registering(IComponentHolder holder) {
        super(holder);
    }

    @Override
    public void onStateActivated(EventData eData) {
        clearNotification();
        updateWFCSettingsStateInfo(ENABLING, null);
    }

    @Override
    public _State onPoorWifiSignal(EventData eData) {
        return getNextState(eData, PoorWifiSignal.class);
    }

    @Override
    public _State onRegistered(EventData eData) {
        return getNextState(eData, Registered.class);
    }

    @Override
    public _State onRegerror(EventData eData) {
        return getNextState(eData, RegError.class);
    }
}
