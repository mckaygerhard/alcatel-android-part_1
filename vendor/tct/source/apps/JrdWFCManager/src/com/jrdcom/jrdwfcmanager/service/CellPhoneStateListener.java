/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.PreciseCallState;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import com.android.ims.ImsReasonInfo;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;

public class CellPhoneStateListener extends PhoneStateListener {
    private static final int DELAY_SEND_CALL_ENDED = 1000;

    private IComponentHolder mHolder;
    private FeatureProcessHandler mProcessHandler;

    public CellPhoneStateListener(IComponentHolder holder) {
        mHolder = holder;
    }

    public void listen() {
        Context context = mHolder.getContext();
        mProcessHandler = mHolder.getProcessHandler();
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(this, PhoneStateListener.LISTEN_SERVICE_STATE |
                PhoneStateListener.LISTEN_PRECISE_CALL_STATE |
                PhoneStateListener.LISTEN_VOLTE_STATE |
                PhoneStateListener.LISTEN_SIGNAL_STRENGTHS |
                PhoneStateListener.LISTEN_IMS_REGISTER_STATE_CHANGE |
                PhoneStateListener.LISTEN_IMS_CALL_HANDOVER);
    }

    public void unListen() { // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
        ((TelephonyManager) mHolder.getContext().getSystemService(Context.TELEPHONY_SERVICE)).
                listen(this, PhoneStateListener.LISTEN_NONE);
    }

    @Override
    public void onPreciseCallStateChanged(PreciseCallState pcsCallState) {
        int callState = pcsCallState.getForegroundCallState();
        if (callState == PreciseCallState.PRECISE_CALL_STATE_IDLE) {
            callState = pcsCallState.getRingingCallState();
        }

        if (callState == PreciseCallState.PRECISE_CALL_STATE_IDLE) {
            callState = pcsCallState.getBackgroundCallState();
        }

        FeatureProcessHandler pHandler = mHolder.getProcessHandler();
        switch (callState) {
            case PreciseCallState.PRECISE_CALL_STATE_IDLE:
            case PreciseCallState.PRECISE_CALL_STATE_DISCONNECTED:
                pHandler.sendMessageDelayed(WFCUtils.IMPL_MSG.IMPL_CALL_ENDED, DELAY_SEND_CALL_ENDED);
                break;
            case PreciseCallState.PRECISE_CALL_STATE_ACTIVE:
                pHandler.sendMessage(WFCUtils.IMPL_MSG.IMPL_CHECK_CALL_ACTIVATED);
            case PreciseCallState.PRECISE_CALL_STATE_HOLDING:
            case PreciseCallState.PRECISE_CALL_STATE_DIALING:
            case PreciseCallState.PRECISE_CALL_STATE_ALERTING:
            case PreciseCallState.PRECISE_CALL_STATE_INCOMING:
            case PreciseCallState.PRECISE_CALL_STATE_WAITING:
                pHandler.removeMessages(WFCUtils.IMPL_MSG.IMPL_CALL_ENDED);
                pHandler.sendMessage(WFCUtils.IMPL_MSG.IMPL_CALL_STARTED);
                break;
        }
    }

    @Override
    public void onServiceStateChanged(ServiceState serviceState) {
        mProcessHandler.sendMessage(WFCUtils.IMPL_MSG.CELL_STATE_UPDATE, serviceState);
    }

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        mProcessHandler.sendMessage(WFCUtils.IMPL_MSG.CELL_STATE_UPDATE, signalStrength);
    }

    public void onIMSRegisterStateChanged() {
        boolean isWFCRegistered = SystemRFStatus.isWFCRegistered(); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
        FeatureProcessHandler.WFCStatusData wfcData = new FeatureProcessHandler.WFCStatusData();
        if (isWFCRegistered) {
            mHolder.getProcessHandler().sendMessage(WFCUtils.IMPL_MSG.IMSOP_REG_REGISTERED, wfcData);
        } else {
            mHolder.getProcessHandler().sendMessage(WFCUtils.IMPL_MSG.IMSOP_REG_REGISTERING, wfcData);
        }
    }

    public void onIMSCallHandover(boolean isHandoverSuccess, int srcAccessTech, int targetAccessTech, ImsReasonInfo reasonInfo) {
        final int COMPLETE_SUCCESS = 1;

        boolean isHandOverFail_01 = reasonInfo.getCode() != COMPLETE_SUCCESS;

        boolean isHandOverFail_02 = reasonInfo.getCode() == 4
                && reasonInfo.getExtraCode() == 4
                && "No Available qualified mobile network".equals(reasonInfo.getExtraMessage());

        if (isHandOverFail_01 || isHandOverFail_02) {
            mHolder.getProcessHandler().sendMessage(WFCUtils.IMPL_MSG.IMPL_IMSOP_HO_FAIL);
        } else {
            mHolder.getProcessHandler().sendMessage(WFCUtils.IMPL_MSG.IMPL_IMSOP_HO_SUCCESS);
        }
    }
}
