/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.content.Context;
import android.os.Message;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.service.FeatureProcessHandler;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;


public class WFCRegEvent extends _FeatureImplementor {

    public WFCRegEvent(IComponentHolder h) {
        super(h);
    }

    @Override
    protected boolean eatMsg(IMPL_MSG what) {
        return what == IMPL_MSG.IMSOP_REG_REGISTERING
                || what == IMPL_MSG.IMSOP_REG_REGISTERED
                || what == IMPL_MSG.IMSOP_REG_ERROR;
    }

    @Override
    protected boolean handleMessage(IMPL_MSG what, Message msg) {
        if (msg.obj == null || !(msg.obj instanceof FeatureProcessHandler.WFCStatusData)) {
            Logger.logd("WFCStatusData@handleMessage, abnormal msg.obj: " + msg.obj);
            return true;
        }

        FeatureProcessHandler.WFCStatusData data = (FeatureProcessHandler.WFCStatusData) msg.obj;
        SystemRFStatus rfStatus = mHolder.getSystemRFStatus();
        Context context = mHolder.getContext();

        // check if ui need to be cleared.
        switch (what) {
            case IMSOP_REG_REGISTERING:
                notifyStateEvent(STATE_EVENT.REGISTERING);
                break;
            case IMSOP_REG_REGISTERED:
                if (rfStatus.isMissing911Address()) {
                    rfStatus.setIsMissing911Address(false);
                    rfStatus.set911PopupDisabled(false);
                }
                notifyStateEvent(STATE_EVENT.REGISTERED);
                break;
            case IMSOP_REG_ERROR:
                if (data.imsReasonInfo.getExtraMessage().toLowerCase().contains("invalid sim card")
                        && rfStatus.isSimCheckingFrezzed()) {
                    Logger.logd("WFCRegEvent, frezzed for checking sim state.");
                } else {
                    if (data.imsReasonInfo.getExtraMessage().toLowerCase().contains("missing 911 address")) {
                        rfStatus.setIsMissing911Address(true);
                    } else if (data.imsReasonInfo.getExtraMessage().toLowerCase().contains("unable to connect")) {
                        data.imsReasonInfo = WFCUtils.ImsReasonInfoFactory.getREG99Info();
                    }

                    notifyStateEvent(STATE_EVENT.REGERROR, data.imsReasonInfo);
                }
                break;
        }

        if (SystemRFStatus.isWFCEnabled()
                && SystemRFStatus.isWifiEnabled()
                && SystemRFStatus.isWifiConnected()) {
            SystemRFStatus.setWFCRegState(what == IMPL_MSG.IMSOP_REG_REGISTERED);
            mHolder.getProcessHandler().sendMessage(IMPL_MSG.IMPL_CHECK_REGED_ON_CALL);
        } else {
            SystemRFStatus.setWFCRegState(false);
        }

        return true;
    }
}