package com.jrdcom.jrdwfcmanager.service.state;

import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.service.FeatureUiHandler;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;
import com.jrdcom.jrdwfcmanager.ui.DialogManagerActivity;
import com.jrdcom.jrdwfcmanager.ui.NotificationManager;

public class Registered extends _State {
    public Registered(IComponentHolder holder) {
        super(holder);
    }


    @Override
    public void onStateActivated(EventData eData) {
        if (eData.eventHandleState.getClass() == OnCall.class) {
            int what = UI_HANDLER_BIT_NOTIFICATION;
            FeatureUiHandler.UiData uiData = new FeatureUiHandler.UiData();

            uiData.notification = NotificationManager.getNotificationPack(NOTIFICATION_CALL_END);
            if (SystemRFStatus.isWFCRegistered()) {
                what |= UI_HANDLER_BIT_POPUP;
                uiData.dialog = DialogManagerActivity.getDialogPack(WFCUtils.DIALOG_FIRST_WFC);
            }

            updateUI(what, uiData);
        } else {
            int what = UI_HANDLER_BIT_NOTIFICATION | UI_HANDLER_BIT_POPUP;
            FeatureUiHandler.UiData uiData = new FeatureUiHandler.UiData();

            uiData.notification = NotificationManager.getNotificationPack(NOTIFICATION_REGISTERED);

            DialogManagerActivity.DialogPack pack = new DialogManagerActivity.DialogPack();
            pack.type = DIALOG_OOBE_WIFI_CONNECTED;
            pack.apSSID = SystemRFStatus.getAPSSID();
            uiData.dialog = DialogManagerActivity.getDialogPack(pack);

            updateUI(what, uiData);
        }

        updateWFCSettingsStateInfo(READY_FOR_CALLS, null);
    }

    @Override
    public _State onOnCall(EventData eData) {
        return getNextState(eData, OnCall.class);
    }

    @Override
    public _State onRegistering(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegerror(EventData eData) {
        return getNextState(eData, RegError.class);
    }
}
