/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:05/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2014 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun Liu                                                       */
/* E-Mail:  shishun.liu@tcl.com                                               */
/* Role  :  MMITest                                                           */
/* Reference documents :  TCL NB NPI MMI TEST SPEC_V1.5.pdf                   */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : /development/apps/MMITest/src/com/android/mmi/util/              */
/*                                                             JRDClient.java */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 05/08/14| Shishun.Liu    |                    | Porting for MMITest        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
package com.jrdcom.jrdwfcmanager.modem;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.SystemClock;

import com.jrdcom.jrdwfcmanager.common.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

public class JRDClient {
    private static final String TAG = "MMITest.JRDClient";

    //public static final String TRACABILITY_SOCKET_NAME = "traceability_proxy";
    public static final String TRACABILITY_SOCKET_NAME = "tctd";
    //public static final String MMI_SOCKET_NAME = "mmi_proxy";
    // define MMI test commands
    //public static final int MMICMD_SDRW = 0x7800;
    //public static final int MMICMD_BL_FLICKER = 0x7801;
    public static final byte CMD_STOPCHARGE = 'z';
    public static final byte CMD_RESTARTCHARGE = 'y';

    // define reply values
    private final int MMIREP_OK = 0x6600;
    private final int MMIREP_UNKNOWN = 0x6601;
    private final int MMIREP_ERROR = 0x6602;

    private static final int DEVICEINFO_DATA_SIZE = 600;//545;
    private InputStream mIn;
    private OutputStream mOut;
    private LocalSocket mSocket;
    private byte cmd_buf[] = new byte[4];
    private byte reply_buf[] = new byte[4];

    public static byte deviceinfo_data[] = new byte[DEVICEINFO_DATA_SIZE]; //see tracability_region_struct_t in deviceinfo.h
    public static boolean isSync = false;

    public JRDClient() {
        //mSocket = new LocalSocket();
//        if (android.os.SystemProperties.get("init.svc.tctd") != "running") {
//            android.os.SystemProperties.set("ctl.start", "tctd");
//            SystemClock.sleep(500);
//        }
    }

    protected boolean connect(String socketName) {
        mSocket = new LocalSocket();
        if (mSocket == null) {
            return false;
        }
        Logger.logd("connecting...");
        try {
            LocalSocketAddress address = new LocalSocketAddress(
                    socketName, LocalSocketAddress.Namespace.RESERVED);

            mSocket.connect(address);

            mIn = mSocket.getInputStream();
            mOut = mSocket.getOutputStream();
        } catch (IOException ex) {
            disconnect();
            return false;
        }
        return true;
    }

    protected void disconnect() {
        Logger.logd("disconnecting...");
        try {
            if (mSocket != null)
                mSocket.close();
        } catch (IOException ex) {
        }
        try {
            if (mIn != null)
                mIn.close();
        } catch (IOException ex) {
        }
        try {
            if (mOut != null)
                mOut.close();
        } catch (IOException ex) {
        }
        mSocket = null;
        mIn = null;
        mOut = null;
    }

    protected boolean readReply() {
        int count;
        try {
            count = mIn.read(reply_buf, 0, 4);
            if (count <= 0) {
                Logger.loge("read error " + count);
            }
        } catch (IOException ex) {
            Logger.loge("read exception");
            return false;
        }
        int reply = (((int) reply_buf[0]) & 0xff)
                | ((((int) reply_buf[1]) & 0xff) << 8);
        Logger.logd("Reply is 0x" + Integer.toHexString(reply));
        if (reply == MMIREP_OK)
            return true;
        else if (reply == MMIREP_ERROR)
            return false;
        else if (reply == MMIREP_UNKNOWN)
            return false;
        else
            return false;
    }

    public boolean forceSyncTraceability() {
        isSync = false;
        return readTraceability();
    }

    public boolean readTraceability() {
        int count;

        if (!isSync) {
            Logger.logd("isSync is false");
            //byte read[] = new byte[1];
            //read[0] = 'r';
            String read = "r";

            if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
                Logger.loge("Connecting tracability proxy fail!");
            } else if (!sendCommand(read.getBytes())) {
                Logger.loge("Send command to tracability proxy fail!");
            } else {
                try {
                    count = mIn.read(deviceinfo_data, 0, 8);
                    if (count == 8) {
                        count = mIn.read(deviceinfo_data, 0, deviceinfo_data.length);
                        Logger.logd("count== " + count);

                        if (count == deviceinfo_data.length) {
                            Logger.loge("read traceability successfully");
                            isSync = true;
                        }
                    }
                } catch (IOException ex) {
                    Logger.loge("read exception");
                }
            }

            disconnect();
        }
//        String str = "";
//        for (int i=0; i<deviceinfo_data.length;i++) {
//            str += deviceinfo_data[i];
//        }
//        System.out.println("deviceinfo_data = "+str);
        Logger.logd("traceability sync = " + isSync);
        return isSync;
    }

    public void write_calibration() {
        byte command[] = new byte[1];
        command[0] = 'k';
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(command)) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            Logger.loge("Send k command successfully!");
        }
        disconnect();
    }

    public void try_to_kill_rawdata() {
        byte command[] = new byte[1];
        command[0] = 'm';
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(command)) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            Logger.loge("Send k command successfully!");
        }
        disconnect();
    }

    public void mic_start() {
        String cmd = "aa";
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(cmd.getBytes())) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            Logger.loge("Send mic command successfully!");
        }
        disconnect();
    }

    public void main_end() {
        String cmd = "ab";
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(cmd.getBytes())) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            Logger.loge("Send mic end command successfully!");
        }
        disconnect();
    }

    public void sec_start() {
        String cmd = "ac";
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(cmd.getBytes())) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            Logger.loge("Send sec command successfully!");
        }
        disconnect();
    }

    public void sec_end() {
        String cmd = "ad";
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(cmd.getBytes())) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            Logger.loge("Send mic endcommand successfully!");
        }
        disconnect();
    }

    public boolean spa_start() {
        boolean isSync_spa = false;
        byte spa[] = new byte[1];
        byte result[] = new byte[4];
        int count = 0;
        spa[0] = '*';
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(spa)) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            Logger.loge("Send mic end command successfully!");
        }
        try {
            count = mIn.read(result, 0, 4);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Logger.loge("SPA_START read exception");
        }
        if (count > 0) {
            isSync_spa = true;
        }
        disconnect();
        return isSync_spa;
    }

    public boolean spb_start() {
        boolean isSync_spb = false;
        byte spb[] = new byte[1];
        byte result[] = new byte[4];
        int count = 0;
        spb[0] = '$';
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(spb)) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            Logger.loge("Send mic end command successfully!");
        }
        try {
            count = mIn.read(result, 0, 4);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Logger.loge("SpeakBOXTest read exception");
        }
        if (count > 0) {
            isSync_spb = true;
        }
        disconnect();
        return isSync_spb;
    }

    public byte[] commandCmd(byte[] cmd) {
        byte[] result = new byte[128];
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(cmd)) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            try {
                mIn.read(result, 0, result.length);
            } catch (IOException ex) {
                Logger.loge("read exception");
            }
        }

        disconnect();
        return result;
    }

    public void writeTraceability(int offset, byte[] data) {
        isSync = false;

        try {
            System.arraycopy(data, 0, deviceinfo_data, offset, data.length);
        } catch (Exception e) {
            Logger.loge("can't copy " + data.length + "bytes to tracability_data[" + offset + "--" + data.length + "]");
            return;
        }

        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
            return;
        }

        String cmdPrex = "w";
        String cmdAck = "200 0 Traceability operation succeeded";
        byte cmd[] = new byte[DEVICEINFO_DATA_SIZE + cmdPrex.length()]; //1 byte used for 'w'
        //cmd[0] = 'w';
        System.arraycopy(cmdPrex.getBytes(), 0, cmd, 0, cmdPrex.length());
        System.arraycopy(deviceinfo_data, 0, cmd, cmdPrex.length(), deviceinfo_data.length);
        if (!sendCommand(cmd)) {
            Logger.loge("Send command to tracability proxy fail!");
        }
        try {
            int count = 0;
            //the expected reply from C layer is "OK" or "ERROR"
            byte[] reply = new byte[cmdAck.length()];
            count = mIn.read(reply, 0, cmdAck.length());
            Logger.logd("count== " + count);

            if (count > 0) {
                if (0 == (cmdAck.compareTo(new String(reply)))) {
                    Logger.loge("write traceability data successfully");
                    //} else if(0 == ("ERROR".compareTo(new String(reply)))) {
                    //    Logger.loge(TAG,"write traceability data unsuccessfully");
                } else {
                    Logger.loge("write traceability data successfully\nreply is: " + new String(reply));
                }
            }
        } catch (IOException ex) {
            Logger.loge("read exception");
        }

        disconnect();
    }


    public byte[] readNv(int item_id) {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        byte result[] = new byte[128];
        byte read[];
        String cmd = String.format(Locale.ENGLISH, "f%d", item_id);
        read = cmd.getBytes();

        byteArray.reset();
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(read)) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            try {
                int read_len;
                int total_read_len = 0;
                do {
                    read_len = mIn.read(result, 0, 128);
                    Logger.loge("read_len = " + read_len);
                    // first 8 bytes is header
                    if (total_read_len + read_len < 8) {
                        // do not save byte to array
                    } else if (total_read_len < 8) {
                        byteArray.write(result, 8 - total_read_len, read_len - (8 - total_read_len));
                    } else {
                        byteArray.write(result, 0, read_len);
                    }
                    total_read_len += read_len;
                } while (read_len == 128);
            } catch (Exception ex) {
                Logger.loge("read exception");
            }
        }

        disconnect();
        return byteArray.toByteArray();
    }

    public byte[] readHdcpInitStatus() {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        byte result[] = new byte[16];
        byte initCmd[] = {'h'};

        byteArray.reset();
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(initCmd)) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            try {
                int read_len;
                read_len = mIn.read(result, 0, 12);
                Logger.loge("read_len = " + read_len);
                // first 8 bytes is header
                if (read_len < 12) {
                    // do not save byte to array
                } else {
                    byteArray.write(result, 8, 4);
                }
            } catch (Exception ex) {
                Logger.loge("read exception");
            }
        }

        disconnect();
        return byteArray.toByteArray();
    }

    public byte[] readEfuseStatus() {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        byte result[] = new byte[16];
        byte initCmd[] = {'e'};

        byteArray.reset();
        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(initCmd)) {
            Logger.loge("Send command to tracability proxy fail!");
        } else {
            try {
                int read_len;
                read_len = mIn.read(result, 0, 12);
                Logger.loge("read_len = " + read_len);
                // first 8 bytes is header
                if (read_len < 12) {
                    // do not save byte to array
                } else {
                    byteArray.write(result, 8, 4);
                }
            } catch (Exception ex) {
                Logger.loge("read exception");
            }
        }

        disconnect();
        return byteArray.toByteArray();
    }

    public void commandCharge(byte flag) {
        byte command[] = new byte[1];
        command[0] = flag;

        if (!connect(JRDClient.TRACABILITY_SOCKET_NAME)) {
            Logger.loge("Connecting tracability proxy fail!");
        } else if (!sendCommand(command)) {
            Logger.loge("Send command to tracability proxy fail!");
        }

        disconnect();
    }


    protected boolean sendCommand(int cmd) {

        cmd_buf[0] = (byte) (cmd & 0xff);
        cmd_buf[1] = (byte) ((cmd >> 8) & 0xff);
        cmd_buf[2] = (byte) ((cmd >> 16) & 0xff);
        cmd_buf[3] = (byte) ((cmd >> 24) & 0xff);

        try {
            if (mOut != null) {
                mOut.write(cmd_buf, 0, 4);
            }
        } catch (IOException ex) {
            Logger.loge("write error");
            return false;
        }
        return true;
    }

    protected boolean sendCommand(byte[] cmd) {
        String prefixCmd = "0 traceability ";
        byte fullCmd[] = new byte[prefixCmd.length() + cmd.length];
        System.arraycopy(prefixCmd.getBytes(), 0, fullCmd, 0, prefixCmd.length());
        System.arraycopy(cmd, 0, fullCmd, prefixCmd.length(), cmd.length);
        try {
            if (mOut != null) {
                mOut.write(fullCmd, 0, fullCmd.length);
            }
        } catch (IOException ex) {
            Logger.loge("write error");
            return false;
        }
        return true;
    }
}
