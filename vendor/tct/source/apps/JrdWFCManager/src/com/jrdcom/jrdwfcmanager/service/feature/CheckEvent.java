/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.content.Context;
import android.os.Message;
import android.telephony.TelephonyManager;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import static com.jrdcom.jrdwfcmanager.common.WFCUtils.IMPL_MSG.IMPL_CHECK_IMS_OPERATOR;
import static com.jrdcom.jrdwfcmanager.common.WFCUtils.IMPL_MSG.IMPL_CHECK_RADIO_POWER;
import static com.jrdcom.jrdwfcmanager.common.WFCUtils.IMPL_MSG.IMPL_CHECK_REGED_ON_CALL;
import static com.jrdcom.jrdwfcmanager.common.WFCUtils.IMPL_MSG.IMPL_CHECK_WIFI_ON_DIALOG;
import com.jrdcom.jrdwfcmanager.service.FeatureProcessHandler;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;


public class CheckEvent extends _FeatureImplementor {

    public CheckEvent(IComponentHolder h) {
        super(h);
    }

    @Override
    protected boolean eatMsg(IMPL_MSG what) {
        return what == IMPL_CHECK_REGED_ON_CALL
                || what == IMPL_CHECK_RADIO_POWER
                || what == IMPL_CHECK_IMS_OPERATOR
                || what == IMPL_CHECK_WIFI_ON_DIALOG
                ;
    }

    @Override
    protected boolean handleMessage(IMPL_MSG what, Message msg) {
        FeatureProcessHandler pHandler = mHolder.getProcessHandler();
        TelephonyManager telephonyManager = (TelephonyManager) mHolder.getContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        switch (what) {
            case IMPL_CHECK_REGED_ON_CALL:
                int state = telephonyManager.getCallState();
                Logger.logd("Check call state when registered, call state: " + state);
                switch (state) {
                    case TelephonyManager.CALL_STATE_RINGING:
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        pHandler.sendMessage(WFCUtils.IMPL_MSG.IMPL_CALL_STARTED);
                        break;
                }
                break;
            case IMPL_CHECK_RADIO_POWER:
                if (SystemRFStatus.isAirplaneModeOn()) {
                    Logger.logd("IMPL_CHECK_RADIO_POWER skipped, airplane mode is on.");
                    break;
                }

                boolean isWFCEnabled = SystemRFStatus.isWFCEnabled();
                boolean isRadioOn = SystemRFStatus.getWFCPref() != IMS_WIFI_ONLY;
                if (!isWFCEnabled) {
                    isRadioOn = true;
                }

                Logger.logd("IMPL_CHECK_RADIO_POWER, isRadioOn: " + isRadioOn);
                telephonyManager.setRadioPower(isRadioOn && !SystemRFStatus.isAirplaneModeOn());
                break;
            case IMPL_CHECK_IMS_OPERATOR:
                pHandler.removeMessages(IMPL_CHECK_IMS_OPERATOR);
                mHolder.getImsOperator().init();
                break;
            case IMPL_CHECK_WIFI_ON_DIALOG:
                if (SystemRFStatus.isWifiEnabled()) {
                    notifyStateEvent(STATE_EVENT.WIFI_ON);
                }
                break;
        }
        return true;
    }
}