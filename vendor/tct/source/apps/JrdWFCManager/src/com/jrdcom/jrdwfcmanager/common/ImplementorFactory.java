/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.common;

import com.jrdcom.jrdwfcmanager.JrdWFCManager;
import java.lang.reflect.Constructor;


/**
 * This is a mini bridge pattern, it use java reflection to create implementor instance.
 * You can call getImplementor() function to get the corresponding implementor with
 * a parameter which is the caller itself.
 */
public class ImplementorFactory {
    private static final char IMPL_CLASS_DOT = '.';
    private static final String IMPL_CLASS_SUFFIX = "Impl";

    public static IImplementor getImplementor(Class<?> caller, Object... arg) {
        IImplementor impl = null;
        StringBuilder className = new StringBuilder();
        className.append(caller.getPackage().getName());
        className.append(IMPL_CLASS_DOT);
        className.append(JrdWFCManager.getCarrierName());
        className.append(IMPL_CLASS_DOT);
        className.append(caller.getSimpleName());
        className.append(IMPL_CLASS_SUFFIX);

        try {
            Class<?> clazz = Class.forName(className.toString());
            if (arg != null && arg.length > 0) {
                Constructor constructor = clazz.getConstructor(Object.class);
                impl = (IImplementor) constructor.newInstance(arg[0]);
            } else {
                impl = (IImplementor) clazz.newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return impl;
    }


    public interface IImplementor {
    }
}