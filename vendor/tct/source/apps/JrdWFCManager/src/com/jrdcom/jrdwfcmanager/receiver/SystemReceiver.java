/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.receiver;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.android.ims.ImsManager;
import com.android.ims.ImsReasonInfo;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.imsphone.ImsPhone;
/* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
import com.jrdcom.jrdwfcmanager.JrdWFCManager;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.modem.JRDClient;
import com.jrdcom.jrdwfcmanager.service.FeatureProcessHandler;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;

import java.util.Arrays;
/* MODIFIED-END by Yuan Yili,BUG-2419568*/

public class SystemReceiver extends BroadcastReceiver {
    private static final String TEST_ACTION = "com.jrdcom.jrdwfcmanager.test.IWFCListener";

    boolean mIsRegistered;
    private IComponentHolder mHolder;

    public SystemReceiver(IComponentHolder holder) {
        mHolder = holder;
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR_MR1)
    public void registerReceiver() {
        if (!mIsRegistered) {
            IntentFilter intentfilter = new IntentFilter();
            intentfilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
            intentfilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            intentfilter.addAction(WifiManager.RSSI_CHANGED_ACTION);
            intentfilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            intentfilter.addAction(TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED);
            intentfilter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
            intentfilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            intentfilter.addAction(Intent.ACTION_LOCALE_CHANGED);
            intentfilter.addAction(Intent.ACTION_NEW_OUTGOING_CALL);

            intentfilter.addAction(ImsManager.ACTION_IMS_REGISTRATION_ERROR);
            intentfilter.addAction(TEST_ACTION);
            mHolder.getContext().registerReceiver(this, intentfilter);
            mIsRegistered = true;
        }
    }

    public void unregisterReceiver() {
        if (mIsRegistered) {
            mHolder.getContext().unregisterReceiver(this);
            mIsRegistered = false;
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        FeatureProcessHandler.WifiStateData data = new FeatureProcessHandler.WifiStateData();
        String action = intent.getAction();
        FeatureProcessHandler pHandler = mHolder.getProcessHandler();
        /**
         * wifi manager
         */
        if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
            data.type = FeatureProcessHandler.WifiStateData.TYPE_WIFI_STATE;
            data.wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_DISABLED);
            pHandler.sendMessage(WFCUtils.IMPL_MSG.WIFI_STATE_UPDATE, data);
        } else if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {
            // do nothing
        } else if (WifiManager.RSSI_CHANGED_ACTION.equals(action)) {
            if (!SystemRFStatus.isWFCRegistered()) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo info = wifiManager.getConnectionInfo();
                if (info != null && info.getBSSID() != null) {
                    data.type = FeatureProcessHandler.WifiStateData.TYPE_WIFI_RSSI;
                    data.rssi = info.getRssi();
                    pHandler.sendMessage(WFCUtils.IMPL_MSG.WIFI_STATE_UPDATE, data);
                }
            }
        }

        /**
         * call event
         */
        else if (ImsManager.ACTION_IMS_REGISTRATION_ERROR.equals(action)) {
            FeatureProcessHandler.WFCStatusData wfcData = new FeatureProcessHandler.WFCStatusData();
            String error = intent.getStringExtra("notificationMessage");
            if (!TextUtils.isEmpty(error)) {
                wfcData.imsReasonInfo = new ImsReasonInfo(0, 0, error);
                mHolder.getProcessHandler().sendMessage(WFCUtils.IMPL_MSG.IMSOP_REG_ERROR, wfcData);
            } else {
                Logger.loge("onReceive, ACTION_IMS_REGISTRATION_ERROR error message is empty!");
            }
        }

        /**
         * call event
         */
        else if (Intent.ACTION_NEW_OUTGOING_CALL.equals(action)) {
            String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            if (PhoneNumberUtils.isEmergencyNumber(phoneNumber)) {
                pHandler.sendMessage(WFCUtils.IMPL_MSG.IMPL_CALL_EMERGENCY);
            }
        }

        /**
         * connectivity manager
         */
        else if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
            ConnectivityManager connectMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifiNetInfo = connectMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (wifiNetInfo != null) {
                data.type = FeatureProcessHandler.WifiStateData.TYPE_WIFI_CONNECTIVITY;
                data.isWifiConnected = wifiNetInfo.isConnected();
                pHandler.sendMessage(WFCUtils.IMPL_MSG.WIFI_STATE_UPDATE, data);
            }
        }

        /**
         * telephony intents
         */
        else if (TelephonyIntents.ACTION_ANY_DATA_CONNECTION_STATE_CHANGED.equals(action)) {
            // do nothing
        } else if (TelephonyIntents.ACTION_SIM_STATE_CHANGED.equals(action)) {
            String simState = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);
            pHandler.sendMessage(WFCUtils.IMPL_MSG.IMPL_SIM_STATE_UPDATE, simState);
        }

        /**
         * system event
         */
        else if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action)) {
            pHandler.sendMessage(WFCUtils.IMPL_MSG.AIRPLANE_MODE_UPDATE, data);
            pHandler.sendMessage(WFCUtils.IMPL_MSG.IMPL_CHECK_RADIO_POWER);
        } else if (Intent.ACTION_LOCALE_CHANGED.equals(action)) {
            pHandler.sendMessage(WFCUtils.IMPL_MSG.IMPL_LOCALE_CHANGED);
        }

        /**
         * test event from adb command, eg.
         * adb shell am broadcast -a com.jrdcom.jrdwfcmanager.test.IWFCListener -e event 1 -e value 3 -e info hello
         */
        else if (TEST_ACTION.equals(action)) {
            try {

                /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
                JrdWFCManager.getRunningActivityName();

                JRDClient client = new JRDClient();
                TelephonyManager telephonyManager = (TelephonyManager) mHolder.getContext()
                        .getSystemService(Context.TELEPHONY_SERVICE);
                Logger.logd("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                Logger.logd("MSISDN: " + telephonyManager.getMsisdn());
                Logger.logd("readNV----: " + Arrays.toString(client.readNv(71)));
                /* MODIFIED-END by Yuan Yili,BUG-2419568*/
                Logger.logd(mHolder.getSystemRFStatus().toString());
                Logger.logd("getIsimIst: " + telephonyManager.getIsimIst());
                Logger.logd("getSimSerialNumber: " + telephonyManager.getSimSerialNumber());
                Logger.logd("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

                int event = Integer.valueOf(intent.getStringExtra("event"));
                int value = Integer.valueOf(intent.getStringExtra("value"));
                String info = intent.getStringExtra("info");
                Logger.loge("onReceive()," +
                        "  event: " + event +
                        "  value: " + value +
                        "  info: " + info);
                switch (event) {
                    case 0:
                        mHolder.getFeatureUiHandler().sendEmptyMessage(WFCUtils.UI_HANDLER_BIT_BEEP);
                }
            } catch (Exception e) {
                Logger.logd("********************************************");
                Logger.loge(e.toString());
                e.printStackTrace();
                Logger.logd("********************************************");
            }
        }
    }


}
