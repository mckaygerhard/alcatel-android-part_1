/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.common;

import com.android.ims.ImsConfig; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
import com.android.ims.ImsReasonInfo;

public interface WFCUtils {
    /**
     * Global int
     */
    int INVALID = Integer.MAX_VALUE;
    String FLAG_MINI_VERSION = "ro.cumini.flag";

    /**
     * Global strings
     */
    /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
    String SYSTEM_PROPERTY_PLATFORM_VERSION = "ro.tct.sys.ver";

    String ACTION_STATUS_BAR_ICON = "com.jrdcom.jrdwfcmanager.WFC_REGISTERED";
    String SYSTEM_PROPERTIES_WFC_REGISTERED = "wfc.registered";

    /**
     * For content provider
     * check values with adb command:
     *      adb shell cat /data/data/com.jrdcom.jrdwfcmanager/shared_prefs/wfc_settings_preferences.xml
     */
    String CONTENT_DEFAULT_PROFILE_SET = "bool.default.profile.set";
    String CONTENT_FIRST_TURN_ON_WIFI_POPUP_DONE = "bool.first.turn.on.wifi.popup.done";
    String CONTENT_FIRST_REGISTERED_POPUP_DONE = "bool.first.registered.popup.done";
    String CONTENT_FIRST_CALL_POPUP_SKIP = "bool.first.call.popup.skip";
    String CONTENT_WIFI_CONNTECTED_TIP_SKIP = "bool.wifi.conntected.tip.skip";
    String CONTENT_SIM_CARD_PLMN = "str.sim.card.plmn";
    /* MODIFIED-END by Yuan Yili,BUG-2419568*/

    String CONTENT_IS_WFC_REGISTERED = "@isRegistered";
    String CONTENT_IS_LOW_SIGNAL = "@isLowSignal";
    String CONTENT_SETTINGS_STATE = "@wfcSettingsState";

    String CONTENT_NOTIFY_LOW_SIGNAL_SEND_MSG = "__notifyLowSignalSendMsg";
    String CONTENT_NOTIFY_DIALER_ENTERED = "__notifyDialerEntered";
    String CONTENT_NOTIFY_MMS_ENTERED = "__notifyMmsEntered";

    /**
     * This is used in FeatureProcessHandler.java
     */
    enum IMPL_MSG {
        WFC_ENABLED_CHANGED,
        WFC_PREFERENCE_CHANGED,
        IMPL_LOCALE_CHANGED,

        FIRST_RUN_CHECK,
        __FIRST_RUN_CHECK,
        CELL_STATE_UPDATE,
        IMPL_SIM_STATE_UPDATE,
        IMPL_SIM_STATE_UPDATE__,
        WIFI_STATE_UPDATE,
        AIRPLANE_MODE_UPDATE,

        DIALOG_NO_CELL_DIAL_TIPS,
        DIALOG_NO_CELL_SEND_MSG_TIPS,
        NOTIFY_NO_CELL_DIALER_ENTERED,
        NOTIFY_NO_CELL_MMS_ENTERED,

        IMSOP_REG_REGISTERING,
        IMSOP_REG_NOT_REGISTERED,
        IMSOP_REG_REGISTERED,
        IMSOP_REG_ERROR,

        IMPL_CHECK_REGED_ON_CALL,
        IMPL_CHECK_RADIO_POWER,
        IMPL_CHECK_IMS_OPERATOR,
        IMPL_CHECK_WIFI_ON_DIALOG,
        IMPL_CHECK_CALL_DROP_ALERT,
        IMPL_CHECK_CALL_ACTIVATED,

        IMPL_CALL_STARTED,
        IMPL_CALL_EMERGENCY,
        IMPL_CALL_ENDED,
        IMPL_IMSOP_HO_FAIL,
        IMPL_IMSOP_HO_SUCCESS,
    }

    /**
     * This is used in FeatureUiHandler.java
     */
    int UI_HANDLER_BIT_POPUP = 1;                  // ..00000001
    int UI_HANDLER_BIT_NOTIFICATION = 2;           // ..00000010
    int UI_HANDLER_BIT_WFC_RIGHT_SIDE_ICON = 4;    // ..00000100
    int UI_HANDLER_BIT_BEEP = 8;                   // ..00001000

    /**
     * Dialog
     */
    int DIALOG_DISSMISS = -1;
    int DIALOG_SKIP = 0;
    int DIALOG_WIFION = 1;
    int DIALOG_ROVEOUT = 2;
    int DIALOG_SIMSWAP = 3;
    int DIALOG_E911 = 4;
    int DIALOG_FIRST_WFC = 5;
    int DIALOG_CALL_DROP_NO_SIGNAL = 6;
    int DIALOG_CALL_DROP_LOST_PACKETS = 7;
    int DIALOG_OOBE_WIFI_CONNECTED = 8;
    int DIALOG_WIFI_CONNTECTED = 9;
    int DIALOG_WFC_NO_CELL_DIAL = 10;
    int DIALOG_WFC_NO_CELL_SEND_MSG = 11;

    String DIALOG_TYPE = "dialog_type";
    String DIALOG_TITLE = "dialog_title";
    String DIALOG_AP_NAME = "dialog_ap_name";

    /**
     * Notification
     */
    int NOTIFICATION_DISSMISS = -1;
    int NOTIFICATION_SKIP = 0;
    int NOTIFICATION_REG_ERROR = 1;
    int NOTIFICATION_REGISTERED = 2;
    int NOTIFICATION_ON_CALL = 3;
    int NOTIFICATION_CALL_END = 4;
    int NOTIFICATION_NO_CELL_DIAL = 5;
    int NOTIFICATION_NO_CELL_SEND_MSG = 6;

    /**
     * ref packages/services/Telephony/res/values/strings.xml
     */
    /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
    int IMS_WIFI_PREF = ImsConfig.WfcModeFeatureValueConstants.WIFI_PREFERRED;
    int IMS_WIFI_ONLY = ImsConfig.WfcModeFeatureValueConstants.WIFI_ONLY;
    int IMS_CELL_PREF = ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED;
    /* MODIFIED-END by Yuan Yili,BUG-2419568*/

    enum STATE_EVENT {
        INITIAL,
        LOCALE_CHANGED,
        CALL_DROP_ALERT,
        NO_CELL_DIAL_TIP,
        NO_CELL_MSG_TIP,
        NO_CELL_DIALER_ENTERED_NOTIFY,
        NO_CELL_MMS_ENTERED_NOTIFY,
        ON_CALL,
        OFF_CALL,
        WIFI_ON,
        WIFI_OFF,
        WIFI_CONNECTED,
        WIFI_DISCONNECTED,
        POOR_WIFI_SIGNAL,
        AIRPLANE_ON,
        AIRPLANE_OFF,
        AUTO_CHECK,
        REGISTERING,
        REGISTERED,
        REGERROR,
        INVALID_SIM,
        ENABLED,
        DISABLED,
        WIFI_PREF,
        WIFI_ONLY,
        CELL_PREF,
    }

    class ImsReasonInfoFactory {
        public static ImsReasonInfo getER05Info() {
            return new ImsReasonInfo(10100, 5, "ER05 - Invalid SIM card");
        }

        public static ImsReasonInfo getMissing911AddressInfo() {
            return new ImsReasonInfo(10100, 403, "REG09 - Missing 911 Address");
        }

        public static ImsReasonInfo getREG99Info() {
            return new ImsReasonInfo(10100, 99, "REG99 - Unable to connect");
        }
    }
}
