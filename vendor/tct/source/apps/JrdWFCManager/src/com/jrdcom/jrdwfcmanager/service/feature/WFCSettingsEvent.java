/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.os.Message;
import com.jrdcom.jrdwfcmanager.service.FeatureProcessHandler;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;


public class WFCSettingsEvent extends _FeatureImplementor {

    public WFCSettingsEvent(IComponentHolder h) {
        super(h);
    }

    @Override
    protected boolean eatMsg(IMPL_MSG what) {
        return what == IMPL_MSG.WFC_ENABLED_CHANGED
                || what == IMPL_MSG.WFC_PREFERENCE_CHANGED
                || what == IMPL_MSG.IMPL_LOCALE_CHANGED
                ;
    }

    @Override
    protected boolean handleMessage(IMPL_MSG what, Message msg) {
        boolean isWFCEnabled = SystemRFStatus.isWFCEnabled();
        int wfcPref = SystemRFStatus.getWFCPref();

        // check if ui need to be cleared.
        switch (what) {
            case WFC_ENABLED_CHANGED:
                if (isWFCEnabled) {
                    notifyStateEvent(STATE_EVENT.ENABLED);
                } else {
                    notifyStateEvent(STATE_EVENT.DISABLED);
                }

                mHolder.getProcessHandler().sendMessage(IMPL_MSG.IMPL_CHECK_RADIO_POWER);
                break;
            case WFC_PREFERENCE_CHANGED:
                switch (wfcPref) {
                    case IMS_WIFI_PREF:
                        notifyStateEvent(STATE_EVENT.WIFI_PREF);
                        break;
                    case IMS_WIFI_ONLY:
                        notifyStateEvent(STATE_EVENT.WIFI_ONLY);
                        break;
                    case IMS_CELL_PREF:
                        notifyStateEvent(STATE_EVENT.CELL_PREF);
                        break;
                }

                mHolder.getProcessHandler().sendMessage(IMPL_MSG.IMPL_CHECK_RADIO_POWER);
                break;
            case IMPL_LOCALE_CHANGED:
                notifyStateEvent(STATE_EVENT.LOCALE_CHANGED);
                break;
        }
        return true;
    }
}