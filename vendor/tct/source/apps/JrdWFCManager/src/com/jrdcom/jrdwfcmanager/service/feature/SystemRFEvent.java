/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Message;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import com.jrdcom.jrdwfcmanager.R;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.service.FeatureProcessHandler;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;
import com.jrdcom.jrdwfcmanager.service.state.PoorWifiSignal;


public class SystemRFEvent extends _FeatureImplementor {

    public SystemRFEvent(IComponentHolder h) {
        super(h);
    }

    @Override
    protected boolean eatMsg(IMPL_MSG what) {
        return what == IMPL_MSG.CELL_STATE_UPDATE
                || what == IMPL_MSG.AIRPLANE_MODE_UPDATE
                || what == IMPL_MSG.WIFI_STATE_UPDATE
                ;
    }

    @Override
    protected boolean handleMessage(IMPL_MSG what, Message msg) {
        SystemRFStatus systemRFStatus = mHolder.getSystemRFStatus();

        switch (what) {
            case AIRPLANE_MODE_UPDATE:
                if (SystemRFStatus.isAirplaneModeOn()) {
                    notifyStateEvent(STATE_EVENT.AIRPLANE_ON);
                } else {
                    notifyStateEvent(STATE_EVENT.AIRPLANE_OFF);
                }
                break;
            case CELL_STATE_UPDATE:
                if (msg.obj == null) {
                    Logger.loge("SystemRFEvent CELL_STATE_UPDATE, msg.obj is null.");
                    break;
                }

                if (msg.obj instanceof ServiceState) {
                    systemRFStatus.setCellServiceState(((ServiceState) msg.obj).getState());
                } else if (msg.obj instanceof SignalStrength) {
                    systemRFStatus.setCellSignalStrengthLevel(((SignalStrength) msg.obj).getLevel());
                }

                SystemRFStatus.setIsLowSignal(mHolder.getSystemRFStatus().isLowSignal());
                break;
            case WIFI_STATE_UPDATE:
                if (msg.obj == null || !(msg.obj instanceof FeatureProcessHandler.WifiStateData)) {
                    break;
                }

                FeatureProcessHandler.WifiStateData pack = (FeatureProcessHandler.WifiStateData) msg.obj;
                switch (pack.type) {
                    case FeatureProcessHandler.WifiStateData.TYPE_WIFI_STATE:
                        switch (pack.wifiState) {
                            case WifiManager.WIFI_STATE_UNKNOWN:
                            case WifiManager.WIFI_STATE_DISABLING:
                            case WifiManager.WIFI_STATE_DISABLED:
                                notifyStateEvent(STATE_EVENT.WIFI_OFF);
                                systemRFStatus.setWifiRssi(WifiInfo.INVALID_RSSI);
                                break;
                            case WifiManager.WIFI_STATE_ENABLING:
                            case WifiManager.WIFI_STATE_ENABLED:
                                notifyStateEvent(STATE_EVENT.WIFI_ON);
                                break;
                        }
                        break;
                    case FeatureProcessHandler.WifiStateData.TYPE_WIFI_CONNECTIVITY:
                        // update state string for settings
                        boolean isConnected = SystemRFStatus.isWifiConnected();
                        if (isConnected) {
                            notifyStateEvent(STATE_EVENT.WIFI_CONNECTED);
                        } else {
                            notifyStateEvent(STATE_EVENT.WIFI_DISCONNECTED);
                            systemRFStatus.setWifiRssi(WifiInfo.INVALID_RSSI);
                            checkCallDropAlert();
                        }
                        break;
                    case FeatureProcessHandler.WifiStateData.TYPE_WIFI_RSSI:
                        systemRFStatus.setWifiRssi(pack.rssi);

                        int roveIn = mHolder.getContext().getResources().getInteger(R.integer.rssi_rove_in);
                        if (SystemRFStatus.isWFCEnabled()
                                && !SystemRFStatus.isWFCRegistered()
                                && pack.rssi <= roveIn) {
                            notifyStateEvent(STATE_EVENT.POOR_WIFI_SIGNAL);
                        } else if (mHolder.getWFCStateProxy().getCurrentState().getClass() == PoorWifiSignal.class) {
                            notifyStateEvent(STATE_EVENT.REGISTERING);
                        }

                        int roveThreshold = mHolder.getContext().getResources().getInteger(R.integer.rssi_rove_threshold);
                        if (pack.rssi <= roveThreshold) {
                            Logger.logd("Call drop alert check, pack.rssi: " + pack.rssi);
                            checkCallDropAlert();
                        }
                        break;
                }
                break;
        }
        return true;
    }

    private void checkCallDropAlert() {
        if (!SystemRFStatus.isVolteEnabled()) {
            mHolder.getProcessHandler().sendMessage(IMPL_MSG.IMPL_CHECK_CALL_DROP_ALERT);
        }
    }
}