/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.provider;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.text.TextUtils;
import com.android.ims.JrdIMSSettings; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
import com.jrdcom.jrdwfcmanager.common.Logger;
import java.util.Map;

public class DBSharePreferences implements IDataSource {
    public final static String WFC_PREFERENCES_NAME = "wfc_settings_preferences";

    private Context mContext;
    SharedPreferences mPreferences;

    public DBSharePreferences(Context context) {
        mContext = context;
        mPreferences = context.getSharedPreferences(WFC_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public String getType(Uri uri) {
        return JrdIMSSettings.MIME_TYPE_SETTINGS_ITEM; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri insertUri = null;
        if (values != null && values.size() >= 2) {
            putPreferences(values);

            insertUri = Uri.withAppendedPath(uri, values.getAsString(JrdIMSSettings.NAME_CV_KEY)); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
            notifyChange(insertUri);
        }
        return insertUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;
        if (selectionArgs != null && selectionArgs.length > 0) {
            removePreferences(selectionArgs[0]);
            notifyChange(uri);
            count = 1;
        }

        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int count = 0;
        if (values != null && values.size() >= 2) {
            /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
            String key = values.getAsString(JrdIMSSettings.NAME_CV_KEY);
            if (mPreferences.contains(key)) {
                putPreferences(values);

                Uri updateUri = Uri.withAppendedPath(uri, values.getAsString(JrdIMSSettings.NAME_CV_KEY));
                /* MODIFIED-END by Yuan Yili,BUG-2419568*/
                notifyChange(updateUri);
                count = 1;
            } else {
                Logger.loge("update(), haven't had this item yet, not updated!" + uri);
            }
        }

        return count;
    }

    @Override
    public Cursor query(Uri uri, String[] select, String selection, String[] selectionArgs, String sort) {
        MatrixCursor cursor = null;
        try {
            if (selectionArgs == null || selectionArgs.length <= 0) {
                throw new Exception("query(), selectionArgs is empty.");
            }

            String key = selectionArgs[0];
            Object value = getPrefValue(key);
            if (value == null) {
                throw new Exception("query(), value for " + key + " is null.");
            }

            if (value instanceof Boolean) {
                value = (Boolean) value ? 1 : 0;
            }

            String[] columnNames = new String[]{key};
            Object[] values = new Object[]{value};

            cursor = new MatrixCursor(columnNames);
            cursor.addRow(values);
            // Logger.logd("DBSharePreferences@query: " + key + "=" + value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cursor;
    }

    private void putPreferences(ContentValues values) {
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        String key = values.getAsString(JrdIMSSettings.NAME_CV_KEY);
        Object val = values.get(JrdIMSSettings.NAME_CV_VALUE);
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/

        if (TextUtils.isEmpty(key) || val == null) {
            return;
        }

        SharedPreferences.Editor editor = mPreferences.edit();
        if (val instanceof Boolean) {
            editor.putBoolean(key, (Boolean) val);
        } else if (val instanceof Long) {
            editor.putLong(key, (Long) val);
        } else if (val instanceof Float) {
            editor.putFloat(key, (Float) val);
        } else if (val instanceof Integer) {
            editor.putInt(key, (Integer) val);
        } else if (val instanceof String) {
            editor.putString(key, (String) val);
        }
        editor.commit();
        Logger.logd("DBSharePreferences@putPreferences: " + key + "=" + val);
    }

    private void removePreferences(String key) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    private Object getPrefValue(String key) {
        Map<String, ?> db = mPreferences.getAll();
        return db.get(key);
    }

    /**
     * @param uri
     */
    private void notifyChange(Uri uri) {
        Logger.logd("DBSharePreferences@notifyChange, notify uri: " + uri);
        mContext.getContentResolver().notifyChange(uri, null);
    }
}
