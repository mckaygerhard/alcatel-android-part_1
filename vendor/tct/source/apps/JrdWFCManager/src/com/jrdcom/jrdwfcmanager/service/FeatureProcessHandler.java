/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service;

import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.android.ims.ImsReasonInfo;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.service.feature.CallDropAlert;
import com.jrdcom.jrdwfcmanager.service.feature.CallEvent;
import com.jrdcom.jrdwfcmanager.service.feature.CheckEvent;
import com.jrdcom.jrdwfcmanager.service.feature.FirstRunCheck;
import com.jrdcom.jrdwfcmanager.service.feature.GAPPEvent;
import com.jrdcom.jrdwfcmanager.service.feature.SimCardEvent;
import com.jrdcom.jrdwfcmanager.service.feature.SystemRFEvent;
import com.jrdcom.jrdwfcmanager.service.feature.WFCRegEvent;
import com.jrdcom.jrdwfcmanager.service.feature.WFCSettingsEvent;
import com.jrdcom.jrdwfcmanager.service.feature._FeatureImplementor;

public class FeatureProcessHandler extends Handler {
    private _FeatureImplementor mHandlerImpl;
    private IComponentHolder mHolder;

    public FeatureProcessHandler(Looper looper, IComponentHolder holder) {
        super(looper);
        mHolder = holder;

        mHandlerImpl = new FirstRunCheck(holder);
        mHandlerImpl
                .setSuccessor(new WFCRegEvent(holder))
                .setSuccessor(new CheckEvent(holder))
                .setSuccessor(new CallEvent(holder))
                .setSuccessor(new GAPPEvent(holder))
                .setSuccessor(new CallDropAlert(holder))
                .setSuccessor(new WFCSettingsEvent(holder))
                .setSuccessor(new SystemRFEvent(holder))
                .setSuccessor(new SimCardEvent(holder));
    }

    public void sendMessageDelayed(WFCUtils.IMPL_MSG msg, int delay) {
        this.sendMessageDelayed(msg, 0, 0, null, delay);
    }

    public void sendMessageDelayed(WFCUtils.IMPL_MSG msg, int arg1, int arg2, int delay) {
        this.sendMessageDelayed(msg, arg1, arg2, null, delay);
    }

    public void sendMessageDelayed(WFCUtils.IMPL_MSG msg, Object obj, int delay) {
        this.sendMessageDelayed(msg, 0, 0, obj, delay);
    }

    public void sendMessageDelayed(WFCUtils.IMPL_MSG msg, int arg1, int arg2, Object obj, int delay) {
        Message message = obtainMessage(msg.ordinal(), arg1, arg2, obj);
        sendMessageDelayed(message, delay);
    }


    public void sendMessage(WFCUtils.IMPL_MSG msg) {
        this.sendMessage(msg, 0, 0, null);
    }

    public void sendMessage(WFCUtils.IMPL_MSG msg, Object obj) {
        this.sendMessage(msg, 0, 0, obj);
    }

    public void sendMessage(WFCUtils.IMPL_MSG msg, int arg1, int arg2) {
        this.sendMessage(msg, arg1, arg2, null);
    }

    public void sendMessage(WFCUtils.IMPL_MSG msg, int arg1, int arg2, Object obj) {
        obtainMessage(msg.ordinal(), arg1, arg2, obj).sendToTarget();
    }

    public void removeMessages(WFCUtils.IMPL_MSG msg) {
        Logger.logd("FeatureProcessHandler removeMessages " + msg);
        removeMessages(msg.ordinal());
    }

    @Override
    public void handleMessage(Message msg) {
        mHandlerImpl.handle(msg);
    }


    /**
     * innert classes for msg.obj data pack
     */
    public static class WifiStateData {
        public static final int TYPE_INVALID = -1;
        public static final int TYPE_WIFI_STATE = 0;
        public static final int TYPE_WIFI_RSSI = 1;
        public static final int TYPE_WIFI_CONNECTIVITY = 2;

        public int type = TYPE_INVALID;
        public int wifiState = WifiManager.WIFI_STATE_DISABLED;
        public int rssi = Integer.MAX_VALUE;
        public boolean isWifiConnected = false;
    }

    public static class WFCStatusData {
        public boolean isWFCEnabled = false;
        public int wfcPrefered = WFCUtils.IMS_WIFI_PREF;
        public ImsReasonInfo imsReasonInfo;

        @Override
        public String toString() {
            return "  isWFCEnabled: " + isWFCEnabled +
                    "  wfcPrefered: " + wfcPrefered +
                    "  imsReasonInfo: " + imsReasonInfo;
        }
    }
}