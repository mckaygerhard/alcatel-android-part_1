/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.ui.metropcs;

import android.app.ActionBar;
import com.jrdcom.jrdwfcmanager.R;

public class QuestionsImpl extends com.jrdcom.jrdwfcmanager.ui.tmo.QuestionsImpl {
    public QuestionsImpl(Object activity) {
        super(activity);
    }

    @Override
    public String[] getQuestions() {
        return mActivity.getResources().getStringArray(R.array.help_top_questions_metropcs);
    }

    @Override
    public String[] getAnswers() {
        return mActivity.getResources().getStringArray(R.array.help_top_questions_answers_metropcs);
    }

    @Override
    public void setActionBar() {
        ActionBar actionBar = mActivity.getActionBar();
    }
}