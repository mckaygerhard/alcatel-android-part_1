package com.jrdcom.jrdwfcmanager.service.state;

import com.jrdcom.jrdwfcmanager.service.IComponentHolder;

public class Disabled extends _State {
    public Disabled(IComponentHolder holder) {
        super(holder);
    }

    @Override
    public void onStateActivated(EventData eData) {
        clearNotification();
        updateWFCSettingsStateInfo(DISABLED, null);
    }

    @Override
    public _State onOnCall(EventData eData) {
        return null;
    }

    @Override
    public _State onOffCall(EventData eData) {
        return null;
    }

    @Override
    public _State onWifiOn(EventData eData) {
        return null;
    }

    @Override
    public _State onWifiOff(EventData eData) {
        return null;
    }

    @Override
    public _State onWifiConnected(EventData eData) {
        return null;
    }

    @Override
    public _State onWifiDisconnected(EventData eData) {
        return null;
    }

    @Override
    public _State onAirplaneOn(EventData eData) {
        return null;
    }

    @Override
    public _State onAirplaneOff(EventData eData) {
        return null;
    }

    @Override
    public _State onRegistering(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegistered(EventData eData) {
        return null;
    }

    @Override
    public _State onRegerror(EventData eData) {
        return null;
    }

    @Override
    public _State onEnabled(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onDisabled(EventData eData) {
        return null;
    }

    @Override
    public _State onWifiPref(EventData eData) {
        return null;
    }

    @Override
    public _State onWifiOnly(EventData eData) {
        return null;
    }

    @Override
    public _State onCellPref(EventData eData) {
        return null;
    }
}
