/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.content.Context;
import android.os.Message;
import android.telephony.TelephonyManager;

import com.android.ims.JrdIMSSettings; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
import com.android.internal.telephony.IccCardConstants;
import com.jrdcom.jrdwfcmanager.JrdWFCManager;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.service.FeatureProcessHandler;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;


public class SimCardEvent extends _FeatureImplementor {
    private static final int SIM_STATE_UPDATE_DELAY = 1000;
    private static final int SIM_STATE_ERROR_DELAY = 10000;

    private static final int APP_START_FREZZED_DELAY = 20;

    public SimCardEvent(IComponentHolder h) {
        super(h);
    }

    @Override
    protected boolean eatMsg(IMPL_MSG what) {
        return what == IMPL_MSG.IMPL_SIM_STATE_UPDATE
                || what == IMPL_MSG.IMPL_SIM_STATE_UPDATE__
                ;
    }

    @Override
    protected boolean handleMessage(IMPL_MSG what, Message msg) {
        FeatureProcessHandler pHandler = mHolder.getProcessHandler();
        SystemRFStatus systemRFStatus = mHolder.getSystemRFStatus();

        switch (what) {
            case IMPL_SIM_STATE_UPDATE:
                pHandler.removeMessages(IMPL_MSG.IMPL_SIM_STATE_UPDATE__);

                int updateDelay = WFCUtils.INVALID;
                boolean isSimOK = checkSimCard();
                String simState = (String) msg.obj;

                /* UNKNOWN means the ICC state is unknown */
                if (IccCardConstants.INTENT_VALUE_ICC_UNKNOWN.equals(simState)) {
                    clearER05Notify();
                    updateDelay = SIM_STATE_ERROR_DELAY;
                    systemRFStatus.setSimCheckUnfrezzedTimePoint();
                    isSimOK = true;
                }
                /* NOT_READY means the ICC interface is not ready (eg, radio is off or powering on) */
                else if (IccCardConstants.INTENT_VALUE_ICC_NOT_READY.equals(simState)) {
                    updateDelay = SIM_STATE_ERROR_DELAY;
                }
                /* ABSENT means ICC is missing */
                else if (IccCardConstants.INTENT_VALUE_ICC_ABSENT.equals(simState)) {
                    if (JrdWFCManager.getAppLivedSeconds() < APP_START_FREZZED_DELAY) {
                        updateDelay = SIM_STATE_ERROR_DELAY;
                    } else if (!hasIccCard()) {
                        updateDelay = SIM_STATE_UPDATE_DELAY;
                    }
                }
                /* CARD_IO_ERROR means for three consecutive times there was SIM IO error */
                else if (IccCardConstants.INTENT_VALUE_ICC_CARD_IO_ERROR.equals(simState)) {
                    updateDelay = SIM_STATE_ERROR_DELAY;
                }
                /* LOCKED means ICC is locked by pin or by network */
                else if (IccCardConstants.INTENT_VALUE_ICC_LOCKED.equals(simState)) {
                    clearER05Notify();
                }
                /* READY means ICC is ready to access */
                else if (IccCardConstants.INTENT_VALUE_ICC_READY.equals(simState)) {
                    if (JrdWFCManager.getAppLivedSeconds() < APP_START_FREZZED_DELAY && !isSimOK) {
                        updateDelay = SIM_STATE_ERROR_DELAY;
                    } else {
                        updateDelay = SIM_STATE_UPDATE_DELAY;
                    }
                }
                /* IMSI means ICC IMSI is ready in property */
                else if (IccCardConstants.INTENT_VALUE_ICC_IMSI.equals(simState)) {
                    updateDelay = SIM_STATE_ERROR_DELAY;
                }
                /* LOADED means all ICC records, including IMSI, are loaded */
                else if (IccCardConstants.INTENT_VALUE_ICC_LOADED.equals(simState)) {
                    updateDelay = SIM_STATE_ERROR_DELAY;

                    /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
                    if (systemRFStatus.isSIMPlmnChanged()) {
                        JrdIMSSettings.put(mHolder.getContext(), CONTENT_DEFAULT_PROFILE_SET, false);
                        pHandler.sendMessage(IMPL_MSG.FIRST_RUN_CHECK);
                    }
                    /* MODIFIED-END by Yuan Yili,BUG-2419568*/
                }

                // when user unlocked sim lock, a ready status will cause er05 error,
                // so freeze state report for 10s.
                if (systemRFStatus.isSimCheckingFrezzed()) {
                    Logger.logd("SimCardEvent, frezzed for checking sim state.");
                    updateDelay = SIM_STATE_ERROR_DELAY;
                }

                if (isSimOK) {
                    updateDelay = SIM_STATE_UPDATE_DELAY;
                }

                Logger.logd("SimCardEvent, [=simState=]: " + simState +
                        "  delay: " + updateDelay +
                        "  isSimOK: " + isSimOK);
                if (updateDelay != WFCUtils.INVALID) {
                    pHandler.sendMessageDelayed(IMPL_MSG.IMPL_SIM_STATE_UPDATE__, isSimOK, updateDelay);
                }
                break;
            case IMPL_SIM_STATE_UPDATE__:
                systemRFStatus.setSimCardOK((Boolean) msg.obj);

                if (systemRFStatus.isSimCardOK()) {
                    if (!SystemRFStatus.isWFCRegistered()) {
                        notifyStateEvent(STATE_EVENT.AUTO_CHECK);
                    }
                } else {
                    notifyStateEvent(STATE_EVENT.INVALID_SIM, ImsReasonInfoFactory.getER05Info());
                }
                break;
        }
        return true;
    }

    private void clearER05Notify() {
        mHolder.getSystemRFStatus().setSimCardOK(true);
        notifyStateEvent(STATE_EVENT.AUTO_CHECK);
    }

    private boolean checkSimCard() {
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        final TelephonyManager telephonyManager = TelephonyManager.getDefault();
        boolean isSimCardOk = hasIccCard();
        String efIst = telephonyManager.getIsimIst();
        if (efIst == null) {
            Logger.loge("ISF is NULL");
        } else {
            boolean isGBASupport = efIst != null && efIst.length() > 1 && (0x02 & (byte) efIst.charAt(1)) != 0;
            isSimCardOk = isSimCardOk && isGBASupport;
        }
        Logger.loge("SimCardEvent checkSimCard(), getIsimIst(): " + getIsimIst() +
                "  isGbaSupported: " + isSimCardOk);
        return isSimCardOk;
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/
    }


    private boolean hasIccCard() {
        TelephonyManager telephonyManager = (TelephonyManager) mHolder.getContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.hasIccCard();
    }

    private String getIsimIst() {
        TelephonyManager telephonyManager = (TelephonyManager) mHolder.getContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getIsimIst();
    }
}
