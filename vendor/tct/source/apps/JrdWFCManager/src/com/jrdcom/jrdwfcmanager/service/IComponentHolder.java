/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service;

import android.content.Context;
import android.os.Handler;

public interface IComponentHolder {
    void onApplicationReady();

    void onApplicationDisabled(); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568

    Context getContext();

    Handler getFeatureUiHandler();

    FeatureProcessHandler getProcessHandler();

    WFCStateProxy getWFCStateProxy();

    IImsOperator getImsOperator();

    SystemRFStatus getSystemRFStatus();
}
