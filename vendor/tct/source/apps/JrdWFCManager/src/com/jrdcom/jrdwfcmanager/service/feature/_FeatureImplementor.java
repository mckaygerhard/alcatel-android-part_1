/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.os.Message;
import com.android.ims.ImsReasonInfo;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;
import com.jrdcom.jrdwfcmanager.service.state._State;
import java.util.HashSet;
import java.util.Set;

public abstract class _FeatureImplementor implements WFCUtils {
    protected IComponentHolder mHolder;

    private Set<IMPL_MSG> mForbiddenMessages;
    private _FeatureImplementor mSuccessor;

    public _FeatureImplementor(IComponentHolder holder) {
        mHolder = holder;
        mForbiddenMessages = new HashSet<IMPL_MSG>();
    }

    public final _FeatureImplementor setSuccessor(_FeatureImplementor successor) {
        return mSuccessor = successor;
    }

    /**
     * @param msg
     */
    public final void handle(Message msg) {
        final WFCUtils.IMPL_MSG what = getEnumMsg(msg.what);
        if (eatMsg(what)) {
            // still may not handle in some cases
            boolean willHandle = true;
            boolean isBusy = isBusyOn(msg);

            // check if handler is busy
            if (isBusy && !canHandleMsgWhenBusy(what)) {
                Logger.logw("_FeatureImplementor@handle(), handler is busy and skip message: " + what);
                willHandle = false;
            }

            // check if WFC is disabled
            else if (!SystemRFStatus.isWFCEnabled() && skipWhenWFCDisabled()) {
                Logger.logw("_FeatureImplementor@handle(), WFC is disabled and skip message: " + what);
                willHandle = false;
            }

            // check if message is forbidden
            else if (isForbiddenMessage(what)) {
                Logger.logw("_FeatureImplementor@handle(), message is forbidden: " + what);
                willHandle = false;
            }

            // handle the message
            if (willHandle) {
                Logger.logd("_FeatureImplementor@handle(), start handle message: " + what);
                handleMessage(what, msg);
            } else {
                Logger.logw("_FeatureImplementor@handle(), skip message: " + what);
            }
        } else if (mSuccessor != null) {
            mSuccessor.handle(msg);
        } else {
            Logger.loge("unhandled message: " + what);
        }
    }

    private boolean isBusyOn(Message msg) {
        return mHolder.getImsOperator().isBusyFor(msg);
    }

    protected abstract boolean eatMsg(WFCUtils.IMPL_MSG what);

    protected boolean canHandleMsgWhenBusy(WFCUtils.IMPL_MSG what) {
        return false;
    }

    protected boolean skipWhenWFCDisabled() {
        return false;
    }

    protected abstract boolean handleMessage(WFCUtils.IMPL_MSG what, Message msg);

    protected final void notifyStateEvent(STATE_EVENT event) {
        this.notifyStateEvent(event, null);
    }

    protected final void notifyStateEvent(STATE_EVENT event, ImsReasonInfo imsReasonInfo) {
        _State.EventData data = new _State.EventData();
        data.event = event;
        data.imsReasonInfo = imsReasonInfo;
        mHolder.getWFCStateProxy().onStateEvent(data);
    }

    protected final void setForbiddenMessage(IMPL_MSG msg) {
        mForbiddenMessages.add(msg);
    }

    protected final void removeForbiddenMessage(IMPL_MSG msg) {
        mForbiddenMessages.remove(msg);
    }

    private boolean isForbiddenMessage(IMPL_MSG msg) {
        return mForbiddenMessages.contains(msg);
    }

    private WFCUtils.IMPL_MSG getEnumMsg(int ordinal) {
        WFCUtils.IMPL_MSG msg = null;
        for (WFCUtils.IMPL_MSG m : WFCUtils.IMPL_MSG.values()) {
            if (m.ordinal() == ordinal) {
                msg = m;
                break;
            }
        }
        return msg;
    }
}