/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.android.ims.JrdIMSSettings; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
import com.android.internal.app.AlertActivity;
import com.android.internal.app.AlertController;
import com.jrdcom.jrdwfcmanager.JrdWFCManager;
import com.jrdcom.jrdwfcmanager.R;
import com.jrdcom.jrdwfcmanager.common.ImplementorFactory;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;

public class DialogManagerActivity extends AlertActivity implements WFCUtils {
    private Resources mResources;
    private DialogManagerActivity.IImplementor mImplementor;

    private static final int TIME_DIALOG_DISMISS = 10 * 1000;
    private static final int EVENT_DIALOG_DISMISS = 1;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_DIALOG_DISMISS:
                    finish();
                    break;
            }
        }
    };

    public static DialogPack getDialogPack(int type) {
        DialogPack pack = new DialogPack();
        pack.type = type;
        return getDialogPack(pack);
    }

    public static DialogPack getDialogPack(DialogPack pack) {
        if (pack == null) {
            Logger.loge("getDialogPack() return, pack is null.");
            return null;
        }

        Context context = JrdWFCManager.getContext();

        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        if (!JrdIMSSettings.isFeatureOn(context, com.android.internal.R.bool.jrd_ims_feature_vowifi_popup)) {
            Logger.loge("getDialogPack() return, jrd_ims_feature_vowifi_popup is false");
            pack.type = DIALOG_SKIP;
        }

        switch (pack.type) {
            case DIALOG_WIFION:
                if (JrdIMSSettings.getBoolean(context, CONTENT_FIRST_TURN_ON_WIFI_POPUP_DONE, false)) {
                    pack.type = DIALOG_SKIP;
                }
                pack.type = DIALOG_SKIP;
                break;
            case DIALOG_FIRST_WFC:
                if (JrdIMSSettings.getBoolean(context, CONTENT_FIRST_CALL_POPUP_SKIP, false)) {
                    pack.type = DIALOG_SKIP;
                }
                break;
            case DIALOG_WIFI_CONNTECTED:
                if (JrdIMSSettings.getBoolean(context, CONTENT_WIFI_CONNTECTED_TIP_SKIP, false)) {
                    pack.type = DIALOG_SKIP;
                }
                break;
            case DIALOG_OOBE_WIFI_CONNECTED:
                if (JrdIMSSettings.getBoolean(context, WFCUtils.CONTENT_FIRST_REGISTERED_POPUP_DONE, false)) {
                /* MODIFIED-END by Yuan Yili,BUG-2419568*/
                    pack.type = DIALOG_SKIP;
                }
                break;
        }
        return pack;
    }

    public DialogManagerActivity() {
        mImplementor = (DialogManagerActivity.IImplementor) ImplementorFactory.getImplementor(this.getClass(), this);
        if (mImplementor == null) {
            throw new RuntimeException("Tutorial's implementor is null.");
        }
    }

    public static void show(DialogPack pack) {
        Context context = JrdWFCManager.getContext();
        Intent intent = new Intent(context, DialogManagerActivity.class);
        intent.putExtra(DIALOG_TYPE, pack.type);
        intent.putExtra(DIALOG_AP_NAME, pack.apSSID);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final AlertController.AlertParams p = mAlertParams;
        p.mForceInverseBackground = true;
        mResources = getResources();

        createDialog(getIntent(), p);

        setupAlert();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        int prevType = getIntent().getIntExtra(DIALOG_TYPE, DIALOG_DISSMISS);
        setIntent(intent);
        int newType = intent.getIntExtra(DIALOG_TYPE, DIALOG_DISSMISS);
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/
        if (newType != prevType) {
            finish();
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeMessages(EVENT_DIALOG_DISMISS);
    }

    private void createDialog(Intent intent, AlertController.AlertParams p) {
        int type = intent.getIntExtra(DIALOG_TYPE, DIALOG_DISSMISS); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568

        if (type == DIALOG_DISSMISS) {
            mHandler.sendEmptyMessage(DialogManagerActivity.EVENT_DIALOG_DISMISS);
            return;
        }

        String[] titles = mResources.getStringArray(R.array.wfc_dialog_title);
        if (type > titles.length) {
            return;
        }
        p.mTitle = titles[type];

        final Context context = JrdWFCManager.getContext();
        String message = mImplementor.getMessageBody(type);
        p.mMessage = TextUtils.isEmpty(message) ? null : message;
        p.mPositiveButtonText = mResources.getStringArray(R.array.wfc_dialog_left_btn)[type];
        p.mNegativeButtonText = mResources.getStringArray(R.array.wfc_dialog_right_btn)[type];
        String inTitle = intent.getStringExtra(DIALOG_TITLE);
        View view = null;

        switch (type) {
            case DIALOG_WIFION:
                view = LayoutInflater.from(this).inflate(R.layout.dialog_view_wifion, null);
                TextView wifion_textview = (TextView) view.findViewById(R.id.title);
                wifion_textview.setText(p.mTitle);
                p.mPositiveButtonListener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        launchActivity(R.string.config_wfc_tutorial_intent);
                    }
                };
                p.mCustomTitleView = view;

                // Don't show anymore
                /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
                if (!JrdIMSSettings.getBoolean(context, WFCUtils.CONTENT_FIRST_TURN_ON_WIFI_POPUP_DONE, false)) {
                    JrdIMSSettings.put(context, WFCUtils.CONTENT_FIRST_TURN_ON_WIFI_POPUP_DONE, true);
                    /* MODIFIED-END by Yuan Yili,BUG-2419568*/
                }
                break;
            case DIALOG_ROVEOUT:
                mHandler.sendEmptyMessageDelayed(DialogManagerActivity.EVENT_DIALOG_DISMISS, DialogManagerActivity.TIME_DIALOG_DISMISS);
                break;
            case WFCUtils.DIALOG_SIMSWAP:
                inTitle = intent.getStringExtra(WFCUtils.DIALOG_TITLE);
                p.mTitle = TextUtils.isEmpty(inTitle) ? p.mTitle : inTitle;
                p.mPositiveButtonListener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        launchActivity(R.string.config_wifi_settings_intent);
                    }
                };
                break;
            case DIALOG_E911:
                p.mTitle = TextUtils.isEmpty(inTitle) ? p.mTitle : inTitle;
                view = LayoutInflater.from(this).inflate(R.layout.dialog_view_missing_e911, null);
                TextView e911_textview = (TextView) view.findViewById(R.id.topmsg);
                e911_textview.setMovementMethod(LinkMovementMethod.getInstance());
                p.mView = view;

                mImplementor.onCreateDialog(p, type);
                break;
            case DIALOG_FIRST_WFC:
                view = LayoutInflater.from(this).inflate(R.layout.dialog_view_first_wfc_call, null);
                CheckBox check = (CheckBox) view.findViewById(R.id.checkbox);
                check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        JrdIMSSettings.put(context, CONTENT_FIRST_CALL_POPUP_SKIP, isChecked); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
                    }
                });
                p.mView = view;
                mImplementor.onCreateDialog(p, type);
                break;
            case DIALOG_CALL_DROP_NO_SIGNAL:
            case DIALOG_CALL_DROP_LOST_PACKETS:
                p.mPositiveButtonListener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        launchActivity(R.string.config_pick_wifi_ap_intent);
                    }
                };
                mImplementor.onCreateDialog(p, type);
                break;
            case DIALOG_OOBE_WIFI_CONNECTED:
                view = LayoutInflater.from(this).inflate(R.layout.dialog_view_oobe_wifi_connected, null);
                TextView textview = (TextView) view.findViewById(R.id.topmsg);
                textview.setText(getString(R.string.oobe_wifi_connected_topmsg, intent.getStringExtra(DIALOG_AP_NAME)));
                p.mView = view;
                mImplementor.onCreateDialog(p, type);

                // Don't show anymore
                /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
                if (!JrdIMSSettings.getBoolean(context, WFCUtils.CONTENT_FIRST_REGISTERED_POPUP_DONE, false)) {
                    JrdIMSSettings.put(context, WFCUtils.CONTENT_FIRST_REGISTERED_POPUP_DONE, true);
                    /* MODIFIED-END by Yuan Yili,BUG-2419568*/
                }
                break;
            case DIALOG_WIFI_CONNTECTED: /** NOT USED */
                view = LayoutInflater.from(this).inflate(R.layout.dialog_view_wfc_disabled_reminder, null);

                p.mNegativeButtonListener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        launchActivity(R.string.config_wifi_settings_intent);
                    }
                };
                check = (CheckBox) view.findViewById(R.id.checkbox);
                check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        JrdIMSSettings.put(context, CONTENT_WIFI_CONNTECTED_TIP_SKIP, b); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
                    }
                });
                p.mView = view;
                break;
            case DIALOG_WFC_NO_CELL_DIAL:
            case DIALOG_WFC_NO_CELL_SEND_MSG:
                p.mNegativeButtonText = null;
                break;
        }
    }

    public void launchActivity(int resid) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent.setComponent(ComponentName.unflattenFromString(mResources.getString(resid)));
        startActivity(intent);
    }

    /**
     * inner classes
     */
    public interface IImplementor extends ImplementorFactory.IImplementor {
        String getMessageBody(int type);

        void onCreateDialog(AlertController.AlertParams p, int type);
    }

    public static class DialogPack {
        public int type;
        public String apSSID;
    }
}
