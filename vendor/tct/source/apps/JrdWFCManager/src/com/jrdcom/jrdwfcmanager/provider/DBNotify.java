/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.android.ims.JrdIMSSettings; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
import com.jrdcom.jrdwfcmanager.common.Logger;

public class DBNotify implements IDataSource {
    private Context mContext;

    public DBNotify(Context context) {
        mContext = context;
    }

    @Override
    public String getType(Uri uri) {
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        return JrdIMSSettings.MIME_TYPE_SETTINGS_ITEM;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri insertUri = Uri.withAppendedPath(uri, values.getAsString(JrdIMSSettings.NAME_CV_KEY));
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/
        notifyChange(insertUri);
        return insertUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new RuntimeException("DBNotify not support delete!");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new RuntimeException("DBNotify not support update!");
    }

    @Override
    public Cursor query(Uri uri, String[] select, String selection, String[] selectionArgs, String sort) {
        throw new RuntimeException("DBNotify not support update!");
    }

    /**
     * @param uri
     */
    private void notifyChange(Uri uri) {
        Logger.logd("DBNotify@notifyChange, notify uri: " + uri);
        mContext.getContentResolver().notifyChange(uri, null);
    }
}
