package com.zhong.myclient.contentprovider.launcher;

import android.provider.BaseColumns;

/**
 * Created by weizhong.zeng on 2016/10/27.
 */

public interface ChangeLogColumns extends BaseColumns {
    /**
     * The time of the last update to this row.
     * <P>Type: INTEGER</P>
     */
    public static final String MODIFIED = "modified";
}
