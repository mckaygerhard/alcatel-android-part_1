package com.zhong.myclient.contentprovider.launcher;

/**
 * Created by weizhong.zeng on 2016/10/27.
 */

public class LauncherProvider {

    static final String OLD_AUTHORITY = "com.android.launcher2.settings";
    static final String AUTHORITY = ProviderConfig.AUTHORITY;

    public static final String TABLE_FAVORITES = "favorites";
    public static final String TABLE_WORKSPACE_SCREENS = "workspaceScreens";

    static final String PARAMETER_NOTIFY = "notify";


}
