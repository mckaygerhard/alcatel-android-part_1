package com.zhong.myclient.contentprovider.launcher;

/**
 * Created by weizhong.zeng on 2016/10/27.
 */

public interface BaseColumns {
    /**
     * The unique ID for a row.
     * <P>Type: INTEGER (long)</P>
     */
    public static final String _ID = "_id";

    /**
     * The count of rows in a directory.
     * <P>Type: INTEGER</P>
     */
    public static final String _COUNT = "_count";
}
