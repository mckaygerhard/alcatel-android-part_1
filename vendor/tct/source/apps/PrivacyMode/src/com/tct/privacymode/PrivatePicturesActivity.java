/******************************************************************************/
/*                                                               Date:03/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 03/02/2016|     caixia.chen      |     task 1427498     |访客模式              */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.tct.privacymode;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import tct.util.privacymode.TctPrivacyModeHelper;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.Manifest;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Video;
import android.provider.MediaStore.Files.FileColumns;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tct.privacymode.R;

public class PrivatePicturesActivity extends ListActivity implements OnItemLongClickListener, OnClickListener {
    public static final String[] PROJECTION = new String[] {
        Images.Media._ID,
        Images.Media.DATA,
        Images.Media.DATE_MODIFIED,
        Images.Media.DISPLAY_NAME,
    };

    private static final int ID_COLUMN_INDEX = 0;
    private static final int DATA_COLUMN_INDEX = 1;
    private static final int DATE_MODIFIED_COLUMN_INDEX = 2;
    private static final int DISPLAY_NAME_COLUMN_INDEX = 3;

    private static final int LOADER_PRIVATE_PICTURES = 628;
    private static final int ADD_PRIVATE_PICTURES = 629;
    private static final int CHECK_PERMISSION_RESULT = 1;
    private static final String TCT_IS_PRIVATE = "tct_is_private";

    boolean mIsMultiChoiceMode = false;
    private boolean mIsEmpty = false;

    ArrayList<String> mChoiceSet = new ArrayList<String>();
    ArrayList<String> mChoiceDataSet = new ArrayList<String>();
    private TextView mCountView;
    private LinearLayout mEmptyView;
    private QueryHandler mQueryHandler;
    private ContentResolver mResolver;
    private AlbumCursorAdapter mAdapter;
    private boolean mShouldDestroy = false;
    private MenuItem mSelectAllMenu;
    private TextView mSelectCount;
    private SimpleDateFormat mDateFormat;
    private IconHolder mIconHolder;
    private TctPrivacyModeHelper mTctPrivacyModeHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CHECK_PERMISSION_RESULT);
        } else {
            init();
        }
    }

    private void init() {
        setContentView(R.layout.private_picture);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        mEmptyView = (LinearLayout) findViewById(R.id.no_picture);
        mCountView = (TextView)findViewById(R.id.private_picture_count);

        mResolver = this.getContentResolver();
        String strDateFormat = android.provider.Settings.System.getString(mResolver,android.provider.Settings.System.DATE_FORMAT);
        if (TextUtils.isEmpty(strDateFormat)) {
            strDateFormat = "dd.MM.yyyy HH:mm:ss"; //"dd/MM/yyyy HH:mm:ss"
        }
        mDateFormat = new SimpleDateFormat(strDateFormat);
        mIconHolder = new IconHolder(this, mResolver);
        mTctPrivacyModeHelper = TctPrivacyModeHelper.createHelper(this);

        mAdapter = new AlbumCursorAdapter(this);
        getListView().setAdapter(mAdapter);
        getListView().setOnItemLongClickListener(this);

        mQueryHandler = new QueryHandler(this);

        loadPictures();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                            int[] grantResults) {
        if(CHECK_PERMISSION_RESULT == requestCode){
            if(this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                init();
            } else {
                finish();
            }
        }
    }

    public void loadPictures() {
        Uri uri = MediaStore.Files.getContentUri("external");
        String orderBy = FileColumns.DATE_MODIFIED + " DESC ";
        String sel = FileColumns.MEDIA_TYPE + "=" + FileColumns.MEDIA_TYPE_IMAGE + " AND " + TCT_IS_PRIVATE + "=1";
        mQueryHandler.startQuery(LOADER_PRIVATE_PICTURES, null, uri, PROJECTION, sel, null, orderBy);
    }

    private class QueryHandler extends AsyncQueryHandler {
        protected WeakReference<PrivatePicturesActivity> mActivity;

        public QueryHandler(Context context) {
            super(context.getContentResolver());
            mActivity = new WeakReference<PrivatePicturesActivity>(
                    (PrivatePicturesActivity) context);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            if (token != LOADER_PRIVATE_PICTURES) {
                return;
            }
            // In the case of low memory, the WeakReference object may be
            // recycled.
            if (mActivity == null || mActivity.get() == null) {
                mActivity = new WeakReference<PrivatePicturesActivity>(
                        PrivatePicturesActivity.this);
            }
            final PrivatePicturesActivity activity = mActivity.get();
            int num = 0;
            if (cursor != null) {
                num = cursor.getCount();
            }
            if (num == 0) {
                showEmptyView();
                mIsEmpty = true;
                return;
            }
            mIsEmpty = false;
            activity.mAdapter.changeCursor(cursor);
            String s = num + " " + getString(R.string.private_picture);
            mCountView.setText(s);
            showListView();
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        if (!mIsMultiChoiceMode) {
            return;
        }
        CheckBox checkBox = (CheckBox) v.findViewById(R.id.pick_picture_check);
        boolean isChecked = !checkBox.isChecked();
        checkBox.setChecked(isChecked);

        PictureInfo info = (PictureInfo) v.getTag();
        if (isChecked) {
            mChoiceSet.add(info.file_id);
            mChoiceDataSet.add(info.data);
        } else {
            mChoiceSet.remove(info.file_id);
            mChoiceDataSet.remove(info.data);
        }
        updateMultiSelectTitle();
    }

    public class AlbumCursorAdapter extends CursorAdapter {

        public AlbumCursorAdapter(Context context) {
            super(context, null, false);
        }

        public void bindView(View v, Context context, Cursor cursor) {
            PictureInfo info = new PictureInfo();
            info.file_id = Integer.toString(cursor.getInt(ID_COLUMN_INDEX));
            info.data = cursor.getString(DATA_COLUMN_INDEX);
            v.setTag(info);

            TextView titleTextView = (TextView) v.findViewById(
                    R.id.picture_name);
            String name = cursor.getString(DISPLAY_NAME_COLUMN_INDEX);
            titleTextView.setText(name);
            TextView timeTextView = (TextView) v.findViewById(
                    R.id.picture_timestamp);
            long dateModifiedInSeconds = cursor.getLong(DATE_MODIFIED_COLUMN_INDEX);
            timeTextView.setText(mDateFormat.format(new Date(dateModifiedInSeconds*1000)));

            ImageView thumbImageView = (ImageView) v.findViewById(
                    R.id.thumb_image);
            mIconHolder.loadDrawable(thumbImageView, info.data, cursor.getLong(ID_COLUMN_INDEX));

            CheckBox checkBox = (CheckBox) v.findViewById(R.id.pick_picture_check);
            if (mIsMultiChoiceMode) {
                if (mChoiceSet.contains(info.file_id)) {
                    checkBox.setChecked(true);
                } else {
                    checkBox.setChecked(false);
                }
                checkBox.setVisibility(View.VISIBLE);
            } else {
                checkBox.setChecked(false);
                checkBox.setVisibility(View.GONE);
            }
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View v = LayoutInflater.from(context).inflate(
                    R.layout.private_picture_item, parent, false);
            return v;
        }
    }

    @Override
    public void onBackPressed() {
        if (mIsMultiChoiceMode) {
            exitMultiSelectMode();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == ADD_PRIVATE_PICTURES) && (resultCode == RESULT_OK)) {
            loadPictures();
        }
    }

    @Override
    public void onDestroy() {
        if (mQueryHandler != null) {
            mQueryHandler.removeCallbacksAndMessages(LOADER_PRIVATE_PICTURES);
        }
        if ((mAdapter != null) && (mAdapter.getCursor() != null)) {
            mAdapter.getCursor().close();
        }
        if (mIconHolder != null) {
            mIconHolder.cleanup();
        }
        super.onDestroy();
    }

    private void selectAll() {
        Cursor cursor = mAdapter.getCursor();
        if (cursor == null) {
            return;
        }

        boolean isSelected = mChoiceSet.size() != cursor.getCount();
        mChoiceSet.clear();

        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            String file_id = Integer.toString(cursor.getInt(ID_COLUMN_INDEX));

            if (isSelected) {
                mChoiceSet.add(file_id);
                mChoiceDataSet.add(cursor.getString(DATA_COLUMN_INDEX));
            } else {
                mChoiceSet.remove(file_id);
                mChoiceDataSet.remove(cursor.getString(DATA_COLUMN_INDEX));
            }
        }

        int count = getListView().getChildCount();
        for (int i = 0; i < count; i++) {
            View v = getListView().getChildAt(i);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.pick_picture_check);
            checkBox.setChecked(isSelected);
        }
        updateMultiSelectTitle();
    }

    private void removePrivatePictures() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
        .setCancelable(false)
        .setTitle(R.string.remove)
        .setMessage(R.string.remove_msg)
        .setPositiveButton(R.string.remove, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mTctPrivacyModeHelper.setFilePrivateFlag(null, mChoiceDataSet, false);
                loadPictures();
                exitMultiSelectMode();
            }
        })
        .setNegativeButton(R.string.dlg_cancel, null)
        .show();
    }

    public void onDone() {
        Uri uri = MediaStore.Files.getContentUri("external");
        ContentValues values = new ContentValues(1);
        values.put(TCT_IS_PRIVATE, 0);
        final String FILE_KEY_IN = Images.Media._ID + " IN (";
        StringBuilder selection = new StringBuilder(FILE_KEY_IN);
        boolean first = true;
        boolean firstBuket = true;
        for (int i = 0; i < mChoiceSet.size(); i++) {
            if (first) {
                first = false;
            } else {
                selection.append(',');
            }
            selection.append(mChoiceSet.get(i));
        }
        selection.append(')');
        String sel = selection.toString();
        mResolver.update(uri, values, selection.toString(), null);
    }

    public void showEmptyView() {
        mEmptyView.setVisibility(View.VISIBLE);
        mCountView.setVisibility(View.GONE);
        getListView().setVisibility(View.GONE);
    }

    public void showListView() {
        mEmptyView.setVisibility(View.GONE);
        mCountView.setVisibility(View.VISIBLE);
        getListView().setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view,
            int position, long id) {
        if (mIsMultiChoiceMode) {
            return false;
        }
        mChoiceSet.clear();
        mChoiceDataSet.clear();
        mChoiceSet.add(((PictureInfo) view.getTag()).file_id);
        mChoiceDataSet.add(((PictureInfo) view.getTag()).data);
        enterMultiSelectMode();
        return true;

    }

    private void enterMultiSelectMode() {
        if (mIsMultiChoiceMode) {
            return;
        }
        mIsMultiChoiceMode = true;
        mAdapter.notifyDataSetChanged();

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.list_actionbar);

        ImageView mBackView = (ImageView) actionBar.getCustomView().findViewById(R.id.back);
        mBackView.setOnClickListener(this);
        mSelectCount = (TextView) actionBar.getCustomView().findViewById(R.id.select_count);
        updateMultiSelectTitle();
    }

    private void exitMultiSelectMode() {
        if (mIsMultiChoiceMode) {
            mIsMultiChoiceMode = false;
            mChoiceSet.clear();
            mChoiceDataSet.clear();
            mAdapter.notifyDataSetChanged();
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowCustomEnabled(false);
            actionBar.setTitle(R.string.private_picture);
        }
    }

    private void updateMultiSelectTitle() {
        mSelectCount.setText(mChoiceSet.size() + "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.private_picture_options, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem addMenu = menu.findItem(R.id.menu_add);
        MenuItem selectItem = menu.findItem(R.id.menu_select_item);
        MenuItem selectAll = menu.findItem(R.id.menu_select_all);
        MenuItem removeMenu = menu.findItem(R.id.menu_remove);
        if (mIsMultiChoiceMode) {
            addMenu.setVisible(false);
            selectItem.setVisible(false);
            selectAll.setVisible(true);
            if (mChoiceSet.size() == 0) {
                removeMenu.setVisible(false);
            } else if (mAdapter.getCount() == mChoiceSet.size()) {
                selectAll.setVisible(false);
                removeMenu.setVisible(true);
            } else {
                selectAll.setVisible(true);
                removeMenu.setVisible(true);
            }
        } else {
            addMenu.setVisible(true);
            removeMenu.setVisible(false);
            if (mIsEmpty) {
                selectItem.setVisible(false);
                selectAll.setVisible(false);
            } else {
                selectItem.setVisible(true);
                selectAll.setVisible(true);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                Intent intent = new Intent("com.tct.privacymode.action.ADD_PRIVACY_ALBUM");
                startActivityForResult(intent, ADD_PRIVATE_PICTURES);
                break;
            case R.id.menu_remove:
                removePrivatePictures();
                break;
            case R.id.menu_select_item:
                enterMultiSelectMode();
                break;
            case R.id.menu_select_all:
                enterMultiSelectMode();
                selectAll();
                break;
            case android.R.id.home:
                finish();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public class PictureInfo {
        String file_id;
        String data;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                onBackPressed();
                break;
        }
    }
}
