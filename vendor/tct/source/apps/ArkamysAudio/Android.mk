# Copyright (C) 2016 Tcl Corporation Limited
ifeq ($(TARGET_PRODUCT), simba6x)
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := ArkamysAudio
LOCAL_SRC_FILES := ArkamysAudio.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_DEX_PREOPT := false
include $(BUILD_PREBUILT)
endif
