# Copyright (C) 2016 Tcl Corporation Limited
#ifeq ($(TARGET_PRODUCT), london)
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional


LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := securitysdk:app/libs/securitysdk-v2_3.0.13.2282.jar

LOCAL_PROGUARD_ENABLED := disabled
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_STATIC_JAVA_LIBRARIES := securitysdk \
                            gson-x

LOCAL_SRC_FILES := $(call all-java-files-under, app/src/main/java)
LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/app/src/main/res


LOCAL_PROGUARD_FLAG_FILES := proguard.flags

LOCAL_PACKAGE_NAME := SecuritySDKInit

LOCAL_CERTIFICATE := platform

#ifneq ($(INCREMENTAL_BUILDS),)
#    LOCAL_PROGUARD_ENABLED := disabled
#    LOCAL_JACK_ENABLED := incremental
#    LOCAL_DX_FLAGS := --multi-dex
#    LOCAL_JACK_FLAGS := --multi-dex native
#endif

include $(BUILD_PACKAGE)
#endif

