package com.tct.securitysdkinit;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;

public class BootReceiver extends BroadcastReceiver {

    private static final String TAG = "BootReceiver";

    public static final int MY_BACKGROUND_JOB = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            boolean initState = preferences.getBoolean("initState", false);
            Log.d(TAG, "onReceive:" + intent.getAction() + " | initState:" + initState);
            if (!initState) {
                JobScheduler js = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                ComponentName cn = new ComponentName(context, InitService.class.getName());
                JobInfo info = new JobInfo.Builder(
                    MY_BACKGROUND_JOB, cn)
                        .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                        .setRequiresCharging(false)
                        .setPeriodic(10 * 60 * 1000L)
                        .build();
                js.schedule(info);
            }
        }
    }



}
