/* ----------|----------------------|----------------------|----------------- */
/*     Modifications on Features list / Changes Request / Problems Report                          */
/* -------------------------------------------------------------------------- */
/*    date        |        author        |         Key          |     comment                                    */
/* -------------------------------------------------------------------------- */
/* ----------|---------------|---------------|------------------------------- */
/* 10/23/2015|       haibiao.lu     |     1103432        |     create for sale mode                   */
/*****************************************************************************/

package com.tcl.tct.salemode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.DropBoxManager;
import android.util.Log;
import android.os.SystemProperties;
import android.app.Activity;
import android.content.SharedPreferences;

public class SaleModeReceiver extends BroadcastReceiver {
    private static final String TAG = "SaleModeReceiver";

    public static final String PREF_NAME = "com.tcl.tct.tctsalemode_preferences";
    public static final String KEY_LAST_CRASH_TIME = "last_crash_time";
    public static final String EXTRA_PROCESS_NAME_AND_TYPE="extra_process_name_and_type";
    public static final String TCT_ACTION_CRASH_REPORT_FOR_SALE_MODE ="android.intent.action.CRASH_REPORT_FOR_SALE_MODE";

    private static final int CRASH_REPORT_DURATION = 5000;
    private long last_crash_time = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        last_crash_time = getLastCrashTime(context);

        if(System.currentTimeMillis() - last_crash_time < CRASH_REPORT_DURATION){
            return;
        }

        boolean isOn = SystemProperties.getBoolean("tct.salemode.switch", true);
        if (!isOn) {
            Log.i(TAG," ----- Salemode switch off---- ");
            return;
        }

        if (DropBoxManager.ACTION_DROPBOX_ENTRY_ADDED.equals(action)) {
            processDropBoxMsg(context,intent);
        }else if(TCT_ACTION_CRASH_REPORT_FOR_SALE_MODE.equals(action)){
            String processName = intent.getStringExtra(EXTRA_PROCESS_NAME_AND_TYPE);
            if(processName != null && processName.length() > 91){
                processName = processName.substring(0,90);
            }

            if(!isErrorHappened(context,processName)){
                setCrashProcessAndType(context, processName);
                processCatchLog(context, processName);
            }
        }
    }

    private void processCatchLog(Context context,String processName) {
        setLastCrashTime(context);
        String state = SystemProperties.get("init.svc.tct_as_log", "stopped");
        if(state != null && "stopped".equals(state)){
            if(processName != null){
                SystemProperties.set("debug.as.name",processName);
            }
            SystemProperties.set("ctl.start", "tct_as_log");
        }
    }

    private void processDropBoxMsg(Context context, Intent intent){
        String tag = intent.getStringExtra(DropBoxManager.EXTRA_TAG);
        long time = intent.getLongExtra(DropBoxManager.EXTRA_TIME, 0);
        boolean ignore = false;
        String processName = null;
         if (tag != null && tag.endsWith("strictmode")) {
            ignore = true;
        }

         if (!ignore) {
             DropBoxManager dropbox = (DropBoxManager) context.getSystemService(Context.DROPBOX_SERVICE);
             DropBoxManager.Entry entry = dropbox.getNextEntry(tag, time - 1);
 
             if (entry != null) {
                 String data = entry.getText(8192);
                 if (data != null) {
                     String[] fields = new String(data).split("\n");
                     for (String tmp : fields) {
                         if (tmp.startsWith("Process: ")) {
                             processName = tmp.substring(9);
                             break;
                         }
                     }
                 }
             }
         }

         if(!ignore){
            processCatchLog(context, processName);
         }
    }

    private void setLastCrashTime(Context context){
        SharedPreferences spref = context.getSharedPreferences(
            PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = spref.edit();
        editor.putLong(KEY_LAST_CRASH_TIME, System.currentTimeMillis()).commit();
    }

   private long getLastCrashTime(Context context){
       long result = 0;
       SharedPreferences spref = context.getSharedPreferences(
               PREF_NAME, Activity.MODE_PRIVATE);
       result = spref.getLong(KEY_LAST_CRASH_TIME, 0);
       return result;
    }

    private void setCrashProcessAndType(Context context, String processName){
        if(processName == null){
            return;
        }
        SharedPreferences spref = context.getSharedPreferences(
            PREF_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = spref.edit();
        editor.putBoolean(processName, true).commit();
    }

    private boolean isErrorHappened(Context context, String processName){
        if(processName == null){
            return false;
        }
        SharedPreferences spref = context.getSharedPreferences(
            PREF_NAME, Activity.MODE_PRIVATE);
        return spref.getBoolean(processName, false);
    }
}
