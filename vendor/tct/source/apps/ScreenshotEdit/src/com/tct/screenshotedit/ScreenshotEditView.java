package com.tct.screenshotedit;

import com.tct.screenshotedit.view.ScrollDoneView;
import com.tct.screenshotedit.view.ScrollSavingView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScreenshotEditView extends LinearLayout {
    private String TAG = "ScreenshotEditService";
    private Context mContext;
    public MyCropView mCropView;
    public ImageButton mScrollButton;
    public ImageButton mDrawButton;
    public ImageButton mShareButton;
    public ImageButton mSaveButton;
    public ImageButton mCloseButton;
    private View mScreenshotEditLayout;
    private String mImageFilePath;
    public boolean hasChanged = false;

    public View mMyScrollSreenshotView;
    private TextView mTxtScrollHint;
    private LinearLayout mPanelEditMenu;
    public ScrollDoneView mBtScrollStop;
    private ScrollSavingView mScrollSaving;
    private boolean scrollMode = false;
    public boolean backFromScroll = false;

    public ScreenshotEditView(Context context, Bitmap image) {
        super(context);
        // TODO Auto-generated constructor stub
        mContext = this.getContext();
        mScreenshotEditLayout = inflate(getContext(), R.layout.crop, null);
        mCropView = new MyCropView(context, image, null);
        FrameLayout container = (FrameLayout) mScreenshotEditLayout.findViewById(R.id.container);
        container.addView(mCropView);

        mScrollButton = (ImageButton) mScreenshotEditLayout.findViewById(R.id.scroll_crop);
        mDrawButton = (ImageButton) mScreenshotEditLayout.findViewById(R.id.draw);
        mShareButton = (ImageButton) mScreenshotEditLayout.findViewById(R.id.share);
        mSaveButton = (ImageButton) mScreenshotEditLayout.findViewById(R.id.save);
        mCloseButton = (ImageButton) mScreenshotEditLayout.findViewById(R.id.bt_close);

        mTxtScrollHint = (TextView) mScreenshotEditLayout.findViewById(R.id.txt_hint);
        mPanelEditMenu = (LinearLayout) mScreenshotEditLayout.findViewById(R.id.panel_edit_menu);
        mBtScrollStop = (ScrollDoneView) mScreenshotEditLayout.findViewById(R.id.btn_scroll_stop);
        mScrollSaving = (ScrollSavingView) mScreenshotEditLayout.findViewById(R.id.hint_scroll_saving);

        setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        addView(mScreenshotEditLayout, getWindowLayoutParams());

        ScaleAnimation animation = new ScaleAnimation(1/0.86f, 1f, 1/0.86f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.0f);
        animation.setDuration(200);
        animation.setStartOffset(300);
        mCropView.setAnimation(animation);
        animation.startNow();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // TODO Auto-generated method stub
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() != KeyEvent.ACTION_UP) {
            if (scrollMode) {
                backFromScroll = true;
                dismissWindow();
                Intent stopIntent = new Intent(mContext, ScreenshotEditService.class);
                mContext.stopService(stopIntent);
            } else {
                if (mCropView != null && mCropView.hasCroped()) {
                    cancelCrop();
                } else {
                    dismissWindow();
                    Intent stopIntent = new Intent(mContext, ScreenshotEditService.class);
                    mContext.stopService(stopIntent);
                }
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void cancelCrop() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        builder.setTitle(R.string.discard_confirm_title)
               .setMessage(R.string.discard_confirm_msg)
               .setCancelable(false)
               .setNegativeButton(android.R.string.cancel,
                       new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       Log.i(TAG,"on confirm dialog cancel");
                   }
               })
               .setPositiveButton(R.string.discard_confirm_ok,
                       new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       Log.i(TAG,"on confirm dialog ok");
                       //Reset crop area to original
                       mCropView.resetCropArea();
                   }
               });

        Dialog mAlert = builder.create();
        mAlert.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);  //Very important, if not set dialog can not be create
        setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        mAlert.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mAlert.show();
    }

    public void updateCropView(String path) {
        scrollMode = false;
        FrameLayout container = (FrameLayout) mScreenshotEditLayout.findViewById(R.id.container);
        container.removeView(mCropView);
        mCropView = new MyCropView(this.getContext(), null, path);
        container.addView(mCropView);
    }

    public void updateScrollView() {
        scrollMode = true;
        FrameLayout container = (FrameLayout) mScreenshotEditLayout.findViewById(R.id.container);
        container.removeView(mCropView);
        mPanelEditMenu.setVisibility(View.GONE);
        mTxtScrollHint.setVisibility(View.VISIBLE);
        mBtScrollStop.setVisibility(View.VISIBLE);

        mMyScrollSreenshotView = inflate(mContext, R.layout.scrollscreenshot, container);
        mScreenshotEditLayout.setBackgroundColor(0xff212121);  //set background grey
    }

    public void updateScrollSavingView() {
        mTxtScrollHint.setVisibility(View.GONE);
        mBtScrollStop.setVisibility(View.GONE);
        mScrollSaving.setVisibility(View.VISIBLE);
    }

    public void showWindow() {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        wm.addView(this, getWindowLayoutParams());
    }

    public void dismissWindow() {
        Log.i(TAG, "dismissWindow isAttach=" + isAttachedToWindow());
        if (isAttachedToWindow()) {
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            wm.removeView(this);
        }
    }

    private WindowManager.LayoutParams getWindowLayoutParams() {
        WindowManager.LayoutParams param = new WindowManager.LayoutParams();

        param.type = WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG;

      //param.format = PixelFormat.TRANSLUCENT;
        param.format = PixelFormat.RGBA_8888;

        param.dimAmount= 0.8f;
        param.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED;
        param.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        param.flags = param.flags | WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION;

        // 以屏幕左上角为原点，设置x、y初始值
        param.x = 0;
        param.y = 0;

        // 设置悬浮窗口长宽数据
        param.width = WindowManager.LayoutParams.MATCH_PARENT;
        param.height = WindowManager.LayoutParams.MATCH_PARENT;

        return param;
    }

    public void temporaryHide(boolean bValue)
    {
        setVisibility(bValue ? View.INVISIBLE: View.VISIBLE);
    }
}
