package com.tct.screenshotedit;

import com.tct.screenshotedit.view.ColorSelectView;
import com.tct.screenshotedit.view.MosaicSelectView;
import com.tct.screenshotedit.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class DrawActivity extends Activity {

    private String TAG = "DrawActivity";
    private MyDrawView view;
    private Resources mResources;
    private LinearLayout colorPanel;
    private LinearLayout mosaicSizePanel;
    private ImageButton mUndoButton;
    private ImageButton mPenButton;
    private ImageButton mThickPenButton;
    private ImageButton mMarkPenButton;
    private ImageButton[] mPenButtons = new ImageButton[3];
    private ImageButton mMosaicButton;
    private ImageButton mCancelButton;
    private ImageButton mdoneButton;

    private boolean mScollImg = false;  //whether scroll screenshot image

    // Color Select Button
    private ColorSelectView[] mColorSelectViews = new ColorSelectView[7];

    private int[] mColorSelectViewIds = { R.id.ic_color_1, R.id.ic_color_2,
            R.id.ic_color_3, R.id.ic_color_4, R.id.ic_color_5, R.id.ic_color_6,
            R.id.ic_color_7 };

    private int[] mSelectViewColors = { 0xFFFF0000, 0xFFFFA500, 0xFFFFFF00,
            0xFF00FF00, 0xFF00FFFF, 0xFF0000FF, 0xFFFF00FF };

    // Mosaic Select Button
    private MosaicSelectView[] mMosaicSelectViews = new MosaicSelectView[5];

    private int[] mMosaicSelectViewIds = { R.id.ic_mosaic_size_1,
            R.id.ic_mosaic_size_2, R.id.ic_mosaic_size_3,
            R.id.ic_mosaic_size_4, R.id.ic_mosaic_size_5 };

    private float[] mMosaicSelectSizes = { 0.18f, 0.36f, 0.54f, 0.72f, 0.9f };

    private int[] mMosaicSizeIds = { R.dimen.draw_mosaic_size_1,
            R.dimen.draw_mosaic_size_2, R.dimen.draw_mosaic_size_3,
            R.dimen.draw_mosaic_size_4, R.dimen.draw_mosaic_size_5 };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        Log.i(TAG,"DrawActivity oncreate");

        mResources = this.getResources();
        setContentView(R.layout.draw);

        colorPanel = (LinearLayout) findViewById(R.id.color_panel);
        mosaicSizePanel = (LinearLayout) findViewById(R.id.mosaic_size_panel);
        mosaicSizePanel.setVisibility(View.GONE);

        Intent intent = getIntent();
        if (intent != null) {
            String imgPath = intent.getStringExtra("imagePath");
            mScollImg = intent.getBooleanExtra("scrollImg", false);
            LinearLayout container = (LinearLayout) findViewById(R.id.container);
            view = new MyDrawView(this, imgPath);
            container.addView(view);
        }

        mUndoButton = (ImageButton) findViewById(R.id.ic_menu_undo);
        mPenButton = (ImageButton) findViewById(R.id.ic_menu_pen);
        mPenButtons[0] = mPenButton;
        mThickPenButton = (ImageButton) findViewById(R.id.ic_menu_thick_pen);
        mPenButtons[1] = mThickPenButton;
        mMarkPenButton = (ImageButton) findViewById(R.id.ic_menu_mark_pen);
        mPenButtons[2] = mMarkPenButton;
        updatePenButton(0);

        mMosaicButton = (ImageButton) findViewById(R.id.ic_menu_pixelate);
        mCancelButton = (ImageButton) findViewById(R.id.ic_menu_cancel);
        mdoneButton = (ImageButton) findViewById(R.id.ic_menu_done);

        for (int i = 0; i < 7; i++) {
            mColorSelectViews[i] = (ColorSelectView) findViewById(mColorSelectViewIds[i]);
            mColorSelectViews[i].setColor(mSelectViewColors[i]);
            mColorSelectViews[i].setIndex(i);
            mColorSelectViews[i].setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = ((ColorSelectView) v).getIndex();
                    updateColorSelectView(index);
                    view.setColor(mSelectViewColors[index]);
                    mPenButtons[view.getDrawType()].setColorFilter(mSelectViewColors[index]);
                }
            });
        }
        updateColorSelectView(0);

        for (int i = 0; i < 5; i++) {
            mMosaicSelectViews[i] = (MosaicSelectView) findViewById(mMosaicSelectViewIds[i]);
            mMosaicSelectViews[i].setSize(mMosaicSelectSizes[i]);
            mMosaicSelectViews[i].setIndex(i);
            mMosaicSelectViews[i].setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = ((MosaicSelectView) v).getIndex();
                    updateMosaicSelectView(index);
                    view.setMosaicSize((int) mResources
                            .getDimension(mMosaicSizeIds[index]));
                }
            });
        }
        //Initial the mosaic size, default 14dp
        mMosaicButton.setAlpha(0.4f);
        updateMosaicSelectView(1);
        view.setMosaicSize((int) mResources
                .getDimension(mMosaicSizeIds[1]));

        mUndoButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"on click undo...");
                view.undo();
            }
        });

        for (int i = 0; i < 3; i++) {
            mPenButtons[i].setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(TAG, "on click pen...");
                    int index = getPenButtonType(v);
                    colorPanel.setVisibility(View.VISIBLE);
                    mosaicSizePanel.setVisibility(view.GONE);
                    updatePenButton(index);
                    mMosaicButton.setAlpha(0.4f);
                }
            });
        }

        mMosaicButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"on click mosaic...");
                view.setDrawType(MyDrawView.typeMosaic);
                colorPanel.setVisibility(View.GONE);
                mosaicSizePanel.setVisibility(view.VISIBLE);
                updatePenButton(MyDrawView.typeMosaic);
                mMosaicButton.setAlpha(1f);
            }
        });

        mCancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"on click cancel...");
                if (view.hasDraw) {
                    cancelDraw();
                } else {
                    DrawActivity.this.finish();
                    if (!mScollImg) {
                        startService(new Intent(DrawActivity.this, ScreenshotEditService.class));
                    }
                }
            }
        });

        mdoneButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"on click done...");
                if (view.hasDraw) {
                    view.save(mScollImg);
                }
                if (mScollImg) {
                    Intent mIntent = new Intent();
                    mIntent.putExtra("hasDraw", view.hasDraw);
                    setResult(RESULT_OK, mIntent);
                    DrawActivity.this.finish();
                } else {
                    DrawActivity.this.finish();
                    Intent intent = new Intent(DrawActivity.this, ScreenshotEditService.class);
                    intent.putExtra("change", true);
                    startService(intent);
                }
            }
        });
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // TODO Auto-generated method stub
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() != KeyEvent.ACTION_UP) {
            Log.i("TAG", "dispatchKeyEvent mScollImg="+mScollImg+" ,view.hasDraw="+view.hasDraw);
            if (view.hasDraw) {
                cancelDraw();
            } else {
                DrawActivity.this.finish();
                if (!mScollImg) {
                    startService(new Intent(DrawActivity.this, ScreenshotEditService.class));
                }
            }
        }
        return super.dispatchKeyEvent(event);
    }

    private void cancelDraw() {
        Log.i(TAG, "Draw Activity, cancelDraw()");
        AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);

        builder.setTitle(R.string.discard_confirm_title)
               .setMessage(R.string.discard_confirm_msg)
               .setCancelable(false)
               .setNegativeButton(android.R.string.cancel,
                       new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       Log.i(TAG,"on confirm dialog cancel");
                       dialog.dismiss();
                   }
               })
               .setPositiveButton(R.string.discard_confirm_ok,
                       new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       Log.i(TAG,"on confirm dialog ok");
                       dialog.dismiss();
                       DrawActivity.this.finish();
                       if (mScollImg) {
                           
                       } else {
                           startService(new Intent(DrawActivity.this, ScreenshotEditService.class));
                       }
                   }
               });

        Dialog mAlert = builder.create();
        mAlert.show();
    }

    private int getPenButtonType(View v) {
        for (int i = 0; i < 3; i++) {
            if (mPenButtons[i].getId() == v.getId()) {
                return i;
            }
        }
        return 0;
    }

    private void updatePenButton(int index) {
        view.setDrawType(index);
        Log.v("updatePenButton", "updatePenButton ----> " + index);
        for (int i = 0; i < 3; i++) {
            if (i == index) {
                mPenButtons[i].setColorFilter(view.getColor());
                mPenButtons[i].setAlpha(1f);
            } else {
                mPenButtons[i].clearColorFilter();
                mPenButtons[i].setAlpha(0.4f);
            }
        }
    }

    private void updateColorSelectView(int index) {
        for (int i = 0; i < 7; i++) {
            if (i == index) {
                mColorSelectViews[i].setColorSelected(true);
            } else {
                mColorSelectViews[i].setColorSelected(false);
            }
        }
    }

    private void updateMosaicSelectView(int index) {
        for (int i = 0; i < 5; i++) {
            if (i == index) {
                mMosaicSelectViews[i].setMosaicSelected(true);
            } else {
                mMosaicSelectViews[i].setMosaicSelected(false);
            }
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        view.recycleBmp();
    }
}
