package com.tct.screenshotedit.scroll;

import com.tct.screenshotedit.nativeimagemerge.BitmapDecoder;
import com.tct.screenshotedit.R;

import android.animation.ObjectAnimator;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.IBinder;
import android.util.FloatProperty;
import android.util.Property;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class DemoService extends Service{
    UIController mUIController = new UIController();
    private static float SCROLL_SPEED = 300; // 300 px per 1 second

    private DemoFloarview mLayoutDemoView;
    private TextView mMsgTest;
    private View mImageRoot;
    private ImageView mImageView;
    private ImageView mBottomImageView;
    private ScrollView mImageScroller;
    private ObjectAnimator mScrollAnimator = null;
    
    private IScrollScreenshot mScrollScreenshot;
    
    private Handler mHandler = new Handler();

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        CTools.PrintLogImage("demo-service onCreate()1");
        mLayoutDemoView = new DemoFloarview(this);
        mLayoutDemoView.showWindow();
        mScrollScreenshot = ScrollScreenshotPlayer.getInstance(this);

        mMsgTest = (TextView) mLayoutDemoView.findViewById(R.id.msgTest);
        mImageRoot = mLayoutDemoView.findViewById(R.id.image_root);
        mImageView = (ImageView) mLayoutDemoView.findViewById(R.id.image);
        mBottomImageView = (ImageView) mLayoutDemoView.findViewById(R.id.bottom_image);
        mImageScroller = (ScrollView) mLayoutDemoView.findViewById(R.id.image_scroller);

        mLayoutDemoView.btnStart.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                CTools.PrintLogImage("btnStart--clicked");
                //((ScrollScreenshotPlayer) mScrollScreenshot).performStartIM();
                mScrollScreenshot.startScrollScreenshot(mUIController);
            }
        });
        mLayoutDemoView.btnStop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CTools.PrintLogImage("btnStop--clicked");
                stopScrollAnimation();
                float height = mImageRoot.getHeight() + getScrollImageY();
                CTools.PrintLogImage("btnStop# height: " + height + ", tranY: " + getScrollImageY() + ", rootHeight: " + mImageRoot.getHeight());
                mScrollScreenshot.stopScrollScreenshot((int)height);
                stopSelf();
            }
        });
        mLayoutDemoView.btnTest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //mLayoutDemoView.TemporaryHide(true);
                mScrollScreenshot.canScrollScreenshot(new IScrollScreenshot.IOnCanScrollCallback() {
                    @Override
                    public void onCanScrollResult(boolean canScroll) {
                        final boolean fCanScroll = canScroll;
                        mHandler.post(new Runnable(){
                            @Override
                            public void run() {
                                mMsgTest.setText(String.valueOf(fCanScroll));
                            }});
                    }
                });
            }
        });
    }

    @Override
    public void onDestroy() {
        CTools.PrintLogImage("onDestory()");
        mLayoutDemoView.dismissWindow();
        super.onDestroy();
    }

    private void updateScrollAnimation() {
        if (mScrollAnimator != null) {
            mScrollAnimator.cancel();
        }

        float imageHeight = mImageView.getHeight();
        float rootViewHeight = mImageScroller.getHeight();
        
        if (imageHeight <= rootViewHeight) {
            return;
        }

        float currY = getScrollImageY();
        float destY = imageHeight - rootViewHeight;
        long duration = (long) (Math.abs(destY - currY) / SCROLL_SPEED * 1000);

        mScrollAnimator = ObjectAnimator.ofFloat(this, DemoService.SCROLL_IMAGE_Y, currY, destY);
        mScrollAnimator.setDuration(duration);
        mScrollAnimator.start();
    }
    
    private void stopScrollAnimation() {
        if (mScrollAnimator != null) {
            mScrollAnimator.cancel();
        }
    }

    public static final Property<DemoService, Float> SCROLL_IMAGE_Y = new FloatProperty<DemoService>("scrollImageY") {
        @Override
        public void setValue(DemoService object, float value) {
            object.setScrollImageY(value);
        }

        @Override
        public Float get(DemoService object) {
            return object.getScrollImageY();
        }
    };
    
    public void setScrollImageY(float scrollY) {
        mImageScroller.smoothScrollTo(0, (int)scrollY);
    }
    
    public float getScrollImageY() {
        return mImageScroller.getScrollY();
    }
    
    private class UIController implements IUIController {

        private Bitmap mMergedBitmap = null;
        private Bitmap mBottomBitmap = null;
        
        @Override
        public void onStart() {
            mImageScroller.scrollTo(0, 0);
        }

        @Override
        public void onBottomBitmapUpdate(Bitmap bitmap) {
            if (mBottomBitmap != null) {
                mBottomBitmap.recycle();
            }
            mBottomBitmap = bitmap;

            /*if (mBottomBitmap != null) {
                BitmapDecoder.writeImage(SSConfig.getInstance().PATH_IMG_BOTTOM, bitmap);
            }*/
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mBottomImageView.setImageBitmap(mBottomBitmap);
                }
            });
        }

        @Override
        public void onMergedBitmapUpdate(Bitmap bitmap) {
            if (mMergedBitmap != null) {
                mMergedBitmap.recycle();
            }
            mMergedBitmap = bitmap;
            
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mImageView.setImageBitmap(mMergedBitmap);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            updateScrollAnimation();
                        }
                    });
                }
            });
        }

        @Override
        public void onFinish() {
        }

        @Override
        public void onFinalBitmap(Bitmap bitmap) {
            if (bitmap != null) {
                BitmapDecoder.writeImage(SSConfig.getInstance().PATH_IMG_OUT, bitmap);
                mHandler.post(new Runnable(){
                    @Override
                    public void run() {
                        Toast.makeText(DemoService.this, "bitmap saved at: " + SSConfig.getInstance().PATH_IMG_OUT, Toast.LENGTH_SHORT).show();
                    }});
            } else {
                mHandler.post(new Runnable(){
                    @Override
                    public void run() {
                        Toast.makeText(DemoService.this, "bitmap is null", Toast.LENGTH_SHORT).show();
                    }});
            }
        }
    }
}
