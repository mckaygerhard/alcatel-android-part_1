package com.tct.screenshotedit.scroll;

import android.graphics.Bitmap;

/**
 * Interface for ScrollScreenshot to callback UI.<br>
 * NOTE: Methods are NOT called on UI thread.
 */
public interface IUIController {
    /**
     * Called when scroll screenshot is started.
     */
    public void onStart();

    /**
     * Update the fixed bottom bitmap.<br>
     * This bitmap is the part below scrollable area 
     * @param bitmap bottom bitmap
     */
    public void onBottomBitmapUpdate(Bitmap bitmap);

    /**
     * Update the latest merged bitmap.<br>
     * This method is called once another screen is merged. UI should update it's
     * display bitmap (bitmap shown to user) after this method is called.
     * @param bitmap Latest merged bitmap
     */
    public void onMergedBitmapUpdate(Bitmap bitmap);

    /**
     * Called when scroll screenshot is finished.
     */
    public void onFinish();
    
    public void onFinalBitmap(Bitmap bitmap);
}
