package com.tct.screenshotedit;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import java.io.FileOutputStream;

import com.tct.screenshotedit.impl.IOnImageScrolled;
import com.tct.screenshotedit.impl.onCropStateChanged;
import com.tct.screenshotedit.view.FullImageview;
import com.tct.screenshotedit.view.FullScrollView;
import com.tct.screenshotedit.view.Preview;

public class LongImageActivity extends Activity {

    private static final int LOAD_IMAGE = 1000;
    private String TAG = "LongImageActivity";
    private FullImageview imageView;
    private Preview mPreview;
    private FullScrollView mScrollView;

    private IOnImageScrolled callback;

    public static final float SCALE_VALUE = 0.725f;

    private float scale = 0.0f;
    private static int autoScrollStartY = 280;

    private Bitmap mSmallBitmap = null;
    private Bitmap mOriginalBitmap = null;

    private static int imageW;
    private static int imageH;

    private static int scrollViewH;

    private static boolean isAutoScroll = false;

    private static int cropStartY = 0;
    private static int cropEndY = 0;

    private Bitmap outBitmap;

    private String mImagePath;
    private Uri mImageUri;

    private ImageView imgSave;
    private ImageView imgDraw;
    private ImageView imgShare;
    private ImageView imgScroll;

    private boolean mHasDraw = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_longimage);
        imageView = (FullImageview) findViewById(R.id.imageView);
        mScrollView = (FullScrollView) findViewById(R.id.scrollView);
        mPreview = (Preview) findViewById(R.id.navigateImg);
        imgSave = (ImageView) findViewById(R.id.save);
        imgDraw = (ImageView) findViewById(R.id.draw);
        imgShare = (ImageView) findViewById(R.id.share);
        imgScroll = (ImageView) findViewById(R.id.scroll_crop);
        imgScroll.setAlpha(0.3f);

        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent();
                sharingIntent.setAction(Intent.ACTION_SEND);
                sharingIntent.putExtra(Intent.EXTRA_STREAM, mImageUri);
                sharingIntent.setType("image/png");

                Intent chooserItent = Intent.createChooser(sharingIntent, null)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(chooserItent);
            }
        });

        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveCropImage();
                finish();
            }
        });

        imgDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveCropImage();
                Intent drawIntent = new Intent(getBaseContext(), DrawActivity.class);
                //drawIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                drawIntent.putExtra("scrollImg", true);
                drawIntent.putExtra("imagePath", mImagePath);
                try {
                    startActivityForResult(drawIntent,0);
                    //startActivity(drawIntent);
                } catch (ActivityNotFoundException ex) {
                    Log.i(TAG, "activity miss...ex=" + ex.toString());
                } catch (Exception ex) {
                    Log.i(TAG, "other...ex=" + ex.toString());
                }
            }
        });

        callback = new IOnImageScrolled() {
            @Override
            public void onPreviewScroll(float level) {
                mScrollView.smoothScrollTo(0, (int) (level * off));
                isAutoScroll = true;
            }

        };

        mScrollView
                .setOnScrollChangeListener(new View.OnScrollChangeListener() {
                    @Override
                    public void onScrollChange(View view, int i, int mScrollY,
                            int i2, int i3) {
                        if (!isAutoScroll) {
                            Log.v("kobe", "mScrollY-------->" + mScrollY);
                            mPreview.updateRectWithOutFix(mScrollY / 14);
                        }

                    }
                });
        mScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                isAutoScroll = false;
                return false;
            }
        });

        imageView.setonCropStatus(new onCropStateChanged() {
            @Override
            public void onCropStateChanged(boolean status) {
                mScrollView.setCropMove(status);
            }

            @Override
            public void onCropAutoScrolled(int offset) {
                mScrollView.smoothScrollBy(0, offset);

            }

            @Override
            public void onCropRectUpdate(int top, int bottom) {

                cropStartY = top;
                cropEndY = bottom;
                mPreview.updateCropRect(top / 14, bottom / 14);
            }
        });
        if (this.getIntent() != null) {
            mImagePath = this.getIntent().getStringExtra("imagePath");
            mImageUri = this.getIntent().getData();
            Log.i("ScreenshotEdit", "Uri="+mImageUri);
            if (mImagePath == null) {
                this.finish();
            }
        }
        new LoadBitmapTask().execute(mImagePath);
    }

    private int off = 0;
    private Runnable ScrollRunnable = new Runnable() {
        @Override
        public void run() {
            off = imageView.getHeight() - mScrollView.getMeasuredHeight();
            if (off > 0) {
                mScrollView.smoothScrollBy(0, 5);
                mPreview.updateRect(5);
                if (mScrollView.getScrollY() == off) {
                    Log.d(TAG, "exit");
                    Thread.currentThread().interrupt();
                } else {
                    mHandler.postDelayed(this, 20);
                }
            }
        }
    };

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.what) {
            case 1:
                break;
            case LOAD_IMAGE:
                break;
            default:
                return false;
            }
            return false;
        }
    });

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            mHasDraw = data.getBooleanExtra("hasDraw", false);
            Log.i(TAG, "onActivityResult hasDraw="+mHasDraw);
        }
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "Long image onResume()");
        if (mHasDraw) {
            new LoadBitmapTask().execute(mImagePath);
            mHasDraw = false;
        }
        super.onResume();
    }

    public class LoadBitmapTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {

            try {
                mImagePath = params[0];
                // InputStream is = getAssets().open(params[0]);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = false;
                options.inSampleSize = 1;
                // mSrcBitmap = BitmapFactory.decodeStream(is, null, options);
                mOriginalBitmap = BitmapFactory.decodeFile(mImagePath, options);
                Matrix matrix = new Matrix();
                matrix.postScale(SCALE_VALUE, SCALE_VALUE); // 长和宽放大缩小的比例
                mSmallBitmap = Bitmap.createBitmap(mOriginalBitmap, 0, 0,
                        mOriginalBitmap.getWidth(),
                        mOriginalBitmap.getHeight(), matrix, true);
                imageH = mSmallBitmap.getHeight();
                imageW = mSmallBitmap.getWidth();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            scrollViewH = mScrollView.getMeasuredHeight();
            off = imageH - scrollViewH;
            int[] location = new int[2];
            mScrollView.getLocationOnScreen(location);
            int scrollViewY = location[1];
            imageView.setBackgroundColor(Color.GRAY);
            imageView.setImageBitmap(mSmallBitmap);
            imageView.setWH(imageView.getDrawable().getIntrinsicWidth(),
                    imageH, scrollViewH, scrollViewY);
            imageView.getParent().requestDisallowInterceptTouchEvent(true);

            mPreview.setOnScrollCallback(callback);
            mPreview.setMaxHeight(imageH / 14);
            mPreview.setMaxWidth(imageW / 2);
            scale = imageH / scrollViewH;
            mPreview.setScale(scale);
            mPreview.setImageBitmap(mSmallBitmap);
            mPreview.setScaleType(ImageView.ScaleType.FIT_XY);
        }
    }

    private void saveCropImage() {
        if (mSmallBitmap == null) {
            Log.i(TAG, "small bitmap is null which should not be happened!");
            return;
        }
        Log.d(TAG, "mSrcBitmap=" + mSmallBitmap.getHeight() + "cropStartY="
                + cropStartY + "cropEndY=" + cropEndY + "imageW=" + imageW);
        outBitmap = Bitmap.createBitmap(mOriginalBitmap, 0,
                (int) (cropStartY / SCALE_VALUE), mOriginalBitmap.getWidth(),
                (int) ((cropEndY - cropStartY) / SCALE_VALUE));

        try {
            FileOutputStream fos = new FileOutputStream(mImagePath);
            outBitmap.compress(CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            Log.i(TAG, "e=" + e.toString());
        }
    }

}
