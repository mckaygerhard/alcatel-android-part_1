package com.tct.screenshotedit.impl;


/**
 * Created by elvis on 16/10/15.
 */

public interface IOnImageScrolled {
    void onPreviewScroll(float level);
}
