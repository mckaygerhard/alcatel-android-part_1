package com.tct.screenshotedit.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ScrollSavingView extends View {
    private Paint mPaint;

    private int mBackgroundColor = Color.WHITE;
    private int mFgShapeColor = Color.BLACK;

    private float mWidth;

    public ScrollSavingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            this.setAlpha(0.7f);
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            this.setAlpha(1.0f);
        }
        return super.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        mWidth = getWidth();
        float center = (float) (mWidth * 0.5);
        float offsetX = (float) (mWidth * 0.1);
        float offsetY = (float) (mWidth * 0.6);

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(mBackgroundColor);
        canvas.drawCircle(center, center, center - 1, mPaint);

        mPaint.setTextSize(45);
        mPaint.setColor(mFgShapeColor);
        canvas.drawText("Saving", offsetX, offsetY, mPaint);

        super.onDraw(canvas);
    }
}
