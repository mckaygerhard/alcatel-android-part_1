package com.tct.screenshotedit.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class ColorSelectView extends View {

    private Context mContext;
    private Paint mPaint;

    private int index = 0;

    private int mBackgroundColor = Color.BLACK;
    private int mSelectEdgeColor = Color.WHITE;

    private boolean mSelected = false;
    private float mWidth;

    public ColorSelectView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        mContext = context;
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        mWidth = getWidth();
        float center = (float) (mWidth * 0.5);
        float innerCircle = (float) (mWidth * 0.46);
        float ringWidth = (float) (mWidth * 0.08);

        float line1SX = (float) (mWidth * 0.25);
        float line1SY = (float) (mWidth * 0.45);
        float line1EX = (float) (mWidth * 0.4);
        float line1EY = (float) (mWidth * 0.65);

        float line2SX = (float) (mWidth * 0.4 - ringWidth / 4.0 * Math.sqrt(2));
        float line2SY = (float) (mWidth * 0.65 + ringWidth / 4.0 * Math.sqrt(2));
        float line2EX = (float) (mWidth * 0.75);
        float line2EY = (float) (mWidth * 0.3875);

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(mBackgroundColor);
        canvas.drawCircle(center, center, center - 1, mPaint);

        if (mSelected) {
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(ringWidth);
            mPaint.setColor(mSelectEdgeColor);
            canvas.drawCircle(center, center, innerCircle, mPaint);

            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setStrokeWidth(ringWidth);
            mPaint.setColor(mSelectEdgeColor);
            canvas.drawLine(line1SX, line1SY, line1EX, line1EY, mPaint);
            canvas.drawLine(line2SX, line2SY, line2EX, line2EY, mPaint);

        }
        super.onDraw(canvas);
    }

    public void setColorSelected(boolean selected) {
        mSelected = selected;
        invalidate();
    }

    public void setColor(int color) {
        mBackgroundColor = color;
        invalidate();
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return this.index;
    }

}
