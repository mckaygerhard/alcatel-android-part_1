package com.tct.screenshotedit;

import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.IRotationWatcher;
import android.view.IWindowManager;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;

public class MyCropView extends View {

    private String TAG = "MyCropView";
    Bitmap bitmapOrig;
    private Bitmap bitmapCrop;
    private Canvas canvasCrop;
    private Paint cornerPaint = new Paint();
    private Paint framePaint = new Paint();
    float leftTopX;
    float leftTopY;
    float rightBottomX;
    float rightBottomY;
    float cornerLength = getResources().getDimension(R.dimen.crop_corner_length);
    float cornerWeight = getResources().getDimension(R.dimen.crop_corner_weight);
    int heightCropContainer = 0;
    int widthCropContainer = 0;
    private int widthBt = 0;
    private int heightBt = 0;
    private float displayScale = 0.725f;

    float orignalLeftTopX;
    float orignalLeftTopY;
    float orignalRightBottomX;
    float orignalRightBottomY;

    boolean isDragMode = false;
    boolean isCropLeftH = false; // Left horizontal crop
    boolean isCropRightH = false; // Right horizontal crop
    boolean isCropTopV = false; // Top vertical crop
    boolean isCropBottomV = false; // Bottom vertical crop

    public boolean haveCrop = false;

    private Display mDisplay;
    private DisplayMetrics mDisplayMetrics;

    public void loadBitmap(Bitmap image, String path) {
        try {
            Log.i(TAG,"loadBitmap() crop...");
            if (image == null) {
                bitmapOrig = BitmapFactory.decodeFile(path);
            } else {
                bitmapOrig = image;
            }

            if (bitmapOrig == null) {
                InputStream is = this.getResources().openRawResource(R.drawable.test);
                bitmapOrig = BitmapFactory.decodeStream(is);
            }

            widthBt = bitmapOrig.getWidth();
            heightBt = bitmapOrig.getHeight();

            mDisplay = ((WindowManager) this.getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            mDisplayMetrics = new DisplayMetrics();
            mDisplay.getRealMetrics(mDisplayMetrics);
            Log.i(TAG, "mDisplayMetrics height="+mDisplayMetrics.heightPixels+" ,width="+mDisplayMetrics.widthPixels+" ,orientation="+mDisplay.getRotation());

            heightCropContainer = (int) (mDisplayMetrics.heightPixels * displayScale);
            widthCropContainer = (int) (mDisplayMetrics.widthPixels * displayScale);

            Log.i(TAG ,"After scale containerHeight="+heightCropContainer+" ,containerWidth="+widthCropContainer);
            Log.i(TAG ,"bitmapOrig height="+bitmapOrig.getHeight()+" ,width="+bitmapOrig.getWidth());

            float scaleX = (float) (widthCropContainer) / widthBt;
            float scaleY = (float) (heightCropContainer) / heightBt;
            float scale = Math.min(scaleX, scaleY);

            widthBt = (int) (widthBt * scale);
            heightBt = (int) (heightBt * scale);
            Log.i(TAG, "width=" + widthBt + " ,height=" + heightBt
                    + " ,scaleX=" + scaleX + " ,scaleY=" + scaleY);

            bitmapCrop = Bitmap.createBitmap(mDisplayMetrics.widthPixels, heightCropContainer, Bitmap.Config.ARGB_8888);
            canvasCrop = new Canvas(bitmapCrop);

            leftTopX = (mDisplayMetrics.widthPixels - widthBt) / 2;
            leftTopY = (heightCropContainer - heightBt) / 2;

            bitmapOrig = Bitmap.createScaledBitmap(bitmapOrig, widthBt, heightBt, true);

            rightBottomX = leftTopX + widthBt;
            rightBottomY = leftTopY + heightBt;

            Log.i(TAG ,"bitmap After left="+leftTopX + " ,leftTopY="+leftTopY);
            canvasCrop.drawBitmap(bitmapOrig, leftTopX, leftTopY, null);

        } catch (Exception e) {
            Log.i(TAG, "load exception:" + e.toString());
        } catch (OutOfMemoryError e) {
            Log.i(TAG, "OutOfMemoryError ex:" + e.toString());
        }
    }

    /**
     * 获取手机stausbar高度
     * @param con
     * @return
     */
    public int getStatusBarH(Context con) {
        int result = 0;
        int resourceId = con.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = con.getResources().getDimensionPixelSize(resourceId);
        }
        if (result == 0) {
            Log.i(TAG, "error,How could StatusBarHeight be zero");
        }
        return result;
    }

    /**
     * Get navigation bar height
     * @param con
     * @return
     */
    public int getNavigationBarH(Context con) {
        int result = 0;
        int resourceId = con.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = con.getResources().getDimensionPixelSize(resourceId);
        }
        if (result == 0) {
            Log.i(TAG, "error,How could NavigationBarHeight be zero");
        }
        return result;
    }

    public MyCropView(Context context, Bitmap image, String path) {
        super(context);
        // TODO Auto-generated constructor stub
        loadBitmap(image, path);
        cornerPaint.setColor(Color.WHITE);
        cornerPaint.setStyle(Paint.Style.FILL);
        cornerPaint.setStrokeWidth(cornerWeight);

        framePaint.setColor(Color.WHITE);
        framePaint.setStyle(Paint.Style.STROKE);
        framePaint.setAlpha(50);
        framePaint.setStrokeWidth(getResources().getDimension(R.dimen.crop_border_weight));

        orignalLeftTopX = leftTopX;
        orignalLeftTopY = leftTopY;
        orignalRightBottomX = rightBottomX;
        orignalRightBottomY = rightBottomY;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            haveCrop = true;
            touch_start(x, y);
            break;
        case MotionEvent.ACTION_MOVE:
            touch_move(x, y);
            invalidate();
            break;
        case MotionEvent.ACTION_UP:
            touch_up(x, y);
            break;
        }
        return true;
    }

    private void isValidXY(float x, float y) {
        if (   (x > (leftTopX + cornerLength))
            && (x < (rightBottomX - cornerLength))
            && (y > (leftTopY + cornerLength))
            && (y < (rightBottomY - cornerLength))) {
            // In drag area, which can drag move crop area, and keep crop
            // size
            isDragMode = true;
            isCropLeftH = true;
            isCropRightH = true;
            isCropTopV = true;
            isCropBottomV = true;
        } else {
            // In crop area, set offset that can modify crop area size
            if (  x >= (leftTopX - cornerLength) && x <= (leftTopX + cornerLength)
               && y >= (leftTopY - cornerLength) && y <= (rightBottomY + cornerLength)) {
                isCropLeftH = true;
            }
            if (  x >= (rightBottomX - cornerLength) && x <= (rightBottomX + cornerLength)
               && y >= (leftTopY - cornerLength) && y <= (rightBottomY + cornerLength)) {
                isCropRightH = true;
            }
            if (  y >= (leftTopY - cornerLength) && y <= (leftTopY + cornerLength)
               && x >= (leftTopX - cornerLength) && x <= (rightBottomX + cornerLength)) {
                isCropTopV = true;
            }
            if (  y >= (rightBottomY - cornerLength) && y <= (rightBottomY + cornerLength)
               && x >= (leftTopX - cornerLength) && x <= (rightBottomX + cornerLength)) {
                isCropBottomV = true;
            }
        }
        Log.i(TAG, "isValidXY, isCropLeftH=" + isCropLeftH
                + " ,isCropRightH=" + isCropRightH + " ,isCropTopV="
                + isCropTopV + " ,isCropBottomV=" + isCropBottomV);
    }

    private float mX, mY;

    private void touch_start(float x, float y) {
        Log.i(TAG, "touch_start");
        mX = x;
        mY = y;
        isValidXY(x, y);
    }

    private void touch_move(float x, float y) {
        float dx = x - mX;
        float dy = y - mY;
        mX = x;
        mY = y;
        boolean isDragHOutOfRange = false;  //Out of range horizon
        boolean isDragVOutOfRange = false;  //Out of range vertical
        if (isDragMode) {
            if ((leftTopX + dx) < orignalLeftTopX
                    || (rightBottomX + dx) > orignalRightBottomX) {
                isDragHOutOfRange = true;
            }
            if ((leftTopY + dy) < orignalLeftTopY
                    || (rightBottomY + dy) > orignalRightBottomY) {
                isDragVOutOfRange = true;
            }
        }

        if ((isCropLeftH) && (!isDragHOutOfRange)) {
            if (isDragMode) {
                if ((leftTopX + dx) >= orignalLeftTopX) {
                    leftTopX += dx;
                }
            } else if (  ((leftTopX + dx) >= orignalLeftTopX)
                       && (leftTopX + dx) <= (rightBottomX - cornerLength * 3)) {
                leftTopX += dx;
            }
        }
        if ((isCropRightH) && (!isDragHOutOfRange)) {
            if (isDragMode) {
                if ((rightBottomX + dx) <= orignalRightBottomX) {
                    rightBottomX += dx;
                }
            } else if (  ((rightBottomX + dx) <= orignalRightBottomX)
                       && ((rightBottomX + dx) >= (leftTopX + cornerLength * 3))) {
                rightBottomX += dx;
            }
        }
        if ((isCropTopV) && (!isDragVOutOfRange)) {
            if (isDragMode) {
                if ((leftTopY + dy) >= orignalLeftTopY) {
                    leftTopY += dy;
                }
            } else if (  ((leftTopY + dy) >= orignalLeftTopY)
                       && (leftTopY + dy) <= (rightBottomY - cornerLength * 3)) {
                leftTopY += dy;
            }
        }
        if ((isCropBottomV) && (!isDragVOutOfRange)) {
            if (isDragMode) {
                if ((rightBottomY + dy) <= orignalRightBottomY) {
                    rightBottomY += dy;
                }
            } else if (  ((rightBottomY + dy) <= orignalRightBottomY)
                       && (rightBottomY + dy) >= (leftTopY + cornerLength * 3)) {
                rightBottomY += dy;
            }
        }
    }

    private void touch_up(float x, float y) {
        isCropLeftH = false;
        isCropRightH = false;
        isCropTopV = false;
        isCropBottomV = false;
        isDragMode = false;
    }

    public boolean hasCroped() {
        boolean result = false;
        if (   leftTopX != orignalLeftTopX
            || leftTopY != orignalLeftTopY
            || rightBottomX != orignalRightBottomX
            || rightBottomY != orignalRightBottomY) {
            result = true;
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        canvas.drawBitmap(bitmapCrop, 0, 0, null);
        drawCorner(canvas);
        drawFrame(canvas);

        canvas.save();
        canvas.clipRect(orignalLeftTopX, orignalLeftTopY, leftTopX, orignalRightBottomY);
        canvas.clipRect(orignalLeftTopX, orignalLeftTopY, orignalRightBottomX, leftTopY, Op.UNION);
        canvas.clipRect(rightBottomX, orignalLeftTopY, orignalRightBottomX, orignalRightBottomY, Op.UNION);
        canvas.clipRect(orignalLeftTopX, orignalRightBottomY, orignalRightBottomX, rightBottomY, Op.UNION);
        canvas.drawColor(0x55000000);
        canvas.restore();

    }

    private void drawFrame(Canvas canvas) {
        canvas.drawRect(new RectF(leftTopX, leftTopY, rightBottomX,rightBottomY), framePaint);
    }

    private void drawCorner(Canvas canvas) {
        float[] cornerLeftTop = { leftTopX + cornerLength, leftTopY, leftTopX,
                leftTopY, leftTopX, leftTopY, leftTopX, leftTopY + cornerLength };
        float[] cornerRightTop = { rightBottomX - cornerLength, leftTopY,
                rightBottomX, leftTopY, rightBottomX, leftTopY, rightBottomX,
                leftTopY + cornerLength };
        float[] cornerLeftBottom = { leftTopX + cornerLength, rightBottomY,
                leftTopX, rightBottomY, leftTopX, rightBottomY, leftTopX,
                rightBottomY - cornerLength };
        float[] cornerRightBottom = { rightBottomX - cornerLength,
                rightBottomY, rightBottomX, rightBottomY, rightBottomX,
                rightBottomY, rightBottomX, rightBottomY - cornerLength };
        canvas.drawLines(cornerLeftTop, cornerPaint);
        canvas.drawLines(cornerRightTop, cornerPaint);
        canvas.drawLines(cornerLeftBottom, cornerPaint);
        canvas.drawLines(cornerRightBottom, cornerPaint);
    }

    public void resetCropArea() {
        leftTopX = orignalLeftTopX;
        leftTopY = orignalLeftTopY;
        rightBottomX = orignalRightBottomX;
        rightBottomY = orignalRightBottomY;
        invalidate();
    }

    public void recycleBmp() {
        Log.i(TAG, "recycleBmp()");
        if (bitmapOrig != null && !bitmapOrig.isRecycled()) {
            bitmapOrig.recycle();
        }
        if (bitmapCrop != null && !bitmapCrop.isRecycled()) {
            bitmapCrop.recycle();
        }
    }
}
