package com.tct.screenshotedit.scroll;

import com.tct.internal.TCTLongScreenshotUtils;
import com.tct.screenshotedit.nativeimagemerge.ImageMerge;
import com.tct.screenshotedit.nativeimagemerge.NativeBitmap;

import android.app.Service;
import android.app.StatusBarManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.ViewConfiguration;
import android.view.WindowManager;

public class ScrollScreenshotPlayer implements IScrollScreenshot {

    private StatusBarManager sbm;
    private Context mContext;

    private static ScrollScreenshotPlayer instance = null;

    public static ScrollScreenshotPlayer getInstance(Context con) {
        if (instance == null) {
            instance = new ScrollScreenshotPlayer(con);
        }
        return instance;
    }

    public void recycle() {
        instance = null;
    }

    private ScrollScreenshotPlayer(Context con) {
        CTools.PrintLogImage("ScrollScreenshotPlayer()");

        mContext = con;

        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display mDisplay = wm.getDefaultDisplay();
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        mDisplay.getRealMetrics(mDisplayMetrics);
        int nScreenWidth = mDisplayMetrics.widthPixels;
        int nScreenHeight = mDisplayMetrics.heightPixels;
        int nScreenRotation = wm.getDefaultDisplay().getRotation();
        int nStatusbarHeight = getStatusBarH(mContext);
        int nNavigationBarHeight = getNavigationBarH(mContext);
        int nMovelen = nScreenHeight / 3;
        int nBottomPadding = 300;
        int nMoveDuration = 1000;
        int mTouchSlop = ViewConfiguration.get(mContext).getScaledTouchSlop();
        SSConfig.init(nScreenWidth, nScreenHeight, nScreenRotation, nStatusbarHeight, nNavigationBarHeight,
                nMovelen, nBottomPadding, nMoveDuration, mTouchSlop);

        // 禁止下拉状态栏
        sbm = (StatusBarManager) mContext.getSystemService(Context.STATUS_BAR_SERVICE);
        sbm.collapsePanels();

    };

    /**
     * 获取手机stausbar高度
     * @param con
     * @return
     */
    public static int getStatusBarH(Context con) {
        int result = 0;
        int resourceId = con.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = con.getResources().getDimensionPixelSize(resourceId);
        }
        if (result == 0) {
            throw new IllegalStateException("error,How could StatusBarHeight be zero");
        }
        return result;
    }

    /**
     * Get navigation bar height
     * @param con
     * @return
     */
    public static int getNavigationBarH(Context con) {
        int result = 0;
        int resourceId = con.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = con.getResources().getDimensionPixelSize(resourceId);
        }
        if (result == 0) {
            throw new IllegalStateException("error,How could NavigationBarHeight be zero");
        }
        return result;
    }

    private ScreenshotRunnable mScreenshotRunnable = null;
    private CanScrollRunnable mCanScrollRunnable = null;
    private class ScreenshotRunnable implements Runnable {
        private IUIController mIUIController = null;
        private int mStopHeight = -1;
        private boolean mIsFinished = false;
        private NativeBitmap mMergedBitmap = null;
        private NativeBitmap mBottomBitmap = null;
        private NativeBitmap mLastBitmap = null;

        public ScreenshotRunnable(IUIController controller) {
            mIUIController = controller;
        }

        public void stop(int height) {
            mStopHeight = height;
            if (mIsFinished) {
                notifyFinalBitmap(mStopHeight);
            }
        }

        public boolean isFinished() {
            return mIsFinished;
        }

        @Override
        public void run() {
            NativeBitmap tempBitmap = null;
            NativeBitmap oldMergedBitmap = null;
            try{
                CTools.PrintLogImage("ScreenshotRunnable.run()");
                ImageMerge imageMerge = new ImageMerge();

                if (mIUIController != null) {
                    mIUIController.onStart();
                }

                // get 1st screenshot
                Bitmap screenshotBmp = TCTLongScreenshotUtils.TCTCaptureTOPActivity(mContext);
                CTools.PrintLogImage("screenshotBmp: " + screenshotBmp);
                mMergedBitmap = NativeBitmap.createFromBitmap(screenshotBmp);
                mLastBitmap = NativeBitmap.create(mMergedBitmap);
                if(screenshotBmp != null) screenshotBmp.recycle();

                if (mIUIController != null) {
                    mIUIController.onMergedBitmapUpdate(mMergedBitmap.toBitmap());
                }

                int[] trimmed = new int[2];
                int distance = 0;
                for (int count = 0; count < SSConfig.MAX_SCROLL_TIME && (mStopHeight < 0); count ++) {

                    // scroll
                    TCTLongScreenshotUtils.TCTScrollForSShot(mContext, true, SSConfig.getInstance().getnMovelen());
                    waitForActivityAnimationFinish();

                    // get screenshot
                    screenshotBmp = TCTLongScreenshotUtils.TCTCaptureTOPActivity(mContext);
                    tempBitmap = NativeBitmap.createFromBitmap(screenshotBmp);
                    if(screenshotBmp != null) screenshotBmp.recycle();

                    // compare & get distance
                    distance = imageMerge.compareByFeature(mLastBitmap, tempBitmap, trimmed);
                    if (distance <= 0) {
                        break; // reach end
                    }

                    // do merge
                    oldMergedBitmap = mMergedBitmap;
                    mMergedBitmap = imageMerge.merge(mMergedBitmap, tempBitmap, trimmed[ImageMerge.INDEX_TRIM_TOP], trimmed[ImageMerge.INDEX_TRIM_BOTTOM], distance);
                    oldMergedBitmap.recycle();

                    // get bottom bitmap
                    if (mBottomBitmap != null) {
                        mBottomBitmap.recycle();
                    }

                    if (trimmed[ImageMerge.INDEX_TRIM_BOTTOM] == 0) {
                        mBottomBitmap = null;
                    } else {
                        mBottomBitmap = NativeBitmap.create(tempBitmap, tempBitmap.getHeight() - trimmed[ImageMerge.INDEX_TRIM_BOTTOM], tempBitmap.getHeight() - 1);
                    }
                    if (mIUIController != null) {
                        mIUIController.onMergedBitmapUpdate(mMergedBitmap.toBitmap());
                        mIUIController.onBottomBitmapUpdate((mBottomBitmap == null) ? null : mBottomBitmap.toBitmap());
                    }

                    // clean
                    mLastBitmap.recycle();
                    mLastBitmap = tempBitmap;

                }

                mLastBitmap.recycle();
                mLastBitmap = null;

                if (mIUIController != null) {
                    mIUIController.onFinish();
                }

                mIsFinished = true;

                if (mStopHeight > 0) {
                    notifyFinalBitmap(mStopHeight);
                }
            }finally{
                //avoid bitmap mem leak when press "back","recent" ...
                if (tempBitmap != null) {
                    tempBitmap.recycle();
                    tempBitmap = null;
                }
                if (oldMergedBitmap != null) {
                    oldMergedBitmap.recycle();
                    oldMergedBitmap = null;
                }
            }
        }

        private void waitForActivityAnimationFinish() {
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void notifyFinalBitmap(int height) {
            CTools.PrintLogImage("mMergedBitmap: " + mMergedBitmap + ", mBottomBitmap: " + mBottomBitmap);
            if (mMergedBitmap != null) {
                int mergedBmpHeight = mMergedBitmap.getHeight();
                int bottomBmpHeight = (mBottomBitmap == null) ? 0: mBottomBitmap.getHeight();
                height = Math.min(height, mergedBmpHeight);

                int bottomStartY;
                int bottomEndY;
                int startY;
                int endY;

                if (height < bottomBmpHeight) {
                    bottomStartY = 0;
                    bottomEndY = height - 1;
                    startY = 0;
                    endY = 0;
                } else {
                    bottomStartY = 0;
                    bottomEndY = bottomBmpHeight - 1;
                    startY = 0;
                    endY = height - bottomBmpHeight - 1;
                }

                ImageMerge imageMerge = new ImageMerge();
                NativeBitmap finalBmpBitmap;
                if (mBottomBitmap == null) {
                    finalBmpBitmap = NativeBitmap.create(mMergedBitmap, startY, endY);
                } else {
                    CTools.PrintLogImage("startY: " + startY + ", endY: " + endY + ", bottomStartY: " + bottomStartY + ", bottomEndY: " + bottomEndY);
                    finalBmpBitmap = imageMerge.merge2(mMergedBitmap, startY, endY, mBottomBitmap, bottomStartY, bottomEndY);
                    mBottomBitmap.recycle();
                    mBottomBitmap = null;
                }
                mMergedBitmap.recycle();
                mMergedBitmap = null;
                if (mIUIController != null) {
                    mIUIController.onFinalBitmap(finalBmpBitmap.toBitmap());
                }
                finalBmpBitmap.recycle();
            } else {
                if (mIUIController != null) {
                    mIUIController.onFinalBitmap(null);
                }
            }
        }
    }

    private class CanScrollRunnable implements Runnable {
        private IOnCanScrollCallback mIOnCanScrollCallback = null;
        private boolean mIsFinished = false;

        public CanScrollRunnable(IOnCanScrollCallback callback) {
            mIOnCanScrollCallback = callback;
        }

        public boolean isFinished() {
            return mIsFinished;
        }

        @Override
        public void run() {
            if (mIOnCanScrollCallback != null) {
                int nCan = TCTLongScreenshotUtils.TCTScrollForSShot(mContext, true, 0);
                CTools.PrintLogImage("==> nCan = " + nCan);
                mIOnCanScrollCallback.onCanScrollResult(nCan == 1);
                mIsFinished = true;
            }
        }
    }

    @Override
    public int startScrollScreenshot(IUIController controller) {
        if (mScreenshotRunnable != null && !mScreenshotRunnable.isFinished()) {
            return -1;
        }

        mScreenshotRunnable = new ScreenshotRunnable(controller);
        Thread thread = new Thread(mScreenshotRunnable);
        thread.start();
        return 0;
    }

    @Override
    public int stopScrollScreenshot(int height) {
        if (mScreenshotRunnable != null) {
            mScreenshotRunnable.stop(height);
        }
        return 0;
    }

    @Override
    public void canScrollScreenshot(IOnCanScrollCallback cb) {
        if (mCanScrollRunnable != null && !mCanScrollRunnable.isFinished()) {
            return;
        }

        mCanScrollRunnable = new CanScrollRunnable(cb);
        Thread thread = new Thread(mCanScrollRunnable);
        thread.start();
    }
}
