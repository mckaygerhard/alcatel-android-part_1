package com.tct.screenshotedit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.android.internal.statusbar.IStatusBarService;
import com.tct.screenshotedit.scroll.IScrollScreenshot;
import com.tct.screenshotedit.scroll.IUIController;
import com.tct.screenshotedit.scroll.ScrollScreenshotPlayer;
import com.tct.screenshotedit.view.ToastView;

import android.animation.ObjectAnimator;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;
import android.os.ServiceManager;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.FloatProperty;
import android.util.Log;
import android.util.Property;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceControl;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class ScreenshotEditService extends Service {

    private String TAG = "ScreenshotEditService";
    private ScreenshotEditView mScreenshotEditView;
    private ScrollScreenshotPlayer player;
    private Bitmap mScreenBitmap;
    private Display mDisplay;
    private DisplayMetrics mDisplayMetrics;
    private Matrix mDisplayMatrix;
    private Bitmap bitmapCrop;

    private String mImageFilePath;
    private Uri imageUri = null;
    private AsyncTask<Void, Void, Void> mSaveInBgTask;

    private int preRotate = 0;
    private static ToastView mToast = null;
    private boolean preRinging = false;
    private boolean preLock = false;

    private final static String ACTION_SCREENSHOT_SHARE = "com.screenshot.share";

    //For scroll screenshot
    UIController mUIController = new UIController();
    private ImageView mBottomImageView;
    private ScrollView mImageScroller;
    private TextView mTextHint;
    private View mImageRoot;
    private ImageView mImageView;
    private ObjectAnimator mScrollAnimator = null;
    private static float SCROLL_SPEED = 300; // 300 px per 1 second
    private boolean completeScroll = false;
    private boolean canScroll = false;
    private boolean unLockFromShare = false;

    private static final int SCREENSHOT_AUTO_SAVE_TIMEOUT = 2500;  //auto save after 2.5seconds
    private static final int MSG_SCREENSHOT_AUTO_SAVE = 0;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
            case MSG_SCREENSHOT_AUTO_SAVE:
                Log.i(TAG, "Handler msg auto save, start");
                if (mScreenshotEditView.mCropView.haveCrop || mScreenshotEditView.mCropView.hasCroped()) {
                    mHandler.removeMessages(MSG_SCREENSHOT_AUTO_SAVE);
                } else {
                    mScreenshotEditView.dismissWindow();
                    ScreenshotEditService.this.stopSelf();
                }
                break;
            default:
                super.handleMessage(msg);
                break;
            }
        }
    };

    /**
     * @return the current display rotation in degrees
     */
    private float getDegreesForRotation(int value) {
        switch (value) {
        case Surface.ROTATION_90:
            return 360f - 90f;
        case Surface.ROTATION_180:
            return 360f - 180f;
        case Surface.ROTATION_270:
            return 360f - 270f;
        }
        return 0f;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        Log.i(TAG, "ScreenshotEditService onCreate().....");

        // Take the screenshot
        mDisplay = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        mDisplayMetrics = new DisplayMetrics();
        mDisplay.getRealMetrics(mDisplayMetrics);

        float[] dims = {mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels};
        float degrees = getDegreesForRotation(mDisplay.getRotation());

        boolean requiresRotation = (degrees > 0);
        mDisplayMatrix = new Matrix();
        if (requiresRotation) {
            // Get the dimensions of the device in its native orientation
            mDisplayMatrix.reset();
            mDisplayMatrix.preRotate(-degrees);
            mDisplayMatrix.mapPoints(dims);
            dims[0] = Math.abs(dims[0]);
            dims[1] = Math.abs(dims[1]);
        }

        Vibrator vibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
        vibrator.vibrate(200);

        // Take the screenshot
        Log.i(TAG, "ScreenshotEdit take shot...dims[0]="+dims[0]+", dims[1]="+dims[1]);
        mScreenBitmap = SurfaceControl.screenshot((int) dims[0], (int) dims[1]);

        if (mScreenBitmap == null) {
            Log.i(TAG, "ScreenshotEdit take shot null!");
            return;
        }

        if (requiresRotation) {
            // Rotate the screenshot to the current orientation
            Bitmap ss = Bitmap.createBitmap(mDisplayMetrics.widthPixels,
                    mDisplayMetrics.heightPixels, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(ss);
            c.translate(ss.getWidth() / 2, ss.getHeight() / 2);
            c.rotate(degrees);
            c.translate(-dims[0] / 2, -dims[1] / 2);
            c.drawBitmap(mScreenBitmap, 0, 0, null);
            c.setBitmap(null);
            // Recycle the previous bitmap
            mScreenBitmap.recycle();
            mScreenBitmap = ss;
        }

        saveScreenshotImg(mScreenBitmap, false);
        mScreenshotEditView = new ScreenshotEditView(this, mScreenBitmap);
        mScreenshotEditView.showWindow();

        mScreenshotEditView.mScrollButton.setAlpha(0.3f);
        player = ScrollScreenshotPlayer.getInstance(this);
        player.canScrollScreenshot(new IScrollScreenshot.IOnCanScrollCallback() {
            @Override
            public void onCanScrollResult(boolean scroll) {
                canScroll = scroll;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "can scroll complete="+canScroll);
                        if (canScroll) {
                            mScreenshotEditView.mScrollButton.setAlpha(1f);
                        }
                    }});
            }
        });

        //Post delay to save auto.
        mHandler.removeMessages(MSG_SCREENSHOT_AUTO_SAVE);
        mHandler.sendEmptyMessageDelayed(MSG_SCREENSHOT_AUTO_SAVE, SCREENSHOT_AUTO_SAVE_TIMEOUT);

        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(Intent.ACTION_SCREEN_ON);
        mFilter.addAction(Intent.ACTION_USER_PRESENT);
        mFilter.addAction("android.intent.action.PHONE_STATE");
        //mFilter.addAction("com.android.alarmclock.ALARM_ALERT");
        mFilter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        mFilter.addAction(ACTION_SCREENSHOT_SHARE);
        registerReceiver(mCommonReceiver, mFilter);

        mScreenshotEditView.mCloseButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "on click close...");
                mHandler.removeMessages(MSG_SCREENSHOT_AUTO_SAVE);
                if (mScreenshotEditView.mCropView != null && mScreenshotEditView.mCropView.hasCroped()) {
                    mScreenshotEditView.cancelCrop();
                } else {
                    mScreenshotEditView.dismissWindow();
                    ScreenshotEditService.this.stopSelf();
                }
            }
        });

        mScreenshotEditView.mScrollButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "on click scroll...");
                mHandler.removeMessages(MSG_SCREENSHOT_AUTO_SAVE);
                if (!canScroll) {
                    if (mToast == null) {
                        mToast = ToastView.makeText(getBaseContext(), getString(R.string.toast_unsupport_scroll), Toast.LENGTH_SHORT);
                    } else {
                        mToast.setText(getString(R.string.toast_unsupport_scroll));
                        mToast.setDuration(Toast.LENGTH_SHORT);
                    }
                    mToast.show();
                } else {
                    mScreenshotEditView.updateScrollView();
                    player.startScrollScreenshot(mUIController);
                }
            }
        });

        mScreenshotEditView.mDrawButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "on click draw...");
                mHandler.removeMessages(MSG_SCREENSHOT_AUTO_SAVE);

                if (mScreenshotEditView.mCropView.hasCroped()) {
                    saveCropImage();
                }
                Intent drawIntent = new Intent(getBaseContext(), DrawActivity.class);
                //drawIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                drawIntent.putExtra("imagePath", mImageFilePath);
                drawIntent.putExtra("width", (int)(mScreenshotEditView.mCropView.rightBottomX - mScreenshotEditView.mCropView.leftTopX));
                drawIntent.putExtra("height", (int)(mScreenshotEditView.mCropView.rightBottomY - mScreenshotEditView.mCropView.leftTopY));
                try {
                    startActivity(drawIntent);
                    IStatusBarService.Stub.asInterface(
                              ServiceManager.getService(Context.STATUS_BAR_SERVICE)).collapsePanels();
                } catch (ActivityNotFoundException ex) {
                    Log.i(TAG, "activity miss...ex=" + ex.toString());
                } catch (Exception ex) {
                    Log.i(TAG, "other...ex=" + ex.toString());
                }
                mScreenshotEditView.setVisibility(View.INVISIBLE);
                preRotate = getLandPortFromRotation(mDisplay.getRotation());
            }
        });

        mScreenshotEditView.mShareButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"on click share...");
                mHandler.removeMessages(MSG_SCREENSHOT_AUTO_SAVE);

                KeyguardManager mKeyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
                boolean lock = mKeyguardManager.inKeyguardRestrictedInputMode();
                if (lock) {
                    unLockFromShare = true;
                    mScreenshotEditView.setVisibility(View.INVISIBLE);
                } else {
                    //mScreenshotEditView.mCropView.isShare = true;
                    //mScreenshotEditView.mCropView.invalidate();
                    sendShareAction();
                }
            }
        });

        mScreenshotEditView.mSaveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG,"on click save...");
                mHandler.removeMessages(MSG_SCREENSHOT_AUTO_SAVE);
                saveCropImage();
                mScreenshotEditView.dismissWindow();
                ScreenshotEditService.this.stopSelf();
            }
        });
    }

    /**
     * used to send share intent.
     */
    private void sendShareAction() {
        Intent sharingIntent = new Intent();
        sharingIntent.setAction(Intent.ACTION_SEND);
        sharingIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        sharingIntent.setType("image/png");

        PendingIntent chooseAction = PendingIntent.getBroadcast(getBaseContext(), 0,
                new Intent(ACTION_SCREENSHOT_SHARE),
                PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        Intent chooserItent = Intent.createChooser(sharingIntent, null,
                chooseAction.getIntentSender())
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);;
        chooserItent.putExtra("screen_shot", true);
        startActivity(chooserItent);
    }

    /**
     * POD used in the AsyncTask which saves an image in the background.
     */
    class SaveImageInBackgroundData {
        Bitmap image;
        Boolean scrollImg;

        void clearImage() {
            image = null;
        }
    }

    /**
     * An AsyncTask that saves an image to the media store in the background.
     */
    class SaveImageInBackgroundTask extends AsyncTask<Void, Void, Void> {

        private static final String SCREENSHOTS_DIR_NAME = "Screenshots";
        private static final String SCREENSHOT_FILE_NAME_TEMPLATE = "Screenshot_%s.png";
        private static final String SCREENSHOT_SHARE_SUBJECT_TEMPLATE = "Screenshot (%s)";

        private final SaveImageInBackgroundData mParams;
        private final File mScreenshotDir;
        private final String mImageFileName;
        private final long mImageTime;

        private final int mImageWidth;
        private final int mImageHeight;

        SaveImageInBackgroundTask(SaveImageInBackgroundData data) {

            Log.i(TAG, "Ansyn SaveImageInBackgroundTask");
            // Prepare all the output metadata
            mParams = data;
            mImageTime = System.currentTimeMillis();
            String imageDate = new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date(mImageTime));
            mImageFileName = String.format(SCREENSHOT_FILE_NAME_TEMPLATE, imageDate);
            mScreenshotDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), SCREENSHOTS_DIR_NAME);
            mImageFilePath = new File(mScreenshotDir, mImageFileName).getAbsolutePath();

            mImageWidth = data.image.getWidth();
            mImageHeight = data.image.getHeight();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // By default, AsyncTask sets the worker thread to have background thread priority, so bump
            // it back up so that we save a little quicker.
            Log.i(TAG, "Ansyn doInBackground");
            Process.setThreadPriority(Process.THREAD_PRIORITY_FOREGROUND);

            Bitmap image = mParams.image;
            try {
                if (mParams.scrollImg && imageUri != null) {
                    //Firstly, delete the common screenshot if save scroll screenshot
                    ContentResolver resolver = getContentResolver();
                    resolver.delete(imageUri, null, null);
                }

                // Create screenshot directory if it doesn't exist
                mScreenshotDir.mkdirs();

                // media provider uses seconds for DATE_MODIFIED and DATE_ADDED, but milliseconds
                // for DATE_TAKEN
                long dateSeconds = mImageTime / 1000;

                // Save
                OutputStream out = new FileOutputStream(mImageFilePath);
                image.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();

                // Save the screenshot to the MediaStore
                ContentValues values = new ContentValues();
                ContentResolver resolver = getContentResolver();
                values.put(MediaStore.Images.ImageColumns.DATA, mImageFilePath);
                values.put(MediaStore.Images.ImageColumns.TITLE, mImageFileName);
                values.put(MediaStore.Images.ImageColumns.DISPLAY_NAME, mImageFileName);
                values.put(MediaStore.Images.ImageColumns.DATE_TAKEN, mImageTime);
                values.put(MediaStore.Images.ImageColumns.DATE_ADDED, dateSeconds);
                values.put(MediaStore.Images.ImageColumns.DATE_MODIFIED, dateSeconds);
                values.put(MediaStore.Images.ImageColumns.MIME_TYPE, "image/png");
                values.put(MediaStore.Images.ImageColumns.WIDTH, mImageWidth);
                values.put(MediaStore.Images.ImageColumns.HEIGHT, mImageHeight);
                values.put(MediaStore.Images.ImageColumns.SIZE, new File(mImageFilePath).length());
                imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                mParams.image = null;
            } catch (Exception e) {
                // IOException/UnsupportedOperationException may be thrown if external storage is not
                // mounted
                Log.i(TAG, "Exception when save ex:" + e.toString());
                mParams.clearImage();
            }

            // Recycle the bitmap data
            if (image != null) {
                image.recycle();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void params) {
            Log.i(TAG, "Ansyn onPostExecute isScrollImg=" + mParams.scrollImg +" ,Uri="+imageUri);
            if (mParams.scrollImg) {
                Log.i(TAG, "start scroll crop activity");
                Intent longImageIntent = new Intent(getBaseContext(), LongImageActivity.class);
                //drawIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                longImageIntent.putExtra("imagePath", mImageFilePath);
                longImageIntent.setData(imageUri);
                try {
                    startActivity(longImageIntent);
                } catch (ActivityNotFoundException ex) {
                    Log.i(TAG, "activity miss...ex=" + ex.toString());
                } catch (Exception ex) {
                    Log.i(TAG, "other...ex=" + ex.toString());
                }
                mScreenshotEditView.dismissWindow();
                ScreenshotEditService.this.stopSelf();
            }
        }

        @Override
        protected void onCancelled(Void params) {
            Log.i(TAG, "Ansyn onCancelled");
            mParams.clearImage();
        }
    }

    private void saveScreenshotImg(Bitmap bmp, boolean scrollImg) {
        SaveImageInBackgroundData data = new SaveImageInBackgroundData();
        data.image = bmp;
        data.scrollImg = scrollImg;
        mSaveInBgTask = new SaveImageInBackgroundTask(data).execute();
    }

    private void saveCropImage() {
        Log.i(TAG,
                "saveCropImage()"
                        + " ,view width=" + ((int) (mScreenshotEditView.mCropView.rightBottomX - mScreenshotEditView.mCropView.leftTopX)) + " ,height="
                        + ((int) (mScreenshotEditView.mCropView.rightBottomY - mScreenshotEditView.mCropView.leftTopY)));

        int offsetX = (int) (mScreenshotEditView.mCropView.leftTopX - mScreenshotEditView.mCropView.orignalLeftTopX);
        int offsetY = (int) (mScreenshotEditView.mCropView.leftTopY - mScreenshotEditView.mCropView.orignalLeftTopY);
        if (offsetX < 0) {
            offsetX = 0;
        }
        if (offsetY < 0) {
            offsetY = 0;
        }
        bitmapCrop = Bitmap.createBitmap(mScreenshotEditView.mCropView.bitmapOrig, offsetX, offsetY,
                (int) (mScreenshotEditView.mCropView.rightBottomX - mScreenshotEditView.mCropView.leftTopX),
                (int) (mScreenshotEditView.mCropView.rightBottomY - mScreenshotEditView.mCropView.leftTopY));

        try {
            FileOutputStream fos = new FileOutputStream(mImageFilePath);
            bitmapCrop.compress(CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            Log.i(TAG, "e=" + e.toString());
        }
    }

    /**
     * @return the current display land or port.
     * 0 is port, 1 is land.
     */
    private int getLandPortFromRotation(int value) {
        switch (value) {
        case Surface.ROTATION_90:
            return 1;
        case Surface.ROTATION_180:
            return 0;
        case Surface.ROTATION_270:
            return 1;
        }
        return 0;
    }

    private BroadcastReceiver mCommonReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(TAG, "Receiver action="+action + " ,preLock=" + preLock + " ,preRinging=" + preRinging + " ,unLockFromShare=" + unLockFromShare);
            if (mScreenshotEditView.getVisibility() != View.VISIBLE && !unLockFromShare) {
                Log.i(TAG, "Receiver return!");
                return;
            }
            if (action.equals(ACTION_SCREENSHOT_SHARE)) {
                //Remove screenshot view after a share target is chosen.
                try {
                    IStatusBarService.Stub.asInterface(
                            ServiceManager.getService(Context.STATUS_BAR_SERVICE)).collapsePanels();
                } catch (Exception ex) {
                    Log.i(TAG, "collapsePanels exception : " + ex.toString());
                }
                mScreenshotEditView.dismissWindow();
                ScreenshotEditService.this.stopSelf();
            } else if (action.equals("android.intent.action.PHONE_STATE")) {
                TelephonyManager tm = (TelephonyManager)getSystemService(Service.TELEPHONY_SERVICE);
                Log.i(TAG, "isRinging ="+tm.isRinging()+" , callState="+tm.getCallState());
                if (tm.isRinging() && !preRinging) {
                    mScreenshotEditView.dismissWindow();
                    ScreenshotEditService.this.stopSelf();
                } else if (!tm.isRinging()) {
                    preRinging = false;
                }
            } else if (action.equals(Intent.ACTION_SCREEN_ON)) {
                KeyguardManager mKeyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
                boolean lock = mKeyguardManager.inKeyguardRestrictedInputMode();
                if (lock && !preLock) {
                    mScreenshotEditView.dismissWindow();
                    ScreenshotEditService.this.stopSelf();
                }
            } else if (action.equals(Intent.ACTION_USER_PRESENT)) {
                if (unLockFromShare) {
                    mScreenshotEditView.setVisibility(View.VISIBLE);
                    sendShareAction();
                }
            }  else if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
                String reason = intent.getStringExtra("reason");
                if ("homekey".equals(reason) || "recentapps".equals(reason)) {
                    Log.i(TAG, "home or recent key pressed");
                    mScreenshotEditView.dismissWindow();
                    ScreenshotEditService.this.stopSelf();
                }
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        Log.i(TAG,"ScreenshotEditService onStartCommand() rotate=" +mDisplay.getRotation());

        if ((mScreenshotEditView != null) && (mScreenshotEditView.getVisibility() == View.INVISIBLE)) {
            mScreenshotEditView.setVisibility(View.VISIBLE);
            mScreenshotEditView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN|View.SYSTEM_UI_FLAG_FULLSCREEN);
            if ((intent != null) && (intent.getBooleanExtra("change", false)) || preRotate != getLandPortFromRotation(mDisplay.getRotation())) {
                mScreenshotEditView.mScrollButton.setAlpha(0.3f);
                canScroll = false;
                mScreenshotEditView.updateCropView(mImageFilePath);
            }
        }

        KeyguardManager mKeyguardManager = (KeyguardManager) this.getSystemService(Context.KEYGUARD_SERVICE);
        boolean lock = mKeyguardManager.inKeyguardRestrictedInputMode();
        if (lock) {
            //Display close button due to maybe no way to return
            mScreenshotEditView.mCloseButton.setVisibility(View.VISIBLE);
            preLock = true;
        } else {
            mScreenshotEditView.mCloseButton.setVisibility(View.GONE);
            preLock = false;
        }

        TelephonyManager tm = (TelephonyManager)getSystemService(Service.TELEPHONY_SERVICE);
        if (tm.isRinging()) {
            preRinging = true;
        } else {
            preRinging = false;
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.i(TAG,"ScreenshotEditService onDestroy() backFromScroll=" + mScreenshotEditView.backFromScroll);
        mHandler.removeMessages(MSG_SCREENSHOT_AUTO_SAVE);
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }

        if (mCommonReceiver != null) {
            this.getBaseContext().unregisterReceiver(mCommonReceiver);
        }

        if (mScreenshotEditView.mCropView != null) {
            mScreenshotEditView.mCropView.recycleBmp();
        }

        if (mScreenshotEditView.backFromScroll && mScrollAnimator != null && mScrollAnimator.isRunning()) {
            stopScrollAnimation();
            float height = mImageRoot.getHeight() + getScrollImageY();
            Log.i(TAG, "btnStop# height: " + height + ", tranY: " + getScrollImageY() + ", rootHeight: " + mImageRoot.getHeight());
            player.stopScrollScreenshot((int)height);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    private void updateScrollAnimation() {
        if (mScrollAnimator != null) {
            mScrollAnimator.cancel();
        }

        float imageHeight = mImageView.getHeight();
        float rootViewHeight = mImageScroller.getHeight();

        Log.i(TAG, "updateScrollAnimation imageHeight="+imageHeight+" ,rootViewHeight="+rootViewHeight);
        if (imageHeight <= rootViewHeight) {
            return;
        }

        float currY = getScrollImageY();
        float destY = imageHeight - rootViewHeight;
        long duration = (long) (Math.abs(destY - currY) / SCROLL_SPEED * 1000);

        Log.i(TAG, "updateScrollAnimation currY:"+currY+" ,destY="+destY);
        mScrollAnimator = ObjectAnimator.ofFloat(this, ScreenshotEditService.SCROLL_IMAGE_Y, currY, destY);
        mScrollAnimator.setDuration(duration);
        mScrollAnimator.setInterpolator(new LinearInterpolator());

        /*mScrollAnimator.addListener(new AnimatorListenerAdapter() {//便利类，只要实现需要的方法
            @Override
            public void onAnimationEnd(Animator animation) {
                Log.i(TAG, "updateScrollAnimation end...");
                super.onAnimationCancel(animation);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                // TODO Auto-generated method stub
                Log.i(TAG, "updateScrollAnimation cancel...");
                super.onAnimationCancel(animation);
            }
        });*/
        mScrollAnimator.start();
    }

    private void stopScrollAnimation() {
        if (mScrollAnimator != null) {
            mScrollAnimator.cancel();
        }
    }

    public static final Property<ScreenshotEditService, Float> SCROLL_IMAGE_Y = new FloatProperty<ScreenshotEditService>("scrollImageY") {
        @Override
        public void setValue(ScreenshotEditService object, float value) {
            object.setScrollImageY(value);
        }

        @Override
        public Float get(ScreenshotEditService object) {
            return object.getScrollImageY();
        }
    };

    public void setScrollImageY(float scrollY) {
        Log.i("==MyTest==", "setScrollImageY()# scrollY: " + scrollY);
        mImageScroller.scrollTo(0, (int)scrollY);
    }

    public float getScrollImageY() {
        return mImageScroller.getScrollY();
    }

    private class UIController implements IUIController {

        private Bitmap mMergedBitmap = null;
        private Bitmap mBottomBitmap = null;
        private boolean isStop = false;

        @Override
        public void onStart() {
            mImageRoot = mScreenshotEditView.mMyScrollSreenshotView.findViewById(R.id.image_root);
            mImageView = (ImageView) mScreenshotEditView.mMyScrollSreenshotView.findViewById(R.id.image);
            mBottomImageView = (ImageView) mScreenshotEditView.mMyScrollSreenshotView.findViewById(R.id.bottom_image);
            mImageScroller = (ScrollView) mScreenshotEditView.mMyScrollSreenshotView.findViewById(R.id.image_scroller);
            mImageScroller.scrollTo(0, 0);

            float transY = (getResources().getDisplayMetrics().heightPixels) * (1 - 0.725f) / 2;
            mImageRoot.setTranslationY(0 - transY);

            mScreenshotEditView.mBtScrollStop.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(TAG, "onClick btnStop()");
                    isStop = true;
                    stopScrollAnimation();
                    mScreenshotEditView.updateScrollSavingView();
                    float height = mImageRoot.getHeight() + getScrollImageY();
                    Log.i(TAG, "btnStop# height: " + height + ", tranY: " + getScrollImageY() + ", rootHeight: " + mImageRoot.getHeight());
                    player.stopScrollScreenshot((int)height);
                }
            });
        }

        @Override
        public void onBottomBitmapUpdate(Bitmap bitmap) {
            if (isStop) {
                return;
            }
            if (mBottomBitmap != null) {
                mBottomBitmap.recycle();
            }
            mBottomBitmap = bitmap;

            /*if (mBottomBitmap != null) {
                BitmapDecoder.writeImage(SSConfig.getInstance().PATH_IMG_BOTTOM, bitmap);
            }*/
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mBottomBitmap != null) {
                        Log.i(TAG, "onBottomBitmapUpdate, width="+mBottomBitmap.getWidth()+" ,height="+mBottomBitmap.getHeight());
                    }
                    mBottomImageView.setImageBitmap(mBottomBitmap);
                }
            });
        }

        @Override
        public void onMergedBitmapUpdate(Bitmap bitmap) {
            if (isStop) {
                return;
            }
            if (mMergedBitmap != null) {
                mMergedBitmap.recycle();
            }
            mMergedBitmap = bitmap;

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "onMergedBitmapUpdate, width="+mMergedBitmap.getWidth()+" ,height="+mMergedBitmap.getHeight());
                    mImageView.setImageBitmap(mMergedBitmap);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            updateScrollAnimation();
                        }
                    });
                }
            });
        }

        @Override
        public void onFinish() {
            Log.i(TAG, "onFinish()");
            //completeScroll = true;
        }

        @Override
        public void onFinalBitmap(Bitmap bitmap) {
            Log.i(TAG, "onFinalBitmap backFromScroll=" + mScreenshotEditView.backFromScroll);
            if (mScreenshotEditView.backFromScroll) {
                //Not save, just back
                Log.i(TAG, "Click back from scroll, not save, just back!");
            } else {
                if (bitmap != null) {
                    saveScreenshotImg(bitmap, true);
                } else {
                    mHandler.post(new Runnable(){
                        @Override
                        public void run() {
                            Log.i(TAG, "onFinalBitmap BitMap is null!");
                            ToastView.makeText(ScreenshotEditService.this, getString(R.string.error_hint), Toast.LENGTH_SHORT).show();
                        }});
                }
            }
        }
    }
}
