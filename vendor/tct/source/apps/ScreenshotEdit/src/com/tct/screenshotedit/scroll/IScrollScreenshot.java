package com.tct.screenshotedit.scroll;

/**
 * Interface for ScrollScreenshot API
 */
public interface IScrollScreenshot {
    /**
     * Result code for OK.
     */
    static final int RESULT_OK = 0;

    /**
     * Result code for unknown error
     */
    static final int RESULT_ERROR_UNKNOWN = 1;

    /**
     * Callback for canScroll()
     */
    interface IOnCanScrollCallback {
        /**
         * Callback method
         * @param canScroll true: can scroll
         */
        void onCanScrollResult(boolean canScroll);
    }

    /**
     * Start scroll screenshot.<br>
     * NOTE: This is an async method.
     * @param controller UI controller for scroll screenshot to callback
     * @return result code. see: {@link #RESULT_OK}, {@link #RESULT_ERROR_UNKNOWN}
     */
    int startScrollScreenshot(IUIController controller);

    /**
     * Stop scroll screenshot.<br>
     * NOTE: This is an async method.
     * @param height the height of bitmap to be saved
     * @return
     */
    int stopScrollScreenshot(int height);

    /**
     * Check whether current activity can be scrolled.
     */
    void canScrollScreenshot(IOnCanScrollCallback cb);
}
