package com.tct.screenshotedit.nativeimagemerge;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.tct.screenshotedit.scroll.CTools;

/**
 * Decoder
 */
public class BitmapDecoder {
    public static NativeBitmap readImage(String path) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeFile(path);
            return NativeBitmap.createFromBitmap(bitmap);
        } finally {
            if (bitmap != null) {
                bitmap.recycle();
            }
        }
    }

    public static NativeBitmap readImage(Bitmap bitmap) {
        return NativeBitmap.createFromBitmap(bitmap);
    }

    public static void writeImage(String path, NativeBitmap bmp) {
        FileOutputStream fos = null;
        Bitmap bitmap = null;
        try {
            File file = new File(path);
            fos = new FileOutputStream(file);

            bitmap = bmp.toBitmap();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (bitmap != null) {
                bitmap.recycle();
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressWarnings("unused")
    public static void writeImage(String path, Bitmap bmp) {
        FileOutputStream fos = null;
        try {
            File file = new File(path);
            fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
            CTools.PrintLogImage("writeImage : path = " + path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
