LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_PACKAGE_NAME := ScreenshotEdit
LOCAL_CERTIFICATE := platform
LOCAL_JNI_SHARED_LIBRARIES := libImageMerge
LOCAL_STATIC_JAVA_LIBRARIES := android-support-v4
#include $(BUILD_PLF)
include $(BUILD_PACKAGE)

include $(call all-makefiles-under,$(LOCAL_PATH))
