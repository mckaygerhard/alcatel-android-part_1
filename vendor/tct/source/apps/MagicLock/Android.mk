LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

LOCAL_STATIC_JAVA_LIBRARIES := \
        android-support-v4
        # android-common \
        # guava
#add by dongdong.wang to fix task 1845428 at 2016-03-21 begin
#LOCAL_JAVA_LIBRARIES := tct.feature_query
#add by dongdong.wang to fix task 1845428 at 2016-03-21 end

LOCAL_PACKAGE_NAME := MagicLock

LOCAL_SRC_FILES := \
        $(call all-java-files-under, src)

LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res

include frameworks/base/packages/SettingsLib/common.mk

include $(BUILD_PACKAGE)

#LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := \
#   libuniversalimageloader:libs/universal-image-loader-1.9.4-with-sources.jar

# the follow line is necessary for private libs.
# include $(BUILD_MULTI_PREBUILT)

# additionally, build tests in sub-folders in a separate .apk
#include $(call all-makefiles-under,$(LOCAL_PATH))
