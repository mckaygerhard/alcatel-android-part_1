package com.tct.magiclock.activity;

import android.content.ContentResolver;
import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.tct.magiclock.db.MagiclockProvider.FavoriteView;
import com.tct.magiclock.db.MagiclockProvider.Picture;
import com.tct.magiclock.Constants;
import com.tct.magiclock.model.ItemInfo;
import com.tct.magiclock.R;

import java.io.File;
import android.net.Uri;
import android.provider.MediaStore;

import android.content.ContentValues;
import com.tct.magiclock.util.DataCollectUtil;

public class MagiclockFavoritesActivity extends MagicLockBaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "FavoritesActivity";

    private Intent intent;
    private boolean isLaucher = false;//Add by dongdong.wang@tcl.com 2015-12.03 for task 1021515
    private View mTipsLayout = null;
    // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.03 Begin
    /**
     * The picture tips, view id is {@link R.id#amm_no_favorite_icon}.
     * It's default visibility is gone. we will change it to visible when data load finished.
     */
    private View mTipsIcon = null;
    // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.03 End
    /**
     * true means should set {@link #mTipsLayout} to visible when resume activity.
     * <p>
     * We modify {@link #isShowTips}'s value at {@link #onLoadFinished(Loader, Cursor)}.
     * And use it ad {@link #onResume()}.
     * </p>
     */
    private boolean isShowTips = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        findViewById(R.id.amf_delete_mypicture).setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(MagiclockFavoritesActivity.this, MagicLockPictureDeleteActivity.class);
//                intent.putExtra(MagicLockPictureDeleteActivity.ACTION_KEY, MagicLockPictureDeleteActivity.ACTION_OP_FAVORITES);
//                startActivity(intent);
//            }
//        });
        mTipsLayout = findViewById(R.id.amf_no_favorite);
        mTipsLayout.setVisibility(View.VISIBLE);
        mNoContentTips = (TextView) findViewById(R.id.amm_no_favorite_msg);
        // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.03 Begin
        // Show loading tips for user when open this interface.
        mNoContentTips.setText(R.string.magiclock_loading);
        mTipsIcon = findViewById(R.id.amm_no_favorite_icon);
        // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.03 End
        getLoaderManager().initLoader(0, null, this);
        registerReceiver(mHideTipsReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));
        //Add by dongdong.wang@tcl.com 2015-12.03 for task 1021515 begin
        intent = getIntent();
        isLaucher = intent.getBooleanExtra("isLaucherWallpaper", false);
        Log.d(TAG, "[onCreate] isLaucher=" + isLaucher);
        //Add by dongdong.wang@tcl.com 2015-12.03 for task 1021515 end

        //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
        ContentValues mValuesData = new ContentValues();
        mValuesData.put("action","ADD");
        mValuesData.put(DataCollectUtil.MAGICLOCK_MYFAVORITE_NUM,String.valueOf(1));
        DataCollectUtil.storeData(this, mValuesData);
        //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494

    }

    /**
     * If {@link #isShowTips} is true, change {@link #mTipsLayout} to visible.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (isShowTips) {
            Log.d(TAG, "[onResume] isShowTips=" + isShowTips);
            setTipsVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mHideTipsReceiver)
            unregisterReceiver(mHideTipsReceiver);
    }

    /**
     * If {@link #mTipsLayout} is null, bind it firstly. then, if visibility isn't equal with
     * {@link #mTipsLayout}'s visibility, apply visibility.
     *
     * @param visibility
     */
    private void setTipsVisibility(int visibility) {
        if (null == mTipsLayout)
            mTipsLayout = findViewById(R.id.amf_no_favorite);
        if (mTipsLayout.getVisibility() != visibility)
            mTipsLayout.setVisibility(visibility);
    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_magiclock_favorites);
        mGridView = (GridView) findViewById(R.id.amf_gridview);
    }

    @Override
    protected void setTitle() {
        setTitle(R.string.magiclock_cat_favorites);
        if (null != mNoContentTips) {
            mNoContentTips.setText(R.string.magiclock_no_favorite);
        }
    }

    @Override
    protected void doDeleteOptions() {
        mAdapter.updateFavorites(this, getTitle());
    }

    /**
     * Instantiate and return a new Loader for the given ID.
     *
     * @param id   The ID whose loader is to be created.
     * @param args Any arguments supplied by the caller.
     * @return Return a new Loader instance that is ready to start loading.
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, FavoriteView.CONTENT_URI,
                new String[]{Picture._ID, Picture.PICTURE_NAME, Picture.ICON_PATH, Picture.ICON},
                null, null, null);
    }

    /**
     * Called when a previously created loader has finished its load.  Note
     * that normally an application is <em>not</em> allowed to commit fragment
     * transactions while in this call, since it can happen after an
     * activity's state is saved.  See  for further discussion on this.
     * <p/>
     * <p>This function is guaranteed to be called prior to the release of
     * the last data that was supplied for this Loader.  At this point
     * you should remove all use of the old data (since it will be released
     * soon), but should not do your own release of the data since its Loader
     * owns it and will take care of that.  The Loader will take care of
     * management of its data so you don't have to.  In particular:
     * <p/>
     * <ul>
     * <li> <p>The Loader will monitor for changes to the data, and report
     * them to you through new calls here.  You should not monitor the
     * data yourself.  For example, if the data is a {@link android.database.Cursor}
     * and you place it in a {@link android.widget.CursorAdapter}, use
     * the {@link android.widget.CursorAdapter#CursorAdapter(android.content.Context,
     * android.database.Cursor, int)} constructor <em>without</em> passing
     * in either {@link android.widget.CursorAdapter#FLAG_AUTO_REQUERY}
     * or {@link android.widget.CursorAdapter#FLAG_REGISTER_CONTENT_OBSERVER}
     * (that is, use 0 for the flags argument).  This prevents the CursorAdapter
     * from doing its own observing of the Cursor, which is not needed since
     * when a change happens you will get a new Cursor throw another call
     * here.
     * <li> The Loader will release the data once it knows the application
     * is no longer using it.  For example, if the data is
     * a {@link android.database.Cursor} from a {@link android.content.CursorLoader},
     * you should not call close() on it yourself.  If the Cursor is being placed in a
     * {@link android.widget.CursorAdapter}, you should use the
     * {@link android.widget.CursorAdapter#swapCursor(android.database.Cursor)}
     * method so that the old Cursor is not closed.
     * </ul>
     *
     * @param loader The Loader that has finished.
     * @param data   The data generated by the Loader.
     */
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.03 Begin
            // We must set it's visibility to VISIBLE. Because we will show this tips to user if there isn't data.
            mTipsIcon.setVisibility(View.VISIBLE);
            // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.03 End
            if (data.getCount() > 0) {
                isShowTips = false;
                setTipsVisibility(View.GONE);
            } else {
                isShowTips = true;
                setTipsVisibility(View.VISIBLE);
            }
            // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.03 Begin
            // Change to no favourite tips
            if (null != mNoContentTips) {
                mNoContentTips.setText(R.string.magiclock_no_favorite);
            }
            // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.03 End
        } catch (Exception e) {
            Log.e(TAG, "[onLoadFinished] Exception: " + e, e);
        }
        try {
            data.setNotificationUri(getContentResolver(), FavoriteView.CONTENT_URI);
            mAdapter.changeCursor(data);
        } catch (Exception e) {
            Log.e(TAG, "[onLoadFinished] Exception: " + e, e);
        }
    }

    /**
     * Called when a previously created loader is being reset, and thus
     * making its data unavailable.  The application should at this point
     * remove any references it has to the Loader's data.
     *
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        try {
            mAdapter.changeCursor(null);
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e, e);
        }
    }

    /**
     * Hide {@link #mTipsLayout} when received Screen off broadcast.
     * <p>
     * Notes: register this receiver into {@link #onCreate(Bundle)}
     * and unregister it into {@link #onDestroy()}
     * </p>
     */
    private BroadcastReceiver mHideTipsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
                setTipsVisibility(View.GONE);
            }
        }
    };
    //modify by dongdong.wang@tcl.com 2015-12.03 for task 1021515   begin
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (isLaucher) {
            ImageView picture = (ImageView) view.getTag();
            ItemInfo data = (ItemInfo) picture.getTag();
            Intent resultIntent = new Intent();
            String picturePath = Constants.getFolderPath(this, TAG) + data.picName;
            File pic = new File(picturePath);
            Uri picUri = Uri.fromFile(pic);
            if (picUri != null){
                resultIntent.setData(picUri);
                Log.d(TAG, "[onItemClick] picUri=" + picUri);
                setResult(Constants.RESULT_CODE_LAUCHER, resultIntent);
                finish();
            }else {
                Log.d(TAG, "[onItemClick] picUri=" + picUri);
            }
        } else {
            super.onItemClick(parent, view, position, id);
        }
    }
    //add by dongdong.wang@tcl.com 2015-12.22 for task 1021515   end
    //add by dongdong.wang@tcl.com 2015-12-22 for defect 1193591   begin
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        if (isLaucher){
            Log.d(TAG, "[onItemLongClick] return false");
            return false;
        }else {
            return super.onItemLongClick(parent, view, position, id);
        }
//add by dongdong.wang@tcl.com 2015-12-22 for defect 1193591   end

    }
}
