package com.tct.magiclock;

import android.app.Fragment;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tct.magiclock.util.*;
import com.tct.magiclock.widgets.RecentItemLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Recent history for Wall Shuffle
 * Created by TCTNB(Guoqiang.Qiu), Solution-2699694
 */

public class RecentFragment extends Fragment implements View.OnClickListener {
    private ViewGroup recents;
    private RecentItemLayout currentView;
    private static final int N = 10;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup host = (ViewGroup) inflater.inflate(R.layout.settings_recent, container, false);

        recents = (ViewGroup) host.findViewById(R.id.recent_list);

        RecentItemLayout item;

        new UpdateCoverTask(getContext()).execute();

        for (int i = 0; i < N; i++) {
            item = (RecentItemLayout) inflater.inflate(R.layout.item_recent, recents, false);
            item.setPosition(i, N);
            recents.addView(item);
            item.setOnClickListener(this);
            if (i == 0) {
                currentView = item;
            }
        }

        return host;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    @Override
    public void onClick(View v) {
        currentView = (RecentItemLayout)v;
        getView().setBackground(Drawable.createFromPath(Constants.MAGICLOCK_FILE_PATH + currentView.getContentDescription()));
    }

    private String getTime(long timeInMills) {
        Calendar today = Calendar.getInstance();
        Calendar date = Calendar.getInstance();
        date.setTimeInMillis(timeInMills);
        java.text.DateFormat format = DateFormat.getTimeFormat(getContext());
        String time = format.format(date.getTime());
        if (today.get(Calendar.DAY_OF_YEAR) != date.get(Calendar.DAY_OF_YEAR)) {
            boolean isSameYear = today.get(Calendar.YEAR) == date.get(Calendar.YEAR);
            String pattern = DateFormat.getBestDateTimePattern(Locale.getDefault(),
                    isSameYear ? "MMM d" : "yyyy MMM d");
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
            time += "\n" + sdf.format(date.getTime());
        }
        return time;
    }

    private class UpdateCoverTask extends AsyncTask<Void, Object, Void> {
        private Context mContext;

        public UpdateCoverTask(Context context) {
            mContext = context;
        }

        @Override
        protected Void doInBackground(Void... params) {
            int i = 0;
            try (Cursor cr = mContext.getContentResolver().query(
                    Uri.parse("content://com.tct.magiclock/pictures"),
                    null, null, null, null)) {
                int width = mContext.getResources().getDimensionPixelSize(R.dimen.item_recent_width);
                int height = mContext.getResources().getDimensionPixelSize(R.dimen.recent_height);
                int indexName = cr.getColumnIndex("pic_name");
                int indexFavorite = cr.getColumnIndex("favorite");
                int indexTime = cr.getColumnIndex("times");
                while (cr.moveToNext()) {
                    String name = cr.getString(indexName);
                    boolean favorite = cr.getInt(indexFavorite) == 1;
                    long time = cr.getLong(indexTime);
                    Bitmap out = PhotoUtils.centerCropImage(width, height, Constants.MAGICLOCK_FILE_PATH + name);;
                    publishProgress(i++, new BitmapDrawable(out), name, favorite, time);
                    if (i > N) break;
                }
            } catch (Exception e) {
                Log.d("Qiu", "Error", e);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            int index = (Integer) values[0];
            RecentItemLayout item = (RecentItemLayout)recents.getChildAt(index);
            String name = (String) values[2];
            if (item != null) {
                item.setBackground((Drawable) values[1]);
                item.setContentDescription(name);
                item.setFavorite((Boolean)values[3]);
                item.setText(getTime((Long)values[4]));
            }
            if (index == 0) {
                getView().setBackground(Drawable.createFromPath(Constants.MAGICLOCK_FILE_PATH + name));
            }
        }
    }
}
