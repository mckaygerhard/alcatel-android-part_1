package com.tct.magiclock;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tct.magiclock.activity.MagicLockGetShownPictureTask;
import com.tct.magiclock.utils.*;
import com.tct.magiclock.util.*;
import com.tct.magiclock.widgets.CategoryItemLayout;

import android.content.ContentValues;
import com.tct.magiclock.util.DataCollectUtil;

/**
 * Category for Wall Shuffle
 * Created by TCTNB(Guoqiang.Qiu), Solution-2699694
 */

public class CategoryFragment extends Fragment implements CategoryItemLayout.OnCheckedChangeListener {
    private int checkedItem = 0xFFF;
    private ViewGroup category;
    private UpdateCoverReceiver mReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkedItem = SettingPref.getCheckedCategory(getContext(), SettingPref.CATEGORY, checkedItem);
        new UpdateCoverTask(getContext()).execute();
        mReceiver = new UpdateCoverReceiver(getContext());
        mReceiver.registerReceiver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View host = inflater.inflate(R.layout.settings_category, container, false);

        category = (ViewGroup) host.findViewById(R.id.category);

        String categorys[] = getResources().getStringArray(R.array.categorys);
        int i = 0;

        CategoryItemLayout item = (CategoryItemLayout) inflater.inflate(R.layout.item_category_local, category, false);
        item.setCover(R.drawable.cover_mypic);
        item.setMaskAndStatus(Config.MASK_CATEGORY_LOCAL, checkedItem);
        item.setText(categorys[i++]);
        item.setOnCheckedChangeListener(this);
        category.addView(item);

        item = (CategoryItemLayout) inflater.inflate(R.layout.item_category_local, category, false);
        item.setCover(R.drawable.cover_favorite);
        item.setMaskAndStatus(Config.MASK_CATEGORY_FAVORITE, checkedItem);
        item.setText(categorys[i++]);
        item.setOnCheckedChangeListener(this);
        category.addView(item);

        for (; i < categorys.length; i++) {
            item = (CategoryItemLayout) inflater.inflate(R.layout.item_category, category, false);
            item.setMaskAndStatus(1 << i, checkedItem);
            item.setText(categorys[i]);
            item.setOnCheckedChangeListener(this);
            category.addView(item);
        }

        //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
        ContentValues mValuesData = new ContentValues();
        mValuesData.put("action","ADD");
        mValuesData.put(DataCollectUtil.MAGICLOCK_CATEGORY_NUM,String.valueOf(1));
        DataCollectUtil.storeData(getContext(), mValuesData);
        //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494

        return host;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mReceiver.unregisterReceiver();
    }

    @Override
    public void onCheckedChange(CategoryItemLayout view, int mask, boolean isChecked) {
        if (isChecked) {
            checkedItem |= mask;
        } else {
            int temp = checkedItem & ~mask;
            if (temp == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(R.string.magic_categority_selectd_tip);
                builder.setPositiveButton(android.R.string.ok, (dialog, which) -> dialog.dismiss());
                builder.create().show();
                view.setChecked(true);
                return;
            }
            checkedItem = temp;
        }
        new MagicLockGetShownPictureTask(MagicLockGetShownPictureTask.MODE_DOWNLOAD).execute(getContext());
        SettingPref.saveCheckedCategory(getContext(), mask, isChecked);

        //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
        tellAllCateChosen(isChecked);
        //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494
    }

    //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
    private void tellAllCateChosen(Boolean isChecked){
        if (isChecked){
            Boolean isAllCheck = true;
            for (int i=0; i < category.getChildCount(); i++){
		    CategoryItemLayout item = (CategoryItemLayout) category.getChildAt(i);
                if (!item.isChecked()){
                    isAllCheck = false;
                    break;
                }
            }
            if (isAllCheck){
                ContentValues mValuesData = new ContentValues();
                mValuesData.put("action","REPLACE");
                mValuesData.put(DataCollectUtil.MAGICLOCK_ALLCATCHOSEN_STA,String.valueOf(true));
                DataCollectUtil.storeData(getContext(), mValuesData);
            }
        }else{
            ContentValues mValuesData = new ContentValues();
            mValuesData.put("action","REPLACE");
            mValuesData.put(DataCollectUtil.MAGICLOCK_ALLCATCHOSEN_STA,String.valueOf(false));
            DataCollectUtil.storeData(getContext(), mValuesData);
        }

    }
    //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494

    private class UpdateCoverTask extends AsyncTask<Void, Object, Void> {
        private Context mContext;
        private int width, height;

        public UpdateCoverTask(Context context) {
            mContext = context;
            width = context.getResources().getDimensionPixelSize(R.dimen.item_category_width);
            height = context.getResources().getDimensionPixelSize(R.dimen.item_category_height);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try (Cursor cr = mContext.getContentResolver().query(
                    Uri.parse("content://com.tct.magiclock/view_cover"), null, null, null, null)) {
                while (cr.moveToNext()) {
                    String name = cr.getString(cr.getColumnIndex("pic_name"));
                    int cat = cr.getInt(cr.getColumnIndex("cat_data_id")) - 1;
                    if (cat > 0) {
                        Bitmap out = PhotoUtils.centerCropImage(width, height, Constants.MAGICLOCK_FILE_PATH + name);
                        publishProgress(cat, out);
                    }
                }
            } catch (Exception e) {}
            return null;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            int index = (Integer) values[0];
            Bitmap bg = (Bitmap) values[1];
            View view = category.getChildAt(index);
            if (view != null && bg != null) {
                view.setBackground(new BitmapDrawable(bg));
            }
        }
    }

    private class UpdateCoverReceiver extends BroadcastReceiver {
        private LocalBroadcastManager mLocalBroadcastManager;
        public UpdateCoverReceiver(Context context) {
            mLocalBroadcastManager = LocalBroadcastManager.getInstance(context);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent != null && Utils.ACTION_UPDATE_COVER.equals(intent.getAction())) {
                new UpdateCoverTask(getContext()).execute();
            }
        }

        public void registerReceiver() {
            IntentFilter filter = new IntentFilter(Utils.ACTION_UPDATE_COVER);
            mLocalBroadcastManager.registerReceiver(this, filter);
        }

        public void unregisterReceiver() {
            mLocalBroadcastManager.unregisterReceiver(this);
        }
    }
}
