package com.tct.magiclock.activity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger; // MODIFIED by dongdong.wang, 2016-07-07,BUG-2116248
import android.util.Log;

import com.tct.magiclock.Constants;
import com.tct.magiclock.db.MagiclockProvider;
import com.tct.magiclock.db.MagiclockProvider.Category;
import com.tct.magiclock.db.MagiclockProvider.Picture;
import com.tct.magiclock.service.BootCompletedService;
import com.tct.magiclock.util.AlarmUtil;
import com.tct.magiclock.utils.SettingPref;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by chunlin.tang on 7/1/15.
 */
public class MagicLockGetShownPictureTask  extends AsyncTask<Context, Void, Void> {
    private static final String TAG = "GetShownPicture";
    private static final String animations[] = {"water","particle","ball","sunshine","lightup"};
    private Handler mHandler;
    private Messenger reply;//add by dongdong.wang to fix defect 2116248 at 2016-07-07.
    private volatile int mDownloadMode = MODE_DOWNLOAD_IF_NO_PIC;
    /**
     * Message for returning current display list
     */
    public static final int MSG_AVALIABLE_FILES = 1100;

    /**
     * control if need to download when no picture for display
     */
    public static final int  MODE_NOT_DOWNLOAD = 0;
    public static final int  MODE_DOWNLOAD = 1;
    public static final int  MODE_DOWNLOAD_IF_NO_PIC = 2;


    public MagicLockGetShownPictureTask(){

    }
    public MagicLockGetShownPictureTask(int modeDownload){
        mDownloadMode = modeDownload;
    }
    public MagicLockGetShownPictureTask(Handler handler){
        mHandler = handler;
    }
    public MagicLockGetShownPictureTask(Handler handler, int modeDownload){
        mHandler = handler;
        mDownloadMode = modeDownload;
    }

    /* MODIFIED-BEGIN by dongdong.wang, 2016-07-07,BUG-2116248*/
    /**
     * instance task.
     * @param downloadMode down mode
     * @param message msg to reply systemui
     */
    public MagicLockGetShownPictureTask(int downloadMode, Messenger response){
        mDownloadMode = downloadMode;
        this.reply = response;
    }
    /* MODIFIED-END by dongdong.wang,BUG-2116248*/


    @Override
    protected Void doInBackground(Context... params) {
        HashSet<String> picSet = new HashSet<>();
        try (Cursor cursor = params[0].getContentResolver().query(
                MagiclockProvider.ShownPictureView.CONTENT_URI, null, null, null, null)) {
            if (cursor == null || !cursor.moveToLast()){
                throw new Exception("Curosr is NULL");
            }
            final int picIndex = cursor.getColumnIndexOrThrow(Picture.PICTURE_NAME);
            final int authorIndex = cursor.getColumnIndexOrThrow(Picture.AUTHOR_NAME);
            final int favIndex = cursor.getColumnIndexOrThrow(Picture.FAVORITE);
            final int localIndex = cursor.getColumnIndexOrThrow(Picture.MY_PICTURE);
            final String path = Constants.getFolderPath(params[0], TAG);
            ArrayList<Bundle> result = new ArrayList<>();
            int num = 0;
            do {
                String picName = cursor.getString(picIndex);
                boolean isFileOk = isFileExist(path+picName);
                if(isFileOk) {
                    boolean isFav = cursor.getInt(favIndex) == 1;
                    boolean isLocal = cursor.getInt(localIndex) == 1;

                    if (isFav || isLocal || num < Constants.NUM_PIC_TOTAL) {
                        String author = isLocal ? null : cursor.getString(authorIndex);
                        Bundle data = new Bundle();
                        data.putString(Constants.KEY_NAME, picName);
                        data.putString(Constants.KEY_AUTHOR, author);
                        data.putBoolean(Constants.KEY_LIKE, isFav);
                        data.putString(Constants.KEY_ANIM, animations[num%5]);
                        result.add(data);
                        picSet.add(picName);
                        if (!isFav && !isLocal) num++;
                    }
                } else{
                    Log.d(TAG, "WAIT TO DELTE SQL RECORD.");
                }
            } while (cursor.moveToPrevious());
            BootCompletedService.mList = result;
        } catch (Exception e) {
            Log.e(TAG, "[addPicturesToBundle] Exception: " + e, e);
        }

        if (null != mHandler) {
            Message msg = mHandler.obtainMessage(MSG_AVALIABLE_FILES);
            msg.obj = picSet;
            mHandler.sendMessage(msg);
        }
        if (needDownloadPicture(params[0], picSet.isEmpty())) {
            Log.d(TAG,"startBackService!");
            AlarmUtil util = new AlarmUtil(params[0]);
            util.cancelMagicLockAlarm();
            util.startMagicLockAlarm();
        }

        return null;
    }

    /**
     * check if category except my pictures selected, but no picture for display
     */
    private boolean needDownloadPicture(Context context,boolean isNoPic) {
        Cursor cursor = null;
        Log.d(TAG,"[needDownloadPicture] isNoPic="+isNoPic+"| mDownloadMode="+mDownloadMode);
        if(!SettingPref.isMagicOn(context)){
            return false;
        }
        if(MODE_NOT_DOWNLOAD == mDownloadMode){
            return false;
        }else if(MODE_DOWNLOAD == mDownloadMode){
            /**
             * Make sure at least one category is selected.
             */
            //modify by dongdong.wang at 2015-12-13 to fix defect 1048092 begin.
            boolean hasSelect = isCategorySelected(context);
            if (!hasSelect){
                Log.d(TAG, " select cat count = 0 and we will delete all file except my pic and favorite.");
                new CleanPictureTask(context).execute();
            }
            return  hasSelect;
            //modify by dongdong.wang at 2015-12-13 to fix defect 1048092 end .
        }else if((MODE_DOWNLOAD_IF_NO_PIC == mDownloadMode) && (isNoPic)) {
            boolean ret = false;
            if(isNoPic){
                ret = isCategorySelected(context);
            }
            Log.d(TAG, "[needDownloadPicture] ret = "+ret);
            return ret;
        }
        /**Default no need to download*/
        return false;
    }

    private  boolean isCategorySelected(Context context) {
        Cursor cursor = null;
        try {
            ContentResolver resolver = context.getContentResolver();
            Uri uri = MagiclockProvider.Category.CONTENT_URI;
            String selection = MagiclockProvider.Category.SELECTED + "="
                    + MagiclockProvider.Category.FLAG_SELECTED
                    + " AND " + Category.CAT + "<>'"+Constants.CAT_MYPIC+"'"
                    +" AND "+Category.CAT + "<>'"+Constants.CAT_FAVORITE+"'";
            String[] projection = new String[] { MagiclockProvider.Category.CAT };
            cursor = resolver.query(uri, projection, selection, null, null);
            if (null == cursor || cursor.getCount() <= 0) {
                return false;
            }
            Log.d(TAG,"[isCategorySelected] "+cursor.getCount());

        } catch (Exception e) {
            Log.e(TAG, "[isCategorySelected] exception:" + e,e);
        } finally {
            if (null != cursor) {
                cursor.close();
                cursor = null;
            }
        }

        return true;

    }

    private boolean isFileExist(String path) {
        File file = new File(path);
        boolean isExist = file.exists();
        Log.d(TAG, path +" isExist= "+ isExist);
        return isExist;
    }

}
