package com.tct.magiclock.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;
import android.os.UserHandle;
import android.util.Log;

import com.tct.magiclock.Constants;
import com.tct.magiclock.R;

import java.io.File;


/**
 * Used for Log output
 */
public class MGLKUtils {
    private static final String TAG = "MKU";

    /**
     * get AvailableSize from Rom
     * Add by dongdong.wang@tcl.com 2015-12.16 for  defect 1048253
     *
     * @return
     */
    public static boolean isRomAvailableSize(Context context) {
        boolean isRomAvailable = false;
        Long availMemory;
        File mPath = Environment.getDataDirectory();
        StatFs mStat = new StatFs(mPath.getPath());
        long blockSize = mStat.getBlockSizeLong();
        long availableBlocks = mStat.getAvailableBlocksLong();
        availMemory = (blockSize * availableBlocks) / 1024 / 1024;
        Log.d(TAG, "getRomAvailableSize=" + availMemory);

        if (availMemory > Constants.WARN_LOW_MEMORY) {
            isRomAvailable = true;
        } else {
            isRomAvailable = false;
        }
        return isRomAvailable;
    }

    /**
     * get AvailableSize from Rom
     * Add by dongdong.wang@tcl.com 2015-12.16 for  defect 1048253
     *
     * @return
     */
    public static void notifyLowMemory(Context context) {
        if (context == null){
            Log.d(TAG, "[notifyLowMemory] context is null");
            return ;
        }
        NotificationManager nManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // PR 1048253 Modified by chunlin.tang@tcl.com 2016-01-28 Begin
        // Dev Egro 1.3, start LowMemoryService to show the Low Memory dialog.
        Intent nIntent = new Intent();
        nIntent.setComponent(new ComponentName("com.tct.magiclock","com.tct.magiclock.activity.LowMemoryActivity"));
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, nIntent, 0);
        // PR 1048253 Modified by chunlin.tang@tcl.com 2016-01-28 End
        Notification.Builder mBuild = new Notification.Builder(context);
        mBuild.setAutoCancel(true)
                .setSmallIcon(R.drawable.noti_magic)
                .setContentIntent(contentIntent)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(context.getString(R.string.magiclock_warn_low_memory_title))
                .setContentText(context.getString(R.string.magiclock_warn_low_memory_tip_new))
                .setStyle(new Notification.BigTextStyle()
                        .bigText(context.getString(R.string.magiclock_warn_low_memory_tip_new)));
        Notification lowMemoryNotification = mBuild.build();
        nManager.notify(0, lowMemoryNotification);
        Log.d(TAG, "[notifyLowMemory] noti has show, please be care.");
    }
}