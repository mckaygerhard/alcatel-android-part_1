package com.tct.magiclock.activity;

import com.tct.magiclock.Constants;
import com.tct.magiclock.R;
import com.tct.magiclock.model.ItemInfo;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import java.util.Locale;

/**
 * Created by chunlin.tang on 8/17/15.
 */
public abstract class MagicLockBaseActivity extends Activity implements AdapterView.OnItemClickListener,AdapterView.OnItemLongClickListener{
    private static final String TAG = "MagicLockBaseActivity";
    protected MagicLockBaseAdapter mAdapter = null;
    protected GridView mGridView = null;
    /**
     * true means edit mode, show delete button and hide add button<br>
     * It will be set to true when long click gridview item.
     */
    protected boolean isEditMode = false;
    /**
     * delete button, it will be set to visible when switch to edit model.
     */
    protected MenuItem mDeleteBtn = null;
    /**
     * Recode last system language type.i.e en_US
     */
    private Locale mLastLocale = null;
    /**
     * Recode last font scale
     */
    private float mLastScale = 0;
    /**
     * true means do nothing when system call onConfigurationChanged.
     */
    private boolean isNoNeedConfiguration = true;

    /**
     * Show this view when there are not pictures.
     */
    protected TextView mNoContentTips = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView();
        initActionBar(View.GONE);
        mAdapter = new MagicLockBaseAdapter(this, null, true);

        mGridView.setAdapter(mAdapter);
        //modify by dongdong.wang@tcl.com 2015-12.03 for task 1021515   begin
        mGridView.setOnItemClickListener(this);
        /*mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "[onItemClick] view=" + view + " | position=" + position + " | id=" + id);
                if (isEditMode) {
                    // If the item isn't selected, mark it as selected and add it's id into selected array.
                    // otherwise, unmark it and remove it's id out from the selected array.
                    doSelectOptions(view);
                } else {
                    startPictureDetailActivity(TAG, view);
                }
            }
        });*/
        //modify by dongdong.wang@tcl.com 2015-12.03 for task 1021515   end
        //add by dongdong.wang@tcl.com 2015-12-22 for defect 1193591   begin
        mGridView.setOnItemLongClickListener(this);
        /*mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // switch to edit mode
                // hide add button and show delete button
                // mark the view as selected
                // add id into seleted array
                Log.d(TAG, "[onItemLongClick] view=" + view + " | position=" + position + " | id=" + id);

                if (!isEditMode) {
                    isEditMode = true;
                    mDeleteBtn.setVisible(true);
                    setAddBtnVisibility(View.GONE);
                }
                doSelectOptions(view);
                return true;
            }
        });*/
        //add by dongdong.wang@tcl.com 2015-12-22 for defect 1193591 end.
        try {
            Configuration config = getResources().getConfiguration();
            mLastLocale = config.locale;
            mLastScale = config.fontScale;
            Log.d(TAG, "[onCreate] mLastLocale=" + mLastLocale + " | mLastScale=" + mLastScale);
        } catch (Exception e) {
            Log.e(TAG, "[onCreate] init config exception.", e);
        }
    }

    /**
     * set {@link #isNoNeedConfiguration} to false.
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "[onSaveInstanceState] isEditMode=" + isEditMode);
        isNoNeedConfiguration = false;
    }


    /**
     * Reset activity title when switch system language.
     * Each child activity should override this function to set different title.
     */
    protected void setTitle() {}

    /**
     * if {@link #isNoNeedConfiguration} is true, do nothing. otherwise, change title text size
     * if font scale has changed, and reset title if system language changed.
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "[onConfigurationChanged]mLastScale=" + mLastScale
              + " | newConfig.fontScale=" + newConfig.fontScale
              + " | mLastLocale=" + mLastLocale + " | newConfig.locale=" + newConfig.locale
              + " | isNoNeedConfiguration=" + isNoNeedConfiguration);
        if (isNoNeedConfiguration) {
            return;
        }

        if (mLastScale != newConfig.fontScale) {
            setTitleSize(); // set title size
            setNoContentTipsSize();
        }
        if (null != newConfig.locale && !newConfig.locale.equals(mLastLocale)) {
            setTitle(); // set title
        }
        // record current config info.
        mLastScale = newConfig.fontScale;
        mLastLocale = newConfig.locale;
        isNoNeedConfiguration = true;
    }

    /**
     * Use reverberation to get the title TextView. Then, change the textview's text size.
     */
    private void setTitleSize() {
        try {
            View toolBar = findViewById(com.android.internal.R.id.action_bar);
            Class<?> classView = toolBar.getClass();
            java.lang.reflect.Field file = classView.getDeclaredField("mTitleTextView");
            file.setAccessible(true);
            TextView title = (TextView)file.get(toolBar);

            float textSize = getResources().getDimension(R.dimen.action_bar_title_size);
            Log.d(TAG, "[setTitleSize] textSize=" + textSize);
            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        } catch (Exception e) {
            Log.e(TAG, "[setTitleSize] change title size exception", e);
        }
    }

    private void setNoContentTipsSize() {
        try {
            float textSize = getResources().getDimension(R.dimen.no_picture_tips_size);
            if (null != mNoContentTips) {
                mNoContentTips.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }
        } catch (Exception e) {
            Log.e(TAG, "[setNoContentTipsSize] change mNoContentTips size exception", e);
        }
    }
    /**
     * If the picture has been selected, unselect it. otherwise, select it.
     * @param view the picture view
     */
    private void doSelectOptions(View view) {
        View selectedFlag = (View) view.getTag();
        ItemInfo data = (ItemInfo)selectedFlag.getTag();
        String key = String.valueOf(view.getId());
        if (data.isSelected) {
            data.isSelected = false;
            selectedFlag.setVisibility(View.GONE);
            mAdapter.getSelectedIdlist().remove(key);

            if (mAdapter.getSelectedIdlist().size() <= 0) {
                isEditMode = false;
                mDeleteBtn.setVisible(false);
                setAddBtnVisibility(View.VISIBLE);
            }
        } else {
            data.isSelected = true;
            selectedFlag.setVisibility(View.VISIBLE);
            mAdapter.getSelectedIdlist().add(key);
        }
    }

    protected void initActionBar(int removeBtnVisibility) {
        ActionBar actionBar = getActionBar();
        Log.d(TAG, "[initActionBar] actionBar=" + actionBar);
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    /**
     * open the picture detail activity
     * @param TAG the tag for log
     * @param imageView the view of the image
     */
    protected void startPictureDetailActivity(final String TAG, View imageView) {
        try {
            ImageView picture = (ImageView)imageView.getTag();
            ItemInfo data = (ItemInfo) picture.getTag();
            //add by dongdong.wang to fix defect 1857937 at 2016-04-19 begin.
            if (null== data){
                Log.d(TAG, "data == null");
                return;
            }
            Intent intent = new Intent(this, MagicLockPictureDetailActivity.class);
            //add by dongdong.wang to fix defect 1857937 at 2016-04-19 begin.
            //intent.putExtra(MagicLockPictureDetailActivity.KEY_IMG_PATH,
            //                Constants.getFolderPath(this, TAG) + data.picName);
            intent.putExtra(MagicLockPictureDetailActivity.KEY_PIC_NAME,
                            data.picName);
            //add by dongdong.wang to fix defect 1857937 at 2016-04-19 end.
            startActivity(intent);
            overridePendingTransition(R.anim.in_from_left, R.anim.out_from_right);
        } catch (Exception e) {
            Log.w(TAG, "[onItemClick] position=" + imageView, e);
        }
    }

    /**
     * Called by {@link #onCreate(Bundle)}<br>
     * To set activity layout, each child activity must override this funcation.
     */
    protected void setContentView(){}

    /**
     * Hide or show the add button.
     * If the child activity has one add button, it should override this funcation.
     * @param value is {@link android.view.View#VISIBLE} or {@link android.view.View#GONE}
     */
    protected void setAddBtnVisibility(int value) {}

    /**
     * this funcation will be called when click delete button.
     * each child activity should override this funcation to do different delete options.
     */
    protected void doDeleteOptions(){}

    /**
     * init delete button. if {@link #isEditMode} is false, hide delete button.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        try {
            getMenuInflater().inflate(R.menu.menu_magiclock_delete_pictures, menu);
            mDeleteBtn = menu.findItem(R.id.action_delete);
            mDeleteBtn.setVisible(isEditMode);
        } catch(Exception e) {
            Log.e(TAG, "[onCreateOptionsMenu] mAdapter=" + mAdapter, e);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_delete:
                isEditMode = false;
                mDeleteBtn.setVisible(false);
                setAddBtnVisibility(View.VISIBLE);
                doDeleteOptions();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * If {@link #isEditMode} is true and user click back key, close edit mode.
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (isEditMode && KeyEvent.KEYCODE_BACK == keyCode) {
            // exist edit mode, clear selected array
            isEditMode = false;
            mDeleteBtn.setVisible(false);
            setAddBtnVisibility(View.VISIBLE);
            mAdapter.clearAll(mGridView);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    //add by dongdong.wang@tcl.com 2015-12.03 for task 1021515   begin
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "[onItemClick] view=" + view + " | position=" + position + " | id=" + id);
        if (isEditMode) {
            // If the item isn't selected, mark it as selected and add it's id into selected array.
            // otherwise, unmark it and remove it's id out from the selected array.
            doSelectOptions(view);
        } else {
            startPictureDetailActivity(TAG, view);
        }
    }
    //modify by dongdong.wang@tcl.com 2015-12.03 for task 1021515   end
    //add by dongdong.wang@tcl.com 2015-12-22 for defect 1193591   begin
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        // switch to edit mode
        // hide add button and show delete button
        // mark the view as selected
        // add id into seleted array
        Log.d(TAG, "[onItemLongClick] view=" + view + " | position=" + position + " | id=" + id);

        if (!isEditMode) {
            isEditMode = true;
            mDeleteBtn.setVisible(true);
            setAddBtnVisibility(View.GONE);
        }
        doSelectOptions(view);
        return true;
    }
    //add by dongdong.wang@tcl.com 2015-12-22 for defect 1193591   end
}
