package com.tct.magiclock;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Constants {

    /**
     * Total Picture number
     */
    public static final int    NUM_PIC_TOTAL = 30;
    /**
     * Total favorite number
     */
    public static final int    NUM_FAV_TOTAL = 20;
    /**
     * Total My Picture number
     */
    public static final int    NUM_MYPIC_TOTAL = 30;
    /**
     * Result code for Laucher
     * Add by dongdong.wang@tcl.com 2015-12.03 for task 1021515
     */
    public static final int    RESULT_CODE_LAUCHER = 10;
    /**
     * Warning Memory
     * Add by dongdong.wang@tcl.com 2015-12.16 for  defect 1048253
     */
    public static final long WARN_LOW_MEMORY = 100; //low warning memory 100M.
    /**
     * Screen size
     */
    public static int    MAGICLOCK_SCREEN_WIDTH = 0; //720;
    public static int    MAGICLOCK_SCREEN_HEIGHT = 0; //1280;

    /* MODIFIED-BEGIN by dongdong.wang, 2016-07-07,BUG-2116248*/
    /**
     * request from systemui to update pic data.
     */
    public static final int REQUEST_UPDATE_PIC_CODE = 1;

    /**
     * Response update pic data to systemui.
     */
    public static final int RESPONSE_UPDATE_PIC_CODE = 2;

    /**
     * request from systemui to deal error.
     */
    public static final int REQUEST_DEAL_ERR_CODE = 3;

    /**
     * response deal error result to systemui
     */
    public static final int RESPONSE_DEAL_ERR_CODE = 4;
    /* MODIFIED-END by dongdong.wang,BUG-2116248*/

    /**APK info. Api_key and api_sec are created by Flickr website
     * Attention: Don't modify API_KEY or API_SEC! They are provided by Flickr
     */
    public static final String API_KEY = "52c2181448bfec1381e535e7e8a6f41a";
    public static final String API_SEC = "6e8e0e2a8d449f16";
    public static final String GROUP_ID = "2876102@N25"; // "67532954@N00";/**should be ALCATEL GROUP*/
    public static String USER_ID = "135099384@N04";//modify by dongdong.wang  at 2015-12-11 to fix task1132271

    /**
     * Local broadcast
     */
    public static final String DOWNLOAD_RESUME = "download_resume";

    // PR 935868 modified by chunlin.tang@tcl.com 2015-11-17 Begin
    /**
     * Magic Unlock switch on/off in system
     */
    public static final String MAGICLOCK_SYS_SWITCH = android.provider.Settings.System.TCT_MAGIC_UNLOCK; // "magiclock_func_on_off";
    // PR 935868 modified by chunlin.tang@tcl.com 2015-11-17 End
    /**Callback url in manifest. Used for user login*/
    public  static final String CALLBACK_URL = "mglk-flickr-callbackurl";

    /**sync data with keyguard*/
    public static final String KEY_TAG = "TAG";
    public static final String KEY_DATA = "LIST";
    public static final String ACTION_DATACHANGED = "com.tct.magiclock.datachanged";
    public static final String ACTION_NEED_RESUME = "com.tct.magiclock.NEED_RESUME";

    public static final String CATEGORY_CHANGE_NOTIFICATION = "com.tct.magiclock.ACTION_CAT_CHANGE";

    public static final String TAG_ADD = "ADD";//add new image for wall paper
    public static final String TAG_REP = "REP";//replace all the existing wall paper
    public static final String TAG_MOD = "MOD";//Modify favorite flag
    public static final String TAG_FAL  = "FAL";//fail to add new favorite.maxnum 20
    public static final String TAG_SWITCH = "SWT";//ON/OFF
    public static final String TAG_DOWNLOAD = "DLD";//start download process
    public static final String TAG_ERR = "ERR";//systemui send err, magiclock should REP

    public static final String DATA_PIC = "PIC";
    public static final String DATA_AUTHOR = "AUTHOR";

    public static final String KEY_NAME = "name";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_LIKE = "like";
    public static final String KEY_ANIM = "animation";


    public static final String FLAG_MYPIC = "TCT-Mypic";//Used for the author of my pictures.

    public static String MAGICLOCK_FILE_PATH = "/storage/emulated/0/Android/data/com.tct.magiclock/files/.flickr_pic/";

    /**share preference*/
    public  static final String PREFS_MGLK_NAME = "mglk-flickr-pref";

    /*Categories*/
    public static final String CAT_MYPIC = "Tct-Mypic";

    public static final String CAT_FAVORITE = "Tct-Favorites";

    public static final String CAT_NATURE = "Nature";

    public static final String CAT_URBAN = "Urban";

    public static final String CAT_MOTORS = "Motors";

    public static final String CAT_ARCH = "Architecture";

    public static final String CAT_BLACK_WHITE = "blackandwhite";

    public static final String CAT_SPACE = "Space";

    public static final String CAT_PET = "Pet";

    public static final String CAT_TRAVEL = "Travel";

    public static final String CAT_FOOD = "foodanddrink";

    public static final String CAT_ART = "Art";


    public static final long FLAG_IS_MYPIC = -101;


    public static final Map<String, CategroyItem> CATEGROY_ITEM_RES = new HashMap<String, CategroyItem>();
    static {
        CATEGROY_ITEM_RES.put(CAT_MYPIC, new CategroyItem(R.drawable.cover_mypic, R.string.magiclock_cat_mypic));
        CATEGROY_ITEM_RES.put(CAT_FAVORITE, new CategroyItem(R.drawable.cover_favorite, R.string.magiclock_cat_favorites));
        CATEGROY_ITEM_RES.put(CAT_NATURE, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_nature));
        CATEGROY_ITEM_RES.put(CAT_URBAN, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_city));
        CATEGROY_ITEM_RES.put(CAT_MOTORS, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_motors));
        CATEGROY_ITEM_RES.put(CAT_ARCH, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_architecture));
        CATEGROY_ITEM_RES.put(CAT_BLACK_WHITE, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_black_white));
        CATEGROY_ITEM_RES.put(CAT_SPACE, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_space));
        CATEGROY_ITEM_RES.put(CAT_PET, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_pet));
        CATEGROY_ITEM_RES.put(CAT_TRAVEL, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_travel));
        CATEGROY_ITEM_RES.put(CAT_FOOD, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_food));
        CATEGROY_ITEM_RES.put(CAT_ART, new CategroyItem(R.drawable.cover_default, R.string.magiclock_cat_art));
    }
    public static class CategroyItem {
        public int titleResId = -1;
        public int imgResId = -1;
        public CategroyItem(int imgId, int titleId) {
            imgResId = imgId;
            titleResId = titleId;
        }
    }

    /**
     *
     * @param context
     * @param TAG the tag for log
     */
    public static boolean checkFileDir(Context context, final String TAG) {
        File dir = context.getExternalFilesDir(".flickr_pic");
        return dir.exists();
    }

    public static String getFolderPath(Context context, final String TAG) {
        String packageName = "com.tct.magiclock";
        return "/storage/emulated/0/Android/data/" + packageName + "/files/.flickr_pic/"; // MODIFIED by dongdong.wang, 2016-07-07,BUG-2116248
    }
    public static boolean isFileExist(String path, final  String TAG) {
        File file = new File(path);
        boolean isExist = file.exists();
        Log.d(TAG, "[isFileExist] " + path + " is exist=" + isExist);
        return isExist;
    }
    public static boolean isSDCardExist(final String TAG) {
        try {
            // If the sdcard0 is removable, means it's SDCard. otherwise, it's phone storage.
            return Environment.isExternalStorageRemovable();
        } catch (Exception e) {
            Log.w(TAG, "[isSDCardExist] exception: " + e, e);
        }
        return false;
    }

    public static final String KEY_SCREEN_WIDTH = "key_screen_width";
    public static final String KEY_SCREEN_HEIGHT = "key_screen_height";
    public static void getScreenSize (Context context, final String TAG) {
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            Constants.MAGICLOCK_SCREEN_HEIGHT = prefs.getInt(KEY_SCREEN_HEIGHT, 0);
            Constants.MAGICLOCK_SCREEN_WIDTH = prefs.getInt(KEY_SCREEN_WIDTH, 0);
        } catch (Exception e) {
            Log.e(TAG, "[getScreenSize] context=" + context, e);
        }

        if (Constants.MAGICLOCK_SCREEN_HEIGHT <= 0) {
            try {
                initScreenSize(context, TAG);
            } catch (Exception e) {
                Log.e(TAG, "[initScreenSize] get metrics exception.", e);
            }
            if (Constants.MAGICLOCK_SCREEN_HEIGHT <= 0) {
                initScreenSize(true, 720, 1280);
            } else {
                saveScreenSize(context, Constants.MAGICLOCK_SCREEN_WIDTH, Constants.MAGICLOCK_SCREEN_HEIGHT, TAG);
            }
        }
    }
    private static void saveScreenSize(Context context,int width, int height, final String TAG) {
        try {
            PreferenceManager.getDefaultSharedPreferences(context)
                    .edit().putInt(KEY_SCREEN_WIDTH, width).putInt(KEY_SCREEN_HEIGHT, height).commit();
        } catch (Exception e) {
            Log.e(TAG, "[saveScreenSize] width=" + width + " | height=" + height, e);
        }
    }
    private static void initScreenSize(Context context, final String TAG) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager w = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display d = w.getDefaultDisplay();
        d.getMetrics(metrics);

        boolean isPort = isPortraitScreen(context, TAG);
        // includes window decorations (statusbar bar/navigation bar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) { // 17
            try {
                android.graphics.Point realSize = new android.graphics.Point();
                Display.class.getMethod("getRealSize",
                        android.graphics.Point.class).invoke(d, realSize);
                initScreenSize(isPort, realSize.x, realSize.y);
            } catch (Exception e) {
                Log.e(TAG, "[initScreenSize] " + e, e);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            try {
                int height = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
                int width = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
                initScreenSize(isPort, width, height);
            } catch (Exception e) {
                Log.e(TAG, "[initScreenSize] " + e, e);
            }
        } else {
            initScreenSize(isPort, metrics.widthPixels, metrics.heightPixels);
        }
        Log.d(TAG, "[initScreenSize] width=" + Constants.MAGICLOCK_SCREEN_WIDTH
                + " | height=" + Constants.MAGICLOCK_SCREEN_HEIGHT + " | isPort=" + isPort);
    }

    private static void initScreenSize(boolean isPort, int width, int height) {
        if (isPort) {
            Constants.MAGICLOCK_SCREEN_HEIGHT = height;
            Constants.MAGICLOCK_SCREEN_WIDTH = width;
        } else {
            Constants.MAGICLOCK_SCREEN_HEIGHT = width;
            Constants.MAGICLOCK_SCREEN_WIDTH = height;
        }
    }
    private static boolean isPortraitScreen(Context context, final String TAG) {
        try {
            return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        } catch (Exception e) {
            Log.e(TAG, "[isPortraitScreen] get screen orientation exception." + e, e);
        }
        return false;
    }
}
