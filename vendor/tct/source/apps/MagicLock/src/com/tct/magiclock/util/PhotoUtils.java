/**
 * @author xiaohui.niu@tcl.com 08/July/2015
 */

package com.tct.magiclock.util;


import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.tct.magiclock.Constants;
import com.tct.magiclock.R;

public class PhotoUtils{

    public static final String TAG = "PU";

    public static final String FILE_PROVIDER_AUTHORITY = "com.tct.magiclock.files";
    private static final String PHOTO_DATE_FORMAT = "'IMG'_yyyyMMdd_HHmmss";

    /**
     * Generate a new, unique file to be used as an out-of-band communication
     * channel, since hi-res Bitmaps are too big to serialize into a Bundle.
     * This file will be passed (as a uri) to other activities (such as the gallery/camera/
     *  cropper/etc.), and read by us once they are finished writing it.
     */
    public static Uri generateTempImageUri(Context context) {
        return FileProvider.getUriForFile(context, FILE_PROVIDER_AUTHORITY,
                new File(pathForTempPhoto(context, generateTempPhotoFileName())));
    }

    private static String generateTempPhotoFileName() {
        final Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat(PHOTO_DATE_FORMAT, Locale.US);
        return "Mypic-" + dateFormat.format(date) + ".jpg";
    }

    private static String pathForTempPhoto(Context context, String fileName) {
        final File dir = context.getCacheDir();
        dir.mkdirs();
        final File f = new File(dir, fileName);
        return f.getAbsolutePath();
    }

    private static String generateTempCroppedPhotoFileName() {
        final Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat dateFormat = new SimpleDateFormat(PHOTO_DATE_FORMAT, Locale.US);
        return "Mypic-" + dateFormat.format(date);// + ".jpg"; hide images
    }

    public static Uri generateTempCroppedImageUri(Context context) {
        return FileProvider.getUriForFile(context, FILE_PROVIDER_AUTHORITY,
                new File(pathForTempPhoto(context, generateTempCroppedPhotoFileName())));
    }

    public static void addCropExtras(Intent intent, int photox, int photoy) {
        intent.putExtra("crop", "true");
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("aspectX", photox);
        intent.putExtra("aspectY", photoy);
        intent.putExtra("outputX", photox);
        intent.putExtra("outputY", photoy); // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
    }

    /**
     * Adds common extras to gallery intents.
     *
     * @param intent The intent to add extras to.
     * @param photoUri The uri of the file to save the image to.
     */
    public static void addPhotoPickerExtras(Intent intent, Uri photoUri) {
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION |
                Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setClipData(ClipData.newRawUri(MediaStore.EXTRA_OUTPUT, photoUri));
    }

    /**
     * Given an input photo stored in a uri, save it to a destination uri
     */
    public static boolean savePhotoFromUriToUri(Context context, Uri inputUri, Uri outputUri,
            boolean deleteAfterSave) {
        FileOutputStream outputStream = null;
        InputStream inputStream = null;
        Log.d(TAG, "savePhotoFromUriToUri");
        try {
            outputStream = context.getContentResolver()
                    .openAssetFileDescriptor(outputUri, "rw").createOutputStream();
            inputStream = context.getContentResolver().openInputStream(
                    inputUri);

            final byte[] buffer = new byte[16 * 1024];
            int length;
            int totalLength = 0;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
                totalLength += length;
            }
            Log.d(TAG, "Wrote " + totalLength + " bytes for photo " + inputUri.toString());
        } catch (IOException e) {
            Log.e(TAG, "Failed to write photo: " + inputUri.toString() + " because: ", e);
            return false;
        } finally {
            try {
                inputStream.close();
                outputStream.close();
            } catch (Exception e) {
                Log.e(TAG, "Failed to close stream " + e, e);
            }
//            Closeables.closeQuietly(inputStream);
//            Closeables.closeQuietly(outputStream);
            if (deleteAfterSave) {
                context.getContentResolver().delete(inputUri, null, null);
            }
        }
        return true;
    }

    public static Bitmap getBitmapFromUri(Context context, Uri uri)throws FileNotFoundException{
        final InputStream imageStream = context.getContentResolver().openInputStream(uri);
        try {
            return BitmapFactory.decodeStream(imageStream);
        } finally {
            //Closeables.closeQuietly(imageStream);
            try {
                imageStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    /**
     * // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
     * @param bm
     * @param fileName
     * @param tag save to db
     */
    public static boolean saveFileandInsertDB(Context context, Bitmap bm, String fileName, String tag, String authorName)
    {
//        File folder = new File(Constants.MAGICLOCK_FILE_PATH);
        Constants.checkFileDir(context, TAG);
        boolean ret = false;
//        if(!folder.exists()){
//            folder.mkdir();
//            try {
//                Runtime.getRuntime()
//                        .exec("chmod 777 "+ folder.getAbsolutePath());
//            } catch (IOException e) {
//                e.printStackTrace();
//                Log.d("Judy", "exception:" + e);
//            }
//        }
        try {
            File file= new File(Constants.getFolderPath(context, TAG)+fileName);
            Log.d("Judy","testSaveFile:"+file);
            BufferedOutputStream bos;
            bos = new BufferedOutputStream(new FileOutputStream(file));
            bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bos.flush();
            bos.close();
            bm.recycle();//add to recycle memory
            //TODO  Add file patch and name to db
//            try {
//                ContentResolver resolver = context.getContentResolver();
//                ContentValues values = new ContentValues();
//
//                values.put(MagiclockProvider.CatData.CAT_DATA, tag);
//                values.put(MagiclockProvider.Picture.AUTHOR_NAME, authorName);
//                values.put(MagiclockProvider.Picture.PICTURE_NAME, fileName);
//                values.put(MagiclockProvider.Picture.DOWNLOAD_TIME, System.currentTimeMillis());
//                values.put(MagiclockProvider.Picture.ICON_PATH, Constants.MAGICLOCK_FILE_PATH);
//                Uri result = resolver.insert(MagiclockProvider.Picture.CONTENT_URI, values);
//                if(null != result){
//                	ret = true;
//                }
//            } catch (Exception e) {
//                Log.e("chunlin03", "[testSaveFile] Exception: " + e);
//            	return false;
//            }

            Log.e(TAG,"testSaveFile MSG_DOWNDLOAD_DONE");

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.d(TAG, "exception2:" + e);
            return false;
        }
        Log.d("Judy","testSaveFile ret = "+ret);
        return ret;

    }


    /**
     * Center crop and scale a image to fit the required width and height
     * // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
     * @param  requireWidth required width of the bitmap.
     *          requireHeight required height of the bitmap
     *          filepath full path of the file
     * @return Bitmap.
     */
    public static Bitmap centerCropImage(int requireWidth, int requireHeight, String filePath) {
        Bitmap ret = null;
        boolean isCrop = false;
        try {
            FileInputStream fis = null;
            BitmapFactory.Options o = new BitmapFactory.Options();
            int inSampleSize = 1;
             File file = new File(filePath);
             fis = new FileInputStream(file);
             o.inJustDecodeBounds = true;
             BitmapFactory.decodeStream(fis, null, o);
             if(o.outHeight > requireHeight || o.outWidth > requireWidth) {
                 int widthRatio = Math.round(o.outWidth * 1.0f / requireWidth);
                 int heightRatio = Math.round(o.outHeight * 1.0f / requireHeight);
                 inSampleSize = Math.min(widthRatio, heightRatio);
                 isCrop = true;
             }
             o.inSampleSize = inSampleSize;
             Log.d(TAG, "Filepath=" + filePath + ";inSampleSize:" + inSampleSize + "; isCrop=" + isCrop);
             o.inJustDecodeBounds = false;

             Bitmap bitmap = BitmapFactory.decodeFile(filePath, o);
             if(isCrop){
                 ret = CropCenter(requireWidth,requireHeight,bitmap);
             }else{
                 ret = bitmap;
             }

        } catch (Exception e) {
            Log.w(TAG, "centerCropImage Exception:", e);
            return null;
        }
        return ret;
    }

    /**
     * Center crop and scale a image to fit the required width and height
     * // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
     * @param  requireWidth required width of the bitmap.
     *          requireHeight required height of the bitmap
     *          filepath full path of the file
     * @return Bitmap.
     */
    public static Bitmap centerCropImageBitmap(int requireWidth, int requireHeight, Bitmap bm){
        Bitmap ret = null;
        boolean isCrop = false;
        //modify by dongdong.wang to fix defect 1782679 at 2016-03-14 begin
        final String tag = "[centerCropImageBitmap]";
        if(null == bm){
            Log.e(TAG,tag+"Null input!");
            return null;
        }
        try{
            float inSampleSize = 1.0f;
            int imageheight = bm.getHeight();
            int imagewidth = bm.getWidth();

             Log.d(TAG, tag+ "imagewidth="+ imagewidth+"; imageheight:"+imageheight);
             if(imagewidth> requireHeight || imageheight > requireWidth){
                float widthRatio = (float)(Math.round(imagewidth*10 / requireWidth))/10;
                float heightRatio = (float)(Math.round(imageheight*10 / requireHeight))/10;
                inSampleSize = Math.min(widthRatio, heightRatio);
                isCrop = true;
             }
             Log.d(TAG,tag+"inSampleSize:"+inSampleSize+"; isCrop="+isCrop);
             Bitmap resultBitmap = null; // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937

             if(inSampleSize>1){
                 float size_temp = imagewidth/inSampleSize;
                 int scaleWidth  = (int)size_temp;
                 size_temp = imageheight/inSampleSize;
                 int scaleHeight = (int)size_temp;
                 Log.d(TAG,tag+"scaleWidth:"+scaleWidth+"; scaleHeight="+scaleHeight);
                 if((scaleWidth < requireWidth) || (scaleHeight<requireHeight)){
                     //Use 0.05 as gap because the max gap is 0.05 when x.x
                     final float ratioGap = 0.05f;
                     inSampleSize = inSampleSize -ratioGap;
                     size_temp = imagewidth/inSampleSize;
                     scaleWidth  = (int)size_temp;
                     size_temp = imageheight/inSampleSize;
                     scaleHeight = (int)size_temp;
                     Log.e(TAG,tag+"Rescale scaleWidth:"+scaleWidth+"; scaleHeight="+scaleHeight);
                 }
                 //modify by dongdong.wang to fix defect 1782679 at 2016-03-14 end.
                 resultBitmap = Bitmap.createScaledBitmap(bm, scaleWidth, scaleHeight, false);
                 bm.recycle();//add by dongdong.wang to fix task 1857939 at 2016-04-05
             }else{
                 resultBitmap = bm;
             }

            if(null == resultBitmap){
                 Log.e(TAG,"[centerCropImageBitmap], NULL bitmap!");
                 return null; // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
             }
             if(isCrop){
                 ret = CropCenter(requireWidth,requireHeight,resultBitmap);
             }else{
                ret = resultBitmap;
             }

        }catch(Exception e){
            Log.w(TAG, "centerCropImage Exception:", e);
            return null;
        }
        return ret;
     }

    /**
     * Center crop a image to fit the required width and height
     *
     * @param requireWidth required width of the bitmap.
     *                     requireHeight required height of the bitmap
     *                     imageRes resource bitmap
     * @return Bitmap.
     */
     public static Bitmap CropCenter(int requireWidth, int requireHeight, Bitmap imageRes){

         try { // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
             int imageheight = imageRes.getHeight();
             int imagewidth = imageRes.getWidth();
             Log.d(TAG, "imagewidth=" + imagewidth + "; imageheight=" + imageheight);
             if(imageheight > requireHeight || imagewidth > requireWidth){
                 int x = (imagewidth > requireWidth) ? ((imagewidth - requireWidth) / 2) : 0;
                 int y = (imageheight > requireHeight) ? ((imageheight - requireHeight) / 2) : 0;
                 int cropwidth = (imagewidth > requireWidth) ? requireWidth : imagewidth;
                 int cropheight = (imageheight > requireHeight) ? requireHeight : imageheight;

                 Log.d(TAG, "x=" + x + "; y=" + y + "; cropheight=" + cropheight + "; cropwidth=" + cropwidth);
                 Bitmap target = Bitmap.createBitmap(imageRes, x, y, cropwidth, cropheight);
                 imageRes.recycle();
                 return target;
             } else {
                 Log.d(TAG, "No need to crop!");
                 return imageRes;
             }
         } catch (Exception e) {
             Log.w(TAG, " Error getLoacalBitmap, e: " + e, e);
         }
         return null;
     }

     private Bitmap.Config getConfig(Bitmap bitmap) {
         Bitmap.Config config = bitmap.getConfig();
         if (config == null) {
             config = Bitmap.Config.ARGB_8888;
         }
         return config;
     }

     public static int getIntFromXml(Context context, int resId, int defaultValue) {
         try {
             return context.getResources().getDimensionPixelSize(resId);
         } catch (Exception e) {
             Log.w(TAG, "[getIntFromXml] Exception: " + e, e);
         }
         return defaultValue;
     }

     public static byte[] flattenBitmap(Bitmap bitmap) {
         /** Try go guesstimate how much space the icon will take when serialized
         * to avoid unnecessary allocations/copies during the write.
         * */
         int size = bitmap.getWidth() * bitmap.getHeight() * 4;
         ByteArrayOutputStream out = new ByteArrayOutputStream(size);
         try {
             bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
             out.flush();
             out.close();
             return out.toByteArray();
         } catch (IOException e) {
             Log.w(TAG, "Could not write icon", e);
             return null;
         }
     }

    /**
     * Add credit information on pictures
     * @author guoqiang.qiu@tcl.com
     *
     * @param author the information will add on picture
     * @param width width of the bitmap
     * @param height height of the bitmap.
     * @param out The picture to add information.
     */
    public static void addCreditInfo(Context context, String author, int width, int height, Bitmap out) {
        Canvas canvas = new Canvas(out);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        paint.setTextSize(context.getResources().getDimensionPixelSize(R.dimen.credit_font_size));
        paint.setShadowLayer(3f, 1f, 1f,Color.BLACK);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        final String flickr = "flickr";
        float offsetY = height - context.getResources().getDimensionPixelSize(R.dimen.credit_margin_bottom);
        float offsetX = width - context.getResources().getDimensionPixelSize(R.dimen.credit_margin_right);
        float len = paint.measureText(flickr);
        offsetX -= len;
        canvas.drawText(flickr, offsetX, offsetY, paint);
        paint.setTypeface(Typeface.DEFAULT);
        len = paint.measureText(author);
        offsetX = offsetX - context.getResources().getDimensionPixelSize(R.dimen.credit_gap) - len;
        canvas.drawText(author, offsetX, offsetY, paint);
    }
}
