package com.tct.magiclock.utils;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * [SOLUTION]-Created by TCTNB(Guoqiang.Qiu), 2016-8-25, Solution-2699694
 */

public class ShakeManager {
    private static final String TAG = "ShakeManager";
    private SensorManager mSensorManager;
    private ShakeSensorListener mShakeListener;
    private Context mContext;

    /**
     * true means {@link #mShakeListener} has been registed, false means otherwise.<br>
     * Called by {@link #registerListener()} and {@link #unregisterListener()}
     */
    private boolean isSensorRegisted = false;

    private static ShakeManager instance;

    private ShakeManager(Context context) {
        mContext = context;
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        mShakeListener = new ShakeSensorListener();
    }

    /**
     * When screen on,call this function.
     * Register sensor only sensor is unregister and toggle is on
     */
    public static void registerSensor(Context context) {
        if (instance == null) {
            instance = new ShakeManager(context);
        }
        instance.registerListener();
    }

    private void registerListener() {
        if (isSensorRegisted) {
            return;
        }
        isSensorRegisted = mSensorManager.registerListener(mShakeListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        mShakeListener.setLastUpdateTime();
        Log.d(TAG, "registerSensor");
    }

    /**
     * When screen off,call this function.
     * If sensor had been register, unregister it no matter toggle is on/off
     */
    public static void unregisterSensor() {
        if (instance != null) {
            instance.unregisterListener();
        }
    }

    private void unregisterListener() {
        if (!isSensorRegisted) {
            return;
        }
        Log.d(TAG, "unregisterSensor");
        mSensorManager.unregisterListener(mShakeListener);
        mShakeListener = null;
        instance = null;
        isSensorRegisted = false;
    }

    private class ShakeSensorListener implements SensorEventListener {

        /**
         * The rate to checking sensor.
         */
        /* MODIFIED-BEGIN by zhuang.yao, 2016-11-14,BUG-3243625*/
        private static final int TIME_INTERVAL = 60;

        /**
         * The delay time before changing picture.
         */
        private static final int TIME_WAIT = 550;

        /**
         * the threshold level of changing picture.
         */
        private static final int SHAKE_THRESHOLD = 700;
        /* MODIFIED-END by zhuang.yao,BUG-3243625*/

        private long mLastUpdateTime;

        /**
         * the last time that the speed over {@link #SHAKE_THRESHOLD}
         */
        private long mEndUpdateTime = 0;

        private float mLast_x, mLast_y;

        /**
         * true means need changing picture. false means no changing option.
         */
        private boolean mNeedChangePic = false;

        public ShakeSensorListener() {
            mLastUpdateTime = System.currentTimeMillis();
        }

        public void setLastUpdateTime() {
            mLastUpdateTime = System.currentTimeMillis();
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            // change picture option
            try {
                if (Sensor.TYPE_ACCELEROMETER != event.sensor.getType()) {
                    Log.d(TAG, "Sensor type isn't TYPE_ACCELEROMETER, return directly!");
                    return;
                }
                long curTime = System.currentTimeMillis();

                long diffTime = (curTime - mLastUpdateTime);
                // only allow one update every TIME_INTERVAL(100ms).
                if (diffTime > TIME_INTERVAL) {
                    mLastUpdateTime = curTime;
                    float x = event.values[0];
                    float y = event.values[1];

                    float deltaX = x - mLast_x;
                    float deltaY = y - mLast_y;
                    // deltaX>0:shake left; deltaX<0 deltaX left
                    // ignore the Z axis because we only care about the X-Y plane.
                    double speed = Math.sqrt(deltaX * deltaX + deltaY * deltaY) / diffTime * 10000;
                    if (speed >= SHAKE_THRESHOLD) {
                        mNeedChangePic = true;
                        mEndUpdateTime = curTime;
                    } else if (mNeedChangePic) {
                        if (curTime - mEndUpdateTime >= TIME_WAIT) {
                            mNeedChangePic = false;
                            try {
                                Utils.getChangePendingIntent(mContext).send();
                            } catch (Exception e) {
                                Log.w(TAG, "[onSensorChanged] Change Fail");
                            }
                        } else {
                            Log.w(TAG, "[onSensorChanged] Please wait several milliseconds.");
                        }
                    }
                    mLast_x = x;
                    mLast_y = y;
                }
            } catch (Exception e) {
                Log.e(TAG, "onSensorChanged Exception:" + e, e);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }
}
