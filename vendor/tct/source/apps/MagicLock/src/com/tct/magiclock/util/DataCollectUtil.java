package com.tct.magiclock.util;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

/**
 * Created by luyiting on 10/31/16.for task-3304494
 * add for diagnostics data collection
 */

public class DataCollectUtil {

    private static final String SCHEME = "content";
    private static final String AUTHORITY = "com.tct.diagnostics.provider.diagnosticsinfo";
    private static final String TABLE_NAME = "diagnostics";
    private static final Uri CONTENT_URI = new Uri.Builder().scheme(SCHEME).authority(AUTHORITY).path(TABLE_NAME).build();

    public static final String MAGICLOCK_FAVORITETAP_NUM = "MAGICLOCK_FAVORITETAP_NUM";
    public static final String MAGICLOCK_PINTAP_NUM = "MAGICLOCK_PINTAP_NUM";
    public static final String MAGICLOCK_DELETETAP_NUM = "MAGICLOCK_DELETETAP_NUM";
    public static final String MAGICLOCK_SETTINGENTER_NUM = "MAGICLOCK_SETTINGENTER_NUM";
    public static final String MAGICLOCK_FUNCENTER_NUM = "MAGICLOCK_FUNCENTER_NUM";
    public static final String MAGICLOCK_OFFENTER_NUM = "MAGICLOCK_OFFENTER_NUM";
    public static final String MAGICLOCK_NOTSETTINGENTER_NUM = "MAGICLOCK_NOTSETTINGENTER_NUM";
    public static final String MAGICLOCK_MAINSETTING_DUR = "MAGICLOCK_MAINSETTING_DUR";
    public static final String MAGICLOCK_OFFUSER_STA = "MAGICLOCK_OFFUSER_STA";
    public static final String MAGICLOCK_OFFTOON_DUR = "MAGICLOCK_OFFTOON_DUR";
    public static final String MAGICLOCK_INTERVAL_STA = "MAGICLOCK_INTERVAL_STA";
    public static final String MAGICLOCK_CATEGORY_NUM = "MAGICLOCK_CATEGORY_NUM";
    public static final String MAGICLOCK_MYPICTURE_NUM = "MAGICLOCK_MYPICTURE_NUM";
    public static final String MAGICLOCK_MYFAVORITE_NUM = "MAGICLOCK_MYFAVORITE_NUM";
    public static final String MAGICLOCK_ALLCATCHOSEN_STA = "MAGICLOCK_ALLCATCHOSEN_STA";


    public static void storeData(Context context, ContentValues values) {
        ContentProviderClient privider = context.getApplicationContext().getContentResolver().acquireUnstableContentProviderClient(AUTHORITY);
        try {
            if (privider != null) {
                privider.update(CONTENT_URI, values, null, null);
            }
        } catch (IllegalArgumentException e) {
            Log.d(context.getPackageName(),"llegalArgumentException Exception: "+e);
        } catch (RemoteException e) {
            Log.d(context.getPackageName(),"write2DB() RemoteException Exception: "+e);
        } finally {
            if (privider != null) {
                privider.release();
            }
        }
    }



}
