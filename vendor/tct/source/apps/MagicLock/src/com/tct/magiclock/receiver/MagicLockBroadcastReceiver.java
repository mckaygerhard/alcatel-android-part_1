package com.tct.magiclock.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.util.Log;

import com.tct.magiclock.Constants;
import com.tct.magiclock.R;
import com.tct.magiclock.activity.MagicLockGetShownPictureTask;
import com.tct.magiclock.service.MagicLockBackService;
import com.tct.magiclock.utils.Utils;
import com.tct.magiclock.utils.SettingPref;

import java.io.File;

/**
 * Created by chunlin.tang on 7/1/15.
 */
public class MagicLockBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "Receiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        Log.d(TAG, "onReceive action:" + action);
        if(!SettingPref.isMagicOn(context)){
    	    return;
    	}

        if((ConnectivityManager.CONNECTIVITY_ACTION).equals(action)){
            checkResumeStatus(context);
        } else if (Intent.ACTION_MEDIA_MOUNTED.equals(action)) {
            checkResumeMediaData(context);//modify by dongdong.wang to fix defect 2116248 at 2016-07-07. // MODIFIED by cuihua.yang, 2016-08-06,BUG-2613812
        } else {
            Log.d(TAG, "[onReceive] unknown action = " +action);
        }
    }

    private void checkResumeMediaData(final Context context) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (Utils.isNetworkOkForDownload(context) &&
                        (Constants.isFileExist(Constants.getFolderPath(context, TAG), TAG))){
                    SharedPreferences sp = context.getSharedPreferences(
                            Constants.PREFS_MGLK_NAME, Context.MODE_PRIVATE);
                    boolean isDownloadResume = sp.getBoolean("down_resume",
                            false);
                    /* MODIFIED-BEGIN by dongdong.wang, 2016-05-20,BUG-2116248*/
                    Log.d(TAG,"[checkResumeMediaData].isDownloadResume = " + isDownloadResume);
                    if (isDownloadResume) {
                        new MagicLockGetShownPictureTask(MagicLockGetShownPictureTask.MODE_NOT_DOWNLOAD).execute(context);
                        Intent i = new Intent(context,
                                MagicLockBackService.class);
                        i.setAction(Constants.ACTION_NEED_RESUME);
                        context.startService(i);
                    }else {
                        new MagicLockGetShownPictureTask().execute(context);
                    }
                } else {
                    Log.d(TAG,"[checkResumeMediaData] no need check download service.");
                    /* MODIFIED-END by dongdong.wang,BUG-2116248*/
                    new MagicLockGetShownPictureTask().execute(context);
                }

            }
        }).start();
    }
    /* MODIFIED-END by dongdong.wang,BUG-2116248*/

    private void checkResumeStatus(final Context context) {
        new Thread() {
            public void run() {
                if (Utils.isNetworkOkForDownload(context)) {

                    SharedPreferences sp = context.getSharedPreferences(
                            Constants.PREFS_MGLK_NAME, Context.MODE_PRIVATE);
                    boolean isDownloadResume = sp.getBoolean("down_resume",
                            false);
                    Log.d(TAG,"[checkResumeStatus].isDownloadResume="+isDownloadResume);
                    if (isDownloadResume) {
                        Intent i = new Intent(context, MagicLockBackService.class);
                        i.setAction(Constants.ACTION_NEED_RESUME);
                        context.startService(i);
                    }
                }
            }
        }.start();
    }

}
