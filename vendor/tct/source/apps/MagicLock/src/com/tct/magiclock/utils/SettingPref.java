package com.tct.magiclock.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.UserHandle;
import android.preference.PreferenceManager;
import android.provider.Settings;

import com.tct.magiclock.R;
import com.tct.magiclock.db.MagiclockProvider;

/**
 * The class {@code Utils} is a util class for Setting of WallShuffle
 * @author guoqiang.qiu@tcl.com
 * @since  n8996, Solution-2699694
 */

public class SettingPref {
    public static final String CHANGE_INTERNAL = "change_interval";
    public static final String WIFI = "wifi_only";
    public static final String OFF_WIFI_TIP = "off_wifi_tip";
    public static final String CATEGORY = "category_status";

    public static boolean getBoolean(Context context, String key, boolean def) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(key, def);
    }

    public static void putBoolean(Context context, String key, boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putBoolean(key, value).apply();
    }

    public static String getString(Context context, String key, String def) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(key, def);
    }

    public static void putString(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString(key, value).apply();
    }

    public static boolean isMagicOn(Context context) {
        return 1 == Settings.System.getInt(context.getContentResolver(),
                Settings.System.TCT_MAGIC_UNLOCK, 1);
    }

    public static void setMagicOn(Context context, boolean value) {
        Settings.System.putInt(context.getContentResolver(),
                Settings.System.TCT_MAGIC_UNLOCK, value ? 1 : 0);
    }

    public static int getCheckedCategory(Context context, String key, int def) {
         try(Cursor cursor = context.getContentResolver().query(MagiclockProvider.Category.CONTENT_URI,
                new String[] { MagiclockProvider.Category._ID, MagiclockProvider.Category.SELECTED},
                null, null, null)) {

            final int idIndex = cursor.getColumnIndex(MagiclockProvider.Category._ID);
            final int statusIndex = cursor.getColumnIndex(MagiclockProvider.Category.SELECTED);

            int ret = 0;

            while (cursor.moveToNext()) {
                int id = cursor.getInt(idIndex) - 1;
                int status = cursor.getInt(statusIndex);
                if (id < 0) continue;
                ret |= (status << id);
            }
            return ret;
        } catch (Exception e) {
        }
        return def;
    }

    public static void saveCheckedCategory(Context context, int mask, boolean value) {
        try {
            ContentValues values = new ContentValues();
            values.put(MagiclockProvider.Category.SELECTED, value ? 1 : 0);
            int where = (int)(Math.log(mask)/Math.log(2)) + 1;
            context.getContentResolver().update(MagiclockProvider.Category.CONTENT_URI,
                    values, MagiclockProvider.Category._ID + " = \'" + where + "\'", null);
        } catch (Exception e) {
        }
    }
}
