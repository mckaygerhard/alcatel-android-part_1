package com.tct.magiclock.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tct.magiclock.R;

/**
 * Item layout of recent history for Wall Shuffle
 * Created by TCTNB(Guoqiang.Qiu), Solution-2699694
 */

public class RecentItemLayout extends RelativeLayout {
    private ImageView mFavorite;
    private ImageView mLine;
    private TextView mTime;

    public RecentItemLayout(Context context) {
        super(context);
    }

    public RecentItemLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        mFavorite = (ImageView) findViewById(R.id.favorite);
        mLine = (ImageView) findViewById(R.id.line);
        mTime = (TextView) findViewById(R.id.time);
    }

    public void setText(int resId) {
        mTime.setText(resId);
    }

    public void setText(CharSequence text) {
        mTime.setText(text);
    }

    public void setFavorite(boolean favorite) {
        mFavorite.setVisibility(favorite ? View.VISIBLE : View.GONE);
    }

    public void setPosition(int index, int count) {
        if (index == 0) {
            mLine.setImageResource(R.drawable.line_time_first);
        } else if (index == count - 1) {
            mLine.setImageResource(R.drawable.line_time_last);
        } else {
            mLine.setImageResource(R.drawable.line_time);
        }
    }

}
