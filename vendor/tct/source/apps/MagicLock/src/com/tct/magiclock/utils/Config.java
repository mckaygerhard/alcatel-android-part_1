package com.tct.magiclock.utils;

public class Config {
    public static final int MASK_CATEGORY_LOCAL    = 1;
    public static final int MASK_CATEGORY_FAVORITE = 1 << 1;
    public static final int MASK_CATEGORY_NATURE   = 1 << 2;
    public static final int MASK_CATEGORY_URBAN    = 1 << 3;
    public static final int MASK_CATEGORY_MOTORS   = 1 << 4;
    public static final int MASK_CATEGORY_ARCH     = 1 << 5;
    public static final int MASK_CATEGORY_B_W      = 1 << 6;
    public static final int MASK_CATEGORY_SPACE    = 1 << 7;
    public static final int MASK_CATEGORY_PET      = 1 << 8;
    public static final int MASK_CATEGORY_TRAVEL   = 1 << 9;
    public static final int MASK_CATEGORY_FOOD     = 1 << 10;
    public static final int MASK_CATEGORY_ART      = 1 << 11;
}
