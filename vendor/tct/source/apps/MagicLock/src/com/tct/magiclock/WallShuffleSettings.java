package com.tct.magiclock;

import android.app.Dialog;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.util.Log;

import com.tct.magiclock.utils.Cache;
import com.tct.magiclock.utils.SettingPref;
import com.tct.magiclock.utils.SwitchBarHolder;
import com.tct.magiclock.utils.Utils;

import android.content.ContentValues;
import com.tct.magiclock.util.DataCollectUtil;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.tct.magiclock.activity.MagicLockGetShownPictureTask; // MODIFIED by dongdong.li, 2016-11-09,BUG-3354800

/**
 * Setting main fragment for Wall Shuffle
 * Created by TCTNB(Guoqiang.Qiu), Solution-2699694
 */

public class WallShuffleSettings extends PreferenceFragment
        implements Preference.OnPreferenceChangeListener {
    private ViewGroup mContainer;
    private View mTips;
    private View mContent;
    private SwitchBarHolder mSwitch;

    private SwitchPreference mToggleWifi;
    private ListPreference mInterval;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.main_prefs);
        mToggleWifi = (SwitchPreference) findPreference(SettingPref.WIFI);
        mToggleWifi.setOnPreferenceChangeListener(this);
        mInterval = (ListPreference) findPreference(SettingPref.CHANGE_INTERNAL);
        mInterval.setOnPreferenceChangeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mContainer = (ViewGroup)inflater.inflate(R.layout.settings_main, container, false);
        mContent = super.onCreateView(inflater, mContainer, savedInstanceState);
        mContainer.addView(mContent);
        mTips = mContainer.findViewById(R.id.empty_tips);
        mSwitch = new SwitchBarHolder(mContainer.findViewById(R.id.switch_bar));
        mSwitch.setOnCheckedChangeListener(status -> {
            initView(status);
            SettingPref.setMagicOn(getContext(), status);
            doBackgroundTask(status);
	    //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
	    SharedPreferences preferences = getContext().getSharedPreferences("data_collection",0);
            SharedPreferences.Editor editor = preferences.edit();
	    Boolean isOnData = preferences.getBoolean("isOnData", true);
	    if (status && !isOnData){
                editor.putBoolean("isOnData", true).commit();
                int onTime = (int) (System.currentTimeMillis()/1000);
                int offTime = preferences.getInt("offTime", 0);
		if (offTime != 0){
                    int offToOnTime = onTime - offTime;
                    ContentValues mValuesData = new ContentValues();
                    mValuesData.put("action","AVERAGE");
                    mValuesData.put(DataCollectUtil.MAGICLOCK_OFFTOON_DUR,String.valueOf(offToOnTime));
                    DataCollectUtil.storeData(getContext(), mValuesData);
		}
	    }else if (!status && isOnData){
		editor.putBoolean("isOnData", false).commit();
		int offTime = (int) (System.currentTimeMillis()/1000);
		editor.putInt("offTime", offTime).commit();
	    }
            //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494
        });
        return mContainer;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.magiclock_unlock_title);
        boolean isMagicOn = SettingPref.isMagicOn(getContext());
        mSwitch.setChecked(isMagicOn);
        bindIntervalSummary(SettingPref.getString(getContext(), SettingPref.CHANGE_INTERNAL, null));
        if (!isMagicOn) {
            initView(false);
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        if (preference == mToggleWifi) {
            if (!(Boolean)value && SettingPref.getBoolean(getContext(),
                    SettingPref.OFF_WIFI_TIP, true)) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setTitle(R.string.magiclock_dialog_title);
                dialog.setContentView(R.layout.dialog_wifi);
                dialog.show();
                View btnEnter = dialog.findViewById(R.id.btn_positive);
                btnEnter.setOnClickListener(view -> {
                    CheckBox notRemind = (CheckBox) dialog.findViewById(R.id.not_remind);
                    SettingPref.putBoolean(getContext(), SettingPref.OFF_WIFI_TIP, !notRemind.isChecked());
                    mToggleWifi.setChecked(false);
                    dialog.dismiss();
                });
                View btnCancel = dialog.findViewById(R.id.btn_cancel);
                btnCancel.setOnClickListener(view -> dialog.dismiss());
                return false;
            }
        } else if (preference == mInterval) {
            //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
	    //add status of interval of device lightup
            ContentValues mValuesData = new ContentValues();
            mValuesData.put("action","REPLACE");
            mValuesData.put(DataCollectUtil.MAGICLOCK_INTERVAL_STA,String.valueOf(value));
            DataCollectUtil.storeData(getContext(), mValuesData);
            //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494

            bindIntervalSummary((String)value);
            Cache.getCache().put(SettingPref.CHANGE_INTERNAL, value);
            Utils.cancelIntervalAlarm(getContext());
            try {
			    Utils.setIntervalAlarm(getContext(), Integer.parseInt((String)value));
		    } catch (Exception e) {
                Log.d("Qiu","setInterval", e);
            } //Do nothing!
        }
        return true;
    }

    private void bindIntervalSummary(String value) {
        int index = mInterval.findIndexOfValue(value);
        index = index > 0 ? index : 0;
        mInterval.setSummary(mInterval.getEntries()[index]);
    }

    private void initView(boolean status) {
        mContent.setVisibility(status ? View.VISIBLE : View.GONE);
        mTips.setVisibility(status ? View.GONE : View.VISIBLE);
        //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
        ContentValues mValuesData = new ContentValues();
        mValuesData.put("action","REPLACE");
        mValuesData.put(DataCollectUtil.MAGICLOCK_OFFUSER_STA,status?"ON":"OFF");
        DataCollectUtil.storeData(getContext(), mValuesData);
        Log.e("test", "MAGICLOCK_OFFUSER_STA:" + (status?"ON":"OFF"));
        //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494
    }

    private void doBackgroundTask(boolean status) {
        Utils.cancelIntervalAlarm(getContext());
        if (status) {
            try {
                    /* MODIFIED-BEGIN by dongdong.li, 2016-11-09,BUG-3354800*/
                    Utils.setIntervalAlarm(getContext(), Integer.parseInt(mInterval.getValue()));
                    Log.d("test","download MagicLock pictures start...");
                    new MagicLockGetShownPictureTask().execute(getContext());
                    /* MODIFIED-END by dongdong.li,BUG-3354800*/
		    } catch (Exception e) {} //Do nothing!
        }
    }
}
