# Copyright (C) 2016 Tcl Corporation Limited
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_JAVA_LIBRARIES := telephony-common org.apache.http.legacy

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := TctPsAutoRegister
LOCAL_CERTIFICATE := platform

# This will install the file in /system/vendor/ChinaTelecom
#LOCAL_MODULE_PATH := $(TARGET_OUT)/vendor/ChinaTelecom/system/app
include $(BUILD_PACKAGE)

# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))
