/* Copyright (C) 2016 Tcl Corporation Limited */
/**
 * Copyright (c) 2014, Qualcomm Technologies, Inc. All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */
/******************************************************************************/
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 02/06/2016|     Dandan.Fang      |      PR1530879       |[Telecom][CHINA]- */
/*           |                      |                      |SIXM-04001 [Mand  */
/*           |                      |                      |atory]auto register*/
/*           |                      |                      |function          */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.qualcomm.qti.service;

import java.io.BufferedReader;
import java.io.File; // MODIFIED by qili.zhang, 2016-05-07,BUG-2094743
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.uicc.IccConstants;
import com.android.internal.telephony.uicc.IccFileHandler;
import com.android.internal.telephony.uicc.UiccCard;
import com.android.internal.telephony.uicc.UiccCardApplication;
import com.android.internal.telephony.uicc.UiccController;
import com.android.internal.telephony.uicc.IccCardApplicationStatus.AppType;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncResult;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.provider.Settings; // MODIFIED by qili.zhang, 2016-05-14,BUG-2151875
import android.telephony.CellLocation;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.text.TextUtils;

//[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,02/06/2016,PR1530879,
//[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager;
import android.telephony.gsm.GsmCellLocation;
import android.telephony.SubscriptionInfo;
import android.util.Log;
//[BUGFIX]-Add-END by TCTNB.Dandan.Fang

public class DeviceInfoPool {

    public static class DeviceInfoReq {
        String key;
        Message callback;

        public DeviceInfoReq(String key, Message callback) {
            this.key = key;
            this.callback = callback;
        }

    }

    private static final String TAG = DeviceInfoPool.class.getSimpleName();
    private static final String BASE_PARAM_REGVER = "1.0";
    private static final String MAN_PARAM_UETYPE = "1";
    private static final String SIM_TYPE_ICC = "1";
    private static final String SIM_TYPE_UICC = "2";
    private static final String DEFAULT_VALUE = "0";
    private static final boolean DBG = false;
    private static final String RESULT = "result";
    private static final String DEFAULT_HW_VERSION = "PVT2.0";
    private static final String FILENAME_META_VERSION = "/firmware/verinfo/ver_info.txt";

    private TelephonyManager mTelephonyMgr = null;
    private final Context mContext;
    private static DeviceInfoPool mDeviceInfoPool;

    //[BUGFIX]-Add-BEGIN by TCTNB.yongzhen.wang,12/21/2015,Task1177186,
    //SMS and data auto-registeration features for China Telecom
    // C.S0074-Av1.0 Section 4
    static final int EF_CSIM_MLPL = 0x4F20;
    static final int EF_CSIM_MSPL = 0x4F21;
    //[BUGFIX]-Add-END by TCTNB.yongzhen.wang
    /* MODIFIED-BEGIN by qili.zhang, 2016-05-07,BUG-2094743*/
    final int SIZE_16 = 16;
    final int SIZE_32 = 32;
    final int SIZE_64 = 64;
    long KB = 1024;
    long MB = KB * 1024;
    long GB = MB * 1024;
    /* MODIFIED-END by qili.zhang,BUG-2094743*/

    public enum NodeParm {

        /* MODIFIED-BEGIN by qili.zhang, 2016-10-26,BUG-3230129*/
        REGVER, MEID, MODELSMS, SWVER, SIM1CDMAIMSI, SIM2CDMAIMSI,

        UETYPE, SIM1ICCID, SIM2ICCID, SIM1GIMSI, SIM1LTEIMSI,SIM2LTEIMSI,SIM1TYPE, SID, NID, MACID,
        /* MODIFIED-END by qili.zhang,BUG-3230129*/

        IMEI1, IMEI2, BASEID, MODELCTA, MANUFACTURE, OSVER, HWVER, PNAME, COLOR, MLPLVER, MSPLVER, CELLID, MMEID,
        ACCESSTYPE, REGDATE,

        NOVALUE;

        public static NodeParm toNodeParam(String str) {
            try {
                if (DBG) {
                    Log.d(TAG, "toNodeParam:" + str);
                }
                return valueOf(str);
            } catch (Exception ex) {
                return NOVALUE;
            }
        }
    }

    private static final int EVENT_GET_DEVICE_INFO = 1;

    private Handler mMainHandler = new Handler(Looper.getMainLooper()) {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EVENT_GET_DEVICE_INFO:
                    DeviceInfoReq req = (DeviceInfoReq) msg.obj;
                    Message message = req.callback;
                    if (DBG) {
                        Log.d(TAG, "handle key:" + req.key);
                    }
                    switch (NodeParm.toNodeParam(req.key)) {
                        // Base params
                        case REGVER:
                            response(message, getRegver());
                            break;
                        case MEID:
                            response(message, getMeid());
                            break;
                        case MODELSMS:
                            response(message, getDeviceModel());
                            break;
                        case SWVER:
                            response(message, getSwVersion());
                            break;
                        case SIM1CDMAIMSI:
                            response(message, getSimCdmaIMSI(SetSim1())); // MODIFIED by qili.zhang, 2016-10-26,BUG-3230129
                            break;
                        // Mandatory params
                        case UETYPE:
                            response(message, getUeType());
                            break;
                        case SIM1ICCID:
                            response(message, getSim1Iccid());
                            break;
                        case SIM2ICCID:
                            response(message, getSim2Iccid());
                            break;
                        case SIM1GIMSI:
                            /* MODIFIED-BEGIN by qili.zhang, 2016-06-04,BUG-2243892*/
                            response(message, getSimGIMSI(SetSim1()));
                            break;
                        case SIM1LTEIMSI:
                            response(message, getSimLTEIMSI(SetSim1()));
                            break;
                        case SIM1TYPE:
                            response(message, getSimType(SetSim1()));
                            break;
                        /* MODIFIED-BEGIN by qili.zhang, 2016-10-26,BUG-3230129*/
                        case SIM2CDMAIMSI:
                            response(message, getSimCdmaIMSI(SetSim2()));
                            /* MODIFIED-END by qili.zhang,BUG-2243892*/
                            break;
                        case SIM2LTEIMSI:
                            response(message,getSimLTEIMSI(SetSim2()));
                            break;
                            /* MODIFIED-END by qili.zhang,BUG-3230129*/
                        case SID:
                            response(message, getSid());
                            break;
                        case NID:
                            response(message, getNid());
                            break;
                        case MACID:
                            response(message, getMacID());
                            break;
                        //[BUGFIX]-Add-BEGIN by TCTNB.yongzhen.wang,12/21/2015,Task1177186,
                        //SMS and data auto-registeration features for China Telecom
                        case IMEI1:
                            response(message, getImei(0));
                            break;
                        case IMEI2:
                            response(message, getImei(1));
                            break;
                        case BASEID:
                            response(message, getBaseId());
                            break;
                       //[BUGFIX]-Add-END by TCTNB.yongzhen.wang
                        // Optional params
                        case MODELCTA:
                            response(message, getModelCTA());
                            break;
                        case MANUFACTURE:
                            response(message, getManufacture());
                            break;
                        case OSVER:
                            response(message, getOSVersion());
                            break;
                        case HWVER:
                            response(message, getHWVersion());
                            break;
                        case MLPLVER:
                            mHandler.getMlplVersion(PhoneConstants.SUB1, message);
                            break;
                        case MSPLVER:
                            mHandler.getMsplVersion(PhoneConstants.SUB1, message);
                            break;
                        case ACCESSTYPE:
                            response(message, getAccessType());
                            break;
                        case REGDATE:
                            response(message, getRegDate());
                            break;
                        default:
                            response(message, null);
                    }
            }
        }

    };

    private void init() {
        mTelephonyMgr = TelephonyManager.getDefault();
    }

    private DeviceInfoPool(Context context) {
        mContext = context;
        init();
    }

    public static DeviceInfoPool getInstance(Context context) {
        if (mDeviceInfoPool == null) {
            mDeviceInfoPool = new DeviceInfoPool(context);
        }
        return mDeviceInfoPool;
    }

    public boolean dispatchNodeOperation(String nodeParam, Message message) {
        if (NodeParm.toNodeParam(nodeParam) != NodeParm.NOVALUE) {
            mMainHandler.obtainMessage(EVENT_GET_DEVICE_INFO,
                    new DeviceInfoReq(nodeParam, message))
                    .sendToTarget();
            return true;
        }
        return false;
    }

    private ObtainVersionHandler mHandler = new ObtainVersionHandler();

    private class ObtainVersionHandler extends Handler {
        private static final int MESSAGE_GET_EF_MSPL = 0;
        private static final int MESSAGE_GET_EF_MLPL = 1;
        // MSPL ID is x bytes data
        private static final int NUM_BYTES_MSPL_ID = 5;
        // MLPL ID is 8 bytes data
        private static final int NUM_BYTES_MLPL_ID = 5;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_GET_EF_MSPL:
                    handleGetEFMspl(msg);
                    break;
                case MESSAGE_GET_EF_MLPL:
                    handleGetEFMlpl(msg);
                    break;
            }
        }

        private void handleGetEFMspl(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;
            String msplVersion = null;
            byte[] data = (byte[]) ar.result;
            if (ar.exception == null) {
                if (data.length > NUM_BYTES_MSPL_ID - 1) {
                    int msplId = ((data[3] & 0xFF) << 8) | (data[4] & 0xFF);
                    msplVersion = String.valueOf(msplId);
                }
            }
            response((Message) ar.userObj, msplVersion);
        }

        private void handleGetEFMlpl(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;
            String mlplVersion = null;
            byte[] data = (byte[]) ar.result;
            if (ar.exception == null) {
                if (data.length > NUM_BYTES_MLPL_ID - 1) {
                    int mlplId = ((data[3] & 0xFF) << 8) | (data[4] & 0xFF);
                    mlplVersion = String.valueOf(mlplId);
                }
            }
            response((Message) ar.userObj, mlplVersion);
        }

        public boolean getMsplVersion(int slotId, Message message) {
            UiccController controller = UiccController.getInstance();
            if (controller != null) {
                IccFileHandler fh = controller.getIccFileHandler(slotId,
                        UiccController.APP_FAM_3GPP2);
                if (fh != null) {
                    fh.loadEFTransparent(EF_CSIM_MSPL, NUM_BYTES_MSPL_ID,
                            mHandler.obtainMessage(ObtainVersionHandler.MESSAGE_GET_EF_MSPL,
                                    message));
                    return true;
                }
            }
            response(message, null);
            return false;
        }

        public boolean getMlplVersion(int slotId, Message message) {
            UiccController controller = UiccController.getInstance();
            if (controller != null) {
                IccFileHandler fh = controller.getIccFileHandler(slotId,
                        UiccController.APP_FAM_3GPP2);
                if (fh != null) {
                    fh.loadEFTransparent(EF_CSIM_MLPL, NUM_BYTES_MLPL_ID,
                            mHandler.obtainMessage(ObtainVersionHandler.MESSAGE_GET_EF_MLPL,
                                    message));
                    return true;
                }
            }
            response(message, null);
            return false;
        }
    }

    private Phone getPhoneInstance() {
        Phone phone = null;
        if (mTelephonyMgr.isMultiSimEnabled()) {
            phone = PhoneFactory.getPhone(PhoneConstants.SUB1);
        } else {
            phone = PhoneFactory.getDefaultPhone();
        }
        return phone;
    }

    public String getRegver() {
        return BASE_PARAM_REGVER;
    }

    public String getDeviceModel() {
        /* MODIFIED-BEGIN by qili.zhang, 2016-05-07,BUG-2094743*/
        String  productname = "TCL-i806";

        if (!SystemProperties.get("ro.product.model").startsWith("ONE TOUCH")) {
            productname = SystemProperties.get("ro.product.model").replace(' ', '-');
        }


        return  productname;
        /* MODIFIED-END by qili.zhang,BUG-2094743*/
    }

    public String getMeid() {
        Phone phone = getPhoneInstance();
        /* MODIFIED-BEGIN by qili.zhang, 2016-05-14,BUG-2151875*/
        if (phone != null&&phone.getPhoneType()==PhoneConstants.PHONE_TYPE_CDMA ){
            return phone.getDeviceId();
        }else if(phone != null&&phone.getPhoneType()==PhoneConstants.PHONE_TYPE_GSM&&
             mContext.getResources().getInteger(com.android.internal.R.integer.def_sms_autoregister_operator) == 3){
           String meid=Settings.System.getString(mContext.getContentResolver(),"phone_meid");
           Log.i(TAG,"Phone_meid:"+meid);
           return meid;
        }
        /* MODIFIED-END by qili.zhang,BUG-2151875*/
        return null;
    }

    public String getSwVersion() {
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,02/06/2016,PR1530879,
        //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
        //String swVersion = SystemProperties.get("ro.build.au_rev", null);
        /* MODIFIED-BEGIN by qili.zhang, 2016-05-07,BUG-2094743*/
        String swVersion = SystemProperties.get("ro.def.ct.software.version", null);
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
        //[BUGFIX]-ADD-Begin By TCTNB qili.zhang for defect 1664041 03/08/2016
        //String swVersion = SystemProperties.get("ro.tct.boot.ver", null).substring(1, 5);
        /* MODIFIED-END by qili.zhang,BUG-2094743*/
        //[BUGFIX]-ADD-END By TCTNB qili.zhang for defect 1664041 03/08/2016
        if (TextUtils.isEmpty(swVersion)) return DEFAULT_VALUE;
        return swVersion;
    }

    public UiccCard getUiccCard(int cardIndex) {
        UiccCard uiccCard = null;
        if (!mTelephonyMgr.isMultiSimEnabled()) {
            uiccCard = UiccController.getInstance().getUiccCard(0);
        } else {
            uiccCard = UiccController.getInstance().getUiccCard(cardIndex);
        }
        return uiccCard;
    }

    private UiccCardApplication getValidApp(AppType appType, UiccCard uiccCard) {
        UiccCardApplication validApp = null;
        int numApps = uiccCard.getNumApplications();
        for (int i = 0; i < numApps; i++) {
            UiccCardApplication app = uiccCard.getApplicationIndex(i);
            if (app != null && app.getType() != AppType.APPTYPE_UNKNOWN
                    && app.getType() == appType) {
                validApp = app;
                break;
            }
        }
        return validApp;
    }

    public String getSimIMISI(int sub, AppType apptype) {
        String simIMSI = null;
        UiccCardApplication validApp = getValidApp(apptype, getUiccCard(sub));
        if (validApp != null) {
            simIMSI = validApp.getIccRecords().getIMSI();
        }
        return simIMSI;
    }

    public String getSimCdmaIMSI(int sub) {
        /* MODIFIED-BEGIN by qili.zhang, 2016-06-04,BUG-2243892
        boolean DdsisTelcom=false;
        int subId=SubscriptionManager.from(mContext).getDefaultDataSubscriptionId();
        int PhoneId=SubscriptionManager.from(mContext).getPhoneId(sub);
        SubscriptionInfo subInfo=SubscriptionManager.from(mContext).getActiveSubscriptionInfo(subId);
        Log.i(TAG,"SIM1 subInfo= "+subInfo+"PhoneId="+PhoneId);
        if(subInfo!=null){
           if(subInfo.getIccId().substring(0,6).equals("898603")||
                subInfo.getIccId().substring(0,6).equals("898611")){
                DdsisTelcom=true;
           }
        }
        if(!DdsisTelcom) return null;
        // MODIFIED-BEGIN by qili.zhang, 2016-10-26, BUG-3230129
        */
        String ruimImsi = getSimIMISI(sub, AppType.APPTYPE_RUIM);
        String csimImsi = getSimIMISI(sub, AppType.APPTYPE_CSIM);
        /* MODIFIED-END by qili.zhang,BUG-3230129*/
        return TextUtils.isEmpty(ruimImsi) ? csimImsi : ruimImsi;
    }

    public String getSimGIMSI(int sub) {
        return getSimIMISI(sub, AppType.APPTYPE_SIM);
    }

    public String getSimLTEIMSI(int sub) {
        return getSimIMISI(sub, AppType.APPTYPE_USIM);
    }

    private String getSim2IMSI(int sub) {
        String mIMSI = null;
        if (mTelephonyMgr.isMultiSimEnabled()) {
            mIMSI = mTelephonyMgr.getSubscriberId(SubscriptionManager.getSubId(sub)[0]);
        } else {
            mIMSI = mTelephonyMgr.getSubscriberId();
        }
        return mIMSI;
    }

    //[BUGFIX]-Add-BEGIN by TCTNB.yongzhen.wang,12/21/2015,Task1177186,
    //SMS and data auto-registeration features for China Telecom
     private String getImei(int slotId) {
        String imei = null;
        if (mTelephonyMgr.isMultiSimEnabled()) {
            imei = mTelephonyMgr.getImei(slotId);
        } else {
            imei = mTelephonyMgr.getImei();
        }
        return imei;
    }

    private String getBaseId() {
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,02/06/2016,PR1530879,
        //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
        String baseId = "";
        int ddsPhoneId = SubscriptionManager.from(mContext).getDefaultDataPhoneId();
        Phone phone = null;
        phone = PhoneFactory.getPhone(ddsPhoneId);
        if (phone == null)
            return "";

        CellLocation cellLocation = phone.getCellLocation();
        if (cellLocation instanceof CdmaCellLocation) {
            //if dds with ctcc, baseId as cdma baseStationId
            baseId = String.valueOf(((CdmaCellLocation) cellLocation).getBaseStationId());
        }else if(cellLocation instanceof GsmCellLocation){
            //if dds with cucc/cmcc, baseId as gsm cell Id
            int Cid = ((GsmCellLocation) cellLocation).getCid();
            int Psc = ((GsmCellLocation) cellLocation).getPsc();
            if(Cid != -1){
                baseId = String.valueOf(Cid);
            }else if(Psc!= -1){
                baseId = String.valueOf(Psc);
            }
        }

        Log.i(TAG,"baseId="+ baseId);
        return baseId;
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
    }
    //[BUGFIX]-Add-END by TCTNB.yongzhen.wang

    public boolean isUiccCard(UiccCard uiccCard) {
        return uiccCard.isApplicationOnIcc(AppType.APPTYPE_CSIM)
                || uiccCard.isApplicationOnIcc(AppType.APPTYPE_USIM)
                || uiccCard.isApplicationOnIcc(AppType.APPTYPE_ISIM);
    }

    public String getSimType(int sub) {
        UiccCard uiccCard = getUiccCard(sub);
        if (uiccCard == null) {
            return null;
        }
        if (isUiccCard(uiccCard)) {
            return SIM_TYPE_UICC;
        }
        return SIM_TYPE_ICC;
    }

    public String getUeType() {
        return MAN_PARAM_UETYPE;
    }

    public String getSim1Iccid() {
        /* MODIFIED-BEGIN by qili.zhang, 2016-06-04,BUG-2243892*/
        int subId=SubscriptionManager.from(mContext).getSubId(SetSim1())[0];
        /* MODIFIED-BEGIN by qili.zhang, 2016-10-26,BUG-3230129*/
        String iccid=null;
        if(subId!=SubscriptionManager.INVALID_SUBSCRIPTION_ID){
            SubscriptionInfo subInfo=SubscriptionManager.from(mContext).getActiveSubscriptionInfo(subId);
            /* MODIFIED-BEGIN by qili.zhang, 2016-11-01,BUG-3273565*/
            iccid=(subInfo!=null ? subInfo.getIccId(): null);
        }
        return iccid;
    }

    public String getSim2Iccid() {

        String iccid=null;
        Log.d(TAG," mTelephonyMgr.isMultiSimEnabled() "+mTelephonyMgr.isMultiSimEnabled());
        if(mTelephonyMgr.isMultiSimEnabled()){
            int subId=SubscriptionManager.from(mContext).getSubId(SetSim2())[0];
            if(subId!=SubscriptionManager.INVALID_SUBSCRIPTION_ID){
               SubscriptionInfo subInfo=SubscriptionManager.from(mContext).getActiveSubscriptionInfo(subId);
               iccid= (subInfo!=null ? subInfo.getIccId(): null);
           }
           /* MODIFIED-END by qili.zhang,BUG-3273565*/
        }
        return iccid;
        /* MODIFIED-END by qili.zhang,BUG-3230129*/
        /* MODIFIED-END by qili.zhang,BUG-2243892*/
    }

    public String getSimIccid(int subId) {
        String iccid = null;
        if (mTelephonyMgr.isMultiSimEnabled()) {
            iccid = mTelephonyMgr.getSimSerialNumber(subId);
        } else {
            iccid = mTelephonyMgr.getSimSerialNumber();
        }
        return iccid;
    }

    public String getSid() {
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,02/06/2016,PR1530879,
        //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
        int ddsPhoneId =  SubscriptionManager.from(mContext).getDefaultDataPhoneId();
        Phone phone = PhoneFactory.getPhone(ddsPhoneId);
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
        if (phone == null) return null;
        String sid = null;
        CellLocation cellLocation = phone.getCellLocation();
        if (cellLocation instanceof CdmaCellLocation) {
            sid = String.valueOf(((CdmaCellLocation) cellLocation).getSystemId());
        }
        return sid;
    }

    public String getNid() {
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,02/06/2016,PR1530879,
        //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
        int ddsPhoneId =  SubscriptionManager.from(mContext).getDefaultDataPhoneId();
        Phone phone = PhoneFactory.getPhone(ddsPhoneId);
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
        if (phone == null) return null;
        String nid = null;
        CellLocation cellLocation = phone.getCellLocation();
        if (cellLocation instanceof CdmaCellLocation) {
            nid = String.valueOf(((CdmaCellLocation) cellLocation).getNetworkId());
        }
        return nid;
    }

    public String getMacID() {
        WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String macAddress = wifiInfo == null ? null : wifiInfo.getMacAddress();
        return macAddress;
    }

    public String getModelCTA() {
        String modelCTA = Build.MODEL;
        return modelCTA;
    }

    public String getManufacture() {
        String manufacture = Build.MANUFACTURER;
        return manufacture;
    }

    public String getOSVersion() {
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,02/06/2016,PR1530879,
        //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
        //OSVersion should be constructed by name of Operation system and version,Android5.0
        String osVersion = "android" + Build.VERSION.RELEASE; // MODIFIED by qili.zhang, 2016-05-07,BUG-2094743
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
        Log.i(TAG,"dandan osVersion = " + osVersion);
        return osVersion;
    }

    public String getHWVersion() {
        String hwVersion = SystemProperties.get("ro.hw_version", null);
        if (TextUtils.isEmpty(hwVersion)) {
            //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,02/06/2016,PR1530879,
            //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
            //hwVersion = DEFAULT_HW_VERSION;
            /* MODIFIED-BEGIN by qili.zhang, 2016-05-07,BUG-2094743*/
            hwVersion=String.format("%dG", getTotalMemory());
            //hwVersion = convertStorage(getTotalMemory());
            /* MODIFIED-END by qili.zhang,BUG-2094743*/
            //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
        }
        return hwVersion;
    }

  //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,02/06/2016,PR1530879,
  //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
    public long getTotalMemory(Context context) {
        MemoryInfo info = new MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(info);
        return info.totalMem; // TODO totalMem N/A in ICS.
    }
    /* MODIFIED-BEGIN by qili.zhang, 2016-05-07,BUG-2094743*/
    public long getTotalMemory()
    {
        long internalTotalBytes=0;
        long totalspace=0;
        final File path = new File("/data");
        long totalBytes=path.getTotalSpace();
        long totalGBytes=totalBytes / GB;
        //Log.i("zhangqili"," neicun wei "+internalTotalBytes); // MODIFIED by qili.zhang, 2016-06-04,BUG-2243892
        Log.i(TAG, "getMemorySpace() totalBytes=" +totalBytes
                         + ", totalGBytes=" + totalGBytes);
        if (totalGBytes < SIZE_16) {
             totalspace =SIZE_16;
        } else if (totalGBytes >SIZE_16&& totalGBytes <SIZE_32) {
            totalspace =SIZE_32;
        }else{
            totalspace =SIZE_64;
        }
        return totalspace;
    }
    /* MODIFIED-END by qili.zhang,BUG-2094743*/

    public String convertStorage(long size) {
        long kb = 1024;
        long mb = kb * 1024;
        long gb = mb * 1024;

        if (size >= gb) {
            return String.format("%.1f GB", (float) size / gb);
        } else if (size >= mb) {
            float f = (float) size / mb;
            return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
        } else if (size >= kb) {
            float f = (float) size / kb;
            return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
        } else
            return String.format("%d B", size);
    }
      //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    public String getRegDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = sdf.format(new java.util.Date());
        return time;
    }

    public String getAccessType() {
        String accessType = null;
        ConnectivityManager connManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = connManager.getAllNetworkInfo();
        for (NetworkInfo networkInfo : networkInfos) {
            if (networkInfo.isConnected()) {
                int networkType = networkInfo.getType();
                if (networkType == ConnectivityManager.TYPE_WIFI
                        || networkType == ConnectivityManager.TYPE_MOBILE) {
                    // 1:network, 2:wifi
                    accessType = String.valueOf(networkType + 1);
                }
            }
        }
        return accessType;
    }

    private static String readLine(String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename), 256);
        try {
            return reader.readLine();
        } finally {
            reader.close();
        }
    }

    private void response(Message callback, String result) {
        if (DBG) {
            Log.i(TAG, "response: [callback]=" + callback + " [result]=" + result);
        }
        if (callback == null ) { // MODIFIED by qili.zhang, 2016-11-11,BUG-3402693
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(RESULT, result);
        if (callback.obj != null && callback.obj instanceof Parcelable) {
            bundle.putParcelable("userobj", (Parcelable) callback.obj);
        }
        callback.obj = bundle;
        if (callback.replyTo != null) {
            try {
                callback.replyTo.send(callback);
            } catch (RemoteException e) {
                Log.w(TAG, "failed to response result", e);
            }
        } else if (callback.getTarget() != null) {
            callback.sendToTarget();
        } else {
            Log.w(TAG, "can't response the result, replyTo and target are all null!");
        }
    }
    /* MODIFIED-BEGIN by qili.zhang, 2016-06-04,BUG-2243892*/
    private int SetSim1()
    {
        int SIM1=PhoneConstants.SUB1;
        if(PhoneConstants.SUB1!=SubscriptionManager.from(mContext).getDefaultDataPhoneId()
            &&SubscriptionManager.from(mContext).getDefaultDataPhoneId()!=-1){
            Log.i(TAG,"SIM1="+SIM1);
            SIM1=PhoneConstants.SUB2;
        }
        return SIM1;
    }
    private int SetSim2(){
        int SIM2=PhoneConstants.SUB2;
        if(PhoneConstants.SUB2==SubscriptionManager.from(mContext).getDefaultDataPhoneId()
           &&SubscriptionManager.from(mContext).getDefaultDataPhoneId()!=-1){
            Log.i(TAG,"SIM2="+SIM2);
            SIM2=PhoneConstants.SUB1;
        }
        return SIM2;
    }
    /* MODIFIED-END by qili.zhang,BUG-2243892*/
}
