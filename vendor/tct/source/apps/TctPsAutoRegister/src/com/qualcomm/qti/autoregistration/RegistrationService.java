/* Copyright (C) 2016 Tcl Corporation Limited */
/**
 * Copyright (c) 2014, Qualcomm Technologies, Inc. All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */
/******************************************************************************/
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 01/29/2016|     Dandan.Fang      |      PR1530879       |[Telecom][CHINA]- */
/*           |                      |                      |SIXM-04001 [Mand  */
/*           |                      |                      |atory]auto register*/
/*           |                      |                      |function          */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.qualcomm.qti.autoregistration;

import com.android.internal.telephony.PhoneConstants;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

//[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
//[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
import android.telephony.SubscriptionInfo;
import android.os.SystemProperties;
//[BUGFIX]-Add-END by TCTNB.Dandan.Fang

public class RegistrationService extends Service {

    private static final String TAG = "RegistrationService";
    private static final String BOOT_COMPLETE_FLAG = "boot_complete";
    private static final String MANUAL_REGISTRATION_FLAG = "manual";
    private static final boolean DBG = true;

    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
    //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
    private static final long DELAY_REQUEST_AFTER_POWER_ON = 60 * 1000; //1 minutes
    private static final long INTERVAL_RESCHEDUAL = 60 * 1000;
    private static final int MAX_REQUEST_TIMES = 5;
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    private static final String PREF_ICCID_ON_SUCCESS = "sim_iccid";
    private static final String PREF_REQUEST_COUNT = "register_request_count";

    public static final String ACTION_AUTO_REGISTERATION = "com.qualcomm.action.AUTO_REGISTRATION";

    private SharedPreferences mSharedPreferences;
    private AlarmManager mAlarmManager;

    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
    //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
    private static final String[] innListCtcc = {"898603","898606", "898611","8985302", "8985307"};
    private final String CARRIER_MOBILE = "46000";
    private final String CARRIER_MOBILE_EX = "46002";
    private final String CARRIER_UNICOM = "46001";
    private final String CARRIER_TELECOM = "46003";
    private final String CARRIER_TELECOM_EX = "46011";
    private final String CARRIER_TELECOM_EX2 = "46005";
    private final String CARRIER_TELECOM_EX3 = "46009";
    private static TelephonyManager mTelephonyManager = null;
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    @Override
    public void onCreate() {
        super.onCreate();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
        //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
        mTelephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
    //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
    @Override
    public void onDestroy() {
        Log.i(TAG,"stop services.");
        super.onDestroy();
    }
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getBooleanExtra(MANUAL_REGISTRATION_FLAG, false)) {
            onRegistrationRquestManually();
        } else {
            onRegistrationRquest(intent != null
                    && intent.getBooleanExtra(BOOT_COMPLETE_FLAG, false));
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void onRegistrationRquestManually() {
        new RegistrationTask(this) {
            @Override
            public void onResult(boolean registered, String resultDesc) {
                toast(resultDesc);
            }
        };
    }

    private void onRegistrationRquest(boolean bootComplete) {
        if (bootComplete) {
            mSharedPreferences.edit().putInt(PREF_REQUEST_COUNT, 0).commit();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onRegistrationRquest(false);
                }
            }, DELAY_REQUEST_AFTER_POWER_ON);
            return;
        }
        // MT in APM or SIM state not ready will not register.

        String iccIdText = getIccIdText();
        if (DBG) {
            Log.d(TAG, "Iccid: " + iccIdText);
        }

        if (isIccIdChanged(iccIdText)
                || iccIdText.equals(mSharedPreferences.getString(PREF_ICCID_ON_SUCCESS, null))) {
            //[BUGFIX]-ADD-Begin by TCTNB.qili.zhang for defect 1742728 03/07/2016
            //Toast.makeText(this, R.string.already_registered, Toast.LENGTH_LONG).show();
            //[BUGFIX]-ADD-Begin by TCTNB.qili.zhang for defect 1742728
            if (DBG) {
                Log.d(TAG, "Registered subs, Ignore");
            }
            onDestroy();
            return;
        }

        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
        //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
        if(!isSimStatusMeetContidion()){
            //Toast.makeText(this, R.string.sim_status_error, Toast.LENGTH_LONG).show();
            if (DBG) {
                Log.d(TAG, "sim status not meet the condition, ,not to register");
            }
            scheduleNextIfNeed();
            return;
        }else if(isNetworkInternationalRoaming()){
            //Toast.makeText(this, R.string.international_roam, Toast.LENGTH_LONG).show();
            if (DBG) {
                Log.d(TAG, "internation roaming, ,not to register");
            }
            scheduleNextIfNeed();
            return;
        }else if(!isAllowedToPsRegister()){
            //Toast.makeText(this, "data/wifi not ok on ctcc sub or wifi not ok on cmcc/cucc sub", Toast.LENGTH_LONG).show();
            if (DBG) {
                Log.d(TAG, "data/wifi not ok on ctcc sub or wifi not ok on cmcc/cucc sub");
            }
            scheduleNextIfNeed();
            return;
        }
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

        new RegistrationTask(this) {
            @Override
            public void onResult(boolean registered, String resultDesc) {
                if (!registered) {
                    scheduleNextIfNeed();
                } else {
                    mSharedPreferences.edit().putString(PREF_ICCID_ON_SUCCESS, getIccIdText())
                            .commit();
                    if (DBG) {
                        Log.d(TAG, "Register Done!");
                    }
                    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
                    //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
                    onDestroy();
                    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
                }
                toast(resultDesc);
            }
        };
    }

    private boolean isDDSInSub1() {
        // Without wifi but dds in SIM2 will not register.
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            if (TelephonyManager.getDefault().isMultiSimEnabled()) {
                if (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                        && SubscriptionManager
                        .getPhoneId(SubscriptionManager.getDefaultDataSubscriptionId())
                            == PhoneConstants.SUB2) {
                    if (DBG) {
                        Log.d(TAG, "DDS now in SIM2 without wifi, not to register");
                    }
                    return false;
                }
            }
        }
        return true;
    }

    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
    //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
    private boolean isNetworkInternationalRoaming(){
        int numPhones = TelephonyManager.getDefault().getPhoneCount();
        for (int index = 0; index < numPhones; index++) {
            if (TelephonyManager.getDefault().getSimState(index) == TelephonyManager.SIM_STATE_READY) {
                  String networkNumeric = null;
                  networkNumeric = TelephonyManager.getDefault().getSimOperatorNumeric(SubscriptionManager.getSubId(index)[0]);
                  Log.i(TAG,"networkNumberic= "+networkNumeric);
                  if (!TextUtils.isEmpty(networkNumeric) && !networkNumeric.startsWith("460")){
                      Log.i(TAG,"International roaming in slot ="+ index );
                      return true;
                  }
            }
        }
        return false;
    }

    private int getCtccCardSubId(){
        int subid = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
        int simCount = mTelephonyManager.getSimCount();
        for(int i = 0; i < simCount; i++){
           SubscriptionInfo sub = SubscriptionManager.from(RegistrationService.this)
                                 .getActiveSubscriptionInfoForSimSlotIndex(i);

           if(sub != null){
             String iccid = sub.getIccId();
             for(int idx = 0; idx < innListCtcc.length; idx++){
                 if(iccid != null && iccid.length() > 0 && innListCtcc[idx].equals(iccid.substring(0,6))){
                     SystemProperties.set("persist.sys.ctcc.card", "3");
                     subid = sub.getSubscriptionId();
                     return subid;
                 }
             }
           }
        }

        return subid;
     }

    private boolean isAllowedToPsRegister() {
       int subWithCtcc = getCtccCardSubId();

        // Without wifi but dds in SIM2 will not register.
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            if (TelephonyManager.getDefault().isMultiSimEnabled()
                    && networkInfo.getType() != ConnectivityManager.TYPE_WIFI) {
                if (subWithCtcc == SubscriptionManager.INVALID_SUBSCRIPTION_ID
                        || SubscriptionManager.getPhoneId(SubscriptionManager
                                .getDefaultDataSubscriptionId()) != SubscriptionManager
                                .getPhoneId(subWithCtcc)) {
                    if (DBG) {
                        Log.d(TAG,
                                "if dds not as ctcc sub, but wifi is not connected, not do auto register by data or wifi.");
                    }
                    return false;
                }
            }
        } else {
            Log.d(TAG,
                    "no data or wifi connected, not do auto register by data or wifi.");
            return false;
        }

        return true;
    }

    private boolean isSimStatusMeetContidion(){
        boolean slot1Ready = false;
        boolean slot2Ready = false;
        int numPhones = TelephonyManager.getDefault().getPhoneCount();
        for (int index = 0; index < numPhones; index++) {
            if (TelephonyManager.getDefault().getSimState(index) == TelephonyManager.SIM_STATE_READY) {
                if (index == PhoneConstants.SUB1) {
                    slot1Ready = true;
                } else {
                    slot2Ready = true;
                }
            }
        }

        if(slot1Ready && slot2Ready){
            if (isChinaOPERATOR(PhoneConstants.SUB1)
                    && isChinaOPERATOR(PhoneConstants.SUB2)) {
                Log.i(TAG, " sim ready with china operator in slot1 and slot2.");
                return true;
            }
        }if(slot1Ready == true && slot2Ready == false){
            if(isChinaOPERATOR(PhoneConstants.SUB1)){
            Log.i(TAG, " sim ready with china operator just in slot1 ");
            return true;
            }
        }
        return false;
    }

    private boolean isChinaOPERATOR(int slotId){
        String numeric;
        try {
            numeric = null;

            int subId = SubscriptionManager.getSubId(slotId)[0];
             if (subId != SubscriptionManager.INVALID_SUBSCRIPTION_ID){
                 Log.i(TAG," -- subId = " + subId +"phoneId =" + SubscriptionManager.getPhoneId(subId));
                numeric= mTelephonyManager.getSimOperator(subId);
             }else{
                return false;
             }

            Log.d(TAG, "subId =" + subId + "uim carrier: " + numeric );
            if ((numeric != null) && (CARRIER_UNICOM.equals(numeric))) {
                return true;
            }else if ((numeric != null) && (CARRIER_MOBILE.equals(numeric)
                    || CARRIER_MOBILE_EX.equals(numeric))){
                return true;
            }else if((numeric != null) && (numeric.equals(CARRIER_TELECOM)
                    || numeric.equals(CARRIER_TELECOM_EX)
                    || numeric.equals(CARRIER_TELECOM_EX2)
                    || numeric.equals(CARRIER_TELECOM_EX3)
                    || numeric.startsWith("455"))){
                return true;
            }
        } catch (Exception e) {
            Log.d(TAG, "ps carrier:error ");
            return false;
        }

        return false;
    }
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    private boolean isIccIdChanged(String iccId) {
        boolean flag = true;
        if (iccId.contains(",")) {
            String[] strArray = TextUtils.split(iccId, ",");
            for (String str : strArray) {
                flag &= TextUtils.isEmpty(str.trim());
            }
        } else {
            flag = TextUtils.isEmpty(iccId.trim());
        }
        return flag;
    }

    private String getIccIdText() {
        String iccId = null;
        if (TelephonyManager.getDefault().isMultiSimEnabled()) {
            int phoneCount = TelephonyManager.getDefault().getPhoneCount();
            for (int index = 0; index < phoneCount; index++) {
                Log.d(TAG, "Registered subs, SubscriptionManager.getSubId(index)[0] = " + SubscriptionManager.getSubId(index)[0] );
                String id = TelephonyManager.getDefault().getSimSerialNumber(
                        SubscriptionManager.getSubId(index)[0]);
                Log.d(TAG,"Iccid= "+id);
                if (id == null) {
                    id = " ";
                }
                if (iccId == null) {
                    iccId = id;
                } else {
                    iccId += ("," + id);
                }
            }
        } else {
            String id = TelephonyManager.getDefault().getSimSerialNumber();
            iccId = (null == id) ? " " : id;
        }
        return iccId;
    }

    protected void scheduleNextIfNeed() {
        int reuqestCount = mSharedPreferences.getInt(PREF_REQUEST_COUNT, 0) + 1;
        mSharedPreferences.edit().putInt(PREF_REQUEST_COUNT, reuqestCount).commit();
        if (reuqestCount < MAX_REQUEST_TIMES) {
            PendingIntent intent = PendingIntent.getBroadcast(this, 0, new Intent(
                    ACTION_AUTO_REGISTERATION), 0);
            mAlarmManager.set(AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis() + INTERVAL_RESCHEDUAL, intent);
        //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
        //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
        }else{
            Log.i(TAG,"retry timers moren then max request times, stop service");
            onDestroy();
        //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
        }
    }

    protected void toast(final String resultDesc) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                String resultInfo = null;
                if (TextUtils.isEmpty(resultDesc)) {
                    resultInfo = RegistrationService.this.getResources().getString(
                            R.string.register_failed);
                } else {
                    resultInfo = getLocalString(resultDesc);
                }
                //Toast.makeText(RegistrationService.this, resultInfo, Toast.LENGTH_LONG).show(); //MODIFIED by yongzhen.wang, 2016-04-07,BUG-1918269
            }
        });
    }

    private String getLocalString(String originalResult) {
        return (android.util.NativeTextHelper.getInternalLocalString(RegistrationService.this,
                originalResult,
                R.array.original_registry_results, R.array.local_registry_results));
    }

}
