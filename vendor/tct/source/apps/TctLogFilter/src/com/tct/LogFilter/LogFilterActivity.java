/******************************************************************************/
/*                                                               Date:10/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Yongzhen.wang                                                   */
/*  Email  :  null                                                            */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/TctLogFilter                            */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 10/17/2016|    Yongzhen.wang     |    defect-3014708    |[Telecom][Intern- */
/*           |                      |                      |al]Remove invalid */
/*           |                      |                      | logs information */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.tct.LogFilter;

import com.tct.LogFilter.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.RemoteException;
import android.os.IBinder;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.os.PowerManager;
import android.os.SystemClock;

public class LogFilterActivity extends Activity {

    private Context mContext;
    private final String TAG = "LogFilterActivity";

    private Button mButton01 = null;
    private Button mButton02 = null;
    private Button mButton03 = null;

    private TextView mTextView1 = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        setContentView(R.layout.main);

        mButton01 = (Button) findViewById(R.id.Button01);
        mButton02 = (Button) findViewById(R.id.Button02);
        mButton03 = (Button) findViewById(R.id.Button03);

        mTextView1=(TextView)findViewById(R.id.textView1);

        mButton03.setEnabled(true);
        String isOff = SystemProperties.get("persist.sys.ril_log_suprss_on", "0");
        if(isOff.equals("0")){
            mButton01.setEnabled(true);
            mButton02.setEnabled(false);
            mTextView1.setText("");
        }else{
            mButton01.setEnabled(false);
            mButton02.setEnabled(true);
            mTextView1.setText("RilQ log filter is running, not all logs can be captured under this scenario!");
        }

        mButton01.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String isOff = SystemProperties.get("persist.sys.ril_log_suprss_on", "0");
                if(isOff.equals("0")) {
                    SystemProperties.set("persist.sys.ril_log_suprss_on", "1");
                    mButton01.setEnabled(false);
                    mButton02.setEnabled(true);
                    mTextView1.setText("RILQ log filter is enabled, please reboot device!");
                }
            }
        });

        mButton02.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
                String isOff = SystemProperties.get("persist.sys.ril_log_suprss_on", "0");
                if(isOff.equals("1")){
                    SystemProperties.set("persist.sys.ril_log_suprss_on", "0");
                    mButton02.setEnabled(false);
                    mButton01.setEnabled(true);
                    mTextView1.setText("RILQ log filter is disabled, please reboot device!");
                }
            }
        });

        mButton03.setOnClickListener(new OnClickListener() {
        public void onClick(View v) {
            new Thread(new Runnable() {
                public void run() {
                    SystemClock.sleep(5000);
                    PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
                    pm.reboot("reboot");
                }
               }).start();
            }
        });
    }

    /**
     * Called when the activity will start interacting with the user.
     */
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    /**
     * Called when the system is about to start resuming a previous activity.
     */
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    /**
     * The final call you receive before your activity is destroyed.
     */
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

    }
}
