package com.jrdcom.arct;

import com.jrdcom.arct.Bundle;
import com.jrdcom.arct.PersoParserException;
import com.jrdcom.arct.PersoStringBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;


public class StringBuilderTests extends ArctTestBase {

    public void testBuild() {
        Bundle bd = new Bundle();
        bd.setLanguages(new String[]{"English"});
        bd.setModules(new String[]{"Undefined"});
        bd.setPersoPath("test/ascii.txt");
        bd.setOutputPath(ROOT+ File.separator +"ascii");
        bd.setCmd(Bundle.ArctCommand.wimdataToAndroidString);

        try {
            PersoStringBuilder builder = new PersoStringBuilder(bd);
            builder.build();
            assertNotNull("");


        } catch (UnsupportedEncodingException e) {
            assertTrue( false);
        } catch (FileNotFoundException e) {
            assertTrue( false);
        } catch (IOException e) {
            assertTrue( false);
        } catch (PersoParserException e) {
            assertTrue( false);
        } catch (LogAbortException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

}
