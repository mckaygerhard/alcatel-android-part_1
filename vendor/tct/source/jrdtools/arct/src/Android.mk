LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_MODULE_TAGS := optional

LOCAL_JAR_MANIFEST := ../etc/manifest.txt
LOCAL_JAVA_LIBRARIES := \
    jxl \
    ostermillerutils_1_07_00
LOCAL_MODULE := arct

include $(BUILD_HOST_JAVA_LIBRARY)

$(LOCAL_BUILT_MODULE) :  $(HOST_OUT_JAVA_LIBRARIES)/jxl$(COMMON_JAVA_PACKAGE_SUFFIX) \
             $(HOST_OUT_JAVA_LIBRARIES)/ostermillerutils_1_07_00$(COMMON_JAVA_PACKAGE_SUFFIX)


