/* ***************************************************************************/
/*                                                        Date : Dec 1, 2009 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct                                                          */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : Main.java                                                   */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*12/01/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

/**
 * Android Resource Customization Tool(arct) can help to transform perso text
 * file to or from android strings. Main businesses are contained in class
 * <b>PersoStringBuilder</b> and <b>StringPersoExporter</b>. PersoStringBuilder
 * do perso file to android string file. StringPersoExporter do on the contrary.
 * We can find important details in these two classes.
 *
 * @author zwshi
 */
public class Main {
    static String progName = "arct";
    static String klName = "arct";
    public static void usage(Log log) {
        log.error("Android Resource Customization Tool %s\n", Version.asString());
        log
                .error(
                        "Usage:\n"
                                + " %s w[imdataToAndroidString] [-L][-l <language name> [-l <language name> ...]]\n"
                                + "        [-M][-m <module name> [-m <module name> ...]]\n"
                                + "        [-f <strings file name>.{xml}]\n"
                                + "        [-I <path to file>]\n"
                                + "        <perso file> <repo base path>", progName);
        log
                .error("   Export string resources from wimdata xls file like \"/home/anywhere/ref.xls\" to"
                        + " repo base path like \"/home/anywhere/opal_repos/\".\n");
        log
                .error(
                        " %s a[ndroidStringToPerso] [-I <path to file>] <repo base path> <output perso file>",
                        progName);
        log
                .error("   Export string resources from repos base like \"/home/anywhere/opal_repos\" to"
                        + " an assigned output perso xls file like \"/home/anywhere/perso.xls\"\n");
        log.error(" %s p[lfToPropertyMakefile] <path to plf file> <output make file> <output prop file>", progName);

        log
                .error(
                        "   Build property make file form PLF file, run command like \"%s p /local/eclair/abc.plf /local/eclair/prop.mk /local/eclair/jrd.prop\"\n",
                        progName);
      //add by wteng
        log.error(" %s k[lfToKeypadLayout] <path to plf file> <output kl file>", klName);
        log
        .error(
                "   Build keypadlayout file form PLF file, run command like \"%s k /local/eclair/abc.plf /local/eclair/abc.kl\"\n",
                klName);
        // add end
        log
                .error(" Modifiers:\n"
                        + "   -L export all language Strings.xml included in perso file.\n"
                        + "   -l export one language, with the default is English. If -L is included, -l will be omitted.\n"
                        + "   -M export all res module included in perso file.\n"
                        + "   -m export one res module, with the default is framework res module. If -M is included, -m will be omitted.\n"
                        + "   -f specify which output file name to be used. The default is strings.xml.\n"
                        + "   -I specify a config file to indicate which paths to process. In the config file, every line is a path of a module, like \"/packages/apps/FileManager/res\", framework res module needn't be included, for it is always exported. If -I is not included, arct will use an internal module table.\n"
                        + "   -o specify a directory to output the string resources. This option would be availabe when w[imdataToAndroidString] is used. \n"
                        + "\n");

    }

    public static void main(String[] args) {


        Log log = Log.myLog;
        log.setVerbose(true);

        int result = 1;
        boolean wantUsage = false;

        String outputPathwhenOutOption=""; //added by zhiyu

        try {

            if (args.length < 1) {
                wantUsage = true;
                throw new LogAbortException("need more arguments");
            }

            Bundle bundle = new Bundle();
            switch (args[0].charAt(0)) {
                case 'w':
                    bundle.setCmd(Bundle.ArctCommand.wimdataToAndroidString);
                    break;
                case 'a':
                    bundle.setCmd(Bundle.ArctCommand.androidStringToPerso);
                    break;
                case 'p':
                    bundle.setCmd(Bundle.ArctCommand.plfToPropertyMakefile);
                    break;
                case 'k':
                    bundle.setCmd(Bundle.ArctCommand.plfToKeypadLayout);
                    break;
                default:
                    log.error("ERROR: Unknown command '%s'\n", args[1]);
                    wantUsage = true;
                    throw new LogAbortException("Unknown command '%s'", args[1]);
            }

            /*
             * Pull out flags. We support "-fv" and "-f -v".
             */
            int i = 1;
            Vector<String> languages = new Vector<String>();
            Vector<String> modules = new Vector<String>();
//            Vector<String> paths = new Vector<String>();
            Vector<ResPath> respaths = new Vector<ResPath>();

            while (args.length > i && args[i].charAt(0) == '-') {
                /* flag(s) found */
                String cp = args[i];
                int pos = 1;
                inner: while (cp.length() > pos) {
                    switch (cp.charAt(pos)) {
                        case 'd':
                        case 'D':
                            bundle.setDedug(true);
                            break;
                        case 'L':
                            bundle.setAllLanguage(true);
                            break;
                        case 'l':
                            i++;
                            appendLanguage(languages, args[i]);
                            break inner;
                        case 'M':
                            bundle.setAllModule(true);
                            break;
                        case 'm':
                            i++;
                            appendModule(modules, args[i]);
                            break inner;
                        case 'f':
                            log.debug("add argu f");
                            break;
                        case 'I':
                            i++;
//                            parsePaths(paths, args[i]);
                            parseResPaths(respaths, args[i]);
                            //added by zhiwei, CR CR106384 initial Constant (local code mapping)
                            //String tctDevice = System.getenv("TARGET_PRODUCT");
                            //modify yanqi.liu
                            //String product_arct_tools_path = "/device/tct/common/perso";
                            //initCodesMap(args[i].substring(0,args[i].lastIndexOf(product_arct_tools_path))+"/custo_wimdata_ng/wlanguage/src/local.config");
                            break;
                        case 'h':
                            throw new LogAbortException("requrire help.");

                        //added by zhiyu, revise for adding output directory option
                        case 'o':
                            i++;
                            outputPathwhenOutOption=args[i];
                            break;
                        // end
                        // add by yanqi.liu
                        case 'c':
                            i++;
                            initCodesMap(args[i]);
                            break;

                        default:
                            log.error("ERROR: Unknown flag '-%c'\n", cp.charAt(pos));
                            wantUsage = true;
                            throw new LogAbortException("unknow flag");
                    }
                    pos++;
                }
                i++;
            }

            if (bundle.getCmd().equals(
                    Bundle.ArctCommand.wimdataToAndroidString)) {
                bundle.setPersoPath(args[i]);
                i++;

                log.errPrintln("arg["+i+"]="+i);
                log.errPrintln("arg["+i+"]="+args[i]);
                log.errPrintln("arg["+i+"]="+args[i]);

                bundle.setSourcePath(args[i]);//added by zhiyu

                if (outputPathwhenOutOption == null
                        || outputPathwhenOutOption.equalsIgnoreCase(""))
                    bundle.setOutputPath(args[i]);
                else
                    bundle.setOutputPath(outputPathwhenOutOption);

                if (!bundle.isAllLanguage() && languages.size() == 0) {
                    throw new LogAbortException("no language is appointed.");
                } else if (!bundle.isAllLanguage() && languages.size() > 0)
                    bundle.setLanguages(languages.toArray(new String[0]));
                if (!bundle.isAllModule() && modules.size() == 0) {
                    throw new LogAbortException("no module is appointed.");
                } else if (!bundle.isAllModule() && modules.size() > 0)
                    bundle.setModules(modules.toArray(new String[0]));
            } else if (bundle.getCmd().equals(Bundle.ArctCommand.androidStringToPerso)) {
                bundle.setRepoPath(args[i]);
                i++;
                bundle.setPersoPath(args[i]);
            } else if (bundle.getCmd().equals(Bundle.ArctCommand.plfToPropertyMakefile)) {
                bundle.setPlfPath(args[i]);
                i++;
                bundle.setMakePath(args[i]);
                i++;
                bundle.setPropPath(args[i]);
            }else if (bundle.getCmd().equals(Bundle.ArctCommand.plfToKeypadLayout)) {
                bundle.setPlfPath(args[i]);
                i++;
                bundle.setKLPath(args[i]);

            }
            if (respaths.size() > 0) {
                Constant.setAppResSetup(respaths);
            }
            bundle.handleCommand();
            System.exit(0);
        } catch (LogAbortException exception) {

            exception.printStackTrace();
     //revised by zhiyu for PR106102
            if (wantUsage) {
                usage(log);
                result = 1;
            } else {
                exception.error(log);
                result = 1;
            }
            System.exit(result);

        }catch (Exception exceptionAll) {
            exceptionAll.printStackTrace();
            if (wantUsage) {
                usage(log);
                result = 1;
            } else {
                result = 1;
            }
            System.exit(result);
        }
        //end of for PR106102
    }

    private static void appendLanguage(Vector<String> names, String input) throws LogAbortException {
        input = input.trim();
        if (input == null)
            throw new LogAbortException("require language name.");
        for (int i = 0; i < names.size(); i++) {
            if (names.elementAt(i).equals(input)) {
                throw new LogAbortException("duplicated language name:" + input);
            }
        }
        String[][] code = Constant.getCodes();
        for (int i = 0; i < code.length; i++) {
            if (code[i][0].equals(input)) {
                names.add(input);
                return;
            }
        }
        throw new LogAbortException("language name %s not found in language table.", input);

    }

    private static void appendModule(Vector<String> names, String input) throws LogAbortException {
        input = input.trim();
        if (input == null)
            throw new LogAbortException("require module name.");
        for (int i = 0; i < names.size(); i++) {
            if (names.elementAt(i).equals(input)) {
                throw new LogAbortException("duplicated module name:" + input);
            }
        }
        names.add(input);

    }

    /**************************************deleted by zhiyu
    private static void parsePaths(Vector<String> paths, String input) throws LogAbortException {
        try {
            BufferedReader in = new BufferedReader(new FileReader(input.trim()));
            String line = null;
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if (line.startsWith("#") || line.length() <= 3)
                    continue;
                int i = line.lastIndexOf("res");
                if (i > 0) {
                    if (paths.contains(line.substring(0, i + 3))) {
                        Log.myLog.info("duplicated path: %s, now omitted", line);
                        continue;
                    }
                    paths.add(line.substring(line.startsWith("./") ? 1 : 0, i + 3));

                } else
                    throw new LogAbortException("Not a valid module path: " + line);
            }

        } catch (FileNotFoundException e) {
            throw new LogAbortException("parsePaths fails: " + e.getMessage());
        } catch (IOException e) {
            throw new LogAbortException("parsePaths fails: " + e.getMessage());
        }

    }*********************************************************/

    //added by zhiyu, reserve for path and module name
    private static void parseResPaths(Vector<ResPath> pathRes, String input) throws LogAbortException {
        try {
            BufferedReader in = new BufferedReader(new FileReader(input.trim()));
            String line = null;
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if (line.startsWith("#") || line.length() <= 3)
                    continue;
                int i = line.lastIndexOf("res");
                if (i > 0) {
                    if (pathRes.contains(line.substring(0, i + 3))) {
                        Log.myLog
                                .info("duplicated path: %s, now omitted", line);
                        continue;
                    }
                    String resp = line.substring(line.startsWith("./") ? 1 : 0,
                            i + 3);// path extraction

                    String mname = "";
                    if (line.indexOf(":") > 0)
                        mname = line.substring(line.indexOf(":")+1); // module name extraction
                    else {
                        String[] ts = line.split("/");
                        mname = ts[ts.length - 2];
                    }

                    pathRes.add(new ResPath(resp, mname));

                } else
                    throw new LogAbortException("Not a valid module path: "
                            + line);
            }

        } catch (FileNotFoundException e) {
            throw new LogAbortException("parsePaths fails: " + e.getMessage());
        } catch (IOException e) {
            throw new LogAbortException("parsePaths fails: " + e.getMessage());
        }

    }

    //add by zhiwei  CR106384
    private static void initCodesMap(String path) throws LogAbortException {
         try {
                BufferedReader in = new BufferedReader(new FileReader(path.trim()));
                String line = null;
                while ((line = in.readLine()) != null) {
                     line = line.trim();
                     if (line.startsWith("#") || line.length() <= 3)
                        continue;
                     String flag=line.substring(0, line.indexOf("="));
                     String content=line.substring(line.indexOf("=")+1)+" ";
                     String[] ts = content.split(",");
                     for (int i=0;i<ts.length;i++){
                         ts[i]=ts[i].trim();
                     }
                     if(!"code".equals(flag) && !"extraCode".equals(flag)) {
                         throw new LogAbortException("initCodesMap: code type is not define" );
                     }
                     if("code".equals(flag)){
                         Constant.language.put(ts[0], ts);
                     }
                     if("extraCode".equals(flag)){
                         Constant.addExtraLanguage(Constant.extraLanguage,ts[0], ts[2]);
                     }
                    }
            }
           catch (FileNotFoundException e) {
                throw new LogAbortException("initCodesMap fails: " + e.getMessage());
            }
           catch (IOException e) {
                throw new LogAbortException("initCodesMap fails: " + e.getMessage());
            }
           catch(Exception e){
               throw new LogAbortException("initCodesMap fails: " + e.getMessage());
           }
    }
}
