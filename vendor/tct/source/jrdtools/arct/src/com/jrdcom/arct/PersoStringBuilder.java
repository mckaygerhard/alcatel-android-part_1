/* ***************************************************************************/
/*                                                            Date : Dec 17, 2009 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct_dint                                               */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : PersoStringBuilderEx.java                                                */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*01/12/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.jrdcom.arct.StringsHelper.StringItem;

/**
 * PersoStringBuilder supply method to transform perso text file to android strings.
 *
 * <h3>perso text file format</h3>
 * <p>Perso text file is a xls(Excel 2000/xp) file.
 * <p>- The file format shall like below:
 * <ul>
 * <li>first line is column names: &quot;RefName    ModOP   Info    ZoneType    IsMono  IsUK    IsGSM   IsTradUpdatable
 * &lt;language name 1&gt;&lt;language name 2&gt;...&quot; Language names can be
 *  found in Constant Class;
 * <li>second line to end line: every line presents a string item.
 * </ul>
 *  <p>- Item value of main language should not be null. Default main language is English(#0044)
 *  <p>- A string item will be export into many strings.xml of different locales.
 *  <h3>reference name</h3>
 *  <p>- Column name refName includes application name, string name or
 *  string array name. names are divided by a ':'. For example: "Phone:tty_mode_entries:0" means
 *  application name is 'Phone', 'tty_mode_entries' is a string array for it is followed by a '0',
 *  the item is the 0 item of tty_mode_entries array.
 *  <h3>value</h3>
 *  <p>- To be compatible with xml format, &lt;(&amp;lt;) &gt;(&amp;gt;) &amp;(&amp;amp;) that not in a tag(xliff or html tag)should be escaped.
 *  <p>- Html tag and xliff tag will be used in android string value. These tags will
 *  be keep in original.
 *  <p>- Symbol '"'(ascii0x22) is special character in android strings.xml since eclair.
 *  It is used to enclose other special characters(like ',@,?) to keep these characters
 *  recognized by android string parser like cases below:
 *  <ul>
 *   <li>'?'or'@' started strings, like: "? hello", "@where.com"; See the whole string is enclosed with "";
 *   <li>''' included strings, like: "say 'hello' to somebody"; See the whole string is enclosed with "";
 *   Another way to use ''' is escape it to &amp;apos;
 *   <li>'"' will be omitted by Android string parser. So a real '"' should be replaced by '\"'(ascii0x5c0x22).
 *   </ul>
 *  <p>- Null not allowed in default language
 *  <p>- Input value of string items may be modified by system in case below:
 *  <ul>
 *   <li>One or more in sequence whitespace character(\t\n\x0B\f\r)  will be replaced by one sp(ascii0x20)‏
 *   </ul>
 *  <p>-(Unapplied rule) For a special string usage, '<'(ascii0x3C) of html or xliff tags shall be
 *  escaped. This situation need a flag column in perso text. When this flag column is arranged, this rule should be applied.
 *   <h3>comment and skip</h3>
 *   <p>- Column  Info will be treated as comment of the string item which will be
 *   put close before the string item.
 *   <p>- If string value of a language is null, a skip tag will be
 *   put at the position in the string xml of this language.
 *   <h3>output folder structure</h3>
 *   <p>- Output base folder should be appointed at command line. In output base folder,
 *   PersoStringBuilder will create a number of folders named as values in Module Name
 *   column. Each ame of these folders also is a name of applications or modules.
 *   For example, assume UnifniedMessage is a module name in perso text file. After transform,
 *   under UnifniedMessage may look like below:
 *   <pre>          UnifniedMessage/res/values
 *                        /res/values-zh-rTW
 *                        /res/values-zh-rCN
 *                        /res/values-th-rTH
 *                        /res/values-es
 *                       /res/values-fr
 *                       /res/values-de
 *                       /res/values-ru
 *                       /res/values-it
 *                       /res/values-ar
 *                        ... ...</pre>
 *   The number and the suffix of the values may be different according to the translation value
 *   in perso text file.
 *   <h3>strings' sequence</h3>
 *   <p>- Every string item has a same sequence as in the perso text file categoried by
 *   module name and language name.
 *
 * @author zwshi
 *
 */

public class PersoStringBuilder extends XMLResourceHandler {
    private Bundle bundle;

    Log log = new Log();

    private int languageCount = 0;

    private String[] langNames;

    private OutputStreamWriter ow;

    HashMap<String, StringsHelper> moduleMap;

    Vector<String> moduleTable;

    DocumentBuilder documentBuilder;

    Element root;

    Document xmldoc;

    Vector<String> noTranslation;

    public PersoStringBuilder(Bundle bundle) throws UnsupportedEncodingException,
            FileNotFoundException, ParserConfigurationException {
        this.bundle = bundle;
        log.setVerbose(true);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        documentBuilder = factory.newDocumentBuilder();
    }

    private PersoStringBuilder() {
    }

     void readinPersoXls() throws LogAbortException, BiffException, IOException,
            PersoParserException {
        // init persoPath
        WorkbookSettings workbookSettings = new WorkbookSettings();
        workbookSettings.setEncoding("ISO-8859-1");
        Workbook w = Workbook.getWorkbook(new File(bundle.getPersoPath()), workbookSettings);
        log.info("Perso path: %s opened", bundle.getPersoPath());
        Sheet sheet = w.getSheet("MESSAGE");
        if (sheet == null)
            throw new PersoParserException("No MESSAGE sheet in xls file.");
        Cell[] row = null;
        row = sheet.getRow(0);
        if (row.length < Constant.persoPreceedColumn.length) {
            throw new PersoParserException("name error of column title.");
        }
        for (int i = 0; i < Constant.persoPreceedColumn.length; i++) {
            if (!row[i].getContents().equals(Constant.persoPreceedColumn[i]))
                throw new PersoParserException("name error of column title: "
                        + row[i].getContents());
        }
        int languageCount = row.length - Constant.persoPreceedColumn.length;
        langNames = new String[languageCount];
        for (int i = Constant.persoPreceedColumn.length; i < row.length; i++) {
            langNames[i - Constant.persoPreceedColumn.length] = row[i].getContents();
        }
        //PR68162-wei.wu start
        log.info("output languages:%s", bundle.isAllLanguage() ? Arrays.deepToString(langNames) : Arrays.deepToString(bundle
               .getLanguages()));
        //PR68162-wei.wu end
        moduleMap = new HashMap<String, StringsHelper>();
        moduleTable = new Vector<String>();
        StringBuffer parseXMLError = new StringBuffer();
        String[] t = null;
        if (bundle.isAllModule()) {
            // create structure preparing for output
            for (int rowN = 1; rowN < sheet.getRows(); rowN++) {
                t = new String[8 + languageCount];
                row = sheet.getRow(rowN);
                for (int columnN = 0; columnN < t.length; columnN++) {
                    if (columnN < row.length)
                        t[columnN] = row[columnN].getContents();
                    else
                        t[columnN] = "";

                }
                StringsHelper strHelper = moduleMap.get(t[1]);
                if (strHelper == null) {
                    moduleTable.add(row[1].getContents());
                    strHelper = new StringsHelper();
                    moduleMap.put(t[1], strHelper);
                }
                checkStringValue(t, parseXMLError);
                strHelper.put(t);
            }
        } else {
            for (int j = 0; j < bundle.getModules().length; j++) {
                moduleTable.add(bundle.getModules()[j]);
                moduleMap.put(bundle.getModules()[j], new StringsHelper());

            }
            // create structure preparing for output
            for (int rowN = 1; rowN < sheet.getRows(); rowN++) {
                t = new String[8 + languageCount];
                row = sheet.getRow(rowN);
                for (int columnN = 0; columnN < t.length; columnN++) {
                    if (columnN < row.length)
                        t[columnN] = row[columnN].getContents();
                    else
                        t[columnN] = "";

                }
                StringsHelper strHelper = moduleMap.get(t[1]);
                if (strHelper != null) {
                    checkStringValue(t, parseXMLError);
                    strHelper.put(t);
                }
            }
        }
        if (parseXMLError.length() > 0) {
            throw new PersoParserException("XML parse error:\r\n" + parseXMLError.toString());
        }
    }

    void checkStringValue(String value[], StringBuffer errorBuffer) {

        StringBuffer noTranslationReport = null;
        for (int i = Constant.persoPreceedColumn.length; i < value.length; i++) {
            if (value[i] == null || value[i].length() <= 0) {
                if (!langNames[i - Constant.persoPreceedColumn.length]
                        .equals(Constant.defaultLanguage)
                        && !value[Constant.isMonoColumn].startsWith("1")) {
                    if (noTranslationReport == null) {
                        noTranslationReport = new StringBuffer();
                        noTranslationReport.append(value[0] + "\t\t has no ");
                        noTranslationReport
                                .append(langNames[i - Constant.persoPreceedColumn.length] + ",");
                    } else {
                        noTranslationReport
                                .append(langNames[i - Constant.persoPreceedColumn.length] + ",");
                    }
                }
                continue;
            }
            try {
                documentBuilder.parse(new InputSource(new StringReader(String.format(
                        "<string>%s</string>", value[i]))));
            } catch (SAXException e) {
                 //PR68162-wei.wu start
                errorBuffer.append(value[0] + " " + value[i] + " " + e.getMessage() + "\r\n");
                //PR68162-wei.wu end
            } catch (IOException e) {
                //PR68162-wei.wu start
                errorBuffer.append(value[0] + " " + value[i] + " " + e.getMessage() + "\r\n");
                //PR68162-wei.wu end
            }
        }
        if (noTranslationReport != null) {
            noTranslation.addElement(noTranslationReport.toString());
        }
    }

    void checkRepo() throws IOException, LogAbortException {
        String[][] coreResSetup = Constant.getCoreResSetup();
        try {
            for (int i = 0; i < coreResSetup.length; i++) {
//                xmldoc = documentBuilder
//                        .parse(new File(bundle.getOutputPath() + coreResSetup[i][1])
//                                + "/values/strings.xml");

                System.out.println("bundle.getSourcePath()="+bundle.getSourcePath()+coreResSetup[i][1]);
                System.out.println("bundle.getOutputPath="+bundle.getOutputPath());
                // revised by zhiyu
                xmldoc = documentBuilder
                .parse(new File(bundle.getSourcePath() + coreResSetup[i][1])
                        + "/values/strings.xml");

                root = xmldoc.getDocumentElement();
                List<StringItem> textList = moduleMap.get(coreResSetup[i][0]).getSortedAll();
                StringBuffer strBuffer = new StringBuffer();
                for (int j = 0; j < textList.size(); j++) {
                    String name = textList.get(j).getName();
                    if (selectSingleNode(String.format("/resources/%s[@name='%s']", textList.get(j)
                            .getType().getValue(), name), root) == null) {
                        strBuffer.append("Warning:" + name + " not found in " + coreResSetup[i][1]
                                + "/values/strings.xml,ignore\r\n");
                    }
                }
                if (strBuffer.length() > 0) {
                    //throw new LogAbortException(strBuffer.toString());
                    log.info("%s", strBuffer.toString());
                }
            }
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    void exportDefaultModule(StringsHelper strHelper, String moduleName) throws LogAbortException,
            PersoParserException,IOException, SAXException {
        String lang = Constant.defaultLanguage;
        // check for default language
        int column = -1;
        for (int i = 0; i < langNames.length; i++) {
            if (langNames[i].equals(lang)) {
                column = Constant.persoPreceedColumn.length + i;
                break;
              }
          }
        if (column == -1) {
            throw new PersoParserException("default language not found: " + lang);
          }
        //begin,added by cwang, 20101116
        //if the moduleName not exist in file brandy_wimdata_ng/wprocedures/string_res.ini
        //perso will not build that module
        if(!Constant.isAppExistInAppRes(moduleName)&&!moduleName.equalsIgnoreCase("frameworks")){ //revised by zhiyu for export res in frameworks/base/core/res/res
//            log.info("perso ignore app %s",moduleName);
            return ;
          }
        //end


        String baseName = bundle.getSourcePath() + Constant.getModulePath(moduleName);//modified by Zhiyu
        String outputName = bundle.getOutputPath()+ Constant.getModulePath(moduleName);//modified by Zhiyu
        if (!new File(baseName).isDirectory())
            throw new LogAbortException(baseName + " directory not existed for module "
                    + moduleName + ".");

        System.out.println("baseName="+baseName);
        System.out.println("outputName="+outputName);

        log.info("now processing module %s default language...", moduleName);
        Constant.ResType resType = Constant.getResType(moduleName);
        HashMap<String, StringItem> increment = (HashMap<String, StringItem>)strHelper.sh.clone();
              File oriFolder = new File(baseName+"/values");
              File newFolder = new File(outputName+"/values");
              if(!oriFolder.exists()){
                  return;
              }
              File[] fileList = oriFolder.listFiles();
              if(fileList != null && fileList.length > 0){
                  if(!newFolder.exists())
                       newFolder.mkdirs();
                  for(int index =0; index < fileList.length; index++){
                Matcher matcher = Constant.FILTER_FILES.matcher(fileList[index].getName());
                if (!matcher.matches()) {
                     //log.info("index="+index+" process fileList="+fileList[index]);
                            File oriFile = new File(fileList[index].getPath());
                            File newFile = new File(newFolder,"strings_created.xml");

            log.info("oriFile=%s,newFile=%s", oriFile.getPath(),newFile.getPath());

            xmldoc = documentBuilder.parse(oriFile);
            Element root = xmldoc.getDocumentElement();
            ow = new OutputStreamWriter(new FileOutputStream(newFile), "utf-8");
            putXmlHeader();
            putResourcesBeginTag();
            NodeList list = root.getChildNodes();
            for (int j = 0; j < list.getLength(); j++) {
                if (!(list.item(j) instanceof Element)) {

                    ow.write(copyNodeText(list.item(j)));
                    ow.flush();
                    continue;
                   }
                Element currN = (Element)(list.item(j));
                try {
//                    if (currN.getAttribute("translatable") != null
//                            && currN.getAttribute("translatable").equals("false")) {
//                        ow.write(copyNodeText(currN));
//                        ow.flush();
//                    } else
                    if (currN.getNodeName().equals("string")) {
                                String name = currN.getAttribute("name");
                                String product = currN.getAttribute("product");
                                StringItem strItem = null;
                                if ("".equals(product) || null == product) {
                                    strItem = strHelper.get("S:" + moduleName + ":" + name); // come
                                                                                             // from
                                                                                             // excel
                                                                                             // file
                                } else {
                                    strItem = strHelper.get("S:" + moduleName + ":" + name + ":"
                                            + product); // come from excel file
                                }

                        increment.remove(strItem.getFullName());
                        String currV = strItem.getAll().get(0)[column];
                        if (currV != null && currV.length() > 0) {
                            currN.setTextContent(String.format("###%s###", currN
                                    .getAttribute("name")));
                            ow.write(copyNodeText(currN).replace(
                                            String.format("###%s###", currN.getAttribute("name")),
                                            currV));
                                } else {
                            ow.write(copyNodeText(currN));
                        }

                    } else if (currN.getNodeName().equals("plurals")) {
                        String name = currN.getAttribute("name");
                        String indexName = (currN.getNodeName().equals("string-array") ? "A:"
                                : "P:")
                                + moduleName + ":" + name;
                        StringItem strItem = strHelper.get(indexName);
                        increment.remove(indexName);
                        NodeList li = selectNodes("./item", currN);
                        String outputStrO = copyNodeText(currN);
                        for (int k = 0; k < li.getLength(); k++) {
                            li.item(k).setTextContent(String.format("###item%d###", k));
                            }
                        String outputStr = copyNodeText(currN);
                        boolean oneItemFail = false;
                        for (int k = 0; k < li.getLength(); k++) {
                            String currV=null ;
                            try{
                                //PR 97244 --zhiwei.liu begin
                                String quantity_xml=li.item(k).getAttributes().getNamedItem("quantity").getNodeValue();
                                String quantity_xls=strItem.getAll().get(k)[0].split(":")[3];
                                if (quantity_xml.equals(quantity_xls)){
                                    currV= strItem.getAll().get(k)[column];
                                     }
                                else{
                                    for (int i=0;i<strItem.getAll().size();i++){
                                        if(quantity_xml.equals(strItem.getAll().get(i)[0].split(":")[3])){
                                            currV=strItem.getAll().get(i)[column];
                                            break;
                                              }
                                        else{
                                            continue;
                                               }
                                    }
                                }
                                //PR 97244 --zhiwei.liu end

                            if (currV != null && currV.length() > 0) {
                                outputStr = outputStr.replace(String.format("###item%d###", k),
                                        currV);
                            } else {
                                oneItemFail = true;
                                break;
                                }
                            }catch (RuntimeException e) {
                                oneItemFail=true ;
                            }
                        }
                        if (oneItemFail)
                            ow.write(outputStrO);
                        else
                            ow.write(outputStr);
                    } else if (currN.getNodeName().equals("string-array")){


                        String name = currN.getAttribute("name");
                        String indexName = (currN.getNodeName().equals(
                                "string-array") ? "A:" : "P:")
                                + moduleName + ":" + name;
                        StringItem strItem = strHelper.get(indexName);
                        increment.remove(indexName);
                        NodeList li = selectNodes("./item", currN);
                        String outputStrO = copyNodeText(currN);
                        for (int k = 0; k < li.getLength(); k++) {
                            li.item(k).setTextContent(
                                    String.format("###item%d###", k));
                        }
                        String outputStr = copyNodeText(currN);
                        boolean oneItemFail = false;
                        try{for (int k = 0; k < li.getLength(); k++) {
                            String currV = null;
                            String indexName1 = indexName + ":" + k;
                            for (int i = 0; i < li.getLength(); i++) {

                                if (indexName1
                                        .equals(strItem.getAll().get(i)[0])) {

                                    try {
                                        currV = strItem.getAll().get(i)[column];

                                        if (currV != null && currV.length() > 0) {
                                            outputStr = outputStr.replace(
                                                    String.format(
                                                            "###item%d###", k),
                                                    currV);
                                        } else {
                                            oneItemFail = true;
                                            break;
                                        }
                                    } catch (RuntimeException e) {
                                        oneItemFail = true;
                                    }
                                }
                            }
                        }
                        }catch (RuntimeException e) {
                            oneItemFail=true ;
                        }
                        if (oneItemFail)
                            ow.write(outputStrO);
                        else
                            ow.write(outputStr);


                    }
                    else {
                        ow.write(copyNodeText(currN));
                    }
                } catch (RuntimeException e) {
                    ow.write(outputString(currN));// ignore process
                }
                ow.flush();
            }
            // MIMS maintains one database for one software project no matter
            // how much branches the project has. So after
            // DR4 branch frozen, if we export the increment into repo base, we
            // may merge wrong strings into it.
            // if (resType.equals(Constant.ResType.app) && !increment.isEmpty())
            // {
            // List<StringItem> inclist = strHelper.getSortedAll(increment);
            // for (int j = 0; j < inclist.size(); j++) {
            // StringItem item = inclist.get(j);
            // if (item.getType().equals(StringType.string)) {
            // putString(item.getName(), item, column);
            // } else if (item.getType().equals(StringType.stringArray)) {
            // String indexName = "A:"+ moduleName + ":" + item.getName();
            // putStringArray(item.getName(),indexName, item, column);
            // } else if (item.getType().equals(StringType.plurals)) {
            // putPlurals(item.getName(), item, column);
            // }
            // }
            // }

            // Now we should warn the strings in increment, not merge them.
            if (!increment.isEmpty()) {
                List<StringItem> inclist = strHelper.getSortedAll(increment);
                for (int j = 0; j < inclist.size(); j++) {
                    StringItem item = inclist.get(j);
                    log.info("Warning: extra string " + item.getFullName() + " not merged");
                }
            }
            putResourcesEndTag();
            ow.close();

            File destFile = new File(newFolder,oriFile.getName());
            if(destFile.exists()) destFile.delete();
               newFile.renameTo(destFile);
               //log.info("newFile=%s,destFile=%s", newFile, destFile);
                log.info("%s Ok", moduleName);
                copyFileToExtralLocal(lang,destFile);

             }


                  }


              }

        }



        private void copyFileToExtralLocal(String lang, File srcFile){
            //added by zhiyu, add the extra language generation for PR106355
            try{
              ArrayList<String> extraL=Constant.getExtraLocale(lang);
              if(extraL!=null&&extraL.size()!=0){
                  for(int i=0;i<extraL.size();i++){
                      System.out.println("extra language is being processsed= "+extraL.get(i));

                      File destFile = new File(String.format("%s/values-%s/%s", srcFile.getParentFile().getParent(),extraL.get(i),srcFile.getName()));
                      if (!destFile.getParentFile().exists())
                          destFile.getParentFile().mkdirs();
                      System.out.println("COPY"+srcFile.getAbsolutePath()+" TO="+destFile.getAbsolutePath());
                          this.copyFile(srcFile, destFile);
                    }
                 }
               }catch(Exception e){
                e.printStackTrace();

            }
        }
//==================================================
    //
    private void buildModule(StringsHelper strHelper, String lang, String module)
            throws IOException, PersoParserException, LogAbortException, SAXException {

        // check if language is available
        int column = -1;
        for (int i = 0; i < langNames.length; i++) {
            //added by zhixun.wang for apply english US translation to english AU and english NZ, bug[970973]
            if ((lang.equals("English_AU")&&langNames[i].equals("English_US"))||(lang.equals("English_NZ")&&langNames[i].equals("English_US"))) {
                column = Constant.persoPreceedColumn.length + i;
                break;
            }
            //end of bug[970973]
            if (langNames[i].equals(lang)) {
                column = Constant.persoPreceedColumn.length + i;
                break;
            }
        }
        if (column == -1) {
            throw new PersoParserException("language name error: " + lang);
        }

        //begin,added by cwang, 20101116
        //if the moduleName not exist in file brandy_wimdata_ng/wprocedures/string_res.ini
        //perso will not build that module
        if(!Constant.isAppExistInAppRes(module)&&!module.equalsIgnoreCase("frameworks")){ //revised by zhiyu for export res in frameworks/base/core/res/res
//            log.info("perso ignore app %s",module);
            return ;
        }
        //end


        // check if has translated item. if no, do not create dir and file
        boolean hasLangItem = false;
        List<StringItem> ilist = strHelper.getSortedAll();
        for (int i = 0; i < ilist.size(); i++) {
            String value = ilist.get(i).getAll().get(0)[column];
            if (value != null && value.trim().length() > 0) {
                hasLangItem = true;
                break;
            }
        }

        if (!hasLangItem) {
            log.info(module + "/" + lang + " has no item, now skip.");
            return;
        }

        String baseName = bundle.getOutputPath() + Constant.getModulePath(module);
        String sourceName = bundle.getSourcePath() + Constant.getModulePath(module);//added by zhiyu
        File oriFolder = new File(sourceName+"/values");
        File newFolder = new File(String.format("%s/values-%s/", baseName,
              Constant.getLocale(lang)));

      if(!oriFolder.exists()){
          return;
      }
      System.out.println(oriFolder);
      File[] fileList = oriFolder.listFiles();
      if(fileList != null && fileList.length > 0){
          if(!newFolder.exists())
               newFolder.mkdirs();
          System.out.println("lenght="+fileList.length);
          for(int index =0; index < fileList.length; index++){
                Matcher matcher = Constant.FILTER_FILES.matcher(fileList[index].getName());
                if (!matcher.matches()) {
                 //log.info("index="+index+" process fileList="+fileList[index]);
                    File oriFile = new File(fileList[index].getPath());
                    File newFile = new File(newFolder,"strings_created.xml");

       log.info("buildmodule 680 %s to %s",oriFile, newFile);
        ow = new OutputStreamWriter(new FileOutputStream(newFile), "utf-8");

        putXmlHeader();
        putResourcesBeginTag();
        ow.write("\r\n");

        xmldoc = documentBuilder.parse(oriFile);//modified by zhiyu


        Element root = xmldoc.getDocumentElement();
        NodeList list = root.getChildNodes();
        for (int j = 0; j < list.getLength(); j++) {
            if (!(list.item(j) instanceof Element)) {
                continue;
            }

            Element currN = (Element)(list.item(j));
            if (currN.getAttribute("translatable") != null
                    && currN.getAttribute("translatable").equals("false")) {
                // skip it
                continue;
            } else if (currN.getNodeName().equals("string")) {
                            String name = currN.getAttribute("name");
                            String product = currN.getAttribute("product");
                            StringItem strItem = null;
                            if ("".equals(product) || null == product) {
                                strItem = strHelper.get("S:" + module + ":" + name);
                            } else {
                                strItem = strHelper.get("S:" + module + ":" + name + ":" + product);
                            }

                            putString(name, strItem, product, column);
            } else if (currN.getNodeName().equals("string-array")) {
                String name = currN.getAttribute("name");
                String indexName = "A:" + module + ":" + name;
                StringItem strItem = strHelper.get(indexName);
                putStringArray(currN, name,indexName, strItem, column);
            } else if (currN.getNodeName().equals("plurals")) {
                String name = currN.getAttribute("name");
                StringItem strItem = strHelper.get("P:" + module + ":" + name);
                putPlurals(currN, name, strItem, column);
            }

        }
        putResourcesEndTag();
        ow.flush(); //added by zhiyu
        ow.close();

        File destFile = new File(newFolder, oriFile.getName());
        destFile.delete();  //modified by zhiyu
        newFile.renameTo(destFile.getAbsoluteFile());
        //log.info("file created:" + oriFile.getAbsolutePath());
        copyFileToExtralLocal(lang,destFile);

                } else {
                    //[BUGFIX]-Add-BEGIN by SCDTABLET.zhangku.guo@tcl.com,08/28/2015,1051103,
                    //fix translate issue with perso only
                    File oriFile = new File(fileList[index].getPath());
                    String copyFolder = String.format("%s/values-%s/", sourceName, Constant.getLocale(lang));
                    String copyToFolder = String.format("%s/values-%s/", baseName, Constant.getLocale(lang));
                    File copyFile = new File(copyFolder,oriFile.getName());
                    File copyToFile = new File(copyToFolder,oriFile.getName());
                    if (copyFile.exists()) {
                        try {
                            this.copyFile(copyFile, copyToFile);
                            //log.info("Copy " + copyFile + " To " + copyToFile);
                        }catch(Exception e){
                            log.info("Copy " + copyFile + " To " + copyToFile + "Failed.......");
                            e.printStackTrace();
                        }
                    }
                    //[BUGFIX]-Add-END by SCDTABLET.zhangku.guo@tcl.com
                }

                      }


                  }

            }

    // Arrays=======================================
    void exportDefaultArrayModule(StringsHelper strHelper, String moduleName)
            throws LogAbortException, PersoParserException {
        String lang = Constant.defaultLanguage;
        // check for default language
        int column = -1;
        for (int i = 0; i < langNames.length; i++) {
            if (langNames[i].equals(lang)) {
                column = Constant.persoPreceedColumn.length + i;
                break;
            }
        }
        if (column == -1) {
            throw new PersoParserException("default language not found: "
                    + lang);
        }
        // begin,added by cwang, 20101116
        // if the moduleName not exist in file
        // brandy_wimdata_ng/wprocedures/string_res.ini
        // perso will not build that module
        if (!Constant.isAppExistInAppRes(moduleName)
                && !moduleName.equalsIgnoreCase("frameworks")) { // revised by
                                                                    // zhiyu for
                                                                    // export
                                                                    // res in
                                                                    // frameworks/base/core/res/res
        // log.info("perso ignore app %s",moduleName);
            return;
        }
        // end

        String baseName = bundle.getSourcePath()
                + Constant.getModulePath(moduleName);// modified by Zhiyu
        String outputName = bundle.getOutputPath()
                + Constant.getModulePath(moduleName);// modified by Zhiyu
        if (!new File(baseName).isDirectory())
            throw new LogAbortException(baseName
                    + " directory not existed for module " + moduleName + ".");

        System.out.println("baseName=" + baseName);
        System.out.println("outputName=" + outputName);

        log.info("now processing module %s default language...", moduleName);
        Constant.ResType resType = Constant.getResType(moduleName);
        HashMap<String, StringItem> increment = (HashMap<String, StringItem>) strHelper.sh
                .clone();
        try {
            File oriFile = new File(baseName + "/values/arrays.xml");
            File newFile = new File(outputName + "/values/arrays_created.xml"); // modified
                                                                                // by
                                                                                // zhiyu

            if (!newFile.getParentFile().exists())
                newFile.getParentFile().mkdirs();

            log.info("check array file=" + oriFile.getAbsolutePath());

            if (!oriFile.exists()) {
                log.info(oriFile.getAbsolutePath()+" doesn't exist, so skip this module for arrays.xml");
                return; // return if source is not existed
            }

            xmldoc = documentBuilder.parse(oriFile);
            Element root = xmldoc.getDocumentElement();
            ow = new OutputStreamWriter(new FileOutputStream(newFile), "utf-8");
            putXmlHeader();
            putResourcesBeginTag();
            NodeList list = root.getChildNodes();
            for (int j = 0; j < list.getLength(); j++) {
                if (!(list.item(j) instanceof Element)) {

                    ow.write(copyNodeText(list.item(j)));
                    ow.flush();
                    continue;
                }
                Element currN = (Element) (list.item(j));
                try {
                    // if (currN.getAttribute("translatable") != null
                    // && currN.getAttribute("translatable").equals("false")) {
                    // ow.write(copyNodeText(currN));
                    // ow.flush();
                    // } else
                    if (currN.getNodeName().equals("string")) {
                        String name = currN.getAttribute("name");
                        String product = currN.getAttribute("product");
                        StringItem strItem = null;
                        if ("".equals(product) || null == product) {
                            strItem = strHelper.get("S:" + moduleName + ":" + name); // come
                                                                                     // from
                                                                                     // excel
                                                                                     // file
                        } else {
                            strItem = strHelper.get("S:" + moduleName + ":" + name + ":" + product); // come
                                                                                                     // from
                                                                                                     // excel
                                                                                                     // file
                        }
                        increment.remove(strItem.getFullName());
                        String currV = strItem.getAll().get(0)[column];

                        if (currV != null && currV.length() > 0) {
                            currN.setTextContent(String.format("###%s###",
                                    currN.getAttribute("name")));
                            ow.write(copyNodeText(currN).replace(
                                    String.format("###%s###",
                                            currN.getAttribute("name")), currV));
                        } else
                            ow.write(copyNodeText(currN));

                    } else if (currN.getNodeName().equals("plurals")) {
                        String name = currN.getAttribute("name");
                        String indexName = (currN.getNodeName().equals(
                                "string-array") ? "A:" : "P:")
                                + moduleName + ":" + name;
                        StringItem strItem = strHelper.get(indexName);
                        increment.remove(indexName);
                        NodeList li = selectNodes("./item", currN);
                        String outputStrO = copyNodeText(currN);
                        for (int k = 0; k < li.getLength(); k++) {
                            li.item(k).setTextContent(
                                    String.format("###item%d###", k));
                        }
                        String outputStr = copyNodeText(currN);
                        boolean oneItemFail = false;
                        for (int k = 0; k < li.getLength(); k++) {
                            String currV = null;
                            try {
                                //PR 97244 --zhiwei.liu begin
                                String quantity_xml=li.item(k).getAttributes().getNamedItem("quantity").getNodeValue();
                                String quantity_xls=strItem.getAll().get(k)[0].split(":")[3];
                                if (quantity_xml.equals(quantity_xls)){
                                    currV= strItem.getAll().get(k)[column];
                                }
                                else{
                                    for (int i=0;i<strItem.getAll().size();i++){
                                        if(quantity_xml.equals(strItem.getAll().get(i)[0].split(":")[3])){
                                            currV=strItem.getAll().get(i)[column];
                                            break;
                                        }
                                        else{
                                            continue;
                                        }
                                    }
                                }
                                //PR 97244 --zhiwei.liu end

                                if (currV != null && currV.length() > 0) {
                                    outputStr = outputStr.replace(
                                            String.format("###item%d###", k),
                                            currV);
                                } else {
                                    oneItemFail = true;
                                    break;
                                }
                            } catch (RuntimeException e) {
                                oneItemFail = true;
                            }
                        }
                        if (oneItemFail)
                            ow.write(outputStrO);
                        else
                            ow.write(outputStr);
                    } else if (currN.getNodeName().equals("string-array")) {

                        String name = currN.getAttribute("name");
                        String indexName = (currN.getNodeName().equals(
                                "string-array") ? "A:" : "P:")
                                + moduleName + ":" + name;
                        StringItem strItem = strHelper.get(indexName);
                        increment.remove(indexName);
                        NodeList li = selectNodes("./item", currN);
                        String outputStrO = copyNodeText(currN);
                        for (int k = 0; k < li.getLength(); k++) {
                            li.item(k).setTextContent(
                                    String.format("###item%d###", k));
                        }
                        String outputStr = copyNodeText(currN);
                        boolean oneItemFail = false;
                        try {
                            for (int k = 0; k < li.getLength(); k++) {
                                String currV = null;
                                String indexName1 = indexName + ":" + k;
                                for (int i = 0; i < li.getLength(); i++) {

                                    if (indexName1.equals(strItem.getAll().get(
                                            i)[0])) {

                                        try {
                                            currV = strItem.getAll().get(i)[column];

                                            if (currV != null
                                                    && currV.length() > 0) {
                                                outputStr = outputStr.replace(
                                                        String.format(
                                                                "###item%d###",
                                                                k), currV);
                                            } else {
                                                oneItemFail = true;
                                                break;
                                            }
                                        } catch (RuntimeException e) {
                                            oneItemFail = true;
                                        }
                                    }
                                }
                            }
                        } catch (RuntimeException e) {
                            oneItemFail = true;
                        }
                        if (oneItemFail)
                            ow.write(outputStrO);
                        else
                            ow.write(outputStr);

                    } else {
                        ow.write(copyNodeText(currN));
                    }
                } catch (RuntimeException e) {
                    ow.write(outputString(currN));// ignore process
                }
                ow.flush();
            }

            // Now we should warn the strings in increment, not merge them.
            if (!increment.isEmpty()) {
                List<StringItem> inclist = strHelper.getSortedAll(increment);
                for (int j = 0; j < inclist.size(); j++) {
                    StringItem item = inclist.get(j);
                    log.info("Warning: extra string " + item.getFullName()
                            + " not merged");
                }
            }
            putResourcesEndTag();
            ow.close();

            // modified by zhiyu
            if (bundle.getSourcePath().equalsIgnoreCase(bundle.getOutputPath())) {
                oriFile.delete();
                newFile.renameTo(oriFile.getAbsoluteFile());
            } else {
                oriFile = new File(outputName + "/values/arrays.xml");
                oriFile.delete();
                newFile.renameTo(oriFile.getAbsoluteFile());
            }

            log.info("%s Ok", moduleName);
            // added by zhiyu, add the extra language generation for PR106355
            ArrayList<String> extraL = Constant.getExtraLocale(lang);
            try {
                if (extraL != null && extraL.size() != 0) {
                    for (int i = 0; i < extraL.size(); i++) {
                        System.out.println("extra language is being processsed= "
                                + extraL.get(i));
                        File extraFile = new File(String.format(
                                "%s/values-%s/arrays.xml", outputName, extraL.get(i)));
                        if (!extraFile.getParentFile().exists())
                            extraFile.getParentFile().mkdirs();
                        System.out.println("COPY" + oriFile.getAbsolutePath()
                                + " TO=" + extraFile.getAbsolutePath());
                        this.copyFile(oriFile, extraFile);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        root = xmldoc.getDocumentElement();

    }

    private void buildArrayModule(StringsHelper strHelper, String lang,
            String module) throws IOException, PersoParserException,
            LogAbortException, SAXException {

        // check if language is available
        int column = -1;
        for (int i = 0; i < langNames.length; i++) {
            //added by zhixun.wang for apply english US translation to english AU and english NZ, bug[970973]
            if ((lang.equals("English_AU")&&langNames[i].equals("English_US"))||(lang.equals("English_NZ")&&langNames[i].equals("English_US"))) {
                column = Constant.persoPreceedColumn.length + i;
                break;
            }
            //end of bug[970973]
            if (langNames[i].equals(lang)) {
                column = Constant.persoPreceedColumn.length + i;
                break;
            }
        }
        if (column == -1) {
            throw new PersoParserException("language name error: " + lang);
        }

        // begin,added by cwang, 20101116
        // if the moduleName not exist in file
        // brandy_wimdata_ng/wprocedures/string_res.ini
        // perso will not build that module
        if (!Constant.isAppExistInAppRes(module)
                && !module.equalsIgnoreCase("frameworks")) { // revised by zhiyu
                                                                // for export
                                                                // res in
                                                                // frameworks/base/core/res/res
        // log.info("perso ignore app %s",module);
            return;
        }
        // end
        // check if has translated item. if no, do not create dir and file
        boolean hasLangItem = false;
        List<StringItem> ilist = strHelper.getSortedAll();
        for (int i = 0; i < ilist.size(); i++) {
            String value = ilist.get(i).getAll().get(0)[column];
            if (value != null && value.trim().length() > 0) {
                hasLangItem = true;
                break;
            }
        }
        if (!hasLangItem) {
            log.info(module + "/" + lang + " has no item, now skip.");
            return;
        }
        String baseName = bundle.getOutputPath()
                + Constant.getModulePath(module);
        String sourceName = bundle.getSourcePath()
                + Constant.getModulePath(module);// added by zhiyu

        File tf = new File(sourceName + "/values/arrays.xml");

        log.info("check array file=" + tf.getAbsolutePath());

        if (!tf.exists()) {
            log.info(tf.getAbsolutePath()+" doesn't exist, so skip this module for arrays.xml");
            return; // return if source is not existed
        }


        File newFile = new File(String.format("%s/values-%s/arrays_created.xml", baseName,
                Constant.getLocale(lang)));
        if (!newFile.getParentFile().exists())
            newFile.getParentFile().mkdirs();

        ow = new OutputStreamWriter(new FileOutputStream(newFile), "utf-8");

        putXmlHeader();
        putResourcesBeginTag();
        ow.write("\r\n");


        xmldoc = documentBuilder.parse(tf);// modified by zhiyu

        Element root = xmldoc.getDocumentElement();
        NodeList list = root.getChildNodes();
        for (int j = 0; j < list.getLength(); j++) {
            if (!(list.item(j) instanceof Element)) {
                continue;
            }

            Element currN = (Element) (list.item(j));
            if (currN.getAttribute("translatable") != null
                    && currN.getAttribute("translatable").equals("false")) {
                // skip it
                continue;
            } else if (currN.getNodeName().equals("string")) {
                String name = currN.getAttribute("name");
                String product = currN.getAttribute("product");
                StringItem strItem = null;
                if ("".equals(product) || null == product) {
                    strItem = strHelper.get("S:" + module + ":" + name);
                } else {
                    strItem = strHelper.get("S:" + module + ":" + name + ":" + product);
                }
                putString(name, strItem, product, column);
            } else if (currN.getNodeName().equals("string-array")) {
                String name = currN.getAttribute("name");
                String indexName = "A:" + module + ":" + name;
                StringItem strItem = strHelper.get(indexName);
                putStringArray(currN,name, indexName, strItem, column);
            } else if (currN.getNodeName().equals("plurals")) {
                String name = currN.getAttribute("name");
                StringItem strItem = strHelper.get("P:" + module + ":" + name);
                putPlurals(currN, name, strItem, column);
            }

        }
        putResourcesEndTag();
        ow.flush(); // added by zhiyu
        ow.close();

        File oriFile = new File(String.format("%s/values-%s/arrays.xml",
                baseName, Constant.getLocale(lang)));
        oriFile.delete(); // modified by zhiyu
        newFile.renameTo(oriFile.getAbsoluteFile());

        log.info("file created:" + oriFile.getAbsolutePath());

         //added by zhiyu, add the extra language generation for PR106355
        ArrayList<String> extraL=Constant.getExtraLocale(lang);
        try{
        if(extraL!=null&&extraL.size()!=0){
            for(int i=0;i<extraL.size();i++){
                System.out.println("extra language is being processsed= "+extraL.get(i));
                File extraFile = new File(String.format("%s/values-%s/arrays.xml", baseName,extraL.get(i)));
                if (!extraFile.getParentFile().exists())
                    extraFile.getParentFile().mkdirs();
                System.out.println("COPY"+oriFile.getAbsolutePath()+" TO="+extraFile.getAbsolutePath());
                    this.copyFile(oriFile, extraFile);
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }

    }
    //end: added by zhiyu for process the languange resource in arrays.xml 2011/3/11

    private void putXmlHeader() throws IOException {
        ow.write("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>" + "\r\n");
        //ow.write("\t<!--" + "\r\n");
        //ow.write("\t\tthis file is built by arct " + Version.asString() + "\r\n");
        //ow.write("\t-->" + "\r\n");
    }

    private void putResourcesBeginTag() throws IOException {
        ow.write("<resources xmlns:xliff=\"urn:oasis:names:tc:xliff:document:1.2\">");
    }

    private void putResourcesEndTag() throws IOException {
        ow.write("</resources>");

    }

    private void putNoTranslation(String name) throws IOException {
        ow.write(String.format("\t<!-- no translation found for %s -->\r\n\t<skip />\r\n", name));
    }

    private void putString(String name, StringItem item, String product, int column)
            throws IOException {
        if (item == null) {
            putNoTranslation(name);
            return;
        }
        String value = item.getAll().get(0)[column];
        if (value == null || value.trim().length() <= 0) {
            putNoTranslation(name);
            return;
        }
        value = escapeValue(value.trim());
        String desc = item.getAll().get(0)[Constant.infoColumn];
        if (desc != null && !desc.equals("N/A"))
            ow.write(String.format("\t<!--%s-->\r\n", desc));
        char firstVisible = value.trim().charAt(0);
        // if the first visible char of value is '?' or '@', ADT will make
        // mistake. so put a '-' before the problem chars to avoid it. This is a
        // temporary solution. When ADT resolve this problem, the prefix is not
        // needed any more.
        if ("".equals(product) || null == product) {
            ow.write(String.format("\t<string name=\"%s\">%s</string>\r\n", name,
                    (((firstVisible == '?' || firstVisible == '@') ? "-" : "") + value)));
        } else {
            ow.write(String.format("\t<string name=\"%s\" product=\"%s\">%s</string>\r\n", name,
                    product,
                    (((firstVisible == '?' || firstVisible == '@') ? "-" : "") + value)));

        }
    }

    private void putStringArray(Element curNode, String name,String indexName, StringItem item, int column) throws IOException {
        if (item == null) {
            putNoTranslation(name);
            return;
        }
        List<String[]> list = item.getAll();
        String desc, value;
        boolean translated = false;
        for (int i = 0; i < list.size(); i++) {
            value = list.get(i)[column];
            if (value.trim().length() > 0) {
                translated = true;
                break;
//[PLATFORM]-Mod-BEGIN by TCTNB.(Yufeng.Lan),2014/01/09,PR-586879, reason Modify arct tools, handle array completely
            }else if(value.equals("")){
                translated = true;
                break;
              }
//[PLATFORM]-Mod-END by TCTNB.((Yufeng.Lan)
        }
        if (!translated) {
            putNoTranslation(name);
            return;
        }
        ow.write(String.format("\t<string-array name=\"%s\">\r\n", name));

         int[] transItemIndex = new int[list.size()];
         int transItemIndexlength = list.size();
         for (int i = 0; i < list.size(); i++) {
                 String refName = list.get(i)[0];
                 transItemIndex[i] = Integer.parseInt(refName.substring(refName.lastIndexOf(":")+1, refName.length()));
           }
         Node m = curNode.getFirstChild();
         int itemIndex = 0;
         while (m != null) {
             if(m.getNodeName() != null && m.getNodeName().equals("item")) {
                 boolean isInxls = false;
                 int k = 0;
                 for(k=0 ; k < transItemIndexlength; k++){
                     if(itemIndex == transItemIndex[k]){
                         isInxls = true;
                         break;
                     }
                 }
                 itemIndex ++;
                 if(isInxls){
                     value = list.get(k)[column];
                    desc = list.get(k)[Constant.infoColumn];
                    value = escapeValue(value);
                    //revised by zhiyu, skip adding when value=empty for PR104525 --> PR107399 zhiwei.liu
                    if("".equals(value) || value.equalsIgnoreCase("\"\"")){
                    //[PLATFORM]-Mod by TCTNB.(Yufeng.Lan),2014/01/09,PR-586879, reason Modify arct tools, handle array completely
                        if("1".equals(list.get(k)[Constant.isMonoColumn]) || "".equals(list.get(k)[Constant.isMonoColumn])){
                           value=list.get(k)[Constant.defaultLanguageColumn];
                        }
                        else{
                           log.info("xls has not this item translation, so skip continue;");
                        continue;
                        }
                    }
                    if (desc != null && !desc.trim().isEmpty() && !desc.equals("N/A"))
                        ow.write(String.format("\t\t<!--%s-->\r\n", desc));

                 }else{
                     if (m.hasChildNodes()) {
                         NodeList nl = m.getChildNodes();
                         StringBuffer sb = new StringBuffer();
                         for (int i = 0; i < nl.getLength(); i++) {
                             sb.append(outputString(nl.item(i)));
                              }
                         value = sb.toString();
                         value = escapeValue(value);
                        } else
                           value = null;
                     ow.write(String.format("\t\t<!--inherit from default language-->\r\n"));
                 }

                    if (value != null && value.trim().length() > 0) {
                        char firstVisible = value.trim().charAt(0);
                // if the first visible char of value is '?' or '@', ADT will
                // make
                // mistake. so put a '-' before the problem chars to avoid it.
                // This
                // is a
                // temporary solution. When ADT resolve this problem, the prefix
                // is
                // no
                // need.
                    ow.write(String.format("\t\t<item>%s</item>\r\n",
                         (((firstVisible == '?' || firstVisible == '@') ? "-" : "") + value)));
                    } else {
                     ow.write("\t\t<item></item>\r\n");
                    }
             }//end if  if(isInxls)

             m = m.getNextSibling();
         }
        ow.write("\t</string-array>\r\n");

    }

    private void putPlurals(Node curNode, String name, StringItem item, int column) throws LogAbortException, IOException {
        if (item == null) {
            putNoTranslation(name);
            return;
        }
        List<String[]> list = item.getAll();
        String subname, desc, value;
        boolean translated = false;
        for (int i = 0; i < list.size(); i++) {
            value = list.get(i)[column];
            if (value.trim().length() > 0) {
                translated = true;
                break;
            }
        }
        if (!translated) {
            putNoTranslation(name);
            return;
        }

        ow.write(String.format("\t<plurals name=\"%s\">\r\n", name));


        String[] transItemIndex = new String[list.size()];
        int transItemIndexlength = list.size();
        for (int i = 0; i < list.size(); i++) {
               transItemIndex[i] = list.get(i)[0].substring(list.get(i)[0].lastIndexOf(":") + 1);
          }
         Node m = curNode.getFirstChild();
         int itemIndex = 0;
         while (m != null) {
               if(m.getNodeName() != null && m.getNodeName().equals("item")) {
                  subname = getStringAttributeValue(m, "quantity");
                if (subname == null)
                   throw new LogAbortException(m.toString()
                        + "has no quantity attribute value");

               boolean isInxls = false;
               int k = 0;
               for(k=0 ; k < transItemIndexlength; k++){
                   if(subname.equals(transItemIndex[k])){
                       isInxls = true;
                       break;
                   }
               }
               itemIndex ++;
               if(isInxls){
                    value = list.get(k)[column];
                    desc = list.get(k)[Constant.infoColumn];
                    value = escapeValue(value);
                    if (desc != null && !desc.trim().isEmpty() && !desc.equals("N/A"))
                        ow.write(String.format("\t\t<!--%s-->\r\n", desc));
               }else{
                   if (m.hasChildNodes()) {
                       NodeList nl = m.getChildNodes();
                       StringBuffer sb = new StringBuffer();
                       for (int i = 0; i < nl.getLength(); i++) {
                           sb.append(outputString(nl.item(i)));
                            }
                       value = sb.toString();
                       value = escapeValue(value);
                      } else
                         value = null;
                   ow.write(String.format("\t\t<!--inherit from default language-->\r\n"));

                } //end if(isInxls){
             if (value != null && value.trim().length() > 0) {
                 char firstVisible = value.trim().charAt(0);
                 // if the first visible char of value is '?' or '@', ADT will
                 // make
                 // mistake. so put a '-' before the problem chars to avoid it.
                 // This
                 // is a
                 // temporary solution. When ADT resolve this problem, the prefix
                 // is
                 // no
                 // need.
                 ow.write(String.format("\t\t<item quantity=\"%s\">%s</item>\r\n", subname,
                         (((firstVisible == '?' || firstVisible == '@') ? "-" : "") + value)));
             } else {
                 ow.write(String.format("\t\t<item quantity=\"%s\"></item>\r\n", subname));
             }
           }//end if item node
           m = m.getNextSibling();
       }//end if while
        ow.write("\t</plurals>\r\n");
    }

    private String escapeValue(String value) {
        // /*
        // * &lt; < &gt; > amp; & &apos; ' &quot; "
        // */
        // value = value.replaceAll("&", "&amp;").replaceAll("<",
        // "&lt;").replaceAll(">", "&gt;")
        // .replaceAll("'", "&apos;").replaceAll("\"", "&quot;");
        if(!value.startsWith("\""))
        {
          String values="\"" + value + "\"";
        return values;
        }
        else
        {
            return value;

        }

    }

    void outputNoTranslation(String noTranslationFileName) throws IOException {
        File f = new File(noTranslationFileName);
        FileWriter fw = new FileWriter(f);
        fw.write(String.format("Perso path: %s opened\r\n", bundle.getPersoPath()));
        fw.write(String.format("output modules count %d.\r\nmodule names are: %s\r\n", moduleTable
                .size(), moduleTable.toString()));
        fw.write(String.format("output languages:%s", bundle.isAllLanguage() ? "all"
                : Arrays.deepToString(bundle.getLanguages())));
        fw.write("\r\n\r\n");
        for (int i = 0; i < noTranslation.size(); i++) {
            fw.write(noTranslation.elementAt(i));
            fw.write("\r\n");
        }
        fw.write("-----------------           END        ------------------------------");
        fw.flush();
        fw.close();
    }

    // void
    public void build() throws IOException, PersoParserException, LogAbortException, SAXException,
            BiffException, IOException, SAXException {

        noTranslation = new Vector<String>();
        // readinPersoText();
    //modified by binbin.zhao
        try{
            readinPersoXls();
        }catch (PersoParserException e){
            log.error("Error!!!: %s", e.getMessage());
            throw e; //revised by zhiyu for PR106102
        }
    //end modified by binbin.zhao
        log.info("output modules count %d.\r\nmodule names are: %s", moduleTable.size(),
                moduleTable.toString());
        log.info("res output path: %s", bundle.getOutputPath());

        for (int i = 0; i < moduleTable.size(); i++) {
            if (bundle.isAllLanguage()) {
                try {
                    for (int j = 0; j < langNames.length; j++) {
                        if (langNames[j].equals(Constant.defaultLanguage)) {
                            if (Constant.getResType(moduleTable.get(i)).equals(Constant.ResType.core)) {
                                checkRepo();
                                log.info("check repo OK");
                            }
                            exportDefaultModule(moduleMap.get(moduleTable.get(i)), moduleTable.get(i));
                          //  exportDefaultArrayModule(moduleMap.get(moduleTable.get(i)), moduleTable.get(i));//added by zhiyu for processing language resource in arrays.xml, 2011/3/11
                   continue;
                        } //else { disabled by zhiyu for processing ENGLISH tag resource in buildModule PR103237
                            //CR68162-wei.wu start
                            //check language compatibility between string.xls and codes array.

                        //if country code not equals null or "", the process of build module would be performed
                        log.info(" %s module is being processsed", moduleTable.get(i));
                        log.info(" %s language is being processsed", langNames[j]);

                        //revised by zhiyu for avoid nullpoint error when langName="" for PR106102
                        if(langNames[j]!=null&&!langNames[j].equalsIgnoreCase("")&&Constant.getLocale(langNames[j])!=null&&!Constant.getLocale(langNames[j]).equalsIgnoreCase("")){ //added by zhiyu for processing ENGLISH tag resource in buildModule PR103237

                             if(Constant.getLanguage(langNames[j]) == null){
                                 log.info("%s language is not configured in arct,skip build", langNames[j]);
                                 continue;
                             }
                            //CR68162-wei.wu end
                            buildModule(moduleMap.get(moduleTable.get(i)), langNames[j], moduleTable
                                    .get(i));
                           // buildArrayModule(moduleMap.get(moduleTable.get(i)), langNames[j], moduleTable
                                   // .get(i));//added by zhiyu for processing language resource in arrays.xml, 2011/3/11
                        } //end of add PR103237

                    }
                    //added by zhixun.wang for apply english US translation to english AU and english NZ, bug[970973]
                    log.info(" %s module is being processsed", moduleTable.get(i));
                    log.info(" English_AU language is being processsed");
                    if(Constant.getLocale("English_AU")!=null&&!Constant.getLocale("English_AU").equalsIgnoreCase("")){
                        if(Constant.getLanguage("English_AU") == null){
                        log.info("English_AU language is not configured in arct,skip build");
                        } else {
                            buildModule(moduleMap.get(moduleTable.get(i)), "English_AU", moduleTable.get(i));
                        }
                    }

                    log.info(" %s module is being processsed", moduleTable.get(i));
                    log.info(" English_NZ language is being processsed");
                    if(Constant.getLocale("English_NZ")!=null&&!Constant.getLocale("English_NZ").equalsIgnoreCase("")){
                        if(Constant.getLanguage("English_NZ") == null){
                        log.info("English_NZ language is not configured in arct,skip build");
                        } else {
                            buildModule(moduleMap.get(moduleTable.get(i)), "English_NZ", moduleTable.get(i));
                        }
                    }
                    //end of bug[970973]
                } catch (LogAbortException e) {
                    if(e.getMessage().contains("directory not existed for module")){
                        log.error("Warning!!!: %s", e.getMessage()); //modified by binbin.zhao
                        continue;
                    }else{
                        throw e;
                    }
                }
            } else {
                for (int j = 0; j < bundle.getLanguages().length; j++) {
                    if (bundle.getLanguages()[j].equals(Constant.defaultLanguage)) {
                        if (Constant.getResType(moduleTable.get(i)).equals(Constant.ResType.core)) {
                            checkRepo();
                        }
                        exportDefaultModule(moduleMap.get(moduleTable.get(i)), moduleTable.get(i));
                        exportDefaultArrayModule(moduleMap.get(moduleTable.get(i)), moduleTable.get(i));//added by zhiyu for processing language resource in arrays.xml, 2011/3/11
                 continue;
                    } else {

                        //if country code not equals null or "", the process of build module would be performed
                        //revised by zhiyu for avoid nullpoint error when langName="" for PR104525
                        if(langNames[j]!=null&&!langNames[j].equalsIgnoreCase("")&&Constant.getLocale(langNames[j])!=null&&!Constant.getLocale(langNames[j]).equalsIgnoreCase("")){ //added by zhiyu for processing ENGLISH tag resource in buildModule PR103237
                        buildModule(moduleMap.get(moduleTable.get(i)), bundle.getLanguages()[j],
                                moduleTable.get(i));
                        buildArrayModule(moduleMap.get(moduleTable.get(i)), bundle.getLanguages()[j],
                                moduleTable.get(i));//added by zhiyu for processing language resource in arrays.xml, 2011/3/11
                        }//end of add PR103237
                    }

                }
                //added by zhixun.wang for apply english US translation to english AU and english NZ, bug[970973]
                if(Constant.getLocale("English_AU")!=null&&!Constant.getLocale("English_AU").equalsIgnoreCase("")){
                    buildModule(moduleMap.get(moduleTable.get(i)), "English_AU", moduleTable.get(i));
                    buildArrayModule(moduleMap.get(moduleTable.get(i)), "English_AU", moduleTable.get(i));
                }
                if(Constant.getLocale("English_NZ")!=null&&!Constant.getLocale("English_NZ").equalsIgnoreCase("")){
                    buildModule(moduleMap.get(moduleTable.get(i)), "English_NZ", moduleTable.get(i));
                    buildArrayModule(moduleMap.get(moduleTable.get(i)), "English_NZ", moduleTable.get(i));
                }
                //end of bug[970973]
            }
        }
        String noTranslationFileName = bundle.getSourcePath() + "/no_translation.txt"; //modified by zhiyu
        outputNoTranslation(noTranslationFileName);
        log.info("work done, no_translate.txt saved to " + bundle.getOutputPath());
    }

    //added by zhiyu, copy the extra language file from Main language file for PR106355
    public static void copyFile(File sourceFile, File targetFile) throws Exception {

         if (!sourceFile.isFile() && !targetFile.exists()) {
                         return;
                     }
                     FileInputStream in = new FileInputStream(sourceFile);
                     FileOutputStream out = new FileOutputStream(targetFile);
                     byte[] arr = new byte[1024];
                     int length = -1;
                     while ((length = in.read(arr)) != -1) {
                         out.write(arr, 0, length);
                     }
                     in.close();
                     out.flush();
    }
}
