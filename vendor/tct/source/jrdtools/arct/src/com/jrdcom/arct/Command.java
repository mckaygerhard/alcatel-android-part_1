/* ***************************************************************************/
/*                                                        Date : Dec 1, 2009 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct                                                          */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : Command.java                                                */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*01/12/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.ParserConfigurationException;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * Class to do the task and deal all the exception in the process.
 *
 * @author zwshi
 */
public class Command {
    Log log = Log.myLog;

    public void doWimdataToAndroidString(Bundle bundle) throws LogAbortException {
        log.info("arct(%s) now doPerso ....",Version.asString());
        PersoStringBuilder builder = null;
        try {
            builder = new PersoStringBuilder(bundle);
        } catch (UnsupportedEncodingException e1) {
            log.exception(e1, "error in doPerso", e1.getMessage());
            throw new LogAbortException(e1.getMessage());
        } catch (FileNotFoundException e1) {
            log.exception(e1, "error in doPerso", e1.getMessage());
            throw new LogAbortException(e1.getMessage());
        } catch (ParserConfigurationException e) {
            log.exception(e, "error in doPerso", e.getMessage());
            throw new LogAbortException(e.getMessage());
        }

        try {
            builder.build();
        } catch (PersoParserException e) {
            log.exception(e, "error in doPerso", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (LogAbortException e) {
            e.error(log);
            throw new LogAbortException(e.getMessage());
        } catch (SAXException e) {
            log.exception(e, "error in doPerso", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (BiffException e) {
            log.exception(e, "error in doPerso", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (IOException e) {
            log.exception(e, "error in doPerso", e.getMessage());
            throw new LogAbortException(e.getMessage());
        }
    }

    public void doAndroidStringToPerso(Bundle bundle) throws LogAbortException {
        log.info("arct(%s) now do export...", Version.asString());
        StringPersoExporter exporter = null;
        exporter = new StringPersoExporter(bundle);

        try {
            exporter.export();
        } catch (ParserConfigurationException e) {
            log.exception(e, "error in do export", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (SAXException e) {
            log.exception(e, "error in do export", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (IOException e) {
            log.exception(e, "error in do export", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (LogAbortException e) {
            log.exception(e, "error in do export", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (RowsExceededException e) {
            log.exception(e, "error in do export", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (WriteException e) {
            log.exception(e, "error in do export", e.getMessage());
            throw new LogAbortException(e.getMessage());
        }

    }

    public void doPlfToPropertyMakefile(Bundle bundle) throws LogAbortException{
        log.info("arct(%s) now do plf to property make file ...", Version.asString());
        PlfToPropertyMakefile builder = null;
        builder = new PlfToPropertyMakefile(bundle);
        try {
            builder.build();
        } catch (ParserConfigurationException e) {
            log.exception(e, "error in do PlfToPropertyMakefile", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (SAXException e) {
            log.exception(e, "error in do PlfToPropertyMakefile", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (IOException e) {
            log.exception(e, "error in do PlfToPropertyMakefile", e.getMessage());
            throw new LogAbortException(e.getMessage());
        }
    }

    // add by wteng
    public void doPlfToKeypadLayout(Bundle bundle) throws LogAbortException{
        log.info("arct(%s) now do plf to keypad layout ...", Version.asString());
        PlfToKeypadLayout builder = null;
        builder = new PlfToKeypadLayout(bundle);
        try {
            builder.build();
        } catch (ParserConfigurationException e) {
            log.exception(e, "error in do PlfToKeypadLayout", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (SAXException e) {
            log.exception(e, "error in do PlfToKeypadLayout", e.getMessage());
            throw new LogAbortException(e.getMessage());
        } catch (IOException e) {
            log.exception(e, "error in do PlfToKeypadLayout", e.getMessage());
            throw new LogAbortException(e.getMessage());
        }
    }
    //add end

}
