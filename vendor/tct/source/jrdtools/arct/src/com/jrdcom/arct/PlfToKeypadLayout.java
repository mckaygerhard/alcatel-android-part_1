package com.jrdcom.arct;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class PlfToKeypadLayout {

    Log log = Log.myLog;

    Bundle bundle = null;
    private FileWriter fwKL = null;

    public PlfToKeypadLayout(Bundle bundle) {
        // TODO Auto-generated constructor stub
        this.bundle = bundle;
    }
    private PlfToKeypadLayout() {
    }

    public void build()throws LogAbortException, ParserConfigurationException, SAXException,
    IOException  {
        // TODO Auto-generated method stub
         Document doc;
            DocumentBuilderFactory factory;
            DocumentBuilder docBuilder;
            Element root;
            FileInputStream in = null;

        String plfFileName = bundle.getPlfPath();
        String klFileName = bundle.getKLPath();

        File outKLFile = new File(klFileName + ".tmp");

        try {

            log.info("now processing %s...", plfFileName);

            in = new FileInputStream(plfFileName);
            factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);

            docBuilder = factory.newDocumentBuilder();
            doc = docBuilder.parse(in);
            root = doc.getDocumentElement();

            if (outKLFile.isFile())
                outKLFile.delete();
            fwKL = new FileWriter(outKLFile);

            Node m = root.getFirstChild();
            m = findNode(m, "TABLE_HISTORY");
            printHistory(m);
            m = findNode(m, "MOD");
            m = m.getFirstChild();
            m = findNode(m, "NAME");
            m = findNode(m, "NAME");
            printNodeMessage(m);
            m = findNode(m, "SDM_AREA");
            printSdmAreaInfo(m);
            fwKL.write("#just for brandy keypad layout\r\n");
            fwKL.write("#author:wteng<wei.teng@jrdcom.com>\r\n");
            fwKL.write("#date  :2011-1-5\r\n");
            m = m.getFirstChild();
            m = findNode(m, "TABLE_VAR");
            m = m.getFirstChild();
            while ((m = m.getNextSibling()) != null) {
                try {
                    m = findNode(m, "VAR");

                } catch (LogAbortException e) {
                    // end of found
                    log.info("-----------end-------------");
                    break;
                    }
                dealVar(m);

               }
            fwKL.close();
            File klfile = new File(klFileName);
            if (klfile.isFile())
                klfile.delete();
            outKLFile.renameTo(klfile);
            log.info("kl file created: " + klfile.getAbsolutePath());

            }finally {
                 if (in != null)
                     in.close();
                 if (fwKL != null)
                     fwKL.close();
                 if (outKLFile.getName().equals(klFileName + ".tmp"))
                     outKLFile.delete();
            }

     }

     void printHistory(Node m) throws DOMException, IOException {

            Node n = m.getFirstChild();
            do {
                try {
                    n = findNode(n, "HISTORY");
                    printComment(n, "HISTORY");
                    n = n.getNextSibling();
                } catch (LogAbortException e) {
                    // end to abort
                    fwKL.write("\r\n");
                    return;
                }

            } while (n != null);
        }

    Node findNode(Node node, String name) throws LogAbortException {

        do {
            if (node.getNodeName().equals(name))
                break;
        } while ((node = node.getNextSibling()) != null);
        if (node == null)
            throw new LogAbortException("Node " + name + " not found.");
        else
            return node;
    }

   void printNodeMessage(Node node) {
        log.info("node message:"
                + (node.getFirstChild() == null ? "" : node.getFirstChild().getTextContent()));
     }

    void printComment(Node node, String title) throws DOMException, IOException {
        fwKL.write("#" + title + ":"
                + (node.getFirstChild() == null ? "" : node.getFirstChild().getTextContent()));
        fwKL.write("\r\n");
     }
    void printSdmAreaInfo(Node m) throws LogAbortException, DOMException, IOException {
        Node n = m.getFirstChild();

        n = findNode(n, "DSA_TITLE");
        printComment(n, "DSA_TITLE");
        n = n.getNextSibling();

        n = findNode(n, "MACRO_NAME");
        printComment(n, "MACRO_NAME");
        n = n.getNextSibling();

        n = findNode(n, "C_NAME");
        printComment(n, "C_NAME");
        n = n.getNextSibling();

        n = findNode(n, "C_TYPE");
        printComment(n, "C_TYPE");
        n = n.getNextSibling();

        n = findNode(n, "IDA");
        printComment(n, "IDA");
        n = n.getNextSibling();

        n = findNode(n, "IS_IN_DSA");
        printComment(n, "IS_IN_DSA");
        n = n.getNextSibling();

        fwKL.write("\r\n\r\n");
    }

    void dealVar(Node m) throws LogAbortException, IOException {
        String cname;
        String desc;
        String metatype;
        String value;
        Node n = m.getFirstChild();
        n = findNode(n, "SIMPLE_VAR");
        n = n.getFirstChild();
        n = findNode(n, "SDMID");
          //  97741 20110307 wei.teng@jrdcom.com brandy three keys need modify for customization
        n = findNode(n, "C_TYPE");
        n = findNode(n, "C_NAME");
        cname = n.getFirstChild().getTextContent();
        n = findNode(n, "ARRAY");
        n = findNode(n, "METATYPE");
        metatype = n.getTextContent().trim().toLowerCase();
        if (!(metatype.startsWith("asciistring") || metatype.startsWith("byte")
                || metatype.startsWith("word") || metatype.startsWith("dword")))
            throw new LogAbortException("wrong metatype: " + metatype );

        n = findNode(n, "IS_CUSTO");
       // desc = n.getFirstChild() == null ? "" : n.getFirstChild().getTextContent();
        n = findNode(n, "VALUE");
       // value = n.getFirstChild() == null ? "" : n.getFirstChild().getTextContent().trim();
        value = n.getFirstChild().getTextContent();
       //101048  2011-3-22  wei.teng@jrdcom.com  The customization of touchkey for T-mobile
        if (value.startsWith("\""))
        value = value.substring(1, value.length()-1);
        fwKL.write(cname + " " + value);
        fwKL.write("\r\n");

        }
}
