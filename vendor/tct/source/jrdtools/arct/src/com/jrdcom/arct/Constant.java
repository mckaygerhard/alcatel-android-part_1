/* ***************************************************************************/
/*                                                        Date : Dec 1, 2009 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct                                                          */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : Constant.java                                               */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*01/12/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*25/12/2013|Yufeng.Lan|579185    |achieved the frameworks module            */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;

/**
 * Contains the business constants in it.
 *
 * @author zwshi
 */
public class Constant {
    final static public String[] persoPreceedColumn = {
            "RefName", "ModOP", "Info", "ZoneType", "IsMono", "IsUK", "IsGSM", "IsTradUpdatable",
    };

    final static public int infoColumn = 2;// column number of "Info"

    final static public int isMonoColumn = 4; // column number of "isMono"

    final static public int defaultLanguageColumn = 8;// column number of defaultLanguage:"English"

    final static public String defaultLanguage = "English";
    /**
     * add by zhiqing.xiong@jrdcom.com arct will export
     * Application/res/values/***.xml to excel. but will filter the files named
     * in filterResFileName. because these files do not need to translate
     */

    final static public String filterPatternFileName = 
            ".*dimen.*|" +
            ".*public.*|" +
            ".*attr.*|" +
            ".*donottranslate.*|" +
            ".*style.*|" +
            ".*color.*|" +
            ".*theme.*|" +
            ".*config.*|" +
            ".*bool.*|" +
            ".*default.*|" +
            ".*id.*|" +
            ".*wallpaper.*|" +
            ".*symbols.*|" +
            ".*isdm_.*|" +
            ".*integer.*";
    
    protected static final Pattern FILTER_FILES = Pattern.compile(filterPatternFileName);
    //modi by zhiwei CR 106384
    /*final static private String[][] codes = {
            {
                    "English", "0044", "en-rGB" //revised by zhiyu for PR103237
            }, {
                    "CHINE_OLD", "0886", "zh-rTW"
            }, {
                    "CHINE_NEW", "0086", "zh-rCN"
            }, {
                    "Thai", "0066", "th-rTH"
            }, {
                    "Spanish", "0034", "es-rES"
            }, {
                    "French", "0033", "fr-rFR" //revised by Zhiyu for PR100413 generating fr-rFR resource
            }, {
                    "German", "0049", "de-rDE"
            }, {
                    "Russian", "0007", "ru-rRU"
            }, {
                    "Italian", "0039", "it-rIT"
            }, {
                    "Arabic", "0966", "ar-rIL"
            }, {
                    "Portuguese-Portugal", "0351", "pt-rPT"
            }, {
                    "Portuguese", "0351", "pt-rPT" //revised by Zhiyu for PR100413 generating pt-rPT resource //revised by MI.Zhaoling 2011/03/18
            }, {
                    "Turkish", "0090", "tr-rTR"
            }, {
                    "Vietnamese", "0084", "vi-rVN"
            }, {
                    "Indonesian", "0062", "id-rID"
            }, {
                    "Malay", "0060", "ms-rMY"
            }, {
                    "Hindi", "0091", "hi"
            }, {
                    "Danish", "0045", "da"
            }, {
                    "Czech", "0420", "cs-rCZ"
            }, {
                    "Polish", "0048", "pl-rPL"
            }, {
                    "Hungarian", "0036", "hu-rHU"
            }, {
                    "Finnish", "0358", "fi"
            }, {
                    "Norwegian", "0047", "nn"
            }, {
                    "Slovak", "0421", "sk-rSK"
            }, {
                    "Dutch", "0031", "nl-rNL"
            }, {
                    "Swedish", "0046", "sv"
            }, {
                    "Croatian", "0385", "hr-rHR"
            }, {
                    "Romanian", "0040", "ro-rRO"
            }, {
                    "Slovenian", "0386", "sl"
            }, {
                    "Greek", "0030", "el-rGR"
            }, {
                    "Hebrew", "0972", "iw-rIL"
            }, {
                    "Bulgarian", "0359", "bg-rBG"
            }, {
                    "Ukranian", "0380", "uk"
            }, {
                    "Brazilian", "0055", "pt-rBR"
            }, {
                    "Catalan", "9903", "ca-rES"
            }, {
                    "Euskera", "9902", "eu-rES"
            }, {
                    "Galician", "9901", "gl-rES"
            }, {
                    "Farsi", "0098", "fa"
            }, {
                    "Albanian", "0355", "sq"
            }, {
                    "Serbian", "0381", "sr-rRS"
            }, {
                    "Macedonian", "0389", "mk-rMK"
            }, {
                    "Norwegian", "0047", "nb"
            }, {
                    "Korean", "0082", "ko-rKR"
            }, {
                    "Japanese", "0081", "ja-rJP"
            },{
                 //added by Song Wang for support language English_US
            "English_US","0001","en-rUS"
            },//
            {
                //added by binbin.zhao for support language French-CA
                "French_CA","0001","fr-rCA"
            },//

            };*/
    //[BUGFIX]-MOD by TCTNB.(Yufeng.Lan),2013/12/25,PR-579185,achieved the frameworks module to frameworkscore
    final static private String[][] coreResSetup = {
        {
                "frameworkscore", "/frameworks/base/core/res/res"
        },
    };

    final static private String[] appResSetup_pre = {
            "/packages/apps/FileManager/res", "/packages/apps/Calculator/res",
            "/packages/apps/TSCalibration/res", "/packages/apps/Email/res",
            "/packages/apps/Calendar/res", "/packages/apps/FM/res", "/packages/apps/Sound/res",
            "/packages/apps/GlobalSearch/res", "/packages/apps/Contacts/res",
            "/packages/apps/PackageInstaller/res", "/packages/apps/Mms/res",
            "/packages/apps/Bluetooth/res", "/packages/apps/IM/res",
            "/packages/apps/TaskManager/res", "/packages/apps/Camera/res",
            "/packages/apps/AlarmClock/res", "/packages/apps/Phone/res", "/packages/apps/Stk/res",
            "/packages/apps/GoogleSearch/res", "/packages/apps/NotePad/res",
            "/packages/apps/Settings/res", "/packages/apps/HTMLViewer/res",
            "/packages/apps/PDFViewer/res", "/packages/apps/Browser/res",
            "/packages/apps/Music/res", "/packages/apps/SyncMLClient/res",
            "/packages/apps/VoiceDialer/res", "/packages/apps/Launcher/res",
            "/packages/inputmethods/OpenWnn/res", "/packages/inputmethods/LatinIME/res",
            "/packages/inputmethods/PinyinIME/res",
            "/packages/providers/GoogleSubscribedFeedsProvider/res",
            "/packages/providers/CalendarProvider/res", "/packages/providers/MediaProvider/res",
            "/packages/providers/DrmProvider/res", "/packages/providers/TelephonyProvider/res",
            "/packages/providers/GoogleContactsProvider/res", "/packages/providers/ImProvider/res",
            "/packages/providers/DownloadProvider/res", "/packages/providers/ContactsProvider/res",
            "/packages/providers/ApplicationsProvider/res",
            "/frameworks/base/packages/SubscribedFeedsProvider/res",
            "/frameworks/base/packages/SettingsProvider/res",
            "/frameworks/base/packages/VpnServices/res", "/packages/apps/SoundTTT/res"
    };

    static private String[][] appResSetup = null;

    public enum ResType {
        core, app, none
    };

     static LinkedHashMap<String, String[]> language = new LinkedHashMap<String, String[]>();

    //added by zhiyu, add the extra language map for PR106355, this MAP only reserve the language extra
     static LinkedHashMap<String, ArrayList<String>> extraLanguage = new LinkedHashMap<String, ArrayList<String>>();



    private static HashMap<String, String> module = new HashMap<String, String>();
    static {

        appResSetup = new String[appResSetup_pre.length][2];
        for (int i = 0; i < appResSetup.length; i++) {
            String[] split = appResSetup_pre[i].split("/");
            appResSetup[i][0] = split[split.length - 2];
            appResSetup[i][1] = appResSetup_pre[i];
            module.put(appResSetup[i][0], appResSetup[i][1]);
        }

    }

    //added by Zhiyu, add the extra language map for PR106355, initialize map for extra language

        static void addExtraLanguage(HashMap<String, ArrayList<String>> mp, String key, String values ){

        if(mp!=null&&key!=null&&!key.equalsIgnoreCase("")){
            if(mp.get(key)!=null){
                boolean addFlag=true;
                ArrayList<String> tl=mp.get(key);

                for(String ts:tl){
                    if(ts.equalsIgnoreCase(values)){
                        addFlag=false;
                    }
                }
                if(addFlag){
                    tl.add(values);
                    mp.put(key,tl);
                    System.out.println(values+" is added into extra languages via tl "+key);
                    System.out.println(" tl size= "+tl.size());
                }
            }else{
                ArrayList<String> al=new ArrayList<String>();
                al.add(values);
                mp.put(key, al);
                System.out.println(values+" is added into extra languages via al "+key);
            }
        }
    }

    public static String getLocale(String lang) {
        return language.get(lang)[2];
    }

  //added by Zhiyu, return the extra language list for PR106355
    public static ArrayList<String> getExtraLocale(String lang) {
        System.out.println(" extra language = "+lang);

        if (extraLanguage.get(lang)!=null)
        System.out.println("list size= "+extraLanguage.get(lang).size());

        return extraLanguage.get(lang);
    }

    //CR68162-wei.wu start
    public static String getLanguage(String lang){
        return (language.get(lang) != null)?language.get(lang)[0]:null;
    }
    //CR68162-wei.wu end

    public static String getModulePath(String moduleName) {
        for (int i = 0; i < coreResSetup.length; i++) {
            if (coreResSetup[i][0].equals(moduleName))
                return coreResSetup[i][1];
        }
        return module.get(moduleName);

    }

    public static ResType getResType(String moduleName) {
        for (int i = 0; i < coreResSetup.length; i++) {
            if (coreResSetup[i][0].equals(moduleName))
                return ResType.core;
        }
        if (module.get(moduleName) != null)
            return ResType.app;
        else
            return ResType.none;
    }

    public static String[][] getAppResSetup() {
        return appResSetup;
    }

    //begin added by cwang 20101116
    public static boolean isAppExistInAppRes(String appName) {
        int len = appResSetup.length;
        int i = 0;
        for(;i< len; i++){
            if(appName.equalsIgnoreCase(appResSetup[i][0])){
                return true;
            }
        }
        return false;
    }
    //end
    public static String[][] getCoreResSetup() {
        return coreResSetup;
    }

    @SuppressWarnings("unchecked")
    public static String[][] getCodes() {
        //CR106384 - zhiwei.liu start
        String[][] codes=new String[Constant.language.size()][];
        int i=0;
        for(Iterator it=Constant.language.entrySet().iterator();it.hasNext();){
            Map.Entry e=(Map.Entry)it.next();
            String[] code=(String[])e.getValue();
            if(Constant.defaultLanguage.equals(code[0])){
                String[] temp=codes[0];
                codes[i]=temp;
                codes[0]=code;
            }
            else{
                codes[i]=code;
            }
            i++;
        }
        //CR106384 - zhiwei.liu end
        return codes;
    }

    public static void setAppResSetup(Vector<ResPath> paths) {
        Constant.appResSetup = new String[paths.size()][2];
        module.clear();
        for (int i = 0; i < appResSetup.length; i++) {

            appResSetup[i][0] = paths.get(i).moduleName;
            appResSetup[i][1] = paths.get(i).resPath;
            module.put(appResSetup[i][0], appResSetup[i][1]);
//            System.out.println(appResSetup[i][0]+"---------"+appResSetup[i][1]);
        }
    }

//    public static void setAppResSetup(Vector<String> paths) {
//        Constant.appResSetup = new String[paths.size()][2];
//        module.clear();
//        for (int i = 0; i < appResSetup.length; i++) {
//            String[] split = paths.get(i).split("/");
//            //modified by binbin.zhao, for android-froyo, the LatinIME's res is at packages/inputmethods/LatinIME/java/res,
//            //not packages/inputmethods/LatinIME/res, so the Module name should be special treated.
//            if (paths.get(i).equals("/packages/inputmethods/LatinIME/java/res"))
//                appResSetup[i][0] = split[split.length - 3];
//            else
//                appResSetup[i][0] = split[split.length - 2];
//            //end modified by binbin.zhao
//            appResSetup[i][1] = paths.get(i);
//            module.put(appResSetup[i][0], appResSetup[i][1]);
//        }
//    }
}
