/* ***************************************************************************/
/*                                                        Date : Dec 1, 2009 */
/*                      Android Resource Customization Tool                  */
/*              Copyright (c) 2009 JRD Communications, Inc.                  */
/* ***************************************************************************/
/*                                                                           */
/*    This material is company confidential, cannot be reproduced in any     */
/*    form without the written permission of JRD Communications, Inc.        */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*   Author :  Android team                                                  */
/*   Role :    arct                                                          */
/*   Reference documents :                                                   */
/*---------------------------------------------------------------------------*/
/* Comments :                                                                */
/*     file    : StringPersoExporter.java                                    */
/*     Labels  :                                                             */
/*===========================================================================*/
/* Modifications   (month/day/year)                                          */
/*---------------------------------------------------------------------------*/
/* date    | author    | modification                                        */
/*---------+-----------+-----------------------------------------------------*/
/*12/01/09 | Zwshi     | on creation                                         */
/*---------+-----------+-----------------------------------------------------*/
/*         |           |                                                     */
/*===========================================================================*/
/* Problems Report                                                           */
/*---------------------------------------------------------------------------*/
/* date    | author    | PR #     |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*---------|-----------|----------|------------------------------------------*/
/*         |           |          |                                          */
/*===========================================================================*/

package com.jrdcom.arct;

import com.Ostermiller.util.ExcelCSVPrinter;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Vector;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jxl.Workbook;
import jxl.format.CellFormat;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * StringPersoExporter supply methods to transform android strings to perso text
 * file. <h3>android resources</h3>
 * <p>
 * - StringPersoExporter find application module defined in Class Constant
 * resSetup. To each module, StringPersoExporter find all the strings.xml under
 * module folder recursively. So multi language resources will be processed. No
 * other xml will be processed by StringPersoExporter by now, include
 * arrays.xml. So string item needed to be exported by StringPersoExporter
 * should be moved into strings.xml.
 * <p>
 * - In some resources, element plural may be found. To avoid plural be omited,
 * we should move plural out of strings.xml.
 * <h3>on string.xml</h3>
 * <p>
 * - Element string, string-array, plurals, #comment will be processed.
 * attribute "name" to RefName, value or sub element to value of language,
 * #comment to Description. RefName is composed of three or four part divided by
 * ':'.
 *
 * <pre>
 * First part : type, S - string, A - string array, P - plurals
 *        Second part: app name
 *        Third part: string name
 *        Fourth part: only string array and plurals has fourth part. For string array is sequence.
 *        For plurals is item name.
 *        For some valid RefName examples:
 *          S:frameworks:low_memory , S:Browser:download_cancel_dlg_title
 *          A:Browser:pref_default_text_encoding_choices:0
 *          P:Mms:slide_duration:few
 * </pre>
 * <p>
 * - One or more whitespace character(\t\n\x0B\f\r) in sequence will be replaced
 * by sp(ascii0x20).
 * <p>
 * - String-array will be export to a number of item which named like
 * "Phone:tty_mode_entries:0" 0 will increase when item be found more.
 * <p>
 * - Comment close before string-array will be added to the comment close before
 * array item as item's description.
 * <p>
 * - Element other than string, string-array, #comment will be reported on
 * console when found.
 * <p>
 * - Language value with no according main language value will be reported on
 * console when found but still will be processed. This may cause unpredictable
 * result in runtime.
 *
 * @author zwshi
 */

public class StringPersoExporter extends XMLResourceHandler {
    private Bundle bundle;

    Log log = Log.myLog;

    private String refOutputPath = "/home/zwshi/tmp/abc_ref.txt";

    // private String repoBase = "/home/zwshi/opal_dint";
    private String repoBase = "/home/zwshi";

    private HashMap<String, ValueItem> valueMap;

    private Vector<String> valueTable;

    private String currAppName;

    private boolean[] translatedCodeColumn = new boolean[Constant.getCodes().length];

    private String[] normalColumns = new String[] {
            "RefName", "ModOP", "Info", "ZoneType", "IsMono", "IsUK", "IsGSM", "IsTradUpdatable"
    };

    class ValueItem {
        String RefName;

        String ModOP;

        String Info;

        String ZoneType;

        boolean IsMono;

        boolean IsUK;

        boolean isGSM;

        boolean IsTradUpdatable;

        String[] arrayValues;

        String[] values;
    }

    public StringPersoExporter(Bundle bundle) {
        this.bundle = bundle;
        if (bundle.getPersoPath() != null)
            refOutputPath = bundle.getPersoPath();
        if (bundle.getRepoPath() != null)
            repoBase = bundle.getRepoPath();
        log.setVerbose(true);
        for (int i = 0; i < translatedCodeColumn.length; i++)
            translatedCodeColumn[i] = false;

    }

    private StringPersoExporter() {
    }

    public void export() throws LogAbortException, ParserConfigurationException, SAXException,
            IOException, RowsExceededException, WriteException {

        log.info("repobase set to " + repoBase);
        if (bundle.isDedug())
            log.info("debug mode ok, will exporting ref_list.txt style text.");
        // init value table
        valueMap = new HashMap<String, ValueItem>();
        valueTable = new Vector<String>();
        String[][] coreResSetup = Constant.getCoreResSetup();
        for (int i = 0; i < coreResSetup.length; i++) {
            processBase(repoBase + coreResSetup[i][1], coreResSetup[i][0]);
        }
        String[][] resSetup = Constant.getAppResSetup();
        for (int i = 0; i < resSetup.length; i++) {
            processBase(repoBase + resSetup[i][1], resSetup[i][0]);
        }
        if (bundle.isDedug()) {
            dumpRef();
            // dumpRef();
        } else {
            dumpXls();
        }
        log.info("OK, ref file saved as " + refOutputPath);
    }

    private void processBase(String baseName, String moduleName) throws LogAbortException,
            ParserConfigurationException, SAXException, IOException {
        Document doc;
        DocumentBuilderFactory factory;
        DocumentBuilder docBuilder;
        Element root;
        FileInputStream in = null;
        FileInputStream inArray = null;    //added by zhiyu for supporting export arrays.xml
        FileInputStream    otherInArray = null;
        if (!new File(baseName).isDirectory())
            throw new LogAbortException(baseName + "directory not existed.");
        log.info("now processing %s...", moduleName);

        factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        docBuilder = factory.newDocumentBuilder();

        String[][] codes = Constant.getCodes();
        for (int i = 0; i < codes.length; i++) {
        //initial string and array exist flag add by zhqing.xiong@jrdcom.com
            File resFolder = new File(baseName + "/values"
                    + ((codes[i][2].length() > 0) ? "-" + codes[i][2] : ""));

             if(!resFolder.exists())  {
                 log.info("Resource path %s do not exist. return;",resFolder.getPath());
                 continue;
             }
             File[] resFileList = resFolder.listFiles();
             if(resFileList != null && resFileList.length > 0){
                  if (translatedCodeColumn[i] == false)
                      translatedCodeColumn[i] = true;

                   String path = null;
                   for(int index=0;index< resFileList.length;index++){
                       Matcher matcher = Constant.FILTER_FILES.matcher(resFileList[index].getName());
                        path = resFileList[index].getPath();
                        if(!matcher.matches()){
                             //log.info("process parse filename %s ", path);
                              in = new FileInputStream(path);
                           doc = docBuilder.parse(in);
                           root = doc.getDocumentElement();
                           currAppName = moduleName;
                           readNodes(root, i);
                           in.close();
                        }
                   }
              }
         }
        return;
    }

    // quote '"' is special character and should not be deleted.
    //quote is used to protect '?','\','@' proceeded strings.
    private String cleanQuote(String s) {
        if (s == null || s.length() < 1)
            return s;
        String tmps = null;
        if (s.length() > 1 && s.charAt(0) == '"' && s.charAt(s.length() - 1) == '"')
            tmps = s.substring(1, s.length() - 1);
        else if (s.charAt(0) == '"')
            tmps = s.substring(1);
        else
            return s;
        if (tmps.startsWith("\""))
            tmps = cleanQuote(tmps);
        return tmps;
    }

    private void readNodes(Element root, int currCode) throws LogAbortException {
        Node n = root.getFirstChild();
        String comment = null;
        String value = null;
        String refName = null;
        String[][] codes = Constant.getCodes();
        while (n != null) {
            if (n.getNodeName() != null && n.getNodeName().equals("#comment")) {
                if (comment != null)
                    comment = comment + " " + n.getNodeValue().replaceAll("(\\s)+", " ");
                else
                    comment = n.getNodeValue().replaceAll("(\\s)+", " ");
            } else if (n.getNodeName() != null && n.getNodeName().equals("skip")) {
                comment = null;
            } else if (n.getNodeName() != null && n.getNodeName().equals("string")) {

				String product = getStringAttributeValue(n, "product");
				if("".equals(product) || null == product){
                	refName = "S:" + currAppName + ":" + getStringAttributeValue(n, "name");
				}else{
                	refName = "S:" + currAppName + ":" + getStringAttributeValue(n, "name")+":"+product;
				}
              //  if ((comment !=null && comment.toLowerCase().indexOf("do not translate") >= 0) || "false".equals(getStringAttributeValue(n, "translatable"))) {
               //     log.error(refName + " do not translate, now omitted.");
                //    }
               // else{
                ValueItem v = valueMap.get(refName);
                if (v == null && currCode != 0) {
                    log.error(refName + " not in main language, now omitted.");
                } else {
                    if (v == null) {
                        v = new ValueItem();
                        v.RefName = refName;
                        v.ModOP = currAppName;
                        v.Info = "";
                        v.values = new String[codes.length];
                        valueMap.put(v.RefName, v);
                        valueTable.add(v.RefName);
                    }
                    if (n.hasChildNodes()) {
                        NodeList nl = n.getChildNodes();
                        StringBuffer sb = new StringBuffer();
                        for (int i = 0; i < nl.getLength(); i++) {
                            sb.append(outputString(nl.item(i)));
                        }
                        value = sb.toString();
                    } else
                        value = "";
                    if (currCode == 0 && comment != null) {
                        v.Info = comment;
                        if ((v.Info != null && v.Info.toLowerCase().indexOf("do not translate") >= 0)
                                 || "false".equals(getStringAttributeValue(n,
                                                         "translatable"))) {
                            log.info(refName + " do not translate, now omitted.");
                            v.IsMono = true;
                        }
                    }
                    v.values[currCode] = value;
                }
                //}
                comment = null;
               // }//[PLATFORM]-Add by TCTNB.(Yufeng.Lan),2014/01/02,PR-572112, reason Modify arct tools,add product attributes.
            } else if (n.getNodeName() != null && n.getNodeName().equals("string-array")) {
                refName = "A:" + currAppName + ":" + getStringAttributeValue(n, "name");
               // if ((comment !=null && comment.toLowerCase().indexOf("do not translate") >= 0) || "false".equals(getStringAttributeValue(n, "translatable"))) {
               //     log.error(refName + " do not translate, now omitted.");
                //    }
            //    else{
                int j = 0;
                Node m = n.getFirstChild();
                String subComment = null;
                while (m != null) {
                    if (m.getNodeName() != null && m.getNodeName().equals("#comment")) {
                        subComment = m.getNodeValue().replaceAll("(\\s)+", " ");
                    } else if (m.getNodeName() != null && m.getNodeName().equals("skip")) {
                        subComment = null;
                    } else if (m.getNodeName() != null && m.getNodeName().equals("item")) {
                        ValueItem v = valueMap.get(refName + ":" + j);
                        if (v == null && currCode != 0) {
                            log.error(refName + ":" + j + " not in main language, now omitted.");
                        } else {
                            if (v == null) {
                                v = new ValueItem();
                                v.RefName = refName + ":" + j;
                                v.ModOP = currAppName;
                                v.Info = "";
                                v.values = new String[codes.length];
                                valueMap.put(v.RefName, v);
                                valueTable.add(v.RefName);
                            }
                            if (m.hasChildNodes()) {
                                NodeList nl = m.getChildNodes();
                                StringBuffer sb = new StringBuffer();
                                for (int i = 0; i < nl.getLength(); i++) {
                                    sb.append(outputString(nl.item(i)));
                                }
                                value = sb.toString();
                            } else
                                value = "";
                            if (currCode == 0) {
                                v.Info = (comment != null ? comment : "")
                                        + (subComment != null ? subComment : "");
                                if ((v.Info != null
                                        && v.Info.toLowerCase().indexOf("do not translate") >= 0)                                             || "false".equals(getStringAttributeValue(m,
                                                         "translatable"))) {
                                    log.info(refName + " do not translate, now omitted.");
                                    v.IsMono = true;
                                }
                            }
                            v.values[currCode] = value;
                        }

                        subComment = null;
                        j++;
                    } else {
                        if (m.getNodeName() != null && m.getNodeName().charAt(0) != '#')
                            log.error("found error node name:" + refName + ":" + j + " "
                                    + m.getNodeName());
                    }

                    m = m.getNextSibling();
                }
               // }
                comment = null;

            } else if (n.getNodeName() != null && n.getNodeName().equals("plurals")) {
                refName = "P:" + currAppName + ":" + getStringAttributeValue(n, "name");
              //  if ((comment !=null && comment.toLowerCase().indexOf("do not translate") >= 0) || "false".equals(getStringAttributeValue(n, "translatable"))) {
                //    log.error(refName + " do not translate, now omitted.");
                //    }
              //  else{
                String subName = null;
                Node m = n.getFirstChild();
                String subComment = null;
                while (m != null) {
                    if (m.getNodeName() != null && m.getNodeName().equals("#comment")) {
                        subComment = m.getNodeValue().replaceAll("(\\s)+", " ");
                    } else if (m.getNodeName() != null && m.getNodeName().equals("skip")) {
                        subComment = null;
                    } else if (m.getNodeName() != null && m.getNodeName().equals("item")) {
                        subName = getStringAttributeValue(m, "quantity");
                        if (subName == null)
                            throw new LogAbortException(m.toString()
                                    + "has no quantity attribute value");
                        ValueItem v = valueMap.get(refName + ":" + subName);
                        if (v == null && currCode != 0) {
                            log.error(refName + ":" + subName
                                    + " not in main language, now omitted.");
                        } else {
                            if (v == null) {
                                v = new ValueItem();
                                v.RefName = refName + ":" + subName;
                                v.ModOP = currAppName;
                                v.Info = "";
                                v.values = new String[codes.length];
                                valueMap.put(v.RefName, v);
                                valueTable.add(v.RefName);
                            }
                            if (m.hasChildNodes()) {
                                NodeList nl = m.getChildNodes();
                                StringBuffer sb = new StringBuffer();
                                for (int i = 0; i < nl.getLength(); i++) {
                                    sb.append(outputString(nl.item(i)));
                                }
                                value = sb.toString();
                            } else
                                value = "";
                            if (currCode == 0) {
                                v.Info = (comment != null ? comment : "")
                                        + (subComment != null ? subComment : "");
                                if ((v.Info != null
                                        && v.Info.toLowerCase().indexOf("do not translate") >= 0)                                             || "false".equals(getStringAttributeValue(m,
                                                         "translatable"))) {
                                    log.info(refName + " do not translate, now omitted.");
                                    v.IsMono = true;
                                }
                            }
                            v.values[currCode] = value;
                        }

                        subComment = null;
                        subName = null;
                    } else {
                        if (m.getNodeName() != null && m.getNodeName().charAt(0) != '#')
                            log.error("found error node name:" + refName + ":" + subName + " "
                                    + m.getNodeName());
                    }
                    m = m.getNextSibling();
                }
             //   }
                comment = null;
            } else if (n.getNodeName() != null && n.getNodeName().charAt(0) != '#') {

                try {
                    log.error(currAppName + ":" + n.getNodeName() + ":"
                            + getStringAttributeValue(n, "name") + " omitted.");
                } catch (Exception e) {
                    log.exception(e, "");
                }

            }
            n = n.getNextSibling();
        }
    }

    private void dumpcsv() {
        try {
            String[][] codes = Constant.getCodes();
            File f = new File(refOutputPath);
            FileWriter fw = new FileWriter(f);
            ExcelCSVPrinter printer = new ExcelCSVPrinter(fw, '"', '\t');
            printer.write(new String[] {
                    "RefName", "ModOP", "Info", "ZoneType", "IsMono", "IsUK", "IsGSM",
                    "IsTradUpdatable"
            });
            for (int i = 0; i < codes.length; i++) {
                printer.write(codes[i][0]);
            }
            printer.writeln();
            printer.flush();
            for (int i = 0; i < valueTable.size(); i++) {
                ValueItem v = valueMap.get(valueTable.get(i));
                printer.write(v.RefName);
                printer.write(v.ModOP);// ModOP
                printer.write(v.Info);
                printer.write("zone_android_no_limit");// zone type
                printer.write(""); // isMono
                printer.write("");// isUK
                printer.write("");// isGsm
                printer.write("");// isTU
                for (int j = 0; j < codes.length; j++) {
                    if (v.values[j] != null) {
                        printer.write(v.values[j]);
                    } else {
                        printer.write("");
                    }
                }
                //
                printer.writeln();
                printer.flush();
            }
            printer.close();
            fw.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void dump() {
        try {
            String[][] codes = Constant.getCodes();
            File f = new File(refOutputPath);
            FileWriter fw = new FileWriter(f);
            fw.write("RefName\tModOP\tInfo\tZoneType\tIsMono\tIsUK\tIsGSM\tIsTradUpdatable");
            for (int i = 0; i < codes.length; i++) {
                fw.write("\t");
                fw.write(codes[i][0]);
            }
            fw.write("\r\n");
            fw.flush();
            for (int i = 0; i < valueTable.size(); i++) {
                ValueItem v = valueMap.get(valueTable.get(i));
                fw.write(v.RefName);
                fw.write("\t");
                fw.write(v.ModOP);
                fw.write("\t"); // ModOP
                fw.write(v.Info);
                fw.write("\t");
                fw.write("zone_android_no_limit\t");// zone type
                fw.write("\t"); // isMono
                fw.write("\t");// isUK
                fw.write("\t");// isGsm
                fw.write("\t");// isTU
                for (int j = 0; j < codes.length; j++) {
                    if (v.values[j] != null) {
                        fw.write(v.values[j]);
                    }
                    fw.write("\t");
                }
                //
                fw.write("\r\n");
                fw.flush();
            }
            fw.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void dumpXls() throws IOException, RowsExceededException, WriteException {
        String[][] codes = Constant.getCodes();
        File outputWorkbook = new File(refOutputPath);

        WritableWorkbook w = Workbook.createWorkbook(outputWorkbook);
        WritableSheet sheet = w.createSheet("MESSAGE", 0);
        WritableCell cell = null;
        CellFormat cf = null;
        Label l = null;
        WritableCellFeatures wcf = null;
        WritableFont tenpoint = new WritableFont(WritableFont.ARIAL, 10);
        int i = 0, line = 0;
        sheet.addCell(new Label(i++, line, "RefName"));
        sheet.addCell(new Label(i++, line, "ModOP"));
        sheet.addCell(new Label(i++, line, "Info"));
        sheet.addCell(new Label(i++, line, "ZoneType"));
        sheet.addCell(new Label(i++, line, "IsMono"));
        sheet.addCell(new Label(i++, line, "IsUK"));
        sheet.addCell(new Label(i++, line, "IsGSM"));
        sheet.addCell(new Label(i++, line, "IsTradUpdatable"));
        for (int j = 0; j < codes.length; j++) {
            if (translatedCodeColumn[j])
                sheet.addCell(new Label(i++, line, codes[j][0]));
        }

        for (int j = 0; j < valueTable.size(); j++) {
            ValueItem v = valueMap.get(valueTable.get(j));
            i = 0;
            line++;
            sheet.addCell(new Label(i++, line, v.RefName));
            sheet.addCell(new Label(i++, line, v.ModOP));
            sheet.addCell(new Label(i++, line, v.Info));
            sheet.addCell(new Label(i++, line, "zone_android_no_limit"));// zone
                                                                         // type
            sheet.addCell(new Label(i++, line, v.IsMono ? "1" : ""));
            ;// isMono
            i++;// isUK
            i++;// isGsm
            i++;// isTU
            for (int k = 0; k < codes.length; k++) {
                if (translatedCodeColumn[k]) {
                    if (v.values[k] != null) {
                        sheet.addCell(new Label(i, line, v.values[k]));
                    }
                    i++;
                }
            }
        }
        w.write();
        w.close();
    }

    private void dumpRefCSV() {
        try {
            String[][] codes = Constant.getCodes();
            File f = new File(refOutputPath);
            FileWriter fw = new FileWriter(f);
            ExcelCSVPrinter printer = new ExcelCSVPrinter(fw, '"', '\t');
            printer.write("Number of Language");
            printer.write("41");
            printer.writeln();

            printer.write(new String[] {
                    "Enum Value", "Module Name", "Max String Length", "Description"
            });
            for (int i = 0; i < codes.length; i++) {
                printer.write(codes[i][0]);
            }
            printer.writeln();
            printer.write(new String[] {
                    "DO NOT MODIFY", "DO NOT MODIFY", "DO NOT MODIFY", "DO NOT MODIFY"
            });
            for (int i = 0; i < codes.length; i++) {
                printer.write("#" + codes[i][1] + "#");
            }
            printer.writeln();
            printer.flush();
            for (int i = 0; i < valueTable.size(); i++) {
                ValueItem v = valueMap.get(valueTable.get(i));
                printer.write(v.RefName);// Enum Value
                printer.write(v.ModOP);// Module Name
                printer.write("N/A");// Max String Length
                printer.write(v.Info);// Description
                for (int j = 0; j < codes.length; j++) {
                    if (v.values[j] != null) {
                        printer.write(v.values[j]);
                    }
                }
                //
                printer.writeln();
                printer.flush();
            }
            printer.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void dumpRef() {
        try {
            String[][] codes = Constant.getCodes();
            File f = new File(refOutputPath);
            FileWriter fw = new FileWriter(f);
            fw.write("Number of Language\t41");
            fw.write("\r\n");

            fw.write("Enum Value\tModule Name\tMax String Length\tDescription");
            for (int i = 0; i < codes.length; i++) {
                fw.write("\t");
                fw.write(codes[i][0]);
            }
            fw.write("\r\n");
            fw.write("DO NOT MODIFY\tDO NOT MODIFY\tDO NOT MODIFY\tDO NOT MODIFY");
            for (int i = 0; i < codes.length; i++) {
                fw.write("\t");
                fw.write("#" + codes[i][1] + "#");
            }
            fw.write("\r\n");
            fw.flush();
            for (int i = 0; i < valueTable.size(); i++) {
                ValueItem v = valueMap.get(valueTable.get(i));
                fw.write(v.RefName);
                fw.write("\t");// Enum Value
                fw.write(v.ModOP);
                fw.write("\t"); // Module Name
                fw.write("N/A\t");// Max String Length
                fw.write(v.Info);
                fw.write("\t");// Description
                for (int j = 0; j < codes.length; j++) {
                    if (v.values[j] != null) {
                        fw.write(v.values[j]);
                    }
                    fw.write("\t");
                }
                //
                fw.write("\r\n");
                fw.flush();
            }
            fw.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void printChildTag(Element curElement, HashMap<String, String> hm) {
        Node n = curElement.getFirstChild();
        while (n != null) {
            if (n.getNodeName() != null && !hm.containsKey(n.getNodeName())) {
                hm.put(n.getNodeName(), null);
                System.out.println("node tag: " + n.getNodeName());
            } else if (n.getNodeName() != null && n.getNodeName().equals("string")) {
                if (n.hasChildNodes()) {
                    NodeList nl = n.getChildNodes();
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < nl.getLength(); i++) {
                        sb.append(outputString(nl.item(i)));
                    }
                    System.out.println("node name: " + sb.toString());
                } else
                    System.out.println("node name: " + outputString(n));
                // System.out.println(n.ge());
            }
            if (n.getNodeName().equals("#comment"))
                System.out.println("comment: " + n.getNodeValue());
            n = n.getNextSibling();

        }
    }

    public static void main(String[] args) {
        StringPersoExporter exporter = null;
        exporter = new StringPersoExporter(new Bundle());

        try {
            exporter.export();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (LogAbortException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (RowsExceededException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (WriteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
