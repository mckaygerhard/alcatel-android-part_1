#!/usr/bin/python
# -*- coding: utf-8 -*-
# Summary:  Read/Write settings modules
# Created:  2014-6-19
# Author :  Yanghuang.Ding
# Contact:  0574-2796-0685
# Email  :  yanghaung.ding@tcl.com

'''
Struct of config files:

[GlobalSettings]
gerrit_username=xxx@tcl.com
nmof_commit=1
last_commit_status=success/fail
last_commit_history_idx=1

[LastCommit]
bid=xxxx
comment=xxxx
product=xxxx
time=2013-1-1:11:00
...

[History1]
bid=xxxx
comment=xxxx
product=xxxx
time=2013-1-1:11:00
...

[History2]
...

[History3]
...


'''
import os
import json
import io


class Settings():
    """ class to read/write settings """
    SECTIONS = ['History', 'GlobalSettings', 'LastCommit']
    COMMIT_STATUS = ['success', 'fail']
    MAX_HISTORY_NUM = 100
    
    def __init__(self):
        if os.environ.has_key('HOME'):
            home = os.environ['HOME'] 
        else:
            home = '/tmp'
        assert (os.path.exists(home))
        self.fn = os.path.join(home, '.history_commit_patch-delivery-gui')
        
        self.config = {}
        self.readSettings()

    def readSettings(self):
        try:
            if os.path.exists(self.fn):
                with io.open(self.fn, 'r', encoding='utf-8') as f:
                    self.config = json.loads(f.read())
        except ValueError:
            self.config = {}
            return

    def writeSettings(self):
        if self.config:
            with io.open(self.fn, 'w', encoding='utf-8') as f:
	            f.write(unicode(json.dumps(self.config, ensure_ascii=False, sort_keys = True, indent = 4)))	
        else:
            raise ValueError

    def readItem(self, section, item):
        if self.config and self.config.has_key(section) and self.config[section].has_key(item):
            return self.config[section][item]
        else:
            return None            

    def writeItem(self, section, item, value):
        if not self.config.has_key(section):
            self.config[section] = {}            
        self.config[section][item] = value      

    def readLastFailedHistory(self):
        if not self.config or not (self.config.has_key('GlobalSettings') and self.config['GlobalSettings'].has_key('last_commit_status')):
            return None
        status = self.config['GlobalSettings']['last_commit_status']
        if status == 'success':
            return None

        return self.__readHistory('LastCommit')

    def writeLastFailedHistory(self, dat):
        if not self.config or type(dat) is not dict:
            return

        self.writeItem('GlobalSettings', 'last_commit_status', 'fail')
        for k,v in dat.items():
            self.writeItem('LastCommit', k, v)  

        self.writeSettings()
        return True
        

    def readHistory(self, bid):
        """ check all history to find out if any of history with bug id @bid """
        if not self.config:
            return None
        #print bid, type(bid)
        section = self.__findBugId(bid)
        if section:
            return self.__readHistory(section)
        return None

    def __readHistory(self, section):
        if not self.config or not self.config.has_key(section):
            return None
        
        return self.config[section]

    def __findBugId(self, bid):
        for section in self.config.keys():
            if section != 'LastCommit' and self.config[section].has_key('bugid'):
                bugid = self.config[section]['bugid']
                if str(bugid).strip() == bid:
                    return section
        return None

    def writeHistory(self, dat):
        """ Write commit history, if bug id already exist in histories, the previous commit history will be over-written """
        if not self.config or not dat or type(dat) is not dict:
            return None
        num = None

        # Only save max_hisotry_num histories
        if self.config.has_key('GlobalSettings') and self.config['GlobalSettings'].has_key('nmof_commit'):
            num = int(self.config['GlobalSettings']['nmof_commit'])
        
        if num:
            idx = num % self.MAX_HISTORY_NUM
        else:
            idx = 0
            num = 0
        section = 'History' + str(idx)

        # Check if the bugid already exists.
        existSection = self.__findBugId(dat['bugid'])
        if existSection:
            section = existSection
            idx = section[-1]

        for k,v in dat.items():
            self.writeItem(section, k, v)  
  
        self.writeItem('GlobalSettings', 'last_commit_history_idx', idx)
        self.writeItem('GlobalSettings', 'nmof_commit', num + 1)
        self.writeItem('GlobalSettings', 'last_commit_status', 'success')
    
        self.writeSettings()


    def readGerritUsername(self):
        return self.readItem('GlobalSettings', 'gerrit_username')

    def readLastStatus(self):
        return self.readItem('GlobalSettings', 'last_commit_status')

    def writeGerritUsername(self, nam):
        self.writeItem('GlobalSettings', 'gerrit_username', nam)
        self.writeSettings()

    def writeLastStatus(self, status):
        if status not in self.COMMIT_STATUS:
            raise ValueError
        self.writeItem('GlobalSettings', 'last_commit_status', status)
        self.writeSettings()    
    

if '__main__'==__name__:
    s = Settings()
    #print 'read settings', s.readSettings()
    s.writeGerritUsername('weimei.huang')
    s.writeLastFailedHistory(dict(bugid=1231, comment=u'asdfasdf自带原版的2.6  编不过; ', product='miata'))
    s.writeHistory(dict(bugid='22321233313', comment=u'sdf自带asdfa1231sdf', product='miata11'))
    s.writeHistory(dict(bugid='2232123', comment='asdfa1231sdf', product='miata11'))
    print s.readLastStatus()
    print s.readLastFailedHistory()

