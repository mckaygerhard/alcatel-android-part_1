# Copyright (C) 2016 Tcl Corporation Limited
################################################################################
# @file
# @brief system awake test tool.
################################################################################

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        tct_watch.c

#LOCAL_LDFLAGS := -static
LOCAL_MODULE := tct_watch
LOCAL_MODULE_TAGS := optional debug

include $(BUILD_EXECUTABLE)

