#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <linux/unistd.h>


#define SAVE_PATH	"/sdcard/tct_awake.log"
#define RPM_PATH	"/d/rpm_stats"
#define LPM_PATH	"/d/lpm_stats/suspend"

struct write_temp
{
	long rpm;
	long lpm;
};

struct read_temp 
{
	long sec;
	long rpm;
	long lpm;
};

void usage(char *cmd) {
    	fprintf(stderr, "Usage: %s [ -s start  ] [ -e end ]  [ -v ] [ -h ]\n"
			 "    NULL    awake sum total .\n"
          	         "    -s      start count .\n"
           	         "    -e      end count .\n"
			 "    -v      Show version.\n"
          	         "    -h      Display this help screen.\n",
   	 cmd);
}

void version(){
    fprintf(stderr,"version:  MSM8996.\n"
		   "          MSM8996 PRO.\n"
		   "          MSM8953.\n"
	    );
}

int get_opt(int argc , char **argv)
{
	static struct option const long_opts[]={ 
		{ "s"      , no_argument, NULL, 's' },
		{ "e"      , no_argument, NULL, 'e' },
		{ "help"   , no_argument, NULL, 'h' },
		{ "version", no_argument, NULL, 'v' },
    		{ NULL     , no_argument, NULL,  0  },
		}; 

	int c;
	while( (c=getopt_long(argc,argv,":sehv",long_opts, NULL) ) != -1 )
	{
		switch(c) {

		            case 's':
		                  return 1;

		            case 'e':
				  return 2;

		            case 'h':
				  usage(argv[0]);
		                  exit(0);

		            case 'v':
		  		  version();
		                  exit(0);

		            case ':':
				 printf("Missing parameter -%c\n",(char)optopt);	 
				 exit(0);
		     	    case '?':
		 		 printf("Erro : invalid option -- %c\nTry '%s -h ' for more information.\n",(char)optopt,argv[0]);
		 		 exit(0);
			    }
		}
	return 0;
}

long get_rpm()
{
	int fd;
 	char buff[512];
	char dump[32];
	char *str;
	long count;
	str=buff; 
	fd = open(RPM_PATH,O_RDONLY);
	if ( fd < 0 ){
		printf("Erro: read /d/rpm_stats\n");
		exit(0);	
		}

	if ( read(fd,buff,512-1) < 0 ) {
		close(fd);
		printf("Erro: read /d/rpm_stats\n");
		exit(0);		
		} 

	for(count = 0; count < 8; count++)
		{  
			for(;;)
			{
				str++;
				if( *str == '\n' )
						break;
			}
		}
	sscanf(str,"    count:%ld",&count);
	close(fd);

	return count;
}

long get_lpm()
{
	int fd;
 	char buff[512];
	char dump[32];
	char *str;
	long count;
	str=buff; 
	fd = open(LPM_PATH,O_RDONLY);
	if ( fd < 0 ){ 
		printf("Erro: read /d/lpm_stats/suspend\n");
		exit(0);	
		}

	if ( read(fd,buff,512-1) < 0 ) {
		close(fd);
		printf("Erro: read /d/lpm_stats/suspend\n");
		exit(0);		
		} 

	for(count = 0; count < 1; count++)
		{  
			for(;;)
			{
				str++;
				if( *str == '\n' )
						break;
			}
		}
	sscanf(str,"  success count:%ld",&count);
	close(fd);

	return count;
}

int temp_read( struct read_temp *temp )
{
	int fd;
	char buff[64];
	char *str=buff;
	long count;

	if ( access(SAVE_PATH,F_OK) < 0) 
				return -1;	 
	else
	{	
		fd = open(SAVE_PATH, O_RDONLY); 
		if (fd < 0) 
			return -2;
		if (read(fd,buff,64-1) < 0 )
			{
				close(fd);
				return -2;	 
			}

		sscanf(buff,"%ld %ld %ld",&temp->sec,&temp->rpm,&temp->lpm);

		if ( remove(SAVE_PATH) < 0 )
					return -3;  
	}
	return 0;
}

int temp_write(struct write_temp *temp)
		{

		FILE *fd;
		
		if ( ! access(SAVE_PATH,F_OK) ) 
				 remove(SAVE_PATH);

		fd = fopen(SAVE_PATH,"w+");
		if (fd < 0) 
			return -1;

		struct sysinfo time; 
		sysinfo(&time);
			
		fprintf(fd,"%ld %ld %ld",time.uptime,temp->rpm,temp->lpm);

		fclose(fd);

		return 0;
		}

int main(int argc , char **argv)
{

	int rc;
	struct sysinfo uptime; 

	rc = get_opt(argc , argv);

	if      (rc == 0 )
		{
			time_t now;
			struct tm *tm_now;
			time(&now);
			tm_now = localtime(&now);
			printf( "[time ] %d:%d\n",
				tm_now->tm_min,
				tm_now->tm_sec
				);
			printf( "[ cpu ] sleep count:  %ld\n"
				"[other] sleep count:  %ld\n",get_rpm(),get_lpm());
		}
	else if (rc == 1 )
		{
			struct write_temp w_temp;
			w_temp.rpm = get_rpm();
			w_temp.lpm = get_lpm();
			temp_write(&w_temp);	
			printf(" start awake count\n");
			    
		}
	else if (rc == 2 )
		{
			struct read_temp temp;
			if ( ! temp_read(&temp) )
			   	{

				sysinfo(&uptime);

				printf( "[uptime]\n"
					"	second: %ld S\n"
					"[ CPU ]\n"
					"	count : %ld\n"
					"	freq  : %.2lf/S\n"
					"[other]\n"
					"	count : %ld\n"
					"	freq  : %.2lf/S\n",
					/*uptime*/
					uptime.uptime-temp.sec,
					/*CPU*/
					get_rpm()-temp.rpm,
					(double)(get_rpm()-temp.rpm)/(uptime.uptime-temp.sec),
					/*other*/
					get_lpm()-temp.lpm,
					(double)(get_lpm()-temp.lpm)/(uptime.uptime-temp.sec)					
					);
				}
			else
				printf("Erro: read temp file\n");	
		}
return 0;
}
