
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := sar_daemon
LOCAL_MODULE_TAGS := optional

LOCAL_C_INCLUDES :=  system/core/include/cutils/ \
		    $(TARGET_OUT_HEADERS)/common/inc/ \
		    $(TARGET_OUT_HEADERS)/diag/include \
		    $(TARGET_OUT_HEADERS)/subsystem_control/inc \
		    $(TARGET_OUT_HEADERS)/qmi-framework/common/inc \
		    $(TARGET_OUT_HEADERS)/qmi-framework/inc \
		    $(TARGET_OUT_HEADERS)/qmi/inc \
		    $(TARGET_OUT_HEADERS)/qmi/platform \
		    $(TARGET_OUT_HEADERS)/qmi-framework/qcsi/inc \
		    $(TARGET_OUT_HEADERS)/qmi-framework/qcci/inc

LOCAL_SRC_FILES := rf_power.c \
                   rf_sar_daemon.c

LOCAL_SHARED_LIBRARIES := libc libcutils libutils libdiag libsubsystem_control libqmi_common_so libqmi_cci libqmi_encdec libqmiservices

LOCAL_CFLAGS += -Wall
include $(BUILD_EXECUTABLE)



