
#include "rf_power.h"

#include "qmi_cci_target.h"
#include "qmi_client.h"
#include "qmi_idl_lib.h"
#include "qmi_cci_common.h"
#include "specific_absorption_rate_v01.h"

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

#ifdef __FUNC__
#undef __FUNC__
#endif
#define  __FUNC__ "set_rf_power"

int set_rf_power(int rf_stat)
{
    int rc, service_connect;
    qmi_cci_os_signal_type /*notifier_os_params,*/ os_params;

    qmi_idl_service_object_type sar_service_obj;
    uint32_t num_services, num_entries=0;
    qmi_service_info *info_array=NULL;
    qmi_service_info info;
    qmi_client_type   rf_sar_client, notifier;

    sar_rf_get_compatibility_key_resp_msg_v01 qmi_get_key_resp;
    qmi_client_error_type qmi_client_error;
    sar_rf_set_state_req_msg_v01      qmi_request;
    sar_rf_set_state_resp_msg_v01     qmi_response;


    sar_service_obj = sar_get_service_object_v01();
    if(NULL == sar_service_obj)
    {
         ALOGE("%s set_rf_power sar service object is NULL",__FUNC__);
         return QMI_INTERNAL_ERR;
    }
    rc= qmi_client_notifier_init(sar_service_obj, &os_params, &notifier);
    ALOGD("%s, qmi client notifier init, result = %d notifier = %d",__FUNC__, rc, notifier);
    //waiting SAR service up, if not wait signal raise
    while(1)
    {
         rc = qmi_client_get_service_list(sar_service_obj, NULL, NULL, &num_services);
         ALOGD("%s, qmi get SAR service list trial, result = %d num_services = %d", __FUNC__,rc, num_services);
         if(rc == QMI_NO_ERR)
            break;
         //waiting SAR server to raise up
         QMI_CCI_OS_SIGNAL_WAIT(&os_params, 0);
    }

    num_entries = num_services;
    info_array= (qmi_service_info *)MALLOC(num_services*sizeof(qmi_service_info));
    if(!info_array)
        return QMI_INTERNAL_ERR;
    rc = qmi_client_get_service_list(sar_service_obj, info_array, &num_entries, &num_services);
    ALOGD("%s, qmi get SAR service list official, result = %d num_entries = %d num_services = %d", __FUNC__, rc, num_entries, num_services);
    if(rc != QMI_NO_ERR)
    {
        FREE(info_array);
        return QMI_SERVICE_ERR;
    }
    //may get many service instance, use the first;
    service_connect=0;
    memcpy(&info, &info_array[service_connect], sizeof(qmi_service_info));
    FREE(info_array);

    rc = qmi_client_init(&info, sar_service_obj, NULL, NULL, NULL, &rf_sar_client);
    ALOGD("%s, qmi client init, result = %d rf_sar_client = %d", __FUNC__, rc, &rf_sar_client);
    if(rc != QMI_NO_ERR)
    return QMI_SERVICE_ERR;

    //qcci initial finished, sent msg to server
    memset(&qmi_request, 0, sizeof(qmi_request));
    memset(&qmi_response, 0, sizeof(qmi_response));
    qmi_request.compatibility_key_valid = FALSE;
    qmi_request.compatibility_key = 0;
    /* */
    //get Modem compatibility_key
    memset(&qmi_get_key_resp, 0, sizeof(qmi_get_key_resp));
    qmi_client_error = qmi_client_send_msg_sync(    rf_sar_client,
                                                    QMI_SAR_GET_COMPATIBILITY_KEY_REQ_MSG_V01,
                                                    NULL,
                                                    0, /*QMI_RIL_ZERO,*/
                                                    (void*) &qmi_get_key_resp,
                                                    sizeof( qmi_get_key_resp ),
                                                    30000 /*QCRIL_QMI_SYNC_REQ_UNRESTRICTED_TIMEOUT*/ );
    if( TRUE == qmi_get_key_resp.compatibility_key_valid )
    {
        //replace Android transfered parameter
        qmi_request.compatibility_key_valid = TRUE;
        qmi_request.compatibility_key = qmi_get_key_resp.compatibility_key; //ril_param->compatibility_key
    } else {
        qmi_request.compatibility_key_valid = FALSE;
        qmi_request.compatibility_key = qmi_get_key_resp.compatibility_key; //ril_param->compatibility_key;
    }
    /*QCCI to call SAR QCSI service*/
    qmi_request.sar_rf_state = rf_stat;
    qmi_client_error = qmi_client_send_msg_sync(    rf_sar_client,
                                                    QMI_SAR_RF_SET_STATE_REQ_MSG_V01,
                                                    (void*) &qmi_request ,
                                                    sizeof( qmi_request ),
                                                    (void*) &qmi_response,
                                                    sizeof( qmi_response ),
                                                    30000 /*QCRIL_QMI_SYNC_REQ_UNRESTRICTED_TIMEOUT*/  );
    ALOGD("%s, qmi_client_error: %ld  qmi_response.resp =%d",__FUNC__, (uint32_t)qmi_client_error,qmi_response.resp);
    // release qcci handle
    rc = qmi_client_release(rf_sar_client);
    ALOGD("%s, qmi client release, result = %d rf_sar_client = %d", __FUNC__, rc, rf_sar_client);
    rc = qmi_client_release(notifier);
    ALOGD("%s, qmi client release trial, result = %d notifier = %d", __FUNC__, rc, notifier);

    return 0;
  }
