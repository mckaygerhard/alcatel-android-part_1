/* Copyright (C) 2016 Tcl Corporation Limited */

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
GENERAL DESCRIPTION:
This process is for backup nv param for tarball upgrade

Author: Binbin.zhu
Date:15/11/2016
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================*/

#ifndef LOG_TAG
#define LOG_TAG "NV_DIAG"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <cutils/log.h>

#include "log.h"
#include "diag_lsm.h"
#include "diag_lsmi.h"
#include "diag_lsm_dci.h"
#include "diag_shared_i.h"

#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include<sys/stat.h>
#include <dirent.h>
#include <fcntl.h>

#define DIAG_NV_ITEM_SIZE 128
#define SML_MAX_FILENAME_SIZE       50
#define SML_FILELIST_SIZE           44
#define DIAG_SIMLOCK_FILE_NAME_SIZE 120
#define SIMLOCK_ROOT_DIR  "/safe/sfs/uim/simlock/"

#define T_S_IRWXU (S_IRUSR|S_IWUSR|S_IXUSR)
#define T_S_IRWXG (S_IRGRP|S_IWGRP|S_IXGRP)
#define T_S_IRXO  (S_IROTH|S_IXOTH)

#define T_S_IRWXUG_IRXO (T_S_IRWXU|T_S_IRWXG|T_S_IRXO)


typedef struct file
{
	char name[SML_MAX_FILENAME_SIZE];
	uint8_t  item_data[DIAG_NV_ITEM_SIZE];
	uint16_t item_size; 
}File;

typedef struct nv_files
{
	uint16_t fileNum;
	File files[SML_FILELIST_SIZE];
}NV_FILES;

typedef struct{
	uint8_t cmdcode;
	uint8_t subsys_id;
	uint16_t subsys_code;
}diagpkt_subsys_header ;        /* Sub System header */

typedef struct
{
	diagpkt_subsys_header header;         /* Sub System header */
	uint16_t item;                             /* NV item */
	uint8_t  file[DIAG_SIMLOCK_FILE_NAME_SIZE];/*NV file name*/
	NV_FILES Files;
}DIAG_NV_type;

typedef DIAG_NV_type DIAG_NV_EXT_SFS_BACKUP_F_req_type;
typedef DIAG_NV_type DIAG_NV_EXT_SFS_BACKUP_F_rsp_type;

enum diag_nv_operation
{
    BACKUP_SFS = 0,
    RESTORE_SFS,
    MAX
}diag_nv_opr_type;


int g_clientID = -1;		//NV client ID

void process_response(unsigned char *ptr, int len, void *data_ptr);
void notify_handler(int signal, siginfo_t *info, void *unused);
void InitNVClient();
int getBackUpFile(NV_FILES *Files);

int main(int argc, char **argv)
{
	InitNVClient();

	//get backup file from medem
	NV_FILES *Files = (NV_FILES *)malloc(sizeof(NV_FILES));
	memset(Files, 0, sizeof(NV_FILES));
	int result = getBackUpFile(Files);
	int i = 0;
	if(0 == result)
	{
		//prepare environment for path
		mkdir("/tctpersist/smlfile/", T_S_IRWXUG_IRXO);
		DIAG_LOGE("getBackUpFile read file num %d\n", Files->fileNum);
		for(i= 0; i < Files->fileNum; i++)
		{
			char fileName[128] = {0,};
			strcat(fileName, "/tctpersist/smlfile/");
			strcat(fileName, Files->files[i].name);
			int fd = open(fileName, O_WRONLY | O_TRUNC | O_CREAT, T_S_IRWXUG_IRXO);
			if(fd > 0)
			{
				int j = 0;
				for(j = 0; j < Files->files[i].item_size; j++)
				{
					write(fd, (void*)&Files->files[i].item_data[j], 1);
				}

				close(fd);
			}
		}

		system("tar -cf nv.tar /tctpersist/smlfile");
		sync();
		int fdTar = open("nv.tar", O_RDONLY);
		int fdPartition = open("/dev/block/bootdevice/by-name/efsbak", O_RDWR);
		if(fdPartition <= 0)
		{
			DIAG_LOGE("Open efsbk error for %s\n", strerror(errno));
		}
		
		if(fdTar > 0 && fdPartition > 0)
		{
			DIAG_LOGE("Here start to write tar file\n");
			int n = 0;
			char buf[1024] = {0,};
			while(n =  read(fdTar, buf, sizeof(buf)) > 0)
			{
				DIAG_LOGE("Read %d bytes\n", n);
				int ret = write(fdPartition, buf, strlen(buf));
				DIAG_LOGE("Write %d bytes to efsbk\n", ret);
				if(ret <= 0)
				{
					DIAG_LOGE("Write error for %s\n", strerror(errno));
				}
				memset(buf, 0, sizeof(buf));
			}

			sync();
		}

		if(fdTar > 0)
		{
			close(fdTar);
		}

		if(fdPartition > 0)
		{
			close(fdPartition);
		}


		//system("rm -rf /tctpersist/smlfile");
		//system("rm -rf nv.tar");
	}
	
	free(Files);
	return 0;
}

void process_response(unsigned char *ptr, int len, void *data_ptr)
{
    int i = 0;
    DIAG_NV_type *rsp =(DIAG_NV_type*)ptr;
    DIAG_LOGE("  Command Code:	result num is %d\n", rsp->Files.fileNum);
    NV_FILES *Files = (NV_FILES*)data_ptr;
	Files->fileNum = rsp->Files.fileNum;
    for(i = 0; i < rsp->Files.fileNum; i++) {
        DIAG_LOGE("  Command Code:	result file %s\n", rsp->Files.files[i].name);
		strncpy((char*)Files->files[i].name, (const char *)rsp->Files.files[i].name, SML_MAX_FILENAME_SIZE - 1);
		memcpy((void*)Files->files[i].item_data, (void*)rsp->Files.files[i].item_data, DIAG_NV_ITEM_SIZE - 1);
        Files->files[i].item_size = rsp->Files.files[i].item_size;
    }

    /* Parsing Logic for the response - Based on the request in this sample */
    if (ptr[0] == 75) {
        DIAG_LOGE("  Command Code:	 Diag\n");
    }
    if (ptr[1] == 18){
        DIAG_LOGE("  Subsystem ID:	 Diag\n");
    }
    printf("\n");
}

/* Signal handler that handles the change in DCI channel */
void notify_handler(int signal, siginfo_t *info, void *unused)
{
   if (info) {
	   int err;
	   diag_dci_peripherals list = 0;

	   DIAG_LOGE("diag: In %s, signal %d received from kernel, data is: %x\n",
						   __func__, signal, info->si_int);

	   if (info->si_int & DIAG_STATUS_OPEN) {
		   if (info->si_int & DIAG_CON_MPSS) {
			   DIAG_LOGE("diag: DIAG_STATUS_OPEN on DIAG_CON_MPSS\n");
	   } else {
		   DIAG_LOGE("diag: DIAG_STATUS_OPEN on unknown peripheral\n");
	   }
	   } else if (info->si_int & DIAG_STATUS_CLOSED) {
		   if (info->si_int & DIAG_CON_MPSS) {
			   DIAG_LOGE("diag: DIAG_STATUS_CLOSED on DIAG_CON_MPSS\n");
		   } else {
			   DIAG_LOGE("diag: DIAG_STATUS_CLOSED on unknown peripheral\n");
		   }
	   }
	   err = diag_get_dci_support_list(&list);
	   if(err != DIAG_DCI_NO_ERROR) {
		   DIAG_LOGE("diag: could not get support list, err: %d\n", err);
	   }
	   /* This will print out all peripherals supporting DCI */
	   if (list & DIAG_CON_MPSS) {
		   DIAG_LOGE("diag: Modem supports DCI\n");
	   }
	   if (list & DIAG_CON_LPASS) {
		   DIAG_LOGE("diag: LPASS supports DCI\n");
	   }
	   if (list & DIAG_CON_WCNSS) {
		   DIAG_LOGE("diag: RIVA supports DCI\n");
	   }
	   if (list & DIAG_CON_APSS) {
		   DIAG_LOGE("diag: APSS supports DCI\n");
	   }
	   if (!list) {
		   DIAG_LOGE("diag: No current dci support\n");
	   }
   } else {
	   DIAG_LOGE("diag: In %s, signal %d received from kernel, but no info value, info: 0x%x\n",
						   __func__, signal, (uint64)info);
   }
}


void InitNVClient()
{
	int err;
    int signal_type = SIGCONT, channel = 0;
    boolean bInit_success = FALSE;
    diag_dci_peripherals list = DIAG_CON_MPSS | DIAG_CON_APSS;

    pthread_t connection_mgr;

    /* Signal handling to handle SSR */
    struct sigaction notify_action;
    sigemptyset(&notify_action.sa_mask);
    notify_action.sa_sigaction = notify_handler;
    /* Use SA_SIGINFO to denote we are expecting data with the signal */
    notify_action.sa_flags = SA_SIGINFO;
    sigaction(signal_type, &notify_action, NULL);


    /* Registering with Diag which gives the client a handle to the Diag driver */
    bInit_success = Diag_LSM_Init(NULL);
    if (!bInit_success) {
        DIAG_LOGE(" Couldn't register with Diag LSM, errno: %d\n", errno);
        return;
    }


    /* Registering with DCI - This assigns a client ID */
    err = diag_register_dci_client(&g_clientID, &list, channel, &signal_type);
    if (err != DIAG_DCI_NO_ERROR) {
        DIAG_LOGE(" Could not register with DCI, err: %d, errno: %d\n", err, errno);
        return;
    } else
        DIAG_LOGE(" Successfully registered with DCI, client ID = %d\n", g_clientID);

    /* Getting supported Peripherals list*/
    DIAG_LOGE(" DCI Status on Processors:\n");
    err = diag_get_dci_support_list(&list);
    if (err != DIAG_DCI_NO_ERROR) {
        printf(" Could not get support list, err: %d, errno: %d\n", err, errno);
        return;
    }
    DIAG_LOGE("   MPSS:\t ");
    DIAG_LOGE((list & DIAG_CON_MPSS) ? "UP\n" : "DOWN\n");
    DIAG_LOGE("   LPASS:\t ");
    DIAG_LOGE((list & DIAG_CON_LPASS) ? "UP\n" : "DOWN\n");
    DIAG_LOGE("   WCNSS:\t ");
    DIAG_LOGE((list & DIAG_CON_WCNSS) ? "UP\n" : "DOWN\n");
    DIAG_LOGE("   APSS:\t ");
    DIAG_LOGE((list & DIAG_CON_APSS) ? "UP\n" : "DOWN\n");

    printf("\n");
}

int getBackUpFile(NV_FILES *Files)
{
	int rc = 0;
	DIAG_NV_type *diag_nv_buf = (DIAG_NV_type*)malloc(sizeof(DIAG_NV_type));
	DIAG_NV_type *diag_nv_rsp_buf = (DIAG_NV_type*)malloc(sizeof(DIAG_NV_type));;

	memset(diag_nv_buf,0,sizeof(DIAG_NV_type));
	memset(diag_nv_rsp_buf, 0, sizeof(DIAG_NV_type));

	diag_nv_buf->header.cmdcode=75;
	diag_nv_buf->header.subsys_id = 103;
	diag_nv_buf->item = 0;
	memcpy(diag_nv_buf->file,SIMLOCK_ROOT_DIR,120);
	DIAG_LOGE("diag:NV file name %s\n",diag_nv_buf->file);
	DIAG_LOGE("diag:NV item %d\n",diag_nv_buf->item);
	DIAG_LOGE("diag:NV resp buf size id %d\n", sizeof(DIAG_NV_type));
	/*Sending Asynchronous Command*/
	diag_nv_buf->header.subsys_code = BACKUP_SFS;
	diag_nv_buf->Files.fileNum = 0;
	

	rc = diag_send_dci_async_req(g_clientID, (unsigned char*)diag_nv_buf, sizeof(DIAG_NV_EXT_SFS_BACKUP_F_req_type), (unsigned char*)diag_nv_rsp_buf, sizeof(DIAG_NV_EXT_SFS_BACKUP_F_req_type), &process_response, (void*)Files);
	
	if (rc != 1001){
		DIAG_LOGE(" Error sending SEND_REQUEST_ASYNC to peripheral %d\n", rc);
	}else{
		DIAG_LOGE("diag:diag_send_dci_async_req rc= %d\n", rc);
	}

	free(diag_nv_buf);
	free(diag_nv_rsp_buf);

	return (rc == 1001) ? 0 : 1;

}


