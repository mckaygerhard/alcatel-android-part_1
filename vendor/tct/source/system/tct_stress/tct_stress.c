/*
 * version :  1.1 
 * Author  :  liu-yang <liu-yang@tcl.com>
*/


#define _GNU_SOURCE     
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <getopt.h>
#include <math.h>
#include <sched.h> 
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h> 
#include <pthread.h> 
#include <sys/time.h>
#include <sys/syscall.h> 




#define  THREAD_MAX 8 



static void *workload_cpu(void *core)
{
	long num=(long)core;



	if(num > -1)
		{
			cpu_set_t mask;
			CPU_ZERO(&mask);   
	        	CPU_SET(num,&mask); 
			sched_setaffinity(0,sizeof(mask),&mask); 
		}	
	for(;;)	
		sqrt(3);  
}


int set_wakelock( char *type )
	{

		if (access("/sys/power/wake_lock", W_OK))	
			return  -1; 
		FILE *fd; 
		fd = fopen("/sys/power/wake_lock", "w");
		if (!fd) 
			return 	-2 ;

		fprintf(fd,"%s",type);
		fclose(fd);

		return 0;
	}

int un_wakelock( char *type )
	{

		if(access("/sys/power/wake_unlock", W_OK))
			 return -1; 
		FILE *fd; 
		fd = fopen("/sys/power/wake_unlock", "w");
		if (!fd) 
			 return -2;
		fprintf(fd,"%s",type);
		fclose(fd);
		return 0;
	}



void sig_exit()
	{
		un_wakelock("WAKE_LOCK_SUSPEND");
		exit(0);
	}




static void usage(char *cmd) {
    fprintf(stderr, "Usage: %s [ -c ] [ -n ] [ -d ] [ -t time ] [ -v version ] [ -h help ] \n"
		    "	-a all      work quantity equals cpu core sum.\n"
		    "	-c cpu      set CPU work quantity.\n"
		    "	-n number   assign CPU core [ -n 012 ] .\n"	
		    "	-w wakelock add wakelock .\n"
                    "	-t time     set uptime (second) default 60S .\n"
                    "	-v version  Display this version screen.\n"
                    "	-h help     Display this help screen.\n",
        cmd);
}




static void version(){
    fprintf(stderr,"version:  1.0 .\n" );
}


int 
main(int argc , char **argv)
{

	long long i;
	int wakelock=0;
	int rc;
	int delay=60; 
	int thread=0;
	int core[THREAD_MAX];
	    memset(core,-1,sizeof(core));


	static struct option const long_opts[]={  
		{ "all"     , no_argument, NULL, 'a' },
		{ "cpu"     , no_argument, NULL, 'c' }, 
		{ "number"  , no_argument, NULL, 'n' },
		{ "wakelock", no_argument, NULL, 'w' },  
		{ "time"    , no_argument, NULL, 't' }, 
		{ "help"    , no_argument, NULL, 'h' },      
		{ "version" , no_argument, NULL, 'v' },
    		{ NULL      , no_argument, NULL,  0  },           
		}; 

	int c;
	while( (c=getopt_long(argc,argv,":ac:n:wt:hv",long_opts, NULL) ) != -1 )
	{
	  switch(c) {

            case 'a':
			i=sysconf(_SC_NPROCESSORS_CONF);		  		
			while(i--)
				core[i]=-2;

                  break;

            case 'c':
		  if( (thread=atoi(optarg)) <= 0 || thread > THREAD_MAX )
				{
					fprintf(stderr,"erro :  -c \n");
					return 0;
				}
			while(thread--)
				core[thread]=-2;
                  break;

            case 'n':  
			for(i=0;i<=8;i++)
				{
				if( optarg[i] == 0 )
					{
						break;
					}
				if( !isdigit(optarg[i])  || (optarg[i]-48) >= sysconf(_SC_NPROCESSORS_CONF) )
					{
						fprintf(stderr,"erro :  -n \n");
						return 0;
					}
				core[optarg[i]-48]=0;
				}
		  break;

	    case 'w':    		  
		  wakelock=1;
		  break;

            case 't':     		  
		  if( (delay=atoi(optarg)) <= 0 )
				{
					fprintf(stderr,"erro :  -t \n");
					return 0;
				}
		  break;

            case 'h':
		  usage(argv[0]);
                  return 0;

            case 'v':
		  version();
                  return 0;

           case ':':
		 printf("Missing parameter -%c\n",(char)optopt);	 		 
		 return 0;

	   case '?':
		 printf("Erro : invalid option -- %c\nTry '%s -h ' for more information.\n",(char)optopt,argv[0]);
		 return 0;
			}	
	}



	if( wakelock == 1)
		{
			if(set_wakelock("WAKE_LOCK_SUSPEND") == -1)
				{
					printf("Erro: add wakelock Permission denied\n");
					exit(0);
				}

		}


	if(daemon(0,0) == -1) 
			{
				fprintf(stderr, "erro: daemon.\n");
				exit(0);
			}

	struct sigaction sig;
	sig.sa_handler = sig_exit;	
	sig.sa_flags = SA_RESETHAND; 
	sigemptyset(&sig.sa_mask);	
	sigaction(SIGALRM, &sig, NULL);


	struct itimerval clock;
    	clock.it_value.tv_sec = delay;	
    	clock.it_value.tv_usec = 0;	
    	clock.it_interval.tv_sec = delay;	
    	clock.it_interval.tv_usec = 0;	
   	setitimer(ITIMER_REAL, &clock, NULL); 

	pthread_t t[THREAD_MAX];
	for(i=0;i<8;i++)
		{
			if(core[i] != -1 )
			{

				if( core[i] == -2 )
					rc = pthread_create(&t[i],NULL,(void *) workload_cpu,(void *)-2);
				else
					rc = pthread_create(&t[i],NULL,(void *) workload_cpu,(void *)i);
				if(rc)
					exit(-1);
				wakelock = 0;
			}
		}

	if( wakelock == 1)
			un_wakelock("WAKE_LOCK_SUSPEND");
	
	pthread_exit(NULL);

return 0;
}




















