#include <stdio.h>
#include <string.h>
#include <cutils/properties.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <utils/Log.h>


#define EXIT_PROP_NAME "service.bootanim.exit"
#define OPEN_TCT_FLAG "init.svc.tct_as_log"
#define PERSIST_BOOT_STEP "persist.sys.bootstep"
#define PERSIST_FAIL_STEP "persist.sys.failstep"

#define STEPS_NUM 3

void startTctLog(char* name);
int getBootStep();
void saveFailStep(char* name, int step);



// 0 --------> power on----------------------->0s
// 1 --------> system_server------------------>10s
// 9 --------> bootanim.exit------------------>45s
int main(int argc, char *argv[])
{
    //log name
    char name[PROPERTY_VALUE_MAX] = "boot_time_check";

    int step[STEPS_NUM] = {0,10,45};
    int i;
    for(i = 1;i < STEPS_NUM - 1;i++)
    {
        sleep(step[i]-step[i-1]);
        if(getBootStep() < i)
        {
            saveFailStep(name,i);
            startTctLog(name);
            return 0;
        }
    }
    property_set(PERSIST_BOOT_STEP,"0");

    //check if bootanimation is finished
    sleep(step[STEPS_NUM-1]-step[STEPS_NUM-2]);
    char boot_finished[PROPERTY_VALUE_MAX];
    property_get(EXIT_PROP_NAME, boot_finished, "0");
    if (atoi(boot_finished) == 0)
    {
       saveFailStep(name,9);
       startTctLog(name);
    }
    return 0;
}

void startTctLog(char* name)
{
    char tct_log_flag[PROPERTY_VALUE_MAX];
    property_get(OPEN_TCT_FLAG, tct_log_flag, "stopped");
    if (strcmp(tct_log_flag,"stopped") == 0)
    {
           property_set("debug.as.name",name);
           property_set("ctl.start","tct_as_log");
    }
}

int getBootStep()
{
   char boot_step[PROPERTY_VALUE_MAX];
   property_get(PERSIST_BOOT_STEP, boot_step, "0");
   return atoi(boot_step);
}

void saveFailStep(char* name, int step)
{
   int i = 0;
   while(name[i] != '\0') i++;
   name[i] = step + 48;
   name[i+1] = '\0';
}
