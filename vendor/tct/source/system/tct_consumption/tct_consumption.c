#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <unistd.h>
#include <getopt.h>
#include <time.h>

#define CURRENT_PATH	"/sys/class/power_supply/battery/current_now" 	
#define VOLTAGE_PATH	"/sys/class/power_supply/battery/voltage_now" 
#define UPDATE_PATH	"/sys/class/power_supply/bms/update_now"      
#define USB_PRESENT	"/sys/class/power_supply/usb/present"	     
#define SAVE_PATH	"/sdcard/power_consumption_bin.log"	     

struct opt_info 
{
	int se;	/* start & end */
	int time;
	int delay;
};

/*display*/
struct power_info 
{
	int criteria_current;	/* 4V */
	int power;
};

long get_second()
{
	time_t t;
	t = time(NULL);
	return t;
}

static int get_current()
{
	int current = -1;
	FILE *fd; 
	fd=fopen(CURRENT_PATH, "r");
	if (!fd)
		 return -1;

	fscanf(fd,"%d",&current);

	fclose(fd);

	return current; 
}

static int get_voltage()	
{
	int voltage = -1;
	FILE *fd; 
	fd=fopen(VOLTAGE_PATH, "r");
	if (!fd)
		 return -1;
	fscanf(fd,"%d",&voltage);
	fclose(fd);
	return voltage ;
}

static int get_usb_state()
{
	int state;
	FILE *fd;
	fd=fopen(USB_PRESENT, "r");
	if (!fd)
		 return -1;
	fscanf(fd,"%d",&state);
	fclose(fd);

	if	( state == 0 )
			return 0;
	else if	( state == 1)
			return 1;
	else
			return 0;
}

static int set_updata_now()
{
	int rc;
	FILE *fd; 
	fd=fopen(UPDATE_PATH, "w");
	if (!fd) 
		return -1;
	rc = fputc('1',fd);
	if(rc < 0 ) 
		return -1;
	fclose(fd);

	return 0;
}

static void usage(char *cmd) {
    fprintf(stderr, "Usage: %s [ -s start ] [ -e end ] [ -t time ] [ -d delay ] [ -v version ] [ -h help ] \n"

		    "    -s start   start.\n"
		    "    -e end     end.\n"
		    "    -t time    test time , default: 60 second .\n"
		    "    -d delay   delay start , default: 5 second .\n"
                    "    -v version Display this version screen.\n"
                    "    -h help    Display this help screen.\n",
        cmd);
}

static void version(){
    fprintf(stderr,"version:  MSM8996.\n"
		   "          MSM8996 PRO.\n"
		   "          MSM8953.\n" 
	    );
}

static int data_write( struct opt_info *in )
{
	int p = in->time;
	int *r;
	r = (int*)malloc(sizeof(int)*p*2);
	if ( NULL == r)
			exit(1);
	long old;

	sleep(in->delay);

	old = get_second();

	while( p -- )
		{
			if( !get_usb_state() && ++old <= get_second()+1 )
				{
					set_updata_now();
					*r = get_voltage(); 	//printf("V: %d\n",*r);
					r++;
					*r = get_current(); 	//printf("I: %d\n",*r);
					r++;
					sleep(1);
				}
			else
				exit(0);
		}

	FILE *fd;
	fd=fopen(SAVE_PATH, "w+");
	if (!fd )
		exit(0);
	
	fprintf(fd,"%d\n",in->time);

	r-=in->time*2;
	p = in->time;

	while( p -- )
		{
			fprintf(fd,"%d ",*r);
			r++;
			fprintf(fd,"%d\n",*r);
			r++;
		}	

	fclose(fd);
	free(r-=in->time*2);
 	r=NULL;

	return 0;
}

static int data_read(struct power_info *p)
{
	int tmp;
	int row;
	FILE *fd;
	fd=fopen(SAVE_PATH, "r");
	if (!fd )
		exit(0);

	fscanf(fd,"%d",&row);
	
	int *r;
	r = (int*)malloc(sizeof(int)*row*2);
	if ( NULL == r)
			exit(1);
	tmp = row;
	while( tmp -- )
	{
		fscanf(fd,"%d ",r);	//printf("V: %d\n",*r);	
		r++;
		fscanf(fd,"%d\n",r);	//printf("I: %d\n",*r);
		r++;
	}
	
	double i_all = 0;
	double v_all = 0;

	r-=row*2;
	tmp = row;	
	while( tmp -- )
	{
		v_all += (double)*r/1000000;	//printf("V: %lf\n",(double)*r/1000000);
		r++;
		i_all += (double)*r/1000;	//printf("I: %lf\n",(double)*r/1000);
		r++;
	}

	p->criteria_current = (v_all/row)*(i_all/row)/4;
	p->power = (v_all/row)*(i_all/row);

	free(r-=row*2);
 	r=NULL;

	remove(SAVE_PATH);
	return 0;
}

static inline void display(struct power_info *d)
{
	printf("[ 4V Criteria current ] %d mA\n",d->criteria_current);
	printf("[ Power ] %d mW\n",d->power);
}



int
main(int argc , char **argv)
{

	struct opt_info input;
	input.time  = 60;
	input.delay = 5 ;

	static struct option const long_opts[]={ 
		{ "start"  , no_argument, NULL, 's' }, 
		{ "end"    , no_argument, NULL, 'e' }, 
		{ "time"   , no_argument, NULL, 't' },
		{ "delay"  , no_argument, NULL, 'd' },		
		{ "help"   , no_argument, NULL, 'h' },      
		{ "version", no_argument, NULL, 'v' },
   		{ NULL     , no_argument, NULL,  0  },           
		}; 

	int c;
	while( (c=getopt_long(argc,argv,":set:d:hv",long_opts, NULL) ) != -1 )
	{
	  switch(c) {

            case 's':
		  input.se = 1 ;
                  break;

            case 'e':
		  input.se = 2;
                  break;

            case 't':     		  
		  input.time = atoi(optarg);
		  if( input.time <= 0){
				printf("Missing parameter -t \n");
				return 0;
			}
			
		  break;

	   case 'd':     		  
		  input.delay = atoi(optarg);
		  if( input.delay <= 0){
				printf("Missing parameter -d \n");
				return 0;
			}
		  break;

            case 'h':
		  usage(argv[0]);
                  return 0;

            case 'v':
		  version();
                  return 0;

           case ':':
		 printf("Missing parameter -%c\n",(char)optopt);	 		 
		 return 0;

	   case '?':
		 printf("Erro : invalid option -- %c\nTry '%s -h ' for more information.\n",(char)optopt,argv[0]);
		 return 0;
			}	
	}

	if	( input.se == 1 )
		{

			if(daemon(0,0) == -1) 
			{
				fprintf(stderr, "erro: daemon.\n");
				exit(0);
			}
				//printf("time %d \ndelay %d\n",input.time,input.delay);
			data_write(&input);
		}

	else if ( input.se == 2 )
		{
			struct power_info power;
			data_read(&power);
			display(&power);
		}
return 0;
}
