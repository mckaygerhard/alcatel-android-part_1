# register description file for pmi8994 wled1

g;pmic_wled1

# this starts the wled_ctrl section
r;wled1_ctrl_perph_type;4
    f;type;0;7
        v;wled;0x17

r;wled1_ctrl_perph_subtype;5
    f;subtype;0;7
        v;wled_ctrl;0x10

r;wled1_ctrl_fault_status;8
    f;rfu1;3;3
    f;sc_fault;2;2
        v;sc_fault;1
        v;no_fault;0
    f;ovp_fault;1;1
        v;ovp_fault;1
        v;no_fault;0
    f;rfu0;0;0

r;wled1_ctrl_module_enable;0x46
    f;en_module;7;7;rw
        v;mod_enable;1;e
        v;mod_disable;0;d

r;wled1_ctrl_feedback_control;0x48
    f;feedback_control;0;2;rw;fbc
        v;auto74;7;a7
        v;auto_6;6;a6
        v;auto_5;5;a5
        v;out_led4;4;out_l4
        v;out_led3;3;out_l3
        v;out_led2;2;out_l2
        v;out_led1;1;out_l1
        v;auto_0;0;a0

r;wled1_ctrl_wled_ovp;0x4d
    f;wled_ovp;1;0;rw;vout
        v;17.8v_5.80v;3;17.8v
        v;19.4v_6.11v;2;19.4v
        v;29.5v_7.65v;1;29.5v
        v;31v_7.84v;0;31v

r;wled1_ctrl_wled_ilim;0x4e
    f;overwrite;7;7;rw
    f;wled_ilim;0;2;rw;ilim
        v;1980ma;7;1980m
        v;1700ma;6;1700m
        v;1420ma;5;1420m
        v;1150ma;4;1150m
        v;980ma;3;980m
        v;660ma;2;660m
        v;385ma;1;385m
        v;105ma;0;105m

r;wled1_ctrl_softstart_ramp_delay;0x53
    f;softstart_ramp_delay;0;2;rw;ss_r_dly

r;wled1_ctrl_en_hw_bl_redn;0x59
    f;en_hw_bl_redn;7;7;rw;hw_bl_redn
        v;bl_reduction_enable;1;e
        v;bl_reduction_disable;0;d

r;wled1_ctrl_en_psm;0x6a
    f;en_psm;7;7;rw;psm
        v;psm_enable;1;e
        v;psm_disable;0;d

# this starts the wled_sink section
r;wled1_sink_perph_type;0x104
    f;type;0;7
        v;wled;0x17

r;wled1_sink_perph_subtype;0x105
    f;subtype;0;7
        v;wled_ctrl;0x11

r;wled1_sink_current_sink_en;0x146
    f;en_current_sink4;7;7;rw;sink4
        v;led4_active;1;e
        v;led4_disable;0;d
    f;en_current_sink3;6;6;rw;sink3
        v;led3_active;1;e
        v;led3_disable;0;d
    f;en_current_sink2;5;5;rw;sink2
        v;led2_active;1;e
        v;led2_disable;0;d
    f;en_current_sink1;4;4;rw;sink1
        v;led1_active;1;e
        v;led1_disable;0;d

r;wled1_sink_iled_sync_bit;0x147
    f;sync_led4;3;3;rw;sync4
        v;sync4_enable;1;e
        v;sync4_disable;0;d
    f;sync_led3;2;2;rw;sync3
        v;sync3_enable;1;e
        v;sync3_disable;0;d
    f;sync_led2;1;1;rw;sync2
        v;sync2_enable;1;e
        v;sync2_disable;0;d
    f;sync_led1;0;0;rw;sync1
        v;sync1_enable;1;e
        v;sync1_disable;0;d

r;wled1_sink_hybrid_dimming_tresh;0x14b
    f;hybrid_dim_thresh;0;2;rw;h_dim_thr
        v;100%;7
        v;50%;6
        v;25%;5
        v;12.5%;4
        v;6.25%;3
        v;3.13%;2
        v;1.56%;1
        v;0.78%;0

r;wled1_sink_led1_modulator_en;0x150
    f;led1_en_modulator;7;7;rw;l1_enable
        v;modulator_enable;1;e
        v;modulator_disable;0;d
    f;led1_cs_gatedrv;0;0;rw;l1_gatedrv
        v;gatedrv_enable;1;e
        v;gatedrv_disable;0;d

r;wled1_sink_led1_idac_sync_delay;0x151
    f;led1_idac_sync_delay;0;2;rw;l1_s_dly
        v;1.14ms;7
        v;1.2ms;6
        v;1ms;5
        v;800us;4
        v;600us;3
        v;400us;2
        v;200us;1
        v;no_delay;0

r;wled1_sink_led1_full_scale_current;0x152
    f;led1_full_scale_current;0;3;rw;l1_curr
        v;30ma;12
        v;27.5ma;11
        v;25ma;10
        v;22.5ma;9
        v;20ma;8
        v;17.5ma;7
        v;15ma;6
        v;12.5ma;5
        v;10ma;4
        v;7.5ma;3
        v;5ma;2
        v;2.5ma;1
        v;0ma;0

r;wled1_sink_led1_modulator_src_sel;0x153
    f;led1_modulator_src_sel;0;0;rw;l1_mod_src
        v;external_modulator;1;ext
        v;internal_modulator;0;int

r;wled1_sink_led1_cabc_en;0x156
    f;led1_cabc_en;7;7;rw;l1_cabc
        v;cabc_enable;1;e
        v;cabc_disable;0;d

r;wled1_sink_led1_brightness_setting_lsb;0x157
    f;led1_brightness_setting_lsb;0;7;rw;l1_br_lsb

r;wled1_sink_led1_brightness_setting_msb;0x158
    f;led1_brightness_setting_msb;0;3;rw;l1_br_msb

r;wled1_sink_led2_modulator_en;0x160
    f;led2_en_modulator;7;7;rw;l2_enable
        v;modulator_enable;1;e
        v;modulator_disable;0;d
    f;led2_cs_gatedrv;0;0;rw;l2_gatedrv
        v;gatedrv_enable;1;e
        v;gatedrv_disable;0;d

r;wled1_sink_led2_idac_sync_delay;0x161
    f;led2_idac_sync_delay;0;2;rw;l2_s_dly
        v;1.14ms;7
        v;1.2ms;6
        v;1ms;5
        v;800us;4
        v;600us;3
        v;400us;2
        v;200us;1
        v;no_delay;0

r;wled1_sink_led2_full_scale_current;0x162
    f;led2_full_scale_current;0;3;rw;l2_curr
        v;30ma;12
        v;27.5ma;11
        v;25ma;10
        v;22.5ma;9
        v;20ma;8
        v;17.5ma;7
        v;15ma;6
        v;12.5ma;5
        v;10ma;4
        v;7.5ma;3
        v;5ma;2
        v;2.5ma;1
        v;0ma;0

r;wled1_sink_led2_modulator_src_sel;0x163
    f;led2_modulator_src_sel;0;0;rw;l2_mod_src
        v;external_modulator;1;ext
        v;internal_modulator;0;int

r;wled1_sink_led2_cabc_en;0x166
    f;led2_cabc_en;7;7;rw;l2_cabc
        v;cabc_enable;1;e
        v;cabc_disable;0;d

r;wled1_sink_led2_brightness_setting_lsb;0x167
    f;led2_brightness_setting_lsb;0;7;rw;l2_br_lsb

r;wled1_sink_led2_brightness_setting_msb;0x168
    f;led2_brightness_setting_msb;0;3;rw;l2_br_msb

r;wled1_sink_led3_modulator_en;0x170
    f;led3_en_modulator;7;7;rw;l3_enable
        v;modulator_enable;1;e
        v;modulator_disable;0;d
    f;led3_cs_gatedrv;0;0;rw;l3_gatedrv
        v;gatedrv_enable;1;e
        v;gatedrv_disable;0;d

r;wled1_sink_led3_idac_sync_delay;0x171
    f;led3_idac_sync_delay;0;2;rw;l3_s_dly
        v;1.14ms;7
        v;1.2ms;6
        v;1ms;5
        v;800us;4
        v;600us;3
        v;400us;2
        v;200us;1
        v;no_delay;0

r;wled1_sink_led3_full_scale_current;0x172
    f;led3_full_scale_current;0;3;rw;l3_curr
        v;30ma;12
        v;27.5ma;11
        v;25ma;10
        v;22.5ma;9
        v;20ma;8
        v;17.5ma;7
        v;15ma;6
        v;12.5ma;5
        v;10ma;4
        v;7.5ma;3
        v;5ma;2
        v;2.5ma;1
        v;0ma;0

r;wled1_sink_led3_modulator_src_sel;0x173
    f;led3_modulator_src_sel;0;0;rw;l3_mod_src
        v;external_modulator;1;ext
        v;internal_modulator;0;int

r;wled1_sink_led3_cabc_en;0x176
    f;led3_cabc_en;7;7;rw;l3_cabc
        v;cabc_enable;1;e
        v;cabc_disable;0;d

r;wled1_sink_led3_brightness_setting_lsb;0x177
    f;led3_brightness_setting_lsb;0;7;rw;l3_br_lsb

r;wled1_sink_led3_brightness_setting_msb;0x178
    f;led3_brightness_setting_msb;0;3;rw;l3_br_msb

r;wled1_sink_led4_modulator_en;0x180
    f;led4_en_modulator;7;7;rw;l4_enable
        v;modulator_enable;1;e
        v;modulator_disable;0;d
    f;led4_cs_gatedrv;0;0;rw;l4_gatedrv
        v;gatedrv_enable;1;e
        v;gatedrv_disable;0;d

r;wled1_sink_led4_idac_sync_delay;0x181
    f;led4_idac_sync_delay;0;2;rw;l4_s_dly
        v;1.14ms;7
        v;1.2ms;6
        v;1ms;5
        v;800us;4
        v;600us;3
        v;400us;2
        v;200us;1
        v;no_delay;0

r;wled1_sink_led4_full_scale_current;0x182
    f;led4_full_scale_current;0;3;rw;l4_curr
        v;30ma;12
        v;27.5ma;11
        v;25ma;10
        v;22.5ma;9
        v;20ma;8
        v;17.5ma;7
        v;15ma;6
        v;12.5ma;5
        v;10ma;4
        v;7.5ma;3
        v;5ma;2
        v;2.5ma;1
        v;0ma;0

r;wled1_sink_led4_modulator_src_sel;0x183
    f;led4_modulator_src_sel;0;0;rw;l4_mod_src
        v;external_modulator;1;ext
        v;internal_modulator;0;int

r;wled1_sink_led4_cabc_en;0x186
    f;led4_cabc_en;7;7;rw;l4_cabc
        v;cabc_enable;1;e
        v;cabc_disable;0;d

r;wled1_sink_led4_brightness_setting_lsb;0x187
    f;led4_brightness_setting_lsb;0;7;rw;l4_br_lsb

r;wled1_sink_led4_brightness_setting_msb;0x188
    f;led4_brightness_setting_msb;0;3;rw;l4_br_msb
