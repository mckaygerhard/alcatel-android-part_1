#register description file for PM8994-PM8996 GPIO

g;pmic_gpio

r;gpio_revision1;0
    f;dig_minor;0;7

r;gpio_revision2;1
    f;dig_major;0;7

r;gpio_revision3;2
    f;ana_minor;0;7

r;gpio_revision4;3
    f;ana_major;0;7

r;gpio_perph_type;4
    f;type;0;7
        v;gpio;0x10

r;gpio_perph_subtype;5
    f;subtype;0;7
        v;gpio_4ch;0x1
        v;gpioc_4ch;0x5
        v;gpio_8ch;0x9
        v;gpioc_8ch;0xd

r;gpio_status1;8
    f;gpio_ok;7;7;r;enabled
        v;disabled;0;d
        v;enabled;1;e
    f;gpio_val;0;0;r;input
        v;gpio_input_low;0;low
        v;gpio_input_high;1;high

r;gpio_int_rt_sts;0x10
    f;gpio_in_sts;0;0
        v;int_rt_status_low;0
        v;int_rt_status_high;1

r;gpio_int_set_type;0x11
    f;gpio_in_type;0;0;rw;it
        v;level;0;l
        v;edge;1;e

r;gpio_int_polarity_high;0x12
    f;gpio_in_high;0;0;rw;ih
        v;high_trigger_disabled;0;d
        v;high_trigger_enabled;1;e

r;gpio_int_polarity_low;0x13
    f;gpio_in_low;0;0;rw;il
        v;low_trigger_disabled;0;d
        v;low_trigger_enabled;1;e

r;gpio_int_latched_clr;0x14
    f;gpio_in_latched_clr;0;0;w

r;gpio_int_en_set;0x15
    f;gpio_in_en_set;0;0;rw;ies
        v;int_disabled;0;d
        v;int_enabled;1;e

r;gpio_int_en_clr;0x16
    f;gpio_in_en_clr;0;0;rw;iec
        v;int_disabled;0;d
        v;int_enabled;1;e

r;gpio_int_latched_sts;0x18
    f;gpio_in_latched_sts;0;0
        v;no_int_latched;0
        v;interrupt_latched;1

r;gpio_int_pending_sts;0x19
    f;gpio_in_pending_sts;0;0
        v;no_int_pending;0
        v;interrupt_pending;1

r;gpio_int_mid_sel;0x1a
    f;int_mid_sel;0;1;rw;ims
        v;mid0;0;m0
        v;mid1;1;m1
        v;mid2;2;m2
        v;mid3;3;m3

r;gpio_int_priority;0x1b
    f;int_priority;0;0;rw;ip
        v;sr;0
        v;a;1

r;gpio_mode_ctl;0x40
    f;mode;4;6;rw;m
        v;digital_input;0;i
        v;digital_output;1;o
        v;digital_in_and_out;2;io
    f;en_and_source_sel;0;3;rw;src
        v;low;0;l
        v;high;1;h
        v;paired;2;p
        v;not_paired;3;np
        v;function1;4;f1
        v;not_function1;5;nf1
        v;function2;6;f2
        v;not_function2;7;nf2
        v;dtest1;8;d1
        v;not_dtest1;9;nd1
        v;dtest2;10;d2
        v;not_dtest2;11;nd2
        v;dtest3;12;d3
        v;not_dtest3;13;nd3
        v;dtest4;14;d4
        v;not_dtest4;15;nd4

r;gpio_dig_vin_ctl;0x41
    f;voltage_sel;0;2;rw;v
        v;vin0;0;v0
        v;vin1;1;v1
        v;vin2;2;v2
        v;vin3;3;v3
        v;vin4;4;v4
        v;vin5;5;v5
        v;vin6;6;v6
        v;vin7;7;v7

r;gpio_dig_pull_ctl;0x42
    f;pullup_sel;0;2;rw;p
        v;pullup_30uA;0;pu30
        v;pullup_1.5uA;1;pu1p5
        v;pullup_31.5uA;2;pu31p5
        v;pullup_1.5_30uA_boost;3;pub
        v;pulldown_10uA;4;pd
        v;no_pull;5;np
        v;reserved;6

r;gpio_dig_in_ctl;0x43
    f;dtest1;0;0;rw;dt1
        v;dtest_disabled;0;d
        v;dtest_enabled;1;e
    f;dtest2;1;1;rw;dt2
        v;dtest_disabled;0;d
        v;dtest_enabled;1;e
    f;dtest3;2;2;rw;dt3
        v;dtest_disabled;0;d
        v;dtest_enabled;1;e
    f;dtest4;3;3;rw;dt4
        v;dtest_disabled;0;d
        v;dtest_enabled;1;e

r;gpio_dig_out_ctl;0x45
    f;output_type;4;5;rw;ot
        v;cmos;0;c
        v;open_high;1;h
        v;open_low;2;l
    f;output_drv_sel;0;1;rw;d
        v;reserved;0;r
        v;low;1;l
        v;med;2;m
        v;high;3;h

r;gpio_en_ctl;0x46
    f;perph_en;7;7;rw;e
        v;gpio_disabled;0;d
        v;gpio_enabled;1;e
