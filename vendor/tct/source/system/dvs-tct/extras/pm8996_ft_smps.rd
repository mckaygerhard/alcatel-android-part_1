# register description file for PM8994 SMPS

g;pmic_ft_smps

r;s_ctrl_perph_subtype;5
    f;subtype;7;0;r;type
        v;ft;9;ft

r;s_ctrl_status_1;8
    f;vreg_ready_flag;7;7
        v;vreg_ready_flag_true;1;true
        v;vreg_ready_flag_false;0;false
    f;vreg_fault_flag;6;6
        v;vreg_fault_flag_true;1;true
        v;vreg_fault_flag_false;0;false
    f;npm_flag;1;1
        v;npm_flag_true;1;true
        v;npm_flag_false;0;false
    f;stepper_done_flag;0;0
        v;stepper_done_flag_true;1;true
        v;stepper_done_flag_false;0;false

r;s_ctrl_status_2;9
    f;ils_flag;4;4
        v;ils_flag_true;1;true
        v;ils_flag_false;0;false
    f;uls_flag;3;3
        v;uls_flag_true;1;true
        v;uls_flag_false;0;false
    f;lls_flag;2;2
        v;lls_true;1;true
        v;lls_false;0;false
    f;gpl_hi_flag;1;1
        v;gpl_hi_flag_true;1;true
        v;gpl_hi_flag_false;0;false
    f;gpl_lo_flag;0;0
        v;gpl_lo_flag_true;1;true
        v;gpl_lo_flag_false;0;false

r;s_ctrl_voltage_ctl1;0x40
    f;mv_range;0;0;rw;range
        v;mv_range_true;1;med
        v;mv_range_false;0;low

r;s_ctrl_voltage_ctl2;0x41
    f;vset;7;0;rw;vs

r;s_ctrl_vset_valid;0x42
    f;vset_valid;7;0

r;s_ctrl_mode_ctl;0x45
    f;npm;7;7;rw
        v;npm_forced_if_enabl;1;pwm
        v;npm_not_forced;0;lpm
    f;auto_mode;6;6;rw;auto
        v;auto_mode_true;1;e
        v;auto_mode_false;0;d

r;s_ctrl_en_ctl;0x46
    f;perph_en;7;7;rw;fts
        v;fts_enable;1;e
        v;fts_disabl;0;d

r;s_ctrl_pd_ctl;0x48
    f;pd_en;7;7;rw;st_pd
        v;strong_pd_on_when_fts_is_disabl;1;e
        v;strong_pd_always_off;0;d
    f;weak_pd_en;6;6;rw;weak_pd
        v;weak_pd_enabl_in_pmic_off_state;1;e
        v;weak_pd_disabl_in_pmic_off_state;0;d
    f;weak_pd_pfm;5;5;rw;weak_pfm
        v;weak_pd_enabl_in_pfm;1;e
        v;weak_pd_disabl_in_pfm;0;d
    f;weak_pd_pwm;4;4;rw;weak_pwm
        v;weak_pd_enabl_in_pwm_and_hcpfm;1;e
        v;weak_pd_disabl_in_pwm_and_hcpfm;0;d
    f;leak_pd_en;3;3;rw;leak_pd
        v;leakage_pd_always_enabl;1;e
        v;leakage_pd_always_off;0;d

r;s_ctrl_freq_ctl;0x50
    f;freq_ctl;3;0;rw;freq
        v;fs_1m6mhz;11;1.6m
        v;fs_3m2mhz;5;3.2m
        v;fs_4m8mhz;3;4.8m
        v;fs_6m4mhz;2;6.4m

r;s_ctrl_phase_id;0x53
    f;phase_id;3;0;rw;phase
        v;phase_number_4;3;4
        v;phase_number_3;2;3
        v;phase_number_2;1;2
        v;phase_number_1;0;1

r;s_ctrl_phase_cnt_max;0x54
    f;phase_cnt_max;3;0;rw;phase_max

r;s_ctrl_ss_ctl;0x60
    f;ss_step;4;3;rw
        v;soft_start_vstep_8_lsb;3;8
        v;soft_start_vstep_4_lsb;2;4
        v;soft_start_vstep_2_lsb;1;2
        v;soft_start_vstep_1_lsb;0;1
    f;ss_delay;2;0;rw
        v;soft_start_time_step_53u3s;7;53.3us
        v;soft_start_time_step_26u7s;6;26.7us
        v;soft_start_time_step_13u3s;5;13.3us
        v;soft_start_time_step_6u7s;4;6.7us
        v;soft_start_time_step_3u3s;3;3.3us
        v;soft_start_time_step_1u67s;2;1.67us
        v;soft_start_time_step_833ns;1;833ns
        v;soft_start_time_step_417ns;0;417ns

r;s_ctrl_vs_ctl;0x61
    f;vs_en;7;7;rw
        v;vstepper_enabl;1;e
        v;vstepper_disabl;0;d
    f;vs_step;4;3;rw
        v;vstepper_step_size_8_lsb;3;8
        v;vstepper_step_size_4_lsb;2;4
        v;vstepper_step_size_2_lsb;1;2
        v;vstepper_step_size_1_lsb;0;1
    f;vs_delay;2;0;rw
        v;vstepper_time_step_53u3s;7;53.3us
        v;vstepper_time_step_26u7s;6;26.7us
        v;vstepper_time_step_13u3s;5;13.3us
        v;vstepper_time_step_6u7s;4;6.7us
        v;vstepper_time_step_3u3s;3;3.3us
        v;vstepper_time_step_1u67s;2;1.67us
        v;vstepper_time_step_833ns;1;833ns
        v;vstepper_time_step_417ns;0;417ns

r;s_ctrl_cfg_vreg_ocp;0x66
    f;ocp_lat_en;7;7;rw;ocp_lat
        v;ocp_latch_enable;1;e
        v;ocp_latch_disable;0;d
    f;ocp_retry_en;6;6;rw;ocp_retry
        v;ocp_retry_enable;1;e
        v;ocp_retry_disable;0;d
    f;ocp_clr;5;5;rw

r;s_ctrl_ul_ll_ctl;0x68
    f;ul_en;7;7;rw;u_stop
        v;ul_stop_enabl;1;e
        v;ul_stop_disabl;0;d
    f;ll_en;6;6;rw;l_stop
        v;ll_stop_enabl;1;e
        v;ll_stop_disabl;0;d

r;s_ctrl_vset_uls;0x69
    f;vset_uls;7;0;rw;uls_set

r;s_ctrl_uls_valid;0x6a
    f;uls_valid;7;0;r;uls_lim

r;s_ctrl_vset_lls;0x6b
    f;vset_lls;7;0;rw;lls_set

r;s_ctrl_lls_valid;0x6c
    f;lls_valid;7;0;r;lls_lim

r;s_ctrl_gpl_hi;0x6d
    f;vset_gpl_hi;7;0;rw;gpl_hi

r;s_ctrl_gpl_lo;0x6e
    f;vset_gpl_lo;7;0;rw;gpl_lo

r;s_ctrl_gang_ctl1;0xc0
    f;gang_leader_pid;7;0;rw;gl_pid

r;s_ctrl_gang_ctl2;0xc1
    f;gang_en;7;7;rw;gang
        v;gang_en_true;1;true
        v;gang_en_false;0;false


# this starts the S_PS section
# seems not able to read any registers from the PS section
# leave this here in case this ever changes (besides I already went through the trouble to put it in)
#r;s_ps_voltage_ctl1;0x140
#    f;ps_mv_range_ps;0;0;rw;ps_range
#        v;mv_range;1;med
#        v;lv_range;0;low
#
#r;s_ps_voltage_ctl2;0x141
#    f;ps_vset;0;7;rw
#
#r;s_ps_mode_ctl;0x145
#    f;ps_npm;7;7;rw
#        v;npm_forced_if_enabl;1;pwm
#        v;npm_not_forced;0;lpm
#    f;ps_auto_mode_ps;6;6;rw;ps_auto
#        v;auto_mode_true;1;true
#        v;suto_mode_false;0;false
#
#r;s_ps_freq_ctl;0x150
#    f;ps_freq_ctl;3;0;rw;ps_freq
#        v;fs_1m6mhz;11;1.6m
#        v;fs_3m2mhz;5;3.2m
#        v;fs_4m8mhz;3;4.8m
#        v;fs_6m4mhz;2;6.4m
#
#r;s_ps_phase_id;0x153
#    f;ps_phase_id;3;0;rw;ps_phase
#        v;phase_number_4;3;4
#        v;phase_number_3;2;3
#        v;phase_number_2;1;2
#        v;phase_number_1;0;1
#
#r;s_ps_phase_cnt_max;0x154
#    f;ps_phase_cnt_max;3;0;rw;ps_phase_max
#
#r;s_ps_gang_ctl1;0x1c0
#    f;ps_gang_leader_pid;7;0;rw;ps_gl_pid
#
#r;s_ps_gang_ctl2;0x1c1
#    f;ps_gang_en;7;7;rw;ps_gang
#        v;gang_en_true;1;true
#        v;gang_en_false;0;false


# this starts the S_FREQ section
r;s_freq_clk_enable;0x246
    f;en_clk_int;7;7;rw;clk
        v;force_en_enable;1;e
        v;force_en_disable;0;d
    f;follow_clk_sx_req;0;0;rw;fol_clk
        v;follow_clk_req_enabled;1;e
        v;follow_clk_req_disabled;0;d

r;s_freq_clk_div;0x250
    f;clk_div;3;0;rw;frq
        v;freq_1m2hz;15;1.2m
        v;freq_1m3hz;14;1.3m
        v;freq_1m4hz;13;1.4m
        v;freq_1m5hz;12;1.5m
        v;freq_1m6hz;11;1.6m
        v;freq_1m7hz;10;1.7m
        v;freq_1m9hz;9;1.9m
        v;freq_2m1hz;8;2.1m
        v;freq_2m4hz;7;2.4m
        v;freq_2m7hz;6;2.7m
        v;freq_3m2hz;5;3.2m
        v;freq_3m8hz;4;3.8m
        v;freq_4m8hz;3;4.8m
        v;freq_6m4hz;2;6.4m
        v;freq_9m6hz;1;9.6m
        v;freq_9m6hz_0;0;ns-9.6m

r;s_freq_clk_phase;0x251
    f;clk_phase;3;0;rw;dly

r;s_freq_gang_ctl1;0x2c0
    f;frq_gang_leader_pid;7;0;rw;f_gl_pid

r;s_freq_gang_ctl2;0x2c1
    f;frq_gang_en;7;7;rw;f_gang_en
        v;ganging_enabled;1;e
        v;ganging_disabled;0;d