# register description file for PMI8994 haptics

g;pmic_haptics

r;haptics_perph_subtype;5
    f;subtype;7;0
        v;haptics;9;hap

r;haptics_status_1;0xa
    f;en_sts;7;7
        v;enabled;1;e
        v;disabled;0;d
    f;auto_res_error;4;4
        v;auto_res_oor;1
        v;auto_res_ok;0
    f;sc_flag;3;3
        v;sc_det;1
        v;no_sc;0
    f;oc_flag;2;2
    f;busy;1;1
        v;busy;1;b
        v;idle;0;i
    f;play_sts;0;0

r;haptics_lra_auto_res_lo;0xb
    f;period_lo;7;0;r

r;haptics_lra_auto_res_hi;0xc
    f;period_hi;7;4;r

r;haptics_en_ctl1;0x46
    f;haptics_en;7;7;rw;hap_en
        v;haptics_en;1;e
        v;haptics_dis;0;d

r;haptics_cfg1;0x4c
    f;actuator_type;0;0;rw;type
        v;erm;1
        v;lra;0

r;haptics_sel;0x4e
    f;wf_source;5;4;rw;source
        v;ext_pwm;3;ext
        v;audio;2;aud
        v;buffer;1;buf
        v;vmax;0;vmax
    f;trigger;0;0;rw;t
        v;line_in;1;line
        v;haptics_play;0;play

# this register is confusing
r;haptics_vmax_cfg;0x51
    f;vmax;5;0;rw

r;haptics_rate_cfg1;0x54
    f;rate_7_0;7;0;rw;r_lo

r;haptics_rate_cfg2;0x55
    f;rate_11_8;3;0;rw;r_hi

r;haptics_external_pwm;0x57
    f;freq_sel;1;0;rw;freq
        v;ext_pwm_100khz;3;100k
        v;ext_pwm_75khz;2;75k
        v;ext_pwm_50khz;1;50k
        v;ext_pwm_25khz;0;25k

r;haptics_sc_clr;0x59
    f;sc_clr;0;0;rw;sc
        v;sc_clr;1;e
        v;sc_dis;0;d

r;haptics_brake;0x5c
    f;pattern4;7;6;rw;p4
        v;p4_vmax;3;vm
        v;p4_vmax_div2;2;vm_d2
        v;p4_vmax_div4;1;vm_d4
        v;p4_zero;0;zero
    f;pattern3;5;4;rw;p3
        v;p3_vmax;3;vm
        v;p3_vmax_div2;2;vm_d2
        v;p3_vmax_div4;1;vm_d4
        v;p3_zero;0;zero
    f;pattern2;3;2;rw;p2
        v;p2_vmax;3;vm
        v;p2_vmax_div2;2;vm_d2
        v;p2_vmax_div4;1;vm_d4
        v;p2_zero;0;zero
    f;pattern1;1;0;rw;p1
        v;p1_vmax;3;vm
        v;p1_vmax_div2;2;vm_d2
        v;p1_vmax_div4;1;vm_d4
        v;p1_zero;0;zero

r;haptics_wf_repeat;0x5e
    f;wf_repeat;6;4;rw;wfr
        v;wf_repeat_128_times;7;r128
        v;wf_repeat_64_times;6;r64
        v;wf_repeat_32_times;5;r32
        v;wf_repeat_16_times;4;r16
        v;wf_repeat_8_times;3;r8
        v;wf_repeat_4_times;2;r4
        v;wf_repeat_2_times;1;r2
        v;wf_repeat_1_times;0;r1
    f;wf_repeat_rsvd;3;2;rw;wfr_res
    f;wf_s_repeat;1;0;rw;wfsr
        v;wf_s_repeat_8_times;3;r8
        v;wf_s_repeat_4_times;2;r4
        v;wf_s_repeat_2_times;1;r2
        v;wf_s_repeat_1_times;0;r1

r;haptics_wf_s1;0x60
    f;s1_sign;7;7;rw
        v;reverse;1;r
        v;forward;0;f
    f;s1_ovd;6;6;rw
        v;ovd_2x;1;2x
        v;ovd_1x;0;1x
    f;s1_amp;5;1;rw

r;haptics_wf_s2;0x61
    f;s2_sign;7;7;rw
        v;reverse;1;r
        v;forward;0;f
    f;s2_ovd;6;6;rw
        v;ovd_2x;1;2x
        v;ovd_1x;0;1x
    f;s2_amp;5;1;rw

r;haptics_wf_s3;0x62
    f;s3_sign;7;7;rw
        v;reverse;1;r
        v;forward;0;f
    f;s3_ovd;6;6;rw
        v;ovd_2x;1;2x
        v;ovd_1x;0;1x
    f;vamp;5;1;rw

r;haptics_wf_s4;0x63
    f;s4_sign;7;7;rw
        v;reverse;1;r
        v;forward;0;f
    f;s4_ovd;6;6;rw
        v;ovd_2x;1;2x
        v;ovd_1x;0;1x
    f;s4_amp;5;1;rw

r;haptics_wf_s5;0x64
    f;s5_sign;7;7;rw
        v;reverse;1;r
        v;forward;0;f
    f;s5_ovd;6;6;rw
        v;ovd_2x;1;2x
        v;ovd_1x;0;1x
    f;s5_amp;5;1;rw

r;haptics_wf_s6;0x65
    f;s6_sign;7;7;rw
        v;reverse;1;r
        v;forward;0;f
    f;s6_ovd;6;6;rw
        v;ovd_2x;1;2x
        v;ovd_1x;0;1x
    f;s6_amp;5;1;rw

r;haptics_wf_s7;0x66
    f;s7_sign;7;7;rw
        v;reverse;1;r
        v;forward;0;f
    f;s7_ovd;6;6;rw
        v;ovd_2x;1;2x
        v;ovd_1x;0;1x
    f;s7_amp;5;1;rw

r;haptics_wf_s8;0x67
    f;s8_sign;7;7;rw
        v;reverse;1;r
        v;forward;0;f
    f;s8_ovd;6;6;rw
        v;ovd_2x;1;2x
        v;ovd_1x;0;1x
    f;s8_amp;5;1;rw

r;haptics_play;0x70
    f;play;7;7;rw
        v;play;1;p
        v;stop;0;s
    f;pause;0;0;rw
        v;pause;1;p
        v;continue;0;c