#!/bin/bash
#
# script to generate a DVS release and upload it to server
#

usage ()
{
    echo "    create a DVS release package and upload it"
    echo "Usage: $0 [-n]"
    echo "    -n  : create release package, but do not upload it"
    return
}


main () {
    while [ $# -gt 0 ]
    do
        case $1 in
        -n*) # No upload
            NO_UPLOAD=1
            break
            ;;
        -h) # help
            usage
            ;;
        *) # anything else is invalid
            echo "invalid option: $1"
            usage
            ;;
        esac
        shift
    done


    if [ -z "${ANDROID_BUILD_TOP}" ]; then
        echo "You must have an Android workspace with environment correctly set up"
        return
    fi

    DVS_DIR=${PWD##*/}
    DVS_SRC=${ANDROID_BUILD_TOP}/vendor/bbry/${DVS_DIR}
    DVS_RELEASE=`cat ${DVS_SRC}/scripts/dvs.lua | grep "DVS_VERSION="| grep -o "[0-9]*\.[0-9]*\.[0-9]*"`
    if [ -z $DVS_RELEASE ]; then
        echo "DVS release version not found"
        return
    fi

    echo "create dir ${DVS_RELEASE}"
    # if release directory already exists, remove it first
    if [ -e ${DVS_RELEASE} ]; then
        echo "removing old release directory"
        rm -rf ${DVS_RELEASE}
    fi
    mkdir -p ${DVS_RELEASE}

    # Scripts
    echo "create dir ${DVS_RELEASE}/target/system/xbin"
    mkdir -p ${DVS_RELEASE}/target/system/xbin
    echo "copy scripts files "
    cp -p ${DVS_SRC}/scripts/*  ${DVS_RELEASE}/target/system/xbin

    # RD files
    echo "create dir ${DVS_RELEASE}/target/system/etc/dvs"
    mkdir -p ${DVS_RELEASE}/target/system/etc/dvs
    echo "copy RD files"
    cp -p ${DVS_SRC}/extras/*.rd  ${DVS_RELEASE}/target/system/etc/dvs

    # libs
    echo "create dir ${DVS_RELEASE}/target/system/lib64"
    mkdir -p ${DVS_RELEASE}/target/system/lib64
    echo "create dir ${DVS_RELEASE}/target/system/lib"
    mkdir -p ${DVS_RELEASE}/target/system/lib
    echo "copy ${ANDROID_PRODUCT_OUT}/system/lib64/libdvsc.so to ${DVS_RELEASE}/target/system/lib64"
    cp -p ${ANDROID_PRODUCT_OUT}/system/lib64/libdvsc.so ${DVS_RELEASE}/target/system/lib64
    echo "copy ${ANDROID_PRODUCT_OUT}/system/lib/libdvsc.so to ${DVS_RELEASE}/target/system/lib"
    cp -p ${ANDROID_PRODUCT_OUT}/system/lib/libdvsc.so ${DVS_RELEASE}/target/system/lib

    # libqmic and libqmiservices_ext
    echo "copy ${ANDROID_PRODUCT_OUT}/system/lib64/libqmic.so to ${DVS_RELEASE}/target/system/lib64"
    cp -p ${ANDROID_PRODUCT_OUT}/system/lib64/libqmic.so ${DVS_RELEASE}/target/system/lib64
    echo "copy ${ANDROID_PRODUCT_OUT}/system/lib/libqmic.so to ${DVS_RELEASE}/target/system/lib"
    cp -p ${ANDROID_PRODUCT_OUT}/system/lib/libqmic.so ${DVS_RELEASE}/target/system/lib
    echo "create dir ${DVS_RELEASE}/target/system/vendor/lib64"
    mkdir -p ${DVS_RELEASE}/target/system/vendor/lib64
    echo "create dir ${DVS_RELEASE}/target/system/vendor/lib"
    mkdir -p ${DVS_RELEASE}/target/system/vendor/lib
    echo "copy ${ANDROID_PRODUCT_OUT}/system/vendor/lib64/libqmiservices_ext.so to ${DVS_RELEASE}/target/system/vendor/lib64"
    cp -p ${ANDROID_PRODUCT_OUT}/system/vendor/lib64/libqmiservices_ext.so ${DVS_RELEASE}/target/system/vendor/lib64
    echo "copy ${ANDROID_PRODUCT_OUT}/system/vendor/lib/libqmiservices_ext.so to ${DVS_RELEASE}/target/system/vendor/lib"
    cp -p ${ANDROID_PRODUCT_OUT}/system/vendor/lib/libqmiservices_ext.so ${DVS_RELEASE}/target/system/vendor/lib

    # install files
    echo "copy install files to ${DVS_RELEASE}"
    cp -p ${DVS_SRC}/install.sh  ${DVS_RELEASE}
    cp -p ${DVS_SRC}/install.bat  ${DVS_RELEASE}

    # set permissions
    chmod +x ${DVS_RELEASE}/*
    chmod -x ${DVS_RELEASE}/target/system/etc/dvs/*
    chmod +x ${DVS_RELEASE}/target/system/xbin/*
    chmod +x ${DVS_RELEASE}/target/system/lib64/*
    chmod +x ${DVS_RELEASE}/target/system/lib/*

    # check if need to upload
    if [ -z "${NO_UPLOAD}" ]; then
        echo "upload ${DVS_RELEASE} to //fsg04yow/projects/8992Factory/DVS/"
        smbclient //fsg04yow/projects/ -c "cd 8992Factory/DVS/;  tarmode; recurse; prompt; mput ${DVS_RELEASE}"
    fi
}

main "$@"
