@echo off
REM
REM  sideloads the 8992 DVS package
REM

REM make adb root, wait if it wasn't already
REM wait for adb to come back after root
ECHO making adb root ...
REM FOR /F %%i IN ('adb root 2^>nul ^| FINDSTR /V /C:"already"') DO TIMEOUT /t 3 /nobreak >nul
adb root
IF %ERRORLEVEL% == 1 (
    GOTO exit
)

REM mount /system rw
ECHO mounting file system as rw ...
adb remount 2>&1 | FINDSTR /C:"remount succeeded" > NUL
IF %ERRORLEVEL% == 1 (
    echo Cannot install DVS files, remount failed
    GOTO exit
)

REM load the files on the device
ECHO loading DVS files ...
adb push target/. /

echo Done
:exit
echo.
