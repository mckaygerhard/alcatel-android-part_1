#!/system/bin/sh
#-----------------------------------
#
# does a sanity check on as many DVS tests as it can -
#    do the executables in DVS run?
#    produce successful looking output?
#    produce any stderr?
#    exit with bad return codes?
#
#    logs tests, reports on results etc.
#-----------------------------------

init() {
    RESULTS=/data/dvs_sanity_results.txt
    ERRORS=/data/dvs_sanity_stderr.txt
    FULLLOG=/data/dvs_sanity_log.txt
    TMP=/data/local/tmp

    # clear out old logs
    echo removing old logs $RESULTS $ERRORS $FULLLOG
    rm $RESULTS $ERRORS $FULLLOG 2>/dev/null
    touch $RESULTS $ERRORS $FULLLOG 2>/dev/null

    # Using the architecture to determine whether running on the host or on the device
    if [ "`uname -m 2>/dev/null`" == "x86_64" ]; then

        # running from desktop linux..
        echo "Running sanity remotely"
        adb root | grep already || sleep 3 # wait for adb to come back after root
        adb remount
        adb push $0 /system/bin/
        adb shell sh /system/bin/dvs_sanity.sh $@ -w
        RESULTS_LOCAL=/tmp/dvs_sanity_results.txt
        ERRORS_LOCAL=/tmp/dvs_sanity_stderr.txt
        FULLLOG_LOCAL=/tmp/dvs_sanity_log.txt
        adb pull $RESULTS $RESULTS_LOCAL
        adb pull $ERRORS $ERRORS_LOCAL
        adb pull $FULLLOG $FULLLOG_LOCAL
        echo copied results to $RESULTS_LOCAL
        echo copied errors to $ERRORS_LOCAL
        echo copied full log to $FULLLOG_LOCAL

        exit
    fi

    #check commandline flags
    for i in "$@"
    do
    case $i in
        -v)
        VERBOSE=1
        ;;

        -a*)
        ASY=assembly
        ;;

        -w)
        NOWAIT=1
        ;;

        # -?*|-h*|--help*)
        *)
        echo
        echo "dvs_sanity.sh  -? (help)  -v (verbose)  -a (ASY/assembly) -w (nowait)"
        exit
        ;;
    esac
    done

    check_root || exit

    platform=`getprop ro.board.platform`

    echo Configuring for ${platform} ${ASY}
    #echo PMIC 8941 revision is `pmic_test -r0x103 -l1 | grep -v slave | grep 0x`
    #echo $platform_m | grep 8974AA >/dev/null && echo "8974 processor is V3"

    # defaults
    SDCARD_MOUNT=/mnt/media_rw/sdcard1/
    I2C_FG_ADD=0x36

    case "$platform" in
        *8992*)
            SW_TEST_IO1=85  # goes to debug board blue led
            SW_TEST_IO2=103 # goes to debug board switch
            MAX_PROC_GPIO=145
            MAX_PM_GPIO=22
            MAX_PMI_GPIO=10
            VOL_UP=2
            VOL_DOWN=3
            SDCARD_DETECT=8
            I2C_BUS=11
            I2C_ADD=60
            I2C_LEN=3
            I2C_REG=03
            I2C_VAL=81
            USBIN=usbin
            PMIC1_NO=0
            PMIC1_NAME=pm8994
            PMIC1_REV="51 09"
            PMIC2_NO=1
            PMIC2_NAME=pmi8994
            PMIC2_REV="51 0A"
            PMIC2_SLAVE=0x2
            FG_SRAM=595
            RAND_REG=usb_otg_vreg
            UNUSED_LDO=pm8994_l25 # actually a camera supply
            LDO1V8=pm8994_l30
            USB_DET=8
            BOOST_BYP=9
            VOL_UP_STATUS=0x0c108
            VOL_UP_MODE_CTL=0x0c140
            SIM_DET=100
            SIM_IN="in  lo"
            TZ_OK=1
            THERMAL_ZONE=0
            THERMAL_TYPE="bms"
            ;;
        *8974*)
            SW_TEST_IO1=91
            SW_TEST_IO2=92
            MAX_PROC_GPIO=145
            MAX_PM_GPIO=36
            #MAX_PMI_GPIO=0
            VOL_UP=22
            VOL_DOWN=23
            SDCARD_DETECT=20
            I2C_SUP1=8941_lvs1
            I2C_SUP2=8941_l17
            I2C_BUS=0
            I2C_ADD=36
            I2C_LEN=3
            I2C_REG=18
            I2C_VAL=e8
            USBIN=usb_in
            PMIC1_NO=0
            PMIC1_NAME=8941
            PMIC1_REV="51 01"
            PMIC2_NO=2
            PMIC2_NAME=8841
            PMIC2_REV="51 02"
            PMIC2_SLAVE=0x4
            RAND_REG=8941_smbb_otg
            UNUSED_LDO=8941_l10
            LDO1V8=8941_l10
            USB_DET=20
            BOOST_BYP=21
            VOL_UP_STATUS=0x0d508
            VOL_UP_MODE_CTL=0x0d540
            SIM_DET=100
            SIM_IN="in  hi"
            #TZ_OK=1
            THERMAL_ZONE=0
            THERMAL_TYPE="tsens_tz_sensor0"
            ;;
        *)
            echo "PLATFORM $platform not supported"
            exit
            ;;
    esac

    #trap 'print got an INT signal' INT # ctrl-c
    #trap 'print got a TERM signal' TERM

}

cleanup () {
    : # nothing
}

#output the ghetto progress indicator with special chars for various types of errors
progress() {
   if [ -z "$VERBOSE" ]; then
        case "$1" in
        0)
            # all good
            echo -n "-"
            ;;
        1)
            # bad output
            echo -n "o"
            ;;
        2)
            # stderr
            echo -n "e"
            ;;
        3)
            # return code bad
            echo -n "X"
            ;;
        4)
            # not run - skipped
            echo -n "_"
            ;;
        *)
            # unknown
            echo "?"
            ;;
        esac
    fi
}

write_result () {
    echo -n "$*" >> $TEMP_RESULTS
}

write_log () {
    echo $* >> $FULLLOG
}

cat_log () {
    cat $1 >> $FULLLOG
}

# deliberately outputs stderr, some unexpected stdout, and returns a bad returncode
# for checking sanity.sh script catches various problems with utilities
dvs_sanity_err () {
    echo "Errors encountered" >&2
    echo "unexpected output"
    return 25
}

# checks we're running as root, as many dvs commands require it.
check_root () {
    id | grep root >/dev/null
    if [ $? == 0 ]; then
       echo "running as root"
       return 0
    else
       echo "Not root! first type:" >&2
       echo "su" >&2
       echo
       return 1
    fi
}

# blindly run the test, keeping track of it's output, stderr, and returncode
# Clear necessary files first, update the test counter when done.
# other functions will check the correctness of the results:
# (success_test, error_test)
run_test() {
    TEMP_ERROR=/$TMP/temp_errors.txt
    TEMP_STDOUT=/$TMP/temp_stdout.txt
    TEMP_RESULTS=/$TMP/temp_results.txt
    if [ -z "${FULLLOG}" ]; then
        echo "FULLLOG isn't defined!"
        exit 1
    fi

    #cleanup
    rm $TEMP_ERROR $TEMP_STDOUT $TEMP_RESULTS 2>/dev/null

    # verbose progress
    if [ -n "$VERBOSE" ]; then
        echo " $*"
    fi

    # execute the test
    $* >$TEMP_STDOUT 2>$TEMP_ERROR

    #save return code
    returncode=$?
    problem=0    # no problem - yet

    # update the counter of how many tests have been run
    echo ${test_counter:=0} >/dev/null  # if unset set to 0
    ((test_counter+=1))

}

# do one test according to arguments.
# output header in log, create a pass/fail entry in results file.
# $1 has the expected output - if grep doesn't find that, fail.
success_test() {
    successString=${1}
    shift

    run_test $*

    # look at results
    # build the line entry in the results file

    # output the running test and report the return code:
    if [ $returncode == 0 ]; then
       write_result "$* \t\t Passed "
    else
       write_result "$* \t\t ## FAILED ## "
    fi

    # check expected output
    if [[ $successString = "" ]]; then
        # not expecting any output...
        write_result "- output ok"
    else
        grep -sqF "${successString}" $TEMP_STDOUT
        # grep's returncode tells us what we need to know
        if [ $? == 0 ]; then
           write_result "- output ok"
        else
           write_result "- output # BAD #"
           problem=1   # bad output
        fi
    fi
    # TODO could also check for "error/fail" in stdout - slogs often have these however...

    # check if there was any stderr
    if [[ -s $TEMP_ERROR ]]; then
        write_result " - with # STDERR #\n"
        problem=2   # had stderr
    else
        write_result ".\n"
    fi ;

    # check return code:
    if [ ! $returncode == 0 ]; then
       problem=3    # bad return code
    fi

    # record test in fulllog
    write_log
    write_log -------------------------------------
    cat_log $TEMP_RESULTS
    write_log "   expecting: ${successString}"
    write_log -------------------------------------
    cat_log $TEMP_STDOUT
    # add any errors to full log too.
    if [[ -s $TEMP_ERROR ]]; then
        write_log "-- stderr --"
        cat_log $TEMP_ERROR
    fi

    # add to the full error log
    echo "## ${*}" >>$ERRORS
    cat $TEMP_ERROR >>$ERRORS

    # add to the full results log
    cat $TEMP_RESULTS >> $RESULTS

    # show our ghetto progress indicator
    progress $problem

    return $problem
}

# run test according to arguments - but expect stderr and bad return code.
# output header in log, create a pass/fail entry in results file.
# $1 has the expected stderr - if grep doesn't find that, fail.
error_test() {
    successString=${1}
    shift

    run_test $*

    # look at results file
    # build the line entry in the results file

    # output the running test and report the return code:
    if [ $returncode == 0 ]; then
       write_result "$* \t\t ## Ret = 0 ## "
    else
       write_result "$* \t\t Passed "
    fi

    # check expected output
    if [[ $successString = "" ]]; then
        # not expecting any output...
        write_result " - output ok"
    else
        grep -sqF "${successString}" $TEMP_ERROR
        # grep's returncode tells us what we need to know
        if [ $? == 0 ]; then
           write_result "- output ok"
        else
           write_result "- output # BAD #"
           problem=1   # bad output
        fi
    fi
    # TODO could also check for "error/fail" in stdout - slogs often have these however...

    # check if there was any stderr
    if [[ -s $TEMP_ERROR ]]; then
        write_result ".\n"
    else
        write_result " - without expected # STDERR #\n"
        problem=2   # no stderr when there should've been
    fi ;

    # check return code:
    if [ $returncode == 0 ]; then
       problem=3    # bad return code
    fi

    # record test in fulllog
    write_log
    write_log -------------------------------------
    cat_log $TEMP_RESULTS
    write_log "   expecting: ${successString} (in stderr)"
    write_log -------------------------------------
    cat_log $TEMP_STDOUT
    # add any errors to full log too.
    if [[ -s $TEMP_ERROR ]]; then
        write_log "-- stderr --"
        cat_log $TEMP_ERROR
    fi

    # add to the full error log
    echo "## ${*}" >>$ERRORS
    # only put stderr in error log if there was a problem.
    # as error_test expects some stderr
    if [ $problem != 0 ]; then
        cat $TEMP_ERROR >>$ERRORS
    fi

    # add to the full results log
    cat $TEMP_RESULTS >> $RESULTS

    # update our ghetto progress indicator
    progress $problem

    return $problem
}

# used for commenting out tests while indicating in the report that they're failing.
# ignore arguments
# output header in log, create a pass/fail entry in results file.
skip_test() {
    TEMP_ERROR=/$TMP/temp_errors.txt
    TEMP_STDOUT=/$TMP/temp_stdout.txt
    TEMP_RESULTS=/$TMP/temp_results.txt
    if [ -z "${FULLLOG}" ]; then
        echo "FULLLOG isn't defined!"
        exit 1
    fi

    #cleanup
    rm $TEMP_ERROR $TEMP_STDOUT $TEMP_RESULTS 2>/dev/null

    successString=${1}
    shift

    # verbose progress
    if [ -n "$VERBOSE" ]; then
        echo " $*  # skipped"
    fi

    write_result "$* \t\t ## FAILED ## - skipped\n"

    write_log
    write_log ------------------------------
    cat_log $TEMP_RESULTS
    write_log ------------------------------
    write_log "Skipping ${*} - May hang, isnt supported, or is temporarily removed"

#    echo "## ${*}" >>$ERRORS
#    echo "didn't run ${*}" >>$ERRORS

    # ghetto progress indicator
    progress 4    # 4:skip

    # update the counter of how many tests have been run
    echo ${test_counter:=0} >/dev/null  # if unset set to 0
    ((test_counter+=1))

    return 4
}

# execute $2+ , after $1 seconds terminate if not already closed
# doesn't work well on scripts - ends up killing self and sanity doesn't complete
call_and_term() {
    delay=${1}
    shift
    successString=${1}
    #echo "${successString}"
    shift

    # approach 1
    # save PID to interrupt - kills this script however...
    #PID=$$
    #(sleep ${delay} ; slay -s SIGINT $PID >/dev/null 2>/dev/null) &
    #success_test $@


    # approach 2
    # will slay process with name $1 in $delay seconds...
    success_test "${successString}" $* &
    #echo -n ${delay}s
    sleep ${delay}
    slay $1 >/dev/null

}


#################################################
# sequence of the tests to run:
#
# run success-case DVS tests like so:
#   success_test "the expected output (non-regex)" <dvs command with arguments>
#
# tests can be marked as a failed without actually running them - eg. if they hang sanity.
#   skip_test "unused string" <dvs command or description of test>
#
# running fail-case DVS tests - for boundary and expected error conditions:
#   error_test "expected stderr" <dvs command>
#
#################################################
call_tests() {
    #pre-test instructions
    echo
    echo "Before starting the test:"
    #echo ". connect USB, optionally via cradle"

    echo ". insert a SD card"
    #echo ". insert a sim card"
    #echo ". touch the screen (if present)"
    #if [ "${ASY}" == "assembly" ]; then
    #   echo ". connect headphones"
    #   echo ". place a NFC tag behind device"
    #fi
    #echo ". connect HDMI cable to monitor"
    #echo ". wave magnet over hall-effect sensor"
    #echo ". press and hold mute key"
    echo
    if [ -z "$NOWAIT" ]; then
        echo "press return key to continue"
        read junk      # pause and wait for "enter" key
    fi

    if [ -z "$VERBOSE" ]; then
        echo "-:success, _:skipped, o:bad o/p, e:stderr, X:bad return"
        echo -n "  test progress: "
    fi

    # dvs_sanity_err should be the first test to run - it checks we can properly catch:
    # any stderr, bad output and a bad return code
    #success_test "Errors encountered" dvs_sanity_err #use when debugging sanity script
    error_test "Errors encountered" dvs_sanity_err

    # check we have lua version that we expect
    success_test "Lua 5.2.3" lua -v

    success_test $platform lua -ldvs -e 'print(get_platform())'

    # -- start of regular DVS tests --
    # Quality of sanity depends on how well we choose options for each utility
    # Do tests in order that they appear on DVS status page

    # turn on debug board blue led - to signal test has started.
    success_test "set   1(enable              ) to gpio_oe" msm_gpio.lua c ${SW_TEST_IO1} oe=1 o=1

    # 4 JTAG tests - 80-NM328-38_A_MSM8994_System_Drivers_PMIC_Overview.pdf
    # 6 USB
    # 7.1 RTC
    success_test "This is a utility for read/write enable/disable the PMIC Real Time Clock (RTC)" rtc_test.lua -h
    success_test "pmic rtc_rdata registers data" rtc_test.lua d
    success_test "RTC is already on" rtc_test.lua c e=enable
    error_test "Incorrect sub-command" rtc_test.lua j
    error_test "I did not find any setting=value string" rtc_test.lua c
    error_test "Wrong value garbage for setting enable" rtc_test.lua c e=garbage
    error_test "must be a number >0" rtc_test.lua c t=abcd
    error_test "must be a number >0" rtc_test.lua c t=-123


    # 7.2 ADC
    success_test "ref_1250v " adc_test.lua d
    success_test "${USBIN} " adc_test.lua d ${USBIN} # on PMI
    success_test "msm_therm " adc_test.lua d msm_therm # on PM
    success_test "msm_therm" adc_test.lua l
    error_test "No data" adc_test.lua d garbage_name
    success_test "adc_test.lua <sub-command> [sub-command arguments]" adc_test.lua
    error_test "Wrong parameter a, Need l or d" adc_test.lua a

    # 7.3 BMS/FG
    success_test "voltage_ocv" bms_test.lua d
    success_test "This is a utility for testing Battery/FG/Charging." bms_test.lua
    error_test "Incorrect sub-command garbage_argument" bms_test.lua garbage_argument
    error_test "I did not find any setting=value string" bms_test.lua c
    error_test "Unknown option" bms_test.lua -g
    error_test "ERROR: Wrong value garbage for setting enable" bms_test.lua c e=garbage
    error_test "ERROR: Wrong value garbage for setting rate" bms_test.lua c r=garbage
    error_test "ERROR: Wrong value garbage for setting dc" bms_test.lua c dc=garbage
    error_test "ERROR: Wrong value garbage for setting usb" bms_test.lua c usb=garbage
    error_test "ERROR: Wrong value garbage for setting capacity" bms_test.lua c c=garbage
    error_test "ERROR: Wrong value 101 for setting capacity_forever" bms_test.lua c capacity_forever=101
    if [ -n "${FG_SRAM}" ]; then
        success_test "" bms_test.lua c e=disable
        success_test "Set usb current limit to 500 mA" bms_test.lua c e=e r=500 dc=500 usb=500
        success_test "Set usb current limit to 300 mA" bms_test.lua c usb=100
        success_test "Set usb current limit to 3000 mA" bms_test.lua c usb=4000
        success_test "258 00 B5 27 00" bms_test.lua r ${FG_SRAM} 10
        success_test "259 -- B5" bms_test.lua r 0x259
        error_test "ERROR: need valid SRAM address" bms_test.lua r
        error_test "ERROR: need valid SRAM address" bms_test.lua r garbage
        error_test "ERROR: wrong count garbage" bms_test.lua r ${FG_SRAM} garbage
        success_test "" bms_test.lua w 0x${FG_SRAM} 3c 00 0x2a
        error_test "Need an address and at least one byte of data" bms_test.lua w
        error_test "Need an address and at least one byte of data" bms_test.lua w ${FG_SRAM}
        error_test "ERROR: need valid SRAM address" bms_test.lua w garbage 00
        error_test "ERROR: Wrong value garbage for setting rate" bms_test.lua c r=garbage
    fi


    # 7.4 Coin cell
    # 7.5 MPP
    # 7.6 Watchdog and SMPL

    # 7.7 LDO and LVS
    # 7.8 SMPS
    success_test "This script uses regulator framework to display and configure regulators" regulator_test.lua -h
    success_test "${PMIC1_NAME}_l24 " regulator_test.lua d
    success_test "${PMIC1_NAME}_s3" regulator_test.lua l
    success_test "${PMIC1_NAME}_lvs2 " regulator_test.lua l
    success_test "${PMIC1_NAME}_s3 " regulator_test.lua d ${PMIC1_NAME}_s3
    success_test "${RAND_REG} " regulator_test.lua d -c ${RAND_REG}
    success_test "${PMIC1_NAME}_l21 " regulator_test.lua d l21
    success_test "${PMIC1_NAME}_lvs2 " regulator_test.lua d lvs2
    success_test "${RAND_REG} " regulator_test.lua d ${RAND_REG}
    success_test "${RAND_REG} " regulator_test.lua d ${RAND_REG}
    success_test "${PMIC1_NAME}_l3 " regulator_test.lua d ${PMIC1_NAME}_l3 -c v=garbage
    success_test "${PMIC1_NAME}_l3 " regulator_test.lua d ${PMIC1_NAME}_l3 c m=garbage
    success_test "disabled" regulator_test.lua d ${UNUSED_LDO}
    success_test "state 1" regulator_test.lua c ${UNUSED_LDO} e=e
    success_test "Already in that state" regulator_test.lua c ${UNUSED_LDO} e=enable
    success_test "state 0" regulator_test.lua c ${UNUSED_LDO} e=0
    success_test "Already in that state" regulator_test.lua c ${UNUSED_LDO} e=disable
    success_test "disabled" regulator_test.lua d ${PMIC1_NAME}_s3
    success_test "state 1" regulator_test.lua c ${PMIC1_NAME}_s3 e=1
    success_test "state 0" regulator_test.lua c ${PMIC1_NAME}_s3 e=disable
    error_test "Wrong value " regulator_test.lua c ${LDO1V8} e=garbage
    error_test "Need a regulator name" regulator_test.lua c
    error_test "I did not find any " regulator_test.lua c ${LDO1V8}

    # 7.9 Wireless Charging
    # 7.10 System clocks
    # 7.11 Charger
    # 7.12 Battery ID
    # 7.13 Battery Temp

    # 7.14 Vibrator
    success_test "This is a utility for turning the vibrator on/off." vibrator_test.lua -h
    success_test "Setting vibrate time to" vibrator_test.lua c t=1000
    success_test "Setting vibrate intensity to" vibrator_test.lua c i=50
    success_test "To protect the hardware, the driver may limit the maximum enable time to 15 seconds" vibrator_test.lua c t=16000
    success_test "Setting vibrate time to" vibrator_test.lua c t=500 i=100
    error_test "ERROR: I did not find any setting=value string" vibrator_test.lua c
    error_test "ERROR: Incorrect sub-command 't=1000'" vibrator_test.lua t=1000
    error_test "ERROR: Wrong value garbage for setting time" vibrator_test.lua c t=garbage
    error_test "ERROR: Wrong value garbage for setting intensity" vibrator_test.lua c i=garbage
    error_test "ERROR: Wrong value -20 for setting intensity" vibrator_test.lua c i=-20 t=500

    # 7.15 PMIC GPIOs
    success_test "pmic_function.lua <sub-command> [sub-command arguments]" pmic_function.lua -h
    error_test "Unknown option" pmic_function.lua -a
    success_test "lvs1 " pmic_function.lua d
    success_test "lvs1 " pmic_function.lua d -s
    success_test "Detailed description for:  lvs1" pmic_function.lua d -d
    success_test "pmic_function.lua <sub-command> [sub-command arguments]" pmic_function.lua
    #pm gpios
    success_test "gpio${MAX_PM_GPIO} " pmic_function.lua d gpio*
    success_test "Detailed description for:  gpio${MAX_PM_GPIO}" pmic_function.lua d -d gpio*
    success_test "gpio_input_high" pmic_function.lua d gpio${VOL_UP}
    success_test "gpio${VOL_UP}" pmic_function.lua d -s gpio${VOL_UP}
    success_test "gpio_input_high" pmic_function.lua d gpio${VOL_DOWN}
    success_test "gpio${VOL_UP}" pmic_function.lua d gpio${VOL_DOWN} gpio${VOL_UP}
    success_test "Detailed description for:  gpio${VOL_UP}" pmic_function.lua d -d gpio${VOL_DOWN} gpio${VOL_UP}
    # pull (i/p) test
    success_test "pullup_30uA" pmic_function.lua d gpio${USB_DET}
    success_test "pulldown_10uA" pmic_function.lua c g${USB_DET} p=pulldown_10uA
    success_test "no_pull" pmic_function.lua c g${USB_DET} p=np
    success_test "pullup_30uA" pmic_function.lua c g${USB_DET} p=pu30
    success_test "pullup_31.5uA" pmic_function.lua c g${USB_DET}
    # strength (output)
    success_test "output_drv_sel" pmic_function.lua d gpio${BOOST_BYP}
    success_test "to output_drv_sel" pmic_function.lua c gpio${BOOST_BYP} d=high
    success_test "to output_drv_sel" pmic_function.lua c gpio${BOOST_BYP} d=m
    success_test "to output_drv_sel" pmic_function.lua c gpio${BOOST_BYP} output_drv_sel=l

    error_test "No matching function" pmic_function.lua d gpio0
    error_test "No matching function" pmic_function.lua d gpio$(($MAX_PM_GPIO+1))
    error_test "Incorrect subcmd" pmic_function.lua gpio${SDCARD_DETECT}
    error_test "wrong value garbage for pullup_sel" pmic_function.lua c g${USB_DET} p=garbage
    error_test "is not supported" pmic_function.lua c p=np

    #pmi gpios
    if [ -n "${MAX_PMI_GPIO}" ]; then
        success_test "pgpio${MAX_PMI_GPIO} " pmic_function.lua d pgpio*
        error_test "No matching function" pmic_function.lua d pgpio0
        error_test "No matching function" pmic_function.lua d pgpio$(($MAX_PMI_GPIO+1))
    fi

    # 7.16 TFT

    # 7.17 SPMI driver
    success_test "This is a utility for reading/writing PMIC registers" pmic_test.lua -h
    success_test "${PMIC1_REV}" pmic_test.lua -p${PMIC1_NAME} -c8 -s 0x00100      # read PM8994 revision
    success_test "${PMIC1_REV}" pmic_test.lua -p${PMIC1_NO} -c8 -s 0x00100      # read PM8994 revision
    success_test "${PMIC1_REV}" pmic_test.lua -c8 -s 100      # read PM8994 revision
    success_test "${PMIC2_REV}" pmic_test.lua -c8 -s ${PMIC2_SLAVE}0100      # read PMI8994 revision
    success_test "${PMIC2_REV}" pmic_test.lua -p${PMIC2_NO} -c8 -s 100      # read PMI8994 revision
    success_test "${PMIC2_REV}" pmic_test.lua -p${PMIC2_NAME} -c8 -s 0x0100      # read PMI8994 revision
    success_test "81" pmic_test.lua -c1 ${VOL_UP_STATUS}    # read PM_gpio_2 vol_up state
    success_test "Assuming pmic is 0" pmic_test.lua -v -c1 ${VOL_UP_STATUS}    # read PM_gpio_2 vol_up state
    success_test "${VOL_UP_MODE_CTL}" pmic_test.lua ${VOL_UP_MODE_CTL} 0x20 # make GPIO 2 output
    success_test "80" pmic_test.lua -p0 -c1 ${VOL_UP_STATUS}    # read PM_gpio_2 vol_up state
    success_test "${VOL_UP_MODE_CTL}" pmic_test.lua -ppm ${VOL_UP_MODE_CTL} 0x00 # make GPIO 2 input
    success_test "81" pmic_test.lua -c1 ${VOL_UP_STATUS}    # read PM_gpio_2 vol_up state
    success_test "success" pmic_test.lua 0x0c040 0x0 0x00 4 00 0 1 0x80  # large write to rw registers
    success_test "success" pmic_test.lua c043 00 00 01 80 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 #multiline write
    success_test "success" pmic_test.lua 0x0c040 0x1 0x07 5 03 0 23 0x00  # large write to rw registers
    error_test "either need data to write or -l" pmic_test.lua -c0 0x0c108
    error_test "needs to be 0, 1, 2" pmic_test.lua -pgarbage 0x0c108
    error_test "must be 0, 1, or 2" pmic_test.lua -p5 0x0c108
    error_test "needs to be 0, 1, 2" pmic_test.lua -pPM8994 0x0c108
    # need to add more error_tests
    error_test "Legacy option -l cannot be used together with -c" pmic_test.lua -c1 -l5 0x0c108
    error_test "Need an address" pmic_test.lua -c1
    error_test "Need data to write in hex bytes" pmic_test.lua 0x0c108 garbage
    error_test "read unit must be 1, 2, or 4" pmic_test.lua -u3 0x0c108
    error_test "does not seem to be a valid address" pmic_test.lua -rgarbage
    error_test "read error. nothing returned" pmic_test.lua 0x00
    error_test "readlength > 65536" pmic_test.lua -c65537 -s 0xc000
    error_test "Missed some data" pmic_test.lua 0x200

    # 7.18 external switches

    # 8 USIM
###    success_test "${SIM_IN}" gpio_test.lua -g${SIM_DET} -r # check state of SIM card
###    success_test "NOT_READY" getprop gsm.sim.state

    # 9. MSM GPIOs
    success_test "This is a utility for displaying/modifying MSM gpio" msm_gpio.lua -h
    success_test "gpio${MAX_PROC_GPIO} " msm_gpio.lua d
    success_test "gpio${MAX_PROC_GPIO} " msm_gpio.lua d -s
    success_test "Detailed description for:  gpio${MAX_PROC_GPIO}" msm_gpio.lua d -d
    success_test "gpio_oe             ) = 1 (enable" msm_gpio.lua d gpio${SW_TEST_IO1}
    success_test "gpio_in             ) = 0 (low" msm_gpio.lua d ${SW_TEST_IO2}
    success_test "gpio${MAX_PROC_GPIO}" msm_gpio.lua d gpio${SW_TEST_I01} gpio${MAX_PROC_GPIO}
    success_test "gpio${MAX_PROC_GPIO}" msm_gpio.lua d -s gpio${SW_TEST_I01} gpio${MAX_PROC_GPIO}
    success_test "Detailed description for:  gpio${MAX_PROC_GPIO}" msm_gpio.lua d -d gpio${SW_TEST_I01} gpio${MAX_PROC_GPIO}

    # todo alt-func tests
    success_test "to drv_strength" msm_gpio.lua c gpio${SW_TEST_IO1} d=4mA
    success_test "to drv_strength" msm_gpio.lua c gpio${SW_TEST_IO1} drv_strength=2m
    success_test "no_pull             ) to gpio_pull" msm_gpio.lua c gpio${SW_TEST_IO2} p=no_pull
    success_test "pull_down           ) to gpio_pull" msm_gpio.lua c gpio${SW_TEST_IO2} gpio_pull=pd

    error_test "No matching function" msm_gpio.lua d -1
    error_test "No matching function" msm_gpio.lua d gpio$(($MAX_PROC_GPIO+1))
    error_test "Incorrect subcmd" msm_gpio.lua ${MAX_PROC_GPIO}

    # 10 memory tests
    # 11 Bluetooth
    # 12 WLAN
    # 13.1 keyboard

    # 13.2 keypad backlight
    success_test "This is a utility for testing keypad backlight" backlight_test.lua
    success_test "This is a utility for testing keypad backlight" dvs.lua backlight
    success_test "ramp_step" backlight_test.lua d
    success_test "Setting brightness to 255" backlight_test.lua c b=255
    success_test "Setting brightness to 0" backlight_test.lua c b=0
    success_test "Setting fs_current to 10000" backlight_test.lua c fs_curr_ua=10000
    success_test "Setting max_brightness to 200" backlight_test.lua c mb=200
    success_test "Starting keypad backlight ramp" backlight_test.lua c sr=e rt=1
    success_test "Setting ramp_step" backlight_test.lua c rt=20 rs=100
    success_test "Starting keypad backlight ramp" backlight_test.lua c sr=1 fc=5000 max_brightness=255  
    error_test "ERROR: I did not find any setting=value string" backlight_test.lua c
    error_test "ERROR: Wrong value 500 for setting brightness" backlight_test.lua c b=500
    error_test "ERROR: Wrong value 30001 for setting fs_curr_ua" backlight_test.lua c fc=30001

    # 13.3 touch screen
    # 13.4 touchpad
    # 13.5 hall effect
    # 13.6 Altimeter
    # 13.7 Gyroscope

    # 13.8 i2c and uart
    #  DDR buck regulator
    success_test "This is a utility for manually exercising the i2c busses" i2c_test.lua
    success_test "i2c-${I2C_BUS}" i2c_test.lua l
    if [ -n "${I2C_SUP1}" ]; then
        success_test "to state 1" regulator_test.lua c ${I2C_SUP1} e=e
        success_test "to state 1" regulator_test.lua c ${I2C_SUP2} e=enable
    fi
    success_test " $I2C_ADD --" i2c_test.lua s $I2C_BUS
    error_test "is not an i2c bus" i2c_test.lua s 3
    success_test "reading $I2C_LEN bytes from bus:i2c-$I2C_BUS, slave:0x$I2C_ADD, starting address:0x$I2C_REG" i2c_test.lua r $I2C_BUS 0x$I2C_ADD $I2C_REG $I2C_LEN
    success_test "reading 1 bytes from bus:i2c-$I2C_BUS, slave:0x$I2C_ADD, starting address:0x$I2C_REG" i2c_test.lua r i2c-$I2C_BUS $I2C_ADD 0x$I2C_REG
    success_test "reading 1 bytes from bus:i2c-$I2C_BUS, slave:0x$I2C_ADD, starting address:0x$I2C_REG" i2c_test.lua r i2c-$I2C_BUS $I2C_ADD 0x$I2C_REG garbage
    error_test "slave-addr:0x88 is not valid" i2c_test.lua r $I2C_BUS 88 $I2C_REG $I2C_LEN
    error_test "data-addr:0xgarbage is not valid" i2c_test.lua r $I2C_BUS $I2C_ADD garbage $I2C_LEN
    success_test "0x$I2C_REG:  $I2C_VAL" i2c_test.lua r $I2C_BUS $I2C_ADD $I2C_REG 0x1
    success_test "1 bytes written" i2c_test.lua w $I2C_BUS $I2C_ADD $I2C_REG $I2C_VAL
    error_test "ERROR: Write failed at addr:0x$I2C_REG, data:0x100" i2c_test.lua w i2c-$I2C_BUS $I2C_ADD $I2C_REG 100
    error_test "ERROR: Write failed at addr:0x$I2C_REG, data:0x100" i2c_test.lua w $I2C_BUS $I2C_ADD 0x$I2C_REG 0x100
    error_test "data:garbage is not valid" i2c_test.lua w $I2C_BUS $I2C_ADD 0x$I2C_REG garbage
    if [ -n "${I2C_SUP1}" ]; then
        success_test "to state 0" regulator_test.lua c ${I2C_SUP1} e=disable
        success_test "to state 0" regulator_test.lua c ${I2C_SUP2} e=d
    fi

    # 13.9 ir and light sensor / tof
    # 13.10 LCD backlight
    # 13.11 OLED

    # 13.12 RGB LED
    success_test "testing RGB LEDs" led_test.lua -h
    success_test "led brightness to" led_test.lua c r=255
    error_test "Wrong value " led_test.lua c r=garbage
    success_test "led brightness to" led_test.lua c g=255
    success_test "led brightness to" led_test.lua c b=255
    error_test "is not a correct setting" led_test.lua c garbage_arg
    success_test "led brightness to" led_test.lua c r=0
    success_test "led brightness to" led_test.lua c red=64
    success_test "testing RGB LEDs" led_test.lua
    success_test "led brightness to" led_test.lua c green=128
    success_test "led brightness to" led_test.lua c b=192
    error_test "ERROR: Incorrect sub-command" led_test.lua r
    success_test "led brightness to" led_test.lua c blue=0
    success_test "led brightness to" led_test.lua c green=0
    error_test "value of blue must between" led_test.lua c red=10 g=5 b=256
    success_test "led blink to 1" led_test.lua f r=1
    success_test "led blink to 1" led_test.lua f r=1 blue=e
    error_test "ERROR: Wrong value" led_test.lua f blue=10
    success_test "led blink to 0" led_test.lua f red=0 b=d green=disable
    success_test "led brightness to" led_test.lua c r=0 blue=0 green=0

    # 13.13 Cameras

    # 13.14 Camera flash
###    success_test "current in mA" camflash_test.lua -h
###    success_test "brightness to 1000" camflash_test.lua -c2000 -f
###    success_test "brightness to 0" camflash_test.lua -c 1000 -f
###    success_test "Leaving torch on" camflash_test.lua -c 400 -t0
###    error_test "unexpected arguments" camflash_test.lua -c 1000 -f garbage_arg
###    error_test "Need one of" camflash_test.lua -c 1000
###    error_test "specify current" camflash_test.lua -f
###    error_test "specify current" camflash_test.lua -c hello -f
###    error_test "Please only one of" camflash_test.lua -c 1000 -f -t 10
###    error_test "Define torch delay" camflash_test.lua -c 400 -t notnumber
###    error_test "Max current is" camflash_test.lua -c 401 -t 10
###    error_test "Max current is" camflash_test.lua -c 2001 -f
###    success_test "Turning off" camflash_test.lua -c 0 -t 0

    # 13.15 Accelerometer
    # 13.16 magnetometer
    # 13.17 HDMI
    # 13.18 NFC
    # 13.19 RFFE
    # 13.20 MyDP
    # 13.21 Digital Interface Stress Testing
    # 14 FM
    # 15 Audio WCD
    # 16 RF hw calibration
    # 17 RF Perf

    # 18 SD card
####    success_test "in  lo" gpio_test.lua ${SDCARD_DETECT} -r # check state of SD detect switch
###    if [ $? -eq 0 ]; then
###        success_test "mmcblk1" ls /dev/block     # check for raw device
###        success_test "camera" ls $SDCARD_MOUNT   # check it is mounted
###    else
###        skip_test "" mount - no sdcard inserted
###    fi

    # 19.1 DVFM/DCVS
    # 20 sleep
    # 21.1 QFuse
    # 21.2 Cradle

    # 21.3 Temperature Sensors
    success_test "thermal_test.lua" thermal_test.lua -h
    success_test "Available thermal channels" thermal_test.lua l
    success_test "Displaying all thermal channels" thermal_test.lua d
    success_test "Displaying all thermal channel trip points" thermal_test.lua d -t
    success_test "${THERMAL_TYPE}" thermal_test.lua d ${THERMAL_ZONE}
    error_test "garbage not found" thermal_test.lua d garbage
    error_test "Unknown option" thermal_test.lua -t
    error_test "Need l or d" thermal_test.lua garbage_arg

    ## - stuff that eventually fails because of TrustZone (TZ)
    # RTC write, some PMIC regulator writes, system clock (for modem), qfuse
###    if [ -n "${TZ_OK}" ]; then
###        success_test "1347483647s since epoch" rtc_test.lua -w 1347483647 -t off
###        success_test "RTC is already off" rtc_test.lua -t 0
###        success_test "2012-09-12" rtc_test.lua -s date
###        success_test "21:00:47" rtc_test.lua -s time
###        success_test "1347483647" rtc_test.lua -s since_epoch
###        success_test "01 02 03 04" rtc_test.lua -w 67305985
###        success_test "RTC is turned off" rtc_test.lua -w 0
###        success_test "RTC is turned on" rtc_test.lua -t 1 -r

###        error_test "read doesn't match data just written!" pmic_test.lua -r0x06040 0x1 #RTC read-only area - writes fail

###        success_test "current_lim_test_enabled=0x20" regulator_test.lua -n${NORM_ON_LDO} -r ldo_current_lim -s current_lim_test_enabled #kills 8974!
###        success_test "current_lim_test_disabled=" regulator_test.lua -n${NORM_ON_LDO} -r ldo_current_lim -s current_lim_test_disabled #kills 8974
###        success_test "softstart_pd_250nA=" regulator_test.lua -n${PMIC1_NAME}_lvs1 -r lvs_soft_start_ctl -s softstart_pd_250nA # tz problem on 8974
###        success_test "1900000uV OK" regulator_test.lua -n${LDO1V8} -v 1900000 # known error and rc
###        success_test "1800000uV OK" regulator_test.lua -n${LDO1V8} -v 1800000
###        error_test "voltage failed" regulator_test.lua -n${LDO1V8} -v 1800000
###    fi

    # -- end of tests --

    # turn off debug blue led
    success_test "set   0(low                 ) to gpio_out" msm_gpio.lua c ${SW_TEST_IO1} o=0

    echo " done"
}

# tests not yet in sanity suite
todo() {

    # holster
    # radio
    :
}

execute_and_report() {
    # call the big sequence of DVS tests
    call_tests
    echo

    # now report results

    # output any errors found in stderr, including the message from dvs_sanity_err
    grep -v "##" $ERRORS   # exclude commands being run from output

    # summarise results
    # report of return codes of various tests
    echo
    echo "return Failures:" `grep -cF "# FAILED #" $RESULTS`
    echo "return passes that should've failed:" `grep -cF "# Ret = 0 #" $RESULTS`
    # report on other problems
    echo "with unexpected stderr:" `grep -cF "# STDERR #" $RESULTS`
    echo "with bad output:" `grep -cF "# BAD #" $RESULTS`
    echo -n "Clean Passes:" `grep -cF "Passed - output ok." $RESULTS`
    echo " out of ${test_counter} tests"
    echo
    # output those tests that didn't get a full pass
    grep -v -F "Passed - output ok." $RESULTS

    echo
    echo find results in $RESULTS
    echo find errors in $ERRORS
    echo find full log in $FULLLOG
    #ls -l $RESULTS $ERRORS $FULLLOG
}

#-----------------------------------
#
# main
#
#-----------------------------------

init $@

execute_and_report

cleanup $@

exit 0
