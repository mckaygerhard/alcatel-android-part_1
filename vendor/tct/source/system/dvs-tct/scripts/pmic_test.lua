#!/system/xbin/lua
--[[
-----------------------------------
-- This script communicates with the PMICs over the SPMI bus
-----------------------------------
]]
local dvs = require("dvs")

local function usage()
    print("This is a utility for reading/writing PMIC registers.")
    print()
    print("USAGE: ")
    print("    pmic_test.lua [-p pmic] [-u unit] [-c count] [-s] <address> [data]")
    if get_platform() == "msm8994" or get_platform() == "msm8992" then
        print('      -p    0 | pm = pm8994, \t 1 | pmi = pmi8994')
    elseif get_platform() == "msm8952" then
        print('      -p    0 | pm = pm8952, \t 1 | pmi = pmi8952')
    elseif get_platform() == "msm8974" then
        print('      -p    0 | 8941 = 8941, \t 2 | 8841 = 8841')
    end
    print("            -p defaults to 0. You can fully specify register addresses or use -p ")
    print("            to add the slave id. These addresses are equivalent: 0x25046, -ppmi 0x5046")
    print("      -c    for read only, unit count (number of unit to read). default to 1")
    print("      -u    for read only, unit size - 1, 2 or 4 (bytes). default to 1")
    print("      -s    short-form output - display multiple bytes per line (default is 1 unit per line)")
    print(" address    example 0x5046, 0x05046, or 5046, in hex always, with or without prefix 0x")
    print("    data    one of more bytes of hex data to write, examples, 0x23 0x45 46 ")
    print("            data values are in hex always, with or without prefix 0x")
    print("\nExample Usage:")
    print("pmic_test.lua c108    # read 0xc108 (PM8994 GPIO_2 status)")
    print("pmic_test.lua c146 0  # write 0x00 to 0xc146 (disable PM8994 GPIO_2)")
    print("\nSee 80-NJ117-2X*.pdf or 80-NJ118-2X*.pdf for registers/values")
    print()
    print("For backward compatibility, you can use option -r to specify address and option -l to")
    print("replace option -c. -r and address should not be used at the same time. -c and -l should")
    print("not be used at the same time. Support for -r and -l options will be removed in the future.")
    print()
end
---------------------------------------------------
-- SPMI communications for PMIC register read/write
---------------------------------------------------
local SPMIPATH = "/d/spmi/spmi-0/"
local s_fnames = { data     = SPMIPATH.."data",
    data_raw = SPMIPATH.."data_raw",
    address  = SPMIPATH.."address",
    count    = SPMIPATH.."count" }

-- spmiWrite takes 20bit "0x12345" hex register, data as hex bytes "ff 00"
-- both inputs are strings.
-- no return values as we can't tell if the write worked,
-- do a verification read to check instead.
local function spmiWrite(register, data)
    if type(register) == "number" then register = sf("0x%05x", register) end
    assert(type(register) == "string")
    if type(data) == "number" then data = sf("0x%02x", data) end
    assert(type(data) == "string")

    log("spmiWrite( %s, %s )", register, data)

    local e = fwrite(s_fnames["address"], register)
    if e then return end  -- bail, as error already reported
    return fwrite(s_fnames["data"], data)
end

-- spmiRead takes 20bit register ("0x12345") and a read length (1024)
-- returns a string of hex data prepended with register address. Will be "" for failure.
-- ("0C140          00 00 01 80 00 00 00 00 00 00 00 00 00")
local function spmiRead(register, length, raw)
    if type(register) == "number" then register = sf("0x%05x", register) end
    assert(type(register) == "string")
    assert(type(length) == "number")
    assert(length>0)

    log("spmiRead( %s, %u )", register, length)

    local e = fwrite(s_fnames["address"], register)
    if e then
        return ""
    end

    e = fwrite(s_fnames["count"], tostring(length))
    if e then
        return ""
    end

    local read = nil
    if raw then
        read = fread_all(s_fnames["data_raw"])
    else
        read = fread_all(s_fnames["data"])
    end

    if read == nil then
        return ""
    end

    return read
end

-- remove the extra spaces and address prefix from spmiRead output
local function spmiTrim( read_string )
    assert( type(read_string) == "string")
    local outstring
    for hex in read_string:gmatch("%x+") do
        if hex:len() < 3 then -- only process 2 digit values
            if outstring then
                outstring = outstring.." "..hex
        else
            outstring = hex
        end
        end
    end
    log("trimmed: %s down to :%s", read_string, outstring)
    return outstring
end

-- read single register and return value as a number
local function spmiReadRegister( register )
    return tonumber(spmiTrim(spmiRead( register, 1)), 16)
end

local function spmiGetFullAddress(slave, reg)
    local slave_nibble = 0x10000
    reg = reg + 2*slave*slave_nibble
    return reg
end


local function spmiGetPmicSlaveNo(pmic)
    if pmic == nil then
        logInf("Assuming pmic is 0, or that register is fully defined.")
        pmic =  0
    elseif type(pmic) == "string" then
        if pmic:match("pmi") then
            pmic = 1
        elseif pmic:match("8841") then
            pmic =  2
        elseif pmic:match("8941") or pmic:match("pm") then
            pmic =  0
        elseif tonumber(pmic) then
            pmic = tonumber(pmic)
            if pmic > 2 or pmic < 0 then
                logErr("pmic must be 0, 1, or 2")
                return
            end
        else
            logErr("pmic needs to be 0, 1, 2, pm, pmi, 8941 or 8841")
            return
        end
    else
        logErr("unexpected type for pmic".. type(pmic))
    end

    log ("pmic is : %d", pmic)
    return pmic
end
-----------------------------------
-- main
-----------------------------------
local function main(arg)

    local optarg, optind=dvs.init(arg, "hvsp:u:l:r:c:", usage)

    if #arg == 0 then
        usage()
        return
    end

    local pmic=optarg["p"]
    local register=optarg["r"]
    local readlen=tonumber(optarg["l"]) or tonumber(optarg["c"]) or 1
    local readunit=tonumber(optarg["u"]) or 1

    if register == nil then
        register = arg[optind]
        optind = optind + 1
    end

    if optarg.l and optarg.c then
        logErr("Legacy option -l cannot be used together with -c")
        exit()
    end

    if register == nil then
        logErr("Need an address")
        exit()
    end

    local writelen=#arg-optind+1
    local write_data
    local long_form = (optarg["s"] == nil)
    if writelen >0 then
        for i = optind, #arg do
            log("index [%s] = %s", i, arg [i])
            -- force interpretation of data as 2 digit hex (with or without 0x) - then format string with 0x
            local value = dvs.str2hex(arg[i])
            if value == nil then
                err("What is %s ?", arg[i])
                err("Need data to write in hex bytes. like: 0x00 ff a")
                exit()
            end
            if write_data == nil then
                write_data = sf("0x%02X", value) -- first time writing to write_data
            else
                write_data = sf("%s 0x%02X", write_data, value)
            end
        end
    end

    log ("")
    log_param("pmic", pmic)
    log_param("register", register)
    log_param("readlen", readlen)
    log_param("readunit", readunit)
    log_param("writelen", writelen)
    if ( writelen > 0 ) then
        log ("write data: "..write_data)
    end

    if not (writelen>0 or readlen>0) then
        err("either need data to write or -l <readlen>")
        usage()
        exit()
    end

    if readunit ~= 1 and readunit ~= 2 and readunit ~=4 then
        err("read unit must be 1, 2, or 4 (bytes)")
        usage()
        exit()
    end

    --check for inconsistent arguments
    if writelen > 0  then
        readlen = 0
    end

    pmic = spmiGetPmicSlaveNo(pmic)
    if pmic == nil then
        logErr("-p needs to be 0, 1, 2, pm, pmi, 8941 or 8841")
        exit()
    end

    local reg = dvs.str2hex(register)
    if reg == nil then
        logErr("%s does not seem to be a valid address", register)
        exit()
    end
    reg = spmiGetFullAddress(pmic, reg)

    register = sf("0x%05x", reg)
    log("register constructed with selected pmic: %s", register)

    -- write
    if writelen>0 then

        print(sf("writing SPMI register: %s with: %s", register, write_data))

        spmiWrite(register, write_data)

        -- do a verification read
        local read_data = spmiTrim(spmiRead(register, writelen))
        write_data = write_data:gsub("0x","") -- remove 0x prefixes so that we can match read_data
        log("<"..read_data..">")
        log("<"..write_data..">")
        if read_data ~= write_data then
            err("read doesn't match data just written!")
            print("wrote this :"..write_data)
            print("read back  :"..read_data)
        else
            print("success")
        end

    end

    --read
    if readlen>0 then
        --check readlength
        if readlen > 65536 then
            err("readlength > 65536")
            exit()
        end

        print(sf("reading %u byte(s) starting with SPMI register: %s", readlen*readunit, register))

        local readlen_temp = math.floor(0x100/readunit)
        if readlen_temp > readlen then readlen_temp = readlen end

        while readlen_temp > 0 do
            local read_data = spmiRead(register, readlen_temp*readunit, long_form)
            if read_data == "" then
                err("read error. nothing returned")
            end

            -- output
            if long_form then
                local err = false
                local count = 0
                local data_array = {}
                local sp,ep = 1,1
                for i = 1, readlen_temp do
                    data_array[i] = 0
                    for j = 1, readunit do
                        sp,ep = string.find(read_data, "%x%x", ep)
                        if sp == nil or ep == nil then
                            logErr("Missed some data")
                            err = true
                            break
                        end
                        local byte = tonumber(string.sub(read_data, sp, ep), 16)
                        data_array[i] = bit32.replace(data_array[i], byte, (j-1)*8, 8)
                        ep = ep + 1
                    end
                    if err == true then break end
                    count = i
                end
                memdump(data_array, readunit, count, register)
            else
                print(read_data)
            end
            register = register + readlen_temp*readunit;
            readlen = readlen - readlen_temp
            if readlen < readlen_temp then readlen_temp = readlen end
        end
    end

    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main               = main,

    spmiTrim            = spmiTrim,
    spmiWrite           = spmiWrite,
    spmiRead            = spmiRead,
    spmiReadRegister    = spmiReadRegister,
    spmiWriteRegister   = spmiWrite,
    spmiGetFullAddress  = spmiGetFullAddress,
    spmiGetPmicSlaveNo  = spmiGetPmicSlaveNo,

}

initModule()
if ... == "pmic_test" then
    return exports --library, export functions
else
    main(arg)
end
