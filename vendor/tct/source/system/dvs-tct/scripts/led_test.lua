#!/system/xbin/lua
--[[
-----------------------------------
-- This script controls the RGB leds using the debug sysfs
-----------------------------------
]]

local gDvs = require("dvs")

local rgb_leds  = { r="red", g="green", b="blue"}
local rgb_files = {}
local rgb_max

local usageStr = [[
This is a utility for testing RGB LEDs.

Usage:
    led_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported:
    c <setting=value> [...]  Set RGB LED
    f <setting=value> [...]  Flash RGB LED

sub-command c (cs, configure)
Configure RGB LED settings. The following settings and their values are supported:
r|red
    Set brightness of red LED. Value can be:
        <value>  Any value between 0 and 255
g|green
    Set brightness of green LED.
        <value>  Any value between 0 and 255
b|blue
    Set brightness of blue LED.
        <value>  Any value between 0 and 255

sub-command f (flash)
Flash RGB LED settings. The following settings and their values are supported:
r|red
        1|e|enable   enable flashing
        0|d|disable  disable flashing
g|green
        1|e|enable   enable flashing
        0|d|disable  disable flashing
b|blue
        1|e|enable   enable flashing
        0|d|disable  disable flashing

Examples
    # flash red and blue leds
    led_test.lua f r=1 blue=e
    # set brightness of green LED to 255 and red LED to 100
    led_test.lua c green=255 r=100
]]

local function usage()
    print(usageStr)
end

-- set a particular led to a certain brightness
-- example: setBrightness("blue", 255)
local function setBrightness( color, brightness )
    assert( rgb_files[color], "What color is "..color.."?")
    assert( type(brightness) == "number" )
    assert( brightness <= rgb_max )
    fwrite(rgb_files[color].."/brightness", tostring(brightness) )
end

local function setBlink(color, blink)
    assert( rgb_files[color], "What color is "..color.."?")
    assert( type(blink) == "number" )
    fwrite(rgb_files[color].."/blink", tostring(blink) )
end

local function doCmdConfigure(subargs)
    local settings = gDvs.getSettings(subargs, { {"red", "r"},  {"green", "g"}, {"blue", "b"} })

    if settings == nil then return end

    for k, s in ipairs(settings) do
        local color, val = s.n, s.v
        local value = tonumber(val)
        if value == nil then
            logErr("Wrong value %s for setting %s", val, color)
            return
        end
        if value > rgb_max  or value < 0 then
            logErr("value of %s must between 0 and %d", color, rgb_max)
            return
        end
        print("Setting "..color.." led brightness to "..value)
        setBrightness( color, value )
    end
end

local function doCmdFlash(subargs)
    local settings = gDvs.getSettings(subargs, { {"red", "r"},  {"green", "g"}, {"blue", "b"} })

    if settings == nil then return end

    for k, s in ipairs(settings) do
        local color, val = s.n, s.v
        local blinkVal
        if val == "enable" or val == "e" or val == "1" then
            blinkVal = 1
        elseif val == "disable" or val == "d" or val == "0" then
            blinkVal = 0
        else
            logErr("Wrong value %s for setting %s", val, color)
            return
        end
        print("Setting "..color.." led blink to "..blinkVal)
        setBlink( color, blinkVal )
    end
end

local function main(arg)
    local optarg, optind=gDvs.init(arg, "hvf", usage)
    if #arg == 0 then
        usage()
        return
    end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    if subcmd == "c" or subcmd == "cs" or subcmd == "configure" then
        doCmdConfigure(subargs)
    elseif subcmd == "f" or subcmd == "flash" then
        doCmdFlash(subargs)
    else
        logErr("Incorrect sub-command '%s'", subcmd)
    end

    exit()
end

local function initModule()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end

    local rgb_path="/sys/class/leds/"
    for k, v in pairs(rgb_leds) do
        rgb_files[v] = rgb_path..v
        if get_platform() == "msm8974" or get_platform() == "msm8992" then
            -- override to msm8974 specific values
            rgb_files[v] = rgb_path.."led:rgb_"..v
        end
        log("%s led is found at %s", v, rgb_files[v])
    end

    -- lookup red max brightness
    local f=assert(io.open(rgb_files["red"].."/max_brightness", "r"),
        "led files may have moved. not at "..rgb_path)
    rgb_max = assert(f:read("*number"))
    f:close()
end

local exports = {
    main          = main,
    setBrightness = setBrightness,
    setBlink      = setBlink,
}

initModule()
if ... == "led_test" then
    return exports --library, export functions
else
    main(arg)
end

