#!/system/xbin/lua
--[[
-----------------------------------
-- This script access to SIM card
-----------------------------------
]]

local gDvs = require("dvs")

local usageStr = [[
This is a utility for read or write from or to SIM card.

Usage:
    sim_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported:
    r|read                           read from SIM card and display
    w|write <times>  [data]        do <times> times writing data to SIM card

sub-command r (read)
    Read all messages from SIM card and display

sub-command w (write)
    Do multi times writing to SIM card

Examples
    sim_test.lua r                   # read all messages from SIM card
    sim_test.lua w 20                # write default data 20 time to SIM card
    sim_test.lua w 1 aaaaaa          # write aaaaaa to SIM card
]]

local SMSICC_DATAPATH = "/data/data/com.bbry.dvs.dataicc/files/"
local WRITESMSSTATUS  = SMSICC_DATAPATH.."writeSmsStatus"
local SMSICC          = SMSICC_DATAPATH.."smsicc"
local SUCCESS_FAIL = {"failed",  "Success" }

local function usage()
    print(usageStr)
end

local function getSMSLine()
    local f, e = io.open(WRITESMSSTATUS, "r")
    local count=0
    local lastline
    if f == nil or e then
        --print("Couldn't open %s for read. %s", WRITESMSSTATUS, tostring(e))
        return count
    end
    while true do
      local line = f:read("*line")
      if line == nil then
         f:close()
         break
      end
       count =count+1
       lastline=line
    end
    return count, lastline
end

local function readSMSLine()
    local f, e = io.open(SMSICC, "r")
    local count=0
    if f == nil or e then
        --print("Couldn't open %s for read. %s", WRITESMSSTATUS, tostring(e))
        return count
    end
    while true do
      local line = f:read("*line")
      if line == nil then
         f:close()
         break
      end
      count =count+1
      print("SMS "..count ..": "..line)
    end
    return count
end

-- read from SIM card
local function doCmdRead(arg)
    print("During reading, please wait few seconds!!")
    run_cmd("am broadcast -a com.bbry.dvs.action.READ_ICCDATA")
    sleep(3)
    if fexists(SMSICC) then
        local c = readSMSLine()
        print("Total SMS: "..c )
        return
    end
    --try once more
    sleep(2)
    if fexists(SMSICC) then
        local c = readSMSLine()
        print("Total SMS: "..c )
        return
    end
    print("Read SIM card failed")
end

-- write to SIM card
local function doCmdWrite(subargs)
    --assert(#subargs == 2, "Please append 'times' to sub-command")

    local times = subargs[1]
    local msg
    if  subargs[2]==nil then
       msg = "\"!!!Write data test to sim card sms field!!!\""
    else
      msg = subargs[2]
    end

    if tonumber(times) > 0 then
        print("Do \""..times.."\" times writing to SIM card")
        -- delete messages firstly
        os.remove(WRITESMSSTATUS)
        run_cmd("am broadcast -a com.bbry.dvs.action.DELETE_ICCDATA")
        print("Delete old data on sim card")
        sleep(3)

        -- send write command 1 this the interval time of write data
        run_cmd("am broadcast -a com.bbry.dvs.action.WRITE_ICCDATA -e -p "..times..",1,"..msg)  

        -- wait for WRITESMSSTATUS
        local timeout = tonumber(times) +10
        local count=0;
        local lastcount=0
        repeat
            local line
            sleep(1)
            timeout = timeout - 1
            count,line=getSMSLine()
            if(count>lastcount) then
                   local items=strsplit(line, ",")
                   if items then
                      print(" "..items[1].." write "..SUCCESS_FAIL[tonumber(items[2])+1])
                   end
            end
            lastcount=count
            if tonumber(times) == count then break end
        until timeout < 0

    else
        logErr("Wrong parameter %s", times)
    end
end

-----------------------------------
-- main
-----------------------------------
local function main(arg)

    local optarg, optind = gDvs.init(arg, "hv", usage)
    if #arg == 0 then usage() exit() end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    if subcmd == "w" or subcmd == "write" then
        doCmdWrite(subargs)
    elseif subcmd == "r" or subcmd == "read" then
        doCmdRead(subargs)
    else
        logErr("Incorrect sub-command '%s'", subcmd)
    end

    exit()
end

local exports = {
    main        = main,
}

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

initModule()
if ... == "sim_test" then
    return exports --library, export functions
else
    main(arg)
end
