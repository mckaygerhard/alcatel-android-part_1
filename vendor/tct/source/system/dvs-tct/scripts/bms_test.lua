#!/system/xbin/lua

local gDvs  = require("dvs")
local gSpmi = require("pmic_test")

local usageStr = [[
This is a utility for testing Battery/FG/Charging.

Usage:
    bms_test.lua <sub-command> [sub-command arguments]

The following sub-command are supported
    d
    c <setting=value> [...]
    r address [count]
    w address [byte1 byte2 ...]

sub-command d (ds, display)
Display status and settings of Battery/FG/Charging

sub-command c (cs, configure)
Set Battery/FG/Charging. The following settings and their values are supported
e|enable
    Enable or disable charging source. Values can be
            0|d|disable   disable both USB and DC charging
            1|e|enable    enable both USB and DC charging
            2|usb         enable USB charging only (will disable DC charging)
            3|dc          enable DC charging only (will disable USB charging)
r|rate
    Set charging rate in mA. Values can be
           <value>    Any value, subjected to minimum and maximum supported value. Also, values
                      will be rounded to the nearest supported values.
dc|dc-current_limit
    Set DC input current limit in mA. Values can be
           <value>    Same as above
usb|usb-current_limit
    Set DC input current limit in mA, Values can be
           <value>    Same as above
c|capacity
    Set the maximum charging capacity (percentage). Values can be
           <value>     Any value from 0 to 100
capacity_forever
    Set the maximum charging capacity (percentage) permanently. Values can be
           <value>   A value between 0 and 100 to specify the forever effective maximum charging
                     capacity percentage. Value 0 means never charging forever.
                     This command will set in-production-flag.
                     WARNING: once set, "no" value shown below must be used to recover. This setting
                     is sticky and there is no other way to recover (reset device or re-flashing
                     device software would not help, and setting maximum charging level to 100 is NOT
                     a valid way to recover).
           no        Recover from capacity_forever (this will clear in-production-flag as well).

sub-command r(read-fg)
Read fuel gauge SRAM. The parameters are
    address = fuel gauge SRAM address
    count   = number of byte to read

sub-command w(write-fg)
Write fuel gauge SRAM. The parameters are
    address           = fuel gauge SRAM address
    [byte1 byte2 ...] = bytes to be written

Examples
    # display status and settings of Battery/FG/Charging
    bms_test.lua d
    # enable charging, set charging rate to 1.5A, and set maximum charging capacity to 80%
    bms_test.lua c e=e r=1500 c=80
    bms_test.lua c capacity_forever=4   # never charging unless battery is lower than 4%
    bms_test.lua c capacity_forever=no  # recover from capacity_forever
    bms_test.lua r 0x595  1             # read 1 byte from fuel gauge SRAM starting at address 0x595
    bms_test.lua w 1536 12 0xfe 0x45 64 # write 4 bytes to fuel gauge SRAM starting at address 1536
]]

----------------
-- FG/BMS
----------------
local BMS_PATH       = "/sys/class/power_supply/"
local ENABLE_FILE    = BMS_PATH.."battery/charging_enabled"
local DC_ENABLE_FILE = BMS_PATH.."dc/charging_enabled"
local RATE_FILE      = BMS_PATH.."battery/constant_charge_current_max"
local USB_RATE_FILE  = BMS_PATH.."usb/current_max"
local DC_RATE_FILE   = BMS_PATH.."dc/current_max"
local CAPACITY_FILE  = BMS_PATH.."battery/capacity"

-- Queries the BMS sysfs, slurps up all files and contents.
-- Returns info as a table
local collectBmsInfo = function ( system_name )
    assert(type(system_name) == "string")
    local bms_directories={ battery=1, bms=1, dc=1, ["qpnp-dc"]=1, usb=1}
    assert(bms_directories[system_name], "unknown BMS system_name:"..system_name)
    local ignore_files = "type uevent subsystem power device"

    local return_table = {}
    local rc, out, err = run_cmd_save_output("ls "..BMS_PATH..system_name.."/")
    local files = assert(io.open(out)) -- f points to a file with the contents of BMS_PATH

    -- loop through the files within that BMS system
    while true do
        local file_name = files:read("*line") -- read a file name at a time

        if not file_name then
            -- end of output
            files:close()
            assert(os.remove(out))
            assert(os.remove(err))
            break
        end
        -- must ignore certain files
        if not ignore_files:match(file_name) then
            return_table[file_name] = fread_all(BMS_PATH..system_name.."/"..file_name)
        end
    end

    return return_table
end

-- displays the battery/bms/charging source info
-- battery, bms, usb, dc are booleans, set true which ones to dump info on.
local displayBms = function (battery, bms, usb, dc)
    local i
    if battery then
        i = collectBmsInfo("battery")
        --dump_table(i, "Battery")
        dump_list(i, "Battery")
        print()
    end
    if bms then
        i = collectBmsInfo("bms")
        --dump_table(i, "BMS")
        dump_list(i, "BMS")
        print()
    end
    if usb then
        i = collectBmsInfo("usb")
        dump_table(i, "USB")
        --dump_list(i, "USB")
        print()
    end
    if dc then
        if get_platform() == "msm8974" then
            i = collectBmsInfo("qpnp-dc")
        else
            i = collectBmsInfo("dc")
        end
        dump_table(i, "DC")
        --dump_list(i, "DC")
        print()
    end
end

local IPF_FILE              = "/nvram/nvuser/inproductionflag"
local MFG_PERSIST_PATH      = "/persist/mfg"
local BATTERY_CAPACITY_FILE = MFG_PERSIST_PATH.."/batterycapacity"

local setCapacityForever = function(level)
    -- create the dir
    if not fexists(MFG_PERSIST_PATH) then
        os.execute("mkdir "..MFG_PERSIST_PATH)
        os.execute("chown system:system "..MFG_PERSIST_PATH)
    end

    -- write the capacity to an nv file
    local tempStr = string.char(level)
    f = io.open(BATTERY_CAPACITY_FILE, "w+")
    f:write(tempStr)
    f:close()

    -- set in-production-flag (note we do not care what IPF is now)
    -- this is needed by SFI load so mfg_battmon will run
    os.execute("echo -e -n $'\001' >"..IPF_FILE)

    -- restart mfgbattmon service to use the new setting
    os.execute("stop mfgbattmon")
    os.execute("start mfgbattmon")

    print()
    printl ("WARNING:")
    printl("    Maximum charging level is set to %d%% and in-production-flag is set to 1.",level)
    printl("    You must use \"bms_test.lua c capacity_forever=no\" to recover(reset")
    printl("    device or re-flash device software cannot recover your device!).")
    print()
end

local recoverCapacityForever = function()
    os.execute("rm "..IPF_FILE)
    os.execute("rm "..BATTERY_CAPACITY_FILE)

    -- restart mfgbattmon service to use the new setting
    os.execute("stop mfgbattmon")
    os.execute("start mfgbattmon")

    -- enable charging
    os.execute(fwrite(ENABLE_FILE, sf("%d", 1), false))

    print()
    print("    As part of the cleanup, in-production-flag is forcefully cleared, charging is")
    print("    enabled, and maximum charging capacity is recovered to default.")
    print()
end

local doCmdDisplay = function(args)
    -- do real work now
    displayBms(1, 1, 1, 1)
end

local enableDcChargingOnly = function()
    if get_platform() ~= "msm8992" then
        print("Not supported yet")
        return
    end

    print("This is not supported by system software so we are writing PMIC register to")
    print("accomplish this test.")
    local configReg = gSpmi.spmiReadRegister(0x21340)
    configReg = bit32.replace(configReg, 2, 3, 2)
    gSpmi.spmiWriteRegister(0x21340, configReg)
    printl("Write 0x%x to register 0x21340", configReg)

    local readback = gSpmi.spmiReadRegister(0x21340)
    if readback ~= configReg  then
        printl("Warning. Register 0x21340: written 0x%x, read-back 0x%x ", configReg, readback)
    end
end

local setUsbCurrentLimit = function(limit)
    if get_platform() ~= "msm8992" then
        print("Not supported yet")
        return
    end

    local supportedRates = { 300, 400, 450, 475, 500, 550, 600, 650, 700, 900, 950, 1000, 1100, 1200,
        1400, 1450, 1500, 1600, 1800, 1850, 1880, 1910, 1930, 1950, 1970, 2000, 2050, 2100, 2300, 2400,
        2500, 3000}

    local value
    for i, rate in ipairs(supportedRates) do
        if rate > limit then value = i-1 break end
    end

    if value == nil then  value = 0x20
    elseif value < 1 then value = 1
    end

    printl("Set usb current limit to %d mA", supportedRates[value])

    value = value -1

    -- unlock, write and read back
    gSpmi.spmiWriteRegister(0x213D0, 0xA5)
    gSpmi.spmiWriteRegister(0x213F2, value)
    local readBack = gSpmi.spmiReadRegister(0x213F2)
    if readBack ~= value then
        printl("Warning. Register 0x213F2: written 0x%x, read-back 0x%x ", value, readBack)
    end
end

local doCmdConfigure = function(args)

    local settings = gDvs.getSettings(args,
        { {"enable", "e"},  {"rate", "r"}, {"capacity", "c"}, {"capacity_forever"},
            { "dc_current_limit", "dc"}, {"usb_current_limit", "usb"}})
    if settings == nil then return end

    local rateValue, capacityValue
    for k, s in ipairs(settings) do
        local setting, value = s.n, s.v
        if setting == "enable" then
            if value == "disable" or value == "d" or value == "0" then
                -- toggle it first to get a clean start
                fwrite(ENABLE_FILE, sf("%d", 1), false)
                fwrite(DC_ENABLE_FILE, sf("%d", 1), false)

                fwrite(ENABLE_FILE, sf("%d", 0), false)
                fwrite(DC_ENABLE_FILE, sf("%d", 0), false)
            elseif value == "enable" or value == "e" or value == "1" then
                -- toggle it first to get a clean start
                fwrite(DC_ENABLE_FILE, sf("%d", 0), false)
                fwrite(ENABLE_FILE, sf("%d", 0), false)

                fwrite(ENABLE_FILE, sf("%d", 1), false)
                fwrite(DC_ENABLE_FILE, sf("%d", 1), false)
            elseif value == "usb" or value == "2" then
                -- toggle it first to get a clean start
                fwrite(DC_ENABLE_FILE, sf("%d", 1), false)
                fwrite(ENABLE_FILE, sf("%d", 0), false)

                fwrite(ENABLE_FILE, sf("%d", 1), false)
                fwrite(DC_ENABLE_FILE, sf("%d", 0), false)
            elseif value == "dc" or value == "3" then
                -- toggle it first to get a clean start
                fwrite(DC_ENABLE_FILE, sf("%d", 0), false)
                fwrite(ENABLE_FILE, sf("%d", 0), false)

                fwrite(ENABLE_FILE, sf("%d", 1), false)
                fwrite(DC_ENABLE_FILE, sf("%d", 1), false)
                enableDcChargingOnly() -- write register directly
            else
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
        elseif setting == "rate" then
            rateValue = tonumber(value)
            if rateValue == nil then
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
            fwrite(RATE_FILE, sf("%d", rateValue*1000), false)
            printl("Set charging current limit to %d mA", fread_all(RATE_FILE)/1000)
        elseif setting == "usb_current_limit" then
            rateValue = tonumber(value)
            if rateValue == nil then
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
            setUsbCurrentLimit(rateValue)
            -- fwrite(USB_RATE_FILE, sf("%d", rateValue*1000), false) -- not supported now
        elseif setting == "dc_current_limit" then
            rateValue = tonumber(value)
            if rateValue == nil then
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
            fwrite(DC_RATE_FILE, sf("%d", rateValue*1000), false)
            printl("Set dc current limit to %d mA", fread_all(DC_RATE_FILE)/1000)
        elseif setting == "capacity" then
            capacityValue = tonumber(value)
            if capacityValue == nil  or capacityValue > 100 or capacityValue < 0 then
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
            fwrite(CAPACITY_FILE, sf("%d", capacityValue), false)
        elseif setting == "capacity_forever" then
            if value == "no" then
                recoverCapacityForever()
                return
            end
            local level = gDvs.str2num(value)
            if level == nil or level <0 or level>100 then
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
            setCapacityForever(level)
        end
    end
end

local FG_MEM_IF_PATH         = "/d/fg_memif"
local FG_MEM_IF_DATA_FILE    = FG_MEM_IF_PATH.."/data"
local FG_MEM_IF_COUNT_FILE   = FG_MEM_IF_PATH.."/count"
local FG_MEM_IF_ADDRESS_FILE = FG_MEM_IF_PATH.."/address"

local doCmdWriteFgMem = function(args)
    if #args < 2 then
        logErr("Need an address and at least one byte of data")
        return
    end

    local address = gDvs.str2num(args[1])
    if address == nil then
        logErr("need valid SRAM address")
        return
    end
    local data = table.concat(args, " ", 2, #args)
    fwrite(FG_MEM_IF_ADDRESS_FILE, sf("0x%x", address))
    fwrite(FG_MEM_IF_DATA_FILE, data)
end

local doCmdReadFgMem = function(args)
    local address = gDvs.str2num(args[1])
    if address == nil then
        logErr("need valid SRAM address")
        return
    end

    local count = 1
    if #args >= 2 then
        count = gDvs.str2num(args[2])
        if count == nil then
            logErr("wrong count %s", args[2])
            return
        end
    end

    fwrite(FG_MEM_IF_ADDRESS_FILE, sf("0x%x", address))
    fwrite(FG_MEM_IF_COUNT_FILE, sf("%d", count))
    local data = fread_all(FG_MEM_IF_DATA_FILE)
    if data then
        print(data)
    end
end

local usage = function()
    print(usageStr)
end

local main = function(arg)
    local optarg, optind = gDvs.init(arg, "hv", usage)
    if #arg == 0 then usage() exit() end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    local rc = false
    if subcmd == "d" or subcmd == "ds" or subcmd == "display" then
        doCmdDisplay(subargs)
    elseif subcmd == "c" or subcmd == "cs" or subcmd == "configure" then
        doCmdConfigure(subargs)
    elseif subcmd == "r" or subcmd == "read-fg" then
        doCmdReadFgMem(subargs)
    elseif subcmd == "w" or subcmd == "write-fg" then
        doCmdWriteFgMem(subargs)
    else
        logErr("Incorrect sub-command %s", subcmd)
    end

    exit()
end

local initModule = function()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end

    if get_platform() == "msm8974" then
        RATE_FILE = BMS_PATH.."battery/input_current_max"
    end
end

local exports = {
    main      = main,
}

initModule()
if ... == "bms_test" then
    return exports --library, export functions
else
    main(arg)
end

