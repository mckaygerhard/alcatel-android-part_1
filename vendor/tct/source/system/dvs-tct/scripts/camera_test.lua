#!/system/xbin/lua
--[[
-----------------------------------
-- This script controls cameras
-----------------------------------
]]

local gDvs = require("dvs")
local gCamflash = require("camflash_test")

local usageStr = [[
Usage:
camera_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported
    o rear|front                            turn on camera
    c                                       turn off current opened camera
    r <addr> [addr ...]                     read current opened camera register
    w <addr>=<data> [<addr>=<data> ...]     write current opened camera register
    f a|amber=<curr1> [w|white=<curr2>]     flash led for 1s

sub-command o (open)
    Turn on rear or front camera, this command MUST run firstly before others

sub-command c (close)
    Turn off current opened camera

sub-command r (read)
    Read data from address

sub-command w (write)
    Write data to address

sub-command f (flash)
    Set led current and flash for 1s

Examples:
    camera_test.lua o front                # open front camera
    camera_test.lua c                      # close current camera
    camera_test.lua r 0xeeee               # read data from 0xeeee
    camera_test.lua w 0xeeee=0x1111 0xffff=0x2222          # write 0x1111 to 0xeeee and 0x2222 to 0xffff
    camera_test.lua f w=1000               # set write led 1000mA current
    camera_test.lua f a=1000 w=500         # set amber led 1000mA current and white 500mA
]]


local CAMERA_SYSPATH      = "/sys/class/camera_device/camera_sensor/"
local SENSOR_INDEX_FILE   = CAMERA_SYSPATH.."sensor_index"
local SENSOR_ADDRESS_FILE = CAMERA_SYSPATH.."sensor_address"
local SENSOR_DATA_FILE    = CAMERA_SYSPATH.."sensor_data"

local function usage()
    print(usageStr)
end

-- turn on camera
local function doCmdOpen(subargs)
    if #subargs ~= 1 then
        logErr("Sub-commmand: o(pen) <rear|front>")
    end

    local camera = tostring(subargs[1])
    local rc, stdout, stderr
    if camera == "rear" then
        rc, stdout, stderr = run_cmd("am broadcast -a com.bbry.dvs.camera.open --ei mode 0")
    elseif camera == "front" then
        rc, stdout, stderr = run_cmd("am broadcast -a com.bbry.dvs.camera.open --ei mode 1")
    else
        logErr("Invalid camera %s", camera)
        return
    end
    if rc == nil then
        logErr("Open %s camera failed", camera)
    end
    -- set sensor_index
    if camera == "rear" then
        fwrite(SENSOR_INDEX_FILE, "0")
    else
        fwrite(SENSOR_INDEX_FILE, "1")
    end
end

-- turn off current camera
local function doCmdClose(subargs)
    local rc, stdout, stderr = run_cmd("am broadcast -a com.bbry.dvs.camera.close")
    if rc == nil then
        logErr("Close current camera failed")
    end
end

-- read data
local function doCmdRead(subargs)
    local addrs = {}
    for _, v in ipairs(subargs) do
        local addr = v:match("0[xX](%x+)")
        if addr ~= nil then
            table.insert(addrs, addr)
        end
    end
    --print(#addrs)
    if #addrs < 1 then
        logErr("Need more parameters")
        return
    end

    for _, v in ipairs(addrs) do
        fwrite(SENSOR_ADDRESS_FILE, "0x"..v)
        local data = fread_all(SENSOR_DATA_FILE)
        if data ~= nil then
            printf("0x%s=%s\n", v, data)
        end
    end
end

-- write data
local function doCmdWrite(subargs)
    local adt = {}
    for _, v in ipairs(subargs) do
        local addr, data = v:match("0[xX](%x+)=0[xX](%x+)")
        if addr ~= nil and data ~= nil then
            if gDvs.str2hex(addr) ~= nil and gDvs.str2hex(data) ~= nil then
                adt[addr] = data
            end
        end
    end

    for addr, data in pairs(adt) do
        fwrite(SENSOR_ADDRESS_FILE, "0x"..addr)
        fwrite(SENSOR_DATA_FILE, "0x"..data)
    end
end

-- flash
local function doCmdFlash(subargs)
    local lct = {}
    for _, v in ipairs(subargs) do
        local l, c = v:match("(%a+)=(%d+)")
        if l ~= nil and c ~= nil then
            local led = string.lower(l)
            local curr = tonumber(c)
            if curr ~= nil then
                if led == "amber" or led == "a" then
                    lct["f0"] = curr
                elseif led == "write" or led == "w" then
                    lct["f1"] = curr
                else
                    logErr("Unknown led name %s", l)
                end
            else
                logErr("Invalid current %s", c)
            end
        end
    end

    gCamflash.camflash_init()

    -- do real work now
    for k, v in pairs(lct) do
        log("k %s, v %d", k, v)
        local this_led_current = math.min(v, gCamflash.get_max_current(k))
        print("Setting "..k.." led brightness to "..this_led_current)
        gCamflash.set_flash_current(k, this_led_current)
    end
    -- now turn on
    gCamflash.led_switch(1)
    sleep(1)  -- don't turn off immediately
    -- now turn off
    gCamflash.led_switch(0)
    for k, v in pairs(lct) do
        print("Turning off "..k)
        gCamflash.set_flash_current(k, 0)
    end
end

local function main(arg)

    local optarg, optind = gDvs.init(arg, "hv", usage)

    local subcmd = arg[optind]
    if subcmd == nil then
        usage()
        exit()
    end

    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    if subcmd == "o" or subcmd == "open" then
        doCmdOpen(subargs)
    elseif subcmd == "c" or subcmd == "close" then
        doCmdClose(subargs)
    elseif subcmd == "r" or subcmd == "read" then
        doCmdRead(subargs)
    elseif subcmd == "w" or subcmd == "write" then
        doCmdWrite(subargs)
    elseif subcmd == "f" or subcmd == "flash" then
        doCmdFlash(subargs)
    else
        logErr("Wrong parameter %s", arg[optind])
    end
    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main                = main,
}

initModule()
if ... == "camera_test" then
    return exports --library, export functions
else
    main(arg)
end
