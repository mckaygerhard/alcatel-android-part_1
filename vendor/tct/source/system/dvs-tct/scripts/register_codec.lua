#!/system/xbin/lua

--feature_debug = 1
local gDvs = require("dvs")

local print, sf, printf = print, sf, printf

--[[
Construct a register descriptor table based on a register description string or a file (.rd file).
A register description string/file can describe one or many registers. So this function return an array of
register descriptors. A register descriptor has the following form
    { parent,name, sname, mname, gname, offset, length, ftable = {}}
        name   - the name of the register,
        sname  - the short name of this register,
        mname  - the name of medium length,
        gname  - the group name of this register
        offset - the address offset of the register,
        length - the number of bits this register has, can be 8, 16 or 32, default to 8
        ftable - field description table
        parent - the register descriptor table

A register may have one or more fields. So a register descriptor includes an array of
field descriptors. A filed descriptor has the following form:
    { parent, name, sname, mname, sb, eb, rw, vtable = {}}
        name   - the name of the field,
        sname  - the short name of this field
        mname  - the name of medium length
        sb     - the start bit of the field
        sb     - the end bit of the field
        rw     - read write flag of this register can be one of c (constant), rw (read and write),
                 r(read), w(write)
        vtable - value descriptor table
        parent - the register descriptor

A filed may have one or more allowed values. So a field descriptor includes an array of value
descriptors. A value descriptor has the following form:
    { parent, name, mname, v}
        name   - the name of this value,
        sname  - the short name of this value
        mname  - the name of medium length
        v      - the value
        parent - the field descriptor

A register description string/file can have the following kinds of lines:
  - Group(g) line, with the following syntax:
          g;<group_name>

  - Register(r) line, with the following syntax
        r;<register_name>;<register_address_offset>;[short name]

  - Field(f) line, with the following syntax
       f;<field_name>;<field_start_bit>;<field_end_bit>;[short name]

  - Value(v) line, with the following syntax
       v;<value_name>;<value_number>;[short name]

   - Comment(#) line, starting with #

   - Emptiy line.

    Leading and trailing white spaces are allowed.
]]

-- return a register descriptor table

local function buildRtableFromStr(str, group)

    local rtable = {}

    local cur_group = nil
    local started;
    if group == nil then
        started = true
    else
        started = false
    end

    local rdesc
    local fdesc
    local group_count = 0
    for line in  string.gmatch(str, "[^\n]*") do
        line = strtrim(line)
        --print(line)
        if line:sub(1,1) == "#" then
        -- do nothing for comment
        elseif started == false then
            local items = strsplit(line, ";")
            if items and items[1] == "g" then
                cur_group = items[2]
                group_count = group_count + 1
                if type(group) == "number" then
                    if group_count == group then started = true end
                elseif group == cur_group then started = true
                end
            end
        else
            local items = strsplit(line, ";")
            if items then
                if(items[1] =="r") then
                    rdesc = {name = items[2], offset = gDvs.str2num(items[3]), sname = items[4],
                        gname = cur_group, ftable={}}
                    table.insert(rtable, rdesc)
                elseif(items[1] =="f") then
                    fdesc = { name = items[2], sb = gDvs.str2num(items[3]), eb = gDvs.str2num(items[4]),
                        rw = items[5], sname = items[6], vtable = {}, parent = rdesc}
                    if fdesc.sb > fdesc.eb then
                        local temp = fdesc.eb
                        fdesc.eb = fdesc.sb
                        fdesc.sb = temp
                    end
                    table.insert(rdesc.ftable, fdesc)
                elseif(items[1] =="v") then
                    local vdesc = { name = items[2], v = gDvs.str2num(items[3]), sname = items[4], parent = fdesc}
                    table.insert(fdesc.vtable, vdesc)
                elseif items[1] =="g" then
                    if group ~= nil then break  -- If only look for group_name, done
                    else cur_group = items[2]        -- Otherwise, continue
                    end
                else
                    logErr("Wrong line %s", line)
                end
            end   --if items
        end -- if
    end -- for

    return rtable
end

local function buildRtableFromFile(fileName, blockName)
    local file = assert(io.open(fileName, "r"))
    local str = file:read("*all");
    file:close()
    return buildRtableFromStr(str, blockName)
end

local function extractField(fdesc, register_code)
    return bit32.extract(register_code, fdesc.sb, fdesc.eb - fdesc.sb + 1)
end

local function replaceField(fdesc, reg_code, field_code)
    return bit32.replace(reg_code, field_code, fdesc.sb, fdesc.eb -fdesc.sb + 1)
end

-- return the code for the field
-- return the field_code and if reg_code is not nil, return the new reg_code as well
local function encodeField(fdesc, value_name, reg_code)
    local field_code = gDvs.str2num(value_name)
    if field_code then
        local maxValue = bit32.extract(0xffffffff, fdesc.sb, fdesc.eb - fdesc.sb + 1)
        if field_code > maxValue or field_code < 0 then
            logErr("Invalid field value %s for field %s in register %s", value_name,
                fdesc.name, fdesc.parent.name)
            return nil
        end
    else
        for i, vdesc in ipairs(fdesc.vtable) do
            if vdesc.name == value_name then field_code = vdesc.v break end
        end

        if field_code == nil then
            for i, vdesc in ipairs(fdesc.vtable) do
                if vdesc.sname == value_name then field_code = vdesc.v break end
            end
        end

        if field_code == nil then
            logErr("wrong value %s for %s", value_name, fdesc.name)
            return
        end
    end

    if reg_code ~= nil then
        reg_code = replaceField(fdesc, reg_code, field_code)
    else
        logErr("Do not understand value %s for field %s in register %s", value_name,
            fdesc.name, fdesc.parent.name)
    end
    return field_code, reg_code
end

local function decodeField(fdesc, field_code)
    -- if field_code is not within range, return "invalid" value item
    local maxValue = bit32.extract(0xffffffff, fdesc.sb, fdesc.eb - fdesc.sb + 1)
    if field_code > maxValue or field_code < 0 then
        local vdesc = { name = "invalid", v = field_code, sname="nil", parent = fdesc}
        return vdesc
    end

    for i, vdesc in ipairs(fdesc.vtable) do
        if vdesc.v == field_code then return vdesc end
    end

    local valueStr= sf("%d", field_code)
    local vdesc = { name = valueStr, v = field_code, sname=valueStr, parent = fdesc}
    return vdesc
end

local function decodeRegister(rdesc, register_code)
    local vtable= {}
    for i, fdesc in ipairs(rdesc.ftable) do
        local field_code = extractField(fdesc, register_code)
        table.insert(vtable, decodeField(fdesc, field_code))
    end
    return vtable
end

local function getAllFields(rtable, rw)
    local ftable = {}
    for  i, rdesc in ipairs(rtable) do
        for  j, fdesc in ipairs(rdesc.ftable) do
            if fdesc.rw == rw then
                table.insert(ftable, fdesc)
            end
        end
    end

    logDbg("getAllFields")
    for i, fdesc in ipairs(ftable) do
        logDbg(fdesc.name)
    end

    return ftable
end

local function getMatchRules(match)
    local temp = {}

    if match == nil then
        temp.name = true
        return temp
    end

    for i=1, match:len() do
        local char = string.sub(match, i, i)
        if     char == "n" or char == "a" then temp.name  = true
        elseif char == "m" or char == "a" then temp.mname = true
        elseif char == "s" or char == "a" then temp.sname = true
        else logErr("Wrong match string %s", match)
        end
    end

    return temp
end

-- if match is nil, then match the full name only
-- otherwise, it has is a string indicating the names to be match
-- the full name if "n" is in the string
-- match the short name if "s" is in the string
-- match the median short name if "m" is in the string
-- match all names if "a" is in the string
local function getRegister(rtable, reg_name, match)
    match = getMatchRules(match)

    -- try to match the full name first
    if match == nil or match.name ~= nil or match.all ~= nil then
        for  i, rdesc in ipairs(rtable) do
            if rdesc.name == reg_name then
                return rdesc
            end
        end
    end

    -- then match the medium short name
    if match.mname ~= nil or match.all ~= nil then
        for  i, rdesc in ipairs(rtable) do
            if rdesc.mname == reg_name then
                return rdesc
            end
        end
    end

    -- then match the short name
    if match.sname ~= nil or match.all ~= nil then
        for  i, rdesc in ipairs(rtable) do
            if rdesc.sname == reg_name then
                return rdesc
            end
        end
    end
end

-- if match is nil, then match the full name only
-- otherwise, it has to be an array indicating the names to be match
-- match the full name if match[name] is not nil
-- match the short name if match[sname] is not nil
-- match the medium short name if match[mname] is not nil
-- match all names if match[all] is not nil
local function getField(ftable, field_name, match)
    match = getMatchRules(match)

    -- try to match the full name first
    if match == nil or match.name ~= nil or match.all ~= nil then
        for  i, fdesc in ipairs(ftable) do
            if fdesc.name == field_name then
                return fdesc
            end
        end
    end

    -- then match the medium short name
    if match.mname ~= nil or match.all ~= nil then
        for  i, fdesc in ipairs(ftable) do
            if fdesc.mname == field_name then
                return fdesc
            end
        end
    end

    -- then match the short name
    if match.sname ~= nil or match.all ~= nil then
        for  i, fdesc in ipairs(ftable) do
            if fdesc.sname == field_name then
                return fdesc
            end
        end
    end
end

--  This function finds the first register that has a field matches field_name.
--  Obviously, this function does not care whether multiple fields (in same register or across
--  several registers) have field with same name
--  If found, return a register description and a field description owned by the register
local function getRegisterByField(rtable, field_name, match)
    match = getMatchRules(match)

    logDbg("getRegisterByField field = %s", field_name)
    -- try to match the full name first
    if match == nil or match.name ~= nil or match.all ~= nil then
        for  i, rdesc in ipairs(rtable) do
            local fdesc = getField(rdesc.ftable, field_name, "n")
            if fdesc ~= nil then
                logDbg("found register %s", rdesc.name)
                return rdesc, fdesc
            end
        end
    end

    -- then match the medium short name
    if match.mname ~= nil or match.all ~= nil then
        for  i, rdesc in ipairs(rtable) do
            local fdesc = getField(rdesc.ftable, field_name, "m")
            if fdesc ~= nil then
                logDbg("found register %s", rdesc.name)
                return rdesc, fdesc
            end
        end
    end

    -- then match the short name
    if match.sname ~= nil or match.all ~= nil then
        for  i, rdesc in ipairs(rtable) do
            local fdesc = getField(rdesc.ftable, field_name, "s")
            if fdesc ~= nil then
                logDbg("found register %s", rdesc.name)
                return rdesc, fdesc
            end
        end
    end
end


-- A device function consists a group of registers. The following functions prefixed with "dfunc"
-- are common procedures for list, display and configure device functions. Those functions ("dfunc"
-- functions) allow users to use device function instead of individual registers as the basic unit
-- to control a device function.

local dfuncSupportedFids
local dfuncSupportedNameInfo
local dfuncGetFinfo
local dfuncWriteRegister
local dfuncReadRegister
local dfuncRtable       -- current loaded register descriptors
local dfuncCurGroup     -- keep track of current group to avoid loading loaded register descriptors

local dfuncLoadRtable = function (finfo)
    local fid = finfo.fid
    if dfuncCurGroup ~= fid.group then
        dfuncRtable = buildRtableFromFile(finfo.rdfile, fid.group)
        if dfuncRtable == nil or #dfuncRtable == 0 then
            logErr("No registers found for function %s", fid.name)
            dfuncCurGroup = nil
            dfuncRtable = nil
        else
            dfuncCurGroup = fid.group
        end
    end
    return dfuncRtable
end

local dfuncRemoveDuplicatedFids = function(rawFids)
    local fids = {}
    local names = {}
    for i, fid in ipairs(rawFids) do
        if names[fid.name] == nil then
            table.insert(fids, fid)
            names[fid.name] = fid
        end
    end
    return fids
end

local dfuncFindFunctions = function(name_pattern)
    -- an embedded function
    local matchFunctions = function(name_pattern)
        local fids = {}
        for i, fid in ipairs(dfuncSupportedFids) do
            if fid.name:match("^"..name_pattern.."$") then
                table.insert(fids, fid)
            elseif fid.sname:match("^"..name_pattern.."$") then
                table.insert(fids, fid)
            end
        end
        return fids
    end

    -- we support * (match all), ?(match one character) and - (to specify a range)
    local namep = string.gsub(name_pattern, "%*", ".-")
    namep = string.gsub(namep, "%?", ".")

    -- Quite some efforts to support range (-) (maybe there is a way that lua string can handle this?)
    -- Only handle the first range (-) in the pattern
    local s, e = string.find(namep, "%d+%-%d+")
    local rawFids = {}
    if s ~= nil and e ~= nil then
        local rangeStr = string.sub(namep, s, e)
        local items = strsplit(rangeStr,"-")
        local rstart, rend = tonumber(items[1]), tonumber(items[2])
        if rstart == nil or rend == nil then
            logErr("wrong name %s", name_pattern)
            return
        end

        local step = 1
        if rend < rstart then step = -1 end

        for i=rstart, rend, step do
            local tempname = string.gsub(namep,"%d+%-%d+", tostring(i))
            local tempFids = matchFunctions(tempname)
            for k,fid in ipairs(tempFids) do table.insert(rawFids, fid) end
        end
    else
        local tempFids = matchFunctions(namep)
        for k,fid in ipairs(tempFids) do table.insert(rawFids, fid) end
    end

    return dfuncRemoveDuplicatedFids(rawFids)
end

local dfuncGetSummary = function (fid)
    local finfo = dfuncGetFinfo(fid.name)
    if finfo == nil then return end
    if not dfuncLoadRtable(finfo) then return end

    local summary = {fid.name}
    local values = {}
    for i, field in ipairs(finfo.summary) do
        local rdesc, fdesc = getRegisterByField(dfuncRtable, field.fname)
        local address =  finfo.base + rdesc.offset

        -- dfuncReadRegister can be very slow, read from cache if available
        local reg_code = values[address]
        if reg_code  == nil then
            reg_code = dfuncReadRegister(address)
            values[address] = reg_code
        end

        local field_code = extractField(fdesc, reg_code)
        local vdesc      = decodeField(fdesc, field_code)
        local v_name = vdesc.sname or vdesc.name
        local dname = field.fdname or field.fname
        table.insert(summary, sf("%s(%s)",dname,v_name))
    end
    return summary
end

local dfuncDisplayOneFunction = function(fid)
    local finfo = dfuncGetFinfo(fid.name)
    if finfo == nil then return end

    if not dfuncLoadRtable(finfo) then return end

    printf("\nDetailed description for:  %s\n", fid.name)
    for i, rdesc in ipairs(dfuncRtable) do
        local address = finfo.base + rdesc.offset
        local reg_code = dfuncReadRegister(address)
        local vtable   = decodeRegister(rdesc,reg_code)
        printf("  0x%x:0x%02x %s\n", address, reg_code, rdesc.name)
        -- printf("%s\n", rdesc.name)
        for j, vdesc in ipairs(vtable) do
            printf("               bit %d-%d (%-20s) = %d (%s)\n",
                vdesc.parent.sb, vdesc.parent.eb, vdesc.parent.name, vdesc.v, vdesc.name)
        end
    end
end

local dfuncDoCmdDisplay = function (subargs)
    local args
    local displayMode

    args = subargs
    if args[1] == "-s" or args[1] == "-d" then
        displayMode = args[1]
        table.remove(args, 1)
    end

    if #args == 0 or args == nil then
        args = {"*"}
    end

    local fids = {}
    for i, arg in ipairs(args) do
        for i, fid in ipairs(dfuncFindFunctions(arg)) do
            table.insert(fids, fid)
        end
    end

    fids = dfuncRemoveDuplicatedFids(fids)

    if fids == nil or #fids == 0 then
        logErr("No matching function")
        return
    end

    if displayMode == nil then
        if #fids == 1 then
            displayMode = "-d"
        else
            displayMode = "-s"
        end
    end

    local summaries = {}
    for i, fid in ipairs(fids) do
        if displayMode == "-s" then
            table.insert(summaries, dfuncGetSummary(fid))
        else
            dfuncDisplayOneFunction(fid)
        end
    end
    gDvs.printAlignedTable(summaries)
end

local listSettings = function(rtable)
    local ftable = getAllFields(rtable, "rw")
    print("Supported settings and their values are:")
    for i, fdesc in ipairs(ftable) do
        printf("\t")
        if fdesc.sname then printf("%s|", fdesc.sname) end
        printf("%s\n", fdesc.name)
        for j, vdesc in ipairs(fdesc.vtable) do
            printf("\t\t")
            if vdesc.v then printf("%s|", vdesc.v) end
            if vdesc.sname then printf("%s|", vdesc.sname) end
            printf("%s\n", vdesc.name)
        end
    end
end

local dfuncDoCmdConfigure = function(args)
    local name = args[1]
    local finfo = dfuncGetFinfo(name)
    if finfo == nil then return end

    if not dfuncLoadRtable(finfo) then return end

    if #args == 1 then
        listSettings(dfuncRtable)
        return
    end

    local codeTable={}
    local orderedCodeTable = {}
    for i=2, #args do
        local arg = args[i]
        local items = strsplit(arg, "=")
        local field_name, value_name = items[1], items[2]
        local rdesc, fdesc = getRegisterByField(dfuncRtable, field_name, "ns")
        if rdesc == nil or fdesc == nil then
            logErr("Unknown setting %s in %s", field_name, arg)
            return
        end

        -- read existing value
        if codeTable[rdesc.name] == nil then
            local address = finfo.base + rdesc.offset
            codeTable[rdesc.name] = { code = dfuncReadRegister(address), address = address}
            table.insert(orderedCodeTable, codeTable[rdesc.name])
        end

        logInf("Updating: register %s, field %s, register_code 0x%x",
            rdesc.name, fdesc.name, codeTable[rdesc.name].code)

        local field_code
        field_code, codeTable[rdesc.name].code = encodeField(fdesc, value_name, codeTable[rdesc.name].code)

        if field_code == nil then
            logErr("Unknown value %s in %s", value_name, arg)
            return
        end

        logInf("Updated: register %s, field %s, register_code 0x%x, field_code 0x%x  field bit (%d-%d)",
            rdesc.name, fdesc.name, codeTable[rdesc.name].code, field_code, fdesc.eb, fdesc.sb)

        printl("\tset %3d(%-20s) to %-20s of register %s", field_code, decodeField(
            fdesc, field_code).name, fdesc.name, rdesc.name)
    end

    for i, code_item in ipairs(orderedCodeTable) do
        dfuncWriteRegister(code_item.address, code_item.code)
        printl("\tWrite 0x%02x to address 0x%x", code_item.code, code_item.address)
        local readback = dfuncReadRegister(code_item.address)
        if readback ~=  code_item.code then
            printl("\tWhat read back (0x%x) is not the same as what written.", readback)
        end
    end
end

local dfuncDoCmdList = function (args)
    print()
    printf("Supported functions are:\n")
    for i, str in ipairs(dfuncSupportedNameInfo) do
        print(str)
    end
    print()
end

--[[ dfuncInit
supportedNameInfo consists a table of strings describing supported function names
supportedFids a table of supported function identifiers (FIDs), a FID must have the following fields
      name  - the name of the function
      sname - the short name of the function group
      group - the register group of the function
getFinfo must be a function  with the following synax
    function(fid).  It returns a FINFO struture as described below:
    Function Information structure (FINFO), which must have the follow fields
      fid - the FID
      rdfile - the full name of the RD file for the register group
      base   - the base address of the register group
      summary - a table consists of one or multiple {fname, fdname} tables
           fname  - the filed name of a register to display
           fdname - the display name of the field (can be nil)
           Only fields listed in summary will be display in summary display.
readRegister, a function to read one register. It has the following syntax
    function(address). It returns the value of the register
writeRegister,a function to write one register. It has the following syntax
    function(address, value)
]]
local dfuncInit = function (supportedNameInfo, supportedFids, getFinfo, readRegister, writeRegister)
    dfuncSupportedNameInfo = supportedNameInfo
    dfuncSupportedFids     = supportedFids
    dfuncGetFinfo   = getFinfo
    dfuncReadRegister = readRegister
    dfuncWriteRegister = writeRegister
    dfuncRtable = nil
    dfuncCurGroup = nil
end

local dfuncMain = function (arg)
    local subcmd = arg[1]
    local subargs = gDvs.slice(arg, 2)
    if subcmd == "d" or subcmd == "ds" or subcmd == "display" then
        dfuncDoCmdDisplay(subargs)
    elseif subcmd == "c" or subcmd == "cs" or subcmd == "configure" then
        dfuncDoCmdConfigure(subargs)
    elseif subcmd == "l"  or subcmd == "ls" or subcmd == "list" then
        dfuncDoCmdList(subargs)
    else
        logErr("Incorrect subcmd %s", subcmd)
    end
end

local function main(arg)
    if arg[1] == nil then
        printl("Usage: %s <regiser description file>", arg[0])
        exit()
    end

    local rtable = {}
    local group_count = 1
    repeat
        rtable = buildRtableFromFile(arg[1], group_count)

        -- printout the table (If fails something is wrong)
        for i, rdesc in ipairs(rtable) do
            if(rdesc.gname) then printf("%s", rdesc.gname) end
            printf(":%s", rdesc.name)
            if(rdesc.sname) then printf(" sname=%s", rdesc.sname) end
            if(rdesc.mname) then printf(" mname=%s", rdesc.sname) end
            if(rdesc.offset) then printf(" offset=0x%x", rdesc.offset) end
            printf("\n")
            for j, fdesc in ipairs(rdesc.ftable) do
                printf("    %s (bit %d-%d)", fdesc.name, fdesc.sb, fdesc.eb)
                if(fdesc.sname) then printf(" sname=%s", fdesc.sname) end
                if(fdesc.mname) then printf(" mname=%s", fdesc.sname) end
                printf("\n")
                for j, vdesc in ipairs(fdesc.vtable) do
                    printf("        %02d %s ", vdesc.v, vdesc.name)
                    if(vdesc.sname) then printf(" sname=%s", vdesc.sname) end
                    if(vdesc.mname) then printf(" mname=%s", vdesc.sname) end
                    printf("\n")
                end
            end
            print()
        end

        -- Registers must have different names
        local names = {}
        for i, rdesc in ipairs(rtable) do
            if names[rdesc.name] then
                logErr("multiple register with the same name %s", rdesc.name)
            else names[rdesc.name] = rdesc
            end

            if rdesc.sname and names[rdesc.sname] then
                logErr(" register %s has the same short name (%s) with register %s", rdesc.name,
                    rdesc.sname, names[rdesc.sname].name)
            elseif rdesc.sname then names[rdesc.sname] = rdesc
            end

            if rdesc.mname and names[rdesc.mname] then
                logErr(" register %s has the same median name (%s) with register %s", rdesc.name,
                    rdesc.mname, names[rdesc.mname].name)
            elseif rdesc.mname then names[rdesc.mname] = rdesc
            end
        end

        -- Registers must have different offset
        local offsets = {}
        for i, rdesc in ipairs(rtable) do
            if rdesc.offset == nil then
                logErr("%s must have a offset", rdesc.name)
            elseif offsets[rdesc.offset] then
                logErr(" register %s has the same offset(%d) with register %s", rdesc.name,
                    rdesc.offset, offsets[rdesc.offset].name)
            else offsets[rdesc.offset] = rdesc
            end
        end

        --writable fields must all be different name
        names = {}
        for i, rdesc in ipairs(rtable) do
            for j, fdesc in ipairs(rdesc.ftable) do
                if fdesc.rw ~= nil and fdesc.rw ~= "rw" and fdesc.rw ~= "r" and fdesc.rw ~= "w" then
                    logErr("register %s, field %s invalid rw flag (%s)", rdesc.name, fdesc.name, fdesc.rw)
                end
                if fdesc.rw == "rw" or fdesc == "w" then
                    if names[fdesc.name] then
                        logErr("register %s, field name %s reused", rdesc.name, fdesc.name)
                    else names[fdesc.name] = fdesc
                    end

                    if fdesc.sname and names[fdesc.sname] then
                        logErr("register %s, field short name %s  reused", rdesc.name, fdesc.sname)
                    elseif fdesc.sname then names[fdesc.sname] = fdesc
                    end

                    if fdesc.mname and names[fdesc.mname] then
                        logErr("register %s, field median name %s reused", rdesc.name, fdesc.mname)
                    elseif fdesc.mname then names[fdesc.mname] = fdesc
                    end
                end
            end
        end

        -- field must have valid start bit and end bit

        for i, rdesc in ipairs(rtable) do
            local bits={}
            for j, fdesc in ipairs(rdesc.ftable) do
                if fdesc.sb == nil or fdesc.eb == nil then
                    logErr( "start bit or end bit missed in field %s register %s", fdesc.name, rdesc.name)
                elseif fdesc.sb > fdesc.eb then
                    logErr( "start bit <= end bit in field %s register %s", fdesc.name, rdesc.name)
                end

                for k = fdesc.sb, fdesc.eb do
                    if bits[k] then
                        logErr("bit %d reused in %s in %s in %s", fdesc.name, rdesc.name)
                    else bits[k] = fdesc
                    end
                end
            end
        end

        -- values in vtable must have valid values and  have different names
        for i, rdesc in ipairs(rtable) do
            for j, fdesc in ipairs(rdesc.ftable) do
                local values = {}
                local names = {}
                for k, value in ipairs(fdesc.vtable) do
                    if value.v == nil or value.name == nil then
                        logErr("wrong value in %s in %s", fdesc.name, rdesc.name)
                    elseif values[value.v] then
                        logErr("value %d reused in %s in %s in %s", value.v, value.name,
                            fdesc.name, rdesc.name)
                    elseif names[value.name] then
                        logErr(" name %s reused in %s in %s", value.name,
                            fdesc.name, rdesc.name)
                    elseif value.sname and names[value.sname] then
                        logErr(" sname %s reused in %s in %s", value.sname,
                            fdesc.name, rdesc.name)
                    elseif value.mname and names[value.mname] then
                        logErr(" mname %s reused in %s in %s", value.mname,
                            fdesc.mname, rdesc.name)
                    else
                        values[value.v] = value
                        names[value.name] = value
                        if value.sname then names[value.sname] = value end
                        if value.mname then names[value.mname] = value end
                    end
                end
            end
        end
        group_count = group_count + 1
    until #rtable == 0

    exit()
end

local exports = {
    buildRtableFromFile   = buildRtableFromFile,

    replaceField          = replaceField,
    extractField          = extractField,

    encodeField           = encodeField,
    decodeField           = decodeField,
    decodeRegister        = decodeRegister,

    getAllFields          = getAllFields,
    getRegister           = getRegister,
    getRegisterByField    = getRegisterByField,
    getField              = getField,

    -- device function related procedures
    dfuncMain           =  dfuncMain,
    dfuncInit           =  dfuncInit,
    dfuncDoCmdList      = dfuncDoCmdList,
    dfuncDoCmdConfigure = dfuncDoCmdConfigure,
    dfuncDoCmdDisplay   = dfuncDoCmdDisplay,
}

if ... == "register_codec" then
    return exports --library, export functions
else
    main(arg)
end


