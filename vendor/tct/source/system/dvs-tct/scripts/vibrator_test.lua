#!/system/xbin/lua
--[[
-----------------------------------
-- This script controls the vibrator
-----------------------------------
]]

local gDvs = require("dvs")


local usageStr = [[
This is a utility for turning the vibrator on/off.

Usage:
    vibrator_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported:
    c <setting=value> [...]  configure vibrator

sub-command c (cs, configure)
Configure vibrator settings. The following settings and their values are supported:
t|time
    Turn vibrator on.
        <value>  time in milliseconds (max 15000)
                 Note: To protect the hardware, the driver may limit the maximum enable time to 15 seconds
i|intensity
    Set vibrator intensity percentage.
        <value>  Any value between 0 and 100
                 Note: Vibrator intensity setting is reset back to 100% after the vibrator stops.

Examples
    # turn on vibrator for 10 seconds
    vibrator_test.lua c t=10000
    # set intensity to 50%
    vibrator_test.lua c i=50
    # turn on vibrator for 5 seconds at 50% intensity
    vibrator_test.lua c t=5000 i=50
]]

local function usage()
    print(usageStr)
end

-- set vibrator
local function setVibrator(intensity, time)
    local vibrator_path="/sys/class/timed_output/vibrator/"

    local result
    -- have to set intensity first
    -- if vibrator is already on, changing intensity has no effect until next time
    if intensity ~= nil then
        printf("Setting vibrate intensity to %s%%\n", intensity)
        result = fwrite(vibrator_path.."intensity", tostring(intensity))
        if result ~= nil then
           printf("\n==============================================================\n")
           printf("\n=== Setting vibrate intensity not support on this hardware ===\n")
           printf("\n==============================================================\n")
           return
        end
    end
    if time ~= nil then
        printf("Setting vibrate time to %smS\n", time)
        if time > 15000 then
            printf("  To protect the hardware, the driver may limit the maximum enable time to 15 seconds\n")
        end
        fwrite(vibrator_path.."enable", tostring(time))
    end
end

-- configure vibrator
local function doCmdConfigure(subargs)
    local settings = gDvs.getSettings(subargs, { {"time", "t"},  {"intensity", "i"} })
    local time, intensity

    if settings == nil then return end

    for k, s in ipairs(settings) do
        local setting, val = s.n, s.v
        if setting == "time" then
            local value = tonumber(val)
            if value == nil or value < 0 then
                logErr("Wrong value %s for setting %s", val, setting)
                return
            end
            -- if vibrator is on, setting intensity has no effect until next time
            -- so have to save the values and write them at the same time
            time = value
        elseif setting == "intensity" then
            local value = tonumber(val)
            if value == nil or value < 0 or value > 100 then
                logErr("Wrong value %s for setting %s", val, setting)
                return
            end
            intensity = value
        end
    end
    setVibrator(intensity, time)
end

local function main(arg)
    local optarg, optind=gDvs.init(arg, "hvf", usage)
    if #arg == 0 then
        usage()
        return
    end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    if subcmd == "c" or subcmd == "cs" or subcmd == "configure" then
        doCmdConfigure(subargs)
    else
        logErr("Incorrect sub-command '%s'", subcmd)
    end

    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main        = main,
    setVibrator = setVibrator,
}

initModule()
if ... == "vibrator_test" then
    return exports --library, export functions
else
    main(arg)
end

