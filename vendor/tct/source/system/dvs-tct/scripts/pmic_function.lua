#!/system/xbin/lua
--[[
-----------------------------------
-- This script attempts to control/display PMIC functions
-- The following two functions have platform dependency:
--         populateSupportedNames and
--         getFunctionInfo
-----------------------------------
]]
local gDvs      = require("dvs")
local gSpmi     = require("pmic_test")
local gRegCodec = require("register_codec")

local print, sf, printf = print, sf, printf

local gRdPath = "/system/etc/dvs/"
local gSupportedNames = {} -- supported FID (function identifiers) indexed by function name

local getrdfile = function(fid)
    printf("Not Support %s",get_platform())
end

local getFinfo = function (name)
    local fid = gSupportedNames[name]
    if fid == nil then
        logErr("function %s is not supported", name)
        return
    end
    return getrdfile(fid)
end

-- populates gSupportedNames, gSupportedFids and gSupportedNameInfo
local populateSupportedNames = function()
     printf("Not Support %s",get_platform())
end



local usageStr =[[
Usage:
pmic_function.lua <sub-command> [sub-command arguments]

This script has intelligence of the PMIC registers related to certain PMIC functions(such as GPIOs,
MPPs, and regulators). Users of this scripts can rely on the script to find the register addresses,
read and write registers' data fields related to supported PMIC functions.

The following sub-command are supported
    l                                          list supported functions
    c <function_name>  <setting=value> [...]   configure and set the function
    d [-s|-d] [function_name ...]              display status and settings of the function(s)

sub-command l (ls, list)
    List the full names and short names of supported functions.

sub-command c (cs, configure)
    sub-command c takes a function name and zero or more <setting=value>
    if no <setting=value> is given, this sub-command displays all supported settings and values of
    the given function.

sub-command d (ds, display)
    Display the settings and status of one or more functions.
    Zero or more function_name_pattern are allowed. A function_name can be an exact function name,
    or a pattern to match function names. Use * to match many characters (such as gpio*), use ? to
    match one character, and - to match a range (such as ldo4-5 and gpio1-20).
    Without any function_name, all supported functions will be displayed.
    By default
      single function name will display detailed information: all registers associated with the function name will be displayed
      multiple function names (or wildcards) will display summary information: limited summary registers are displayed
    The -s and -d options will override the default display mode
      -s will only display summary information
      -d will only display detailed information

Examples:
    pmic_function.lua    l    # list names of supported functions
    pmic_function.lua d  ps1   # display VREG_S1 of the platform PMIC, which is pmi8994_s1 on msm8992
                               # platform
    pmic_function.lua d  "gpio*"  # display all gpio
    pmic_function.lua c   g1 e=1 m=io # enable GPIO 1 of the MSM PMIC, set it as input and output
]]
local usage = function()
    printf(usageStr)
end

local main = function (arg)
    local optarg, optind = gDvs.init(arg, "hv", usage)
    if #arg == 0 then usage() exit() end

    local subargs = gDvs.slice(arg, optind)
    gRegCodec.dfuncMain(subargs)

    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
    local supportedFids = {} -- a table of supported function identifiers (FIDs)
    local nameInfo={}        -- a string table which has name information used by list command


    nameInfo,supportedFids = populateSupportedNames(gSupportedNames)
    gRegCodec.dfuncInit(nameInfo, supportedFids, getFinfo, gSpmi.spmiReadRegister, gSpmi.spmiWriteRegister)

end

local exports = {
    main      = main,
}


if get_platform() == "msm8996" then 
    printf("Support msm8996 \n")
    local gMsm8996  = require("msm8996")
    populateSupportedNames = gMsm8996.pmicSupportedNames
    getrdfile = gMsm8996.getrdfile
end

initModule()

if ... == "pmic_function" then
    return exports --library, export functions
else
    main(arg)
end
