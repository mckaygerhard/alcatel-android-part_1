#!/system/xbin/lua

local gDvs = require("dvs")
local memPoke = require("mem_poke")
local gRegCodec = require("register_codec")


local print, sf, printf = print, sf, printf

local gRdPath = "/system/etc/dvs/"
local gSupportedNames = {} -- supported FID (function identifiers) indexed by function name

local readAddress = function(address)
    local value = memPoke.memget(address, 4, 1)
    if value ~= nil then value = value[1] end
    if value == nil then logErr("failed reading address 0x%08x", address) end
    return value
end

local writeAddress = function(address, value)
    logInf("writing 0x%x to address 0x%x", value, address )
    if not memPoke.memset(address, value, 4, 1) then
        logErr("failed writing address 0x%08x, value 0x%08x", address, value)
        return false
    end
    return true
end

local getrdfile = function(fid)
    printf("Not Support %s",get_platform())
end


local getFinfo = function (name)
    local fid = gSupportedNames[name]
    if fid == nil then
        logErr("function %s is not supported", name)
        return
    end
    return getrdfile(fid)
end

-- populates gSupportedNames, gSupportedFids and gSupportedNameInfo
local populateSupportedNames = function()
    printf("Not Support %s",get_platform())
end

local usageStr = [[
Usage:
This is a utility for displaying/modifying MSM gpio.

msm_gpio.lua <sub-command> [sub-command arguments]

The following sub-command are supported
    l                                      list supported GPIOs
    c <gpio_name>  <setting=value> [...]   configure and set the GPIO
    d [-s|-d] [gpio_name ...]              display status and settings of the GPIO(s)

sub-command l (ls, list)
    List the full names and short names of supported GPIOs (aka functions).

sub-command c (cs, configure)
    sub-command c takes a function name and zero or more <setting=value>
    if no <setting=value> is given, this sub-command displays all supported settings and values of
    the given GPIO.

sub-command d (ds, display)
    Display the settings and status of one or more GPIOs.
    Zero or more gpio_name_pattern are allowed. A gpio_name can be an exact gpio name,
    or a pattern to match GPIO names. Use * to match many characters (such as gpio*), use ? to
    match one character, and - to match a range (such as 20-5 and gpio1-20).
    Without any gpio_name, all GPIOs will be displayed.
    By default
      single gpio name will display detailed information: all registers associated with the gpio name will be displayed
      multiple gpio names (or wildcards) will display summary information: limited summary registers for the gpio(s) are displayed
    The -s and -d options will override the default display mode
      -s will only display summary information
      -d will only display detailed information

Examples:
    msm_gpio.lua    l      # list names of supported GPIOs
    msm_gpio.lua d  1-20 gpio30 gpio4*   # display GPIO_1 to GPIO_20, and GPIO30, and GPIO_40 to GPIO_49
    msm_gpio.lua d         # display all GPIOs
    # The following command configure GPIO_115, enable output without pull, set output to high
    # and set drive strength to 4mA
    msm_gpio.lua c   115 oe=1 p=np o=1 d=4m
]]

local usage = function()
    printf(usageStr)
end

local main = function(arg)
    local optarg, optind = gDvs.init(arg, "hv", usage)

    local supportedFids = {} -- a table of supported function identifiers (FIDs)
    local nameInfo={}        -- a string table which has name information used by list command

    if #arg == 0 then usage() exit() end
    nameInfo,supportedFids = populateSupportedNames(gSupportedNames)
    gRegCodec.dfuncInit(nameInfo, supportedFids, getFinfo, readAddress, writeAddress)
    gRegCodec.dfuncMain(gDvs.slice(arg, optind))

    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main      = main,
}

initModule()

if get_platform() == "msm8996" then 
    printf("Support msm8996 \n")
    local gMsm8996 = require("msm8996")
    populateSupportedNames = gMsm8996.msmSupportedNames
    getrdfile = gMsm8996.getrdfile
end


if ... == "msm_gpio" then
    return exports --library, export functions
else
    main(arg)
end
