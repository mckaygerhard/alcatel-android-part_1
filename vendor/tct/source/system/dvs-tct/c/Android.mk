LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS      := eng debug
LOCAL_SRC_FILES        := dvsc.c
LOCAL_MODULE           := libdvsc
LOCAL_C_INCLUDES       += $(LOCAL_PATH)/../lua-5.2.3/src
LOCAL_CFLAGS           += -Wall -Werror
LOCAL_STATIC_LIBRARIES := liblua

LOCAL_MULTILIB         := first

include $(BUILD_SHARED_LIBRARY)

################################################################################
#include $(CLEAR_VARS)
#LOCAL_MODULE := libqmic
#LOCAL_SRC_FILES := qmic.c
#LOCAL_MODULE_TAGS := debug
#LOCAL_MULTILIB := first

#LOCAL_SHARED_LIBRARIES := \
#    libqmiservices_ext \
#    libqmi_cci \
#    libril-qc-qmi-1

#LOCAL_STATIC_LIBRARIES := liblua

#LOCAL_C_INCLUDES := \
#    $(LOCAL_PATH)/../lua-5.2.3/src \
#    $(TARGET_OUT_HEADERS)/qmi-framework/inc \
#    vendor/qcom/proprietary/qmi/services_ext \
#    hardware/ril/include/telephony \
#    vendor/qcom/proprietary/qcril/qcril_qmi \
#    vendor/qcom/proprietary/common/inc \
#    vendor/qcom/proprietary/qcril/common/uim \
#    vendor/qcom/proprietary/qmi/inc \
#    vendor/qcom/proprietary/qmi/platform \
#    vendor/qcom/proprietary/diag/include \
#   vendor/qcom/proprietary/qcril/qcril_qmi/ims_socket \
#    external/nanopb-c

#LOCAL_CFLAGS := \
#    -DQCRIL_DATA_OFFTARGET \
#   -DFEATURE_UNIT_TEST

#include $(BUILD_SHARED_LIBRARY)
