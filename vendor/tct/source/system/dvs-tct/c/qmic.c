#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <lua.h>
#include <lauxlib.h>

#include "qmi_client.h"
#include "device_management_service_v01.h"
#include "wireless_messaging_service_v01.h"

#include "ril_cdma_sms.h"
#include "qcril_qmi_sms.h"

qmi_client_type                   qmi_svc_client;

static int client_init(qmi_idl_service_object_type so)
{
    qmi_client_error_type client_err;
    qmi_client_os_params os_params;

    // Initialize the QMI DMS client
    client_err = qmi_client_init_instance(so,
                                          QMI_CLIENT_INSTANCE_ANY,
                                          NULL,
                                          NULL,
                                          &os_params,
                                          4,
                                          &qmi_svc_client);
    //fprintf(stderr, "qmi_client_init_instance returned (%d) for DMS \n", client_err);

    return client_err;
}

static void client_release()
{
    if (qmi_svc_client != NULL) {
        qmi_client_release(qmi_svc_client);
    }
}

static int qmi_get_operating_mode_state()
{
    int rc = -1;
    dms_get_operating_mode_resp_msg_v01 qmi_response;
    qmi_client_error_type qmi_transport_error;

    memset(&qmi_response, 0, sizeof(qmi_response));

    if (client_init(dms_get_service_object_v01()) != 0) {
        return -1;
    }

    qmi_transport_error = qmi_client_send_msg_sync(qmi_svc_client,
                                                   QMI_DMS_GET_OPERATING_MODE_REQ_V01,
                                                   NULL,
                                                   0,
                                                   &qmi_response,
                                                   sizeof( qmi_response ),
                                                   3000);
    client_release();

    if (qmi_transport_error == QMI_NO_ERR) {
        rc = (int) qmi_response.operating_mode;
    }
    //fprintf(stderr, ".. known modem operating mode %d, result %d\n", rc, qmi_transport_error);

    return rc;
}

static int qmi_set_operating_mode_state(int mode)
{
    int rc = -1;
    dms_set_operating_mode_req_msg_v01   qmi_request;
    dms_set_operating_mode_resp_msg_v01   qmi_response;
    qmi_client_error_type qmi_transport_error;

    memset(&qmi_request, 0, sizeof(qmi_request));
    memset(&qmi_response, 0, sizeof(qmi_response));
    qmi_request.operating_mode = (dms_operating_mode_enum_v01) mode;

    if (client_init(dms_get_service_object_v01()) != 0) {
        return -1;
    }

    qmi_transport_error = qmi_client_send_msg_sync(qmi_svc_client,
                                                   QMI_DMS_SET_OPERATING_MODE_REQ_V01,
                                                   &qmi_request,
                                                   sizeof( qmi_request ),
                                                   &qmi_response,
                                                   sizeof( qmi_response ),
                                                   74000);
    client_release();

    if (qmi_transport_error == QMI_NO_ERR) {
        rc = 0;
    }
    //fprintf(stderr, "Requested operating mode setted to modem is %d result %d\n", mode, qmi_transport_error);

    return rc;
}

#define MASK_B(offset, len) \
  ((0xff >> offset) & (0xff << (8 - (offset + len))))

boolean sms_is_tag_mo(int tag)
{
  boolean is_tag_mo;
  switch ( tag )
  {
    case 0:
      is_tag_mo = FALSE;
      break;
    case 1:
      is_tag_mo = FALSE;
      break;
    case 2:
      is_tag_mo = TRUE;
      break;
    case 3:
      is_tag_mo = TRUE;
      break;
    default:
      printf("Attempting to write an SMS with an unrecognized tag: %d", tag);
      is_tag_mo = TRUE;
      break;
  }

  return is_tag_mo;
}

byte sms_hex_char_to_byte
(
  char hex_char
)
{
  byte byte_value;

  if (hex_char >= 'A' && hex_char <= 'Z')
  {
    hex_char = hex_char + 'a' - 'A';
  }

  if (hex_char >= 'a' && hex_char <= 'f')
  {
    byte_value = (byte)(hex_char - 'a' + 10);
  }
  else if (hex_char >= 'A' && hex_char <= 'F')
  {
    byte_value = (byte)(hex_char - 'A' + 10);
  }
  else if (hex_char >= '0' && hex_char <= '9')
  {
    byte_value = (byte)(hex_char-'0');
  }
  else
  {
    byte_value = 0;
  }

  return (byte_value);
}

void _b_packb(byte src, byte dst[], word pos, word len)
{
   word   t_pos = pos % 8;
   word   bits  = 8 - t_pos;

   dst += (pos+len-1)/8;

   if ( bits >= len )
   {
       *dst &= (byte) ~MASK_B(t_pos, len);
       *dst |= (byte) (MASK_B(t_pos, len) & (src << (bits - len)));
   }
   else /* len > bits */
   {
       dst--;
       *dst &= (byte) ~MASK_B(t_pos, bits);
       *dst |= (byte) (MASK_B(t_pos, bits) & (src >> (len - bits)));

       dst++;
       *dst &= (byte) ~MASK_B(0, (len - bits));
       *dst |= (byte) (MASK_B(0, (len - bits)) & (src << (8 - (len - bits))));
   }
}

void sms_hex_to_byte(const char * hex_pdu,byte * byte_pdu,uint32 num_hex_chars)
{
  uint16 buf_pos = 0;
  uint32 i;
  for (i=0; i < num_hex_chars; i++)
  {
    _b_packb(sms_hex_char_to_byte(hex_pdu[i]), byte_pdu, buf_pos, (word)4);
    buf_pos += 4;
  }
}

boolean mo_sms_error_check(const char *pdu)
{
  boolean err_check_passed = TRUE;
  if ( pdu == NULL )
  {
    printf("PDU in SMS is NULL!");
    err_check_passed = FALSE;
  }

  /* Check if the length exceeds the maximum */
  else if ( ( strlen( pdu ) / 2 ) > WMS_MESSAGE_LENGTH_MAX_V01 )
  {
    printf("PDU in SMS exceeds maximum allowable length!");
    err_check_passed = FALSE;
  }
  return (err_check_passed);
}

boolean sms_fill_wms_payload
(
  boolean payload_in_cdma_format,
  RIL_CDMA_SMS_Message *cdma_sms_msg,
  const char *gw_smsc_address,
  const char *gw_pdu,
  wms_message_format_enum_v01  * format,
  uint32 * raw_message_len,
  uint8  * raw_message,
  boolean sms_on_ims,
  boolean is_mo_sms
)
{
  uint8 byte_array_pos = 0;
  boolean success = TRUE;

  printf( "payload in cdma format valid %d\n", payload_in_cdma_format);
  /* 3GPP2 format */
  if ( payload_in_cdma_format )
  {
    if(cdma_sms_msg != NULL)
    {
       /* Convert the message to OTA format */
       if (  qcril_sms_convert_sms_ril_to_qmi(cdma_sms_msg,
                                           raw_message,
                                           WMS_MESSAGE_LENGTH_MAX_V01,
                                           (uint16 *) raw_message_len,
                                           sms_on_ims,
                                           is_mo_sms) != TRUE)
       {
          printf( "Conversion from RIL format to QMI format failed\n" );
          success = FALSE;
       }
       else
       {
          *format = WMS_MESSAGE_FORMAT_CDMA_V01;
       }
    }
    else
    {
      printf("cdma_sms_msg is NULL!");
      success = FALSE;
    }
  }
  /* 3GPP format */
  else
  {
    if(gw_pdu != NULL)
    {
      *format = WMS_MESSAGE_FORMAT_GW_PP_V01;
       if ( gw_smsc_address == NULL )
       {
          /* There is no SMSC address.  The first byte of the raw data is the SMSC length,which is 0. */
          raw_message[0] = 0;
          byte_array_pos = 1;
       }
       else
       {
          /* Convert the SMSC address from ASCII Hex to byte*/
          sms_hex_to_byte( gw_smsc_address,&raw_message[0],strlen(gw_smsc_address) );

          /* The input is in ASCII hex format, and it is being packed into a byte array.
           Two ASCII characters fit in each byte, so divide the length by two */
          byte_array_pos = strlen(gw_smsc_address) / 2;
       }

      /* Translate the PDU from ASCII hex to bytes */
      sms_hex_to_byte( gw_pdu,&raw_message[byte_array_pos],strlen( gw_pdu ) );
      *raw_message_len = byte_array_pos + strlen(gw_pdu) / 2;
    }
    else
    {
      printf("gw_pdu is NULL!");
      success = FALSE;
    }
  }

  return (success);
}

int write_sms_to_uim(char *pdu,int length)
{
    int rc = -1;
  wms_raw_write_req_msg_v01 write_request_msg;
  wms_raw_write_resp_msg_v01 write_response_msg;
  char *gw_smsc_address = NULL;
  qmi_client_error_type qmi_err;

    if (client_init(wms_get_service_object_v01()) != 0) {
        return -1;
    }

  memset(&write_request_msg, 0, sizeof(wms_raw_write_req_msg_v01));
  if (!mo_sms_error_check(pdu))
  {
      printf("qcril_mo_sms_error_check failed for SMS, which is going to written in SIM.\n");
      return -1;
  }

    /* Fill in the QMI request to write the message to the SIM */
  write_request_msg.raw_message_write_data.storage_type = WMS_STORAGE_TYPE_UIM_V01;
  sms_fill_wms_payload( FALSE,
                                   NULL,
                                   gw_smsc_address,
                                   pdu,
                                   &write_request_msg.raw_message_write_data.format,
                                   (uint32 *)&write_request_msg.raw_message_write_data.raw_message_len,
                                   &write_request_msg.raw_message_write_data.raw_message[0],
                                   FALSE,
                                   sms_is_tag_mo(3));

  memset(&write_response_msg, 0, sizeof(wms_raw_write_resp_msg_v01));
  write_request_msg.tag_type_valid = TRUE;
  write_request_msg.tag_type = qcril_sms_map_ril_tag_to_qmi_tag(3);
  qmi_err = qmi_client_send_msg_sync(qmi_svc_client,
                                           QMI_WMS_RAW_WRITE_REQ_V01,
                                           &write_request_msg,
                                           sizeof(wms_raw_write_req_msg_v01),
                                           &write_response_msg,
                                           sizeof(wms_raw_write_resp_msg_v01),
                                           30000);

    client_release();

  if(QMI_NO_ERR == qmi_err)
  {
    printf("QMI_WMS_RAW_WRITE_RESP received: SUCCESS\n");
    rc = 0;
  }
  return rc;
}

int read_sms_from_uim(char *data)
{
    wms_raw_read_req_msg_v01 wms_raw_read_req_msg_ptr;
    wms_raw_read_resp_msg_v01 wms_raw_read_resp_msg;
    qmi_client_error_type qmi_client_error;
    int rc = -1;

    if (client_init(wms_get_service_object_v01()) != 0) {
        return -1;
    }

    memset(&wms_raw_read_req_msg_ptr,0,sizeof(wms_raw_read_req_msg_ptr));
    memset(&wms_raw_read_resp_msg,0,sizeof(wms_raw_read_resp_msg));

    wms_raw_read_req_msg_ptr.message_mode_valid = TRUE;
    wms_raw_read_req_msg_ptr.message_mode = WMS_MESSAGE_MODE_GW_V01;
    wms_raw_read_req_msg_ptr.message_memory_storage_identification.storage_type = WMS_STORAGE_TYPE_UIM_V01;
    wms_raw_read_req_msg_ptr.message_memory_storage_identification.storage_index = 0;


    printf("MSG PARAMS %d %d %d\n",
                   wms_raw_read_req_msg_ptr.message_memory_storage_identification.storage_index,
                   wms_raw_read_req_msg_ptr.message_memory_storage_identification.storage_type,
                   wms_raw_read_req_msg_ptr.message_mode);

    qmi_client_error = qmi_client_send_msg_sync(qmi_svc_client,
                                                           QMI_WMS_RAW_READ_REQ_V01,
                                                           &wms_raw_read_req_msg_ptr,
                                                           sizeof( wms_raw_read_req_msg_ptr ),
                                                           &wms_raw_read_resp_msg,
                                                           sizeof( wms_raw_read_resp_msg ),
                                                           30000);

    client_release();

    printf("sent sms, qmi_client_error : %d\n", qmi_client_error);
    if(QMI_NO_ERR == qmi_client_error)
    {
       printf("Raw Read Request succeeded, wms tag = %d,format = %d",wms_raw_read_resp_msg.raw_message_data.tag_type,wms_raw_read_resp_msg.raw_message_data.format);
       data = wms_raw_read_resp_msg.raw_message_data.data;
       rc = wms_raw_read_resp_msg.raw_message_data.data_len;
    }

    return rc;
}

static int qmicGetOpMode(lua_State *L)
{
    int rc = qmi_get_operating_mode_state();
    lua_pushinteger(L, rc);
    return 1;
}

static int qmicSetOpMode(lua_State *L)
{
    int mode = luaL_checkint(L, 1);
    int rc = qmi_set_operating_mode_state(mode);
    lua_pushinteger(L, rc);
    return 1;
}

static int qmicWriteSMS(lua_State *L)
{
    const char *pdu = luaL_checkstring(L, 1);
    int rc = write_sms_to_uim(pdu, strlen(pdu));
    lua_pushinteger(L, rc);
    return 1;
}

static int qmicReadSMS(lua_State *L)
{
    char *data = NULL;
    int rc = read_sms_from_uim(data);
    lua_pushstring(L, data);
    lua_pushinteger(L, rc);
    return 2;
}

int qmicEntry(lua_State *L) {
    lua_register(L, "qmicGetOpMode", qmicGetOpMode);
    lua_register(L, "qmicSetOpMode", qmicSetOpMode);
    lua_register(L, "qmicWriteSMS", qmicWriteSMS);
    lua_register(L, "qmicReadSMS", qmicReadSMS);
    return 0;
}

static void usage(const char *prog)
{
    printf("\n\
%s\n\
    -g\n\
    -s mode_id\n\
       ONLINE = 0x00,\n\
       LOW_POWER = 0x01,\n\
       FACTORY_TEST_MODE = 0x02,\n\
       OFFLINE = 0x03,\n\
       RESETTING = 0x04,\n\
       SHUTTING_DOWN = 0x05,\n\
       PERSISTENT_LOW_POWER = 0x06,\n\
       MODE_ONLY_LOW_POWER = 0x07,\n\
       NET_TEST_GW = 0x08,\n\
\n", prog);
    exit(1);
}

#if 0
#include <getopt.h>
int main(int argc, char *argv[])
{
    int mode;
    int c;

    if (argc < 2) {
        usage(argv[0]);
    }

    while ((c = getopt(argc, argv, "gs:")) != -1) {
        switch (c) {
        case 'g':
            qmi_get_operating_mode_state();
            break;
        case 's':
            mode = atoi(optarg);
            if (mode < DMS_OP_MODE_ONLINE_V01 || mode > DMS_OP_MODE_NET_TEST_GW_V01) {
                printf("invailed mode id %d\n", mode);
                exit(1);
            }
            qmi_set_operating_mode_state(mode);
            break;
        default:
            usage(argv[0]);
        }
    }

    return 0;
}
#endif
