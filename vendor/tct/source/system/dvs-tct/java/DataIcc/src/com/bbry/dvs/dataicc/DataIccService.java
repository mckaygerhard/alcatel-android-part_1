
package com.bbry.dvs.dataicc;
import android.app.AppOpsManager;
import android.app.Service;
import android.os.IBinder;
import android.os.Binder;
import android.content.Intent;

import android.telephony.TelephonyManager;
import android.util.Log;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.SubscriptionManager;

//PBM
import android.os.RemoteException;
import android.os.ServiceManager;
import com.android.internal.telephony.IIccPhoneBook;
import com.android.internal.telephony.uicc.AdnRecord;
import com.android.internal.telephony.uicc.IccConstants;
import java.util.List;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import android.os.Environment;
import java.text.SimpleDateFormat;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DataIccService extends Service {
    static final String TAG = "DataIccService";
    static final boolean DEBUG = true;
    private static final String READ_SMS_FROM_SIM_FLAG = "read_sms";
    private static final String WRITE_SMS_TO_SIM_FLAG = "write_sms";
    private static final String DELETE_SMS_FROM_SIM_FLAG = "delete_sms";
    private static final long WRITE_SMS_TIMEOUT = 1000;
    private  int mTimes = 0;
    private  int maxTimes = 0;
    private  int mIccCapacityAll = -1;
    private byte smsPdu[];
    private  int mDelay = 0;
    private  int mSq = 1;

    private static final int EVENT_WRITE_SMS = 1;
    private static final int EVENT_WRITE_SMS_DONE = 2;
    
    private static TelephonyManager mTelephonyManager = null;
    private AppOpsManager mAppOpsManager;

    private final String mMsg = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";


    private Handler mHandler = new Handler() {

        @Override
        public void
        handleMessage(Message msg) {

            switch (msg.what) {
                case EVENT_WRITE_SMS:
                case EVENT_WRITE_SMS_DONE:
                  if(mTimes > 0){
                    mTimes = mTimes - 1;
                    onWriteSmsToIcc(mSq,mIccCapacityAll);
                    mSq = mSq + 1;
                    if(mSq > maxTimes){
                       mSq = 1;
                    }
                  }
                break;

               default:
                Log.d(TAG,"Error option!");
                break;
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mAppOpsManager = (AppOpsManager) this.getSystemService(Context.APP_OPS_SERVICE);
        mAppOpsManager.setMode(AppOpsManager.OP_WRITE_ICC_SMS, Binder.getCallingUid(), this.getPackageName(), AppOpsManager.MODE_ALLOWED);
        mTelephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        mIccCapacityAll = SmsManager.getSmsManagerForSubscriptionId(SmsManager.getDefaultSmsSubscriptionId()).getSmsCapacityOnIcc();
        //getSmscAddress
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        Log.i(TAG,"stop services.");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getBooleanExtra(READ_SMS_FROM_SIM_FLAG, false)) {
            Log.i(TAG,"Read sms from icc");
            onReadSmsFromIcc();
        } else if(intent != null && intent.getBooleanExtra(DELETE_SMS_FROM_SIM_FLAG,false)){
           if(DEBUG) Log.i(TAG,"Delete sms from icc");
           onDeleteSmsFromIcc();
        } else if(intent != null && intent.getBooleanExtra("boot_complete",false)){
             if(DEBUG) Log.i(TAG,"Boot completed!");
        } else {
             if(intent != null){
               boolean isDigitString = false;
               String str = (String)intent.getExtra("parameters");
               String[] params = str.split(",");
               //int times = Integer.parseInt((String)intent.getExtra("times"));
               maxTimes = Integer.parseInt(params[0]);
               if(maxTimes > mIccCapacityAll){
                  maxTimes = mIccCapacityAll;
               }
               mSq = 1;
               mTimes = maxTimes;
               mDelay = Integer.parseInt(params[1]);
               String msg = params[2];

               if(msg != null){
                  Pattern pattern = Pattern.compile("[0-9]*");
                  Matcher isDigit = pattern.matcher(msg);
                  if(isDigit.matches()){
                     isDigitString = true;
                  }
                  if(!isDigitString && msg.length() > 140){
                     msg = params[2].substring(0,139);
                  }
                  if(isDigitString && msg.length() > 160){
                     msg = params[2].substring(0,159);
                  }
               }else{
                  msg = mMsg;
               }
               smsPdu = SmsMessage.getSubmitPdu("16502530001111111111", "12348900011111111111", msg, true, SmsManager.getDefaultSmsSubscriptionId()).encodedMessage;
               if(DEBUG) Log.i(TAG,"Start to write sms : " + msg + " max times : " + maxTimes + " size : " + msg.length() + " pdu length : " + smsPdu.length
                      + " Sms capability : " + mIccCapacityAll);
               mHandler.sendMessageDelayed(mHandler.obtainMessage(EVENT_WRITE_SMS),0);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }
    
    private boolean isSimCardPresent() {
        return mTelephonyManager.getPhoneType() != TelephonyManager.PHONE_TYPE_NONE &&
                mTelephonyManager.getSimState() != TelephonyManager.SIM_STATE_ABSENT;
    }
    

    private void onDeleteSmsFromIcc(){
       new Thread(){
          public void run(){
            ArrayList<SmsMessage> messages;
            long token = Binder.clearCallingIdentity();
            try {
                messages = SmsManager.getSmsManagerForSubscriptionId(SmsManager.getDefaultSmsSubscriptionId()).getAllMessagesFromIcc();
            } finally {
                Binder.restoreCallingIdentity(token);
            }
            int count = messages.size();
            if(DEBUG) Log.i(TAG,"Total " + count + " sms on icc, delete all sms!");
            for(int i = 1; i <= count; i++){
               SmsManager.getSmsManagerForSubscriptionId(SmsManager.getDefaultSmsSubscriptionId()).deleteMessageFromIcc(i);
            }
            try {
               File dir = new File("/data/data/com.bbry.dvs.dataicc/files");
               File[] files = dir.listFiles();
               if(files != null){
                  for(File file : files){
                     file.delete();
                  }
               }
               /*File file = new File("/data/data/com.bb.smsicc/files","smsicc");
               if(file.exists()){
                  file.delete();
               }*/

            }catch(Exception e){
               Log.v(TAG, e.toString());
            }
          }
       }.start();
    }

    private void onReadSmsFromIcc(){
         new Thread() {
            public void run() {
                //read sms on icc card
                ArrayList<SmsMessage> messages;
                long token = Binder.clearCallingIdentity();
                try {
                    messages = SmsManager.getSmsManagerForSubscriptionId(SmsManager.getDefaultSmsSubscriptionId()).getAllMessagesFromIcc();
                } finally {
                    Binder.restoreCallingIdentity(token);
                }

                if (messages == null) {
                    Log.i(TAG,"fails to read sms from card!");
                    return;
                }
                final int count = messages.size();
                Log.i(TAG,"SMS count is : " + count);

                try {
                    FileOutputStream fs = openFileOutput("smsicc",Context.MODE_PRIVATE);
                    if(count >= 1){
                        for(int i = 0; i < count; i++){
                          SmsMessage message = messages.get(i);
                          if (message != null) {
                            fs.write(message.getMessageBody().getBytes());
                            fs.write('\n');
                          }
                        }
                    }else{
                       fs.write(0x01);
                    }
                    fs.flush();
                    fs.close();

                } catch (Exception e) {
                   Log.v(TAG, e.toString());
                }

                //read contacts on icc card
                /*List<AdnRecord> adnRecords = null;
                try {
                    IIccPhoneBook iccIpb = IIccPhoneBook.Stub.asInterface(
                            ServiceManager.getService("simphonebook"));
                    if (iccIpb != null) {
                        adnRecords = iccIpb.getAdnRecordsInEfForSubscriber(SubscriptionManager.getDefaultSubId(), IccConstants.EF_ADN);
                    }
                } catch (RemoteException ex) {
                    // ignore it
                } catch (SecurityException ex) {
                    if (DEBUG) Log.i(TAG,ex.toString());
                }

                if (adnRecords != null) {
                   final int N = adnRecords.size();
                   if(DEBUG) Log.i(TAG,"Number of records : " + N);
                }*/
            }
        }.start();
    }
    
    
    private class writeSmsThread extends Thread {
        private int mMaxTimes;
        private int mSquence;
        private int mInterval;
        private byte [] mPdu;
        private int mIccCapacityAll;

        public writeSmsThread(byte pdu[], int sequence, int maxtimes,int interval, int iccCapacityAll) {
           super("writeSms");
           mPdu = new byte[pdu.length];
           System.arraycopy(pdu, 0, mPdu, 0, pdu.length);
           mMaxTimes = maxtimes;
           mInterval = interval;
           mSquence = sequence;
           mIccCapacityAll = iccCapacityAll;
        }


        public void run() {
            boolean result = false;
            int ret = 0x0;
            //if(DEBUG) Log.i(TAG,"mPdu len :" + mPdu.length);
            result = SmsManager.getSmsManagerForSubscriptionId(SmsManager.getDefaultSmsSubscriptionId()).copyMessageToIcc(null,mPdu, SmsManager.STATUS_ON_ICC_SENT);
            if(DEBUG) Log.i(TAG,"writing sms: " + mSquence + " to icc, status: " + result);
            ret = result?0x01:0x00;
            String output = mSquence + "," + ret + "\n";
            try {
                FileOutputStream fs = openFileOutput("writeSmsStatus",Context.MODE_APPEND);
                fs.write(output.getBytes());
                fs.flush();
                fs.close();
            } catch (Exception e) {
                Log.v(TAG, e.toString());
            }
            mHandler.sendMessageDelayed(mHandler.obtainMessage(EVENT_WRITE_SMS_DONE),1000*mInterval);
            if(DEBUG && maxTimes == mSquence) Log.i(TAG,"Write data commpletely.");
            if(DEBUG && mIccCapacityAll == mSquence) Log.i(TAG,"Sim memory is full.");
        }
    }
    private void onWriteSmsToIcc(int squence,int iccCapacityAll){
       new writeSmsThread(smsPdu,squence,maxTimes,mDelay,iccCapacityAll).start();
    }

   private  byte[]hexStringToBytes(String s) {
        byte[] ret;

        if (s == null) return null;

        int sz = s.length();

        ret = new byte[sz/2];

        for (int i=0 ; i <sz ; i+=2) {
            ret[i/2] = (byte) ((hexCharToInt(s.charAt(i)) << 4)
                                | hexCharToInt(s.charAt(i+1)));
        }

        return ret;
  }

  private int hexCharToInt(char c) {
        if (c >= '0' && c <= '9') return (c - '0');
        if (c >= 'A' && c <= 'F') return (c - 'A' + 10);
        if (c >= 'a' && c <= 'f') return (c - 'a' + 10);

        throw new RuntimeException ("invalid hex char '" + c + "'");
    }
}
