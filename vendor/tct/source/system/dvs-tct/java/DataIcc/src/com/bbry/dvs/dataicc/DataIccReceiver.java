package com.bbry.dvs.dataicc;

import android.os.Bundle;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.UserHandle;
import android.util.Log;

public class DataIccReceiver extends BroadcastReceiver {
    private static final String TAG = "DataIccReceiver";
    private static final String BOOT_COMPLETE_FLAG = "boot_complete";
    private static final String READ_SMS_FROM_SIM_FLAG = "read_sms";
    private static final String WRITE_SMS_TO_SIM_FLAG = "write_sms";
    private static final String DELETE_SMS_FROM_SIM_FLAG = "delete_sms";
    private static final String WRITE_SMS_TO_SIM_CONTENT = "write_sms_content";
    private static final boolean DBG = true;
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;

        Intent i = new Intent(context, DataIccService.class);
        String receivedAction = intent.getAction();
        Log.d(TAG,"Action : " + receivedAction);
        if (receivedAction.equals(Intent.ACTION_BOOT_COMPLETED)) {
            // start service to do the work.
            if (DBG) {
                Log.d(TAG, "Action boot completed received..");
            }
            i.putExtra(BOOT_COMPLETE_FLAG, true);
            context.startServiceAsUser(i,UserHandle.CURRENT);
        } else if (receivedAction.equals("com.bbry.dvs.action.READ_ICCDATA")) {
            if (DBG) {
                Log.d(TAG, "Action READ DATA received..");
            }
            i.putExtra(READ_SMS_FROM_SIM_FLAG, true);
            context.startServiceAsUser(i,UserHandle.CURRENT);
        } else if (receivedAction.equals("com.bbry.dvs.action.WRITE_ICCDATA")) {
            if (DBG) {
                Log.d(TAG, "Action WRITE DATA received..");
            }
            String msg = (String)intent.getExtra("-p");
            Log.i(TAG,"parameters : " + msg);
            Bundle extras = new Bundle();
            if (msg != null) {
              i.putExtra("parameters", msg);
            }

            //String times = (String)intent.getExtra("n");
            //i.putExtra("times",times);
            //i.putExtra("android.sms.extra.WRITE_SMSICC", extras);
            context.startServiceAsUser(i,UserHandle.CURRENT);
        }else if (receivedAction.equals("com.bbry.dvs.action.DELETE_ICCDATA")) {
            if (DBG) {
                Log.d(TAG, "Action DELETE DATA received..");
            }
            i.putExtra(DELETE_SMS_FROM_SIM_FLAG, true);
            context.startServiceAsUser(i,UserHandle.CURRENT);
        }
    }
}
