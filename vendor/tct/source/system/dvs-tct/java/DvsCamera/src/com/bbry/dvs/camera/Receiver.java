package com.bbry.dvs.camera;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Receiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("com.bbry.dvs.camera.open")) {
            Intent intent2 = new Intent(context, Main.class);
            intent2.putExtra("mode", intent.getIntExtra("mode", -1));
            intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent2);
        }
    }
}
