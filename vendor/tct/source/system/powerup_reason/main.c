/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Get powerup reason from /sys/bootinfo/powerup_reason.
 *dmesg-0:oops info
 *dmesg-1:kernel panic info
 *consol: kernel printk info
 *pmsg: log user space messages
 *save 10 times log info
 */

#include <fcntl.h>
#include <time.h>
#include <stdio.h>
#include <sys/stat.h>
#include <cutils/log.h>
#include <stdlib.h>
#include <cutils/properties.h>
#include <unistd.h>
#include <string.h>
#define TRUE 1
#define FALSE 0

#define PATH_NAME_LENGTH_MAX   255
#define KERNEL_LOG_FILE_NUM_MAX   6
#define JRD_MINILOG_PATH  "/tctpersist/minilog"
#define JRD_MINI_KERNEL_LOG_PATH  JRD_MINILOG_PATH"/kernel_crash"
//#define SYS_POWERUP_REASON_PATH "/sys/bootinfo/powerup_reason"
#define SYS_POWERUP_REASON_PATH "/proc/sys/kernel/boot_reason"
#define JRD_POWERUP_REASON_PATH JRD_MINILOG_PATH"/powerup_reason/reset_reason" //"/tctpersist/powerup_reason/reset_reason"

#define WDT_SOURCE_PATH "/proc/last_cpu"
#define WDT_DEST_PATH JRD_MINI_KERNEL_LOG_PATH"/watchdog_reboot" //"/tctpersist/crash_report/watchdog_reboot"

#define PSTORE_SOURCE_PATH0 "/sys/fs/pstore/dmesg-ramoops-0.enc.z"
#define PSTORE_SOURCE_PATH1 "/sys/fs/pstore/dmesg-ramoops-1.enc.z"

#define PSTORE_SOURCE_PATH2 "/sys/fs/pstore/console-ramoops"//add by SH-gtguo for task-528826
#define PSTORE_SOURCE_PATH3 "/sys/fs/pstore/pmsg-ramoops-0" //add PMSG by SH-gtguo for task-528826

#define JRD_PSTORE_DEST_PATH0 JRD_MINI_KERNEL_LOG_PATH"/dmesg-ramoops-0.enc.z" //"/tctpersist/crash_report/dmesg-ramoops-0.enc.z"
#define JRD_PSTORE_DEST_PATH1 JRD_MINI_KERNEL_LOG_PATH"/dmesg-ramoops-1.enc.z" //"/tctpersist/crash_report/dmesg-ramoops-1.enc.z"
#define JRD_PSTORE_DEST_PATH2 JRD_MINI_KERNEL_LOG_PATH"/console-ramoops" //"/tctpersist/crash_report/console-ramoops"
#define JRD_PSTORE_DEST_PATH3 JRD_MINI_KERNEL_LOG_PATH"/pmsg-ramoops-0" //"/tctpersist/crash_report/pmsg-ramoops-0"//add PMSG by SH-gtguo for task-528826

#define JRD_PSTORE_KMSG_LAST JRD_MINI_KERNEL_LOG_PATH"/console-ramoops-last" //"/tctpersist/crash_report/console-ramoops-last"
#define JRD_PSTORE_KMSG_LAST_1 JRD_MINI_KERNEL_LOG_PATH"/console-ramoops-last-1" //"/tctpersist/crash_report/console-ramoops-last-1"
#define JRD_PSTORE_KMSG_LAST_2 JRD_MINI_KERNEL_LOG_PATH"/console-ramoops-last-2" //"/tctpersist/crash_report/console-ramoops-last-2"
//add PMSG by SH-gtguo for task-528826
#define JRD_PSTORE_PMSG_LAST JRD_MINI_KERNEL_LOG_PATH"/pmsg-ramoops-last" //"/tctpersist/crash_report/pmsg-ramoops-last"
#define JRD_PSTORE_PMSG_LAST_1 JRD_MINI_KERNEL_LOG_PATH"/pmsg-ramoops-last-1" //"/tctpersist/crash_report/pmsg-ramoops-last-1"
#define JRD_PSTORE_PMSG_LAST_2 JRD_MINI_KERNEL_LOG_PATH"/pmsg-ramoops-last-2" //"/tctpersist/crash_report/pmsg-ramoops-last-2"
//end
#define JRD_PSTORE_OOPS_COUNT JRD_MINI_KERNEL_LOG_PATH"/koops_count" //"/tctpersist/crash_report/koops_count"
#define POWERUP_REASON_LEN 40
#define POWERUP_REASON_ID_LEN 10
#define CTIME_LEN 26

#define TOTAL_RECORDS 10
#define RECORD_LEN ((POWERUP_REASON_LEN) + (CTIME_LEN)) + 2

#define ALLIGN_ITEMS	16
#define TIMES_LEN	7
#define PERCENT_LEN	2

#define STRUCT_VER "002"
#define VER_LEN		4

#define TITLE1 "Latest 10 records:\n"
#define TITLE2 "Earliest 10 records:\n"
#define TITLE_LEN		25

#define Hard_Reset "Hard Reset"
#define SMPL       "sudden momentary power loss"
#define RTC        "RTC alarm expiry"
#define DC         "DC charger insertion"
#define USB        "USB charger insertion"
#define PON1       "secondary PMIC"
#define CBL        "external power supply"
#define KPD        "power key press"
#define OTHERS     "others"

#define TAG "powerup_reason"

typedef enum{
	EN_Hard_Reset = 0, /*Hard Reset*/
	EN_SMPL,/*sudden momentary power loss*/
	EN_RTC,/*RTC alarm expiry*/
	EN_DC,	/*DC charger insertion*/
	EN_USB,/*USB charger insertion*/
	EN_PON1,/*secondary PMIC*/
	EN_CBL,/*external power supply*/
	EN_KPD,/*power key press*/
	EN_OTHERS,
	EN_MAX
}POWERON_REASON_CODE;

typedef struct {
	unsigned int total_times;
	unsigned int statistics[EN_MAX+1];//include unkown state
	unsigned int pad[ALLIGN_ITEMS - 1 - (EN_MAX+1)];
	char statistics_str[EN_MAX+1][POWERUP_REASON_LEN+TIMES_LEN+PERCENT_LEN+1+2];// poweron reason + times + dd% \n\0
	char title1[TITLE_LEN];
	char reasons[TOTAL_RECORDS][RECORD_LEN];
	char title2[TITLE_LEN];
	char earliest_reasons[TOTAL_RECORDS][RECORD_LEN];
	char ver[VER_LEN];
}PowerupReasonStruct;

static POWERON_REASON_CODE get_poweron_reason_code(char* reason_str)
{
	char* pw_reasons[] = {
		Hard_Reset,
		SMPL,
		RTC,
		DC,
		USB,
		PON1,
		CBL,
		KPD,
		OTHERS };
	int i = 0;
	for(i=0; i<EN_MAX; i++){
		if(0 == strncmp(pw_reasons[i],reason_str,strlen(pw_reasons[i])))
			break;
	}
	return (POWERON_REASON_CODE)i;
}

static char* get_poweron_string(POWERON_REASON_CODE code)
{
	printf("powerup reason main() --- code=%d\n",code);
	SLOGD("powerup reason main() --- code=%d\n",code);
	switch (code){
	case EN_Hard_Reset:
		return Hard_Reset;
	case EN_SMPL:
		return SMPL;
	case EN_RTC:
		return RTC;
	case EN_DC:
		return DC;
	case EN_USB:
		return USB;
	case EN_PON1:
		return PON1;
	case EN_CBL:
		return CBL;
	case EN_KPD:
		return KPD;
	case EN_OTHERS:
		return OTHERS;
	default:
		return "unknown";
	}
}


int convert(const char *str)/*string to int*/
{
	int v = 0;
	do{
	      v = 10*v+*str-'0';
	      str++;
	}while((*str>='0')&&(*str<='9'));
	return v;
}

int get_kernel_panic_count(void) {
  int count=0;
  char oops_num[4];
  int fd=open(JRD_PSTORE_OOPS_COUNT,O_RDONLY );
  if ( fd > 0 ) {
    if (read(fd, oops_num, 4) > 0) {
      count = atoi(oops_num);
      close(fd);
      return count;
    }
    close(fd);
    printf("error\n");
    return -1;
  }
  return 0;
}

void set_kernel_panic_count(int count) {
  int fd=open(JRD_PSTORE_OOPS_COUNT,O_RDWR | O_CREAT, 0644 );
  char oops_num[4];
  sprintf(oops_num, "%d\n", count);
  if(fd>0) {
    write(fd, oops_num, 4);
    close(fd);
//    chmod(JRD_PSTORE_OOPS_COUNT,0644);
  }
}


void copy_pstore_log(const char* source, const char* destination)
{
 	struct tm *local;
	char mytime[30];
	//char pstore_dest_file[PATH_NAME_LENGTH_MAX] = {0};
	//char pstore_remove_file[PATH_NAME_LENGTH_MAX] = {0};
	//char build_id[PROPERTY_VALUE_MAX];
	char *pstore_dest_file=NULL;
	char *pstore_remove_file=NULL;
	char *build_id=NULL;

	FILE * sourcefile = NULL;
	FILE * destfile = NULL;
	int c = 0;
	int panic_count;

 	time_t nowtime;
 	nowtime = time(NULL);

	//get system current time
 	local=localtime(&nowtime);

	strftime(mytime,30,"%Y-%m-%d-%H-%M-%S",local);

	pstore_dest_file=malloc(PATH_NAME_LENGTH_MAX);
	if (pstore_dest_file==NULL) {
		ALOGE("Can't alloc pstore_dest_file");
		goto skip_copy;
	}
	memset(pstore_dest_file,0,PATH_NAME_LENGTH_MAX);
	pstore_remove_file=malloc(PATH_NAME_LENGTH_MAX);
	if (pstore_remove_file==NULL) {
		ALOGE("Can't alloc pstore_remove_file");
		goto skip_copy;
	}
	memset(pstore_remove_file,0,PATH_NAME_LENGTH_MAX);
	build_id=malloc(PROPERTY_VALUE_MAX);
	if(build_id !=NULL){
	memset(build_id, 0, PROPERTY_VALUE_MAX);
        property_get("ro.build.version.incremental", build_id, "unknow");
	}

        panic_count = get_kernel_panic_count();
//	sprintf(pstore_dest_file,"%s-%s",destination,mytimn);
	sprintf(pstore_dest_file,"%s-%d", destination, panic_count);

	sourcefile = fopen(source,"r");

	if (sourcefile == NULL) {
	        ALOGE("Can't open %s\n", source);
		printf(" %s is NULL!\n",source);
	} else {
		//open destnation file
                destfile = fopen(pstore_dest_file,"w");

		if (destfile != NULL) {
			//copy log file
			printf("JRD_PSTORE_SOURCE_PATH : %s\n",source);
			printf("JRD_PSTORE_DEST_PATH : %s\n",pstore_dest_file);

			while((c = fgetc(sourcefile)) != EOF)
			{
				if (fputc(c,destfile) == EOF)
					ALOGE("copy %s error!\n",pstore_dest_file);
			}
			fprintf(destfile, "\n panic file create time: %s\n", mytime);
			if(build_id !=NULL) fprintf(destfile, "\n build verison: %s\n", build_id); // MODIFIED by lijuan.wu, 2016-08-23,BUG-2786366
			//change property
			chmod(pstore_dest_file,0664);
			//chown(pstore_dest_file,0,1000);

			fclose(destfile);
			fclose(sourcefile);
			remove(source);
		        ALOGI("copy %s finished.\n", pstore_dest_file);
		        printf("copy %s finished.\n", pstore_dest_file);
			if(panic_count >= KERNEL_LOG_FILE_NUM_MAX)
			{
				sprintf(pstore_remove_file ,"%s-%d" ,destination ,panic_count - KERNEL_LOG_FILE_NUM_MAX );
				remove(pstore_remove_file);  //remove early log to keep only 10 times log
			}
		}
	}
	skip_copy:
	if(pstore_dest_file!=NULL){
		free(pstore_dest_file);
		pstore_dest_file=NULL;
	}
	if(build_id !=NULL){
		free(pstore_remove_file);
		pstore_remove_file=NULL;
	}
	if(build_id !=NULL){
		free(build_id);
		build_id=NULL;
	}
}

void copy_wdtlog(char *srcpath, char *destpath)
{
	int ret=0;
	FILE *src = fopen(srcpath,"r");
	char *build_id=NULL;
	struct tm *local;
	char mytime[30];

	time_t nowtime;
	nowtime = time(NULL);

	//get system current time
	local=localtime(&nowtime);

	strftime(mytime,30,"%Y-%m-%d-%H-%M-%S",local);
	build_id=malloc(PROPERTY_VALUE_MAX);
	if(build_id !=NULL){
	memset(build_id, 0, PROPERTY_VALUE_MAX);
	property_get("ro.build.version.incremental", build_id, "NULL");
	}
	if (src != NULL) {
	  //open destnation file
	    FILE *dest = fopen(destpath,"a+");
		if (dest != NULL) {
	        do {
	           ret = fgetc(src);
	           if ( ret == EOF)
	             break;
	           fputc((char)ret,dest);
	         }while( ret != EOF );
			fprintf(dest, "\n watchdog reboot record time: %s\n\n\n", mytime);
			if(build_id !=NULL)  fprintf(dest, "\n build verison: %s\n", build_id);
	        fclose(dest);
	    }
	    fclose(src);
	  }
	if(build_id !=NULL){
		free(build_id);
		build_id=NULL;
	}
}

void copy(char *srcpath, char *destpath)
{
  int ret=0;
  char *build_id=NULL;
  FILE *src = fopen(srcpath,"r");

  build_id=malloc(PROPERTY_VALUE_MAX);
  if(build_id !=NULL){
  memset(build_id, 0, PROPERTY_VALUE_MAX);
  property_get("ro.build.version.incremental", build_id, "NULL");
  }

  if (src != NULL) {
  //open destnation file
    FILE *dest = fopen(destpath,"w");
    if (dest != NULL) {
      do {
        ret = fgetc(src);
        if ( ret == EOF)
          break;
        fputc((char)ret,dest);
      }while( ret != EOF );
      if(build_id !=NULL)  fprintf(dest, "\n  build verison: %s\n", build_id);
      fclose(dest);
    }
    fclose(src);
  }
	if(build_id !=NULL){
		free(build_id);
		build_id=NULL;
	}
}


int main(int argc, char *argv[])
{
	int ret = -1;
	unsigned int i = 0;
	int j = 0;
	time_t timeval;
	struct stat stat_buf;
	int powerup_reason_fd = 0;
	int index = -1;
	int ID_len = 0;
	char index_str[POWERUP_REASON_ID_LEN] = {0};
	char current_powerup_reason[POWERUP_REASON_LEN+1] = {0};

	char (*pRecord)[RECORD_LEN] = NULL;
	char (*pOutRecord)[RECORD_LEN] = NULL;
	unsigned int *pTotal_times = NULL;

        int panic=0;

	memset(&stat_buf, 0 ,sizeof(stat_buf));

	PowerupReasonStruct my_records;
	PowerupReasonStruct my_out_records;
	memset(&my_records, 0, sizeof(my_records));
	memset(&my_out_records, 0, sizeof(my_out_records));

	memcpy(my_out_records.ver,STRUCT_VER,VER_LEN); // first fill the out struct version number
	memcpy(my_out_records.title1,TITLE1,strlen(TITLE1));
	memcpy(my_out_records.title2,TITLE2,strlen(TITLE2));

	pRecord = (char (*)[RECORD_LEN])&(my_records.reasons[0][0]);
	pOutRecord = (char (*)[RECORD_LEN])&(my_out_records.reasons[0][0]);
	pTotal_times = &(my_records.total_times);

        char pval[PROPERTY_VALUE_MAX] = "";
        do {
          sleep(5);
          property_get("init.svc.time_daemon",pval,"error");
          //printf("init.svc.time_daemon : %s \n", pval);
          /* MODIFIED-BEGIN by linjian.xiang, 2016-12-27,BUG-3854752*/
          j++;
        //}while(strncmp(pval,"running",7) || j >= 24);
        }while(strncmp(pval,"running",7) && j < 24);
        /* MODIFIED-END by linjian.xiang,BUG-3854752*/

	//save pstore logs to /tctpersist/crash_report/

        if ( !stat(PSTORE_SOURCE_PATH2,&stat_buf) ) {
  	  copy(JRD_PSTORE_KMSG_LAST_1 ,JRD_PSTORE_KMSG_LAST_2);
  	  copy(JRD_PSTORE_KMSG_LAST   ,JRD_PSTORE_KMSG_LAST_1);
	  copy(PSTORE_SOURCE_PATH2,JRD_PSTORE_KMSG_LAST);
          chmod(JRD_PSTORE_KMSG_LAST_2,0664);
          chmod(JRD_PSTORE_KMSG_LAST_1,0664);
          chmod(JRD_PSTORE_KMSG_LAST,0664);
        }
        //add PMSG by SH-gtguo for task-528826
        if ( !stat(PSTORE_SOURCE_PATH3,&stat_buf) ) {
  	  copy(JRD_PSTORE_PMSG_LAST_1 ,JRD_PSTORE_PMSG_LAST_2);
  	  copy(JRD_PSTORE_PMSG_LAST   ,JRD_PSTORE_PMSG_LAST_1);
	  copy(PSTORE_SOURCE_PATH3,JRD_PSTORE_PMSG_LAST);
          chmod(JRD_PSTORE_PMSG_LAST_2,0664);
          chmod(JRD_PSTORE_PMSG_LAST_1,0664);
          chmod(JRD_PSTORE_PMSG_LAST,0664);
        }

	//save watchdog reboot logs to /tctpersist/swd3_log/
	if ( !stat(WDT_SOURCE_PATH, &stat_buf) ) {
		copy_wdtlog(WDT_SOURCE_PATH , WDT_DEST_PATH);
		chmod(WDT_DEST_PATH , 0664);
	}

        // if dmesg-ramoops files exist, we assume there was a kernel panic
       // if ( !stat(PSTORE_SOURCE_PATH0,&stat_buf) && !stat(PSTORE_SOURCE_PATH1,&stat_buf) ) {
  	if ( !stat(PSTORE_SOURCE_PATH0,&stat_buf) || !stat(PSTORE_SOURCE_PATH1,&stat_buf) ) {
	copy_pstore_log(PSTORE_SOURCE_PATH0,JRD_PSTORE_DEST_PATH0);
	  copy_pstore_log(PSTORE_SOURCE_PATH1,JRD_PSTORE_DEST_PATH1);
	  copy_pstore_log(PSTORE_SOURCE_PATH2,JRD_PSTORE_DEST_PATH2);
          copy_pstore_log(PSTORE_SOURCE_PATH3,JRD_PSTORE_DEST_PATH3);//add PMSG by SH-gtguo for task-528826
          panic = 1;
        }

	if(0 <= stat(JRD_POWERUP_REASON_PATH,&stat_buf) && sizeof(PowerupReasonStruct) != stat_buf.st_size)
		unlink(JRD_POWERUP_REASON_PATH); // if old data struct size is not as the same as now, just clean it

	if(0 > (powerup_reason_fd = open(JRD_POWERUP_REASON_PATH,O_RDONLY))) {
		// first time, do nothing
	}else{
		// get old data from saved file /tctpersist/powerup_reason
		read(powerup_reason_fd, &my_records,sizeof(my_records));
		close(powerup_reason_fd);
		if(0 != strcmp(STRUCT_VER,my_records.ver)){ // if version confilct, discard the old data
			memset(&my_records,0,sizeof(my_records));
			unlink(JRD_POWERUP_REASON_PATH);
		}
	}


	do {
		// get this time powerup reason
		if(0 > (powerup_reason_fd = open(SYS_POWERUP_REASON_PATH,O_RDONLY))){
			//LOGD("open mtk powerup reason failed!");
			break;
		}
		ID_len = read(powerup_reason_fd, index_str, POWERUP_REASON_ID_LEN);//read len including end flag
		printf("powerup reason main() --- ID_len=%d\tindex_str=%s\n",ID_len,index_str);
		SLOGD("powerup reason main() --- ID_len=%d\tindex_str=%s\n",ID_len,index_str);
		if(0 > ID_len){
			//LOGD("read mtk powerup reason failed!");
			close(powerup_reason_fd);
			break;
		}
		close(powerup_reason_fd);
		index = convert(index_str) -1;/*because the array in c is start with 0, not 1, so need to minus 1 */
		sprintf(current_powerup_reason,"%s",get_poweron_string(index));
                if (panic) {
                  int panic_count;
                  strcat(current_powerup_reason, " !PANIC");
                  panic_count=get_kernel_panic_count();
                  if ( panic_count >= 0 ) {
                    panic_count++;
                    set_kernel_panic_count(panic_count);
                  }
                }
		//printf("powerup reason main() --- index=%d\tpoweron_str=%s\n",index,current_powerup_reason);
		//SLOGD("powerup reason main() --- index=%d\tpoweron_str=%s\n",index,current_powerup_reason);
		time(&timeval);//get current time

		for(i=0; i < TOTAL_RECORDS; i++)
		{
			// fill the output record
			if(0 == i){
				//add the new record to be the first record
				sprintf((char*)pOutRecord++,"%*s%*s",-POWERUP_REASON_LEN,current_powerup_reason,-CTIME_LEN,ctime(&timeval));
			}else{
				memcpy(pOutRecord++,pRecord++,RECORD_LEN);
			}
		}

		memcpy(my_out_records.earliest_reasons[0],  my_records.earliest_reasons[0], sizeof(my_out_records.earliest_reasons));
		//save the earliest 10 records
		for(i=0; i < TOTAL_RECORDS; i++)
		{
			if(strlen(my_out_records.earliest_reasons[i])==0)
			{
				memcpy(my_out_records.earliest_reasons[i], my_out_records.reasons[0], RECORD_LEN);
				break;
			}
		}

		my_out_records.total_times = *pTotal_times + 1; //update total poweron times
		//update other statistics data
		for(i=0; i<EN_MAX+1; i++)
		{
			if(i == get_poweron_reason_code(current_powerup_reason)){
				my_out_records.statistics[i] = my_records.statistics[i] + 1;
			}else{
				my_out_records.statistics[i] = my_records.statistics[i];
			}
			sprintf(my_out_records.statistics_str[i],"%*s%*d%*d%%\n",-POWERUP_REASON_LEN,get_poweron_string(i),-TIMES_LEN,my_out_records.statistics[i],PERCENT_LEN,my_out_records.statistics[i]*100/my_out_records.total_times);
		}

		// save powerup reason
		if(0 > (powerup_reason_fd = open(JRD_POWERUP_REASON_PATH,O_WRONLY | O_CREAT|O_TRUNC, 00644))){ //O_RDONLY O_WRONLY O_RDWR O_APPEND O_CREAT
			//LOGD("open jrd powerup reason failed!");
			break;
		}
		if(0 > (write(powerup_reason_fd, &my_out_records, sizeof(my_out_records)))){
			//LOGD("write jrd powerup reason failed!");
			close(powerup_reason_fd);
			break;
		}
		close(powerup_reason_fd);
                chmod(JRD_POWERUP_REASON_PATH,0644);

		ret = 0;
	} while(0);

	return ret;
}
