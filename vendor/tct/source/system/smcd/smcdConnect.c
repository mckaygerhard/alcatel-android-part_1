/* Copyright (C) 2016 Tcl Corporation Limited */

#define LOG_TAG "Feedback_smcd"

#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <sys/epoll.h>
#include <sys/un.h>
#include <cutils/sockets.h>
#include <log/log.h>
#include "cmd.h"

#ifndef __unused
#define __unused __attribute__((__unused__))
#endif

#define MAX_EPOLL_EVENTS 3

/* control socket listen and data */
static int ctrl_lfd;
static int ctrl_dfd = -1;
static int ctrl_dfd_reopened;

static int epollfd;
static int maxevents;
static struct sockaddr_un addr;


int ctrl_data_read(char *buf,size_t bufsz){
    int ret = 0;

    ret = read(ctrl_dfd,buf,bufsz);

    if(ret == -1){
        ALOGE("In %s line %d,control data socket read failed; errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
    }
    else if(ret == 0){
        ALOGE("In %s line %d, Got EOF on control data socket",__FUNCTION__,__LINE__);
        ret = -1;
    }

    return ret;
}

static void ctrl_data_close(void){
    if( ctrl_dfd < 0 || maxevents <= 1){
        if(bSmcdDebug) ALOGD("In %s line %d, ctrl_dfd == %d , maxevents == %d",__FUNCTION__,__LINE__,ctrl_dfd,maxevents);
        return ;
    }
    if(bSmcdDebug) ALOGD("In %s line %d, Closing Feedback connection ctrl_dfd %d",__FUNCTION__,__LINE__,ctrl_dfd);
    if( ctrl_dfd > 0){
       close(ctrl_dfd);
       ctrl_dfd = -1;
    }
    if(maxevents>1)
    maxevents--;
    if(bSmcdDebug) ALOGD("In %s line %d, maxevents = %d",__FUNCTION__,__LINE__,maxevents);
}
static void ctrl_epoll_del( ){
    if(epoll_ctl(epollfd,EPOLL_CTL_DEL,ctrl_dfd,/*&epev*/NULL) == -1){
        ALOGE("In %s line %d, epoll_ctl delete fd failed; errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
    }
    ctrl_data_close();
}
int ctrl_data_write(char cmd,char subcmd,char *buf){
    int ret = 0;
    char response[LINE_MAX];
    memset(response,0,sizeof(response));
    response[0] = cmd;
    response[1] = subcmd;
    strncat(response,buf,sizeof(response)-strlen(response)-1);
    ret = (int) write(ctrl_dfd, response, strlen(response));
    if(bSmcdDebug) ALOGD("In %s line %d, write response \"%s\" to socket",__FUNCTION__,__LINE__,response);
    if(ret == -1){
        ALOGE("In %s line %d,control data socket read failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
    }
    else if(ret == 0){
        ALOGE("In %s line %d, Got EOF on control data socket",__FUNCTION__,__LINE__);
        ret = -1;
    }

    return ret;
}

static void ctrl_data_handler(uint32_t events){
    if(events & EPOLLHUP){
        if(bSmcdDebug) ALOGD("In %s line %d, Feedback disconnected",__FUNCTION__,__LINE__);
        if(!ctrl_dfd_reopened) ctrl_epoll_del( );
    }
    else if(events & EPOLLIN){
        smcd_command_handler( );
    }
}

static void ctrl_connect_handler(uint32_t events __unused){
    struct sockaddr caddr;
    socklen_t alen;
    struct epoll_event epev;

    if(ctrl_dfd >= 0){
        ctrl_epoll_del( );
        ctrl_dfd_reopened = 1;
    }

    alen = sizeof(caddr);
    ctrl_dfd = accept(ctrl_lfd,&caddr,&alen);

    if(ctrl_dfd < 0){
        ALOGE("In %s line %d, smcd control socket accept failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        return;
    }

    if(bSmcdDebug) ALOGD("In %s line %d, socket connected",__FUNCTION__,__LINE__);
    maxevents++;
    epev.events = EPOLLIN;
    epev.data.ptr = (void *)ctrl_data_handler;
    if(epoll_ctl(epollfd,EPOLL_CTL_ADD,ctrl_dfd,&epev) == -1){
        ALOGE("In %s line %d, epoll_ctl for data connection socket failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        ctrl_data_close();
        return;
    }
}

void mainloop(void){
    while(1){
        struct epoll_event events[maxevents];
        int nevents;
        int i;

        ctrl_dfd_reopened = 0;
        nevents = epoll_wait(epollfd,events,maxevents,-1);

        if(nevents == -1){
            if(errno == EINTR) continue;
        }

        for(i = 0;i < nevents;++i){
            if(events[i].events & EPOLLERR)
                ALOGE("In %s line %d, EPOLLERR on event #%d",__FUNCTION__,__LINE__,i);

            if(events[i].data.ptr) (*(void (*)(uint32_t))events[i].data.ptr)(
                    events[i].events);
        }
    }
}

int connectInit(){
   int ret;
   struct epoll_event epev;
   epollfd = epoll_create(MAX_EPOLL_EVENTS);
    if(epollfd == -1){
        ALOGE("In %s line %d, epoll_create failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        return -1;
    }

    ctrl_lfd = android_get_control_socket("smcd");
    if(ctrl_lfd < 0){
        ALOGE("In %s line %d, get smcd control socket failed",__FUNCTION__,__LINE__);
        return -1;
    }

    ret = listen(ctrl_lfd,1);
    if(ret < 0){
        ALOGE("In %s line %d, smcd control socket listen failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        return -1;
    }

    epev.events = EPOLLIN;
    epev.data.ptr = (void *)ctrl_connect_handler;
    if(epoll_ctl(epollfd,EPOLL_CTL_ADD,ctrl_lfd,&epev) == -1){
        ALOGE("In %s line %d, epoll_ctl for smcd control socket failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        return -1;
    }
    maxevents++;
  return 0;
}