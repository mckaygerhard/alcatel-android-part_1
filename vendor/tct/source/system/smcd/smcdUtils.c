/* Copyright (C) 2016 Tcl Corporation Limited */

#define LOG_TAG "Feedback_smcd"

#include <errno.h>
#include <string.h>
#include <log/log.h>
#include <mntent.h>

#include "smcdUtils.h"

int is_mountpoint_mounted(const char *mp)
{
    int found_mp = 0;
    struct mntent *mentry;
    FILE *fp;

    fp = setmntent("/proc/mounts", "r");
    if (fp == NULL) {
        ALOGE("Error opening /proc/mounts (%s)", strerror(errno));
        return found_mp;
    }
    while ((mentry = getmntent(fp)) != NULL) {
        if (strcmp(mentry->mnt_dir, mp) == 0) {
            found_mp = 1;
            break;
        }
    }
    endmntent(fp);

    return found_mp;
}
