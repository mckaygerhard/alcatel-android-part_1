/* Copyright (C) 2016 Tcl Corporation Limited */

#define LOG_TAG "Feedback_smcd"

#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>
#include <sys/epoll.h>
#include <pthread.h>
#include <cutils/sockets.h>
#include <log/log.h>
#include <dirent.h>
#include "cmd.h"

#ifndef __unused
#define __unused __attribute__((__unused__))
#endif

#define CPU_FREQ_MAX_PATH "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_max_freq"
#define CPU_FREQ_MIN_PATH "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_min_freq"
#define GFX_CLK_PATH "/d/clk/gpu_gx_gfx3d_clk/measure"
#define GFX_CLK_MAX_PATH "/sys/class/kgsl/kgsl-3d0/max_gpuclk"

#define CPU_FREQ_PATH "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq"
#define CPU_TEMP_PATH "/sys/devices/virtual/thermal/thermal_zone%d/temp"
#define CPU_TEMP_TYPE_PATH "/sys/devices/virtual/thermal/thermal_zone%d/type"
#define CPU_BATTERY_TMP "/sys/class/power_supply/battery/temp"
#define BATTERY_CAPACITY "/sys/class/power_supply/battery/capacity"
#define BATTERY_LEVEL "/sys/class/power_supply/battery/system_temp_level"
#define CHARGE_CURRENT "/sys/class/power_supply/battery/current_now"

static volatile bool freq_cpu_catch = false;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void setCPUStatus(int state){
    pthread_mutex_lock(&mutex);
    freq_cpu_catch = state;
    pthread_mutex_unlock(&mutex);
}

/*------------------------------------------------------------------------------
 * Function      : readNodeData(const char *chpPathName,char *chpDataBuffer,const char *chpErrTag,int iDataSize)
 * Description   : Read Node Data,and replace LF('\n') with NUL('\0')
 * Input         :
      const char *chpPathName : Node Path
      char *chpDataBuffer     : The data buffer to store node's data.
      const char *chpErrTag   : The error tag save to the data buffer when read node error.
      int iDataSize           : The size of data to read.
 * Output        :
      char *chpDataBuffer     : The data buffer to store node's data.
 * Return        : Return 0 on success,otherwise,-1 will be return.
------------------------------------------------------------------------------*/
int readNodeData(const char *chpPathName,char *chpDataBuffer,const char *chpErrTag,int iDataSize){
    if (chpPathName == NULL){
        ALOGE("In %s line %d, The argument const char *chpPathName is a NULL object",__FUNCTION__,__LINE__);
        return -1;
    }
    if (chpDataBuffer == NULL){
        ALOGE("In %s line %d, The argument char *chpDataBuffer is a NULL object",__FUNCTION__,__LINE__);
        return -1;
    }
    if (chpErrTag == NULL){
        ALOGE("In %s line %d, The argument const char *chpErrTag is a NULL object",__FUNCTION__,__LINE__);
        return -1;
    }
    if (iDataSize < 1){
        ALOGE("In %s line %d, The argument int iDataSize is too small",__FUNCTION__,__LINE__);
        return -1;
    }
    int fd;
    char chBuffer[iDataSize];
    // init chBuffer
    memset(chBuffer,0,iDataSize);

    if((fd = open(chpPathName,O_RDONLY)) == -1){
        strncat(chpDataBuffer,chpErrTag,LINE_MAX - strlen(chpDataBuffer)-1);
        return -1;
    }else if(read(fd,chBuffer,iDataSize) == -1){
        strncat(chpDataBuffer,chpErrTag,LINE_MAX - strlen(chpDataBuffer)-1);
        if(fd != -1) close(fd);
        return -1;
    }else{
        unsigned int i,iMaxNum;
        iMaxNum = strlen(chBuffer);
        // replace LF with NUL
        for(i = 0; i < iMaxNum;i++){
            if(chBuffer[i] == '\n'){
                chBuffer[i] = '\0';
            }
        }
        strncat(chpDataBuffer,chBuffer,LINE_MAX - strlen(chpDataBuffer)-1);
    }
    if(fd != -1) close(fd);
    return 0;
}

void get_current_date(char *result){
    int last = 0;
    time_t rawtime;
    struct tm *timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    sprintf(result,"%s",asctime(timeinfo));
    last = strlen(result);
    result[last - 1] = ',';
}

int readline_freq_and_temp(char *result,int cpu_number,int thremal_number){
    int i;
    get_current_date(result);

    for(i = 0;i < cpu_number;i++){
        char path[100];
        sprintf(path,CPU_FREQ_PATH,i);
        readNodeData(path,result,"0",20);
        strncat(result,",",LINE_MAX - strlen(result)-1);
    }

    for(i = 0; i < cpu_number; i++) {
        char path[100];
        sprintf(path,CPU_FREQ_MAX_PATH,i);
        readNodeData(path,result,"0",20);
        strncat(result,",",LINE_MAX - strlen(result)-1);
    }

    for(i = 0; i < cpu_number; i++) {
        char path[100];
        sprintf(path,CPU_FREQ_MIN_PATH,i);
        readNodeData(path,result,"0",20);
        strncat(result,",",LINE_MAX - strlen(result)-1);
    }

    readNodeData(GFX_CLK_PATH, result, "NA", 20);
    strncat(result, ",",LINE_MAX - strlen(result)-1);
    readNodeData(GFX_CLK_MAX_PATH, result, "NA", 20);
    strncat(result, ",",LINE_MAX - strlen(result)-1);
    /* MODIFIED-END by wu.yan,BUG-1972923*/

    for(i = 0;i < thremal_number;i++){
        char path[100];
        sprintf(path,CPU_TEMP_PATH,i);
        readNodeData(path,result,"NA",20);
        strncat(result,",",LINE_MAX - strlen(result)-1);
    }

    //battery temp
    readNodeData(CPU_BATTERY_TMP,result,"NA",20);
    strncat(result,",",LINE_MAX - strlen(result)-1);

    readNodeData(BATTERY_CAPACITY,result,"NA",20);
    strncat(result,",",LINE_MAX - strlen(result)-1);

    readNodeData(BATTERY_LEVEL,result,"NA",20);
    strncat(result,",",LINE_MAX - strlen(result)-1);

    readNodeData(CHARGE_CURRENT,result,"NA",20);

    strncat(result,"\n",LINE_MAX - strlen(result)-1);
    return 0;
}

static int is_the_command(char *src,char *dest,int size){
    int i;
    for(i = 0;i < size;i++)
        if(src[i] != dest[i]) return 0;
    return 1;
}

static int is_cpu_file (char *name) {
    return is_the_command(name,"cpu0",4)
            || is_the_command(name,"cpu1",4)
            || is_the_command(name,"cpu2",4)
            || is_the_command(name,"cpu3",4)
            || is_the_command(name,"cpu4",4)
            || is_the_command(name,"cpu5",4)
            || is_the_command(name,"cpu6",4)
            || is_the_command(name,"cpu7",4);
}

static int no_count_rules (char *name) {
    char *temp = name;
    return 1;
}

static int count_file_number (char *file_dir,int (* count_rules)(char *)) {
    DIR* dir = opendir(file_dir);
    int count = 0;
    if (dir == NULL) {
        ALOGE("In %s line %d, Could not open %s",__FUNCTION__,__LINE__,file_dir);
    } else {
        struct dirent* entry;
        while ((entry = readdir(dir))) {
            char* name = entry->d_name;

            if (!strcmp(name, ".") || !strcmp(name, ".."))
                continue;
            if (count_rules(name))
                count++;
        }
    }
    closedir(dir);
    return count;
}

static int format_title (char *title,int cpunumber,int thermalnumber) {
    int i;
    strcat(title,"Time,");

    for(i = 0;i < cpunumber;i++){
        char cpu[10];
        snprintf(cpu,sizeof(cpu),"CPU%d,",i);
        strncat(title,cpu,LINE_MAX-strlen(title)-1);
    }

    /* MODIFIED-BEGIN by wu.yan, 2016-05-27,BUG-1972923*/
    for (i = 0; i < cpunumber; ++i) {
        char cpu[18];
        snprintf(cpu,sizeof(cpu), "CPU%d Max Freq,", i);
        strncat(title, cpu,LINE_MAX-strlen(title)-1);
    }

    for(i = 0; i < cpunumber; ++i) {
        char cpu[18];
        snprintf(cpu,sizeof(cpu), "CPU%d Min Freq,", i);
        strncat(title, cpu,LINE_MAX-strlen(title)-1);
    }
    strncat(title, "GFX clk, GFX max clk,",LINE_MAX-strlen(title)-1);
    /* MODIFIED-END by wu.yan,BUG-1972923*/

    for(i = 0;i < thermalnumber;i++){
        char path[100];
        snprintf(path,sizeof(path),CPU_TEMP_TYPE_PATH,i);
        readNodeData(path,title,"NA",20);
        strncat(title,",",LINE_MAX-strlen(title)-1);
    }
    //battery temp
    strncat(title,"Battery,battery capacity,Battery Level,charging current\n",LINE_MAX-strlen(title)-1);
    return 0;
}

static void get_cpu_temp_thread(char *chpTime){
    char file_name[PATH_MAX];
    char title[LINE_MAX]="";
    int cpu_number = 0;
    int thermal_number = 0;
    int interval = atoi(chpTime+2);
    if(bSmcdDebug) ALOGD("In %s line %d,interval:%d",__FUNCTION__,__LINE__,interval);
    if (interval <= 0){
		free(chpTime);
        return;
    }
    if((interval < 50) || (interval > 10000)){
        ALOGE("In %s line %d, the interval %d is violation, the val should be 50~999, it will be set as 65"
                ,__FUNCTION__,__LINE__,interval);
        interval = 65;
    }

    cpu_number = count_file_number("/sys/devices/system/cpu",is_cpu_file);
    thermal_number = count_file_number("/sys/devices/virtual/thermal",no_count_rules);

    if(format_title(title,cpu_number,thermal_number) < 0){
        ALOGE("In %s line %d, failed to format_title",__FUNCTION__,__LINE__);
    }

    create_dir(CPU_FOLDER);
    if( create_file_name(file_name,"cpu_temp",".csv") < 0 ){
        ALOGE("In %s line %d, failed to create log file",__FUNCTION__,__LINE__);
        free(chpTime);
		return;
    }

    setCPUStatus(1);
    FILE *fp = fopen(file_name,"w+");
    if(fp == NULL) return;
    fwrite(title,sizeof(char),strlen(title),fp);
    if(bSmcdDebug) ALOGD("In %s line %d,interval: %d",__FUNCTION__,__LINE__,interval);
    while(freq_cpu_catch){
        char result[LINE_MAX] = "";
        readline_freq_and_temp(result,cpu_number,thermal_number);
        fwrite(result,sizeof(char),strlen(result),fp);
        usleep(interval * 1000);
    }
    if(fp != NULL) fclose(fp);
    free(chpTime);
    ctrl_data_write(SMC_CPU_LOGGING,'k',
            "Command success,Stop catching temperauter logging");
}

static int CPU_start(char *command){
    char *chpCommand = (char *)malloc(COMMAND_Length_MAX);
    memset(chpCommand,0,COMMAND_Length_MAX);
    if (NULL == command){
        snprintf(chpCommand,COMMAND_Length_MAX,"%c%c%s",SMC_CPU_LOGGING,'s',DEFAULT_CPU_CMD);
    }else strlcpy(chpCommand,command,COMMAND_Length_MAX);
    pthread_t tid_cmd_exec;
    pthread_attr_t attr_cmd_exec;
    int subcmd = command[1];
    if(bSmcdDebug) ALOGD("In %s line %d",__FUNCTION__,__LINE__);
    switch (subcmd){
        case 's': // start
            if( freq_cpu_catch == true ) {
                ctrl_data_write(SMC_CPU_LOGGING,'c',"Command failed,alread run in thread");
                return 0;
            }
            if((pthread_attr_init(&attr_cmd_exec) != 0)
                    || (pthread_attr_setdetachstate(&attr_cmd_exec,
                            PTHREAD_CREATE_DETACHED) != 0)){
            }
            if(pthread_create(&tid_cmd_exec,&attr_cmd_exec,
                    (void *)get_cpu_temp_thread,chpCommand) != 0){
                ALOGE("In %s line %d, create thread get_cpu_temp_thread failed, errno=%d, %s"
                        ,__FUNCTION__,__LINE__,errno,strerror(errno));
                return -1;
            }
            pthread_attr_destroy(&attr_cmd_exec);
            ctrl_data_write(SMC_CPU_LOGGING,'s',"Command success,logging start");
            break;
        case 'k': // stop
            setCPUStatus(0);
            ctrl_data_write(SMC_CPU_LOGGING,command[1],"temperature command has been stopped");
            break;
        case 'c':
            if(freq_cpu_catch){
                ctrl_data_write(SMC_CPU_LOGGING,command[1],CMDRUNNING);
            }else{
                ctrl_data_write(SMC_CPU_LOGGING,command[1],"logging stop");
            }
            break;
    default:
        break;
    }

    return 0;
}

static int CPU_stop(char *command){
  setCPUStatus(0);
  ctrl_data_write(SMC_CPU_LOGGING,command[1],"temperature command has been stopped");
  return 0;
}

static int CPU_getState(char *command){
    if(freq_cpu_catch){
        ctrl_data_write(SMC_GET_STATUS,command[1],CMDRUNNING);
    }else{
        ctrl_data_write(SMC_GET_STATUS,command[1],CMDSTOPPED);
    }
   return 0;
}

cmdHandler CPUHandler={
  .start=CPU_start,
  .stop =CPU_stop,
  .getState = CPU_getState
};
