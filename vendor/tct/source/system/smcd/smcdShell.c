/* Copyright (C) 2016 Tcl Corporation Limited */

#define LOG_TAG "Feedback_smcd"

#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <pthread.h>
#include <cutils/sockets.h>
#include <log/log.h>
#include "cmd.h"

static int is_the_command(char *src,char *dest,int size){
    int i;
    for(i = 0;i < size;i++)
        if(src[i] != dest[i]) return 0;
    return 1;
}
//we only allow following command
int is_allowed_command(char *command){

    return is_the_command(command,"echo ",5)
            || is_the_command(command,"cat ",4)
            || is_the_command(command,"ps ",3)
            || is_the_command(command,"cp ",3)
            || is_the_command(command,"mkdir ",6)
            || is_the_command(command,"dumpsys ",8)
            || is_the_command(command,"top ",4)
            || is_the_command(command,"bugreport ",10)
            || is_the_command(command,"iwpriv ",7)
            || is_the_command(command,"mv ",3)
            || is_the_command(command,"logcat ",7)
            || is_the_command(command,"dmesg ",6)
            || is_the_command(command,"diag_mdlog ",11);
}

void shell_cmd_execute(char *command){
    int result;
    char response[LINE_MAX];

    char cTempCMD[PATH_MAX] = {0};
    strncpy(cTempCMD,command,sizeof(cTempCMD));
    cTempCMD[ strlen(cTempCMD) ] = '\0';
    if(cTempCMD[0]==SMC_BUGREPORT){  //for bugreport command
        create_dir(BUGREPORT_FOLDER);
        char file[PATH_MAX];
        if( create_file_name(file,&cTempCMD[2],"bugreport.txt") < 0 ){
            ALOGE("In %s line %d, failed to create file",__FUNCTION__,__LINE__);
            return;
        }
        strncat(cTempCMD,"> ",sizeof(cTempCMD)-strlen(cTempCMD)-1);
        if( (strlen(cTempCMD) + strlen(file)) > PATH_MAX ){
            ALOGE("In %s line %d, file path %s is too long",__FUNCTION__,__LINE__,file);
            free(command);
            return;
        }
        strncat(cTempCMD,file,sizeof(cTempCMD)-strlen(cTempCMD)-1);
        if(bSmcdDebug) ALOGD("In %s line %d, running bugreport command: %s",__FUNCTION__,__LINE__,&cTempCMD[2]);
    }
    memset(response,0,sizeof(response));
    if(bSmcdDebug) ALOGD("In %s line %d, exec command: %s",__FUNCTION__,__LINE__,&cTempCMD[2]);
    if(is_allowed_command(&cTempCMD[2])){
        result = system(&cTempCMD[2]);
        if(bSmcdDebug) ALOGD("In %s line %d, system.command:%s,Result:%d",__FUNCTION__,__LINE__,&cTempCMD[2],result);
        if(WEXITSTATUS(result) == 0) strcpy(response,"Command success");
        else{
            strcpy(response,"Command Failed,exec failed ");
            strncat(response,&cTempCMD[2],sizeof(response) - strlen(response)-1);
            ALOGE("In %s line %d, Command Failed,exec failed %s",__FUNCTION__,__LINE__,&cTempCMD[2]);
        }
    }
    else{
        strcpy(response,"Command Failed,not allowed command: ");
        strncat(response,&cTempCMD[2],sizeof(response) - strlen(response)-1);

    }
    if((result = ctrl_data_write(cTempCMD[0],cTempCMD[1],response)) == -1){
        ALOGE("In %s line %d, control data socket write failed",__FUNCTION__,__LINE__);
    }
    free(command);
}

static int shell_start(char *command ){
    if (NULL == command){
        ALOGE("In %s line %d,command can't be NULL",__FUNCTION__,__LINE__);
        return -1;
    }
    pthread_t tid_cmd_exec = 0;
    char *chpCommand = (char *)malloc(COMMAND_Length_MAX);
    memset(chpCommand,0,COMMAND_Length_MAX);
    strncpy(chpCommand,command,COMMAND_Length_MAX);
    strncat(chpCommand,"\0",COMMAND_Length_MAX-strlen(chpCommand)-1);
    pthread_attr_t attr_cmd_exec;
    if((pthread_attr_init(&attr_cmd_exec) != 0)
            || (pthread_attr_setdetachstate(&attr_cmd_exec,
                    PTHREAD_CREATE_DETACHED) != 0)){
    }
    if(pthread_create(&tid_cmd_exec,&attr_cmd_exec,(void *)shell_cmd_execute,chpCommand)
            != 0){
        ALOGE("In %s line %d, create pthread shell_cmd_execute failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        return -2;
    }
    pthread_attr_destroy(&attr_cmd_exec);
    return 0;
}

static int shell_stop(){
  return 0;
}

static int shell_getState(){
   return 0;
}
cmdHandler shellHandler={
  .start=shell_start,
  .stop =shell_stop,
  .getState = shell_getState
};