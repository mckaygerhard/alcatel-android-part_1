/* Copyright (C) 2016 Tcl Corporation Limited */
#define LOG_TAG "Feedback_smcd"

#include <errno.h>
#include <signal.h>
#include <sys/epoll.h>
#include <sys/wait.h>
#include <cutils/sockets.h>
#include <log/log.h>
#include "cmd.h"

#define TZ_QSEE_LOG  "cat /sys/kernel/debug/tzdbg/qsee_log"
#define TZ_LOG       "cat /sys/kernel/debug/tzdbg/log"

static  pid_t tz_qsee_pid=-1;
static  pid_t tz_pid=-1;

static void tzlog_exec(){
 //    id_t pid;
    tz_qsee_pid = fork( );
    if(tz_qsee_pid == 0){
        //tz_qsee_pid= getpid();
        if(tz_qsee_pid<0){
            ALOGE("In %s line %d, get tz_qsee_pid failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
            ctrl_data_write(SMC_TZ_LOG,'s',
                "Command Failed,cat qsee_log failed");
            return ;
        }
        char logFile[PATH_MAX];
        char cmd[PATH_MAX];
        if( create_file_name(logFile,"tzdbg","_qsee_log.txt") < 0 ){
            ALOGE("In %s line %d, failed to create file",__FUNCTION__,__LINE__);
            return;
        }
        snprintf(cmd,sizeof(cmd),TZ_QSEE_LOG " >%s",logFile);
        if(bSmcdDebug) ALOGD("In %s line %d, tzdbg qsee command pid=%d  cmd=%s"
                ,__FUNCTION__,__LINE__,getpid(),cmd);
        execl("/system/bin/sh","sh","-c",cmd,(char*)0);
    }
    tz_pid = fork( );  //for tz logFile
    if(tz_pid == 0){
          //tz_pid=getpid();
          if(tz_pid<0){
               ALOGE("In %s line %d, get tz_pid failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
               ctrl_data_write(SMC_TZ_LOG,'s',
                    "Command Failed,cat tz_log failed");
               return;
           }
           char logFile[PATH_MAX];
           char cmd[PATH_MAX];
           if( create_file_name(logFile,"tzdbg","_tz_log.txt") < 0 ){
                ALOGE("In %s line %d, failed to create file",__FUNCTION__,__LINE__);
                return;
            }
           snprintf(cmd,sizeof(cmd),TZ_LOG " >%s",logFile);
           if(bSmcdDebug) ALOGD("In %s line %d, tzdbg tz command pid=%d  cmd=%s"
                    ,__FUNCTION__,__LINE__,getpid(),cmd);
           execl("/system/bin/sh","sh","-c",cmd,(char*)0);
     }
     ctrl_data_write(SMC_TZ_LOG,'s',
                    "Command success,qsee logging start");
   // return;
}

static int TZ_stop(){
  int stat;
  if(bSmcdDebug) ALOGD("In %s line %d, tz stop %d %d",__FUNCTION__,__LINE__,tz_qsee_pid,tz_pid);
  if(tz_qsee_pid>0){
      kill(tz_qsee_pid,SIGKILL);
      waitpid(tz_qsee_pid, &stat, WUNTRACED);
      tz_qsee_pid=-1;
  }
   if(tz_pid>0){
      kill(tz_pid,SIGKILL);
      waitpid(tz_pid, &stat, WUNTRACED);
      tz_pid=-1;
  }

  return 1;
}

static int TZ_getState(char *command){
    if (strlen(command) < 2){
        return -1;
    }
   if(tz_qsee_pid>0 || tz_pid>0){
       ctrl_data_write(SMC_GET_STATUS,command[1],CMDRUNNING);
       return 1;
   }
   else {
       ctrl_data_write(SMC_GET_STATUS,command[1],CMDSTOPPED);
       return 0;
   }
}

int TZ_start(char * command){
    if (strlen(command) < 2){
        return -1;
    }
    int subcmd = command[1];
    int result;
    if(bSmcdDebug) ALOGD("In %s line %d, get command %s",__FUNCTION__,__LINE__,command);

    create_dir(TZ_FOLDER);

    switch(subcmd){
        case 's':  //start
            tzlog_exec( );

            break;
        case 'k':   //stop
            TZ_stop();
            break;
        case 'c':   //check status
            if(tz_pid>0 || tz_qsee_pid>0){
                ctrl_data_write(SMC_TZ_LOG,'c',
                        "Command success,logging running");
            }
            else{
                ctrl_data_write(SMC_TZ_LOG,'c',
                        "Command success,logging stopped");
            }
            break;
    }
  return 0;
}

cmdHandler TZHandler={
  .start=TZ_start,
  .stop =TZ_stop,
  .getState = TZ_getState
};