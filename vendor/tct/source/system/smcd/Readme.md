*Copyright (C) 2016 Tcl Corporation Limited*
# smcd 编程规则

## 路径，文件规则
1. 路径变量值或者宏应该以‘/’结尾，如
```c
    #define DEFAULT_DIRECTORY   "/sdcard/BugReport/"
```
使用时直接进行字符串拼接即可。
```c
    sprintf(oFile,"%s%sV%s_%s_kmsg_current.txt",logPath,folder,build_id,tStr);
```

