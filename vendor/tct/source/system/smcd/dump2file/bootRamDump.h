#ifndef BOOT_RAW_PARTITION_RAMDUMP_H
#define BOOT_RAW_PARTITION_RAMDUMP_H

/*===========================================================================

                         Boot Raw Partition RAM Dump Header File

GENERAL DESCRIPTION
  This header file contains declarations and definitions for memory
  dumps to raw parition.

Copyright 2013, 2014 by Qualcomm Technologies, Inc.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/20/14   kedara  Support 64bit compilation using llvm.
07/10/13   dh      Move some defines from c file
03/19/13   dh      Initial version . 

============================================================================*/

/*===========================================================================

                           INCLUDE FILES

===========================================================================*/
#include <stdint.h>

/*===========================================================================

                      PUBLIC DATA DECLARATIONS

===========================================================================*/

#define SECTION_NAME_LEN 20 
#define RAM_DUMP_VALID_MASK 0x00000001
#define RAM_DUMP_INSUFFICIENT_STORAGE_MASK 0x00000002
#define RAM_DUMP_HEADER_VER          0x00001000
#define RAM_DUMP_SECTION_HEADER_VER  0x00001000
#define RAM_DUMP_HEADER_SIGNATURE    {0x52,0x61,0x77,0x5F,0x44,0x6D,0x70,0x21}
/**
 * This struct represents the header of the whole raw parition ram dump
 * size is 56 bytes
 */
struct boot_raw_parition_dump_header
{
  /* Signature indicating presence of ram dump */
  uint8_t signature[8];
  
  /* Version number, should be 0x1000*/
  uint32_t version;
  
  /* bit 0: dump valid
     bit 1: insufficant storage 
     bit 31:2 reserved, should be 0 */
  uint32_t validity_flag;
  
  /* Not used by SBL ram dump */
  uint64_t os_data;
  
  /* Not used by SBL ram dump */
  uint8_t cpu_context[8];
  
  /* Not used by SBL ram dump */
  uint32_t reset_trigger;
  
  /* Total size of the actual dump including headers */
  uint64_t dump_size;
  
  /* Total size required */
  uint64_t total_dump_size_required;
  
  /* Number of sections included in this dump */
  uint32_t sections_count;
} __attribute__((packed));


/**
 * Enum indicating the type of data in each dump section
 */
typedef enum
{
  RAW_PARITION_DUMP_RESERVED = 0,
  
  /* Device memory dump */
  RAW_PARITION_DUMP_DDR_TYPE      = 1, 
  
  /* CPU context, not used */
  RAW_PARITION_DUMP_CPU_CXT_TYPE  = 2,
  
  /* Silicon Vendor specific data */
  RAW_PARITION_DUMP_SV_TYPE      = 3,
  
  /* Force it to uint32 size */
  RAW_PARITION_DUMP_MAX      = 0x7FFFFFFF
  
}boot_raw_partition_dump_section_type;


/* Type specifc information. It's part of section header*/
union type_specific_info
{
  /* Base address of a device memory dump */
  uint64_t base_addr;
  
  /* Not used by SBL dump */
  uint8_t cpu_content[6];
  
  /* Not used by SBL dump */
  uint8_t sv_specific[16];
} __attribute__((packed));

/**
 * This struct represents the header of one ram dump section
 *  size is 64 bytes
 */
struct boot_raw_partition_dump_section_header
{
  /* bit 0: dump valid
     bit 1: insufficant storage 
     bit 31:2 reserved, should be 0 */
  uint32_t validity_flag;
  
  /*Version number*/
  uint32_t section_version;
  
  /* Type of this section */
  boot_raw_partition_dump_section_type section_type;

  /* Byte offset to the start of this section's data */
  uint64_t section_offset;
  
  /* Total size of the section's data */
  uint64_t section_size;
  
  /* Type specific information, 
     we use it to store base address of device memory*/
  union type_specific_info section_info;
  
  /* Name of this section */
  uint8_t section_name[SECTION_NAME_LEN];
} __attribute__((packed));


/* Max number of raw dump sections we support */
#define MAX_RAW_DUMP_SECTION_NUM 20

/* Size of the overall header */
#define DUMP_HEADER_SIZE  (sizeof(struct boot_raw_parition_dump_header))

/* Size of a single section header */
#define SECTION_HEADER_SIZE  \
                  (sizeof(struct boot_raw_partition_dump_section_header))

#endif  /* BOOT_RAW_PARTITION_RAMDUMP_H */

