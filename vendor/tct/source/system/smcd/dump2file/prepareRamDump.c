/* Copyright (C) 2016 Tcl Corporation Limited */
#define _LARGEFILE64_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <private/android_filesystem_config.h>

#include "f2bs.h"
#include "bootRamDump.h"
#include "dump2file.h"

#define MMCBLKP "/dev/block/bootdevice/by-name/traceability"
#define MMCBLKP_OFFSET (0x1200 + 0x8)
#define RAW_PARTITION_RAMDUMP_SIZE 4362076160UL /* (4 * 1024 + 64) MB */
#define WRITE_BUFF_SIZE 0x200000 //2MB

static void usage(const char *prog) {
    fprintf(stderr, "usage: %s <filepath> [size]\n", prog);
    fprintf(stderr, "       %s -c\n", prog);
}

static int create_and_fill(const char *path, uint64_t size) {
    int fd;
    if ((fd = open(path, O_CREAT | O_WRONLY | O_LARGEFILE, 0600)) < 0) {
        fprintf(stderr, "failed to create %s: %s\n", path, strerror(errno));
        return -1;
    }
    fchown(fd, AID_SYSTEM, AID_SYSTEM);

#if 1 //always wrong blocks, why? TODO use O_LARGEFILE when open?
    fallocate64(fd, 0, 0, size);
    char buff[DUMP_HEADER_SIZE];
    memset(buff, 0xE9, DUMP_HEADER_SIZE);
    if (write(fd, buff, DUMP_HEADER_SIZE) == -1) {
        fprintf(stderr, "failed to write %s: %s\n", path, strerror(errno));
        close(fd);
        return -1;
    }
#else
    char buff[WRITE_BUFF_SIZE];
    memset(buff, 0xE9, WRITE_BUFF_SIZE);
    while (size > WRITE_BUFF_SIZE) {
        if (write(fd, buff, WRITE_BUFF_SIZE) == -1) {
            fprintf(stderr, "failed to write %s: %s\n", path, strerror(errno));
            close(fd);
            return -1;
        }
        size -= WRITE_BUFF_SIZE;
    }
    if (size > 0) {
        if (write(fd, buff, size) == -1) {
            fprintf(stderr, "failed to write %s: %s\n", path, strerror(errno));
            close(fd);
            return -1;
        }
    }
#endif

    fsync(fd);
    close(fd);

    return 0;
}

static int write_to_partition(struct f2bs_header *header,
        struct f2bs_range ranges_table[]) {
    int fd;
    if ((fd = open(MMCBLKP, O_WRONLY)) < 0) {
        fprintf(stderr, "failed to open %s for writing: %s\n", MMCBLKP,
                strerror(errno));
        return -1;
    }

    if (ranges_table != NULL) {
        /* Write ranges_table firstly */
        lseek64(fd, MMCBLKP_OFFSET + F2BS_HEADER_SIZE, SEEK_SET);
        if (write(fd, ranges_table, header->ranges * F2BS_RANGE_SIZE) == -1) {
            fprintf(stderr, "failed to write %s: %s\n", MMCBLKP, strerror(errno));
            close(fd);
            return -1;
        }
    }

    /* Write header later */
    lseek64(fd, MMCBLKP_OFFSET, SEEK_SET);
    if (write(fd, header, F2BS_HEADER_SIZE) == -1) {
        fprintf(stderr, "failed to write %s: %s\n", MMCBLKP, strerror(errno));
        close(fd);
        return -1;
    }

    fsync(fd);
    close(fd);

    return 0;
}

int do_prepare(const char *path, uint64_t size) {
    if (size == 0) {
        fprintf(stderr, "warning: size=0, set default value %"PRIu64"\n",
                RAW_PARTITION_RAMDUMP_SIZE);
        size = RAW_PARTITION_RAMDUMP_SIZE;
    }
    struct stat sb;
    if (stat(path, &sb) == -1) {
        if (errno == ENOENT) { //no such file, so create it
            /* Allocate space and fill */
            if (create_and_fill(path, size) != 0) {
                fprintf(stderr, "failed to create %s: %s\n", path,
                        strerror(errno));
                return -1;
            }
            /* Stat again */
            stat(path, &sb);
        } else { //other errors
            fprintf(stderr, "failed to stat %s: %s\n", path, strerror(errno));
            return -1;
        }
    }
    if (!S_ISREG(sb.st_mode)) { //check type
        fprintf(stderr, "%s is not a regular file\n", path);
        return -1;
    }
    if ((uint64_t) sb.st_size != size) { //check size
        fprintf(stderr, "file_size=%"PRIu64", not equals to %"PRIu64"\n",
                sb.st_size, size);
        /* Unlink existing file */
        if (unlink(path) != 0) {
            fprintf(stderr, "failed to unlink %s: %s\n", path,
                    strerror(errno));
            return -1;
        }
        /* Allocate space and fill */
        if (create_and_fill(path, size) != 0) {
            fprintf(stderr, "failed to create %s: %s\n", path,
                    strerror(errno));
            return -1;
        }
    }

    struct f2bs_header header;
    struct f2bs_range ranges_table[F2BS_MAX_RANGES];
    if (f2bs_find_blocks(path, &header, ranges_table, NULL) != 0) {
        fprintf(stderr, "failed to find blocks %s\n", path);
        return -1;
    }
    f2fs_dump_blocks_info(&header, ranges_table, NULL);

    return write_to_partition(&header, ranges_table);
}

int do_clean() {
    struct f2bs_header header;
    memset((void *) &header, 0, F2BS_HEADER_SIZE);
    return write_to_partition(&header, NULL);
}

#ifdef BUILD_STANDALONE
int main(int argc, char *argv[]) {
    if (argc != 2 && argc != 3) {
        usage(argv[0]);
        return -1;
    }

    if (argc == 2 && strcmp(argv[1], "-c") == 0) {
        return do_clean();
    }

    return do_prepare(argv[1],
            (argc == 2) ? RAW_PARTITION_RAMDUMP_SIZE : atoll(argv[2]));
}
#endif /* BUILD_STANDALONE */
