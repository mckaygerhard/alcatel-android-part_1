/* Copyright (C) 2016 Tcl Corporation Limited */

#define _LARGEFILE64_SOURCE

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#include "tar.h"

#define READ_BUFF_SIZE 0x1000 //4K

struct tar_hdr {
    char name[100], mode[8], uid[8], gid[8], size[12], mtime[12], chksum[8],
            type, link[100], magic[8], uname[32], gname[32], major[8], minor[8],
            prefix[155], padd[12];
};


static void itoo(char *str, int len, off_t val) {
    char *t, tmp[sizeof(off_t) * 3 + 1];
    int cnt = sprintf(tmp, "%0*llo", len, (unsigned long long) val);

    t = tmp + cnt - len;
    if (*t == '0')
        t++;
    memcpy(str, t, len);
}

void tar_add_file(int fd, const char *fname, unsigned char *addr, int read_fd,
        uint64_t offset, uint64_t size) {
    struct tar_hdr hdr;
    unsigned int i;

    memset(&hdr, 0, sizeof(hdr));
    strncpy(hdr.name, fname, sizeof(hdr.name));
    itoo(hdr.mode, sizeof(hdr.mode), 0666 & 07777);
    itoo(hdr.uid, sizeof(hdr.uid), 0);
    itoo(hdr.gid, sizeof(hdr.gid), 0);
    itoo(hdr.size, sizeof(hdr.size), size);
    itoo(hdr.mtime, sizeof(hdr.mtime), 0);
    for (i = 0; i < sizeof(hdr.chksum); i++) {
        hdr.chksum[i] = ' ';
    }
    hdr.type = '0'; //regular file
    strcpy(hdr.magic, "ustar  ");
    snprintf(hdr.uname, sizeof(hdr.uname), "%d", 0);
    snprintf(hdr.gname, sizeof(hdr.gname), "%d", 0);

    //calculate chksum.
    unsigned int sum = 0;
    for (i = 0; i < 512; i++) {
        sum += (unsigned int) ((char*) &hdr)[i];
    }
    itoo(hdr.chksum, sizeof(hdr.chksum) - 1, sum);

    char buf[512];
    memset(buf, 0, sizeof(buf));

    write(fd, (void *) &hdr, 512);
    if (addr != NULL) {
        while (size > READ_BUFF_SIZE) {
            write(fd, (void *) (addr + offset), READ_BUFF_SIZE);
            size -= READ_BUFF_SIZE;
            offset += READ_BUFF_SIZE;
        }
        write(fd, (void *) (addr + offset), size);
        if (size % 512) {
            write(fd, buf, (512 - (size % 512)));
        }
    } else if (read_fd >= 0) {
        uint8_t buffer[READ_BUFF_SIZE];
        lseek64(read_fd, offset, SEEK_SET);
        while (size > READ_BUFF_SIZE) {
            read(read_fd, buffer, READ_BUFF_SIZE);
            write(fd, buffer, READ_BUFF_SIZE);
            size -= READ_BUFF_SIZE;
        }
        read(read_fd, buffer, size);
        write(fd, buffer, size);
        if (size % 512) {
            write(fd, buf, (512 - (size % 512)));
        }
    }
}

void tar_append_eof(int fd) {
    char buf[1024];
    memset(buf, 0, sizeof(buf));
    write(fd, buf, 1024);
}
