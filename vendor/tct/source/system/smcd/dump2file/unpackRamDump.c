/* Copyright (C) 2016 Tcl Corporation Limited */
#define _LARGEFILE64_SOURCE

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <linux/fs.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/mman.h>
#include <signal.h>
#include <getopt.h>

#include "f2bs.h"
#include "tar.h"
#include "bootRamDump.h"

#define MMCBLKP_USERDATA "/dev/block/bootdevice/by-name/userdata"
#define READ_BUFF_SIZE 0x200000 //2MB

#define FLAG_CHECK      0x0001
#define FLAG_COMPRESS   0x0002
#define FLAG_PERSIST    0x0004

struct mmap_t {
    void *addr;
    uint64_t length;
};

static void usage(const char *prog) {
    fprintf(stderr, "usage: %s <filepath> -z <dump.tgz>\n", prog);
    fprintf(stderr, "       %s <filepath> -d <outdir>\n", prog);
#ifndef BUILD_HOST
    fprintf(stderr, "       %s <filepath> -c\n", prog);
#endif /* BUILD_HOST */
}

static void dump_rawdump_info(
        struct boot_raw_parition_dump_header *raw_dump_header,
        struct boot_raw_partition_dump_section_header raw_dump_section_header_table[]) {
    unsigned int i;
    fprintf(stderr, "raw_dump_header.signature=");
    for (i = 0; i < 8; ++i) {
        fprintf(stderr, "%c", raw_dump_header->signature[i]);
    }
    fprintf(stderr, "\n");
    fprintf(stderr, "raw_dump_header.version=0x%08x\n",
            raw_dump_header->version);
    fprintf(stderr, "raw_dump_header.validity_flag=0x%08x\n",
            raw_dump_header->validity_flag);
    fprintf(stderr, "raw_dump_header.total_dump_size_required=%lu\n",
            raw_dump_header->total_dump_size_required);
    fprintf(stderr, "raw_dump_header.sections_count=%u\n",
            raw_dump_header->sections_count);

    if (raw_dump_section_header_table != NULL) {
        for (i = 0; i < raw_dump_header->sections_count; ++i) {
            fprintf(stderr, "[%d].validity_flag=0x%08x\n", i,
                    raw_dump_section_header_table[i].validity_flag);
            fprintf(stderr, "[%d].section_version=0x%08x\n", i,
                    raw_dump_section_header_table[i].section_version);
            fprintf(stderr, "[%d].section_type=0x%08x\n", i,
                    raw_dump_section_header_table[i].section_type);
            fprintf(stderr, "[%d].section_offset=%lu\n", i,
                    raw_dump_section_header_table[i].section_offset);
            fprintf(stderr, "[%d].section_size=%lu\n", i,
                    raw_dump_section_header_table[i].section_size);
            fprintf(stderr, "[%d].section_name=%s\n", i,
                    raw_dump_section_header_table[i].section_name);
        }
    }
}

static int dump_rawdump_section(void *addr, const char *outdir,
        struct boot_raw_partition_dump_section_header *section) {
    char fname[256];
    memset(fname, 0, sizeof(fname));
    strcpy(fname, outdir);
    strcat(fname, "/");
    strcat(fname, (char *) section->section_name);

    int fd;
    if ((fd = open(fname, O_WRONLY | O_CREAT, 0644)) < 0) {
        fprintf(stderr, "create %s failed: %s\n", section->section_name,
                strerror(errno));
        return -1;
    }

    uint64_t total_size = section->section_size;
    uint64_t offset = section->section_offset;
    while (total_size > READ_BUFF_SIZE) {
        if (write(fd, (unsigned char *) addr + offset, READ_BUFF_SIZE) == -1) {
            fprintf(stderr, "failed to write %s: %s\n", fname, strerror(errno));
            close(fd);
            return -1;
        }
        total_size -= READ_BUFF_SIZE;
        offset += READ_BUFF_SIZE;
    }
    if (total_size > 0) {
        if (write(fd, (unsigned char *) addr + offset, total_size) == -1){
            fprintf(stderr, "failed to write %s: %s\n", fname, strerror(errno));
            close(fd);
            return -1;
        }
    }

    close(fd);

    return 0;
}

static int compress_stream(const char *path) {
    int fd;
    if ((fd = open(path, O_CREAT | O_WRONLY, 0644)) < 0) {
        fprintf(stderr, "failed to create %s: %s\n", path, strerror(errno));
        return -1;
    }

    int pipefd[2];
    pid_t cpid;

    pipe(pipefd);
    signal(SIGPIPE, SIG_IGN);

    cpid = fork();
    if (cpid == -1) {
        fprintf(stderr, "failed to fork: %s\n", strerror(errno));
        return -1;
    }

    if (!cpid) { /* Child reads from pipe */
        char *argv[] = { "gzip", "-f", NULL };
        close(pipefd[1]); /* Close unused write*/
        dup2(pipefd[0], 0);
        dup2(fd, 1); //write to tar fd
        execvp(argv[0], argv);
    } else {
        close(pipefd[0]); /* Close unused read end */
        dup2(pipefd[1], fd); //write to pipe
    }

    return fd;
}

static int compress_ramdump(unsigned char *addr, int read_fd,
        const char *path /* int archive_fd */,
        struct boot_raw_parition_dump_header *raw_dump_header,
        struct boot_raw_partition_dump_section_header raw_dump_section_header_table[]) {
    int archive_fd = compress_stream(path);
    if (archive_fd < 0) {
        fprintf(stderr, "failed to compress %s: %s\n", path, strerror(errno));
        return -1;
    }

    unsigned int i;
    for (i = 0; i < raw_dump_header->sections_count; ++i) {
        tar_add_file(archive_fd, (char *) raw_dump_section_header_table[i].section_name,
                addr, read_fd, raw_dump_section_header_table[i].section_offset,
                raw_dump_section_header_table[i].section_size);
    }
    tar_append_eof(archive_fd);

    close(archive_fd);

    return 0;
}

#ifdef BUILD_HOST
static int mmap_raw_ramdump(const char *path, struct mmap_t *mm) {
    int fd;
    if ((fd = open(path, O_RDONLY)) < 0) {
        fprintf(stderr, "failed to open %s for reading: %s\n", path,
                strerror(errno));
        return -1;
    }
    struct stat sb;
    if (fstat(fd, &sb) != 0) {
        fprintf(stderr, "failed to stat %s: %s\n", path, strerror(errno));
        return -1;
    }
    if (!S_ISREG(sb.st_mode)) {
        fprintf(stderr, "%s is not a regular file\n", path);
        return -1;
    }

    /* Fill mm */
    mm->addr = mmap64(NULL, sb.st_size, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0);
    if (MAP_FAILED == mm->addr) {
        fprintf(stderr, "failed to map %s: %s\n", path, strerror(errno));
        close(fd);
        return -1;
    }
    mm->length = (uint64_t) sb.st_size;

    close(fd);

    return 0;
}
#else /* !BUILD_HOST */
static int mmap_raw_ramdump(const char *path, struct mmap_t *mm) {
    struct f2bs_header header;
    struct f2bs_range ranges_table[F2BS_MAX_RANGES];
    if (f2bs_find_blocks(path, &header, ranges_table, NULL) != 0) {
        fprintf(stderr, "failed to find blocks %s\n", path);
        return -1;
    }
    f2fs_dump_blocks_info(&header, ranges_table, NULL);

    /* Read data from blocks under userdata partition */
    int fd;
    if ((fd = open(MMCBLKP_USERDATA, O_RDONLY)) < 0) {
        fprintf(stderr, "failed to open %s for reading: %s\n", MMCBLKP_USERDATA,
                strerror(errno));
        return -1;
    }

    mm->addr = f2bs_ranges_mmap(&header, ranges_table, fd);
    if (NULL == mm->addr) {
        fprintf(stderr, "failed to map %s: %s\n", path, strerror(errno));
        close(fd);
        return -1;
    }
    mm->length = (uint64_t) header.blocks * header.blksize;

    close(fd);

    return 0;
}
#endif /* BUILD_HOST */

static int munmap_raw_ramdump(struct mmap_t *mm) {
    return munmap(mm->addr, mm->length);
}

static int clean_ramdump_header(const char *path) {
    int fd = open(path, O_WRONLY);
    char buff[DUMP_HEADER_SIZE];
    memset(buff, 0xE9, DUMP_HEADER_SIZE);
    if (write(fd, buff, DUMP_HEADER_SIZE) == -1) {
        fprintf(stderr, "failed to write %s: %s\n", path, strerror(errno));
        close(fd);
        return -1;
    }
    fsync(fd);
    close(fd);
    return 0;
}

#ifndef BUILD_HOST
int do_check_ramdump(const char *path) {
#if 0
    struct mmap_t mm;
    if (mmap_raw_ramdump(path, &mm) != 0) {
        fprintf(stderr, "map raw ramdump failed\n");
        return -1;
    }

    /* Read header */
    struct boot_raw_parition_dump_header raw_dump_header;
    memcpy((void *) &raw_dump_header, mm.addr, DUMP_HEADER_SIZE);
    /* Check signature and flag */
    uint8_t ram_dump_signature[] = RAM_DUMP_HEADER_SIGNATURE;
    if (memcmp(raw_dump_header.signature, ram_dump_signature, 8)
            || raw_dump_header.validity_flag != RAM_DUMP_VALID_MASK) {
        fprintf(stderr, "invalid ramdump header\n");
        dump_rawdump_info(&raw_dump_header, NULL);
        munmap_raw_ramdump(&mm);
        return -1;
    }

    dump_rawdump_info(&raw_dump_header, NULL);
    munmap_raw_ramdump(&mm);

    return 0;
#else
    int fd;
    if ((fd = open(path, O_RDONLY)) < 0) {
        fprintf(stderr, "failed to open %s for reading: %s\n", path,
                strerror(errno));
        return -1;
    }

    /* Read header */
    char buff[DUMP_HEADER_SIZE];
    if (read(fd, buff, DUMP_HEADER_SIZE) == -1) {
        fprintf(stderr, "failed to read %s: %s\n", path, strerror(errno));
        return -1;
    }
    close(fd);

    unsigned int i;
    for (i = 0; i < DUMP_HEADER_SIZE; ++i) {
        if ((int) buff[i] != 0xE9) { //after unpack ramdump, set header data to 0xE9
            return 0;
        }
    }
    return -1;
#endif
}
#endif /* BUILD_HOST */

/**
 * Unpack files from raw ramdump, to a directory or compressed .tgz.
 * Return 0 on success, -1 on failure.
 */
int do_unpack(const char *path, const char *path2, int flags) {
    struct mmap_t mm;
    if (mmap_raw_ramdump(path, &mm) != 0) {
        fprintf(stderr, "map raw ramdump failed\n");
        return -1;
    }

    /* Read header */
    struct boot_raw_parition_dump_header raw_dump_header;
    memcpy((void *) &raw_dump_header, mm.addr, DUMP_HEADER_SIZE);
    /* Check signature and flag */
    uint8_t ram_dump_signature[] = RAM_DUMP_HEADER_SIGNATURE;
    if (memcmp(raw_dump_header.signature, ram_dump_signature, 8)
            || raw_dump_header.validity_flag != RAM_DUMP_VALID_MASK) {
        fprintf(stderr, "invalid ramdump header\n");
        dump_rawdump_info(&raw_dump_header, NULL);
        munmap_raw_ramdump(&mm);
        return -1;
    }

    /* Read section headers */
    struct boot_raw_partition_dump_section_header raw_dump_section_header_table[MAX_RAW_DUMP_SECTION_NUM];
    memcpy((void *) &raw_dump_section_header_table[0],
            (void *) ((unsigned char *) mm.addr + DUMP_HEADER_SIZE),
            SECTION_HEADER_SIZE * raw_dump_header.sections_count);
    dump_rawdump_info(&raw_dump_header, raw_dump_section_header_table);

    if (flags & FLAG_COMPRESS) {
        compress_ramdump(mm.addr, -1, path2, &raw_dump_header,
                raw_dump_section_header_table);
    } else {
        unsigned int i;
        for (i = 0; i < raw_dump_header.sections_count; ++i) {
            dump_rawdump_section(mm.addr, path2,
                    &raw_dump_section_header_table[i]);
        }
    }

    munmap_raw_ramdump(&mm);

    /* Finally, clean old raw ramdump header */
#ifndef BUILD_HOST
    if (!(flags & FLAG_PERSIST)) {
        clean_ramdump_header(path);
    }
#endif /* BUILD_HOST */

    return 0;
}

static int dump_rawdump_section_nommap(int read_fd, const char *outdir,
        struct boot_raw_partition_dump_section_header *section) {
    int write_fd;
    uint8_t buffer[READ_BUFF_SIZE];
    uint64_t total_size = section->section_size;

    char fname[256];
    memset(fname, 0, 256);
    strcpy(fname, outdir);
    strcat(fname, "/");
    strcat(fname, (char *) section->section_name);

    if ((write_fd = open(fname, O_WRONLY | O_CREAT, 0644)) < 0) {
        fprintf(stderr, "create %s failed: %s\n", section->section_name,
                strerror(errno));
        return -1;
    }

    lseek64(read_fd, section->section_offset, SEEK_SET);
    while (total_size > READ_BUFF_SIZE) {
        read(read_fd, buffer, READ_BUFF_SIZE);
        write(write_fd, buffer, READ_BUFF_SIZE);
        total_size -= READ_BUFF_SIZE;
    }
    if (total_size > 0) {
        read(read_fd, buffer, total_size);
        write(write_fd, buffer, total_size);
    }

    close(write_fd);

    return 0;
}

/* Since mmap64 need root privilege, so use old solution. */
static int do_unpack_nommap(const char *path, const char *path2, int flags) {
    int read_fd;
    if ((read_fd = open(path, O_RDONLY)) < 0) {
        fprintf(stderr, "failed to open %s for reading: %s\n", path,
                strerror(errno));
        return -1;
    }

    /* Read header */
    struct boot_raw_parition_dump_header raw_dump_header;
    if (read(read_fd, &raw_dump_header, DUMP_HEADER_SIZE) != DUMP_HEADER_SIZE) {
        fprintf(stderr, "read raw_dump_header failed: %s\n", strerror(errno));
        return -errno;
    }
    /* Check signature and flag */
    uint8_t ram_dump_signature[] = RAM_DUMP_HEADER_SIGNATURE;
    if (memcmp(raw_dump_header.signature, ram_dump_signature, 8)
            || raw_dump_header.validity_flag != RAM_DUMP_VALID_MASK) {
        fprintf(stderr, "invalid ramdump header\n");
        dump_rawdump_info(&raw_dump_header, NULL);
        close(read_fd);
        return -1;
    }

    struct boot_raw_partition_dump_section_header raw_dump_section_header_table[MAX_RAW_DUMP_SECTION_NUM];
    if (read(read_fd, raw_dump_section_header_table,
            SECTION_HEADER_SIZE * raw_dump_header.sections_count)
            != SECTION_HEADER_SIZE * raw_dump_header.sections_count) {
        fprintf(stderr, "read raw_dump_section_header_table failed: %s\n",
                strerror(errno));
        return -1;
    }
    /* Read section headers */
    dump_rawdump_info(&raw_dump_header, raw_dump_section_header_table);

    if (flags & FLAG_COMPRESS) {
        compress_ramdump(NULL, read_fd, path2, &raw_dump_header,
                raw_dump_section_header_table);
    } else {
        unsigned int i;
        for (i = 0; i < raw_dump_header.sections_count; ++i) {
            dump_rawdump_section_nommap(read_fd, path2,
                    &raw_dump_section_header_table[i]);
        }
    }

    close(read_fd);

    return 0;
}

#ifdef BUILD_STANDALONE
int main(int argc, char *argv[]) {
    int opt;
    int flags = 0;
    char *path;
    while ((opt = getopt(argc - 1, argv + 1, "cd:nz:")) != -1) {
        switch (opt) {
#ifndef BUILD_HOST
        case 'c':
            flags |= FLAG_CHECK;
            break;
#endif /* BUILD_HOST */
        case 'd':
            flags &= ~FLAG_COMPRESS;
            path = strdup(optarg);
            break;
        case 'n':
            flags |= FLAG_PERSIST;
            break;
        case 'z':
            flags |= FLAG_COMPRESS;
            path = strdup(optarg);
            break;
        default:
            usage(argv[0]);
        }
    }
    //fprintf(stderr, "argc=%d, optind=%d, flags=%x\n", argc, optind, flags);
    if (argc <= 2 || optind >= argc) {
        usage(argv[0]);
        return -1;
    }

#ifndef BUILD_HOST
    if (flags & FLAG_CHECK) {
        return do_check_ramdump(argv[1]);
    }
#endif /* BUILD_HOST */

#ifdef BUILD_NOMMAP
    return do_unpack_nommap(argv[1], path, flags);
#else /* !BUILD_NOMMAP */
    return do_unpack(argv[1], path, flags);
#endif /* BUILD_NOMMAP */
}
#endif /* BUILD_STANDALONE */
