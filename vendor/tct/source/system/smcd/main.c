/* Copyright (C) 2016 Tcl Corporation Limited */
/*
* Copyright (C) 2013 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#define LOG_TAG "Feedback_smcd"

#include <arpa/inet.h>
#include <signal.h>
#include <sys/epoll.h>
#include <pthread.h>
#include <cutils/sockets.h>
#include <log/log.h>
#include <cutils/properties.h>
#include "cmd.h"

#ifndef __unused
#define __unused __attribute__((__unused__))
#endif

volatile int bSmcdDebug = BSMCDDEBUG;

pthread_t cmdExecHdl;
pthread_mutex_t cmdMutex;
pthread_cond_t cmdCond;

void getSmcdVersion(char *chpCommand){
    if (0 == chpCommand[1]){
        chpCommand[1] = SMC_COMMAND;
    }
    if( ctrl_data_write(SMC_VERSION,chpCommand[1],SMCDVERSION) < 0 ){
        ALOGE("In %s line %d, control data socket write failed",__FUNCTION__,__LINE__);
    }
}

void smcd_command_handler(void){
    char ibuf[COMMAND_Length_MAX];
    int len;
    int cmd = -1;

    memset(ibuf,0,sizeof(ibuf));
    len = ctrl_data_read((char *)ibuf,sizeof(ibuf));
    if(len <= 0) return;
    if (sizeof(ibuf) == len){
        ALOGE("In %s line %d, command is too long, handler will exit",__FUNCTION__,__LINE__);
        return;
    }
    if(bSmcdDebug) ALOGD("In %s line %d, Received ibuf %s, length %d",__FUNCTION__,__LINE__,ibuf,len);
    smcdCommand q;
    q.func=NULL;

    cmd = ibuf[0];
    switch(cmd){
        if(bSmcdDebug) ALOGD("In %s line %d, get cmd as %s",__FUNCTION__,__LINE__,(char *)&cmd);
        case SMC_VERSION:
            getSmcdVersion(ibuf);
            return;
        case SMC_CMD_EXEC:
            if(strstr(ibuf,"kmsg") != 0){
                q.func= kmsgHandler.start;
                if(bSmcdDebug) ALOGD("In %s line %d, set q.func as kmsgHandler.start",__FUNCTION__,__LINE__);
            } else if (strstr(ibuf,"logcat") != 0) {
                q.func=logcatHandler.start;
            }
            break;
        case SMC_CMD_STOP:
            if(strstr(ibuf,"kmsg") != 0){
                q.func=kmsgHandler.stop;
                if(bSmcdDebug) ALOGD("In %s line %d,set q.func as kmsgHandler.stop",__FUNCTION__,__LINE__);
            } else if (strstr(ibuf,"logcat") != 0) {
                q.func=logcatHandler.stop;
            }
            break;
        case SMC_COMMAND:
        case SMC_BUGREPORT:
            q.func=shellHandler.start;
            break;
        case SMC_GET_STATUS:
            if(strstr(ibuf,"kmsg") != 0){
                q.func=kmsgHandler.getState;
            } else if (strstr(ibuf,"logcat") != 0) {
                q.func=logcatHandler.getState;
            } else if (strstr(ibuf,"temperature")){
                q.func = CPUHandler.getState;
            }
            break;
        case SMC_CPU_LOGGING:
            q.func=CPUHandler.start;
            break;
        case SMC_TZ_LOG:
            q.func=TZHandler.start;
            break;
        case SMC_AFTERSALE :
            mass_aftersale_handler(ibuf[1]);
            return;
        case SMC_ONETIME_LOG:
            mass_tplog_handler();
            return;
        case SMC_CHARGE_LOG:
            if(ibuf[1]=='s')
                mass_enable_charging_log();
            else if(ibuf[1]=='k')
                mass_disable_charging_log();
            return;//need return directly
        case SMC_RAM_DUMP:
            mass_traceability_ram_dump_handler(ibuf);
            return;
        case SMC_TCPDUMP_LOG:
            q.func=TCPHandler.start;
            break;
        default:
            ALOGE("In %s line %d, Received unknown command %s",__FUNCTION__,__LINE__,ibuf);
            return;
    }
    strlcpy(q.logCmd,ibuf,COMMAND_Length_MAX);
    if(bSmcdDebug) ALOGD("In %s line %d, Received command %s, it will be added to Command Queue",__FUNCTION__,__LINE__,ibuf);
    pthread_mutex_lock(&cmdMutex);
    SendCommandQueue(&q);
    pthread_cond_signal(&cmdCond);
    pthread_mutex_unlock(&cmdMutex);
    if(bSmcdDebug) ALOGD("In %s line %d, Received command %s, it has been added to Command Queue",__FUNCTION__,__LINE__,q.logCmd);
    return;
}

void CommandExecuteThread(){
    smcdCommand cmd;
    while(1){
        pthread_mutex_lock(&cmdMutex);
        pthread_cond_wait(&cmdCond,&cmdMutex);
        if(isEmptyQueue()) {
            pthread_mutex_unlock(&cmdMutex);
            continue;
        }
        while(GetCommandQueue(&cmd)==1){
            if(cmd.func){
                cmd.func(cmd.logCmd);
            }else{
                ALOGE("In %s line %d, Command:%s not implement!",__FUNCTION__,__LINE__,cmd.logCmd);
            }
        }
        pthread_mutex_unlock(&cmdMutex);
    }//end while(1)
}

static int init(void){
    int i = 0;
    int ret = 0;
    int config = 0;
    char logCommand[COMMAND_Length_MAX];
    char chTempTime[10];
    memset(logCommand,0,sizeof(logCommand));
    initQueue();
    mass_traceability_ram_dump_reader();
    ramDumpFolderCheckThread();
    pthread_mutex_init(&cmdMutex, NULL);
    pthread_cond_init(&cmdCond, NULL);
    pthread_create( &cmdExecHdl, NULL, (void *)CommandExecuteThread, NULL);
    if (cmdExecHdl == 0) {
        ALOGE("In %s line %d, failed to create thread CommandExecuteThread",__FUNCTION__,__LINE__);
        ret = -1;
    }
    config = read_logs_config(&logCommand[2],&chTempTime[2]);
    if(bSmcdDebug) ALOGD("In %s line %d, get AutoStart Mask 0x%x from config file",__FUNCTION__,__LINE__,config);
    while ((i < 120) && (access("/sdcard",F_OK|R_OK|W_OK) < 0)){
        ALOGE("In %s line %d, can't access /sdcard %d, errno=%d, %s",__FUNCTION__,__LINE__,i,errno,strerror(errno));
        i++;
        sleep(1);
    }
    renameAllFile();
    if( (config & 0x1) !=0 ){
        if(NULL == strstr(&logCommand[2],"logcat")){
            memset(logCommand,0,sizeof(logCommand));
            strlcpy(&logCommand[2],DEFAULTLOGCATCMD,sizeof(logCommand)-2);
        }
        logCommand[0]=SMC_CMD_EXEC;
        logCommand[1]='a';
        if(bSmcdDebug) ALOGD("In %s line %d, will start command %s",__FUNCTION__,__LINE__,logCommand);
        logcatHandler.start(logCommand);
    }
    if((config & 0x2)!=0  ){
        kmsgHandler.start(NULL);
    }
    if((config & 0x4)!=0){
        property_set("ctl.start","tct_diag_mdlog");
    }
    if((config & 0x8)!=0){
        // if(0 == chTempTime[2]){
        //     memset(chTempTime,0,sizeof(chTempTime));
        //     strlcpy(&chTempTime[2],DEFAULT_CPU_CMD,sizeof(chTempTime)-2);
        // }
        // chTempTime[0] = SMC_CPU_LOGGING;
        // chTempTime[1] = 's';
        // CPUHandler.start(chTempTime);
    }
    /* MODIFIED-BEGIN by lu wenshen, 2016-06-27,BUG-2152462*/
    if((config & 0x10)!=0){
        TZHandler.start("sstart tzlog");
    }
    /* MODIFIED-END by lu wenshen,BUG-2152462*/
    return ret;
}

int main(int argc __unused,char **argv __unused){
    if(bSmcdDebug) ALOGD("SMC Daemon (smcd) V%s starting",SMCDVERSION);
    init();
    if(!connectInit( )) mainloop( );

    if(bSmcdDebug) ALOGD("SMC Daemon (smcd) V%s exiting",SMCDVERSION);
    pthread_mutex_destroy(&cmdMutex);
    pthread_cond_destroy(&cmdCond);
    return 0;
}
