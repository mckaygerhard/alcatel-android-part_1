/* Copyright (C) 2016 Tcl Corporation Limited */

#define LOG_TAG "Feedback_smcd"

#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <cutils/sockets.h>
#include <log/log.h>
#include <cutils/klog.h>
#include <cutils/properties.h>
#include <dirent.h>
#include <sys/stat.h>
#include "cmd.h"

/*--- ram dump mask ---*/
#define MMCBLKP "/dev/block/bootdevice/by-name/traceability"
#define TRACEABILITY_RAM_DUMP_OFFSET 0xE7000
#define TRACEABILITY_RAM_DUMP_PROPERTY "persist.tct.ram_dump"
#define PROP_TCT_DEBUG_SMTLOG "tct.debug.smtlog"
#define ENABLE_RAM_DUMP 0xAAAA
#define DISABLE_RAM_DUMP 0x5555
enum dump_device_location_type {
    LOCATION_INTERNAL_ONLY = 0,           /* try ramdump partition -> QPST */
    LOCATION_EXTERNAL_ONLY,                  /* try SD card -> QPST */
    LOCATION_INTERNAL_FORCE,                 /* force ramdump partition -> QPST */
    LOCATION_EXTERNAL_FORCE,                 /* force SD card -> QPST */
    LOCATION_INTERNAL_PREFER,                /* try ramdump partition -> try SD card -> QPST */
    LOCATION_EXTERNAL_PREFER,                /* try SD card -> try ramdump partition -> QPST */
    LOCATION_DEVICE_AUTO,                    /* SD card & ramdump partition both failed -> QPST */
    LOCATION_DEVICE_DEFAULT   = LOCATION_DEVICE_AUTO
};
struct ramDumpHeader {
    uint16_t magic;     /* 0xAAAA or 0x5555 */
    uint8_t  timeout;   /* time out minutes waiting for SD card */
    uint8_t  location;  /* dump device location type */
} __attribute__((packed));

/*--- After Sale ---*/
#define TRACEABILITY_AFTERSALE_SIGN_OFFSET 0xE6FF0
#define PERMANENT_MARK         0x11220000
#define ONETIME_MARK           0x12120000
#define CLEAR_32               0x00000000

/*--- power off charging log ---*/
#define CHARGING_LOGCONFIG "/cache/charging_log.config"

static char TPLOG_CMDGROUP[][256] = {
    "axmkdir -p /sdcard/BugReport/SysAgentSvc/",
    "axdumpsys alarm > /sdcard/BugReport/SysAgentSvc/dumpsys_alarm_`date +%Y%m%d_%H%M%S_%Z`_SAS.txt",
    "axcat /proc/meminfo > /sdcard/BugReport/SysAgentSvc/proc_meminfo_`date +%Y%m%d_%H%M%S_%Z`_SAS.txt",
    "axcat /proc/loadavg > /sdcard/BugReport/SysAgentSvc/proc_loadavg_`date +%Y%m%d_%H%M%S_%Z`_SAS.txt",
    "axdumpsys meminfo > /sdcard/BugReport/SysAgentSvc/dumpsys_meminfo_`date +%Y%m%d_%H%M%S_%Z`_SAS.txt",
    "axtop -m 10 -d 1 -t -n 20 > /sdcard/BugReport/SysAgentSvc/top_`date +%Y%m%d_%H%M%S_%Z`_SAS.txt"
};
static char TP_DMESG_CMD[] = {"axdmesg > /sdcard/BugReport/SysAgentSvc/dmesg_`date +%Y%m%d_%H%M%S_%Z`_SAS.txt"};

static int orig_logcat_state;
static int orig_kmsg_state;
static int orig_cpu_state;

int mass_enable_charging_log(void){
    system("echo 8 > /proc/sys/kernel/printk");
    system("echo 0xffff > /sys/module/qpnp_smbcharger/parameters/debug_mask");
    system("echo 0xffff > /sys/module/qpnp_fg/parameters/debug_mask");
    if(bSmcdDebug) ALOGD("In %s line %d, enable charging log for power on mode",__FUNCTION__,__LINE__);
    return 1;
}

int mass_disable_charging_log(void){
    system("echo 6 > /proc/sys/kernel/printk");
    system("echo 0x0 > /sys/module/qpnp_smbcharger/parameters/debug_mask");
    system("echo 0x0 > /sys/module/qpnp_fg/parameters/debug_mask");
    if(bSmcdDebug) ALOGD("In %s line %d, disable charging log for power on mode",__FUNCTION__,__LINE__);
    return 1;
}

static void get_newest_file(char* file_dir , char* ret){
    DIR * dp;
    char current_file[PATH_MAX];
    struct dirent * dirp;
    struct stat stat_buf;
    time_t select_time = (time_t)0;

    if((dp = opendir(file_dir)) != NULL){
        while((dirp = readdir(dp)) != NULL){
            snprintf(current_file,sizeof(current_file),"%s/%s",file_dir,dirp->d_name);
            if((stat(current_file,&stat_buf)) != -1){
                if((signed int)(stat_buf.st_mtime) > select_time ){
                    select_time = stat_buf.st_mtime;
                    strcpy(ret,current_file);
                }
            }
        }
    }
    closedir(dp);
}

void copylog2TPdir(){
    char L_log[PATH_MAX] = {0};
    char C_log[PATH_MAX] = {0};
    char K_log[PATH_MAX] = {0};
    char cmd[PATH_MAX] = {0};

    get_newest_file("/sdcard/BugReport/Logcat" , L_log);
    snprintf(cmd,sizeof(cmd),"cp %s /sdcard/BugReport/SysAgentSvc",L_log);
    if(bSmcdDebug) ALOGD("In %s line %d, copy SysAgentSvc log cmd %s",__FUNCTION__,__LINE__,cmd);
    system(cmd);

    memset(cmd, 0 , sizeof(cmd));
    get_newest_file("/sdcard/BugReport/CPUTemp",C_log );
    snprintf(cmd,sizeof(cmd),"cp  %s  /sdcard/BugReport/SysAgentSvc",C_log);
    if(bSmcdDebug) ALOGD("In %s line %d, copy SysAgentSvc log cmd %s",__FUNCTION__,__LINE__,cmd);
    system(cmd);

    memset(cmd, 0 , sizeof(cmd));
    get_newest_file("/sdcard/BugReport/Kernel",K_log );
    snprintf(cmd,sizeof(cmd),"cp  %s  /sdcard/BugReport/SysAgentSvc",K_log);
    if(bSmcdDebug) ALOGD("In %s line %d, copy SysAgentSvc log cmd %s",__FUNCTION__,__LINE__,cmd);
    system(cmd);

}

// run dmsg & copy logs
void tplog_after_ops(int sig){
    char response[LINE_MAX];
    memset(response,0,sizeof(response));
    if(sig == SIGALRM){

        //run dmesg
        shellHandler.start(TP_DMESG_CMD);
       //restore logcat & cpu log state
        if(orig_logcat_state==0)
            logcatHandler.stop(NULL);
        if(orig_kmsg_state==0)
            kmsgHandler.stop(NULL);
        if(orig_cpu_state==0)
            CPUHandler.stop(NULL);
        //copy log to /sdcard/BugReport/SysAgentSvc
        copylog2TPdir();
    }
    if(bSmcdDebug) KLOG_DEBUG(LOG_TAG,"In %s line %d, tp log has been stopped\n",__FUNCTION__,__LINE__);

    strcpy(response,"Command success");
    if( -1 == ctrl_data_write(SMC_ONETIME_LOG,SMC_COMMAND,response) ){
        ALOGE("In %s line %d, control data socket write failed",__FUNCTION__,__LINE__);
    }
}

void mass_tplog_handler(){

    if( -1 == ctrl_data_write(SMC_ONETIME_LOG,SMC_COMMAND,CMDSTARTING) ){
        ALOGE("In %s line %d, control data socket write failed",__FUNCTION__,__LINE__);
    }

    create_dir(SYSAGENT_FOLDER);
    orig_logcat_state=logcatHandler.getState(NULL);
    orig_kmsg_state=kmsgHandler.getState(NULL);
    orig_cpu_state=CPUHandler.getState(NULL);

    if(bSmcdDebug) KLOG_DEBUG(LOG_TAG,"In %s line %d, will start get tp log",__FUNCTION__,__LINE__);
    //if log already running, it will do nothing
    logcatHandler.start(NULL);
    kmsgHandler.start(NULL);
    CPUHandler.start(NULL);

    //start dumpsys & top & loadavg
    int len = sizeof(TPLOG_CMDGROUP) / sizeof(TPLOG_CMDGROUP[0]) ;
    int i = 0;
    for( ; i < len ; i++ ){
        shellHandler.start(TPLOG_CMDGROUP[i]);
    }

    //wait 15s for top & dumpsys cmd
    signal(SIGALRM, tplog_after_ops);
    alarm(15);

}

static int mass_traceability_write (void *buf, int size, uint32_t offset){
    int fdwrite = -1;
    off64_t result = 0;
    int fd = TEMP_FAILURE_RETRY(open(MMCBLKP, O_RDWR));
    if(fd == -1) {
        ALOGE("In %s line %d, open MMCBLKP failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
        return -1;
    }
    result = lseek64(fd, offset, SEEK_SET);
    if(bSmcdDebug) ALOGD("In %s line %d, reposition 64-bit read/write file offset to 0x%llx"
            ,__FUNCTION__,__LINE__,(long long)result);
    if(offset == result){
        fdwrite = (int) write(fd, buf, (size_t) size);
        if(bSmcdDebug) ALOGD("In %s line %d, write traceability ram dump as 0x%x in offset 0x%x,and return %d"
                ,__FUNCTION__,__LINE__,*(uint32_t*)buf,offset,fdwrite);
        if(fdwrite <=0){
            ALOGE("In %s line %d, write file failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
            if(fd > 0) close(fd);
            return -1;
        }
        if(fdwrite == size){
            if(bSmcdDebug) ALOGD("In %s line %d, write traceability ram dumo Success",__FUNCTION__,__LINE__);
            if(fd > 0) close(fd);
            return 0;
        }
    }else{
        ALOGE("In %s line %d, reposition 64-bit read/write file offset failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        if(fd > 0) close(fd);
        return -1;
    }
    if(fd > 0) close(fd);
    return 0;
}

int mass_traceability_read(void *buf, int size, uint32_t offset){
    int fdread = -1;
    off64_t result = 0;
    int fd = TEMP_FAILURE_RETRY(open(MMCBLKP,O_RDONLY));

    if(fd == -1){
        ALOGE("In %s line %d, open MMCBLKP failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
        return -1;
    }
    result = lseek64(fd,offset,SEEK_SET);
    if(bSmcdDebug) ALOGD("In %s line %d, reposition 64-bit read/write file offset to 0x%llx",__FUNCTION__,__LINE__,(long long)result);
    if((off64_t)offset == result){
        fdread = (int) read(fd, buf, (size_t) size);
        if(bSmcdDebug) ALOGD("In %s line %d, read traceability ram dump as 0x%x from offset 0x%x"
                ,__FUNCTION__,__LINE__,*(uint32_t*)buf,offset);
        if(fdread <=0){
            ALOGE("In %s line %d, write file failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
            if(fd > 0) close(fd);
            return -1;
        }
        if(fdread == size){
            if(fd > 0) close(fd);
            return 0;
        }
    }else{
        ALOGE("In %s line %d, reposition 64-bit read/write file offset failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        if(fd > 0) close(fd);
        return -1;
    }
    if(fd > 0) close(fd);
    return 0;
}

void mass_aftersale_handler(char subcmd){
    int ret = -1 ;
    uint64_t size = sizeof(uint32_t);
    uint32_t buf = 0;

    if(bSmcdDebug) ALOGD("In %s line %d, get subcmd %c",__FUNCTION__,__LINE__,subcmd);
    if (subcmd == '1'){
        buf = ONETIME_MARK ;
        ret = mass_traceability_write(&buf , (int) size, TRACEABILITY_AFTERSALE_SIGN_OFFSET ) ;
        if ( ret == 0){
            ctrl_data_write(SMC_AFTERSALE,'1',"Command success");
            if (property_set(PROP_TCT_DEBUG_SMTLOG, "true")) {
                ALOGE("In %s line %d, Set property %s to false failed!",__FUNCTION__, __LINE__,
                      TRACEABILITY_RAM_DUMP_PROPERTY);
            }
        }
    }else if(subcmd == '0'){
        buf = CLEAR_32 ;
        ret = mass_traceability_write(&buf , (int) size, TRACEABILITY_AFTERSALE_SIGN_OFFSET ) ;
        if ( ret == 0){
            ctrl_data_write(SMC_AFTERSALE,'0',"Command success");
            if (property_set(PROP_TCT_DEBUG_SMTLOG, "false")) {
                ALOGE("In %s line %d, Set property %s to false failed!",__FUNCTION__, __LINE__,
                      TRACEABILITY_RAM_DUMP_PROPERTY);
            }
        }
    }else{
        ALOGE("In %s line %d, get unknown subcommand %c",__FUNCTION__,__LINE__,subcmd);
    }
}

int mass_traceability_ram_dump_handler(char *command) {
    int result = -1;
    struct ramDumpHeader stcRamDumpHeader;
    if (strstr(command, "ram dump") != 0) {
        if (bSmcdDebug)
            ALOGD("In %s line %d, Received command: %s", __FUNCTION__, __LINE__,command);
        if (strstr(command, "0") != 0) {
            stcRamDumpHeader.magic = DISABLE_RAM_DUMP;
            stcRamDumpHeader.timeout = 0;
            stcRamDumpHeader.location = LOCATION_EXTERNAL_ONLY;
            result = mass_traceability_write(&stcRamDumpHeader, sizeof(stcRamDumpHeader),
                                             TRACEABILITY_RAM_DUMP_OFFSET);
            if (1 == result) {
                if (property_set(TRACEABILITY_RAM_DUMP_PROPERTY, "false")) {
                    ALOGE("In %s line %d, Set property %s to false failed!",__FUNCTION__, __LINE__,
                          TRACEABILITY_RAM_DUMP_PROPERTY);
                    return -1;
                }
            }
        }
        if (strstr(command, "1") != 0) {
            stcRamDumpHeader.magic = ENABLE_RAM_DUMP;
            stcRamDumpHeader.timeout = 5;
            stcRamDumpHeader.location = LOCATION_EXTERNAL_ONLY;
            result = mass_traceability_write(&stcRamDumpHeader, sizeof(stcRamDumpHeader),
                                             TRACEABILITY_RAM_DUMP_OFFSET);
            if (1 == result) {
                if (property_set(TRACEABILITY_RAM_DUMP_PROPERTY, "true")) {
                    ALOGE("In %s line %d, Set property %s to true failed!",
                            __FUNCTION__, __LINE__,
                            TRACEABILITY_RAM_DUMP_PROPERTY);
                    return -1;
                }
            }
        }
        if (result != 1)
            ctrl_data_write(SMC_RAM_DUMP, '0', "Command failed");
    }
    ctrl_data_write(SMC_RAM_DUMP, '0', "Command success");
    return 0;
}

int mass_traceability_ram_dump_reader(){
    uint32_t buf = 0;
    if(mass_traceability_read(&buf,sizeof(buf),TRACEABILITY_RAM_DUMP_OFFSET)){
        ALOGE("In %s line %d, read ram dump mask failed",__FUNCTION__,__LINE__);
        return -1;
    }
    if(bSmcdDebug) ALOGD("In %s line %d, get ram dump mask in offset 0X%x as 0x%x"
            ,__FUNCTION__,__LINE__,TRACEABILITY_RAM_DUMP_OFFSET,buf);

    if (ENABLE_RAM_DUMP == buf){
        if(property_set(TRACEABILITY_RAM_DUMP_PROPERTY,"true")) {
            ALOGE("In %s line %d, Set property %s to true failed!",__FUNCTION__,__LINE__,TRACEABILITY_RAM_DUMP_PROPERTY);
            return -1;
        }
    }else{
        if(property_set(TRACEABILITY_RAM_DUMP_PROPERTY,"false")) {
            ALOGE("In %s line %d, Set property %s to false failed!",__FUNCTION__,__LINE__,TRACEABILITY_RAM_DUMP_PROPERTY);
            return -1;
        }
    }

    return 0;
}
