/* Copyright (C) 2016 Tcl Corporation Limited */

#ifndef __SMCD_UTILS_H__
#define __SMCD_UTILS_H__

/**
 * Check if mount point mounted.
 *
 * @param mp
 *     Mount point.
 *
 * @return
 *     1 if mounted, 0 if un-mounted.
 */
int is_mountpoint_mounted(const char *);

#endif /* __SMCD_UTILS_H__ */
