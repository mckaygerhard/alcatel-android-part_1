/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:                                                                    */
/* E-Mail:                                                                    */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_debug_on.c             */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/07/10| yongliang.wang | FR-483802          | Send command to turn on adb*/
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/


#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_debug_on.h"

static uint8 delay = 1;

static const diagpkt_user_table_entry_type tct_debug_on_tbl[] =
{
    {TCT_DEBUG_ON, TCT_DEBUG_ON, tct_debug_on_func},
};

PACK(void *) tct_debug_on_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    uint16 switcher;
    DIAG_LOGE("tct_debug_on_func \n");

    req_type_tct_debug_on_exec *req = (req_type_tct_debug_on_exec *) req_pkt;
    rsp_type_tct_debug_on_exec *rsp = NULL;
    int ret = -1;

    /*Allocate the same length as the request*/
    rsp = (rsp_type_tct_debug_on_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_DEBUG_ON,
            sizeof(rsp_type_tct_debug_on_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_tct_debug_on_exec));
    memcpy(rsp, req, sizeof(req_type_tct_debug_on_exec) - 1);
    rsp->result = 0;

    switcher = req->switcher;

    pthread_t tid;

    printf("before thread_create delay: %d\n",delay);
    if(switcher == 1)
    {
        if(pthread_create(&tid,NULL,(void*)&debug_on_thread,NULL)!=0)
        {
            printf("thread create failed\n");
        }
    }
    else
    {
       if(pthread_create(&tid,NULL,(void*)&debug_off_thread,NULL)!=0)
        {
            printf("thread create failed\n");
        }
    }
    return (rsp);
}

void debug_on_thread()
{
    //sleep(delay);
    system("setprop sys.usb.config default,mass_storage,diag,serial_smd,serial_tty,adb");
}
void debug_off_thread()
{
    //sleep(delay);
    system("setprop sys.usb.config default,mass_storage,diag,serial_smd,serial_tty");
}

void tct_debug_on_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_FT, tct_debug_on_tbl);
}

