/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cutils/sockets.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>

#define LOG_TAG "Tctd"
#include "cutils/log.h"

#include "TctdListener.h"

#ifndef TARGET_BUILD_TCT_DIAG_STANDALONE
#include "DiagManager.h"
#endif /* TARGET_BUILD_TCT_DIAG_STANDALONE */
#include "PrivilegeManager.h"
#include "QmiManager.h"
#include "TraceabilityManager.h"

int main(int argc, char *argv[]) {

    TctdListener *cl;
#ifndef TARGET_BUILD_TCT_DIAG_STANDALONE
    DiagManager *dm;
#endif /* TARGET_BUILD_TCT_DIAG_STANDALONE */
    PrivilegeManager *pm;
    QmiManager *qm;
    TraceabilityManager *tm;

    SLOGI("Tct Daemon (tctd) 1.0 starting");

    // Quickly throw a CLOEXEC on the socket we just inherited from init
    fcntl(android_get_control_socket("tctd"), F_SETFD, FD_CLOEXEC);

    /* Create our singleton managers */
#ifndef TARGET_BUILD_TCT_DIAG_STANDALONE
    if (!(dm = DiagManager::Instance())) {
        SLOGE("Unable to create DiagManager");
        //exit(1);
    }
#endif /* TARGET_BUILD_TCT_DIAG_STANDALONE */
    if (!(pm = PrivilegeManager::Instance())) {
        SLOGE("Unable to create PrivilegeManager");
        //exit(1);
    }
    if (!(qm = QmiManager::Instance())) {
        SLOGE("Unable to create QmiManager");
        //exit(1);
    }
    if (!(tm = TraceabilityManager::Instance())) {
        SLOGE("Unable to create TraceabilityManager");
        //exit(1);
    }

    cl = new TctdListener("tctd", true);
#ifndef TARGET_BUILD_TCT_DIAG_STANDALONE
    if (dm) {
        dm->setBroadcaster((SocketListener *) cl);
    }
#endif /* TARGET_BUILD_TCT_DIAG_STANDALONE */
    if (pm) {
        pm->setBroadcaster((SocketListener *) cl);
    }
    if (qm) {
        qm->setBroadcaster((SocketListener *) cl);
    }
    if (tm) {
        tm->setBroadcaster((SocketListener *) cl);
    }

#ifndef TARGET_BUILD_TCT_DIAG_STANDALONE
    if (dm && dm->start()) {
        SLOGE("Unable to start DiagManager (%s)", strerror(errno));
        //exit(1);
    }
#endif /* TARGET_BUILD_TCT_DIAG_STANDALONE */
    if (pm && pm->start()) {
        SLOGE("Unable to start PrivilegeManager (%s)", strerror(errno));
        //exit(1);
    }
    if (qm && qm->start()) {
        SLOGE("Unable to start QmiManager (%s)", strerror(errno));
        //exit(1);
    }
    if (tm && tm->start()) {
       SLOGE("Unable to start TraceabilityManager (%s)", strerror(errno));
        //exit(1);
    }

    /*
     * Now that we're up, we can respond to commands
     */
    if (cl->startListener()) {
        SLOGE("Unable to start TctdListener (%s)", strerror(errno));
        exit(1);
    }

    // Eventually we'll become the monitoring thread
    while(1) {
        sleep(1000);
    }

    SLOGI("Tct Daemon (tctd) exiting");
    exit(0);
}
