/* Copyright (C) 2016 Tcl Corporation Limited */
#include <errno.h>

#define LOG_TAG "TctdCommand"

#include <cutils/log.h>

#include "TctdCommand.h"

#define UNUSED __attribute__((unused))

TctdCommand::TctdCommand(const char *cmd) {
    init(cmd, false);
}

TctdCommand::TctdCommand(const char *cmd, bool handleRawData) {
    init(cmd, handleRawData);
}

void TctdCommand::init(const char *cmd, bool handleRawData) {
    mCommand = cmd;
    mHandleRawData = handleRawData;
}

int TctdCommand::runCommand(SocketClient *c UNUSED, int argc UNUSED,
                            char **argv UNUSED) {
    SLOGW("Command %s has no runCommand handler!", getCommand());
    errno = ENOSYS;
    return -1;
}

int TctdCommand::runCommandWithRawData(SocketClient *cli, char *data, int size) {
    SLOGW("Command %s has no runCommandWithBinaryData hanlder!", getCommand());
    errno = ENOSYS;
    return -1;
}
