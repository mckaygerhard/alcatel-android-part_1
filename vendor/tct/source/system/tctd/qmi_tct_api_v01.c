/* Copyright (C) 2016 Tcl Corporation Limited */
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                        Q M I _ T C T _ A P I _ V 0 1  . C

GENERAL DESCRIPTION
  This is the file which defines the tct service Data structures.




 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/
/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
 *THIS IS AN AUTO GENERATED FILE. DO NOT ALTER IN ANY WAY
 *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/* This file was generated with Tool version 5.1
   It was generated on: Thu Jun  2 2016
   From IDL File: qmi_tct_api_v01.idl */

#include "stdint.h"
#include "qmi_idl_lib_internal.h"
#include "qmi_tct_api_v01.h"


/*Type Definitions*/
/*Message Definitions*/
static const uint8_t test_req_msg_data_v01[] = {
  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x01,
  QMI_IDL_FLAGS_IS_ARRAY |  QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(test_req_msg_v01, ping),
  4
};

static const uint8_t test_resp_msg_data_v01[] = {
  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x01,
  QMI_IDL_FLAGS_IS_ARRAY |  QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(test_resp_msg_v01, pong),
  4
};

static const uint8_t test_ind_msg_data_v01[] = {
  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x01,
  QMI_IDL_FLAGS_IS_ARRAY |  QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(test_ind_msg_v01, hello),
  5
};

static const uint8_t retrofit_nv_read_req_msg_data_v01[] = {
  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x01,
  QMI_IDL_FLAGS_IS_ARRAY | QMI_IDL_FLAGS_SZ_IS_16 |   QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(retrofit_nv_read_req_msg_v01, efs_path),
  ((FS_PATH_MAX_V01) & 0xFF), ((FS_PATH_MAX_V01) >> 8)
};

static const uint8_t retrofit_nv_read_resp_msg_data_v01[] = {
  0x01,
  QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET8(retrofit_nv_read_resp_msg_v01, nv_data_len),

  0x02,
  QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(retrofit_nv_read_resp_msg_v01, result),

  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x03,
  QMI_IDL_FLAGS_IS_ARRAY | QMI_IDL_FLAGS_SZ_IS_16 |   QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(retrofit_nv_read_resp_msg_v01, nv_data),
  ((QMI_RETROFIT_NV_ITEM_DATA_MAX_SIZE_V01) & 0xFF), ((QMI_RETROFIT_NV_ITEM_DATA_MAX_SIZE_V01) >> 8)
};

static const uint8_t nvdiag_req_msg_data_v01[] = {
  0x01,
  QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET8(nvdiag_req_msg_v01, item),

  0x02,
  QMI_IDL_FLAGS_IS_ARRAY |  QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(nvdiag_req_msg_v01, file),
  QMI_NV_DIAG_FILE_PATH_V01,

  0x03,
  QMI_IDL_GENERIC_2_BYTE,
  QMI_IDL_OFFSET8(nvdiag_req_msg_v01, item_size),

  0x04,
  QMI_IDL_GENERIC_2_BYTE,
  QMI_IDL_OFFSET8(nvdiag_req_msg_v01, mode),

  0x05,
  QMI_IDL_GENERIC_2_BYTE,
  QMI_IDL_OFFSET8(nvdiag_req_msg_v01, compress_size),

  0x06,
  QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET8(nvdiag_req_msg_v01, operation),

  0x07,
  QMI_IDL_FLAGS_IS_ARRAY | QMI_IDL_FLAGS_SZ_IS_16 |   QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(nvdiag_req_msg_v01, value),
  ((QMI_NV_DIAG_ITEM_SIZE_V01) & 0xFF), ((QMI_NV_DIAG_ITEM_SIZE_V01) >> 8),

  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x08,
  QMI_IDL_FLAGS_OFFSET_IS_16 | QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET16ARRAY(nvdiag_req_msg_v01, result)
};

static const uint8_t nvdiag_resp_msg_data_v01[] = {
  0x01,
  QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET8(nvdiag_resp_msg_v01, item),

  0x02,
  QMI_IDL_FLAGS_IS_ARRAY |  QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(nvdiag_resp_msg_v01, file),
  QMI_NV_DIAG_FILE_PATH_V01,

  0x03,
  QMI_IDL_GENERIC_2_BYTE,
  QMI_IDL_OFFSET8(nvdiag_resp_msg_v01, item_size),

  0x04,
  QMI_IDL_GENERIC_2_BYTE,
  QMI_IDL_OFFSET8(nvdiag_resp_msg_v01, mode),

  0x05,
  QMI_IDL_GENERIC_2_BYTE,
  QMI_IDL_OFFSET8(nvdiag_resp_msg_v01, compress_size),

  0x06,
  QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET8(nvdiag_resp_msg_v01, operation),

  0x07,
  QMI_IDL_FLAGS_IS_ARRAY | QMI_IDL_FLAGS_SZ_IS_16 |   QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(nvdiag_resp_msg_v01, value),
  ((QMI_NV_DIAG_ITEM_SIZE_V01) & 0xFF), ((QMI_NV_DIAG_ITEM_SIZE_V01) >> 8),

  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x08,
  QMI_IDL_FLAGS_OFFSET_IS_16 | QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET16ARRAY(nvdiag_resp_msg_v01, result)
};

static const uint8_t getlist_req_msg_data_v01[] = {
  0x01,
  QMI_IDL_FLAGS_IS_ARRAY | QMI_IDL_FLAGS_SZ_IS_16 |   QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(getlist_req_msg_v01, path),
  ((FS_PATH_MAX_V01) & 0xFF), ((FS_PATH_MAX_V01) >> 8),

  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x02,
  QMI_IDL_FLAGS_OFFSET_IS_16 | QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET16ARRAY(getlist_req_msg_v01, result)
};

static const uint8_t getlist_resp_msg_data_v01[] = {
  0x01,
  QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET8(getlist_resp_msg_v01, num),

  0x02,
  QMI_IDL_FLAGS_IS_ARRAY | QMI_IDL_FLAGS_SZ_IS_16 |   QMI_IDL_GENERIC_1_BYTE,
  QMI_IDL_OFFSET8(getlist_resp_msg_v01, fileList),
  ((FS_PATH_MAX_V01) & 0xFF), ((FS_PATH_MAX_V01) >> 8),

  QMI_IDL_TLV_FLAGS_LAST_TLV | 0x03,
  QMI_IDL_FLAGS_OFFSET_IS_16 | QMI_IDL_GENERIC_4_BYTE,
  QMI_IDL_OFFSET16ARRAY(getlist_resp_msg_v01, result)
};

/* Type Table */
/* No Types Defined in IDL */

/* Message Table */
static const qmi_idl_message_table_entry tct_message_table_v01[] = {
  {sizeof(test_req_msg_v01), test_req_msg_data_v01},
  {sizeof(test_resp_msg_v01), test_resp_msg_data_v01},
  {sizeof(test_ind_msg_v01), test_ind_msg_data_v01},
  {sizeof(retrofit_nv_read_req_msg_v01), retrofit_nv_read_req_msg_data_v01},
  {sizeof(retrofit_nv_read_resp_msg_v01), retrofit_nv_read_resp_msg_data_v01},
  {sizeof(nvdiag_req_msg_v01), nvdiag_req_msg_data_v01},
  {sizeof(nvdiag_resp_msg_v01), nvdiag_resp_msg_data_v01},
  {sizeof(getlist_req_msg_v01), getlist_req_msg_data_v01},
  {sizeof(getlist_resp_msg_v01), getlist_resp_msg_data_v01}
};

/* Predefine the Type Table Object */
static const qmi_idl_type_table_object tct_qmi_idl_type_table_object_v01;

/*Referenced Tables Array*/
static const qmi_idl_type_table_object *tct_qmi_idl_type_table_object_referenced_tables_v01[] =
{&tct_qmi_idl_type_table_object_v01};

/*Type Table Object*/
static const qmi_idl_type_table_object tct_qmi_idl_type_table_object_v01 = {
  0,
  sizeof(tct_message_table_v01)/sizeof(qmi_idl_message_table_entry),
  1,
  NULL,
  tct_message_table_v01,
  tct_qmi_idl_type_table_object_referenced_tables_v01
};

/*Arrays of service_message_table_entries for commands, responses and indications*/
static const qmi_idl_service_message_table_entry tct_service_command_messages_v01[] = {
  {QMI_PING_REQ_V01, TYPE16(0, 0), 7},
  {QMI_RETROFIT_NV_READ_REQ_V01, TYPE16(0, 3), 1028},
  {QMI_NV_DIAG_REQ_V01, TYPE16(0, 5), 1186},
  {QMI_GETLIST_REQ_V01, TYPE16(0, 7), 1035}
};

static const qmi_idl_service_message_table_entry tct_service_response_messages_v01[] = {
  {QMI_PING_RESP_V01, TYPE16(0, 1), 7},
  {QMI_RETROFIT_NV_READ_RESP_V01, TYPE16(0, 4), 4110},
  {QMI_NV_DIAG_RESP_V01, TYPE16(0, 6), 1186},
  {QMI_GETLIST_RESP_V01, TYPE16(0, 8), 1042}
};

static const qmi_idl_service_message_table_entry tct_service_indication_messages_v01[] = {
  {QMI_HELLO_IND_V01, TYPE16(0, 2), 8}
};

/*Service Object*/
struct qmi_idl_service_object tct_qmi_idl_service_object_v01 = {
  0x05,
  0x01,
  0x02FF,
  4110,
  { sizeof(tct_service_command_messages_v01)/sizeof(qmi_idl_service_message_table_entry),
    sizeof(tct_service_response_messages_v01)/sizeof(qmi_idl_service_message_table_entry),
    sizeof(tct_service_indication_messages_v01)/sizeof(qmi_idl_service_message_table_entry) },
  { tct_service_command_messages_v01, tct_service_response_messages_v01, tct_service_indication_messages_v01},
  &tct_qmi_idl_type_table_object_v01,
  0x00,
  NULL
};

/* Service Object Accessor */
qmi_idl_service_object_type tct_get_service_object_internal_v01
 ( int32_t idl_maj_version, int32_t idl_min_version, int32_t library_version ){
  if ( TCT_V01_IDL_MAJOR_VERS != idl_maj_version || TCT_V01_IDL_MINOR_VERS != idl_min_version
       || TCT_V01_IDL_TOOL_VERS != library_version)
  {
    return NULL;
  }
  return (qmi_idl_service_object_type)&tct_qmi_idl_service_object_v01;
}

