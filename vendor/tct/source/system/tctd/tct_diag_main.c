/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_diag.c                     */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/09/05| Linjian.Xiang  | FR-508414          | Screen Unlock Tool         */
/* 13/04/23| Xinjian.Fang   | CR-443080          | Add efuse check            */
/* 13/01/24| Qianbo.Pan     | PR-397767          | Audio loop command         */
/* 12/12/06| Qianbo.Pan     | PR-315079          | Trace read write command   */
/* 12/11/21| Qianbo.Pan     | PR-318657          | Fix bug 318657,346358      */
/* 12/10/11| He.Zhang       | FR-334604          | Add ft_cmd & backup        */
/* 12/10/08| He.Zhang       | FR-334604          | Add '\n' for log display   */
/* 12/09/21| He.Zhang       | FR-334604          | Create for tct diag        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#include "msg.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include "diagpkt.h"
#include "diag.h"
#include "diag_lsm.h"
#include "tct_diag.h"

int tct_diag_init()
{
    boolean bInit_Success;

    DIAG_LOGE("calling LSM init\n");
    bInit_Success = Diag_LSM_Init(NULL);

    if (!bInit_Success) {
        DIAG_LOGE("main(): Diag_LSM_Init() failed.\n");
    } else {
        DIAG_LOGE("main(): Diag_LSM_Init succeeded.\n");
    }
    return (bInit_Success == TRUE ? 0 : 1);
}

int tct_diag_deinit()
{
    boolean bDeInit_Success;

    DIAG_LOGE("calling LSM DeInit\n");
    bDeInit_Success = Diag_LSM_DeInit();

    if (!bDeInit_Success) {
        DIAG_LOGE("main(): Diag_LSM_DeInit() failed.\n");
    } else {
        DIAG_LOGE("main(): Diag_LSM_DeInit succeeded.\n");
    }

    return (bDeInit_Success == TRUE ? 0 : 1);
}

void register_all_commands()
{
    /* TODO, please call interface of diag command register here */
    /* ft cmd */
    tct_ft_cmd_register();
    /*[BUGFIX]-Del-BEGIN by TCTNB.(Qianbo Pan), 2012/11/27, for PR318657, 346358*/
    /* NV backup */
    tct_backup_register();
    /*[BUGFIX]-Add-BEGIN by TCTNB.(Qianbo Pan), 2013/01/24, for PR397767 Audio loop command*/

    /*[FEATURE]-Add-BEGIN by TCTNB.Linjian.Xiang, 2013/09/05, for FR-508414 Screen Unlock Tool*/
    tct_unlockscreen_register();
    tct_unlockscreen_v2_register();
    /*[FEATURE]-Add-END by TCTNB.Linjian.Xiang*/


    /*[FEATURE]-Add-BEGIN by TCTNB.(shihun.liu), 2014/12/17, for PR874466 DIAG CMD Upgrade*/
    tct_shutdown_register();
    tct_debug_on_register();
    tct_audioloop_cmd_register();
    tct_trace_cmd_register();
    tct_charging_on_register();
    tct_emmc_cmd_register();
    /*[FEATURE]-Add-END by TCTNB.(shishun.liu)*/

    tct_rdata_cmd_register();
}


#ifdef TARGET_BUILD_TCT_DIAG_STANDALONE
int main(int argc, char *argv[])
{
    int ret;

    ret = tct_diag_init();
    if (ret) {
        DIAG_LOGE("tct_diag_init() failed.\n");
        return 1;
    }
    DIAG_LOGE("tct_diag_init succeeded.\n");

    register_all_commands();

    for (;;) {
       sleep(MAX_SLEEP_TIME);
    }

    ret = tct_diag_deinit();

    return ret;
}
#endif /* TARGET_BUILD_TCT_DIAG_STANDALONE */
