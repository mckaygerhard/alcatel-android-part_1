/* Copyright (C) 2016 Tcl Corporation Limited */
#define LOG_TAG "TctFeedbackManager"

#include <fcntl.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>

#include <cutils/log.h>
#include <private/android_filesystem_config.h>

#include "ResponseCode.h"

#include "TctFeedbackManager.h"

TctFeedbackManager *TctFeedbackManager::sInstance = NULL;

TctFeedbackManager *TctFeedbackManager::Instance() {
    if (!sInstance)
        sInstance = new TctFeedbackManager();
    return sInstance;
}

TctFeedbackManager::TctFeedbackManager() {
}

TctFeedbackManager::~TctFeedbackManager() {
}

int TctFeedbackManager::start() {
    return 0;
}

int TctFeedbackManager::stop() {
    return 0;
}

int TctFeedbackManager::dumpPmic(SocketClient *cli) {

#define BUF_SIZE 1024
    char buf[BUF_SIZE];
    char pmic[BUF_SIZE*6];
	char * offset;
	int fd, size;
    do {
	    system("echo 0x1004 > /sys/kernel/debug/spmi/spmi-0/address");
	    system("echo 0x6cc > /sys/kernel/debug/spmi/spmi-0/count");
		system("mkdir storage/sdcard0/BugReport/");
	    system("cat /sys/kernel/debug/spmi/spmi-0/data > /storage/sdcard0/BugReport/Pmic_Register");
        fd = open("/sys/kernel/debug/spmi/spmi-0/data", O_RDONLY);
        if (fd < 0) {
            SLOGE("DumpPmic: open sys/kernel/debug/spmi/spmi-0/data fail, errno (%d)", errno);
            break;
        }
		offset = pmic;
        while ((size = read(fd, buf, BUF_SIZE)) > 0) {
			memcpy(offset, buf, size);
			memset(buf, 0, BUF_SIZE);
			offset += size;
        }
		SLOGE("DumpPmic: data length %d ,read data %s", strlen(pmic), pmic);

        cli->sendBinaryMsg(ResponseCode::TraceabilityReadResult, pmic,BUF_SIZE*6);

    } while (0);
    if (fd > 0) {
        close(fd);
    }

    return 0;
}
