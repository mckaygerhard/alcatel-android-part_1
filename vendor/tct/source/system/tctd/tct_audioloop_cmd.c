/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_audioloop_cmd.c            */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 24/01/13| Pan Qianbo     | PR-397767          | Audio loop command         */
/* 10/06/13| Fang Xinjian   | PR-466605          | Modify loop command        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <cutils/properties.h>
#include <errno.h>
#include <string.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_audioloop_cmd.h"

#define LOG_TAG "AudioLoop"

/*
command1    command2    tc    loop_name
8           0           53    bottom mic    --> top speaker
9           0           54    top mic       --> bottom speaker
0           0           55    bottom mic    --> top receiver
1           0           56    top mic       --> bottom receiver
2           0           61    headset mic   --> top speaker
3           0           62    headset mic   --> bottom speaker
4           0           57    headset mic   --> top receiver
5           0           58    headset mic   --> bottom receiver
a           0           63    headset mic   --> headset speaker
6           0           60    top mic       --> headset receiver
7           0           59    bottom mic    --> headset receiver
*/

static const diagpkt_user_table_entry_type tct_audioloop_cmd_tbl[] =
{
    {TCT_AUDIOLOOP_CMD, TCT_AUDIOLOOP_CMD, tct_audioloop_cmd_func},
};

PACK(void *) tct_audioloop_cmd_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    char product[PROP_VALUE_MAX];
#ifdef TCT_TARGET_MMITEST_PRODUCT
    strcpy(product, TCT_TARGET_MMITEST_PRODUCT);
#else
    property_get("ro.tct.product",product, "");
#endif

    DIAG_LOGE("tct_audioloop_cmd_func \n");

    req_type_tct_audioloop_cmd_exec *req = (req_type_tct_audioloop_cmd_exec *) req_pkt;
    rsp_type_tct_audioloop_cmd_exec *rsp = NULL;
    int ret = -1;

    /*Allocate the same length as the request*/
    rsp = (rsp_type_tct_audioloop_cmd_exec *)diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_AUDIOLOOP_CMD,
            sizeof(rsp_type_tct_audioloop_cmd_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_tct_audioloop_cmd_exec));
    memcpy(rsp, req, sizeof(req_type_tct_audioloop_cmd_exec));
    rsp->result = 0;

    if (req->command1 == 0 && req->command2 == 0) {
        // bottom mic    --> top receiver
        if(strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 55 -c /system/etc/ftm_test_config_simba6x -d 365 -v 55 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 55 -c /system/etc/ftm_test_config_london -d 365 -v 55 &");
            rsp->result = 1;
        }
    } else if (req->command1 == 0 && req->command2 == 1) {
        property_set("rw.tct.ftm.audio", "off");
        rsp->result = 1;
    } else if (req->command1 == 1 && req->command2 == 0) {
        // top mic       --> bottom receiver
        if(strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 56 -c /system/etc/ftm_test_config_simba6x -d 365 -v 50 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 56 -c /system/etc/ftm_test_config_london -d 365 -v 50 &");
            rsp->result = 1;
        }
    } else if (req->command1 == 1 && req->command2 == 1){
        property_set("rw.tct.ftm.audio", "off");
        rsp->result = 1;
    } else if (req->command1 == 2 && req->command2 == 0) {
        // headset mic   --> top speaker
        if(strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 61 -c /system/etc/ftm_test_config_simba6x -d 365 -v 70 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 61 -c /system/etc/ftm_test_config_london -d 365 -v 70 &");
            rsp->result = 1;
        }
    } else if (req->command1 == 2 && req->command2 == 1){
        property_set("rw.tct.ftm.audio", "off");
        rsp->result = 1;
    } else if (req->command1 == 3 && req->command2 == 0) {
        // headset mic   --> bottom speaker
        if(strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 62 -c /system/etc/ftm_test_config_simba6x -d 365 -v 70 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 62 -c /system/etc/ftm_test_config_london -d 365 -v 70 &");
            rsp->result = 1;
        }
    } else if (req->command1 == 3 && req->command2 == 1){
        property_set("rw.tct.ftm.audio", "off");
        rsp->result = 1;
    } else if (req->command1 == 4 && req->command2 == 0) {
        // headset mic   --> top receiver
        if(strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 57 -c /system/etc/ftm_test_config_simba6x -d 365 -v 60 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 57 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
            rsp->result = 1;
        }
    } else if (req->command1 == 4 && req->command2 == 1){
        property_set("rw.tct.ftm.audio", "off");
        rsp->result = 1;
    } else if (req->command1 == 5 && req->command2 == 0) {
        // headset mic   --> bottom receiver
        if(strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 58 -c /system/etc/ftm_test_config_simba6x -d 365 -v 70 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 58 -c /system/etc/ftm_test_config_london -d 365 -v 70 &");
            rsp->result = 1;
        }
    } else if (req->command1 == 5 && req->command2 == 1){
        property_set("rw.tct.ftm.audio", "off");
        rsp->result = 1;
    } else if (req->command1 == 6 && req->command2 == 0) {
        // top mic       --> headset receiver
        if (strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 60 -c /system/etc/ftm_test_config_simba6x -d 365 -v 50 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 60 -c /system/etc/ftm_test_config_london -d 365 -v 50 &");
            rsp->result = 1;
        }
    } else if (req->command1 == 6 && req->command2 == 1){
        property_set("rw.tct.ftm.audio", "off");
        rsp->result = 1;
    } else if (req->command1 == 7 && req->command2 == 0) {
        // bottom mic    --> headset receiver
        if (strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 59 -c /system/etc/ftm_test_config_simba6x -d 365 -v 50 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 59 -c /system/etc/ftm_test_config_london -d 365 -v 50 &");
            rsp->result = 1;
        }
    } else if (req->command1 == 7 && req->command2 == 1){
        property_set("rw.tct.ftm.audio", "off");
        rsp->result = 1;
    } else if (req->command1 == 8 && req->command2 == 0) {
        // bottom mic    --> top speaker
        if (strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 53 -c /system/etc/ftm_test_config_simba6x -d 365 -v 60 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 53 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
            rsp->result = 1;
        }
    } else if (req->command1 == 8 && req->command2 == 1){
        property_set("rw.tct.ftm.audio", "off");
        rsp->result = 1;
    } else if (req->command1 == 9 && req->command2 == 0) {
        // top mic       --> bottom speaker
        if (strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 54 -c /system/etc/ftm_test_config_simba6x -d 365 -v 60 &");
            rsp->result = 1;
        }
        else if(strcmp(product, "london")==0) {
            property_set("rw.tct.ftm.audio", "on");
            system("mm-audio-ftm -tc 54 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
            rsp->result = 1;
        }
    }else if (req->command1 == 9 && req->command2 == 1){
        if (strcmp(product, "simba6x")==0) {
            property_set("rw.tct.ftm.audio", "off");
            rsp->result = 1;
        }
    }

    return (rsp);
}

void tct_audioloop_cmd_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_FT, tct_audioloop_cmd_tbl);
}

