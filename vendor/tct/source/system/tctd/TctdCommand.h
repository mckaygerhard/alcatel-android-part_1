/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef __TCTD_CMD_HANDLER_H
#define __TCTD_CMD_HANDLER_H

#include <sysutils/List.h>
#include <sysutils/SocketClient.h>

class TctdCommand {
private:
    const char *mCommand;
    bool mHandleRawData;

public:
    TctdCommand(const char *cmd);
    TctdCommand(const char *cmd, bool handleRawData);
    virtual ~TctdCommand() { }

    virtual int runCommand(SocketClient *cli, int argc, char **argv) = 0;
    virtual int runCommandWithRawData(SocketClient *cli, char *data, int size) = 0;

    const char *getCommand() { return mCommand; }
    bool isHandleRawData() { return mHandleRawData; }

private:
    void init(const char *cmd, bool handleRawData);
};

typedef android::sysutils::List<TctdCommand *> TctdCommandCollection;
#endif
