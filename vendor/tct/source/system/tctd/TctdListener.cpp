/* Copyright (C) 2016 Tcl Corporation Limited */
#define LOG_TAG "TctdListener"

#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include <cutils/log.h>
#include <sysutils/SocketClient.h>
#include <cutils/properties.h>

#include "TctdListener.h"
#include "TctdCommand.h"
#include "ResponseCode.h"

#include "TraceabilityManager.h"
#include "PrivilegeManager.h"
#include "TctFeedbackManager.h"


#define DUMP_ARGS 1

#define UNUSED __attribute__((unused))

TctdListener::TctdListener(const char *socketName, bool withSeq) :
                            SocketListener(socketName, true, withSeq) {
    init(socketName, withSeq);
}

TctdListener::TctdListener(const char *socketName) :
                            SocketListener(socketName, true, false) {
    init(socketName, false);
}

TctdListener::TctdListener(int sock) :
                            SocketListener(sock, true) {
    init(NULL, false);
}

void TctdListener::init(const char *socketName UNUSED, bool withSeq) {
    mCommands = new TctdCommandCollection();
    errorRate = 0;
    mCommandCount = 0;
    mWithSeq = withSeq;

    registerCmd(new TraceabilityCmd());
    registerCmd(new PrivilegeCmd());
    registerCmd(new TctFeedbackCmd());
}

void TctdListener::dumpArgs(int argc, char **argv, int argObscure) {
#if DUMP_ARGS
    char buffer[4096];
    char *p = buffer;

    memset(buffer, 0, sizeof(buffer));
    int i;
    for (i = 0; i < argc; i++) {
        unsigned int len = strlen(argv[i]) + 1; // Account for space
        if (i == argObscure) {
            len += 2; // Account for {}
        }
        if (((p - buffer) + len) < (sizeof(buffer)-1)) {
            if (i == argObscure) {
                *p++ = '{';
                *p++ = '}';
                *p++ = ' ';
                continue;
            }
            strcpy(p, argv[i]);
            p+= strlen(argv[i]);
            if (i != (argc -1)) {
                *p++ = ' ';
            }
        }
    }
    SLOGD("%d, %s", argc, buffer);
#endif
}

bool TctdListener::onDataAvailable(SocketClient *c) {
    char buffer[CMD_BUF_SIZE];
    int len;

    len = TEMP_FAILURE_RETRY(read(c->getSocket(), buffer, sizeof(buffer)));
    if (len < 0) {
        SLOGE("read() failed (%s)", strerror(errno));
        return false;
    } else if (!len) {
        return false;
    }

    int offset = 0;
    int i;

    if (dispatchCommandWithRawData(c, buffer, len)) {
        for (i = 0; i < len; i++) {
            if (buffer[i] == '\0') {
                /* IMPORTANT: dispatchCommand() expects a zero-terminated string */
                dispatchCommand(c, buffer + offset);
                offset = i + 1;
            }
        }
    }

    return true;
}

void TctdListener::registerCmd(TctdCommand *cmd) {
    mCommands->push_back(cmd);
}

void TctdListener::dispatchCommand(SocketClient *cli, char *data) {
    TctdCommandCollection::iterator i;
    int argc = 0;
    char *argv[TctdListener::CMD_ARGS_MAX];
    char tmp[TctdListener::CMD_BUF_SIZE];
    char *p = data;
    char *q = tmp;
    char *qlimit = tmp + sizeof(tmp) - 1;
    bool esc = false;
    bool quote = false;
    int k;
    bool haveCmdNum = !mWithSeq;

    memset(argv, 0, sizeof(argv));
    memset(tmp, 0, sizeof(tmp));
    while(*p) {
        if (*p == '\\') {
            if (esc) {
                if (q >= qlimit)
                    goto overflow;
                *q++ = '\\';
                esc = false;
            } else
                esc = true;
            p++;
            continue;
        } else if (esc) {
            if (*p == '"') {
                if (q >= qlimit)
                    goto overflow;
                *q++ = '"';
            } else if (*p == '\\') {
                if (q >= qlimit)
                    goto overflow;
                *q++ = '\\';
            } else {
                cli->sendMsg(500, "Unsupported escape sequence", false);
                goto out;
            }
            p++;
            esc = false;
            continue;
        }

        if (*p == '"') {
            if (quote)
                quote = false;
            else
                quote = true;
            p++;
            continue;
        }

        if (q >= qlimit)
            goto overflow;
        *q = *p++;
        if (!quote && *q == ' ') {
            *q = '\0';
            if (!haveCmdNum) {
                char *endptr;
                int cmdNum = (int)strtol(tmp, &endptr, 0);
                if (endptr == NULL || *endptr != '\0') {
                    cli->sendMsg(500, "Invalid sequence number", false);
                    goto out;
                }
                cli->setCmdNum(cmdNum);
                haveCmdNum = true;
            } else {
                if (argc >= CMD_ARGS_MAX)
                    goto overflow;
                argv[argc++] = strdup(tmp);
            }
            memset(tmp, 0, sizeof(tmp));
            q = tmp;
            continue;
        }
        q++;
    }

    *q = '\0';
    if (argc >= CMD_ARGS_MAX)
        goto overflow;
    argv[argc++] = strdup(tmp);
#if 0
    for (k = 0; k < argc; k++) {
        SLOGD("arg[%d] = '%s'", k, argv[k]);
    }
#endif

    if (quote) {
        cli->sendMsg(500, "Unclosed quotes error", false);
        goto out;
    }

    if (errorRate && (++mCommandCount % errorRate == 0)) {
        /* ignore this command - let the timeout handler handle it */
        SLOGE("Faking a timeout");
        goto out;
    }

    for (i = mCommands->begin(); i != mCommands->end(); ++i) {
        TctdCommand *c = *i;

        if (!strcmp(argv[0], c->getCommand())) {
            if (c->runCommand(cli, argc, argv)) {
                SLOGW("Handler '%s' error (%s)", c->getCommand(), strerror(errno));
            }
            goto out;
        }
    }
    cli->sendMsg(500, "Command not recognized", false);
out:
    int j;
    for (j = 0; j < argc; j++)
        free(argv[j]);
    return;

overflow:
    LOG_EVENT_INT(78001, cli->getUid());
    cli->sendMsg(500, "Command too long", false);
    goto out;
}

bool TctdListener::dispatchCommandWithRawData(SocketClient *cli, char *data, int size) {
    TctdCommandCollection::iterator i;
    char tmp[CMD_BUF_SIZE];
    char *p = data;
    char *q = tmp;
    bool haveCmdNum = !mWithSeq;
    bool isCmdGot = false;

    memset(tmp, 0, sizeof(tmp));
    while (p < data + size) {
        *q = *p++;
        if (*q == ' ' || *q == '\0') {
            *q = '\0';
            if (!haveCmdNum) {
                char *endptr;
                int cmdNum = (int)strtol(tmp, &endptr, 0);
                if (endptr == NULL || *endptr != '\0') {
                    cli->sendMsg(500, "Invalid sequence number", false);
                    break;
                }
                cli->setCmdNum(cmdNum);
                haveCmdNum = true;
            } else {
                isCmdGot = true;
                break;
            }
            memset(tmp, 0, sizeof(tmp));
            q = tmp;
            continue;
        }
        q++;
    }

    if (isCmdGot) {
        TctdCommand *c = lookupCommand(tmp);
        if (c != NULL && c->isHandleRawData()) {
            c->runCommandWithRawData(cli, p, size - (p - data));
            return false;
        }
    }

    return true;
}

TctdCommand *TctdListener::lookupCommand(const char *command) {
    TctdCommandCollection::iterator i;

    for (i = mCommands->begin(); i != mCommands->end(); ++i) {
        if (!strcmp((*i)->getCommand(), command)) {
            return (*i);
        }
    }
    return NULL;
}


TctdListener::TraceabilityCmd::TraceabilityCmd() :
                               TctdCommand("traceability", true) {
}

int TctdListener::TraceabilityCmd::runCommand(SocketClient *cli,
                                                      int argc, char **argv) {
    SLOGW("Command %s has no runCommand handler!", getCommand());
    errno = ENOSYS;
    return -1;
}


#if defined(IDOL4S_TFA9897_FTM_CAL) || defined(IDOL4S_VDF_TFA9897_FTM_CAL)
#ifdef __cplusplus
extern "C" {
#endif
int tfa98xx_ftm_calibration(void);
int tfa9897_speaker_resonance(int model);
#ifdef __cplusplus
}
#endif
#endif

//                                               open    close
    //phone bottom mic --> phone top receiver         aa      ab
    //phone top mic --> phone bottom receiver         ac      ad
    //phone bottom mic --> phone top speaker          ae      af
    //phone top mic --> phone bottom speaker          ag      ah
    //phone bottom(right) mic ->  headset             ai      aj
    //phone top(left) mic ->  headset                 ak      al
    //phone sub mic --> phone top receiver            am      an

    //headset mic ->  phone top(left) speaker         ba      bb
    //headset mic ->  phone bottom(right) speaker     bc      bd
    //headset mic ->  phone top(left) receiver        be      bf
    //headset mic ->  phone bottom(right) receiver    bj      bh

int TctdListener::TraceabilityCmd::runCommandWithRawData(SocketClient *cli, char *data, int size) {
    TraceabilityManager *tm = TraceabilityManager::Instance();
    int rc = 0;
    //int32_t initRet = 0;
    int32_t initEfuse = 0;

    SLOGD("runCommandWithRawData[%d]", size);
    SLOGD("%s", data);
    switch (data[0]) {
    case 'r':
        SLOGI("Read traceability data");
        if (0 == tm->traceability_read_data(cli)) {
            SLOGI("Send the traceability data read to socket");
            return 0;
        } else {
            SLOGE("ERROR When Read Traceability");
        }
        break;
    case 'w':
        SLOGI("Write traceability data");
        rc = tm->traceability_write_data(cli, &data[1], size-1);
        break;
    case 'a':
         switch (data[1]) {
         case 'a':
             tm->bottom_mic_to_top_receiver_open();
             break;
         case 'b':
             tm->bottom_mic_to_top_receiver_close();
             break;
         case 'c':
             tm->top_mic_to_bottom_receiver_open();
             break;
         case 'd':
             tm->top_mic_to_bottom_receiver_close();
             break;
         case 'e':
             tm->bottom_mic_to_top_speaker_open();
             break;
         case 'f':
             tm->bottom_mic_to_top_speaker_close();
             break;
         case 'g':
             tm->top_mic_to_bottom_speaker_open();
             break;
         case 'h':
             tm->top_mic_to_bottom_speaker_close();
             break;
         case 'i':
             tm->bottom_mic_to_headset_open();
             break;
         case 'j':
             tm->bottom_mic_to_headset_close();
             break;
         case 'k':
             tm->top_mic_to_headset_open();
             break;
         case 'l':
             tm->top_mic_to_headset_close();
             break;
         //case 'm':
         //    tm->top_mic_to_top_receiver_open();
         //    break;
         //case 'n':
         //    tm->top_mic_to_top_receiver_close();
         //    break;
         }
         break;
    case 'b':
         switch (data[1]) {
         case 'a':
             tm->headset_mic_to_top_speaker_start();
             break;
         case 'b':
             tm->headset_mic_to_top_speaker_end();
             break;
         case 'c':
             tm->headset_mic_to_bottom_speaker_start();
             break;
         case 'd':
             tm->headset_mic_to_bottom_speaker_end();
             break;
         case 'e':
             tm->headset_mic_to_top_receiver_start();
             break;
         case 'f':
             tm->headset_mic_to_top_receiver_end();
             break;
         case 'g':
             tm->headset_mic_to_bottom_receiver_start();
             break;
         case 'h':
             tm->headset_mic_to_bottom_receiver_end();
             break;
         }
         break;

     case 'p':
        system("echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
        system("echo 1401600 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
        break;
    case 'q':
        system("echo userspace > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor");
        system("echo 691200 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed");
        break;

    case 's':
    ////Enter FTM Mode////
        SLOGD("QLIB_DIAG_CONTROL_F");
        system("tct-diag -p '0x29 0x03 0x00'");

    ////LTE B7////
        SLOGD("QLIB_FTM_SET_SECONDARY_CHAIN");
        system("tct-diag -p '0x4B 0x0B 0x23 0x00 0x79 0x00 0x00 0x00'");
        SLOGD("QLIB_FTM_SET_MODE");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x07 0x00 0x23 0x00'");
        SLOGD("QLIB_FTM_LTE_SET_TX_BANDWIDTH");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x90 0x01 0x03'");
        SLOGD("QLIB_FTM_LTE_SET_RX_BANDWIDTH");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x91 0x01 0x03'");
        SLOGD("QLIB_FTM_SET_CHAN");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x08 0x00 0x6C 0x52 0x00 0x00'");
        SLOGD("QLIB_FTM_SET_TX_ON");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x02 0x00'");
        SLOGD("QLIB_FTM_LTE_SET_TX_WAVEFORM");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x93 0x01 0x01 0x32 0x00 0x00'");
        SLOGD("QLIB_FTM_SET_PA_RANGE");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x36 0x00 0x00 0x00'");
        SLOGD("QLIB_FTM_SET_PA_STATE");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0xB6 0x00 0x02'");
        SLOGD("QLIB_FTM_SET_SMPS_PA_BIAS_OVERRIDE");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x12 0x00 0x01'");
        SLOGD("QLIB_FTM_SET_SMPS_PA_BIAS_VAL");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x11 0x00 0xAC 0x0D'");
        SLOGD("QLIB_FTM_SET_TX_GAIN_INDEX");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x92 0x01 0x50 0x00'");
        break;
    case 't':
    ////STOP LTE B7////
        SLOGD("QLIB_FTM_LTE_STOP_TX_WAVEFORM");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x94 0x01'");
        SLOGD("QLIB_FTM_SET_TX_OFF");
        system("tct-diag -p '0x4B 0x0B 0x1D 0x00 0x03 0x00'");
        SLOGD("QLIB_FTM_RF_MODE_EXIT");
        system("tct-diag -p '0x4B 0x0B 0x14 0x00 0x66 0x02 0x00 0x00 0x0C 0x00'");

    ////Enter OnLine Mode////
        SLOGD("QLIB_DIAG_CONTROL_F");
        system("tct-diag -p '0x29 0x04 0x00'");
        break;

#if defined(IDOL4S_TFA9897_FTM_CAL) || defined(IDOL4S_VDF_TFA9897_FTM_CAL)
    case '*':
    SLOGE("########## tfa98xx_ftm_calibration #############\n");
        rc = tfa98xx_ftm_calibration();
    break;
    case '$':
    SLOGE("########## tfa9897_speaker_resonance #############\n");
        rc = tfa9897_speaker_resonance(0);
    break;
#endif

    case 'y':
        rc = tm->switchChging(true);
        break;
    case 'z':
        rc = tm->switchChging(false);
        break;
    case 'c':
        if (data[1] == 'c')  {
            tm->headset_mic_to_headset_speaker_open();
        }else if (data[1] == '#'){
            tm->headset_mic_to_headset_speaker_close();
        }
        break;
    case 'n':
        char buffer[128];
        if (0 == tm->getNFCFirmware(buffer, 128) ) {
            cli->sendMsg(ResponseCode::CommandOkay, buffer, false);
            return 0;
        }
        else {
            SLOGE("ERROR When Read NFC firmware");
        }
        break;
    case 'k':
        SLOGI("iam in case K");
        tm->write_calibration();
        break;
    case 'f':
        if (0 == tm->read_nv_item_and_send(cli, &data[1], size-1)) {
            return 0;
        } else {
            SLOGE("ERROR When Read NV");
            cli->sendMsg(ResponseCode::OperationFailed, "ERROR", true);
        }
        break;
    case 'm':
        SLOGI("iam in case m");
        tm->try_to_kill_rawdata();
        break;

/*    case 'h':
        initRet = tm->init_hdcp();
        cli->sendBinaryMsg(ResponseCode::TraceabilityReadResult, (char *)(&initRet), sizeof(initRet));
        break;
*/
    case 'e':
        initEfuse = tm->init_efuse();
        cli->sendBinaryMsg(ResponseCode::TraceabilityReadResult, (char *)(&initEfuse), sizeof(initEfuse));
        break;
    case 'd':
        tm->read_ali_wechat_key_status(cli);
        break;
    case 'o':
        system("echo 1 > /sys/class/power_supply/battery/tcl_fixtemp");
        break;
    case 'g':
        system("echo 0 > /sys/class/power_supply/battery/tcl_fixtemp");
        break;

    default:
        SLOGE("Invalid msg received from Java Layer");
        break;
    }

    if (!rc) {
        cli->sendMsg(ResponseCode::CommandOkay, "Traceability operation succeeded", false);
    } else {
        cli->sendMsg(ResponseCode::OperationFailed, "Traceability operation failed", true);
    }

    return 0;
}

TctdListener::PrivilegeCmd::PrivilegeCmd() :
                 TctdCommand("privilege") {
}

int TctdListener::PrivilegeCmd::runCommand(SocketClient *cli,
                                                      int argc, char **argv) {
    dumpArgs(argc, argv, -1);

    if (argc < 2) {
        cli->sendMsg(ResponseCode::CommandSyntaxError, "Missing Argument", false);
        return 0;
    }

    PrivilegeManager *pm = PrivilegeManager::Instance();
    int rc = 0;

    if (!pm->isPermitted(cli, argv[1])) {
        cli->sendMsg(ResponseCode::CommandNoPermission, "No permission to run privilege commands", false);
        return 0;
    }

    if (!strcmp(argv[1], "silent_install")) {
        if (argc < 3) {
            cli->sendMsg(ResponseCode::CommandSyntaxError, "Usage: privilege slient_install apkpath", false);
            return 0;
        }
        rc = pm->silentInstall(argc, argv);
    } else if (!strcmp(argv[1], "root_check")) {
        if (argc != 2) {
            cli->sendMsg(ResponseCode::CommandSyntaxError, "Usage: privilege root_check", false);
            return 0;
        }
        rc = pm->rootCheck(cli);
    } else if (!strcmp(argv[1], "trigger_modem_crash")) {
        rc = pm->triggerModemCrash();
/*[FEATURE]-Add-BEGIN by LWS,2015/09/07, FR-1067379 ,add write splash by tctd*/
    } else if (!strcmp(argv[1], "rewrite_splash")) {
    if(argc < 3) {
            cli->sendMsg(ResponseCode::CommandSyntaxError, "Usage: privilege rewrite_splash file_path", false);
        return 0;
    }
    rc = pm->reWriteSplash(argv) ;
/*[FEATURE]-Add-END*/
    } else if (!strcmp(argv[1], "efs_backup")) {
        if (argc != 5) {
            cli->sendMsg(ResponseCode::CommandSyntaxError, "Usage: privilege efs_backup tempdir tarpath <true|false>", false);
            return 0;
        }
        rc = pm->efsBackup(argv[2], argv[3], !strcmp(argv[4], "true") ? 1 : 0);
    } else if (!strcmp(argv[1], "setinproductionflag")) {
        if (argc != 3) {
            cli->sendMsg(ResponseCode::CommandSyntaxError, "Usage: privilege setinproductionflag <true|false>", false);
            return 0;
        }
        rc = pm->setInProductionFlag(argv[2]);
    } else {
        cli->sendMsg(ResponseCode::CommandSyntaxError, "Unknown privilege cmd", false);
    }

    if (!rc) {
        cli->sendMsg(ResponseCode::CommandOkay, "Privilege operation succeeded", false);
    } else {
        cli->sendMsg(ResponseCode::OperationFailed, "Privilege operation failed", true);
    }

    return 0;
}

int TctdListener::PrivilegeCmd::runCommandWithRawData(SocketClient *cli, char *data, int size) {
    SLOGW("Command %s has no runCommandWithBinaryData hanlder!", getCommand());
    errno = ENOSYS;
    return -1;
}

TctdListener::TctFeedbackCmd::TctFeedbackCmd() :
                 TctdCommand("tctfeedback") {
}

int TctdListener::TctFeedbackCmd::runCommand(SocketClient *cli,
                                                      int argc, char **argv) {
    dumpArgs(argc, argv, -1);

    if (argc < 2) {
        SLOGD("DumpPmic: Missing Argument\n");
        cli->sendMsg(ResponseCode::CommandSyntaxError, "Missing Argument", false);
        return 0;
    }

    TctFeedbackManager *tm = TctFeedbackManager::Instance();
    int rc = 0;

    if (!strcmp(argv[1], "dump_pmic")) {
        if (argc != 2) {
            SLOGD("Usage: tctfeedback dump_pmic \n");
            cli->sendMsg(ResponseCode::CommandSyntaxError, "Usage: tctfeedback dump_pmic", false);
            return 0;
        }
        rc = tm->dumpPmic(cli);
    } else {
        SLOGD("Unknown TctFeedback cmd \n");
        cli->sendMsg(ResponseCode::CommandSyntaxError, "Unknown TctFeedback cmd", false);
    }

    if (!rc) {
        SLOGD("TctFeedback operation succeeded\n");
        cli->sendMsg(ResponseCode::CommandOkay, "TctFeedback operation succeeded", false);
    } else {
        SLOGD("TctFeedback operation failed\n");
        cli->sendMsg(ResponseCode::OperationFailed, "TctFeedback operation failed", true);
    }

    return 0;
}

int TctdListener::TctFeedbackCmd::runCommandWithRawData(SocketClient *cli, char *data, int size) {
    SLOGW("Command %s has no runCommandWithBinaryData hanlder!", getCommand());
    errno = ENOSYS;
    return -1;
}

