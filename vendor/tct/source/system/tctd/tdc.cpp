#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>

#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>

#include <cutils/sockets.h>
#include <private/android_filesystem_config.h>

static void usage(char *progname);
static int do_monitor(int sock, int stop_after_cmd);
static int do_cmd(int sock, int argc, char **argv);

int main(int argc, char **argv) {
    int sock;
    int wait_for_socket;
    char *progname;

    progname = argv[0];

    wait_for_socket = argc > 1 && strcmp(argv[1], "--wait") == 0;
    if(wait_for_socket) {
        argv++;
        argc--;
    }

    if(argc < 2) {
        usage(progname);
        exit(5);
    }

    while ((sock = socket_local_client("tctd",
                                 ANDROID_SOCKET_NAMESPACE_RESERVED,
                                 SOCK_STREAM)) < 0) {
        if(!wait_for_socket) {
            fprintf(stderr, "Error connecting (%s)\n", strerror(errno));
            exit(4);
        } else {
            sleep(1);
        }
    }

    if (!strcmp(argv[1], "monitor")) {
        exit(do_monitor(sock, 0));
    } else {
        exit(do_cmd(sock, argc, argv));
    }
}

static int do_cmd(int sock, int argc, char **argv) {
    char final_cmd[255] = "0 "; /* 0 is a (now required) sequence number */
    int i;
    size_t ret;

    for (i = 1; i < argc; i++) {
        char *cmp;

        if (!strchr(argv[i], ' '))
            asprintf(&cmp, "%s%s", argv[i], (i == (argc -1)) ? "" : " ");
        else
            asprintf(&cmp, "\"%s\"%s", argv[i], (i == (argc -1)) ? "" : " ");

        ret = strlcat(final_cmd, cmp, sizeof(final_cmd));
        if (ret >= sizeof(final_cmd))
            abort();
        free(cmp);
    }

    if (write(sock, final_cmd, strlen(final_cmd) + 1) < 0) {
        perror("write");
        return errno;
    }

    return do_monitor(sock, 1);
}

static int do_monitor(int sock, int stop_after_cmd) {
    char *buffer = (char *)malloc(4096);

    if (!stop_after_cmd)
        printf("[Connected to Tctd]\n");

    while(1) {
        fd_set read_fds;
        struct timeval to;
        int rc = 0;

        to.tv_sec = 10;
        to.tv_usec = 0;

        FD_ZERO(&read_fds);
        FD_SET(sock, &read_fds);

        if ((rc = select(sock +1, &read_fds, NULL, NULL, &to)) < 0) {
            fprintf(stderr, "Error in select (%s)\n", strerror(errno));
            free(buffer);
            return errno;
        } else if (!rc) {
            continue;
            fprintf(stderr, "[TIMEOUT]\n");
            return ETIMEDOUT;
        } else if (FD_ISSET(sock, &read_fds)) {
            memset(buffer, 0, 4096);
            if ((rc = read(sock, buffer, 4096)) <= 0) {
                if (rc == 0)
                    fprintf(stderr, "Lost connection to Tctd - did it crash?\n");
                else
                    fprintf(stderr, "Error reading data (%s)\n", strerror(errno));
                free(buffer);
                if (rc == 0)
                    return ECONNRESET;
                return errno;
            }

            int offset = 0;
            int i = 0;

            if (rc >= 8 && buffer[3] == 0) {
                //a binary message, 4 bytes for the code & null + 4 bytes for the len
                int code;
                uint32_t len;
                char tmp[4];
                memcpy(tmp, buffer, 4);
                code = atoi(tmp);
                memcpy(&len, buffer + 4, 4);
                len = ntohl(len);
                printf("%d %d\n", code, len);
                for (i = 8; i < len; ++i) {
                    printf("%02x", buffer[i]);
                    if (i % 16 == 7) {
                        printf("\n");
                    } else {
                        printf(" ");
                    }
                }
                printf("\n");
                if (stop_after_cmd) {
                    if (code >= 200 && code < 600) {
                        return 0;
                    }
                }
            } else {
                for (i = 0; i < rc; i++) {
                    if (buffer[i] == '\0') {
                        int code;
                        char tmp[4];

                        strncpy(tmp, buffer + offset, 3);
                        tmp[3] = '\0';
                        code = atoi(tmp);

                        printf("%s\n", buffer + offset);
                        if (stop_after_cmd) {
                            if (code >= 200 && code < 600)
                                return 0;
                        }
                        offset = i + 1;
                    }
                }
            }
        }
    }
    free(buffer);
    return 0;
}

static void usage(char *progname) {
    fprintf(stderr,
            "Usage: %s [--wait] <monitor>|<cmd> [arg1] [arg2...]\n", progname);
 }

