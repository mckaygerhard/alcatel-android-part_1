/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_audioloop_cmd.h            */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 09/07/14| Liu Shishun     | PR-725046          | SIM detect command         */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#ifndef __TCT_SIM_H__
#define __TCT_SIM_H__

#define TCT_SIM_CMD                  0x0000

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 command1;
    uint8 command2;
} __attribute__((packed)) req_type_tct_sim_cmd_exec;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 command1;
    uint8 command2;
    uint8 result;
} __attribute__((packed)) rsp_type_tct_sim_cmd_exec;


PACK(void *) tct_sim_cmd_func(PACK(void *)req_pkt, uint16 pkt_len);

#endif /* __TCT_SIM_CMD_H__ */
