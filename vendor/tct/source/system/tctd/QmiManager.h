/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef _QMIMANAGER_H
#define _QMIMANAGER_H

#ifdef __cplusplus
#include <sysutils/SocketListener.h>

#include "qmi_client.h"
#include "qmi_tct_api_v01.h"

class QmiManager {
private:
    static QmiManager *sInstance;

private:
    SocketListener        *mBroadcaster;

public:
    virtual ~QmiManager();

    int start();
    int stop();

    void setBroadcaster(SocketListener *sl) { mBroadcaster = sl; }
    SocketListener *getBroadcaster() { return mBroadcaster; }

    static QmiManager *Instance();

    int qmiClientInit(qmi_client_ind_cb ind_cb, void *ind_cb_data, qmi_client_type *user_handle);
    int qmiClientRelease(qmi_client_type *user_handle);

private:
    QmiManager();
};
#endif /* __cplusplus */

#endif
