/* Copyright (C) 2016 Tcl Corporation Limited */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <linux/ioctl.h>

//[BUGFIX]-Add-BEGIN by TCTNB.(Shishun.Liu), 2014/06/05 FR694468, Add NFC fireware version in MMITest
#include <ctype.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <string.h>
#include <errno.h>

#define PN544_SET_PWR   _IOW(0xe9, 0x01, unsigned int)
//[BUGFIX]-Add-END by TCTNB.(Shishun.Liu)
//[BUGFIX]-Add-BEGIN by TCTNB.(Shishun.Liu), 2015/01/09 FR887772, HDCP init check

#include <binder/IServiceManager.h>
#include <binder/IBinder.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IPCThreadState.h>
#include <private/binder/binder_module.h>
//#include "Dx_Hdcp_Provisioning.h"
using namespace android;

//[BUGFIX]-Add-END by TCTNB.(Shishun.Liu)
#define LOG_TAG "TraceabilityManager"

#include <cutils/log.h>
#include <cutils/properties.h>

#include "TraceabilityManager.h"
#include "ResponseCode.h"

#include "trace_partition.h"
#include "trace_nv_read.h"


bool TraceabilityManager::is_product(const char *buffer)
{
#ifdef TCT_TARGET_MMITEST_PRODUCT
    SLOGD("tctd product = %s"   , TCT_TARGET_MMITEST_PRODUCT);
    return strcmp(buffer, TCT_TARGET_MMITEST_PRODUCT)==0 ? true : false;
#else
    SLOGE("TCT_TARGET_MMITEST_PRODUCT not defined");
    return false;
#endif
}

int traceability_set_multiple_prop(void){
    char buf[4];
    char AAC_Bytes[4] = {0x57, 0x68, 0x79, 0x8a };
    char LC_Bytes[4] = {0x1F, 0x2E, 0x3C, 0x4B };

    TraceabilityManager *tm = TraceabilityManager::Instance();
    memset(buf, 0, sizeof(buf));
    tm->readData(TCT_SPEAKER_INFO_OFFSET, buf, 4);

    if (buf[0]==AAC_Bytes[0]
        && buf[1]==AAC_Bytes[1]
        && buf[2]==AAC_Bytes[2]
        && buf[3]==AAC_Bytes[3]){
        property_set("ro.tct.multiple", "AAC");
    }
    else if (buf[0]==LC_Bytes[0]
            && buf[1]==LC_Bytes[1]
            && buf[2]==LC_Bytes[2]
            && buf[3]==LC_Bytes[3]){
        property_set("ro.tct.multiple", "LC");
    }
    else{
        property_set("ro.tct.multiple", "unknown");
        ALOGE("Unknown multiple!");
        return -1;
    }
    return 0;
}

int open_file_set_prop(char * path, char * which_property){

    int fd,ret;
    char buf[100] = {0};
    char swap_buf[20] = {0};
    char *partition_type;

    if( (fd = open(path, O_RDONLY)) < 0 ) {
        ALOGE("open %s failed\n", path);
        return -1;
    }

    if( (ret = read(fd, buf, sizeof(buf)) )< 0){
        ALOGE("read fd %d failed : %d\n", fd, errno);
        close(fd);
        return -1;
    }

    close(fd);

    if((partition_type = strstr(which_property,"reco")) != NULL) {
	memcpy(swap_buf, buf+TCT_RECO_OFFSET, TCT_VERSION_LEN);
    }
    else if((partition_type = strstr(which_property,"sml")) != NULL) {
	memcpy(swap_buf, buf+TCT_SML_OFFSET, TCT_VERSION_LEN);
    }
    else {
	memcpy(swap_buf, buf, TCT_VERSION_LEN);
    }

    property_set(which_property, swap_buf);

    return 0;
}
#define DEFAULT_FILE "sys/kernel/debug/dynamic_debug/control"
#define KERNEL_CONFIG_PROP "ro.tct.kernelconfig"
void differ_perf_from_default(){
    int ret = -1;
    if((ret = access(DEFAULT_FILE, F_OK|R_OK)) != 0){
         property_set(KERNEL_CONFIG_PROP,"Performance");
    } else {
         property_set(KERNEL_CONFIG_PROP,"Default");
    }
}
void tctd_get_ver(void){

    int flag;

    if((flag = open_file_set_prop(FBOOVER,BOOT_VER_PROPERTY)) < 0){
        ALOGE("%s set %s failed, ",__func__,BOOT_VER_PROPERTY);
    }
    if((flag = open_file_set_prop(FSYSVER,SYSTEM_VER_PROPERTY)) < 0){
        ALOGE("%s set %s failed, ",__func__,SYSTEM_VER_PROPERTY);
    }
    if((flag = open_file_set_prop(MMCBLKR,RECOVERY_VER_PROPERTY)) < 0){
        ALOGE("%s set %s failed, ",__func__,RECOVERY_VER_PROPERTY);
    }
    if((flag = open_file_set_prop(FNONVER,MODEM_VER_PROPERTY)) < 0){
        ALOGE("%s set %s failed, ",__func__,MODEM_VER_PROPERTY);
    }
//    we not use it anymore in idol4s
//    if((flag = open_file_set_prop(FCUSVER,CUSTPACK_VER_PROPERTY)) < 0){
//        ALOGE("%s set %s failed, ",__func__,CUSTPACK_VER_PROPERTY);
//    }
    if((flag = open_file_set_prop(MMCBLKX,SIMLOCK_VER_PROPERTY)) < 0){
	ALOGE("%s set %s failed, ",__func__,SIMLOCK_VER_PROPERTY);
    }
}

int traceability_setprop(int offset, int size, char * prop_name){

    char property[24];
    memset(property, 0, sizeof(property));
    TraceabilityManager *tm = TraceabilityManager::Instance();
    tm->readData(offset, property, size);
    property_set(prop_name, property);

    return 0;
}
void smartcare_set_property(void){

    traceability_setprop(TCT_HANDSET_REF_OFFSET,TCT_HANDSET_REF_LEN, "ro.tct.handsetref");
    traceability_setprop(TCT_PTM_OFFSET,TCT_PTM_LEN, "ro.tct.ptm");
    traceability_setprop(TCT_PTM_OFFSET,TCT_PTM_LEN, "ro.tct.trace.pth");
    traceability_setprop(TCT_SERIAL_NO_OFFSET,TCT_SERIAL_NO_LEN, "ro.tct.trace.bsn");
    traceability_setprop(TCT_PTS_OFFSET,TCT_PTS_LEN, "ro.tct.trace.pts");
    traceability_setprop(TCT_CU_REF_OFFSET,TCT_CU_REF_LEN, "ro.tct.curef");

}
int create_dir(const char * path){
    int i = 0;
    int ret = -1;
    char dir_name[256];
    mode_t mode = S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH;
    mode_t oldmod = umask(0);
    int len = strlen(path);

    strcpy(dir_name,path);
    if(dir_name[len-1] != '/')
        strcat(dir_name,"/");

    len = strlen(dir_name);
    for(i = 1; i < len; i++){
        if(dir_name[i] == '/'){
            dir_name[i] = 0;
            if((ret = access(dir_name, F_OK|R_OK)) != 0){
                if(mkdir(dir_name,mode) != 0){
                    ALOGE("make %s failed: %d %s\n", dir_name, errno, strerror(errno));
                    umask(oldmod);
                    return -1;
                }
            }
            dir_name[i] = '/';
        }
    }
    umask(oldmod);
    return 0;
}

int traceability_write_btwifi_to_file(char * file_name, char * buf){

    int fd = -1;
    int ret = 0;

    if(create_dir(CONFIG_DIR) != 0){
        ALOGE("make %s failed: %d %s\n", CONFIG_DIR, errno, strerror(errno));
        return -1;
    }
    if((fd = open(file_name, O_CREAT|O_RDWR, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)) < 0) {
        ALOGE("open file %s failed : %d %s\n", file_name, errno, strerror(errno));
    }
    if( (ret = write(fd, buf, strlen(buf)) )< 0){
        ALOGE("write file %s %d failed : %d %s\n", file_name, fd, errno, strerror(errno));
        close(fd);
        return -1;
    }
    close(fd);

    if(chmod(file_name,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)==-1)
    {
       ALOGE("failed to chmod");
       return -1;
    }
    return 0;
}

int traceability_set_bt_wifi_address(void){

    char buf[24];
    char swap_buf[24];

    TraceabilityManager *tm = TraceabilityManager::Instance();

    //bt addr
    memset(buf, 0, sizeof(buf));
    tm->readData(TCT_BT_ADDR_OFFSET, buf, 10);

    sprintf(swap_buf, "%02x:%02x:%02x:%02x:%02x:%02x",
        buf[5], buf[4], buf[3], buf[2], buf[1], buf[0]);

    property_set("ro.boot.btmacaddr", swap_buf);
    traceability_write_btwifi_to_file(BTADDR, swap_buf);


    //wifi addr
    memset(buf, 0, sizeof(buf));
    tm->readData(TCT_WIFI_ADDR_OFFSET, buf, 10);
    sprintf(swap_buf, "%02x:%02x:%02x:%02x:%02x:%02x",
        buf[5], buf[4], buf[3], buf[2], buf[1], buf[0]);

    property_set("ro.tct.wifimac", swap_buf);
    traceability_write_btwifi_to_file(WIFIADDR, swap_buf);

    return 0;

}

// [FEATURE]-Add-BEGIN by TCTNB.(shishun.liu), 2015/05/18 FR-1003416, Use CU as mini test in production line
#define MINI_CU_FLAG_OFFSET   (310+67)
int __mini_cu_flag(void)
{
    unsigned char ucMini_CU_Flag[4];

    TraceabilityManager *tm = TraceabilityManager::Instance();
    tm->readData(MINI_CU_FLAG_OFFSET, (char *)ucMini_CU_Flag, 4);

    SLOGD("mini_cu_flag = 0x%x,0x%x,0x%x,0x%x",
            ucMini_CU_Flag[0], ucMini_CU_Flag[1], ucMini_CU_Flag[2], ucMini_CU_Flag[3]);

    if(ucMini_CU_Flag[0]==0x87
            && ucMini_CU_Flag[1]==0x96
            && ucMini_CU_Flag[2]==0xA5
            && ucMini_CU_Flag[3]==0xB4) {
        property_set("dev.tct.MMITest", "true");
        property_set("persist.sys.usb.config", "mtp,diag,adb");
        property_set("persist.sys.usb.protect", "0");
        SLOGD("set property [dev.tct.MMITest] = \"true\"");
        SLOGD("set property [persist.sys.usb.config] = \"mtp,diag,adb\"");
        SLOGD("set property [persist.sys.usb.protect] = \"0\"");
    }

    return 0;
}
// [FEATURE]-Add-END by TCTNB.(shishun.liu), 2015/05/18 PR-1003416

TraceabilityManager *TraceabilityManager::sInstance = NULL;

TraceabilityManager *TraceabilityManager::Instance() {
    if (!sInstance)
        sInstance = new TraceabilityManager();
    return sInstance;
}

TraceabilityManager::TraceabilityManager() {
}

TraceabilityManager::~TraceabilityManager() {
}

int TraceabilityManager::start() {

    fd = TEMP_FAILURE_RETRY(open(MMCBLKP, O_RDWR));
    if (fd < 0) {
        return -errno;
    }
    differ_perf_from_default();
    tctd_get_ver();
    smartcare_set_property();
    traceability_set_bt_wifi_address();
    traceability_set_multiple_prop();
    __mini_cu_flag();

    return 0;
}

int TraceabilityManager::stop() {
    close(fd);
    return 0;
}

int TraceabilityManager::traceability_read_data(SocketClient *cli) {
    int ret;
    char buf[TRACEABILITY_MMITEST_SIZE + 1];
    memset(buf, 0, sizeof(buf));
    TraceabilityManager *tm = TraceabilityManager::Instance();
    tm->readData(TRACEABILITY_MMI_OFFSET, buf, TRACEABILITY_MMITEST_SIZE);
    ret = cli->sendBinaryMsg(ResponseCode::TraceabilityReadResult, buf, TRACEABILITY_MMITEST_SIZE);
    return ret;
}

int TraceabilityManager::traceability_write_data(SocketClient *cli, const char *buf, int size) {
    return writeData(TRACEABILITY_MMI_OFFSET, buf, size);
}

int TraceabilityManager::readData(int offset, char *buf, int size) {
    lseek64(fd, offset, SEEK_SET);
    read(fd, buf, size);
    return 0;
}

int TraceabilityManager::writeData(int offset, const char *buf, int size) {
    lseek64(fd, offset, SEEK_SET);
    write(fd, buf, size);
    sync();
    return 0;
}

extern "C" int trace_readData(int offset, char *buf, int size) {
    TraceabilityManager *tm = TraceabilityManager::Instance();
    return tm->readData(TRACEABILITY_DIAG_OFFSET + offset, buf, size);
}

extern "C" int trace_writeData(int offset, const char *buf, int size) {
    TraceabilityManager *tm = TraceabilityManager::Instance();
    return tm->writeData(TRACEABILITY_DIAG_OFFSET + offset, buf, size);
}

void TraceabilityManager::bottom_mic_to_top_speaker_open()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 53 -c /system/etc/ftm_test_config_simba6x -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 53 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::bottom_mic_to_top_speaker_close()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::bottom_mic_to_top_receiver_open()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 55 -c /system/etc/ftm_test_config_simba6x -d 365 -v 55 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 55 -c /system/etc/ftm_test_config_london -d 365 -v 55 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::bottom_mic_to_top_receiver_close()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::top_mic_to_bottom_receiver_open()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 56 -c /system/etc/ftm_test_config_simba6x -d 365 -v 50 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 56 -c /system/etc/ftm_test_config_london -d 365 -v 50 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::top_mic_to_bottom_receiver_close()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::top_mic_to_bottom_speaker_open()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 54 -c /system/etc/ftm_test_config_simba6x -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 54 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::top_mic_to_bottom_speaker_close()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::headset_mic_to_top_receiver_start()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 57 -c /system/etc/ftm_test_config_simba6x -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 57 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::headset_mic_to_top_receiver_end()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::headset_mic_to_bottom_speaker_start()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 62 -c /system/etc/ftm_test_config_simba6x -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 62 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::headset_mic_to_bottom_speaker_end()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::bottom_mic_to_headset_open()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 59 -c /system/etc/ftm_test_config_simba6x -d 365 -v 50 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 59 -c /system/etc/ftm_test_config_london -d 365 -v 50 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::bottom_mic_to_headset_close()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::top_mic_to_headset_open()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 60 -c /system/etc/ftm_test_config_simba6x -d 365 -v 50 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 60 -c /system/etc/ftm_test_config_london -d 365 -v 50 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::top_mic_to_headset_close()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::headset_mic_to_top_speaker_start()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 61 -c /system/etc/ftm_test_config_simba6x -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 61 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::headset_mic_to_top_speaker_end()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::headset_mic_to_bottom_receiver_start()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 58 -c /system/etc/ftm_test_config_simba6x -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 58 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::headset_mic_to_bottom_receiver_end()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

void TraceabilityManager::headset_mic_to_headset_speaker_open()
{
    if(is_product("simba6x")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 63 -c /system/etc/ftm_test_config_simba6x -d 365 -v 50 &");
        SLOGE("%s end\n", __func__);
    }
    else if(is_product("london")) {
        SLOGE("%s start\n", __func__);
        property_set("rw.tct.ftm.audio", "on");
        system("mm-audio-ftm -tc 63 -c /system/etc/ftm_test_config_london -d 365 -v 60 &");
        SLOGE("%s end\n", __func__);
    }
}

void TraceabilityManager::headset_mic_to_headset_speaker_close()
{
    SLOGE("%s start\n", __func__);
    property_set("rw.tct.ftm.audio", "off");
    SLOGE("%s end\n", __func__);
}

int sendrecv(int fp, unsigned char* in, int inLen, unsigned char* out)
{
    int ret = 0, i = 0, rspLen = 0;

    usleep(5000);
    ALOGD("\nPC->NFC: ");
    for (i=0;i<inLen;i++)
        ALOGE("%02x ", in[i]);
    ret= write(fp, in, inLen);
    if(ret<0)
        ALOGE("\nPhysical Write Error!");
    else
        ret = 0;

    ALOGD("\nNFC->PC: ");
    if(ret == 0)
    {
        ret = read(fp, out, 3);
        if ( ret < 0 )
            ALOGE("\npn544 read error");
        else
        {
            for (i=0;i<3;i++)
                ALOGD("%02x ", out[i]);
            rspLen = out[2];
            ret = read(fp, out+3, rspLen);//ret = read(fp, out+3, out[2]);
            if(ret < 0)
                ALOGE("\npn544 read error");
            else
                ret = 0;
        }
        for (i=0;i<rspLen;i++)//for (i=0;i<out[2];i++)
            ALOGD("%02x ", out[3+i]);
    }

    return ret;
}

int TraceabilityManager::getNFCFirmware(char* buffer, int bufferSize)
{
    unsigned char recvBuf[50];
    unsigned char CORE_RESET_CMD[] ={0x20, 0x00, 0x01, 0x01};
    unsigned char CORE_INIT_CMD[] ={0x20,0x01,0x00};
    int ret = 0, i, rspLen = 0;
    int fp;

    fp = open("/dev/pn544", O_RDWR);
    if( fp < 0 ) {ret = -1;
        ALOGE("failed to open /dev/pn544");
        return -1;
    }

    if(ret == 0)
    {
        ALOGD("\n==Hardware reseting==");
        ioctl(fp, PN544_SET_PWR, 1);
        ioctl(fp, PN544_SET_PWR, 0);
        ioctl(fp, PN544_SET_PWR, 1);
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_RESET_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_RESET_CMD,sizeof(CORE_RESET_CMD),recvBuf);
        if(ret == 0)
        {
            ALOGD("\nConfiguration Status: %02x",recvBuf[3]);
            ALOGD("\nNCI Version: %02x",recvBuf[4]);
        }
    }

    if(ret == 0)
    {
        ALOGD("\n==CORE_INIT_CMD==");
        memset(recvBuf,0,sizeof(recvBuf));
        ret = sendrecv(fp,CORE_INIT_CMD,sizeof(CORE_INIT_CMD),recvBuf);
        if(ret == 0 && recvBuf[3] == 0x00)
        {
            rspLen = recvBuf[2];
            ALOGD("\nFirmware Version Number: %02x,%02x,%02x",recvBuf[3+rspLen-3],recvBuf[3+rspLen-2],recvBuf[3+rspLen-1]);
            char *ptemp;
            asprintf(&ptemp, "%02x,%02x,%02x",recvBuf[3+rspLen-3],recvBuf[3+rspLen-2],recvBuf[3+rspLen-1]);
            strncpy(buffer, ptemp, bufferSize);
            buffer[bufferSize-1] = '\0';
            free(ptemp);
        }
    }

    ALOGD("getNFCFirmware end");
    close(fp);
    return ret;
}



void  TraceabilityManager::try_to_kill_rawdata(){
        system("toybox killall com.goodix.rawdata");
}

#define PERSIST_CALIBRATION ("/persist/sensorCalibration")
int TraceabilityManager::write_calibration()
{
    char buf[64];
    char w_buf[16+2];// start with '@' and end with '@', 4*sizeof(int)
    int len = 63;    // need 4 unsigned int number
    uint32_t proxDetect, proxRelease, pulse_count, calibrated_token;
    int iParamOffset = 32    // size of trace partition head = sizeof(rtrf_header)
                     + 4     // "TRAC"
                     + 15    // IMEI
                     + 6*2   // BT/WIFI address
                     + 4     // MMI_RECORD
                     + 381;  // offset of P Sensor Calibration Param in 512 traceability

    memset(buf, 0, sizeof(buf));
    FILE* fp_p = fopen(PERSIST_CALIBRATION, "r");
    FILE* fp_t = fopen(MMCBLKP, "wb");

    if (fp_p == NULL) {
        SLOGI("failed to open emmc partition \"%s\": %s\n", PERSIST_CALIBRATION,
                strerror(errno));
        if (fp_t) {
            fclose(fp_t);
        }
        return -1;
    }
    if (fp_t == NULL) {
        SLOGI("failed to open emmc partition \"%s\": %s\n", MMCBLKP,
                strerror(errno));
        return -1;
    }

    if (fread(buf, len, 1, fp_p) < len) {
        SLOGI("short read to trace (%s)\n", strerror(errno));
    //    return -1;
    }

    SLOGI("short read to %s\n", buf);
    if (sscanf(buf, "%u,%u,%u,%u", &proxDetect, &proxRelease, &pulse_count, &calibrated_token)!=4) {
        SLOGI("data format error in buf\n");
        return -1;
    }
    SLOGI("proxDetect=%u,proxRelease=%u,pulse_count=%u,calibrated_token=%u\n", proxDetect, proxRelease, pulse_count, calibrated_token);
    w_buf[0] = '@';
    memcpy( &(w_buf[1]), &proxDetect, sizeof(proxDetect) );
    memcpy( &(w_buf[5]), &proxRelease, sizeof(proxRelease) );
    memcpy( &(w_buf[9]), &pulse_count, sizeof(pulse_count) );
    memcpy( &(w_buf[13]), &calibrated_token, sizeof(calibrated_token) );
    w_buf[sizeof(w_buf)-1] = '@';

    fseek(fp_t, iParamOffset, SEEK_SET);
    if (fwrite(w_buf, sizeof(w_buf), 1, fp_t) != 1) {
        SLOGI("short write to %s (%s)\n", MMCBLKP, strerror(errno));
        return -1;
    }

    //SLOGI("write to %s %d \n", buf,k);
    if (fclose(fp_t) != 0) {
        SLOGI("error closing %s (%s)\n", PERSIST_CALIBRATION, strerror(errno));
        return -1;
    }
    if (fclose(fp_p) != 0) {
        SLOGI("error closing %s (%s)\n", MMCBLKP, strerror(errno));
        return -1;
    }
    return 0;

}

int TraceabilityManager::read_nv_item_and_send(SocketClient *cli, char *data, int size)
{
    int rc = -1;
    uint32_t item_id;
    char *pitem_id, *pend;
    unsigned char buffer[T_DATA_NV_SIZE];

    pitem_id = (char*)malloc(size+1);
    if(pitem_id){
        memcpy(pitem_id, data, size);
        pitem_id[size] = '\0';
        item_id = strtoul(pitem_id, &pend, 10);
        if(item_id>0) {
            ALOGD("read nv=%u.\n", item_id);
            int request_len = tct_nv_read_register(item_id, (char*)buffer, (uint32_t)T_DATA_NV_SIZE);

            if (request_len<=0 || request_len>T_DATA_NV_SIZE){
                ALOGE("read nv fail, request_len=%d.\n", request_len);
            } else {
                ALOGD("read nv success, request_len=%d.\n", request_len);
                for(int i=0; i<request_len; i++){
                    ALOGD("data[i] = %02u ", buffer[i]);
                }
                rc = cli->sendBinaryMsg(ResponseCode::TraceabilityReadResult, &buffer, request_len);
            }
        }
        free(pitem_id);
    }

    return rc;
}

int TraceabilityManager::switchChging(bool on)
{
    if(on) {
        system("echo 1 > /sys/class/power_supply/battery/charging_enabled");
    }
    else {
        system("echo 0 > /sys/class/power_supply/battery/charging_enabled");
    }
    return 0;
}

/*
int32_t TraceabilityManager::init_hdcp()
{
    int ret;
    int32_t initReturn;
    Parcel in1;
    Parcel out1;
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> b = sm->getService(String16("tcl.fty.sec"));
    if (b == NULL)
    {
        sleep(1);
        b = sm->getService(String16("tcl.fty.sec"));
        if (b == NULL)
        {
            ALOGE("%s:Can't find binder service \"tcl.fty.sec\"", __FUNCTION__);
            return -12;
        }
    }
    ret = b->transact(5, in1, &out1, 0);
    if(!ret)
    {
        initReturn = out1.readInt32();
        ALOGE("%s:transact exec return %d", __FUNCTION__, initReturn);
        return initReturn;
    }
    else
    {
        ALOGE("%s:transact return %d", __FUNCTION__, ret);
        return -13;
    }
}*/

int32_t TraceabilityManager::init_efuse()
{
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> b = sm->getService(String16("tcl.fty.sec"));
    if (b == NULL)
    {
       sleep(1);
       b = sm->getService(String16("tcl.fty.sec"));
       if (b == NULL)
       {
           ALOGE("%s:Can't find binder service \"tcl.fty.sec\"", __FUNCTION__);
           return -1;
       }
    }
    Parcel in1,out1;
    uint32 result;
    uint32 v1;
    uint32 v2;

    b->transact(9, in1, &out1, 0);
    if( b->transact(9, in1, &out1, 0) == 0 && out1.readInt32() == 0)
    {
      result=2;
      out1.read(&v1,  4);
      out1.read(&v2,  4);

      ALOGE("v1=0x%x", v1);
      ALOGE("v2=0x%x", v2);

      if( (v1 & 0x3f) == 0 /*&& v2 == 0xf*/)
      {
        ALOGE("Secure boot enabled");
        return 1;
      }
      else
      {
        ALOGE("Secure boot not enabled");
        return 0;
      }
    }
  else
  {
    ALOGE("getSecureStatus failed");
    return -1;
  }

  return 0;
}

int TraceabilityManager::read_ali_key_status()
{
    int iKeyStatus = 1;
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> b = sm->getService(String16("tcl.alipay"));
    if (b == NULL)
    {
       ALOGE("%s:Can't find binder service \"tcl.alipay\"", __FUNCTION__);
       return 1;
    }
    Parcel in1,out1;
    uint32 result;
    ALOGD("start to send cmd for alikey read");
    if( b->transact(7, in1, &out1, 0) == 0 ) {
        iKeyStatus = out1.readInt32();
        ALOGD("Ali key status read = %d", iKeyStatus);
    }
    else {
        ALOGE("Failed to send cmd to read ali key status");
    }

    return iKeyStatus;
}

int TraceabilityManager::read_wechat_key_status()
{
    int iKeyStatus = 1;
    char szBuffer[8];
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> b = sm->getService(String16("tcl.fty.sec"));
    if (b == NULL)
    {
       ALOGE("%s:Can't find binder service \"tcl.fty.sec\"", __FUNCTION__);
       return 1;
    }
    Parcel in1,out1;
    uint32 result;

    ALOGD("start to send cmd for wechat read");
    *((int *)szBuffer)=2;
    *(((int *)szBuffer)+1)=0;
    in1.writeInt32(8);
    in1.write(szBuffer,8);
    if( b->transact(2, in1, &out1, 0) == 0 ) {
        if(out1.readInt32()==0) {
            int len = out1.readInt32();
            if(len==8) {
                memset(szBuffer, 0, sizeof(szBuffer));
                out1.read(szBuffer, len);
                iKeyStatus = *(((int*)szBuffer)+1);
                ALOGD("Wechat key status read = %d", iKeyStatus);
            }
            else {
                ALOGE("cmd return error, len !=8");
            }
        }
        else {
            ALOGE("cmd return error");
        }
    }
    else {
        ALOGE("Failed to send cmd to read wechat key status");
    }

    return iKeyStatus;
}

void TraceabilityManager::read_ali_wechat_key_status(SocketClient *cli)
{
    char szBuffer[8] = {0};

    *((int *)szBuffer) = read_ali_key_status();
    *(((int *)szBuffer)+1) = read_wechat_key_status();
    cli->sendBinaryMsg(ResponseCode::TraceabilityReadResult, szBuffer, sizeof(szBuffer));
}

