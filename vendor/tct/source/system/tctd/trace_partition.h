/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/trace_partition.h              */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/09/21| He.Zhang       | FR-334604          | Adjust to fit new role     */
/* 12/09/21| Guobing.Miao   | FR-334604          | Create for nv backup       */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#ifndef __TRACE_PARTITION_H__
#define __TRACE_PARTITION_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct {
    uint32_t magic1;                 /* 4 bytes */
    uint32_t magic2;                 /* 4 bytes */
    uint32_t total_length_bytes;     /* 4 bytes */
    uint32_t checksum;               /* 4 bytes */
    uint32_t nb_missing_items;       /* 4 bytes */
    uint8_t status;                  /* 1 byte */
    unsigned char name[10];         /* 10 bytes */
    uint8_t pad[1];                  /* 1 byte */
    /* total header size             = 32 bytes */
} __attribute__((packed)) rtrf_header;

typedef struct {
    unsigned char        AKEY[16];
    unsigned char        AKEY_CHECKSUM[4];
    unsigned char        SPC[6];
    unsigned char        OTKSL[6];
    unsigned char        IMEI_2[15];                // stored imei 2
    unsigned char        SPARE[427];
    unsigned char        DEF_FLAG;        // definition flag 0x31
} __attribute__((packed)) ExtendZONE;

typedef struct {
    unsigned char name[4];
    unsigned char imei[15];
    unsigned char bd_addr[6];
    unsigned char wifi_addr[6];
    unsigned char test_status[4];

    unsigned char data[512];
    //New added to meet NPI requirement
    ExtendZONE  data_extend;  //475 length
    unsigned short checksum;
} __attribute__((packed)) tracability_region_struct_t;

#define RETROFIT_MAGIC1 0xFACB10C1L
#define RETROFIT_MAGIC2 0xFACB10C2L

#define RETROFIT_DONE   0xa8

#define MMCBLKP     "/dev/block/bootdevice/by-name/traceability"
#define MMCBLKR     "/dev/block/bootdevice/by-name/recovery"
#define MMCBLKN     "/dev/oemver"
#define SYSUSB     "/sys/class/android_usb/android0/iSerial"
#define CONFIG_DIR	"/tctpersist/data/param"
#define BTADDR   "/tctpersist/data/param/btaddr"
#define WIFIADDR "/tctpersist/data/param/macaddr"
#define FNONVER	"/firmware/verinfo/non.ver"
#define FSYSVER	"/system/system.ver"
#define FCUSVER	"/custpack/custpack.ver"
#define FBOOVER "/boot.ver"
#define MMCBLKX "/dev/block/bootdevice/by-name/simlock"

#define TCT_VERSION_LEN             12
#define TCT_RECO_OFFSET             48
#define TCT_SML_OFFSET              (4+8)//=>8 bytes of length and 4 bytes of header

#define BOOT_VER_PROPERTY      "ro.tct.boot.ver"
#define SYSTEM_VER_PROPERTY    "ro.tct.sys.ver"
#define MODEM_VER_PROPERTY     "ro.tct.non.ver"
#define RECOVERY_VER_PROPERTY  "ro.tct.reco.ver"
#define CUSTPACK_VER_PROPERTY  "ro.tct.cust.ver"
#define SIMLOCK_VER_PROPERTY    "ro.tct.sml.ver"


//properties in trace partition
#define TCT_BT_ADDR_OFFSET          51
#define TCT_BT_ADDR_LEN             6

#define TCT_WIFI_ADDR_OFFSET        57
#define TCT_WIFI_ADDR_LEN           6

#define TCT_SERIAL_NO_OFFSET        79
#define TCT_SERIAL_NO_LEN           15

#define TCT_HANDSET_REF_OFFSET      94
#define TCT_HANDSET_REF_LEN         12

#define TCT_PTM_OFFSET              106
#define TCT_PTM_LEN                 2

#define TCT_CU_REF_OFFSET           233
#define TCT_CU_REF_LEN              20

#define TCT_PTS_OFFSET              253
#define TCT_PTS_LEN                 3

#define TCT_SPEAKER_INFO_OFFSET     67+485
#define TCT_SPEAKER_INFO_LEN        4

#define TRACABILITY_TOTAL_SIZE \
        (sizeof(rtrf_header) + sizeof(tracability_region_struct_t))

#define TRACEABILITY_MMI_OFFSET            (sizeof(rtrf_header) + 4)
#define TRACEABILITY_DIAG_OFFSET           (sizeof(rtrf_header) + 4 + 31)
#define TRACEABILITY_MMITEST_SIZE          (1018)//31+512+475
#define TRACEABILITY_MMAP_SIZE             (1024 * 1024) //total size
#define TRACEABILITY_ROOTFLAG_OFFSET       (4096)

#define TRACEABILITY_INPRODUCTION_OFFSET   377

#ifdef __cplusplus
}
#endif

#endif /* __TRACE_PARTITION_H__ */
