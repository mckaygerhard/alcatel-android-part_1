/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:                                                                    */
/* E-Mail:                                                                    */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_shutdown.c             */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/07/10| yongliang.wang | FR-483802          | Send command to shutdown   */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/


#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_shutdown.h"

#include <sys/reboot.h>

#include <pthread.h>
#include <time.h>


//static int tct_service_initialized = 0;

static uint8 delay = 3;

static const diagpkt_user_table_entry_type tct_shutdown_tbl[] =
{
    {TCT_SHUTDOWN, TCT_SHUTDOWN, tct_shutdown_func},
};

PACK(void *) tct_shutdown_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    DIAG_LOGE("tct_unlockscreen_func \n");

    req_type_tct_shutdown_exec *req = (req_type_tct_shutdown_exec *) req_pkt;
    rsp_type_tct_shutdown_exec *rsp = NULL;
    int ret = -1;

    /*Allocate the same length as the request*/
    rsp = (rsp_type_tct_shutdown_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_SHUTDOWN,
            sizeof(rsp_type_tct_shutdown_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_tct_shutdown_exec));
    memcpy(rsp, req, sizeof(req_type_tct_shutdown_exec));
    rsp->result = 0;

    delay = req->delay;
    pthread_t tid;

    printf("before thread_create delay: %d\n",delay);

    if(pthread_create(&tid,NULL,(void*)&shutdown_thread,NULL)!=0)
    {
        printf("thread create failed\n");
    }

    return (rsp);
}

void shutdown_thread()
{
    sleep(delay);
    reboot(RB_POWER_OFF);
}

void tct_shutdown_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_FT, tct_shutdown_tbl);
}

