# Copyright (C) 2016 Tcl Corporation Limited
LOCAL_PATH := $(call my-dir)

common_src_files := \
    main.cpp \
    qmi_tct_api_v01.c \
    trace_nv_read.c \
    TctdListener.cpp \
    TctdCommand.cpp \
    TraceabilityManager.cpp \
    PrivilegeManager.cpp \
    TctFeedbackManager.cpp \
    QmiManager.cpp \

common_c_includes := \
    bionic \
    $(TARGET_OUT_HEADERS)/qmi-framework/inc \
    $(QC_PROP_ROOT)/data/dss_new/src/platform/inc \
    system/core/include/sysutils \
    system/core/libpackagelistparser/include \
    $(TARGET_OUT_HEADERS)/common/inc \
    $(TARGET_OUT_HEADERS)/diag/include

common_shared_libraries := \
    libsysutils \
    libcutils \
    liblog \
    libutils \
    libqmi_cci \
    libqmi_common_so \
    libqmiservices \
    libpowermanager \
    libbinder \
    libdiag \
    libpackagelistparser

tct_diag_src_files := \
    tct_diag_main.c \
    tct_trace_cmd.c \
    tct_unlockscreen.c \
    tct_handler.cpp \
    tct_shutdown.c \
    tct_debug_on.c \
    tct_charging_on.c \
    tct_audioloop_cmd.c \
    tct_nv_bk.c \
    nv_info_tab.c \
    tct_ft_cmd.c \
    tct_emmc_cmd.c

################################################################################
include $(CLEAR_VARS)

LOCAL_MODULE := tctd
#change the path for selinux, bug 847087
#LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR_EXECUTABLES)

LOCAL_MODULE_TAGS := optional debug

LOCAL_SRC_FILES := tct_rdata.cpp \
    aes.c \
    $(common_src_files)

LOCAL_C_INCLUDES := \
    $(common_c_includes) \
    $(QC_PROP_ROOT)/wfd-noship/mm/hdcp/HDCP_API \
    $(QC_PROP_ROOT)/securemsm/QSEEComAPI

LOCAL_CFLAGS := \
    -Wall \
    -Wno-implicit-function-declaration

LOCAL_SHARED_LIBRARIES := \
    $(common_shared_libraries) \
    libQSEEComAPI

ifeq ($(TCT_TARGET_MMITEST_PRODUCT),idol4s_cn)
LOCAL_SHARED_LIBRARIES += libtfa9890
LOCAL_CFLAGS += -DIDOL4S_TFA9897_FTM_CAL
endif

ifeq ($(TCT_TARGET_MMITEST_PRODUCT),notdef)
LOCAL_CFLAGS += -DTCT_TARGET_MMITEST_PRODUCT=\"london\"
endif

ifneq ($(TARGET_BUILD_TCT_DIAG_STANDALONE),true)
LOCAL_SRC_FILES += \
    $(tct_diag_src_files) \
    DiagManager.cpp
else
LOCAL_CFLAGS += -DTARGET_BUILD_TCT_DIAG_STANDALONE
endif # ifneq ($(TARGET_BUILD_TCT_DIAG_STANDALONE),true)

ifeq ($(TARGET_BUILD_VARIANT),user)
LOCAL_CFLAGS += -DTARGET_BUILD_VARIANT_USER
endif

LOCAL_ADDITIONAL_DEPENDENCIES := inprodoff

LOCAL_INIT_RC := tctd.rc

include $(BUILD_EXECUTABLE)

################################################################################
include $(CLEAR_VARS)
LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk
LOCAL_CLANG := true
LOCAL_SRC_FILES := tdc.cpp
LOCAL_MODULE := tdc
LOCAL_SHARED_LIBRARIES := libcutils libbase
include $(BUILD_EXECUTABLE)

################################################################################
ifeq ($(TARGET_BUILD_TCT_DIAG_STANDALONE),true)
include $(CLEAR_VARS)
LOCAL_CFLAGS += -DTARGET_BUILD_TCT_DIAG_STANDALONE
LOCAL_C_INCLUDES := \
    $(common_c_includes)
LOCAL_SRC_FILES := \
    $(tct_diag_src_files) \
    qmi_tct_api_v01.c
LOCAL_MODULE := tct_diag
LOCAL_SHARED_LIBRARIES := \
    $(common_shared_libraries)
#LOCAL_MODULE_PATH := $(TARGET_OUT_VENDOR_EXECUTABLES)
LOCAL_MODULE_TAGS := optional debug
include $(BUILD_EXECUTABLE)
endif # ifeq ($(TARGET_BUILD_TCT_DIAG_STANDALONE),true)

################################################################################
include $(CLEAR_VARS)
LOCAL_MODULE := inprodoff
LOCAL_SRC_FILES := scripts/$(LOCAL_MODULE)
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_MODULE_TAGS := optional
include $(BUILD_PREBUILT)
