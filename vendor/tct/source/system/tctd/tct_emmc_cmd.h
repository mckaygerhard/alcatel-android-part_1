/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun.Liu                                                         */
/* E-Mail:  Shishun.Liu@tcl.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tctd/tct_emmc_cmd.h            */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 09/03/14| Zhengyang.ma   | PR780904           | EMMC Capacity command      */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
#ifndef __TCT_EMMC_CMD_H__
#define __TCT_EMMC_CMD_H__

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 command;
} __attribute__((packed)) req_type_tct_emmc_cmd_exec;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 command;
    uint8 result;
} __attribute__((packed)) rsp_type_tct_emmc_cmd_exec;


PACK(void *) tct_emmc_cmd_func(PACK(void *)req_pkt, uint16 pkt_len);

#endif /* __TCT_EMMC_CMD_H__ */
