/* Copyright (C) 2016 Tcl Corporation Limited */
#define LOG_TAG "PrivilegeManager"

#include <fcntl.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>

#include <cutils/log.h>
#include <private/android_filesystem_config.h>
#include <packagelistparser/packagelistparser.h>

#include "ResponseCode.h"

#include "PrivilegeManager.h"

#include "TraceabilityManager.h"

#define MAX_PATH 512

#define DBG 1

struct cmd_uid_pkg {
    const char *cmd;
    const char *pkgname;
    const uid_t uid;
};

struct cmd_uid_pkg whitelist[] = {
    { "silent_install", "com.aspire.mm",                0         },
    { "root_check",     NULL,                           0         },
    { "trigger_modem_crash",     "com.tct.cmcc.salestracker",                           0         },
    { "efs_backup",     NULL,                           2000      },
    { "rewrite_splash",     NULL,                       1000       },
    { "setinproductionflag",     NULL,                       AID_SHELL       },
    { NULL, NULL, 0 }
};

PrivilegeManager *PrivilegeManager::sInstance = NULL;

PrivilegeManager *PrivilegeManager::Instance() {
    if (!sInstance)
        sInstance = new PrivilegeManager();
    return sInstance;
}

PrivilegeManager::PrivilegeManager() {
}

PrivilegeManager::~PrivilegeManager() {
}

int PrivilegeManager::start() {
    return 0;
}

int PrivilegeManager::stop() {
    return 0;
}

/*[FEATURE]-Add-BEGIN by LWS,2015/09/07, FR-1067379 ,add write splash by tctd*/
#define MAX_SPLASH_SIZE 4096
int PrivilegeManager::reWriteSplash(char **argv) {
    char * partition = "/dev/block/bootdevice/by-name/splash" ;
    char buf[MAX_SPLASH_SIZE] ;
    int rc = 0;
    int len ;
    FILE* fd = NULL;
    FILE *fd_splash = NULL;

    fd = fopen(argv[2], "rb");

    if(!fd){
        SLOGE("write splash:open fail");
        rc = -1 ;
        goto err1;
    }

    fd_splash = fopen(partition, "wb");
    if(!fd_splash){
        SLOGE("write splash:open splash fail");
        rc = -1 ;
        goto err2;
    }

    while(1) {
        memset(buf, 0, sizeof(buf));
        len = fread(buf,1,sizeof(buf),fd);
        if( len < 0 ) {
            rc = -1 ;
            break ;
        }
        if( 0 == len ) {
            break ;
        }
        if( len != fwrite(buf, 1, len, fd_splash) ) {
            rc = -1 ;
            break ;
        }
    }

    fclose(fd_splash);

err2:
    fclose(fd);

err1:
    return rc ;
}/*[FEATURE]-Add-END*/

static bool package_list_parser_cb(pkg_info *info, void *userdata) {
    pkg_info *appinfo = (pkg_info *) userdata;

    if (DBG) SLOGD("info->uid=[%d], info->name=[%s]", info->uid, info->name);

    bool rc = true;
    if (info->uid == appinfo->uid) {
        appinfo->name = strdup(info->name);
        // false to stop processing
        rc = false;
    }

    packagelist_free(info);
    return rc;
}

static bool is_permitted(pkg_info *appinfo, const char *cmd,
        struct cmd_uid_pkg *blacklist, struct cmd_uid_pkg *whitelist) {
    int i;

    if (DBG) SLOGD("cmd=[%s], blacklist=[%p], whitelist=[%p]", cmd, blacklist, whitelist);

    if (blacklist) {
        for (i = 0; blacklist[i].cmd; ++i) {
            if (!strcmp(cmd, blacklist[i].cmd)) {
                /* Match uid or package name */
                if ((appinfo->uid == blacklist[i].uid)
                        || (appinfo->name && !strcmp(appinfo->name, blacklist[i].pkgname))) {
                    SLOGI("deny by blacklist[%d]={%s, %d, %s}", i,
                            blacklist[i].cmd, blacklist[i].uid,
                            blacklist[i].pkgname);
                    return false;
                }
            }
        }
    }

    if (whitelist) {
        for (i = 0; whitelist[i].cmd; ++i) {
            if (!strcmp(cmd, whitelist[i].cmd)) {
                /* Match uid and package name (if not NULL) */
                if ((appinfo->uid == whitelist[i].uid)
                        && (!appinfo->name || !strcmp(appinfo->name, whitelist[i].pkgname))) {
                    if (DBG) SLOGD("allow by whitelist[%d]={%s, %d, %s}", i,
                            whitelist[i].cmd, whitelist[i].uid,
                            whitelist[i].pkgname);
                    return true;
                }
            }
        }
    }

    /* Return false by default */
    return false;
}

bool PrivilegeManager::isPermitted(SocketClient *cli, const char *subCommand) {
    bool rc;

    uid_t uid = cli->getUid();
    gid_t gid = cli->getGid();

    if (DBG) SLOGD("uid=[%d], gid=[%d], subCommand=[%s]", uid, gid, subCommand);

    /* Always return true if caller is root */
    if (uid == 0) {
        return true;
    }

    pkg_info appinfo;
    appinfo.uid = uid % AID_USER;
    appinfo.name = NULL;
    if (appinfo.uid >= AID_APP) {
        rc = packagelist_parse(package_list_parser_cb, &appinfo);
        if (!rc) {
            SLOGE("Could NOT parse package list");
            return false;
        }
    }

    return is_permitted(&appinfo, subCommand, NULL, whitelist);
}

int PrivilegeManager::silentInstall(int argc, char **argv) {
    int rc;
    int pid;
    int status;
    int i;

    pid = vfork();
    if (pid > 0) {
        /* Parent.  Wait for the child to return */
        waitpid(pid, &status, 0);
        if (WIFEXITED(status)) {
            rc = WEXITSTATUS(status);
        } else {
            rc = -1;
        }
    } else if (pid == 0) {
        /* Child. Temporarily switch effective UID to allow us to call pm */
        seteuid(AID_SHELL);
        char *exec_args[argc + 1];
        memset(exec_args, 0, sizeof(exec_args));
        for (i = 2; i < argc; ++i) {
            exec_args[i] = argv[i];
        }
        exec_args[0] = "/system/bin/pm";
        exec_args[1] = "install";
        exit(execvp(exec_args[0], exec_args));
    } else {
        /* Fork failed, return an error */
        rc = -1;
    }

    return rc;
}

#define ROOTFLAG "109f10eed3f021e3"
int PrivilegeManager::rootCheck(SocketClient *cli) {
    TraceabilityManager *tm = TraceabilityManager::Instance();
    char buf[256];
    char *apath[] = {
        "/sbin",
        "/vendor/bin",
        "/system/sbin",
        "/system/bin",
        "/system/xbin",
        NULL,
    };
    int i;
    bool checked = false;
    int fd;

    /* Read flag in traceability partition firstly. */
    tm->readData(TRACEABILITY_ROOTFLAG_OFFSET, buf, sizeof(buf));
    if (!memcmp(ROOTFLAG, buf, 16)) {
        SLOGI("rootCheck: flag found");
//        cli->sendBinaryMsg(ResponseCode::RootCheckResult, buf, sizeof(buf));
//        return 0;
    }

    /* Check if recovery modified. */
    do {
        fd = open("/dev/block/platform/msm_sdcc.1/by-name/recovery", O_RDONLY);
        if (fd < 0) {
            SLOGE("rootCheck: open recovery fail, errno (%d)", errno);
            break;
        }
        lseek(fd, 0x230, SEEK_SET); //boot_img_hdr.cmdline last 16 bytes
        if (read(fd, buf, 16) != 16) {
            SLOGE("rootCheck: read recovery fail, errno (%d)", errno);
            break;
        }
        if (memcmp(ROOTFLAG, buf, 16)) {
            checked = true;
            SLOGI("rootCheck: recovery modified");
            tm->writeData(TRACEABILITY_ROOTFLAG_OFFSET + 16, "recovery", 9);
            cli->sendMsg(ResponseCode::RootCheckResult, "recovery", false);
        }
    } while (0);
    if (fd > 0) {
        close(fd);
    }

    /* Check if boot modified. */
    do {
        fd = open("/dev/block/platform/msm_sdcc.1/by-name/boot", O_RDONLY);
        if (fd < 0) {
            SLOGE("rootCheck: open boot fail, errno (%d)", errno);
            break;
        }
        lseek(fd, 0x230, SEEK_SET); //boot_img_hdr.cmdline last 16 bytes
        if (read(fd, buf, 16) != 16) {
            SLOGE("rootCheck: read boot fail, errno (%d)", errno);
            break;
        }
        if (memcmp(ROOTFLAG, buf, 16)) {
            checked = true;
            SLOGI("rootCheck: boot modified");
            tm->writeData(TRACEABILITY_ROOTFLAG_OFFSET + 16 + 16, "boot", 5);
            cli->sendMsg(ResponseCode::RootCheckResult, "boot", false);
        }
    } while (0);
    if (fd > 0) {
        close(fd);
    }

    /* Check if 'su' under $PATH. */
    memset(buf, 0, sizeof(buf));
    for (i = 0; apath[i] != NULL; ++i) {
        strcpy(buf, apath[i]);
        strcat(buf, "/su");
        if (!access(buf, F_OK)) {
            checked = true;
            SLOGI("rootCheck: %s found", buf);
            tm->writeData(TRACEABILITY_ROOTFLAG_OFFSET + 16 + 32, buf, sizeof(buf));
            cli->sendMsg(ResponseCode::RootCheckResult, buf, false);
            break;
        }
    }

#ifdef TARGET_BUILD_VARIANT_USER
    /* CR-670846, only in user software we write flag. */
    if (checked) {
        SLOGD("rootCheck: write flag");
        tm->writeData(TRACEABILITY_ROOTFLAG_OFFSET, ROOTFLAG, 16);
    }
#endif /* TARGET_BUILD_VARIANT_USER */

    return 0;
}

int PrivilegeManager::triggerModemCrash() {
    return system("echo restart > /sys/kernel/debug/msm_subsys/modem");
}

extern "C" int efs_backup(const char *tempdir, const char *tarpath, int override);
int PrivilegeManager::efsBackup(const char *tempdir, const char *tarpath, int override) {
    return efs_backup(tempdir, tarpath, override);
}

int PrivilegeManager::setInProductionFlag(const char *flag) {
    TraceabilityManager *tm = TraceabilityManager::Instance();
    char data[4];
    if (!strcmp(flag, "true")) {
        data[0] = 0x87;
        data[1] = 0x96;
        data[2] = 0xA5;
        data[3] = 0xB4;
    } else {
        memset(data, 0, 4);
    }
    return tm->writeData(TRACEABILITY_INPRODUCTION_OFFSET, data, 4);
}
