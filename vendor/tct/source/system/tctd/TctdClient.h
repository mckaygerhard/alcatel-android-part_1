/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef _TCTD_CLIENT_H
#define _TCTD_CLIENT_H

#include "List.h"

#include <pthread.h>

class TctdClient {
    int             mSocket;
    pthread_mutex_t mWriteMutex;

public:
    TctdClient(int sock);
    virtual ~TctdClient() {}

    int sendMsg(const char *msg);
    int sendMsg(const char *msg, const char *data);
};

typedef android::sysutils::List<TctdClient *> TctdClientCollection;
#endif
