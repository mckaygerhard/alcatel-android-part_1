/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef _TRACEABILITYMANAGER_H
#define _TRACEABILITYMANAGER_H

#ifdef __cplusplus
#include <sysutils/SocketListener.h>

#include "trace_partition.h"

class TraceabilityManager {
private:
    static TraceabilityManager *sInstance;

private:
    SocketListener        *mBroadcaster;
    int                   fd;

public:
    virtual ~TraceabilityManager();

    int start();
    int stop();

    void setBroadcaster(SocketListener *sl) { mBroadcaster = sl; }
    SocketListener *getBroadcaster() { return mBroadcaster; }

    static TraceabilityManager *Instance();

    int traceability_read_data(SocketClient *cli);
    int traceability_write_data(SocketClient *cli, const char *buf, int size);
    int readData(int offset, char *buf, int size);
    int writeData(int offset, const char *buf, int size);
    int getNFCFirmware(char* buffer, int bufferSize);

    void bottom_mic_to_top_receiver_open();
    void bottom_mic_to_top_receiver_close();
    void top_mic_to_bottom_receiver_open();
    void top_mic_to_bottom_receiver_close();
    //void top_mic_to_top_receiver_open();
    //void top_mic_to_top_receiver_close();
    void headset_mic_to_top_receiver_start();
    void headset_mic_to_top_receiver_end();
    void headset_mic_to_bottom_speaker_start();
    void headset_mic_to_bottom_speaker_end();
    void bottom_mic_to_headset_open();
    void bottom_mic_to_headset_close();
    void top_mic_to_headset_open();
    void top_mic_to_headset_close();
//Add Begin by liping.gao(SH)-2015/10/29:tct diag,task:528833
    void bottom_mic_to_top_speaker_open();
    void bottom_mic_to_top_speaker_close();
    void top_mic_to_bottom_speaker_open();
    void top_mic_to_bottom_speaker_close();
    void headset_mic_to_top_speaker_start();
    void headset_mic_to_top_speaker_end();
    void headset_mic_to_bottom_receiver_start();
    void headset_mic_to_bottom_receiver_end();
//Add End by liping.gao (SH)-2015/10/29,task:528833
    void headset_mic_to_headset_speaker_open();
    void headset_mic_to_headset_speaker_close();
    void  try_to_kill_rawdata();
    int write_calibration();
    int read_nv_item_and_send(SocketClient *cli, char *data, int size);
    int switchChging(bool on);
    //int32_t init_hdcp();
    int32_t init_efuse();
    bool is_product(const char *buffer);
#if 0
    void get_tp_supplier_type();
    int switchChging(SocketClient *cli, bool on);
#endif

    void read_ali_wechat_key_status(SocketClient *cli);
private:
    TraceabilityManager();
    int read_wechat_key_status();
    int read_ali_key_status();
};

extern "C" {
#endif /* __cplusplus */
    int trace_readData(int offset, char *buf, int size);
    int trace_writeData(int offset, const char *buf, int size);
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
