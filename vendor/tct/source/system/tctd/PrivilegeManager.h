/* Copyright (C) 2016 Tcl Corporation Limited */
  #ifndef _PRIVILEGEMANAGER_H
#define _PRIVILEGEMANAGER_H

#ifdef __cplusplus
#include <sysutils/SocketListener.h>

class PrivilegeManager {
private:
    static PrivilegeManager *sInstance;

private:
    SocketListener        *mBroadcaster;

public:
    virtual ~PrivilegeManager();

    int start();
    int stop();

    void setBroadcaster(SocketListener *sl) { mBroadcaster = sl; }
    SocketListener *getBroadcaster() { return mBroadcaster; }

    static PrivilegeManager *Instance();

    bool isPermitted(SocketClient *cli, const char *subCommand);

    int silentInstall(int argc, char **argv);
    int rootCheck(SocketClient *cli);
    int triggerModemCrash();
    int efsBackup(const char *tempdir, const char *tarpath, int override);
/*[FEATURE]-Add-BEGIN by LWS,2015/09/07, FR-1067379 ,add write splash by tctd*/
    int reWriteSplash( char **argv);
/*[FEATURE]-Add-END*/
    int setInProductionFlag(const char *flag);
private:
    PrivilegeManager();
};
#endif /* __cplusplus */

#endif
