LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := $(TARGET_PRODUCT)
LOCAL_MODULE_TAGS := optional

ALL_QCNS :=  \
    $(TARGET_PRODUCT)_emea

QCNS_DIR := $(LOCAL_PATH)/$(TARGET_PRODUCT)_qcn
LISTS_DIR := $(LOCAL_PATH)/make_tar/lists
SCRIPTS_DIR := $(LOCAL_PATH)/make_tar/scripts
APPENDS_DIR := $(LOCAL_PATH)/make_tar/appends
TARBALL_OUT_DIR := $(ANDROID_PRODUCT_OUT)/tarball
AUTO_SCRIPTS_DIR := $(LOCAL_PATH)/auto_make_tar
tar_mergeplf_tool := "development/tcttools/mergeplf/mergeplf"

TARGET_NV_CONTROL_PLF := $(PRODUCT_OUT)/plf/isdm_nv_control.plf
TARGET_PROPERTY_PLF := $(PRODUCT_OUT)/plf/isdm_sys_properties.plf
ORIGIN_NV_CONTROL_PLF := $(TOP)/device/tct/common/perso/plf/nv_control/isdm_nv_control.plf
ORIGIN_PROPERTY_PLF := $(TOP)/device/tct/common/perso/plf/sys_properties/isdm_sys_properties.plf
$(TARGET_NV_CONTROL_PLF) : $(ORIGIN_NV_CONTROL_PLF)
	@echo "Merge isdm_nv_control.plf"    
	$(tar_mergeplf_tool) 0 $(shell dirname $<) $(PRODUCT_OUT)/plf $(TARGET_PRODUCT)

$(TARGET_PROPERTY_PLF) : $(ORIGIN_PROPERTY_PLF)
	@echo "Merge isdm_sys_properties.plf"    
	$(tar_mergeplf_tool) 0 $(shell dirname $<) $(PRODUCT_OUT)/plf $(TARGET_PRODUCT)
	
tar := $(PRODUCT_OUT)/tarball	

tar: $(TARGET_NV_CONTROL_PLF) $(TARGET_PROPERTY_PLF)
	@echo ""
	@echo "Auto Generate Tarball Now!"
	@echo ""
	rm -fr $(TARBALL_OUT_DIR)
	mkdir -p $(TARBALL_OUT_DIR)
ifeq ($(TARGET_BOARD_PLATFORM),msm8953)
	python $(AUTO_SCRIPTS_DIR)/maketar.py $(TARGET_PRODUCT) $(TARGET_BUILD_MMITEST) $(TARGET_NV_CONTROL_PLF) $(TOP)/amss_8953/MSM8953.LA.2.0/contents.xml $(TARBALL_OUT_DIR)
else
	python $(AUTO_SCRIPTS_DIR)/maketar.py $(TARGET_PRODUCT) $(TARGET_BUILD_MMITEST) $(TARGET_NV_CONTROL_PLF) $(TOP)/amss_8996/contents.xml $(TARBALL_OUT_DIR)
endif
	@echo "Generate a splash.img from the picture"
	$(hide) python $(JRD_TOOLS)/logo_gen/multi_logo_gen.py $(ANDROID_PRODUCT_OUT)/splash.img $(JRD_BUILD_PATH_DEVICE)/logo.png $(JRD_BUILD_PATH_DEVICE)/Dload_logo.png $(JRD_BUILD_PATH_DEVICE)/Low_power_logo.png $(JRD_BUILD_PATH_DEVICE)/Charger_boot_logo.png
	@echo "Build splash.img done."
	@echo ""
	@echo "Auto Generate Tarball End!"
	@echo ""

tarball: FORCE
	@echo ""
	@echo "Generate Tarball Now!"
	@echo ""
	rm -fr $(TARBALL_OUT_DIR)
	mkdir -p $(TARBALL_OUT_DIR)
	cp $(LISTS_DIR)/modify_list.txt $(TARBALL_OUT_DIR)/modify_list.txt
	awk -f $(SCRIPTS_DIR)/dial_to_modify.awk device/tct/$(TARGET_PRODUCT)/perso/isdm_nv_control.plf \
	    >> $(TARBALL_OUT_DIR)/modify_list.txt
	awk -f $(SCRIPTS_DIR)/A5algo_modify.awk device/tct/$(TARGET_PRODUCT)/perso/isdm_nv_control.plf \
	    >> $(TARBALL_OUT_DIR)/modify_list.txt
	awk -f $(SCRIPTS_DIR)/CSIM_support.awk device/tct/$(TARGET_PRODUCT)/perso/isdm_nv_control.plf \
	    >> $(TARBALL_OUT_DIR)/modify_list.txt
	#qcn to tar
	for i in $(ALL_QCNS); do \
	    $(SCRIPTS_DIR)/qcn_to_tar/qcn_to_tar.pl $(QCNS_DIR)/$${i}.qcn \
	        -o $(TARBALL_OUT_DIR)/$${i}.tar \
	        --nv-sizemap $(LISTS_DIR)/nv_size_map.txt \
	        --filter-list $(LISTS_DIR)/filter_list.txt \
	        --padding-list $(LISTS_DIR)/padding_list.txt \
	        --modify-list $(TARBALL_OUT_DIR)/modify_list.txt; \
	    for line in `find $(APPENDS_DIR)/$${i}/ -type f 2> /dev/null`; do \
	        efsline=`echo $${line} | sed "s:$(APPENDS_DIR)/$${i}/::"`; \
	        echo "Add "$${efsline}" file to "$${i}.tar; \
	        $(SCRIPTS_DIR)/add_file_to_tar.pl \
	            --tar $(TARBALL_OUT_DIR)/$${i}.tar \
	            --path $${line}:$${efsline}; \
	    done; \
	    echo "Tarball: "$(PRODUCT_OUT)/tarball/$${i}.tar; \
	    echo ""; \
	done
	@echo "now generate a splash.img from the picture"
	$(hide) python $(JRD_TOOLS)/logo_gen/multi_logo_gen.py $(ANDROID_PRODUCT_OUT)/splash.img $(JRD_BUILD_PATH_DEVICE)/logo.png $(JRD_BUILD_PATH_DEVICE)/Dload_logo.png $(JRD_BUILD_PATH_DEVICE)/Low_power_logo.png $(JRD_BUILD_PATH_DEVICE)/Charger_boot_logo.png
	@echo "build splash.img done."
	@echo ""
	@echo "Generate Tarball End!"
	@echo ""

#include $(BUILD_MULTI_PREBUILT)

