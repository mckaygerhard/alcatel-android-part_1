#!/usr/bin/env python

""" This File is used to get extra customization NV parameters defined in isdm_nv_control.plf file.

    FORMAT DEFINITION:
    ==================
    <VAR>
    <SIMPLE_VAR>
    <SDMID>A5_supported_for_perso</SDMID>
    <C_TYPE>AsciiString</C_TYPE>
    <C_NAME>CUSTOM_NVID_553</C_NAME>  ---> C_NAME node must be defined with format 'CUSTOM_NVID_###', '###' should be replaced with NV ID.
    <METATYPE>AsciiString,104</METATYPE>
    <IS_CUSTO>1</IS_CUSTO>
    <VALUE>"5"</VALUE>
    <FEATURE>CustomNV</FEATURE>   --->FEATURE suggest to be set as CustomNV, to group all NV SDM into one page in CLID system.
    <DESC>...</DESC>
    </SIMPLE_VAR>
    </VAR>

"""

import os
import sys
import xml.dom.minidom

def checkEccNumber(value):
    """ check ecc number list"""
    return value

def getExtraNvFromPLF(infile):
    """ This function read and parse the plf file, then return a struct like below:
        nvs = {
         'nv_id1' : {'type' : type, 'data' : value },
         'nv_id2' : {'type' : type, 'data' : value },
        }

    """
    if not infile:
        return
    if not os.path.exists(infile):
        print 'input file not exist.'
        return
    nv = {}
    #parse xml file by minidom
    dom = xml.dom.minidom.parse(infile)
    var = dom.getElementsByTagName("VAR")

    for v in var:
        # Get the SDM atrributes from plf
        sdmid = v.getElementsByTagName("SDMID")[0].firstChild.nodeValue
        #print sdmid
        node  = v.getElementsByTagName("METATYPE")
        meta  = node[0].firstChild.nodeValue if node and node[0].firstChild else None
        node  = v.getElementsByTagName("VALUE")
        value = node[0].firstChild.nodeValue if node and node[0].firstChild else None
        node  = v.getElementsByTagName("C_NAME")
        cname = node[0].firstChild.nodeValue if node and node[0].firstChild else None
        node  = v.getElementsByTagName("C_TYPE")
        ctype = node[0].firstChild.nodeValue if node and node[0].firstChild else None
        node  = v.getElementsByTagName("ARRAY")
        array = node[0].firstChild.nodeValue if node and node[0].firstChild else None
        node  = v.getElementsByTagName("NV_FORMAT")
        nvformat = node[0].firstChild.nodeValue if node and node[0].firstChild else None

        # If nv value is not set, just skip this SDM
        value = str(value).replace('"', '')
        if not value:
            continue

        # Check the SDM format
        if not meta or not cname or not ctype:
            print "METATYPE, C_NAME and C_TYPE node must not be None"
            continue

        if ctype != "AsciiString" and not meta.startswith('AsciiString'):
            print "SDM C_TYPE and META fields must be \'AsciiString\'", sdmid, cname
            raise ValueError

        if cname.startswith('CUSTOM_NVID_'):
            idx = cname.rindex('_')
            Id = cname[idx+1:].strip()
            if not Id.isdigit():
                print cname
                print "C_NAME node must be a string and ends with NV ID number, like \'CUSTOM_NVID_7777\'"
                raise ValueError

            if nv.has_key(Id):
                print "Duplicate SDM ID detected --> ", sdmid, infile
                raise ValueError

            nv[Id] = value

        elif cname.startswith('CUSTOM_EFS_'):
            idx = len('CUSTOM_EFS_')
            path= cname[idx:]
            if not path.startswith("/"):
                print "CNAME shall specify the whole path of the EFS file: ", cname
                raise ValueError

            data_format = 'array' if array == "ARRAY" else 'ascii'

            if nv.has_key(path):
                print "Duplicate SDM ID detected -->", sdmid, infile
                raise ValueError

            nv[path] = {'type' : 'efs', 'value': value, 'format': data_format, 'nv_format': nvformat}
        else:
            print "C_NAME node must be a string and starts with \'CUSTOM_NVID_\' or \'CUSTOM_EFS_\'", sdmid
            raise TypeError

    dom.unlink()
    return nv

def getSVN(infile):
    """ This function used to get the SVN number from isdm_sys_properties.plf and write to nv #5153 """
    if not infile:
        return
    if not os.path.exists(infile):
        print 'input file not exist.'
        return
    #parse xml file by minidom
    dom = xml.dom.minidom.parse(infile)
    var = dom.getElementsByTagName("VAR")
    value = None
    for v in var:
        # Get the SDM atrributes from plf
        sdmid = v.getElementsByTagName("SDMID")[0].firstChild.nodeValue
        if sdmid.find('ro_def_software_svn') > -1:
            node  = v.getElementsByTagName("VALUE")
            value = node[0].firstChild.nodeValue if node and node[0].firstChild else None
            break

    # SVN number should be format as '010 00x', with at least 5 characters
    if value:
        assert len(value) > 4
        print "Get SVN number string is: %s" % value
        value = str(value).lower().translate(None, '"abcdefghijklmnopqrstuvwxyz')[-2:]
        if value.isdigit():
            return value
    return

def getModel(infile):
    """ This function used to get the SVN number from isdm_sys_properties.plf and write to nv #5153 """
    if not infile:
        return
    if not os.path.exists(infile):
        print 'input file not exist.'
        return
    #parse xml file by minidom
    dom = xml.dom.minidom.parse(infile)
    var = dom.getElementsByTagName("VAR")
    value = None
    for v in var:
        # Get the SDM atrributes from plf
        sdmid = v.getElementsByTagName("SDMID")[0].firstChild.nodeValue
        if sdmid.find('PRODUCT_MODEL') > -1:
            node  = v.getElementsByTagName("VALUE")
            value = node[0].firstChild.nodeValue if node and node[0].firstChild else None
            break
    return value

# MODIFIED-BEGIN by Ding Yanghunag, 2016-08-19,BUG-2766016
def getRegion(infile):
    """ This function used to get the region info from isdm_sys_properties.plf and write to nv #3 """
    if not infile:
        return
    if not os.path.exists(infile):
        print 'input file not exist.'
        return
    #parse xml file by minidom
    dom = xml.dom.minidom.parse(infile)
    var = dom.getElementsByTagName("VAR")
    value = None
    for v in var:
        # Get the SDM atrributes from plf
        sdmid = v.getElementsByTagName("SDMID")[0].firstChild.nodeValue
        if sdmid.find('def_region_for_perso') > -1:
            node  = v.getElementsByTagName("VALUE")
            value = eval(node[0].firstChild.nodeValue) if node and node[0].firstChild else None
            break
    return value
    # MODIFIED-END by Ding Yanghunag,BUG-2766016

if __name__ == "__main__":
    print getExtraNvFromPLF(sys.argv[1])
