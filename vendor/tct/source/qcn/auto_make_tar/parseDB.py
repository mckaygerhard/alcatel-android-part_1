#!/usr/bin/python

import os
import sys
from struct import calcsize as struct_calcsize

# define the DB file name
workspace = os.path.dirname(os.path.realpath(__file__))
STRUCT_DB = os.path.join(workspace, "./database/Struct.txt")
FIELD_DB  = os.path.join(workspace, "./database/Field.txt")
NVITEM_DB = os.path.join(workspace, "./database/Diag.txt")
NV_SIZE_DB= os.path.join(workspace, "./database/nv_size_map.txt")

ENCODE_TYPE = { '0': 'dec',
                '1': 'hex'
              }

nvdb = {}
fielddb = {}
structdb = {}
nvsizedb = {}

            
def parseNvItemDB():
    """
    parse the diag.txt database, the content of each line is similar like below
    6^"65537"^"/nv/item_files/modem/lte/rrc/lte_r..."^201553^-1^0^-1^0^0
    6^ NV ID ^NV description or pathname for efsitem ^struct_id^0^-1^0^0
    """
    print "parseNvItemDB()"
    if not os.path.exists(NVITEM_DB):
        print "Can't find " + NVITEM_DB
        exit(-1)

    nvitems = {}

    f = file(NVITEM_DB, 'r')
    line = f.readline()
    while line: 
        arr = line.replace('"','').split('^')
        line = f.readline()    
        if not arr:
            continue
        nv = {}
        Id           = arr[1]
        nv['path']   = arr[2]
        nv['struct'] = arr[3]

        nvitems[Id] = nv
    return nvitems

def parseFieldDB():
    """
    parse the field database, the content of each line is similar like below
        305269  ^"Service"^8      ^   0     ^ 2      ^ 0     ^7638    ^ 0
        field_id^name     ^bitsize^indicator^datatype^enc_typ^desc_id ^ reserved
    """   
    print "parseFieldDB()" 
    if not os.path.exists(FIELD_DB):
        print "Can't find " + FIELD_DB
        exit(-1)

    nvfield = {}

    FIELD_TYPE = [ '?', # '0 - BOOL'
                   'b', # '1 - INT8'
                   'B', # '2 - UINT8'
                   'h', # '3 - INT16'
                   'H', # '4 - UINT16'
                   'i', # '5 - INT32'
                   'I', # '6 - UINT32'
                   'q', # '7 - INT64'
                   'Q', # '8 - UINT64'
                   's', # '9 - FIX LENGTH ANSI string'
                   's', # '10 - FIX LENGTH UNICODE string'
                   's', # '11 - Null terminated ANSI string'
                   's', # '12 - UNICODE null terminated ANSI string'
                   'f', # '13 - float'
                   'd', # '14 - double'
                 ]
    ENUM_FIELD_TYPE = { 8  : 'b',  # 8 bit enum
                        16 : 'h',  # 16 bit enum
                        32 : 'i',  # 32 bit enum
                      }

    NMOF_FIELD_TYPE = len(FIELD_TYPE)

    f = file(FIELD_DB, 'r')
    line = f.readline()
    while line:
        arr = line.strip().replace('"','').split('^')
        line = f.readline() 
        if not arr:
            continue
        #print arr
        try:
            (fid, name, bitsize, indicator , datatype, encode_type, desc_id, rev) = arr
        except:
            continue
        
        if indicator == '0' and int(datatype) < NMOF_FIELD_TYPE: # normal field data type
            field_format = FIELD_TYPE[int(datatype)]
            
            if field_format == 's':  # Fixed length ANSI strings
                #5166^"cgps_umts_pde_server_addr_url"^0^0^11^0^-1^0
                # bits can be 0 if datatype is null terminated string
                bits = int(bitsize)
                field_format =  bits * 's' if bits > 0 else '0s'

                    
                #print fid, field_format            
        # if indicator > 0, then datatype field = Enum data type ID from Enum.txt
        elif indicator == '1': # unsigned Enum data type
            bits = int(bitsize)
            if bits == 8 or bits == 16 or bits == 32:
                field_format = ENUM_FIELD_TYPE[bits].upper()                
            elif bits == 1 or bits == 7:
            # TODO: Qualcomm add special nv 70188, which bit size is 1 or 7, ignore it now.
                continue
            else:
                print "Unexpected bitsize for unsigned enum"
                exit(1)
        elif indicator == '2': # signed Enum data type
            bits = int(bitsize)
            if bits == 8 or bits == 16 or bits == 32:
                field_format = ENUM_FIELD_TYPE[bits]
            else:
                print "Unexpected bitsize for signed enum"
                exit(1)
        else:
            print "Unsupported data type"
            print arr
            exit(1)

        nvfield[fid] = field_format

    return nvfield

def parseStructDB():
    """
    parse the struct.txt database, the content of each line is similar like below
    201543  ^  0      ^  1        ^  79    ^"nv_mru3_type"^-1^0         ^ "21"
    structID^field idx^indicator1 ^field ID^Field name    ^-1^indicator2^numof field
    
    indicator1 = 0, next item is field id
               = 1, next item is sub-struct id
               = 2, next item is max size of above variable struct (field idx should be > 0)
                    else, if field idx is 0, it means no sub-struct.
    
    indicator2 = 0, next item is null
               = 1, next item is number of field (array size of field).
               = 2, next item is dependent sub-struct id, and if the dependent struct value is 0, this field won't appear, others, totally number of the dependent value fields will appear. 
               = 5, next item is condition vaiable, means this field only appear conditionally.
    """
    print "parseStructDB()"
    if not os.path.exists(STRUCT_DB):
        print "Can't find " + STRUCT_DB
        exit(-1)

    structdb = {}

    f = file(STRUCT_DB, 'r')
    line = f.readline()
    while line: 
        arr = line.replace('"','').split('^')
        line = f.readline()    
        if not arr:
            continue
        (struct_id, idx, indicator_1, value_1, field_nam, rev0, indicator_2, value_2) = arr
        struct = {}
        if indicator_1 == '0':
            struct['field'] = value_1
        elif indicator_1 == '1':
            struct['sub'] = value_1
        elif indicator_1 == '2':
            struct['max'] = value_1
        else:
            pass
            #print "other condition for indicator_1", arr
            #print arr
            # ignore case indicator_1 = 2
        if indicator_2 == '0':
            struct['num'] = 1
        elif indicator_2 == '1':
            struct['num'] = int(value_2)
        elif indicator_2 == '2':
            struct['num'] = 0 
            struct['dep'] = value_2.strip()
        elif indicator_2 == '5':
            struct['num'] = 1 
            struct['cond'] = value_2.strip()
            #print indicator_1, value_2.strip().split(' ')
        else:
            print "other condition for indicator_2", arr

        if not  structdb.has_key(  struct_id):
            structdb[struct_id] = []
            structdb[struct_id].append(struct)
        else:
            structdb[struct_id].append(struct)
            
    return structdb

def getFormat(struct):
    # TODO: handle the special case for 'cond' and 'dep' field in struct db        
    global fielddb, structdb
    format = ''
    for field in struct:
        #print field
        if field.has_key('field') and fielddb.has_key(field['field']): 
            f = fielddb[field['field']]
            if field['num'] > 1:
                f = f * field['num']
            format += f
        elif field.has_key('sub'):
            f = getFormat(structdb[field['sub']])
            format += f * int(field['num'])
        else:
            print "er?"
    return format
            
def getCondFormat(struct, d, cond_field):
    global fielddb, structdb
    format = ''
    idx = 0
    value = '0'

    for field in struct:        
        if field.has_key('field') and fielddb.has_key(field['field']): 
            f = fielddb[field['field']]
            if field['num'] > 1:
                f = f * field['num']
            format += f
            if field['field'] == cond_field:
                value = d[idx]
        idx += 1
        if field.has_key('cond'):
            cond_str = field['cond'].split(' ')
            if cond_str[1] == '=':
                cond_str[1] = '=='
            cond_str[0] = str(value)
            if not eval(' '.join(cond_str)):
                # not meet the condition, skip this 
                continue
            print field, cond_str
            assert(field.has_key('sub'))
            f = getFormat(structdb[field['sub']])
            format += f * int(field['num'])
        elif field.has_key('max') and idx > 1:
            max_len = int(field['max']) >> 3
            if struct_calcsize(format) > len(d):
                format = format[:len(d)]
            else:
                format = format + (len(d) - struct_calcsize(format)) * 'B'         

    return format
	    

          
def getStructFormat(nv):
    global nvdb, fielddb, structdb
    if not nvdb:
        nvdb = parseNvItemDB()
        fielddb = parseFieldDB()
        structdb = parseStructDB()
    
    if not nvdb.has_key(nv):
        print "No such nv item", nv
        return None
    #print nvdb[nv]['path']
    s = nvdb[nv]['struct']
    if not structdb.has_key(s):
        print "No such struct definition", nv
        return None
    #print structdb[s]
    return getFormat(structdb[s])

def getConditionStructFormat(nv, d, cond):
    if not d or not cond:
        return None
    global nvdb, fielddb, structdb
    if not nvdb:
        nvdb = parseNvItemDB()
        fielddb = parseFieldDB()
        structdb = parseStructDB()
    if not nvdb.has_key(nv):
        print "No such nv item", nv
        return None
    s = nvdb[nv]['struct']
    if not structdb.has_key(s):
        print "No such struct definition", nv
        return None
    
    t = structdb[s][0] 
    if t.has_key('sub') and t['sub'] != cond :
        # for those nv return by checkFirstFieldOfCondNV()
        return getCondFormat(structdb[t['sub']], d, cond) 
    else:
        return getCondFormat(structdb[s], d, cond) 


def isNvInDatabase(nv):
    global nvdb

    return True if nvdb.has_key(nv) else False

def isCondition(struct):
    for field in struct:
        if field.has_key('cond'):
            arr = field['cond'].split(' ')
	    if not arr or len(arr) != 3:
                return None
            return arr[0]
        elif field.has_key('sub'):
            return isCondition(structdb[field['sub']])
    return None

def isNvHasConditionValue(nv):
    global nvdb

    if not nvdb.has_key(nv):
        return None

    s = nvdb[nv]['struct']
    if not structdb.has_key(s):
        return None

    return isCondition(structdb[s])
    
def prepareDB():
    global nvdb, fielddb, structdb, nvsizedb
    if not nvdb:
        nvdb = parseNvItemDB()
        fielddb = parseFieldDB()
        structdb = parseStructDB()
    if not nvsizedb:    
        print "parse nv size db"
        with open(NV_SIZE_DB, 'r') as f: 
            for line in f.readlines():
                (i, size, arr, per) = line.split(':')
                nvsizedb[i] = int(size)
    return

def getEfsPath(Id):
    global nvdb
    if not nvdb:
        nvdb = parseNvItemDB()
    if not nvdb.has_key(Id):
        return None
    path = nvdb[Id]['path']
    if not path.startswith('/nv/item_files'):
        return None
    return path

def NVhasNamField(Id):
    """ Some NV items has the first item named 'nam', which will impact the nv item 
    file name in EFS, this function will check the first field and return True if it's 'nam' field
    """
    pad_list = ['10','1206','176','177','1958','20','209','21','212','255','256','259','262','263', '285',\
                '264','265','266','2954','32','33','441','442','465','57','847','850','945','946']
    return True if Id in pad_list else False

    """if Id == '2954':
        return True
    global nvdb, fielddb, structdb

    if not nvdb.has_key(Id):
        print "No such nv item", nv
        return False
    #print nvdb[nv]['path']
    s = nvdb[Id]['struct']
    if not structdb.has_key(s):
        print "No such struct definition", Id
        return False

    firstField = structdb[s][0]

    print 'check nam: ', firstField
    # nam field has field ID == 855
    if firstField.has_key('field') and firstField['field'] == '855':
        return True
    else:
        return False
    """

def checkFirstFieldOfCondNV():
    """
    This function used to check those nv with condition value, but the first field was not the condition field.
    71523, 71525, 71544, 65627, 65628, 945
    """
    for s in nvdb.keys():
        if int(nvdb[s]['struct']) > 0 :
            cond = isCondition(structdb[nvdb[s]['struct']])
            if cond:
                t = structdb[nvdb[s]['struct']][0]
                if t.has_key('field') and t['field'] == cond:
                    pass
                else:
                    print s

def getNvMapSize(Id):
    """ This function used to get the NV size map from nv_size_map.txt, which used to convert qcn to tarball file before.
    The file defined the total size of NV for those ID < 65535.
    """
    global nvsizedb
    if not nvsizedb:
        print "parse nv size map"
        with open(NV_SIZE_DB, 'r') as f: 
            for line in f.readlines():
                (i, size, arr, per) = line.split(':')
                nvsizedb[i] = int(size)
    if not nvsizedb.has_key(Id):
        return 
    return nvsizedb[Id]


if __name__ == '__main__':

    if len(sys.argv) < 2:
        nv = '156'
    else:
        nv = sys.argv[1]

    prepareDB()
    print getStructFormat(nv)
    cond = isNvHasConditionValue(nv)
    if cond:
        d = [1,196,19,192,39,9,0,192,39,9,0,184,1]
        f = getConditionStructFormat(nv, d, cond)
        print f, len(f), 
        import struct
        print 'calcsize: ', struct.calcsize(f)
    else:
        print getStructFormat(nv)


    


