#============================================================================
#
# CBSP Builders build rules
#
# GENERAL DESCRIPTION
#    Contains builder(s) to create MBN file
#
# Copyright 2011 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#----------------------------------------------------------------------------
#
#  $Header: //components/rel/core.mpss/3.4.c3/bsp/build/scripts/mbn_builder.py#1 $
#  $DateTime: 2015/09/28 23:04:45 $
#  $Author: csdssvc $
#  $Change: 9120648 $
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who       what, where, why
# --------   ---       ------------------------------------------------------
# 10/03/11   dxiang    Clean up 
#============================================================================
import os
import subprocess
import string
import re
import zipfile
import glob
import shutil
import mbn_tools

#----------------------------------------------------------------------------
# Stage1 MBN builder
#----------------------------------------------------------------------------
def stage1_mbn_builder(target, source, env):
   source_path, source_file = os.path.split(str(source))
   target_path, target_file = os.path.split(str(target))
   source_base = os.path.splitext(str(source))[0]
   target_base = os.path.splitext(str(target))[0]
   source_full = str(source)
   target_full = str(target)
   
   if 'USES_SECBOOT' in env:
      if env['DISABLE_RELOCATABLE'] is True:
         target_bin_base = env.subst("${MBN_ROOT}/non-reloc/")
      else:
         target_bin_base = env.subst("${MBN_ROOT}/")

      target_bin_dir = target_bin_dir_name(env)

   # SECBOOT CASE
   if 'USES_SECBOOT' in env:
      target_bin_nonsec = target_bin_base + target_bin_dir + "/" + \
         str(os.path.split(target_base)[1]) + ".mbn"

      target_nonsec = target_base + ".mbn"
      image_header_secflag = "secure"

      # Find zip file in source_path
      target_zip_cert_dir = target_bin_base + target_bin_dir + "/cert"
      if not os.path.exists(target_zip_cert_dir):
         os.makedirs(target_zip_cert_dir)
         print "Created " + target_zip_cert_dir + " Folder\n"
      target_zip_str = target_zip_cert_dir + '/*.zip'

      source_zip = ""
      zipfiles = glob.glob(target_zip_str)

      if (len(zipfiles) > 0):
         source_zip = zipfiles[0]

   # NON_SECBOOT CASE
   else:
      image_header_secflag = "nonsecure"

   # Default initializations
   #qdst_xml = "/QDST_"+env.subst("$IMAGE_TYPE")+".xml"
   #image_header_cmd = env.subst("$IMAGE_TYPE")  
   #target_full_enc = target_base + "_enc.mbn"

   #-------------------------------------------------------------------------
   # Binary MBN generation
   #-------------------------------------------------------------------------
   if env['MBN_TYPE'] == 'bin':
      pass
                
   #-------------------------------------------------------------------------
   # ELF MBN generation 
   #-------------------------------------------------------------------------
   elif env['MBN_TYPE'] == 'elf':

      target_hash = target_base + ".hash"
      target_hash_hd = target_base + "_hash.hd"
      target_cert_chain = target_base + "_hash_cert_chain.mbn"
      target_hash_full = target_base + "_hash_sec.mbn"
      target_phdr_elf = target_base + "_phdr.pbn"
      target_add_hash_elf = target_phdr_elf
      target_pre_encrypt_elf = target_base + "_pre_encrypt.pbn"
      target_encrypt_xml = target_base + "_enc_md.xml"
 
      # For relocatable modem, get specific parameters for hash size and address
      if ('USES_RELOCATABLE_MODEM' in env) and (env['DISABLE_RELOCATABLE'] is True):  
          (hash_seg_size, hash_seg_offset, last_phys_addr) = env.PbnGetDemandLoadingData()
          hash_seg_size  = eval(hash_seg_size)
          last_phys_addr = eval(last_phys_addr)
      else: 
          hash_seg_size = None
          last_phys_addr = None

      if ('USES_ENCRYPT_MBN' in env) and (env['ENABLE_ENCRYPT'] is True):
         encrypted_image = True 
      else:
         encrypted_image = False

      rv = mbn_tools.pboot_gen_elf(env, source_full, target_hash, 
                          elf_out_file_name = target_phdr_elf,
                          secure_type = image_header_secflag,
                          hash_seg_max_size = hash_seg_size,
                          last_phys_addr = last_phys_addr,        
                          append_xml_hdr = encrypted_image)   
      if not rv:
         rv = mbn_tools.image_header(env, env['GEN_DICT'], target_hash, target_hash_hd, 
                              image_header_secflag, elf_file_name = source_full)

      if 'USES_SECBOOT' in env:
         target_nonsec = target_base + ".mbn"
       
         if not rv:
            files_to_cat_in_order = [target_hash_hd, target_hash]

            target_bin_nonsec = target_bin_base + target_bin_dir + "/" + \
               str(os.path.split(target_nonsec)[1])
            concat_files (target_nonsec, files_to_cat_in_order)
            env.Depends(target_bin_nonsec, target_nonsec)
            shutil.copy(target_nonsec, target_bin_nonsec)
            if source_zip == "":
               print target_bin_nonsec + " image has not been " + \
                  "signed. Generate certificates and signature "+ \
                  "from CSMS website and place it at " + target_bin_dir_name(env)+\
                  "\\cert folder \n"

            if 'USES_SECBOOT_QDST' in env:
               ##### Run QDST script
               qdst_root = env.subst(QDST_ROOT)
               cmds = env.subst("python " + qdst_root + "/QDST.py image=" + target_bin_nonsec + " xml=" + qdst_root + qdst_xml)

               data, err, rv = env.ExecCmds(cmds, target=target_file)
               ##### End Run QDST script

            target_full = target_nonsec

      else:
         target_nonsec = target_base + "_combined_hash.mbn"
         target_nonsec_xml = target_base + "_combined_hash_xml.mbn"

         if not rv:
            files_to_cat_in_order = [target_hash_hd, target_hash]
            concat_files (target_nonsec, files_to_cat_in_order)

            # If modem encryption is ON, generate XML header and append to hash segment
            if encrypted_image is True: 
               mbn_tools.generate_meta_data(env, target_encrypt_xml)
               files_to_cat_in_order.append(target_encrypt_xml) 
               concat_files (target_nonsec_xml, files_to_cat_in_order)

            # Add the hash segment into the ELF 
            mbn_tools.pboot_add_hash(env, target_add_hash_elf, target_nonsec, target_full)

            # If Encryption Feature is ON, add the hash segment with XML information into the ELF and encrypt 
            if encrypted_image is True:
               mbn_tools.pboot_add_hash(env, target_add_hash_elf, target_nonsec_xml, target_pre_encrypt_elf)
               mbn_tools.encrypt_elf_segments(env, target_pre_encrypt_elf, target_full_enc)
 
   return None
#END OF STAGE1_MBN_BUILDER

#----------------------------------------------------------------------------
# Target_bin_dir
# This will return the temporary directory name used for the image signing
#----------------------------------------------------------------------------
def target_bin_dir_name(env):
    target_bin_dir = env.subst("$IMAGE_TYPE") 
    return target_bin_dir

def concat_files (target, sources):
   if type(sources) is not list:
      sources = [sources]

   target_file = open(target,'wb')

   for fname in sources:
      file = open(fname,'rb')
      while True:
         bin_data = file.read(65536)
         if not bin_data:
            break
         target_file.write(bin_data)
      file.close()
   target_file.close()

if __name__=="__main__":
   ''' python mbn_builder.py source target '''
   import sys
   env = {}
   env['MBN_TYPE'] = 'elf'
   env['IMAGE_TYPE'] = sys.argv[3]
   env['DISABLE_RELOCATABLE'] = 'false'
   env['GEN_DICT'] = {'IMAGE_KEY_FLASH_AUTO_DETECT_MIN_PAGE': 2048,
                      'IMAGE_KEY_FLASH_TYPE': 'nand',
                      'IMAGE_KEY_MBN_TYPE': 'elf', 
                      'IMAGE_KEY_IMAGE_ID': 0,
                      'IMAGE_KEY_OEM_ROOT_CERT_SEL': 1,
                      'IMAGE_KEY_MAX_SIZE_OF_VERIFY_BUFFER': 8192,
                      'IMAGE_KEY_IMAGE_SOURCE': 0,
                      'IMAGE_KEY_FLASH_AUTO_DETECT_MAX_PAGE': 8192,
                      'IMAGE_KEY_BOOT_SMALL_PREAMBLE': 1,
                      'IMAGE_KEY_IMAGE_DEST': 0,
                      'IMAGE_KEY_ID_MATCH_STR': 'MCFG_SW_IMG',
                      'IMAGE_KEY_OEM_NUM_ROOT_CERTS': 1
                      }
   stage1_mbn_builder(sys.argv[1], sys.argv[2], env)
