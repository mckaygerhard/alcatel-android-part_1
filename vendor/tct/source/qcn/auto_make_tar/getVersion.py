#!/usr/bin/env python

""" This File is used to get product parameter version."""

import os
import sys
import xml.dom.minidom

def getProdParaVersion(infile = None):
    """ This function parse the input xml file and return the parameter version of all products.
        The return struct is something like below:
        product_list = {
            "eos_cucc"  : { "EOS_CUCC"   : "V01.60" },
            "eos"       : { "EOS_EMEA"   : "V01.60" },
            "miata"     : { "Miata_EMEA" : "V01.70",
                            "Miata_LATAM": "V01.70",
                          },
            "Rio4g"     : { "RIO4G_EMEA" : "V01.10" },
            "rio4g_tf"  : { "RIO4G_TF"   : "V01.10" },
        }
    """
    if not infile:
        return None

    if not os.path.exists(infile):
        print 'input file not exist.'
        return

    dom = xml.dom.minidom.parse(infile)
    products = dom.getElementsByTagName("product")
    if not products:
        return None

    pv = {}
    for p in products:
        prod = p.getAttribute('name')
        if not prod:
            continue
        paras = p.getElementsByTagName("parameter")
        if not paras:
            continue
        vers = {}
        for pa in paras:
            mbn = {}
            para = pa.getAttribute('name')
            version  = pa.getAttribute('version')
            output = pa.getAttribute('output')
            server = pa.getAttribute('server')
            use_mbn= pa.getAttribute('use_mcfg_mbn')
            if use_mbn == "true":
                generate_mbn = True
            else:
                generate_mbn = False
            if not para or not version or not version.startswith('V') or not server:
                raise ValueError
            if use_mbn == "true" and pa.hasChildNodes() :
                mbns = pa.getElementsByTagName("mbn")
                for node in mbns:
                    mbn[node.getAttribute('name')]=dict(typ=node.getAttribute('type'), path=node.getAttribute('path'))

            vers[para] = dict(version=version, output=output, server=server, generate_mbn=generate_mbn)
        pv[prod] = vers

    dom.unlink()
    return pv

if __name__ == "__main__":
    print len(sys.argv), sys.argv
    print getProdParaVersion("product_parameter_vesrion.xml")
