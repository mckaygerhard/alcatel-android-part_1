#include <pthread.h>
#include <binder/IServiceManager.h>
#include <binder/IBinder.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IPCThreadState.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include "common.h"

using namespace android;

#define ALISVR "tcl.alipay"
#define FTYSVR "tcl.fty.sec"

const int tlen=8192;
char buf[tlen];

int testSS()
{
  sp<IServiceManager> sm = defaultServiceManager();
  sp<IBinder> b = sm->getService(String16(FTYSVR));
  Parcel in1,out1;
  b->transact(9, in1, &out1, 0);
  if(out1.readInt32() == 0)
  {
    LOGD("getSecureStatus ok");
    out1.read(buf, 8);
    LOGD("Secure status value 0x%x, 0x%x", *((uint32_t *)buf), *((uint32_t *)(buf+4)));
  }
  else
    LOGD("getSecureStatus failed");
  return 0;
}

int main(int argc, char **argv)
{
  testSS();

  return 0;
}
