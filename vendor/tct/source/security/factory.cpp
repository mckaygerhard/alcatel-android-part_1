#include <binder/IServiceManager.h>
#include <binder/IBinder.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IPCThreadState.h>
#include <stdint.h>
#include "common.h"
#include "factory.h"
#include "inc/Dx_Hdcp_Provisioning.h"
#include "inc/DX_Hdcp_Transmitter.h"
/* MODIFIED-BEGIN by Xiaoyun.Wei, 2016-11-11,BUG-3335803*/
#include "qseeta.h"

#define FTYSVR "tcl.fty.sec"
/* MODIFIED-BEGIN by Xiaoyun.Wei, 2016-11-14,BUG-3335803*/
#ifdef TCLAPP_DBG
#define FTYTA "/etc/firmware/tclapp.mbn"
#else
#define FTYTA "/firmware/image/tclapp.mbn"
#endif
/* MODIFIED-END by Xiaoyun.Wei,BUG-3335803*/
/* MODIFIED-END by Xiaoyun.Wei,BUG-3335803*/

static const int blen=8192;

void dxcb(EDxHdcpEventType type, void *dummy1, void* dummy2)
{
}

#define retchk(fmt, ret, err) \
{\
  if(ret != 0) \
  { \
    LOGE(fmt, ret); \
    reply->writeInt32(err); \
    break; \
  } \
}

#define ptrchk(fmt, ptr, err) \
{\
  if(ptr == NULL) \
  { \
    LOGE(fmt); \
    reply->writeInt32(err); \
    break; \
  } \
}

FactorySvr::FactorySvr()
{
  mydescriptor = String16(FTYSVR);
}

FactorySvr::~FactorySvr()
{

}

const String16& FactorySvr::getInterfaceDescriptor() const
{
  ALOGD("this is enter ==========getInterfaceDescriptor");
  return mydescriptor;
}

void FactorySvr::instantiate()
{
  LOGD("Add service %s", FTYSVR);
  defaultServiceManager()->addService(String16(FTYSVR), new FactorySvr());
}

status_t FactorySvr::onTransact( uint32_t code, const Parcel& data, Parcel* reply, uint32_t flags)
{
  /* MODIFIED-BEGIN by Xiaoyun.Wei, 2016-11-11,BUG-3335803*/
  ALOGD("enter FtySvr onTransact and the code is %d",code);

  int len;
  int ret;
  sp<IBinder> b;
  unsigned char *buf;
  qseecom_cmd * qcc=NULL;
  /* MODIFIED-END by Xiaoyun.Wei,BUG-3335803*/

  switch (code)
  {
  case 1:
  {
    len=data.readInt32();
    buf = (unsigned char *) malloc(len+16);
    if(!buf)
      reply->writeInt32(-13);
    else
    {
      data.read(buf, len);
      data.read(buf+len, 16);
#ifdef HDCP_ENABLE
      ret = DxHdcp_Provisioning_Init();
      if (ret != 0)
      {
        ALOGE("DxHdcp_Provisioning_Init failed\n");
        reply->writeInt32(-5);
        return 0;
      }
      ret = DxHDCP_ProvisionWithCEK(buf,len,buf+len);
      if (ret != 0)
      {
        ALOGE("DxHDCP_ProvisionWithCEK failed\n");
        reply->writeInt32(-6);
        return 0;
      }

      ret = DxHDCP_StoreCEK(buf+len);
      if (ret != 0)
      {
        ALOGE("DxHDCP_StoreCEK failed\n");
        reply->writeInt32(-7);
        return 0;
      }

      ret = DxHDCP_ProvisionValidate(buf,len,buf+len);
      if (ret != 0)
      {
        ALOGE("DxHDCP_ProvisionValidate failed\n");
        reply->writeInt32(-8);
        return 0;
      }

      ret = DxHdcp_Provisioining_Terminate();
      if (ret != 0)
      {
        ALOGE("DxHdcp_Provisioining_Terminate failed\n");
        reply->writeInt32(-9);
        return 0;
      }
      sync();
      reply->writeInt32(0);
#else
      reply->writeInt32(-2);
#endif
    }
    break;
  }
  case 2:
  {
    len = data.readInt32();
    break;
  }
  case 3:
  {
    len = data.readInt32();
    break;
  }
  case 4:
  {
    len = data.readInt32();
    break;
  }
case 5:
  {
#ifdef HDCP_ENABLE
     reply->writeInt32(DX_HDCP_Tsmt_Init(dxcb));
     DX_HDCP_Tsmt_Close();
#endif
     break;
  }
/* MODIFIED-BEGIN by Xiaoyun.Wei, 2016-11-11,BUG-3335803*/
case 9:
  {
    TCLSecQSEETA tee;
    if(tee.load(FTYTA, blen) == 0)
    {
      qcc= (qseecom_cmd *) tee.getBuf();
      qcc->cmd=6;
      qcc->len=sizeof(qseecom_cmd);
      if(tee.sendCmd(qcc) == 0)
      {
        if(qcc->len == 12)
        {
          LOGD("get secure status ok");
          reply->writeInt32(0);
          reply->write(qcc+1, 12);
        }
        else
        {
          LOGE("get secure status failed");
          reply->writeInt32(-11);
        }
      }
      else
      {
        LOGE("qta send cmd error");
        reply->writeInt32(-10);
      }
    }
    else
    {
      LOGE("load tclapp ta error");
      reply->writeInt32(-9);
    }
    break;
  }
  /* MODIFIED-END by Xiaoyun.Wei,BUG-3335803*/

  default:
    return BBinder::onTransact(code, data, reply, flags);
    break;
  }
  return 0;
}






