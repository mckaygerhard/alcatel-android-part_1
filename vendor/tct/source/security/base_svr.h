/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef TCLSEC_BASE_SVR_H
#define TCLSEC_BASE_SVR_H

#include <binder/IServiceManager.h>
#include <binder/IBinder.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IPCThreadState.h>

#include "mutex.h"
#include "tee.h"
#include "common.h"

using namespace android;


extern const char *svrName[];
typedef enum
{
  ALISVR=0,
}SvrIndex;

template<class T, SvrIndex index>
class TCLSecBaseSvr : public BBinder
{
protected:
  TCLSecBaseSvr():mutex(TCLSecMutex()), tee(NULL){}
  virtual ~TCLSecBaseSvr(){}
public:
  static void instantiate()
  {
    LOGD("Add service %s", svrName[index]);
    defaultServiceManager()->addService(String16(svrName[index]), new T());
  }
  virtual const String16& getInterfaceDescriptor() const{return descriptor;}

protected:
  TCLSecMutex mutex;
  String16 descriptor;
  TCLSecTee *tee;
};




#endif
