#include <binder/ProcessState.h>
#include <binder/IPCThreadState.h>

#include "common.h"
#include "factory.h"
#include "alisvr.h" // MODIFIED by Xiaoyun.Wei, 2016-11-10,BUG-3335803



int main()
{
  sp<ProcessState> proc(ProcessState::self());
  sp<IServiceManager> sm = defaultServiceManager();
  ALOGI("ServiceManager: %p", sm.get());

  FactorySvr::instantiate();

  AlipaySvr::instantiate(); // MODIFIED by Xiaoyun.Wei, 2016-11-10,BUG-3335803

  ProcessState::self()->startThreadPool();
  IPCThreadState::self()->joinThreadPool();

  return 0;
}























