/* Copyright (C) 2016 Tcl Corporation Limited */
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>

#include "qseeta.h"
#include "common.h"

int TCLSecQSEETA::load(const char *path, int bs)
{
  int fd=0;
  struct stat fInfo;
  char *tbuf=NULL;
  int ret=0;

  if(isload && qseeHandle)
  {
    LOGD("%s has loaded path=%s", ta, path);
    return 0;
  }

  if(!path | !bs)
  {
    LOGE("path is null or bs=0");
    return -1;
  }

  fd = open(path, 0);  //
  if(fd == -1)
  {
    LOGE("open %s failed", path);
    return -1;
  }

  if(fstat(fd, &fInfo) == -1)
  {
    LOGE("fstat failed errno =%d", errno);
    ret=-1;
    goto exit_close;
  }
  tbuf=(char *)malloc(fInfo.st_size);
  if(!tbuf)
  {
    LOGE("malloc failed with size %lld", fInfo.st_size);
    ret=-1;
    goto exit_close;
  }
  ret = read(fd, tbuf, fInfo.st_size);
  if(ret == -1)
  {
    LOGE("read failed with size %lld", fInfo.st_size);
    goto exit_free;
  }
  ret = QSEECom_start_app_V2(&qseeHandle, "alipay", (unsigned char *)tbuf, fInfo.st_size, bs);
  if(0 == ret)
  {
    isload = 1;
    bufSize = bs;
    memset(ta, 0, TAMAX_PATH);
    memcpy(ta, path, strlen(path) < (TAMAX_PATH - 1) ? strlen(path) : (TAMAX_PATH - 1));
    LOGD("load %s ok", ta);
  }
  else
    LOGE("load %s failed", path);

exit_free:
  free(tbuf);
exit_close:
  close(fd);

  return ret;
}

int TCLSecQSEETA::sendCmd(qseecom_cmd *send_cmd)
{
  int32_t ret = 0;
  int32_t req_len = 0;
  int32_t rsp_len = 0;
  qseecom_cmd *msgrsp = 0;
  struct qseecom_app_info app_info;

  if(!qseeHandle )
    return -1;

  LOGD("Send cmd=%d, len=%d", send_cmd->cmd, send_cmd->len);
  ret = QSEECom_get_app_info(qseeHandle, &app_info);
  if (ret) {
    LOGE("Error to get app info\n");
    return -1;
  }
  /* populate the data in shared buffer */
  if (!app_info.is_secure_app_64bit) {
/*
		msgreq=(struct qsc_send_cmd *)qseeHandle->ion_sbuffer;
		msgreq->cmd_id = send_cmd->cmd_id;
		msgreq->data = 100;
		send_buf = (void *)msgreq;
		req_len = sizeof(struct qsc_send_cmd);
		rsp_len = sizeof(struct qsc_send_cmd_rsp);
*/
    LOGD("send cmd to 32bit app, req cmd = %d", send_cmd->cmd);
  } else {
/*
		msgreq_64bit=(struct qsc_send_cmd_64bit *)qseeHandle->ion_sbuffer;
		msgreq_64bit->cmd_id = send_cmd->cmd_id;
		msgreq_64bit->data = 100;
		send_buf = (void *)msgreq_64bit;
		req_len = sizeof(struct qsc_send_cmd_64bit);
		rsp_len = sizeof(struct qsc_send_cmd_rsp);
*/
    LOGD("send cmd to 64bit app, req cmd = %d", send_cmd->cmd);
  }
  QSEECom_set_bandwidth(qseeHandle, true);
  req_len=send_cmd->len;
  rsp_len=bufSize-req_len;

  if (req_len & QSEECOM_ALIGN_MASK)
    req_len = QSEECOM_ALIGN(req_len);
  //if(req_len > 128)
    //req_len=128;

  if (rsp_len & QSEECOM_ALIGN_MASK)
    rsp_len = QSEECOM_ALIGN(rsp_len);
  msgrsp=(qseecom_cmd *)qseeHandle->ion_sbuffer;
  ret = QSEECom_send_cmd(qseeHandle, send_cmd, req_len, msgrsp, rsp_len);
  QSEECom_set_bandwidth(qseeHandle, false);
  return ret;
}

int TCLSecQSEETA::shutDown()
{
  int ret = 0;
  if(qseeHandle)
  {
    ret = QSEECom_shutdown_app(&qseeHandle);
    if(ret)
      LOGE("shutdown %s failed", ta);
    else
    {
      isload=0;
      bufSize=0;
      qseeHandle=NULL;
      LOGE("shutdown %s ok", ta);
    }
  }

  return ret;
}



