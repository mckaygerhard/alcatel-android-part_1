/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef TCLSEC_MUTEX_H
#define TCLSEC_MUTEX_H

#include <pthread.h>



class TCLSecMutex
{
public:
  TCLSecMutex()
  {
    pthread_mutex_init(&mMutex,NULL);
  }
  ~TCLSecMutex()
  {
    pthread_mutex_destroy(&mMutex);
  }

  void lock()
  {
    pthread_mutex_lock(&mMutex);
  }

  void unlock()
  {
    pthread_mutex_unlock(&mMutex);
  }

private:
  pthread_mutex_t mMutex;
};












#endif
