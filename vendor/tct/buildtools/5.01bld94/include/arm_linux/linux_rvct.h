/*
 * linux_rvct.h
 * Interoperation with the GNU toolchain and libraries
 *
 * Copyright (C) ARM Limited, 2007,2008. All rights reserved.
 *
 * RCS $Revision: 172124 $
 * Checkin $Date: 2011-11-03 12:00:41 +0000 (Thu, 03 Nov 2011) $
 * Revising $Author: statham $
 */

/* Linux/glibc and libstdc++ constants and builtins mappings */

#warning "linux_rvct.h is deprecated, please use linux_armcc.h instead."

#include <linux_armcc.h>

/* end of linux_rvct.h */

