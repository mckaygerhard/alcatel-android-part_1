/*
 * limits_rvct.h
 *
 * Copyright (C) ARM Limited, 2007,2008,2010. All rights reserved.
 *
 * RCS $Revision: 172124 $
 * Checkin $Date: 2011-11-03 12:00:41 +0000 (Thu, 03 Nov 2011) $
 * Revising $Author: statham $
 * $URL: http://cam-svn1.cambridge.arm.com/svn/devsys/rvct/clib/BRA_RVCT_5.01/arm_linux/limits_rvct.h $
 */

#warning "limits_rvct.h is deprecated, please use limits_armcc.h instead."

#include <limits_armcc.h>

/* end of limits_rvct.h */

