/* iso646.h: ISO 'C' NA1 library header */
/* Copyright 1996 ARM Limited. All rights reserved. */

/*
 * Copyright ARM Ltd, 1999. All rights reserved.
 * RCS $Revision: 172124 $
 * Checkin $Date: 2011-11-03 12:00:41 +0000 (Thu, 03 Nov 2011) $
 * Revising $Author: drodgman $
 */


#ifndef __iso646_h
#define __iso646_h
#define __ARMCLIB_VERSION 410000

#define and    &&
#define and_eq &=
#define bitand &
#define bitor  |
#define compl  ~
#define not    !
#define not_eq !=
#define or     ||
#define or_eq  |=
#define xor    ^
#define xor_eq ^=

#endif

/* end of iso646.h */

