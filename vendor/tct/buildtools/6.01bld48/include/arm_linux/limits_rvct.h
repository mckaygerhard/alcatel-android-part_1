/*
 * limits_rvct.h
 *
 * Copyright (C) ARM Limited, 2007,2008,2010. All rights reserved.
 *
 * RCS $Revision: 178362 $
 * Checkin $Date: 2013-01-10 16:08:02 +0000 (Thu, 10 Jan 2013) $
 * Revising $Author: pwright $
 * $URL: http://cam-svn1.cambridge.arm.com/svn/devsys/rvct/clib/BRA_RVCT_6.01/arm_linux/limits_rvct.h $
 */

#define __ARMCLIB_VERSION 6010007

#warning "limits_rvct.h is deprecated, please use limits_armcc.h instead."

#include <limits_armcc.h>

/* end of limits_rvct.h */

