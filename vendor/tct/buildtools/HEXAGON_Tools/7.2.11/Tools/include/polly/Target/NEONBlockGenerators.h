//===------ NEONCodeGeneration.h - Code generate the Scops for ARM NEON----===//
//
//                     The LLVM Compiler Infrastructure
//
// Author: Zino Benaissa
// History:
//      Aug, 2012: Support for type conversions.
//
// (c) 2013 Qualcomm Innovation Center, Inc. All rights reserved.
//===----------------------------------------------------------------------===//
//
// This files defines NeonBlockGenerator class, an extension of  Polly
// VectorBlockGenerator. This class targets ARM NEON instructions.
//
//
//===----------------------------------------------------------------------===//

#include "polly/CodeGen/IRBuilder.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/IR/IntrinsicInst.h"

namespace llvm {
class Pass;
class ScalarEvolution;
class DataLayout;
}

namespace polly {
extern bool EnablePartialSums;
extern unsigned PrefetchOffset;
using namespace llvm;
class ScopStmt;

extern int MaxVF;

class NEONBlockGenerator : VectorBlockGenerator {
public:

  static void
  generateNEON(PollyIRBuilder &B, ScopStmt &Stmt, VectorValueMapT &GlobalMaps,
               std::vector<LoopToScevMapT> &VLTS, __isl_keep isl_map *Schedule,
               Pass *P, MapValueVectorT &Reductions, const SumOfSums &Sum2,
               Value *InductionMap[], bool AddAlignment, const DataLayout *Td,
               MDNode *ParallelMD, BasicBlock *BeforeLoop,
               BasicBlock *AfterLoop, bool UseHalfVector) {
    NEONBlockGenerator Generator(B, GlobalMaps, VLTS, Stmt, Schedule, P,
                                 ParallelMD, Reductions, Sum2, InductionMap,
                                 AddAlignment, Td, BeforeLoop, AfterLoop,
                                 UseHalfVector);
    Generator.copyBB();
  }

  static Value *sumScalars(PollyIRBuilder &B, Value *Vector,
                           const Instruction *Inst);

protected:
  NEONBlockGenerator(PollyIRBuilder &B, VectorValueMapT &GlobalMaps,
                     std::vector<LoopToScevMapT> &VLTS, ScopStmt &Stmt,
                     __isl_keep isl_map *Schedule, Pass *P, MDNode *ParallelMD,
                     MapValueVectorT &Reductions, const SumOfSums &Sum2,
                     Value *InductionMap[], bool AddAlignment,
                     const DataLayout *Td, BasicBlock *BeforeLoop,
                     BasicBlock *AfterLoop, bool UseHV)
      : VectorBlockGenerator(B, GlobalMaps, VLTS, Stmt, Schedule, P, ParallelMD,
                             Reductions, Sum2, AddAlignment, Td),
        InductionMap(InductionMap), AccessMap(MemoryMapsT(MaxVF)),
        BeforeLoop(BeforeLoop), AfterLoop(AfterLoop), UseHalfVector(UseHV) {}

  ~NEONBlockGenerator() {}

  // Offset from which at most 16 iterations are vectorized.
  int UBase;

  Value **InductionMap;

  /// @brief Track multistrided accesses.
  MemoryMapsT AccessMap;

  // @brief Vector Map
  VectorValueMapT *VMaps;

  /// @brief Track prefetched accesses.
  /// TODO: Add data structure for tracking prefetched accesses.

  bool UsedMultiStrideStore;

  MapValueVectorT TransposedMap;

  // @brief Track reduction binary operations.
  SetVector<const Value *> ReductionOps;

  // @brief Status indicating whether transpose of 64-bit data is
  // allowed.
  bool Allow64BitTranspose;

  BasicBlock *BeforeLoop, *AfterLoop;

  // Vectorize into half vector.
  bool UseHalfVector;

  bool hasVectorOperands(const Instruction *Inst, ValueMapT &ScalarMap,
                         VectorValueMapT &VectorMaps,
                         bool *HasBothVectorAndScalarMap, int *Idx);

  /// copyUnaryInst vectorizes type conversion operation. It performs this
  /// operation so that it never produces a vector that exceeds the size
  /// of the register size. For example to convert <16 x i8> to <16 x i32>,
  /// it emits the following code:
  ///
  /// lo = shufflevector src, src, < 0, 1, 2, 3, 4, 5, 6, 7 >
  /// loTo16 = sext <8 x i8> lo to <8 x i16>
  /// hi = shufflevector src, src, < 7, 8, 9, 10, 11, 12, 13, 14, 15 >
  /// hiTo16 = sext <8 x i8> hi to <8 x i16>
  /// lo_lo = shufflevector loTo16, lo_to16, < 0, 1, 2, 3 >
  /// lo_loTo32 = sext <4 x i16> lo_lo to <4 x i32>
  /// lo_hi = shufflevector loTo16, lo_to16, < 4, 5, 6, 7 >
  /// lo_hiTo32 = sext <4 x i16> lo_hi to <4 x i32>
  /// hi_lo = shufflevector hiTo16, hi_to16, < 0, 1, 2, 3 >
  /// hi_loTo32 = sext <4 x i16> hi_lo to <4 x i32>
  /// hi_hi = shufflevector hiTo16, lo_to16, < 4, 5, 6, 7 >
  /// hi_hiTo32 = sext <4 x i16> hi_hi to <4 x i32>
  ///
  /// Four vector values are generated to fit <16 x i32>.
  void copyUnaryInst(const UnaryInstruction *Inst, VectorValueMapT &VectorMap,
                     VectorValueMapT &ScalarMaps, int Idx);

  bool extractScalarValues(const Instruction *Inst, VectorValueMapT &VectorMap,
                           VectorValueMapT &ScalarMaps);

  /// coptInstructions decides which instruction can be vectorized by NEON
  /// and if the instruction is vecotrized then it emits the code so that the
  /// input/output vector values fit in a NEON 128-bits register.
  void copyInstruction(const Instruction *Inst, VectorValueMapT &VectorMap,
                       VectorValueMapT &ScalarMaps);

  virtual void copyBB();

  /// @brief Generate a load vector when memory access stride is unknown.
  virtual Value *generateUnknownStrideLoad(const LoadInst *Load,
                                           VectorValueMapT &ScalarMaps);

  /// @brief Generate a store vector when memory access stride is unknown.
  virtual void copyUnknownStrideStore(const StoreInst *Store,
                                      ValueMapT &VectorMap,
                                      VectorValueMapT &ScalarMaps);

private:
  int getVectorFactor();

  void setElementsPerVector(Type *ScalarType, int *Index);

  // Vector primitive support functions.

  /// @brief Combines half vectors into full vectors.
  /// @param Vectors       an array of half vectors.
  /// @param NumVectors    number of vectors.
  ///
  /// Each consecutive pair of vectors is combined into one full vector.
  /// Returns NumVector / 2.
  int combineHalfVectors(Value *Vectors[], int NumVectors);

  /// @brief Split vectors in two halves.
  /// @param Vectors       an array of full vectors.
  /// @param NumVectors    number of vectors.
  ///
  /// Each vector is split in two halves.
  /// Returns 2 * NumVector.
  int splitVectorsIntoHalves(Value *Opnd[], int NumVectors);

  /// Merge two halves (or pad when Opnd2 == nullptr) to form one vector.
  Value *mergeHalves(Value *Opnd, Value *Opnd2 = nullptr);

  Value *getHalf(Value *Opnd, unsigned i = 0);

  /// Sign/zero extend each element of the vector to double its size.
  Value *extendVector(Value *Opnd, llvm::Instruction::CastOps Opcode);

  /// Truncate each element of the vector to half it size.
  Value *truncateVector(Value *Opnd, llvm::Instruction::CastOps Opcode);

  Type *getScalarTypeFromIndex(int Index);

  int getIndex(const Value *Oper, VectorValueMapT &VectorMaps);

  void extendVectorOperand(const Value *Oper, VectorValueMapT &VectorMaps,
                           llvm::Instruction::CastOps Opcode, int Idx);

  void truncateVectorOperand(const Value *Oper, VectorValueMapT &VectorMaps,
                             int Idx);

  int mapOperand(const Value *Oper, VectorValueMapT &VectorMaps,
                 VectorValueMapT &ScalarMaps, Loop *L,
                 CastInst *CastOp = nullptr,
                 const BinaryOperator *Binary = nullptr);

  /// @brief Generate a load vector whose elements are within the multistride
  /// memory access.
  /// Currently valid for ARM NEON target to emit vld2, vld3 and vld4
  /// instructions. This function would emit an intrinsic call for these
  /// instructions.
  Value *generateMultiStrideLoad(const LoadInst *Load, ValueMapT &BBMap,
                                 VectorValueMapT &ScalarMaps, int Stride);

  /// @brief Generate a multistride store.
  /// Currently valid for ARM NEON target to emit vst2, vst3 and vst4
  /// instructions. This function would emit an intrinsic call for these
  /// instructions.
  void copyMultiStrideStore(const StoreInst *Store, ValueMapT &VectorMap,
                            VectorValueMapT &ScalarMaps, int Stride);

  /// @brief Generate comparison for 64-bit vector elements.
  /// There are no direct NEON compare instructions for 64-bit
  /// types. We would have to use vceq and vrev to peform this operation.
  /// Currently we have supported for following conditions,
  /// - EQ
  /// - NE
  void copyCompare64bitInst(const CmpInst *Inst, VectorValueMapT &VectorMap,
                            VectorValueMapT &ScalarMaps, int Idx);

  /// @brief Generate multibit selection (vbsl).
  void copyMultibitSelection(const SelectInst *Inst, VectorValueMapT &VectorMap,
                             VectorValueMapT &ScalarMaps, int Idx);

  /// @brief Generate prefetches.
  void generatePrefetch(const LoadInst *Load, ValueMapT &BBMap,
                        VectorValueMapT &ScalarMaps);

  void insertModuloPrefetch(const Value *Pointer);

  void insertPrefetch(Value *Pointer, int Offset, StringRef str = "");

  /// @brief Generate vector MIN/MAX instructions.
  void generateVMINMAX(const SelectInst *Inst, ValueMapT &VectorMap,
                       VectorValueMapT &ScalarMaps, Intrinsic::ID IntrinsicId);

  /// @brief Generate select instruction.
  bool generateSelectInst(const SelectInst *Inst, VectorValueMapT &VectorMap,
                          VectorValueMapT &ScalarMaps, int VF);

  void transposeVectors(const Value *Inst, int Idx, ValueMapT &VectorMaps);

  void transposeScalarOperand(const Value *Oper, VectorValueMapT &VectorMaps,
                              VectorValueMapT &ScalarMaps, Loop *L);

  /// @brief Scan current basic block to check if we can transpose
  /// 64-bit data loads.
  bool canTranspose64BitValues(BasicBlock *BB);

  /// @brief Generate vector intriniscs
  bool generateVectorIntrinsics(const IntrinsicInst *Call,
                                VectorValueMapT &VectorMap,
                                VectorValueMapT &ScalarMaps, int Idx);

  /// @brief Generate vector saturate.
  bool generateVectorSaturate(const IntrinsicInst *Call,
                              VectorValueMapT &VectorMap,
                              VectorValueMapT &ScalarMaps, int Idx);

  /// @brief Get the saturate intrinsic function call.
  Value *getSaturateIntrinsicCall(Value *Operand, Intrinsic::ID id);

  /// @brief Get the shift right saturate narrow intrinsic function call.
  Value *getShrSaturateIntrinsicCall(Value *Op, Value *ShrVal,
                                     Intrinsic::ID id);

  // @brief Pad half vector operands to saturate.
  int padSaturateOperands(Value *Vectors[], int NumVectors);

  // @brief cache a small array in NEON register (before entering loop) for
  // table lookup.
  void loadSmallArray(GetElementPtrInst *P, int offset, int Size,
                      AllocaInst *Arr, const LoadInst *Load);

  Value *isSmallOffset(Value *Index, int *Offset, int *Bits);

  Value *generateTableLookup(const LoadInst *Load, VectorValueMapT &ScalarMaps);

  // @brief Generate sequence of instructions for multibit
  // selection which will be lowered by backend into an
  // optimal one.
  Value *generateMultiBitSelectionInstructions(Value *OpCond, Value *OpZero,
                                               Value *OpOne);
};
}
