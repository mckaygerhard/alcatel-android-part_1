//===-RunTimeChecks.h -Insert Run Time Checks to check alignment-*- C++ -*-===//
//
// (C) 2013 Qualcomm Innovation Center, Inc. All rights reserved.
//
//
//===----------------------------------------------------------------------===//

#ifndef POLLY_RUN_TIME_CHECKS_H
#define POLLY_RUN_TIME_CHECKS_H

#include "llvm/IR/IRBuilder.h"
#include "llvm/ADT/MapVector.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "polly/CodeGen/Utils.h"
#include "polly/ScopInfo.h"
#include "isl/map.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/AliasSetTracker.h"
#include "llvm/Analysis/LoopInfo.h"

#include <vector>

namespace llvm {
class Pass;
class Loop;
class ScalarEvolution;
class DataLayout;
class AliasAnalysis;
}

Value *hoistStrideZeroPointer(polly::PollyIRBuilder &Builder, const Value *P,
                              Region &R, ScalarEvolution *SE, LoopInfo *LI);

namespace polly {
using namespace llvm;

typedef std::pair<const SCEV *, const SCEV *> StartEndScevPair;

class RunTimeChecksInserter {
public:
  RunTimeChecksInserter(Scop *Sc, Pass *P, const DataLayout *TD, bool AlignmentCheck,
                        AliasAnalysis &AA, bool RTCAlias)
      : S(Sc), P(P), TD(TD), InsertAlignmentChecks(AlignmentCheck),
        AssumeAligned(false), AST(AA), InsertAliasingChecks(RTCAlias),
        ShouldSwapSuccessors(false) {}
  ;

  typedef std::pair<const SCEV *, unsigned> SCEVAlignmentPair;
  typedef SmallVector<SCEVAlignmentPair, 8> SCEVAlignmentVector;
  typedef SmallVector<SCEVAlignmentPair, 8>::iterator SCEVAlignmentIterator;
  typedef SmallVector<MemoryAccess *, 8> MemoryAccessVector;
  typedef SmallVector<MemoryAccess *, 8>::iterator MemoryAccessIterator;
  typedef MapVector<Value *, SetVector<StartEndScevPair> > AliasingChecksMapT;
  typedef MapVector<Value *, SetVector<StartEndScevPair> >::iterator
  AliasingChecksMapIterT;
  typedef SetVector<StartEndScevPair>::iterator SEPairsIterT;
  typedef MapVector<AliasSet *, SetVector<Instruction *> > ASToAccessesMapT;
  typedef MapVector<AliasSet *, SetVector<Instruction *> >::iterator
  ASToAccessesMapIterT;

  unsigned getRequiredAlignment(unsigned Size, int VF) {
    unsigned BitWidth = Size * VF * 8;
    return TD->getABIIntegerTypeAlignment(BitWidth);
  }
  /// @brief Return if codegen should assume aligned vector loads/stores.
  bool shouldAssumeAligned() { return AssumeAligned; }
  ;

  /// @brief Version the Scop by inserting run-time checks.
  BasicBlock *versionScop();

  /// @brief Insert the required run-time checks in the predecessor of BB to
  /// safely version the Scop.
  void insertNecessaryChecks(BasicBlock *BB);

  /// @brief Return true alignment checks are needed for memory accesses in the
  /// Scop.
  bool areAlignmentChecksNeeded(int VF);

  /// @brief: Return true if aliasing checks are needed for addresses in the
  /// Scop.
  bool areAliasingChecksNeeded(int VF);

  /// @brief Generate the alignment checks and inserts them into BB.
  void insertAlignmentChecksInScop(BasicBlock *BB, int VF);

  /// @brief Traverse the SCEV backwards to see if the starting
  /// address is properly aligned.
  ///
  /// @param: The Scev whose base address has been subtracted from it.
  /// i.e. If the AccessFunction is {%x, +, 1}, then Scev is {0, +, 1}
  //  or if the AccessFunction is {%x}, then Scev is {0}.
  bool checkBaseAlignment(const SCEV *Scev, unsigned Alignment);
  /// @brief Analyze BaseAddr and AccessFunction to check for alignment.
  ///
  /// @param BaseAddr The address to check for alignment.
  /// @param AccessFunction The Scalar Evolution of the pointer in the
  ///                       MemoryAccess.
  /// @param Alignment The alignment that we need to check for.
  /// @detail This function analyzes AccessFunction that is an SCEVAddRecExpr,
  /// which are of the type {init_value, operator, step}. Specifically, we
  /// are interested in the init_value. If the init_value is a SCEVUnknown,
  /// then it has to be equal to BaseAddr for us to be able to give any
  /// guarantees by way of RTCs. If the init_value is a SCEVAddrRecExpr, then
  /// its step should be a constant that is a multiple of the Alignment. For
  /// all other cases we cannot guarantee safety by way of RTCs and the function
  /// returns false.
  bool analyzeBaseAddrAndAccessFunction(const Value *BaseAddr,
                                        const SCEVAddRecExpr *AccessFunction,
                                        unsigned Alignment);
  bool analyzeUnknownAccessFunction(const Value *BaseAddr,
                                    const SCEVUnknown *AccessFunction,
                                    unsigned Alignment);

  /// @brief Generate run-time checks for aliasing and insert them into BB.
  void insertAliasingChecksInScop(BasicBlock *BB);

  /// @brief Add pointers in AS to our running list of pointers requiring
  /// run-time checks for aliasing.
  void addPointersInAliasSet(AliasSet *AS);

  // @brief Reduce the number of pointers we have to insert run-time checks for.
  StartEndScevPair reduceChecks(SetVector<StartEndScevPair> &SEPairs);

  /// @brief Print run-time checks.
  void dumpChecks();

private:
  void createAndAddPointerCheckData(Value *Ptr);

  // The Scop that needs to be versioned.
  Scop *S;
  Pass *P;
  const DataLayout *TD;

  BasicBlock *PollyBlock;
  BasicBlock *SeqBlock;
  /// True, if RTCs for alignment should be inserted where appropriate.
  bool InsertAlignmentChecks;

  /// True, if RTCs for alignment have been inserted.
  bool AssumeAligned;

  /// A vector of SCEVs that holds the SCEVs we need to generate
  /// run-time alignment checks for.
  SCEVAlignmentVector SCEVsForAlignmentChecks;

  AliasSetTracker AST;
  /// True, if RTCs for aliasing should be inserted where appropriate.
  bool InsertAliasingChecks;

  /// A Map of where the base address is the key and the values are a set of
  /// StartEndScevPair - i.e pairs of start and end SCEVs of the pointers
  /// in the memory accesses in the Scop.
  AliasingChecksMapT AliasingChecksMap;

  // All the run-time checks that we insert result in a boolean value that, if
  // true at runtime, means we should take the non-vectorized path.
  // This bool indicates that the terminator instruction of the block in which
  // we are adding checks is a conditional branch that does the opposite, i.e.
  // it branches to the vectorized code on true. After inserting our run-time
  // checks, we should, therefore, swap the successors of the conditional
  // branch. If this is false, then it means that after inserting our run-time
  // checks, we should logically and the the result of our checks with the
  // condition in the branch.
  bool ShouldSwapSuccessors;

  DenseSet<StartEndScevPair> Seen;

  /// Alias set to memroy accesses in Scop.
  ASToAccessesMapT ASToAccessesMap;
 };

}
#endif

