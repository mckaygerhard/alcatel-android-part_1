
from subprocess import *
import re
import bisect

objdump_commands = [ "hexagon-objdump", "hexagon-llvm-objdump", "qdsp6-objdump" ]


funcs = []
extrasyms = []
sortsyms = []
sortsym_addrs = []

symtab = {}
rsymtab = {}

# Form PC to function dictionary
pc_to_func = {}

def lookup(name):
	return symtab.get(name,None)

def find_altfunc(pc):
	try:
		return rsymtab[sortsym_addrs[bisect_right(sortsym_addrs,pc)-1]]
	except:
		return '<unknown>'

def pc_to_funcs(all_pcs):
	def findfunc(pc,idx):
		if (idx >= len(funcs)):
			pc_to_func[pc] = find_altfunc(pc)
			return True
		elif (pc < funcs[idx][0]): 
			pc_to_func[pc] = find_altfunc(pc)
			return True
		elif (pc < funcs[idx][1]): 
			pc_to_func[pc] = funcs[funcidx][2]
			return True
		return False
	funcidx = 0
	for pc in all_pcs:
		if findfunc(pc,funcidx): continue
		while (pc >= funcs[funcidx][1]):
			funcidx += 1
			if (funcidx >= len(funcs)): break
		if findfunc(pc,funcidx): continue
		pc_to_func[pc] = find_altfunc(pc)
	return pc_to_func

# read symbols
# "0000b9c0 g     F .text       0000002c unlockMutex"
# "0000b9c0 g       .text       00000000 globallabel"
# Put into list of tuples (start, end, 'name')

# Read functions, return list of tuples (start, end, 'name) or None if bad parse
# Would be cleaner with regex now that F is optional
# Also.... need to put things in symtab so we can lookup for arguments

func_re = re.compile(r"(?P<addr>[0-9a-f]+)\s+(?:[gwl]+\s+)?(?P<isfunc>F\s+)?(?P<section>[.]\S+)\s+(?P<size>[0-9a-f]+)\s+(?P<name>\S+)$")

def read_funcs(cmdname, binname):
	global sortsyms
	global sortsym_addrs
	try:
		p = Popen([cmdname , '-t', binname ],stdout=PIPE,stderr=STDOUT,bufsize=1)
	except:
		# Couldn't execute function
		return None
	for line in p.stdout.xreadlines():
		m = func_re.match(line.strip())
		if not m:
			#print "can't parse <<%s>>" % line.strip()
			continue
		d = m.groupdict()
		if d['isfunc']:
			start = int(d['addr'],16)
			length = int(d['size'],16)
			funcs.append( (start,start+length,d['name']) )
			symtab[d['name']] = start
			rsymtab[start] = d['name']
		elif "text" in d['section']:
			start = int(d['addr'],16)
			extrasyms.append( (start,d['name']) )
			symtab[d['name']] = start
			rsymtab[start] = d['name']
	ret = p.wait()
	if ret != 0: return None
	sortsyms = sorted(extrasyms, lambda x,y: cmp(x[0],y[0]))
	sortsym_addrs = [ x[0] for x in sortsyms]
	funcs.sort(lambda x,y: cmp(x[0],y[0]))
	return funcs

disdict = {}

#func_re = re.compile(r'(?P<addr>[0-9a-f]+)\s+<(?P<name>[^>]+)>:')
disas_re = re.compile(r'\s*(?P<addr>[0-9a-f]+):\s+(?:[0-9a-f]{2}\s+){4}\s*[0-9a-f]{8}\s+(?P<disas>\S.*)\s*$')

def read_disassembly(cmdname, binname):
	packet_pc = 0
	packet = []
	try:
		p = Popen([cmdname , '-d', binname ],stdout=PIPE,stderr=STDOUT,bufsize=1)
	except:
		# couldn't execute function
		return None
	for line in p.stdout.xreadlines():
		line = line.strip()
		#fm = func_re.match(line)
		dm = disas_re.match(line)
		if not dm:
			#if func_re.match(line): continue
			#print "could not parse: <<%s>>" % line
			continue
		pc = int(dm.group('addr'),16)
		disas = dm.group('disas')
		if "{" in disas:
			if packet: disdict[packet_pc] = ";\n".join(packet)
			packet = [ disas ]
			packet_pc = pc
		elif "}" in disas:
			packet.append(disas)
			disdict[packet_pc] = ";\n".join(packet)
			packet = []
		elif '***warn' in disas:
			packet = []
			continue
		elif packet:
			packet.append(disas)
		else:
			disdict[pc] = disas

def get_info(binname, debugmode):
	for cmd in objdump_commands:
		if not read_funcs(cmd,binname):
			continue
		read_disassembly(cmd,binname)

		if debugmode:
			print "Disassembler used: ",
			for l in Popen(['bash', '-c', 'type ' + cmd], stdout=PIPE).stdout.xreadlines():
				print l,

		break

if __name__ == "__main__":
	import sys
	get_info(sys.argv[1])
	items = sorted(disdict.items(), lambda x,y: cmp(x[0],y[0]))
	for pc,disas in items:
		print pc,'::::',disas
		print
