/* ======================================================================== */
/*  QUALCOMM TECHNOLOGIES, INC.                                             */
/*                                                                          */
/*  HEXAGON HVX Image/Video Processing Library                              */
/*                                                                          */
/* ------------------------------------------------------------------------ */
/*          Copyright (c) 2014 QUALCOMM TECHNOLOGIES Incorporated.          */
/*                           All Rights Reserved.                           */
/*                  QUALCOMM Confidential and Proprietary                   */
/* ======================================================================== */

/*[========================================================================]*/
/*[ FUNCTION                                                               ]*/
/*[     invsqrt                                                            ]*/
/*[                                                                        ]*/
/*[------------------------------------------------------------------------]*/
/*[ DESCRIPTION                                                            ]*/
/*[     This function computes 1 / squareroot(x) using interpolation.      ]*/
/*[     Input is in unsigned 16 bits while the output is in Q12 format.    ]*/
/*[     If zero is passed in, the function will return largest number in   ]*/
/*[     Q12 format.                                                        ]*/
/*[                                                                        ]*/
/*[------------------------------------------------------------------------]*/
/*[ REVISION DATE                                                          ]*/
/*[     AUG-01-2014                                                        ]*/
/*[                                                                        ]*/
/*[========================================================================]*/
#include "invsqrt.h"

/* ======================================================================== */
/*  Reference C version.                                                    */
/* ======================================================================== */
void invsqrt(
    unsigned short *input,
    unsigned short *sqrt_recip_shft,
    unsigned short *sqrt_recip_val,
    unsigned        width
    )
{
    unsigned t1, x, i;
    unsigned short shft, t2, idx, frac, y, slope, t3;
    static unsigned short val_table[24] = {
        4096, 3862, 3664, 3493, 3344, 3213, 3096, 2991,
        2896, 2810, 2731, 2658, 2591, 2528, 2470, 2416,
        2365, 2317, 2272, 2230, 2189, 2151, 2115, 2081
    };
    static unsigned short slope_table[24] = {
        234, 198, 171, 149, 131, 117, 105, 95,
        86, 79, 73, 67, 63, 58, 54, 51,
        48, 45, 42, 41, 38, 36, 34, 33
    };
    int     shift_nbits;

    for (i = 0; i<width; i++)
    {
        x = input[i];
        if (x == 0) x = 1;

        if (x >> 30) shft = 15;
        else if (x >> 28) shft = 14;
        else if (x >> 26) shft = 13;
        else if (x >> 24) shft = 12;
        else if (x >> 22) shft = 11;
        else if (x >> 20) shft = 10;
        else if (x >> 18) shft = 9;
        else if (x >> 16) shft = 8;
        else if (x >> 14) shft = 7;
        else if (x >> 12) shft = 6;
        else if (x >> 10) shft = 5;
        else if (x >> 8) shft = 4;
        else if (x >> 6) shft = 3;
        else if (x >> 4) shft = 2;
        else if (x >> 2) shft = 1;
        else              shft = 0;

        shift_nbits = 13 - 2 * shft;
        t1 = (shift_nbits >= 0) ? (x << shift_nbits) : (x >> -shift_nbits);
        t2 = t1 >> 10;
        idx = t2 - 8;

        frac = t1 & 0x3ff;
        y = val_table[idx];
        slope = slope_table[idx];
        t3 = (slope * frac + 512) >> 10;

        sqrt_recip_val[i] = y - t3;
        sqrt_recip_shft[i] = shft;
    }
}
