/* ======================================================================== */
/*  QUALCOMM TECHNOLOGIES, INC.                                             */
/*                                                                          */
/*  HEXAGON HVX Image/Video Processing Library                              */
/*                                                                          */
/* ------------------------------------------------------------------------ */
/*          Copyright (c) 2014 QUALCOMM TECHNOLOGIES Incorporated.          */
/*                           All Rights Reserved.                           */
/*                  QUALCOMM Confidential and Proprietary                   */
/* ======================================================================== */

/*[========================================================================]*/
/*[ FUNCTION                                                               ]*/
/*[     fast9                                                              ]*/
/*[                                                                        ]*/
/*[------------------------------------------------------------------------]*/
/*[ DESCRIPTION                                                            ]*/
/*[     This function performs an FAST feature detection. It checks 16     ]*/
/*[ pixels in a circle around the candidate point. If 9 contiguous pixels  ]*/
/*[ are all brighter or darker than the nucleus by a threshold, the pixel  ]*/
/*[ under the nucleus is considered to be a feature.                       ]*/
/*[                                                                        ]*/
/*[------------------------------------------------------------------------]*/
/*[ REVISION DATE                                                          ]*/
/*[     DEC-01-2014                                                        ]*/
/*[                                                                        ]*/
/*[========================================================================]*/
#include <stdio.h>
#include <stdlib.h>
#include "hvx.cfg.h"
/* ======================================================================== */
/*  Functions defined in Assembly                                           */
/* ======================================================================== */
void fast9_detect_coarse(
    unsigned char       *im,
    unsigned int         xsize,
    unsigned int         stride,
    unsigned int         barrier,
    unsigned int        *bitmask,
    unsigned int         boundary
    );

int fast9_detect_fine(
    const unsigned char *im,
    unsigned int         num_pixels32,
    unsigned int         stride,
    unsigned int         barrier,
    unsigned int        *bitmask,
    short int           *xpos,
    int                  xstart
    );

/* ======================================================================== */
void sort( short *a, int n )
{
    int i, j;
    short temp;

    for (i = 1; i < n; ++i)
    {
        for (j = 0; j < (n-i); ++j)
        {
            if(a[j] > a[j+1])
            {
                temp  = a[j  ];
                a[j  ]= a[j+1];
                a[j+1]= temp;
            }
        }
    }
}

/* ======================================================================== */
void fast9(
    const unsigned char *im,
    unsigned int         stride,
    unsigned int         xsize,
    unsigned int         ysize,
    unsigned int         barrier,
    unsigned int         border,
    short int           *xy,
    int                  maxnumcorners,
    int                 *numcorners
    )
{
    *numcorners = 0;
    int boundary = border>3 ? border : 3;
    unsigned int xstart = boundary&(-VLEN);
    unsigned int num_pixels = xsize - xstart - boundary;
    num_pixels = (num_pixels+8*VLEN-1)&(-8*VLEN); // roundup to 8*VLEN
    unsigned int num_pixels32 = num_pixels >> 5;

    unsigned int *bitmask = (unsigned int *)memalign(VLEN, num_pixels32*sizeof(unsigned int));
    short        *xpos    = (short        *)malloc(xsize*sizeof(short));

    int y, num, n, k;

    for (y = boundary; y < (ysize-boundary); ++y)
    {
        unsigned char* p = (unsigned char *)im + y*stride;

        fast9_detect_coarse(p, xsize, stride, barrier, bitmask, boundary);

        num = fast9_detect_fine(p, num_pixels32, stride, barrier, bitmask, xpos, xstart);

        sort(xpos, num);

        k = maxnumcorners - (*numcorners);
        n = ( k > num ) ? num : k;

        for (k = 0; k < n; k++)
        {
            *(xy++) = xpos[k];
            *(xy++) = y;
        }

        *numcorners += n;

        if( *numcorners >= maxnumcorners )
        {
            break;
        }
    }

    free(xpos   );
    free(bitmask);
}


