/* ======================================================================== */
/*  QUALCOMM TECHNOLOGIES, INC.                                             */
/*                                                                          */
/*  HEXAGON HVX Image/Video Processing Library                              */
/*                                                                          */
/* ------------------------------------------------------------------------ */
/*          Copyright (c) 2014 QUALCOMM TECHNOLOGIES Incorporated.          */
/*                           All Rights Reserved.                           */
/*                  QUALCOMM Confidential and Proprietary                   */
/* ======================================================================== */

/*[========================================================================]*/
/*[ FUNCTION                                                               ]*/
/*[     invsqrt                                                            ]*/
/*[                                                                        ]*/
/*[------------------------------------------------------------------------]*/
/*[ DESCRIPTION                                                            ]*/
/*[     This function computes 1 / squareroot(x) using interpolation.      ]*/
/*[     Input is in unsigned 16 bits while the output is in Q12 format.    ]*/
/*[     If zero is passed in, the function will return largest number in   ]*/
/*[     Q12 format.                                                        ]*/
/*[                                                                        ]*/
/*[------------------------------------------------------------------------]*/
/*[ REVISION DATE                                                          ]*/
/*[     AUG-01-2014                                                        ]*/
/*[                                                                        ]*/
/*[========================================================================]*/
#include <assert.h>
#include <stdlib.h>
#include "hvx.cfg.h"
#include "invsqrt.h"
#if defined(__hexagon__)
#include "hexagon_types.h"
#endif
#include "io.h"

/* ======================================================================== */
/*  Intrinsic C version.                                                    */
/* ======================================================================== */
void invsqrt(
    unsigned short *restrict input,
    unsigned short *restrict sqrt_recip_shft,
    unsigned short *restrict sqrt_recip_val,
    unsigned        width
    )
{
    unsigned i;
    static unsigned short val_table[24] __attribute__((aligned(VLEN))) = {
        4096, 3862, 3664, 3493, 3344, 3213, 3096, 2991,
        2896, 2810, 2731, 2658, 2591, 2528, 2470, 2416,
        2365, 2317, 2272, 2230, 2189, 2151, 2115, 2081
    };
    static unsigned char slope_table[24] __attribute__((aligned(VLEN))) = {
        234, 198, 171, 149, 131, 117, 105, 95,
        86, 79, 73, 67, 63, 58, 54, 51,
        48, 45, 42, 41, 38, 36, 34, 33
    };
    HVX_Vector *pinput = (HVX_Vector *)input;
    HVX_Vector *pval = (HVX_Vector *)sqrt_recip_val;
    HVX_Vector *pshft = (HVX_Vector *)sqrt_recip_shft;
    HVX_Vector sConst_2 = Q6_V_vsplat_R(0xfffefffe);
    HVX_Vector sConst31 = Q6_V_vsplat_R(0x1f1f1f1f);
    HVX_Vector sConst3ff = Q6_V_vsplat_R(0x7fe07fe0);
    HVX_Vector sZero = Q6_V_vzero();
    HVX_Vector sConst15 = Q6_V_vsplat_R(0x0f000f);
    HVX_Vector sConst8 = Q6_V_vsplat_R(0x8080808);
    HVX_Vector  sValtbl = Q6_Vh_vshuff_Vh(*(HVX_Vector*)val_table);
    HVX_Vector  sSlopetbl = Q6_Vb_vshuff_Vb(*(HVX_Vector*)slope_table);
    HVX_Vector sShft0, sShft1, sX0, sX1, sX2, sX3, sFracL, sFracH, sIdxL, sIdxH, sSlopeL, sSlopeH;
    HVX_VectorPair dY, dSlope;
    int length = (width + VLEN - 1) & -VLEN;

    for (i = 0; i < length; i += VLEN)
    {

        sX0 = *pinput++;
        sShft0 = Q6_Vuh_vcl0_Vuh(sX0);
        sShft0 = Q6_V_vand_VV(sShft0, sConst_2);
        sX1 = *pinput++;
        sShft1 = Q6_Vuh_vcl0_Vuh(sX1);
        sShft1 = Q6_V_vand_VV(sShft1, sConst_2);

        sX0 = Q6_Vh_vasl_VhVh(sX0, sShft0);
        sX1 = Q6_Vh_vasl_VhVh(sX1, sShft1);

        sShft0 = Q6_Vh_vnavg_VhVh(sConst15, sShft0);
        sShft0 = Q6_Vh_vmax_VhVh(sZero, sShft0);
        *pshft++ = sShft0;

        sShft1 = Q6_Vh_vnavg_VhVh(sConst15, sShft1);
        sShft1 = Q6_Vh_vmax_VhVh(sZero, sShft1);
        *pshft++ = sShft1;

        sX2 = Q6_Vb_vshuffo_VbVb(sX1, sX0);
        sFracL = Q6_Vh_vasl_VhR(sX0, 4);
        sFracL = Q6_V_vand_VV(sFracL, sConst3ff);
        sFracH = Q6_Vh_vasl_VhR(sX1, 4);
        sFracH = Q6_V_vand_VV(sFracH, sConst3ff);
        sIdxL = Q6_Vuh_vlsr_VuhR(sX2, 3);
        sIdxL = Q6_V_vand_VV(sIdxL, sConst31);
        sIdxL = Q6_Vub_vsub_VubVub_sat(sIdxL, sConst8);
        sIdxL = Q6_Vb_vdeal_Vb(sIdxL);
        sSlopeL = Q6_Vb_vlut32_VbVbR(sIdxL, sSlopetbl, 0);
        dY = Q6_Wh_vlut16_VbVhR(sIdxL, sValtbl, 0);
        dY = Q6_Wh_vlut16or_WhVbVhR(dY, sIdxL, sValtbl, 1);
        dSlope = Q6_W_vshuff_VVR(sZero, sSlopeL, -1);

        dY = Q6_W_vshuff_VVR(Q6_V_hi_W(dY), Q6_V_lo_W(dY), -2);
        sSlopeL = Q6_Vh_vmpy_VhVh_s1_rnd_sat(Q6_V_lo_W(dSlope), sFracL);
        sSlopeH = Q6_Vh_vmpy_VhVh_s1_rnd_sat(Q6_V_hi_W(dSlope), sFracH);
        *pval++ = Q6_Vh_vsub_VhVh(Q6_V_lo_W(dY), sSlopeL);
        *pval++ = Q6_Vh_vsub_VhVh(Q6_V_hi_W(dY), sSlopeH);
    }
}
