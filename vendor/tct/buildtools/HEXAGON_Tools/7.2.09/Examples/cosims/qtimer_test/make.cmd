::###############################################################
:: Copyright (c) \$Date\$ QUALCOMM INCORPORATED.
:: All Rights Reserved.
:: Modified by QUALCOMM INCORPORATED on \$Date\$
::################################################################
::
:: Make.cmd to build the example
::

::@echo off
@set HEXAGON_TOOLS=

::################################################
:: Make sure that `where` command only picks up ##
:: first path it finds, in case there are more  ##
:: than one version tools path in PATH variable ##
::################################################
@for /f "delims=" %%a in ('where hexagon-sim') do (
    @set HEXAGON_TOOLS=%%a
    @goto :done
)
:done
@set HEXAGON_TOOLS=%HEXAGON_TOOLS%\..\..

for /f "delims=" %%a in ('cd') do @set SRC_TOP=%%a

@set CC=hexagon-clang
@set SIM=hexagon-sim
@set Q6VERSION=v5
@set TESTFILE_NAME=qtimer_test
@set CFLAGS=-m%Q6VERSION% -g -m%Q6VERSION% -DCSR_BASE=0xfc900000 -DQTMR_FREQ=19200000 -DIRQ1=35 -DIRQ2=36
@set SIMFLAGS=--timing --cosim_file %Q6_CFG% -m%Q6VERSION% %TESTFILE_NAME%.elf

@set Q6_CFG=config/q6ss_win.cfg
@set T32_CFG=config/t32.cfg hexagon-mcd64.dll
@set T32_EXE=bin/windows64/t32mqdsp6.exe

@IF "%1"=="clean" GOTO Clean
@IF "%1"=="build" GOTO Build
@IF "%1"=="sim" GOTO Sim
@IF "%1"=="t32" GOTO T32

::
:: Clean up
::
:Clean
@echo.
@echo Cleaning files...
@IF EXIST pmu_stats* @DEL pmu_stats*
@IF EXIST stats.txt @DEL stats.txt
@IF EXIST %TESTFILE_NAME%.elf @DEL %TESTFILE_NAME%.elf
@IF "%1"=="clean" GOTO End

::
:: Build the bus cosim
::
:Build
@echo.
%CC% %CFLAGS% %TESTFILE_NAME%.c -o %TESTFILE_NAME%.elf -lhexagon -DJUST_WAIT
@IF "%1"=="build" GOTO End

::
:: Simulate the example
::
:Sim
@echo.
@echo Simulating example
%SIM% %SIMFLAGS%
GOTO End

::
:: Debug using Trace32
::
:T32
@set T32SYS=%T32SYS%
%T32SYS%/%T32_EXE% -c %T32_CFG% -s cmm/hexagon.cmm %TESTFILE_NAME%.elf %Q6VERSION% %Q6_CFG%

::
:: El Fin
:End
::
