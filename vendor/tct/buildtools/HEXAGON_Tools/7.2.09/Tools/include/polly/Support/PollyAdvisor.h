//===--- PollyAdvisor.h - Offer advice on how to use polly ------*- C++ -*-===//
//
// (C) 2014 Qualcomm Innovation Center, Inc. All rights reserved.
//
//===----------------------------------------------------------------------===//
// Print diagnostics and suggestions.
//===----------------------------------------------------------------------===//
#ifndef POLLY_SUPPORT_ADVISOR_H
#define POLLY_SUPPORT_ADVISOR_H

namespace llvm {
class BasicBlock;
class Loop;
class Twine;
class Region;
class RegionInfo;
class LoopInfo;
}
using namespace llvm;

namespace polly {
class RejectReason;
class VectorizerDiagnostic;

enum class RegionStat {
  Invalid, Valid, Found, Empty
};

class PollyAdvisor {
public:
  PollyAdvisor();
  void reportInvalidScop(const RejectReason &Reason, const Region &R,
                         const RegionInfo *RI, const LoopInfo *LI) const;
  void reportVectorizerDiag(const VectorizerDiagnostic &VD) const;
  void reportRegionStat(RegionStat Stat, const Region &R, const RegionInfo *RI,
                        const LoopInfo *LI) const;
  bool enabled() const { return EnableNotes || EnableStats || EnableOptReport; }

private:
  bool EnableNotes;
  bool EnableStats;
  bool EnableOptReport;
};
}

#endif
