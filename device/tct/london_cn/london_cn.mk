DEVICE_PACKAGE_OVERLAYS := device/tct/$(TARGET_PRODUCT)/overlay
DEVICE_PACKAGE_OVERLAYS += custo_wimdata_ng/wlanguage/overlay
TARGET_USES_QCOM_BSP := true
#BOARD_HAVE_QCOM_FM := true
# Add QC Video Enhancements flag
TARGET_ENABLE_QC_AV_ENHANCEMENTS := true
TARGET_USES_NQ_NFC := false
TARGET_USES_NXP_NFC := true
TARGET_KERNEL_VERSION := 3.18
#QTIC flag
-include $(QCPATH)/common/config/qtic-config.mk
TARGET_USES_QTIC := false

# Enable features in video HAL that can compile only on this platform
TARGET_USES_MEDIA_EXTENSIONS := true

# media_profiles and media_codecs xmls for msm8953
ifeq ($(TARGET_ENABLE_QC_AV_ENHANCEMENTS), true)
PRODUCT_COPY_FILES += device/qcom/msm8953_32/media/media_profiles_8953.xml:system/etc/media_profiles.xml \
                      device/qcom/msm8953_32/media/media_codecs_8953.xml:system/etc/media_codecs.xml \
                      device/qcom/msm8953_32/media/media_codecs_performance_8953.xml:system/etc/media_codecs_performance.xml
endif

PRODUCT_COPY_FILES += device/tct/$(TARGET_PRODUCT)/whitelistedapps.xml:system/etc/whitelistedapps.xml \
                      device/tct/$(TARGET_PRODUCT)/gamedwhitelist.xml:system/etc/gamedwhitelist.xml

#[FEATURE]-Add-BEGIN by TCTNB.93391,11/30/2015,ALM927723,USB Driver Auto Install
PRODUCT_COPY_FILES += \
    device/tct/common/USBDriver_Automatic.iso:system/etc/USBDriver.iso
#[FEATURE]-Add-END by TCTNB.93391,11/30/2015,ALM927723,USB Driver Auto Install

# Task-3425273
BOARD_FRP_PARTITION_NAME := config

PRODUCT_PROPERTY_OVERRIDES += \
           dalvik.vm.heapminfree=4m \
           dalvik.vm.heapstartsize=16m
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)
$(call inherit-product, device/qcom/common/common64.mk)
$(call inherit-product, device/tct/common/common.mk)

PRODUCT_NAME := london_cn
PRODUCT_DEVICE := london_cn
PRODUCT_BRAND := TCL
PRODUCT_MODEL := msm8953 for arm64

PRODUCT_BOOT_JARS += tcmiface

ifneq ($(strip $(QCPATH)),)
PRODUCT_BOOT_JARS += WfdCommon
#PRODUCT_BOOT_JARS += com.qti.dpmframework
#PRODUCT_BOOT_JARS += dpmapi
#PRODUCT_BOOT_JARS += com.qti.location.sdk
#Android oem shutdown hook
PRODUCT_BOOT_JARS += oem-services
endif

#ifeq ($(strip $(BOARD_HAVE_QCOM_FM)),true)
PRODUCT_BOOT_JARS += qcom.fmradio
#endif #BOARD_HAVE_QCOM_FM
PRODUCT_BOOT_JARS += qcmediaplayer

# default is nosdcard, S/W button enabled in resource
PRODUCT_CHARACTERISTICS := nosdcard

# When can normal compile this module,  need module owner enable below commands
# font rendering engine feature switch
#-include $(QCPATH)/common/config/rendering-engine.mk
#ifneq (,$(strip $(wildcard $(PRODUCT_RENDERING_ENGINE_REVLIB))))
#    MULTI_LANG_ENGINE := REVERIE
#    MULTI_LANG_ZAWGYI := REVERIE
#endif



#Android EGL implementation
PRODUCT_PACKAGES += libGLES_android

# Audio configuration file
-include $(TOPDIR)hardware/qcom/audio/configs/msm8953/msm8953.mk

# MIDI feature
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.midi.xml:system/etc/permissions/android.software.midi.xml

#ANT+ stack
PRODUCT_PACKAGES += \
    AntHalService \
    libantradio \
    antradio_app

PRODUCT_PACKAGES += wcnss_service

# MSM IRQ Balancer configuration file
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/msm_irqbalance.conf:system/vendor/etc/msm_irqbalance.conf

#TCTNB.THW - nxp smart pa speaker develop
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/nxp/tfa98xx.cnt:system/etc/firmware/tfa98xx.cnt
#end TCTNB.THW

#[BUGFIX]-MOD-BEGIN by ZhangJie,10/19/2016,3141159,
#wlan driver
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/WCNSS_qcom_cfg.ini:system/etc/wifi/WCNSS_qcom_cfg.ini \
    device/qcom/msm8953_32/WCNSS_wlan_dictionary.dat:persist/WCNSS_wlan_dictionary.dat \
    device/tct/$(TARGET_PRODUCT)/WCNSS_tct_wlan_nv.bin:persist/WCNSS_tct_wlan_nv.bin
#[BUGFIX]-MOD-END by TCTNB.ZhangJie

PRODUCT_PACKAGES += \
    wpa_supplicant_overlay.conf \
    p2p_supplicant_overlay.conf

# Feature definition files for msm8953
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:system/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:system/etc/permissions/android.hardware.sensor.stepdetector.xml

PRODUCT_PACKAGES += telephony-ext
PRODUCT_BOOT_JARS += telephony-ext

# When can normal compile this module, need module owner enable below commands
# Add the overlay path
#PRODUCT_PACKAGE_OVERLAYS := $(QCPATH)/qrdplus/Extension/res \
#        $(QCPATH)/qrdplus/globalization/multi-language/res-overlay \
#        $(PRODUCT_PACKAGE_OVERLAYS)
#PRODUCT_PACKAGE_OVERLAYS := $(QCPATH)/qrdplus/Extension/res \

#Tcl_monster add for mst-framework,2016.6.22 {
PRODUCT_PACKAGES += \
    mst-framework \
    mst-framework-res

# Override the PRODUCT_BOOT_JARS to include the system base modules for global access
PRODUCT_BOOT_JARS += \
    mst-framework
#Tcl_monster }

#[SOLUTION]-Add-BEGIN by TCTNB.wen.zhuang, 08/09/2016, SOLUTION-2559001
PRODUCT_PACKAGES += TctSaleMode
#[SOLUTION]-Add-END by TCTNB.wen.zhuang

PRODUCT_PACKAGES += OTAProvisioningClient

#for android_filesystem_config.h
PRODUCT_PACKAGES += \
    fs_config_files

PRODUCT_PACKAGES += MagicLock \
    VLife \
    libvlife_media \
    libvlife_openglutil \
    libvlife_render

PRODUCT_PACKAGES += \
    busybox \
    backupAppData.sh \
    restoreAppData.sh

# Sensor HAL conf file
 PRODUCT_COPY_FILES += \
     device/tct/$(TARGET_PRODUCT)/sensors/hals.conf:system/etc/sensors/hals.conf

# Disable Verity boot feature
PRODUCT_SUPPORTS_VERITY := true

#[BUGFIX]-Add-BEGIN by Dandan.Fang, 2016/08/15 ,for Task2657073
PRODUCT_COPY_FILES += \
    device/tct/common/region_ver:system/etc/region_ver
#[BUGFIX]-Add-END by Dandan.Fang
#for tct after sale log task1167051
PRODUCT_COPY_FILES += \
    device/tct/london/tct_as_log.sh:system/etc/tct_as_log.sh

ifeq ($(strip $(TARGET_USES_NQ_NFC)),true)
PRODUCT_PACKAGES += \
    NQNfcNci \
    libnqnfc-nci \
    libnqnfc_nci_jni \
    nfc_nci.nqx.default \
    libp61-jcop-kit \
    com.nxp.nfc.nq \
    com.nxp.nfc.nq.xml \
    libpn547_fw.so \
    libpn548ad_fw.so \
    libnfc-brcm.conf \
    libnfc-nxp.conf \
    nqnfcee_access.xml \
    nqnfcse_access.xml \
    Tag \
    com.android.nfc_extras \
    libQPayJNI \
    com.android.qti.qpay \
    com.android.qti.qpay.xml \
    SmartcardService \
    org.simalliance.openmobileapi \
    org.simalliance.openmobileapi.xml

PRODUCT_COPY_FILES += \
    packages/apps/Nfc/migrate_nfc.txt:system/etc/updatecmds/migrate_nfc.txt \
    frameworks/native/data/etc/com.nxp.mifare.xml:system/etc/permissions/com.nxp.mifare.xml \
    frameworks/native/data/etc/com.android.nfc_extras.xml:system/etc/permissions/com.android.nfc_extras.xml \
    frameworks/native/data/etc/android.hardware.nfc.xml:system/etc/permissions/android.hardware.nfc.xml \
    frameworks/native/data/etc/android.hardware.nfc.hce.xml:system/etc/permissions/android.hardware.nfc.hce.xml
# SmartcardService, SIM1,SIM2,eSE1 not including eSE2,SD1 as default
ADDITIONAL_BUILD_PROPERTIES += persist.nfc.smartcard.config=SIM1,SIM2,eSE1
endif # TARGET_USES_NQ_NFC

ifeq ($(TARGET_USES_NXP_NFC),true)
PRODUCT_PACKAGES += \
    nfc.msm8952 \
    NfcNci \
    libnfc-nci \
    libnfc_nci_jni \
    Tag \
    nfc_nci.pn54x.default \
    com.android.nfc_extras \
    com.gsma.services.nfc \
    com.gsma.services.nfc.xml \
    libnfctest

# NFCEE access control config
ifeq ($(TARGET_BUILD_VARIANT),user)
     NFCEE_ACCESS_PATH := device/tct/$(TARGET_PRODUCT)/nfc/nfcee_access.xml
else
     NFCEE_ACCESS_PATH := device/tct/$(TARGET_PRODUCT)/nfc/nfcee_access_debug.xml
endif

PRODUCT_COPY_FILES += \
    packages/apps/Nfc/migrate_nfc.txt:system/etc/updatecmds/migrate_nfc.txt \
    $(NFCEE_ACCESS_PATH):system/etc/nfcee_access.xml \
    frameworks/native/data/etc/com.nxp.mifare.xml:system/etc/permissions/com.nxp.mifare.xml \
    frameworks/native/data/etc/com.android.nfc_extras.xml:system/etc/permissions/com.android.nfc_extras.xml \
    frameworks/native/data/etc/android.hardware.nfc.xml:system/etc/permissions/android.hardware.nfc.xml \
    frameworks/native/data/etc/android.hardware.nfc.hce.xml:system/etc/permissions/android.hardware.nfc.hce.xml \
    frameworks/native/data/etc/android.hardware.nfc.hcef.xml:system/etc/permissions/android.hardware.nfc.hcef.xml \
    device/tct/$(TARGET_PRODUCT)/nfc/libnfc-brcm.conf:system/etc/libnfc-brcm.conf \
    external/libnfc-nci/halimpl/pn54x/libpn548ad_fw.so:system/vendor/firmware/libpn548ad_fw.so \
    device/tct/$(TARGET_PRODUCT)/nfc/libnfc-nxp.conf:system/etc/libnfc-nxp.conf
endif # TARGET_USES_NXP_NFC

# List of AAPT configurations
PRODUCT_AAPT_CONFIG += xlarge large
#[SOLUTION]-Add-BEGIN by TCTNB.wen.zhuang, 08/09/2016, SOLUTION-2559001
PRODUCT_PACKAGES += boot_time_check
#[SOLUTION]-Add-END by TCTNB.wen.zhuang

#[FEATURE]-Add-BEGIN by TCTNB.dong.Jiang,2016/08/17, Task-2655792, [Telecom][Debug]Record Network related abnormal info
#Porting from Task-1647082
PRODUCT_PACKAGES += TelecomRecord
#[FEATURE]-Add-END by TCTNB.Dong.Jiang

# Task-2753780, exfat file-system
PRODUCT_PACKAGES += mkfs.exfat fsck.exfat

#jiangunag.sun task-2987595
PRODUCT_PACKAGES += FingerprintProvider
#Task-2778076, thermal in power off charger
PRODUCT_PACKAGES += charger.fstab.qcom
#Task-3097508, disable FDE for mmi build
PRODUCT_PACKAGES += mini.fstab.qcom

#for android_filesystem_config.h
PRODUCT_PACKAGES += \
    fs_config_files

# Enable logdumpd service only for non-perf bootimage
ifeq ($(findstring perf,$(KERNEL_DEFCONFIG)),)
    ifeq ($(TARGET_BUILD_VARIANT),user)
        PRODUCT_DEFAULT_PROPERTY_OVERRIDES+= \
            ro.logdumpd.enabled=0
    else
        PRODUCT_DEFAULT_PROPERTY_OVERRIDES+= \
            ro.logdumpd.enabled=1
    endif
else
    PRODUCT_DEFAULT_PROPERTY_OVERRIDES+= \
        ro.logdumpd.enabled=0
endif
#[FEATURE]-Add-BEGIN by TCTNB.Yang.Hu,2015/9/30, FR-572426 TCT DRM solution
PRODUCT_PACKAGES += \
    libcombinedengine \
    libcombined-common \
    libcombined-converter \
    libcombined-decoder \
    libdrmdecoder \
    libdrmparserdm \
    libdrmrightmanager \
    libdrmxmlparser \
    libseparate-common \
    libseparate-decoder \
    libseparate-parser \
    libseparateengine
#[FEATURE]-Add-END by TCTNB.Yang.Hu,2015/9/30, TCT DRM solution

#[SOLUTION]-Add-BEGIN by TCTNB.Zhang Jinbo, 08/19/2016, TASK-2776549
ifeq ($(TCT_TARGET_CONNECTIVITY_VERBOSE_LOG_ENABLE),true)
PRODUCT_PACKAGES += tcpdump
PRODUCT_PACKAGES += ptt_socket_app
PRODUCT_PACKAGES += WifiLogger_app
endif
#[SOLUTION]-Add-BEGIN by TCTNB.Zhang Jinbo, 08/19/2016, TASK-2776549

#[SOLUTION]-Add-BEGIN by TCTNB(Guoqiang.Qiu),2016-8-26, Solution-2699694
PRODUCT_PACKAGES += MagicLock \
    VLife \
    libvlife_media \
    libvlife_openglutil \
    libvlife_render
#[SOLUTION]-Add-END by TCTNB(Guoqiang.Qiu)

# [FEATURE]-Add-BEGIN by TCTNB.yandong.sun, 2016/08/18 2759149, add atfwd
PRODUCT_PACKAGES += atfwd
# [FEATURE]-Add-END by TCTNB.yandong.sun

#BUGFIX]-Add-BEGIN by TCTNB.qili.zhang,30/08/2016,Task2813004 portingTask 1177186
#SMS and data auto-registeration features for China Telecom
PRODUCT_PACKAGES +=\
      TctAutoRegister\
      TctPsAutoRegister
#[BUGFIX]-Add-END by TCTNB.qili.zhang

#System Application for London spec
PRODUCT_PACKAGES += \
      Settings2 \
      WebViewGoogle

PRODUCT_PACKAGES += \
      PowerOnAlert

#[FEATURE]-Add-BEGIN by TCL_XA, 09/28/2016,FR-2825857
PRODUCT_PACKAGES += \
      SecretMessage2
#[FEATURE]-End-BEGIN by TCL_XA, 09/28/2016,FR-2825857

#[BUGFIX]-Add-BEGIN by chunzhi.sun, 2016/10/08 ,for Task2831181
PRODUCT_PACKAGES += \
      MaxxAudio
#[BUGFIX]-Add-END by chunzhi.sun

PRODUCT_PACKAGES += \
      MaxxService

# smcn hujianwei 20160922 modify for add smcn properties control start
SMCN_ROM_CONTROL_FLAG := true
# smcn hujianwei 20160922 modify for add smcn properties control end

# smcn weijiang.he 20161024 modify for add rom version start
ROM_VERSION := V1.0.1
ROM_PROJECT := London
# smcn weijiang.he 20161024 modify for add rom version end

# smcn hujianwei 20160926 modify for add monster product control start
$(call inherit-product-if-exists, smcn/packages/apps/monster/product.mk)
# smcn hujianwei 20160926 modify for add monster product control end

# MODIFIED-BEGIN by Wang Xiongke, 2016-09-28,BUG-3005175
#SmartContainer modify begin
PRODUCT_RESTRICT_VENDOR_FILES := false
#change to false if want to totally disable clone function
BOARD_CONFIG_ENABLE_CLONE := true
# MODIFIED-BEGIN by Wang Xiongke, 2016-10-20,BUG-3005175
ifeq ($(BOARD_CONFIG_ENABLE_CLONE), true)
PRODUCT_PROPERTY_OVERRIDES += persist.sys.cmplus.disabled=null
# MODIFIED-END by Wang Xiongke,BUG-3005175
endif
$(call inherit-product-if-exists, vendor/cmx/product/cmx_smartcontainer.mk)
# SmartContainer Modified end
# MODIFIED-END by Wang Xiongke,BUG-3005175

#[FEATURE]-Add-BEGIN by TCTNB.lijiang,For TBR
ifeq ($(FEATURE_TCL_BUG_RECORD),true)
PRODUCT_PACKAGES += \
    jrdrecord
endif
#[FEATURE]-Add-END by TCTNB.lijiang

#GAPP list
PRODUCT_PACKAGES += \
       CellBroadcastReceiver \
       FileManagerCN \
       MeetingAssistantCN \
       MusicCN \
       NoteCN \
       SetupWizardCN \
       SoundRecorderCN \
       TransferCN \
       WeatherCN \
       CameraCN \
       OneTouchFeedback \
       Robust \
       TctMiddleMan

PRODUCT_PACKAGES += \
       SecuritySDKInit \
       ChipManager

#ThirdPart apk list
PRODUCT_PACKAGES += \
       Wrist \
       SogouIME

#[FEATURE]-Add-BEGIN by TCTNB.shishun.liu, FR-3104030
PRODUCT_PACKAGES += \
    MMITest \
    TestMode
#[FEATURE]-Add-END by TCTNB.shishun.liu
#[FEATURE]-Add-BEGIN by TCTNB.Shishun.Liu,2016/10/20, task-3166888
PRODUCT_COPY_FILES += \
    device/tct/$(TARGET_PRODUCT)/ftm_test_config_london:system/etc/ftm_test_config_london
#[FEATURE]-Add-END by TCTNB.Shishun.Liu,2016/10/20, task-3166888

#[FEATURE]-Add-BEGIN by TCTNB.LongNa,2016/10/21, task-3169347
PRODUCT_DEFAULT_PROPERTY_OVERRIDES+= \
    qcom.bluetooth.soc=wcnss
#[FEATURE]-Add-BEGIN by TCTNB.LongNa,2016/10/21, task-3169347

#[FEATURE]-Add-BEGIN by TCTNB.Tianhongwei
PRODUCT_PACKAGES += \
    fingerprintd \
    fingerprint.goodix \
    gxfingerprint.default \
    libfp_client \
    libfpnav \
    libfpservice \
    gx_fpd \
    secsvr

PRODUCT_PACKAGES += FingerprintProvider

#[FEATURE]-Add-END by TCLNB.Tianhongwei

# [FEATURE] Add by minjie.cai@tcl.com 2016-10.25 for Task3059712 begin
 PRODUCT_COPY_FILES += \
     device/tct/$(TARGET_PRODUCT)/waves/libEQGraphCore.so:system/priv-app/MaxxAudio/lib/arm/libEQGraphCore.so \
     device/tct/$(TARGET_PRODUCT)/waves/libEQGraphLib.so:system/priv-app/MaxxAudio/lib/arm/libEQGraphLib.so \
     device/tct/$(TARGET_PRODUCT)/waves/libgnustl_shared.so:system/lib/libgnustl_shared.so \
     device/tct/$(TARGET_PRODUCT)/waves/libAndroidAlgSys.so:system/lib/libAndroidAlgSys.so \
     device/tct/$(TARGET_PRODUCT)/waves/libMAM2-AlgFX-Coretex_A9.so:system/lib/libMAM2-AlgFX-Coretex_A9.so \
     device/tct/$(TARGET_PRODUCT)/waves/libmaxxeffect-cembedded.so:system/lib/soundfx/libmaxxeffect-cembedded.so \
     device/tct/$(TARGET_PRODUCT)/waves/libmaxxeffectwrapper.so:system/priv-app/MaxxService/lib/arm/libmaxxeffectwrapper.so \
     device/tct/$(TARGET_PRODUCT)/waves/libosl-maxxaudio-itf.so:system/priv-app/MaxxService/lib/arm/libosl-maxxaudio-itf.so \
     device/tct/$(TARGET_PRODUCT)/waves/default.mps:system/vendor/etc/default.mps \
     device/tct/$(TARGET_PRODUCT)/waves/maxxsense.db:system/vendor/etc/maxxsense.db
# [FEATURE] Add by minjie.cai@tcl.com 2016-10.25 for Task3059712 end

# [FEATURE] Add by jie.zhang@tcl.com 2016-11.09 for Task3373852 begin
ifneq ($(TARGET_BUILD_VARIANT),user)
PRODUCT_COPY_FILES += \
    development/apps/WLANTestMode/wl_london/wavegenerator.sh:data/wl/wavegenerator.sh \
    development/apps/WLANTestMode/wl_london/rx.sh:data/wl/rx.sh \
    development/apps/WLANTestMode/wl_london/stop.sh:data/wl/stop.sh \
    development/apps/WLANTestMode/wl_london/rxn.sh:data/wl/rxn.sh \
    development/apps/WLANTestMode/wl_london/txstop.sh:data/wl/txstop.sh \
    development/apps/WLANTestMode/wl_london/rxstop.sh:data/wl/rxstop.sh \
    development/apps/WLANTestMode/wl_london/txn.sh:data/wl/txn.sh \
    development/apps/WLANTestMode/wl_london/RxOn.sh:data/wl/RxOn.sh \
    development/apps/WLANTestMode/wl_london/RxReconnect.sh:data/wl/RxReconnect.sh \
    development/apps/WLANTestMode/wl_london/txunmod.sh:data/wl/txunmod.sh \
    development/apps/WLANTestMode/wl_london/txbg_fixed.sh:data/wl/txbg_fixed.sh
endif

PRODUCT_COPY_FILES += \
    device/tct/common/rootdir/etc/init.qcom.wifitesttx_london.sh:system/etc/init.qcom.wifitesttx.sh \
    device/tct/common/rootdir/etc/init.qcom.wifitestmaxpowertx_london.sh:system/etc/init.qcom.wifitestmaxpowertx.sh \
    development/apps/WLANTestMode/wl_london/txbg.sh:data/wl/txbg.sh
# [FEATURE] Add by jie.zhang@tcl.com 2016-11.09 for Task3373852 end

# Task-3438690, build tools for dump to file feature
PRODUCT_PACKAGES += preprd unpackrd

include ./device/tct/common/perso/perso.mk
