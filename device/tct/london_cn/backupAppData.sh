#!/system/bin/sh

finishAll()
{
    sleep 0.5
    exec 7>&-
    exec 8>&-
    rm $1 $2 $3 $4
}

ListFile="/data/.backup.cfg"
PausePath="/data/.backupTrans.cfg"
CfgFile="/data/.backupCfg.cfg"
BusyBoxPath="/system/bin/busybox"
TarTool="$BusyBoxPath tar"
MkfifoTool="$BusyBoxPath mkfifo"

TmpFile="/data/tmpfile"
ProcessFile="/data/processfile"

$MkfifoTool $TmpFile
exec 7<>$TmpFile
chmod 777 $TmpFile

$MkfifoTool $ProcessFile
exec 8<>$ProcessFile
chmod 777 $ProcessFile

tmp=`cat $CfgFile`

dstPath=`echo $tmp |cut -d"," -f1|cut -d"=" -f2`
dstType=`echo $tmp |cut -d"," -f2|cut -d"=" -f2`

echo "=====Start" >&7

if [ x"$dstPath" = "x" ]; then
    echo "=====Exit1" >&7
    finishAll $ListFile $CfgFile $TmpFile $ProcessFile
    exit
fi

if [ x"$dstType" = "x" ]; then
    echo "=====Exit2" >&7
    finishAll $ListFile $CfgFile $TmpFile $ProcessFile
    exit
fi

if [ "$dstType" != "2" ] && [ "$dstType" != "1" ]; then
    echo "=====Exit3" >&7
    finishAll $ListFile $CfgFile $TmpFile $ProcessFile
    exit
fi

mkdir -p "$dstPath"

#Environment Setting
export PATH=/sbin:/system/sbin:/system/bin:/system/xbin:$PATH

while read line
do
    echo "=====Start:$line" >&7
    if [ "$dstType" = "1" ]; then
        $TarTool cvf "$dstPath/$line.tar" /data/user/0/"$line" 2>&7 >&7 && echo SuccessSuccess >&8 || echo FailFail >&8 &
    else
        cp -RLv "$line" "$dstPath" 2>&7 >&7 && echo SuccessSuccess >&8 || echo FailFail >&8 &
    fi

    read a<&8
    if [ x"$a" = x"FailFail" ]; then
        echo "=====Fail,$line" >&7
    else
        echo "=====Success,$line" >&7
    fi

    echo "=====End:$line" >&7

    if [ -e "$PausePath" ]; then
        echo "=====Pause" >&7
        finishAll $ListFile $CfgFile $TmpFile $ProcessFile
        exit
    fi
done < "$ListFile"

echo "=====End" >&7
finishAll $ListFile $CfgFile $TmpFile $ProcessFile
exit
