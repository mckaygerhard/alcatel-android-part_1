#this makefile is defined to build the customized plf file to the target xml file.
#this will be included in the plf's Android.mk
MY_LOCAL_PATH := $(LOCAL_PATH)
LOCAL_PLF_MODULE := $(LOCAL_PACKAGE_NAME)
FINAL_PLF_PATH := $(PRODUCT_OUT)/plf
PLF_SOURCE_FILE := $(FINAL_PLF_PATH)/isdm_$(LOCAL_PLF_MODULE).plf
PLF_PARSE_TOOL := ./device/tct/common/perso/tools/prd2xml
# VERTU - erlei.ding - BUG-2148253
LOCAL_PLF_FILE := $(LOCAL_PATH)/isdm_$(LOCAL_PLF_MODULE).plf
LOCAL_SPLF_FILE := device/tct/$(TARGET_PRODUCT)/perso/plf/$(LOCAL_PATH)/isdm_$(LOCAL_PLF_MODULE).splf
local_mergeplf_tool := "./development/tcttools/mergeplf/mergeplf"
local_make_symbols_tool := ./build/tools/make_java_symbols.sh
# end vertu - BUG-2148253

PLF_TARGET_XML := $(LOCAL_PATH)/res/values/isdm_$(LOCAL_PLF_MODULE)_defaults.xml

ifeq ("$(filter perso,$(MAKECMDGOALS))","perso")
MAKE_PERSO_OR_NOT := 1
else
MAKE_PERSO_OR_NOT := 0
endif

file := $(PLF_TARGET_XML)

$(file) : PRIVATE_PLF_SRC_FILE := $(PLF_SOURCE_FILE)
$(file) : PRIVATE_PLF_TARGET_XML := $(PLF_TARGET_XML)
$(file) : PRIVATE_LOCAL_PLF_MODULE := $(LOCAL_PLF_MODULE)
# VERTU - erlei.ding - BUG-2148253
$(file) : PRIVATE_MY_LOCAL_PATH := $(MY_LOCAL_PATH)
$(file) : PRIVATE_FINAL_PLF_PATH := $(FINAL_PLF_PATH)

# VERTU - erlei.ding - BUG-2409515
ifeq ($(wildcard ${LOCAL_SPLF_FILE}),)
$(file) : $(LOCAL_PLF_FILE)
else
$(file) : $(LOCAL_PLF_FILE) ${LOCAL_SPLF_FILE}
endif
# end vertu - BUG-2409515
	@echo "generated the file: $(PRIVATE_MY_LOCAL_PATH) $(PRIVATE_PLF_SRC_FILE)"
	$(local_mergeplf_tool) $(MAKE_PERSO_OR_NOT) $(PRIVATE_MY_LOCAL_PATH) $(PRIVATE_FINAL_PLF_PATH) $(TARGET_PRODUCT)
	@mkdir -p $(dir $(PRIVATE_PLF_TARGET_XML))
	@python $(PLF_PARSE_TOOL)/writeSdmToXML.py $(PRIVATE_PLF_TARGET_XML) $(PRIVATE_PLF_SRC_FILE)
	@echo "generated the file: $(PRIVATE_PLF_TARGET_XML)"
ifeq ($(LOCAL_PLF_MODULE),framework-res)
	$(local_make_symbols_tool) $(TARGET_PRODUCT)
endif
# end vertu - BUG-2148253

#the LOCAL_PLF_MODULE must be defined the same as app module package name.
ifeq ($(LOCAL_PLF_MODULE),framework-res)
   $(call intermediates-dir-for,APPS,$(LOCAL_PLF_MODULE),,COMMON)/package-export.apk: $(file)
else
   $(call intermediates-dir-for,APPS,$(LOCAL_PLF_MODULE),,COMMON)/src/R.stamp : $(file)
endif

$(file) :=
