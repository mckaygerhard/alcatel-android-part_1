#!/bin/sh
#ANDROID_HOME=/local/works/projects/brandy_gingerbread

# photos include:
# 1)  all files under frameworks/base/core/res/res/drawable*/
# 2)  png,gif under frameworks/base/core/res/assets/
# 3)  all files under frameworks/base/packages/*/res/drawable*/
# 4)  all files under packages/*/res/drawable*/
# 5)  png,gif under packages/*/assets/
# 6)  png,gif under packages/*/res/raw*/
# 7)  all files under bootable/recovery/res/images/
zip -r ./custo_wimdata_ng/wcustores/Photos/images.zip * \
			-i "frameworks/base/core/res/res/drawable*/*" \
			"frameworks/base/core/res/assets/*.png" "frameworks/base/core/res/assets/*.gif" \
			"frameworks/base/core/res/assets/*/*.png" "frameworks/base/core/res/assets/*/*.gif" \
			"frameworks/base/packages/*/res/drawable*/*" \
			"packages/*/res/drawable*/*" \
			"packages/*/assets/*.png" "packages/*/assets/*.gif" \
			"packages/*/assets/*/*.png" "packages/*/assets/*/*.gif" \
			"packages/*/res/raw*/*.png" "packages/*/res/raw*/*.gif" \
			"packages/*/res/raw*/*/*.png" "packages/*/res/raw*/*/*.gif" \
			"bootable/recovery/res/images/*" \
			"development/apps/*/res/drawable*/*" \
			"development/samples/*/res/drawable*/*" \
			-x "frameworks/base/core/res/res/drawable*/Thumbs.db" \
			-x "packages/*/res/drawable*/Thumbs.db" \
			-x "frameworks/base/core/res/res/drawable*/*.xml" \
			-x "frameworks/base/packages/*/res/drawable*/*.xml" \
			-x "packages/*/res/drawable*/*.xml" \
			-x "development/apps/*/res/drawable*/*.xml" \
			-x "development/samples/*/res/drawable*/*xml"
