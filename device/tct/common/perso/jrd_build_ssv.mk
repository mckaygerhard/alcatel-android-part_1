$(eval PLF_DIRS := $$(shell find $$(JRD_WIMDATA)/wprocedures/$$(TARGET_PRODUCT)/ -name 'plf*'))

#$(info PLF_DIRS2=$(PLF_DIRS))
PLF_DIR := $(PRODUCT_OUT)/plf
#$(info PLF_DIR=$(PLF_DIR))
$(eval include $(BUILD_PLF_SSV))
$(foreach PLF_DIR, $(PLF_DIRS), \
    $(eval LOCAL_SSV_VALUE_DIR := $$(shell echo $$(PLF_DIR) | sed -e 's/^.*plf//g' 2>/dev/null)) \
    $(eval LOCAL_SSV_MCCMNC := $$(shell echo $$(PLF_DIR) | sed -e 's/^.*plf//g' 2>/dev/null | sed -e 's/[^0-9]//g' 2>/dev/null)) \
    $(eval include $(BUILD_PLF_SSV)) \
)
