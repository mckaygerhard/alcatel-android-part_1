#this makefile is defined to build the customized plf file to the target xml file.
#this will be included in the plf's Android.mk
MY_LOCAL_PATH := $(LOCAL_PATH)
LOCAL_PLF_MODULE := $(LOCAL_PACKAGE_NAME)
FINAL_PLF_PATH := $(PRODUCT_OUT)/plf
PLF_SOURCE_FILE := $(PLF_DIR)/isdm_$(LOCAL_PLF_MODULE).plf
PLF_PARSE_TOOL := ./device/tct/common/perso/tools/prd2xml
$(info PLF_SOURCE_FILE=$(PLF_SOURCE_FILE))
PLF_TARGET_XML := $(JRD_CUSTOM_RES)/$(LOCAL_PATH)/res/values$(LOCAL_SSV_VALUE_DIR)/isdm_$(LOCAL_PLF_MODULE)_defaults.xml

ifeq ($(IS_INDEPENDENT_APP),true)
PLF_TARGET_XML := $(LOCAL_PATH)/res/values$(LOCAL_SSV_VALUE_DIR)/isdm_$(LOCAL_PLF_MODULE)_defaults.xml
endif

ifeq ("$(filter perso,$(MAKECMDGOALS))","perso")
MAKE_PERSO_OR_NOT := 1
else
MAKE_PERSO_OR_NOT := 0
endif

$(shell mergeplf $(MAKE_PERSO_OR_NOT) $(MY_LOCAL_PATH) $(FINAL_PLF_PATH) $(TARGET_PRODUCT) )

#remove make msm7627a $(LOCAL_PATH)/res/values/isdm_$(LOCAL_PLF_MODULE)_defaults.xml,for make perso to build plf files
ifneq ("$(realpath $(LOCAL_PATH)/res/values$(LOCAL_SSV_VALUE_DIR)/isdm_$(LOCAL_PLF_MODULE)_defaults.xml)", "")
$(shell  rm -r $(LOCAL_PATH)/res/values$(LOCAL_SSV_VALUE_DIR)/isdm_$(LOCAL_PLF_MODULE)_defaults.xml)
endif

# transform the modules'plf to xml
# using prd2xml tools.
# NOTE: This macro should be include in target recipies. To execute in
# make script, use $(shell transform-module-plf-to-xml)
# $(1) The path of the plf file
# $(2) The path of the output file
define transform-module-plf-to-xml
	mkdir -p $(dir $(2))
	python $(PLF_PARSE_TOOL)/writeSdmToXML.py $(2) $(1)
endef

ifneq ("$(realpath $(PLF_SOURCE_FILE))", "")
file := $(PLF_TARGET_XML)

$(file) : PRIVATE_PLF_SRC_FILE := $(PLF_SOURCE_FILE)
$(file) : PRIVATE_PLF_TARGET_XML := $(PLF_TARGET_XML)
$(file) : PRIVATE_LOCAL_PLF_MODULE := $(LOCAL_PLF_MODULE)

$(file) : $(PLF_SOURCE_FILE)
	$(call transform-module-plf-to-xml,$(PRIVATE_PLF_SRC_FILE),$(PRIVATE_PLF_TARGET_XML))
	@echo "generated the file: $(PRIVATE_PLF_TARGET_XML)"

#the LOCAL_PLF_MODULE must be defined the same as app module package name.
ifeq ($(LOCAL_PLF_MODULE),framework-res)
   $(call intermediates-dir-for,APPS,$(LOCAL_PLF_MODULE),,COMMON)/package-export.apk: $(file)
else
   $(call intermediates-dir-for,APPS,$(LOCAL_PLF_MODULE),,COMMON)/src/R.stamp : $(file)
endif

$(file) :=

else
$(info $(PLF_SOURCE_FILE) not exist!)
endif
