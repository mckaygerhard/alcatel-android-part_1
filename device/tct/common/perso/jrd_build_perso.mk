# include the jrd definition methods
include $(JRD_BUILD_PATH)/common/perso/jrd_definitions.mk

# include the jrd resource processor makefiles
include $(JRD_BUILD_PATH)/common/perso/buildres/jrd_build_extra_res.mk
include $(JRD_BUILD_PATH)/common/perso/buildres/jrd_build_medias.mk
include $(JRD_BUILD_PATH)/common/perso/buildres/jrd_build_strings.mk

# don't need any app in MMI Test
ifneq ($(TARGET_BUILD_MMITEST),true)
include $(JRD_BUILD_PATH)/common/perso/buildres/jrd_build_apps.mk
endif #ifeq($(TARGET_BUILD_MMITEST),false)

.PHONY: perso
perso : $(APPS_FLAG) $(LOGO_FLAG) $(AUDIO_FLAG) $(FONTS_FLAG) $(MEDIA_FLAG) $(THEME_FLAG) $(UM_FLAG) $(FILES_FLAG) $(SSVAPKCONFIG_FLAG) $(INDEPENDENT_APP_FLAG) $(JRD_SYS_PROPERTIES_OUTPUT) $(INSTALLED_BUILD_PROP_TARGET) $(STRINGS_FLAG) $(ICONS_FLAG) $(WALLPAPERS_FLAG) $(filter-out perso,$(MAKECMDGOALS))
	$(call build-custpackimage-target)

droid: perso
