#create a system soft link,because the file is moved to the custpack to the inside
define build-custpackimage-target
  $(call pretty,"Target custpack fs image: $(INSTALLED_CUSTPACKIMAGE_TARGET)")
  @mkdir -p $(TARGET_OUT)
endef
#remove the files in system, because we should use the ones in custpack
define remove-files
	@echo "removing system/media/audio/"
	@rm -r $(JRD_PRODUCT_OUT)/system/media/audio
	#@echo "removing system/fonts/"
	#@rm -r $(JRD_PRODUCT_OUT)/system/fonts
	@echo "removing system/etc/apns-conf.xml"
	@rm $(JRD_PRODUCT_OUT)/system/etc/apns-conf.xml
endef
