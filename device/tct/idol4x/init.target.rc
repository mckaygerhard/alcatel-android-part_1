# Copyright (c) 2013-2015, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#

on early-init
    mkdir /firmware 0771 system system
    mkdir /bt_firmware 0771 system system
    symlink /data/tombstones /tombstones
    mkdir /dsp 0771 media media
    mkdir /tctpersist 0771 system system

on fs
    wait /dev/block/bootdevice
    mount_all fstab.qcom
    swapon_all fstab.qcom

    # Keeping following partitions outside fstab file. As user may not have
    # these partition flashed on the device. Failure to mount any partition in fstab file
    # results in failure to launch late-start class.

    wait /dev/block/bootdevice/by-name/cache
    mount ext4 /dev/block/bootdevice/by-name/cache /cache nosuid nodev barrier=1

    wait /dev/block/bootdevice/by-name/persist
    mount ext4 /dev/block/bootdevice/by-name/persist /persist nosuid nodev barrier=1
    mkdir /persist/data 0700 system system
    mkdir /persist/bms 0700 root system
    restorecon_recursive /persist

    wait /dev/block/bootdevice/by-name/dsp
    mount ext4 /dev/block/bootdevice/by-name/dsp /dsp ro nosuid nodev barrier=1

    wait /dev/block/bootdevice/by-name/modem
    mount vfat /dev/block/bootdevice/by-name/modem /firmware ro shortname=lower,uid=1000,gid=1000,dmask=227,fmask=337,context=u:object_r:firmware_file:s0

    wait /dev/block/bootdevice/by-name/bluetooth
    mount vfat /dev/block/bootdevice/by-name/bluetooth /bt_firmware ro shortname=lower,uid=1002,gid=3002,dmask=227,fmask=337,context=u:object_r:bt_firmware_file:s0

on init
    write /sys/module/qpnp_rtc/parameters/poweron_alarm 1
    
    wait /dev/block/bootdevice/by-name/tctpersist
    mount ext4 /dev/block/bootdevice/by-name/tctpersist /tctpersist nosuid nodev barrier=1
    restorecon_recursive /tctpersist
    wait /dev/block/bootdevice/by-name/hdcp
    mount ext4 /dev/block/bootdevice/by-name/hdcp /persist/data nosuid nodev barrier=1

    chown system tctpersist /tctpersist
    chmod 0771 /tctpersist
    chown system system /persist/data
    chmod 0700 /persist/data

on post-fs-data
    mkdir /data/tombstones 0771 system system
    mkdir /tombstones/modem 0771 system system
    mkdir /tombstones/lpass 0771 system system
    mkdir /tombstones/wcnss 0771 system system
    mkdir /tombstones/dsps 0771 system system
    mkdir /persist/data/sfs 0700 system system
    mkdir /persist/data/tz 0700 system system
    mkdir /data/misc/dts 0770 media audio
    mkdir /data/misc/seemp 0700 system system
    mkdir /data/usf 0700 system system
    mkdir /data/misc/tloc/ 0700 system drmrpc
    mkdir /data/misc/qvop 0660 system system
    mkdir /data/misc/qvr 0770 system system
    mkdir /data/misc/audio_pp 0771 media audio
    mkdir /data/wl 0777 system system

#[FEATURE]-Add-BEGIN by TCTNB.93391,11/30/2015,ALM927723,USB Driver Auto Install
on post-fs
    write /sys/class/android_usb/android0/f_mass_storage/lun0/file /system/etc/USBDriver.iso
#[FEATURE]-Add-END by TCTNB.93391,11/30/2015,ALM927723,USB Driver Auto Install

#[FEATURE]-Add-BEGIN by TCTNB.93391,09/01/2016,ALM2655973,Screen Unlock
on property:sys.tct.screenunlock=true
   restorecon_recursive /data/system/magickey
#[FEATURE]-Add-END by TCTNB.93391,09/01/2016,ALM2655973,Screen Unlock

#[PLATFORM]-Remove  by TCTNB.WPL, FR-1839902, 2016/03/18, resolve LCD green screen issue
#on property:init.svc.bootanim=running
#    write /sys/class/leds/wled/brightness 4095

on boot
    start rmt_storage
    start rfs_access
# access permission for secure touch
    chmod 0660 /sys/devices/soc/75ba000.i2c/i2c-12/12-004a/secure_touch_enable
    chmod 0440 /sys/devices/soc/75ba000.i2c/i2c-12/12-004a/secure_touch
    chown system drmrpc /sys/devices/soc/75ba000.i2c/i2c-12/12-004a/secure_touch_enable
    chown system drmrpc /sys/devices/soc/75ba000.i2c/i2c-12/12-004a/secure_touch
    write /proc/sys/kernel/sched_boost 1
    write /sys/devices/soc/75ba000.i2c/i2c-12/12-0020/input/input0/update_fw 1

    chown system audio /sys/class/ftm_spr_res/status
    chown system audio /sys/class/ftm_cal/status
    #Add-BEGIN by TCTNB.YQJ, 2015/11/17,PR-2596975
    chown system system /sys/class/tp_device/tp_gesture/doubleclick_enable
    chown system system /sys/class/tp_device/tp_gesture/gesture_data
    chown system system /sys/class/tp_device/tp_gesture/gesture_enable
    chown system system /sys/class/tp_device/tp_glove/glove_enable
    chown system system /sys/class/tp_device/tp_hover/hover_enable
    chown system system /sys/class/tp_device/tp_sensitivity/cover_enable
    chown system system /sys/class/sensors/virtual-proximity/enable
    chown system system /sys/class/sensors/virtual-proximity/poll_delay
    chown system system /sys/class/tp_device/tp_vr/vr_enable
    chown system system /sys/class/tp_device/tp_debug/tp_log_switch
    #Add-END by TCTNB.YQJ
    #ADD-BEGIN by TCTNB.guangchao.su,2016/08/19,task-LED
    chown system system /sys/class/leds/red/duty_pcts
    chown system system /sys/class/leds/red/ramp_step_ms
    chown system system /sys/class/leds/red/blink
    #ADD-END by TCTNB.guangchao.su

#start camera server as daemon
service qcamerasvr /system/bin/mm-qcamera-daemon
    class late_start
    user camera
    group camera system inet input graphics

#fingerprint service
#Add by TCLNB.XQJ for fingerprint services task 1019320
#synaptics fingerpirnt service
service vfmService /system/bin/vfmService
    class late_start
    user root
    group system

#fingerpirntd
service fingerprintd /system/bin/fingerprintd
    class late_start
    user system
    group system

on post-fs
    mkdir /dev/validity  0775 system system
on post-fs-data
    mkdir /data/validity 0775 system system

service qfp-daemon /system/bin/qfp-daemon
    class late_start
    user system
    group system drmrpc diag

service qvop-daemon /system/bin/qvop-daemon
    class late_start
    user system
    group system drmrpc

service qvrd /system/vendor/bin/qvrservice
    class late_start
    user system
    group system camera graphics
    socket qvrservice stream 0666 system system

#Start up peripheral manager
service per_mgr system/bin/pm-service
    class core
    user system
    group system net_raw
    ioprio rt 4

service per_proxy /system/bin/pm-proxy
    class core
    user system
    group system
    disabled

on property:ro.kernel.nfc.enable=true
    setprop ro.nfc.port "I2C"
    chmod 0660 /dev/pn544
    chown nfc nfc /dev/pn544
    mkdir /data/nfc
    chmod 0777 /data/nfc

on property:init.svc.per_mgr=running
    start per_proxy

on property:sys.shutdown.requested=*
    stop per_proxy

service mdm_launcher /system/bin/sh init.mdm.sh
     class core
     oneshot

service mdm_helper /system/bin/mdm_helper
     class core
     group root system
     disabled
## Allow usb charging to be disabled peristently
#on property:persist.usb.chgdisabled=1
#    write /sys/class/power_supply/battery/charging_enabled 0
#
#on property:persist.usb.chgdisabled=0
#    write /sys/class/power_supply/battery/charging_enabled 1
#
#service qrngd /system/bin/qrngd -f
#   class main
#   user root
#   group root
#
#service qrngp /system/bin/qrngp
#    class main
#    user root
#    group root
#    oneshot
#    disabled
#
#on property:sys.boot_completed=1
#    start qrngp

service qseecomd /system/bin/qseecomd
   class core
   user root
   group root

service gamed /system/vendor/bin/gamed
   class main
   user system
   group system
   disabled
   socket gamed seqpacket 0640 system system

service seempd /system/bin/seempd
   class late_start
   user system
   group system
   socket seempdw dgram 0666 system system

service secotad /system/bin/secotad
   class late_start
   user system
   group system

#service mpdecision /system/bin/mpdecision --avg_comp
#   user root
#   disabled
#
#service qosmgrd /system/bin/qosmgr /system/etc/qosmgr_rules.xml
#   user system
#   group system
#   disabled
#
service thermal-engine /system/vendor/bin/thermal-engine
   class main
   user root
   socket thermal-send-client stream 0666 system system
   socket thermal-recv-client stream 0660 system system
   socket thermal-recv-passive-client stream 0666 system system
   group root
   socket thermal-send-client stream 0666 system system
   socket thermal-recv-client stream 0660 system system
   socket thermal-recv-passive-client stream 0666 system system

#service security-check1 /sbin/security_boot_check system
#    class core
#    oneshot
#
#service security-check2 /sbin/security_boot_check recovery
#    class core
#    oneshot
#
service time_daemon /system/bin/time_daemon
   class late_start
   user root
   group root

service adsprpcd /system/bin/adsprpcd
   class main
   user media
   group media

service audiod /system/bin/audiod
   class late_start
   user system
   group system


service usf_tester /system/bin/usf_tester
    user system
    group system inet
    disabled
    oneshot

service usf_epos /system/bin/usf_epos
    class main
    user system
    group system inet
    disabled
    oneshot

service usf_gesture /system/bin/usf_gesture
    user system
    group system inet
    disabled
    oneshot

service usf_sync_gesture /system/bin/usf_sync_gesture
    user system
    group system inet audio
    disabled
    oneshot

service usf_p2p /system/bin/usf_p2p
    user system
    group system inet
    disabled
    oneshot

service usf_hovering /system/bin/usf_hovering
    user system
    group system inet
    disabled
    oneshot

service usf_proximity /system/bin/usf_proximity
    class late_start
    user system
    group system inet audio

service usf_pairing /system/bin/usf_pairing
    user system
    group system inet
    disabled
    oneshot

service usf_sw_calib /system/bin/usf_sw_calib
    user system
    group system inet
    disabled
    oneshot

service usf-post-boot /system/bin/sh /system/etc/usf_post_boot.sh
    class late_start
    user root
    disabled
    oneshot

on property:init.svc.bootanim=stopped
    start usf-post-boot

service imsqmidaemon /system/bin/imsqmidaemon
    class main
    user system
    socket ims_qmid stream 0660 system radio
    group radio net_raw log diag

service imsdatadaemon /system/bin/imsdatadaemon
    class main
    user system
    socket ims_datad stream 0660 system radio
    group system wifi radio inet net_raw log diag net_admin
    disabled

service pd_mapper /system/vendor/bin/pd-mapper
     class core
     disabled

on property:persist.sys.pd_enable=1
     start pd_mapper
     write /sys/class/service_locator/service_locator_status 1

on property:persist.sys.pd_enable=0
     write /sys/class/service_locator/service_locator_status 0

#
## QCA1530 SoC late_start group trigger
#service gnss-init /system/vendor/bin/gnss.qca1530.sh init
#   class late_start
#   oneshot
#
on property:sys.ims.QMI_DAEMON_STATUS=1
    start imsdatadaemon

service ims_rtp_daemon /system/bin/ims_rtp_daemon
   class main
   user system
   socket ims_rtpd stream 0660 system radio
   group radio net_raw diag inet log
   disabled

service imscmservice /system/bin/imscmservice
   class main
   user system
   group radio net_raw diag diag log
   disabled

on property:sys.ims.DATA_DAEMON_STATUS=1
   start ims_rtp_daemon
   start imscmservice

service dts_configurator /system/bin/dts_configurator
    class late_start
    user system
    group system media audio
    oneshot

service dtseagleservice /system/bin/dts_eagle_service
    class late_start
    user system
    group audio media
    disabled

on property:init.svc.dts_configurator=stopped
    start dtseagleservice

service ppd /system/vendor/bin/mm-pp-dpps
    class late_start
    user system
    group system graphics
    socket pps stream 0660 system system
    disabled

on property:init.svc.surfaceflinger=stopped
    stop ppd

on property:init.svc.surfaceflinger=running
    start ppd

on property:init.svc.surfaceflinger=restarting
    stop ppd

on property:init.svc.zygote=stopped
    stop ppd

on property:init.svc.zygote=running
    start ppd

on property:init.svc.zygote=restarting
    stop ppd

service tlocd /system/bin/tloc_daemon
    class late_start
    user system
    group drmrpc gps net_raw

service energy-awareness /system/bin/energy-awareness
    class main
    user root
    group system
    oneshot

service hvdcp_opti /system/bin/hvdcp_opti
    class main
    user root
    group root

service mdtpd /system/vendor/bin/mdtpd
   class late_start
   user root
   group system radio drmrpc

on charger
    write /sys/devices/system/cpu/cpu2/online 0
    write /sys/devices/system/cpu/cpu3/online 0
    write /sys/module/lpm_levels/parameters/sleep_disabled 0

#Task-2778076, add-BEGIN-by TCTNB.yanwu
on charger
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq 307200
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq 307200
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor powersave
    write /sys/devices/system/cpu/cpu1/online 0
    mount_all charger.fstab.qcom
    start thermal-engine
#Task-2778076, add-END-by TCTNB.yanwu
#Task-2779963, add-BEGIN-by TCTNB.lijiang
    setprop persist.sys.usb.config default,mass_storage,diag,serial_smd,serial_tty,adb
    setprop sys.usb.configfs 0
    mkdir /dev/usb-ffs 0770 shell shell
    mkdir /dev/usb-ffs/adb 0770 shell shell
    mount functionfs adb /dev/usb-ffs/adb uid=2000,gid=2000
    write /sys/class/android_usb/android0/f_ffs/aliases adb
#Task-2779963, add-END-by TCTNB.lijiang

#[FEATURE] ADD-BEGIN by TCTNB.Ruili.Liu 2633323 Wifi SAR issue
on property:wlan.sar.status=2
    exec - root shell -- /system/bin/iwpriv wlan0 setTxMaxPower 12

on property:wlan.sar.status=1
    exec - root shell -- /system/bin/iwpriv wlan0 setTxMaxPower 15

on property:wlan.sar.status=0
    exec - root shell -- /system/bin/iwpriv wlan0 setTxMaxPower 40
#[FEATURE] ADD-END by TCTNB.Ruili.Liu

#[FEATURE] ADD-BEGIN by TCTNB.Fuqiang.Song 2016.08.11 task-2706546 RF Power
on property:sys.boot_completed=1
    exec - root shell -- /system/bin/sar_daemon 1

on property:rf.sar.status=1
    exec - root shell -- /system/bin/sar_daemon 1

on property:rf.sar.status=0
    exec - root shell -- /system/bin/sar_daemon 0
#[FEATURE] ADD-END by TCTNB.Fuqiang.Song

#FR-2754770, TCTNB.huaidi.feng Crash DUMP Flag Management
on property:persist.sys.dload.enable=*
    write /sys/module/msm_poweroff/parameters/download_mode ${persist.sys.dload.enable}

#Start wifi test mode certification services
#Change files permission to write .etc
on property:sys.boot_completed=1
    chmod 0777 /data/wl/rx.sh
    chmod 0777 /data/wl/rxn.sh
    chmod 0777 /data/wl/wavegenerator.sh
    chmod 0777 /data/wl/rxstop.sh
    chmod 0777 /data/wl/txstop.sh
    chmod 0777 /data/wl/txbg.sh
    chmod 0777 /data/wl/txn.sh
    chmod 0777 /data/wl/RxOn.sh
    chmod 0777 /data/wl/stop.sh
    chmod 0777 /data/wl/RxReconnect.sh
    chmod 0777 /data/wl/txunmod.sh
    chmod 0777 /data/wl/wlarm
    chmod 0777 /data/wl

service wifitest /system/bin/sh /system/etc/init.qcom.wifitest.sh
    disabled
    oneshot

service wifimaxpowertx /system/bin/sh /system/etc/init.qcom.wifitestmaxpowertx.sh
    disabled
    oneshot

service wifitesttx /system/bin/sh /system/etc/init.qcom.wifitesttx.sh
    disabled
    oneshot

service wifitestwg /system/bin/sh /system/etc/init.qcom.wifitestwg.sh
    disabled
    oneshot

service wifitesttxn /system/bin/sh /system/etc/init.qcom.wifitesttxn.sh
    disabled
    oneshot

service wifitestRxOn /system/bin/sh /system/etc/init.qcom.wifitestRxOn.sh
    disabled
    oneshot

service RxReconnect /system/bin/sh /system/etc/init.qcom.RxRconnect.sh
    disabled
    oneshot

service wifiUnmodTst /system/bin/sh /system/etc/init.qcom.wifitesttxunmod.sh
    disabled
    oneshot

service RxStop /system/bin/sh /system/etc/init.qcom.wifitestRxStop.sh
    disabled
    oneshot

service txbg_fixed /system/bin/sh /system/etc/init.qcom.wifitesttxbg_fixed.sh
    disabled
    oneshot

service TxStop /system/bin/sh /system/etc/init.qcom.wifitestTxStop.sh
    disabled
    oneshot
