LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk

LOCAL_SHARED_LIBRARIES := \
    liblog \
    libexif \
    libjpeg

LOCAL_SRC_FILES := \
    ExifUtils.cpp

LOCAL_CFLAGS += -fno-short-enums

LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)

LOCAL_MODULE := libcommon_exifutils

include $(BUILD_SHARED_LIBRARY)
