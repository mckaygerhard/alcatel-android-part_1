/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "CallbackDelegate.h"
#include "CapturedFrame.h"

#include <ExifUtils.h>
#include <cutils/log.h>

namespace android {

// TODO: implement still JPEG capturing.
// TODO: implement video recording.

CallbackDelegate::CallbackDelegate() :
        mNotifyCallback(NULL),
        mDataCallback(NULL),
        mDataTimestampCallback(NULL),
        mRequestMemoryCallback(NULL),
        mOpaqueData(NULL),
        mEnabledMessages(0) {}

void CallbackDelegate::reset() {
    HAL_LOG_ENTER();
    mNotifyCallback = NULL;
    mDataCallback = NULL;
    mDataTimestampCallback = NULL;
    mRequestMemoryCallback = NULL;
    mOpaqueData = NULL;
    mEnabledMessages = 0;
}

void CallbackDelegate::setCallbacks(
        camera_notify_callback notify_cb,
        camera_data_callback data_cb,
        camera_data_timestamp_callback data_timestamp_cb,
        camera_request_memory memory_cb,
        void* user) {
    HAL_LOGV("%p, %p, %p, %p (%p)", notify_cb, data_cb, data_timestamp_cb, memory_cb, user);
    mNotifyCallback = notify_cb;
    mDataCallback = data_cb;
    mDataTimestampCallback = data_timestamp_cb;
    mRequestMemoryCallback = memory_cb;
    mOpaqueData = user;
}

void CallbackDelegate::enableMessage(int32_t msg_type) {
    mEnabledMessages |= msg_type;
}

void CallbackDelegate::disableMessage(int32_t msg_type) {
    mEnabledMessages &= ~msg_type;
}

int CallbackDelegate::isMessageEnabled(int32_t msg_type) const {
    return mEnabledMessages.load() & msg_type;
}

void CallbackDelegate::onDeviceNextPreviewFrameAvailable(const CapturedFrame& frame,
                                                         nsecs_t /* timestamp */,
                                                         uint32_t preview_hal_pixel_format) {
    HAL_LOG_ENTER();
    if (isMessageEnabled(CAMERA_MSG_PREVIEW_FRAME)) {
        // TODO: cache and reuse camera_memory_t buffer.
        camera_memory_t* cam_mem;
        size_t output_size = CapturedFrame::getConvertedSize(frame, preview_hal_pixel_format);
        cam_mem = mRequestMemoryCallback(-1, output_size, 1, mOpaqueData);
        if (NULL != cam_mem && NULL != cam_mem->data) {
            CapturedFrame::convert(frame, preview_hal_pixel_format, cam_mem->data, output_size);
            mDataCallback(CAMERA_MSG_PREVIEW_FRAME, cam_mem, 0, NULL, mOpaqueData);
            cam_mem->release(cam_mem);
        } else {
            HAL_LOGE("Memory failure in CAMERA_MSG_PREVIEW_FRAME");
        }
    }
}

void CallbackDelegate::onDeviceNextPictureFrameAvailable(const CapturedFrame& frame) {
    if (isMessageEnabled(CAMERA_MSG_SHUTTER)) {
        mNotifyCallback(CAMERA_MSG_SHUTTER, 0, 0, mOpaqueData);
    }
    if (isMessageEnabled(CAMERA_MSG_RAW_IMAGE_NOTIFY)) {
        mNotifyCallback(CAMERA_MSG_RAW_IMAGE_NOTIFY, 0, 0, mOpaqueData);
    }
    if (isMessageEnabled(CAMERA_MSG_RAW_IMAGE)) {
        mDataCallback(CAMERA_MSG_RAW_IMAGE, 0, 0, NULL, mOpaqueData);
    }
    if (isMessageEnabled(CAMERA_MSG_POSTVIEW_FRAME)) {
        // TODO(shenghao): Implement CAMERA_MSG_POSTVIEW_FRAME.
    }

    if (isMessageEnabled(CAMERA_MSG_COMPRESSED_IMAGE)) {
        if (frame.fourcc == V4L2_PIX_FMT_MJPEG) {
            onMjpegPictureTaken(frame);
        } else {
            HAL_LOGE("Compressed frame only supports and MJPEG source format. "
               "FOURCC 0x%x is unsupported", frame.fourcc);
        }
    }
}

void CallbackDelegate::onMjpegPictureTaken(const CapturedFrame& frame) {
    ExifUtils utils;
    utils.initialize(frame.buffer, frame.dataSize);
    // TODO(shenghao): Set other Exif tags.
    utils.setImageWidth(frame.width);
    utils.setImageLength(frame.height);
    camera_memory_t* buffer = allocateCameraMemory(utils.getOutputJpegLength());
    if (NULL != buffer) {
        utils.writeData(static_cast<uint8_t*>(buffer->data));
        mDataCallback(CAMERA_MSG_COMPRESSED_IMAGE, buffer, 0, NULL, mOpaqueData);
        buffer->release(buffer);
    }
}

camera_memory_t* CallbackDelegate::allocateCameraMemory(size_t size) {
    camera_memory_t* buffer = mRequestMemoryCallback(-1, size, 1, mOpaqueData);
    if (NULL != buffer) {
        if (NULL != buffer->data) {
            return buffer;
        }
        buffer->release(buffer);
    }
    HAL_LOGE("Memory allocation of size %zu failed.", size);
    return NULL;
}

void CallbackDelegate::onDeviceError(int error) {
    HAL_LOGV("error = %d", error);
    if (isMessageEnabled(CAMERA_MSG_ERROR)) {
        mNotifyCallback(CAMERA_MSG_ERROR, error, 0, mOpaqueData);
    }
}

}  // namespace android
