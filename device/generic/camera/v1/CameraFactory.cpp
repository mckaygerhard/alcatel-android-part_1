/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "CameraFactory.h"

#include "CameraClient.h"
#include "CameraDevice.h"
#include "CameraHal.h"
#include "CameraProperties.h"

#include <cutils/log.h>

namespace android {

ANDROID_SINGLETON_STATIC_INSTANCE( CameraFactory )

CameraFactory::CameraFactory() :
        mCameras(),
        mCallbacks(NULL) {
    HAL_LOG_ENTER();

    if (mPropertyManager.hasBackCamera() &&
        !createCamera(mPropertyManager.getBackProperties())) {
        HAL_LOGE("can't create back camera.");
    }

    if (mPropertyManager.hasFrontCamera() &&
        !createCamera(mPropertyManager.getFrontProperties())) {
        HAL_LOGE("can't create front camera.");
    }

    HAL_LOGV("number of cameras is %d", getNumberOfCameras());
}

CameraFactory::~CameraFactory() {
    HAL_LOG_ENTER();
    mCameras.clear();
}

bool CameraFactory::createCamera(const CameraProperties& properties) {
    HAL_LOG_ENTER();

    const int camera_id = mCameras.size();
    sp<CameraClient> camera = new CameraClient(properties, &HAL_MODULE_INFO_SYM.common);
    HAL_LOGV("new camera object %d", camera_id);
    mCameras.push_back(camera);
    mProperties.push_back(properties);
    return true;
}

int CameraFactory::deviceOpen(int camera_id, hw_device_t** hw_device) {
    *hw_device = NULL;
    if (camera_id < 0 || camera_id >= getNumberOfCameras()) {
        HAL_LOGE("camera id %d is out of bounds [0,%d]", camera_id, getNumberOfCameras() - 1);
        return -EINVAL;
    }
    CameraDevice *camera_device = new CameraDevice(mProperties[camera_id]);
    return mCameras[camera_id]->deviceOpen(camera_device, hw_device);
}

int CameraFactory::getCameraInfo(int camera_id, struct camera_info* info) {
    HAL_LOGV("id = %d", camera_id);
    if (camera_id < 0 || camera_id >= getNumberOfCameras()) {
        HAL_LOGE("camera id %d is out of bounds [0,%d]", camera_id, getNumberOfCameras() - 1);
        return -EINVAL;
    }
    memset(info, 0, sizeof(*info));

    info->device_version = HARDWARE_DEVICE_API_VERSION(1, 0);

    info->facing = mProperties[camera_id].facing;
    info->orientation = mProperties[camera_id].orientation;
    HAL_LOGV("facing %d, orientation %d", info->facing, info->orientation);
    return 0;
}

int CameraFactory::setCallbacks(const camera_module_callbacks_t* callbacks) {
    HAL_LOGV("new callbacks = %p", callbacks);
    mCallbacks = callbacks;
    return OK;
}

}  // namespace android
