/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_CAMERA_BASE_H
#define ANDROID_DEVICE_CAMERA_CAMERA_BASE_H

#include "CallbackDelegate.h"
#include "CameraDevice.h"
#include "CameraProperties.h"
#include "PreviewStream.h"

#include <camera/CameraParameters.h>
#include <hardware/camera.h>
#include <UniquePtr.h>
#include <utils/Mutex.h>
#include <utils/RefBase.h>
#include <utils/Timers.h>

namespace android {

/*
 * This class is the main controller of camera logic, which interfaces camera device and camera
 * framework calls to HAL. This class is thread-safe. It assures thread-safety between these
 * threads:
 *
 * 1. Binder thread - called from camera framework, including callbacks of
 *    - hw_module_methods_t
 *    - camera_module_t
 *    - camera_device_ops_t
 *    - camera_device.common.close()
 *
 * 2. Worker thread - called from CameraDevice's subclass.
 *    - Overrides of CameraDeviceClientBase.
 *
 * So the consistency of internal states (CameraClient, PreviewStream, CallbackDelegate,
 * CameraDevice) are guaranteed.
 */
class CameraClient : public CameraDeviceClientBase {
public:
    CameraClient(const CameraProperties& properties, struct hw_module_t* module);
    ~CameraClient();

    // Callbacks for hw_module_methods_t:
    // CameraClient takes the ownership of |camera_device|.
    // Connects camera device and return the associated hw_device_t in |hw_device|.
    int deviceOpen(CameraDevice* camera_device, hw_device_t** hw_device);

    // Callbacks for camera_device_ops_t:
    int setPreviewWindow(struct preview_stream_ops *window);
    void setCallbacks(
            camera_notify_callback notify_cb, camera_data_callback data_cb,
            camera_data_timestamp_callback data_cb_timestamp,
            camera_request_memory get_memory, void* user);
    void enableMsgType(int32_t msg_type);
    void disableMsgType(int32_t msg_type);
    int msgTypeEnabled(int32_t msg_type) const;
    int startPreview();
    void stopPreview();
    int previewEnabled() const;
    int storeMetaDataInBuffers(int enable);
    int startRecording();
    void stopRecording();
    int recordingEnabled() const;
    void releaseRecordingFrame(const void* opaque);
    int autoFocus();
    int cancelAutoFocus();
    int takePicture();
    int cancelPicture();
    int setParameters(const char* parms);
    char* getParameters();
    void putParameters(char* params);
    int sendCommand(int32_t cmd, int32_t arg1, int32_t arg2);
    void release();
    int dump(int /* fd */);

    // CameraDeviceClientBase overrides:
    void onDeviceNextFrameAvailable(CameraDevice* device, const CapturedFrame& frame,
                                    nsecs_t timestamp) override;
    void onDeviceError(CameraDevice* device, int err) override;

    // Callback for camera_device.common.close().
    static int closeHwDevice(struct hw_device_t* hw_device);

private:
    // Return the associated CameraDevice object.
    CameraDevice* getDevice() {
        return mCameraDevice.get();
    }

    // Initialize parameters and data without acquiring lock.
    // Return true on success; otherwise, return false.
    bool initialize_locked();

    // Release the hardware resources owned by this object without acquiring lock.
    int release_locked();

    // Stop preview without acquiring lock.
    void stopPreview_locked();

    // Return the preview pixel format defined as HAL_PIXEL_FORMAT_XXX in
    // /system/core/include/system/graphics.h. Return 0 on error.
    uint32_t getPreviewPixelFormat_locked();

    // Try lock from callback in CameraDeviceClientBase.
    // Return true if acquiring lock successfully; otherwise, false.
    bool tryLockFromDevice(CameraDevice* device) const;

    // Make public methods thread-safe.
    mutable Mutex mLock;

    camera_device mHwDevice;

    UniquePtr<CameraDevice> mCameraDevice;

    CameraProperties mProperties;

    CameraParameters mParams;

    // The formats used to report to apps.
    SupportedFormats mQualifiedFormats;

    // High-level flag to indicate if the preview function is enabled.
    // It is equivalent to camera_device_ops_t.preview_enabled().
    bool mPreviewEnabled;

    /*
     * This state controls whether or not to notify the framework about compressed
     * image, shutter, and other picture related events.
     */
    bool mTakingPicture;

    UniquePtr<PreviewStream> mPreviewStream;
    UniquePtr<CallbackDelegate> mCallbackDelegate;
};

}  // namespace android

#endif
