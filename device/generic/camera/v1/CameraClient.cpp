/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "CameraClient.h"

#include "CallbackDelegate.h"
#include "CameraDevice.h"
#include "CameraHalDeviceOps.h"
#include "CameraProperties.h"
#include "CapturedFrame.h"
#include "PreviewStream.h"

#include <camera/CameraParameters.h>
#include <cutils/log.h>
#include <hardware/camera_common.h>
#include <ui/Rect.h>
#include <utils/String8.h>
#include <utils/Vector.h>

#include <cmath>
#include <linux/videodev2.h>

namespace android {

// Capture frame rate of the camera (in FPS). We only use constant frame rate 30
// because preview and still capture are fast. Also it matches the number in
// camera parameters we report to frameworks.
const float kFrameRate = 30.0;

// Return corresponding format by matching resolution |width|x|height| in |formats|.
static const SupportedFormat *findFormatByResolution(const SupportedFormats& formats,
                                                     int width, int height) {
    for (const auto& format : formats) {
        if (format.width == width && format.height == height) {
            return &format;
        }
    }
    return NULL;
}

// Find the largest frame size.
static SupportedFormat getMaximumFormat(const SupportedFormats& supported_formats) {
    SupportedFormat max_format;
    memset(&max_format, 0, sizeof(max_format));
    for (const auto& supported_format : supported_formats) {
        if (supported_format.width >= max_format.width &&
                supported_format.height >= max_format.height) {
            max_format = supported_format;
        }
    }
    return max_format;
}

// Find the all formats which supports 30 frames per second.
// The resolutions in returned SupportedFormats vector are unique.
static SupportedFormats getQualifiedFormats(const SupportedFormats& supported_formats) {
    // The preference of supported fourccs in the list is from high to low.
    const std::vector<uint32_t> supported_fourccs = CapturedFrame::getSupportedFourCCs();
    SupportedFormats qualified_formats;
    for (const auto& supported_fourcc : supported_fourccs) {
        for (const auto& supported_format : supported_formats) {
            if (supported_format.fourcc != supported_fourcc) {
                continue;
            }
            // Skip if |qualified_formats| already has the same resolution with a more preferred
            // fourcc.
            if (findFormatByResolution(qualified_formats, supported_format.width,
                                       supported_format.height) != NULL) {
                continue;
            }

            // Check whether the format supports 30 frames per second or not.
            for (const auto& frame_rate : supported_format.frameRates) {
                if (std::fabs(frame_rate - kFrameRate) <= std::numeric_limits<float>::epsilon()) {
                    qualified_formats.push_back(supported_format);
                    break;
                }
            }
        }
    }
    return qualified_formats;
}

// Put all supported resolutions into one string with "w1xh1,w2xh2" format.
static String8 getSizesString(const SupportedFormats& supported_formats) {
    String8 size_string;
    for (const auto& supported_format : supported_formats) {
        String8 one_size = String8::format("%s%dx%d", size_string.size() ? "," : "",
                supported_format.width, supported_format.height);
        size_string.append(one_size);
    }
    return size_string;
}

CameraClient::CameraClient(const CameraProperties& properties,
                           struct hw_module_t* module) :
        mProperties(properties),
        mParams(),
        mPreviewEnabled(false),
        mPreviewStream(new PreviewStream()),
        mCallbackDelegate(new CallbackDelegate()),
        mTakingPicture(false) {
    // Configure mHwDevice.
    memset(&mHwDevice, 0, sizeof(mHwDevice));

    struct hw_device_t* hw_device = &mHwDevice.common;
    hw_device->tag = HARDWARE_DEVICE_TAG;
    hw_device->version = HARDWARE_DEVICE_API_VERSION(1, 0);
    hw_device->module = module;
    hw_device->close = CameraClient::closeHwDevice;

    // v1.0 specific fields.
    mHwDevice.ops = &g_camera_device_ops;
    mHwDevice.priv = this;
}

CameraClient::~CameraClient() {
}

bool CameraClient::initialize_locked() {
    HAL_LOG_ENTER();

    // Set picture and preview size.
    SupportedFormats supported_formats;
    supported_formats = getDevice()->getDeviceSupportedFormats();
    if (supported_formats.empty()) {
        HAL_LOGE("Failed to get supported formats");
        return false;
    }

    mQualifiedFormats = getQualifiedFormats(supported_formats);
    if (mQualifiedFormats.empty()) {
        HAL_LOGE("Failed to find qualified format");
        return false;
    }

    String8 qualified_sizes = getSizesString(mQualifiedFormats);
    HAL_LOGV("Supported picture/preview sizes %s", qualified_sizes.string());
    mParams.set(CameraParameters::KEY_SUPPORTED_PICTURE_SIZES, qualified_sizes.string());
    mParams.set(CameraParameters::KEY_SUPPORTED_PREVIEW_SIZES, qualified_sizes.string());

    SupportedFormat maximum_format = getMaximumFormat(mQualifiedFormats);
    mParams.setPictureSize(maximum_format.width, maximum_format.height);
    mParams.setPreviewSize(maximum_format.width, maximum_format.height);

    // Dummy required parameters.
    mParams.set(CameraParameters::KEY_SUPPORTED_JPEG_THUMBNAIL_SIZES,
                "320x240,0x0");
    mParams.set(CameraParameters::KEY_JPEG_THUMBNAIL_WIDTH, "320");
    mParams.set(CameraParameters::KEY_JPEG_THUMBNAIL_HEIGHT, "240");
    mParams.set(CameraParameters::KEY_JPEG_THUMBNAIL_QUALITY, "90");
    mParams.set(CameraParameters::KEY_JPEG_QUALITY, "90");
    // TODO: Get these values from property because driver cannot provide it.
    mParams.set(CameraParameters::KEY_FOCAL_LENGTH, "4.31");
    mParams.set(CameraParameters::KEY_HORIZONTAL_VIEW_ANGLE, "54.8");
    mParams.set(CameraParameters::KEY_VERTICAL_VIEW_ANGLE, "42.5");

    // Supported preview formats:
    //
    // For Camera1 API (android.hardware.Camera), the device implementation must provide NV21
    // (YCbCr_420_SP) data as the default preview format. The device implementation must also
    // support YV12 for camera previews. (The camera may use any native pixel format, but the
    // device implementation must support conversion to YV12.)
    //
    // Camera2 API has different requirements for pixel formats, but it's unsupported by HALv1.
    char supported_preview_formats[64];
    snprintf(supported_preview_formats, sizeof(supported_preview_formats), "%s,%s",
             CameraParameters::PIXEL_FORMAT_YUV420SP,  // NV21
             CameraParameters::PIXEL_FORMAT_YUV420P);  // YV12
    mParams.set(CameraParameters::KEY_SUPPORTED_PREVIEW_FORMATS,
                supported_preview_formats);
    mParams.setPreviewFormat(CameraParameters::PIXEL_FORMAT_YUV420SP);

    // Preview frame rate
    mParams.set(CameraParameters::KEY_SUPPORTED_PREVIEW_FPS_RANGE,
            String8::format("(%.0f,%.0f)", kFrameRate, kFrameRate));
    mParams.set(CameraParameters::KEY_PREVIEW_FPS_RANGE,
            String8::format("%.0f,%.0f", kFrameRate, kFrameRate));
    mParams.set(CameraParameters::KEY_SUPPORTED_PREVIEW_FRAME_RATES,
            String8::format("%.0f", kFrameRate));
    mParams.setPreviewFrameRate(kFrameRate);

    // Video format
    mParams.set(CameraParameters::KEY_VIDEO_FRAME_FORMAT,
                CameraParameters::PIXEL_FORMAT_YUV420SP);

    // Picture format
    mParams.set(CameraParameters::KEY_SUPPORTED_PICTURE_FORMATS,
                CameraParameters::PIXEL_FORMAT_JPEG);
    mParams.setPictureFormat(CameraParameters::PIXEL_FORMAT_JPEG);

    // TODO: Check V4L2 support of EV control.
    // Set exposure compensation.
    mParams.set(CameraParameters::KEY_MAX_EXPOSURE_COMPENSATION, "0");
    mParams.set(CameraParameters::KEY_MIN_EXPOSURE_COMPENSATION, "0");
    mParams.set(CameraParameters::KEY_EXPOSURE_COMPENSATION_STEP, "0");
    mParams.set(CameraParameters::KEY_EXPOSURE_COMPENSATION, "0");

    // Sets the white balance modes and the device-dependent scale factors.
    mParams.set(CameraParameters::KEY_SUPPORTED_WHITE_BALANCE,
                CameraParameters::WHITE_BALANCE_AUTO);
    mParams.set(CameraParameters::KEY_WHITE_BALANCE, CameraParameters::WHITE_BALANCE_AUTO);

    // No support of AF.
    mParams.set(CameraParameters::KEY_SUPPORTED_FOCUS_MODES, CameraParameters::FOCUS_MODE_FIXED);
    mParams.set(CameraParameters::KEY_FOCUS_MODE, CameraParameters::FOCUS_MODE_FIXED);

    return true;
}

int CameraClient::deviceOpen(CameraDevice* camera_device, hw_device_t** hw_device) {
    Mutex::Autolock lock(&mLock);
    HAL_LOG_ENTER();

    mCameraDevice.reset(camera_device);
    mCameraDevice->setClient(this);
    if (!initialize_locked()) {
        HAL_LOGE("Unable to initialize camera");
        mCameraDevice.reset();
        return -EIO;
    }

    CameraDevice* device = getDevice();
    int res = device->connect();
    if (res == NO_ERROR) {
        *hw_device = &mHwDevice.common;
    } else {
        HAL_LOGE("Device connect failed %d", res);
        mCameraDevice.reset();
    }
    return res;
}

int CameraClient::closeHwDevice(struct hw_device_t* hw_device) {
    HAL_LOG_ENTER();
    CameraClient* client = reinterpret_cast<CameraClient*>(
            reinterpret_cast<struct camera_device*>(hw_device)->priv);
    if (!client) {
        HAL_LOGE("camera client is NULL");
        return -EIO;
    }
    Mutex::Autolock lock(&client->mLock);
    return client->release_locked();
}

void CameraClient::setCallbacks(
        camera_notify_callback notify_cb, camera_data_callback data_cb,
        camera_data_timestamp_callback data_cb_timestamp,
        camera_request_memory get_memory, void* user) {
    Mutex::Autolock lock(&mLock);
    mCallbackDelegate->setCallbacks(notify_cb, data_cb, data_cb_timestamp, get_memory, user);
}

int CameraClient::setPreviewWindow(struct preview_stream_ops *window) {
    Mutex::Autolock lock(&mLock);
    return mPreviewStream->setPreviewWindow(window);
}

void CameraClient::enableMsgType(int32_t msg_type) {
    // Lock is not needed here because the synchronization mechanism is handled
    // in CallbackDelegate level.
    mCallbackDelegate->enableMessage(msg_type);
}

void CameraClient::disableMsgType(int32_t msg_type) {
    // Lock is not needed here because the synchronization mechanism is handled
    // in CallbackDelegate level.
    mCallbackDelegate->disableMessage(msg_type);
}

int CameraClient::msgTypeEnabled(int32_t msg_type) const {
    // Lock is not needed here because the synchronization mechanism is handled
    // in CallbackDelegate level.
    return mCallbackDelegate->isMessageEnabled(msg_type);
}

int CameraClient::startPreview() {
    Mutex::Autolock lock(&mLock);

    int width, height;
    mParams.getPreviewSize(&width, &height);

    // Try to find a corresponding resolution.
    const SupportedFormat *format = findFormatByResolution(mQualifiedFormats, width, height);
    if (format == NULL) {
        HAL_LOGE("Cannot support preview size: %dx%d.", width, height);
        return -EINVAL;
    }
    HAL_LOGD("Starting preview: %dx%d with 0x%X format.", width, height, format->fourcc);

    // Connect device and start streaming.
    CameraDevice* device = getDevice();
    // We need to stop preview here because the stream for image capture may
    // still be on.
    stopPreview_locked();
    int res;
    if ((res = device->streamOn(width, height, kFrameRate, format->fourcc)) != 0) {
        return res;
    }
    if ((res = device->startDeliveringFrames()) != 0) {
        device->streamOff();
        // If startPreview() fails, it can be connected but streaming should be stopped.
        return res;
    }

    mPreviewEnabled = true;
    return 0;
}

void CameraClient::stopPreview() {
    Mutex::Autolock lock(&mLock);
    stopPreview_locked();
}

void CameraClient::stopPreview_locked() {
    CameraDevice* device = getDevice();
    device->stopDeliveringFrames();
    device->streamOff();
    mPreviewEnabled = false;
    mTakingPicture = false;
}

int CameraClient::previewEnabled() const {
    Mutex::Autolock lock(&mLock);
    return mPreviewEnabled;
}

int CameraClient::storeMetaDataInBuffers(int /* enable */) {
    Mutex::Autolock lock(&mLock);
    HAL_LOGW("not implemented");
    return 0;
}

int CameraClient::startRecording() {
    return -EIO; // video encoding not implemented.
}

void CameraClient::stopRecording() {
    // TODO: video recording to be implemented.
}

int CameraClient::recordingEnabled() const {
    return false;
}

void CameraClient::releaseRecordingFrame(const void* /* opaque */) {
    // TODO: implement along with video recording.
}

int CameraClient::autoFocus() {
    // Apps can still call autoFocus() if the camera only supports fixed focus for backward
    // compatibility.
    HAL_LOGW("not implemented");
    return 0;
}

int CameraClient::cancelAutoFocus() {
    // Apps can still call cancelAutoFocus() if the camera only supports fixed focus for backward
    // compatibility.
    HAL_LOGW("not implemented");
    return 0;
}

int CameraClient::cancelPicture() {
    HAL_LOGW("not implemented");
    return 0;
}

int CameraClient::sendCommand(int32_t cmd, int32_t arg1, int32_t arg2) {
    HAL_LOGW("not implemented. cmd = %d, arg1 = %d, arg2 = %d", cmd, arg1, arg2);
    return 0;
}

void CameraClient::release() {
    Mutex::Autolock lock(&mLock);
    release_locked();
}

int CameraClient::release_locked() {
    int res = 0;

    // Stop and disconnect the camera device.
    CameraDevice* device = getDevice();
    if (device) {
        stopPreview_locked();
        if ((res = device->disconnect()) != 0) {
            HAL_LOGE("fails to disconnect camera device - %d", res);
        }
    }
    mCallbackDelegate->reset();
    mCameraDevice.reset();
    return res;
}

int CameraClient::dump(int /* fd */) {
    HAL_LOGW("not implemented");
    return -EIO;
}

int CameraClient::setParameters(const char* parms) {
    Mutex::Autolock lock(&mLock);
    HAL_LOGV("called with %s", parms);

    mParams.unflatten(String8(parms));

    return 0;
}

char* CameraClient::getParameters() {
    Mutex::Autolock lock(&mLock);
    HAL_LOG_ENTER();
    String8 params(mParams.flatten());
    size_t str_len = params.bytes();
    char* params_str = new char[str_len + 1];
    strncpy(params_str, params.string(), str_len);
    params_str[str_len] = '\0';
    return params_str;
}

void CameraClient::putParameters(char* params_str) {
    Mutex::Autolock lock(&mLock);
    HAL_LOG_ENTER();
    delete[] params_str;
}

int CameraClient::takePicture() {
    Mutex::Autolock lock(&mLock);
    HAL_LOG_ENTER();

    if (mTakingPicture) {
        HAL_LOGE("Enter takePicture() while another picture is being taken.");
        return -EINVAL;
    }

    int width, height;
    mParams.getPictureSize(&width, &height);

    // Find corresponded format given width and height.
    const SupportedFormat *format = findFormatByResolution(mQualifiedFormats, width, height);
    if (format == NULL) {
        HAL_LOGE("Cannot support picture size: %dx%d.", width, height);
        return -EINVAL;
    }

    // Stop preview before taking picture.
    stopPreview_locked();

    HAL_LOGV("Starting camera streaming for picture: [%dx%d]", width, height);
    CameraDevice* device = getDevice();
    int res = device->streamOn(width, height, kFrameRate, format->fourcc);
    if (res) {
        HAL_LOGE("streamOn failed: %d.", res);
        return res;
    }

    res = device->startDeliveringFrames();
    if (res) {
        HAL_LOGE("startDeliveringFrames failed: %d. Stop capturing picture.", res);
        device->streamOff();
    }
    mTakingPicture = true;
    return res;
}

void CameraClient::onDeviceNextFrameAvailable(CameraDevice* device, const CapturedFrame& frame,
                                              nsecs_t timestamp) {
    if (!tryLockFromDevice(device)) {
        return;
    }

    if (device == getDevice()) {
        if (mPreviewEnabled || mTakingPicture) {
            mPreviewStream->onDeviceNextFrameAvailable(frame, timestamp);
        }
        if (mPreviewEnabled) {
            uint32_t preview_hal_pixel_format = getPreviewPixelFormat_locked();
            if (preview_hal_pixel_format) {
                mCallbackDelegate->onDeviceNextPreviewFrameAvailable(frame, timestamp,
                                                                     preview_hal_pixel_format);
            }
        }
        if (mTakingPicture) {
            mCallbackDelegate->onDeviceNextPictureFrameAvailable(frame);
            mTakingPicture = false;
        }
    } else {
        HAL_LOGE("Invalid device");
    }

    mLock.unlock();
};

void CameraClient::onDeviceError(CameraDevice* device, int err) {
    if (!tryLockFromDevice(device)) {
        return;
    }

    if (device == getDevice()) {
        mCallbackDelegate->onDeviceError(err);
    } else {
        HAL_LOGE("Invalid device");
    }

    mLock.unlock();
}

#define LOCK_SLEEP_INTERVAL 1 // 1ms
bool CameraClient::tryLockFromDevice(CameraDevice* device) const {
    // It will check if the worker thread from CameraDevice is being terminated to avoid a
    // potential deadlock. Example:
    //
    // Binder thread => camera framework => .. => CameraClient => (acquired mLock) => .. =>
    //         CameraDeviceClientBase::stopDeliveringFrames() => .. =>
    //         (blockingly wait) WorkerThread::stop()
    //
    // Worker thread => WorkerThreadClient::threadLoop() => .. =>
    //         CameraClient::onDeviceNextFrameAvailable() => (try to acquire mLock)
    //
    // It is a deadlock because binder thread can not terminate the worker thread, and worker
    // thread can not acquire CameraClient::mLock.
    for (;;) {
        if (device->isStoppingDeliveryingFrames()) {
            HAL_LOGV("Abort because thread is terminating");
            return false;
        }
        if (mLock.tryLock() == NO_ERROR) {
            return true;
        }
        usleep(LOCK_SLEEP_INTERVAL * 1000);
    }
}

uint32_t CameraClient::getPreviewPixelFormat_locked() {
    const char* format_text = mParams.getPreviewFormat();
    if (strcmp(format_text, CameraParameters::PIXEL_FORMAT_YUV420P) == 0) {
        return HAL_PIXEL_FORMAT_YV12;
    } else if (strcmp(format_text, CameraParameters::PIXEL_FORMAT_YUV420SP) == 0) {
        return HAL_PIXEL_FORMAT_YCrCb_420_SP;
    } else {
        HAL_LOGE("Unsupported format %s", format_text);
        return 0;
    }
}

}  // namespace android
