/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_CAMERA_DEVICE_H
#define ANDROID_DEVICE_CAMERA_CAMERA_DEVICE_H

#include "CommonTypes.h"
#include "WorkerThread.h"

#include <hardware/camera.h>
#include <system/graphics.h>
#include <UniquePtr.h>
#include <utils/KeyedVector.h>
#include <utils/String8.h>

#include <string>

namespace android {

class CameraDevice;
class CameraDeviceDelegate;
class CameraProperties;
struct CapturedFrame;

// Client of CameraDevice.
class CameraDeviceClientBase : public RefBase {
public:
    // |timestamp| is the arrival time of |frame| from device.  Timestamps are measured in
    // nanoseconds, and must be comparable and monotonically increasing between two frames in the
    // same preview stream. They do not need to be comparable between consecutive or parallel
    // preview streams, cameras, or app runs.
    virtual void onDeviceNextFrameAvailable(CameraDevice* device, const CapturedFrame& frame,
                                            nsecs_t timestamp) = 0;
    virtual void onDeviceError(CameraDevice* device, int err)  = 0;
};

// Camera device-level operations. The class provides WorkerThreadClient interface, and its
// subclass should implement device-specific polling by overriding
// WorkerThreadClient::threadLoop().
class CameraDevice : public WorkerThreadClient {
public:
    CameraDevice(const CameraProperties& properties);
    ~CameraDevice() override;
    void setClient(CameraDeviceClientBase* client);

    // Lifecycle controls:
    int connect();
    int disconnect();

    // Start streaming. |fps| is the target frame rate (frames per second).
    int streamOn(int width, int height, float fps, uint32_t pixel_format);

    // Stop streaming.
    // Remarks: call stopDeliveringFrames() before streamOff().
    int streamOff();

    // Query device capabilities:
    const SupportedFormats getDeviceSupportedFormats();

    // Frame delivery controls:
    // startDeliveringFrames() must be called after streaming is started.
    // The frames will be delivered to CameraDeviceClientBase::onDeviceNextFrameAvailable().
    // TODO: merge frame delivery controls with streamOn()/streamOff() to simplify the logic.
    int startDeliveringFrames();
    int stopDeliveringFrames();
    bool isStoppingDeliveryingFrames() const;

    // WorkerThreadClient implementation. This runs in WorkerThread.
    bool threadLoop() override;

protected:
    const CameraProperties& mProperties;
    wp<CameraDeviceClientBase> mCameraClient;

private:
    // Memory unmap buffers and close all file descriptors.
    void releaseBuffers();

    sp<WorkerThread> mWorkerThread;

    // Whether the worker thread is being stopped.
    std::atomic<bool> mStoppingWorkerThread;

    UniquePtr<CameraDeviceDelegate> mCameraDelegate;

    // The camera device path. For example, /dev/video2.
    std::string mDevicePath;

    // Memory mapped buffers which are shared from CameraDeviceDelegate.
    std::vector<CapturedFrame> mBuffers;

    // The size allocated for each buffer.
    uint32_t mBufferSize;

    // File descriptors of frame buffers. CameraDevice should close them when done.
    std::vector<int> mFds;

    DISALLOW_COPY_AND_ASSIGN(CameraDevice);
};

}  // namespace android

#endif
