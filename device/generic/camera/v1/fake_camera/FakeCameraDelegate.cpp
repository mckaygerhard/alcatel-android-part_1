/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "FakeCameraDelegate.h"

#include <errno.h>
#include <linux/videodev2.h>

namespace android {

FakeCameraDelegate::FakeCameraDelegate() {}

FakeCameraDelegate::~FakeCameraDelegate() {}

int FakeCameraDelegate::Connect(const std::string& /* device_path */) {
    return -EIO;
}

void FakeCameraDelegate::Disconnect() {
}

int FakeCameraDelegate::StreamOn(
        int /* width */, int /* height */, uint32_t /* fourcc */, float /* frame_rate */,
        std::vector<int>* /* fds */, uint32_t* /* buffer_size */) {
    return -EIO;
}

int FakeCameraDelegate::StreamOff() {
    return 0;
}

int FakeCameraDelegate::GetNextFrameBuffer(uint32_t* /* buffer_id */, uint32_t* /* data_size */ ) {
    return -EIO;
}

int FakeCameraDelegate::ReuseFrameBuffer(uint32_t /* buffer_id */) {
    return 0;
}

const SupportedFormats FakeCameraDelegate::GetDeviceSupportedFormats(
        const std::string& /* device_path */) {
    // Emulate a VGA camera.
    SupportedFormats supported_formats;
    SupportedFormat format;
    format.width = 640;
    format.height = 480;
    format.fourcc = V4L2_PIX_FMT_YUYV;
    format.frameRates.push_back(30);
    supported_formats.push_back(format);
    return supported_formats;
}

const DeviceInfos FakeCameraDelegate::GetCameraDeviceInfos() {
    DeviceInfos device_infos;
    DeviceInfo device_info;
    device_info.devicePath = "/dev/fake_video";
    device_info.vid = "0";
    device_info.pid = "0";
    device_infos.push_back(device_info);
    return device_infos;
}

}  // namespace android
