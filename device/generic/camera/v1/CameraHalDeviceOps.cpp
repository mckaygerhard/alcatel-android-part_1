/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Common.h"
#include "CameraHalDeviceOps.h"

#include "CameraClient.h"

#include <hardware/camera_common.h>

namespace android {

static CameraClient* get_camera_client(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = reinterpret_cast<CameraClient*>(device->priv);
    if (!client) {
        HAL_LOGE("Unexpected NULL client device provided");
    }
    return client;
}

static int set_preview_window(struct camera_device* device,
                              struct preview_stream_ops* window) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->setPreviewWindow(window);
}

static void set_callbacks(
        struct camera_device* device,
        camera_notify_callback notify_cb,
        camera_data_callback data_cb,
        camera_data_timestamp_callback data_cb_timestamp,
        camera_request_memory get_memory,
        void* user) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return;
    }
    client->setCallbacks(notify_cb, data_cb, data_cb_timestamp, get_memory, user);
}

static void enable_msg_type(struct camera_device* device, int32_t msg_type) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return;
    }
    client->enableMsgType(msg_type);
}

static void disable_msg_type(struct camera_device* device, int32_t msg_type) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return;
    }
    client->disableMsgType(msg_type);
}

static int msg_type_enabled(struct camera_device* device, int32_t msg_type) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->msgTypeEnabled(msg_type);
}

static int start_preview(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->startPreview();
}

static void stop_preview(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return;
    }
    client->stopPreview();
}

static int preview_enabled(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->previewEnabled();
}

static int store_meta_data_in_buffers(struct camera_device* device, int enable) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->storeMetaDataInBuffers(enable);
}

static int start_recording(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->startRecording();
}

static void stop_recording(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return;
    }
    client->stopRecording();
}

static int recording_enabled(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->recordingEnabled();
}

static void release_recording_frame(struct camera_device* device,
                                    const void* opaque) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return;
    }
    client->releaseRecordingFrame(opaque);
}

static int auto_focus(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->autoFocus();
}

static int cancel_auto_focus(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->cancelAutoFocus();
}

static int take_picture(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->takePicture();
}

static int cancel_picture(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->cancelPicture();
}

static int set_parameters(struct camera_device* device, const char* parms) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->setParameters(parms);
}

static char* get_parameters(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return NULL;
    }
    return client->getParameters();
}

static void put_parameters(struct camera_device* device, char* params) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return;
    }
    client->putParameters(params);
}

static int send_command(struct camera_device* device,
                        int32_t cmd, int32_t arg1, int32_t arg2) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->sendCommand(cmd, arg1, arg2);
}

static void release(struct camera_device* device) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return;
    }
    client->release();
}

static int dump(struct camera_device* device, int fd) {
    HAL_LOG_ENTER();
    CameraClient* client = get_camera_client(device);
    if (!client) {
        return -EINVAL;
    }
    return client->dump(fd);
}

// In frameworks/av/services/camera/libcameraservice/api1/CameraClient.*, it uses mutex to avoid
// calling two callbacks in camera_device_ops_t simultaneously.
camera_device_ops_t g_camera_device_ops = {
    set_preview_window,
    set_callbacks,
    enable_msg_type,
    disable_msg_type,
    msg_type_enabled,
    start_preview,
    stop_preview,
    preview_enabled,
    store_meta_data_in_buffers,
    start_recording,
    stop_recording,
    recording_enabled,
    release_recording_frame,
    auto_focus,
    cancel_auto_focus,
    take_picture,
    cancel_picture,
    set_parameters,
    get_parameters,
    put_parameters,
    send_command,
    release,
    dump
};

}  // namespace android
