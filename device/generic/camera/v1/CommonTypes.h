/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_COMMON_TYPES_H
#define ANDROID_DEVICE_CAMERA_COMMON_TYPES_H

#include <string>
#include <vector>

namespace android {

// The types in this file should match ChromeOS.

struct DeviceInfo {
    std::string devicePath;
    std::string vid; // USB vender id
    std::string pid; // USB product id
};

typedef std::vector<DeviceInfo> DeviceInfos;

struct SupportedFormat {
    int width;
    int height;
    uint32_t fourcc;
    // All the supported frame rates in fps with given width, height, and
    // pixelformat. This is not sorted. For example, suppose width, height, and
    // fourcc are 640x480 YUYV. If frameRates are 15.0 and 30.0, the camera
    // supports outputting  640X480 YUYV in 15fps or 30fps.
    std::vector<float> frameRates;
};

typedef std::vector<SupportedFormat> SupportedFormats;

}  // namespace android

#endif
