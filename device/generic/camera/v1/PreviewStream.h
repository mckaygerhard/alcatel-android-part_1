/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_DEVICE_CAMERA_PREVIEW_STREAM_H
#define ANDROID_DEVICE_CAMERA_PREVIEW_STREAM_H

#include <hardware/camera.h>
#include <utils/Timers.h>

namespace android {

struct CapturedFrame;

// Delegate class of preview stream ops.
//
// The class is not thread-safe. Thread-safety should be implemented on the caller side
// (e.g., CameraClient).
class PreviewStream {
public:
    PreviewStream();
    ~PreviewStream() {}

    // Callbacks for camera_device_ops_t:
    int setPreviewWindow(struct preview_stream_ops* ops);

    // Callback when a new frame is ready:
    void onDeviceNextFrameAvailable(const CapturedFrame& frame, nsecs_t timestamp);

private:
    preview_stream_ops* mOps;

    // Current dimension of preview_stream_ops.set_buffers_geometry().
    int mGeometryWidth;
    int mGeometryHeight;
};

}  // namespace android

#endif
